/*
 * HReporteAnticiposPagados.java
 *
 * Created on 19 octubre de 2006, 03:53 PM
 */

package com.tsp.operation.model.threads;
import com.aspose.cells.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;
/**
 *
 * @author  ffernandez
 */
public class HReporteEquiposTrailers extends Thread{
    String user;
     String procesoName;
    String des;
    //String dstrct;
    Model model = new Model();
    private String Mensaje;
 
    public void start(String usuario){
        this.user = usuario;
        this.procesoName = "Reporte Equipos Trailers";
        this.des = "Reporte Equipo trailers";
        //this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
            Vector vector = new Vector(); 
            model.reporteGeneralService.ReoporteEquipo();
            vector = model.reporteGeneralService.getVector();
            if(vector.size()>=0){
                 this.setMensaje("no");
            }else{
                this.setMensaje("ok");
            }
            
            
            Date fecha = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String now = format.format(fecha);
            String fechaCrea = now.substring(0,10);

            Workbook libro = new Workbook();

            Worksheets hojas = libro.getWorksheets();

            Worksheet hoja = hojas.getSheet(0);

            hoja.setName("HOJA1");

            Font fuente = new Font();
            fuente.setBold(true);
            fuente.setName("arial");
            
            //CELDA TITULO
            Cell celda = hoja.getCell(1,1);
            Style estiloEmpresa = libro.createStyle();
            estiloEmpresa.setColor( com.aspose.cells.Color.GRAY);
            celda.setValue("TRASPORTES SANCHEZ POLO ");
            celda.setStyle(estiloEmpresa);
            estiloEmpresa.setFont ( fuente ); 
            estiloEmpresa.getBorderColor(2);
            //hoja.autoFitColumns();
            
           
            //CELDA FECHA INICIO
               
            int columnas = 0;
            int filas = 5;
            
              
            Style estiloDatos = libro.createStyle();
            estiloDatos.setColor(com.aspose.cells.Color.GRAY);
            estiloDatos.setFont ( fuente ); 
            
           
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("ITEMS");
            celda.setStyle(estiloDatos);
            
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("PLACA DEL TRAILER");
            celda.setStyle(estiloDatos);
            
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("No_INTERNO");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("EQUIPO");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("CABEZOTE");
            celda.setStyle(estiloDatos);
            
            
             celda = hoja.getCell(filas, columnas++);
            celda.setValue("WG CABEZOTE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("OT");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("OC");
            celda.setStyle(estiloDatos);
            hoja.setColumnWidth( columnas,12 );
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("FECHA DE DESPACHO");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("ORIGEN OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("DESTINO OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("FECHA CUMPLIDO");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("FECHA ULTIMO REPORTE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("CODIGO CLIENTE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("NOMBRE CLIENTE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("WG EQUIPO TRAILER");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("OBSERVACIONES DE TRAFICO");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("ORIGEN OT");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("DESTINO OT");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("ZONA DE DISPONIBILIDAD");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("F_D");
            celda.setStyle(estiloDatos);
            
             columnas = 1;
             int contador = 1;
             String RemesaAnt= "";
             String RemesaAct= "";
             String valorFact= "";
            for(int i=0; i<vector.size();i++ ){
                RemesaAct= "";
                columnas =0;
                valorFact = "";
                filas++ ;
          
                ReporteEquipos equipo = (ReporteEquipos) vector.get(i);
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+contador);
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getPlacaTrailer());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getNoInterno());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getEquipo());
                 
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getCabezote());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getWgEquipo_cabezote());
                                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getOT());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getOc());
                
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getFecha_despacho());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getOrigen_oc());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getDestino_oc());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getFecha_cumplido());
                                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+ equipo.getFecha_ult_reporte());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getCliente());
                
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getNombreCliente());
                
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getWgEquipo());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getObservaciones_trafico());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getOrigen_ot());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getDestino_ot());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getZona_disponibilidad());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+equipo.getF_D());
                
                 
                 contador++;
                 
            }
         
            //hoja.autoFitColumns();
           
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
            libro.save(Ruta1 +"ReporteEquipoTrailers" + hoy + ".xls");
             
        
          

          
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
   
    public static void main(String[]sfhgsd) throws Exception{
        HReporteEquiposTrailers h = new HReporteEquiposTrailers();
        h.start("HOSORIO");
    }
    
    /**
     * Getter for property Mensaje.
     * @return Value of property Mensaje.
     */
    public java.lang.String getMensaje() {
        return Mensaje;
    }
    
    /**
     * Setter for property Mensaje.
     * @param Mensaje New value of property Mensaje.
     */
    public void setMensaje(java.lang.String Mensaje) {
        this.Mensaje = Mensaje;
    }
    
}
