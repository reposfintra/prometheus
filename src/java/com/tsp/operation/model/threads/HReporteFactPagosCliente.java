/*
 * HReporteAnticiposPagados.java
 *
 * Created on 19 octubre de 2006, 03:53 PM
 */

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.factura.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;
/**
 *
 * @author  ffernandez
 */
public class HReporteFactPagosCliente extends Thread{
    String user;
    String FECI;
    String FECF;
    String procesoName;
    String des;
    String codcli;
    //String dstrct;
   Model model= null ;
    
    public void start(String FecI, String fecF,String usuario, String codcli,String dstrct){
        this.user = usuario;
        this.procesoName = "Reporte factura pago Cliente";
        this.des = "Reporte Documentos factura pago Cliente";
        this.FECI = FecI;
        this.FECF = fecF;
        this.codcli = codcli;
        model = new Model(dstrct);    
        //this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
            model.facturaService.ReportePagosClientes(FECI, FECF, codcli);            
            Vector vec =  model.facturaService.getVector();
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
            
            POIWrite xls = new POIWrite(Ruta1 +"ReportePagosCliente" + hoy + ".xls", user, Util.getFechaActual_String(5));
            
            HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero      = xls.nuevoEstilo("Book Antiqua", 9, false , false, "###,##"         , xls.NONE , xls.NONE , xls.NONE );
            
            HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.GOLD.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle encabezado      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.LIGHT_BLUE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            
            
                     
            xls.obtenerHoja("REPORTE");
            xls.cambiarMagnificacion(4,4);
            
            // cabecera
            factura rep1 = (factura) vec.get(0);   
            
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", encabezado);
            xls.combinarCeldas(0, 0, 0, 2);
            xls.adicionarCelda(1,0, "REPORTE DE PAGOS REALIZADOS POR  "+rep1.getCodcli(), encabezado);
            xls.combinarCeldas(1, 0, 0, 2);
            xls.adicionarCelda(2,0, "FECHA DEL REPORTE:  "+com.tsp.util.Util.getFechaActual_String(4), encabezado);
            xls.combinarCeldas(2, 0, 0, 2);
            xls.adicionarCelda(3,0, "RANGO DEL REPORTE:  DE: "+FECI+" HASTA: "+FECF, encabezado);
            xls.combinarCeldas(3, 0, 0, 2);
            
           
           // subtitulos            
            
            int fila = 7;
            int col  = 0;
           
            xls.adicionarCelda(fila ,col++ , "ITEM"     , titulo ); 
            xls.adicionarCelda(fila ,col++ , "No FACTURA "     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA DE FACTURA "     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA DE VENCIMIENTO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "DIF EN DIAS"     , titulo );
            xls.adicionarCelda(fila ,col++ , "VALOR DE FACTURA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "VALOR CANCELADO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "SALDO PENDIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA DE PAGO "     , titulo );              
            xls.adicionarCelda(fila ,col++ , "N� INGRESO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "VALOR INGRESO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA INGRESO"     , titulo );            
            xls.adicionarCelda(fila ,col++ , "NC. APLICADAS"     , titulo );
            xls.adicionarCelda(fila ,col++ , "VALOR NC"     , titulo );
            xls.adicionarCelda(fila ,col++ , "MONEDA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "BANCO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "SUCURSAL"     , titulo );
           
                       
           int pp = 1; 
           for(int i=0; i<vec.size();i++ ){
                fila++;
                col = 0;
                factura rep = (factura) vec.get(i);
                xls.adicionarCelda(fila ,col++ ,pp, texto );
                xls.adicionarCelda(fila ,col++ ,rep.getDocumento(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_factura() , texto );
                if (!rep.getFecha_vencimiento().equals("099-01-01")){
                    xls.adicionarCelda(fila ,col++ ,rep.getFecha_vencimiento() , texto );
                }else{
                    xls.adicionarCelda(fila ,col++ ,"" , texto );
                }
                xls.adicionarCelda(fila ,col++ ,rep.getPlazo(), numero );
                if (!rep.getDocumento().equals("")){
                    xls.adicionarCelda(fila ,col++ ,rep.getValor_factura() , numero );
                    xls.adicionarCelda(fila ,col++ ,rep.getValor_abonome() , numero );
                    xls.adicionarCelda(fila ,col++ ,rep.getValor_saldome() , numero );
                }else{
                    xls.adicionarCelda(fila ,col++ ,"" , texto );
                    xls.adicionarCelda(fila ,col++ ,"" , texto );
                    xls.adicionarCelda(fila ,col++ ,"" , texto );
                }
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_ultimo_pago() , texto );              
                xls.adicionarCelda(fila ,col++ ,rep.getNum_ingreso(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_abono() , numero );//se obtiene el valor del ingreso no el saldo.
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_ingreso(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getTipo_documento() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_saldo() , numero );
                xls.adicionarCelda(fila ,col++ ,rep.getMoneda() , texto );         
                xls.adicionarCelda(fila ,col++ ,rep.getBanco(), texto );         
                xls.adicionarCelda(fila ,col++ ,rep.getSucursal(), texto );         
                
                
              pp = pp + 1; 
           }
           			
            
            xls.cerrarLibro();
            //System.out.println("cerro proceso");
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
   
    
}
