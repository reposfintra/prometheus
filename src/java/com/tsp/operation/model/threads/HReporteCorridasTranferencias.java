/*
 * HReporteAnticiposPagados.java
 *
 * Created on 19 octubre de 2006, 03:53 PM
 */

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;
/**
 *
 * @author  ffernandez
 */
public class HReporteCorridasTranferencias extends Thread{
    String user;
    String Corridas1;
    String Nit1;
    String Propietario1;
    String procesoName;
    String des;
     //String dstrct;
    Model model = new Model();
    
    public void start(String Corridas, String Propietario,String usuario){
        this.user = usuario;
        this.procesoName = "Reporte Corridas Transferencias";
        this.des = "Reporte Documentos Corridas de Transferencias";
        this.Corridas1 = Corridas;
        this.Propietario1 = Propietario;
               
        //this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
           model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
           Vector vec = model.corridaService.getVector();
           Vector datos = model.corridaService.getVector();
           Vector datos2 = model.corridaService.getVectorValor();
           double valorTemp = 0;
           double convertir = 0;
           if(Propietario1.equals("") && !Corridas1.equals("")){
                   model.corridaService.busquedaXHiloCorridas(Corridas1);
                   vec = model.corridaService.getVector();
                }   
                
                if(!Propietario1.equals("") && Corridas1.equals("")){ 
                   model.corridaService.busquedaXPropietario(Propietario1);
                   vec = model.corridaService.getVector();
                  
                   /*valor total*/
                   /*****************************************************/
                   model.corridaService.SumaValorCorridas(Propietario1);
                   datos2 = model.corridaService.getVectorValor();
                 }
                
                if(!Propietario1.equals("") && !Corridas1.equals("")){
                    model.corridaService.busquedaXPropietario(Propietario1);
                    vec = model.corridaService.getVector();
                  
                   /*valor total*/
                   /*****************************************************/
                   model.corridaService.SumaValorCorridas(Propietario1);
                   datos2 = model.corridaService.getVectorValor();
                }
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteCorridasTransferencias" + hoy + ".xls", user, Util.getFechaActual_String(5));
            
            HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero      = xls.nuevoEstilo("Book Antiqua", 9, false , false, "#.##"         , xls.NONE , xls.NONE , xls.NONE );
            
            HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.LIGHT_BLUE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle encabezado      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.WHITE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            
            
            /****************************************
             *    compos de la tabla en agencia     *
             ****************************************/
            
            xls.obtenerHoja("REPORTE");
            xls.cambiarMagnificacion(4,4);
            
            // cabecera
            BeanGeneral prom = (BeanGeneral) vec.lastElement();
             for(int z=0; z<vec.size();z++ ){
               BeanGeneral leer = (BeanGeneral) vec.get(z);
               convertir = Double.parseDouble(leer.getValor_11());
               valorTemp = valorTemp + convertir;
           } 
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", encabezado);
            xls.combinarCeldas(0, 0, 0, 2);
            xls.adicionarCelda(1,0, "REPORTE DE CORRIDAS POR TRANSFERENCIAS "+ prom.getValor_01() , encabezado);
            xls.combinarCeldas(1, 0, 0, 2);
            xls.adicionarCelda(2,4, "TOTAL DE VALOR = "+ Util.customFormat(valorTemp), titulo);
             
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila ,col++ , "ITEM"     , titulo ); 
            xls.adicionarCelda(fila ,col++ , "CORRIDA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CEDULA DEL BENEFICIARIO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "NOMBRE DEL BENEFICIARIO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "BANCO DE TRANFERENCIAS"     , titulo );
            xls.adicionarCelda(fila ,col++ , "SUCURSAL DEL BANCO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TIPO DE PAGO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "VALOR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TIPO DE CUENTA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CEDULA DE LA CUENTA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "NUMERO DE LA CUENTA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "NOMBRE DE LA CUENTA"     , titulo );
          
            
           for(int i=0; i<vec.size();i++ ){
                fila++;
                col = 0;
                BeanGeneral rep = (BeanGeneral) vec.get(i);
                xls.adicionarCelda(fila ,col++ ,rep.getValor_13(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_01(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_02() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_03() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_04() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_05() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_06() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_11(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_09() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_08() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_07() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_10() , texto );
                
               
           }
           			
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    public static void main(String a []){
        HReporteCorridasTranferencias proc = new HReporteCorridasTranferencias();
        //proc.start("2006-09-05","2006-09-17","LREALES","BQ");
        
    }
    
}
