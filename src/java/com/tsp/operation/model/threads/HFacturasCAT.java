/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.threads;

import com.tsp.operation.model.DAOS.Tipo_impuestoDAO;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Tipo_impuesto;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

/**
 *
 * @author geotech
 */
/**
 *
 * @author Ing. Iris Vargas
 */
public class HFacturasCAT extends Thread {

    Model model;
    Usuario usuario;
    private String ciclo="";
    private String periodo="";
    
    public HFacturasCAT(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public HFacturasCAT(Usuario usuario, String ciclo, String periodo) {
        this.usuario = usuario;
        this.ciclo = ciclo;
        this.periodo = periodo;
    }

    public synchronized void run() {

        try {

            model = new Model(usuario.getBd());

            ArrayList<BeanGeneral> list = (ciclo.equalsIgnoreCase("") || periodo.equalsIgnoreCase("")) ? model.Negociossvc.datosCxcCat() : model.Negociossvc.datosCxcCat(ciclo, periodo);
            TransaccionService tService = new TransaccionService(usuario.getBd());

            for (int i = 0; i < list.size(); i++) {
                Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(usuario.getBd());
                Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(list.get(i).getValor_09(), list.get(i).getValor_06(), list.get(i).getValor_12());
                
                tService.crearStatement();
                String documento = model.Negociossvc.UpCP(list.get(i).getValor_01());

                double ivacat=(Double.parseDouble( list.get(i).getValor_05())*impuesto.getPorcentaje1())/(100+impuesto.getPorcentaje1());
                double catneto=Double.parseDouble( list.get(i).getValor_05())-ivacat;
                
                list.get(i).setValor_19(usuario.getLogin());

                tService.getSt().addBatch(model.Negociossvc.ingresarCXCMicrocreditoPost(list.get(i),Double.parseDouble( list.get(i).getValor_05()), documento));
                tService.getSt().addBatch(model.Negociossvc.ingresarDetalleCXCMicrocreditoPost(list.get(i), catneto, documento, "1"));
                list.get(i).setValor_08(impuesto.getCod_cuenta_contable());
                tService.getSt().addBatch(model.Negociossvc.ingresarDetalleCXCMicrocreditoPost(list.get(i), ivacat, documento, "2"));               
                tService.getSt().addBatch(model.Negociossvc.updateDocumentoCat(documento, list.get(i).getValor_13(), list.get(i).getValor_07()));
                tService.execute();

                System.out.println("CXC CAT DEL NEGOCIO " + list.get(i).getValor_13() + " INSERTADA.");
            }



        } catch (Exception e) {

            System.out.print("ERROR Hilo: " + e.getMessage());

        }


    }
}
