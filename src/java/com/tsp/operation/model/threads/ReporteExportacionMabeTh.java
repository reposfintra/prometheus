/*
 * reporteImportacionesMateriaPrima.java
 *
 * Created on 2 de agosto de 2006, 08:26 AM
 */
/******************************************************************************
 * Nombre clase :                   reporteImportacionesMateriaPrima.java     *
 * Descripcion :                    Genera reporte importaciones Materia Prima*
 * Autor :                          David Pi�a Lopez                          *
 * Fecha :                          2 de agosto de 2006, 08:26 AM             *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import java.sql.SQLException;

import org.apache.log4j.*;

/**
 *
 * @author  David
 */
public class ReporteExportacionMabeTh extends Thread {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private Model model;
    private String cliente;
    private String tipodoc;
    private String usuario;
    private String[] docs;
    
    /** Creates a new instance of reporteImportacionesMateriaPrima */
    public ReporteExportacionMabeTh() {
        model = new Model();
    }
    
    public void start( String usuario, String[] docs ) {
        //this.cliente = cliente;
        //this.tipodoc = tipodoc;
        this.usuario = usuario;
        this.docs = docs;
        super.start();        
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso( "Reporte Exportacion MABE", this.hashCode(), "Reporte Exportacion MABE", usuario );
            POIWrite pw = new POIWrite();            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta  = path + "/exportar/migracion/" + usuario + "/";
            
            
            
            for( int i = 0; i < this.docs.length; i++ ){
                
                StringTokenizer stoken = new StringTokenizer(docs[i],"|");
                
                String dstrct = stoken.nextToken();
                String tipo_doc = stoken.nextToken();
                String documento = stoken.nextToken();
                
                logger.info("DSTRCT: " + dstrct);
                logger.info("TIPO_DOC: " + tipo_doc);
                logger.info("DOCUMENTO: " + documento);
                logger.info("ARCHIVO: " + Ruta + /*cab.get( "nombrecliente" ) + " " +*/ documento + ".xls");
                
                
                pw.nuevoLibro( Ruta + /*cab.get( "nombrecliente" ) + " " +*/ documento + ".xls" );
                
                HSSFCellStyle estiloCabecera = pw.nuevoEstilo("Arial", 14, true, false, "", HSSFColor.YELLOW.index, HSSFColor.DARK_BLUE.index, HSSFCellStyle.ALIGN_CENTER, HSSFCellStyle.BORDER_MEDIUM );
                HSSFCellStyle estiloDetalle0 = pw.nuevoEstilo("Arial", 10, true, false, "", HSSFColor.YELLOW.index, HSSFColor.DARK_BLUE.index, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.BORDER_MEDIUM );                
                HSSFCellStyle estiloDetalle1 = pw.nuevoEstilo("Arial", 10, true, false, "", HSSFColor.LIGHT_YELLOW.index, HSSFColor.RED.index, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.BORDER_MEDIUM );
                HSSFCellStyle estiloDetalle2 = pw.nuevoEstilo("Arial", 10, false, false, "", HSSFColor.BLACK.index, HSSFColor.WHITE.index, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.BORDER_MEDIUM );
                HSSFCellStyle estiloDetalle3 = pw.nuevoEstilo("Arial", 10, true, false, "", HSSFColor.YELLOW.index, HSSFColor.DARK_BLUE.index, HSSFCellStyle.ALIGN_LEFT, HSSFCellStyle.BORDER_MEDIUM );
                
                boolean faltantes = false;
                model.impoExpoService.obtenerExpoCliente( dstrct, tipo_doc, documento);
                Vector detalles = model.impoExpoService.getVector();
                
                
                pw.obtenerHoja( "REPORTE" );
                
                pw.cambiarAnchoColumna( 1, 10000 );
                pw.cambiarAnchoColumna( 2, 10000 );
                
                pw.combinarCeldas( 1, 1, 1, 2 );
                pw.adicionarCelda( 1, 1, "EXPORTACIONES", estiloCabecera );
                pw.adicionarCelda( 1, 2, "", estiloCabecera );
                
                pw.adicionarCelda( 3, 1, "N. EXPORTACION", estiloDetalle0 );
                
                pw.adicionarCelda( 6, 1, "CIUDAD CARGUE DEL CONTENEDOR", estiloDetalle0 );
                pw.adicionarCelda( 7, 1, "FECHA DE CARGUE CONTENEDOR", estiloDetalle0 );
                pw.adicionarCelda( 8, 1, "N. DEL CONTENEDOR", estiloDetalle0 );
                
                pw.adicionarCelda( 10, 1, "PLACAS DE VEH", estiloDetalle0 );
                pw.adicionarCelda( 11, 1, "NOMBRE DE CONDUCTOR", estiloDetalle0 );
                pw.adicionarCelda( 12, 1, "CEDULA CONDUCTOR", estiloDetalle0 );
                
                pw.adicionarCelda( 14, 1, "FECHA DE CARGUE DE EXPORTACION", estiloDetalle0 );
                pw.adicionarCelda( 15, 1, "UTILIZACION DE ESCOLTA", estiloDetalle0 );
                pw.adicionarCelda( 16, 1, "FECHA DE DESCARGUE EN PUERTO", estiloDetalle0 );
                
                pw.adicionarCelda( 18, 1, "OT", estiloDetalle1 );
                //pw.adicionarCelda( 19, 1, "OT. REPOSICION", estiloDetalle1 );
                pw.adicionarCelda( 19, 1, "No. FACTURA", estiloDetalle1 );
                pw.adicionarCelda( 20, 1, "FECHA EMISION FACTURA TSP", estiloDetalle1 );
                //pw.adicionarCelda( 22, 1, "VALOR POR REPOSICION", estiloDetalle1 );
                //pw.adicionarCelda( 23, 1, "VALOR POR TRANSPORTE", estiloDetalle1 );
                pw.adicionarCelda( 21, 1, "VALOR TOTAL FACTURA", estiloDetalle1 );
                
                pw.adicionarCelda( 24, 1, "OBSERVACIONES:", estiloDetalle3 );
                                
                int c = 2;
                                
                for( int j = 0; j < detalles.size(); j++ ){
                    Hashtable det = (Hashtable)detalles.get( j );
                    logger.info("DATOS: " + det);
                    //pw.obtenerHoja( det.get( "documento_rel" ) + "-" + det.get( "planilla" ) );
                    
                    //String documento = "" + documento;                    
                    String origenviaje = ""+det.get( "origenviaje" );
                    if( origenviaje.equals("") )faltantes = true;                    
                    String contenedores = ""+det.get( "contenedores" );
                    if( contenedores.equals("") )faltantes = true;                   
                    String conductor = ""+det.get( "conductor" );
                    if( conductor.equals("") )faltantes = true;
                    String placa = ""+det.get( "placa" );
                    if( placa.equals("") )faltantes = true;
                    String horasalida = ""+det.get( "hora_salida" );
                    if( horasalida.equals("0099-01-01") ){horasalida="";faltantes = true;}
                    
                    String escolta = ""+det.get( "escolta" );                    
                    String fechafactura = ""+det.get( "fechafactura" );
                    if( fechafactura.equals("0099-01-01") ){fechafactura="";faltantes = true;}
                    String nofactura = "" + det.get( "nfactura" );
                    String moneda_fra = "" + det.get( "fra_moneda" );
                    String valortransporte = det.get( "vr_transp" )!=null ? "$ " + UtilFinanzas.customFormat2(Double.parseDouble( "" + det.get( "vr_transp" ) ) ) + " " + moneda_fra : "0";
                    
                    String fecpla_2daoc = "" + det.get("fec_2daoc");
                    String fecEntregaF_2daOC = "" + det.get("fecEcl_2doc"); 
                    
                    logger.info("fecpla_2daoc: " + fecpla_2daoc);
                    logger.info("fecEntregaF_2daOC: " + fecEntregaF_2daOC);
                    if( fecEntregaF_2daOC.equals("0099-01-01") ){fecEntregaF_2daOC = "";} 
                    
                    String cedcon = "" + det.get("cedcon"); 
                    String ot_transp = "" + det.get("documento_rel");                

                    pw.adicionarCelda( 3, c, documento, estiloDetalle2 );
                    
                    pw.adicionarCelda( 6, c, origenviaje, estiloDetalle2 );
                    pw.adicionarCelda( 7, c, horasalida, estiloDetalle2 ); //AMATURANA 20.12.2006
                    pw.adicionarCelda( 8, c, contenedores, estiloDetalle2 );
                    
                    pw.adicionarCelda( 10, c, placa, estiloDetalle2 );
                    pw.adicionarCelda( 11, c, conductor, estiloDetalle2 );
                    pw.adicionarCelda( 12, c, cedcon, estiloDetalle2 );
                                        
                    pw.adicionarCelda( 14, c, fecpla_2daoc, estiloDetalle2 );
                    pw.adicionarCelda( 15, c, escolta, estiloDetalle2 );
                    pw.adicionarCelda( 16, c, fecEntregaF_2daOC, estiloDetalle2 );
                    
                                        
                    pw.adicionarCelda( 18, c, ot_transp, estiloDetalle2 );
                    //pw.adicionarCelda( 19, c, ot_repos, estiloDetalle2 );         
                    pw.adicionarCelda( 19, c, nofactura, estiloDetalle2 );
                    pw.adicionarCelda( 20, c, fechafactura, estiloDetalle2 );
                    //pw.adicionarCelda( 22, c, "$ " + UtilFinanzas.customFormat( valordevolucion ) + " " + moneda_fra, estiloDetalle2 );
                    //pw.adicionarCelda( 23, c, "$ " + UtilFinanzas.customFormat( valortransporte ) + " " + moneda_fra, estiloDetalle2 );                    
                    pw.adicionarCelda( 21, c, valortransporte, estiloDetalle2 ); 
                                        
                    pw.adicionarCelda( 24, c, "", estiloDetalle2 );
                    
                    c++;
                }
                if( !faltantes ){
                    //model.impoExpoService.actualizarGeneracion( dstrct, tipo_doc, documento, usuario );
                }
                pw.cerrarLibro();
            }
            
            model.LogProcesosSvc.finallyProceso( "Reporte Exportacion MABE", this.hashCode(), usuario, "PROCESO EXITOSO" );
        }catch( Exception ex ){
            ex.printStackTrace();            
            //Capturo errores finalizando proceso
            try{                
                model.LogProcesosSvc.finallyProceso( "Reporte Exportacion MABE", this.hashCode(), usuario, "ERROR : " + ex.getMessage() );            
            } catch( Exception f ){                
                f.printStackTrace();
                try{                    
                    model.LogProcesosSvc.finallyProceso( "Reporte Exportacion MABE",this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                } catch( Exception p ){ p.printStackTrace(); }                
            }
        }
    }
    private void crearEstilos( POIWrite poi ){
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        reporteImportacionesMateriaPrima hilo = new reporteImportacionesMateriaPrima();
       // hilo.start("031", "IMP", "GPINA" );
    }
    
}

// Entregado a tito 24 febrero
