/*
 * ActualizarPlanillasPlaneacion.java
 *
 * Created on 18 de julio de 2006, 12:00 PM
 */

package com.tsp.operation.model.threads;
import java.io.*;
import jxl.write.*;
import com.tsp.operation.model.beans.JXLWrite;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
/**
 *
 * @author  dbastidas
 */
public class ActualizarPlanillasPlaneacion extends Thread{
    private String dstrct;
    private String usuario;
    private Model model = new Model();
    
    public void start(String dstrct, String Usuario){
        this.dstrct = dstrct;
        this.usuario = Usuario;
        super.start();
    }
    
    
    public synchronized void run(){
        try{
           model.LogProcesosSvc.InsertProceso("ACTUALIZAR PLANILLAS FRONTERA", this.hashCode(), "ACTUALIZAR PLANILLAS FRONTERA", this.usuario);  
           model.reqclienteServices.actualizarPlanillasPlaneacion(dstrct); 
           model.LogProcesosSvc.finallyProceso("ACTUALIZAR PLANILLAS FRONTERA", this.hashCode(), this.usuario, "PROCESO EXITOSO");
        }catch(Exception e){
           try{
                 model.LogProcesosSvc.finallyProceso("ACTUALIZAR PLANILLAS FRONTERA", this.hashCode(),this.usuario,"ERROR :" + e.getMessage()); 
             }
             catch(Exception f){ 
                try{ 
                    model.LogProcesosSvc.finallyProceso("ACTUALIZAR PLANILLAS FRONTERA",this.hashCode(),this.usuario,"ERROR :"); 
                }catch(Exception p){    }
             }
        }  
    }
    
     public static void main(String a []){
        ActualizarPlanillasPlaneacion proc = new ActualizarPlanillasPlaneacion();
        proc.start("FINV","DBASTIDAS");
    }
}
