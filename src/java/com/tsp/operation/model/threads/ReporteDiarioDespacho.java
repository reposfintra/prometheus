/*
 * ReporteDiarioDespacho.java
 *
 * Created on 5 de septiembre de 2005, 11:08 AM
 */

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  Administrador
 */
public class ReporteDiarioDespacho extends Thread{
    
    private Vector reporte;
    private String fechai;
    private String fechafin;
    private String id;
    private Vector agencias;
    private String path;
    //
    private String proceso;
    private String descripcion;
    
    Model model;
    
    /** Creates a new instance of ReporteDiarioDespacho */
    public ReporteDiarioDespacho() {
    }
    
    public void start(Model model, Vector reporte, Vector agencias,String fechai, String fechaf, String id) {
        
        this.reporte = reporte;
        this.agencias = agencias;
        this.fechai = fechai;
        this.fechafin = fechaf;
        this.id = id;
        this.proceso = "Reporte Diario De Despacho";
        this.descripcion = "Reporte Diario De Despacho";
        this.model = model;
        super.start();
        
    }
    
    public synchronized void run(){
        
        try{
            
            //Inicia el proceso
            model.LogProcesosSvc.InsertProceso(this.proceso, this.hashCode(), descripcion, this.id);
            
            //sandrameg 60905
            Util u = new Util();
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            path = rb.getString("ruta");
            
            File file = new File(path + "/exportar/migracion/"+id);
            file.mkdirs();
            
            String nombreArch= "ReporteDiarioDespacho["+fechai+"]["+fechafin+"].xls";
            String       Hoja  = "ReporteDiario";
            String       Ruta  = path + "/exportar/migracion/"+ id +"/" + nombreArch; ////System.out.println("RUTA : "+Ruta);
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for (int col=0; col<40 ; col++)
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 20 ) ) );
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x0));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x0));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.ORANGE.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            HSSFCellStyle e_agencia = wb.createCellStyle();
            e_agencia.setFont(fuente3);
            
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente3);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.BLACK.index);
            
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            
            /****************************************************/
            /*estilo format ###.##*/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setDataFormat(wb.createDataFormat().getFormat("###,##"));
            
            
            HSSFCellStyle vetado = wb.createCellStyle();
            vetado.setFont(fuente4);
            vetado.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            vetado.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            vetado.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            vetado.setBottomBorderColor(HSSFColor.BLACK.index);
            vetado.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            vetado.setLeftBorderColor  (HSSFColor.BLACK.index);
            vetado.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            vetado.setRightBorderColor(HSSFColor.BLACK.index);
            vetado.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            vetado.setTopBorderColor   (HSSFColor.BLACK.index);
            
            HSSFCellStyle aux = wb.createCellStyle();
            
            row  = sheet.createRow((short)(0));
            
            for (int j=0;j<7;j++) {
                
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
                
            }
            
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            
            
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO");
            
            
            row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("Reporte Diario de Despacho");
            
            /*************************************************************************************/
            
            /***** RECORRER LAS PLANILLAS ******/
            int Fila = 4;
            
            Iterator It = agencias.iterator();
            
            
            while(It.hasNext()) {
                
                Agencia ag = (Agencia) It.next();
                
                int cont_agencia = 0;
                
                row2  = sheet.createRow((short)( Fila - 1));
                cell = row2.createCell((short)(0));
                cell.setCellStyle(e_agencia);
                cell.setCellValue("AGENCIA " + ag.getNombre());
                
                row  = sheet.createRow((short)( Fila ));
                
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Planilla");
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Fecha de Impresion");
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Origen");
                
                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Destino");
                
                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Placa");
                
                cell = row.createCell((short)(5));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Trailer");
                
                cell = row.createCell((short)(6));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Contenedores");
                
                cell = row.createCell((short)(7));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Conductor");
                
                cell = row.createCell((short)(8));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Cliente");
                
                cell = row.createCell((short)(9));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Cantidad despachada");
                
                cell = row.createCell((short)(10));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Unidades despachadas");
                
                cell = row.createCell((short)(11));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Vacio");
                
                cell = row.createCell((short)(12));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Tipo Carga");
                
                cell = row.createCell((short)(13));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Valor planilla");
                
                cell = row.createCell((short)(14));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Moneda");
                
                cell = row.createCell((short)(15));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Valor anticipo");
                
                cell = row.createCell((short)(16));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Moneda");
                
                cell = row.createCell((short)(17));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Porcentaje anticipo maximo");
                
                cell = row.createCell((short)(18));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Remesa");
                
                cell = row.createCell((short)(19));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Standard");
                
                cell = row.createCell((short)(20));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Valor Remesa");
                
                cell = row.createCell((short)(21));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Moneda");
                
                cell = row.createCell((short)(22));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Cantidad facturada");
                
                cell = row.createCell((short)(23));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Unidad");
                
                cell = row.createCell((short)(24));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Documentos");
                
                cell = row.createCell((short)(25));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Vlr Remesa Base");
                
                cell = row.createCell((short)(26));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Vlr Remesa Prorrateado");
                
                cell = row.createCell((short)(27));
                cell.setCellStyle(estilo3);
                cell.setCellValue("% Remesa Prorrateado");
                
                cell = row.createCell((short)(28));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Vlr Planilla Base");
                
                cell = row.createCell((short)(29));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Vlr Planilla Prorrateado");
                
                cell = row.createCell((short)(30));
                cell.setCellStyle(estilo3);
                cell.setCellValue("% Planilla Prorrateado");
                
                cell = row.createCell((short)(31));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Utilidad");
                
                cell = row.createCell((short)(32));
                cell.setCellStyle(estilo3);
                cell.setCellValue("% Utilidad Bruta");
                
                cell = row.createCell((short)(33));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Despachador");
                
                /*Reporte Diario 15-11-05*/
                cell = row.createCell((short)(34));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Vlr costo extraflete");
                
                cell = row.createCell((short)(35));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Descripcion costo extraflete");
                
                cell = row.createCell((short)(36));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Vlr ingreso extraflete");
                
                cell = row.createCell((short)(37));
                cell.setCellStyle(estilo3);
                cell.setCellValue("Descripcion ingreso extraflete");
                
                Iterator It2 = reporte.iterator();
                
                while(It2.hasNext()) {
                    
                    Informe inf = (Informe)It2.next();
                    
                    if( inf.getAgencia().equals(ag.getId_agencia())){
                        
                        Fila++;
                        row  = sheet.createRow((short)(Fila));
                        if( inf.isFlag() )
                            aux  = estilo4;
                        else
                            aux = vetado;
                        
                        cont_agencia++;
                        
                        cell = row.createCell((short)(0));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getNumpla()!=null)?inf.getNumpla():"");
                        
                        cell = row.createCell((short)(1));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getFecha()!=null)?inf.getFecha():"");
                        
                        cell = row.createCell((short)(2));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getOrigen()!=null)?inf.getOrigen():"");
                        
                        cell = row.createCell((short)(3));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getDestino()!=null)?inf.getDestino():"");
                        
                        cell = row.createCell((short)(4));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getPlaca()!=null)?inf.getPlaca():"");
                        
                        cell = row.createCell((short)(5));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getPlatlr()!=null)?inf.getPlatlr():"");
                        
                        cell = row.createCell((short)(6));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getContenedores()!=null)?inf.getContenedores():"");
                        
                        cell = row.createCell((short)(7));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getConductor()!=null)?inf.getConductor():"");
                        
                        cell = row.createCell((short)(8));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getCliente()!=null)?inf.getCliente():"");
                        
                        cell = row.createCell((short)(9));
                        cell.setCellStyle(aux);
                        cell.setCellValue(inf.getTonelaje());
                        
                        cell = row.createCell((short)(10));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getUnidadesdesp()!=null)?inf.getUnidadesdesp():"");
                        
                        cell = row.createCell((short)(11));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getVacio()!=null)?inf.getVacio():"");
                        
                        cell = row.createCell((short)(12));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getTipocarga()!=null)?inf.getTipocarga():"");
                        
                        cell = row.createCell((short)(13));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getVlrpla()));
                        
                        cell = row.createCell((short)(14));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getMonedapla()!=null)?inf.getMonedapla():"");
                        
                        cell = row.createCell((short)(15));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getAnticipo()));
                        
                        cell = row.createCell((short)(16));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getMonedaant()!=null)?inf.getMonedaant():"");
                        
                        cell = row.createCell((short)(17));
                        cell.setCellStyle(aux);
                        cell.setCellValue(inf.getPorcentajemaximoant());
                        
                        cell = row.createCell((short)(18));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getRemision()!=null)?inf.getRemision():"");
                        
                        cell = row.createCell((short)(19));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getStdjob()!=null)?inf.getStdjob():"");
                        
                        cell = row.createCell((short)(20));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getVlrrem()));
                        
                        cell = row.createCell((short)(21));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getMonedarem()!=null)?inf.getMonedarem():"");
                        
                        cell = row.createCell((short)(22));
                        cell.setCellStyle(aux);
                        cell.setCellValue(inf.getCantidadfacturada());
                        
                        cell = row.createCell((short)(23));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getUnidad()!=null)?inf.getUnidad():"");
                        
                        cell = row.createCell((short)(24));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getDocumentos()!=null)?inf.getDocumentos():"");
                        
                        cell = row.createCell((short)(25));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getVlr_rem_base()));
                        
                        cell = row.createCell((short)(26));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getVlr_rem_pro()));
                        
                        cell = row.createCell((short)(27));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getPor_rem_por()));
                        
                        cell = row.createCell((short)(28));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getVlr_pla_base()));
                        
                        cell = row.createCell((short)(29));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getVlr_pla_pro()));
                        
                        cell = row.createCell((short)(30));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getPor_pla_por()));
                        
                        cell = row.createCell((short)(31));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getUtilidadbruta()));
                        
                        cell = row.createCell((short)(32));
                        cell.setCellStyle(aux);
                        cell.setCellValue(u.customFormat(inf.getPor_utilidad_bruta()));
                        
                        cell = row.createCell((short)(33));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getDespachador()!=null)?inf.getDespachador():"");
                        
                        cell = row.createCell((short)(34));
                        cell.setCellStyle(aux);
                        cell.setCellValue(inf.getVlrcostoflete());
                        
                        cell = row.createCell((short)(35));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getCostoflete()!=null)?inf.getCostoflete():"");
                        
                        cell = row.createCell((short)(36));
                        cell.setCellStyle(aux);
                        cell.setCellValue(inf.getVlringresoflete());
                        
                        cell = row.createCell((short)(37));
                        cell.setCellStyle(aux);
                        cell.setCellValue((inf.getIngresoflete()!=null)?inf.getIngresoflete():"");
                        
                    }//fin de Validacion de Agencias
                    
                }//Fin de Registros
                
               /* if( cont_agencia == 0 ){
                    row = sheet.getRow(Fila);
                    sheet.removeRow(row);
                    row = sheet.getRow(Fila - 1);
                    sheet.removeRow(row);                    
                }*/
                row  = sheet.createRow((short)(Fila++));
                Fila+=2;
                
            }//Fin de Agencias
            
            
            
            /************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream(Ruta);
            wb.write(fo);
            fo.close();
            
            //Finalizo el proceso exitosamente
            model.LogProcesosSvc.finallyProceso(this.proceso, this.hashCode(), this.id, "PROCESO EXITOSO");
            
        } catch(Exception e){
            
            e.printStackTrace();
            
            //Capturo errores finalizando proceso
            try{
                
                model.LogProcesosSvc.finallyProceso(this.proceso, this.hashCode(),this.id,"ERROR : " + e.getMessage());
                
            } catch(Exception f){
                
                f.printStackTrace();
                
                try{
                    
                    model.LogProcesosSvc.finallyProceso(this.proceso,this.hashCode(),this.id,"ERROR : " + f.getMessage());
                    
                }catch(Exception p){    }
                
            }
            
        } finally{
            
            super.destroy();
            
        }
        
    }
    
}
