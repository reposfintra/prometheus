/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;


import java.util.*;
import java.text.*;
import java.io.*;

import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.services.ConstanteService;
import com.tsp.util.LogWriter;
import com.tsp.util.Util;
import com.aspose.cells.License;
import com.aspose.cells.*;





import java.io.File;
import java.util.List;













/**
 *
 * @author Alvaro
 */
public class HRecaudoElectrificadora extends Thread {
    

    private Model   model;
    private Usuario usuario;
    private String  dstrct;
    private List    listaArchivos;
    private LogWriter logWriter;
    


    // Variables
    private SimpleDateFormat fmt;


    // Constantes
    final String  NOMBRE_LIBRO = "Estado Informe_de_Recaudo_MS_Junio_2010";
    final String  ETIQUETA_HOJA = "Recaudo_Multiservicios";
    final String  ETIQUETA_HOJA_AIRES = "Recaudo_aires";
    final int     LINEA_TITULO_COLUMNA = 1;




    // -------------------------------------------------------------------------
    // DEFINICION PARA USAR EXCEL
    // Variables del archivo de excel

    AsposeUtil xlsUtil = new AsposeUtil();
    Workbook   wb;
    String     rutaInformes;
    String     nombre;

    int fila = 0;
    // FIN DEFINICION PARA USAR EXCEL
    // -------------------------------------------------------------------------






    
        /** Creates a new instance of HPrefacturaDetalle */
    public HRecaudoElectrificadora () {
    }

    public void start(Model model, Usuario usuario, String distrito,  List listaArchivos, LogWriter logWriter ){

        this.usuario        = usuario;
        this.model          = model;
        this.dstrct         = distrito;
        this.listaArchivos  = listaArchivos;
        this.logWriter      = logWriter;

        super.start();
    }

    
    


    public synchronized void run(){


        try{

            java.util.Date fechaActual = new Date();
            String fechaCreacion = fechaActual.toString();

            // Leer archivo

            String item = (String) listaArchivos.get(0);
            String[] campoItem = item.split("%");
            String directorio = campoItem[0];
            String archivo = campoItem[1];

            verificarLicenciaAspose ( localizaRutaLicenciaAspose() );


            this.generarRUTA( usuario);   // crear el directorio donde va a incluirse el archivo excell

            wb = xlsUtil.abrirLibro(directorio, archivo, FileFormatType.EXCEL2003 );
 
            //Worksheet sheet = xlsUtil.obtenerHoja( wb,  ETIQUETA_HOJA);
            
            boolean finalFila = false, archivoMulti = true;
            int fila = 0;

            
            Recaudo recaudo = new Recaudo();
            String  periodo_recaudo = "";

            Worksheets worksheets = wb.getWorksheets();
            Worksheet sheet = worksheets.getSheet(ETIQUETA_HOJA);
            if (sheet == null)  {
                sheet = worksheets.getSheet(ETIQUETA_HOJA_AIRES);
                archivoMulti = false;
            } 
            worksheets = null;
            
            model.consorcioService.creaTablaRecaudo( logWriter );
            
            if(archivoMulti) {
            
            while (!finalFila) {

                recaudo.inicializar();

                fila++;
    
                periodo_recaudo =   xlsUtil.getCeldaString( sheet,  fila,  0)  ;

                if(periodo_recaudo == null  ||  periodo_recaudo.isEmpty()) {
                    finalFila = true;
                    break;
                }

                recaudo.setPeriodo_recaudo(periodo_recaudo);
                recaudo.setNom_empresa( xlsUtil.getCeldaString( sheet,  fila,  1) );
                recaudo.setNom_unicom( xlsUtil.getCeldaString( sheet,  fila,  2) );
                recaudo.setCod_unicom( xlsUtil.getCeldaString( sheet,  fila,  3) );
                recaudo.setGestor( xlsUtil.getCeldaString( sheet,  fila,  4) );
                recaudo.setNom_cli( xlsUtil.getCeldaString( sheet,  fila,  5) );
                recaudo.setNic( xlsUtil.getCeldaString( sheet,  fila,  6) );
                recaudo.setNis_rad( xlsUtil.getCeldaString( sheet,  fila,  7) );
                recaudo.setNum_acu (  Integer.parseInt(  xlsUtil.getCeldaString( sheet,  fila,  8) ) );
                recaudo.setSimbolo_var( xlsUtil.getCeldaString( sheet,  fila,  9) );
                recaudo.setF_fact( xlsUtil.getCeldaString( sheet,  fila,  10) );
                recaudo.setF_puesta( xlsUtil.getCeldaString( sheet,  fila,  11) );
                recaudo.setCo_concepto( xlsUtil.getCeldaString( sheet,  fila,  12) );
                recaudo.setDesc_concepto( xlsUtil.getCeldaString( sheet,  fila,  13) );
                recaudo.setImp_facturado_concepto( xlsUtil.getCeldaDouble( sheet,  fila,  14) );
                recaudo.setImp_pagado_concepto( xlsUtil.getCeldaDouble( sheet,  fila,  15) );
                recaudo.setImp_recaudo( xlsUtil.getCeldaDouble( sheet,  fila,  16) );


                model.consorcioService.setInsertarRecaudo(recaudo,  dstrct,  usuario.getLogin() ,  fechaCreacion,   logWriter );


                
            } // Final del while (Recorrido de filas)
            } else {
                if(sheet != null) {
                    while (!finalFila) {

                        recaudo.inicializar();

                        fila++;
                        periodo_recaudo =   xlsUtil.getCeldaString( sheet,  fila,  0)  ;

                        if(periodo_recaudo == null  ||  periodo_recaudo.isEmpty()) {
                            finalFila = true;
                            break;
                        }

                        recaudo.setPeriodo_recaudo(periodo_recaudo);
                        recaudo.setNic( xlsUtil.getCeldaString(sheet,  fila,  4) );
                        recaudo.setSimbolo_var( xlsUtil.getCeldaString(sheet,  fila,  5) );
                        recaudo.setF_fact(xlsUtil.getCeldaString( sheet,  fila,  3) );
                        recaudo.setDesc_concepto( xlsUtil.getCeldaString( sheet,  fila,  2) );
                        recaudo.setImp_recaudo(xlsUtil.getCeldaDouble( sheet,  fila,  1) );

                        model.consorcioService.setInsertarRecaudo(recaudo,  dstrct,  usuario.getLogin() ,  fechaCreacion,   logWriter );

                    }   
                }
            }

            model.consorcioService.procesarRecaudo(model, usuario.getLogin(), fechaCreacion, logWriter, archivoMulti);

    
        }catch (Exception ex){
            logWriter.log(ex.getMessage());

            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                logWriter.log(e.getMessage());
                // System.out.println("Error HFacturaContratista ...\n" + e.getMessage());
            }
        } 
    }
    
    








        /**
         * Ubica la informacion de la ruta donde quedara el informe
         * Si no existe crea el directorio
         *
         * @author  Alvaro Pabon Martinez
         * @version %I%, %G%
         * @since   1.0
         *
         */

        public void generarRUTA( Usuario usuario) throws Exception{
            try{

                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
                File archivo = new File( rutaInformes );
                if (!archivo.exists()) archivo.mkdirs();

            }catch (Exception ex){
                ex.printStackTrace();
                throw new Exception(ex.getMessage());
            }

        }



        public void verificarLicenciaAspose(String archivoLicencia) {

            FileInputStream fstream = null;
            try {
                fstream=new FileInputStream(archivoLicencia);
                License license=new License();
                license.setLicense(fstream);

            } catch (Exception e){
                System.out.println("Error al validar Licencia de Aspose ...\n"  + e.getMessage());
            }
        }



        public String localizaRutaLicenciaAspose() throws Exception{

            String rutaLicencia = "";
            try{
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                rutaLicencia =  rb.getString("rutaLicenciaAspose");
            }catch (Exception e){
                e.printStackTrace();
                throw new Exception(e.getMessage());
            }
            finally {
                return rutaLicencia;
            }
        }














    
    

}
