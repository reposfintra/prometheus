/*
 * ReporteConsultas.java
 *
 * Created on 19 de diciembre de 2005, 10:24 AM
 */

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.ReporteCumplidoAction;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
/**
 *
 * @author  dlamadrid
 */
public class ReporteConsultasXSL extends Thread
{
    
    /** Creates a new instance of ReporteConsultas */
    public ReporteConsultasXSL ()
    {
    }
    
      private String mensaje="";
        String Agencia;
        String user;
        String fecha;
        int rango_i;
        int rango_f;
        //ReportePlaRemPorFecha datos = new ReportePlaRemPorFecha();
        Model model;
        Vector vConsulta;
        Vector vCampos;
        
        public void start(Vector vConsulta,Vector vCampos,String user,String fecha){
                ////System.out.println("Aqui toy en el start");
                this.vConsulta=vConsulta;
                this.vCampos=vCampos;
                this.user=user;
                this.fecha=fecha;
                super.start();
        }
        
         public synchronized void run(){
                ////System.out.println("Aqui toy en el run");
        
                try{
                        Model model = new Model();
                        ////System.out.println(" va a setear el vector");
                        if (vConsulta!=null && vConsulta.size()>0){
                                ////System.out.println("seteo el vector");
                                Calendar FechaHoy = Calendar.getInstance();
                                Date d = FechaHoy.getTime();
                                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
                                SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
                                String FechaFormated = s.format(d);
                                String FechaFormated1 = s1.format(d);


                                //obtener cabeera de ruta
                                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                                String path = rb.getString("ruta");
                                //armas la ruta
                                String Ruta1  = path + "/exportar/migracion/" + user + "/";
                                //  String Ruta1  ="c:exportar/";
                                //crear la ruta
                                File file = new File(Ruta1);
                                file.mkdirs();

                                POIWrite xls = new POIWrite(Ruta1+"consulta"+user+".xls",user,fecha);

                                ////System.out.println("Ruta del Archivo xls " + Ruta1+"ReporteDXP-"+user+".xls");
                                HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                                HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
                                HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);

                                HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                                HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
                                HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );


                                xls.obtenerHoja("BASE");
                                xls.cambiarMagnificacion(3,4);

                                // cabecera

                                xls.adicionarCelda(0,0, "REPORTE DE GENERACION DE CONSULTAS ", header);
                                xls.combinarCeldas(0, 0, 0, 2);
                                
                                // subtitulos


                                int fila = 6;
                                int col  = 0;
                                ////System.out.println("bandera 1");
                                for(int i=0;i<vCampos.size ();i++){
                                    xls.adicionarCelda(fila ,col++ ,""+vCampos.elementAt (i), titulo );
                                }
                                
                                ////System.out.println("bandera 2");    
                                java.util.Date utilDate = new java.util.Date(); //fecha actual
                                long lnMilisegundos = utilDate.getTime();
                                java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
                                ////System.out.println("conta tama�o"+vConsulta.size ());
                                int ii=0;
                                fila++;
                                for (int i =7 ;i < vConsulta.size()+7;i++){
                                      Vector filaC =(Vector)vConsulta.elementAt(ii);
                                      ////System.out.println("fila C"+filaC.size ());
                                      int xx=0;
                                      for ( int x=0;x<filaC.size();x++)

                                      {
                                          ////System.out.println("fila en c "+filaC.elementAt(x));
                                         xls.adicionarCelda(fila,xx++ ,""+filaC.elementAt(x) , texto );
                                    }
                                      fila++;
                                      ii++;
                                }
                                ////System.out.println("bancdera 3");
                                xls.cerrarLibro();
                                ////System.out.println("cerro el libro");
                        }
                        else{
                                //ReportePlaRemPorFechaXLS.setMensaje("El reporte no Arroj� datos");
                                this.setMensaje("El reporte no Arroj� datos");
                        }

                }//Fin del try
                catch (Exception ex){
                        ////System.out.println("Error : " + ex.getMessage());
                }
        }
        
        public void setMensaje(String Mensaje){

        this.mensaje = Mensaje;

        }

        public String getMensaje(){
        return mensaje;
        }
        /*public static void main(String []args)throws Exception{
        ReportePlaRemPorFechaXLS hilo = new ReportePlaRemPorFechaXLS() ;
        hilo.start("2005-05-21","2005-05-21","CG");

        }*/  

    
}
