/*
 * HReporteAnticiposPagados.java
 *
 * Created on 19 octubre de 2006, 03:53 PM
 */

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;
/**
 *
 * @author  ffernandez
 */
public class HReporteBancoPropietario extends Thread{
    String user;
    String Banco1;
    String HC1;
    String Propietario1;
    String procesoName;
    String des;
    //String dstrct;
    Model model;
    
    public void start(String Propietario, String HC ,String Banco, Usuario usuario){
        this.user = usuario.getLogin();
        this.procesoName = "Reporte Propietario Banco";
        this.des = "Reporte Documentos Propietario Banco";
        this.Propietario1 = Propietario;
        this.HC1 = HC;
        this.Banco1 = Banco;
        model = new Model(usuario.getBd());
        //this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
             
             Vector datos = model.banco_PropietarioService.getVector();
             model.banco_PropietarioService.BusquedaBancoPro(Propietario1, HC1, Banco1);
             datos = model.banco_PropietarioService.getVector();
           
             
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
            
            POIWrite xls = new POIWrite(Ruta1 +"BancoPropietario" + hoy + ".xls", user, Util.getFechaActual_String(5));
            
            HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero      = xls.nuevoEstilo("Book Antiqua", 9, false , false, "#.##"         , xls.NONE , xls.NONE , xls.NONE );
            
            HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.WHITE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLACK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle encabezado      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLACK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            
            
            /****************************************
             *    compos de la tabla en agencia     *
             ****************************************/
            
            xls.obtenerHoja("REPORTE");
            xls.cambiarMagnificacion(4,4);
            
            // cabecera
             
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", encabezado);
            xls.combinarCeldas(0, 0, 0, 2);
            xls.adicionarCelda(1,0, "RESPORTE DE PROPIETARIO BANCO" , encabezado);
            xls.combinarCeldas(1, 0, 0, 2);
           
                          
            int fila = 7;
            int col  = 0;
            
            xls.adicionarCelda(fila ,col++ , "ITEM"     , titulo ); 
            xls.adicionarCelda(fila ,col++ , "DISTRITO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PROPIETARIO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "HC"     , titulo );
            xls.adicionarCelda(fila ,col++ , "BANCO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "SUCURSAL"     , titulo );
            xls.adicionarCelda(fila ,col++ , "USUARIO DE CREACION"     , titulo );
            xls.adicionarCelda(fila ,col++ , "BASE"     , titulo );
           
           
           int cont = 1; 
           for(int i=0; i<datos.size();i++ ){
                fila++;
                col = 0;
                Banco_Propietario  BancoPro = (Banco_Propietario) datos.get(i);
                
                
                xls.adicionarCelda(fila ,col++ ,cont, texto );
                xls.adicionarCelda(fila ,col++ ,BancoPro.getDistrito(), texto );
                xls.adicionarCelda(fila ,col++ ,BancoPro.getPropietario() , texto );
                xls.adicionarCelda(fila ,col++ ,BancoPro.getDescripcionHc(), texto );
                xls.adicionarCelda(fila ,col++ ,BancoPro.getBanco() , texto );
                xls.adicionarCelda(fila ,col++ ,BancoPro.getSucursal() , texto );
                xls.adicionarCelda(fila ,col++ ,BancoPro.getCreation_user() , texto );
                xls.adicionarCelda(fila ,col++ ,BancoPro.getBase() , texto );

              cont = cont + 1; 
           }
           			
            
            xls.cerrarLibro();
            //System.out.println("cerro proceso");
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
   
    
}
