/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;





import java.util.*;
import java.text.*;
import java.io.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;
import com.tsp.util.Util;

import com.tsp.util.LogWriter;
import com.aspose.cells.*;





    /**
     * Genera un reporte de PyG basado en un esquema predefinido en la tabla de generador financiero
     * El Reporte debe contar con un esquema de 3 niveles .
     * Maneja un Titulo para grupos de S1 a S3
     * Define niveles de Sx, B = Lineas blancas, Tx Titulos para grupos de Sx, GTx Grandes totales para cada Titulo,
     * Define formulas Fx, a ser traducidas a Excell
     *
     * @param   model Variable propia del usuario para acceder en forma segura a los metodos y propiedades exclusivas del usuario
     * @param   usuario Objeto Usuario que identifica al usuario en entra en sesion
     * @param   distrio Compania asociada al usuario
     * @param   anio Ano del reporte
     * @param   mes  Mes del reporte
     * @see     ClaseRegistro
     * @see     ElementoFormula
     * @see     ControlNivel
     * @author  Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since   1.0
     *
     */



public class HEstadosFinancieros1 extends Thread {


    // Parametros recibidos
    private Model   model;
    private Usuario usuario;
    private String  distrito;
    private String anio;
    private String mes;
    private String informe;
    private double secuencia_inicial;
    private double secuencia_final;
    private int grupofinalSecuencia;
    private String  NOMBRE_LIBRO = "Estado Financiero";

    // Constantes
    final String  ETIQUETA_HOJA = "Informe EF";
    final int     LINEA_TITULO_COLUMNA = 4;
    final int     COLUMNA_INICIAL_PERIODO = 5;
    final int     COLUMNA_FINAL_PERIODO = 17;



    // Variables
    private SimpleDateFormat fmt;




    // -------------------------------------------------------------------------
    // DEFINICION PARA USAR EXCEL
    // Variables del archivo de excel
    
    AsposeUtil xlsUtil = new AsposeUtil();
    Workbook   wb;
    String     rutaInformes;
    String     nombre;

    Style header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado;
    Style porcentaje, letraCentrada, numeroNegrita, ftofecha;
    Style dineroSinCentavo, dineroSinCentavoMarron , dineroSinCentavoAzul, dineroSinCentavoVerde;
    Style idMarron, idAzul, idVerde, idLetra;
    Style letraMarron, letraAzul, letraVerde;
    Style gtLetra, gtDineroSinCentavo, gtId, fLetra, fDineroSinCentavo, fId, T1Letra ;

    Hashtable colorPersonalizado = new Hashtable();

    int fila = 0;
    // FIN DEFINICION PARA USAR EXCEL
    // -------------------------------------------------------------------------





    /** Creates a new instance of HPrefacturaDetalle */
    public HEstadosFinancieros1 () {
    }

    public void start(Model model, Usuario usuario, String distrito, String anio, String mes, String informe, double secuencia1,double secuencia2){

        this.usuario  = usuario;
        this.model    = model;
        this.distrito = distrito;
        this.anio = anio;
        this.mes = mes;
        this.informe = informe;
        this.secuencia_inicial = secuencia1;
        this.secuencia_final = secuencia2;
        this.NOMBRE_LIBRO = usuario.getBd()+"_Estado Financiero";
        super.start();
    }




    public synchronized void run(){

        try{

            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion ingresos a partir de egresos TSP " + usuario.getLogin());

            verificarLicenciaAspose ( localizaRutaLicenciaAspose() );

            this.generarRUTA();   // crear el directorio donde va a incluirse el archivo excell
            String nombreLibro = xlsUtil.crearNombreLibro(NOMBRE_LIBRO, FileFormatType.EXCEL2003, true);
            wb = xlsUtil.crearLibro(nombreLibro);
            inicializarEstilos(wb);
            inicializarHoja( wb,ETIQUETA_HOJA);

            Worksheet sheet = wb.getWorksheets().getSheet(ETIQUETA_HOJA);


            // Encabezado : titulos de las columnas
            String [] cabecera = {"ID","DESCRIPCION","CUENTA CONTABLE","NIT" ,
                                  "", "", "", "", "", "", "", "", "", "", "", "", "", "ANUAL"};
            // definiendo los titulos de ano
            for (int i=1 ; i<=13 ; i++){
                cabecera[i+3] = anio + Util.ceroPad(i,2);
            }
            // ancho de las columnas
            float  [] dimensiones = new float [] {
                11,30, 14, 14, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11
            };
            // arma los tiulos en el excell
            xlsUtil.generarTitulos(sheet,LINEA_TITULO_COLUMNA,titulo2, cabecera, dimensiones);

            // Genera el proceso para el cuerpo del reporte
            this.crearReporte(model,usuario,distrito,wb, rutaInformes, sheet,LINEA_TITULO_COLUMNA, informe);

            xlsUtil.cerrarLibro(wb, rutaInformes, nombreLibro, FileFormatType.EXCEL2003);


            // model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HEstadosFinancieros1 ...\n" + e.getMessage());
            }
        }
    }



    /**
     * Crea la informacion que va en las columnas
     *
     * @param   model Variable propia del usuario para acceder en forma segura a los metodos y propiedades exclusivas del usuario
     * @param   usuario Objeto Usuario que identifica al usuario en entra en sesion
     * @param   distrio Compania asociada al usuario
     * @param   wb Variable que identifica el acceso al libro de excell
     * @param   rutaInformes Ruta del directorio donde quedara grabado el informe
     * @param   sheet Varibale que identifica el acceso a la hoja de excell donde esta el informe
     * @param   fila Numero de fila inicial del excell donde se registraran los datos
     * @see     ClaseRegistro
     * @see     ElementoFormula
     * @see     ControlNivel
     * @author  Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since   1.0
     *
     */


public void crearReporte(Model model,Usuario usuario,String distrito, Workbook wb, String rutaInformes, Worksheet sheet, int fila,
                         String informe) throws Exception{

    List     listaMayor         = null;



    // -------------------------------------------------------------------------
    // DEFINICION DEL LOG DE ERROR
    // Variables
    PrintWriter pw;
    LogWriter logWriter;
    java.text.SimpleDateFormat formatoFecha;
    String fechaDocumento;
    java.util.Date fecha;

    // Abriendo un log de error

    File file = new File(rutaInformes);
    file.mkdirs();
    pw        = new PrintWriter(System.err, true);

    Calendar fechaProceso  = Calendar.getInstance();
    fecha                  = fechaProceso.getTime();
    formatoFecha           = new java.text.SimpleDateFormat("yyyy-MM-dd HHmm");
    fechaDocumento         = formatoFecha.format(fecha);

    String logFile   = rutaInformes + "/"+usuario.getBd()+"_Estados Financieros "+fechaDocumento+".txt";

    pw        = new PrintWriter(new FileWriter(logFile, true), true);
    logWriter = new LogWriter("EF-TSP", LogWriter.INFO, pw);
    logWriter.setPrintWriter(pw);

    // FIN DEFINICION DEL LOG DE ERROR
    // -------------------------------------------------------------------------


    // Definiendo los estilos propios para los colores basados en estilos estandares
    Style letraColor = letra;
    Style dineroSinCentavoColor = dineroSinCentavo;
    Style idColor = idLetra;

    // Vectores para controlar los niveles, los totales y las formulas
    Vector controlDeGrupo      = new Vector();
    Vector vectorGranTotal     = new Vector();
    Vector vectorClaseRegistro = new Vector();


    try {

        logWriter.log("****************************************************************",LogWriter.INFO);
        logWriter.log("*  PROCESO PARA GENERACION DE ESTADOS FINANCIEROS              *",LogWriter.INFO);
        logWriter.log("*  FechaProceso :  "+fechaDocumento+"                          *",LogWriter.INFO);
        logWriter.log("*  Empresa :  "+usuario.getBd()+"                              *",LogWriter.INFO);
        logWriter.log("****************************************************************" +"\n",LogWriter.INFO);

        // Crea una lista de movimientos del generador financiero con la tabla mayor y mayor subledger

        logWriter.log("LECTURA DE REGISTROS DEL GENERADOR FINANCIERO EF_1  \n",LogWriter.INFO);

        // Crea tablas temporales de mayor_terceros para cada mes en un solo registro


        model.tablaGenService.buscarDatos("GENERADOR", informe);
        TablaGen getTblgen = model.tablaGenService.getTblgen();
        String unidad = getTblgen.getReferencia();

        model.estadoFinancieroService.creaMayorTercero(distrito,anio, informe, unidad);
        model.estadoFinancieroService.creaMovimientoTercero(distrito,anio, informe, unidad);

        for (int i=1; i<=13 ; i++){
            model.estadoFinancieroService.setMayorTercero(anio, Util.ceroPad(i,2));
        }

        

        // Crea una lista de los registros a presentar en el excell de acuerdo al esquema definido
        listaMayor = model.estadoFinancieroService.getEstadoFinanciero1(distrito, anio, informe,secuencia_inicial,secuencia_final);
        this.grupofinalSecuencia=listaMayor.size();
        if (listaMayor.size() > 0) {
            
            Iterator it = listaMayor.iterator();
            EstadoFinanciero1 estadoFinanciero1 = new EstadoFinanciero1();
            while (it.hasNext()) {
               estadoFinanciero1 = (EstadoFinanciero1)it.next();

               fila++;
               int col = 0;
               String tipoRegistro    = estadoFinanciero1.getTipo_registro();
               String formulaRegistro = estadoFinanciero1.getFormula();

               // Define los colores para cada tipo de nivel o tipo de registro segun informacion del esquema
               if(tipoRegistro.equalsIgnoreCase("S1")){
                   letraColor = letraMarron;
                   dineroSinCentavoColor = dineroSinCentavoMarron;
                   idColor = idMarron;
               }
               else if (tipoRegistro.equalsIgnoreCase("S2")){
                   letraColor = letraAzul;
                   dineroSinCentavoColor = dineroSinCentavoAzul;
                   idColor = idMarron;
               }
               else if (tipoRegistro.equalsIgnoreCase("S3")){
                   letraColor = letraVerde;
                   dineroSinCentavoColor = dineroSinCentavoVerde;
                   idColor = idMarron;
               }
               else if (tipoRegistro.length()>=3){
                       if (tipoRegistro.substring(0, 2).equalsIgnoreCase("GT")){
                       letraColor = gtLetra;
                       dineroSinCentavoColor = gtDineroSinCentavo;
                       idColor = gtId;
                   }
               }
               else if (tipoRegistro.length()>1){
                       if (tipoRegistro.substring(0, 1).equalsIgnoreCase("F")){
                       letraColor = fLetra;
                       dineroSinCentavoColor = fDineroSinCentavo;
                       idColor = fId;
                   }
               }
               else {
                   letraColor = letra;
                   dineroSinCentavoColor = dineroSinCentavo;
                   idColor = idLetra;
               }

               int signo = 1;

               if(tipoRegistro.equalsIgnoreCase("B")){

               }
               else if (tipoRegistro.equalsIgnoreCase("T1")) {
                   xlsUtil.setCelda(sheet, fila  , col++ , estadoFinanciero1.getSecuencia(), idColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , estadoFinanciero1.getDescripcion(), T1Letra  );

                   Cells cells = sheet.getCells();
                   Cell cell = cells.getCell(fila, col-1);
                   Style estiloCelda = cell.getStyle();
                   estiloCelda.setIndent(0);
                   cell.setStyle(estiloCelda);

               }

               // Procesa todos los registros diferentes  B y T1
               else {

                   // Identificando los ingresos para cambiarles el signo
                   // Tratamiento especial para cuentas que en la contabilidad tiene el signo negativo
                   String cuenta = estadoFinanciero1.getCuenta();
                   if (cuenta.length() > 1){
                       String sigla  = cuenta.substring(0, 1);
                       if (sigla.equalsIgnoreCase("I")){
                           signo = -1;
                       }
                       if (sigla.equalsIgnoreCase("2")){
                           signo = -1;
                       }
                   }
                   // Registrando en las celdas de excell
                   xlsUtil.setCelda(sheet, fila  , col++ , estadoFinanciero1.getSecuencia(), idColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , estadoFinanciero1.getDescripcion(), letraColor  );

                   if ( (tipoRegistro.equalsIgnoreCase("S1")) || (tipoRegistro.equalsIgnoreCase("S2")) || (tipoRegistro.equalsIgnoreCase("S3")) || (tipoRegistro.equalsIgnoreCase(""))  ){
                       indentarCelda(sheet, fila  , col-1 , estadoFinanciero1);
                   }


                   xlsUtil.setCelda(sheet, fila  , col++ , estadoFinanciero1.getCuenta(), letraColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , estadoFinanciero1.getTercero(), letraColor  );

                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes01(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes02(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes03(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes04(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes05(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes06(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes07(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes08(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes09(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes10(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes11(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes12(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes13(), dineroSinCentavoColor  );
                   xlsUtil.setCelda(sheet, fila  , col++ , signo * estadoFinanciero1.getVlr_mes13(), dineroSinCentavoColor  );
                   

                   String letraColumnaInicial = xlsUtil.getLetraExcell(COLUMNA_INICIAL_PERIODO);
                   String letraColumnaFinal   = xlsUtil.getLetraExcell(COLUMNA_FINAL_PERIODO);
                   String formula = "=SUM("+letraColumnaInicial + (fila+1) + ":"+letraColumnaFinal + (fila+1) + ")";
                   Cells cells    = sheet.getCells();
                   Cell cell    = cells.getCell(fila, col-1);
                   cell.setFormula (formula);

               }

               // Crea un vector que controla donde comienza fisicamente un grupo y donde termina,
               // para definir posteriormente los totales por grupos del nivel 1
               controlDeNiveles(estadoFinanciero1, controlDeGrupo, fila);
               creaVectorRegistros(tipoRegistro, formulaRegistro, vectorGranTotal, fila, vectorClaseRegistro,estadoFinanciero1.getSecuencia());

            }

            // Revisa y ajusta final en caso de que lineas no existan en el reporte
            rectificaControlDeNiveles(controlDeGrupo);
            // Crea las formulas de totales para finalizacion de los niveles , para formulas especificas y para grandes totales
            totalesNivelesDetallados(sheet, controlDeGrupo,3, COLUMNA_INICIAL_PERIODO, COLUMNA_FINAL_PERIODO);
            totalesNivelesGlobales( sheet, controlDeGrupo, 3, COLUMNA_INICIAL_PERIODO, COLUMNA_FINAL_PERIODO);
            totalesNivelesGlobales( sheet, controlDeGrupo, 2, COLUMNA_INICIAL_PERIODO, COLUMNA_FINAL_PERIODO);
            agruparNivel(sheet, controlDeGrupo, 1);
            agruparNivel(sheet, controlDeGrupo, 2);
            agruparNivel(sheet, controlDeGrupo, 3);

            adicionarGrandesTotales(sheet, controlDeGrupo, vectorGranTotal, COLUMNA_INICIAL_PERIODO,COLUMNA_FINAL_PERIODO);
            procesarFormulas(sheet, vectorClaseRegistro, COLUMNA_INICIAL_PERIODO,COLUMNA_FINAL_PERIODO);


            // Codigo para monitorear donde inicia y donde termina un grupo de tipo  S1 a S3

           
            int numeroRegistros = controlDeGrupo.size();
            fila++;

            for (int k=0 ; k<=numeroRegistros-1 ; k++){
                 int col = 0;
                 fila++;
                 ControlNivel controlNivel = (ControlNivel) controlDeGrupo.elementAt(k);
                 xlsUtil.setCelda(sheet, fila  , col++ , controlNivel.getNivel(), numero  );
                 xlsUtil.setCelda(sheet, fila  , col++ , controlNivel.getInicio_nivel(), numero  );
                 xlsUtil.setCelda(sheet, fila  , col++ , controlNivel.getFinal_nivel(), numero  );
            }
            // Codigo para monitorear en que linea se encuentra un total de tipo GT, que corresponderia a la suma de los niveles S1
            numeroRegistros = vectorGranTotal.size();
            fila++;
           
            for (int k=0 ; k<=numeroRegistros-1 ; k++){
                 int col = 0;
                 fila++;
                 ClaseRegistro claseRegistro=(ClaseRegistro)vectorGranTotal.elementAt(k);
                 int lineaTotal = (Integer) claseRegistro.getFila();
                 xlsUtil.setCelda(sheet, fila  , col++ , lineaTotal, numero  );
            }
            


        }  

        java.text.SimpleDateFormat formatoFechaFinal;
        Calendar fechaFinalProceso  = Calendar.getInstance();
        fecha                       = fechaFinalProceso.getTime();
        formatoFechaFinal           = new java.text.SimpleDateFormat("yyyy-MM-dd HH.mm:ss");
        fechaDocumento              = formatoFechaFinal.format(fecha);



        logWriter.log("****************************************************************",LogWriter.INFO);
        logWriter.log("*  FINAL DEL PROCESO " + fechaDocumento + "                       *",LogWriter.INFO);
        logWriter.log("****************************************************************",LogWriter.INFO);

    }catch (Exception e){
        System.out.println("Error ProntoPagoAccederAction, procedimiento crearReporte  ...\n"  + e.getMessage());
    }

}







public void verificarLicenciaAspose(String archivoLicencia) {

    FileInputStream fstream = null;
    try {
        fstream=new FileInputStream(archivoLicencia);
        License license=new License();
        license.setLicense(fstream);

    } catch (Exception e){
        System.out.println("Error al validar Licencia de Aspose ...\n"  + e.getMessage());
    }
}

    public String localizaRutaLicenciaAspose() throws Exception{

        String rutaLicencia = "";
        try{
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaLicencia =  rb.getString("rutaLicenciaAspose");
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        finally {
            return rutaLicencia;
        }
    }


    /**
     * Ubica la informacion de la ruta donde quedara el informe
     * Si no existe crea el directorio
     *
     * @author  Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since   1.0
     *
     */

    public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( rutaInformes );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }





    /**
     * Metodo para definir y etiquetar la hoja de excell
     * @autor apabon
     * @param wb Libro de excell
     * @param etiqueta Nombre para etiquetar la hoja
     * @throws Exception.
     */
    private void inicializarHoja(Workbook wb, String etiqueta) throws Exception{
        try{

            Worksheet sheet = wb.getWorksheets().getActiveSheet() ;

            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            xlsUtil.etiquetarHoja(wb, etiqueta);
            titularHoja(sheet,"Informe de Estados Financieros");

        }catch (Exception e){
            e.printStackTrace();
            throw new Exception( "Error en inicializar Hoja ....\n"  + e.getMessage() );
        }
    }


    /**
     * Metodo para crear el  archivo de excel
     * @autor apabon
     * @param sheet  Nombre de la hoja donde queda el informe
     * @param titulo Titulo en la hoja del informe
     * @throws Exception.
     */
    private void titularHoja(Worksheet sheet, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            xlsUtil.combinarCeldas(sheet, 0, 1, 0, 8);
            xlsUtil.setCelda(sheet, 0,1, titulo, header);
            xlsUtil.setCelda(sheet, 1,1, "FECHA" , titulo1);
            xlsUtil.setCelda(sheet, 1,2, fmt.format( new Date())  , titulo1 );
            xlsUtil.setCelda(sheet, 2,1, "USUARIO", titulo1);
            xlsUtil.setCelda(sheet, 2,2, usuario.getNombre() , titulo1);


        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }



    /**
     * Metodo para Incializar los colores propios y los estilos
     * @autor apabon
     * @param wb Libro de excell especifico para el cual se definen los colores y estilos propios
     * @throws Exception.
     */
    private void inicializarEstilos(Workbook wb) throws Exception{
        try{

            // colores
            xlsUtil.crearColor(wb,colorPersonalizado, "AZUL_CLARO_FINTRA", 55, 149,179,215);
            xlsUtil.crearColor(wb,colorPersonalizado, "VERDE_CLARO_FINTRA", 52, 194,214,154);
            xlsUtil.crearColor(wb,colorPersonalizado, "VERDE_FINTRA", 53, 51,153,102);
            xlsUtil.crearColor(wb,colorPersonalizado, "MARRON_CLARO_FINTRA", 51, 221,217,195);
            xlsUtil.crearColor(wb,colorPersonalizado, "GRIS", 54, 192,192,192);
            xlsUtil.crearColor(wb,colorPersonalizado, "NONE", 50, 0,0,0);
            xlsUtil.crearColor(wb,colorPersonalizado, "MEDIO", 49, 127,127,127);
            xlsUtil.crearColor(wb,colorPersonalizado, "AZUL_FORMULA", 48, 23,55,93);

            // estilos

            header  = xlsUtil.crearEstilo(wb, "Tahoma", 14, true, false, 0, "49",  com.aspose.cells.Color.GREEN, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.LEFT);
            titulo1 = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false,  0, "49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.LEFT);
            titulo2 = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_FINTRA"), TextAlignmentType.CENTER);
            titulo3 = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);
            titulo4 = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,com.aspose.cells.Color.OLIVE, TextAlignmentType.CENTER);
            titulo5 = xlsUtil.crearEstilo(wb, "Tahoma", 14, true, false,  0,"49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.LEFT);

            letra   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false, 0, "49", com.aspose.cells.Color.BLACK, false, com.aspose.cells.Color.OLIVE , TextAlignmentType.LEFT);

            letraMarron = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false,  0,"49", com.aspose.cells.Color.BLACK,  true, xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.LEFT);
            letraAzul   = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false,  0,"49", com.aspose.cells.Color.BLACK,  true, xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"),   TextAlignmentType.LEFT);
            letraVerde  = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false,  0,"49", com.aspose.cells.Color.BLACK,  true, xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"),  TextAlignmentType.LEFT);

            letraCentrada    = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false,  0,"49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);
            numero           = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false,  0,"#", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);
            numeroCentrado   = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false,  0,"#", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);
            dinero           = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0.00", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);
            dineroSinCentavo = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);

            dineroSinCentavoMarron = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            dineroSinCentavoAzul   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"),   TextAlignmentType.RIGHT);
            dineroSinCentavoVerde  = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"),  TextAlignmentType.RIGHT);

            idMarron  = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            idAzul    = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            idVerde   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            idLetra   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);

            gtLetra             = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MEDIO"), TextAlignmentType.LEFT);
            gtDineroSinCentavo  = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "#,##0", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MEDIO"),  TextAlignmentType.RIGHT);
            gtId                = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MEDIO"),  TextAlignmentType.RIGHT);

            fLetra              = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"), TextAlignmentType.LEFT);
            fDineroSinCentavo   = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "#,##0", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"),  TextAlignmentType.RIGHT);
            fId                 = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"),  TextAlignmentType.RIGHT);
            T1Letra             = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 1, "49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"), TextAlignmentType.LEFT);
            numeroNegrita  = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false, 0, "#", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);
            porcentaje     = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false, 0, "0.00%", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);
            ftofecha       = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false, 0, "yyyy-mm-dd", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);

        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Error en Inicializar Estilos .... \n" + e.getMessage());
        }

   }


    /**
     * Metodo para crar un vector de solo los registros de tipo Sx, identifica el inicio y final de cada grupo
     * @autor apabon
     * @param estadoFinanciero Objeto que contiene informacion de asociada a
     *        un tipo de registro segun el esquema definido para el reporte
     * @param controlDeGrupo Vector que contiene un objeto de tipo ControlNivel
     * @param fila  Fila donde queda registra la informacion en la hoja de excell
     * @see ControlNivel
     * @throws Exception.
     */
    public void controlDeNiveles(EstadoFinanciero1 estadoFinanciero1, Vector controlDeGrupo, int fila)    {

        int numeroNivel = 0;
        int numeroNivelUltimoRegistro = 0;
        ControlNivel controlNivel = new ControlNivel();
        ControlNivel controlNivelUltimoRegistro  = new ControlNivel();
        ControlNivel controlNivelTemporal = new ControlNivel();
        
        int filaExcell = fila;

        filaExcell++;  // Porque el indice comienza con 0.

        String tipo_registro = estadoFinanciero1.getTipo_registro();

        int largo = tipo_registro.length();


        // Se detecto un tipo de registro que puede ser de tipo Sx, T, Tx, GT, B
        if(largo>0){
            String subnivel = tipo_registro.substring(0,1);
            if (subnivel.equalsIgnoreCase("S")){

                // Se detecto una linea de Nivel Sx


                try {
                   numeroNivel = Integer.parseInt(tipo_registro.substring(1).trim() );
                }
                catch (NumberFormatException nfe) {
                    numeroNivel = 1;
                }
                   // Localizando el ultimo nivel adicionado
                   
                   if  (controlDeGrupo.size()>0)  {

                       // Ya existen niveles adicionados
                       // Actualizando la fila final del ultimo nivel

                       // Localizando el ultimo nivel adicionado
                       controlNivelUltimoRegistro = (ControlNivel) controlDeGrupo.elementAt( controlDeGrupo.size()-1 );

                       if (!controlNivelUltimoRegistro.isNiveles_previos_cerrados()) {
                           // Nivel previo esta cerrado

                           numeroNivelUltimoRegistro = controlNivelUltimoRegistro.getNivel();
                           // Primer caso : El nuevo numero de nivel es mayor que el ultimo nivel adicionado
                           if(numeroNivel > numeroNivelUltimoRegistro){
                               // Adicionar solamente un nuevo registro de nivel
                               controlNivel.setControlNivel(numeroNivel,filaExcell,0, false);
                               controlDeGrupo.addElement(controlNivel);
                           }
                           // Segundo caso : El nuevo numero de nivel es igual al ultimo nivel adicionado
                           else if(numeroNivel == numeroNivelUltimoRegistro){
                               // Actualiza el final del nivel anterior

                               controlNivelUltimoRegistro.setFinal_nivel(filaExcell);
                               controlDeGrupo.setElementAt(controlNivelUltimoRegistro, controlDeGrupo.size()-1);
                               // Adiciona un nuevo registro de nivel
                               controlNivel.setControlNivel(numeroNivel,filaExcell,0,false);
                               controlDeGrupo.addElement(controlNivel);
                           }
                           // Tercer caso : El nuevo numero de nivel es menor al ultimo nivel adicionado
                           else {
                               // Determina cuales son los niveles hacia atras que deben ser actualizados como finalizados
                               int i = numeroNivel;
                               int j = numeroNivelUltimoRegistro;
                               // Buscar cada nivel en el vector
                               for (int kNivel=i; kNivel<=j ; kNivel++){

                                   boolean localizado = false;
                                   int numeroRegistro = controlDeGrupo.size();

                                   // Inicia la busqueda del nivel en el vector, buscando de atras hacia el principio, el primero que encuentre
                                   while (localizado == false){
                                       numeroRegistro = numeroRegistro - 1;
                                       if (numeroRegistro >= 0) {
                                           // Extrae el objeto nivel del vector
                                           controlNivelTemporal= (ControlNivel) controlDeGrupo.elementAt(numeroRegistro );
                                           if (controlNivelTemporal.getNivel() == kNivel ) {
                                               // Localizo el nivel y actualiza el final de ese grupo
                                               controlNivelTemporal.setFinal_nivel(filaExcell);
                                               controlDeGrupo.setElementAt(controlNivelTemporal, numeroRegistro);
                                               localizado = true;
                                           }
                                       }
                                       else {
                                           localizado = true;
                                           System.out.println("Inconsistencia en busqueda de nivel");
                                       }
                                   }
                               }
                               // Adicionar al final el nuevo registro de nivel
                               controlNivel.setControlNivel(numeroNivel,filaExcell,0,false);
                               controlDeGrupo.addElement(controlNivel);
                           }
                       }
                       else {

                       // Se inicia un nuevo grupo de niveles. Los anteriores hasta el inicio estan cerrados
                       controlNivel.setControlNivel(numeroNivel,filaExcell,0,false);
                       controlDeGrupo.addElement(controlNivel);
                       }

                   }
                   else {
                       // Es el primer objeto nivel adicionado al vector
                       controlNivel.setControlNivel(numeroNivel,filaExcell,0,false);
                       controlDeGrupo.addElement(controlNivel);
                   }
            }
            else {

                // Se encontro un nivel que no es de subnivel.

                if( controlDeGrupo.size()>0)    {

                    // Hay registros en el control de grupo. Si no hubiesen se considera que son los primeros y no se debe hacer nada
                    // Se considerara que todos los niveles se cerraran hasta el nivel 1

                   // Localizando el ultimo nivel adicionado
                   controlNivelUltimoRegistro = (ControlNivel) controlDeGrupo.elementAt( controlDeGrupo.size()-1 );
                   numeroNivelUltimoRegistro = controlNivelUltimoRegistro.getNivel();
                   
                   if (!controlNivelUltimoRegistro.isNiveles_previos_cerrados() ) {

                        // Determina cuales son los niveles hacia atras que deben ser actualizados como finalizados
                        int i = 1;
                        int j = numeroNivelUltimoRegistro;
                        // Buscar cada nivel en el vector
                        for (int kNivel=i; kNivel<=j ; kNivel++){

                            boolean localizado = false;
                            int numeroRegistro = controlDeGrupo.size();

                            // Inicia la busqueda del nivel en el vector, buscando de atras hacia el principio, el primero que encuentre
                            while (localizado == false){
                                numeroRegistro = numeroRegistro - 1;
                                if (numeroRegistro >= 0) {
                                    // Extrae el objeto nivel del vector
                                    controlNivelTemporal= (ControlNivel) controlDeGrupo.elementAt(numeroRegistro );

                                    if (controlNivelTemporal.getNivel() == kNivel ) {
                                        // Localizo el nivel y actualiza el final de ese grupo
                                        controlNivelTemporal.setNiveles_previos_cerrados(true);
                                        controlNivelTemporal.setFinal_nivel(filaExcell);
                                        controlDeGrupo.setElementAt(controlNivelTemporal, numeroRegistro);
                                        localizado = true;

                                    }
                                }
                                else {
                                    localizado = true;
                                    System.out.println("Inconsistencia en busqueda de nivel");
                                }
                            }
                        }
                    }
                    //
                }
            }
        }
        else {
            // El tipo de registro es de detalle
           if(controlDeGrupo.size()>0){
               // Actualizando la fila final del ultimo nivel
               controlNivel = (ControlNivel) controlDeGrupo.elementAt( controlDeGrupo.size()-1 );
               controlNivel.setFinal_nivel(filaExcell);
               controlDeGrupo.setElementAt(controlNivel, controlDeGrupo.size()-1);
           }

        }

    }


    public void rectificaControlDeNiveles(Vector controlDeGrupo) {

        for ( int i=0; i<controlDeGrupo.size() ; i++){

            ControlNivel controlNivel = (ControlNivel) controlDeGrupo.elementAt( i);

            if ( (controlNivel.getInicio_nivel()+1) == controlNivel.getFinal_nivel()){
                controlNivel.setFinal_nivel(controlNivel.getInicio_nivel());
                controlDeGrupo.setElementAt(controlNivel, i);
            }
        }
    }












    /**
     * Metodo para crear los totales desde una columna inicial hasta una columna final para los niveles Sx
     * @autor apabon
     * @param sheet Hoja en la que se registra el informe
     * @param controlDeGrupo Vector que contiene un objeto de tipo ControlNivel
     * @param nivelDetalle Numero de nivel
     * @param columnaInicial Numero de columna inicial desde la cual se iniciara la suma
     * @param columnaFinal Numero de columna final hasta donde se finaliza la suma
     * @see ControlNivel
     * @throws Exception.
     */

    public void totalesNivelesDetallados(Worksheet sheet, Vector controlDeGrupo,int nivelDetalle,int columnaInicial, int columnaFinal){

        Cells cells;
        Cell cell;

        ControlNivel controlNivel = new ControlNivel();

        int largo = controlDeGrupo.size();
        int nivel, filaSuma, inicioSuma, finalizaSuma = 0;

        for (int i=0; i<largo ; i++){
            controlNivel = (ControlNivel) controlDeGrupo.elementAt(i);
            nivel = controlNivel.getNivel();
            if (nivel == nivelDetalle){

                for (int j=columnaInicial; j<=columnaFinal ; j++){

                    filaSuma       = controlNivel.getInicio_nivel();
                    
                    if(controlNivel.getInicio_nivel() == controlNivel.getFinal_nivel()){
                        cells = sheet.getCells();
                        cell = cells.getCell(filaSuma-1, j-1);
                        cell.setValue(0);
                    }
                    else {
                        inicioSuma     = filaSuma+1;
                        finalizaSuma   = controlNivel.getFinal_nivel() - 1;
                        String letraColumna = xlsUtil.getLetraExcell(j);

                        String formula = "=SUM("+letraColumna+inicioSuma+":"+letraColumna+finalizaSuma+")";

                        cells = sheet.getCells();
                        cell = cells.getCell(filaSuma-1, j-1);

                        cell .setFormula (formula);
                    }
                }
            }

        }

    }



    /**
     * Metodo para crear los grandes totales. Sumatoria de todos los niveles S1 para cada Titulo (T1)
     * @autor apabon
     * @param sheet Hoja en la que se registra el informe
     * @param controlDeGrupo Vector que contiene un objeto de tipo ControlNivel
     * @param nivelGlobal Numero de nivel
     * @param columnaInicial Numero de columna inicial desde la cual se registran formulas de totales
     * @param columnaFinal Numero de columna final hasta donde se registran formulas de totales
     * @see ControlNivel
     * @throws Exception.
     */

    public void totalesNivelesGlobales(Worksheet sheet, Vector controlDeGrupo,int nivelGlobal,int columnaInicial, int columnaFinal){

        Cells cells;
        Cell cell;

        ControlNivel controlNivel = new ControlNivel();
        int k, nivel, filaSuma = 0;


        int largo = controlDeGrupo.size();

        int nivelTotal = nivelGlobal - 1;



        for (int j=columnaInicial; j<=columnaFinal ; j++){

            String formula = "=";

            for (int i=0; i<largo ; i++){

                k = largo - i - 1;
                controlNivel = (ControlNivel) controlDeGrupo.elementAt(k);
                nivel = controlNivel.getNivel();
                if (nivel == nivelGlobal){
                    filaSuma = controlNivel.getInicio_nivel();
                    String letraColumna = xlsUtil.getLetraExcell(j);
                    formula = formula + "+" + letraColumna+filaSuma;
                }
                if (nivel == nivelTotal){
                    int filaNivelTotal = controlNivel.getInicio_nivel()-1;

                    cells = sheet.getCells();
                    cell = cells.getCell(filaNivelTotal, j-1);
                    cell .setFormula (formula);

                    formula = "=";
                }
            }
        }
    }


    /**
     * Metodo para crear los grandes totales. Sumatoria de todos los niveles S1 para cada Titulo (T1)
     * @autor apabon
     * @param sheet Hoja en la que se registra el informe
     * @param controlDeGrupo Vector que contiene un objeto de tipo ControlNivel
     * @param vectorGranTotal Vector que contiene una lista de lineas de tipo GTx, que identifica un gran total a calcular
     * @param columnaInicial Numero de columna inicial desde la cual se registran formulas de totales
     * @param columnaFinal Numero de columna final hasta donde se registran formulas de totales
     * @see ControlNivel
     * @see ClaseRegistro
     */

    public void adicionarGrandesTotales(Worksheet sheet, Vector controlDeGrupo,Vector vectorGranTotal, int columnaInicial, int columnaFinal){

        Cells cells;
        Cell cell;
        ControlNivel controlNivel  = new ControlNivel();
        ClaseRegistro granTotal = new ClaseRegistro();

        int numeroTotales = vectorGranTotal.size();


        for (int columna=columnaInicial; columna<=columnaFinal; columna++){

            String letraColumna = xlsUtil.getLetraExcell(columna);

            int itemControlDeGrupo = 0;
            String formula = "=";
            // Recorre el vector donde estan las lineas donde hay que totalizar
            for (int i=0; i<numeroTotales ; i++){

                // Toma el primer item de total

                granTotal = (ClaseRegistro) vectorGranTotal.elementAt(i);

                int lineaTotal = (Integer) granTotal.getFila();
                // Toma el primer registro de control de niveles
                controlNivel = (ControlNivel) controlDeGrupo.elementAt(itemControlDeGrupo);
                int nivel        = controlNivel.getNivel();
                int inicio_nivel = controlNivel.getInicio_nivel();
                int final_nivel  = controlNivel.getFinal_nivel();

                boolean finalGrupo = false;
                // Forma un while hasta encontrar un final de nivel que sea igual a la linea donde hay que colocar un total
                while (finalGrupo == false) {

                    if(nivel == 1){
                        // Va adicionando a la formula cuando el nivel es S1
                        formula = formula + "+" + letraColumna + inicio_nivel;
                        if (final_nivel != lineaTotal+1) {
                            // Continua con el siguiente registro de nivel ya que no es S1
                            itemControlDeGrupo++;
                            controlNivel = (ControlNivel) controlDeGrupo.elementAt(itemControlDeGrupo);
                            nivel        = controlNivel.getNivel();
                            inicio_nivel = controlNivel.getInicio_nivel();
                            final_nivel  = controlNivel.getFinal_nivel();
                        }
                        else{
                            // Detecto que se llego a un nivel donde debe finalizar la sumatoria de totales S1. 
                            cells = sheet.getCells();
                            cell = cells.getCell(lineaTotal, columna-1);
                            cell .setFormula (formula);
                            formula = "=";
                            finalGrupo = true;
                        }
                    }
                    else {
                        // El nivel no es S1 y pasa entonces al siguiente nivel
                        itemControlDeGrupo++;
                        controlNivel = (ControlNivel) controlDeGrupo.elementAt(itemControlDeGrupo);
                        nivel        = controlNivel.getNivel();
                        inicio_nivel = controlNivel.getInicio_nivel();
                        final_nivel  = controlNivel.getFinal_nivel();
                    }
                }
                itemControlDeGrupo++;
            }
        } // Final del recorrido para todas las columnas a las que hay que ponerle formula

    }



    /**
     * Metodo para crear una lista de los diferentes registros marcados como GTx
     * @param formulaRegistro
     * @param secuencia
     * @autor apabon
     * @param tipoRegistro Tipo de registro definido para una linea de un esquema
     * @param vectorGranTotal Vector de objetos que almacena los registros de tipo Gtx
     * @param fila Numero de fila donde esta el registro en el excell
     * @param vectorRegistro Vector que contiene todas las lineas con tipos de registros diferentes a detalle
     * @see ClaseRegistro
     */
    public void creaVectorRegistros(String tipoRegistro, String formulaRegistro, Vector vectorGranTotal,int fila, Vector vectorRegistro,double secuencia){

        ClaseRegistro granTotal = new ClaseRegistro();
        ClaseRegistro claseRegistro = new ClaseRegistro();

        // Almacena todos los registro y las filas donde se encuentran en el excell para interpretacion de formulas

        if(!tipoRegistro.equals("")) {
            claseRegistro.setFila(fila);
            claseRegistro.setTipoRegistro(tipoRegistro);
            claseRegistro.setFormula(formulaRegistro);
            claseRegistro.setSecuencia(secuencia);
            vectorRegistro.addElement(claseRegistro);
        }
        // Almacena solo los registros de tipo GT para totalizar
        if (tipoRegistro.length()>=3){
            if (tipoRegistro.substring(0,2).equalsIgnoreCase("GT")){
                granTotal.setFila(fila);
                granTotal.setTipoRegistro(tipoRegistro);
                granTotal.setFormula(formulaRegistro);
                claseRegistro.setSecuencia(secuencia);
                vectorGranTotal.addElement(granTotal);
            }
        }
    }



    /**
     * Analiza todos los diferentes tipos de registros (Tx,Fx,GTx,B,Sx) para un esquema financiero, detecta los de tipo formula (Fx)
     * y procede a traducir la formula a  una formula tipo excell, asignando a los operandos de la formula
     * la verdadera posicion en la hoja excell
     *
     * Una vez traducida la formula esta es registrada en la hoja de excell en las columnas indicadas por los parametros
     * de entrada y en la linea de excell donde qued� registrada la formula
     *
     * @param   sheet Hoja excell
     * @param   vectorClaseRegistro Vector que almacena los registros del esquema financiero diferentes a tipo de registros de detalle
     * @param   columna_inicial_periodo Numero de columna desde la cual se aplicara la formula traducida
     * @param   columna_final_periodo Numero de columna hasta donde se aplicara la formula traducida
     * @see     ClaseRegistro
     * @author  Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since   1.0
     *
     */

    public void procesarFormulas(Worksheet sheet, Vector vectorClaseRegistro, int COLUMNA_INICIAL_PERIODO, int COLUMNA_FINAL_PERIODO){

        Cells cells    = sheet.getCells();


        ClaseRegistro claseRegistro = new ClaseRegistro();


        for (int j=COLUMNA_INICIAL_PERIODO; j<=COLUMNA_FINAL_PERIODO; j++) {

            String formula = "";
            int longitud = vectorClaseRegistro.size();

            for (int i=0 ; i<longitud; i++){

                // Localizar registros de tipo Fx
                claseRegistro = (ClaseRegistro) vectorClaseRegistro.elementAt(i);
                if (claseRegistro.getTipoRegistro().substring(0, 1).equalsIgnoreCase("F") ){
                    formula = claseRegistro.getFormula();
                    String formulaReal = "=" + analizaFormula(vectorClaseRegistro, formula, j,claseRegistro.getTipoRegistro());

                    int filaFormula = claseRegistro.getFila();

                    Cell cell = cells.getCell(filaFormula, j-1);
                    cell.setFormula (formulaReal);
                }
            }
        }
    }


    /**
     * Analiza todos los diferentes tipos de registros (Tx,Fx,GTx,B,Sx) para un esquema financiero, detecta los de tipo formula (Fx)
     * y procede a traducir la formula a  una formula tipo excell, asignando a los operandos de la formula
     * la verdadera posicion en la hoja excell
     *
     * Una vez traducida la formula esta es registrada en la hoja de excell en las columnas indicadas por los parametros
     * de entrada y en la linea de excell donde qued� registrada la formula
     *
     * @param   sheet Hoja excell
     * @param   controlDeGrupo Vector que almacena los tipos de registros Sx
     * @param   grupoNivel Numero que identifica el numero de nivel
     * @see     ControlNivel
     * @author  Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since   1.0
     *
     */
    public void agruparNivel(Worksheet sheet, Vector controlDeGrupo, int grupoNivel){


        Cells cells;
        cells = sheet.getCells();


        ControlNivel controlNivel = new ControlNivel();

        int nivel, filaInicial,filaFinal = 0;

        int largo = controlDeGrupo.size();

        for (int i=0; i<largo ; i++){

            controlNivel = (ControlNivel) controlDeGrupo.elementAt(i);
            nivel = controlNivel.getNivel();

            if (nivel == grupoNivel) {
                filaInicial = controlNivel.getInicio_nivel();
                //filaFinal   = controlNivel.getFinal_nivel() - 2;
                filaFinal = (controlNivel.getFinal_nivel() - 2) == -2 ? (int) grupofinalSecuencia : (controlNivel.getFinal_nivel() - 2);
                cells.groupRows(filaInicial,filaFinal,false);
            }
        }


    }


    /**
     * Efectua sangria (indentacion) en excell dependiendo del tipo de registro Sx. Indenta x veces la leyenda
     *
     * @param   sheet Hoja en la que se registra el informe
     * @param   fila Fila de la celda a indentar
     * @param   estadoFinanciero1 Objeto que contiene una fila del reporte
     * @author  Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since   1.0
     *
     */
    public void indentarCelda(Worksheet sheet, int fila  , int col , EstadoFinanciero1 estadoFinanciero1){

        int numeroNivel = 1;

        String tipo_registro = estadoFinanciero1.getTipo_registro();
        String lineaDetalle  = estadoFinanciero1.getCuenta();
        int longitudCuenta   = lineaDetalle.length();

        int largo = tipo_registro.length();

        if(largo>0){
            String subnivel = tipo_registro.substring(0,1);
            if (subnivel.equalsIgnoreCase("S")){
                // Detecta registro de tipo Sx

                try {
                    // Extrae el numero de nivel del tipo de registro de tipo Sx
                    numeroNivel = Integer.parseInt(tipo_registro.substring(1).trim() );
                }
                catch (NumberFormatException nfe) {
                    numeroNivel = 1;
                }
                // Indenta x veces segun nivel Sx
                Cells cells = sheet.getCells();
                Cell cell = cells.getCell(fila, col);
                Style estiloCelda = cell.getStyle();
                estiloCelda.setIndent(numeroNivel);
                cell.setStyle(estiloCelda);
            }
            else if  (longitudCuenta > 0) {
                // Indenta 4 posiciones para el detalle
                Cells cells = sheet.getCells();
                Cell cell = cells.getCell(fila, col);
                Style estiloCelda = cell.getStyle();
                estiloCelda.setIndent(4);
                cell.setStyle(estiloCelda);
            }
            else {
                Cells cells = sheet.getCells();
                Cell cell = cells.getCell(fila, col);
                Style estiloCelda = cell.getStyle();
                estiloCelda.setIndent(4);
                cell.setStyle(estiloCelda);
            }
        }
        else{
            // Indenta 4 posiciones para todos los demas casos
            numeroNivel = 4;
            Cells cells = sheet.getCells();
            Cell cell   = cells.getCell(fila, col);
            Style estiloCelda = cell.getStyle();
            estiloCelda.setIndent(numeroNivel);
            cell.setStyle(estiloCelda);
        }
    }


    /**
     * Extrae un operando de una formula y la convierte en una posicion excell
     *
     * @param   vectorClaseRegistro Vector que contiene un objete de tipo
     *          ClaseRegistro que almacena todos los tipos de registros diferentes a detalles
     * @param   formula formula a interpretar
     * @param   columna Numero de columna donde se debe insertar la formula traducida
     * @param   tipoRegistro tipo de formula a procesar 
     * @return  String Formula convertida en posiciones de excell
     * @see     ClaseRegistro
     * @see     ElementoFormula
     * @author  Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since   1.0
     *
     */
    public String  analizaFormula(Vector vectorClaseRegistro, String formula, int columna, String tipoRegistro){

        ClaseRegistro claseRegistro = new ClaseRegistro();
        ElementoFormula elementoFormula = new ElementoFormula();

        boolean inicioBloque = false;
        int posicionInicial = 0;
        int posicionFinal   = 0;
        boolean finalAnalisis = false;

        int i=0;
        while ( finalAnalisis==false) {


            String caracter = formula.substring(i,i+1);

            // Busca los operandos identificados por # al inicio y final del operando
            if(caracter.equalsIgnoreCase("#")){
                // Inicio de un operando
                if(inicioBloque == false) {
                    posicionInicial = i;
                    inicioBloque = true;
                    i++;
                }
                else{
                    // Final de un operando
                    posicionFinal = i;
                    inicioBloque = false;
                    // Crea un objeto para el operando
                    elementoFormula = new ElementoFormula(formula, posicionInicial, posicionFinal);
                    System.out.println(elementoFormula.toString());

                    boolean operandoLocalizado = false;
                    int k=0;
                    // Ubica el operando entre todos los tipos de registros del esquema
                    while (operandoLocalizado == false) {
                        claseRegistro = (ClaseRegistro) vectorClaseRegistro.elementAt(k);
                        if (claseRegistro.getTipoRegistro().equalsIgnoreCase(elementoFormula.getOperando())) {
                            //esta validacion es para la suma final del reporte
                            if(tipoRegistro.equalsIgnoreCase("F4") && elementoFormula.getOperando().equals("S1") && claseRegistro.getSecuencia()!=54701 ){
                               k++; 
                               continue;
                            }
                                                        // operando localizado en el esquema
                            operandoLocalizado= true;
                            // Detecta la linea donde se encuentra el operando
                            int lineaOperando = claseRegistro.getFila() + 1 ;
                            // Traduce a excell el operando
                            String celdaOperando = xlsUtil.getLetraExcell(columna) + lineaOperando;
                            String elemento = elementoFormula.getElemento();
                            // Reemplaza el operando
                            formula = formula.replace(elemento, celdaOperando);
                            i= 0;
                            System.out.println(formula);
                        }
                        else {
                            k++;
                            if (k>vectorClaseRegistro.size()){
                                operandoLocalizado = true;
                            }
                        }
                    }
                }
            }
            else {
                i++;
                if (i>=formula.length()) {
                    finalAnalisis = true;

                }



            }

        }

        // Almacena en el objeto de formula
        elementoFormula.setOperandoTraducido(formula);
        System.out.println(elementoFormula.toString());
        return elementoFormula.getOperandoTraducido();
    }



}






