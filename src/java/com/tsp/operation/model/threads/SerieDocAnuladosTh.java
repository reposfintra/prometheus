
/*
* Nombre        SerieDocAnuladosTh.java
* Descripci�n   Exportaci�n a MS Excel de la consulta
* Autor         Ing. Andr�s Maturana D.
* Fecha         22 de marzo de 2007
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.SerieDocAnulado;
import com.tsp.operation.model.Model;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;


/**
 *
 * @author  equipo
 */
public class SerieDocAnuladosTh extends Thread{
    

    Vector info;
    Model model;
    Usuario usuario;
    SimpleDateFormat fmt;
    String processName = "Consulta Serie Documentos Anulados";
    
    
    
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;
    
    
    /** Creates a new instance of HReporteOcsConAnticipoSinReporte */
    public SerieDocAnuladosTh() {
    }
    
    public void start(Model model, Vector info, Usuario usuario){
        this.info = info;
        this.usuario  = usuario;
        this.model    = model;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del Reporte de Serie de Documentos Anulados.", usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                //System.out.println("Error HReporteOcsConAnticipoSinReporte ...\n" + e.getMessage());
            }
        }
    }
    
    
    public void generarRUTA() throws Exception{
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            /*cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);*/
            
            
            // estilos
            header         = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo3        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo4        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo5        = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
        
    }
    
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            //xls.adicionarCelda(2,0, "PERIODO", titulo1);
            //xls.adicionarCelda(2,1, finicial + " - " + ffinal  , titulo1);
            xls.adicionarCelda(2,0, "USUARIO", titulo1);
            xls.adicionarCelda(2,1, usuario.getNombre() , titulo1);
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ReporteSerieDocAnulados_" + fmt.format( new Date() ) +".xls", "REPORTE DE SERIE DE DOCUMENTOS ANULADOS ");
            fila = 6;
            
            Vector vector = this.info;
            if (vector!=null && !vector.isEmpty()){
                


                    // encabezado
                String [] cabecera = { 
                    "Distrito", "Tipo de Documento", "No. Documento", "Referencia 1", "Referencia 2", "Referencia 3",
                    "Causa de Anulaci�n", "Observaciones", "Fecha Creaci�n", "Usuario Cre�", "Ultima Actualizaci�n", "Usuario Modific�", "Base"
                };
                short  [] dimensiones = new short [] { 
                    6000, 5500, 3500, 4000, 3500, 3500, 
                    3000, 2000, 6000, 3000, 3000, 3000, 3000
                };
                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }                
                fila++;
                
                
                for ( int i = 0; i< vector.size(); i++, fila++){
                    int col = 0;
                    SerieDocAnulado rp = (SerieDocAnulado) vector.elementAt(i);

                    xls.adicionarCelda(fila  , col++ , rp.getDstrct(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getTipo_documento_desc(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getNo_documento(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getReferencia_1(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getReferencia_2(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getReferencia_3(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getCausa_anulacion_desc(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getObservacion(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getCreation_date(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getCreation_user(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getLast_update(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getUser_update(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getBase(), letra  );
                }            

                
                
            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;                
            }
            
            
            this.cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
}

//22 Marzo de 2007
