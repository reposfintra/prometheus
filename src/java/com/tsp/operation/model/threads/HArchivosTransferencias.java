   /***************************************
    * Nombre Clase ............. HArchivosTransferenciaBancos.java
    * Descripci�n  .. . . . . .  Permite Generar los archivos para transferencia al banco
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  05/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.LogProcesosService.*;
import java.lang.*;
import java.io.*;

import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.*;
import java.util.Date;



public class HArchivosTransferencias extends Thread {
    
  
    
// Archivo de Transferencia MSF265 para migrar a MIMS:
    private  FileOutputStream        fw265;
    private  BufferedWriter    bf265;
    private  PrintWriter       linea265;
    private  List          Popietario_secuencias;
    private  File f;
      
    
 // Archivo de Transferencia
    private  FileOutputStream        fw;
    private  BufferedWriter    bf;
    private  PrintWriter       linea;
        
    
    private  String URL_TEM_TRANS     = "";
    private  String URL_TEM_MSF265    = "";
    
    
    
    private  String        procesoName;    
    private  Model         model;    
    private  Usuario       usuario ;
    private  List          listaTransferencia; 
    private  List          listaAgrupada; 
    
    private  String[]      anticipos;
    private  String[]      cxps;
    
    private String   AAMMDD                = "yyMMdd";
    private String   AAAAMMDD              = "yyyyMMdd";
    private String   SPACE                 = " ";
    private String   CERO                  = "0";
    private String   DERECHA               = "R";
    private String   IZQUIERDA             = "L";
    private String   CTA_AHORR0            = "CA";
    private String   CTA_CORRIENTE         = "CC";
    private String   CTA_EFECTIVO          = "EF";
    private String   CPAG                  = "CPAG";
    
    private int      TOTALDETALLE          = 0;    
    private double   TOTALDEBITO           = 0;    
    private double   TOTALCREDITO          = 0;
    
    private String   NIT_PROVEEDOR         = "";
    private String   NOMBRE_PROVEEDOR      = "";    
    private String   CTA_PROVEEDOR         = "";        
    private String   TIPO_CTA_PROVEEDOR    = ""; 
    private String   BANCO_PROVEEDOR       = "";
    private String   DESC_BANCO_PROVEEDOR  = "";
    
    
    private String   TRANSFERENCIA         = "";
    private boolean PRESTAMO               = false; //TMolina 2008-09-13
     private String   BANCOLOMBIA_CPAG_TIPO_TRANSACTION= "320";
    
// CONFIGURACION DE BANCOS:
    
    // 1.  Bancolombia:
           private String   BANCOLOMBIA                   = "07";
           private String   TABLA_BANCOLOMBIA_BANCO       = "BANCOLOMBI";   // BANCOS ESTIPULADOS POR BANCOLOMBIA
           private String   TABLA_BANCOLOMBIA_CIUDAD      = "CIUBANCOLO";   // CODIGO DE CIUDAD ESTIPULADOS POR BANCOLOMBIA
           
           private String   BANCOLOMBIA_TIPO_REGISTRO     = "1";
           private String   BANCOLOMBIA_TIPO_REGISTRO_DET = "6";
           private String   BANCOLOMBIA_TIPO_TRANSACTION  = "220";    
           private String   BANCOLOMBIA_PROPOSITO         = "PAGFINTRA";  
           private String   BANCOLOMBIA_CTA_AHORR0        = "37";
           private String   BANCOLOMBIA_CTA_CORRIENTE     = "27";
           private String   BANCOLOMBIA_CTA_EFECTIVO      = "26";
    
    
           
    // 2. Occidente
          private String   OCCIDENTE                      = "23";
          private String   OCCIDENTE_TIPO_REGISTRO        = "1";
          private String   OCCIDENTE_TIPO_REGISTRO_DET    = "2";
          private String   OCCIDENTE_TIPO_REGISTRO_FIN    = "3";

          private String   OCCIDENTE_CTA_AHORR0           = "A";    
          private String   OCCIDENTE_CTA_CORRIENTE        = "C";
          private String   OCCIDENTE_CTA_EFECTIVO         = SPACE;
          private String   OCCIDENTE_CONCEPTO             = "PAGOS FINTRA"; 
          private String   TABLA_OCCIDENTE_BANCO          = "BOCCIDENTE";

          private   String  CUENTA_ANTICIPO     = "2205050406";
          private   String  CUENTA_LIQUIDACION  = "2205050404";
          
          
          
          // 5.  Bancolombia formato PAB
           private String   BANCOLOMBIAPAB                   = "7B";
           private String   BANCOLOMBIAPAB_TIPO_APLICACION   = "I"; // I=inmediata
           
           

    
    
    
    public HArchivosTransferencias() {        
    }
    
    
    
    
    
    public void init(){
            TOTALDETALLE          = 0;    
            TOTALDEBITO           = 0;    
            TOTALCREDITO          = 0;            
            
            NOMBRE_PROVEEDOR      = "";  
            NIT_PROVEEDOR         = "";
            CTA_PROVEEDOR         = "";        
            TIPO_CTA_PROVEEDOR    = "";
            BANCO_PROVEEDOR       = "";
            DESC_BANCO_PROVEEDOR  = "";
            
            URL_TEM_TRANS         = "";
            URL_TEM_MSF265        = "";
            
            TRANSFERENCIA         = "";
            
            PRESTAMO              = false; //TMolina 2008-09-13
    }
    
    
    
    
    
    /**
     * M�todo inicializa  el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void start(Model modelo, Usuario user,String[] anticipos, String nit, String name, String banco,String desbanco, String cta, String tCta,String[] cxps) throws Exception{
         try{
                init();
                this.model                 = modelo;
                this.usuario               = user;
                this.anticipos             = anticipos;
                this.cxps             = cxps;
                this.listaAgrupada         = new LinkedList();
                
                String[] str; //Mod Tmolina 2008-09-13
                str           = name.split(",");
                if(str.length==2){
                    this.PRESTAMO = true;
                }
                
                this.NIT_PROVEEDOR         = nit;
                this.NOMBRE_PROVEEDOR      = str[0];//name; //TMolina 2008-09-13
                this.BANCO_PROVEEDOR       = banco;
                this.DESC_BANCO_PROVEEDOR  = desbanco;                
                this.CTA_PROVEEDOR         = cta;
                this.TIPO_CTA_PROVEEDOR    = tCta;
                

                this.procesoName = "TRANSFERENCIA BANCO " + banco;
            
            super.start();
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
     /**
     * Metodo que  permite ejecutar el SQL
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
      
      
     
      
     
     /**
     * Metodo que  determina si hay rutina para el banco seleccionado
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public String validarFormatosRealizados(String codebanco){
         String msj = "No hay formato definido(rutina) para el banco " + codebanco;       
         if( codebanco.equals(  BANCOLOMBIA ) )  msj=""; 
         if( codebanco.equals(  BANCOLOMBIAPAB ) )
             msj="";
         if( codebanco.equals(  OCCIDENTE   ) )  msj="";        
         if( codebanco.equals("10000003"))  msj="";        
        return msj;
    }
     
    
    
    
     
     
     /**
     * Metodo que  escribe cabecera archivo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public void writeHeader(String codeBanco)throws Exception{
        try{            
            
            
            int sec = 1;
                    
            
         // BANCOLOMBIA
            if(codeBanco.equals(BANCOLOMBIA)){
                     String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO                                           +  // 1.1  Tipo de Registro 
                                     rellenar(this.NIT_PROVEEDOR,               CERO,  10, IZQUIERDA )   +  // 1.2  Nit entidad que envia
                                     rellenar(this.NOMBRE_PROVEEDOR,            SPACE, 16, DERECHA   )   +  // 1.3  Nombre entidad que envia
                                     BANCOLOMBIA_TIPO_TRANSACTION                                        +  // 1.4  Clase de transaccion, segun formato es 220
                                     rellenar(this.BANCOLOMBIA_PROPOSITO ,      SPACE, 10, DERECHA   )   +  // 1.5  Proposito descripci�n
                                     this.getFecha(AAMMDD)                                               +  // 1.6  Fecha transacci�n  AAMMDD
                                     this.convertLetra(sec)                                              +  // 1.7  Secuencia del archivo del dia en letra A.B.C....                                     
                                     this.getFecha(AAMMDD)                                               +  // 1.8  Fecha aplicaci�n  AAMMDD
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  6 , IZQUIERDA )   +  // 1.9  N�mero de registros de detalle
                                     rellenar(String.valueOf((int)TOTALDEBITO ),CERO,  12, IZQUIERDA )   +  // 1.10 Sumatoria debito
                                     rellenar(String.valueOf((int)TOTALCREDITO),CERO,  12, IZQUIERDA )   +  // 1.10 Sumatoria debito
                                     rellenar(this.CTA_PROVEEDOR,               CERO,  11, IZQUIERDA )   +  // 1.11 Cta Cliente
                                     equivalenciaTipoCta( this.BANCO_PROVEEDOR, this.TIPO_CTA_PROVEEDOR );  // 1.12 Tipo Cta  S : aho  /  D : cte                
                     
                    linea.println( dato );
                    //System.out.println("DatoHeader---------"+dato);
            }
            
            
            
         // OCCIDENTE
            if(codeBanco.equals( OCCIDENTE )){
                String dato =
                                     OCCIDENTE_TIPO_REGISTRO                                           +  // 1.1 Tipo de Registro
                                     rellenar( CERO,                            CERO,  4,  DERECHA   ) +  // 1.2 Consecutivo
                                     this.getFecha(AAAAMMDD)                                           +  // 1.3 Fecha del archivo, formato YYYYMMDD                                     
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  4 , IZQUIERDA ) +  // 1.4 N�mero de registros de detalle                                     
                                     rellenar(String.valueOf((int)TOTALCREDITO) + "00" , CERO,  18, IZQUIERDA ) +  // 1.5 Valor total de pago
                                     rellenar(this.CTA_PROVEEDOR,               CERO,  16, IZQUIERDA ) +  // 1.6 Cta Cliente                                     
                                     rellenar(String.valueOf(0),              CERO,   6, IZQUIERDA ) +  // 1.7 Secuencia
                                     rellenar( CERO,                            CERO, 142, DERECHA   ) ;  // 1.8 Ceros
                
                linea.println( dato ); 
            }
            
            
            // BANCOLOMBIA FORMATO PAB
            if(codeBanco.equals(BANCOLOMBIAPAB)){
                     String secuenciaPAB = model.AnticiposPagosTercerosSvc.obtenerSecuenciaBanco(BANCOLOMBIAPAB, usuario.getLogin());
                     String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO                                           +  // 1.1  Tipo de Registro
                                     rellenar(this.NIT_PROVEEDOR,               CERO,  15, IZQUIERDA )   +  // 1.2  Nit entidad que envia
                                     BANCOLOMBIAPAB_TIPO_APLICACION                                      +  // 1.3  Tipo de aplicacion
                                     rellenar(SPACE,SPACE, 15, DERECHA   )                               +  // 1.4  Filler                             
                                     BANCOLOMBIA_TIPO_TRANSACTION                                        +  // 1.5  Clase de transaccion, segun formato es 220 pago proveedores
                                     rellenar(this.BANCOLOMBIA_PROPOSITO ,      SPACE, 10, DERECHA   )   +  // 1.6  Proposito descripci�n
                                     this.getFecha(AAAAMMDD)                                             +  // 1.7  Fecha transacci�n  AAAAMMDD                             
                                     rellenar(secuenciaPAB,                     SPACE, 2, DERECHA    )   +  // 1.8  Secuencia del archivo del dia en letra A.B.C....
                                     this.getFecha(AAAAMMDD)                                             +  // 1.9  Fecha aplicaci�n  AAAAMMDD
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  6 , IZQUIERDA )   +  // 1.10  N�mero de registros de detalle
                                     rellenar(String.valueOf((int)TOTALDEBITO ) + "00",CERO,  17, IZQUIERDA )+  // 1.11 Sumatoria debito
                                     rellenar(String.valueOf((int)TOTALCREDITO) + "00",CERO,  17, IZQUIERDA )+  // 1.12 Sumatoria credito
                                     rellenar(this.CTA_PROVEEDOR,               CERO,  11, IZQUIERDA )   +  // 1.13 Cta Cliente
                                     equivalenciaTipoCta( BANCOLOMBIA         , this.TIPO_CTA_PROVEEDOR )+  // 1.14 Tipo Cta  S : aho  /  D : cte
                                     rellenar(SPACE,SPACE, 149, DERECHA);                                   // 1.15  Filler
                             

                    linea.println( dato );
            }
            
            if(codeBanco.equals("10000003")){
//              String dato = obtenerInfoTextoEfecty(anticipos);
                String dato = model.AnticiposPagosTercerosSvc.obtenerInfoTextoEfecty(anticipos);
                linea.println( dato );
            }

          //System.out.println("Escribio Biennnn Occ");   
        }catch(Exception e){  
            throw new Exception(" writeHeader "+e.getMessage());
        }  
    }
     
     
    
    
    
    
     /**
     * Metodo que  ecribe en el archivo de transferencia a banaco
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public void writeTransfer(String codeBanco, AnticiposTerceros  trans)throws Exception{
        try{
            
         // BANCOLOMBIA
            if( codeBanco.equals(BANCOLOMBIA ) )    bancolombia( trans ); 
            
         // BANCOLOMBIA PAB
            if( codeBanco.equals(BANCOLOMBIAPAB ) )    bancolombiaPAB( trans );
            
        // OCCIDENTE
            if( codeBanco.equals( OCCIDENTE ) )    occidente( trans ); 

        }
        catch(Exception e){  
            throw new Exception(e.getMessage());
        }  
    }
    
    
     
    
    /**
     * Metodo que  ecribe bloque final
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public void writeFinal(String codeBanco)throws Exception{
        try{
            
         // OCCIDENTE
            if(codeBanco.equals( OCCIDENTE )){
                String dato =
                                     OCCIDENTE_TIPO_REGISTRO_FIN                                         +  // 1.1 Tipo de Registro final
                                     "9999"                                                              +  // 1.2 Secuencia
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  4 , IZQUIERDA )   +  // 1.3 N�mero de pagos                                    
                                     rellenar(String.valueOf((int)TOTALCREDITO) +"00",CERO,  18, IZQUIERDA )   +  // 1.4 Valor total de pago
                                     rellenar( CERO,                            CERO, 172, DERECHA   )   ;  // 1.5 Ceros
                linea.println( dato ); 
            }
            
            
            
        } catch(Exception e){  
            throw new Exception(e.getMessage());
        }  
    }
    
    
    
    
    
    
    /**
     * Metodo que  ecribe en formato BANCOLOMBIA
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public void bancolombia(AnticiposTerceros  transf)throws Exception{
           try{ 
               
               
         // 2. DETALLE DE TRANSACCION:
               
               String tipoCta          = transf.getTipo_cuenta();
               
               
            // Codigo del  banco del beneficiario
               String codigoBanco     =  CERO;
               Hashtable  infoBanco   =  model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_BANCOLOMBIA_BANCO, transf.getBanco() );
               if(infoBanco==null)
                     throw new Exception ( "No hay informaci�n del banco  " + transf.getBanco() + " en formato bancolombia");
               else
                    codigoBanco  =   (String)infoBanco.get("descripcion");
               
               
            // Indicador de pago: sucursal   
               String indicador_pago   = "S";
               
            // Tipo de transacion:   
               String tipo_transaction = equivalenciaTipoTransaction( BANCOLOMBIA, tipoCta ); 
               
            // Valor   
               int    valor            = (int)transf.getVlrConsignar();
               
               String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO_DET                                      +  // 1.1  Tipo de Registro detalle
                                     rellenar( transf.getNit_cuenta(),          CERO,  15, IZQUIERDA  ) +  // 1.2  Nit beneficiario
                                     rellenar( transf.getNombre_cuenta(),       SPACE, 18, DERECHA    ) +  // 1.3  Nombre del beneficiario 
                                     rellenar( codigoBanco,                     CERO,   9, IZQUIERDA  ) +  // 1.4  Codigo banco 
                                     rellenar( transf.getCuenta(),              CERO,  17, IZQUIERDA  ) +  // 1.5  N�mero de la Cta 
                                     indicador_pago                                                     +  // 1.6  Lugar de pago 
                                     tipo_transaction                                                   +  // 1.7  Tipo de Transaction 
                                     rellenar( String.valueOf(valor),           CERO,  10, IZQUIERDA  ) +  // 1.8  Valor
                                     rellenar( this.BANCOLOMBIA_PROPOSITO ,     SPACE,  9, DERECHA    ) +  // 1.9  Proposito descripci�n
                                     rellenar( transf.getLote_transferencia(),        SPACE, 12, DERECHA    ) +  // 1.10 Referencia
                                     SPACE                                                              ;  // 1.11 Espacio en blanco
               
              linea.println( dato );  
              //System.out.println("Dato---------"+dato);
           } catch(Exception e){  
             throw new Exception( " bancolombia " + e.getMessage());
          }
    }
    
    
    
    
    /**
     * Metodo que  ecribe en formato OCCIDENTE
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public void occidente(AnticiposTerceros  transf)throws Exception{
           try{ 
               
               
         // 2. DETALLE DE TRANSACCION:
               
               String tipoCta          = transf.getTipo_cuenta();
               
            // N�mero de la cuenta, si es efectivo 0, de lo contrario la cuenta del beneficiario     
               String cuenta           = ( tipoCta.equals( CTA_EFECTIVO ))? CERO : transf.getCuenta() ;
                              
               
            // Codigo del banco  -- Definir codigos
               String codigoBanco      = CERO;
               Hashtable  infoBanco    =  model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_OCCIDENTE_BANCO, transf.getBanco() );
               if(infoBanco==null)
                     throw new Exception ( "No hay informaci�n del banco  " + transf.getBanco() + " en formato occidente");
               else
                    codigoBanco  =   (String)infoBanco.get("descripcion");
               
               
           //  Forma de pago:               
               String formaPago        = ( tipoCta.equals( CTA_EFECTIVO ) )? "4" : ( ( transf.getBanco().equals("BANCO OCCIDENTE"))?"2":"4"  );               
               
           //  Tipo de transacion:   
               String tipo_transaction = equivalenciaTipoTransaction( OCCIDENTE, tipoCta ); 
               
           //  Valor
               int  valor   = (int) transf.getVlrConsignar();
               
               
           // Comprobante -- Definir
              String comprobante       = transf.getNit_cuenta();//(transf.getReanticipo().equals("N") )?CUENTA_ANTICIPO : CUENTA_LIQUIDACION ; // CUENTA CONTABLE
            
               
               String dato =    OCCIDENTE_TIPO_REGISTRO_DET                                                  +  // 1.1   Tipo Registro detalle
                                rellenar( String.valueOf(transf.getSecuencia()),     CERO,   4, IZQUIERDA  ) +  // 1.2   Consecutivo del registro                                
                                rellenar( this.CTA_PROVEEDOR,                        CERO,  16, IZQUIERDA  ) +  // 1.3   Cta Beneficiario                                
                                rellenar( transf.getNombre_cuenta(),                 SPACE, 30, DERECHA    ) +  // 1.4   Nombre del beneficiario                                
                                rellenar( transf.getNit_cuenta(),                    CERO,  11, IZQUIERDA  ) +  // 1.5   Nit beneficiario                                
                                rellenar( codigoBanco,                               CERO,   4, IZQUIERDA  ) +  // 1.6   Codigo banco beneficiario
                                this.getFecha(AAAAMMDD)                                                      +  // 1.7   Fecha a realizar el pago                                 
                                formaPago                                                                    +  // 1.8   Forma de pago 
                                rellenar( String.valueOf(valor) + "00",              CERO,  15, IZQUIERDA  ) +  // 1.9   Valor                                
                                rellenar( cuenta,                                    SPACE, 16, DERECHA    ) +  // 1.10  N�mero de la Cta                                 
                                rellenar( comprobante,                               SPACE, 12, IZQUIERDA  ) +  // 1.11  Comprobante                          
                                tipo_transaction                                                             +  // 1.12  Tipo de transacction                                
                                rellenar( OCCIDENTE_CONCEPTO,                        SPACE,  80, DERECHA   ) ;  // 1.13  Concepto de pago 
                                
                                
              linea.println( dato );
                   
               
           } catch(Exception e){  
             throw new Exception( " occidente " + e.getMessage());
          }
    }
    
    
     /**
     * Metodo que  devuelve la equivalencia tipo de transacci�n de acuerdo al banco
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public String equivalenciaTipoTransaction(String codeBanco, String tipo){
        String equivale = tipo;
        
        // BANCOLOMBIA
            if(codeBanco.equals(BANCOLOMBIA)){
               if( tipo.equals( CTA_AHORR0     ))  equivale = BANCOLOMBIA_CTA_AHORR0;
               if( tipo.equals( CTA_CORRIENTE  ))  equivale = BANCOLOMBIA_CTA_CORRIENTE;                
               if( tipo.equals( CTA_EFECTIVO   ))  equivale = BANCOLOMBIA_CTA_EFECTIVO;                
            }
        
        
        // OCCIDENTE
            if(codeBanco.equals( OCCIDENTE )){
               if( tipo.equals( CTA_AHORR0     ))  equivale = OCCIDENTE_CTA_AHORR0;
               if( tipo.equals( CTA_CORRIENTE  ))  equivale = OCCIDENTE_CTA_CORRIENTE;                
               if( tipo.equals( CTA_EFECTIVO   ))  equivale = OCCIDENTE_CTA_EFECTIVO;                
            }
        
        return equivale;
    }
     
    
    /**
     * Metodo que  devuelve la equivalencia tipo de cuenta de acuerdo al banco
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public String equivalenciaTipoCta(String codeBanco, String tipo){
        String equivale = tipo;
        
        // BANCOLOMBIA
            if(codeBanco.equals(BANCOLOMBIA)){
               if( tipo.equals(CTA_AHORR0))     equivale = "S";
               if( tipo.equals(CTA_CORRIENTE))  equivale = "D";
               if(tipo.equals(CPAG))   equivale="D";
            }
        
        return equivale;
    }
    
    
    
     
    /**
     * Metodo para sacar la equivalencia de un caracter decimal
     * en un caracter de notacion
     * @autor mfontalvo
     */
    public static String convertLetra(int numero){
        return  String.valueOf(  (char) (numero + 65) );
    }  
    
    
     
     /**
    * M�todo que rellena los campos
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/  
    public  String rellenar(String cadena, String caracter, int tope, String posicion)throws Exception{
       try{
           int lon = cadena.length();
           if(tope>lon){
             for(int i=lon;i<tope;i++){
                 if(posicion.equals( DERECHA))    cadena += caracter;
                 else                                 cadena  = caracter + cadena;
             }
           }
           else
               cadena = Trunc(cadena, tope );
           
       }catch(Exception e){
           throw new Exception(e.getMessage());
       }
       return cadena;
    }

    
   
   /**
    * M�todo que trunca cadena
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/  
   public static String Trunc(String Cadena, int Longitud){
       return (Cadena==null?Cadena: (Cadena.length()>=Longitud? Cadena.substring(0,Longitud):Cadena  )  );
   }

    
    
     
     
     /**
     * Metodo que crea el archivo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void createFile()throws Exception{
         try{
             
             String nameFile     =  getNombreArchivo( this.DESC_BANCO_PROVEEDOR );
             model.DirectorioSvc.create(this.usuario.getLogin());
             System.out.println(" crear archivo "+ this.usuario.getLogin()+"/"+ nameFile);
             String ruta         =  model.DirectorioSvc.getUrl() + this.usuario.getLogin()  +"/"+ nameFile;
             
             URL_TEM_TRANS       =  ruta;
             
             this.fw             = new FileOutputStream    (ruta   );
             this.bf             = new BufferedWriter( new OutputStreamWriter(fw,"ISO-8859-1" ) );
             this.linea          = new PrintWriter   (this.bf);
         }catch(Exception e){
             throw new Exception( e.getMessage() );
         }
     }
     
     
     
            
     /**
     * Metodo que guarda el archivo de transferencia
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public void saveTransfer(){
        this.linea.close();
    }
    
    
     
     
    /**
     * Metodo que crea el archivo para migrar a mims MSF265
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void createFile265()throws Exception{
         try{
             
             String hora         =  Util.getFechaActual_String(6).replaceAll("/|:","").replaceAll(" ","_"); 
             String ruta265      =  model.DirectorioSvc.getUrl() + this.usuario.getLogin()  +"/MSF265_"+  hora   +".txt";
             
             URL_TEM_MSF265      = ruta265;
             
             this.fw265          = new FileOutputStream    ( ruta265     );
             this.bf265          = new BufferedWriter( new OutputStreamWriter(fw265,"ISO-8859-1" ) );
             this.linea265       = new PrintWriter   ( this.bf265  );
             
         }catch(Exception e){
             throw new Exception( e.getMessage() );
         }
     }
     
     
     
     /**
     * Metodo que guarda el archivo para migrar a mims
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public void save265(){
        this.linea265.close();
    }
     
     
     
    
    
    /**
     * Metodo que borra los archivos en caso de error
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void deleteFiles()throws Exception{
         try{
             
             File  fT =  new File(URL_TEM_TRANS);   fT.delete();
             File  fM =  new File(URL_TEM_MSF265);  fM.delete();
             
         }catch(Exception e){
             throw new Exception( e.getMessage() );
         }
     }
    
     
    
     
    /**
    * M�todo que permite crear nombre del archivo
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/   
    public String getNombreArchivo(String desBanco)throws Exception{ 
        String name    ="";
        try{
            String hora    =  Util.getFechaActual_String(6).replaceAll("/|:","").replaceAll(" ","_");             
                   name    =  desBanco +"_"+ hora + ".txt";
        }catch(Exception e){
           throw new Exception("getNombreArchivo " + e.getMessage());
        }
        return name;        
    }
     
    
    
         
     /**
    * M�todo que permite dar la fecha actual dependiendo el formato
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/  
    public String getFecha(String formato)throws Exception{        
       String Fecha = "";
       try{
           
            SimpleDateFormat FMT = null;
            FMT = new SimpleDateFormat(formato);      
            Fecha = FMT.format(new Date());
            
       }catch(Exception e){
           throw new Exception("getFecha " + e.getMessage());
       }
       return Fecha.toUpperCase() ;
    }

  

    /**
     * Metodo que  ecribe al archivo MSF265 para MIMS
     * @autor: ....... Fernel Villacob
     * @Modificado ... Julio Barros Rueda
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void write265(AnticiposTerceros  trans)throws Exception{
        try{
            
            
             String  GRABADOR           = "79458875";        //  Pablo Rosales
             String  CUENTA             = (trans.getReanticipo().equals("N") )?CUENTA_ANTICIPO : CUENTA_LIQUIDACION ;   // CUENTA CONTABLE REGISTRO
             
             
             String  FACTURA            =  trans.getFactura_mims();
             
             String  FECHA              =  Util.getFechaActual_String(5) +"/"+  Util.getFechaActual_String(3) +"/"+ Util.getFechaActual_String(1).substring(2,4);
             String  COMA               = ",";
             
             double  valor              = (int) Math.round( trans.getVlr());
             
             String  banco              = "";
             String  sucursal           = "";
             String  mims               = "";
             double  vlr                = 0;
             banco     = "";
             sucursal  = "";
             mims      = "";
             vlr       = 0;
             Hashtable bancoTer      =  model.AnticiposPagosTercerosSvc.getBancoNIT( trans.getProveedor_anticipo()  );
             if( bancoTer!= null  ){
                 banco     = (String) bancoTer.get("banco");
                 sucursal  = (String) bancoTer.get("sucursal");
                 mims      = (String) bancoTer.get("mims");
             }
             vlr   =  valor;
             
             String  DESCRIPCION        = "PP Fintra Liq " + trans.getFactura_mims()+" Por $"+valor;
             
             String SPACE2 = "";
             
             String  datoProveedor   =  
                                        banco.trim()                  + COMA +
                                        sucursal.trim()               + COMA +
                                        GRABADOR.trim()               + COMA +                                        
                                        mims.trim()                   + COMA +
                                        FACTURA.trim()                + COMA + 
                                        FECHA.trim()                  + COMA +  
                                        FECHA.trim()                  + COMA +
                                        String.valueOf( vlr ).trim()  + COMA +
                                        SPACE2.trim()                 + COMA + 
                                        DESCRIPCION.trim()            + COMA + 
                                        String.valueOf( vlr ).trim()  + COMA +
                                        SPACE2.trim()                 + COMA +
                                        GRABADOR.trim()               + COMA +
                                        SPACE2.trim()                 + COMA +
                                        CUENTA.trim()
                                        ;
            
           //  linea265.println( datoPropietario );
             linea265.println( datoProveedor   );
             
             
        } catch(Exception e){  
            throw new Exception(e.getMessage());
        }  
    }
    
  
     /**
     * Metodo que  permite ejecutar el SQL
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
      public void ejecutar(String sql)throws Exception{
        try{
            TransaccionService  svc =  new  TransaccionService(usuario.getBd());
            svc.crearStatement();
            svc.getSt().addBatch(sql);
            svc.execute();
            
        }catch(Exception e){
            
            System.out.println("SQL: " + sql );
            
            throw new Exception( e.getMessage() );
        }
    }
    
      
     /**
     * M�todo que ejecuta el proceso de  generaci�n del archivo
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public synchronized void run(){
       try{
            String comentario="EXITOSO";
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(), this.procesoName , this.usuario.getLogin() );
            String control = validarFormatosRealizados(this.BANCO_PROVEEDOR);
            if(control.equals("")){
                  //listaAgrupada = model.AnticiposPagosTercerosSvc.getApruparF(anticipos);
                  if(PRESTAMO){
                      listaAgrupada = model.AnticiposPagosTercerosSvc.groupByPrestamos(anticipos);
                  }else{
                      //listaAgrupada = model.AnticiposPagosTercerosSvc.getApruparF(anticipos);
                      listaAgrupada = model.AnticiposPagosTercerosSvc.getApruparN(anticipos,cxps);
                  }
                  if(  listaAgrupada.size()>0 ){
                           // Buscamos la transferencia:
                            TRANSFERENCIA = model.AnticiposPagosTercerosSvc.getSerie();
                            if( !TRANSFERENCIA.equals("") ){
                                // Definimos factura
                                this.TOTALDETALLE  = listaAgrupada.size();
                                for(int i=0;i<listaAgrupada.size();i++){
                                      AnticiposTerceros  trans  = (AnticiposTerceros) listaAgrupada.get(i);
                                      trans.setSecuencia      (i+1);
                                      //trans.setFactura_mims   (  TRANSFERENCIA +"_"+ trans.getSecuencia()  );
                                      trans.setTransferencia  ( this.TRANSFERENCIA       );
                                      this.TOTALCREDITO +=  trans.getVlrConsignar();
                                }
                                try{           
                                    if(this.BANCO_PROVEEDOR.equals("10000003")){ //cuando es efecty
                                        // ARCHIVOS:
                                        // Escribimos en el archivo para la transferencia:
                                        createFile();
                                        writeHeader(this.BANCO_PROVEEDOR);
                                        saveTransfer(); 
                                    }else{
                                    // ARCHIVOS:
                                    // Escribimos en el archivo para la transferencia:
                                       createFile();
                                       obtenerTipoTransaccion(BANCO_PROVEEDOR, TIPO_CTA_PROVEEDOR);
                                       writeHeader(this.BANCO_PROVEEDOR);
                                       for(int i=0;i<listaAgrupada.size();i++){
                                            //System.out.println("para ------------------------------>");
                                             AnticiposTerceros  trans  = (AnticiposTerceros) listaAgrupada.get(i);
                                             writeTransfer(this.BANCO_PROVEEDOR, trans);
                                       }
                                       writeFinal(this.BANCO_PROVEEDOR);
                                       saveTransfer(); 
                                    }
        


                                              
                             }catch(Exception e){
                                deleteFiles();
                                throw new Exception( e.getMessage() );
                             }
                       }
                       else
                          comentario = "No hay serie de transferencia...";
                  }
                  else
                      comentario = "No se agruparon los registros...";
            }
            else
                comentario = control;
            //System.out.println("pasa a escribir en el log de prosesos");
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);          
       }catch(Exception e){
           try{
               model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage()); 
           }catch(Exception f){ }   
       }
    }
     
     
     /**
     * Metodo que ecribe los las transferencias a realizar en formato BANCOLOMBIA PAB
     * @autor Diana Arrieta
     * @throws Exception
     */
    public void bancolombiaPAB(AnticiposTerceros  transf)throws Exception{
           try{

               // 2. DETALLE DE TRANSACCION:

               String tipoCta = transf.getTipo_cuenta();


               // Codigo del  banco del beneficiario
               String codigoBanco = CERO;
               Hashtable infoBanco = model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_BANCOLOMBIA_BANCO, transf.getBanco());
               if (infoBanco == null) {
                   throw new Exception("No hay informaci�n del banco  " + transf.getBanco() + " en formato bancolombia");
               } else {
                   codigoBanco = (String) infoBanco.get("descripcion");
               }


               // Indicador de pago: sucursal
               String indicador_pago = "S";

               // Tipo de transacion:
               String tipo_transaction = equivalenciaTipoTransaction(BANCOLOMBIA, tipoCta);

               // Valor
               int valor = (int) transf.getVlrConsignar();

               String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO_DET                                      +  // 1  Tipo de Registro detalle
                                     rellenar( transf.getNit_cuenta(),          SPACE,  15, DERECHA   ) +  // 2  Nit beneficiario
                                     rellenar( transf.getNombre_cuenta(),       SPACE, 30, DERECHA    ) +  // 3  Nombre del beneficiario
                                     rellenar( codigoBanco,                     CERO,   9, IZQUIERDA  ) +  // 4  Codigo banco
                                     rellenar( transf.getCuenta(),              SPACE,  17, DERECHA   ) +  // 5  N�mero de la Cta
                                     indicador_pago                                                     +  // 6  Lugar de pago
                                     tipo_transaction                                                   +  // 7  Tipo de Transaction
                                     rellenar( String.valueOf(valor) + "00",    CERO,  17, IZQUIERDA  ) +  // 8  Valor
                                     this.getFecha(AAAAMMDD)                                            +  // 9  Fecha aplicaci�n  AAAAMMDD
                                     rellenar( this.BANCOLOMBIA_PROPOSITO ,     SPACE,  9, DERECHA    ) +  // 10  Proposito descripci�n
                                     rellenar( transf.getLote_transferencia(),        SPACE, 12, DERECHA    ) +  // 11 referencia                       
                                     rellenar( SPACE,                           SPACE,  1, DERECHA    ) +  // 12 Tipo de documento de identificaci�n
                                     rellenar( SPACE,                           SPACE,  5, DERECHA    ) +  // 13 Oficina de entrega
                                     rellenar( SPACE,                           SPACE, 15, DERECHA    ) +  // 14 N�mero de Fax
                                     rellenar( SPACE,                           SPACE, 80, DERECHA    ) +  // 15 E-mail beneficiario
                                     rellenar( SPACE,                           SPACE, 15, DERECHA    ) +  // 16 N�mero identificaci�n del autorizado
                                     rellenar( SPACE,                           SPACE, 27, DERECHA    );   // 17 Filler

              linea.println( dato );


           } catch(Exception e){
             throw new Exception( " bancolombia " + e.getMessage());
          }
    }
    
    
        /**
     * Obtiene el tipo de tranasaccion para un banco
     * @param codeBanco
     * @param listaAnticipos 
     */
    public void obtenerTipoTransaccion(String codeBanco, String tipo){
        // BANCOLOMBIA
        if(codeBanco.equals(BANCOLOMBIAPAB)){
            if(tipo.equals(CPAG)){
                BANCOLOMBIA_TIPO_TRANSACTION = BANCOLOMBIA_CPAG_TIPO_TRANSACTION;
            }
        }
    } 
    

}
//Entregado a Jbarros 21 Febrero
