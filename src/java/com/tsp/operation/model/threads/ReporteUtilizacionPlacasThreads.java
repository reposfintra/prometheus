/*
 * ReporteUtilizacionPlacasThreads.java
 *
 * Created on 24 de abril de 2007, 10:41 AM
 */

package com.tsp.operation.model.threads;

import com.aspose.cells.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;

/**
 *
 * @author  EQUIPO26
 */
public class ReporteUtilizacionPlacasThreads extends Thread{
    
    String user;
    String FechaI1;
    String FechaF1;
    String Agencia;
    String procesoName;
    String des;
    Model model = new Model();
    
    /** Creates a new instance of ReporteUtilizacionPlacasThreads */
    public void start(String FechaI, String FechaF, String Agencia, String usuario){
        this.user = usuario;
        this.procesoName = "Reporte Analisis de Flota";
        this.des = "Reporte Analisis de Flota";
        this.FechaI1 = FechaI;
        this.Agencia = Agencia;
        this.FechaF1 = FechaF;
        //this.dstrct = dstrct;
        super.start();
    }
    
   public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
            Vector vector =  model.reporteGeneralService.reporteUtilizacionPlaca(FechaI1, FechaF1, Agencia);
            Date fecha = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String now = format.format(fecha);
            String fechaCrea = now.substring(0,10);

            Workbook libro = new Workbook();

            Worksheets hojas = libro.getWorksheets();

            Worksheet hoja = hojas.getSheet(0);

            hoja.setName("HOJA1");
            
            
            Cells celdas = hoja.getCells();
            
            //datos de los titulos
            Font fuenteTitulo = new Font();
            fuenteTitulo.setBold(true);
            fuenteTitulo.setName("arial");
            fuenteTitulo.setColor( com.aspose.cells.Color.BLACK );
            
            //CELDA TITULO
            Cell celda = hoja.getCell(0,0);
            Style estiloEmpresa = libro.createStyle();
            estiloEmpresa.setColor( com.aspose.cells.Color.WHITE );
            celda.setValue("TRANSPORTES SANCHEZ POLO ");
            celda.setStyle(estiloEmpresa);
            estiloEmpresa.setFont ( fuenteTitulo ); 
            estiloEmpresa.getBorderColor(2);
            celdas.merge(0, 0, 0, 2);
            //hoja.autoFitColumns();
            

            
            //CELDA FECHA INICIO
            celda = hoja.getCell(1,0);
            celda.setValue("FECHA INICIO : "+FechaI1);
            celda.setStyle(estiloEmpresa);
            estiloEmpresa.setFont ( fuenteTitulo ); 
            estiloEmpresa.getBorderColor(2);
            celdas.merge(1, 0, 1, 2);
            
            //CELDA FECHA FINAL
            celda = hoja.getCell(2,0);
            celda.setValue("FECHA FINAL : "+FechaF1);
            celda.setStyle(estiloEmpresa);
            estiloEmpresa.setFont ( fuenteTitulo ); 
            estiloEmpresa.getBorderColor(2);
            celdas.merge(2, 0, 2, 2);
            
              
            
            //datos de los sub titulos
            Font fuenteSubTitulo = new Font();
            fuenteSubTitulo.setBold(true);
            fuenteSubTitulo.setName("arial");
            fuenteSubTitulo.setColor( com.aspose.cells.Color.BLUE );

            Style estiloSubTitulo = libro.createStyle();
            estiloSubTitulo.setColor( com.aspose.cells.Color.YELLOW );
            estiloSubTitulo.setHAlignment( HorizontalAlignmentType.CENTRED );
            
            celda = hoja.getCell(5,0);
            celda.setValue("VIAJES DESPACHO");
            celda.setStyle( estiloSubTitulo );
            estiloSubTitulo.setFont ( fuenteSubTitulo ); 
            estiloSubTitulo.getBorderColor(2);
            celdas.merge(5, 0, 5, 7);
            
            celda = hoja.getCell(5,9);
            celda.setValue("REEXPEDICION");
            celda.setStyle( estiloSubTitulo );
            estiloSubTitulo.setFont ( fuenteSubTitulo ); 
            estiloSubTitulo.getBorderColor(2);
            celdas.merge(5, 9, 5, 16);            
            
            
            int columnas = 0;
            int filas = 6;
            
            //fuente de los datos
            Font fuente = new Font();
            fuente.setBold(true);
            fuente.setName("arial");
            fuente.setColor( com.aspose.cells.Color.WHITE );  
            
            Style estiloDatos = libro.createStyle();
            estiloDatos.setColor(com.aspose.cells.Color.BLUE);
            estiloDatos.setFont ( fuente ); 
            
            estiloDatos.setHAlignment( HorizontalAlignmentType.CENTRED );
           
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("FECHA IMPRESION");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("PLACA");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("TIPO DE VIAJE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("ORIGEN");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("DESTINO");
            celda.setStyle(estiloDatos);
            hoja.setColumnWidth( columnas,12 );
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("CLIENTE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("FECHA ENTREGA");
            celda.setStyle(estiloDatos);
            
            columnas ++;
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("FECHA IMPRESION");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("PLACA");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("TIPO DE VIAJE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("ORIGEN");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("DESTINO");
            celda.setStyle(estiloDatos);
            hoja.setColumnWidth( columnas,12 );
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("CLIENTE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("FECHA ENTREGA");
            celda.setStyle(estiloDatos);
            
            columnas = 1;
            
            for(int i=0; i<vector.size();i++ ){
                columnas =0;
                filas++ ;
          
                Hashtable ht = (Hashtable) vector.get(i);
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "fecha_impresion" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "placa" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "tipoviaje" ) );
                                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "oc" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "origen" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "destino" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "cliente" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "fecha_reporte" ) );
                
                columnas++;
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "Rfecha_impresion" )==null?"":ht.get( "Rfecha_impresion" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "Rplaca" )==null?"":ht.get( "Rplaca" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "Rtipoviaje" )==null?"":ht.get( "Rtipoviaje" ) );
                                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "Roc" )==null?"":ht.get( "Roc" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "Rorigen" )==null?"":ht.get( "Rorigen" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "Rdestino" )==null?"":ht.get( "Rdestino" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "Rcliente" )==null?"":ht.get( "Rcliente" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "Rfecha_reporte" )==null?"":ht.get( "Rfecha_reporte" ) );

            }
           
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
            libro.save(Ruta1 +"ReporteAnalisisFlota_" + hoy + ".xls");
             
        System.out.println("YA");
          
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    
    public static void main(String[]sfhgsd) throws Exception{
        ReporteUtilizacionPlacasThreads h = new ReporteUtilizacionPlacasThreads();
        h.start("2007-04-01", "2007-04-17", "ST", "HOSORIO");
    }    
    
}
