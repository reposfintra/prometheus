/*
 * HImportarTexto.java
 *
 * Created on 24 de marzo de 2009, 14:40
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;

import java.util.*;
import java.text.*;

import com.tsp.operation.model.LogProcesosService;

/**
 *
 * @author  Fintra
 */
public class HImportarTexto  extends Thread{
    
    
    private Model   model;
    private Usuario usuario;
    private String procesoName;
    
    /** Creates a new instance of HImportarTexto */
    public HImportarTexto() {
    }
    
    public void start(Model model, Usuario usuario){

        this.usuario = usuario;
        this.model = model;
        this.procesoName = "Procesamiento de Avales";

        super.start();
    }


    public synchronized void run(){
        LogProcesosService log = new LogProcesosService(usuario.getBd());
        try{    
            
            log.InsertProceso(this.procesoName, this.hashCode(), "Gestion de avales "  ,this.usuario.getLogin() ); 
            List listica=model.importeTextoService.searchArchivos("aval", usuario.getLogin());
            model.importeTextoService.setProcess( false );//20100706
            String comentario    =  model.importeTextoService.getESTADO(); 
            //model.ContabilizacionNegSvc.setProcess( false );           
            log.finallyProceso(this.procesoName, this.hashCode(), this.usuario.getLogin(),comentario);              
           
        }catch (Exception ex){
            try{//20100706
		model.importeTextoService.setProcess( false );//20100706
		log.finallyProceso(this.procesoName, this.hashCode(), this.usuario.getLogin(),"error:"+ex.toString());//20100706
	    }catch (Exception e){//20100706
		System.out.println("error en himportartexto"+e.toString());
		e.printStackTrace();//20100706
	    }//20100706
	    ex.printStackTrace(); //20100706
            //ex.printStackTrace();            
             System.out.println("Error HImportarTexto ...\n" + ex.getMessage());
        }
        
    }


}

