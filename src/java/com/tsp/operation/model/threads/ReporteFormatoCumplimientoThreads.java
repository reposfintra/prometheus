/*
 * ReporteFormatoCumplimientoThreads.java
 *
 * Created on 1 de noviembre de 2006, 11:42 AM
 */

package com.tsp.operation.model.threads;

import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author  EQUIPO13
 */
public class ReporteFormatoCumplimientoThreads extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String dstrct = "";
    String agencia = "";
    String cliente = "";
    String fecha_inicial = "";
    String fecha_final = "";
    Model model;
    /** Creates a new instance of ReporteFormatoCumplimientoThreads */
    public ReporteFormatoCumplimientoThreads ( Model model ) {
        this.model = model;
    }
    
    public void start(String usuario, String dstrct, String fecha_inicial, String fecha_final, String agencia, String cliente){        
        this.id = usuario;
        this.dstrct = dstrct;
        this.procesoName = "Reporte Formato Cumplimiento";
        this.des = "Generacion del Reporte Formato Cumplimiento creado por "+usuario;
        this.fecha_inicial = fecha_inicial;
        this.fecha_final = fecha_final;
        this.agencia = agencia;
        this.cliente=cliente;
        super.start();
    }
    
    public synchronized void run(){
        try{
            String fecha_actual = Util.getFechaActual_String(6);
            
            //registramos el proeso en el log de procesos
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            
            //obtenemos la lista de facturas creadas segun parametros definidos
            Vector lista = model.reporteGeneralService.reporteFormatoCumplimiento (fecha_inicial, fecha_final,agencia,cliente);
            System.out.println("Lista "+lista.size());
            System.gc();
            if (lista!=null && lista.size()>0){
                
                Usuario us = (Usuario) model.usuarioService.obtenerUsuario(id);
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                File file = new File(path);
                file.mkdirs();
                
                String a�o = fecha_actual.substring(0,4);
                String mes = fecha_actual.substring(5,7);
                String dia = fecha_actual.substring(8,10);
                String hora = fecha_actual.substring(11,13);
                String min = fecha_actual.substring(14,16);
                String seg = fecha_actual.substring(17,19);
                
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite(path+"/REPORTE_FORMATO_CUMP " +a�o+"-"+mes+"-"+dia+ ".xls");
                
                // Definicion de estilos: fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado                                           
                HSSFCellStyle fecha = xls.nuevoEstilo(      "verdana", 12, false , false, "yyyy/mm/dd", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_RIGHT );
                HSSFCellStyle texto = xls.nuevoEstilo(      "verdana", 12, false, false, "text", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle numero = xls.nuevoEstilo(     "verdana", 12, false, false, "_(#,##0.00_);(@_)", xls.NONE, xls.NONE, HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle negrita = xls.nuevoEstilo(    "verdana", 12, true, false, "text", HSSFColor.BLACK.index, xls.NONE, xls.NONE);
                HSSFCellStyle header = xls.nuevoEstilo(     "verdana", 18, true, false, "text", HSSFColor.LIME.index,xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo = xls.nuevoEstilo(     "verdana", 16, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle subtitulo = xls.nuevoEstilo(  "verdana", 14, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle cabecera = xls.nuevoEstilo(   "verdana", 12, true, false, "text", HSSFColor.WHITE.index, HSSFColor.INDIGO.index, HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja("BASE");
                //formateo al abrir hoja (tamano)
                xls.cambiarMagnificacion(3,4);
                
                //Titulo (cabecera)
                xls.adicionarCelda(0, 0, "TRANSPORTES SANCHEZ POLO S.A", header);
                xls.combinarCeldas(0, 0, 0, 5);  
                xls.adicionarCelda(2, 0, "FORMATO DE ARCHIVO DE CUMPLIMIENTO", titulo);
                xls.combinarCeldas(2, 0, 0, 5);
                xls.adicionarCelda(3, 0, "DISTRITO:", subtitulo);
                xls.adicionarCelda(3, 1, dstrct.toUpperCase (), negrita);
                xls.adicionarCelda(4, 0, "ELABORADO POR : ", subtitulo);
                xls.adicionarCelda(4, 1, us.getNombre().toUpperCase (), negrita);
                xls.adicionarCelda(5, 0, "FECHA INICIAL :", subtitulo);
                xls.adicionarCelda(5, 1, fecha_inicial, negrita);
                xls.adicionarCelda(6, 0, "FECHA FINAL :", subtitulo);
                xls.adicionarCelda(6, 1, fecha_final, negrita);

                
                int fila = 6;
                int col  = 0;
                
                //definicion de titulos de la tabla
                xls.adicionarCelda(fila ,col++ , "PEDIDO"                                           , cabecera );
                xls.adicionarCelda(fila ,col++ , "DELIVERY"                                         , cabecera );
                xls.adicionarCelda(fila ,col++ , "CLIENTE "                                         , cabecera );
                xls.adicionarCelda(fila ,col++ , "CIUDAD DESTINO / ZONA"                            , cabecera );
                xls.adicionarCelda(fila ,col++ , "PESO KGS"                                         , cabecera );
                xls.adicionarCelda(fila ,col++ , "TIPO DE VEHICULO"                                 , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA SOLICITUD"                                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "HORA SOLICITUD"                                   , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA MAX CARGUE-24 HORAS"                        , cabecera );
                xls.adicionarCelda(fila ,col++ , "HORA MAX CARGUE-24 HORAS"                         , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA REAL MAX CARGUE SEG�N SOLICITUD(24 HORAS)"  , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA MAX CARGUE-36 HORAS"                        , cabecera );
                xls.adicionarCelda(fila ,col++ , "HORA MAX CARGUE-36 HORAS"                         , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA REAL MAX CARGUE SEG�N SOLICITUD(36 HORAS)"  , cabecera );
                xls.adicionarCelda(fila ,col++ , "PROGRAMADO"                                       , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA/HORA PROGRAMADA CARGUE"                     , cabecera );
                xls.adicionarCelda(fila ,col++ , "PLACA"                                            , cabecera );
                xls.adicionarCelda(fila ,col++ , "CEDULA CONDUCTOR"                                 , cabecera );
                xls.adicionarCelda(fila ,col++ , "NOMBRE CONDUCTOR"                                 , cabecera );
                xls.adicionarCelda(fila ,col++ , "COMENTARIOS"                                      , cabecera );
                xls.adicionarCelda(fila ,col++ , "NUMERO FACTURA"                                   , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA LLEGADA PLANTA"                             , cabecera );
                xls.adicionarCelda(fila ,col++ , "HORA LLEGADA PLANTA"                              , cabecera );
                xls.adicionarCelda(fila ,col++ , "HORA DE RADICADA ORDEN DE CARGUE"                 , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA ENTRADA CARGUE"                             , cabecera );
                xls.adicionarCelda(fila ,col++ , "HORA ENTRADA CARGUE"                              , cabecera );
                xls.adicionarCelda(fila ,col++ , "HORA TERMINACION DE CARGUE"                       , cabecera );
                xls.adicionarCelda(fila ,col++ , "HORA ENTREGA DOCUMENTOS XOM"                      , cabecera );
                xls.adicionarCelda(fila ,col++ , "HORA SALIDA PLANTA"                               , cabecera );
                xls.adicionarCelda(fila ,col++ , "TIEMPO DE ESPERA"                                 , cabecera );
                xls.adicionarCelda(fila ,col++ , "TIEMPO ENTREGA DOCUMENTOS"                        , cabecera );
                xls.adicionarCelda(fila ,col++ , "TIEMPO DE CARGUE"                                 , cabecera );
                xls.adicionarCelda(fila ,col++ , "OT"                                               , cabecera );
                xls.adicionarCelda(fila ,col++ , "OC"                                               , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA DE CARGUE"                                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "DIFERENCIA FECHA COLOCACION - PROGRAMADA"         , cabecera );
                xls.adicionarCelda(fila ,col++ , "CARGADO NO VACIO"                                          , cabecera );
                xls.adicionarCelda(fila ,col++ , "CAUSA"                                            , cabecera );
                xls.adicionarCelda(fila ,col++ , "DIFERENCIA FECHA CARGUE - FECHA REAL MAX CARGUE SEG�N SOLICITUD(36 HORAS)"    , cabecera );
                xls.adicionarCelda(fila ,col++ , "TIEMPO CARGUE DESPU�S DE SOLICITUD"               , cabecera );
                xls.adicionarCelda(fila ,col++ , "CAUSA"                                            , cabecera );
                xls.adicionarCelda(fila ,col++ , "DIFERENCIA FECHA CARGUE - FECHA REAL MAX CARGUE SEG�N SOLICITUD(24 HORAS)"    , cabecera );
                xls.adicionarCelda(fila ,col++ , "OTROS"                                            , cabecera );
                xls.adicionarCelda(fila ,col++ , "ORIGEN REMESA"                                            , cabecera );
                xls.adicionarCelda(fila ,col++ , "ORIGEN PLANILLA"                                            , cabecera );
                System.gc();
                //definicion de la tabla con los datos obtenidos de la consulta
                for (int i = 0; i < lista.size (); i++ ){
                    System.out.println("Agregando fila "+i);
                    fila++;
                    col = 0;
                    ReporteFormatoCumplimiendo r = (ReporteFormatoCumplimiendo) lista.get(i);
                    xls.adicionarCelda(fila ,col++ , r.getPedido ()                             , texto );
                    xls.adicionarCelda(fila ,col++ , r.getDelivery()                            , texto );
                    xls.adicionarCelda(fila ,col++ , r.getCliente ()                            , texto );
                    xls.adicionarCelda(fila ,col++ , r.getCiudad_destino ()                     , texto );
                    xls.adicionarCelda(fila ,col++ , r.getPeso ()                               , texto );
                    xls.adicionarCelda(fila ,col++ , r.getTipo_vehiculo ()                      , texto );
                    xls.adicionarCelda(fila ,col++ , "0099-01-01".equals(r.getFecha_solicitud ())?"":r.getFecha_solicitud ()                    , texto );
                    xls.adicionarCelda(fila ,col++ , "00:00:00".equals(r.getHora_solicitud ())?"":r.getHora_solicitud ()                     , texto );
                    xls.adicionarCelda(fila ,col++ , "0099-01-01".equals(r.getFecha_max_carge_24 ())?"":r.getFecha_max_carge_24 ()                 , texto );
                    xls.adicionarCelda(fila ,col++ , "0099-01-01".equals(r.getHora_max_carge_24 ())?"":r.getHora_max_carge_24 (), texto );
                    xls.adicionarCelda(fila ,col++ , "0099-01-01".equals(r.getMax_carge_solicitud_24 ())?"":r.getMax_carge_solicitud_24 ()             , texto );
                    xls.adicionarCelda(fila ,col++ , "0099-01-01".equals(r.getFecha_max_carge_36 ())?"":r.getFecha_max_carge_36 ()                 , texto );
                    xls.adicionarCelda(fila ,col++ ,  "0099-01-01".equals(r.getHora_max_carge_36 ())?"":r.getHora_max_carge_36 ()                  , texto );
                    xls.adicionarCelda(fila ,col++ ,  "0099-01-01".equals(r.getMax_carge_solicitud_36 ())?"":r.getMax_carge_solicitud_36 ()             , texto );
                    xls.adicionarCelda(fila ,col++ , r.getProgramado ()                         , texto );
                    xls.adicionarCelda(fila ,col++ , "0099-01-01".equals(r.getFecha_programada_carge ())?"":r.getFecha_programada_carge ()             , texto );
                    xls.adicionarCelda(fila ,col++ , r.getPlaca ()                              , texto );
                    xls.adicionarCelda(fila ,col++ , r.getCedula ()                             , texto );
                    xls.adicionarCelda(fila ,col++ , r.getNombre_cond ()                        , texto );
                    xls.adicionarCelda(fila ,col++ , r.getComentario ()                         , texto );
                    xls.adicionarCelda(fila ,col++ , r.getFactura ()                            , texto );
                    xls.adicionarCelda(fila ,col++ , r.getFecha_llegada_planta ()               , texto );
                    xls.adicionarCelda(fila ,col++ , r.getHora_llegada_planta ()                , texto );
                    xls.adicionarCelda(fila ,col++ , r.getHora_radicada_ocarge ()               , texto );
                    xls.adicionarCelda(fila ,col++ , r.getFecha_entrada_carge ()                , texto );
                    xls.adicionarCelda(fila ,col++ , r.getHora_entrada_carge ()                 , texto );
                    xls.adicionarCelda(fila ,col++ , r.getHora_terminacion_carge ()             , texto );
                    xls.adicionarCelda(fila ,col++ , r.getHora_entrega_doc_xom ()               , texto );
                    xls.adicionarCelda(fila ,col++ , r.getHora_salida_planta ()                 , texto );
                    xls.adicionarCelda(fila ,col++ , "00:00".indexOf(r.getFecha_programada_carge ())>=0?"":r.getTiempo_espera ()                      , texto );
                    xls.adicionarCelda(fila ,col++ , r.getTiempo_entrega_doc ()                 , texto );
                    xls.adicionarCelda(fila ,col++ , r.getTiempo_carge ()                       , texto );
                    xls.adicionarCelda(fila ,col++ , r.getOt ()                                 , texto );
                    xls.adicionarCelda(fila ,col++ , r.getOc ()                                 , texto );
                    xls.adicionarCelda(fila ,col++ , r.getFecha_carge ()                        , texto );
                    xls.adicionarCelda(fila ,col++ ,"00:00".indexOf(r.getFecha_diferencia_colocacion () )==0?"":r.getFecha_diferencia_colocacion ()      , texto );
                    xls.adicionarCelda(fila ,col++ , r.getCargado ()                            , texto );
                    xls.adicionarCelda(fila ,col++ , r.getCausa1 ()                             , texto );
                    xls.adicionarCelda(fila ,col++ ,"00:00".indexOf(r.getDiferencia_fcarge_freal24 ()  )==0?"": r.getDiferencia_fcarge_freal24 ()  , texto );
                    xls.adicionarCelda(fila ,col++ , r.getTiempo_carge_despues_sol ()           , texto );
                    xls.adicionarCelda(fila ,col++ , r.getCausa2 ()                             , texto );
                    xls.adicionarCelda(fila ,col++ , "00:00".indexOf(r.getDiferencia_fcarge_freal36 ())==0?"":  r.getDiferencia_fcarge_freal36 ()        , texto );
                    xls.adicionarCelda(fila ,col++ , r.getOtros()                               , texto );
                    xls.adicionarCelda(fila ,col++ , r.getOrirem()                              , texto );
                    xls.adicionarCelda(fila ,col++ , r.getOripla()                              , texto );

                }
                xls.cerrarLibro();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "Proceso Exitoso!");
            }else {
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "No se encontraron Registros...");
            }
            model.cxpDocService.setEnproceso();
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
                model.cxpDocService.setEnproceso();
            }
            catch(Exception f){
               try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    public static void main( String[]jkd )throws Exception{
        //Model model = new Model();
        com.tsp.operation.model.threads.ReporteFormatoCumplimientoThreads r = new com.tsp.operation.model.threads.ReporteFormatoCumplimientoThreads( new Model());
      //  r.start("KREALES", "FINV","2007-04-01", "2007-04-17");
        
    }
    
    
}
