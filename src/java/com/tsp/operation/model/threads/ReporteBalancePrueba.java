/*
 * ReporteBalancePrueba.java
 *
 * Created on 20 de octubre de 2005, 05:07 PM
 */

package com.tsp.operation.model.threads;

import java.io.*;
import java.util.*;

import java.io.*;
import java.sql.SQLException;
import com.tsp.pdf.*;
import java.util.*;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.Usuario;
/**
 *
 * @author  JuanM
 */
public class ReporteBalancePrueba extends Thread{
    private Usuario user;
    private Model model;
    private String path;
    
    private String anio;
    private int mes;
    private String distrito;
    private String periodo;
    private String procesoName;
    private String des;
    
    private BalancePruebaPDF2 balance;

    public ReporteBalancePrueba(String dataBaseName) {
        model = new Model(dataBaseName);
    }
    
    public void start(Usuario user, String distrito, int mes, String anio ){
        this.user       = user;
        this.distrito   = distrito;
        this.mes        = mes;
        this.anio       = anio;
        String indice = ( mes<10 )?"0"+mes:String.valueOf(mes);
        this.periodo    = anio + indice;
        this.procesoName = "Balance Prueba";
        this.des = "Reporte de Balance de Prueba";
        super.start();
    }
    
    public synchronized void run(){
        try{
            
                       
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(),this.des, this.user.getLogin() );
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            
            File route = new File(ruta + "/exportar/migracion/"+this.user );
            
            if( !route.exists() ) route.mkdir();
            
            //File pdf   = new File(ruta + "/exportar/migracion/"+this.user+"/BalancePrueba"+this.periodo+".pdf");                        
            
            String rep = ruta  + "/exportar/migracion/"+this.user.getLogin()+"/"+this.user.getBd()+"_BalancePrueba"+this.periodo+".pdf";
            
            balance = new BalancePruebaPDF2( rep , this.distrito, this.anio, this.mes, this.user );
            balance.reporte();
            
            String periodo = anio;
            String indice = ( mes<10 )?"0"+mes:String.valueOf(mes);
            periodo+=indice;
            
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.user.getLogin() ,"Proceso Exitoso");
            
        }catch (java.lang.OutOfMemoryError mem){
            mem.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.user.getLogin() ,"ERROR HILO : " + mem.getMessage());
            }catch(SQLException ex){}
        }
        catch(Exception e){
            //System.out.println(e.getMessage());
            e.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.user.getLogin() ,"ERROR HILO : " + e.getMessage());
            }catch(SQLException ex){}
        }
    }
    /*
    public static void main(String a []){
        ReporteBalancePrueba hilo = new ReporteBalancePrueba();
        hilo.start("HOSORIO", "TSP", 3, "2006");
    }
    */
}
