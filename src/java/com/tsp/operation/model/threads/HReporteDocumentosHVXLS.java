/*
 * HReporteDocumentosHVXLS.java
 *
 * Created on 15 de septiembre de 2006, 03:53 PM
 */

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;
/**
 *
 * @author  dbastidas
 */
public class HReporteDocumentosHVXLS extends Thread{
    String user;
    String fecha1;
    String fecha2;
    String agencia;
    String procesoName;
    String des;
    String dstrct;
    Model model = new Model();
    
    public void start(String FechaI, String FechaF ,String user, String Agencia,String dstrct){
        this.user = user;
        this.procesoName = "Documentos Digitalizados";
        this.des = "Reporte Documentos Digitalizados Hoja de vida";
        this.fecha1 = FechaI;
        this.fecha2 = FechaF;
        this.user = user;
        this.agencia = Agencia;
        this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
            //System.out.println("Inicia");
            
            Vector vec = model.reporteplacanitfotoService.obtenerDespachos(dstrct, agencia, fecha1, fecha2);
            ////System.out.println("Cantidad Registros "+vec.size());
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteDocumentos_HV" + hoy + ".xls", user, Util.getFechaActual_String(5));
            
            HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero      = xls.nuevoEstilo("Book Antiqua", 9, false , false, "#.##"         , xls.NONE , xls.NONE , xls.NONE );
            
            HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.LIGHT_BLUE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle titulo1      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.LIGHT_GREEN.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle encabezado      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.WHITE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            
            
            /****************************************
             *    FOTOS DE PLACAS Y CONDUCTORES     *
             ****************************************/
            
            xls.obtenerHoja("REPORTE");
            xls.cambiarMagnificacion(4,4);
            
            // cabecera
            
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", encabezado);
            xls.combinarCeldas(0, 0, 0, 2);
            xls.adicionarCelda(1,0, "DOCUMENTOS DIGITALIZADOS DE HOJA DE VIDA", encabezado);
            xls.combinarCeldas(1, 0, 0, 2);
            xls.adicionarCelda(2,0, "Fecha Inicial :"   , titulo);
            xls.adicionarCelda(2,1, fecha1 , fecha);
            xls.adicionarCelda(2,2, "Fecha Final :"   , titulo);
            xls.adicionarCelda(2,3, fecha2, fecha);
            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila ,col++ , "AGENCIA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "OC"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "RUTA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "DESPACHADOR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA DESPACHO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TIPO VIAJE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SALIDA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA POSIBLE LLEGADA"     , titulo );
            //PLACA
            xls.adicionarCelda(fila ,col++ , "PLACA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FOTO VEHICULO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "CERTIFICADO EMISION GASES"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "CONTRATO ARRENDAMIENTO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "CONTRATO COMPRAVENTA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "POLIZA ANDINA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "REGISTRO NACIONAL DE CARGA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "REGISTRO NACIONAL DE CARGA SEMIREMOLQUE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "SOAT"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "TARJETA DE HABILITACION"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "TARJETA EMPRESARIAL"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "TARJETA PROPIEDAD CABEZOTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "TARJETA PROPIEDAD SEMIREMOLQUE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            //CONDUCTOR
            xls.adicionarCelda(fila ,col++ , "CEDULA CONDUCTOR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CONDUCTOR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FOTO CONDUCTOR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "ARP"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "CEDULA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "CERTIFICADO JUDICIAL"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "EPS"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "FIRMA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "LIBRETA MILITAR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "LIBRETA TRIPULANTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "PASAPORTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "PASE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "VISA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "PRIMERA HUELLA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            xls.adicionarCelda(fila ,col++ , "SEGUNDA HUELLA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA SUBIDA DE IMAGEN "     , titulo );
            //Plan Viaje
            xls.adicionarCelda(fila ,col++ , "DEBE PERNOCTAR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PERNOCTACION"     , titulo1 );
            xls.adicionarCelda(fila ,col++ , "HOJA DE REPORTE"     , titulo1 );
            xls.adicionarCelda(fila ,col++ , "PLAN VIAJE"     , titulo1 );
            xls.adicionarCelda(fila ,col++ , "PUESTO 1"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TELEFONO "     , titulo );
            xls.adicionarCelda(fila ,col++ , "PUESTO 2"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TELEFONO "     , titulo );
            xls.adicionarCelda(fila ,col++ , "PUESTO 3"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TELEFONO "     , titulo );
            xls.adicionarCelda(fila ,col++ , "CELULAR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "NRO CELULAR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TIEMPO DE VIAJE PROYECTADO"     , titulo );
            
            
            
            
            // datos
            
            
            for(int i=0; i<vec.size();i++ ){
                fila++;
                col = 0;
                ReporteDocumentosHV rep = (ReporteDocumentosHV) vec.get(i);
                xls.adicionarCelda(fila ,col++ ,rep.getAgencia(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getNumpla() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getCliente() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getRuta() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getDespachador() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFechadsp() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getTipo_viaje() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_salida_dsp() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_pos_llegada() , texto );
                //PLACA
                xls.adicionarCelda(fila ,col++ ,rep.getPlaca() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFoto_placa() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_foto_placa() , fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getEmision_gas() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_emision_gas(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getCont_arriendo() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_cont_arriendo(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getCont_compra() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_cont_compra(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getPoliza_andina() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_poliza_andina(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getReg_nal_carga() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_reg_nal_carga(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getReg_nal_semire(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_reg_nal_semire(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getSoat(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_soat(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getTar_habi(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_tar_habi(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getTar_empresarial(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_tar_empresarial(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getTar_propiedad_cab() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_tar_propiedad_cab(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getTar_propiedad_semi() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_tar_propiedad_semi(), fecha );
                //Conductor
                xls.adicionarCelda(fila ,col++ ,rep.getNrocedula() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getConductor(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFoto_con() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_foto_con(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getArp() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_arp(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getCedula() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_cedula(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getCert_jud() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_cert_jud(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getEps() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_eps(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getFirma() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_firma(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getLib_mil() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_lib_mil(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getLib_tripulante() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_lib_tripulante(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getPasaporte() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_pasaporte(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getPase() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_pase(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getVisa() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_visa(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getPrimera_h() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_primera_h(), fecha );
                xls.adicionarCelda(fila ,col++ ,rep.getSegunda_h(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getFecha_segunda_h(), fecha );
                //DATOS DE PLAN VIAJE
                if(rep.getPlanviaje().equals("SI")){
                    xls.adicionarCelda(fila ,col++ ,model.reporteplacanitfotoService.tienePernoctacion((!rep.getFecha_salida().equals("0099-01-01 00:00:00")?rep.getFecha_salida():rep.getFecha_salida_dsp()),rep.getFecha_pos_llegada()), texto );
                }else{
                    xls.adicionarCelda(fila ,col++ ,model.reporteplacanitfotoService.tienePernoctacion(rep.getFecha_salida_dsp(),rep.getFecha_pos_llegada()), texto );
                }
                xls.adicionarCelda(fila ,col++ ,rep.getPernotacion(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getHreporte(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getPlanviaje(), texto );//indica si tiene plan de viaje
                xls.adicionarCelda(fila ,col++ ,rep.getPuesto1(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getTel1(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getPuesto2(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getTel2(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getPuesto3(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getTel3(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getCelular(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getNro_celular(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getTiempo(), numero );
                
                
            }
            
            
            xls.cerrarLibro();
            //System.out.println("Finaliza");
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("Error : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    
    public static void main(String a []){
        HReporteDocumentosHVXLS proc = new HReporteDocumentosHVXLS();
        proc.start("2006-12-25","2006-12-28","DBASTIDAS","%","FINV");
        
    }
    
}
