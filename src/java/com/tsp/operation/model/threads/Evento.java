/**
 * Nombre        Evento.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         10 de noviembre de 2006, 09:31 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.Usuario;

public abstract class Evento extends Thread {
    
    Usuario usuario;
    
    public Evento (){}
    
    
    public Evento(Usuario usuario){
        setUsuario (usuario);
    }
    
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public Usuario getUsuario() {
        return usuario;
    }    
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
}
