/*
 * HImportarDiscrepancia.java
 *
 * Created on 28 de marzo de 2007, 09:50 AM
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.services.DiscrepanciaService;
import com.tsp.operation.model.beans.Discrepancia;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.UtilFinanzas;


import java.io.*;
import java.util.*;
import java.text.*;

public class HImportarDiscrepancia extends Evento{
    
    
    FileWriter       fw  = null;
    PrintWriter      pw  = null;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    SimpleDateFormat fmt2= new SimpleDateFormat("yyyyMMdd_kkmmss");
    Date             fechaActual = new Date();
    
    DiscrepanciaService svc = null;
        
    
    
    /** Creates a new instance of HImportarDiscrepancia */
    public HImportarDiscrepancia() {
    }
    
    /** Crea una nueva instancia de  HImportacionCXP */
    public HImportarDiscrepancia(Usuario usuario) {
        super(usuario);
    }    

    
    public synchronized void run (){
        try {
            String ruta  = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + usuario.getLogin());
            fw = new FileWriter (ruta + "/LogMigDiscrepancia_"+ fmt2.format(fechaActual) +".txt");
            pw = new PrintWriter(new BufferedWriter(fw));            
            
            
            try{
                svc = new DiscrepanciaService();
                
                writeLog("*****************************************************");
                writeLog("* Inicio del Proceso de Importacion de Dicrepancias *");
                writeLog("*****************************************************");
                Procesar();
                writeLog("*****************************************************");
                writeLog("*                 Proceso Finalizado                *");
                writeLog("*****************************************************");
            }catch (Exception ex){
                ex.printStackTrace();
                writeLog("*****************************************************");
                writeLog("*      Finalizando Proceso pero con anomalias       *");
                writeLog("*****************************************************");
                writeLog(ex.getMessage());
            }
            
            
            pw.close();            
        } catch (Exception ex){
            ex.printStackTrace();            
        }
    }
    
    
    public void Procesar() throws Exception{
        try{
            Vector datos = svc.obtenerDiscrepanciasPendientes(getUsuario().getLogin());
            if (datos!=null && !datos.isEmpty()){
                for (int i=0; i<datos.size(); i++){
                    Discrepancia d = (Discrepancia) datos.get(i);
                    d.setBase(getUsuario().getBase());
                    d.setDescripcion      ( d.getObservacion() );
                    try {
                        svc.grabarDiscrepanciaImportacion(d);
                        writeLog( d.getNro_planilla() + " : DISCREPANCIA GRABADA. -> " + d.getNro_discrepancia()  );
                    } catch (Exception e ){
                        writeLog( d.getNro_planilla() + " : No pudo ser grabada.\n" + e.getMessage());
                    }
                }
            } else
                writeLog("NO HAY DATOS PARA IMPORTAR");
        } catch (Exception ex){
            throw new Exception ( ex.getMessage() );
        }
    }
    
    
    public void writeLog(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length;i++)
                    pw.println("[" + getCurrentTime() + "] " +  msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    public void writeLog2(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length ; i++)
                    pw.println( msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }    
    public String getCurrentTime(){
        fechaActual.setTime( System.currentTimeMillis());
        return fmt.format(fechaActual );
    }
    
    
}
