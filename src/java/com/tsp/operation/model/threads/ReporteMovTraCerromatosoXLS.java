/********************************************************************
 *  Nombre Clase.................   ReporteMovTraCerromatosoXLS.java
 *  Descripci�n..................   Hilo que genera el archivo .xls
 *                                  del reporte de movimientos de trafico
 *                                  del cliente Cerromatoso
 *  Autor........................   Ing. Leonardo Parodi
 *  Fecha........................   12.01.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.Model;
import java.util.*;
import java.text.*;
/**
 *
 * @author  EQUIPO12
 */
public class ReporteMovTraCerromatosoXLS extends Thread{
        private String user;
        private String id;
        private String des;
        private String procesoName;
        private Model model = new Model();
        private List Reportes;
        String Fecha;
        ReporteMovTraficoCerromatoso reporte = new ReporteMovTraficoCerromatoso();
        /** Creates a new instance of ReporteMovTraCerromatosoXLS */
        public ReporteMovTraCerromatosoXLS() {
        }
        
        public void start(String user, List reportes){
                ////System.out.println("Aqui toy en el start");
                this.Reportes = reportes;
                this.id = user;
                this.user = user;
                this.des = "Realiza un reporte de los movimientos de trafico del cliente Cerromatoso";
                this.procesoName = "Reporte de Movimientos de trafico del cliente Cerromatoso";
                super.start();
                
        }
        
        public synchronized void run(){
                
                try{
                        
                        model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
                        Calendar FechaHoy = Calendar.getInstance();
                        Date d = FechaHoy.getTime();
                        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
                        SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd_kkmm");
                        SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");
                        String FechaFormated = s.format(d);
                        String FechaFormated1 = s1.format(d);
                        Fecha = fec.format(d);
                        
                        
                        //obtener cabeera de ruta
                        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                        String path = rb.getString("ruta");
                        //armas la ruta
                        String Ruta1  = path + "/exportar/migracion/" + user + "/";
                        //crear la ruta
                        File file = new File(Ruta1);
                        file.mkdirs();
                        
                        POIWrite xls = new POIWrite(Ruta1 +"/ReporteMovTraficoCerromatoso_" + FechaFormated1 + ".xls", user, Fecha);
                        
                        HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                        HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
                        HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
                        
                        HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                        HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
                        HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
                        
                        /************************************
                         *      Estilos de Hojas 3 y 4      *
                         ************************************/
                        
                        HSSFCellStyle header2 = xls.nuevoEstilo("Arial", 12, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                        HSSFCellStyle header3 = xls.nuevoEstilo("Arial", 8, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                        HSSFCellStyle texto2  = xls.nuevoEstilo("Arial", 8, false , false, "text", xls.NONE , xls.NONE , xls.NONE);
                        HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 8, false , false, "\"$ \"#,##0.00", xls.NONE , xls.NONE , xls.NONE);
                        HSSFCellStyle headerfecha  = xls.nuevoEstilo("Arial", 9, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                        HSSFCellStyle celda = xls.nuevoEstilo("Arial", 9, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                        
                        celda.setBorderLeft(celda.BORDER_MEDIUM);
                        
                        header2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                        header3.setAlignment(header3.ALIGN_CENTER);
                        texto2.setAlignment(texto2.ALIGN_CENTER);
                        moneda.setAlignment(moneda.ALIGN_CENTER);
                        
                        texto2.setBorderBottom(texto2.BORDER_THIN);
                        texto2.setBorderTop(texto2.BORDER_THIN);
                        texto2.setBorderRight(texto2.BORDER_THIN);
                        texto2.setBorderLeft(texto2.BORDER_THIN);
                        
                        moneda.setBorderBottom(texto2.BORDER_THIN);
                        moneda.setBorderTop(texto2.BORDER_THIN);
                        moneda.setBorderRight(texto2.BORDER_THIN);
                        moneda.setBorderLeft(texto2.BORDER_THIN);
                        
                        header3.setBorderBottom(header3.BORDER_MEDIUM);
                        header3.setBorderTop(header3.BORDER_MEDIUM);
                        header3.setBorderLeft(header3.BORDER_MEDIUM);
                        header3.setBorderRight(header3.BORDER_MEDIUM);
                        
                        /****************************************
                         *    FOTOS DE PLACAS Y CONDUCTORES     *
                         ****************************************/
                        ////System.out.println("Tama�o de la lista placa = "+Reportes.size());
                        xls.obtenerHoja("Reporte");
                        xls.cambiarMagnificacion(4,4);
                        
                        // cabecera
                        
                        xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
                        xls.combinarCeldas(0, 0, 0, 2);
                        xls.adicionarCelda(1,0, "MOVIMIENTOS TRAFICO CERROMATOSO", header2);
                        xls.combinarCeldas(1, 0, 0, 2);
                        xls.adicionarCelda(3,0, "Fecha y hora Proceso :"   , headerfecha);
                        xls.adicionarCelda(3,1, FechaFormated1, fecha);
                        
                        // subtitulos
                        
                        
                        int fila = 6;
                        int col  = 0;
                        
                        xls.adicionarCelda(fila ,col++ , "Creation_date"     , header3 );
                        xls.adicionarCelda(fila ,col++ , "Numpla"     , header3 );
                        xls.adicionarCelda(fila ,col++ , "Numrem"     , header3 );
                        xls.adicionarCelda(fila ,col++ , "Nom_tipo_procedencia"      , header3 );
                        xls.adicionarCelda(fila ,col++ , "Nom_tipo_reporte"     , header3 );
                        xls.adicionarCelda(fila ,col++ , "Nom_ubicacion"      , header3 );
                        xls.adicionarCelda(fila ,col++ , "Nom_zona"     , header3 );
                        xls.adicionarCelda(fila ,col++ , "Cliente"     , header3 );
                        
                        // datos
                        
                        int it = 0;
                        while(it < Reportes.size()){
                                fila++;
                                col = 0;
                                reporte = (ReporteMovTraficoCerromatoso)Reportes.get(it);
                                if (reporte.getCreation_date()!= null) {
                                        String fecha1 = reporte.getCreation_date().toString();
                                        xls.adicionarCelda(fila ,col++ , fecha1.substring(0,4)+"/"+fecha1.substring(5,7)+"/"+fecha1.substring(8,10), texto2 );
                                }else{
                                        xls.adicionarCelda(fila ,col++ , "", texto2 );
                                }
                                xls.adicionarCelda(fila ,col++ , reporte.getNumpla(), texto2);
                                xls.adicionarCelda(fila ,col++ , reporte.getNumrem(), texto2 );
                                xls.adicionarCelda(fila ,col++ , reporte.getNom_tipo_procedencia(), texto2 );
                                xls.adicionarCelda(fila ,col++ , reporte.getNom_tipo_reporte(), texto2 );
                                xls.adicionarCelda(fila ,col++ , reporte.getNom_ubicacion(), texto2 );
                                xls.adicionarCelda(fila ,col++ , reporte.getNom_zona(), texto2 );
                                xls.adicionarCelda(fila ,col++ , reporte.getNom_cliente(), texto2 );
                                
                                it++;
                                
                        }
                        
                        
                        xls.cerrarLibro();
                        model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "PROCESO EXITOSO");
                        
                }catch (Exception ex){
                        try{
                                Model model = new Model();
                                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + ex.getMessage());
                        }
                        catch(Exception f){
                                try{
                                        Model model = new Model();
                                        model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                                }catch(Exception p){    }
                        }
                }
                
        }
        
}
