/********************************************************************
 *  Nombre Clase.................   ReporteRetroactivosTh.java
 *  Descripci�n..................   Hilo que genera el archivo .xls
 *                                  del reporte de retroactivos
 *  Autor........................   Ing. Tito Andr�s Maturana
 *                                  Ing. Leonardo Parodi
 *  Fecha........................   14.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;

/**
 *
 * @author  EQUIPO12
 */
public class ReporteRetroactivosTh extends Thread{
    String FechaI;
    String FechaF;
    Vector Unicos;
    Vector Repetidos;
    Vector Intermediarios;
    Vector Directos;
    String user;
    String Fecha;
    ReporteRetroactivo datos = new ReporteRetroactivo();
    
    public void start(String FechaI,String FechaF ,String user , Vector unicos, Vector repetidos, Vector intermediarios, Vector directos){
        ////System.out.println("Aqui toy en el start");
        this.FechaI = FechaI;
        this.FechaF = FechaF;
        this.Unicos = unicos;
        this.Repetidos = repetidos;
        this.Intermediarios = intermediarios;
        this.Directos = directos;
        
        this.user = user;
        this.Fecha = Fecha;
        super.start();
    }
    
    public synchronized void run(){
        
        try{
            
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            Fecha = fec.format(d);
            
            
            //obtener cabeera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteRetroactivos_" + FechaFormated1 + ".xls", user, Fecha);
            
            HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            
            /************************************
             *      Estilos de Hojas 3 y 4      *
             ************************************/
            
            HSSFCellStyle header2 = xls.nuevoEstilo("Arial", 12, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header3 = xls.nuevoEstilo("Arial", 8, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2  = xls.nuevoEstilo("Arial", 8, false , false, "text", xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 8, false , false, "\"$ \"#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle headerfecha  = xls.nuevoEstilo("Arial", 9, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle celda = xls.nuevoEstilo("Arial", 9, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            //"$ "#,##0
            celda.setBorderLeft(celda.BORDER_MEDIUM);
            
            header2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            header3.setAlignment(header3.ALIGN_CENTER);
            texto2.setAlignment(texto2.ALIGN_CENTER);
            moneda.setAlignment(moneda.ALIGN_CENTER);
            
            texto2.setBorderBottom(texto2.BORDER_THIN);
            texto2.setBorderTop(texto2.BORDER_THIN);
            texto2.setBorderRight(texto2.BORDER_THIN);
            texto2.setBorderLeft(texto2.BORDER_THIN);
            
            moneda.setBorderBottom(texto2.BORDER_THIN);
            moneda.setBorderTop(texto2.BORDER_THIN);
            moneda.setBorderRight(texto2.BORDER_THIN);
            moneda.setBorderLeft(texto2.BORDER_THIN);
            
            header3.setBorderBottom(header3.BORDER_MEDIUM);
            header3.setBorderTop(header3.BORDER_MEDIUM);
            header3.setBorderLeft(header3.BORDER_MEDIUM);
            header3.setBorderRight(header3.BORDER_MEDIUM);
            
            /****************************************
             *          UNICOS Y REPETIDOS          *
             ****************************************/
            
            xls.obtenerHoja("UNICOS");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.combinarCeldas(0, 0, 0, 2);
            xls.adicionarCelda(1,0, "PRODUCCION VIAJES CON UN SOLO CODIGO", header2);
            xls.combinarCeldas(1, 0, 0, 2);
            xls.adicionarCelda(2,0, "Fecha Inicial :"   , headerfecha);
            xls.adicionarCelda(2,1, FechaI , fecha);
            xls.adicionarCelda(2,2, "Fecha Final :"   , headerfecha);
            xls.adicionarCelda(2,3, FechaF, fecha);
            xls.adicionarCelda(3,0, "Fecha y hora Proceso :"   , headerfecha);
            xls.adicionarCelda(3,1, FechaFormated1, fecha);
            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila ,col++ , "NitProp"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Propietario"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Placa"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Planilla"     , header3 );
            xls.adicionarCelda(fila ,col++ , "FecPla"      , header3 );
            xls.adicionarCelda(fila ,col++ , "Toneladas"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Ruta"      , header3 );
            xls.adicionarCelda(fila ,col++ , "Tipo"  , header3 );
            
            // datos
            
            int it = 0;
            while(it < Unicos.size()){
                fila++;
                col = 0;
                datos = (ReporteRetroactivo)Unicos.get(it);
                xls.adicionarCelda(fila ,col++ , datos.getNit() , texto2 );
                xls.adicionarCelda(fila ,col++ , datos.getPropietario(), texto2);
                xls.adicionarCelda(fila ,col++ , datos.getPlaca(), texto2 );
                xls.adicionarCelda(fila ,col++ , datos.getNumpla(), texto2 );
                String fecha1 = datos.getFecPla().toString();
                xls.adicionarCelda(fila ,col++ , fecha1.substring(0,4)+"/"+fecha1.substring(5,7)+"/"+fecha1.substring(8,10), texto2 );
                xls.adicionarCelda(fila ,col++ , datos.getToneladas(), texto2 );
                xls.adicionarCelda(fila ,col++ , datos.getRuta(), texto2 );
                xls.adicionarCelda(fila ,col++ , datos.getTipo(), texto2 );
                
                it++;
                
            }
            
            xls.obtenerHoja("REPETIDOS");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.combinarCeldas(0, 0, 0, 2);
            xls.adicionarCelda(1,0, "PRODUCCION DE PLACAS CON MAS DE DOS CODIGOS.", header2);
            xls.combinarCeldas(1, 0, 0, 2);
            xls.adicionarCelda(2,0, "Fecha Inicial :"   , headerfecha);
            xls.adicionarCelda(2,1, FechaI , fecha);
            xls.adicionarCelda(2,2, "Fecha Final :"   , headerfecha);
            xls.adicionarCelda(2,3, FechaF, fecha);
            xls.adicionarCelda(3,0, "Fecha y hora Proceso :"   , headerfecha);
            xls.adicionarCelda(3,1, FechaFormated1, fecha);
            
            // subtitulos
            
            
            fila = 6;
            col  = 0;
            
            xls.adicionarCelda(fila ,col++ , "NitProp"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Propietario"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Placa"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Planilla"     , header3 );
            xls.adicionarCelda(fila ,col++ , "FecPla"      , header3 );
            xls.adicionarCelda(fila ,col++ , "Toneladas"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Ruta"      , header3 );
            xls.adicionarCelda(fila ,col++ , "Tipo"  , header3 );
            
            // datos
            
            it = 0;
            while(it < Repetidos.size()){
                fila++;
                col = 0;
                datos = (ReporteRetroactivo)Repetidos.get(it);
                xls.adicionarCelda(fila ,col++ , datos.getNit() , texto2 );
                xls.adicionarCelda(fila ,col++ , datos.getPropietario(), texto2);
                xls.adicionarCelda(fila ,col++ , datos.getPlaca(), texto2 );
                xls.adicionarCelda(fila ,col++ , datos.getNumpla(), texto2 );
                String fech = datos.getFecPla().toString();
                xls.adicionarCelda(fila ,col++ , fech.substring(0,4)+"/"+fech.substring(5,7)+"/"+fech.substring(8,10), texto2 );
                xls.adicionarCelda(fila ,col++ , datos.getToneladas(), texto2 );
                xls.adicionarCelda(fila ,col++ , datos.getRuta(), texto2 );
                xls.adicionarCelda(fila ,col++ , datos.getTipo(), texto2 );
                
                it++;
                
            }
            
             /************************************************
             *          CLASIFICADOS INTERMEDIARIOS          *
             *************************************************/
            Hashtable ht;
            Vector a = new Vector();
            Vector b = new Vector();
            
            
            ht = new Hashtable();
            ht = (Hashtable) this.Directos.elementAt(0);
            
            String retro_jagua = ht.get("retro_jagua").toString();
            String retro_loma = ht.get("retro_loma").toString();
            String retro_carbosan = ht.get("retro_carbosan").toString();
            
            xls.obtenerHoja("INTERMEDIARIOS");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.combinarCeldas(0, 0, 0, 14);
            
            xls.adicionarCelda(2, 0, "Reporte de fecha:     " + FechaI + "      Hasta:     " + FechaF, headerfecha);
            xls.combinarCeldas(2, 0, 2, 14);
            
            xls.adicionarCelda(4,0, "CLASIFICADOS", header2);
            xls.combinarCeldas(4, 0, 4, 14);
            
            
            fila = 8;
            col  = 0;
            
            for( int i=1; i<this.Intermediarios.size(); i++){
                ht = new Hashtable();
                ht = (Hashtable) this.Intermediarios.elementAt(i);
                
                if( Integer.valueOf(ht.get("ttl_vjs").toString()).intValue() >= 14 ){
                    a.add(ht);
                } else {
                    b.add(ht);
                }
            }
            
            if( a.size()>0 ){
                double total = 0;
                
                xls.adicionarCelda(fila, 3, "LOMA", header3);
                xls.combinarCeldas(fila, 3, fila, 5);
                xls.adicionarCelda(fila, 6, "CARBOSAN", header3);
                xls.combinarCeldas(fila, 6, fila, 8);
                xls.adicionarCelda(fila, 9, "JAGUA", header3);
                xls.combinarCeldas(fila, 9, fila, 11);
                xls.adicionarCelda(fila, 12, "TOTALES", header3);
                xls.combinarCeldas(fila, 12, fila, 14);
                xls.adicionarCelda(fila, 15, "", celda);
                
                fila++;
                
                xls.adicionarCelda(fila, col++, "Placa", header3);
                xls.adicionarCelda(fila, col++, "Propietario", header3);
                xls.adicionarCelda(fila, col++, "Nit", header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_loma, header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_carbosan, header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_jagua, header3);
                xls.adicionarCelda(fila, col++, "Total Vjs", header3);
                xls.adicionarCelda(fila, col++, "Total Tons", header3);
                xls.adicionarCelda(fila, col++, "Total $", header3);
                
                for( int i=0; i<a.size(); i++){
                    fila++;
                    
                    ht = new Hashtable();
                    ht = (Hashtable) a.elementAt(i);
                    
                    String ln_ttl = ht.get("total").toString();
                    total += Double.valueOf(ln_ttl).doubleValue(); 
                    
                    col = 0;
                    xls.adicionarCelda(fila, col++, ht.get("placa").toString(), texto2);
                    xls.adicionarCelda(fila, col++, ht.get("propietario").toString(), texto2);
                    xls.adicionarCelda(fila, col++, ht.get("nit").toString(), texto2);
                    xls.adicionarCelda(fila, col++,(ht.get("viajes_loma")!=null) ? ht.get("viajes_loma").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_loma")!=null) ? ht.get("tons_loma").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_loma")!=null) ? Double.valueOf(ht.get("ttl_loma").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("viajes_carbosan")!=null) ? ht.get("viajes_carbosan").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_carbosan")!=null) ? ht.get("tons_carbosan").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_carbosan")!=null) ? Double.valueOf(ht.get("ttl_carbosan").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("viajes_jagua")!=null) ? ht.get("viajes_jagua").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_jagua")!=null) ? ht.get("tons_jagua").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_jagua")!=null) ? Double.valueOf(ht.get("ttl_jagua").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_vjs")!=null) ? ht.get("ttl_vjs").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_tons")!=null) ? ht.get("ttl_tons").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, Double.valueOf(ln_ttl).doubleValue(), moneda);
                    //xls.adicionarCelda(fila, col++, .toString(), texto);
                                     
                }
                
                xls.adicionarCelda(fila+=2, 14, Double.valueOf(String.valueOf(com.tsp.util.Util.redondear(total,2))).doubleValue(), moneda);                   
                fila += 3;
                col  = 0;
            }
            
            if( b.size()>0 ){
                
                double total = 0;
                
                xls.adicionarCelda(fila, 3, "LOMA", header3);
                xls.combinarCeldas(fila, 3, fila, 5);
                xls.adicionarCelda(fila, 6, "CARBOSAN", header3);
                xls.combinarCeldas(fila, 6, fila, 8);
                xls.adicionarCelda(fila, 9, "JAGUA", header3);
                xls.combinarCeldas(fila, 9, fila, 11);
                xls.adicionarCelda(fila, 12, "TOTALES", header3);
                xls.combinarCeldas(fila, 12, fila, 14);
                xls.adicionarCelda(fila, 15, "", celda);
                
                fila ++;
                
                xls.adicionarCelda(fila, col++, "Placa", header3);
                xls.adicionarCelda(fila, col++, "Propietario", header3);
                xls.adicionarCelda(fila, col++, "Nit", header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_loma, header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_carbosan, header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_jagua, header3);
                xls.adicionarCelda(fila, col++, "Total Vjs", header3);
                xls.adicionarCelda(fila, col++, "Total Tons", header3);
                xls.adicionarCelda(fila, col++, "Total $", header3);
                
                for( int i=0; i<b.size(); i++){
                    
                    fila++;
                    
                    ht = new Hashtable();
                    ht = (Hashtable) b.elementAt(i);
                    
                    String ln_ttl = ht.get("total").toString();
                    total += Double.valueOf(ln_ttl).doubleValue(); 
                    
                    col = 0;
                    xls.adicionarCelda(fila, col++, ht.get("placa").toString(), texto2);
                    xls.adicionarCelda(fila, col++, ht.get("propietario").toString(), texto2);
                    xls.adicionarCelda(fila, col++, ht.get("nit").toString(), texto2);
                    xls.adicionarCelda(fila, col++,(ht.get("viajes_loma")!=null) ? ht.get("viajes_loma").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_loma")!=null) ? ht.get("tons_loma").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_loma")!=null) ? Double.valueOf(ht.get("ttl_loma").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("viajes_carbosan")!=null) ? ht.get("viajes_carbosan").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_carbosan")!=null) ? ht.get("tons_carbosan").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_carbosan")!=null) ? Double.valueOf(ht.get("ttl_carbosan").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("viajes_jagua")!=null) ? ht.get("viajes_jagua").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_jagua")!=null) ? ht.get("tons_jagua").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_jagua")!=null) ? Double.valueOf(ht.get("ttl_jagua").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_vjs")!=null) ? ht.get("ttl_vjs").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_tons")!=null) ? ht.get("ttl_tons").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, Double.valueOf(ln_ttl).doubleValue(), moneda);    
                    /*////System.out.println("............................Placa: " + ht.get("placa").toString() 
                            + ", total: " + ln_ttl);*/
                       
                }
                
                xls.adicionarCelda(fila+=2, 14, Double.valueOf(String.valueOf(com.tsp.util.Util.redondear(total,2))).doubleValue(), moneda);  
                //////System.out.println("....................TOTAL INTERMEDIARIOS: " + com.tsp.util.Util.redondear(total,2));
            }
            
            /*******************************************
             *          CLASIFICADOS DIRECTOS          *
             *******************************************/
            a = new Vector();
            b = new Vector();
            
            
            ht = new Hashtable();
            ht = (Hashtable) this.Directos.elementAt(0);
            
            retro_jagua = ht.get("retro_jagua").toString();
            retro_loma = ht.get("retro_loma").toString();
            retro_carbosan = ht.get("retro_carbosan").toString();
            
            xls.obtenerHoja("DIRECTOS");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.combinarCeldas(0, 0, 0, 14);
            
            xls.adicionarCelda(2, 0, "Reporte de fecha:     " + FechaI + "      Hasta:     " + FechaF, headerfecha);
            xls.combinarCeldas(2, 0, 2, 14);
            
            xls.adicionarCelda(4,0, "CLASIFICADOS POR EXTRACTO", header2);
            xls.combinarCeldas(4, 0, 4, 14);
            
            
            fila = 8;
            col  = 0;
            
            for( int i=1; i<this.Directos.size(); i++){
                ht = new Hashtable();
                ht = (Hashtable) this.Directos.elementAt(i);
                
                if( Integer.valueOf(ht.get("ttl_vjs").toString()).intValue() >= 14 ){
                    a.add(ht);
                } else {
                    b.add(ht);
                }
            }
            
            if( a.size()>0 ){
                double total = 0;
                
                xls.adicionarCelda(fila, 3, "LOMA", header3);
                xls.combinarCeldas(fila, 3, fila, 5);
                xls.adicionarCelda(fila, 6, "CARBOSAN", header3);
                xls.combinarCeldas(fila, 6, fila, 8);
                xls.adicionarCelda(fila, 9, "JAGUA", header3);
                xls.combinarCeldas(fila, 9, fila, 11);
                xls.adicionarCelda(fila, 12, "TOTALES", header3);
                xls.combinarCeldas(fila, 12, fila, 14);
                xls.adicionarCelda(fila, 15, "", celda);
                
                fila++;
                
                xls.adicionarCelda(fila, col++, "Placa", header3);
                xls.adicionarCelda(fila, col++, "Propietario", header3);
                xls.adicionarCelda(fila, col++, "Nit", header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_loma, header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_carbosan, header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_jagua, header3);
                xls.adicionarCelda(fila, col++, "Total Vjs", header3);
                xls.adicionarCelda(fila, col++, "Total Tons", header3);
                xls.adicionarCelda(fila, col++, "Total $", header3);
                
                for( int i=0; i<a.size(); i++){
                    fila++;
                    
                    ht = new Hashtable();
                    ht = (Hashtable) a.elementAt(i);
                    
                    String ln_ttl = ht.get("total").toString();
                    total += Double.valueOf(ln_ttl).doubleValue(); 
                    
                    col = 0;
                    xls.adicionarCelda(fila, col++, ht.get("placa").toString(), texto2);
                    xls.adicionarCelda(fila, col++, ht.get("propietario").toString(), texto2);
                    xls.adicionarCelda(fila, col++, ht.get("nit").toString(), texto2);
                    xls.adicionarCelda(fila, col++,(ht.get("viajes_loma")!=null) ? ht.get("viajes_loma").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_loma")!=null) ? ht.get("tons_loma").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_loma")!=null) ? Double.valueOf(ht.get("ttl_loma").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("viajes_carbosan")!=null) ? ht.get("viajes_carbosan").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_carbosan")!=null) ? ht.get("tons_carbosan").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_carbosan")!=null) ? Double.valueOf(ht.get("ttl_carbosan").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("viajes_jagua")!=null) ? ht.get("viajes_jagua").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_jagua")!=null) ? ht.get("tons_jagua").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_jagua")!=null) ? Double.valueOf(ht.get("ttl_jagua").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_vjs")!=null) ? ht.get("ttl_vjs").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_tons")!=null) ? ht.get("ttl_tons").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, Double.valueOf(ln_ttl).doubleValue(), moneda);               
                    //xls.adicionarCelda(fila, col++, .toString(), texto);
                                     
                }
                
                xls.adicionarCelda(fila+=2, 14, Double.valueOf(String.valueOf(com.tsp.util.Util.redondear(total,2))).doubleValue(), moneda);                    
                fila += 3;
                col  = 0;
            }
            
            if( b.size()>0 ){
                
                double total = 0;
                
                xls.adicionarCelda(fila, 3, "LOMA", header3);
                xls.combinarCeldas(fila, 3, fila, 5);
                xls.adicionarCelda(fila, 6, "CARBOSAN", header3);
                xls.combinarCeldas(fila, 6, fila, 8);
                xls.adicionarCelda(fila, 9, "JAGUA", header3);
                xls.combinarCeldas(fila, 9, fila, 11);
                xls.adicionarCelda(fila, 12, "TOTALES", header3);
                xls.combinarCeldas(fila, 12, fila, 14);
                xls.adicionarCelda(fila, 15, "", celda);
                
                fila ++;
                
                xls.adicionarCelda(fila, col++, "Placa", header3);
                xls.adicionarCelda(fila, col++, "Propietario", header3);
                xls.adicionarCelda(fila, col++, "Nit", header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_loma, header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_carbosan, header3);
                xls.adicionarCelda(fila, col++, "Vjs", header3);
                xls.adicionarCelda(fila, col++, "Tons", header3);
                xls.adicionarCelda(fila, col++, "$ " + retro_jagua, header3);
                xls.adicionarCelda(fila, col++, "Total Vjs", header3);
                xls.adicionarCelda(fila, col++, "Total Tons", header3);
                xls.adicionarCelda(fila, col++, "Total $", header3);
                
                for( int i=0; i<b.size(); i++){
                    
                    fila++;
                    
                    ht = new Hashtable();
                    ht = (Hashtable) b.elementAt(i);
                    
                    String ln_ttl = ht.get("total").toString();
                    total += Double.valueOf(ln_ttl).doubleValue(); 
                    
                    col = 0;
                    xls.adicionarCelda(fila, col++, ht.get("placa").toString(), texto2);
                    xls.adicionarCelda(fila, col++, ht.get("propietario").toString(), texto2);
                    xls.adicionarCelda(fila, col++, ht.get("nit").toString(), texto2);
                    xls.adicionarCelda(fila, col++,(ht.get("viajes_loma")!=null) ? ht.get("viajes_loma").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_loma")!=null) ? ht.get("tons_loma").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_loma")!=null) ? Double.valueOf(ht.get("ttl_loma").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("viajes_carbosan")!=null) ? ht.get("viajes_carbosan").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_carbosan")!=null) ? ht.get("tons_carbosan").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_carbosan")!=null) ? Double.valueOf(ht.get("ttl_carbosan").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("viajes_jagua")!=null) ? ht.get("viajes_jagua").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("tons_jagua")!=null) ? ht.get("tons_jagua").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_jagua")!=null) ? Double.valueOf(ht.get("ttl_jagua").toString()).doubleValue() : 0, moneda);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_vjs")!=null) ? ht.get("ttl_vjs").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, (ht.get("ttl_tons")!=null) ? ht.get("ttl_tons").toString() : "0", texto2);
                    xls.adicionarCelda(fila, col++, Double.valueOf(ln_ttl).doubleValue(), moneda);          
                       
                }
                
                xls.adicionarCelda(fila+=2, 14, Double.valueOf(String.valueOf(com.tsp.util.Util.redondear(total,2))).doubleValue(), moneda);
            }
            
            
            
            
            xls.cerrarLibro();
            
        }catch (Exception ex){
            ////System.out.println("Error : " + ex.getMessage());
            ex.printStackTrace();
        }
        
    }
    
}
