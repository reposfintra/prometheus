/*
 * ReporteFacturasCreadasXLS.java
 *
 * Created on 03 de diciembre de 2005, 12:15 PM
 */

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author  Henry
 */
public class ReporteRemesasPlanillasXLS extends Thread{
    private String procesoName;
    private String des;
    String id = "";    
    Vector remDocs = null;
    
    /** Creates a new instance of ReporteFacturasCreadasXLS */
    public ReporteRemesasPlanillasXLS() {
    }
    
    public void start(String id, Vector remDocs){
        ////System.out.println("entro por hilo");
        this.id = id;        
        this.procesoName = "Remesas planilla";
        this.des = "Las remesas q estan creadas en el sistema";
        this.remDocs = remDocs;
        super.start();
    }
    
    public synchronized void run(){
        try{
            Model model = new Model();
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);                        
            String fecha_actual = Util.getFechaActual_String(6);
            if (remDocs!=null) {
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                String a�o = fecha_actual.substring(0,4);
                String mes = fecha_actual.substring(5,7);
                String dia = fecha_actual.substring(8,10);
                String hora = fecha_actual.substring(11,13);
                String min = fecha_actual.substring(14,16);
                String seg = fecha_actual.substring(17,19);
                File file = new File(path);
                file.mkdirs();
                
                com.tsp.operation.model.threads.POIWrite xls = new com.tsp.operation.model.threads.POIWrite(path+"/REMESA_PLANILLA"+a�o+mes+dia+" "+hora+min+seg+".xls","","");
                // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
                HSSFCellStyle fecha       = xls.nuevoEstilo("verdana", 12, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                HSSFCellStyle texto       = xls.nuevoEstilo("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero      = xls.nuevoEstilo("verdana", 12, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle negrita     = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE);
                HSSFCellStyle header      = xls.nuevoEstilo("verdana", 18, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle fechatitle  = xls.nuevoEstilo("verdana", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );                
                
                xls.obtenerHoja("BASE");
                xls.cambiarMagnificacion(3,4);                                
                // cabecera
                
                xls.adicionarCelda(0,0, "REMESAS PLANILLA", header);
                xls.combinarCeldas(0, 0, 0, 15);
                
                // subtitulos

                xls.adicionarCelda(3,0, "Fecha Proceso: "   , negrita);
                xls.adicionarCelda(3,1, fecha_actual , fecha);
                xls.adicionarCelda(4,0, "Elaborado Por: "   , negrita);                
                xls.adicionarCelda(4,1, id , texto);                                
                
                int fila = 6;
                int col  = 0;
                
                xls.adicionarCelda(fila ,col++ , "Estado"                         , titulo );
                xls.adicionarCelda(fila ,col++ , "Planilla"                              , titulo );
                xls.adicionarCelda(fila ,col++ , "Remesa"                                  , titulo );
                xls.adicionarCelda(fila ,col++ , "Standar Job"                         , titulo );
                xls.adicionarCelda(fila ,col++ , "Cant. a facturar"                           , titulo );
                xls.adicionarCelda(fila ,col++ , "Porcentaje"                     , titulo );
                xls.adicionarCelda(fila ,col++ , "Docs. Internos"                       , titulo );
                xls.adicionarCelda(fila ,col++ , "Facts. Comerciales"               , titulo );                
                
                // datos
                for (int j=0; j<8; j++) {
                     xls.cambiarAnchoColumna(j,5600);
                }                
                for (int i=0; i<remDocs.size(); i++) {
                    fila++;
                    col = 0;                    
                    Remesa r = (Remesa) remDocs.elementAt(i); 
                   
                    xls.adicionarCelda(fila ,col++ , r.getReg_status()            , texto );
                    xls.adicionarCelda(fila ,col++ , r.getNumpla()                , texto );
                    xls.adicionarCelda(fila ,col++ , r.getNumrem()                , texto );
                    xls.adicionarCelda(fila ,col++ , r.getStdJobNo()              , texto );
                    xls.adicionarCelda(fila ,col++ , r.getPesoReal()              , numero );
                    xls.adicionarCelda(fila ,col++ , r.getPorcentaje()            , numero );
                    xls.adicionarCelda(fila ,col++ , r.getDocInterno()            , texto );
                    xls.adicionarCelda(fila ,col++ , r.getFaccial()               , texto );
                }
                
                xls.cerrarLibro();
            }
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                Model model = new Model();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                try{
                    Model model = new Model();
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
   /* public static void main (String arg[]) {
        try {
            ReporteRemesasPlanillasXLS reporte = new ReporteRemesasPlanillasXLS();
            remesa_doctoDAO rem = new remesa_doctoDAO();
            Vector planillas = new Vector();
            planillas.addElement("PL443");
            planillas.addElement("PL444");
            planillas.addElement("PL445");
            planillas.addElement("PL446");        
            planillas.addElement("PL447");   
            rem.searchRemesaDoc(planillas);
            reporte.start("kreales", rem.getVectorReporteRemPla());
            ////System.out.println("PROCESSO EXITOSO");
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }*/
}
