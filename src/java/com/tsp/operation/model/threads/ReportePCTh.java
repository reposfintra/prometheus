/********************************************************************
 *      Nombre Clase.................   ReportePCTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   03.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;

//Logger
import org.apache.log4j.*;
import com.tsp.operation.model.beans.POIWrite;


public class ReportePCTh extends Thread{
    
    Logger logger = Logger.getLogger (ReportePCTh.class);
    
    private Vector reporte;
    private Usuario user;    
    private String fechai;
    private String fechaf;
    
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Usuario user, String fecha_ini, String fecha_fin){
        this.reporte = reporte;
        this.user = user;
        this.fechaf = fecha_fin;
        this.fechai = fecha_ini;
        
        this.model = modelo;
        
        this.procesoName = "Reporte de Puestos de Control";
        this.des = "Reporte de Puestos de Control:  " + fechai + " - " + fechaf;
                
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user.getLogin());
            
            model.rmtService.reportePuestosControl(fechai, fechaf);
            this.reporte = model.rmtService.getReportesPlanilla();
            
            logger.info("RESULTADOS ENCONTRADOS: " + this.reporte.size());
        
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user.getLogin() + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReportePC_" + FechaFormated + ".xls");
            String nom_archivo = "ReportePC_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd hh:mm"  , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            //\"$ \"
                                                
            xls.obtenerHoja("Reporte de Puestos de Control");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            int nceldas = 17;
            
            xls.combinarCeldas(0, 0, 0, nceldas);
            xls.combinarCeldas(3, 0, 3, nceldas);
            xls.combinarCeldas(4, 0, 4, nceldas);
            xls.adicionarCelda(0, 0, "REPORTE DE PUESTOS DE CONTROL", header2);
            xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
                            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila, col++, "PLANILLA", header1);
            xls.adicionarCelda(fila, col++, "PLACA", header1);
            xls.adicionarCelda(fila, col++, "CED. CONDUCTOR", header1);
            xls.adicionarCelda(fila, col++, "NOMBRE CONDUCTOR", header1);
            xls.adicionarCelda(fila, col++, "ORIGEN", header1);
            xls.adicionarCelda(fila, col++, "DESTINO", header1);
            xls.adicionarCelda(fila, col++, "FECHA REPORTE", header1);
            xls.adicionarCelda(fila, col++, "PROCEDENCIA", header1);
            xls.adicionarCelda(fila, col++, "TIPO PROCEDENCIA", header1);
            xls.adicionarCelda(fila, col++, "TIPO REPORTE", header1);
            xls.adicionarCelda(fila, col++, "FECHA CREACION", header1);
            xls.adicionarCelda(fila, col++, "CREADO POR", header1);
            xls.adicionarCelda(fila, col++, "ZONA", header1);
            xls.adicionarCelda(fila, col++, "FECHA PROX. REPORTE", header1);
            xls.adicionarCelda(fila, col++, "CODIGO CAUSA", header1);
            xls.adicionarCelda(fila, col++, "CAUSA", header1);
            xls.adicionarCelda(fila, col++, "CLASIFICACION", header1);
            xls.adicionarCelda(fila, col++, "OBSERVACION", header1);
            //xls.adicionarCelda(fila, col++, "", header1);
            
            fila++;
            
            for( int i=0; i<reporte.size(); i++){
                col = 0;
                
                Hashtable ht = (Hashtable) reporte.elementAt(i);
                
                xls.adicionarCelda(fila, col++, ht.get("numpla").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("plaveh").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("cedcond").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("nomcond").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("oripla").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("despla").toString(), texto);
                                
                java.util.Date date = com.tsp.util.Util.ConvertiraDate0(ht.get("fecharep").toString()+":00");
                //xls.adicionarCelda(fila, col++, ht.get("fecharep").toString(), texto);
                xls.adicionarCelda(fila, col++, date, fecha); 
                                
                xls.adicionarCelda(fila, col++, ht.get("pc").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("tproc").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("treporte").toString(), texto);
                
                date = com.tsp.util.Util.ConvertiraDate0(ht.get("cdate").toString()+":00");
                //xls.adicionarCelda(fila, col++, ht.get("cdate").toString(), texto);
                xls.adicionarCelda(fila, col++, date, fecha);                 
                
                xls.adicionarCelda(fila, col++, ht.get("login").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("zona").toString(), texto);
                
                date = com.tsp.util.Util.ConvertiraDate0(ht.get("fechapla").toString()+":00");
                //xls.adicionarCelda(fila, col++, ht.get("fechapla").toString(), texto);
                xls.adicionarCelda(fila, col++, date, fecha); 
                                
                xls.adicionarCelda(fila, col++, ht.get("causa").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("causades").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("clasif").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("observacion").toString(), texto);
                                
                //date = com.tsp.util.Util.ConvertiraDate0(ht.get("fecha").toString()+":00");
                //xls.adicionarCelda(fila, col++, ht.get("fecha_creacion").toString(), texto);
                //xls.adicionarCelda(fila, col++, date, fecha); 
                
                fila++;
            }         
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "PROCESO EXITOSO");
            
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}
