/*
 * ReporteProduccionEquiposThreads.java
 *
 * Created on 20 de abril de 2007, 04:23 PM
 */

package com.tsp.operation.model.threads;

import com.aspose.cells.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;

/**
 *
 * @author  EQUIPO26
 */
public class ReporteProduccionEquiposThreads extends Thread{
    
    String user;
    String FechaI1;
    String FechaF1;
    String procesoName;
    String des;
    //String dstrct;
    Model model = new Model();
    
    /** Creates a new instance of ReporteProduccionEquiposThreads */
    public void start(String FechaI, String FechaF, String usuario){
        this.user = usuario;
        this.procesoName = "Reporte Producción Equipos";
        this.des = "Reporte Producción de Equipos";
        this.FechaI1 = FechaI;
        this.FechaF1 = FechaF;
        //this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
            Vector vector =  model.reporteGeneralService.reporteProduccionEquipo(FechaI1, FechaF1);
           
            Date fecha = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String now = format.format(fecha);
            String fechaCrea = now.substring(0,10);

            Workbook libro = new Workbook();

            Worksheets hojas = libro.getWorksheets();

            Worksheet hoja = hojas.getSheet(0);

            hoja.setName("HOJA1");
            
            //fuente de los datos
            Font fuente = new Font();
            fuente.setBold(true);
            fuente.setName("arial");
            fuente.setColor( com.aspose.cells.Color.WHITE );  
            Cells celdas = hoja.getCells();
            
            //datos de los titulos
            Font fuenteTitulo = new Font();
            fuenteTitulo.setBold(true);
            fuenteTitulo.setName("arial");
            fuenteTitulo.setColor( com.aspose.cells.Color.BLACK );
            
            
            //CELDA TITULO
            Cell celda = hoja.getCell(0,0);
            Style estiloEmpresa = libro.createStyle();
            estiloEmpresa.setColor( com.aspose.cells.Color.WHITE );
            celda.setValue("TRANSPORTES SANCHEZ POLO ");
            celda.setStyle(estiloEmpresa);
            estiloEmpresa.setFont ( fuenteTitulo ); 
            estiloEmpresa.getBorderColor(2);
            celdas.merge(0, 0, 0, 2);
            //hoja.autoFitColumns();
            
            
           
            //CELDA FECHA INICIO
            celda = hoja.getCell(1,0);
            celda.setValue("FECHA INICIO : "+FechaI1);
            celda.setStyle(estiloEmpresa);
            estiloEmpresa.setFont ( fuenteTitulo ); 
            estiloEmpresa.getBorderColor(2);
            celdas.merge(1, 0, 1, 2);
            
            
            //CELDA FECHA FINAL
            celda = hoja.getCell(2,0);
            celda.setValue("FECHA FINAL : "+FechaF1);
            celda.setStyle(estiloEmpresa);
            estiloEmpresa.setFont ( fuenteTitulo ); 
            estiloEmpresa.getBorderColor(2);
            celdas.merge(2, 0, 2, 2);
            //COLUMNA DE DATOS
              
            int columnas = 0;
            int filas = 5;
            
            
            Style estiloDatos = libro.createStyle();
            estiloDatos.setColor(com.aspose.cells.Color.BLUE);
            estiloDatos.setFont ( fuente ); 
            
            estiloDatos.setHAlignment( HorizontalAlignmentType.CENTRED );
           
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("FECHA");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("TIPO");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("SEMIREMOLQUE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("DESCRIPCIÓN");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("CABEZOTE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("OT");
            celda.setStyle(estiloDatos);
            hoja.setColumnWidth( columnas,12 );
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("ORIGEN OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("DESTINO OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("NRO INTERNO");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("KMS");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("VALOR OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("VALOR OT");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("CLIENTE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("ORIGEN OT");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("DESTINO OT");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("CONCEPTO");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("DESCUENTO");
            celda.setStyle(estiloDatos);
            
             columnas = 1;

            for(int i=0; i<vector.size();i++ ){
                columnas =0;
                filas++ ;
          
                Hashtable ht = (Hashtable) vector.get(i);
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "fecdsp" ) );
                
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "tipo" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "semiremolque" ) );
                                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "descripcion" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "cabezote" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "ot" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "oc" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "origenOC" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "destinoOC" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "Ninterno" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( Double.parseDouble( (String) ht.get( "km" ) ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( Double.parseDouble( (String) ht.get( "valorOC" ) ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( Double.parseDouble( (String) ht.get( "valorOT" ) ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "cliente" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "origenOT" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "destinoOT" ) );
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue( (String) ht.get( "concepto" ) );
                
                 celda = hoja.getCell(filas, columnas++);
                 celda.setValue( Double.parseDouble( (String) ht.get( "descuento" ) ) );

            }
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
            libro.save(Ruta1 +"ReporteProducionEquipo_" + hoy + ".xls");
             
        System.out.println("YA");
          
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    
    public static void main(String[]sfhgsd) throws Exception{
        ReporteProduccionEquiposThreads h = new ReporteProduccionEquiposThreads();
        h.start("2007-04-01", "2007-04-17", "HOSORIO");
    }
}
