/*
 * ListadoAnticiposTh.java
 *
 * Created on 28 de mayo de 2008, 19:05
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.RepGral;
import com.tsp.operation.model.beans.AnticipoGasolina;
import com.tsp.operation.model.Model;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import com.tsp.util.Util;

/**
 *
 * @author  Fintra
 */
public class ListadoAnticiposTh extends Thread{
    
    
    
    private ArrayList info;
    private Model model;
    private Usuario usuario;
    
    private SimpleDateFormat fmt;
    private String processName = "Reporte de Anticipos";
    
    
    
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;
    
    
    /** Creates a new instance of HReporteOcsConAnticipoSinReporte */
    public ListadoAnticiposTh() {
    }
    
    public void start(Model model,ArrayList info, Usuario usuario){
        this.info = info;
        this.usuario = usuario;
        this.model = model;
        super.start();
    }
    
    public synchronized void run(){
        try{
            //System.out.println("run en lnth");
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del Reporte de Anticipos", usuario.getLogin());
            //System.out.println("despues de proceso insertado");
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HReporteOcsConAnticipoSinReporte ...\n" + e.getMessage());
            }
        }
    }
    
    
    public void generarRUTA() throws Exception{
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            /*cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);*/
            
            
            // estilos
            header         = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo3        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo4        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo5        = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            ftofecha       = xls.nuevoEstilo("Tahoma", 8 , false , false, "yyyy-mm-dd" , xls.NONE , xls.NONE , xls.NONE);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
        
    }
    
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            xls.adicionarCelda(2,0, "USUARIO", titulo1);
            xls.adicionarCelda(2,1, usuario.getNombre() , titulo1);
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ReporteAnticipos_" + fmt.format( new Date() ) +".xls", "REPORTE DE ANTICIPOS ");
            fila = 6;
            
            ArrayList vector = this.info;
            if (vector!=null && !vector.isEmpty()){
                


                    // encabezado
                String [] cabecera = { 
                    "ID","PLANILLA", "FECHA","VALOR ANTICIPO","CONDUCTOR","VALOR ENTREGADO","PROPIETARIO","FACTURADOR","FECHA FACTURA","APROBADOR","FECHA APROBACION","VALOR COMBUSTIBLE","VALOR EFECTIVO","ESTADO","POSIBLE CXP","POSIBLE CXP 2","CXP","SUMA ANTICIPOS DE PLANILLA","SUMA CXPS TSP DE PLANILLA","RESTA","CXP TSP POSIBLE","CORRIDA","CHEQUE","VALOR","CXP TSP POSIBLE 2","CORRIDA","CHEQUE","VALOR"
                };//090830
                short  [] dimensiones = new short [] { 
                    2590,2590, 3060, 4450, 8340, 5000,8340,4170,3750,5280,6000,5500,5400,2328,2819,2969,2000,6669,6328,2328,3723,2328,2328,2328,4001,2328,2328,2328
                };//090830
                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }                
                fila++;
                double vrneg=0;
                String tipo="";
                for ( int i = 0; i< vector.size(); i++, fila++){
                    int col = 0;
                    AnticipoGasolina anticipoGasolina = (AnticipoGasolina) vector.get(i);
                    xls.adicionarCelda(fila  , col++ , anticipoGasolina.getId(), letra  );//090829
                    xls.adicionarCelda(fila  , col++ , anticipoGasolina.getPlanilla(), letra  );
                    xls.adicionarCelda(fila  , col++ , (anticipoGasolina.getFecha_anticipo()).substring(0,10), letra  );
                    xls.adicionarCelda(fila  , col++ , com.tsp.util.Util.customFormat(anticipoGasolina.getVlr()), dinero  );
                    xls.adicionarCelda(fila  , col++ , anticipoGasolina.getNombreConductor() , letra  );
                    xls.adicionarCelda(fila  , col++ , com.tsp.util.Util.customFormat(anticipoGasolina.getVlrNeto()), dinero  );                    
                    xls.adicionarCelda(fila  , col++ , anticipoGasolina.getPla_owner()+" - "+anticipoGasolina.getNombrePropietario(), letra  );                    
                    xls.adicionarCelda(fila  , col++ , anticipoGasolina.getUser_transferencia(), letra  );
                    if ((anticipoGasolina.getFecha_transferencia()).equals("")){			
                        xls.adicionarCelda(fila  , col++ , anticipoGasolina.getFecha_transferencia(), letra  );                        
                    }else{
                        xls.adicionarCelda(fila  , col++ , anticipoGasolina.getFecha_transferencia().substring(0,16), letra  );                        
                    }                    
                    xls.adicionarCelda(fila  , col++ ,anticipoGasolina.getUser_autorizacion() , letra  );
                    if ((anticipoGasolina.getFecha_autorizacion()).equals("")){			
                        xls.adicionarCelda(fila  , col++ , anticipoGasolina.getFecha_autorizacion(), letra  );                        
                    }else{
                        xls.adicionarCelda(fila  , col++ , anticipoGasolina.getFecha_autorizacion().substring(0,16), letra  );                        
                    }
                    xls.adicionarCelda(fila  , col++ , com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getVlr_gasolina())), dinero  );
                    xls.adicionarCelda(fila  , col++ , com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getVlr_efectivo())), dinero  );
                    xls.adicionarCelda(fila  , col++ , anticipoGasolina.getEstado_pago_tercero(), letra  );
                    
                    xls.adicionarCelda(fila  , col++ , anticipoGasolina.getCxpPosible(), letra  );
                    xls.adicionarCelda(fila  , col++ , anticipoGasolina.getCxpPosible2(), letra  );
                    xls.adicionarCelda(fila  , col++ , anticipoGasolina.getCxp(), letra  );                
                    
                    xls.adicionarCelda(fila  , col++ , ""+com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getSumaAnticiposDplanilla())), dinero  );
                    xls.adicionarCelda(fila  , col++ , ""+com.tsp.util.Util.customFormat(Double.parseDouble(anticipoGasolina.getSumaCxpsTspDplanilla())	)	, dinero  );                    
                    xls.adicionarCelda(fila  , col++ , ""+com.tsp.util.Util.customFormat((Double.parseDouble(anticipoGasolina.getSumaCxpsTspDplanilla())-Double.parseDouble(anticipoGasolina.getSumaAnticiposDplanilla()))), dinero );
                    
                    String[] CxpTspPosible=anticipoGasolina.getCxpTspPosible_corrida_cheque().split("_");
                    if (CxpTspPosible.length>0){xls.adicionarCelda(fila  , col++ , CxpTspPosible[0], letra  );}else{xls.adicionarCelda(fila  , col++ ,"", letra  );}
                    if (CxpTspPosible.length>1){xls.adicionarCelda(fila  , col++ , CxpTspPosible[1], letra  );}else{xls.adicionarCelda(fila  , col++ ,"", letra  );}
                    if (CxpTspPosible.length>2){xls.adicionarCelda(fila  , col++ , CxpTspPosible[2], letra  );}else{xls.adicionarCelda(fila  , col++ ,"", letra  );}
                    if (CxpTspPosible.length>3){xls.adicionarCelda(fila  , col++ , com.tsp.util.Util.customFormat(Double.parseDouble(CxpTspPosible[3])), dinero  );}else{xls.adicionarCelda(fila  , col++ ,"0", dinero  );}
                    CxpTspPosible=anticipoGasolina.getCxpTspPosible2_corrida_cheque().split("_");
                    if (CxpTspPosible.length>0){xls.adicionarCelda(fila  , col++ , CxpTspPosible[0], letra  );}else{xls.adicionarCelda(fila  , col++ ,"", letra  );}
                    if (CxpTspPosible.length>1){xls.adicionarCelda(fila  , col++ , CxpTspPosible[1], letra  );}else{xls.adicionarCelda(fila  , col++ ,"", letra  );}
                    if (CxpTspPosible.length>2){xls.adicionarCelda(fila  , col++ , CxpTspPosible[2], letra  );}else{xls.adicionarCelda(fila  , col++ ,"", letra  );}
                    if (CxpTspPosible.length>3){xls.adicionarCelda(fila  , col++ , com.tsp.util.Util.customFormat(Double.parseDouble(CxpTspPosible[3])), dinero  );}else{xls.adicionarCelda(fila  , col++ ,"0", dinero  );}
                    
                    //vrneg=rp.getVr_negocio();
                    
                    //xls.adicionarCelda(fila  , col++ , vrneg, dinero  );
                    
                    /*xls.adicionarCelda(fila  , col++ , rp.getVr_desem(), dinero  );
                    xls.adicionarCelda(fila  , col++ , rp.getTotpagado(), dinero  );
                    xls.adicionarCelda(fila  , col++ , rp.getEstado(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getFecha_neg(), ftofecha  );
                    xls.adicionarCelda(fila  , col++ , rp.getFechatran(), ftofecha  );*/
                    
                    /*if(rp.getCmc().equals("01")){
                        tipo="Normal";
                    }else{
                        tipo="Cartera";
                    }*/
                    //xls.adicionarCelda(fila  , col++ , tipo, ftofecha  );
                    
                }            

                
                
            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;                
            }
            
            
            this.cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
}

//22 Marzo de 2007

