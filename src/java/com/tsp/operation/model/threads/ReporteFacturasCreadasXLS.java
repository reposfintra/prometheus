/*
 * ReporteFacturasCreadasXLS.java
 *
 * Created on 10 de octubre de 2005, 04:27 PM
 */

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class ReporteFacturasCreadasXLS extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String dstrct = "";
    //sescalante 16.03.06
    String agencia = "";
    String fchi = "";
    String fchf = "";
    Model model;
    
    /** Creates a new instance of ReporteFacturasCreadasXLS */
    public ReporteFacturasCreadasXLS(Model model) {
	this.model = model;
    }
    
    public void start(String usuario, String dstrct, String agencia, String fchi, String fchf){        
        this.id = usuario;
        this.dstrct = dstrct;
        this.procesoName = "Reporte de Facturas Creadas";
        this.des = "Generacion de Reporte de Facturas creadas entre " + fchi + " - " + fchf;
        //sescalante
        this.agencia = agencia;
        this.fchi = fchi;
        this.fchf = fchf;
        super.start();
    }
    
    public synchronized void run(){
        try{
            String fecha_actual = Util.getFechaActual_String(6);
            
            //registramos el proeso en el log de procesos
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            
            //obtenemos la lista de facturas creadas segun parametros definidos
            model.cxpDocService.reporteFacturasXLS(dstrct, agencia, fchi, fchf);
            List lista = model.cxpDocService.getFacturas();
            
            if (lista!=null && lista.size()>0){
                
                Usuario us = (Usuario) model.usuarioService.obtenerUsuario(id);
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                
                File file = new File(path);
                file.mkdirs();
                
                String a�o = fecha_actual.substring(0,4);
                String mes = fecha_actual.substring(5,7);
                String dia = fecha_actual.substring(8,10);
                String hora = fecha_actual.substring(11,13);
                String min = fecha_actual.substring(14,16);
                String seg = fecha_actual.substring(17,19);
                
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite(path+"/FACTURAS_CREADAS " +a�o + "-" + mes + "-" + dia + " " + hora + "." + min + "." + seg + ".xls");
                
                // Definicion de estilos: fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado                                           
                HSSFCellStyle fecha = xls.nuevoEstilo(      "verdana", 12, false , false, "yyyy/mm/dd", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_RIGHT );
                HSSFCellStyle texto = xls.nuevoEstilo(      "verdana", 12, false, false, "text", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle numero = xls.nuevoEstilo(     "verdana", 12, false, false, "", xls.NONE, xls.NONE, HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle negrita = xls.nuevoEstilo(    "verdana", 12, true, false, "text", HSSFColor.BLACK.index, xls.NONE, xls.NONE);
                HSSFCellStyle header = xls.nuevoEstilo(     "verdana", 18, true, false, "text", HSSFColor.LIME.index,xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo = xls.nuevoEstilo(     "verdana", 16, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle subtitulo = xls.nuevoEstilo(  "verdana", 14, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle cabecera = xls.nuevoEstilo(   "verdana", 12, true, false, "text", HSSFColor.WHITE.index, HSSFColor.INDIGO.index, HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja("BASE");
                //formateo al abrir hoja (tamano)
                xls.cambiarMagnificacion(3,4);
                
                //Titulo (cabecera)
                xls.adicionarCelda(0, 0, "TRANSPORTES SANCHEZ POLO S.A", header);
                xls.combinarCeldas(0, 0, 0, 5);  
                xls.adicionarCelda(2, 0, "REPORTE DE FACTURAS CREADAS", titulo);
                xls.combinarCeldas(2, 0, 0, 5);
                xls.adicionarCelda(3, 0, "Fecha Inicial:", subtitulo);
                xls.adicionarCelda(3, 1, fchi, negrita);
                xls.adicionarCelda(3, 2, "Fecha Final:", subtitulo);
                xls.adicionarCelda(3, 3, fchf, negrita);
                xls.adicionarCelda(4, 0, "Elaborado Por: ", subtitulo);
                xls.adicionarCelda(4, 1, us.getNombre(), negrita);
                xls.adicionarCelda(4, 2, "Agencia:", subtitulo);
                xls.adicionarCelda(4, 3, model.ciudadservice.obtenerNombreCiudad(us.getId_agencia()), negrita);
                xls.adicionarCelda(4, 4, "Distrito:", subtitulo);
                xls.adicionarCelda(4, 5, dstrct, negrita);
                xls.adicionarCelda(5, 0, "Fecha Reporte", subtitulo);
                xls.adicionarCelda(5, 1, fecha_actual, negrita);
                
                int fila = 7;
                int col  = 0;
                
                //definicion de titulos de la tabla
                xls.adicionarCelda(fila ,col++ , "Proveedor"                                , cabecera );
                xls.adicionarCelda(fila ,col++ , "Nombre"                                   , cabecera );
                xls.adicionarCelda(fila ,col++ , "Handle Code"                              , cabecera );
                xls.adicionarCelda(fila ,col++ , "Id Mims"                                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "Tipo Documento"                           , cabecera );
                xls.adicionarCelda(fila ,col++ , "Numero del Documento"                     , cabecera );
                xls.adicionarCelda(fila ,col++ , "Fecha del Doumento"                       , cabecera );
                xls.adicionarCelda(fila ,col++ , "Tipo Documento Relacionado"               , cabecera );
                xls.adicionarCelda(fila ,col++ , "Documento Relacionado"                    , cabecera );
                xls.adicionarCelda(fila ,col++ , "Usuario de Aprobaci�n"                    , cabecera );
                xls.adicionarCelda(fila ,col++ , "Banco"                                    , cabecera );
                xls.adicionarCelda(fila ,col++ , "Sucursal"                                 , cabecera );
                xls.adicionarCelda(fila ,col++ , "Valor Neto"                               , cabecera );
                xls.adicionarCelda(fila ,col++ , "Valor Neto ME"                            , cabecera );
                xls.adicionarCelda(fila ,col++ , "Moneda"                                   , cabecera );
                xls.adicionarCelda(fila ,col++ , "Tasa"                                     , cabecera );
                xls.adicionarCelda(fila ,col++ , "Usuario de Creacion"                      , cabecera );
                xls.adicionarCelda(fila ,col++ , "Agencia"                                  , cabecera );
                
                //definicion de la tabla con los datos obtenidos de la consulta
                Iterator it = lista.iterator();
                
                while(it.hasNext()){
                    fila++;
                    col = 0;
                    
                    CXP_Doc factura = (CXP_Doc) it.next();                    
                    
                    xls.adicionarCelda(fila ,col++ , factura.getProveedor()             , texto );
                    xls.adicionarCelda(fila ,col++ , factura.getNomProveedor()          , texto );
                    xls.adicionarCelda(fila ,col++ , factura.getHandle_code()           , texto );
                    xls.adicionarCelda(fila ,col++ , factura.getId_mims()               , texto );                    
                    xls.adicionarCelda(fila ,col++ , factura.getTipo_documento()        , texto );
                    xls.adicionarCelda(fila ,col++ , factura.getDocumento()             , texto );
                    xls.adicionarCelda(fila ,col++ , factura.getFecha_documento()       , fecha );    
                    xls.adicionarCelda(fila ,col++ , factura.getTipo_documento_rel()    , texto );    
                    xls.adicionarCelda(fila ,col++ , factura.getDocumento_relacionado() , texto );
                    xls.adicionarCelda(fila ,col++ , factura.getUsuario_aprobacion()    , texto );    
                    xls.adicionarCelda(fila ,col++ , factura.getBanco()                 , texto );    
                    xls.adicionarCelda(fila ,col++ , factura.getSucursal()              , texto );    
                    xls.adicionarCelda(fila ,col++ , factura.getVlr_neto()              , numero );
                    xls.adicionarCelda(fila ,col++ , factura.getVlr_neto_me()           , numero );
                    xls.adicionarCelda(fila ,col++ , factura.getMoneda()                , texto );
                    xls.adicionarCelda(fila ,col++ , factura.getTasa()                  , numero );
                    xls.adicionarCelda(fila ,col++ , factura.getCreation_user()         , texto );
                    xls.adicionarCelda(fila ,col++ , factura.getUcagencia()             , texto );
                }
                
                xls.cerrarLibro();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "Proceso Exitoso!");
            }else {
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "No se encontraron Facturas creadas en este rango de fechas...");
            }
            model.cxpDocService.setEnproceso();
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
                model.cxpDocService.setEnproceso();
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
