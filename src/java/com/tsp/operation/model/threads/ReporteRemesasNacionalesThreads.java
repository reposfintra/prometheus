/*
 * ReporteRemesasNacionalesThreads.java
 *
 * Created on 25 de abril de 2007, 04:57 PM
 */

package com.tsp.operation.model.threads;

import com.aspose.cells.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;

/**
 *
 * @author  EQUIPO26
 */
public class ReporteRemesasNacionalesThreads extends Thread{
    
    String user;
    String FechaI1;
    String FechaF1;
    String procesoName;
    String des;
    Model model = new Model();
    
    /** Creates a new instance of ReporteProduccionEquiposThreads */
    public void start(String FechaI, String FechaF, String usuario){
        this.user = usuario;
        this.procesoName = "Reporte Remesas Nacionales";
        this.des = "Reporte Remesas Nacionales";
        this.FechaI1 = FechaI;
        this.FechaF1 = FechaF;
        //this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
            
            List ListC1 =  model.reporteGeneralService.reporteRemesasNacionalesCaso1(FechaI1, FechaF1);
            
            if( ListC1 != null && ListC1.size()>0 ){
                //DECLARACION DE VARIABLES
                Date fecha = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String now              = format.format(fecha);
                String fechaCrea        = now.substring(0,10);
                Vector vectorC2         =  new Vector();
                int mayor               = 0;
                Workbook libro          = new Workbook();
                Worksheets hojas        = libro.getWorksheets();
                Worksheet hoja          = hojas.getSheet(0);
                Font fuente             = new Font();
                Font fuenteTitulo       = new Font();
                Style estiloEmpresa     = libro.createStyle();
                Cells celdas            = hoja.getCells();
                Cell celda              = hoja.getCell(0,0);
                int columnas            = 0;
                int filas               = 5;
                int j                   = 0 ;
                int k                   = 0 ;
                List ListC3             = new LinkedList();
                
                hoja.setName("HOJA1");
                
                //fuente de los datos
                fuente.setBold(true);
                fuente.setName("arial");
                fuente.setColor( com.aspose.cells.Color.WHITE );
                
                
                //datos de los titulos
                fuenteTitulo.setBold(true);
                fuenteTitulo.setName("arial");
                fuenteTitulo.setColor( com.aspose.cells.Color.BLACK );
                
                
                //CELDA TITULO
                estiloEmpresa.setColor( com.aspose.cells.Color.WHITE );
                celda.setValue("TRANSPORTES SANCHEZ POLO ");
                celda.setStyle(estiloEmpresa);
                estiloEmpresa.setFont( fuenteTitulo );
                estiloEmpresa.getBorderColor(2);
                celdas.merge(0, 0, 0, 2);
                
                
                //CELDA FECHA INICIO
                celda = hoja.getCell(1,0);
                celda.setValue("FECHA INICIO : "+FechaI1);
                celda.setStyle(estiloEmpresa);
                estiloEmpresa.setFont( fuenteTitulo );
                estiloEmpresa.getBorderColor(2);
                celdas.merge(1, 0, 1, 2);
                
                
                //CELDA FECHA FINAL
                celda = hoja.getCell(2,0);
                celda.setValue("FECHA FINAL : "+FechaF1);
                celda.setStyle(estiloEmpresa);
                estiloEmpresa.setFont( fuenteTitulo );
                estiloEmpresa.getBorderColor(2);
                celdas.merge(2, 0, 2, 2);
                
                //CELDA USUARIO
                celda = hoja.getCell(3,0);
                celda.setValue("GENERADO POR : "+user);
                celda.setStyle(estiloEmpresa);
                estiloEmpresa.setFont( fuenteTitulo );
                estiloEmpresa.getBorderColor(2);
                celdas.merge(3, 0, 3, 2);
                
                
                Style estiloDatos = libro.createStyle();
                estiloDatos.setColor(com.aspose.cells.Color.BLUE);
                estiloDatos.setFont( fuente );
                estiloDatos.setHAlignment( HorizontalAlignmentType.CENTRED );
                
                
                
                //SUBTITULOS
                celda = hoja.getCell(filas, columnas++); celda.setValue("TIPO OT"); celda.setStyle(estiloDatos);
                celda = hoja.getCell(filas, columnas++); celda.setValue("OT"); celda.setStyle(estiloDatos);
                celda = hoja.getCell(filas, columnas++); celda.setValue("ESTADO OT"); celda.setStyle(estiloDatos);
                celda = hoja.getCell(filas, columnas++); celda.setValue("FECHA OT"); celda.setStyle(estiloDatos);
                celda = hoja.getCell(filas, columnas++); celda.setValue("CLIENTE"); celda.setStyle(estiloDatos);
                celda = hoja.getCell(filas, columnas++); celda.setValue("ORIGEN OT"); celda.setStyle(estiloDatos);
                celda = hoja.getCell(filas, columnas++); celda.setValue("DESTINO OT"); celda.setStyle(estiloDatos);
                celda = hoja.getCell(filas, columnas++); celda.setValue("AGENCIA"); celda.setStyle(estiloDatos);
                celda = hoja.getCell(filas, columnas++); celda.setValue("OT PADRE"); celda.setStyle(estiloDatos);
                celda = hoja.getCell(filas, columnas++); celda.setValue("OT RELACIONADAS"); celda.setStyle(estiloDatos);
                
                Hashtable h = (Hashtable)ListC1.get( ListC1.size()-1 );
                mayor = Integer.parseInt( (String) h.get("tamano") );
                
                for ( j = 1 ; j <= mayor; j++ ){
                    
                    celda = hoja.getCell(filas, columnas++); celda.setValue("OC "+ j ); celda.setStyle(estiloDatos);
                    celda = hoja.getCell(filas, columnas++); celda.setValue("CASO"); celda.setStyle(estiloDatos);
                    celda = hoja.getCell(filas, columnas++); celda.setValue("FECHA OC "+ j ); celda.setStyle(estiloDatos);
                    celda = hoja.getCell(filas, columnas++); celda.setValue("PLACA "+ j ); celda.setStyle(estiloDatos);
                    celda = hoja.getCell(filas, columnas++); celda.setValue("ORIGEN OC "+ j  ); celda.setStyle(estiloDatos);
                    celda = hoja.getCell(filas, columnas++); celda.setValue("DESTINO OC "+ j  ); celda.setStyle(estiloDatos);
                    celda = hoja.getCell(filas, columnas++); celda.setValue("VALOR ANTICIPO"); celda.setStyle(estiloDatos);
                    celda = hoja.getCell(filas, columnas++); celda.setValue("FECHA ANTICIPO"); celda.setStyle(estiloDatos);
                    celda = hoja.getCell(filas, columnas++); celda.setValue("ULT REP EN TRAFICO"); celda.setStyle(estiloDatos);
                    
                }
                
                for ( int i = 0 ; i < ListC1.size(); i++ ){
                    columnas =0;
                    filas++ ;
                    
                    Hashtable ht = (Hashtable) ListC1.get(i);
                    
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue( (String) ht.get( "tipoOT" ) );
                    
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue( (String) ht.get( "ot" ) );
                    
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue( (String) ht.get( "estadoOT" ) );
                    
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue( (String) ht.get( "fechaOT" ) );
                    
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue( (String) ht.get( "clienteOT" ) );
                    
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue( (String) ht.get( "origenOT" ) );
                    
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue( (String) ht.get( "destinoOT" ) );
                    
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue( (String) ht.get( "agenciaOT" ) );
                    
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue( (String) ht.get( "remesa_padre" ) );
                    
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue( (String) ht.get( "remesa_relacionada" ) );
                    
                    //obtengo las planillas asociadas a esa remesa
                    vectorC2 =  model.reporteGeneralService.reporteRemesasNacionalesCaso2( (String) ht.get("ot") );
                    
                    if( vectorC2 != null && vectorC2.size() > 0 ){
                        
                        for ( j = 0 ; j < vectorC2.size(); j++ ){
                            
                            ListC3 = model.reporteGeneralService.reporteRemesasNacionalesCaso3( (String) vectorC2.get(j) );
                            
                            if( ListC3 != null && ListC3.size() > 0 ){
                                
                                for ( k = 0 ; k < ListC3.size(); k++ ){
                                    
                                    Hashtable ht2 = (Hashtable) ListC3.get(k);
                                    
                                    celda = hoja.getCell(filas, columnas++);
                                    celda.setValue( (String) ht2.get( "oc" ) );
                                    
                                    celda = hoja.getCell(filas, columnas++);
                                    celda.setValue( (String) ht2.get( "caso" ) );
                                    
                                    celda = hoja.getCell(filas, columnas++);
                                    celda.setValue( (String) ht2.get( "fechaOC" ) );
                                    
                                    celda = hoja.getCell(filas, columnas++);
                                    celda.setValue( (String) ht2.get( "placaOC" ) );
                                    
                                    celda = hoja.getCell(filas, columnas++);
                                    celda.setValue( (String) ht2.get( "origenOC" ) );
                                    
                                    celda = hoja.getCell(filas, columnas++);
                                    celda.setValue( (String) ht2.get( "destinoOC" ) );
                                    
                                    celda = hoja.getCell(filas, columnas++);
                                    celda.setValue( Double.parseDouble( (String) ht2.get( "anticipo" ) ) );
                                    
                                    celda = hoja.getCell(filas, columnas++);
                                    celda.setValue( (String) ht2.get( "fecha_anticipo" ) );
                                    
                                    celda = hoja.getCell(filas, columnas++);
                                    celda.setValue( (String) ht2.get( "ult_reporte" ) );
                                    
                                }
                            }
                        }
                        
                        vectorC2.clear();
                        
                    }
                }
                
                //GENERACION DEL ARCHIVO
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta");
                //armas la ruta
                String Ruta1  = path + "/exportar/migracion/" + user + "/";
                //crear la ruta
                File file = new File(Ruta1);
                file.mkdirs();
                String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
                libro.save(Ruta1 +"ReporteRemesasNacionales_" + hoy + ".xls");
                
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
                
            }
            else
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"No se encontraron datos para el reporte");
            model.cxpDocService.setEnproceso();
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
                model.cxpDocService.setEnproceso();
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                    model.cxpDocService.setEnproceso();
                }catch(Exception p){    }
            }
        }
        
    }
    
    public static void main(String[]sfhgsd) throws Exception{
        ReporteRemesasNacionalesThreads h = new ReporteRemesasNacionalesThreads();
        h.start("2007-04-23", "2007-04-24", "HOSORIO");
    }
    
}
