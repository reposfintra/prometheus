/********************************************************************
 *      Nombre Clase.................   RepDespachoManualTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   03.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;

//Logger
import org.apache.log4j.*;
import com.tsp.operation.model.beans.POIWrite;


public class RepDespachoManualTh extends Thread{
    
    Logger logger = Logger.getLogger (ReportePCTh.class);
    
    private Vector reporte;
    private Usuario user;    
    private String dstrct;
    private String fechai;
    private String fechaf;
    
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Usuario usuario, String dstrct, String fechai, String fechaf){
        
        logger.info("GENERACION DEL REPORTE DE VIAJES MANUALES");
        
        this.user = usuario;
        this.dstrct = dstrct;
        this.fechaf = fechaf;
        this.fechai = fechai;
        
        this.model = modelo;
        
        this.procesoName = "Reporte de Despachos Manuales";
        this.des = "Reporte de Despachos Manuales del " + fechai + " hasta " + fechaf;
                
        super.start();
    }    
    
    public synchronized void run(){
        try{
            
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user.getLogin());
            
            model.traficoService.reporteDespachoManual(dstrct, this.fechai, this.fechaf);
            this.reporte = model.traficoService.getRegistros();
            
            logger.info("RESULTADOS ENCONTRADOS: " + this.reporte.size());
        
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user.getLogin() + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"Reporte_Viajes_Manuales_" + FechaFormated + ".xls");
            String nom_archivo = "ReportePC_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd hh:mm"  , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            //\"$ \"
                                                
            xls.obtenerHoja("Reporte de Viajes Manuales");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            int nceldas = 16;
            
            xls.combinarCeldas(0, 0, 0, nceldas);
            xls.combinarCeldas(3, 0, 3, nceldas);
            xls.combinarCeldas(4, 0, 4, nceldas);
            xls.adicionarCelda(0, 0, "REPORTE DE VIAJES MANUALES", header2);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
                            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila, col++, "OC", header1);
            xls.adicionarCelda(fila, col++, "FECHA DESPACHO", header1);
            xls.adicionarCelda(fila, col++, "PLACA", header1);
            xls.adicionarCelda(fila, col++, "AGENCIA DE DESPACHO", header1);
            xls.adicionarCelda(fila, col++, "RUTA", header1);
            xls.adicionarCelda(fila, col++, "VIA", header1);
            xls.adicionarCelda(fila, col++, "OT", header1);
            xls.adicionarCelda(fila, col++, "DESCRIPCION", header1);
            xls.adicionarCelda(fila, col++, "CONDUCTOR", header1);
            xls.adicionarCelda(fila, col++, "CLIENTE", header1);
            xls.adicionarCelda(fila, col++, "LEGALIZADA", header1);
            xls.adicionarCelda(fila, col++, "MOTIVO MANUAL", header1);
            
            xls.adicionarCelda(fila, col++, "PC ULTIMO REPORTE", header1);
            xls.adicionarCelda(fila, col++, "FECHA ULTIMO REPORTE", header1);
            xls.adicionarCelda(fila, col++, "PC PROX REPORTE", header1);
            xls.adicionarCelda(fila, col++, "FECHA PROX. REPORTE", header1);
            xls.adicionarCelda(fila, col++, "ULTIMA OBSERVACION", header1);
            //xls.adicionarCelda(fila, col++, "", header1);
            
            fila++;
            
            for( int i=0; i<reporte.size(); i++){
                col = 0;
                
                Hashtable ht = (Hashtable) reporte.elementAt(i);
                
                xls.adicionarCelda(fila, col++, ht.get("oc").toString(), texto);
                                
                java.util.Date date = com.tsp.util.Util.ConvertiraDate0(ht.get("fecdespacho").toString()+":00");
                //xls.adicionarCelda(fila, col++, ht.get("fecharep").toString(), texto);
                xls.adicionarCelda(fila, col++, date, fecha); 
                
                xls.adicionarCelda(fila, col++, ht.get("placa").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("agencia").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("origen").toString() + " - " + ht.get("destino").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("via").toString(), texto);
                xls.adicionarCelda(fila, col++, "", texto);
                                
                xls.adicionarCelda(fila, col++, ht.get("descripcion").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("nomcond").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("cliente").toString(), texto);
                                
                xls.adicionarCelda(fila, col++, ht.get("legal").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("justific").toString(), texto);
                
                
                xls.adicionarCelda(fila, col++, ht.get("nompto_control_ultreporte").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("fecha_ult_reporte").toString(), texto);                                
                xls.adicionarCelda(fila, col++, ht.get("nompto_control_proxreporte").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("fecha_prox_reporte").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("ult_observacion").toString(), texto);
                
                fila++;
            }         
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "PROCESO EXITOSO");
            
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}