/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;


import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.DetallePrefactura;

import com.tsp.operation.model.Model;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;


/**
 *
 * @author Alvaro
 */
public class HPrefacturaDetalle extends Thread{


    private Model model;
    private Usuario usuario;
    private String prefactura;

    private SimpleDateFormat fmt;
    private String processName = "Reporte de Prefactura";



    // Variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;




    /** Creates a new instance of HPrefacturaDetalle */
    public HPrefacturaDetalle () {
    }


    public void start(Model model, Usuario usuario, String prefactura){

        this.usuario = usuario;
        this.model = model;
        this.prefactura = prefactura;
        super.start();
    }



    public synchronized void run(){
        try{
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del reporte de prefactura : " + prefactura, usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HPrefacturaDetalle ...\n" + e.getMessage());
            }
        }
    }


    public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }



    /**
     * Exportacion de una prefactura detallada
     * @autor apabon
     * @throws Exception.
     */
    private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ReportePrefactura_" + fmt.format( new Date() ) +".xls", "REPORTE DE PREFACTURA ");
            fila = 6;
            
            
            // encabezado
            String [] cabecera = {
                "PREFACTURA","FECHA PREFACTURA","ORDEN","NUM OS","ACCION", "CONSECUTIVO", "FACTURA CONFORMADA",
                "NOMBRE CLIENTE", "SIMBOLO VARIABLE","MATERIAL","MANO DE OBRA","OTROS","SUBTOTAL (TOTALPREV1)" ,

                "ADMNISTRACION","IMPREVISTO","UTILIDAD",

                "BONIFICACION","SUBTOTAL",
                "BASE IVA",
                "IVA","VALOR PREFACTURA","% RET MAT","RET MATERIAL",
                "% RET MOB","RET MANO OBRA","% RET OTROS", "RET OTROS","TOTAL RETENCION",
                "CUOTAS","% FACTORING","FACTORING","% FORMULA","FORMULA",
                "VALOR A PAGAR"
            };//linea que cambio    //new

            short  [] dimensiones = new short [] {
                5000,5500, 5500, 6500, 3500, 5000,6000,6000,6000,6000,6000,
                6000,6000,6000,6000,6000,6000,6000,6000,6000,6000,6000,6000,
                6000,6000,6000,6000,6000,6000,6000,6000,6000,6000,6000
            };//linea que cambio    //new
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                if ( i < dimensiones.length )
                    xls.cambiarAnchoColumna(i, dimensiones[i] );
            }            
            
 
            // Inicia el proceso de listar a excell
            
            List listaDetallePrefactura = model.applusService.getDetallePrefactura();
            DetallePrefactura detallePrefactura =  new DetallePrefactura();

            Iterator it = listaDetallePrefactura.iterator();
            
            double suma_material=0;
            double suma_mo=0;
            double suma_otros=0;
            double suma_subtotalprev1=0;
            double suma_boni=0;
            double suma_subtotal1=0;
            double suma_iva=0;
            double suma_valprefactura=0;
            double suma_retmat=0;
            double suma_retmo=0;
            double suma_retotros=0;
            double suma_totret=0;
            double suma_factoring=0;
            double suma_formula=0;
            double suma_tot=0;
            double suma_a=0,suma_i=0,suma_u=0,suma_baseiva=0;
                
            while (it.hasNext()) {
            
              detallePrefactura = (DetallePrefactura)it.next();

              fila++;
              int col = 0;

              double totalprev1 = detallePrefactura.getVlr_mat() +
                                  detallePrefactura.getVlr_mob() +
                                  detallePrefactura.getVlr_otr();
              double subtotal1  = totalprev1 + detallePrefactura.getBonificacion();
              double subtotal2  = subtotal1  + detallePrefactura.getVlr_iva();
              double vlrPago    = subtotal1 + detallePrefactura.getVlr_iva() +
                                  detallePrefactura.getVlr_retencion()   +
                                  detallePrefactura.getValor_factoring() +
                                  detallePrefactura.getValor_formula();
              
     
              suma_material=suma_material+detallePrefactura.getVlr_mat();
              suma_mo=suma_mo+detallePrefactura.getVlr_mob();
              suma_otros=suma_otros+detallePrefactura.getVlr_otr();
              suma_subtotalprev1=suma_subtotalprev1+totalprev1;
              suma_boni=suma_boni+detallePrefactura.getBonificacion();
              suma_subtotal1=suma_subtotal1+subtotal1;
              suma_iva=suma_iva+detallePrefactura.getVlr_iva();
              suma_valprefactura=suma_valprefactura+subtotal2;
              suma_retmat=suma_retmat+detallePrefactura.getVlr_rmat();
              suma_retmo=suma_retmo+detallePrefactura.getVlr_rmob();
              suma_retotros=suma_retotros+detallePrefactura.getVlr_rotr();
              suma_totret=suma_totret+detallePrefactura.getVlr_retencion();
              suma_factoring=suma_factoring+detallePrefactura.getValor_factoring();
              suma_formula=suma_formula+detallePrefactura.getValor_formula();
              suma_tot=suma_tot+vlrPago;
              
              suma_a=suma_a+detallePrefactura.getVlr_admnistracion();//new
              suma_i=suma_i+detallePrefactura.getVlr_imprevisto();//new
              suma_u=suma_u+detallePrefactura.getVlr_utilidad();//new
              
              suma_baseiva=suma_baseiva+detallePrefactura.getVlr_base_iva();//new
              
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getPrefactura(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getFecha_prefactura(), letra  );              
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getId_orden(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getNumOs(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getId_accion(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getConsecutivo(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getFactura_conformada(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getNombre_cliente(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getSimbolo_variable(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_mat(), dinero  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_mob(), dinero  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_otr(), dinero  );
              xls.adicionarCelda(fila  , col++ , totalprev1, dinero  );
              
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_admnistracion(), dinero  );//new
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_imprevisto(), dinero  );//new
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_utilidad(), dinero  );//new
                            
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getBonificacion(), dinero  );
              xls.adicionarCelda(fila  , col++ ,subtotal1, dinero  );
              
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_base_iva(), dinero  );//new         
              
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_iva(), dinero  );
              xls.adicionarCelda(fila  , col++ ,subtotal2, dinero  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getPorcentaje_rmat(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_rmat(), dinero  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getPorcentaje_rmob(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_rmob(), dinero  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getPorcentaje_rotr(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_rotr(), dinero  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getVlr_retencion(), dinero  );
              
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getCuotas_reales(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getPorcentaje_factoring(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getValor_factoring(), dinero  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getPorcentaje_formula(), letra  );
              xls.adicionarCelda(fila  , col++ , detallePrefactura.getValor_formula(), dinero  );
              xls.adicionarCelda(fila  , col++ , vlrPago, dinero  );

        }

        fila++;fila++;
        int col = 9;
        xls.adicionarCelda(fila  , col++ ,suma_material, dinero  );
        xls.adicionarCelda(fila  , col++ ,suma_mo, dinero  );
        xls.adicionarCelda(fila  , col++ ,suma_otros, dinero  );
        xls.adicionarCelda(fila  , col++ ,suma_subtotalprev1, dinero  );
        
        xls.adicionarCelda(fila  , col++ ,suma_a, dinero  );//new
        xls.adicionarCelda(fila  , col++ ,suma_i, dinero  );//new
        xls.adicionarCelda(fila  , col++ ,suma_u, dinero  );//new
        
        xls.adicionarCelda(fila  , col++ ,suma_boni, dinero  );
        xls.adicionarCelda(fila  , col++ ,suma_subtotal1, dinero  );
        
        xls.adicionarCelda(fila  , col++ ,suma_baseiva, dinero  );
        
        xls.adicionarCelda(fila  , col++ ,suma_iva, dinero  );
        xls.adicionarCelda(fila  , col++ ,suma_valprefactura, dinero  );
        col++;
        xls.adicionarCelda(fila  , col++ ,suma_retmat, dinero  );
        col++;
        xls.adicionarCelda(fila  , col++ ,suma_retmo, dinero  );
        col++;
        xls.adicionarCelda(fila  , col++ ,suma_retotros, dinero  );
        xls.adicionarCelda(fila  , col++ ,suma_totret, dinero  );
        col++;
        
        col++;
        xls.adicionarCelda(fila  , col++ ,suma_factoring, dinero  );
        col++;
        xls.adicionarCelda(fila  , col++ ,suma_formula, dinero  );
        xls.adicionarCelda(fila  , col++ ,suma_tot, dinero  );
        
        this.cerrarArchivo();
    }catch (Exception ex){
        ex.printStackTrace();
        throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
    }
   }


    
    /**
     * Metodo para crear el  archivo de excel
     * @autor apabon
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            //xls.adicionarCelda(2,0, "PERIODO", titulo1);
            //xls.adicionarCelda(2,1, finicial + " - " + ffinal  , titulo1);
            xls.adicionarCelda(2,0, "USUARIO", titulo1);
            xls.adicionarCelda(2,1, usuario.getNombre() , titulo1);
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }    
    

    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor apabon
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            /*cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);*/
            
            
            // estilos
            
            header         = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo3        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo4        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo5        = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            

            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);

            
            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            ftofecha       = xls.nuevoEstilo("Tahoma", 8 , false , false, "yyyy-mm-dd" , xls.NONE , xls.NONE , xls.NONE);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
        
   }
    
    
    
    
    
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor apabon
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }    



}
