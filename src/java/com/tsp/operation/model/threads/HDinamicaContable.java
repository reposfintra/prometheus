/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.ParmetrosDinamicaTSP;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author egonzalez
 */
public class HDinamicaContable implements Runnable {

    private final ParmetrosDinamicaTSP parametros;
    PreparedStatement ps = null;
    Connection conn;
    private final String query;
    Usuario usuario;

    /**
     * Ejecuta un hilo de procesamiento para consolidar informacion en tiempo
     * real
     *
     * @param parametros
     * @param conn
     * @param query
     * @param usuario
     */
    public HDinamicaContable(ParmetrosDinamicaTSP parametros, Connection conn, String query, Usuario usuario) {
        this.parametros = parametros;
        this.conn = conn;
        this.query = query;
        this.usuario = usuario;
    }

    @Override
    public void run() {
        PreparedStatement prepareupdate = null;
        try {
            //actualiza procesos
            //prepareupdate = conn.prepareStatement(parametros.getUpdateLogProcesos());

            ps = conn.prepareStatement(parametros.getInsertLogProcesos());
            ps.setString(1, parametros.getNombreLogProceso());
            ps.setString(2, parametros.getNombreLogProceso());
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, this.hashCode());
            ps.executeUpdate();

            //Proceso principal
            ps = conn.prepareStatement(query);
            ps.setString(1, parametros.getFiltro_general());
            ps.setString(2, parametros.getPeriodo_inicio());
            ps.setString(3, parametros.getPeriodo_fin());
            ps.setString(4, parametros.getTipo_anticipo());
            ps.setString(5, parametros.getPaso());
            ResultSet executeQuery = ps.executeQuery();
            if (executeQuery.next()) {
                System.out.println("executeQuery: " + executeQuery.getString("tabla"));
                if (executeQuery.getString("tabla").equalsIgnoreCase("OK")) {
                    // prepareupdate.setString(1, "");

                }
            }

        } catch (SQLException ex) {
//            try {
//                prepareupdate.setString(1, ex.getMessage());
//
//            } catch (SQLException ex1) {
//                Logger.getLogger(HDinamicaContable.class.getName()).log(Level.SEVERE, null, ex1);
//            }
        } finally {
            try {
                //   prepareupdate.executeUpdate();
                conn.close();
                //    prepareupdate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                Logger.getLogger(HDinamicaContable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
