/*
 * Nombre        HExportarExcel_TiemposPC.java
 * Descripci�n   Genera el reporte de tiempos entre puestos de control en formato excel
 * Autor         Alejandro Payares
 * Fecha         14 de febrero de 2006, 10:07 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.ReporteExcelSOT;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Utility;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Iterator;

// EXCEL
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

/**
 * Genera el reporte de tiempos entre puestos de control en formato excel
 * @author Alejandro Payares
 */
public class HExportarExcel_TiemposPC extends ReporteExcelSOT {
    
    /**
     * El modelo de la aplicaci�n
     */
    private   Model       model;
    /**
     * El nombre de la unidad fisica donde ser� guardado el reporte.
     */
    private   String      unidad;
    /**
     * El nombre del archivo donde ser� guardado el reporte
     */
    private   String      fileName;
    /**
     * El usuario en sesi�n
     */
    private   Usuario     usuario;
    
    private String fechaIni;
    
    private String fechaFin;
    
    /**
     * Variable que indica la fila dentro de la hoja deexcel en donde se est� escribiendo la informaci�n
     */
    private int filaActual = -1;
    
    private String titulos [] = {"PLANILLA","RUTA","PUESTO DE CONTROL 1","CODIGO MIMS PC 1","FECHA Y HORA PC 1","PUESTO DE CONTROL 2","CODIGO MIMS PC 2","FECHA Y HORA PC 2","TIEMPO DEL TRAMO"};
    private String campos [] = {"planilla","ruta","puesto_de_control","codmims","hora_pc","puesto_de_control2","codmims2","hora_pc2","diferencia"};
    
    /**
     * Crea una nueva instancia de HExportarExcel_TiemposPC
     * @autor  Alejandro Payares
     */
    public HExportarExcel_TiemposPC(Model model, Usuario user, String fechaIni, String fechaFin) throws Exception{
        String hoy    = Utility.getDate(6).replaceAll("/","").replaceAll(":","_");
        this.fileName = "RepTPC_"+hoy;
        this.model       = model;
        this.usuario     = user;
        this.fechaIni = fechaIni;
        this.fechaFin = fechaFin;
        String dir       = user.getLogin();
        this.unidad      = model.DirectorioSvc.getUrl() + dir;
        //this.unidad = "c:";
        this.model.DirectorioSvc.create(dir);
        this.path        = unidad +"/"+ fileName + ".xls";
        initExcel(fileName);
    }
    
    /**
     * Es el encargado de iniciar el hijo que genera el reporte en el archivo excel
     */
    public void generar(){
        new Thread(this,fileName).start();
    }
    
    /**
     * Crea el encabezado del reporte
     * @throws Exception si alg�n error ocurre
     */
    private void crearEncabezado() throws Exception {
        int columna = 0;
        crearCelda(++filaActual, columna);
        HSSFCellStyle estilo = clonarEstilo(estiloBlancoGrande);
        quitarBordeAEstilo(estilo);
        cell.setCellStyle(estilo);
        cell.setCellValue("TRANSPORTES SANCHEZ POLO S.A");
        for( int i=1; i< 5; i++ ){
            crearCelda(filaActual, i);
        }
        crearCeldaCombinada(filaActual, columna, filaActual, 6, false);
        
        //this.prepared(++filaActual, 5);
        crearCelda(++filaActual, columna);
        estilo = clonarEstilo(estiloBlancoGrande);
        quitarBordeAEstilo(estilo);
        cell.setCellStyle(estilo);
        cell.setCellValue("REPORTE DE TIEMPOS ENTRE PUESTOS DE CONTROL");
        for( int i=1; i< 5; i++ ){
            crearCelda(filaActual, i);
        }
        crearCeldaCombinada(filaActual, columna, filaActual, 6, false);
        
        //this.prepared(++filaActual, 7);
        //this.prepared(filaActual+1, 7);
        
        // la fecha del reporte
        crearCelda(++filaActual, columna);
        cell.setCellStyle(this.estiloAzulGrande);
        cell.setCellValue("Fecha del reporte");
        crearCelda(filaActual+1, columna);
        cell.setCellStyle(this.estiloBlanco);
        cell.setCellValue(Utility.getDate(6));
        
        // la fecha de inicio de consulta
        crearCelda(filaActual, ++columna);
        cell.setCellStyle(this.estiloAzulGrande);
        cell.setCellValue("Fecha inicial");
        crearCelda(filaActual+1, columna);
        cell.setCellStyle(this.estiloBlanco);
        cell.setCellValue(fechaIni);
        
        // la fecha de inicio de consulta
        crearCelda(filaActual, ++columna);
        cell.setCellStyle(this.estiloAzulGrande);
        cell.setCellValue("Fecha final");
        crearCelda(filaActual+1, columna);
        cell.setCellStyle(this.estiloBlanco);
        cell.setCellValue(fechaFin);
        
        filaActual+=2;
    }
    
    /**
     * Crea los titulos del reporte
     * @throws Exception Si algun error ocurre
     */
    public void crearTitulos() throws Exception {
        filaActual++;
        int columna = -1;
        for(int i=0; i<titulos.length; i++ ){
            this.crearCelda(filaActual, ++columna);
            cell.setCellStyle(this.estiloAzul);
            cell.setCellValue(titulos[i]);
        }
    }
    
    /**
     * Metodo encargado de ejecutar la generaci�n del reporte.
     */
    public void run() {
        try {
            model.repTiemposPCService.buscarDatos(fechaIni, fechaFin);
            LinkedList datos = model.repTiemposPCService.obtenerDatos();
            this.crearEncabezado();
            //this.prepared(++filaActual, 1);
            this.crearCelda(++filaActual, 0);
            cell.setCellStyle(this.estiloNormalGrande);
            cell.setCellValue(datos.size()+" Registros encontrados");
            crearTitulos();
            Iterator devolIt = datos.iterator();
            while( devolIt.hasNext() ) {
                Hashtable fila = (Hashtable) devolIt.next();
                filaActual++;
                int columna = -1;
                for( int i = 0; i < campos.length; i++ ) {
                    String str = ((String)fila.get(campos[i]));
                    str = str == null? "":str;
                    str = str.equals("0099-01-01")?"":str;
                    crearCelda(filaActual, ++columna);
                    cell.setCellStyle(this.estiloBlanco);
                    cell.setCellValue(str.equals("")?"-":str);
                    
                }
            }
            model.repTiemposPCService.borrarDatos();
            save();
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
    }
    
}
