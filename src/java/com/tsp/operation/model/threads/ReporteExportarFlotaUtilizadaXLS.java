/***************************************************************************
 * Nombre:            ReporteExportarFlotaUtilizadaXLS.java                *
 * Descripci�n:       Hilo para crear el reporte de las flotas utilizadas  * 
 * Autor:             Ing. Henry A. Osorio Gonzalez                        *
 * Fecha:             9 de diciembre de 2005, 09:04 AM                     *
 * Versi�n:           Java 1.0                                             *           
 * Copyright:         Fintravalores S.A. S.A.                         *
 ***************************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class ReporteExportarFlotaUtilizadaXLS extends Thread{
    private String procesoName;
    private String des;
    String id = "";    
    Vector vecFlotas = null;    
    Vector resumen = null;
    String feci = "";
    String fecf = "";
    /** Creates a new instance of ReporteFacturasCreadasXLS */
    public ReporteExportarFlotaUtilizadaXLS() {
    }
    
    public void start(String id, Vector flotas, Vector resum, String feci, String fecf) {
        ////System.out.println("entro por hilo");
        this.id = id;
        this.procesoName = "Flota Utilizada";
        this.des = "Los vehiculos que han sido despahcados o en espera";
        this.vecFlotas = flotas;        
        this.resumen   = resum;
        this.feci      = feci;
        this.fecf      = fecf;           
        super.start();
    }
    
    public synchronized void run(){
        try{
            /** Cargamos el detalle por agencia en diferentes vectores**/            
            Model model = new Model();
            Vector vecDetalle = new Vector();
            String agencia = "";
            for (int i=0; i<resumen.size(); i++) {
                agencia = ((ResumenFlota) resumen.elementAt(i)).getAgencia();
                Vector vecAgencias = new Vector();
                for (int j=0; j<vecFlotas.size(); j++ ){
                    FlotaUtilizada fl = (FlotaUtilizada) vecFlotas.elementAt(j);
                    if(agencia.equals(fl.getOri_dest())) {
                        vecAgencias.addElement(fl);
                    }
                }       
                vecDetalle.addElement(vecAgencias);
            }               
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);                        
            String fecha_actual = Util.getFechaActual_String(6);
            if (vecFlotas!=null) {                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path =  rb.getString("ruta") + "/exportar/migracion/"+id;
                
                File file   = new File(path);
                file.mkdirs();
                
                com.tsp.operation.model.threads.POIWrite xls = new com.tsp.operation.model.threads.POIWrite(path+"/FLOTA_UTILIZADA["+feci+"]["+fecf+"].xls","","");
                // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
                HSSFCellStyle fecha       = xls.nuevoEstilo("verdana", 12, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                HSSFCellStyle texto       = xls.nuevoEstilo("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero      = xls.nuevoEstilo("verdana", 12, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle negrita     = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE);
                HSSFCellStyle header      = xls.nuevoEstilo("verdana", 18, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle fechatitle  = xls.nuevoEstilo("verdana", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );                
                
                xls.obtenerHoja("RESUMEN");
                xls.cambiarMagnificacion(3,4);                                
                // cabecera
                
                xls.adicionarCelda(0,0, "RESUMEN FLOTA  UTILIZADA", header);
                xls.combinarCeldas(0, 0, 0, 15);
                
                // subtitulos

                xls.adicionarCelda(3,0, "Periodo Ejecutado: "   , negrita);
                xls.adicionarCelda(3,1, feci+"-"+fecf , fecha);
                xls.adicionarCelda(4,0, "Elaborado Por: "   , negrita);                
                xls.adicionarCelda(4,1, id , texto);                                
                
                int fila = 6;
                int col  = 0;
                
                xls.adicionarCelda(fila ,col++ , "Agencia"                         , titulo );
                xls.adicionarCelda(fila ,col++ , "Veh. Disponibles"                              , titulo );
                xls.adicionarCelda(fila ,col++ , "Veh. Utilizados"                                  , titulo );
                xls.adicionarCelda(fila ,col++ , "Remesas Despachadas"                         , titulo );                
                xls.adicionarCelda(fila ,col++ , "% Utilidad Flota"                         , titulo );                
                xls.adicionarCelda(fila ,col++ , "% Retorno"                         , titulo );                                
                
                // datos
                for (int j=0; j<10; j++) {
                     xls.cambiarAnchoColumna(j,5600);
                }                
                for (int i=0; i<resumen.size(); i++) {
                    fila++;
                    col = 0;                    
                    ResumenFlota r = (ResumenFlota) resumen.elementAt(i);                     
                    xls.adicionarCelda(fila ,col++ , r.getAgencia()            , texto );
                    xls.adicionarCelda(fila ,col++ , r.getVehDisponibles()                , numero );
                    xls.adicionarCelda(fila ,col++ , r.getVehUtilizados()                , numero );
                    xls.adicionarCelda(fila ,col++ , r.getTotal()              , numero );
                    xls.adicionarCelda(fila ,col++ , r.getPerUtilidad()              , numero );
                    xls.adicionarCelda(fila ,col++ , r.getPerRetorno()            , numero );                    
                }
                /** Generacion de hojas por Agencia **/
                
                for (int k=0; k<vecDetalle.size(); k++) {                   
                    Vector ag = (Vector) vecDetalle.elementAt(k);                    
                    FlotaUtilizada fl = (FlotaUtilizada) ag.elementAt(0); 
                    
                    xls.obtenerHoja(fl.getOri_dest());
                    xls.cambiarMagnificacion(3,4);                                
                    // cabecera

                    xls.adicionarCelda(0,0, "DETALLE FLOTA UTILIZADA "+fl.getOri_dest().toUpperCase(), header);
                    xls.combinarCeldas(0, 0, 0, 15);

                    // subtitulos

                    xls.adicionarCelda(3,0, "Fecha Proceso: "   , negrita);
                    xls.adicionarCelda(3,1, fecha_actual , fecha);
                    xls.adicionarCelda(4,0, "Elaborado Por: "   , negrita);                
                    xls.adicionarCelda(4,1, id , texto);                                

                    fila = 6;
                    col  = 0;

                    xls.adicionarCelda(fila ,col++ , "Agencia"                         , titulo );
                    xls.adicionarCelda(fila ,col++ , "Placa"                              , titulo );
                    xls.adicionarCelda(fila ,col++ , "Planilla Llegada"                                  , titulo );
                    xls.adicionarCelda(fila ,col++ , "Cliente Llegada"                                  , titulo );
                    xls.adicionarCelda(fila ,col++ , "Origen"                                  , titulo );
                    xls.adicionarCelda(fila ,col++ , "Planilla Salida"                         , titulo );
                    xls.adicionarCelda(fila ,col++ , "Cliente Salida"                                  , titulo );
                    xls.adicionarCelda(fila ,col++ , "Destino"                                  , titulo );
                    xls.adicionarCelda(fila ,col++ , "Fecha llegada"                           , titulo );
                    xls.adicionarCelda(fila ,col++ , "Fecha Salida"                     , titulo );                

                    // datos
                    for (int j=0; j<6; j++) {
                         xls.cambiarAnchoColumna(j,5600);
                    }                
                    for (int i=0; i<ag.size(); i++) {                        
                        fila++;
                        col = 0;                    
                        fl = (FlotaUtilizada) ag.elementAt(i); 
                        xls.adicionarCelda(fila ,col++ , fl.getOri_dest()            , texto );
                        xls.adicionarCelda(fila ,col++ , fl.getPlaca()                , texto );
                        xls.adicionarCelda(fila ,col++ , fl.getNumpla()                , texto );
                        xls.adicionarCelda(fila ,col++ , fl.getCliente()                , texto );
                        xls.adicionarCelda(fila ,col++ , fl.getOrigen()                , texto );
                        xls.adicionarCelda(fila ,col++ , fl.getPlanilla_Salida()              , texto );
                        xls.adicionarCelda(fila ,col++ , fl.getClienteSalida()                , texto );
                        xls.adicionarCelda(fila ,col++ , fl.getDestino()                , texto );
                        xls.adicionarCelda(fila ,col++ , fl.getFecha()              , texto );
                        xls.adicionarCelda(fila ,col++ , fl.getFecha_salida()            , texto );                    
                    }  
                   
                }
                System.gc();
                xls.cerrarLibro();                                
            }
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            e.printStackTrace();
            try{
                Model model = new Model();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                f.printStackTrace();
                try{
                    Model model = new Model();
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){  
                    p.printStackTrace();
                }
            }
        }
    }   
}
