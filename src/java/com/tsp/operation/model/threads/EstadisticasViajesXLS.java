/*****************************************************************************
 * Nombre clase :                   EstadisticasViajesXLS.jav                *
 * Descripcion :                    Hilo que permite la actualizacion de la  *
 *                                  tabla de record_viajes                   *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          11 de enero de 2006, 09:54 AM            *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 ****************************************************************************/
package com.tsp.operation.model.threads;
import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;

public class EstadisticasViajesXLS extends Thread{
    
    /** Creates a new instance of EstadisticasViajesXLS */
    public EstadisticasViajesXLS() {
    }
    
    public void start() {      
        super.start();
    }
    
    public synchronized void run(){
        try{
            Model model = new Model();
            model.rdSvc.Actualizar_record();
            
        }catch(Exception e){
            ////System.out.println(e.getMessage());
        }finally{
            super.destroy();
        }
    }
}
