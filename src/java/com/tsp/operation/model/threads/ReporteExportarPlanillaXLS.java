/*
 * ReporteExportarPlanillaXLS.java
 *
 * Created on 7 de octubre de 2005, 10:53 AM
 */

package com.tsp.operation.model.threads;


import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;
/**
 *
 * @author  Jose
 */
public class ReporteExportarPlanillaXLS extends Thread{
    private String procesoName;
    private String des;
    String id="";
    /** Creates a new instance of ReporteExportarPlanillaXLS */
    public ReporteExportarPlanillaXLS() {
    }
    
    public void start(String id){
        ////System.out.println("entro por hilo");
        this.id= id;
        this.procesoName = "Planillas sin reporte en trafico";
        this.des = "Planillas que no presentan ningun reporte en movivmiento de trafico";
        super.start();
    }
    
    public synchronized void run(){
        try{
            Model model = new Model();
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            List    lista = (List)    model.planillaService.reportePlanillasXLS();
            String fecha_actual = Util.getFechaActual_String(6);
            if (lista!=null && lista.size()>0){
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                String a�o = fecha_actual.substring(0,4);
                String mes = fecha_actual.substring(5,7);
                String dia = fecha_actual.substring(8,10);
                String hora = fecha_actual.substring(11,13);
                String min = fecha_actual.substring(14,16);
                String seg = fecha_actual.substring(17,19);
                File file = new File(path);
                file.mkdirs();
                
                POIWrite xls = new POIWrite(path+"/PLNREP"+a�o+mes+dia+" "+hora+min+seg+".xls","","");
                // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
                HSSFCellStyle fecha  = xls.nuevoEstilo("verdana", 12, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                HSSFCellStyle texto  = xls.nuevoEstilo("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero = xls.nuevoEstilo("verdana", 12, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle negrita      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE);
                HSSFCellStyle header      = xls.nuevoEstilo("verdana", 18, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle titulo      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle fechatitle  = xls.nuevoEstilo("verdana", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja("BASE");
                xls.cambiarMagnificacion(3,4);
                
                // cabecera
                
                xls.adicionarCelda(0,0, "PLANILLA SIN REPORTE EN TRAFICO", header);
                xls.combinarCeldas(0, 0, 0, 7);
                
                // subtitulos
                
                xls.adicionarCelda(2,0, "Fecha Proceso: "   , negrita);
                xls.adicionarCelda(2,1, fecha_actual , fecha);
                xls.adicionarCelda(3,0, "Elaborado Por: "   , negrita);
                xls.adicionarCelda(3,1, id , fecha);
                
                
                int fila = 5;
                int col  = 0;
                
                xls.adicionarCelda(fila ,col++ , "Numero Planilla"                      , titulo );
                xls.adicionarCelda(fila ,col++ , "Fecha Planilla"                       , titulo );
                xls.adicionarCelda(fila ,col++ , "Agencia Despacho"                     , titulo );
                xls.adicionarCelda(fila ,col++ , "Placa"                                , titulo );
                xls.adicionarCelda(fila ,col++ , "Conductor"                            , titulo );
                xls.adicionarCelda(fila ,col++ , "Nombre Origen"                        , titulo );
                xls.adicionarCelda(fila ,col++ , "Nombre Destino"                       , titulo );
                xls.adicionarCelda(fila ,col++ , "Dias Entre Fecha Reporte y Despacho"  , titulo );
                
                // datos
                
                Iterator it = lista.iterator();
                
                while(it.hasNext()){
                    
                    fila++;
                    col = 0;
                    
                    Planilla planilla = (Planilla) it.next();
                    
                    //COMBIERTE LA DIFERENCIA DE LAS FECHAS EN DIAS
                    String vec[] = planilla.getUltimoreporte().split("day");
                    String num_dias = "0";
                    if(vec.length>0){
                        if(vec[0].indexOf(":")<0)
                            num_dias = vec[0];
                    }
                    String vec2[] = num_dias.split(" ");
                    if(vec2.length>0){
                        num_dias = vec2[0];
                    }
                    int num = Integer.parseInt(num_dias);
                    
                    xls.adicionarCelda(fila ,col++ , planilla.getNumpla()           , texto );
                    xls.adicionarCelda(fila ,col++ , planilla.getFecdsp()           , texto );
                    xls.adicionarCelda(fila ,col++ , planilla.getAgcpla()           , texto );
                    xls.adicionarCelda(fila ,col++ , planilla.getPlaveh()           , texto );
                    xls.adicionarCelda(fila ,col++ , planilla.getNomCond()          , texto );
                    xls.adicionarCelda(fila ,col++ , planilla.getOripla()           , texto );
                    xls.adicionarCelda(fila ,col++ , planilla.getDespla()           , texto );
                    xls.adicionarCelda(fila ,col++ , num                            , numero );
                }
                
                xls.cerrarLibro();
            }
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                Model model = new Model();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                try{
                    Model model = new Model();
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
