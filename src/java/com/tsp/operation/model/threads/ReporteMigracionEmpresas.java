/*************************************************************
 * Nombre: ReporteMigracionCumplidos.java
 * Descripci�n: Hilo para crear el ReporteMigracionEmpresas
 * Autor: Ing. Jose de la rosa
 * Fecha: 24 de octubre de 2005, 11:39 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author  fily
 */
public class ReporteMigracionEmpresas extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String fechaI = "";
    String fechaF = "";
    private PrintWriter pw;
    private LogWriter   logTrans;
    private String path;
    public ReporteMigracionEmpresas () {
    }
    
    public void start (String id, String FechaI,String FechaF){
        this.id = id;
        this.fechaI = FechaI;
        this.fechaF = FechaF;
        this.procesoName = "Empresas Migrado";
        this.des = "Migracion de Empresas por fechas";
        super.start ();
    }
    
    public synchronized void run (){
        try{
            
            Model model = new Model ();
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            path = ruta + "/exportar/migracion/" + id;

            File archivo = new File( path );
            if (!archivo.exists()) archivo.mkdirs();
            initLog();
            
            model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, this.id);
            model.transitoService.BusquedaEmpresas(fechaI, fechaF);
            Vector lista = (Vector) model.transitoService.getVector();
            Date hoy = new Date ();
            SimpleDateFormat s = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
            String fecha_hoy = s.format (hoy);
            String a�o = fecha_hoy.substring(0,4);
            String mes = fecha_hoy.substring(5,7);
            String dia = fecha_hoy.substring(8,10);
            if (lista!=null && lista.size ()>0){
                
                ResourceBundle rbT = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
                String pathT = rbT.getString ("ruta");
                File fileT = new File (pathT + "/exportar/migracion/" + id + "/");
                fileT.mkdirs ();
                String NombreArchivo = pathT + "/exportar/migracion/" + id + "/890103161"+mes+a�o+"EMPRESAS"+".txt";
                PrintStream archivoT = new PrintStream (NombreArchivo);
                
                
                Iterator it = lista.iterator ();
                String datos1;
                String datos2;
                String datos3;
                String datos4;
                String datos5;
                String datos6;
                String datos7;
                        
                while(it.hasNext ()){
                    BeanGeneral Bean = (BeanGeneral) it.next ();
                    if(Bean.getValor_01().equals("")){
                        logTrans.log("La empresa de Numero = "+Bean.getValor_02()+" no tiene datos en el campo Tipo de identificacion ", logTrans.INFO);
                    }
                    datos1= this.TruncarDatos( Bean.getValor_01() , 1);
                   
                    if(Bean.getValor_02().equals("")){
                        logTrans.log("La empresa de nombre = "+Bean.getValor_03()+"no tiene un Numero de Identificacion ", logTrans.INFO);
                    }
                    datos2= this.TruncarDatos( Bean.getValor_02() , 11);
                    
                    if(Bean.getValor_03().equals("")){
                        logTrans.log("La empresa de Numero = "+Bean.getValor_02()+" no tiene datos en el campo Nombre ", logTrans.INFO);
                    }
                    datos3= this.TruncarDatos( Bean.getValor_03() , 60);
                    
                    if(Bean.getValor_04().equals("")){
                        logTrans.log("La empresa de Numero = "+Bean.getValor_02()+" no tiene datos en el campo Telefono ", logTrans.INFO);
                    }
                    datos4= this.TruncarDatos( Bean.getValor_04() , 10);
                    
                   if(Bean.getValor_05().equals("")){
                        logTrans.log("La empresa de Numero = "+Bean.getValor_02()+" no tiene datos en el campo Direccion ", logTrans.INFO);
                    }
                    datos5= this.TruncarDatos( Bean.getValor_05() , 40);
                    
                    if(Bean.getValor_06().equals("")){
                        logTrans.log("La empresa de Numero = "+Bean.getValor_02()+" no tiene datos en el campo Codigo de la Ciudad Segun el Dane ", logTrans.INFO);
                    }
                    datos6= this.TruncarDatos( Bean.getValor_06() , 8);
                    
                    if(Bean.getValor_07().equals("")){
                        logTrans.log("La empresa de Numero = "+Bean.getValor_02()+" no tiene datos en el campo Codigo Otorgado por el Mnisterio de Trasporte ", logTrans.INFO);
                    }
                    datos7= this.TruncarDatos( Bean.getValor_07() , 4);
                    
                   archivoT.println (datos1 +" \t "+datos2+" \t "+datos3+" \t "+ datos4+ " \t "+datos5 +
                                     " \t "+ datos6+  " \t "+datos7);
                 
                }
                closeLog();
            }
            
            model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                Model model = new Model ();
                model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (),this.id,"ERROR :" + e.getMessage ());
            }
            catch(Exception f){
                try{
                    Model model = new Model ();
                    model.LogProcesosSvc.finallyProceso (this.procesoName,this.hashCode (),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
    /*funciones para llenar */
    public String agregarEspacios(String valor, int longitud ) throws Exception {
       int tama�oValor = valor.length();
       int tama�o = longitud - tama�oValor;
       String vacios = "";
       String relleno = "";
        for(int i= 0;i< tama�o; i++){
            vacios = vacios+" "; 
        }
        relleno =  valor + vacios ;
        
        return relleno;
       
    }  
    
    public String TruncarDatos(String valor, int longitud ) throws Exception {
       String valorFinal = "";
       int tama�oValor = valor.length();
       String valorTruncado = "";
       if(tama�oValor > longitud ){
          valorTruncado = valor.substring(0, longitud);
       }else{
           valorTruncado = valor;
       }
       valorFinal = this.agregarEspacios(valorTruncado, longitud);
       return valorFinal;
       
    }  
    
    public void initLog()throws Exception{
        
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");
        pw     = new PrintWriter(new BufferedWriter(new FileWriter(path + "/logMigracionEmpresas_.txt")));
        logTrans = new LogWriter("Empresas_migrqaciones", LogWriter.INFO , pw );
        logTrans.log( "Proceso Inicializado", logTrans.INFO );
    }
    
    public void closeLog() throws Exception{
        logTrans.log( "Proceso Finalizado", logTrans.INFO );
        pw.close();
    }
    
}
