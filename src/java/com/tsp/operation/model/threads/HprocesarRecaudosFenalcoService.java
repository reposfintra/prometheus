    /***************************************
    * Nombre Clase ............. hnegocios.java
    * Descripci�n  .. . . . . .  contabiliza negocios
    * Autor  . . . . . . . . . . ING ROBERTO ROCHA P
    * Fecha . . . . . . . . . .  10/09/2007
    * versi�n . . . . . . . . .  1.0
    * Copyright ...FINTRAVALORES SA.
    *******************************************/


package com.tsp.operation.model.threads;

import java.io.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.util.*;
import java.lang.*;
import java.text.*;


public class HprocesarRecaudosFenalcoService extends Thread{
    
    private Model     model;
    private String    procesoName;
    private Usuario   user;
    private String    fechaCorte;
    private int       opcion;

    PrintWriter      pw  = null;
    FileWriter       fw  = null;

    

    public void initLOG () throws Exception{
        try{
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + user.getLogin());
            fw = new FileWriter (ruta + "/PRF_"+ getCurrentTime("yyyyMMdd_kkmmss") +".txt");
            pw = new PrintWriter(new BufferedWriter(fw));            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
  public void writeLOG(String msg)throws Exception {
        try{
            if (pw!=null ){
                System.out.println(msg);
                String []msgs = msg.split("\n");
                for (int i = 0; i< msgs.length;i++)
                    pw.println("[" + getCurrentTime("yyyy-MM-dd kk:mm:ss") + "] " +  msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    
    public String getCurrentTime(String dateformat){
        Date fechaActual = new Date();    
        SimpleDateFormat fmt=new SimpleDateFormat(dateformat);
        fechaActual.setTime( System.currentTimeMillis());
        return fmt.format(fechaActual );
    }
    
    
    public HprocesarRecaudosFenalcoService() {
    }
    

    
    public void start(Model modelo, Usuario usuario, int opcion) throws Exception{
         try{
                this.model       = modelo;
                this.user        = usuario;
                this.procesoName = "Procesar Recaudos Fenalco";
                this.opcion      = opcion;
                super.start();
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    public void closeLOG() throws Exception {
        try{
            pw.close();
            fw.close();
        } catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public synchronized void run(){
       LogProcesosService log = new LogProcesosService(this.user.getBd());
       try{ 
           initLOG();
           writeLOG("INICIO PROCESO DE APLICACION DE PAGOS FENALCO.");
           String comentario="EXITOSO";
           log.InsertProceso(this.procesoName, this.hashCode(), "",this.user.getLogin() ); 
           if(opcion==1)
           {    model.procesarRecaudosFenalcoService.ProcesarRecaudos(user.getLogin());
           }
           else{
               model.procesarRecaudosFenalcoService.ProcesarNotas(user.getLogin(),opcion);
           }
           //comentario    =  model.procesarRecaudosFenalcoService.getErrores();
           model.procesarRecaudosFenalcoService.setProcess( false );           
           writeLOG(model.procesarRecaudosFenalcoService.getErrores());
           log.finallyProceso(this.procesoName, this.hashCode(), this.user.getLogin(),comentario);  
           closeLOG();
        }catch(Exception e){
           try{  
               e.printStackTrace();
               System.out.println("E:"+e.getMessage());
               //writeLOG(e.getMessage());
               model.procesarRecaudosFenalcoService.setProcess( false );
               log.finallyProceso(this.procesoName,this.hashCode(), this.user.getLogin(),"ERROR Hilo: " + e.getMessage()); 
               closeLOG();
           }
           catch(Exception f){
                f.printStackTrace();
                System.out.println("F"+f.getMessage());
           }   
       }
    }
    
    
}
