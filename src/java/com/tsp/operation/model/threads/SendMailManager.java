/*
 * ReporteBalancePrueba.java
 *
 * Created on 20 de octubre de 2005, 05:07 PM
 */

package com.tsp.operation.model.threads;

import java.io.*;
import java.util.*;

import com.tsp.exceptions.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.pdf.*;
import java.util.*;
import org.dom4j.Element;
import javax.mail.*;
import com.tsp.operation.model.Model;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.util.*;
/**
 *
 * @author  JuanM
 */
public class SendMailManager extends Thread{
    private String to;
    private String from;
    private String smtp;
    private String file;
    private String estado;
    private String usuario;
    private List listaD;
    private String procesoName;
    private String des;
    private com.tsp.operation.model.Model model;
    /** Creates a new instance of MigracionCambioCheque */
    public SendMailManager() {
    }
    
    public void start(String id, List lista ){
        this.to = "";
        this.from = "";
        this.file = "";
        this.smtp = "";
        this.file = "";
        this.estado = "";
        this.usuario = id;
        this.listaD = lista;
        this.procesoName = "Send Mail";
        this.des = "Proceso de envio email de discrepancia generada por rechazo";
        super.start();
    }
    
    public synchronized void run(){
        try{
            /*Archivo excel*/
            model = new Model();
            Util util   = new Util();
            
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(),this.des, this.usuario );
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            
            File archivo = new File(path + "/exportar/migracion/"+usuario);
            archivo.mkdirs();
            
            String nombreArch= "ReporteNovedades.xls";
            String       Hoja  = "ReporteNovedades";
            String       Ruta  = path + "/exportar/migracion/"+ usuario +"/" + nombreArch;
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFRow      row   = null;
            HSSFCell     cell  = null;
            
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(8)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente3);
            estilo1.setFillForegroundColor(HSSFColor.BLACK.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.YELLOW.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(8)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            int Fila = 1;
            
            row  = sheet.createRow((short)(Fila));
            
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA NOVEDAD");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CLIENTE");
            
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("N� FACTURA");
            
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("REFERENCIA");
            
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CANT");
            
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("MOTIVO");
            
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OBSERVACIONES");
            
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CSR");
            
            
            
            /*Fin Archivo excel*/
            
            Iterator it = listaD.iterator();
            while( it.hasNext() ){
                Fila++;
                Discrepancia obj = (Discrepancia)it.next();
                List destinatarios = model.tablaGenService.obtenerInfoTablaGen("EMAIL", obj.getGrupo());
                
                Iterator aux = destinatarios.iterator();
                //Destinatarios
                while(aux.hasNext()){
                    TablaGen tobj = (TablaGen)aux.next();
                    if( tobj.getDescripcion().indexOf("@") != -1){
                        to += tobj.getDescripcion()+";";
                    }
                    else{
                        model.usuarioService.searchUsuario( tobj.getDescripcion().toUpperCase() );
                        Usuario u = model.usuarioService.getUsuario();
                        if(!u.getEmail().equals(""))
                            to += u.getEmail()+";";
                    }
                }//Fin destinatarios
                
                row  = sheet.createRow((short)(Fila));
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue(util.getFechaActual_String(6)); //Fecha Novedad
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue(obj.getContacto()); //Cliente
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo4);
                cell.setCellValue(obj.getDocumento()); //Factura
                
                String cod_prod   = obj.getCod_producto();
                String referencia = "";
                if( obj.getCausa_dev().equals("35"))
                    referencia = cod_prod;
                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo4);
                cell.setCellValue(referencia); //Referencia
                
                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo4);
                cell.setCellValue(obj.getCantidad()); //Cantidad
                
                
                String causa = model.causadiscreService.descripcionCausa(obj.getCausa_dev());
                cell = row.createCell((short)(5));
                cell.setCellStyle(estilo4);
                cell.setCellValue(causa); //Motivo
                
                cell = row.createCell((short)(6));
                cell.setCellStyle(estilo4);
                cell.setCellValue(obj.getObservacion()); //Observaciones
                
                cell = row.createCell((short)(7));
                cell.setCellStyle(estilo4);
                cell.setCellValue(obj.getGrupo()); //CSR
            }
            
            FileOutputStream fo = new FileOutputStream(Ruta);
            wb.write(fo);
            fo.close();
            
            /*Enviar correo*/
            Sendfile email = new Sendfile();
            String arg [] = new String [7];
            arg[0] = to;
            arg[1] = "procesos";
            arg[2] = "mail.tsp.com";
            arg[3] = Ruta;
            arg[4] = "true";
            arg[5] = "DISCREPANCIA GENERADA POR RECHAZO\n";
            arg[6] = "DISCREPANCIA GENERADA POR RECHAZO";
            if( to.length() > 0 )
                email.main(arg);
            /*Fin enviar correo*/
            
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,"Proceso Exitoso");
            
        }catch(Exception e){
            e.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,"ERROR HILO : " + e.getMessage());
            }catch(Exception ex){}
        }
    }
    
    public static void main(String a []){
        SendMailManager hilo = new SendMailManager();
        // hilo.start("jescandon@mail.tsp.com", "jescandon@mail.tsp.com", "mail.tsp.com", "c:/ReportePC.xls", "true", "HOSORIO");
    }
    
}
