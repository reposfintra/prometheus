/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.threads;

import com.tsp.finanzas.contab.model.services.ContabilizacionIngresosServices;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Ingreso;
import com.tsp.operation.model.beans.Ingreso_detalle;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.LogWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.Vector;

/**
 *
 * @author geotech
 */
/**
 *
 * @author Ing. Iris Vargas
 */
public class HDevoluciones extends Thread {

    Model model;
    private Usuario usuario;
    private PrintWriter pw;
    private LogWriter logWriter;
    java.text.SimpleDateFormat anomes;
    java.text.SimpleDateFormat fecha;
    java.text.SimpleDateFormat fechaHora;
    String periodo;
    String fechadoc;
    String fechaInicio;
    java.util.Date pdate;
    java.util.Date ahora;
    String[] ingresos;

    public HDevoluciones() {
    }

    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start(Model model, String[] ingresos, Usuario usuario) throws Exception {
        /*
         *Busco la fecha actual y le resto un dia.
         */
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -1);
       this.ingresos= ingresos  ;
        this.usuario=usuario ;
         this.model=model ;

        pdate = hoy.getTime();
        ahora = new java.util.Date();

        anomes = new java.text.SimpleDateFormat("yyyyMM");
        fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");

        periodo = anomes.format(pdate);
        fechadoc = fecha.format(pdate);
        fechaInicio = fechaHora.format(ahora);

        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();

        File file = new File(ruta);
        file.mkdirs();

        pw = new PrintWriter(System.err, true);

        String logFile = ruta + "/Devoluciones" + fechadoc + ".txt";

        pw = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("Devoluciones", LogWriter.INFO, pw);
        logWriter.setPrintWriter(pw);

        super.start();
    }

    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run() {

        try {

            logWriter.log("**********************************************************", LogWriter.INFO);
            logWriter.log("*            PROCESO DE DEVOLUCION DE CHEQUES            *", LogWriter.INFO);
            logWriter.log("**********************************************************", LogWriter.INFO);


            model.LogProcesosSvc.InsertProceso("proceso de devoluciones", this.hashCode(), "proceso de devolucion de cheques", usuario.getLogin());


            for (int k = 0; k < ingresos.length; k++) {
                String[] vecIngreso = ingresos[k].split(";");
                if (vecIngreso.length == 3) {


                    Ingreso ingreso = new Ingreso();
                    ingreso.setDstrct(vecIngreso[0]);
                    ingreso.setTipo_documento(vecIngreso[1]);
                    ingreso.setNum_ingreso(vecIngreso[2]);
                    logWriter.log("Procesando Ingreso numero "+ingreso.getNum_ingreso(), LogWriter.INFO);
                    TransaccionService scv = new TransaccionService(usuario.getBd());
                    scv.crearStatement();

                    ingreso = model.Negociossvc.getIngreso(ingreso);
                    ContabilizacionIngresosServices contabilizacionIngresosServices = new ContabilizacionIngresosServices(usuario.getBd());

                    Vector detalles_ingresito = contabilizacionIngresosServices.obtenerDetalleIngresoNuevo(ingreso);//se obtienen los detalles del ingreso
                    String num_ingreso = model.ingresoService.buscarSerie("ICA" + "C");//se consigue el num_ingreso de la nueva IA
                    ingreso.setNum_ingreso(num_ingreso);//se cambia el num_ingreso del ingreso por el de la nueva IA
                    logWriter.log("Creando nota de ajuste "+ingreso.getNum_ingreso(), LogWriter.INFO);
                    ingreso.setAuxiliar("RD-" + ingreso.getNitcli());
                    ingreso.setTipo_documento("ICA");
                    ingreso.setCreation_user(usuario.getLogin());//setCreation_user
                    ingreso.setUser_update(usuario.getLogin());//setUser_update
                    double positivos = 0;
                    for (int j = 0; j < detalles_ingresito.size(); j++) {
                        Ingreso_detalle ingreso_det = (Ingreso_detalle) detalles_ingresito.get(j);

                        if (ingreso_det.getFactura() != null && !ingreso_det.getFactura().equals("")) {
                            positivos += ingreso_det.getValor_ingreso();
                        } 
                    }
                    double valor_ingreso=ingreso.getVlr_ingreso();
                    ingreso.setVlr_ingreso(positivos);
                    ingreso.setVlr_ingreso_me(positivos);
                    scv.getSt().addBatch(model.ingresoService.insertarIngreso(ingreso));//se inserta la nueva nota de ajuste

                    for (int j = 0; j < detalles_ingresito.size(); j++) {//para cada detalle del ingreso

                        Ingreso_detalle ingreso_det = (Ingreso_detalle) detalles_ingresito.get(j);//se tiene 1 detalle del ingreso
    
                        String cuenta_banco_ingreso = ingreso.getCuenta_banco();//se consulta la cuenta del banco del ingreso que sera la del detalle de la nueva nota de ajuste                 
                       
                        ingreso_det.setNumero_ingreso(num_ingreso);//se cambia el num_ingreso del detalle del ingreso por el del detalle de la nueva IA
                        ingreso_det.setTipo_documento("ICA");
                        ingreso_det.setCreation_user(usuario.getLogin());
                        ingreso_det.setUser_update(usuario.getLogin());
                       
                         if (ingreso_det.getFactura() != null && !ingreso_det.getFactura().equals("")) {
                            ingreso_det.setCuenta(cuenta_banco_ingreso);////se cambia la cuenta del detalle del ingreso y se pone la cuenta del detalle de la nueva IA}
                            String documento="";
                           /* if(ingreso_det.getFactura().split("-").length>1){
                                int num=Integer.parseInt(ingreso_det.getFactura().split("-")[1])+1;
                                documento=ingreso_det.getFactura()+"-"+num;
                            }else{
                                documento=ingreso_det.getFactura()+"-1";
                            }*/
                         try{documento=model.Negociossvc.UpCP("NOTASD");}catch(Exception e){throw new SQLException();}
                            ingreso_det.setValor_ingreso(valor_ingreso);
                            ingreso_det.setValor_ingreso_me(valor_ingreso);
                            ingreso_det.setValor_saldo_factura(valor_ingreso);
                            logWriter.log("Creando factura "+documento, LogWriter.INFO);
                            ArrayList<String> listSql = model.Negociossvc.newfactRemix(ingreso_det.getFactura(), documento);//se crea la nd
                            for (String sql : listSql) {
                                scv.getSt().addBatch(sql);
                            }
                            scv.getSt().addBatch(model.Negociossvc.updateFacturaDevuelta(ingreso_det.getFactura(), usuario.getDstrct(), usuario.getLogin()));// se marca la factura como devuelta.
                        } else {
                            ingreso_det.setValor_ingreso(ingreso_det.getValor_ingreso() * -1);
                            ingreso_det.setValor_ingreso_me(ingreso_det.getValor_ingreso_me() * -1);
                        }
                         scv.getSt().addBatch(model.ingreso_detalleService.insertarIngresoDetalle(ingreso_det));//se mete el detalle de la nueva IA

                    }

                    try {

                        scv.execute();
                    } catch (Exception e) {
                        System.out.println("error en creacion de nd" + e.toString() + "_" + e.getMessage());
                        e.printStackTrace();
                        throw new Exception("Error Durante la Insercion de las Notas o creacion de nd" + e.getMessage());
                    }

                } else {
                    logWriter.log("ingreso con datos faltantes (distrito, tipo_documento, num ingreso)\n", LogWriter.INFO);
                }

            }
     
            model.LogProcesosSvc.finallyProceso("Proceso de devolucion de cheques", this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");

        } catch (Exception e) {
            System.out.println("error en contab de egresos" + e.toString());
            e.printStackTrace();
            try {
                model.LogProcesosSvc.finallyProceso("Proceso de devolucion de cheques", this.hashCode(), usuario.getLogin(), "ERROR :" + e.getMessage());
            } catch (SQLException ex) {
                System.out.println("Error guardando el proceso");
            }
            logWriter.log("ERROR: " + e.toString(), LogWriter.ERROR);
            System.out.println("Error " + e.toString());
            e.printStackTrace();
        } finally {
            logWriter.log("FIN DEL PROCESO ", LogWriter.INFO);
        }
    }
}
