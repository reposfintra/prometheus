/*************************************************************
 * Nombre: ReporteDieferenciaTiempoThreads.java
 * Descripci�n: Hilo para crear el reporte de diferencias de tiempos.
 * Autor: Ing. Jose de la rosa
 * Fecha: 16 de junio de 2006, 08:55 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class ReporteDieferenciaTiempoThreads extends Thread{
    
    /*public static void main ( String [] args ){
        try{
            ReporteDieferenciaTiempoThreads hilo = new ReporteDieferenciaTiempoThreads ();
            hilo.start ("2006-01-20", "2006-01-31", "000031", "KREALES" );
        }catch(Exception ex){
            ex.getMessage ();
        }
    }*/
    
    private String procesoName = "";
    private String des = "";
    private String id = "";
    private String fecha_ini = "";
    private String fecha_fin = "";
    private String cliente = "";
    
    /** Creates a new instance of ReporteDieferenciaTiempoThreads */
    public ReporteDieferenciaTiempoThreads () {
    }
    
    public void start ( String fecha_ini, String fecha_fin, String cliente, String id){
        this.id = id;
        this.fecha_ini = fecha_ini;
        this.fecha_fin = fecha_fin;
        this.cliente = cliente;
        this.des = "El reporte de diferencia de tiempos carge y descarge";
        super.start ();
    }
    
    public synchronized void run (){
        Model model = new Model ();
        try{
            model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, this.id);
            String fecha_actual = Util.getFechaActual_String (4);
            Vector vec = model.reporteGeneralService.consultaDiferenciaViajes (fecha_ini, fecha_fin, cliente);
            //Declaraci�n de variables
            int fila = 5;
            int col  = 0;
            //comienzo conexion
            ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
            String path = rb.getString ("ruta") + "/exportar/migracion/"+id;
            String dia = fecha_actual.substring (8,10);
            File file = new File (path);
            file.mkdirs ();
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite (path+"/ReporteDifCargeDescarge"+fecha_actual+".xls");
            // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
            HSSFCellStyle fecha  = xls.nuevoEstilo ("verdana", 12, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo ("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle texto_fondo  = xls.nuevoEstilo ("verdana", 12, false , false, "text"        , xls.NONE , HSSFColor.GREY_25_PERCENT.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero = xls.nuevoEstilo ("verdana", 12, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle negrita      = xls.nuevoEstilo ("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE);
            HSSFCellStyle header      = xls.nuevoEstilo ("verdana", 18, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.BLUE.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo ("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.LIGHT_BLUE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo ("verdana", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );
            
            xls.obtenerHoja ("GENERAL");
            xls.cambiarMagnificacion (3,4);
            // cabecera
            
            xls.adicionarCelda (0,0, "REPORTE DE DIFERENCIA DE TIEMPOS DE CARGES Y DESCARGES", header);
            xls.combinarCeldas (0, 0, 0, 14);
            
            // subtitulos
            xls.adicionarCelda (2,0, "Fecha Proceso: "   , negrita);
            xls.adicionarCelda (2,1, fecha_actual , fecha);
            xls.adicionarCelda (3,0, "Elaborado Por: "   , negrita);
            xls.adicionarCelda (3,1, id , fecha);
            
            xls.adicionarCelda (fila ,col++ , "Numero Planilla"                        , titulo );
            xls.adicionarCelda (fila ,col++ , "Numero Remesa"                          , titulo );
            xls.adicionarCelda (fila ,col++ , "Cliente"                                , titulo );
            xls.adicionarCelda (fila ,col++ , "Estandar Job"                           , titulo );
            xls.adicionarCelda (fila ,col++ , "Fecha de la Planilla"                   , titulo );
            xls.adicionarCelda (fila ,col++ , "Placa"	                               , titulo );
            xls.adicionarCelda (fila ,col++ , "Origen Planilla"	                       , titulo );
            xls.adicionarCelda (fila ,col++ , "Destino Planilla"                       , titulo );
            xls.adicionarCelda (fila ,col++ , "Agencia"	                               , titulo );
            xls.adicionarCelda (fila ,col++ , "Fecha Carge Planificada"                , titulo );
            xls.adicionarCelda (fila ,col++ , "Fecha Carge Trafico"                    , titulo );
            xls.adicionarCelda (fila ,col++ , "Diferencia Horas Carge"                       , titulo );
            xls.adicionarCelda (fila ,col++ , "Fecha Descarge Planificada"             , titulo );
            xls.adicionarCelda (fila ,col++ , "Fecha Descarge Trafico"                 , titulo );
            xls.adicionarCelda (fila ,col++ , "Diferencia Horas Descarge"                    , titulo );
            // datos
            for(int i = 0; i < vec.size () ; i++){
                ReporteDiferenciaTiempos rep = (ReporteDiferenciaTiempos) vec.get (i);
                fila++;
                col = 0;
                
                xls.adicionarCelda (fila ,col++ , rep.getPlanilla ()                  , texto );
                xls.adicionarCelda (fila ,col++ , rep.getRemesa ()                    , texto );
                xls.adicionarCelda (fila ,col++ , rep.getCliente ()                   , texto );
                xls.adicionarCelda (fila ,col++ , rep.getStd_job_desc ()              , texto );
                xls.adicionarCelda (fila ,col++ , rep.getFecha_despacho ()            , texto );
                xls.adicionarCelda (fila ,col++ , rep.getPlaca ()                     , texto );
                xls.adicionarCelda (fila ,col++ , rep.getOrigen ()                    , texto );
                xls.adicionarCelda (fila ,col++ , rep.getDestino ()                   , texto );
                xls.adicionarCelda (fila ,col++ , rep.getAgencia ()                   , texto );
                xls.adicionarCelda (fila ,col++ , rep.getFecha_carge_planeada ()      , texto );
                xls.adicionarCelda (fila ,col++ , rep.getFecha_carge_trafico ()       , texto );
                xls.adicionarCelda (fila ,col++ , rep.getDiferencia_carge()           , texto );
                xls.adicionarCelda (fila ,col++ , rep.getFecha_descarge_planeada ()   , texto );
                xls.adicionarCelda (fila ,col++ , rep.getFecha_descarge_trafico ()    , numero );
                xls.adicionarCelda (fila ,col++ , rep.getDiferencia_descarge()        , numero );
            }
            xls.cerrarLibro ();
            model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (),this.id,"ERROR :" + e.getMessage ());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso (this.procesoName,this.hashCode (),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
