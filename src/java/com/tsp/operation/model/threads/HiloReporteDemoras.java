/******************************************************************************
 * Nombre clase :                   HiloReporteProveedoresFintra.java         *
 * Descripcion :                    Clase que maneja los eventos relacionados *
 *                                  con el programa que busca el reporte de   *
 *                                  los movimientos de fintra en la BD.       *
 * Autor :                          LREALES                                   *
 * Fecha :                          12 de septiembre de 2006, 11:00 AM        *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import java.sql.SQLException;
import com.tsp.util.UtilFinanzas.*;

public class HiloReporteDemoras extends Thread {
        
    private BeanGeneral bean;
    private Vector datos;
    private String path;
    private String usuario;
    private String fi;
    private String ff;
    private String m = "";//
    private String procesoName;
    private String des;
    private Model model = new Model();
    
    public HiloReporteDemoras() { }
    
    public void start( Vector datos, String usu, String fecha_inicial, String fecha_final ) {
      
        this.datos = datos;
        this.procesoName = "Reporte Demoras";
        this.des = "Reporte De Demoras";
        this.usuario = usu;  
        this.fi = fecha_inicial;
        this.ff = fecha_final;
        
        super.start();
        
    }
    
    public synchronized void run(){
        
        try{
                        
            //Inicia el proceso
            model.LogProcesosSvc.InsertProceso( procesoName, this.hashCode(), des, usuario );
            
            //FORMATO DATE
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            //SimpleDateFormat fmt = new SimpleDateFormat("dd hh:mm:ss");
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            String path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
               
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch  = "ReporteDemoras[" + fecha + "].xls";
            String       Hoja  = "ReporteDemoras";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 19 ; col++ ){ //COLUMNAS
                
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 15 ) ) ); 
                
            }
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo1.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo1.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo1.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo1.setRightBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo1.setTopBorderColor   (HSSFColor.WHITE.index);
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estiloX.setBottomBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estiloX.setLeftBorderColor  (HSSFColor.WHITE.index);
            estiloX.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estiloX.setRightBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estiloX.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor(HSSFColor.BLACK.index);
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/  
            /* DIFERENCIA DIAS */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("dd hh:mm:ss"));
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            row  = sheet.createRow((short)(7));
            row  = sheet.createRow((short)(8));
            row  = sheet.createRow((short)(9));
            
            for ( int j = 0; j < 20; j++ ) { //COLUMNAS
                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);
                
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
                        
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Reporte de Demoras");
            
            for( int i = 2; i < 5; i++ ){ //FILAS
                 
                for ( int j = 0; j < 20; j++ ) { //COLUMNAS
                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);
                    
                }      
                
            }
            
            //FECHA
            row = sheet.getRow((short)(3));              
            cell = row.createCell((short)(0));            
            cell = row.getCell((short)(0));            
            cell.setCellStyle(estilo6);
            cell.setCellValue("Desde: " + fi + " - Hasta: " + ff);
            
            /*************************************************************************************/
            /***** RECORRER LOS DATOS ******/
            
            int Fila = 5; 
            for ( int t = 0; t < datos.size(); t++ ){
                
                BeanGeneral info = ( BeanGeneral ) datos.elementAt( t );
                
                Fila++;
                row  = sheet.createRow( ( short )( Fila ) );
                                
                cell = row.createCell( ( short )( 0 ) );// PLANILLA - PLANILLA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_01() );
                
                cell = row.createCell( ( short )( 1 ) );// FECDES - FECHA DESPACHO
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_09() );
                
                cell = row.createCell( ( short )( 2 ) );// NUMEQUI - PLACA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_07() );
                
                cell = row.createCell( ( short )( 3 ) );// NUMWORK - REMESA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_10() );
                
                cell = row.createCell( ( short )( 4 ) );// NOMDEMO - DESCRIPCION
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_19() );
                
                cell = row.createCell( ( short )( 5 ) );// CAUSA VARADO
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_22() );
                
                cell = row.createCell( ( short )( 6 ) );// FECHAREPORTE - FECHA INICIO
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_02() );
                
                cell = row.createCell( ( short )( 7 ) );// FECHAREPORTE - FECHA FIN
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_03() );
                
                if ( info.getValor_03().equals( "" ) ) {
                    cell = row.createCell( ( short )( 8 ) );// DIFDIASDEM - DIF.DIAS
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( "" );  
                } else {
                    double dif_real = ((double)( fmt.parse( info.getValor_03() ).getTime() - fmt.parse( info.getValor_02() ).getTime())/(double)(1000*60*60*24));
                    cell = row.createCell( ( short )( 8 ) );// DIFDIASDEM - DIF.DIAS                     
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( UtilFinanzas.customFormat("##0.00000",dif_real, 5) );  
                }
                /*
                if ( info.getValor_21().equals( "" ) ) {
                    cell = row.createCell( ( short )( 7 ) );// DIFDIASDEM - DIF.DIAS
                    cell.setCellStyle( estilo7 );
                    cell.setCellValue( "" );
                } else {                    
                    cell = row.createCell( ( short )( 7 ) );// DIFDIASDEM - DIF.DIAS
                    cell.setCellStyle( estilo7 );
                    cell.setCellValue( fmt.parse( info.getValor_21() ) );                    
                }
                */
                
                cell = row.createCell( ( short )( 9 ) );// NOMCIUORIP - ORIGEN PLANILLA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_05() );
                
                cell = row.createCell( ( short )( 10 ) );// NOMCIUDESP - DESTINO PLANILLA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_06() );
                
                cell = row.createCell( ( short )( 11 ) );// NOMCIUORIR - ORIGEN REMESA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_11() );
                
                cell = row.createCell( ( short )( 12 ) );// NOMCIUDESR - DESTINO REMESA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_12() );
                
                cell = row.createCell( ( short )( 13 ) );// NOMAGENCIA - AGENCIA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_04() );
                
                cell = row.createCell( ( short )( 14 ) );// ESTANDAR - CLIENTE
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_13() );
                
                cell = row.createCell( ( short )( 15 ) );// NOMCLIENTE - NOMBRE CLIENTE
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_14() );
                
                cell = row.createCell( ( short )( 16 ) );// NOMCONDUC - NOMBRE CONDUCTOR
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_08() );
                
                cell = row.createCell( ( short )( 17 ) );// NOMDESTIN - NOMBRE DESTINATARIO
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_15() );
                
                cell = row.createCell( ( short )( 18 ) );// COMERCIAL - FACTURA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_20() );
                
                cell = row.createCell( ( short )( 19 ) );// NOMSUPE - USUARIO CREACION
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_18() );
                
            }
                        
            row = sheet.createRow((short)(5));            
            row = sheet.getRow( (short)(5) );     
            
            cell = row.createCell((short)(0));// PLANILLA
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLANILLA");
            
            cell = row.createCell((short)(1));// FECDES
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA DESPACHO");
            
            cell = row.createCell((short)(2));// NUMEQUI
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLACA");
            
            cell = row.createCell((short)(3));// NUMWORK
            cell.setCellStyle(estilo3);
            cell.setCellValue("REMESA");
            
            cell = row.createCell((short)(4));// NOMDEMO
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESCRIPCION");
            
            cell = row.createCell((short)(5));// CAUSA VARADO
            cell.setCellStyle(estilo3);
            cell.setCellValue("CAUSA VARADO");
            
            cell = row.createCell((short)(6));// FECHAREPORTE
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA INICIO");
            
            cell = row.createCell((short)(7));// FECHA
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA FIN");
            
            cell = row.createCell((short)(8));// DIFDIASDEM
            cell.setCellStyle(estilo3);
            cell.setCellValue("DIF.DIAS");
            
            cell = row.createCell((short)(9));// NOMCIUORIP
            cell.setCellStyle(estilo3);
            cell.setCellValue("ORIGEN PLANILLA");
            
            cell = row.createCell((short)(10));// NOMCIUDESP
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESTINO PLANILLA");
            
            cell = row.createCell((short)(11));// NOMCIUORIR
            cell.setCellStyle(estilo3);
            cell.setCellValue("ORIGEN REMESA");
            
            cell = row.createCell((short)(12));// NOMCIUDESR
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESTINO REMESA");
            
            cell = row.createCell((short)(13));// NOMAGENCIA
            cell.setCellStyle(estilo3);
            cell.setCellValue("AGENCIA");
            
            cell = row.createCell((short)(14));// ESTANDAR
            cell.setCellStyle(estilo3);
            cell.setCellValue("CLIENTE");
            
            cell = row.createCell((short)(15));// NOMCLIENTE
            cell.setCellStyle(estilo3);
            cell.setCellValue("NOMBRE CLIENTE");
            
            cell = row.createCell((short)(16));// NOMCONDUC
            cell.setCellStyle(estilo3);
            cell.setCellValue("NOMBRE CONDUCTOR");
            
            cell = row.createCell((short)(17));// NOMDESTIN
            cell.setCellStyle(estilo3);
            cell.setCellValue("NOMBRE DESTINATARIO");
            
            cell = row.createCell((short)(18));// COMERCIAL
            cell.setCellStyle(estilo3);
            cell.setCellValue("FACTURA");
            
            cell = row.createCell((short)(19));// NOMSUPE
            cell.setCellStyle(estilo3);
            cell.setCellValue("USUARIO CREACION");
            
            /******************************************************************/
            /***** GUARDAR DATOS EN EL ARCHIVO *****/
            FileOutputStream fo = new FileOutputStream ( Ruta );
            wb.write ( fo );
            fo.close ();
        
            //Finalizo el proceso exitosamente
            model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "PROCESO EXITOSO" );
            
        } catch( Exception e ){    
            
            e.printStackTrace();
            
            //Capturo errores finalizando proceso
            try{
                
                model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "ERROR : " + e.getMessage() );
            
            } catch( Exception f ){
                
                f.printStackTrace();
                
                try{
                    
                    model.LogProcesosSvc.finallyProceso( procesoName,this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                
                } catch( Exception p ){ p.printStackTrace(); }
                
            }

        }
        
    }

               
        
   
    
}