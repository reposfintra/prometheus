/*
 * Nombre        InfoClienteEnviarMail.java
 * Descripci�n   Realiza envios automaticos de informacion al cliente, via email
 * Autor         David Pi�a Lopez
 * Fecha         23 de julio de 2006, 01:25 PM
 * Ultima modificaci�n: Octubre 3 de 2005.
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.model.threads;

import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.util.*;
import java.lang.*;


// EXCEL
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;





/**
 * Crea el reporte en excel de infocliente.
 * CLase modificada por Alejandro Payares.
 * Ultima modificaci�n: Mayo 4 de 2006.
 * @author  fvillacob
 */
public class InfoClienteEnviarMail extends ReporteExcelSOT {
    /**
     * un vector con los parametros del reporte
     */    
    private String [] argumentos;
    /**
     * El modelo de la aplicaci�n
     */    
    private   Model   model;
    /**
     * La unidad fisica donde ser� guardado el archivo excel
     */    
    private   String  unidad;
    /**
     * El usuario en sesi�n
     */    
    private   Usuario usuario;
    /**
     * Las notas del cliente
     */    
    private   String  notas;
    /**
     * Una tabla que contiene los datos del encabezado del reporte
     */    
    private   Hashtable  header;
    
    private   String  nombrecliente;
    /**
     * Un arreglo de colores que pueden ser modificados en la paleta sin afectar a los ya usados
     */    
    private short [] coloresModificables = {
        HSSFColor.AQUA.index,
        HSSFColor.BLUE_GREY.index,
        HSSFColor.BRIGHT_GREEN.index,
        HSSFColor.CORAL.index,
        HSSFColor.CORNFLOWER_BLUE.index,
        HSSFColor.DARK_BLUE.index,
        HSSFColor.DARK_GREEN.index,
        HSSFColor.DARK_RED.index,
        HSSFColor.DARK_TEAL.index,
        HSSFColor.DARK_YELLOW.index,
        HSSFColor.GOLD.index,
        HSSFColor.GREY_25_PERCENT.index,
        HSSFColor.GREY_40_PERCENT.index,
        HSSFColor.GREY_50_PERCENT.index,
        HSSFColor.GREY_80_PERCENT.index,
        HSSFColor.INDIGO.index,
        HSSFColor.LAVENDER.index,
        HSSFColor.LEMON_CHIFFON.index,
        HSSFColor.LIGHT_BLUE.index,
        HSSFColor.LIGHT_GREEN.index,
        HSSFColor.LIGHT_ORANGE.index,
        HSSFColor.LIGHT_TURQUOISE.index,
        HSSFColor.LIGHT_YELLOW.index,
        HSSFColor.LIME.index,
        HSSFColor.ORCHID.index
    };
    
    
    /**
     * variable auxiliar para indicar la fila actual que se est� editando en la hoja del reporte
     */    
    private int filaActual = 0;
    /**
     * El nombre del archivo excel generado
     */    
    private String fileName;
    //   private   HSSFCellStyle  style6;
    
    private String nombre_fisico;
    
    /**
     * Crea una instancia de la clase.
     */    
    public InfoClienteEnviarMail() {
        String hoy    = Utility.getDate(6).replaceAll("/","").replaceAll(":","_");
        fileName = "InfoCliente_"+hoy;
    }
    
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param model El modelo de la aplicaci�n
     * @param user El usuario en sesi�n
     * @param head Datos del encabezado del reporte
     * @param args Los argumentos del reporte
     * @param nota Las notas del cliente
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */    
    public void start(  Hashtable  head, String[] args, String nota ) throws Exception{
        try{
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            
            this.argumentos = args;
            model = new Model();
            this.header      = head;            
            this.notas       = nota;
            String dir       = "ADMIN";
            // this.unidad      = model.DirectorioSvc.getUrl() + dir;
            this.unidad      = rb.getString("ruta") + "/exportar/migracion/" + dir;
            model.DirectorioSvc.create(dir);            
            new Thread(this, "exportacion").start();
        }catch(Exception e){ throw new Exception( " Hilo: "+ e.getMessage() );}
    }    
    
    /**
     * Crea el encabezado cuando escogen criterios de busqueda
     * @param criteria El criterio de busqueda
     * @param header El encabezado del reporte
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */    
    public void ifCriteria(String criteria, Hashtable header) throws Exception{
        try{
            prepared(filaActual,6);
            cell = row.getCell((short)0) ;   cell.setCellStyle(estiloAzulGrande); cell.setCellValue("CLIENTE");
            HSSFCellStyle estilo = clonarEstilo(estiloAzulGrande);
            this.quitarBordeAEstilo(estilo);
            cell.setCellStyle(estilo);
            this.crearCeldaCombinada(0, (short)0, 0, (short)2, true);
            cell = row.getCell((short)3) ;   cell.setCellStyle(estilo); cell.setCellValue(criteria);
            
            this.crearCeldaCombinada(0, (short)3, 0, (short)5, true);
            prepared(++filaActual,6);
            cell = row.getCell((short)0) ;   cell.setCellStyle(estiloAmarillo); cell.setCellValue((String)header.get("nombreCliente"));
            estilo = clonarEstilo(estiloAmarillo);
            this.quitarBordeAEstilo(estilo);
            cell.setCellStyle(estilo);
            this.crearCeldaCombinada(1, (short)0, 1, (short)2, true);
            cell = row.getCell((short)3) ;   cell.setCellStyle(estilo); cell.setCellValue((String) header.get("userDefValue"));
            this.crearCeldaCombinada(1, (short)3, 1, (short)5, true);
            filaActual++;
            
            ifNOTCriteria(true,header);
        }catch(Exception e){ throw new Exception(e.getMessage());}
    }
    
    /**
     * Crea el encabezado del reporte cuando no escojieron ningun criterio de busqueda
     * @param fecha La fecha
     * @param header El encabezado del reporte
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */    
    public void ifNOTCriteria(boolean fecha,Hashtable header) throws Exception{
        try{
            //////System.out.println("header: "+header);
            prepared(filaActual,6);
            int i = 1;
            row.setHeightInPoints(25);
            cell = row.getCell((short)0) ;   cell.setCellStyle(estiloAzulGrande); cell.setCellValue("CLIENTE");
            HSSFCellStyle estilo = clonarEstilo(estiloAzulGrande);
            this.quitarBordeAEstilo(estilo);
            cell.setCellStyle(estilo);
            this.crearCeldaCombinada(filaActual, 0, filaActual, 1, true);
            if(fecha){
                cell = row.getCell((short)2) ;  cell.setCellStyle(estiloAzulGrande); cell.setCellValue("FECHA INICIAL");
                i=3;
                cell = row.getCell((short)i) ;  cell.setCellStyle(estiloAzulGrande); cell.setCellValue("FECHA FINAL");
                i++;
            }
            cell = row.getCell((short)i) ;   cell.setCellStyle(estiloAzulGrande); cell.setCellValue("TIPO VIAJE");
            i++; cell = row.getCell((short)i) ;   cell.setCellStyle(estiloAzulGrande); cell.setCellValue("ULTIMA ACTUALIZACION");
            
            prepared(++filaActual,6);
            i=1;
            cell = row.getCell((short)0) ;  cell.setCellStyle(estiloAmarillo);   cell.setCellValue((String)header.get("nombreCliente"));
            estilo = clonarEstilo(estiloAmarillo);
            this.quitarBordeAEstilo(estilo);
            cell.setCellStyle(estilo);
            this.crearCeldaCombinada(filaActual, 0, filaActual, 1, true);
            if(fecha){
                cell = row.getCell((short)2) ; cell.setCellStyle(estiloAmarillo); cell.setCellValue((String)header.get("fechaini"));
                i=3;
                cell = row.getCell((short)i) ; cell.setCellStyle(estiloAmarillo); cell.setCellValue((String)header.get("fechafin"));
                i++;
            }
            cell = row.getCell((short)i) ;  cell.setCellStyle(estiloAmarillo);   cell.setCellValue((String) header.get("listaTipoViaje"));
            i++; cell = row.getCell((short)i) ;  cell.setCellStyle(estiloAmarillo);   cell.setCellValue(leerUltimaActualizacion());
            
        }catch(Exception e){ throw new Exception(e.getMessage());}
        
    }
    
    /**
     * Agrega las celdas correspondientes a las convenciones del reporte
     * @throws Exception si alg�n error ocurre
     * @autor Alejandro Payares
     */
    public void mostrarConvensiones() throws Exception{
        try{
            prepared(++filaActual,6);
            cell = row.getCell((short)0);
            HSSFCellStyle estilo = clonarEstilo(estiloAzulGrande);
            this.quitarBordeAEstilo(estilo);
            cell.setCellStyle(estilo);
            cell.setCellValue("CONVENSION CAMPO TIPO DE VIAJE");
            this.crearCeldaCombinada(filaActual, 0, filaActual, 5, true);
            prepared(++filaActual,6);
            cell = row.getCell((short)0) ;
            cell.setCellValue("NA: Nacional");
            cell.setCellStyle(estiloAmarillo);
            cell = row.getCell((short)1) ;
            cell.setCellValue("RM: Reexpe. Maracaibo.");
            cell.setCellStyle(estiloAmarillo);
            cell = row.getCell((short)2) ;
            cell.setCellValue("DM: Directo Maracaibo");
            cell.setCellStyle(estiloAmarillo);
            cell = row.getCell((short)3) ;cell.setCellValue("RC: Reexpe. C�cuta");
            cell.setCellStyle(estiloAmarillo);
            cell = row.getCell((short)4) ;
            cell.setCellValue("DC: Directo C�cuta");
            cell.setCellStyle(estiloAmarillo);
            cell = row.getCell((short)5) ;
            cell.setCellValue("RE: Reexpe. Ecuador.");
            cell.setCellStyle(estiloAmarillo);
        }catch(Exception e){ throw new Exception(e.getMessage());}
    }
    
    /**
     * Agrega las celdas que muestran el numero de registros encontrados
     * @param total el numero de registros o filas del reporte
     * @throws Exception si algun error ocurre
     * @autor Alejandro Payares
     */
    public void mostrarCantidad(int total) throws Exception{
        try{
            prepared(++filaActual,1);
            cell = row.getCell((short)0);
            HSSFFont font = libroExcel.createFont();
            font.setFontHeightInPoints((short)10);        //tamano letra
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            HSSFCellStyle estilo = cell.getCellStyle();
            estilo.setFont(font);
            cell.setCellValue("Registros Encontrados: " + String.valueOf(total));
        }catch(Exception e){ e.printStackTrace(); }
    }
    
    /**
     * Crea una celda donde se muestran los errores
     * @param error El mensaje de error
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void titleError(String error) throws Exception{
        try{
            prepared(5,1);
            cell = row.getCell((short)0) ; cell.setCellValue(error);
        }catch(Exception e){ throw new Exception(e.getMessage());}
    }
    
    
    /**
     * Lee la ultima actualizaci�n de la base de datos SOT y devuelve el valor leido en un String
     * @return La fecha de la ultima actualizaci�n de los datos del reporte
     * @autor Alejandro Payares
     */
    private String leerUltimaActualizacion(){
        try {
            File f = new File(header.get("rutaPaginaLastUpdate")+"/lastUpdateSotDb.html");
            FileInputStream fis = new FileInputStream(f);
            byte [] datos = new byte[fis.available()];
            fis.read(datos);
            return new String(datos);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return "(ninguna)";
    }
    
    /**
     * Bandera para saber si que tipo de viaje escogieron
     */    
    boolean isListaTipoViajeNA;
    
    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */    
    public void run(){
        try{
            model.tbl_GeneralProgService.buscarClientesEmail();
            Vector clientesemail = model.tbl_GeneralProgService.getTodosTblGeneralProg();
            for( int im = 0; im < clientesemail.size(); im++ ){
                //inicializan variables
                String hoy    = Utility.getDate(6).replaceAll("/","").replaceAll(":","_");
                String codcliente = (String)clientesemail.get( im );
                fileName = "InfoCliente_"+codcliente;                
                path          = unidad +"/"+ fileName + "_" + hoy + ".xls";
                nombre_fisico = fileName + "_" + hoy + ".xls";
                filaActual = 0;                
                argumentos[ 0 ] = codcliente;                

                model.clienteService.datosCliente( codcliente );
                Cliente cli = model.clienteService.getCliente();
                header.put( "nombreCliente", cli.getNomcli() );                
                this.model.reporteInfoClienteService.buscarDatosDeReporte(argumentos);
                initExcel(fileName);
                String [] campos           = model.reporteInfoClienteService.obtenerCamposDeReporte();
                String criteria            = (String) header.get("criteria");
                isListaTipoViajeNA = false;
                boolean hayFechaIni = (header.get("fechaini") != null);            
                //--- Encabezados
                if( criteria != null )
                    this.ifCriteria(criteria, header);
                else{
                    this.ifNOTCriteria( hayFechaIni, header);
                    String listaTipoViaje = (String) header.get("listaTipoViaje");
                    if( listaTipoViaje.equals("NA") )
                        isListaTipoViajeNA = true;
                }
                mostrarConvensiones();
                Vector datos = model.reporteInfoClienteService.obtenerDatosReporte();
                mostrarCantidad(datos.size());

                crearTitulos();
                if( datos != null && !datos.isEmpty()) {
                    int j=0;
                    for( int x=0; x<datos.size(); x++ ) {
                        Hashtable fila = (Hashtable) datos.elementAt(x);
                        prepared(++filaActual,campos.length);
                        for( int i = 0; i < campos.length; i++ ) {
                            String str = ((String)fila.get(campos[i])).trim();
                            str = str.equals("")?"-":str;
                            cell = row.getCell((short)i);
                            if ( campos[i].equals("docuinterno") || campos[i].equals("facturacial") || campos[i].equals("docs_rel") ){
                                str = formatearCampo(str);
                                cell.setCellStyle(estiloNormalWrapped);
                            }
                            else if (   campos[i].equals("planillas") ||
                            campos[i].equals("placas") ||
                            campos[i].equals("conductores") ||
                            campos[i].equals("demoras") ||
                            campos[i].equals("peso_cargado") ||
                            campos[i].equals("capacidad") ||
                            campos[i].equals("tipo_vehiculo")){
                                str = str.replaceAll("<br>", "\n");
                                cell.setCellStyle(estiloNormalWrapped);
                            }
                            else {
                                cell.setCellStyle(estiloNormal);

                            }
                            if (campos[i].equals("conductores")){
                                hojaExcel.setColumnWidth((short)i,((short)13568));
                            }
                            else if ( campos[i].equals("demoras") ){
                                hojaExcel.setColumnWidth((short)i,((short)8960));
                                //////System.out.println("Demora "+(j+1)+": "+str);
                            }
                            else {
                                hojaExcel.setColumnWidth((short)i,(short)5376);
                            }
                            cell.setCellValue(str.trim());
                        }
                        j++;
                    }
                }
                else{
                    prepared(++filaActual,campos.length);
                    cell = row.getCell((short)0);
                    HSSFCellStyle estilo = clonarEstilo(estiloResaltado);
                    this.quitarBordeAEstilo(estilo);
                    cell.setCellStyle(estilo);
                    cell.setCellValue("NO SE ENCONTRARON REGISTROS");
                    this.crearCeldaCombinada(filaActual, 0, filaActual, campos.length-1, true);
                }
                save();
                model.tbl_GeneralProgService.buscarEmailsCliente( codcliente );
                Vector emailsCliente = model.tbl_GeneralProgService.getTodosTblGeneralProg();
                String emailsI = "";
                String emailsE = "";
                for( int km = 0; km < emailsCliente.size(); km++ ){
                    TblGeneralProg tg = (TblGeneralProg)emailsCliente.get( km );
                    String emailtemp = (tg.getFC()!=null)?tg.getFC(): "" ;//Email
                    String procedencia = (tg.getprogram()!=null)?tg.getprogram():"";//Interno o externo
                    if( emailtemp.indexOf ("@") != -1){
                        if( procedencia.equals("I") ) emailsI += emailtemp + ";" ;
                        else if( procedencia.equals("E") ) emailsE += emailtemp + ";" ;
                    }else{
                        model.usuarioService.searchUsuario ( emailtemp.toUpperCase() );
                        Usuario u = model.usuarioService.getUsuario ();
                        if( u != null && !u.getEmail().equals("") ){
                            if( procedencia.equals("I") ) emailsI += u.getEmail() + ";" ;
                            else if( procedencia.equals("E") ) emailsE += u.getEmail() + ";" ;
                        }                            
                    }                                        
                }
                com.tsp.operation.model.beans.SendMail em = new com.tsp.operation.model.beans.SendMail();
                em.setEmailfrom("");
                em.setEmailfrom ("procesos@sanchezpolo.com");                
                em.setSendername ( "INFORMACION AL CLIENTE" );
                em.setRecstatus ("A");
                em.setEmailcode ("");
                em.setEmailcopyto ("");
                em.setEmailsubject ("INFORMACION AL CLIENTE TSP");
                em.setEmailbody ("Estamos enviando a uds. Reporte de Informacion al Cliente a corte "+Util.getFechaActual_String( 6 )+" \n\n\nAtentamente,\n\n\nTransporte S�nchez Polo" );
                em.setNombrearchivo( nombre_fisico );
                
                File archivoExcel = new File( path );
                if( emailsE.length() > 1 ){
                    emailsE = emailsE.substring (0, emailsE.length () - 1 );
                    em.setEmailto( emailsE );
                    em.setTipo( "E" );
                    model.sendMailService.sendMail( em, archivoExcel );
                }                
                if( emailsI.length() > 1 ){
                    emailsI = emailsI.substring (0, emailsI.length () - 1 );
                    em.setEmailto( emailsI );
                    em.setTipo( "I" );
                    model.sendMailService.sendMail( em, archivoExcel );
                }                
                //archivoExcel.delete();
                ////System.out.println("Emails externos del cliente "+codcliente + ": "+emailsE);
                ////System.out.println("Emails Internos del cliente "+codcliente + ": "+emailsI);
            }
            
        }catch(Exception e){
            e.printStackTrace();
            try{
                titleError(e.getMessage());
                save();
            }catch(Exception k){}
        }
    }
    
    /**
     * Crea los titulos del reporte
     * @throws Exception si alg�n error ocurre
     * @autor Alejandro Payares
     */
    public void crearTitulos() throws Exception {
        // obtenemos todos los campos del reporte por default
        String [] camposReporte = model.reporteInfoClienteService.obtenerCamposDeReporte();
        prepared(++filaActual,camposReporte.length);
        // la fila de los titulos de los grupos de campos del reporte
        HSSFRow filaGrupos = row;
        prepared(++filaActual,camposReporte.length);
        // la fila de los titulos de los campos del reporte
        HSSFRow filaCampos = row;
        // la altura de la fila de cmapos
        filaCampos.setHeightInPoints(45);
        // variables auxiliares
        String grupoAnterior = "";
        HSSFCellStyle estiloAnterior = null;
        int columnaInicioGrupoAnterior = 0;
        
        // obtenemos el detalle de todos los campos del reporte, es decir titulo, grupo, color, etc
        model.reporteInfoClienteService.buscarDetalleDeCampos("infocliente",camposReporte);
        Hashtable detalleDeCampos = model.reporteInfoClienteService.obtenerDetalleDeCampos();
                
        for( int i=0,indiceColor = 0; i<camposReporte.length; i++ ){
            // obtenemos el detalle del campo
            Hashtable detalle = (Hashtable) detalleDeCampos.get(camposReporte[i]);
            // variables auxiliares
            String grupoActual = "";
            HSSFCellStyle estiloActual = null;
            // obtenemos la celda a editar de la fila i del array de campos
            cell = filaCampos.getCell((short)i);
            // clonamos un estilo que no es usado en el reporte para tomar de el el tipo de letra.
            cell.setCellStyle(this.clonarEstilo(estiloRojo));
            // obtenemos el nombre del grupo del campo actual
            grupoActual = detalle.get("grupo").toString();
            
            // si hay cambio de grupo, posiblemente haya cambio de color
            if ( grupoAnterior.length() > 0 && !grupoActual.equals(grupoAnterior) ) {
                indiceColor++;
                // con esto evitamos que el programa no se reviente
                if ( indiceColor >= this.coloresModificables.length ) {
                    indiceColor = 0;
                }
            }
            // aplicamos el color que lengthcorresponde a la celda actual.
            this.aplicarColoresACelda(cell, detalle.get("colorgrupo").toString(), indiceColor);
            // colocamos el titulo a la celda
            cell.setCellValue(detalle.get("titulocampo").toString());            
            // con esto hacemos que el estilo de letra grande para el titulo del grupo tome el color especificado en la BD
            estiloActual = this.clonarEstilo(estiloRojoGrande);
            // el color HSSFColor.AQUA es modificado en la paleta por el metodo aplicarColoresACelda con el color del grupo especificado en la BD
            estiloActual.setFillForegroundColor(this.coloresModificables[indiceColor]);
            // si hay cambio en el grupo, debemos dejar listo el grupo anterior antes de cambiarlo
            if ( grupoAnterior.length() > 0 && !grupoActual.equals(grupoAnterior) ){
                // obtenemos la primera celda del grupo anterior
                cell = filaGrupos.getCell((short)columnaInicioGrupoAnterior);
                // hacemos una copia del estilo de esa celda
                HSSFCellStyle estilo = clonarEstilo(estiloAnterior);
                // configuramos la celda
                this.quitarBordeAEstilo(estilo);
                cell.setCellStyle(estilo);
                cell.setCellValue(grupoAnterior);
                this.crearCeldaCombinada(filaActual-1, columnaInicioGrupoAnterior, filaActual-1, i-1, true);
                columnaInicioGrupoAnterior = i;
            }
            grupoAnterior = grupoActual;
            estiloAnterior = estiloActual;
        }
        // este proceso es para configurar el grupo de la ultima celda, o el ultimo grupo
        cell = filaGrupos.getCell((short)columnaInicioGrupoAnterior);
        HSSFCellStyle estilo = clonarEstilo(estiloAnterior);
        this.quitarBordeAEstilo(estilo);
        cell.setCellStyle(estilo);
        cell.setCellValue(grupoAnterior);
        this.crearCeldaCombinada(filaActual-1, columnaInicioGrupoAnterior, filaActual-1, camposReporte.length-1, true);
    }
    
    
    /**
     * Metodo que permite aplicar un color de fondo a una celda a partir de un String con el
     * color en formato hexadecimal #RRGGBB.
     * @param celda La celda a la cual le ser� aplicado el color
     * @param color El color en formato #RRGGBB
     * @param indiceColor EL indice del color en la paleta que ser� reemplazado por el nuevo color.
     * @autor Alejandro Payares
     */    
    private void aplicarColoresACelda(HSSFCell celda, String color, int indiceColor){
        if ( color.startsWith("#") && color.length() == 7 ) { //#0099FF
            HSSFPalette palette = libroExcel.getCustomPalette();
            palette.setColorAtIndex(coloresModificables[indiceColor],
            (byte) Integer.parseInt(color.substring(1,3),16),  //RGB red (0-255)
            (byte) Integer.parseInt(color.substring(3,5),16),    //RGB green
            (byte) Integer.parseInt(color.substring(5,7),16)     //RGB blue
            );
            ////System.out.println("columna = "+celda.toString()+" color = "+color+" -> "+Integer.parseInt(color.substring(1,3),16)+","+Integer.parseInt(color.substring(3,5),16)+","+Integer.parseInt(color.substring(5,7),16));
            HSSFCellStyle estilo = celda.getCellStyle();
            estilo.setFillForegroundColor( coloresModificables[indiceColor] );
        }
    }
    
    
    /**
     * Parte la cadena dada con saltos de linea cada 2 caracteres guion '-' encontrados.
     * Para evitar que queden celdas demasiado largas en el reporte, por ejemplo en los documentos internos.
     * @param str El valor a formatear
     * @return El valor formateado para que no haya problemas al crear el archivo excel
     * @autor Alejandro Payares
     */    
    private static String formatearCampo(String str){
        String [] vec = str.split("-");
        if ( vec.length == 0 ) {
            return str;
        }
        StringBuffer sb = new StringBuffer();
        for( int i=0; i< vec.length - 1; i++ ){
            if ( i % 2 == 0 ) {
                sb.append(vec[i] + "\n");
            }
            else {
                sb.append(vec[i] + "-");
            }
        }
        sb.append(vec[vec.length-1]);
        return sb.toString();
    }
    /**
     * m�todo principal para la ejecucion del hilo 
     * @param args the command line arguments
     */
    public static void main(String[] args)throws Exception{
        try{            
            String [] arg = new String[11];
            String fecha = Util.getFechaActual_String( 4 );
            arg[0] = "";//request.getParameter("cliente");
            arg[1] = fecha;//request.getParameter("fechaini");
            arg[2] = fecha;//request.getParameter("fechafin");
            arg[3] = "";//request.getParameter("listaTipoViaje");//NA,RM,RC,RE,DC,DM,DE
            arg[4] = "";//request.getParameter("viajes"); ---> vacio
            arg[5] = "";//request.getParameter("userDefValue");
            arg[6] = "";//request.getParameter("criteria");
            arg[7] = "ALL";//request.getParameter("tipo_carga");
            arg[8] = "";//request.getParameter("mostrarPEC");
            arg[9] = "false";//request.getParameter("todosLosClientes");
            arg[10] = "ADMIN";//usuario.getLogin();
            for( int idx = 0; idx < 6; idx++ ) {
                if (arg[idx] != null) arg[idx] = arg[idx].trim();
                else arg[idx] = "";
            }            
            Hashtable headerFields = new Hashtable();
            headerFields.clear();            
            headerFields.put( "nombreCliente", "" );//request.getParameter("nombreCliente");
            String fechaini       = arg[ 1 ];
            String fechafin       = arg[ 2 ];            
            if( fechaini != null && !fechaini.equals("") ) {
                headerFields.put( "fechaini", fechaini );
                headerFields.put( "fechafin", fechafin );
            }
            headerFields.put( "listaTipoViaje", arg[ 3 ] );
            headerFields.put( "userDefValue", arg[5] );            
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            headerFields.put( "rutaPaginaLastUpdate", ruta + "/WEB-INF/" );
            InfoClienteEnviarMail hilo = new InfoClienteEnviarMail();
            hilo.start( headerFields, arg, "" );
            //System.out.println("YA");
        }catch(Exception e){
            ////System.out.println( "Error "+e.getMessage() );
        }
    }
}