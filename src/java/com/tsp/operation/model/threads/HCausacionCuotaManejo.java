/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.threads;

import com.tsp.operation.model.Model;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

/**
 *
 * @author mcastillo
 */
public class HCausacionCuotaManejo extends Thread  {
   
    Model model;
    Usuario usuario;
    private String ciclo="";
    private String periodo="";
    
    public HCausacionCuotaManejo(Usuario usuario) {
        this.usuario = usuario;
    }
         
    public void start(String ciclo,String periodo) throws Exception {
        try {      
            this.ciclo=ciclo;
            this.periodo=periodo;
            super.start();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
   
     public synchronized void run(){

        try {
            model = new Model(usuario.getBd());
            ArrayList<BeanGeneral> list = model.Negociossvc.datosNegCuotaManejo(ciclo, periodo);
            TransaccionService tService = new TransaccionService(usuario.getBd());

            for (int i = 0; i < list.size(); i++) {
                double valor = Double.parseDouble(list.get(i).getValor_05()) - Double.parseDouble(list.get(i).getValor_06());
                tService.crearStatement();
                String documento = model.Negociossvc.UpCP(list.get(i).getValor_01());

                list.get(i).setValor_19(usuario.getLogin());

                //Creacion de las Facturas    
                tService.getSt().addBatch(model.Negociossvc.ingresarCXCMicrocreditoPost(list.get(i), valor, documento));
                tService.getSt().addBatch(model.Negociossvc.ingresarDetalleCXCMicrocreditoPost(list.get(i), valor, documento, "1"));

                valor = Double.parseDouble(list.get(i).getValor_06()) + valor;
            
                //Actualiza la tabla documentos_neg_aceptados -> Cuota manejo causada.
                tService.getSt().addBatch(model.Negociossvc.updateCuotaManejoCausado(valor, list.get(i).getValor_12(), list.get(i).getValor_13(), list.get(i).getValor_07()));

                tService.execute();

                System.out.println("CXC CUOTA ADMIN DEL NEGOCIO " + list.get(i).getValor_13() + " INSERTADA.");
            }
        }  catch (Throwable e) {
            e.printStackTrace();           
        } 
     }
}
