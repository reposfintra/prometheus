/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.threads;

import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.DAOS.GestionConveniosDAO;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.ConvenioCxc;
import com.tsp.operation.model.beans.SolicitudDocumentos;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.GenerarFacturasAvalService;
import com.tsp.operation.model.services.GestionSolicitudAvalService;
import com.tsp.operation.model.services.ProcesoIndemnizacionServices;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author geotech
 */
/**
 *
 * @author Ing. Iris Vargas
 */
public class HDescargaContingIndem extends Thread {

    Model model;
    private String procesoName;
    private Usuario usuario;
    private String fecha;
    private int items;//los items del comprobante;
    public List listitems;

    public HDescargaContingIndem() {
    }

    public synchronized void run() {
        LogProcesosService log = new LogProcesosService(usuario.getBd());
        ProcesoIndemnizacionServices indemserv = new ProcesoIndemnizacionServices(usuario.getBd());
        try {
            model = new Model(usuario.getBd());
            log.InsertProceso(this.procesoName, this.hashCode(), "Fecha de corte " + fecha, usuario.getLogin());
            ArrayList<Comprobantes> list = (ArrayList<Comprobantes>) indemserv.getNegociosVencidaIndemnizacion(fecha);
            TransaccionService tService = null;
            String periodo = fecha.substring(0, 7).replaceAll("-", "");
            String error = "";
            int exito=0;
            for (int i = 0; i < list.size(); i++) {
                tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();
                Comprobantes comprobante = (Comprobantes) list.get(i);
                items = 0;
                listitems = new LinkedList();

                GestionConveniosDAO convenioDao = new GestionConveniosDAO(usuario.getBd());
                Convenio convenio = convenioDao.buscar_convenio(usuario.getBd(), String.valueOf(comprobante.getId_convenio()));
                ConvenioCxc tituloValor = convenio.buscarTituloValor(comprobante.getTipoNegocio());

                comprobante.setFechadoc(fecha);//fecha del comprobante
                comprobante.setPeriodo(periodo);
                comprobante.setTipo_operacion("001");
                comprobante.setUsuario(usuario.getLogin());
                comprobante.setAprobador(usuario.getLogin());
                comprobante.setTipo("C");
                comprobante.setNumdoc(comprobante.getNumdoc() + comprobante.getRef_5());
                comprobante.setTdoc_rel("NEG");
                comprobante.setDocrelacionado(comprobante.getNumdoc());
                comprobante.setTotal_debito(comprobante.getValor());
                //DEBITO
                comprobante.setCuenta(tituloValor.getCuenta_prov_cxc());
                items++;//contar los items
                listitems.add(indemserv.itemscopy(comprobante, tituloValor.getCuenta_prov_cxp() != null ? tituloValor.getCuenta_prov_cxp() : "", comprobante.getValor(), "D"));//falta agregar a lista de items
                comprobante.setTercero(comprobante.getRef_1());
                //CREDITO
                items++;//contar los items
                listitems.add(indemserv.itemscopy(comprobante, tituloValor.getCuenta_prov_cxc(), comprobante.getValor(), "C"));//falta agregar a lista de items


                comprobante.setTotal_credito(comprobante.getValor());
                GenerarFacturasAvalService gfacserv = new GenerarFacturasAvalService(usuario.getBd());

                comprobante.setTotal_items(items);
                comprobante.setItems(listitems);
                error = gfacserv.insertar(comprobante, usuario.getLogin());
                if (!error.substring(0, 5).equals("Error")) {
                    GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
                    tService.getSt().addBatch(error);
                    SolicitudDocumentos doc = new SolicitudDocumentos();
                    doc.setNumeroSolicitud((int)comprobante.getRef_4()+"");
                    doc.setNumTitulo(comprobante.getComentario());
                    doc.setEstIndemnizacion("DES");
                    doc.setUserUpdate(usuario.getLogin());
                    tService.getSt().addBatch(gsaserv.updateEstIndDocs(doc));
                    tService.execute();
                    error = "";
                } else {
                    error = "Titulo: " + comprobante.getComentario() + " Negocio " + comprobante.getNumdoc() + " - " + error;
                    exito=i;
                    i = list.size();

                }
            }
            indemserv.setProcess(false);
            if (error.equals("")) {
                log.finallyProceso(this.procesoName, this.hashCode(), this.usuario.getLogin(), list.size() + " titulos descargados con exito");
            } else {
                log.finallyProceso(this.procesoName, this.hashCode(), this.usuario.getLogin(), exito+" titulos insertados con exito </br>"+"ERROR Hilo: " + error);
            }


        } catch (Exception e) {
            try {
                System.out.print("ERROR Hilo: " + e.getMessage());
                indemserv.setProcess(false);
                log.finallyProceso(this.procesoName, this.hashCode(), this.usuario.getLogin(), "ERROR Hilo: " + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(HDescargaContingIndem.class.getName()).log(Level.SEVERE, null, ex);
            }

        }


    }

    public void start(Usuario usuario, String fecha) throws Exception {
        try {
            this.usuario = usuario;
            this.procesoName = "Descarga titulos vencidos";
            this.fecha = fecha;
            super.start();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
