/*
 * HMigrarFechaCargue.java
 *
 * Created on 14 de septiembre de 2006, 10:04 AM
 */

package com.tsp.operation.model.threads;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.POIWrite;
import java.io.*;
import java.util.*;
import com.tsp.finanzas.presupuesto.model.beans.Upload;
import com.tsp.util.UtilFinanzas;

public class HMigrarFechaCargue extends Thread {
    private List        listado;
    private Model       model;
    private Usuario     usuario;
    private File  file;
    private String cliente;
    /** Creates a new instance of HImportacionCF */
    public HMigrarFechaCargue(Model m, Usuario u, File f, String c) {
        model = m;
        usuario = u;
        file =f;
        this.cliente = c;
        super.start();
    }
    public synchronized void run() {
        try{
            model.LogProcesosSvc.InsertProceso("Migrar fecha Cargue", this.hashCode(), "Migracion fecha planeacion Cargue", this.usuario.getLogin());
            procesarXLS(file, this.cliente);
            model.LogProcesosSvc.finallyProceso("Migrar fecha Cargue", this.hashCode(), usuario.getLogin(),  "Proceso Exitoso");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
               model.LogProcesosSvc.finallyProceso("Migrar fecha Cargue", this.hashCode(), usuario.getLogin(),  ex.getMessage());
            } catch (Exception e){}
        }
        
    }
    
    public void procesarXLS(File f, String cliente) throws Exception{
        listado = new LinkedList();
        Plan_llegada_cargue planllegada = null;
        int i = 1;
        try {
            String [] datos = null;
            String tem = "";
            String tem2 = "";
            JXLRead xls = new JXLRead(f.getAbsolutePath() );
            xls.obtenerHoja("BASE");
            //Leo la informacion del XLS
            while (  !isEmpty(datos = xls.getValores(i++)) ){
                for(int h=0; h<datos.length;h=h+3){
                    if(datos[h+2].length() >3){
                        tem = ( Integer.parseInt( datos[h+2].substring(0,datos[h+2].indexOf(":")) ) < 10 ) ?"0"+datos[h+2].substring(0,datos[h+2].indexOf(":")) :datos[h+2].substring(0,datos[h+2].indexOf(":"));
                        tem2 = datos[h+2].substring(datos[h+2].indexOf(":"),datos[h+2].length());
                    }
                    planllegada =  new Plan_llegada_cargue();
                    planllegada.setNro_trasn(datos[h]);
                    planllegada.setLlegada_cargue(datos[h+1].replaceAll("/", "-")+" "+tem+tem2);
                }
                listado.add(planllegada);
            }
            xls.cerrarLibro();
            ////System.out.println("Registros Leidos "+listado.size());
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en la rutina procesarXLS en [HImportacionesCF] ....\n" + ex.getMessage());
        }
        
        
        //Proceso para insertar en la BD
        String numrem = "";
        String fecha_actual = Util.fechaActualTIMESTAMP();
        //para generar el csv
        FileWriter        fw;
        BufferedWriter    bf;
        PrintWriter       linea;
        String ruta = UtilFinanzas.obtenerRuta("ruta","/exportar/migracion/" + usuario.getLogin());
        fw             = new FileWriter    (ruta+"/MigraciollegadaFechacargue.csv");
        bf             = new BufferedWriter(fw);
        linea          = new PrintWriter   (bf);
        
        for (i=0; i<listado.size(); i++){
            planllegada = (Plan_llegada_cargue) listado.get(i);
            numrem = model.planllegadacargue.buscarInfoTransporte(cliente,planllegada.getNro_trasn());
            //System.out.println(numrem);
            if(!numrem.equals("")){
                planllegada.setNumrem(numrem);
                planllegada.setCreation_user(usuario.getLogin());
                planllegada.setCreation_date(fecha_actual);
                planllegada.setUser_update(usuario.getLogin());
                planllegada.setLast_update(fecha_actual);
                planllegada.setCodcli(cliente);
                planllegada.setDstrct(usuario.getDstrct());
                planllegada.setBase(usuario.getBase());
                
                try{
                    model.planllegadacargue.insertarPlanllegadaCargue(planllegada);
                }catch (Exception ex){
                    ex.printStackTrace();
                    linea.println(planllegada.getNro_trasn()+","+planllegada.getLlegada_cargue()+","+planllegada.getNumrem()+","+ex.getMessage() );
                }
            }else{
                linea.println(planllegada.getNro_trasn()+","+planllegada.getLlegada_cargue()+", NO Existe el transporte" );
            }
            
        }
        linea.close();
        
    }
    
    
    
    /**
     * Metodo que indica si elo array contiene o no datos
     * @autor mfontalvo
     * @param datos, Arreglo de String
     * @return boolean , estado del array
     */
    private boolean isEmpty(String []datos ){
        boolean empty = true;
        if (datos!=null){
            for (int i = 0; i < datos.length && empty ; i++){
                if ( datos[i] !=null && !datos[i].trim().equals(""))
                    empty = false;
            }
        }
        return empty;
    }
    
}
