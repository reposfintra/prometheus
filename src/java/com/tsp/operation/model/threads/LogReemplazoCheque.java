/*
 * LogReemplazoCheque.java
 *
 */

package com.tsp.operation.model.threads;

import java.io.*;
import java.util.*;

import com.tsp.exceptions.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.pdf.*;
import java.util.*;
import org.dom4j.Element;
import javax.mail.*;
import com.tsp.operation.model.Model;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.util.*;
/**
 *
 * @author  JuanM
 */
public class LogReemplazoCheque extends Thread{
    private  FileWriter        fw;
    private  BufferedWriter    bf;
    private  PrintWriter       linea;
    private String usuario;
    private List listaD;
    private String procesoName;
    private String des;
    private List listaI;
    private com.tsp.operation.model.Model model;
    /** Creates a new instance of MigracionCambioCheque */
    public LogReemplazoCheque() {
    }
    
    public void start(String id, List errores, List insertados ){
        this.usuario        = id;
        this.listaD         = errores;
        this.listaI         = insertados;
        this.procesoName    = "Reemplazo Cheques";
        this.des            = "Proceso de creacion del Log del Reemplazo Cheques";
        super.start();
    }
    
    public synchronized void run(){
        try{
            /*Archivo excel*/
            model = new Model();
            Util util       = new Util();
            UtilFinanzas ut  = new UtilFinanzas();
            
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(),this.des, this.usuario );
            
            String url        = ut.obtenerRuta("ruta", "/exportar/migracion/" + this.usuario );
            
            String fecha = model.movplaService.obtenerFechaActualEnFormato("YYYYMMDD_HHMIss");
            url += "/ReemplazoChequeLogErrores"+fecha+".txt";
            
            this.fw         = new FileWriter    ( url );
            this.bf         = new BufferedWriter( this.fw  );
            this.linea      = new PrintWriter   ( this.bf  );
            
            linea.println("ERRORES ENCONTRADOS                  ");
            
            Iterator it = listaD.iterator();
            while( it.hasNext() ){
                Egreso inf  = (Egreso)it.next();
                String msg  = 	" No se pudo reemplazar el cheque # " + inf.getDocument_no() + " Para el Banco : " + inf.getBranch_code() + " - " + inf.getBank_account_no() + "\n" +
			 	" ya que no existe serie definada, para el Banco : " + inf.getBanco_reemplazo() + " - " + inf.getSuc_reemplazo() + " que corresponde al cheque de reemplazo ";	
                linea.println( msg );                
            }
            // Fin de recorrido de lista
            
            String msg = "";
            if( listaD.size() > 0 ){
                msg += " Hay " + listaD.size() + " registros  que marcaron  \n"+
                      " errores, verificar en el administrador de archivos \n" +
                      " el log de errores. ";
            }
            if( listaI.size() > 0 ){
                msg += "Se Reemplazaron " + listaI.size() + " cheques ";                    
            }
            
            Iterator it2 = listaI.iterator();
            while( it2.hasNext() ){
                Egreso inf  = (Egreso)it2.next();
                String msg2  = "Se Genero el nuevo cheque  # : " + inf.getCheque_reemplazo() + " Para el Banco  : " + inf.getBanco_reemplazo() + " - " + " Sucursal : " + inf.getSuc_reemplazo();
                linea.println(msg2);
            }
            
            linea.println("FIN DE ARCHIVO" );
            
            this.linea.close(); //Fin de la archivo
            
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,msg);
            
        }catch(Exception e){
            e.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,"ERROR HILO : " + e.getMessage());
            }catch(Exception ex){}
        }
    }
    
    public static void main(String a []){
        SendMailManager hilo = new SendMailManager();
        // hilo.start("jescandon@mail.tsp.com", "jescandon@mail.tsp.com", "mail.tsp.com", "c:/ReportePC.xls", "true", "HOSORIO");
    }
    
}
