/***************************************
 * Nombre Clase ............. HCorridas.java
 * Descripci�n  .. . . . . .  Permite Generar las corridas
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  22/02/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...Transportes Sanchez Polo S.A.
 *******************************************/



package com.tsp.operation.model.threads;



import java.io.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;



public class HCorridas extends Thread{
    
    private  Model             model;
    private  Usuario           usuario ;
    private  String            procesoName;
    private  Hashtable         filtros;
    private  String            ACTIVO   = "ON";
    private  String            CARBON   = "'spo','pco','PRC'";
    private  String            GENERAL  = "'COL',''";
    private  FileWriter        fw;
    private  BufferedWriter    bf;
    private  PrintWriter       linea;
    private  String            CORRIDA  = "";
    boolean  isCheque_cero     = false;
    
    
    private final String FILTRO_4  = "TIPO4";
    private final String FILTRO_OP = "TIPOOP";
    
    
    
    public HCorridas() {
    }
    
    
    
    
    /**
     * M�todo que setea el url para generar el archivo de Error
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void initFile(String url)throws Exception{
        try{
            this.fw         = new FileWriter    ( url      );
            this.bf         = new BufferedWriter( this.fw  );
            this.linea      = new PrintWriter   ( this.bf  );
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    /**
     * M�todo inicializa  el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void start(Model modelo,  Usuario user, String  corrida,boolean cheque_cero) throws Exception{
        try{
            
            this.model         = modelo;
            this.usuario       = user;
            this.procesoName   = "GENERACION DE CORRIDAS ";
            this.CORRIDA       = corrida;
            this.isCheque_cero = cheque_cero;
            super.start();
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    /**
     * M�todo que ejecuta el proceso de  generaci�n de la Corrida
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public synchronized void run(){
        try{
            
       
            model.ExtractosSvc.activarProcesos();
            
            String comentario="EXITOSO";
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode()," Generaci�n de Corridas", this.usuario.getLogin() );
            
            
            String distrito       =  model.ExtractosSvc.getDistritoCorrida();   //this.usuario.getDstrct();
            
            
            
            // SQL A EJECUTAR
            filtros               =  model.ExtractosSvc.getFiltros();
            String  filtroViaje   =  (String) filtros.get("viaje");
            String  SQL_EJECT     =  ( filtroViaje.equals( ACTIVO )  )?"SQL_BUSCAR_FACTURAS_VIAJES":"SQL_BUSCAR_FACTURAS";
            
            
            
            
           // CONDICIONES DE FILTROS
            
           // 1. Viajes
              String filtrosViajes             =  ( filtroViaje.equals( ACTIVO )  )?this.getWhereViajes():"";            
              String filtrosFacturasOPs        =  this.getWhere( FILTRO_OP ); 
            
           // 2. Tipo 4
              String filtrosFacturasTipo4      =  this.getWhere( FILTRO_4  );        
              String filtrosHC                 =  ( filtroViaje.equals( ACTIVO )  )?this.getWhereHC()    :"";
               
              
              String filtroPlacas              =  this.getWherePlacas(); 
            
            
            
            
          //  DEFINICION RUTA DE ARCHIVOS  
               
            String ruta            = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + this.usuario.getLogin());
            String fecha           = Util.getFechaActual_String(6).replaceAll(":| |/","");
            String filename        = "CORRIDA_VALIDACION"  + fecha + ".txt";   // Validaci�n de datos
            String filenameError   = "CORRIDA_EJECUCION"   + fecha + ".txt";   // Errores presentadoe durante la ejecucion SQL
            String url             =  ruta + "/"+  filename;
            String urlError        =  ruta + "/"+  filenameError;
            
            
            
           // BUSCAMOS LAS FACTURAS A INCLUIR EN LA CORRIDA:
            List   facturas        = model.ExtractosSvc.getFacturas(
            
                                                                     SQL_EJECT, 
                                                                     distrito, 
                                                                     filtrosViajes, 
                                                                     filtrosFacturasOPs, 
                                                                     filtrosFacturasTipo4, 
                                                                     filtrosHC, 
                                                                     filtroPlacas,
                                                                     
                                                                     url , 
                                                                     model.ExtractosSvc.getTipoPago()
                                                                     
                                                                   );
            
            
            
            
            
           
            
            
            if( facturas.size()>0){
                    
                    
                    TransaccionService  tsvc = new TransaccionService(this.usuario.getBd());
                    
                    
                    // Inicializamos el txt del Error
                    initFile( urlError );
                    
                    
                    
                    // Definici�n de Parametros
                    String    corrida        =   ( this.CORRIDA.equals("") )?  model.ExtractosSvc.getNoCorrida( distrito ) :  this.CORRIDA  ;
                    String    usuario        =   this.usuario.getLogin();
                    String    fechaCorrida   =   Util.getFechaActual_String( 9 );
                    String    msjComple      =   "generado";
                    
                    
                    if( !this.CORRIDA.equals("") )
                        msjComple     =  "modificado";
                    else{
                        // Informaci�n de la corrida
                        String[] sqlInfo = this.infoCorrida(corrida, usuario ).split(";");
                        
                        tsvc.crearStatement();
                        for (int i = 0; i < sqlInfo.length; i++) {
                           
                            tsvc.getSt().addBatch(sqlInfo[i]);
                        }                      
                        tsvc.execute();
                    }
                    
                    
                    
                    
                    
                    // Facturas para incluir a la corrida
                    int  total = 0;
                    for(int i=0;i<facturas.size();i++){
                        Corrida  factura = (Corrida)facturas.get(i);
                        
                        String SQL  =  model.ExtractosSvc.insert         ( factura, corrida, usuario , fechaCorrida );  // Corrida
                        SQL +=  model.ExtractosSvc.updateFacturas        ( factura, corrida, usuario );                 // Facturas
                        
                        try{
                            // Ejecutamos el sql:
                            tsvc.crearStatement();
                            String[] SqlArray =SQL.split(";");
                            for (int j = 0; j < SqlArray.length; j++) {
                                tsvc.getSt().addBatch(SqlArray[j]);                                
                            }                           
                            tsvc.execute();
                            total++;
                            
                        }catch(Exception k){
                            this.linea.println("No se pudo aplicar a la corrida la factura "+ factura.getDocumento() +" del proveedor "+  factura.getProveedor() +" ERROR :" + k.getMessage()  );
                        }
                        
                    }
                    
                    
                    
                    
                    // Cerramos el archivo:
                    this.linea.close();
                    
                    // Mensaje al Usuario:
                    comentario  += "<br>Se ha "+  msjComple  +" la corrida No "+ corrida +" , con "+  total + " facturas";
                    
                    
            }
            else
                    comentario  = "No se pudo generar la Corrida, No se encontraron Facturas con esos criterios, consulte el archivo novedades [ "+ filename  +" ]...";
            
            
            
            
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);            
            model.ExtractosSvc.desactivarProcesos();
            
            
            
            
            
        }catch(Exception e){
            try{
                e.printStackTrace();
                model.ExtractosSvc.desactivarProcesos();
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage());
            }
            catch(Exception f){ }
        }
        finally{
            model.ExtractosSvc.desactivarProcesos();
        }
        
        
    }
    
    
    
    
    
    //***********************************************************************************************************************
    
    
    
    public String resetST(String val, String tipo){
        if( val==null ||  val.equals("null") )
            val = (tipo.equals("F"))?"0099-01-01 00:00:00":"";
            return val;
    }
    
    
    
     /**
     * M�todo que forma el SQL de los filtros seleccionados, teniendo en cuenta la agencia del usuario
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getWhere(String tipoFiltro) throws Exception{
        String where = "";
        try{
            
            String ckBancos         = (String) filtros.get("banco");
            String ckProveedores    = (String) filtros.get("proveedor");
            String ckFecha          = (String) filtros.get("fecha");
            
            String  filtroProveedor = (String) filtros.get("filtroProveedor");
            
            String OP               = model.ExtractosSvc.getOficinaPPal();
            
            
            // BANCOS:
            if( ckBancos.equals( ACTIVO ) ){
                String[] bancos      = model.ExtractosSvc.getBancosCorrida();
                String[] sucursales  = model.ExtractosSvc.getSucursalesCorrida();
                
                if( sucursales!=null && sucursales.length>0){
                    String   cadenaSuc   = model.ExtractosSvc.getToString( sucursales );
                    where += "  AND  a.banco||a.sucursal  IN (" +  cadenaSuc +  ")" ;
                }
                else{
                    String   cadenaBanc  = model.ExtractosSvc.getToString( bancos );
                    where += "  AND  a.banco  IN (" +  cadenaBanc +  ")" ;
                }
            }
            
            
            // PROVEEDORES :
            if( ckProveedores.equals( ACTIVO ) ){
                
                if ( filtroProveedor.equals("NIT") ){
                    String[] proveedores = model.ExtractosSvc.getProveedoresCorrida();
                    String   cadenaProv  = model.ExtractosSvc.getToString( proveedores );
                    where += "  AND  a.proveedor  IN (" +  cadenaProv +  ")" ;
                }
                
                if ( filtroProveedor.equals("HC") ){
                    
                    String[] proveedores = model.ExtractosSvc.getProveedoresCorrida();
                    String   cadenaProv  = model.ExtractosSvc.getToString( proveedores );
                    where += "  AND  a.handle_code  IN (" +  cadenaProv +  ")" ;
                }
                
            }
            
            
            // FECHA DE FACTURAS:
            if(  tipoFiltro.equals( FILTRO_4 )  )
                 if( ckFecha.equals( ACTIVO ) ){
                        String fechaIni = model.ExtractosSvc.getFechaIniCorrida();
                        String fechaFin = model.ExtractosSvc.getFechaFinCorrida();
                        where += "  AND  a.fecha_vencimiento   BETWEEN  '" + fechaIni + "' and  '"+   fechaFin  +"'";
                 }
            
            
            
            // AGENCIA DEL USUARIO:
           // String agencia  =   "= 'BQ'";//( this.usuario.getId_agencia().equals( OP )  ) ? " like '%' " :  " ='" + this.usuario.getId_agencia()  +"'";
           // where += "  AND   a.agencia " + agencia;
            
            
            
        }catch(Exception e){
            throw new Exception( "getWhere " +e.getMessage() );
        }
        return where;
    }
    
    
    
    
    
    /**
     * M�todo que forma el SQL de los filtros por placas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getWherePlacas() throws Exception{
        String where = "";
        try{
            
            String  ckPlaca    = (String) filtros.get("placa");
            if( ckPlaca.equals( ACTIVO ) ){
                String[] placas  = model.ExtractosSvc.getPlacasFiltro();
                if( placas!=null && placas.length>0){
                    String   cadenaPlaca  = model.ExtractosSvc.getToString( placas );
                    where += "  WHERE  a.placa   IN (" +  cadenaPlaca +  ")" ;
                }
            }
            
        }catch(Exception e){
            throw new Exception( "getWherePlacas " +e.getMessage() );
        }
        return where;
    }
    
    
    
    
    /**
     * M�todo que filtro de fecha de viajes
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getWhereViajes() throws Exception{
        String where = "";
        try{
            String     ckViaje        = (String) filtros.get("Viaje");
            
            String fechaIni  = model.ExtractosSvc.getFechaIniViaje();
            String fechaFin  = model.ExtractosSvc.getFechaFinViaje();
            
            
            String  tipo     = model.ExtractosSvc.getTipoViaje();
            String  base     = (  tipo.equals("C") )? CARBON : GENERAL ;
            
            
            where           = "     feccum    BETWEEN  '" + fechaIni + "' and  '"+   fechaFin  +"'";
            where          += " AND base      IN ("  +  base  +")";
            
            
            String resto = "  AND  reg_status = 'C'     AND   substr(cia,1,3)   = ?    AND  factura   != ''   ";
            
            if ( base.equals(CARBON)){
                resto    = "  AND  reg_status != 'A'    AND   substr(cia,1,3)   = ?    AND  factura   != ''   ";
            }
            
            where += resto;
            
            
            
        }catch(Exception e){
            throw new Exception( "getWhereViajes " + e.getMessage() );
        }
        return where;
    }
    
    
    
    
    
    /**
     * M�todo que filtro de hc de la factura tipo 4
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getWhereHC() throws Exception{
        String where = "";
        try{           
            
            String  tipo      = model.ExtractosSvc.getTipoViaje();
            String  base      = (  tipo.equals("C") )? CARBON : GENERAL ;
            where             = (base.equals(CARBON))?" AND a.handle_code ='FC' ":"  AND a.handle_code !='FC' ";
            
        }catch(Exception e){
            throw new Exception( "getWhereHC " + e.getMessage() );
        }
        return where;
    }
    
    
    
    
    
    
    
   
    /**
     * M�todo que filtro de fecha de viajes
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String infoCorrida(String corrida, String user) throws Exception{
        String insert = "";
        try{
            
            Hashtable  filtros = model.ExtractosSvc.getFiltros();
            String     dis            =  resetST( model.ExtractosSvc.getDistritoCorrida(),"");
            String     tpago         =  resetST( model.ExtractosSvc.getTipoPago()       ,"");
            String     tviaje         =  resetST( model.ExtractosSvc.getTipoViaje()      ,"");
            String     fechacumini    =  resetST( model.ExtractosSvc.getFechaIniViaje()  ,"F");
            String     fechacumfin    =  resetST( model.ExtractosSvc.getFechaFinViaje()  ,"F");
            String     bancos        =  model.ExtractosSvc.getToString( model.ExtractosSvc.getBancosCorrida()      ).replaceAll("'","");
            String     sucursal      =  model.ExtractosSvc.getToString( model.ExtractosSvc.getSucursalesCorrida()  ).replaceAll("'","");
            String     fproveedor     =  resetST( (String)filtros.get("filtroProveedor") ,"");
            String     proveedores   =  model.ExtractosSvc.getToString( model.ExtractosSvc.getProveedoresCorrida() ).replaceAll("'","");
            String     placas        =  model.ExtractosSvc.getToString( model.ExtractosSvc.getPlacasFiltro()       ).replaceAll("'","");
            String     fechavenini    =  resetST( model.ExtractosSvc.getFechaIniCorrida(),"F");
            String     fechavenfin    =  resetST( model.ExtractosSvc.getFechaFinCorrida(),"F");
            
            
            if( proveedores.equals("") )
                fproveedor = "";
            
            if( !sucursal.equals("") )
                bancos = sucursal;
            
            
            insert = model.ExtractosSvc.insertInfoCorrida(dis, corrida, tpago, tviaje, fechacumini, fechacumfin, bancos, fproveedor, proveedores, placas, fechavenini, fechavenfin, user,(isCheque_cero)?"S":"N");
            
            
            
        }catch(Exception e){
            throw new Exception( "infoCorrida " + e.getMessage() );
        }
        return insert;
    }
    
    
    
}
