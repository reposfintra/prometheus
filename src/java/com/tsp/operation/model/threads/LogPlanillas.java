/*
 * ReporteBalancePrueba.java
 *
 * Created on 20 de octubre de 2005, 05:07 PM
 */

package com.tsp.operation.model.threads;

import java.io.*;
import java.util.*;

import com.tsp.exceptions.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.pdf.*;
import java.util.*;
import org.dom4j.Element;
import javax.mail.*;
import com.tsp.operation.model.Model;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.util.*;
/**
 *
 * @author  JuanM
 */
public class LogPlanillas extends Thread{
    private  FileWriter        fw;
    private  BufferedWriter    bf;
    private  PrintWriter       linea;
    private String usuario;
    private List listaD;
    private String procesoName;
    private String des;
    private com.tsp.operation.model.Model model;
    /** Creates a new instance of MigracionCambioCheque */
    public LogPlanillas() {
    }
    
    public void start(String id, List lista ){
        this.usuario = id;
        this.listaD = lista;
        this.procesoName = "Impresion de Planillas";
        this.des = "Proceso de creacion del Log de impresion de planillas";
        super.start();
    }
    
    public synchronized void run(){
        try{
            /*Archivo excel*/
            model = new Model();
            Util util   = new Util();
            
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(),this.des, this.usuario );
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String url = rb.getString("ruta");
            String fecha = model.movplaService.obtenerFechaActualEnFormato("YYYYMMDD_HHMIss");
            url += "/exportar/migracion/"+usuario+"/ImpresionPlanillasLog"+fecha+".txt";
            
            //AMATURANA 22.03.2007
            File ruta = new File( "/exportar/migracion/"+usuario+"/" );
            if (!ruta.exists()) ruta.mkdirs();
            File file = new File(url);
            
            this.fw         = new FileWriter    ( url );
            this.bf         = new BufferedWriter( this.fw  );
            this.linea      = new PrintWriter   ( this.bf  );
            
            linea.println("PLANILLAS NO IMPRESAS                  ");
            
            Iterator it = listaD.iterator();
            while( it.hasNext() ){
                DatosPlanillaImpPDF inf = (DatosPlanillaImpPDF)it.next();
                //Validacion de Valor de l
                linea.println(" La OC # " + inf.getNumeroPlanilla() + " no tiene tiene plan de Viaje " );
            }
            // Fin de recorrido de lista
            
            String msg = "Generación exitosa del log de impresion de planillas";
            
            linea.println("FIN DE ARCHIVO" );
            
            this.linea.close(); //Fin de la archivo
            
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,msg);
            
        }catch(Exception e){
            e.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,"ERROR HILO : " + e.getMessage());
            }catch(Exception ex){}
        }
    }
    
    public static void main(String a []){
        SendMailManager hilo = new SendMailManager();
        // hilo.start("jescandon@mail.tsp.com", "jescandon@mail.tsp.com", "mail.tsp.com", "c:/ReportePC.xls", "true", "HOSORIO");
    }
    
}
//22 Marzo de 2007
