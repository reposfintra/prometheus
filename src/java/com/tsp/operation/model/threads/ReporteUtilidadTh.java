/********************************************************************
 *      Nombre Clase.................   ReporteUtilidadTh.java
 *      Descripci�n..................   Hilo para la generaci�n del reporte de utilidad
 *      Autor........................   Ing. Tito Andr�s Maturana D.
 *      Fecha........................   20.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import java.sql.*;

//import com.tsp.operation.model.threads.POIWrite;



public class ReporteUtilidadTh extends Thread{
    
    private String ano;
    private String mes;
    private Usuario user;
    private String cia;
    private String tipo;
    
    private String nombre_tbl;
    
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, String ano, String mes, Usuario user, String cia, String tipo){
        this.ano = ano;
        this.mes = mes;
        this.user = user;
        this.cia = cia;
        this.tipo = tipo;
        
        this.model   = modelo;
        this.procesoName = "Reporte de Utilidad";
        this.des = "Per�odo: " + ano + " - " + mes;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            java.util.Date d = FechaHoy.getTime();
            SimpleDateFormat tbl = new SimpleDateFormat("yyyy_MM_dd");
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            this.nombre_tbl = "_" + tbl.format(d);
            
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user.getLogin());
            
            model.rep_utilidadSvc.crearTabla(this.nombre_tbl, cia);
            model.rep_utilidadSvc.planillasPeriodo(ano, mes, cia, tipo);
            ResultSet rs = model.rep_utilidadSvc.getRs();
            /*rs.last();
            int ncol = rs.getRow();
            ////System.out.println("............................ RS: " + ncol);
            rs.beforeFirst();*/
            
            while( rs.next() ){
                RepUtilidad rep = new RepUtilidad();
                rep.Load(rs);
                rep.setUsuario(user.getLogin());
                rep.setPeriodo(ano+mes);
                model.rep_utilidadSvc.setRepUtil(rep);
                if( !model.rep_utilidadSvc.existe() ){
                    model.rep_utilidadSvc.insertar();
                }                
            }
            
            rs.beforeFirst();
            
            while( rs.next() ){
                RepUtilidad rep = new RepUtilidad();
                rep.Load(rs);
                this.getRemesas(rep.getPlanilla());
            }
            
            /**
             *  GENERACION DEL ARCHIVO XLS
             */            
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user.getLogin() + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteUtilidad_" + FechaFormated + ".xls", user.getLogin(), Fecha);
            //String nom_archivo = "ReporteUtilidad_" + FechaFormated + ".xls";  
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd hh:mm:ss"  , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle fecha2  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 9, false , false, ""        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 9, false , false, ""        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            //\"$ \"
            
            int npag = 1;
            int fila = this.nuevaPagina(xls, Fecha, npag);
            int col = 0;
            
            java.util.Date fechai = (java.util.Date) com.tsp.util.Util.ConvertiraDate1(ano + "-" + mes + "-01");
            java.util.Date fechaf = (java.util.Date) com.tsp.util.Util.ConvertiraDate1(ano + "-" + mes + "-" + com.tsp.util.UtilFinanzas.diaFinal(ano, mes));
            
            
            /* Se obtiene la informaci�n de la tabla temporal */
            model.rep_utilidadSvc.leer(cia, ano+mes);
            rs = model.rep_utilidadSvc.getRs();
            
            while ( rs.next() ){
                if( rs.getRow()>25000 ) {
                    npag++;
                    fila = this.nuevaPagina(xls, Fecha, npag);
                }
                
                col = 0;
                RepUtilidad rep = new RepUtilidad();
                rep.Load(rs);
                
                xls.adicionarCelda(fila, col++, rep.getPlanilla(), texto);
                xls.adicionarCelda(fila, col++, rep.getAgencia(), texto);
                xls.adicionarCelda(fila, col++, rep.getPlaca(), texto);
                
                java.util.Date date = com.tsp.util.Util.ConvertiraDate0(rep.getFecdsp());
                xls.adicionarCelda(fila, col++, date, fecha);
                if( date.before(fechai) ){
                    rep.setPeriodopla("Anterior");
                } else if ( date.after(fechaf) ){
                    rep.setPeriodopla("Posterior");
                } else {
                    rep.setPeriodopla("Actual");
                }
                
                xls.adicionarCelda(fila, col++, rep.getOripla(), texto);
                xls.adicionarCelda(fila, col++, rep.getDespla(), texto);                
                xls.adicionarCelda(fila, col++, rep.getRemesa(), texto);
                
                date = com.tsp.util.Util.ConvertiraDate1(rep.getFecrem());
                xls.adicionarCelda(fila, col++, date, fecha2);
                if( date.before(fechai) ){
                    rep.setPeriodorem("Anterior");
                } else if ( date.after(fechaf) ){
                    rep.setPeriodorem("Posterior");
                } else {
                    rep.setPeriodorem("Actual");
                }
                
                xls.adicionarCelda(fila, col++, rep.getCliente(), texto);
                xls.adicionarCelda(fila, col++, rep.getEstandar(), texto);
                xls.adicionarCelda(fila, col++, rep.getDescripcion(), texto);
                xls.adicionarCelda(fila, col++, rep.getOrirem(), texto);
                xls.adicionarCelda(fila, col++, rep.getDesrem(), texto);
                xls.adicionarCelda(fila, col++, com.tsp.util.Util.redondear(rep.getVlrpla(), 2) , moneda);
                xls.adicionarCelda(fila, col++, com.tsp.util.Util.redondear(rep.getVlrrem(), 2) , moneda);
                xls.adicionarCelda(fila, col++, com.tsp.util.Util.redondear((rep.getVlrrem() - rep.getVlrpla()), 2) , moneda);
                xls.adicionarCelda(fila, col++, rep.getPorcentaje(), entero);
                xls.adicionarCelda(fila, col++, rep.getVacio().compareTo("VAC")==0 ? "VACIO" : "", texto);
                xls.adicionarCelda(fila, col++, com.tsp.util.Util.redondear(rep.getUnidadesdsp(), 2) , flotante);
                xls.adicionarCelda(fila, col++, rep.getPeriodorem(), texto);
                xls.adicionarCelda(fila, col++, rep.getPeriodopla(), texto);
                
                /*xls.adicionarCelda(fila, col++, , texto);
                xls.adicionarCelda(fila, col++, com.tsp.util.Util.redondear(, 2) , moneda);
                java.util.Date date = com.tsp.util.Util.ConvertiraDate0(ht.get("fecdsp").toString());
                xls.adicionarCelda(fila, col++, date, fecha); */
                
                fila++;
            }       
            
            xls.cerrarLibro();
            
            ////System.out.println("..................... FINALIZO EL PROCESO");
            model.rep_utilidadSvc.dropTable();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "PROCESO FINALIZADO CON EXITO.");
            
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.rep_utilidadSvc.dropTable();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :" + ex.getMessage());
            } catch (SQLException exc ) {
                
            } catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
    
    
    /**
     * Obtiene las planillas de una remesa
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @version 1.0.
     **/
    private synchronized void getPlanillas(String numrem) throws Exception {    
        model.rep_utilidadSvc.getPlanillas(numrem);
	Vector planillas = model.rep_utilidadSvc.getVector();
        
        //////System.out.println("...................... REMESA: " + numrem + " - PLANILLAS ENCONTRADAS: " + planillas.size());
        
        for( int i=0; i<planillas.size(); i++){
            RepUtilidad rep = (RepUtilidad) planillas.elementAt(i);
            rep.setPeriodo(ano+mes);
            model.rep_utilidadSvc.setRepUtil(rep);
            if( !model.rep_utilidadSvc.existe() ){
                rep.setUsuario(user.getLogin());
                rep.setPeriodo(ano+mes);
                model.rep_utilidadSvc.setRepUtil(rep);
                model.rep_utilidadSvc.insertar();
                this.getRemesas(rep.getPlanilla());                
            }
        }
    }
    
    /**
     * Obtiene las remesas de una planilla
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @version 1.0.
     **/
    private synchronized  void getRemesas(String numpla) throws Exception {
        model.rep_utilidadSvc.getRemesas(numpla);
	Vector remesas = model.rep_utilidadSvc.getVector();
        
        //////System.out.println("...................... PLANILLA: " + numpla + " - REMESAS ENCONTRADAS: " + remesas.size());
        
        for( int i=0; i<remesas.size(); i++){
            RepUtilidad rep = (RepUtilidad) remesas.elementAt(i);            
            this.getPlanillas(rep.getRemesa());
        }    
    }   
    
    /**
     * Crea una nueva p�gina en el archivo de Excel
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws Exception si ocurre una execpcion al crear la nueva p�gina
     * @version 1.0.
     **/
    private synchronized int nuevaPagina(POIWrite xls, String Fecha, int npag) throws Exception{
        xls.obtenerHoja("P�gina " + npag);
        xls.cambiarMagnificacion(3,4);
        
        xls.obtenerHoja("P�gina 1");
        xls.cambiarMagnificacion(3,4);
        
        
        //Estilos
        
        HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
        HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
        HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);

        // cabecera
        xls.combinarCeldas(0, 0, 0, 20);
        xls.combinarCeldas(3, 0, 3, 20);
        xls.combinarCeldas(4, 0, 4, 20);
        xls.combinarCeldas(5, 0, 5, 20);
        xls.adicionarCelda(0, 0, "REPORTE DE UTILIDAD", header2);
        xls.adicionarCelda(3, 0, "Per�odo: " + mes + "/" + ano, header3);
        xls.adicionarCelda(4, 0, "Proyecto: " + ( tipo.compareTo("1") == 0 ? "CARGA GENERAL" : "CARBON" ), header3);
        xls.adicionarCelda(5, 0, "Fecha del proceso: " + Fecha, header3);
        
        int fila = 7;
        int col  = 0;
        
        xls.adicionarCelda(fila, col++, "PLANILLA", header1);
        xls.adicionarCelda(fila, col++, "AGENCIA", header1);
        xls.adicionarCelda(fila, col++, "PLACA", header1);
        xls.adicionarCelda(fila, col++, "FECHA DE DESPACHO", header1);
        xls.adicionarCelda(fila, col++, "ORIGEN PLANILLA", header1);
        xls.adicionarCelda(fila, col++, "DESTINO PLANILLA", header1);
        xls.adicionarCelda(fila, col++, "REMESA", header1);
        xls.adicionarCelda(fila, col++, "FECHA DE REMESA", header1);
        xls.adicionarCelda(fila, col++, "CLIENTE", header1);
        xls.adicionarCelda(fila, col++, "ESTANDAR", header1);
        xls.adicionarCelda(fila, col++, "DESCRIPCION", header1);
        xls.adicionarCelda(fila, col++, "ORIGEN REMESA", header1);
        xls.adicionarCelda(fila, col++, "DESTINO REMESA", header1);
        xls.adicionarCelda(fila, col++, "VLR. PLANILLA", header1);
        xls.adicionarCelda(fila, col++, "VLR. REMESA", header1);
        xls.adicionarCelda(fila, col++, "UTILIDAD", header1);
        xls.adicionarCelda(fila, col++, "PORCENTAJE", header1);
        xls.adicionarCelda(fila, col++, "VACIO", header1);
        xls.adicionarCelda(fila, col++, "UNIDS. DESPACHO", header1);
        xls.adicionarCelda(fila, col++, "PERIODO REMESA", header1);
        xls.adicionarCelda(fila, col++, "PERIODO PLANILLA", header1);
        //xls.adicionarCelda(fila, col++, "", header1);
        
        
        fila++;
        
        return fila;
    }
    
}
