/*
 * ReporteEgreso.java
 *
 * Created on 13 de septiembre de 2005, 03:42 PM
 */

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  Henry
 */
public class ReporteNumeroOCTrafimo extends Thread{
    
    private Vector datos;
    private String fechai;
    private String fechaf;
    private String dstr;
    
    //
    private String path;
    private String usuario;
    /** Creates a new instance of ReporteEgreso */
    public ReporteNumeroOCTrafimo() {
    }
    
    public void start(Vector datos, String fechai, String fechafin, String dstr, String u) {
        this.datos = datos;
        this.fechai = fechai;
        this.fechaf = fechafin;
        this.dstr = dstr;
        
        this.usuario = u;
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            Util u = new Util();
            //
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            path = rb.getString("ruta");
                        
            File file = new File(path +"/exportar/migracion/"+usuario);
            file.mkdirs();
            
            String nombreArch= "ReporteNumerOcTrafimo["+fechai+"_"+fechaf+"]["+dstr+"].xls";
            String       Hoja  = "ReporteOCTrafimo";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            ////System.out.println("RUTA: " + Ruta);
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for (int col=0; col<40 ; col++)
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 20 ) ) );
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.ORANGE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x1));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /****************************************************/
            
            //sheet.createFreezePane(0,5);
                        
            row  = sheet.createRow((short)(0));
            
            for (int j=0;j<11;j++) {
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
            }
            
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            
            
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO");
            
            
            row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("Reporte de Control");
            /*************************************************************************************/
            
            /***** RECORRER LOS DATOS ******/
            int Fila = 2;
            
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("BANCO");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("SUCURSAL");
            
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PROVEEDOR A PAGAR");
            
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FACTURA ORIGINAL");
            
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CORRIDA CHEQUE");
            
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CHEQUE NUMERO");
            
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR NETO");
            
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLACA");
            
            cell = row.createCell((short)(8));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PROVEEDOR");
            
            cell = row.createCell((short)(9));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FACTURA");
            
            cell = row.createCell((short)(10));
            cell.setCellStyle(estilo3);
            cell.setCellValue("MOVIMIENTOS");            
            
            for (int i=0; i<datos.size(); i++){
                NumeroRptControlOracle re = (NumeroRptControlOracle) datos.elementAt(i);
                Fila++;
                row  = sheet.createRow((short)(Fila));
                
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getBRANCH_CODE()!=null)?re.getBRANCH_CODE():"");
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getBANCK_ACCT_NO()!=null)?re.getBANCK_ACCT_NO():"");
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getSUPP_TO_PAY()!=null)?re.getSUPP_TO_PAY():"");
                
                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getEXT_INV_NO()!=null)?re.getEXT_INV_NO():"");
                
                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getCHEQUE_RUN_NO()!=null)?re.getCHEQUE_RUN_NO():"");
                
                cell = row.createCell((short)(5));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getCHEQUE_NO()!=null)?re.getCHEQUE_NO():"");
                
                cell = row.createCell((short)(6));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getLOC_INV_ORIG()!=null)?re.getLOC_INV_ORIG():"");
                
                cell = row.createCell((short)(7));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getSUPPLIER_NO()!=null)?re.getSUPPLIER_NO():"");
                
                cell = row.createCell((short)(8));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getSUPPLIER_NAME()!=null)?re.getSUPPLIER_NAME():"");
                
                cell = row.createCell((short)(9));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getINV_NO()!=null)?re.getINV_NO():"");
                
                cell = row.createCell((short)(10));
                cell.setCellStyle(estilo4);
                String val = "";
                if (re.getNumMov().equals("0"))
                    val = "Nunguno";
                else
                    val = re.getNumMov();
                cell.setCellValue(val);
                
            
        }//end for datos
        
        /************************************************************************************/
        /**** GUARDAR DATOS EN EL ARCHIVO  ***/
        FileOutputStream fo = new FileOutputStream(Ruta);
        wb.write(fo);
        fo.close();
    }catch(Exception e){
        ////System.out.println(e.getMessage());
    }finally{
        super.destroy();
    }
}

}
