/*
 * Nombre        NProveedoresNoRegThread.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         18 de marzo de 2006, 11:45 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.threads;

import java.io.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;

public class NProveedoresNoRegThread extends Thread {
    
    private Model model;
    private List lista;
    private List lprov;
    private String procesoName;
    private String usuario;
    
    private  FileWriter      fw;
    private  BufferedWriter  bf;
    private  PrintWriter     linea;
    
    /**
     * Crea una nueva instancia de  NProveedoresNoRegThread
     */
    public NProveedoresNoRegThread(){
    }
    
    
    public void start(Model modelo, List lista, List lprov, String usuario) throws Exception{
        try{
            
            this.model = modelo;
            this.lista = lista;
            this.lprov = lprov;
            this.usuario = usuario;
            this.procesoName = "NOTIFICACION PROVEEDORES NO REGISTRADOS EN SISTEMA";
            
            super.start();
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    public void initTxt()throws Exception{
        try{
            
            String hoy = Util.getFechaActual_String(6).replaceAll("/|:","");
            //obtenemos ruta para grabacion del archivo a generar
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta") + "/exportar/migracion/"+this.usuario + "/ProveedoresNoEncontrado" + hoy + ".txt";
            
            this.fw     = new FileWriter    (path);
            this.bf     = new BufferedWriter(this.fw);
            this.linea  = new PrintWriter   (this.bf);
            
        }catch(Exception e){
            throw new Exception(e.getMessage() );
        }
    }
    
    public void save(){
        this.linea.close();
    }
    
    public synchronized void run(){
        try{
            if (lista != null && lista.size() > 0 && lprov != null && lprov.size() > 0){
                //registramos el proceso en el log de procesos
                model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(),
                "Notificacion Proveedores no Registrados en el Sistema",this.usuario);
                
                String mensaje = "";
                
                initTxt();
                
                String msg = "Para las siguientes planillas NO SE GENERARON OPs porque los proveedores no se encontraron registrados en el sistema";
                mensaje += msg + "\n\nNo. PLANILLAS \n-----------------\n";
                linea.println(msg);
                linea.println(" ");
                linea.println("No. PLANILLAS");
                linea.println("-----------------");
                for(int i=0;i<lista.size();i++){
                    Planilla p = (Planilla) lista.get(i);
                    String  dato = p.getNumpla();
                    linea.println( dato );
                    mensaje += dato + "\n";
                }
                
                linea.println(" ");
                msg = "Los siguientes proveedores no se encuentran registrados en el sistema";
                mensaje += "\n\n" + msg + "\n\nNo. PROVEEDORES \n-----------------\n";
                linea.println(" ");
                linea.println("No. PROVEEDORES");
                linea.println("-----------------");
                
                for(int i=0;i<lprov.size();i++){
                    Planilla p = (Planilla) lprov.get(i);
                    String  dato = p.getNitpro();
                    linea.println( dato );
                    mensaje += dato + "\n";
                }
                
                save();
                //////System.out.println("MENSAJE MAIL " + mensaje);
                
                //generamos mail de notificacion
                String correos = model.usuarioService.buscarEmailPerfil("ADMIN");
                
                if(!correos.equals("") ){
                    Email mail = new Email();
                    mail.setEmailfrom   ( "procesos@mail.tsp.com" );
                    mail.setEmailto     ( correos);
                    mail.setEmailsubject( "GENERACION OP");
                    mail.setEmailbody   ( mensaje );
                    try{
                        model.emailService.saveMail(mail);
                    }
                    catch(Exception e){
                    }
                }
                
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usuario,"Se ha generado un archivo con los proveedores no registrados en el sistema (" + lprov.size() + ") de la consulta");
            }
        }catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),
                this.usuario," ERROR Hilo: " + e.getMessage());
            } catch(Exception f){}
        }
    }
    
}
