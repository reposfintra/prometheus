/**
 * Nombre        HExportacionesPrestamos.java
 * Descripci�n   Clase de exportaciones de prestammos.
 * Autor         Mario Fontalvo Solano
 * Fecha         26 de marzo de 2006, 09:38 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.threads;


import com.tsp.operation.model.beans.Prestamo;
import com.tsp.operation.model.beans.Amortizacion;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.ResumenPrestamo;
import com.tsp.operation.model.beans.FuncionesFinancieras;
import com.tsp.operation.model.Model;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;


public class HExportacionesPrestamos extends Thread {
    
    // Declaracion de variables generales de importacion
    Model    model;
    Usuario  usuario;
    String   tipo;
    SimpleDateFormat fmt;
    
    
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, letra, numero, porcentaje, letraCentrada, numeroNegrita;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    
    
    /** Crea una nueva instancia de  HExportacionesPrestamos */
    public HExportacionesPrestamos() {
    }
    
    
    public void start(String tipoExportacion, Model model, Usuario usuario){
        this.tipo    = tipoExportacion;
        this.model   = model;
        this.usuario = usuario;
    }
    
    
    
    public synchronized void run(){
        try{
            runMain();
        }catch (Exception ex){
            ////System.out.println("Error en HExportacionesPrestamos ....\n " + ex.getMessage());
        }
    }
    
    
    /**
     * Metodo de ejecucion del proceso general
     * fue creado de esta forma por si se requiere que la calse no trabaje como un hilo
     * @auto mfontalvo
     * @return boolean, indicador del estado del proceso.
     */
    public boolean runMain() throws Exception {
        try{
            // creacion del diractorio del usuario
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
            
            // prestamo y sus amortizaciones amortizaciones
            if (tipo.equals("001")){
                ExportarPrestamo();
            }
            // listado general de prestamos
            else if (tipo.equals("002")){
                ExportarListadoPrestamo();
            }
            // resumen general
            else if (tipo.equals("003")){
                ExportarResumenBeneficiario();
            }
            // resumen por beneficiario
            else if (tipo.equals("004")){
                ExportarResumenPrestamos();
            }
            // amortizaciones migradas
            else if (tipo.equals("005")){
                ExportarAmortizacionesMigradas();
            }            
        }catch (Exception ex){
            ////System.out.println("Error en runMain ....\n " + ex.getMessage());
            return false;
        }
        return true;
    }
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAzul.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo3      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo4      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            letra        = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada= xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero       = xls.nuevoEstilo("Tahoma", 8 , false , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 8 , true  , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            porcentaje   = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            
        }catch (Exception ex){
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
        
    }
    
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            xls.adicionarCelda(2,0, "USUARIO", titulo1);
            xls.adicionarCelda(2,1, usuario.getLogin() , titulo1);
            
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    /**
     * Expotacion de un Prestamo con sus amortizaciones.
     * @autor mfontalvo
     * @throws Exception.
     */
    private void ExportarPrestamo() throws Exception {
        try{
            Prestamo pt = model.PrestamoSvc.getPrestamo();
            this.crearArchivo("Prestamo_" + pt.getId()+".xls", "PRESTAMO N� " +pt.getId());
            int fila = 4;
            
            
            xls.adicionarCelda(fila  , 0 , "BENEFICIARIO"   , titulo1);
            xls.adicionarCelda(fila  , 1 , "[" + pt.getBeneficiario() + "] " +  pt.getBeneficiarioName() , letra);
            xls.combinarCeldas(fila  , 1, fila++, 5);
            
            
            
            xls.adicionarCelda(fila  , 0 , "CUOTAS"   , titulo1);
            xls.adicionarCelda(fila++, 1 , pt.getCuotas()             , letra);
            
            xls.adicionarCelda(fila  , 0 , "TASA"   , titulo1);
            xls.adicionarCelda(fila++, 1 , (pt.getTasa()/100)         , porcentaje);
            
            xls.adicionarCelda(fila  , 0 , "FECHA ENTREGA DINERO"   , titulo1);
            xls.adicionarCelda(fila++, 1 , pt.getFechaEntregaDinero() , letra);
            
            xls.adicionarCelda(fila  , 0 , "PERIODO"   , titulo1);
            xls.adicionarCelda(fila++, 1 , pt.getFrecuencias()        , letra);
            
            
            ////////////////////////////////////////////////////////////////////
            fila = 5;
            
            
            xls.adicionarCelda(fila  , 3 , "TIPO DE PRESTAMO"   , titulo1);
            xls.adicionarCelda(fila++, 4 , pt.getTipoPrestamoName()   , letra);
            
            xls.adicionarCelda(fila  , 3 , "VALOR PRESTAMO"   , titulo1);
            xls.adicionarCelda(fila++, 4 , pt.getMonto()              , numero);
            
            xls.adicionarCelda(fila  , 3 , "VALOR INTERESES"   , titulo1);
            xls.adicionarCelda(fila++, 4 , pt.getIntereses()          , numero);
            
            xls.adicionarCelda(fila  , 3 , "VALOR A PAGAR"   , titulo1);
            xls.adicionarCelda(fila++, 4 , (pt.getMonto() - pt.getIntereses()) , numero);
            
            
            
            
            for ( int i = 0; i<=7 ; xls.adicionarCelda(fila,  i++, "" , titulo2));
            xls.adicionarCelda(fila, 0 , "DATOS GENERALES DE LA AMORTIZACION", titulo2);
            xls.combinarCeldas(fila, 0, fila, 7);
            
            
            for ( int i = 8; i<=13 ; xls.adicionarCelda(fila,  i++, "" , titulo2));
            xls.adicionarCelda(fila, 8 , "BENEFICIARIO", titulo2);
            xls.combinarCeldas(fila, 8, fila, 13);
            
            for ( int i = 14; i<=19 ; xls.adicionarCelda(fila,  i++, "" , titulo2));
            xls.adicionarCelda(fila, 14, "TERCERO", titulo2);
            xls.combinarCeldas(fila, 14, fila, 19);
            
            fila++;
            
            String [] cabecera = {
                "CUOTA", "FECHA", "MONTO", "CAPITAL", "INTERES", "TOTAL A PAGAR", "SALDO", "MIGRACION",
                "ESTADO MIMS", "BANCO", "SUCURSAL", "CHEQUE", "FECHA DESCUENTO", "VALOR DESCONTADO",
                "ESTADO MIMS", "BANCO", "SUCURSAL", "CHEQUE", "FECHA PAGO", "VALOR PAGADO"
            };
            short [] dimensiones = {
                5100, 5100, 5100, 5100, 5100, 5100, 5100, 5100,
                5100, 5100, 5100, 5100, 5100, 5100,
                5100, 5100, 5100, 5100, 5100, 5100
            };
            
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                xls.cambiarAnchoColumna(i, dimensiones [i]);
            }
            
            fila++;
            
            
            if (pt.getAmortizacion()==null || pt.getAmortizacion().isEmpty()){
                if (pt.getAprobado().equals("S")){
                    model.PrestamoSvc.searchAmortizacionPorPrestamo(pt.getDistrito(), pt.getId());
                    pt.setAmortizacion(model.PrestamoSvc.getListaAmortizaciones());
                }else{
                    pt.setAmortizacion( FuncionesFinancieras.getAmortizacion(pt) );
                }
            }
            
            
            List listAmortizacion = pt.getAmortizacion();
            for(int i=0; i<listAmortizacion.size(); i++, fila++){
                Amortizacion amort = ( Amortizacion )listAmortizacion.get(i);
                
                xls.adicionarCelda(fila, 0, i+1, letra);
                xls.adicionarCelda(fila, 1, amort.fmt.format( amort.getFecha() ), letraCentrada);
                xls.adicionarCelda(fila, 2, amort.getMonto()      , numero);
                xls.adicionarCelda(fila, 3, amort.getCapital()    , numero);
                xls.adicionarCelda(fila, 4, amort.getInteres()    , numero);
                xls.adicionarCelda(fila, 5, amort.getTotalAPagar(), numero);
                xls.adicionarCelda(fila, 6, amort.getSaldo()      , numero);
                xls.adicionarCelda(fila, 7, (!amort.getFechaMigracion().equals("0099-01-01 00:00:00")?amort.getFechaMigracion():"")  , letraCentrada);
                
                xls.adicionarCelda(fila, 8, amort.getDescripcionEstado(0) , letra);
                xls.adicionarCelda(fila, 9, amort.getBancoDescuento()     , letra);
                xls.adicionarCelda(fila, 10, amort.getSucursalDescuento() , letra);
                xls.adicionarCelda(fila, 11, amort.getChequeDescuento()   , letra);
                xls.adicionarCelda(fila, 12, (!amort.getFechaDescuento().equals("0099-01-01")?amort.getFechaDescuento():"")   , letraCentrada);
                xls.adicionarCelda(fila, 13, amort.getValorDescuento()    , numero);
                
                xls.adicionarCelda(fila, 14, amort.getDescripcionEstado(1)  , letra);
                xls.adicionarCelda(fila, 15, amort.getBancoPagoTercero()    , letra);
                xls.adicionarCelda(fila, 16, amort.getSucursalPagoTercero() , letra);
                xls.adicionarCelda(fila, 17, amort.getChequePagoTercero()   , letra);
                xls.adicionarCelda(fila, 18, (!amort.getFechaPagoTercero().equals("0099-01-01")?amort.getFechaPagoTercero():"")  , letraCentrada);
                xls.adicionarCelda(fila, 19,  amort.getValorPagoTercero()   , numero);
            }
            this.cerrarArchivo();
        }catch (Exception ex){
            throw new Exception("Error en ExportarPrestamo ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Expotacion de un Listado de Prestamos
     * @autor mfontalvo
     * @throws Exception.
     */
    private void ExportarListadoPrestamo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ListadoPrestamos_" + fmt.format( new Date() ) +".xls", "LISTADO DE PRESTAMOS");
            int fila = 4;
            
            String [] cabecera = { "ID", "CLASIFICACION", "TERCERO", "NIT", "BENEFICIARIO", "TIPO PRESTAMO", "FECHA ENTREGA", "VALOR", "INTERESES", "VALOR A ENTREGAR", "CUOTAS", "TASA", "SALDO" };
            short  [] dimensiones = new short [] { 3703, 3703, 3703, 3703 , 9000, 5740, 3703, 4037, 4037, 4037, 2729, 2729, 5740  };
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                xls.cambiarAnchoColumna(i, dimensiones[i] );
            }
            
            fila++;
            List lista = model.PrestamoSvc.getListaPrestamos();
            for ( int i = 0; i<lista.size(); i++, fila++){
                Prestamo pt = (Prestamo) lista.get(i);
                xls.adicionarCelda(fila  , 0 , pt.getId()                 , letraCentrada);
                xls.adicionarCelda(fila  , 1 , pt.getClasificacionName()  , letra);
                xls.adicionarCelda(fila  , 2 , pt.getTerceroName()        , letra);
                xls.adicionarCelda(fila  , 3 , pt.getBeneficiario()       , letraCentrada);
                xls.adicionarCelda(fila  , 4 , pt.getBeneficiarioName()   , letra);
                xls.adicionarCelda(fila  , 5 , pt.getTipoPrestamoName()   , letra);
                xls.adicionarCelda(fila  , 6 , pt.getFechaEntregaDinero() , letraCentrada);
                xls.adicionarCelda(fila  , 7 , pt.getMonto()              , numero);
                xls.adicionarCelda(fila  , 8 , pt.getIntereses()          , numero);
                xls.adicionarCelda(fila  , 9 , (pt.getTipoPrestamo().equals("IA")?pt.getMonto() - pt.getIntereses():pt.getMonto()) , numero);
                xls.adicionarCelda(fila  , 10, pt.getCuotas()             , letraCentrada);
                xls.adicionarCelda(fila  , 11, (pt.getTasa()/100)         , porcentaje);
                xls.adicionarCelda(fila  , 12 ,(pt.getMonto()-pt.getCapitalDescontado())  , numero);
            }
            
            
            // TOTALES
            fila++;
            xls.adicionarCelda(fila, 0 ,"TOTALES",  titulo1);
            xls.combinarCeldas(fila, 0 , fila, 6);
            xls.adicionarFormula(fila, 7 , "SUM(H6:H"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 8 , "SUM(I6:I"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 9 , "SUM(J6:J"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 12, "SUM(M6:M"+(fila-1)+")", numeroNegrita);
            this.cerrarArchivo();
            
            
        }catch (Exception ex){
            throw new Exception("Error en ExportarPrestamo ...\n" + ex.getMessage() );
        }
    }
    
    /**
     * Exportacion del Resumen Beneficiario
     * @autor mfontalvo
     * @throws Exception.
     */
    private void ExportarResumenBeneficiario() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ResumenBeneficiario_" + fmt.format( new Date() ) +".xls", "RESUMEN BENEFICIARIO");
            int fila = 4;
            
            List lista = model.PrestamoSvc.getListaPrestamos();
            Prestamo pi = (Prestamo) lista.get(0);
            
            
            xls.adicionarCelda(fila  , 0 , "CLASIFICACION"            , titulo1);
            xls.adicionarCelda(fila++, 1 , pi.getClasificacionName()  , titulo1);
            xls.adicionarCelda(fila  , 0 , "TERCERO"                  , titulo1);
            xls.adicionarCelda(fila++, 1 , pi.getTerceroName()        , titulo1);
            xls.adicionarCelda(fila  , 0 , "BENEFICIARIO"             , titulo1);
            xls.adicionarCelda(fila++, 1 , "["+ pi.getBeneficiario() +"]" + pi.getBeneficiarioName()   , titulo1);
            
            
            String [] cabecera = { "ID", "TIPO PRESTAMO", "CUOTAS", "TASA", "FECHA ENTREGA", "VALOR", "INTERESES", "VALOR A ENTREGAR", "VALOR MIGRADO", "VALOR REGISTRADO", "VALOR DESCONTADO", "VALOR PAGADO", "CAPITAL PAGADO", "SALDO CAPITAL" };
            short  [] dimensiones = new short [] { 5740, 8703, 2729, 2729 , 5740, 5037, 5037, 5037,  5037, 5037, 5037, 5037, 5037, 5037};
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                xls.cambiarAnchoColumna(i, dimensiones[i] );
            }
            
            fila++;
            for ( int i = 0; i<lista.size(); i++, fila++){
                Prestamo pt = (Prestamo) lista.get(i);
                xls.adicionarCelda(fila  , 0 , pt.getId()                 , letraCentrada);
                xls.adicionarCelda(fila  , 1 , pt.getTipoPrestamoName()   , letra        );
                xls.adicionarCelda(fila  , 2 , pt.getCuotas()             , letra        );
                xls.adicionarCelda(fila  , 3 , (pt.getTasa()/100)         , porcentaje   );
                xls.adicionarCelda(fila  , 4 , pt.getFechaEntregaDinero() , letraCentrada);
                xls.adicionarCelda(fila  , 5 , pt.getMonto()              , numero);
                xls.adicionarCelda(fila  , 6 , pt.getIntereses()          , numero);
                xls.adicionarCelda(fila  , 7 , (pt.getTipoPrestamo().equals("IA")?pt.getMonto() - pt.getIntereses():pt.getMonto()) , numero);
                xls.adicionarCelda(fila  , 8 , pt.getValorMigrado()       , numero);
                xls.adicionarCelda(fila  , 9 , pt.getValorRegistrado()    , numero);
                xls.adicionarCelda(fila  , 10, pt.getValorDescontado()    , numero);
                xls.adicionarCelda(fila  , 11, pt.getValorPagado()        , numero);
                xls.adicionarCelda(fila  , 12, pt.getCapitalDescontado()  , numero);
                xls.adicionarCelda(fila  , 13, (pt.getMonto()-pt.getCapitalDescontado())  , numero);
            }
            
            
            // TOTALES
            fila++;
            xls.adicionarCelda(fila, 0 ,"TOTALES",  titulo1);
            xls.combinarCeldas(fila, 0 , fila, 4);
            xls.adicionarFormula(fila, 5, "SUM(F9:F"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 6, "SUM(G9:G"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 7, "SUM(H9:H"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 8, "SUM(I9:I"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 9, "SUM(J9:J"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila,10, "SUM(K9:K"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila,11, "SUM(L9:L"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila,12, "SUM(M9:M"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila,13, "SUM(N9:N"+(fila-1)+")", numeroNegrita);
            
            this.cerrarArchivo();
        }catch (Exception ex){
            throw new Exception("Error en ExportarResumenBeneficiario ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Exportacion del Resumen de Prestamos Generales
     * @autor mfontalvo
     * @throws Exception.
     */
    private void ExportarResumenPrestamos() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ResumenPrestamos_" + fmt.format( new Date() ) +".xls", "RESUMEN PRESTAMOS GENERALES");
            int fila = 4;
            
            List lista = model.ResumenPrtSvc.getListaPrestamos();
            ResumenPrestamo pi = (ResumenPrestamo) lista.get(0);
            
            String [] cabecera = { "NIT", "BENEFICIARIO", "CAPITAL", "INTERESES", "CAPITAL PAGADO", "INTERES PAGADO", "VLR MIGRADO MIMS", "VLR REGISTRADO MIMS", "VLR DESCONTADO PROP", "VLR PAGADO FINTRA", "SALDO NO MIGRADO", "SALDO DESCONTADO", "SALDO FINTRA" };
            short  [] dimensiones = new short [] { 5100, 9000, 5100, 5100, 5100 , 5100, 5100, 5100, 5100,  5100, 5100, 5100, 5100};
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                xls.cambiarAnchoColumna(i, dimensiones[i] );
            }
            
            fila++;
            for ( int i = 0; i<lista.size(); i++, fila++){
                ResumenPrestamo pt = (ResumenPrestamo) lista.get(i);
                xls.adicionarCelda(fila  , 0 , pt.getNit()               , letra );
                xls.adicionarCelda(fila  , 1 , pt.getNombre()            , letra );
                xls.adicionarCelda(fila  , 2 , pt.getValor()             , numero);
                xls.adicionarCelda(fila  , 3 , pt.getIntereses()         , numero);
                xls.adicionarCelda(fila  , 4 , pt.getValorDesc()         , numero);
                xls.adicionarCelda(fila  , 5 , pt.getInteresesDesc()     , numero);
                xls.adicionarCelda(fila  , 6 , pt.getVlrMigradoMims()    , numero);
                xls.adicionarCelda(fila  , 7 , pt.getVlrRegistradoMims() , numero);
                xls.adicionarCelda(fila  , 8 , pt.getVlrDescontadoProp() , numero);
                xls.adicionarCelda(fila  , 9 , pt.getVlrPagadoFintra()   , numero);
                xls.adicionarCelda(fila  , 10, pt.saldoNoMigrado()       , numero);
                xls.adicionarCelda(fila  , 11, pt.saldoDescontado()      , numero);
                xls.adicionarCelda(fila  , 12, pt.saldoPagadoTercero()   , numero);
            }
            
            // TOTALES
            fila++;
            xls.adicionarCelda(fila, 0 ,"TOTALES",  titulo1);
            xls.combinarCeldas(fila, 0 , fila, 1);
            xls.adicionarFormula(fila, 2, "SUM(C6:C"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 3, "SUM(D6:D"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 4, "SUM(E6:E"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 5, "SUM(F6:F"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 6, "SUM(G6:G"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 7, "SUM(H6:H"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 8, "SUM(I6:I"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 9, "SUM(J6:J"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila,10, "SUM(K6:K"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila,11, "SUM(L6:L"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila,12, "SUM(M6:M"+(fila-1)+")", numeroNegrita);
            
            this.cerrarArchivo();
        }catch (Exception ex){
            throw new Exception("Error en ExportarResumenPrestamos ...\n" + ex.getMessage() );
        }
    }
    
    
    
    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void ExportarAmortizacionesMigradas() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("AmortizacionesMigradas_" + fmt.format( new Date() ) +".xls", "AMORTIZACIONES MIGRADAS");
            int fila = 4;
            
            
            List lista = model.PrestamoSvc.getListaAmortizaciones();
            
            String [] cabecera = { "DISTRITO", "TERCERO", "NIT", "BENEFICIARIO", "PRESTAMO", "COUTA", "FECHA PAGO", "VALOR A PAGAR", "ESTADO DESCUENTO", "FECHA DESCUENTO", "VALOR DESCUENTO", "ESTADO PAGO", "FECHA PAGO", "VALOR PAGO" };
            short  [] dimensiones = new short [] { 3100, 3400, 5100, 9000, 4000, 4000, 5100 , 5100, 5100, 5100, 5100,  5100, 5100, 5100};
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                xls.cambiarAnchoColumna(i, dimensiones[i] );
            }
            
            fila++;
            for ( int i = 0; i<lista.size(); i++, fila++){
                Amortizacion am = (Amortizacion) lista.get(i);
                xls.adicionarCelda(fila  , 0 , am.getDstrct()            , letra );
                xls.adicionarCelda(fila  , 1 , am.getNameTercero()       , letra );
                xls.adicionarCelda(fila  , 2 , am.getBeneficiario()      , letra );
                xls.adicionarCelda(fila  , 3 , am.getNameBeneficiario()  , letra );
                xls.adicionarCelda(fila  , 4 , am.getPrestamo()          , letraCentrada );
                xls.adicionarCelda(fila  , 5 , am.getItem()              , letraCentrada );
                xls.adicionarCelda(fila  , 6 , am.getFechaPago()         , letraCentrada );
                xls.adicionarCelda(fila  , 7 , am.getTotalAPagar()       , numero);
                xls.adicionarCelda(fila  , 8 , am.getEstadoDescuento()   , letraCentrada);                
                xls.adicionarCelda(fila  , 9 , (am.getFechaDescuento  ()==null || am.getFechaDescuento().equals("0099-01-01")?"":am.getFechaDescuento())    , letraCentrada);
                xls.adicionarCelda(fila  , 10, am.getValorDescuento()    , numero);
                xls.adicionarCelda(fila  , 11, am.getEstadoPagoTercero()   , letraCentrada);                
                xls.adicionarCelda(fila  , 12, (am.getFechaPagoTercero()==null || am.getFechaPagoTercero().equals("0099-01-01")?"":am.getFechaPagoTercero())    , letraCentrada);
                xls.adicionarCelda(fila  , 13, am.getValorPagoTercero()    , numero);
            }
            
            // TOTALES
            fila++;
            xls.adicionarCelda(fila, 0 ,"TOTALES",  titulo1);
            xls.combinarCeldas(fila, 0 , fila, 6);
            xls.adicionarFormula(fila, 7 , "SUM(H6:H"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 10, "SUM(K6:K"+(fila-1)+")", numeroNegrita);
            xls.adicionarFormula(fila, 13, "SUM(N6:N"+(fila-1)+")", numeroNegrita);
            
            this.cerrarArchivo();
        }catch (Exception ex){
            throw new Exception("Error en ExportarResumenPrestamos ...\n" + ex.getMessage() );
        }
    }    
    
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    

    
}

