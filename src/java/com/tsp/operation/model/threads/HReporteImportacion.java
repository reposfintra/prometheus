/*
 * Nombre        HReporteImportacion.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         23 de noviembre de 2006, 11:36 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.text.*;

import com.tsp.operation.model.beans.POIWrite;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.ImpoExpoDAO;
import com.tsp.operation.model.*;
import com.tsp.util.UtilFinanzas.*;

public class HReporteImportacion extends Thread{
    
    private String path;
    private String procesoName;
    private ImpoExpoDAO imp;
    private String des;
    Model model;
    
    Usuario user;
    int cont = 0;
    String   ruta;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, letra, numero, porcentaje, letraCentrada, numeroNegrita, letraDerecha;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int           fila = 0;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    
    String[] tipos;
    String[] titulos;
    String impos;
    
    /**
     * Crea una nueva instancia de  HReporteImportacion
     */
    public HReporteImportacion() {
    }
    
    public void generarReporteImportacion(){
        try{
                        
            String comentario="EXITOSO";
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode()," Reporte Importaci�n", this.user.getLogin() );                        
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/"+user.getLogin() ;
            
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
            String fecha = Util.getFechaActual_String(6);
            fecha = fecha.replaceAll("/","-");
            fecha = fecha.replaceAll(":","");
            
            
            String split[] = impos.split(",");            
            
            imp = new ImpoExpoDAO();
            for( int h=0; h<split.length; h++ ){       
                
                Vector datos = imp.reporteImportacion( split[h] ,user.getLogin() , user.getDstrct() );
                this.InitArchivo( "ReporteImportacion_"+split[h]+"_"+fecha );
                
                for( int i=0; i<datos.size(); i++){
                    
                    ImpoExpo imp = (ImpoExpo) datos.elementAt(i);
                    this.crearHoja( nvl(imp.getOt()) ,"REPORTE DE IMPORTACI�N", imp);                    
                    
                }
                cont++;
                /**** GUARDAR DATOS EN EL ARCHIVO  ***/
                this.cerrarArchivo();
                
            }
                                    
            comentario += ": "+cont +" Archivo(s) Generado(s)";
            
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.user.getLogin() ,comentario);
            
        }catch(Exception ex){
            //System.out.println("error en generarReporteImportacion: "+ex.getMessage());
            try {
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.user.getLogin() ,"ERROR Hilo: " + ex.getMessage());                        
            } catch (Exception e1){
                e1.printStackTrace();
            }
        }
    }
    
    public void InitArchivo(String nameFile) throws Exception{
        try{
            
            xls          = new com.tsp.operation.model.beans.POIWrite();
            xls.nuevoLibro( ruta + "/" + nameFile + ".xls" );
            
            // colores
            cAzul       = xls.obtenerColor(204,255,255);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 12, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAzul.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo3      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo4      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            letra        = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada= xls.nuevoEstilo("Tahoma", 10 , false , false, ""      , xls.NONE , HSSFColor.WHITE.index , HSSFCellStyle.ALIGN_CENTER,2);
            letraDerecha = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , cAzul.getIndex() , HSSFCellStyle.ALIGN_RIGHT,1);
            numero       = xls.nuevoEstilo("Tahoma", 8 , false , false, "_(#,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 8 , true  , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            porcentaje   = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
    }
    
    private void crearHoja(String nameSheet, String titulo, ImpoExpo imp) throws Exception{
        try{
            
            //InitArchivo(nameFile);
            xls.obtenerHoja( nameSheet );
            
            
            xls.combinarCeldas(2, 0, 2, 14);
            xls.adicionarCelda(0, 0, "TRANSPORTES SANCHEZ POLO S.A.", header);
            xls.adicionarCelda(1, 0, titulo, header);
            xls.adicionarCelda(2, 0, "Importacion: "+imp.getDocumento() , header);
            
            
            xls.adicionarCelda(4, 0, "Generado: " + fmt.format( new Date() ) , titulo1);
            xls.adicionarCelda(5, 0, "Usuario: " + user.getLogin() , titulo1);
            
            
            int col = 0;
            
            short [] dimensiones = new short[14];
            
            
            for( int i=0; i<14; i++ ){
                dimensiones[i] = (short)( 4 * 1000 );
            }
            
            fila=7;
            
            xls.cambiarAnchoColumna(0, 10000);
            xls.cambiarAnchoColumna(1, 10000);
            
            
            // TITULOS
            xls.adicionarCelda(fila++,  0, "CONTENEDOR", titulo2);
            xls.adicionarCelda(fila++,  0, "CONDUCTOR", titulo2);
            xls.adicionarCelda(fila++,  0, "PLACA CAMI�N", titulo2);
            xls.adicionarCelda(fila++,  0, "HORA SALIDA", titulo2);
            xls.adicionarCelda(fila++,  0, "FECHA DE CARGUE EN PUERTO", titulo2);
            xls.adicionarCelda(fila++,  0, "FECHA LLEGADA CDR", titulo2);
            xls.adicionarCelda(fila++,  0, "FECHA SALIDA CDR", titulo2);
            xls.adicionarCelda(fila++,  0, "PATIO DE CONTENEDORES", titulo2);
            xls.adicionarCelda(fila++,  0, "OT. TRANSPORTE", titulo2);
            xls.adicionarCelda(fila++,  0, "No. FACTURA", titulo2);
            xls.adicionarCelda(fila++,  0, "FECHA EMISI�N FACTURA TSP", titulo2);
            //xls.adicionarCelda(fila++,  0, "VALOR POR TRANSPORTE", titulo2);
            //xls.adicionarCelda(fila++,  0, "VALOR TOTAL POR FACTURA", titulo2);
            xls.adicionarCelda(fila++,  0, "FECHA SIA", titulo2);
            xls.adicionarCelda(fila++,  0, "DESCRIPCION IMP", titulo2);            
            
            xls.adicionarCelda(fila++,  0,  "", letraCentrada );
            xls.adicionarCelda(fila++,  0, "OBSERVACIONES", titulo2);
            xls.adicionarCelda(fila++,  0, "FECHA ESTIMADA ARRIBO", titulo2);
            
            fila=7;
            // DATOS
            xls.adicionarCelda(fila++, 1,  nvl( imp.getContenedor()        ), letraCentrada ) ;
            xls.adicionarCelda(fila++, 1,  nvl( imp.getConductor()         ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getPlaca()             ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getHora_salida()       ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getFecha_cargue()      ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getFecha_llegada_cdr() ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getFecha_salida_cdr()  ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getPatio()             ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getOt()                ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getFactura()           ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getFecha_fac()         ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getFecha_sia()         ), letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getTipo_doc()          ), letraCentrada );//Descripcion de la Importacion
            
            /*
            String transp = "";
            if( !nvl(imp.getVlr_transporte()).equals("") ){
                transp = UtilFinanzas.customFormat("#,##0.00", Double.parseDouble( nvl( imp.getVlr_transporte() ) ), 2 );
            }
            String fact = "";
            if( !nvl(imp.getVlr_factura()).equals("") ){
                fact = UtilFinanzas.customFormat("#,##0.00", Double.parseDouble( nvl( imp.getVlr_factura() ) ), 2 );
            }*/
            //xls.adicionarCelda(fila++, 1,  transp                           , letraCentrada );
            //xls.adicionarCelda(fila++, 1,  fact                             , letraCentrada );
            
            xls.adicionarCelda(fila++, 1,  ""                               , letraCentrada );
            xls.adicionarCelda(fila++, 1,  nvl( imp.getDescripcion()       ), letraCentrada );//Observacion de la planilla
            xls.adicionarCelda(fila++, 1,  nvl( imp.getFecha_eta()         ), letraCentrada );
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearHoja ....\n"  + ex.getMessage() );
        }
    }
    
    
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en cerrarArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    public String nvl(String value){
        return value!=null? value : "";
    }
    
    
    
    public void start(  Usuario user, String impos, Model model ) throws Exception{
        
        this.user    = user;
        this.impos = impos;
        this.procesoName = "Reporte Importacion";
        this.des = "Reporte Importacion";
        this.model = model;
        
        super.start();
    }
    
    public void run(){
        
        this.generarReporteImportacion();
    }
    
    /*
    public static void main(String[]hhh) throws Exception{
                                
        Usuario u = new Usuario();
        u.setDstrct("FINV");
        u.setLogin("OPEREZ");
        
        Model m = new Model();
        
        HReporteImportacion h = new HReporteImportacion();
        h.start( u, "IMP01",m );
        
    }
    */
}
