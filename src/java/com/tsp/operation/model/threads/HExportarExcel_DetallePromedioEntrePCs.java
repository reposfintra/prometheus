/*
 * Nombre        HExportarExcel_DetallePromedioEntrePCs.java
 * Descripci�n   Clase que permite generar el detalle del tiempor promedio entre 2
 *               puestos de control en formato excel.
 * Autor         Alejandro Payares
 * Fecha         20 de febrero de 2006, 08:54 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.ReporteExcelSOT;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Utility;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Iterator;

// EXCEL
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;


/**
 * Clase que permite generar el detalle del tiempor promedio entre 2
 * puestos de control en formato excel.
 * @author Alejandro Payares
 */
public class HExportarExcel_DetallePromedioEntrePCs extends ReporteExcelSOT {
    
    /**
     * El modelo de la aplicaci�n
     */
    private   Model       model;
    /**
     * El nombre de la unidad fisica donde ser� guardado el reporte.
     */
    private   String      unidad;
    /**
     * El nombre del archivo donde ser� guardado el reporte
     */
    private   String      fileName;
    /**
     * El usuario en sesi�n
     */
    private   Usuario     usuario;
    
        /**
     * Variable que indica la fila dentro de la hoja deexcel en donde se est� escribiendo la informaci�n
     */
    private int filaActual = -1;
    
    private String titulos [] = {"PLANILLA","DURACION","DEMORA","TOTAL"};
    private String [] campos = {"numpla","duracion","demora","total"};
    
    
    private String nombrePcOrigen;
    private String nombrePcDestino;
    
    
    /**
     * Crea una nueva instancia de HExportarExcel_DetallePromedioEntrePCs
     * @autor  Alejandro Payares
     */
    public HExportarExcel_DetallePromedioEntrePCs(Model model, Usuario user, String nombrePcOrigen, String nombrePcDestino) throws Exception{
        String hoy    = Utility.getDate(6).replaceAll("/","").replaceAll(":","_");
        this.fileName = "DetPromedio_"+hoy;
        this.model       = model;
        this.usuario     = user;
        String dir       = user.getLogin();
        this.nombrePcDestino = nombrePcDestino;
        this.nombrePcOrigen  = nombrePcOrigen;
        this.unidad      = model.DirectorioSvc.getUrl() + dir;
        //this.unidad = "c:";
        this.model.DirectorioSvc.create(dir);
        this.path        = unidad +"/"+ fileName + ".xls";
        initExcel(fileName);
    }
    
    /**
     * Es el encargado de iniciar el hilo que genera el reporte en el archivo excel
     */
    public void generar(){
        new Thread(this,fileName).start();
    }
    
    /**
     * Crea el encabezado del reporte
     * @throws Exception si alg�n error ocurre
     */
    private void crearEncabezado() throws Exception {
        int columna = 0;
        crearCelda(++filaActual, columna);
        HSSFCellStyle estilo = clonarEstilo(estiloBlancoGrande);
        quitarBordeAEstilo(estilo);
        cell.setCellStyle(estilo);
        cell.setCellValue("TRANSPORTES SANCHEZ POLO S.A");
        for( int i=1; i< 5; i++ ){
            crearCelda(filaActual, i);
        }
        crearCeldaCombinada(filaActual, columna, filaActual, 6, false);
        
        //this.prepared(++filaActual, 5);
        crearCelda(++filaActual, columna);
        estilo = clonarEstilo(estiloBlancoGrande);
        quitarBordeAEstilo(estilo);
        cell.setCellStyle(estilo);
        cell.setCellValue("DETALLE DEL PROMEDIO ENTRE 2 PUESTOS DE CONTROL");
        for( int i=1; i< 5; i++ ){
            crearCelda(filaActual, i);
        }
        crearCeldaCombinada(filaActual, columna, filaActual, 6, false);
        
        //this.prepared(++filaActual, 7);
        //this.prepared(filaActual+1, 7);
        
        // la fecha del reporte
        crearCelda(++filaActual, columna);
        cell.setCellStyle(this.estiloAzulGrande);
        cell.setCellValue("Fecha del reporte");
        crearCelda(filaActual+1, columna);
        cell.setCellStyle(this.estiloBlanco);
        cell.setCellValue(Utility.getDate(6));
        
        // El puesto de control de origen
        crearCelda(filaActual, ++columna);
        cell.setCellStyle(this.estiloAzulGrande);
        cell.setCellValue("Puesto de control de origen");
        crearCelda(filaActual+1, columna);
        cell.setCellStyle(this.estiloBlanco);
        cell.setCellValue(this.nombrePcOrigen);
        
        // El puesto de control de destino
        crearCelda(filaActual, ++columna);
        cell.setCellStyle(this.estiloAzulGrande);
        cell.setCellValue("Puesto de control de destino");
        crearCelda(filaActual+1, columna);
        cell.setCellStyle(this.estiloBlanco);
        cell.setCellValue(this.nombrePcDestino);
        
        // El promedio entre los 2 puestos de control
        crearCelda(filaActual, ++columna);
        cell.setCellStyle(this.estiloAzulGrande);
        cell.setCellValue("Promedio");
        crearCelda(filaActual+1, columna);
        cell.setCellStyle(this.estiloBlanco);
        cell.setCellValue(model.repTiemposPCService.obtenerPromedio());
        
        filaActual+=2;
    }
    
    /**
     * Crea los titulos del reporte
     * @throws Exception Si algun error ocurre
     */
    public void crearTitulos() throws Exception {
        filaActual++;
        int columna = -1;
        for(int i=0; i<titulos.length; i++ ){
            this.crearCelda(filaActual, ++columna);
            cell.setCellStyle(this.estiloAzul);
            cell.setCellValue(titulos[i]);
        }
    }
    
    public void run() {
        try {
            LinkedList datos = model.repTiemposPCService.obtenerDatos();
            this.crearEncabezado();
            //this.prepared(++filaActual, 1);
            this.crearCelda(++filaActual, 0);
            cell.setCellStyle(this.estiloNormalGrande);
            cell.setCellValue(datos.size()+" Registros encontrados");
            crearTitulos();
            Iterator devolIt = datos.iterator();
            while( devolIt.hasNext() ) {
                Hashtable fila = (Hashtable) devolIt.next();
                filaActual++;
                int columna = -1;
                for( int i = 0; i < campos.length; i++ ) {
                    String str = ((String)fila.get(campos[i]));
                    str = str == null? "":str;
                    str = str.equals("0099-01-01")?"":str;
                    crearCelda(filaActual, ++columna);
                    cell.setCellStyle(this.estiloBlanco);
                    cell.setCellValue(str.equals("")?"-":str);
                    
                }
            }
            model.repTiemposPCService.borrarDatos();
            save();
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
    }
    
}
