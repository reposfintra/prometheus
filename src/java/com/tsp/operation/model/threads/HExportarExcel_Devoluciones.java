/*
* Nombre        DevolucionesSearchAction.java
* Descripci�n   Crea el reporte en excel del reporte de devoluciones
* Autor         fvillacob
* Fecha         27 de julio de 2005, 12:16 PM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/


package com.tsp.operation.model.threads;




import com.tsp.util.*;
import com.tsp.util.Utility;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;


// EXCEL
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;


public class HExportarExcel_Devoluciones extends Thread{
    
    private   Model     model;
    private   String    unidad;
    private   String    path;
    private   String    fileName;
    private   String[]  argumentos;
    private   Usuario   usuario;
    private   Hashtable header;
    
    // Excel:
    private   HSSFWorkbook   libroExcel;
    private   HSSFSheet      hojaExcel;
    private   HSSFRow        row;
    private   HSSFCell       cell;
    private   HSSFFont       font;
    private   HSSFCellStyle  style;
    private   HSSFFont       font2;
    private   HSSFCellStyle  style2;
    
    
    public HExportarExcel_Devoluciones() {
        String hoy    = Utility.getDate(6).replaceAll("/","").replaceAll(":","_");
        this.fileName = "Devoluciones_"+hoy;
    }
    
    
    public void start(  Model model, Usuario user, Hashtable head, String[] args) throws Exception{
        try{
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            this.model       = model;
            this.argumentos  = args;
            this.usuario     = user;
            this.header      = head;
            String dir       = user.getLogin();
            this.unidad      = rb.getString("ruta") + "/exportar/migracion/" + dir;
            model.DirectorioSvc.create(dir);
            this.path        = unidad +"/"+ fileName + ".xls";
            //this.path        = "c:/exportar" +"/"+ fileName + ".xls";
            ////System.out.println("inicializando archivo excel...");
            initExcel();
            super.start();
        }catch(Exception e){ 
            e.printStackTrace();
            throw new Exception( " Hilo: "+ e.getMessage());
        }
    }
    
    
    public void initExcel(){
        this.libroExcel = new HSSFWorkbook();
        this.hojaExcel  = libroExcel.createSheet(fileName);
        this.row        = null;
        this.cell       = null;
        createStyle();
    }
    
    public void createStyle(){
        font = libroExcel.createFont();
        font.setFontHeightInPoints((short)10);        //tamano letra
        font.setFontName("Arial");                    //Tipo letra
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); //letra negrita
        font.setColor((short)0x1);                    //color letra blanca
        
        style = libroExcel.createCellStyle();
        style.setFont(font);
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern((short) style.SOLID_FOREGROUND);
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
        style.setAlignment(style.ALIGN_CENTER);
        
        
        font2 = libroExcel.createFont();
        font2.setFontHeightInPoints((short)10);        //tamano letra
        font2.setFontName("Arial");                    //Tipo letra
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); //letra negrita
        font2.setColor( HSSFColor.BLACK.index );       //color letra negra
        style2 = libroExcel.createCellStyle();
        style2.setFont(font2);
        style2.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
    }
    
    
    public void prepared(int fila, int columnas){
        row = hojaExcel.createRow((short)fila);
        for(int j=0;j<=columnas;j++)
            cell = row.createCell((short)j);
        row = hojaExcel.getRow((short)fila);
    }
    
    
    public void write(String dato, int columna){
        cell = row.getCell((short)columna) ;
        cell.setCellValue(dato);
    }
    
    
    public void title(){
        prepared(0,0);
        cell.setCellStyle(style2);
        write("TRANSPORTES SANCHEZ POLO - REPORTE DE DEVOLUCIONES",0);
    }
    
    
    public void titlePedido(){
        prepared(1,1);
        write("NUMERO DE PEDIDO",0);
        write(header.get("nroPedido").toString(),1);
    }
    
    
    public void titleNoPedido(){
        prepared(1,4);
        cell.setCellStyle(style2); write("CLIENTE",0);
        cell.setCellStyle(style2); write("FECHA INICIAL",1);
        cell.setCellStyle(style2); write("FECHA FINAL",2);
        cell.setCellStyle(style2); write("TIPO DE DEVOLUCION",3);
        cell.setCellStyle(style2); write("FECHA DE GENERACION",4);
        prepared(2,4);
        write(header.get("cliente").toString(),0);
        if( header.get("fechaini") != null ) {
            write(header.get("fechaini").toString(),1);
            write(header.get("fechafin").toString(),2);
        }
        write(header.get("tipoDevolucion").toString(),3);
        write( Utility.getDate(6),4);
    }
    
    
    
    public void titleColumnas(String [] campos, Hashtable titulos){
        prepared(4,campos.length-1);
        for(int i=0; i<campos.length; i++ ){
            write(titulos.get(campos[i]).toString(),i);
            cell.setCellStyle(style);
        }
    }
    
    
    public void run(){        
        try{
            model.reporteDevolucionesService.buscarDevoluciones(this.usuario, this.argumentos);
            title();
            if( header.get("nroPedido") != null ) titlePedido();
            else                                  titleNoPedido();
            List devolList = model.reporteDevolucionesService.obtenerDatosDevoluciones();
            String campos[] = model.reporteDevolucionesService.obtenerCamposReporteDevoluciones();
            Hashtable titulos = model.reporteDevolucionesService.obtenerTitulosDeReporteDevoluciones();
            int total = (devolList==null)?0:devolList.size();
            prepared(3,0);
            ////System.out.println("Registros Encontrados: "+ total );
            write("Registros Encontrados: "+String.valueOf( total ),0);
            titleColumnas(campos,titulos);
            if( devolList != null ){
                Iterator devolIt = devolList.iterator();
                int contadorDeFilas = 5;
                while( devolIt.hasNext() ){
                    Hashtable fila = (Hashtable) devolIt.next();
                    prepared(contadorDeFilas,campos.length);
                    ////System.out.println("Guardando fila "+contadorDeFilas+" en excel: "+fila);
                    for( int i = 0; i < campos.length; i++ ) {
                        Object x = fila.get(campos[i]);
                        x = x == null? "":x;
                        x = x.equals("0099-01-01")?"":x;
                        write(x+"",i);
                    }
                    contadorDeFilas++;
                }
            }
            ////System.out.println("guardando archivo excel");
            save();
        }catch(Exception e){
            e.printStackTrace();
            try{
                prepared(3,0);
                write("ERROR: "+e.getMessage(),0);
                save();
            }catch(Exception k){}
        }
    }
    
    public void save() throws Exception{
        try{
            FileOutputStream fo = new FileOutputStream(path);
            libroExcel.write(fo);
            fo.close();
            ////System.out.println("archivo guardado en: "+path);
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
}
