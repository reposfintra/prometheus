/*************************************************************
 * Nombre: ReporteMigracionCumplidos.java
 * Descripci�n: Hilo para crear el ReporteMigracionManifiesto
 * Autor: Ing. Jose de la rosa
 * Fecha: 24 de octubre de 2005, 11:39 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author  fily
 */
public class ReporteMigracionManifiesto extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String fechaI = "";
    String fechaF = "";
    private PrintWriter pw;
    private LogWriter   logTrans;
    private String path;
    public ReporteMigracionManifiesto () {
    }
    
    public void start (String id, String FechaI,String FechaF){
        this.id = id;
        this.fechaI = FechaI;
        this.fechaF = FechaF;
        this.procesoName = "Manifiesto Migrado";
        this.des = "Migracion de Manifiesto por fechas";
        super.start ();
    }
    
    public synchronized void run (){
        try{
            
            Model model = new Model ();
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            path = ruta + "/exportar/migracion/" + id;

            File archivo = new File( path );
            if (!archivo.exists()) archivo.mkdirs();
            initLog();
            
            model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, this.id);
            model.transitoService.BusquedaMinifiesto(fechaI, fechaF);
            Vector lista = (Vector) model.transitoService.getVector();
            Date hoy = new Date ();
            SimpleDateFormat s = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
            String fecha_hoy = s.format (hoy);
            String a�o = fecha_hoy.substring(0,4);
            String mes = fecha_hoy.substring(5,7);
            String dia = fecha_hoy.substring(8,10);
            if (lista!=null && lista.size ()>0){
                
                ResourceBundle rbT = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
                String pathT = rbT.getString ("ruta");
                File fileT = new File (pathT + "/exportar/migracion/" + id + "/");
                fileT.mkdirs ();
                String NombreArchivo = pathT + "/exportar/migracion/" + id + "/890103161"+mes+a�o+"MANIFIESTO"+".txt";
                PrintStream archivoT = new PrintStream (NombreArchivo);
                
                
                Iterator it = lista.iterator ();
                String datos1;
                String datos2;
                String datos3;
                String datos4;
                String datos5;
                String datos6;
                String datos7;
                String datos8;
                String datos9;
                String datos10;
                String datos11;
                String datos12;
                String datos13;
                String datos14;
                String datos15;
                String datos16;
                String datos17;
                String datos18;
                String datos19;
                String datos20;
                String datos21;
                String datos22;
                String datos23;
                String datos24;
               
                while(it.hasNext ()){
                    BeanGeneral Bean = (BeanGeneral) it.next ();
                    if(Bean.getValor_01().equals("")){
                        logTrans.log("No se encuentra valor de numero de manifiesto", logTrans.INFO);
                    }
                    datos1= this.TruncarDatos( Bean.getValor_01() , 15);
                   
                    if(Bean.getValor_02().equals("0099/01/01")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene fecha de expedicion", logTrans.INFO);
                    }
                    //datos2= this.agregarEspacios( Bean.getValor_02() , 14);
                    if(Bean.getValor_03().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene ciudad de Origen", logTrans.INFO);
                    }
                    datos3= this.TruncarDatos( Bean.getValor_03() , 8);
                    
                    if(Bean.getValor_04().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene ciudad de Destino", logTrans.INFO);
                    }
                    datos4= this.TruncarDatos( Bean.getValor_04() , 8);
                    
                    if(Bean.getValor_05().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene no tiene placa de vehiculo", logTrans.INFO);
                    }
                    datos5= this.TruncarDatos( Bean.getValor_05() , 6);
                    
                    if(Bean.getValor_09().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene tipo de identificacion del conductor", logTrans.INFO);
                    }
                    datos6= this.TruncarDatos( Bean.getValor_09() , 1);
                    
                    if(Bean.getValor_10().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene numero de identificacion del conductor", logTrans.INFO);
                    }
                    datos7= this.TruncarDatos( Bean.getValor_10() , 11);
                    
                    if(Bean.getValor_21().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene placa remolque o semi_remolque", logTrans.INFO);
                    }
                    datos8= this.TruncarDatos( Bean.getValor_21() , 6);
                    
                    if(Bean.getValor_22().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene tipo de carroceria del vehiculo", logTrans.INFO);
                    }
                    datos9= this.TruncarDatos( Bean.getValor_22() , 3);
                    
                    if(Bean.getValor_11().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene configuracion del vehiculo", logTrans.INFO);
                    }
                    datos10= this.TruncarDatos( Bean.getValor_11() , 4);
                    
                    if(Bean.getValor_12().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene peso total del vehiculo cuando esta vacio", logTrans.INFO);
                    }
                    datos11= this.TruncarDatos( Bean.getValor_12() , 5);
                    
                    if(Bean.getValor_06().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene valor total del viaje", logTrans.INFO);
                    }
                    datos12= this.TruncarDatos( Bean.getValor_06() , 12);
                    
                    if(Bean.getValor_25().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene valor de retefuente", logTrans.INFO);
                    }
                    datos13= this.TruncarDatos( Bean.getValor_25() , 12);
                    
                    if(Bean.getValor_26().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene valor de descuentos pactados", logTrans.INFO);
                    }
                    datos14= this.TruncarDatos( Bean.getValor_26() , 12);
                    
                    if(Bean.getValor_23().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene valor anticipo ", logTrans.INFO);
                    }
                    datos15= this.TruncarDatos( Bean.getValor_23() , 12);
                    
                    if(Bean.getValor_16().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene codigo de la ciudad donde se efectura el pago", logTrans.INFO);
                    }
                    datos16= this.TruncarDatos( Bean.getValor_16() , 8);
                    
                    if(Bean.getValor_27().equals("16/01/0099")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene fecha de pago", logTrans.INFO);
                    }
                    datos17= this.agregarEspacios( Bean.getValor_27() , 15);
                    
                    if(Bean.getValor_34().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene el nombre de la persona que efectura el pago de cargue", logTrans.INFO);
                    }
                    datos18= this.TruncarDatos( Bean.getValor_34() , 25);
                    
                    if(Bean.getValor_34().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene el nombre de la persona que realizara el pago de descargue", logTrans.INFO);
                    }
                    datos19= this.TruncarDatos( Bean.getValor_34() , 25);
                    
                    if(Bean.getValor_20().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene tipo de identficacion de compa�ia aseguradora", logTrans.INFO);
                    }
                    datos20= this.TruncarDatos( Bean.getValor_20() , 1);
                    
                    if(Bean.getValor_18().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene numero de identificacion de la compa�ia aseguradora", logTrans.INFO);
                    }
                    datos21= this.TruncarDatos( Bean.getValor_18() , 11);
                    
                    if(Bean.getValor_17().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene numero de poliza de la carga", logTrans.INFO);
                    }
                    datos22= this.TruncarDatos( Bean.getValor_17() , 20);
                    
                    if(Bean.getValor_19().equals("16/01/0099")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene fecha vigencia de la poliza de la carga", logTrans.INFO);
                    }
                    datos23= this.agregarEspacios( Bean.getValor_19() , 15);
                    
                    if(Bean.getValor_08().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene estado actual", logTrans.INFO);
                    }
                    datos24= this.TruncarDatos( Bean.getValor_08() , 1);
                    
                      
                    
                    
                    archivoT.println (datos1 +" \t "+Bean.getValor_02()+" \t "+datos3+" \t "+ datos4+ " \t "+datos5 +
                                     " \t "+ datos6+  " \t "+datos7+ " \t "+datos8+ " \t "+datos9 +
                                     " \t "+datos10+" \t "+datos11+" \t "+datos12+" \t "+datos13+" \t "+datos14+
                                     " \t "+datos15+" \t "+datos16+ " \t "+datos16+" \t "+datos18+" \t "+datos19+
                                     " \t "+datos20+" \t "+datos21+" \t "+datos22+" \t "+datos23+" \t "+datos24);
                 
                }
                closeLog();
            }
            System.out.println("ya termino");
            model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                Model model = new Model ();
                //model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (),this.id,"ERROR :" + e.getMessage ());
                e.printStackTrace();
            }
            catch(Exception f){
                try{
                    Model model = new Model ();
                    model.LogProcesosSvc.finallyProceso (this.procesoName,this.hashCode (),this.id,"ERROR :");
                }catch(Exception p){ p.printStackTrace();   }
            }
        }
    }
    
    /*funciones para llenar */
    public String agregarEspacios(String valor, int longitud ) throws Exception {
       int tama�oValor = valor.length();
       int tama�o = longitud - tama�oValor;
       String vacios = "";
       String relleno = "";
        for(int i= 0;i< tama�o; i++){
            vacios = vacios+" "; 
        }
        relleno =  valor+ vacios;
        
        return relleno;
       
    }  
    
    public String TruncarDatos(String valor, int longitud ) throws Exception {
       String valorFinal = "";
       int tama�oValor = valor.length();
       String valorTruncado = "";
       if(tama�oValor > longitud ){
          valorTruncado = valor.substring(0, longitud);
       }else{
           valorTruncado = valor;
       }
       valorFinal = this.agregarEspacios(valorTruncado, longitud);
       return valorFinal;
       
    }  
    
    public void initLog()throws Exception{
        
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");
        pw     = new PrintWriter(new BufferedWriter(new FileWriter(path + "/logMigracionManifiestos_.txt")));
        logTrans = new LogWriter("Manifiesto_migrqaciones", LogWriter.INFO , pw );
        logTrans.log( "Proceso Inicializado", logTrans.INFO );
    }
    
    public void closeLog() throws Exception{
        logTrans.log( "Proceso Finalizado", logTrans.INFO );
        pw.close();
    }
    
    public static void main(String[]sfhgsd) throws Exception{
        ReporteMigracionManifiesto h = new ReporteMigracionManifiesto();
        h.start("HOSORIO","2007-04-17", "2007-04-30");
    }
    
}
