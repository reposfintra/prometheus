/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;


import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;

import java.util.*;
import java.text.*;


/**
 *
 * @author Alvaro
 */


public class HGenerarFacturaComisionEca extends Thread{


    private Model   model;
    private Usuario usuario;
    private String  dstrct;


    /** Creates a new instance of HFacturaApplusBonificacion */
    public HGenerarFacturaComisionEca () {
    }

    public void start(Model model, Usuario usuario, String distrito){

        this.usuario = usuario;
        this.model = model;
        this.dstrct = distrito;

        super.start();
    }


    public synchronized void run(){
        try{
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del reporte de prefactura : " + prefactura, usuario.getLogin());

            Vector comandos_sql =new Vector();
            String comandoSQL = "";

            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();

            List listaPrefacturaEca =  model.applusService.buscaPrefacturaComisionEca();

            if (listaPrefacturaEca.size() != 0){

                SerieGeneral serie = null;
                serie = new SerieGeneral();
                PrefacturaEca prefacturaEca =  new PrefacturaEca();

                Iterator it = listaPrefacturaEca.iterator();
                while (it.hasNext()) {

                    prefacturaEca = (PrefacturaEca)it.next();

                    // Datos comunes a la cabecera y a los items


                    // Cambiar el nit al de ECA
                    String proveedor = "8020076706";
                    String auxiliar  = proveedor;
                    String tipo_documento = "FAP";

                    // Buscar un consecutivo
                    serie = model.serieGeneralService.getSerie("FINV","OP","FAPEC");
                    model.serieGeneralService.setSerie("FINV","OP","FAPEC");
                    String documento_inicial = serie.getUltimo_prefijo_numero();

                    String user_update = usuario.getLogin();
                    String creation_user = user_update;
                    String base = "COL";

                    // CREACION DE LA CABECERA EN CXP_DOC

                    // Datos de la cabecera

                    int  id_orden     = prefacturaEca.getId_orden() ;
                    String num_os     = prefacturaEca.getNum_os();

                    String simbolo_variable     = prefacturaEca.getSimbolo_variable();
                    String descripcion_factura  = "NUMERO OS: " + num_os + "   ORDEN: " + id_orden +
                                                  "   SIMBOLO VARIABLE: " + simbolo_variable;

                    String facturaEca     = prefacturaEca.getFactura_eca();
                    
                    String agencia        = "BQ";
                    Proveedor objeto_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
                    //String handle_code = objeto_proveedor.getC_hc();
                    
                    String handle_code = "ME";
                    
                    String aprobador   = "JGOMEZ";
                    String usuario_aprobacion = "JGOMEZ";
                    String banco = objeto_proveedor.getC_branch_code();
                    String sucursal = objeto_proveedor.getC_bank_account();
                    String moneda = "PES";

                    String observacion = "";
                    String clase_documento = "4";
                    // String moneda_banco = objeto_proveedor.getC_currency_bank();
                    String moneda_banco = "PES";
                    String clase_documento_rel = "4";

                    // Calculo de la fecha de vencimiento de la factura//

                    String fecha_factura_eca = prefacturaEca.getFecha_factura_eca();

                    Calendar calendarFechaVencimiento = Calendar.getInstance();
                    String ano = fecha_factura_eca.substring(0,4);
                    String mes = fecha_factura_eca.substring(5,7);
                    String dia = fecha_factura_eca.substring(8,10);
                    calendarFechaVencimiento.set(Integer.parseInt(ano),Integer.parseInt(mes),Integer.parseInt(mes),8,0,0);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                    double e_comision_eca = prefacturaEca.getE_comision_eca();
                    double e_iva_comision_eca = prefacturaEca.getE_iva_comision_eca();
                    
                    // Divide los valores segun el numero de cuotas
                    double valor_factura = 0;


                    int cuotas_reales = prefacturaEca.getCuotas_reales();

                    double item_comision_eca              = Util.redondear2(e_comision_eca/cuotas_reales ,0) ;
                    double item_iva_comision_eca          = Util.redondear2(e_iva_comision_eca/cuotas_reales ,0) ;
                                        
                    // Inicializando variables que totalizan los items de todas las facturas
                    double total_item_comision_eca              = 0.0 ;
                    double total_item_iva_comision_eca          = 0.0 ;

                    
                    // Generar un numero de facturas igual al numero de cuotas reales

                    for(int i = 1 ;i < cuotas_reales ; i++) {

                        int k = 0;                            
                        
                        String documento = documento_inicial + "_" + Integer.toString(i);
                        
                        valor_factura    = item_comision_eca  +  item_iva_comision_eca ;
                        double vlr_neto  =  valor_factura;
                        double vlr_total_abonos = 0;
                        double vlr_saldo = vlr_neto;
                        double vlr_neto_me = vlr_neto;
                        double vlr_total_abonos_me = 0;
                        double vlr_saldo_me = vlr_neto;
                      
                        // Calculo de la fecha de vencimiento de la factura//


                        calendarFechaVencimiento.add(Calendar.DATE, 30);
                        String sFechaFacturaVencimiento = sdf.format(calendarFechaVencimiento.getTime()) ;                    

                        // CABECERA


                        String descripcion    = "Factura por comision e iva de factura : " + facturaEca+ "   " + descripcion_factura;
                        comandoSQL = model.applusService.setCxp_doc(dstrct, proveedor, tipo_documento, documento,
                                      "Cuota " + Integer.toString(i) + ":  " + descripcion,
                                      agencia, handle_code, aprobador, usuario_aprobacion, banco, sucursal,
                                      moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                                      vlr_total_abonos_me, vlr_saldo_me, 1.0, observacion, user_update,
                                      creation_user, base, clase_documento, moneda_banco,
                                      fecha_factura_eca, sFechaFacturaVencimiento, clase_documento_rel,
                                      creation_date, creation_date);

                       comandos_sql.add(comandoSQL);                        
  
                        // Item del comision
                        descripcion = "Comision eca: " + descripcion_factura ;

                        comandoSQL  = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++k),  descripcion,
                                         item_comision_eca, item_comision_eca, "23050705",
                                         user_update, creation_user, base, "086", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);                            
                        
                        
                        // Item del iva valor comision
                        descripcion = "Iva comision eca: " + descripcion_factura + "   Base: " +
                                       Double.toString(e_comision_eca) + "   ";

                        comandoSQL  = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++k),  descripcion,
                                         item_iva_comision_eca, item_iva_comision_eca, "23050705",
                                         user_update, creation_user, base, "087", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);                       
                        
                        // Suma de los items de todas las facturas

                       total_item_comision_eca              +=  item_comision_eca;
                       total_item_iva_comision_eca          +=  item_iva_comision_eca;
                    }                  

                   // Calculo de los items de la ultima factura

                   double ultimo_item_comision_eca              =  e_comision_eca - total_item_comision_eca;
                   double ultimo_item_iva_comision_eca          =  e_iva_comision_eca -  total_item_iva_comision_eca;


                   // Calculo los valores de la cabecera de la ultima factura

                   valor_factura = ultimo_item_comision_eca  +  ultimo_item_iva_comision_eca;


                   // Cabecera de la ultima factura

                   String leyenda = "";
                   if (cuotas_reales == 1) {
                       leyenda = "Cuota unica:" ;
                   }
                   else {
                       leyenda = "Cuota " + Integer.toString(cuotas_reales);
                   }

                   String documento = documento_inicial + "_" + Integer.toString(cuotas_reales);

                   // Calculo de la fecha de vencimiento de la factura//

                   calendarFechaVencimiento.set(Integer.parseInt(ano),Integer.parseInt(mes),Integer.parseInt(mes),8,0,0);
                   calendarFechaVencimiento.add(Calendar.DATE, 30);

                   String sFechaFacturaVencimiento = sdf.format(calendarFechaVencimiento.getTime()) ;

                   // CABECERA


                   String descripcion    = "Factura por comision e iva de factura : " + facturaEca+ "   " + descripcion_factura;
                   comandoSQL = model.applusService.setCxp_doc(dstrct, proveedor, tipo_documento, documento,
                                leyenda + ":  "  + descripcion,
                                agencia, handle_code, aprobador, usuario_aprobacion, banco, sucursal,
                                moneda, valor_factura, 0.0, valor_factura, valor_factura,
                                0.0, valor_factura, 1.0, observacion, user_update,
                                creation_user, base, clase_documento, moneda_banco,
                                fecha_factura_eca, sFechaFacturaVencimiento, clase_documento_rel,
                                creation_date, creation_date);

                   comandos_sql.add(comandoSQL);


                   // ITEMS

                   int k = 0;

                   // Item del comision
                   descripcion = "Comision eca: " + descripcion_factura ;

                   comandoSQL  = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                     tipo_documento, documento,Integer.toString(++k),  descripcion,
                                     ultimo_item_comision_eca, ultimo_item_comision_eca, "23050705",
                                     user_update, creation_user, base, "086", auxiliar,
                                     creation_date, creation_date);
                   comandos_sql.add(comandoSQL);

                   // Item del iva valor comision
                   descripcion = "Iva comision eca: " + descripcion_factura + "   Base: " +
                                   Double.toString(e_comision_eca) + "   ";

                   comandoSQL  = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                     tipo_documento, documento,Integer.toString(++k),  descripcion,
                                     ultimo_item_iva_comision_eca, ultimo_item_iva_comision_eca, "23050705",
                                     user_update, creation_user, base, "087", auxiliar,
                                     creation_date, creation_date);
                   comandos_sql.add(comandoSQL);



                   // ACTUALIZA EL NUMERO DE FACTURA ECA EN OFERTA

                   comandoSQL = model.applusService.setFacturaComisionEca(id_orden,documento,fecha_factura_eca);
                   comandos_sql.add(comandoSQL);

                }

                // Grabando todo a la base de datos.
                model.applusService.ejecutarSQL(comandos_sql);
            }


            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HGenerarFacturaComisionEca ...\n" + e.getMessage());
            }
        }
    }


}
