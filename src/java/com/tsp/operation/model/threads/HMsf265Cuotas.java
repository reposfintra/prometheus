/**
 * Nombre        HMsf265Cuotas.java
 * Descripción   Hilo que construye el archivo de migracion Msf265 para las cuotas de un prestamo
 * Autor         fvillacob
 * Fecha         19/02/2006
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 **/



package com.tsp.operation.model.threads;




import java.io.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;




public class HMsf265Cuotas extends Thread{
    
    private   Model           model;  
    private   String          user;
    private   String          nitUser;
    
    private   String          UNIDAD;
    private   String          PATH;
    private   String          FILENAME;
    private   FileWriter      file;
    private   BufferedWriter  buffer;
    private   PrintWriter     linea;
    private   String          SEPERADOR;

    private   String          NIT_FINTRA     = "802022016";
    private   String          ACCOUNT_FINTRA = "23050101029";  //PRESTAMOS.
    
    private    String        procesoName;
    
    
    private   Usuario   usuario;
    
    
    public HMsf265Cuotas() {
       FILENAME       = "Cuotas265_" + Util.getFechaActual_String(6).replaceAll("/|:",""); 
       SEPERADOR      = ",";
       procesoName    = "MIGRACION CUOTAS";
    }
    
    
    
    
     /**
     * Metodo que  ejecuta el hilo
     * @param ........Model modelo, String user
     * @version ...... 1.0
     */ 
    public  void start(Model modelo, Usuario usuar, String nitUsuario) throws Exception {
      try{
         this.model    = modelo;
         this.user     = usuar.getLogin();
         this.usuario  = usuar;
         //this.nitUser  = "79458875"; // Pablo Rosales  -- //nitUsuario;
         iniFile();
         super.start();
      }catch(Exception e){ throw new Exception ( " Hilo: "+ e.getMessage());}
    }
    
    
    
    
     /**
     * Metodo que define variables del archivo
     * @version ...... 1.0
     */ 
     public void iniFile() throws Exception{
      try{  
           model.DirectorioSvc.create(this.user);  
           
           this.UNIDAD      =  model.DirectorioSvc.getUrl()  +  this.user ;
           this.PATH        =  UNIDAD +"/"+ FILENAME + ".txt";
           
           this.file        =  new FileWriter    (this.PATH);
           this.buffer      =  new BufferedWriter(this.file);
           this.linea       =  new PrintWriter   (this.buffer);   
      }catch(Exception e){ throw new Exception ( " Thread: iniFile "+ e.getMessage());}
    }
     
     
     
     
     
     /**
     * Metodo que ejecuta el proceso de generacion del archivo
     * @version ...... 1.0
     */ 
    public void run(){
      try{
          String sql="";
          String comentario="EXITOSO";
          model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), " Migrar cuotas prestamos a Mims" , this.user ); 
          List lista = model.CuotasSvc.getListMigracion();
          if(lista.size()>0){
              this.linea.println( "Proveedor"      + SEPERADOR + 
                                  "Documento"      + SEPERADOR + 
                                  "Descripcion"    + SEPERADOR +
                                  "Valor PRestamo" + SEPERADOR +
                                  "Fecha Migracion"+ SEPERADOR );
            for(int i=0;i<lista.size();i++){
                Amortizacion amort = (Amortizacion)lista.get(i); 
                sql+=gererarRow(amort,this.usuario);
                sql+=model.CuotasSvc.updateMigracion(amort, user);
            }
          }
          System.out.println("sql --> "+sql);
          saveTXT();
          try{
              model.tService.crearStatement();    
              model.tService.getSt().addBatch(sql);
              model.tService.execute();
          }catch(Exception e){
              e.printStackTrace();
              throw new Exception(e.getMessage());
          }finally{
             model.tService.closeAll();
          }
          model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.user, comentario);
       }catch(Exception ex){}
    }
     
    
    
        
    
    
    
    /**
     * Metodo que permite generar los 2 registros por cada cuota
     * @parameter      Amortizacion amort
     * @version ...... 1.0
     */ 
    public String gererarRow(Amortizacion amort,Usuario usuario) throws Exception{
        String sql = "";
        String ABC = "";
        String TipoDoc="";
        
        try{
             String factura = "PR" + amort.getPrestamo() + "C" +  amort.getItem();
            String moneda_local = model.ingreso_detalleService.monedaLocal ("FINV");//usuario.getDstrct());
            double vlr_tasa = 1;
            OP op = null;
            String NitPro = ""+amort.getBeneficiario();
            //Hashtable  beneficiario =  model.CuotasSvc.getProveedor( amort.getBeneficiario() );
            
            double valorReanticipo = Math.round(amort.getTotalAPagar() * -1)  ;
            if ( valorReanticipo != 0){
            ////////////////////////////////////////////////////////////    
            // asignacion del valor del reanticipo
                String Fecha01 =  Util.getFechaActual_String(4);
                String Fecha02 =  Util.getFechaActual_String(1) + Util.getFechaActual_String(3);
                Proveedor prov = model.proveedorService.obtenerProveedorPorNit(NitPro);
                Tasa tasa = model.tasaService.buscarValorTasa(moneda_local, "PES", "PES", Util.getFechaActual_String(6)); 
                if (tasa != null)
                    vlr_tasa = tasa.getValor_tasa();
                //ABC = op2.getABC( usuario.getId_agencia() );
                CXP_Doc factur = new CXP_Doc();
                factur.setReg_status                                   ("");
                factur.setDstrct                                       ( usuario.getDstrct() );
                factur.setProveedor                                    (NitPro);
                factur.setTipo_documento                               ("010"); // Tipo_documento
                factur.setDocumento                                    (factura);
                factur.setObservacion                                  ("");
                factur.setDescripcion                                  (" CUOTA PRESTAMO " + amort.getTercero().toUpperCase() +" "+ factura);
                factur.setId_mims                                      ("");
                factur.setFecha_documento                              (Fecha01);
                /***********************************************************************************************/
                factur.setDocumento_relacionado                        ("");//"OP"+doc.getOC()
                factur.setTipo_documento_rel                           ("");
                /***********************************************************************************************/
                factur.setFecha_aprobacion                             (Fecha01+" 00:00:00");
                factur.setUsuario_aprobacion                           ("ADMIN");
                factur.setAprobador                                    ("ADMIN");
                factur.setFecha_vencimiento                            (Fecha01);
                factur.setUltima_fecha_pago                            ("0099-01-01 00:00:00");
                factur.setAgencia                                      ( prov.getC_agency_id() );
                factur.setBanco                                        ( prov.getC_branch_code() );
                factur.setSucursal                                     ( prov.getC_bank_account());
                //Si cambian los prestamos de moneda deven cambiaer las dos siguientes lineas
                factur.setMoneda                                       ("PES");
                factur.setMoneda_banco                                 ("PES");
                factur.setVlr_neto                                     (valorReanticipo );// aca va el valor neto calculado en esta infaz
                factur.setVlr_total_abonos                             (0);
                factur.setVlr_saldo                                    (valorReanticipo);// el mismo valor neto
                factur.setVlr_neto_me                                  (valorReanticipo);// el mismo valor neto
                factur.setVlr_total_abonos_me                          (0);
                factur.setVlr_saldo_me                                 (valorReanticipo);
                factur.setHandle_code                                  (prov.getHandle_code());
                factur.setTasa                                         (vlr_tasa);
                factur.setUsuario_contabilizo                          ("");
                factur.setFecha_contabilizacion                        ("0099-01-01 00:00:00");
                factur.setUsuario_anulo                                ("");
                factur.setFecha_anulacion                              ("0099-01-01 00:00:00");
                factur.setFecha_contabilizacion_anulacion              ("0099-01-01 00:00:00");
                factur.setNum_obs_autorizador                          (0);
                factur.setNum_obs_pagador                              (0);
                factur.setNum_obs_registra                             (0);
                factur.setCreation_user                                (usuario.getLogin());
                factur.setUser_update                                  (usuario.getLogin());
                factur.setBase                                         (usuario.getBase());  
                factur.setPeriodo                                      (Fecha02);
                factur.setFecha_vencimiento                            (Fecha01);
                factur.setClase_documento                              ("4");
                factur.setClase_documento_rel                          ("4");
                //se crean vacias la lista de impuestos,tipo impuestos e items
                Vector vTipImp  = new Vector();
                CXPItemDoc item = new CXPItemDoc();
                Vector vTipoImp = model.TimpuestoSvc.vTiposImpuestos();
                Vector vItems   = new Vector();
                // Se crea el Item unico
                item.setReg_status                                      ("");
                item.setDstrct                                          (usuario.getDstrct() );
                item.setProveedor                                       (NitPro);
                item.setTipo_documento                                  ("010"); // Tipo_documento       036 ("ND");
                item.setDocumento                                       (factura);
                item.setDescripcion                                     (" CUOTA PRESTAMO " + amort.getTercero().toUpperCase() +" "+ factura);
                item.setItem                                            ("001");
                item.setVlr                                             ( valorReanticipo );//valor moneda local  el mismo valor neto
                item.setVlr_me                                          ( valorReanticipo );//valor me del item   el mismo valor neto
                item.setCodigo_cuenta                                   ( ACCOUNT_FINTRA );
                item.setCodigo_abc                                      (ABC);
                item.setPlanilla                                        ("");
                item.setCodcliarea                                      ("");
                item.setTipcliarea                                      ("");
                item.setCreation_user                                   (usuario.getLogin());
                item.setUser_update                                     (usuario.getLogin());
                item.setBase                                            (usuario.getBase());
                item.setAuxiliar                                        ("");
                item.setTipoSubledger                                   ("");
                item.setConcepto                                        ("");//
                vItems.add(item);
                //retorna la consulat que crea el nuevo registro a cxp con todas las asociaciones.
                //--- Registro de Tercero:
                if( !amort.getTercero().toUpperCase().equals("FINV")){
                    sql += model.cxpDocService.insertarCXPDoc_SQL(factur,vItems,vTipImp,usuario.getId_agencia(),"" );
                    this.write(NitPro, factura," CUOTA PRESTAMO " + amort.getTercero().toUpperCase() +" "+ factura ,""+valorReanticipo ,Util.getFechaActual_String(6) );     
                }   
                ///////////////////////////////////////////////////////////////////////////////////////////////
                // Para que Fintra le cobre a TSP                                                            //
                ///////////////////////////////////////////////////////////////////////////////////////////////
                vItems.removeAllElements();
                valorReanticipo = Math.round(amort.getTotalAPagar());
                NitPro = NIT_FINTRA;
                Hashtable  fintra     =   model.CuotasSvc.getProveedor( NitPro);
                prov = model.proveedorService.obtenerProveedorPorNit(NitPro);
                factur.setProveedor                                    (NitPro);
                factur.setTipo_documento                               ("010"); // Tipo_documento
                factur.setVlr_neto                                     (valorReanticipo );// aca va el valor neto calculado en esta infaz
                factur.setVlr_saldo                                    (valorReanticipo);// el mismo valor neto
                factur.setVlr_neto_me                                  (valorReanticipo);// el mismo valor neto
                factur.setVlr_saldo_me                                 (valorReanticipo);
                factur.setHandle_code                                  (prov.getHandle_code());  
                factur.setAgencia                                      ( prov.getC_agency_id() );
                factur.setBanco                                        ( prov.getC_branch_code() );
                factur.setSucursal                                     ( prov.getC_bank_account());
                item.setProveedor                                       (NitPro);
                item.setTipo_documento                                  ("010");
                item.setVlr                                             ( valorReanticipo );//valor moneda local  el mismo valor neto
                item.setVlr_me                                          ( valorReanticipo );//valor me del item   el mismo valor neto
                item.setAuxiliar                                        ("");
                vItems.add(item);
                //--- Registro del beneficiario:
                sql += model.cxpDocService.insertarCXPDoc_SQL(factur,vItems,vTipImp,usuario.getId_agencia(),"" );   
                this.write(NitPro, factura," CUOTA PRESTAMO " + amort.getTercero().toUpperCase() +" "+ factura ,""+valorReanticipo ,Util.getFechaActual_String(6) );     
            }
        }catch(Exception e){
            this.write("Error en insercion de cuotas",""+e.getMessage(),"","","");
            throw new Exception(e.getMessage());    
        }
        return sql;
    }
    
    
    
    
    
    
    
    
    
    /**
     * Metodo que ecribe en el archivo
     * @param ........Amortizacion amort
     * @version ...... 1.0
     */ 
    public void write(String Proveedor , String Documento, String Descripcion, String ValorN, String FechaM ){
        
        String space     = "";
        
        this.linea.println(  Proveedor.trim()      + SEPERADOR + 
                             Documento.trim()      + SEPERADOR + 
                             Descripcion.trim()    + SEPERADOR +
                             ValorN.trim()         + SEPERADOR +
                             FechaM.trim()         + SEPERADOR );
        
    }
    
    
    
     /**
     * Metodo que guarda el archivo
     * @version ...... 1.0
     */ 
    public void saveTXT(){
        this.linea.close();
    }
     
     
}
