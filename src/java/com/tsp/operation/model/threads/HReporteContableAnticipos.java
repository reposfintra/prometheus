    /***************************************
    * Nombre Clase ............. HReporteContableAnticipos.java
    * Descripci�n  .. . . . . .  Permite Generar Reporte produccion 
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  22/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.threads;




import com.tsp.operation.model.beans.POIWrite;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import java.io.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;



public class HReporteContableAnticipos   extends Thread{
    
    
    private  Model        model;
    private  Usuario      usuario ;
    private  String       procesoName;

    
    private String          url;
    private POIWrite        Excel; 
    private String          hoja;
    private int             fila;
    private int             columna; 
    
    private HSSFCellStyle   texto;
    private HSSFCellStyle   negrilla;
    private HSSFCellStyle   numero; 
    private HSSFCellStyle   titulo;

    
    
    
    public HReporteContableAnticipos() {
    }
    
    
    
    
    /**
     * M�todo inicializa  el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void start(Model modelo,  Usuario user) throws Exception{
         try{
            this.model       = modelo;
            this.usuario     = user;
            this.procesoName = "GENERACION DE REPORTE PRODUCCION ";
            
            
            
            
            super.start();
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }

    
    
    
    
    
    
    /**
     * M�todo que ejecuta el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public synchronized void run(){
       try{
           
           String comentario="EXITOSO";
           model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode()," Reporte Produccion ", this.usuario.getLogin() ); 
           
           
           configXLS();
           createHoja("Produccion");
           titulo();
           
           List  lista   = model.ReporteContableAnticiposSvc.getListReporte();
           for(int i=0;i<lista.size();i++){
                 AnticiposTerceros anticipo = (AnticiposTerceros)lista.get(i);
                 addAnticipo( anticipo );  
           }
           
           Excel.cerrarLibro();   
           
           model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);           
           
             
       }catch(Exception e){
           try{
               model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage()); 
           }
           catch(Exception f){ }   
       }
       
    }
    
    
     
     
    
     
     
      /**
     * M�todo que establece titulos xls
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void titulo()throws Exception{
        try{            
            
            
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, model.ReporteContableAnticiposSvc.getNombreProveedor() +" "+ model.ReporteContableAnticiposSvc.getProveedor(),                this.negrilla ); incFila();
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "REPORTE DE PRODUCCION",                                         this.negrilla ); incFila();
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "RANGO FECHA " + model.ReporteContableAnticiposSvc.getFechas(),  this.negrilla ); incFila();
              
            
            incFila();
            incFila();
            setearColumna();
            
            
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "AGENCIA",             this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CONDUCTOR",           this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 6000 );  Excel.adicionarCelda( this.fila, this.columna, "NOMBRE CONDUCTOR",    this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 2000 );  Excel.adicionarCelda( this.fila, this.columna, "PROPIETARIO",         this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "NOMBRE PROPIETARIO",  this.titulo); incColumna();
            
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "PLACA",               this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "PLANILLA",            this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA ANTICIPO",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA PAGO",          this.titulo); incColumna();
            
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VALOR ANTICIPO",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "% DESCUENTO",         this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VALOR DESCUENTO",     this.titulo); incColumna();            
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VALOR NETO",          this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VALOR COMISION",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VALOR CONSIGNADO",    this.titulo); incColumna();
            
                        
            incFila();
            setearColumna();            
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    

    
    
     /**
     * M�todo que adiciona informacion del anticipo al reporte
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void addAnticipo(AnticiposTerceros anticipo)throws Exception{
        try{ 
            
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getNombreAgencia()           ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getConductor()               ,this.texto); incColumna();            
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getNombreConductor()         ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getPla_owner()               ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getNombrePropietario()       ,this.texto); incColumna();            
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getSupplier()                ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getPlanilla()                ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getFecha_anticipo()          ,this.texto); incColumna();            
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getFecha_transferencia()     ,this.texto); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna,  anticipo.getVlr()                    ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna,  anticipo.getPorcentaje()             ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna,  anticipo.getVlrDescuento()           ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna,  anticipo.getVlrNeto()                ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna,  anticipo.getVlrComision()            ,this.numero); incColumna();            
            Excel.adicionarCelda( this.fila, this.columna,  anticipo.getVlrConsignar()           ,this.numero); incColumna();
            
            
            
            incFila();
            setearColumna();            
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }

    
    
    
    
    
     
    
    
    /**
     * M�todo que configura parametros de Excel
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public void configXLS() throws Exception{
       try{          
            DirectorioService dd = new DirectorioService();
            dd.create( this.usuario.getLogin()  );
            url = dd.getUrl() +  this.usuario.getLogin() + "/RepProduccion"+ Util.getFechaActual_String(6).replaceAll("/|:| ","") + ".xls";
            
            this.Excel  = new POIWrite(url);
            createStyle();
            
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }

    
      
     
    /**
     * M�todo que Define estilo del archivo xls
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void createStyle()throws Exception{
       try{
          this.texto       = Excel.nuevoEstilo("Tahoma", 7,   false , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);
          this.numero      = Excel.nuevoEstilo("Tahoma", 7,   false , false, ""            , Excel.NONE , Excel.NONE , HSSFCellStyle.ALIGN_RIGHT);
          this.titulo      = Excel.nuevoEstilo("Tahoma", 8,   true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLACK.index  , HSSFCellStyle.ALIGN_CENTER);              
          this.negrilla    = Excel.nuevoEstilo("Tahoma", 10,  true  , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);                     
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }

    
    
    
     /**
     * M�todo que crea la hoja
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void createHoja(String nombreHoja)throws Exception{
        try{
            
            hoja = nombreHoja;
            this.Excel.obtenerHoja(hoja);             
            setearFila();
            setearColumna();
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }

    
    public void incFila()      {  this.fila++;      }
    public void incColumna()   {  this.columna++;   }    
    public void setearFila()   {  this.fila=0;      }
    public void setearColumna(){  this.columna=0;   }
    

    
    
    
}
