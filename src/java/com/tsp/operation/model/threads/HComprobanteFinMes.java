/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;

import java.util.*;
import java.text.*;
import java.io.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;
import com.tsp.util.Util;


import com.tsp.util.LogWriter;




/**
 * Genera un comprobante contable de fin de mes para cerrar las cuentas de tipo I,C,G contra las cuentas puc
 * @param   model Variable propia del usuario para acceder en forma segura a los metodos y propiedades exclusivas del usuario
 * @param   usuario Objeto Usuario que identifica al usuario en entra en sesion
 * @param   distrio Compania asociada al usuario
 * @param   anio Ano del reporte
 * @param   mes  Mes del reporte
 * @param   opcion Generar= Se genera el comprobante, Eliminar= Se elimina el comprobante si existe
 * @author  Alvaro Pabon Martinez
 * @version %I%, %G%
 * @since   1.0
 *
 */
public class HComprobanteFinMes extends Thread {


    // Parametros recibidos
    private Model   model;
    private Usuario usuario;
    private String  distrito;
    private String anio;
    private String mes;
    private String opcion;


    // Variables
    String     rutaInformes;


    // Constantes
    final String NUMERO_CD = "9998";        // Numero del comprobante diario de fin de mes


    /** Creates a new instance of HPrefacturaDetalle */
    public HComprobanteFinMes () {
    }

    public void start(Model model, Usuario usuario, String distrito, String anio, String mes, String opcion){

        this.usuario  = usuario;
        this.model    = model;
        this.distrito = distrito;
        this.anio   = anio;
        this.mes    = mes;
        this.opcion = opcion;

        super.start();
    }




    public synchronized void run(){

        try{

            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion ingresos a partir de egresos TSP " + usuario.getLogin());


            // Genera el proceso para el cuerpo del reporte
            if(opcion.equalsIgnoreCase("GENERAR")){
                generarComprobante(model,usuario,distrito,anio,mes);
            }
            else if(opcion.equalsIgnoreCase("ELIMINAR")){
                eliminarComprobante(model,usuario,distrito,anio,mes);
            }




            // model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HComprobanteFinMes ...\n" + e.getMessage());
            }
        }
    }

    /**
     * Ubica la informacion de la ruta donde quedara el informe
     * Si no existe crea el directorio
     *
     * @author  Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since   1.0
     *
     */

    public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( rutaInformes );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }

    /**
     * Crea la informacion que va en las columnas
     *
     * @param   model Variable propia del usuario para acceder en forma segura a los metodos y propiedades exclusivas del usuario
     * @param   usuario Objeto Usuario que identifica al usuario en entra en sesion
     * @param   distrio Compania asociada al usuario
     * @param   wb Variable que identifica el acceso al libro de excell
     * @param   rutaInformes Ruta del directorio donde quedara grabado el informe
     * @param   sheet Varibale que identifica el acceso a la hoja de excell donde esta el informe
     * @param   fila Numero de fila inicial del excell donde se registraran los datos
     * @see     ClaseRegistro
     * @see     ElementoFormula
     * @see     ControlNivel
     * @author  Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since   1.0
     *
     */


    public void generarComprobante(Model model,Usuario usuario,String distrito, String anio, String mes) throws Exception{



        // -------------------------------------------------------------------------
        // DEFINICION DEL LOG DE ERROR
        // Variables
        PrintWriter pw;
        LogWriter logWriter;
        java.text.SimpleDateFormat formatoFecha;
        String fechaDocumento;
        java.util.Date fecha;


        // Abriendo un log de error

        this.generarRUTA();   // crear el directorio donde va a incluirse el archivo excell

        File file = new File(rutaInformes);
        file.mkdirs();
        pw        = new PrintWriter(System.err, true);

        Calendar fechaProceso  = Calendar.getInstance();
        fecha                  = fechaProceso.getTime();
        formatoFecha           = new java.text.SimpleDateFormat("yyyy-MM-dd HHmm");
        fechaDocumento         = formatoFecha.format(fecha);

        String logFile   = rutaInformes + "/"+usuario.getBd()+"_Comprobante cierre "+fechaDocumento+".txt";

        pw        = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("EF-FINV", LogWriter.INFO, pw);
        logWriter.setPrintWriter(pw);

        // FIN DEFINICION DEL LOG DE ERROR
        // -------------------------------------------------------------------------


        try {

            logWriter.log("******************************************************************",LogWriter.INFO);
            logWriter.log("*  PROCESO PARA GENERACION DE COMPROBANTE CONTABLE DE FIN DE MES *",LogWriter.INFO);
            logWriter.log("*  FechaProceso :  "+fechaDocumento+"                            *",LogWriter.INFO);
            logWriter.log("*  Empresa :  "+usuario.getBd()+"                                *",LogWriter.INFO);
            logWriter.log("******************************************************************" +"\n",LogWriter.INFO);

            // Crea una lista de movimientos del generador financiero con la tabla mayor y mayor subledger

            logWriter.log("VERIFICACION DE CUADRE CONTABLE ENTRE MAYOR,MOVIMIENTO  \n",LogWriter.INFO);

            // Revisa si todo los comprobantes contable estan aplicados y cuadrados

            String mesCaracter = Util.ceroPad(Integer.parseInt(mes),2);
            List listaTotales = model.estadoFinancieroService.validaCuadreContable(distrito,anio,mesCaracter) ;

            TotalesContables totalesContables = new TotalesContables();
            if(listaTotales.size()> 0){

                Iterator it = listaTotales.iterator();
                boolean primerRegistro = true;
                double valor_debito = 0.00;
                double valor_credito = 0.00;
                double valor_diferencia = 0.00;
                double valor_base = 0.00;
                boolean cuadreContable = true;
                String descripcion = "";

                while (it.hasNext()) {
                   totalesContables = (TotalesContables)it.next();
                   descripcion = totalesContables.getDescripcion();
                   valor_debito     = totalesContables.getValor_debito();
                   valor_credito    = totalesContables.getValor_credito();
                   valor_diferencia = totalesContables.getValor_diferencia();

                   logWriter.log(Util.padRight(descripcion, 45 ) + ":   Debito: " + Util.FormatoMiles(valor_debito) + "   Credito: " + Util.FormatoMiles(valor_credito) + "   Diferencia: " + Util.FormatoMiles(valor_diferencia)  ,LogWriter.INFO);

                   if (primerRegistro){
                       primerRegistro = false;
                       valor_base = totalesContables.getValor_debito();
                   }
                   if(valor_base != valor_debito) {
                       cuadreContable = false;
                   }
                   if(valor_base != valor_credito) {
                       cuadreContable = false;
                   } 
                }

                if(cuadreContable == false){
                    logWriter.log("EL COMPROBANTE CONTABLE DE CIERRE DE FIN DE MES NO SE GENERO ",LogWriter.INFO);
                    logWriter.log("Revise las cifras debito, credito y diferencia " +"\n",LogWriter.INFO);

                }
                else {

                    // Valida que no exista ya un comprobante de cierre
                    String documento ="CD"+ anio.substring(2) + Util.ceroPad(Integer.parseInt(mes),2) + NUMERO_CD;
                    if (model.estadoFinancieroService.validaExistenciaComprobante(distrito, "CDIAR", documento) ) {
                        logWriter.log("EL COMPROBANTE CONTABLE DE CIERRE DE FIN DE MES NO SE GENERO ",LogWriter.INFO);
                        logWriter.log("PORQUE YA EXISTIA UN COMPROBANTE CON EL MISMO NUMERO:  " + documento ,LogWriter.INFO);
                        logWriter.log("Consulte el comprobante y si es de cierre de fin de mes puede utilizar la opcion de eliminar" +"\n",LogWriter.INFO);
                    }

                    // Generar el comprobante
                    CreaComprobante(distrito, usuario, anio, mes, logWriter);
                }


            }


            java.text.SimpleDateFormat formatoFechaFinal;
            Calendar fechaFinalProceso  = Calendar.getInstance();
            fecha                       = fechaFinalProceso.getTime();
            formatoFechaFinal           = new java.text.SimpleDateFormat("yyyy-MM-dd HH.mm:ss");
            fechaDocumento              = formatoFechaFinal.format(fecha);



            logWriter.log("****************************************************************",LogWriter.INFO);
            logWriter.log("*  FINAL DEL PROCESO " + fechaDocumento + "                       *",LogWriter.INFO);
            logWriter.log("****************************************************************",LogWriter.INFO);

        }catch (Exception e){
            System.out.println("Error HComprobanteFinMes, procedimiento generarComprobante  ...\n"  + e.getMessage());
        }

    }

    public void CreaComprobante(String distrito, Usuario usuario, String anio, String mes, LogWriter logWriter) {

        String comandoSQL = "";
        Vector comandos_sql =new Vector();

        try {
            boolean validacionOK = true;
            String mesCaracter  = Util.ceroPad(Integer.parseInt(mes),2);
            model.estadoFinancieroService.creaResumenPuc(distrito, anio, mesCaracter);
            int grupoTransaccion = model.estadoFinancieroService.getSecuencia("con.comprobante_grupo_transaccion_seq");
            String detalle = "Registro realocacion mvto cuentas de resultado del mes " + mesCaracter;
            comandoSQL = model.estadoFinancieroService.setCabecera(distrito, usuario.getLogin(), anio, mesCaracter, grupoTransaccion, detalle, NUMERO_CD);
            if(comandoSQL.equals("") ){
                validacionOK = false;
                logWriter.log("EL COMPROBANTE CONTABLE DE CIERRE DE FIN DE MES NO SE GENERO ",LogWriter.INFO);
                logWriter.log("PORQUE HUBO ERROR EN LA CREACION DE LA CABECERA " ,LogWriter.INFO);
            }
            else{
               comandos_sql.add(comandoSQL);
            }

            comandoSQL = model.estadoFinancieroService.setDetalle(distrito, usuario.getLogin(), anio, mesCaracter, grupoTransaccion, detalle, NUMERO_CD);
            if(comandoSQL.equals("") ){
                validacionOK = false;
                logWriter.log("EL COMPROBANTE CONTABLE DE CIERRE DE FIN DE MES NO SE GENERO ",LogWriter.INFO);
                logWriter.log("PORQUE HUBO ERROR EN LA CREACION DEL DETALLE " ,LogWriter.INFO);
            }            
            else{
               comandos_sql.add(comandoSQL);
            }

            // Actualizando el mayor con el nuevo comprobante de cierre
            String[] SQL = model.estadoFinancieroService.setMayor(distrito, usuario.getLogin(), anio, mesCaracter, grupoTransaccion, NUMERO_CD).split(";");
            if(SQL.length <= 0 ){
                validacionOK = false;
                logWriter.log("EL COMPROBANTE CONTABLE DE CIERRE DE FIN DE MES NO SE GENERO ",LogWriter.INFO);
                logWriter.log("PORQUE HUBO ERROR EN EL QUERY DE ACTUALIZACION AL MAYOR " ,LogWriter.INFO);
            }
            else{
                for (int i = 0; i < SQL.length; i++) {
                    comandos_sql.add(SQL[i]);
                }
              
            }


            // Grabando todo a la base de datos.
            if (validacionOK){
                model.applusService.ejecutarSQL(comandos_sql);
                comandos_sql.removeAllElements();

                logWriter.log("EL COMPROBANTE CONTABLE DE CIERRE DE FIN DEBIO HABERSE GENERADO ",LogWriter.INFO);
                logWriter.log("REVISE DOCUMENTO: CDIAR  CD" + anio.substring(2) + mesCaracter + NUMERO_CD ,LogWriter.INFO);

            }


        }catch (Exception e){
            System.out.println("Error HComprobanteFinMes, procedimiento CreaComprobante  ...\n"  + e.getMessage());
        }
    }




    public void eliminarComprobante(Model model,Usuario usuario,String distrito, String anio, String mes) throws Exception{

        String comandoSQL = "";
        Vector comandos_sql =new Vector();

        // -------------------------------------------------------------------------
        // DEFINICION DEL LOG DE ERROR
        // Variables
        PrintWriter pw;
        LogWriter logWriter;
        java.text.SimpleDateFormat formatoFecha;
        String fechaDocumento;
        java.util.Date fecha;


        // Abriendo un log de error

        this.generarRUTA();   // crear el directorio donde va a incluirse el archivo excell

        File file = new File(rutaInformes);
        file.mkdirs();
        pw        = new PrintWriter(System.err, true);

        Calendar fechaProceso  = Calendar.getInstance();
        fecha                  = fechaProceso.getTime();
        formatoFecha           = new java.text.SimpleDateFormat("yyyy-MM-dd HHmm");
        fechaDocumento         = formatoFecha.format(fecha);

        String logFile   = rutaInformes + "/"+usuario.getBd()+"_Comprobante cierre "+fechaDocumento+".txt";

        pw        = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("EF-FINV", LogWriter.INFO, pw);
        logWriter.setPrintWriter(pw);

        // FIN DEFINICION DEL LOG DE ERROR
        // -------------------------------------------------------------------------

        boolean validacionOK = true;
        try {

            logWriter.log("******************************************************************",LogWriter.INFO);
            logWriter.log("*  PROCESO PARA ELIMINAR EL  COMPROBANTE CONTABLE DE FIN DE MES  *",LogWriter.INFO);
            logWriter.log("*  FechaProceso :  "+fechaDocumento+"                            *",LogWriter.INFO);
            logWriter.log("*  Empresa :  "+usuario.getBd()+"                                *",LogWriter.INFO);
            logWriter.log("******************************************************************" +"\n",LogWriter.INFO);

            // Valida que exista ya un comprobante de cierre
            String documento ="CD"+ anio.substring(2) + Util.ceroPad(Integer.parseInt(mes),2) + NUMERO_CD;
            if (model.estadoFinancieroService.validaExistenciaComprobante(distrito, "CDIAR", documento) ) {


                // Actualizando el mayor con la reversion del comprobante de cierre
                String[] SQL = model.estadoFinancieroService.setReversaMayor(distrito, usuario.getLogin(), anio, Util.ceroPad(Integer.parseInt(mes),2), NUMERO_CD ).split(";");
                if(SQL.length <=0 ){
                    validacionOK = false;
                    logWriter.log("EL COMPROBANTE CONTABLE NO SE REVERSO  ",LogWriter.INFO);
                    logWriter.log("PORQUE HUBO ERROR EN EL QUERY DE REVERSION AL MAYOR " ,LogWriter.INFO);
                }
                else{
                  for(int i = 0; i < SQL.length; i++) {
                    comandos_sql.add(SQL[i]);
                  }
                }

                // Eliminar el comprobante
                SQL = model.estadoFinancieroService.eliminaComprobante("CDIAR", documento).split(";");
                if(SQL.length <=0 ){
                    validacionOK = false;
                    logWriter.log("EL COMPROBANTE CONTABLE DE CIERRE DE FIN DE MES NO SE GENERO ",LogWriter.INFO);
                    logWriter.log("PORQUE HUBO ERROR EN EL QUERY DE ELIMINACION DEL COMPROBANTE " ,LogWriter.INFO);
                }
                else{
                  for(int i = 0; i < SQL.length; i++) {
                    comandos_sql.add(SQL[i]);
                  }
                }


                // Grabando todo a la base de datos.
                if (validacionOK){
                    model.applusService.ejecutarSQL(comandos_sql);
                    comandos_sql.removeAllElements();

                    logWriter.log("EL COMPROBANTE CONTABLE DE CIERRE DE FIN DEBIO HABERSE ELIMINADO ",LogWriter.INFO);
                    logWriter.log("REVISE DOCUMENTO: CDIAR  CD" + anio.substring(2) + mes + NUMERO_CD ,LogWriter.INFO);

                }


            }
            else {
                logWriter.log("EL COMPROBANTE CONTABLE DE CIERRE DE FIN DE MES NO SE ELIMINO ",LogWriter.INFO);
                logWriter.log("PORQUE NO EXISTE UN COMPROBANTE CON EL NUMERO:  " + documento ,LogWriter.INFO);
            }

            java.text.SimpleDateFormat formatoFechaFinal;
            Calendar fechaFinalProceso  = Calendar.getInstance();
            fecha                       = fechaFinalProceso.getTime();
            formatoFechaFinal           = new java.text.SimpleDateFormat("yyyy-MM-dd HH.mm:ss");
            fechaDocumento              = formatoFechaFinal.format(fecha);



            logWriter.log("****************************************************************",LogWriter.INFO);
            logWriter.log("*  FINAL DEL PROCESO " + fechaDocumento + "                       *",LogWriter.INFO);
            logWriter.log("****************************************************************",LogWriter.INFO);

        }catch (Exception e){
            System.out.println("Error HComprobanteFinMes, procedimiento eliminarComprobante  ...\n"  + e.getMessage());
        }

    }
}









