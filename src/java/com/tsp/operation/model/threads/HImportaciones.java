/*************************************************************
 * Nombre      ............... HImportaciones.java
 * Descripcion ............... Clase general de importaciones
 * Autor       ............... mfontalvo
 * Fecha       ............... Octubre - 01 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.operation.model.threads;


import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.POIWrite;

import java.io.*;
import java.util.*;



/**
 * Clase de general de Importacion de archivos
 */
public class HImportaciones extends Thread {
    
    /**
     * declaracion variables generales
     */
    private List        listado;
    private Model       model;
    private Usuario     usuario;
    private TreeMap     constantes;
    private String      tipoImportacion;
    
    
    
    /**
     * declaracion de flujos de salida del archivo
     */
    private PrintWriter pw;
    private LogWriter   logImp;
    private String      path;
    private String      filename;
    
    
    
    /** Crea una nueva instancia de HImportaciones */
    public HImportaciones() {
        
        filename = "";
        listado  = null;
        model    = null;
        usuario  = null;
        
    }
        
    
    /**
     * Metodo Inicial del hilo
     * @autor mfontalvo
     * @see start principal del hilo.
     * @param model modelo de la session
     * @param usuario usuario del proceso o session
     * @param nombreArchivo Nombre del archivo o ruta delos archivos a importar
     * @param constantes Constantes de Importacion definidos por session
     */
    public void start ( Model model, Usuario usuario, String nombreArchivo, TreeMap constantes, String tipo){
        
        // inicializando parametros del hilo
        
        this.model    = model;
        this.usuario  = usuario;
        this.filename = nombreArchivo; 
        this.constantes = constantes;
        this.tipoImportacion = tipo;
        
        super.start();
    }
    
       
    
    
    /**
     * Metodo de Procesamiento general de importacion.
     * @autor mfontalvo
     * @see initLog().
     * @see procesarArchivo(File).
     * @see closeLog().
     */
    public synchronized void run (){
        try{
            
            model.LogProcesosSvc.InsertProceso("HImportaciones",this.hashCode(), "Log de Importaciones", usuario.getLogin());
            // ubicando directorio para realizar consulta
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            path = ruta + "/exportar/migracion/" + usuario.getLogin();
            
            
            File archivo = new File( path + "/" + filename );
            archivo.mkdirs();        
            
            initLog();  // preparando archivo de log
            ////////////////////////////////////////////////////
            
            if (archivo.isDirectory()){
               // en caso de ser un directorio se recorren todos los archivo 
               File [] arc = archivo.listFiles( );
               for (int i = 0; i < arc.length; i++)
                   procesarArchivo ( arc[i] );
               
            }
            else
               procesarArchivo ( archivo );                
            
            /////////////////////////////////////////////////////
            closeLog(); // cerrando archivo de log
            model.LogProcesosSvc.finallyProceso("HImportaciones", this.hashCode(), usuario.getLogin(), "Exitoso");
        }catch (Exception ex){
            ////System.out.println ( "Error en el hilo HImportaciones ...\n" + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso("HImportaciones", this.hashCode(), usuario.getLogin(), "Error en el hilo HImportaciones ...\n" + ex.getMessage());
            } catch (Exception e){}
        }
    }
    
    
    
    
    /**
     * Metodo que procesa los archivos de manera independiente
     * @autor mfontalvo
     * @see procesarCSV(File).
     * @see procesarXLS(File).
     * @see procesarXLS(File).
     * @see model.ImportacionSvc.importar (List, File, TreeMap).
     * @see generarLogArchivo(String, TreeMap).
     * @param file archivo a procesar
     * @throws Exception .
     * @fecha 2005-10-01
     */
    public void procesarArchivo (File file) throws Exception{
        
        boolean procesado = true;
        String name = "";
        String ext  = "";
        try{
            
            /////////////////////////////////////////////////////////////////////
            
            int pos = file.getName().lastIndexOf(".");
            if (pos!=-1) {
                name = file.getName().substring(0,pos);
                ext  = file.getName().substring(pos+1, file.getName().length());
            }
                
            model.LogProcesosSvc.updateProceso("HImportaciones", this.hashCode(), usuario.getLogin(), "Archivo  :" + file.getName());
            logImp.log("Archivo  :" + file.getName(), logImp.INFO);     
            logImp.log("Tipo proceso seleccionado : " + tipoImportacion , logImp.INFO);
            
            
            if ( ext.toLowerCase().equals("csv") )
                procesarCSV( file );
            else if ( ext.toLowerCase().equals("xls") )
                procesarXLS( file );
            else if ( ext.toLowerCase().equals("xml") )
                procesarXML( file );
            else 
                procesado = false;
            /////////////////////////////////////////////////////////////////////
            
        } catch (Exception ex){
            logImp.log(ex.getMessage(),logImp.ERROR );
            model.LogProcesosSvc.updateProceso("HImportaciones", this.hashCode(), usuario.getLogin(), ex.getMessage());
        }

        if ( procesado ) {
            if (listado!=null && listado.size()>0){
                
                logImp.log("Registros a procesar : " + listado.size() , logImp.INFO);
                
                /////////////////////////////////////////////////////////////
                //intenta procesar el listado en la base de datos
                try{
                    if (tipoImportacion.equalsIgnoreCase("Insercion"))
                        model.ImportacionSvc.importarInsert( listado, file, constantes, usuario );
                    else if (tipoImportacion.equalsIgnoreCase("Actualizacion"))
                        model.ImportacionSvc.importarUpdate( listado, file, usuario);
                    
                    logImp.log("Procesado.", logImp.INFO);
                    model.LogProcesosSvc.updateProceso("HImportaciones", this.hashCode(), usuario.getLogin(), "Procesado");
                    
                    
                    
                    TreeMap errores = model.ImportacionSvc.getErrores();
                    if (!errores.isEmpty()){
                        generarLogArchivo(name, errores);
                        if (tipoImportacion.equalsIgnoreCase("Insercion")){
                            model.LogProcesosSvc.updateProceso("HImportaciones", this.hashCode(), usuario.getLogin(), "Se genero un reporte de Registros No Montados ("+ name +"_RNM.xls).");
                            logImp.log("Se genero un reporte de Registros No Montados ("+ name +"_RNM.xls).", logImp.INFO);
                        }
                        else if (tipoImportacion.equalsIgnoreCase("Actualizacion")){
                            model.LogProcesosSvc.updateProceso("HImportaciones", this.hashCode(), usuario.getLogin(), "Se genero un reporte de estado de registros procesados ("+ name +"_ERP.xls).");
                            logImp.log("Se genero un reporte de Estados de Registros Procesados ("+ name +"_ERP.xls).", logImp.INFO);
                        }
                    }
                    
                }catch (Exception ex){
                    logImp.log(ex.getMessage(),logImp.ERROR );
                    model.LogProcesosSvc.updateProceso("HImportaciones", this.hashCode(), usuario.getLogin(), ex.getMessage());
                }
                /////////////////////////////////////////////////////////////
            }
            else {
                logImp.log("No se encontraron datos.", logImp.INFO);
                model.LogProcesosSvc.updateProceso("HImportaciones", this.hashCode(), usuario.getLogin(), "No se encontraron datos");
            }
        }
        
    }
      
    /**
     * Metodo que procesa los archivos csv
     * @param f archivo scv a procesar
     * @throws Exception .
     */    
    public void procesarCSV(File f) throws Exception{
        try {
            listado = new LinkedList();
            CSVRead csv = new CSVRead( f.getAbsolutePath() );
            String [] datos = null;
            while (  !isEmpty(datos = csv.getValores()) ){
                listado.add(datos);
            }
            csv.close();
        }catch (Exception ex){
            throw new Exception("Error en la rutina procesarCSV en [HImportaciones] ....\n" + ex.getMessage());
        }
    }
    
    /**
     * Metodo que procesa los archivos de excel
     * @param f archivo xls a procesar
     * @throws Exception .
     */    
    public void procesarXLS(File f) throws Exception{
        try {
            listado = new LinkedList();
            JXLRead xls = new JXLRead (f.getAbsolutePath() );
            xls.obtenerHoja("BASE");
            String [] datos = null;
            int i = 0;
            while (  !isEmpty (datos = xls.getValores(i++)) ){
                listado.add(datos);
            }
            xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception("Error en la rutina procesarXLS en [HImportaciones] ....\n" + ex.getMessage());
        }
    }  
    
    /**
     * Metodo de procesamiento de archivos xml
     * @param f Archivo xml a procesar
     * @throws Exception .
     */    
    public void procesarXML(File f) throws Exception{
        try {
            
            listado = new LinkedList();
            reporteRequest xml = new reporteRequest();
            xml.procesarReporte(usuario.getDstrct(), usuario.getLogin(), usuario.getBase(), f.getAbsolutePath());
            listado = xml.getListado();
            
        }catch (Exception ex){
            throw new Exception("Error en la rutina procesarXLS en [HImportaciones] ....\n" + ex.getMessage());
        }
    }  
    
   
    
    
    /**
     * Inicia el log de importaciones
     * @throws Exception .
     */    
    public void initLog ()throws Exception{
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");
        
        model.LogProcesosSvc.updateProceso("HImportaciones", this.hashCode(), usuario.getLogin(), "Inicializando Log de Importaciones, archivo [logImportacion_"+ fmt.format(new Date()) +".txt" +"]");
        pw     = new PrintWriter (new BufferedWriter(new FileWriter(path + "/logImportacion_"+ fmt.format(new Date()) +".txt")));
        logImp = new LogWriter("Importaciones", LogWriter.INFO , pw ); 
        logImp.log( "Proceso Inicializado", logImp.INFO );
    }
    
    /**
     * Cierra el log general de importaciones
     * @throws Exception .
     */    
    public void closeLog() throws Exception{
        model.LogProcesosSvc.updateProceso("HImportaciones", this.hashCode(), usuario.getLogin(), "Cerrando log de Importacion");
        logImp.log( "Proceso Finalizado", logImp.INFO );
        pw.close();
    }
    
    
    
    /**
     * Metodo que genera un registro especial de errores por cada archivo importado
     * @param namefile Nombre del Archivo
     * @param errores errores generados por el proceso de importacion
     * @throws Exception .
     */    
    public void generarLogArchivo (String namefile,  TreeMap errores) throws Exception{
        
        try{
            
            String sufijo = (tipoImportacion.equalsIgnoreCase("Insercion")? "_RNM" : "_ERP");
            
            POIWrite xls = new POIWrite (path + "/" + namefile + sufijo + ".xls");
            
            
            // registro de datos no montados
            xls.obtenerHoja("BASE");
            List reg = (List) errores.get("rnm");            
            for (int fila = 0; fila<reg.size(); fila++){
                String [] datos = (String []) reg.get(fila);
                
                // adicionando campos a la hoja
                for (int i=0;i<datos.length;i++)
                    xls.adicionarCelda(fila,i,datos[i], null);
            }
            
            
            // registro de errores
            String hoja = (tipoImportacion.equalsIgnoreCase("Insercion")? "ERRORES" : "ESTADO");
            xls.obtenerHoja( hoja );
            List err = (List) errores.get("err");     
            int numErrors = 0;
            for (int fila = 0; fila<err.size(); fila++){
                String error = (String) err.get(fila);
                xls.adicionarCelda(fila,0,error, null);
                if (error.toUpperCase().indexOf("ERROR")!=-1) numErrors++;
            }
            
            xls.cerrarLibro();
            
            logImp.log(numErrors + " registro(s) no pudieron ser procesados correctamente.", logImp.INFO);
        }catch (Exception ex){
            ////System.out.println("Error en generarLogArchivo ...\n" + ex.getMessage());
        }
        
    }
    
    /**
     * Metodo que indica si elo array contiene o no datos
     * @autor mfontalvo
     * @param datos, Arreglo de String
     * @return boolean , estado del array
     */
    private boolean isEmpty (String []datos ){
        boolean empty = true;
        if (datos!=null){
            for (int i = 0; i < datos.length && empty ; i++){
                if ( datos[i] !=null && !datos[i].trim().equals(""))
                    empty = false;
            }
        }
        return empty;
    }
    
}

