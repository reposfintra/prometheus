/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;


import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;

import java.util.*;
import java.text.*;



/**
 *
 * @author Alvaro
 */
public class HFacturaContratista extends Thread{


    private Model model;
    private Usuario usuario;
    private String dstrct;

    /** Creates a new instance of HPrefacturaDetalle */
    public HFacturaContratista () {
    }

    public void start(Model model, Usuario usuario, String distrito){

        this.usuario = usuario;
        this.model = model;
        this.dstrct = distrito;

        super.start();
    }


    public synchronized void run(){
        try{
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del reporte de prefactura : " + prefactura, usuario.getLogin());

            Vector comandos_sql =new Vector();
            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();



            List listaFacturaGeneral =  model.applusService.getFacturaGeneral();

            if (listaFacturaGeneral.size() != 0){

                FacturaGeneral facturaGeneral = new FacturaGeneral();
                Iterator it = listaFacturaGeneral.iterator();

                String comandoSQL = "";

                while (it.hasNext()  ) {

                    facturaGeneral = (FacturaGeneral)it.next();

                    // Datos comunes a la cabecera y a los items


                    String id_contratista = facturaGeneral.getId_contratista();
                    String prefactura     = facturaGeneral.getPrefactura();

                    model.applusService.buscaContratista(id_contratista);
                    Contratista contratista = model.applusService.getContratista();
                    String proveedor = contratista.getNit();
                    String tipo_documento = "FAP";
                    String documento      = facturaGeneral.getPrefactura();
                    String user_update = usuario.getLogin();
                    String creation_user = user_update;
                    String base = "COL";

                    

                    // CREACION DE LA CABECERA EN CXP_DOC

                    // Datos de la cabecera


                    String descripcion    = "Factura correspondiente a prefactura : " + documento;
                    String agencia        = "BQ";
                    Proveedor objeto_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
                    
                    //String handle_code = objeto_proveedor.getC_hc();
                    
                    String handle_code = "MC";
                    
                    String aprobador   = "JGOMEZ";
                    String usuario_aprobacion = "JGOMEZ";
                    String banco = objeto_proveedor.getC_branch_code();
                    String sucursal = objeto_proveedor.getC_bank_account();
                    String moneda = "PES";

                    double vlr_mat = facturaGeneral.getVlr_mat();
                    double vlr_mob = facturaGeneral.getVlr_mob();
                    double vlr_otr = facturaGeneral.getVlr_otr();
                    double vlr_administracion = facturaGeneral.getVlr_administracion();
                    double vlr_imprevisto     = facturaGeneral.getVlr_imprevisto();
                    double vlr_utilidad       = facturaGeneral.getVlr_utilidad();
                    double vlr_bonificacion   = facturaGeneral.getVlr_Bonificacion();
                    double vlr_iva            = facturaGeneral.getVlr_iva();
                    double vlr_rmat           = facturaGeneral.getVlr_rmat();
                    double vlr_rmob           = facturaGeneral.getVlr_rmob();
                    double vlr_rotr           = facturaGeneral.getVlr_rotr();
                    double vlr_factoring      = facturaGeneral.getVlr_factoring();
                    double vlr_formula        = facturaGeneral.getVlr_formula();
                    double vlr_base_iva       = facturaGeneral.getVlr_base_iva();
                    double vlr_formula_provintegral=facturaGeneral.getVlr_formula_provintegral();

                    double vlr_iva_bonificacion=facturaGeneral.getIva_bonificacion();           // apabon 20091229
                    
                    double vlr_neto  = vlr_mat + vlr_mob + vlr_otr +
                                       vlr_administracion + vlr_imprevisto + vlr_utilidad +
                                       vlr_bonificacion + vlr_iva + vlr_rmat + vlr_rmob + vlr_rotr +
                                       vlr_factoring + vlr_formula+vlr_formula_provintegral +   // apabon 20091229
                                       vlr_iva_bonificacion - vlr_iva_bonificacion;             // apabon 20091229

                    double vlr_total_abonos = 0;
                    double vlr_saldo = vlr_neto;
                    double vlr_neto_me = vlr_neto;
                    double vlr_total_abonos_me = 0;
                    double vlr_saldo_me = vlr_neto;
                    double tasa = 1;

                    String observacion = "";
                    String clase_documento = "4";
                    // String moneda_banco = objeto_proveedor.getC_currency_bank();
                    String moneda_banco = "PES";
                    String clase_documento_rel = "4";
                    Date fecha_hoy = new Date();
                    DateFormat formato;
                    formato = new SimpleDateFormat("yyyy-MM-dd");
                    String fecha_documento = formato.format(fecha_hoy);
                    Date fecha_pago = fecha_hoy ;
                    String fecha_vencimiento = fecha_documento;
 
                    comandoSQL = model.applusService.setCxp_doc(dstrct, proveedor, tipo_documento, documento, descripcion,
                            agencia, handle_code, aprobador, usuario_aprobacion, banco, sucursal,
                            moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                            vlr_total_abonos_me, vlr_saldo_me, tasa, observacion, user_update,
                            creation_user, base, clase_documento, moneda_banco,
                            fecha_documento, fecha_vencimiento, clase_documento_rel,
                            creation_date, creation_date);

                    comandos_sql.add(comandoSQL);


                    // CREACION DE LOS ITEMS EN CXP_ITEMS_DOC

                    // Datos comunes a todos los items

                    String auxiliar = proveedor;
                    int item = 0;

                    String ceros1="";
                    if (item==0){ ceros1="00";}else{ ceros1="";}
                    
                    // Item de material , mano de obra y otros
                    descripcion = "Material : " + Util.FormatoMiles(vlr_mat) +
                                  "  Mano de obra : " + Util.FormatoMiles(vlr_mob) +
                                  "  Otros : " + Util.FormatoMiles(vlr_otr);

                    comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                     tipo_documento, documento,ceros1+Integer.toString(++item),
                                     descripcion, vlr_mat + vlr_mob + vlr_otr,
                                     vlr_mat + vlr_mob + vlr_otr, "28050601",
                                     user_update, creation_user, base, "101", auxiliar,
                                     creation_date, creation_date);
                    comandos_sql.add(comandoSQL);


                    // Item de administracion e imprevisto

                    if ((vlr_administracion+vlr_imprevisto) != 0) {

                        descripcion = "Administracion : " + Util.FormatoMiles(vlr_administracion) +
                                      "  Imprevisto : " + Util.FormatoMiles(vlr_imprevisto) ;

                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         descripcion , vlr_administracion + vlr_imprevisto ,
                                         vlr_administracion + vlr_imprevisto , "28050601",
                                         user_update, creation_user, base,  "103", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }


                    // Item de utilidad
                    if (vlr_utilidad != 0) {
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         "Utilidad", vlr_utilidad, vlr_utilidad, "28050601",
                                         user_update, creation_user, base,  "104", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }

                    // Item de iva
                    descripcion = "Iva, Base Iva : " + Util.FormatoMiles(vlr_base_iva) ;
                    comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                     tipo_documento, documento,Integer.toString(++item),
                                     descripcion, vlr_iva, vlr_iva, "28050601",
                                     user_update, creation_user, base, "102", auxiliar,
                                     creation_date, creation_date);
                    comandos_sql.add(comandoSQL);

                    // Item de bonificacion
                    if (vlr_bonificacion != 0) {
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         "Bonificacion", vlr_bonificacion, vlr_bonificacion, "23050701",
                                         user_update, creation_user, base, "105", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }

                    // Item de iva de bonificacion
                    if (vlr_iva_bonificacion != 0) {
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         "Iva de Bonificacion", vlr_iva_bonificacion, vlr_iva_bonificacion, "23050701",
                                         user_update, creation_user, base, "105", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }

                    // Item de iva de bonificacion
                    if (vlr_iva_bonificacion != 0) {
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         "Iva de Bonificacion", -vlr_iva_bonificacion, -vlr_iva_bonificacion, "28050601",
                                         user_update, creation_user, base, "105", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }


                    // Item de factoring
                    comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                     tipo_documento, documento,Integer.toString(++item),
                                     "Factoring", vlr_factoring, vlr_factoring, "27050550",
                                     user_update, creation_user, base, "106", auxiliar,
                                     creation_date, creation_date);
                    comandos_sql.add(comandoSQL);

                    // Item de formula
                    
                    //if (modelo anterior)                        
                        
                    comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                     tipo_documento, documento,Integer.toString(++item),
                                     "Formula Fintra", vlr_formula, vlr_formula, "27050551",
                                     user_update, creation_user, base, "107", auxiliar,
                                     creation_date, creation_date);//090511
                    comandos_sql.add(comandoSQL);


                    //inicio090511
                    /*String esquema_comision=model.negociosApplusService.getEsquemaComision(""+id_orden);//090511*/
                    
                    if (vlr_formula_provintegral!=0){
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         "Formula Provintegral", vlr_formula_provintegral, vlr_formula_provintegral, "28150501",
                                         user_update, creation_user, base, "111", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }

                    //fin090511
                    
                    // Item de valor retencion material

                    if (vlr_rmat != 0) {
                        descripcion = "Retencion por material, Base retencion : " + Util.FormatoMiles(vlr_mat);
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         descripcion, vlr_rmat, vlr_rmat, "23050702",
                                         user_update, creation_user, base, "108", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }

                     // Item de valor retencion mano de obra
                    if (vlr_rmob != 0) {
                        descripcion = "Retencion por mano de obra, Base retencion : " + Util.FormatoMiles(vlr_mob);
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         descripcion, vlr_rmob, vlr_rmob, "23050702",
                                         user_update, creation_user, base, "109", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }

                    // Item de valor retencion otros
                    if (vlr_rotr != 0) {
                        descripcion = "Retencion por otros, Base retencion : " + Util.FormatoMiles(vlr_otr);
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         descripcion, vlr_rotr, vlr_rotr, "23050702",
                                         user_update, creation_user, base, "110", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }


                    // ACTUALIZA LAS ACCIONES CON EL NUMERO DE FACTURA Y FECHA DE FACTURA DEL CONTRATISTA
                    
                    comandoSQL = model.applusService.setFacturaContratista(documento,creation_date, id_contratista, prefactura );
                    comandos_sql.add(comandoSQL);

                }
                // Grabando todo a la base de datos.
                model.applusService.ejecutarSQL(comandos_sql);
            }


            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HFacturaContratista ...\n" + e.getMessage());
            }
        }
    }


}
