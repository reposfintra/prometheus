/**
 * Nombre        ExportarPorcentajes.java
 * Descripci�n
 * Autor         Andres Martinez
 * Fecha         24 de julio de 2006, 01:48 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.Corrida;
import com.tsp.operation.model.beans.Planilla;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.Model;

import javax.swing.*;
import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author  ALVARO
 */
public class ComparacionPlanillas extends Thread {
Model model;
    Vector datos;
    Usuario usuario;
    SimpleDateFormat fmt;    
    String fechaInicio;
    String fechaFinal;
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, porcentaje, letraCentrada, numeroNegrita,porcentajew, letraCentradaw ;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris, cRojo ;
        
    
    /** Crea una nueva instancia de  ExportarPorcentajes */
    public ComparacionPlanillas(String fechainicio,String fechafinal,Model model, Usuario usuario) {
        this.fechaInicio=fechainicio;
        this.fechaFinal=fechafinal;
        this.usuario = usuario;
        this.model   = model;
        super.start();
    }
     public synchronized void run(){
        try{
            
            model.LogProcesosSvc.InsertProceso("CompararPlanillas",this.hashCode(), "Iniciado", usuario.getLogin());
            
            // creacion del diractorio del usuario
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            
            if(model.comparacionplanillasservice.buscarPlanillasPostgres(fechaInicio,fechaFinal)){
                if(model.comparacionplanillasservice.buscarPlanillasOracle()){
                    CompararPlanillas();
                    model.LogProcesosSvc.finallyProceso("CompararPlanillas", this.hashCode(), usuario.getLogin(), "Exitoso");    
                }
                else{
                    model.LogProcesosSvc.finallyProceso("CompararPlanillas", this.hashCode(), usuario.getLogin(), "No se pudo generar el archivo, error en la consulta de las remesas de mims");
                }
            }
            else{
                model.LogProcesosSvc.finallyProceso("CompararPlanillas", this.hashCode(), usuario.getLogin(), "No se pudo generar el archivo, error en la consulta de las remesas de postgres");
            }
                        
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso("CompararPlanillas", this.hashCode(), usuario.getLogin(),  ex.getMessage());
            } catch (Exception e){}            
        }
    }
    
    
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);
            cRojo       = xls.obtenerColor(255,0,0);
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAzul.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo3      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo4      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo5      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cGris.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            
            letra        = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada= xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero       = xls.nuevoEstilo("Tahoma", 8 , false , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 8 , true  , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            porcentaje   = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            porcentaje  = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada = xls.nuevoEstilo("Tahoma", 8 , false , false, "text" , xls.NONE , xls.NONE , xls.NONE);
            letraCentradaw = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
    }    
    
    

    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            xls.adicionarCelda(2,0, "USUARIO", titulo1);
            xls.adicionarCelda(2,1, usuario.getLogin() , titulo1);
            xls.adicionarCelda(3,0, "CEDULA" , titulo1);
            xls.adicionarCelda(3,1, usuario.getCedula(), titulo1);
            xls.adicionarCelda(4,0, "NOMBRE", titulo1);
            xls.adicionarCelda(4,1, usuario.getNombre() , titulo1);
            
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
 /**
     * CompararRemesas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void CompararPlanillas() throws Exception {
        try{
             int fila = 6;
            
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("COMPARACION_PLANILLAS_MIMS_POSTGRES" + fmt.format( new Date() ) +".xls", "MIGRACION MIMIS - POSTGRES");
            String [] cabecera0 = { "POSTGRES", "","","","","","","","","","","", "", "", "", "","","",""};
            String [] cabecera1 = { "MIMS", "","","","","","","","","","","", "", "", "", "","","",""};
            String [] cabecera = { "Planilla", "Cia","Fecha Creacion","Agencia","Origen","Destino","Placa","Cedula","Fecha despacho","Fechaposllegada","Valor","Nomcon", "Orinom", "Desnom", "Celular", "UltimoReporte","Observacion","Status_220","Despachador","Planilla", "Cia","Fecha Creacion","Agencia","Origen","Destino","Placa","Cedula","Fecha despacho","Fechaposllegada","Valor","Nomcon", "Orinom", "Desnom", "Celular", "UltimoReporte","Observacion","Status_220","Despachador"};
            
            short  [] dimensiones = new short [] {3000, 3000, 3000, 3000 , 3000,3000, 3000, 3000, 3000 , 3000,3000, 3000, 3000, 3000 , 3000,3000, 3000, 3000, 3000 , 3000,3000, 3000, 3000, 3000 , 3000,3000, 3000, 3000, 3000 , 3000,3000, 3000, 3000, 3000 , 3000,3000, 3000, 3000 };
            for ( int i = 0; i<cabecera0.length; i++){
                xls.adicionarCelda(fila,  i, cabecera0[i], titulo4);
                //xls.cambiarAnchoColumna(i, dimensiones[i] );
            }
            for ( int i = cabecera0.length; i<cabecera1.length+cabecera0.length; i++){
                xls.adicionarCelda(fila,  i, cabecera1[i-cabecera0.length], titulo2);
                //xls.cambiarAnchoColumna(i, dimensiones[i] );
            }
            fila++;
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila,  i, cabecera[i], titulo5);
                xls.cambiarAnchoColumna(i, dimensiones[i] );
            }
            Vector VPostgres;
            Vector VOracle;
            VPostgres = (Vector)model.comparacionplanillasservice.getVPostgres().clone();
            ////System.out.println("VPostgres:"+VPostgres.size());
            VOracle = (Vector)model.comparacionplanillasservice.getVOracle().clone();
            ////System.out.println("VOracle:"+VOracle.size());
           
            /*Vector VPostgres=new Vector();
            Vector VOracle=new Vector();
           
                 Plarem po1 = new Plarem(); 
                 po1.setNumpla("p1");
                 po1.setNumrem("r1");
                 po1.setPorcent("100");
                 VPostgres.add(po1);
                 Plarem po2 = new Plarem(); 
                 po2.setNumpla("p1");
                 po2.setNumrem("r2");
                 po2.setPorcent("100");
                 VPostgres.add(po2);
                 Plarem po3 = new Plarem(); 
                 po3.setNumpla("p1");
                 po3.setNumrem("r3");
                 po3.setPorcent("50");
                 VPostgres.add(po3);
                 Plarem po4 = new Plarem(); 
                 po4.setNumpla("p2");
                 po4.setNumrem("r2");
                 po4.setPorcent("30");
                 VPostgres.add(po4);
                 Plarem po5 = new Plarem(); 
                 po5.setNumpla("p2");
                 po5.setNumrem("r3");
                 po5.setPorcent("70");
                 VPostgres.add(po5);
               
                 Plarem pa1 = new Plarem(); 
                 pa1.setNumpla("p1");
                 pa1.setNumrem("r1");
                 pa1.setPorcent("100");
                 VOracle.add(pa1);
                 Plarem pa2 = new Plarem(); 
                 pa2.setNumpla("p1");
                 pa2.setNumrem("r2");
                 pa2.setPorcent("100");
                 VOracle.add(pa2);
                 Plarem pa3 = new Plarem(); 
                 pa3.setNumpla("p1");
                 pa3.setNumrem("r3");
                 pa3.setPorcent("50");
                 VOracle.add(pa3);
                 Plarem pa4 = new Plarem(); 
                 pa4.setNumpla("p2");
                 pa4.setNumrem("r1");
                 pa4.setPorcent("100");
                 VOracle.add(pa4);
                 Plarem pa5 = new Plarem(); 
                 pa5.setNumpla("p3");
                 pa5.setNumrem("r1");
                 pa5.setPorcent("100");
                 VOracle.add(pa5);
                 */
             int j=0,l=0;
            
            Vector Postgresfinal= new Vector();
            Vector Oraclefinal= new Vector();
            
             ////System.out.println("Bloque para conseguir el vector de postgres");
            /*Bloque para conseguir el vector de postgres*/
            for(int i=j;i<VPostgres.size();i++){
                Vector v1= new Vector();
                Vector v2= new Vector();
                
                Planilla p1 = (Planilla)VPostgres.get(i);
                
                /*postgres...*/
                for(j=i;j<VPostgres.size();j++){
                    Planilla p2 = (Planilla)VPostgres.get(j);
                    if(p1.getNumpla().equals(p2.getNumpla())){
                        v1.add(p2);
                        i=j;
                        ////System.out.println("vector1:"+p2.getNumpla());
                    }
                    else{
                        i=j-1;
                        break;
                    }
                }
                /*fin postgres*/
                /*oracle*/
                for(int k=l;k<VOracle.size();k++){
                    ////System.out.println("entro");
                    Planilla p3 = (Planilla)VOracle.get(k);
                    if(p1.getNumpla().equals(p3.getNumpla())){
                        v2.add(p3);
                        ////System.out.println("vector2:"+p3.getNumpla());
                        l=k;
                        if(k+1==VOracle.size()){
                           l=k+1;   
                        }
                    }
                    else{
                        l=k;
                        break;
                    }
                }
              
                /*fin oracle*/
                /*comienza la */
                Vector guardados=new Vector();
                String pla="";
                for(int g=0;g<v1.size();g++){
                    Planilla p4 = (Planilla)v1.get(g);
                    pla= p4.getNumpla();
                        int r=0;
                        for(int f=0;f<v2.size();f++){
                            Planilla p5 = (Planilla)v2.get(f);
                            if(p4.getNumpla().equals(p5.getNumpla())){
                                r++;
                                v2.remove(f);
                            }                
                        }
                        if(r>0){
                            Postgresfinal.add(p4);    
                        }
                        else{
                            guardados.add(p4);
                        }
                }
                for(int w=0;w<guardados.size();w++){
                    Planilla p6 = (Planilla)guardados.get(w);
                    Postgresfinal.add(p6);
                }
                for(int f=0;f<v2.size();f++){
                    Planilla q = new Planilla();
                    q.setNumpla(pla);
                    q.setCia("");
                    q.setFechapla(new Date(""));
                    q.setAgcpla("");
                    q.setOripla("");
                    q.setDespla("");
                    q.setPlaveh("");
                    q.setCedcon("");
                    q.setFecdsp("");
                    q.setFechaposllegada(new Date(""));
                    q.setVlrpla(0);
                    q.setNomcon("");
                    q.setOrinom("");
                    q.setDestinopla("");
                
                    q.setCelularcon("");
                    q.setUltimoreporte("");
                   
                    q.setObservacion("");
                  
                    q.setStatus_220("");
                    q.setDespachador("");
                
                    Postgresfinal.add(q);
                }
            }
            for(int f=l;f<VOracle.size();f++){
                    Planilla qq = (Planilla)VOracle.get(f);;
                    Planilla q = new Planilla();
                    q.setNumpla(qq.getNumpla());
                    q.setCia("");
                    q.setFechapla(new Date());
                    q.setAgcpla("");
                    q.setOripla("");
                    q.setDespla("");
                    q.setPlaveh("");
                    q.setCedcon("");
                    q.setFecdsp("");
                    q.setFechaposllegada(new Date());
                    q.setVlrpla(0);
                    q.setNomcon("");
                    q.setOrinom("");
                    q.setDestinopla("");
                
                    q.setCelularcon("");
                    q.setUltimoreporte("");
                   
                    q.setObservacion("");
                  
                    q.setStatus_220("");
                    q.setDespachador("");
                    Postgresfinal.add(q);
            }
            /*fin bloque de postgres*/
            
            j=0;
            l=0;
            ////System.out.println("Bloque para conseguir el vector de oracle");
            
            /*Bloque para conseguir el vector de oracle*/
            for(int i=j;i<VOracle.size();i++){
                Vector v1= new Vector();
                Vector v2= new Vector();
                
                Planilla p1 = (Planilla)VOracle.get(i);
                
                /*postgres...*/
                for(j=i;j<VOracle.size();j++){
                    Planilla p2 = (Planilla)VOracle.get(j);
                    if(p1.getNumpla().equals(p2.getNumpla())){
                        v1.add(p2);
                        i=j;
                        ////System.out.println("vector1:"+p2.getNumpla());
                    }
                    else{
                        i=j-1;
                        break;
                    }
                }
                /*fin postgres*/
                /*oracle*/
                for(int k=l;k<VPostgres.size();k++){
                    ////System.out.println("entro");
                    Planilla p3 = (Planilla)VPostgres.get(k);
                    if(p1.getNumpla().equals(p3.getNumpla())){
                        v2.add(p3);
                        ////System.out.println("vector2:"+p3.getNumpla());
                        l=k;
                        if(k+1==VPostgres.size()){
                            l=k+1;    
                        }
                    }
                    else{
                        l=k;
                        if(k+1==VPostgres.size()){
                            l=k+1;    
                        }
                        break;
                    }
                }
                ////System.out.println("******fin oracle **********");
                /*fin oracle*/
                /*comienza la */
                Vector guardados=new Vector();
                String pla="";
                for(int g=0;g<v1.size();g++){
                    Planilla p4 = (Planilla)v1.get(g);
                    pla=p4.getNumpla();
                        int r=0;
                        for(int f=0;f<v2.size();f++){
                            Planilla p5 = (Planilla)v2.get(f);
                            if(p4.getNumpla().equals(p5.getNumpla())){
                                r++;
                                v2.remove(f);
                            }                
                        }
                        if(r>0){
                            Oraclefinal.add(p4);    
                        }
                        else{
                            guardados.add(p4);
                        }
                }
                for(int f=0;f<v2.size();f++){
                    Planilla q = new Planilla();
                    q.setNumpla(pla);
                    q.setCia("");
                    q.setFechapla(new Date());
                    q.setAgcpla("");
                    q.setOripla("");
                    q.setDespla("");
                    q.setPlaveh("");
                    q.setCedcon("");
                    q.setFecdsp("");
                    q.setFechaposllegada(new Date());
                    q.setVlrpla(0);
                    q.setNomcon("");
                    q.setOrinom("");
                    q.setDestinopla("");
                
                    q.setCelularcon("");
                    q.setUltimoreporte("");
                   
                    q.setObservacion("");
                  
                    q.setStatus_220("");
                    q.setDespachador("");
                 
                    Oraclefinal.add(q);
                }
                for(int w=0;w<guardados.size();w++){
                    Planilla p6 = (Planilla)guardados.get(w);
                    Oraclefinal.add(p6);
                }
                
            }
            for(int f=l;f<VPostgres.size();f++){
                    Planilla qq = (Planilla)VPostgres.get(f);;
                    Planilla q = new Planilla();
                    q.setNumpla(qq.getNumpla());
                    
                    
                    
                    q.setCia("");
                    q.setFechapla(new Date());
                    q.setAgcpla("");
                    q.setOripla("");
                    q.setDespla("");
                    q.setPlaveh("");
                    q.setCedcon("");
                    q.setFecdsp("");
                    q.setFechaposllegada(new Date());
                    q.setVlrpla(0);
                    q.setNomcon("");
                    q.setOrinom("");
                    q.setDestinopla("");
                
                    q.setCelularcon("");
                    q.setUltimoreporte("");
                   
                    q.setObservacion("");
                  
                    q.setStatus_220("");
                    q.setDespachador("");
                  
                     
                    
                    Oraclefinal.add(q);
            }
            /*fin bloque de postgres*/
            
            ////System.out.println("*******************************************");
            /*Para sacar las que estan ok!!!*/
            Vector vok=new Vector();
            j=0;
            l=0;
           
            for(int i=j;i<Postgresfinal.size();i++){
                Vector v1= new Vector();
                Vector v2= new Vector();
                ////System.out.println("bandera1");
                Planilla p1 = (Planilla)Postgresfinal.get(i);
                ////System.out.println("bandera2");
                for(j=i;j<Postgresfinal.size();j++){
                    Planilla p2 = (Planilla)Postgresfinal.get(j);
                    if(p1.getNumpla().equals(p2.getNumpla())){
                        v1.add(p2);
                        i=j;
                        ////System.out.println("vector1:"+p2.getNumpla());
                    }
                    else{
                        i=j-1;
                        break;
                    }
                }
               ////System.out.println("bandera3");
                for(int k=l;k<Oraclefinal.size();k++){
                    Planilla p3 = (Planilla)Oraclefinal.get(k);
                    if(p1.getNumpla().equals(p3.getNumpla())){
                        v2.add(p3);
                        ////System.out.println("vector2:"+p3.getNumpla());
                       
                    }
                    else{
                        l=k;
                        break;
                    }
                }
               ////System.out.println("bandera4");
                int ok=0;
                String p1anilla="";
              
                for(int g=0;g<v1.size();g++){
                    Planilla p4 = (Planilla)v1.get(g);
                    Planilla p5 = (Planilla)v2.get(g);
                            if((p4.getNumpla().equals(p5.getNumpla()))&&(p4.getCia().equals(p5.getCia()))&&(p4.getFechapla().equals(p5.getFechapla()))&&(p4.getAgcpla().equals(p5.getAgcpla()))&&(p4.getOripla().equals(p5.getOripla()))&&(p4.getDespla().equals(p5.getDespla()))&&(p4.getPlaveh().equals(p5.getPlaveh()))&&(p4.getCedcon().equals(p5.getCedcon()))&&(p4.getFecdsp().equals(p5.getFecdsp()))&&(p4.getFechaposllegada().equals(p5.getFechaposllegada()))&&(p4.getVlrpla()==(p5.getVlrpla()))&&(p4.getNomcon().equals(p5.getNomcon()))&&(p4.getOrinom().equals(p5.getOrinom()))&&(p4.getDestinopla().equals(p5.getDestinopla()))&&(p4.getCedcon().equals(p5.getCedcon()))&&(p4.getUltimoreporte().equals(p5.getUltimoreporte()))&&(p4.getObservacion().equals(p5.getObservacion()))&&(p4.getStatus_220().equals(p5.getStatus_220()))&&(p4.getDespachador().equals(p5.getDespachador()))){
                                ok=0;
                                p1anilla=p4.getNumpla();
                                ////System.out.println("Son iguales!!!");
                            }
                            else{
                                ////System.out.println("Encontro diferente!!!");
                                p1anilla="";
                                ok=1;
                                break;
                            }
                       
                }
                ////System.out.println("bandera5");
                if(ok==0){
                    vok.add(p1anilla);
                }
                ////System.out.println("bandera6");
            }
            //
            ////System.out.println("elimina...");
            for(int z3=vok.size()-1;z3>=0;z3--){
                for(int z4=Postgresfinal.size()-1;z4>=0;z4--){
                Planilla po = (Planilla)Postgresfinal.get(z4); 
                    if(po.getNumpla().equals((String)vok.get(z3))){
                        ////System.out.println("elimino de post");
                        Postgresfinal.remove(z4);
                    }  
                }    
                for(int z4=Oraclefinal.size()-1;z4>=0;z4--){
                Planilla po = (Planilla)Oraclefinal.get(z4); 
                    if(po.getNumpla().equals((String)vok.get(z3))){
                        ////System.out.println("elimino de oracle");
                        Oraclefinal.remove(z4);
                    }  
                }  
            }
           ////System.out.println("******************************");
            /*fin ok*/
            
           fila++;
            for ( int i = 0; i<Postgresfinal.size(); i++, fila++){
                
                Planilla p1 = (Planilla)Postgresfinal.get(i);
                Planilla p2 = (Planilla)Oraclefinal.get(i);
          
                xls.adicionarCelda(fila  , 0 , (p1.getNumpla()!=null)?p1.getNumpla():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 1 , (p1.getCia()!=null)?p1.getCia():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                String f1 =" "+p1.getFechapla();
                xls.adicionarCelda(fila  , 2 , f1, porcentaje);
                xls.adicionarCelda(fila  , 3 , (p1.getAgcpla()!=null)?p1.getAgcpla():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 4 , (p1.getOripla()!=null)?p1.getOripla():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
               
                xls.adicionarCelda(fila  , 5 , (p1.getDespla()!=null)?p1.getDespla():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 6 , (p1.getPlaveh()!=null)?p1.getPlaveh():"",(p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 7 , (p1.getCedcon()!=null)?p1.getCedcon():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 8 , (p1.getFecdsp()!=null)?p1.getFecdsp():"",(p1.getCia()!="")?letraCentrada:letraCentradaw);
                String f2 =" "+p1.getFechapla();
                xls.adicionarCelda(fila  , 9 ,  f2, (p1.getCia()!="")?letraCentrada:letraCentradaw);
                float v1=0;
                v1+=p1.getVlrpla();
                xls.adicionarCelda(fila  , 10 ,v1, (p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 11 , (p1.getNomcon()!=null)?p1.getNomcon():"",(p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 12 , (p1.getOrinom()!=null)?p1.getOrinom():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 13 , (p1.getDestinopla()!=null)?p1.getDestinopla():"",(p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 14 , (p1.getCelularcon()!=null)?p1.getCelularcon():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                
                xls.adicionarCelda(fila  , 15 , (p1.getUltimoreporte()!=null)?p1.getUltimoreporte():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 16 , (p1.getObservacion()!=null)?p1.getObservacion():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 17 , (p1.getStatus_220()!=null)?p1.getStatus_220():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 18 , (p1.getDespachador()!=null)?p1.getDespachador():"", (p1.getCia()!="")?letraCentrada:letraCentradaw);
                
                
                xls.adicionarCelda(fila  , 19 , (p2.getNumpla()!=null)?p2.getNumpla():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 20 , (p2.getCia()!=null)?p2.getCia():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                String f3 =" "+p2.getFechapla();
                xls.adicionarCelda(fila  , 21 , f3, (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 22 , (p2.getAgcpla()!=null)?p2.getAgcpla():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 23 , (p2.getOripla()!=null)?p2.getOripla():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
               
                xls.adicionarCelda(fila  , 24 , (p2.getDespla()!=null)?p2.getDespla():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 25 , (p2.getPlaveh()!=null)?p2.getPlaveh():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 26 , (p2.getCedcon()!=null)?p2.getCedcon():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 27 , (p2.getFecdsp()!=null)?p2.getFecdsp():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                String f4 =" "+p2.getFechaposllegada();
                xls.adicionarCelda(fila  , 28 , f4, (p2.getCia()!="")?letraCentrada:letraCentradaw);
                float v2=0;
                v2+=p2.getVlrpla();
                xls.adicionarCelda(fila  , 29 , v2, (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 30 , (p2.getNomcon()!=null)?p2.getNomcon():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 31 , (p2.getOrinom()!=null)?p2.getOrinom():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 32 , (p2.getDestinopla()!=null)?p2.getDestinopla():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 33 , (p2.getCelularcon()!=null)?p2.getCelularcon():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                
                xls.adicionarCelda(fila  , 34 , (p2.getUltimoreporte()!=null)?p2.getUltimoreporte():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 35 , (p2.getObservacion()!=null)?p2.getObservacion():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 36 , (p2.getStatus_220()!=null)?p2.getStatus_220():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 37 , (p2.getDespachador()!=null)?p2.getDespachador():"", (p2.getCia()!="")?letraCentrada:letraCentradaw);
               
            }       
            this.cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en ExportarTransferencias ...\n" + ex.getMessage() );
        }
    }
    /*public static void main(String a []){
        ExportarPorcentajes b = new ExportarPorcentajes("33","33");
        try{
            b.CompararRemesas();
        }catch(Exception e){
            e.printStackTrace();
        }
       
    }*/
}