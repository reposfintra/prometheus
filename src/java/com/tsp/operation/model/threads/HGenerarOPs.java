/***************************************
 * Nombre       HGenerarOPs.java 
 * Autor        FERNEL VILLACOB DIAZ
 * Fecha        20/10/2005
 * versión      1.0
 * Copyright    Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.model.threads;



import java.io.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;



public class HGenerarOPs extends Thread{
    
    private Model         model;
    private Usuario        usuario;
    private String        distrito;
    private String        fechaIni;
    private String        fechaFin;
    private String        HC;
    private String        procesoName;
    
    private String        PROCEDENCIA = "OP";
    
    
    
    private LogWriter logWriter;
    
    
        
    public HGenerarOPs() {}
    
    
    
    public void start(Model modelo, String f1, String f2, Usuario user,String hc, String distrito) throws Exception{
        try{
            this.model           = modelo;
            this.fechaIni        = f1;
            this.fechaFin        = f2;
            this.HC              = hc;
            this.usuario         = user;
            this.distrito        = distrito;
            this.procesoName = "GENERACION DE OPS";
            super.start();
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    public synchronized void run(){
        try{
            String comentario="Proceso Exitoso!";
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode()," Periodo "+ this.fechaIni +" - "+ this.fechaFin,this.usuario.getLogin());
            
                 
         // Creamos directorio del usuario.
            model.DirectorioSvc.create(this.usuario.getLogin() );
            
            
            
        // GENERACION DE OPS:
            
        // Buscamos Planillas a Generar Ops.
            model.OpSvc.generateOP(this.fechaIni, this.fechaFin,this.HC, distrito); 
            List listOp = model.OpSvc.getOP();
            
            if ( listOp != null  &&  listOp.size() > 0){
                
                
              //  ARCHIVO LOG:
                 
                  String fecha      =  Util.getFechaActual_String(6).replaceAll("/|:","");
                  String url        =  model.DirectorioSvc.getUrl() + this.usuario.getLogin() + "/GENOP"+ fecha +".txt";   // Log.             
                  PrintWriter  pw   =  new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
                  LogWriter    logM =  new LogWriter("",LogWriter.INFO, pw);
            
                  
                  
              //   listOp  = model.LiqOCSvc.getExcluirRepetidos( listOp );  //Excluimos oc repetidas provenientes del SQL.
                 
                 
                  
              // Recorremos las planillas
                 logM.log("Inicio de la Generación OP....",logM.INFO);
                 for( int i=0; i<listOp.size(); i++ ){
                     OP op = (OP)listOp.get(i);
                     
                     try{                          
                          op = model.LiqOCSvc.formarOP(op);  // Formamos la Factura OP.

                       // Insertamos:
                          String msj = model.OpSvc.insertOP( op, this.usuario );  

                       // Grabamos estado de la inserción al Log.
                          logM.log( msj, logM.INFO );
                      
                     }catch(Exception kk){                         
                          logM.log( "ERROR  OC:" + op.getOc() + " CAUSA: " + kk.getMessage() , logM.INFO);
                     }
                     
                     
                 }                
                 logM.log("Finalización OP......",logM.INFO);                
                 pw.close();
            
                                  
                 
                
            }else
                comentario += " -> No se encontraron planillas válidas";     
            
            
            
            model.LiqOCSvc.setEnproceso();
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usuario.getLogin(),comentario);
            
            
            
        }catch(Exception e){
            e.printStackTrace();
            model.LiqOCSvc.setEnproceso();
            try{model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usuario.getLogin(),"ERROR Hilo: " + e.getMessage()); }
            catch(Exception f){}
        }
    }
    
    
    
    
    
    
    
    
}






