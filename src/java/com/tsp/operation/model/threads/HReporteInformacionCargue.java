/***************************************************************************
 * Nombre clase : ............... HReporteInformacionCargue.java           *
 * Descripcion :................. Hilo que permite generar un reporte de   *
 *                                informacion del formato de instrucciones *
 *                                de despacho + el formato de trakin       *
 * Autor :....................... Ing. Karen Reales                        *
 * Fecha :........................ 29 de Enero de 2007, 08:38   AM         *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/

package com.tsp.operation.model.threads;
import java.util.*;
import java.io.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import java.sql.*;

public class HReporteInformacionCargue extends Thread{
    
    private Vector reporte;
    private String fec1;
    private String fec2;
    private String user;
    private String ocompra;
    private String factura;
    private String destinatario;
    private String origen;
    private String destino;
    Model model;
    
    /** Creates a new instance of ReporteRegistroTiempoXLS */
    public HReporteInformacionCargue() {
    }
    
    public void start( String fec1,String fec2,String user,String ocompra, String factura, String destinatario, String origen, String destino) {
        this.fec1 = fec1;
        this.fec2 = fec2;
        this.user = user;
        this.ocompra=ocompra;
        this.factura=factura;
        this.destinatario=destinatario;
        this.origen=origen;
        this.destino=destino;
        model= new Model();
        super.start();
    }
    
    public synchronized void run(){
        
        try{
             model.LogProcesosSvc.InsertProceso("Generacion Reporte Traking LG", this.hashCode(), "Inicio de reporte traking LG" ,this.user);
            
            //  //System.out.println("Inicio...");
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta") + "/exportar/migracion";
            
            String ruta = path + "/" + user + "/";
            
            try{
                
                //        //System.out.println("Inicio BUSQUEDA...");
                model.instrucciones_despachoService.reporteTrakin(ocompra, factura, destinatario, origen, destino, fec1, fec2);
                reporte = model.instrucciones_despachoService.getLista();
                //     //System.out.println("FIN BUSQUEDA...");
                
            }catch(SQLException e){
                //  //System.out.println("Error llenando el informe "+e.getMessage());
                throw new SQLException("ESTE ES EL ERROR... " + e.getMessage() + " " + e.getErrorCode());
            }
            ////System.out.println("Se encontraron "+reporte.size()+" datos");
            
            fec1 = fec1.substring(0,10).replaceAll("-","");
            fec2 = fec2.substring(0,10).replaceAll("-","");
            
            String nombre=" ["+fec1+"]["+fec2+"]";
            if(!ocompra.equals("")){
                nombre = ocompra;
                fec1 ="";
                fec2 = "";
            }
            if(!factura.equals("")){
                nombre = factura;
                fec1 ="";
                fec2 = "";
            }
            
            
            String nombreArch= "ReporteTrakinLG"+nombre+".xls";
            String       Hoja  = "Formato Traking";
            String       Ruta  =ruta+ nombreArch;
            
            
            // Definicion de Estilos para la hoja de excel
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( Ruta);
            
            HSSFCellStyle fecha   = xls.nuevoEstilo("Verdana", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle texto   = xls.nuevoEstilo("Verdana", 10 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle texto3  = xls.nuevoEstilo("Verdana", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle texto2  = xls.nuevoEstilo("Verdana", 9 ,  true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle total   = xls.nuevoEstilo("Verdana", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
            HSSFCellStyle titulo2 = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.WHITE.index, HSSFColor.BLUE.index, HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero  = xls.nuevoEstilo("Verdana", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle titulo  = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle textoIzq  = xls.nuevoEstilo("Verdana", 9 ,  false    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            xls.obtenerHoja("Formato Traking");
            
            int fila = 1;
            int col  = 0;
            xls.combinarCeldas(fila, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO S.A"             , texto  );
            xls.combinarCeldas(2, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "LG Electronics Colombia Ltda."             , texto  );
            xls.combinarCeldas(3, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "Despachos del "+fec1+" al "+fec2           , texto  );
            
            xls.cambiarAnchoColumna(0, 4000);
            xls.cambiarAnchoColumna(1, 4000);
            for(int i =2; i<=59;i++){
                xls.cambiarAnchoColumna(i, 6000);
            }
            xls.cambiarAnchoColumna(6, 15000);
            xls.cambiarAnchoColumna(17, 15000);
            xls.cambiarAnchoColumna(22, 10000);
            xls.cambiarAnchoColumna(37, 10000);
            xls.cambiarAnchoColumna(22, 15000);
            
            fila++;
            
            
            xls.adicionarCelda(fila,0 , "DESP" , titulo2 );
            xls.adicionarCelda(fila,1 , "F.VENCIM" , titulo2 );
            xls.adicionarCelda(fila,2 , "ORDEN #" , titulo2 );
            xls.adicionarCelda(fila,3 , "FACT" , titulo2 );
            xls.adicionarCelda(fila,4 , "PED" , titulo2 );
            xls.adicionarCelda(fila,5 , "VENDEDOR" , titulo2 );
            xls.adicionarCelda(fila,6 , "CLIENTE" , titulo2 );
            xls.adicionarCelda(fila,7 , "ITEM" , titulo2 );
            xls.adicionarCelda(fila,8 , "QTY" , titulo2 );
            xls.adicionarCelda(fila,9 , "QTY INST" , titulo2 );
            xls.adicionarCelda(fila,10 , "CUB  UNIT" , titulo2 );
            xls.adicionarCelda(fila,11 , "CUB TOTAL" , titulo2 );
            xls.adicionarCelda(fila,12 , "PRECIO" , titulo2 );
            xls.adicionarCelda(fila,13 , "DCT" , titulo2 );
            xls.adicionarCelda(fila,14 , "TOTAL" , titulo2 );
            xls.adicionarCelda(fila,15 , "FECHA" , titulo2 );
            xls.adicionarCelda(fila,16 , "F FACT" , titulo2 );
            xls.adicionarCelda(fila,17, "DIRECCION" , titulo2 );
            xls.adicionarCelda(fila,18 , "CIUDAD_DES" , titulo2 );
            xls.adicionarCelda(fila,19 , "#  OC" , titulo2 );
            xls.adicionarCelda(fila,20 , "CONDICION" , titulo2 );
            xls.adicionarCelda(fila,21 , "ALMACENADORA" , titulo2 );
            xls.adicionarCelda(fila,22 , "TRANSPORTADORA" , titulo2 );
            xls.adicionarCelda(fila,23 , "OBSERVACIONES" , titulo2 );
            xls.adicionarCelda(fila,24 , "OBSERVACIONES FACTURA" , titulo2 );
            xls.adicionarCelda(fila,25 , "INST ESPECIALES" , titulo2 );
            xls.adicionarCelda(fila,26 , "ITEM(1)" , titulo2 );
            xls.adicionarCelda(fila,27, "LOC" , titulo2 );
            xls.adicionarCelda(fila,28 , "APPOINT_DATE" , titulo2 );
            xls.adicionarCelda(fila,29 , "APPOINT_TIME" , titulo2 );
            xls.adicionarCelda(fila,30 , "IDENT" , titulo2 );
            xls.adicionarCelda(fila,31 , "FECHA_DESP" , titulo2 );
            xls.adicionarCelda(fila,32 , "E+" , titulo2 );
            xls.adicionarCelda(fila,33 , "F.SALIDA ALMM" , titulo2 );
            xls.adicionarCelda(fila,34 , "No. PLANILLA" , titulo2 );
            xls.adicionarCelda(fila,35 , "QTY DESPACHADA" , titulo2 );
            xls.adicionarCelda(fila,36 , "PLACA" , titulo2 );
            xls.adicionarCelda(fila,37 , "CONDUCTOR" , titulo2 );
            xls.adicionarCelda(fila,38 , "OBSERVACIONES ALMM" , titulo2 );
            xls.adicionarCelda(fila,39 , "VERIFICACION #1" , titulo2 );
            xls.adicionarCelda(fila,40 , "REMESA" , titulo2 );
            xls.adicionarCelda(fila,41 , "FECHA CITA" , titulo2 );
            xls.adicionarCelda(fila,42 , "QTY   ENTREGADA A CLIENTE" , titulo2 );
            xls.adicionarCelda(fila,43 , "FECHA DE ENTREGA A CLIENTE" , titulo2 );
            xls.adicionarCelda(fila,44 , "OBSERVACIONES DE TRANSPORTADORA" , titulo2 );
            xls.adicionarCelda(fila,45 , "QTY NOVEDAD" , titulo2 );
            xls.adicionarCelda(fila,46 , "No NOVEDAD" , titulo2 );
            xls.adicionarCelda(fila,47 , "MOTIVO DE LA NOVEDAD (CODIGO)" , titulo2 );
            xls.adicionarCelda(fila,48 , "FECHA ENTREGA DE LA DEVOLUCION A LA ALMACENADORA" , titulo2 );
            xls.adicionarCelda(fila,49 , "QTY ENTREGADA EN LA ALMACENADORA" , titulo2 );
            xls.adicionarCelda(fila,50 , "OBSERVACIONES DE LA ENTREGA  A  ALMM" , titulo2 );
            xls.adicionarCelda(fila,51 , "FECHA DE REPORTE NOVEDAD" , titulo2 );
            xls.adicionarCelda(fila,52, "VERIFICACION #2" , titulo2 );
            xls.adicionarCelda(fila,53, "VERIFICACION #3" , titulo2 );
            xls.adicionarCelda(fila,54, "TIEMPO DE CARGUE" , titulo2 );
            xls.adicionarCelda(fila,55 , "TIEMPO DE ENTREGA" , titulo2 );
            xls.adicionarCelda(fila,56, "ROTTERDAN" , titulo2 );
            xls.adicionarCelda(fila,57, "PROVEEDOR" , titulo2 );
            xls.adicionarCelda(fila,58, "FORMULA" , titulo2 );
            xls.adicionarCelda(fila,59 , "FALT" , titulo2 );
            xls.adicionarCelda(fila,60 , "TIENE IMAGEN" , titulo2 );
            
            
            
            
            for(int i =0; i<reporte.size();i++){
                Instrucciones_Despacho inst = (Instrucciones_Despacho) reporte.elementAt(i);
                fila++;
                System.out.println("Registro "+i);
                xls.adicionarCelda(fila,0 , inst.getDesp() , numero );
                xls.adicionarCelda(fila,1 , inst.getExpire_date(), texto3 );
                xls.adicionarCelda(fila,2 , inst.getOrder_no() , texto3 );
                xls.adicionarCelda(fila,3 , inst.getFactura() , texto3 );
                xls.adicionarCelda(fila,4 , "" , texto3 );
                xls.adicionarCelda(fila,5 , "" , texto3 );
                xls.adicionarCelda(fila,6 , inst.getShip_to_name() , textoIzq );
                xls.adicionarCelda(fila,7 , inst.getModelo() , texto3 );
                xls.adicionarCelda(fila,8 , inst.getOrder_qty(), texto3 );
                xls.adicionarCelda(fila,9 , "" , texto3 );
                xls.adicionarCelda(fila,10 , inst.getCub_unit() , numero );
                xls.adicionarCelda(fila,11 , inst.getTotal_volumen() , numero );
                xls.adicionarCelda(fila,12 ,"" , texto3 );
                xls.adicionarCelda(fila,13 , "" , texto3 );
                xls.adicionarCelda(fila,14 , "" , texto3 );
                xls.adicionarCelda(fila,15 , "" , texto3 );
                xls.adicionarCelda(fila,16 , inst.getFecha_factura() , texto3 );
                xls.adicionarCelda(fila,17, inst.getAddr1() , textoIzq );
                xls.adicionarCelda(fila,18 , inst.getCiudad() , textoIzq );
                xls.adicionarCelda(fila,19 , inst.getNumpla() , texto3 );
                xls.adicionarCelda(fila,20 , "" , texto3 );
                xls.adicionarCelda(fila,21 , inst.getBodega() , texto3 );
                xls.adicionarCelda(fila,22 , "TRANSPORTES SANCHEZ POLO" , texto3 );
                xls.adicionarCelda(fila,23 , inst.getObservacion() , texto3 );
                xls.adicionarCelda(fila,24 , "" , texto3 );
                xls.adicionarCelda(fila,25 , inst.getInst_especiales() , texto3 );
                xls.adicionarCelda(fila,26 , inst.getItem() , texto3 );
                xls.adicionarCelda(fila,27, inst.getLoc() , texto3 );
                xls.adicionarCelda(fila,28 , inst.getAppoint_date() , texto3 );
                xls.adicionarCelda(fila,29 ,inst.getAppoint_time() , texto3 );
                xls.adicionarCelda(fila,30 , "", texto3 );
                xls.adicionarCelda(fila,31 , inst.getFechaDesp(), texto3 );
                xls.adicionarCelda(fila,32 , "" , texto3 );
                xls.adicionarCelda(fila,33 , inst.getFechaDespRex(), texto3 );
                xls.adicionarCelda(fila,34 , inst.getNumplaRex(), texto3 );
                xls.adicionarCelda(fila,35 , inst.getCant_despachada(), numero );
                xls.adicionarCelda(fila,36 , inst.getPlaca() , texto3 );
                xls.adicionarCelda(fila,37 , inst.getConductor() , texto3 );
                xls.adicionarCelda(fila,38 , "" , texto3 );
                xls.adicionarCelda(fila,39 , "" , texto3 );
                xls.adicionarCelda(fila,40 , inst.getNumrem() , texto3 );
                xls.adicionarCelda(fila,41 , inst.getFecha_cargue() , texto3 );
                xls.adicionarCelda(fila,42 ,inst.getCantida_entregada(), numero );
                xls.adicionarCelda(fila,43 , inst.getFechaEntrega() , texto3 );
                xls.adicionarCelda(fila,44 ,inst.getObservacion_novedad() , texto3 );
                xls.adicionarCelda(fila,45 , inst.getCantida_novedad(), numero );
                xls.adicionarCelda(fila,46 , inst.getCodigo_novedad() , texto3 );
                xls.adicionarCelda(fila,47 ,inst.getMotivo_novedad() , texto3 );
                xls.adicionarCelda(fila,48 , inst.getFecha_devolucion() , texto3 );
                xls.adicionarCelda(fila,49 , inst.getFecha_devolucion().equals("")?0:inst.getCantida_novedad() , texto3 );
                xls.adicionarCelda(fila,50 , inst.getFecha_devolucion().equals("")?"":inst.getObservacion_devolucion()  , texto3 );
                xls.adicionarCelda(fila,51 ,  inst.getFecha_novedad() , texto3 );
                xls.adicionarCelda(fila,52,  ""  , texto3 );
                xls.adicionarCelda(fila,53,  ""  , texto3 );
                xls.adicionarCelda(fila,54,  ""  , texto3 );
                xls.adicionarCelda(fila,55 ,  ""  , texto3 );
                xls.adicionarCelda(fila,56,  ""  , texto3 );
                xls.adicionarCelda(fila,57,  ""  , texto3 );
                xls.adicionarCelda(fila,58,  ""  , texto3 );
                xls.adicionarCelda(fila,59 ,  ""  , texto3 );
                xls.adicionarCelda(fila,60 ,  !inst.getFactura().equals("") && model.ImagenSvc.existeImagen("014","FAC",inst.getFactura())?"SI":"NO"  , texto3 );
                
            }
            
            
            
            xls.cerrarLibro();
           model.LogProcesosSvc.finallyProceso( "Generacion Reporte Traking LG", this.hashCode(), user, "PROCESO FINALIZADO CON EXITO!" );
        }catch(Exception e){
            //System.out.println("Hay un error: "+e.getMessage());
            
            try{
                
                model.LogProcesosSvc.finallyProceso( "Generacion Reporte Traking LG", this.hashCode(), user, "ERROR : " + e.getMessage() );
                
            } catch( Exception f ){
                
                f.printStackTrace();
                
                try{
                    
                    model.LogProcesosSvc.finallyProceso( "Generacion Reporte Traking LG",this.hashCode(), user, "ERROR : " + f.getMessage() );
                    
                } catch( Exception p ){ p.printStackTrace(); }
                
            }
        }
    }
    
    public static void main(String a [])throws SQLException{
        HReporteInformacionCargue hilo = new HReporteInformacionCargue();
        hilo.start( "2007-04-01 00:00","2007-04-17 00:00","KREALES","","", "", "","");
    }
}
