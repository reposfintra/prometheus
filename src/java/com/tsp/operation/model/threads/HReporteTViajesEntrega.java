/***************************************************************************
 * Nombre clase : ............... HReporteTViajesEntrega.java              *
 * Descripcion :................. Hilo que permite generar un reporte de   *
 *                                tiempos de viajes entregados             *
 * Autor :....................... Ing. Karen Reales                        *
 * Fecha :........................ 4 de Marzo de 2006, 11:38   AM          *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/

package com.tsp.operation.model.threads;
import java.util.*;
import java.io.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

public class HReporteTViajesEntrega extends Thread{
    private Model model;
    private Vector reporte;
    private String fec1;
    private String fec2;
    private String tipo;
    private Vector agencia;
    private String usuario;
    /** Creates a new instance of ReporteRegistroTiempoXLS */
    public HReporteTViajesEntrega() {
    }
    static Logger logger = Logger.getLogger(HReporteTViajesEntrega.class);
    
    public void start(String tipo, String fechai,String fechaf,Vector agencia,String usuario) {
        
        model=new Model();
        this.fec1 = fechai;
        this.fec2 = fechaf;
        this.tipo=tipo;
        this.agencia= agencia;
        this.usuario=usuario;
        try{
            model.LogProcesosSvc.InsertProceso("Generacion Reporte Tiempos de Viajes Entregados", this.hashCode(), "Reporte Tiempos de Viajes Entregados",usuario);
        }catch(Exception e){
            ////System.out.println("Error insertando el log");
        }
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            ////System.out.println("Empezamos...");
            
            
            
            
            /*
             *Creamos el archivo donde se va a guardar el reporte.
             */
            String nombre = "";
            if(agencia.size()==1){
                nombre = (String) agencia.elementAt(0);
            }
            else{
                nombre = "TODAS";
            }
            String nombreArch= "ReporteTViajes"+nombre+"["+fec1.replaceAll("-","")+"]["+fec2.replaceAll("-","")+"].xls";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta") + "/exportar/migracion";
            String Ruta  =path + "/"+usuario+"/"+nombreArch;
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( Ruta);
            
            HSSFCellStyle fecha   = xls.nuevoEstilo("Verdana", 8  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle texto   = xls.nuevoEstilo("Verdana", 8 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle textoRojo   = xls.nuevoEstilo("Verdana", 8 , true    , false, "text"       , HSSFColor.RED.index , xls.NONE ,HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle texto2  = xls.nuevoEstilo("Verdana", 8 ,  false    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle titulo2 = xls.nuevoEstilo("Verdana", 8  , true    , false, "text"       , HSSFColor.WHITE.index, HSSFColor.BLUE.index, HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle promedios = xls.nuevoEstilo("Verdana", 8  , true    , false, "text"       ,xls.NONE , xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle tituloOtro = xls.nuevoEstilo("Verdana", 8  , true    , false, "text"       , HSSFColor.WHITE.index, HSSFColor.LIME.index, HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle azulClaro = xls.nuevoEstilo("Verdana", 8  , true    , false, "text"       , HSSFColor.BLACK.index, HSSFColor.LIGHT_BLUE.index, HSSFCellStyle.ALIGN_CENTER);
            /*
             *Se crea una hoja para los promedios
             */
            
            xls.obtenerHoja("PROMEDIOS");
            
            int fila = 1;
            int col  = 0;
            xls.combinarCeldas(fila, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO"             , texto  );
            xls.combinarCeldas(2, col, fila, col+3);
            xls.combinarCeldas(3, col, fila, col+3);
            xls.combinarCeldas(4, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "FECHA INICIO: "+fec1+" FECHA FIN:"+fec2             , texto  );
            xls.adicionarCelda(fila++ ,col   , "PROMEDIOS DE TIEMPOS ", texto  );
            for(int a=0; a<agencia.size();a++ ){
                
                String agency = (String) agencia.elementAt(a);
                
                Vector clientes = model.TiempoViajeSvc.searchClientes(agency);
                
                if(clientes.size()>0){
                    
                    boolean swReporte =false;
                    
                    for (int k  = 0; k<clientes.size();k++){
                        Cliente c = (Cliente) clientes.elementAt(k);
                        model.TiempoViajeSvc.searchOTs(tipo, fec1, fec2, c.getCodcli());
                        reporte = model.TiempoViajeSvc.getOTs();
                        if(reporte.size()>0){
                            swReporte=true;
                        }
                    }
                    if(swReporte){
                        fila+=3;
                        
                        xls.combinarCeldas(fila, 1, fila,2);
                        xls.adicionarCelda(fila ,1   , "AGENCIA "+model.ciudadService.buscarNomCiudadxCodigo(agency), titulo2  );
                        
                        fila++;
                        xls.combinarCeldas(fila, 1, fila,2);
                        xls.adicionarCelda(fila ,1   , "PROMEDIOS TIEMPOS POR CLIENTE"             , azulClaro  );
                        
                        fila ++;
                        xls.cambiarAnchoColumna(1, 11000);
                        xls.adicionarCelda(fila ,1   , "CLIENTE"             , tituloOtro  );
                        xls.cambiarAnchoColumna(2, 6500);
                        xls.adicionarCelda(fila ,2   , "TIEMPO PROMEDIO(DIAS)"             , tituloOtro  );
                        
                        
                        for (int k  = 0; k<clientes.size();k++){
                            
                            Cliente c = (Cliente) clientes.elementAt(k);
                            model.TiempoViajeSvc.searchOTs(tipo, fec1, fec2, c.getCodcli());
                            reporte = model.TiempoViajeSvc.getOTs();
                            
                            if(reporte.size()>0){
                                fila++;
                                
                                xls.adicionarCelda(fila,1 , c.getCodcli()+"  "+c.getNomcli() , promedios );
                                
                                double total = 0;
                                for (int j  = 0; j<reporte.size();j++){
                                    OTs ot = (OTs)reporte.elementAt(j);
                                    total = ot.getDias()+total;
                                }
                                double promedio = total/reporte.size();
                                
                                xls.adicionarCelda(fila,2 ,com.tsp.util.Util.redondear(promedio,2),  textoRojo);
                                
                            }
                            
                        }
                    }
                }
            }
            xls.obtenerHoja("DETALLE");
            fila = 1;
            col  = 0;
            
            xls.combinarCeldas(fila, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO"             , texto  );
            xls.combinarCeldas(2, col, fila, col+3);
            xls.combinarCeldas(3, col, fila, col+3);
            xls.combinarCeldas(4, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "FECHA INICIO: "+fec1+" FECHA FIN:"+fec2             , texto  );
            xls.adicionarCelda(fila++ ,col   , "DETALLE DE ENTREGAS ", texto  );
            
            fila+=2;
            /*
             *Se crea una hoja por cliente de la agencia
             */
            xls.cambiarAnchoColumna(0, 5000);
            xls.adicionarCelda(fila,0 , "Agencia" , titulo2 );
            xls.cambiarAnchoColumna(1, 5000);
            xls.adicionarCelda(fila,1 , "Remesa" , titulo2 );
            xls.cambiarAnchoColumna(2, 10900);
            xls.adicionarCelda(fila,2 , "Standar" , titulo2 );
            xls.cambiarAnchoColumna(3, 7000);
            xls.adicionarCelda(fila,3 , "Origen", titulo2 );
            xls.cambiarAnchoColumna(4, 7000);
            xls.adicionarCelda(fila,4 , "Destino", titulo2 );
            xls.cambiarAnchoColumna(5, 6000);
            xls.adicionarCelda(fila,5 , "Factura Comercial", titulo2 );
            xls.cambiarAnchoColumna(6, 5000);
            xls.adicionarCelda(fila,6 , "Tipo de Viaje", titulo2 );
            xls.cambiarAnchoColumna(7, 5000);
            xls.adicionarCelda(fila,7 , "Fecha de Salida", titulo2 );
            xls.cambiarAnchoColumna(8, 5000);
            xls.adicionarCelda(fila,8 , "Fecha de Entrega", titulo2 );
            xls.cambiarAnchoColumna(9, 5000);
            xls.adicionarCelda(fila,9 , "Tiempo (Dias)", titulo2 );
            
            for(int a=0; a<agencia.size();a++ ){
                
                String agency = (String) agencia.elementAt(a);
                Vector clientes = model.TiempoViajeSvc.searchClientes(agency);
                
                for (int k  = 0; k<clientes.size();k++){
                    
                    Cliente c = (Cliente) clientes.elementAt(k);
                    
                    model.TiempoViajeSvc.searchOTs(tipo, fec1, fec2, c.getCodcli());
                    reporte = model.TiempoViajeSvc.getOTs();
                    
                    if(reporte.size()>0){
                        for (int j  = 0; j<reporte.size();j++){
                            OTs ot = (OTs)reporte.elementAt(j);
                            
                            fila++;
                            xls.adicionarCelda(fila,0 , model.ciudadService.buscarNomCiudadxCodigo(agency), texto2 );
                            xls.adicionarCelda(fila,1 , ot.getOT() , texto2 );
                            xls.adicionarCelda(fila,2 , ot.getDesc(), texto2 );
                            xls.adicionarCelda(fila,3 , ot.getOrigen(), texto2 );
                            xls.adicionarCelda(fila,4 ,ot.getDestino(), texto2 );
                            xls.adicionarCelda(fila,5 , ot.getFactComercial(), texto2 );
                            xls.adicionarCelda(fila,6 , ot.getTipoViaje(), texto2 );
                            xls.adicionarCelda(fila,7 , ot.getDespacho(), fecha );
                            xls.adicionarCelda(fila,8 , ot.getFechaentrega(), fecha );
                            xls.adicionarCelda(fila,9 , ot.getDias(), textoRojo );
                            
                        }
                    }
                    
                }
            }
            
            xls.cerrarLibro();
            
            model.LogProcesosSvc.finallyProceso("Generacion Reporte Tiempos de Viajes Entregados", this.hashCode(),usuario,"PROCESO FINALIZADO CON EXITO." );
            
        }catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso("Generacion Reporte Tiempos de Viajes Entregados", this.hashCode(),usuario,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                ////System.out.println("Hay un error: "+f.getMessage());
            }
            ////System.out.println("Hay un error: "+e.getMessage());
        }
    }
    
    public static void main(String a [])throws SQLException{
        Vector agencia = new Vector();
        
        agencia.add("BG");
        
        HReporteTViajesEntrega hilo = new HReporteTViajesEntrega();
        hilo.start("ALL","2006-06-01","2006-06-30",agencia,"KREALES");
    }
    
    
}
