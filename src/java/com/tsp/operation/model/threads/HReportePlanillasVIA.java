    /***************************************
    * Nombre Clase ............. HReportePlanillasVIA.java
    * Descripci�n  .. . . . . .  Permite Generar Reporte planillas por via
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.threads;




import com.tsp.operation.model.beans.POIWrite;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import java.io.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;



public class HReportePlanillasVIA  extends Thread{
    
    
    private  Model     model;
    private  Usuario   usuario ;
    private  String    procesoName;
    
    private  String    fechaIni;
    private  String    fechaFin;
    private  String    origen;
    private  String    destino;
    private  String    via;
    private  List      cliente;
    private  String    distrito;
    
    
    
    private String             url;
    private POIWrite           Excel; 
    private String             hoja;
    private int                fila;
    private int                columna;  

    private HSSFCellStyle      texto;
    private HSSFCellStyle      negrillaGra;
    private HSSFCellStyle      negrillaPeq;
    private HSSFCellStyle      numero; 
    private HSSFCellStyle      titulo;
    
    
    
    
    
    
    
    public HReportePlanillasVIA() {
    }
    
    
    
    
    /**
     * M�todo inicializa  el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void start(Model modelo,  Usuario user,  List cliente, String fechaIni, String fechaFin, String origen, String destino, String via ) throws Exception{
         try{
            this.model       = modelo;
            this.usuario     = user;
            this.procesoName = "GENERACION DE REPORTE PLANILLAS POR VIA ";
            
            this.cliente     = cliente;
            this.fechaIni    = fechaIni;
            this.fechaFin    = fechaFin;
            this.origen      = origen;
            this.destino     = destino;
            this.via         = via;
            this.distrito    = this.usuario.getDstrct();
            
            super.start();
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
    
    /**
     * M�todo que ejecuta el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public synchronized void run(){
       try{
           
           String comentario="EXITOSO";
           model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode()," Reporte planillas por via " + via  +"["+ fechaIni +" "+ fechaFin +"]"  , this.usuario.getLogin() ); 
           
           List pcs =  model.ReportePlanillasViaSvc.getPCs(via);
           
           
           configXLS();
           createHoja("Viajes");
           titulos(pcs);
           
           
           for(int j=0; j<cliente.size(); j++){
                Hashtable  ht     = (Hashtable)cliente.get(j);
                String     codigo = (String) ht.get("codigo");
                
                model.ReportePlanillasViaSvc.searchPlanillas(distrito, codigo, fechaIni, fechaFin, origen, destino, via);
                List lista = model.ReportePlanillasViaSvc.getLista();
                if(lista.size()>0){

                     for(int i=0;i<lista.size();i++){
                         Hashtable  oc = (Hashtable)lista.get(i);
                         addPlanilla(oc, pcs);                     
                     }
                }
           
           }
            
            
            
           Excel.cerrarLibro();
           
           model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);
           
           
             
       }catch(Exception e){
           try{
               model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage()); 
           }
           catch(Exception f){ }   
       }
       
    }
    
    
     
     
     
     
     
    
     
    
    
    /**
     * M�todo que configura parametros de Excel
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public void configXLS() throws Exception{
       try{          
            DirectorioService dd = new DirectorioService();
            dd.create( this.usuario.getLogin()  );
            url = dd.getUrl() +  this.usuario.getLogin() + "/ReporteTraficoPlanillas_"+ Util.getFechaActual_String(6).replaceAll("/|:| ","") + ".xls";
            
            this.Excel  = new POIWrite(url);
            createStyle();
            
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
     
     
     
    /**
     * M�todo que Define estilo del archivo xls
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void createStyle()throws Exception{
        try{
          this.texto       = Excel.nuevoEstilo("Tahoma", 7,   false , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);
          this.numero      = Excel.nuevoEstilo("Tahoma", 7,   false , false, ""            , Excel.NONE , Excel.NONE , HSSFCellStyle.ALIGN_RIGHT);
          this.titulo      = Excel.nuevoEstilo("Tahoma", 8,   true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLACK.index  , HSSFCellStyle.ALIGN_CENTER);              
          this.negrillaPeq = Excel.nuevoEstilo("Tahoma", 10,  true  , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);          
          this.negrillaGra = Excel.nuevoEstilo("Tahoma", 12,  true  , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);          
           
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
     /**
     * M�todo que crea la hoja
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void createHoja(String nombreHoja)throws Exception{
        try{
            
            hoja = nombreHoja;
            this.Excel.obtenerHoja(hoja);             
            setearFila();
            setearColumna();
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
    
     /**
     * M�todo que establece titulos xls
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void titulos(List lista)throws Exception{
        try{            
            
            String  fecha = Util.getFechaActual_String(6).replaceAll("/|:","");
            
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "TRANSPORTES SANCHEZ POLO S.A",                                                                                                               this.negrillaGra ); incFila();
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "REPORTE DE VIAJES",                                                                                                                          this.negrillaGra ); incFila();
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "RUTA    " +  model.ReportePlanillasViaSvc.getNombreCiudad(this.origen) +" - "+  model.ReportePlanillasViaSvc.getNombreCiudad(this.destino),  this.negrillaPeq ); incFila();
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "RANGO FECHA VIAJES  " + this.fechaIni +" - "+ this.fechaFin + "  [" + via +"]",                                                                                 this.negrillaPeq ); incFila();
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "FECHA REPORTE "       + fecha,                                                                                                               this.negrillaPeq ); incFila();
            
            incFila();
            
            
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CLIENTE",    this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "NOMBRE",     this.titulo); incColumna();           
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "OT",         this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "OC",         this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "PLACA",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 6000 );  Excel.adicionarCelda( this.fila, this.columna, "CONDUCTOR",  this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 2000 );  Excel.adicionarCelda( this.fila, this.columna, "VACIO",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "SALIDA",     this.titulo); incColumna();
            
            for(int i=0;i<lista.size();i++){
                Hashtable  pc     = (Hashtable) lista.get(i);
                String     puesto = (String) pc.get("puesto");
                String     nombre = (String) pc.get("nombre");                
                Excel.cambiarAnchoColumna( columna, 6000 );  Excel.adicionarCelda( this.fila, this.columna,  nombre,     this.titulo); incColumna();
            }
            
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "PARQUEDERO",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "SALIDA PARQUEO",   this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "ENTREGA",          this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "SITIO PERNOCTADA", this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA INI PERNOCT",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA REINICI PERNOCT",   this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "STANDARD",         this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "INI_DEMORA",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "FIN_DEMORA",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "EST_DEMORA",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "SEGUN ESTANDAR",   this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "REAL",             this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "DIFERENCIA",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "ESTADO",           this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "TVIAJE",           this.titulo); incColumna();
            
            
            
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "TPERNOC",          this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "TDEMEXT",          this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "TEFECT",           this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "TEFECT-DEMEXT",    this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "ESPERA DESCARGUE", this.titulo); incColumna();
            
            
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "PERMANENCIA MONTELIBANO", this.titulo); incColumna();
            
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "NOVEDADES",        this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "OBSERVACIONES",    this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "CARGA",            this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 10000 );  Excel.adicionarCelda( this.fila, this.columna, "DOCINT",          this.titulo); incColumna();

            incFila();
            setearColumna();            
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
     /**
     * M�todo que adiciona informacion de la planilla al reporte
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void addPlanilla(Hashtable oc, List listaPC)throws Exception{
        try{           
           
            oc = model.ReportePlanillasViaSvc.complementar(oc, listaPC );
            
            
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("cliente")        ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("cliente_name")   ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("ot")             ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("oc")             ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("placa")          ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("conductor")      ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("vacio")          ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("salida")         ,this.texto); incColumna();
            
            
            List  lista  = (List)oc.get("fechasReportes");
            for(int i=0;i<lista.size();i++){
                String  fechaReport = (String)lista.get(i);           
                Excel.adicionarCelda( this.fila, this.columna,   fechaReport ,  this.texto); incColumna();
            }
            
           
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("fechaParqueos")       ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("salidaParqueos")      ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("entrega")             ,this.texto); incColumna();
             Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("sitiosPernotadas")    ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("fechaPernotada")      ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("fechaInicio")         ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("tiempo")              ,this.texto); incColumna(); 
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("inicio_demora")       ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("fin_demora")          ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("estado_demora")       ,this.texto); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("llegada")             ,this.texto); incColumna(); //Segun Standar            
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("entrega")             ,this.texto); incColumna(); //Real
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("diferencia")          ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("estado")              ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("tViaje")              ,this.texto); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("difPernotadas")       ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("difDemoras")          ,this.texto); incColumna();
            
            
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("tEfectivo")          ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("tEfec_demora")       ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("descargue")          ,this.texto); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("pcMontelibano")      ,this.texto); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("novedad")             ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("observacion")         ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("tipocarga")           ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)oc.get("documento")           ,this.texto); incColumna();
            
            incFila();
            setearColumna();            
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
   
    
    
    
    public void incFila()      {  this.fila++;      }
    public void incColumna()   {  this.columna++;   }    
    public void setearFila()   {  this.fila=0;      }
    public void setearColumna(){  this.columna=0;   }
    
    
    
    
}
