/*
* Nombre        HExportarExcel_UbicacionLLegada.java
* Descripción   Crea el reporte de excel de infocliente.
* Autor         fvillacob
* Fecha         22 de julio de 2005, 04:01 PM
* Ultima modificación: 10 de octubre de 2005.
* Versión       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.threads;




import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;


// EXCEL
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;



/** 
 * Crea el reporte de excel de ubicación y llegada de equipos en ruta.
 * Esta clase fue modificada por Alejandro Payares.
 * Ultima modificación: 10 de octubre de 2005.
 * @author fvillacob
 */
public class HExportarExcel_UbicacionLLegada extends ReporteExcelSOT{
    
    private   Model       model;
    private   String      unidad;
    private   String      fileName;
    private   Usuario     usuario;
    private   Hashtable   header;
    private   String[]    args;
    private int filaActual = -1;
    private String [] campos;
            String procesoName;
            String des;
            String usu;    
    
    
    
    public HExportarExcel_UbicacionLLegada() {
        String hoy    = Utility.getDate(6).replaceAll("/","").replaceAll(":","_");
        this.fileName = "Ubicacion_"+hoy;
    }
    
    
    public void start(  Model model, Usuario user, Hashtable head, String[] argumentos) throws Exception{
        try{
            this.procesoName = "Exportar exel Ubicacion vehicular";
            this.des = "Reporte Documentos Digitalizados ubicacion Vehicular";
            this.model       = model;
            this.header      = head;
            this.usuario     = user;
            this.usu         = user.getLogin(); 
            this.args        = argumentos;
            String dir       = user.getLogin();
            this.unidad      = model.DirectorioSvc.getUrl() + dir;
            //this.unidad = "c:";
            this.model.DirectorioSvc.create(dir);
            this.path        = unidad +"/"+ fileName + ".xls";
            initExcel(fileName);
            new Thread(this,fileName).start();
        }catch(Exception e){ throw new Exception( " Hilo: "+ e.getMessage());}
    }
    
    
    
    
    
    
    
    /**
     * Agrega las filas correspondientes al encabezado del reporte
     * @param fecha la fecha de creación del reporte
     * @param listaTipoViaje la lista de tipos de viaje escogidos
     * @param estadoViajes los estados de viaje escogidos
     * @param placa las placas consultadas
     * @param tipo el tipo de reporte
     * @param planilla las planillas consultadas
     */    
    private void crearEncabezado(boolean fecha, String listaTipoViaje,   String estadoViajes , String placa,    String tipo, String planilla){
        try {
            int columna = -1;
            filaActual++;
            if ( fecha ){
                crearCelda(filaActual, ++columna);
                cell.setCellStyle(estiloAzulGrande);
                cell.setCellValue("FECHA INICIAL");
                
                crearCelda(filaActual+1, columna);
                cell.setCellValue((String)header.get("fechaInicial"));
                
                crearCelda(filaActual, ++columna);
                cell.setCellStyle(estiloAzulGrande);
                cell.setCellValue("FECHA FINAL");
                
                crearCelda(filaActual+1, columna);
                cell.setCellStyle(estiloBlanco);
                cell.setCellValue((String)header.get("fechaFinal"));
            }
            crearCelda(filaActual, ++columna);
            cell.setCellStyle(estiloAzulGrande);
            cell.setCellValue("TIPOS DE VIAJE");
            
            crearCelda(filaActual+1, columna);
            cell.setCellStyle(estiloBlanco);
            cell.setCellValue(listaTipoViaje);
            
            crearCelda(filaActual, ++columna);
            cell.setCellStyle(estiloAzulGrande);
            cell.setCellValue("ESTADO DE VIAJE");
            
            crearCelda(filaActual+1, columna);
            cell.setCellStyle(estiloBlanco);
            cell.setCellValue(estadoViajes);
            
            if ( placa != null ){
                crearCelda(filaActual, ++columna);
                cell.setCellStyle(estiloAzulGrande);
                cell.setCellValue("LISTA DE "+tipo.toUpperCase());
                
                crearCelda(filaActual+1, columna);
                cell.setCellStyle(estiloBlanco);
                cell.setCellValue(placa);
            }
            
            if( planilla != null ){
                this.crearCelda(filaActual, ++columna);
                cell.setCellStyle(estiloAzulGrande);
                cell.setCellValue("Lista de OC(s) / Planilla(s)".toUpperCase());
                
                crearCelda(filaActual+1, columna);
                cell.setCellStyle(estiloBlanco);
                cell.setCellValue(planilla);
            }
            
            crearCelda(filaActual, ++columna);
            cell.setCellStyle(estiloAzulGrande);
            cell.setCellValue("ULTIMA ACTUALIZACION");
            
            crearCelda(filaActual+1, columna);
            cell.setCellStyle(estiloBlanco);
            cell.setCellValue(leerUltimaActualizacion());
            
            crearCelda(filaActual, ++columna);
            HSSFCellStyle estilo = clonarEstilo(estiloAzulGrande);
            quitarBordeAEstilo(estilo);
            cell.setCellStyle(estilo);
            cell.setCellValue("CONVENCIÓN DE COLORES PARA EL REPORTE");
            crearCelda(filaActual, columna+1);
            crearCelda(filaActual, columna+2);
            crearCelda(filaActual, columna+3);
            crearCeldaCombinada(filaActual, columna, filaActual, columna+3, true);
            
            
            crearCelda(filaActual+1, columna);
            cell.setCellStyle(this.estiloAmarillo);
            cell.setCellValue("En Ruta");
            
            crearCelda(filaActual+1, ++columna);
            cell.setCellStyle(this.estiloVerde);
            cell.setCellValue("Con Entrega");
            
            crearCelda(filaActual+1, ++columna);
            cell.setCellStyle(this.estiloRojo);
            cell.setCellValue("Con Problemas");
            
            crearCelda(filaActual+1, ++columna);
            cell.setCellStyle(estiloBlanco);
            cell.setCellValue("Por Confirmar Salida");
            
            filaActual++;
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    /**
     * Lee la ultima actualización de la base de datos SOT y devuelve el valor leido en un String
     * @return La fecha de la ultima actualización de los datos del reporte
     */  
    private String leerUltimaActualizacion(){
        try {
            File f = new File(header.get("rutaPaginaLastUpdate")+"/lastUpdateSotDb.html");
            FileInputStream fis = new FileInputStream(f);
            byte [] datos = new byte[fis.available()];
            fis.read(datos);
            return new String(datos);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return "(ninguna)";
    }
    
    
    public void copy(ReporteUbicacionEquipoRuta rptUbVeh) throws Exception{
        HSSFCellStyle estilo = estiloVerde;
        String cTieneDev = rptUbVeh.getDevoluciones().toUpperCase().trim();
        String cEstado   = rptUbVeh.getEstado();
        if (cTieneDev.equals("SI")) {
            estilo = estiloRojo;
        }
        else{
            String estado = cEstado.trim();
            estilo = (estado.equals("En Ruta")? estiloAmarillo:( estado.equals("Con Entrega")? estiloVerde: estiloBlanco));
        }
        prepared(++filaActual,campos.length);
        for( int i=0; i<campos.length; i++ ){
            cell = row.getCell((short)i);
            cell.setCellStyle(estilo);
            cell.setCellValue( rptUbVeh.obtenerValor(campos[i]));
        }
    }
    
    /**
     * Agrega las celdas que muestran el numero de registros encontrados
     * @param total el numero de registros o filas del reporte
     * @throws Exception si algun error ocurre
     */     
    public void mostrarCantidad(int total) throws Exception{
        try{
            //////System.out.println("campos: "+campos);
            prepared(++filaActual,campos.length);
            crearCelda(filaActual, 0);
            HSSFCellStyle estilo = clonarEstilo(estiloBlancoGrande);
            quitarBordeAEstilo(estilo);
            cell.setCellStyle(estilo);
            String s = total == 1? "":"s";
            cell.setCellValue(String.valueOf(total) +" Registro"+s+" encontrado" +s );
            crearCeldaCombinada(filaActual, 0, filaActual, campos.length-1, false);
            
        }catch(Exception e){ e.printStackTrace(); }
    }
    
    /**
     * Crea los titulos del reporte
     * @throws Exception si algún error ocurre
     */    
    public void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.usu);
             model.reporteUbicacionVehService.buscarDatosReporte(args);
            campos = model.reporteUbicacionVehService.obtenerCamposDeReporte();
            boolean hayFechaIni    = (header.get("fechaInicial") != null);
            String listaTipoViaje  = (String) header.get("listaTipoViaje");
            String estadoViajes    = (String) header.get("estadoViajes");
            String placasTrailers  = (String) header.get("placasTrailers");
            String tipoBusqueda    = (String) header.get("tipoBusqueda");
            String planilla        = (String) header.get("planillas");
            crearEncabezado(hayFechaIni, listaTipoViaje, estadoViajes, placasTrailers, tipoBusqueda, planilla);
            LinkedList ubicVehList = model.reporteUbicacionVehService.obtenerDatosReporte();
            mostrarCantidad(ubicVehList.size());
            this.crearTitulos();
            if(ubicVehList!=null && ubicVehList.size()>0){
                ReporteUbicacionEquipoRuta rptUbVeh = null;
                Iterator ubicVehIt  = ubicVehList.iterator();
                while( ubicVehIt.hasNext() ){
                    rptUbVeh = (ReporteUbicacionEquipoRuta)ubicVehIt.next();
                    copy(rptUbVeh);
                }
            }
            save();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.usu,"PROCESO EXITOSO");
             }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.usu,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usu,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
    
    
    public void crearTitulos() throws Exception {
        Hashtable titulos = model.reporteUbicacionVehService.obtenerTitulosDeReporte();
        this.prepared(++filaActual, campos.length);
        row.setHeightInPoints(32);
        for(int i=0; i<campos.length; i++ ){
            cell = row.getCell((short)i);
            cell.setCellStyle(estiloAzulGrande);
            cell.setCellValue((String)titulos.get(campos[i]));
            hojaExcel.setColumnWidth((short)i,(short)6376);
        }
    }
    
    
    
}
