/***************************************************************************
 * Nombre clase : ............... ReporteOCAnticipoTh.java                 *
 * Descripcion :................. Hilo que permite la creacion del reporte *
 *                                de OC con Anticipoen un archivo en excel *
 * Autor :.......................  Enrique De Lavalle Rizo                 *
 * Fecha :........................ 25 de noviembre de 2006, 12:40 AM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import org.apache.log4j.*;

public class ReporteOCAnticipoVaciosTh extends Thread{
    private List Lista;
    private String fechaini;
    private String fechafin;
    private String distrito;
    private String usuario;
 
    Model model = new Model ();
    
    
    /** Creates a new instance of ReporteAnulacionPlanilla */
    public ReporteOCAnticipoVaciosTh() {
    }
     public void start(List Lista, String fechaini, String fechafin, String distrito, String usuario) {
        this.Lista = Lista;
        this.fechaini = fechaini;
        this.fechafin = fechafin;
        this.distrito = distrito;
        this.usuario = usuario;
         //System.out.println("paso");
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso( "Reporte OC Vacio con anticipo sin reporte", this.hashCode(), "Proceso iniciado", usuario );
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            
            File file = new File(path + "/exportar/migracion/"+usuario);
            file.mkdirs();
            
            String nombreArch= "ReporteOCAnticipoVacios["+fechaini+"]["+fechafin+"].xls";
           
            String       Hoja  = "ReporteOCVaciosAnt";
            String       Hoja4  = "hoja adicional";
            
            String       Ruta  = path + "/exportar/migracion/"+ usuario +"/" + nombreArch;
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFSheet    sheet4 = wb.createSheet(Hoja4);
            
            HSSFRow      row   = null;
            HSSFRow      row4  = null;
            
            HSSFCell     cell  = null;
            
            int dias = 0;
            
            
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x1));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x1));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            //estilo5.setDataFormat(wb.createDataFormat().getFormat("####-##-## ##:##:##"));
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente4);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.BLACK.index);
           // estilo6.setDataFormat(wb.createDataFormat().getFormat("###,##"));
            
            
            /****************************************************/
            
            //hoja 1
            row  = sheet.createRow((short)(0));
            
            for (int j=0;j<10;j++) {
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
            }
            
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            
            sheet.createFreezePane(0,5);
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO - Reporte de OC de Vacios con  Anticipo sin Tr�fico");
            
            
            row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("");
            // fin hoja1
            
           
            //hoja 4
            row4  = sheet4.createRow((short)(0));
            
            for (int j=0;j<10;j++) {
                cell = row4.createCell((short)(j)); cell.setCellStyle(estilo1);
            }
            
            row4  = sheet4.createRow((short)(1));
            row4  = sheet4.createRow((short)(2));
            row4  = sheet4.createRow((short)(3));
            row4  = sheet4.createRow((short)(4));
            
            sheet4.createFreezePane(0,5);
            
            row4  = sheet4.getRow((short)(0));
            cell = row4.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO - Reporte de OC de Vacios con  Anticipo sin Reporte");
            
            
            row4  = sheet4.getRow((short)(1));
            cell = row4.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("");
            
            //fin hoja 4
            
            
            
            /*************************************************************************************/
            int Fila = 4;
            
            row  = sheet.createRow((short)(Fila));
            
            sheet.setColumnWidth((short)0, (short)5000 );
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Ag. Gen. OC");
            
            sheet.setColumnWidth((short)1, (short)5000 );
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Usr. Gen. OC");
            
            sheet.setColumnWidth((short)2, (short)5000 );
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OC");
            
            sheet.setColumnWidth((short)3, (short)3000 );
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Cont. OC.");
            
            sheet.setColumnWidth((short)4, (short)5000 );
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor OC");
            
            sheet.setColumnWidth((short)5, (short)3000 );
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Placa");
            
            sheet.setColumnWidth((short)6, (short)7000 );
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha. Gen. OC");
            
            sheet.setColumnWidth((short)7, (short)5500 );
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("D�as. Gen. OC");
            
            sheet.setColumnWidth((short)8, (short)7000 );
            cell = row.createCell((short)(8));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha. Imp. OC");
            
            sheet.setColumnWidth((short)9, (short)7000 );
            cell = row.createCell((short)(9));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha. Gen. Ant.");
            
            sheet.setColumnWidth((short)10, (short)5000 );
            cell = row.createCell((short)(10));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Vlr. Anticipo");
            
            sheet.setColumnWidth((short)11, (short)5000 );
            cell = row.createCell((short)(11));
            cell.setCellStyle(estilo3);
            cell.setCellValue("No. Cheq. Ant.");
            
            sheet.setColumnWidth((short)12, (short)7000 );
            cell = row.createCell((short)(12));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha. Imp. Cheq. Ant.");
            
            sheet.setColumnWidth((short)13, (short)13000 );
            cell = row.createCell((short)(13));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Ruta");
            
            sheet.setColumnWidth((short)14, (short)5000 );
            cell = row.createCell((short)(14));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Agen. Orig.");
            
            sheet.setColumnWidth((short)15, (short)7000 );
            cell = row.createCell((short)(15));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Agen. Dest.");
            
            sheet.setColumnWidth((short)16, (short)5000 );
            cell = row.createCell((short)(16));
            cell.setCellStyle(estilo3);
            cell.setCellValue("No. psto. Ctrl.");
            
            sheet.setColumnWidth((short)17, (short)7000 );
            cell = row.createCell((short)(17));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Desc. psto. Crtl.");
            
            sheet.setColumnWidth((short)18, (short)4000 );
            cell = row.createCell((short)(18));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OT");
            
            sheet.setColumnWidth((short)19, (short)5000 );
            cell = row.createCell((short)(19));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor OT");
            
            sheet.setColumnWidth((short)20, (short)15000 );
            cell = row.createCell((short)(20));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Desc. OT.");
            
            sheet.setColumnWidth((short)21, (short)8000 );
            cell = row.createCell((short)(21));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Cliente");
            
            sheet.setColumnWidth((short)22, (short)5000 );
            cell = row.createCell((short)(22));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fact Cliente");
            
            sheet.setColumnWidth((short)23, (short)6000 );
            cell = row.createCell((short)(23));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Ag. Anticipo");
            
            sheet.setColumnWidth((short)24, (short)6000 );
            cell = row.createCell((short)(24));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Ced. Gen. Ant.");
            
            sheet.setColumnWidth((short)25, (short)7000 );
            cell = row.createCell((short)(25));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Nombre gen Antcp");
            
            sheet.setColumnWidth((short)26, (short)5000 );
            cell = row.createCell((short)(26));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor. Flete");
            
            sheet.setColumnWidth((short)27, (short)7000 );
            cell = row.createCell((short)(27));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha. Cumpl. OC");
            
            sheet.setColumnWidth((short)28, (short)7000 );
            cell = row.createCell((short)(28));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Ced. Usr. Cumpl.");
            
            sheet.setColumnWidth((short)29, (short)7000 );
            cell = row.createCell((short)(29));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Cumplida por");
            
            sheet.setColumnWidth((short)30, (short)7000 );
            cell = row.createCell((short)(30));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Cod. Cliente");
            
                     
            sheet.setColumnWidth((short)31, (short)7000 );
            cell = row.createCell((short)(31));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Otras OC Asociadas");
            
            sheet.setColumnWidth((short)32, (short)7000 );
            cell = row.createCell((short)(32));
            cell.setCellStyle(estilo3);
            cell.setCellValue("D�as");
            
           /**************************************************/ 
            int Fila4 = 4;
            
            row4  = sheet4.createRow((short)(Fila4));
            
            sheet4.setColumnWidth((short)0, (short)5000 );
            cell = row4.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OC");
            
            /***************************************************/
            
            UtilFinanzas u = new UtilFinanzas();
            Iterator It = this.Lista.iterator();
            while(It.hasNext()) {
                
                ReporteOCVacioAnticipo rpv = (ReporteOCVacioAnticipo) It.next();
        
                Fila++;
                row  = sheet.createRow((short)(Fila));
                
                if(!(rpv.getDocument().equals("")&&rpv.getItem().equals(""))){
                    Fila4++;
                    row4  = sheet4.createRow((short)(Fila4));
                    cell = row4.createCell((short)(0));
                    cell.setCellStyle(estilo4);
                    cell.setCellValue((rpv.getNumpla()!=null)?rpv.getNumpla():"");
                }

                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rpv.getCiudad()!=null)?rpv.getCiudad():"");

                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rpv.getDespachador()!=null)?rpv.getDespachador():"");

                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rpv.getNumpla()!=null)?rpv.getNumpla():"");

                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rpv.getContaroc()!=null)?rpv.getContaroc():"");

                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rpv.getVlrpla2()!=null)?u.customFormat(rpv.getVlrpla2()):"");

                cell = row.createCell((short)(5));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rpv.getPlatlr()!=null)?rpv.getPlatlr():"");

                cell = row.createCell((short)(6));
                cell.setCellStyle(estilo5);
                cell.setCellValue(( rpv.getCreation_date() != null )?rpv.getCreation_date():"");

                if(rpv.getDif().equals("0") && rpv.getPrinter_date().equals("No Impresa")&& rpv.getCreation_date().equals("0099-01-01 00:00:00") ){
                    cell = row.createCell((short)(7));
                    cell.setCellStyle(estilo4);
                    cell.setCellValue("");                
                }
                else{
                    cell = row.createCell((short)(7));
                    cell.setCellStyle(estilo4);
                    cell.setCellValue(( rpv.getDif() != null )?rpv.getDif():"");
                }

                cell = row.createCell((short)(8));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getPrinter_date() != null )?rpv.getPrinter_date():"");

                cell = row.createCell((short)(9));
                cell.setCellStyle(estilo6);
                cell.setCellValue(( rpv.getFechaanticipo() != null )?rpv.getFechaanticipo():"");

                cell = row.createCell((short)(10));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getVlr() != null )?u.customFormat(rpv.getVlr()):"");

                cell = row.createCell((short)(11));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getDocument() != null )?rpv.getDocument():"");

                cell = row.createCell((short)(12));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getFecha_cheque() != null )?rpv.getFecha_cheque():"");

                cell = row.createCell((short)(13));
                cell.setCellStyle(estilo4);
                String rutaExc= ( rpv.getRuta_pla() != null )?rpv.getRuta_pla():"";
                String o = ( rpv.getOrigenoc() != null )?rpv.getOrigenoc():"";
                String d = ( rpv.getDestinooc() != null )?rpv.getDestinooc():"";
                cell.setCellValue(rutaExc + "--" + o + "-" + d);

                cell = row.createCell((short)(14));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getOrigenoc() != null )?rpv.getOrigenoc():"");

                cell = row.createCell((short)(15));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getDestinooc() != null )?rpv.getDestinooc():"");

                cell = row.createCell((short)(16));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getPuestocontrol() != null )?rpv.getPuestocontrol():"");

                cell = row.createCell((short)(17));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getPuestocontrol() != null )?u.customFormat(Integer.parseInt(rpv.getPuestocontrol())*30000):"");

                cell = row.createCell((short)(18));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getNumrem() != null )?rpv.getNumrem():"");

                cell = row.createCell((short)(19));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getVlrrem2() != null )?u.customFormat(rpv.getVlrrem2()):"");

                cell = row.createCell((short)(20));
                cell.setCellStyle(estilo6);

                String producto = ( rpv.getProducto() != null )?rpv.getProducto():"";
                cell.setCellValue( producto );

                cell = row.createCell((short)(21));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getNomCliente() != null )?rpv.getNomCliente():"");

                cell = row.createCell((short)(22));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getFacturacliente() != null )?rpv.getFacturacliente():"");

                cell = row.createCell((short)(23));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getAgenciaanticipo() != null )?rpv.getAgenciaanticipo():"");

                cell = row.createCell((short)(24));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getCedulaanticipo() != null )?rpv.getCedulaanticipo():"");

                cell = row.createCell((short)(25));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getNombreanticipo() != null )?rpv.getNombreanticipo():"");

                cell = row.createCell((short)(26));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getValorflete() != null )?u.customFormat(rpv.getValorflete()):"");

                cell = row.createCell((short)(27));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getFechacumplido() != null )?rpv.getFechacumplido():"");

                cell = row.createCell((short)(28));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getCedulacumplido() != null )?rpv.getCedulacumplido():"");

                cell = row.createCell((short)(29));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getNombrecumplido() != null )?rpv.getNombrecumplido():"");

                cell = row.createCell((short)(30));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getCodCliente() != null )?rpv.getCodCliente():"");

                cell = row.createCell((short)(31));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rpv.getOcasociadas() != null )?rpv.getOcasociadas():"");

                cell = row.createCell((short)(32));
                cell.setCellStyle(estilo4);
                               
		int dgen = 0;
                dgen = Integer.parseInt(rpv.getDias());
		 if(!rpv.getCreation_date().equals("0099-01-01 00:00:00")){
                    if(rpv.getDias().equals("1")){
                        cell.setCellValue("1 D�a");
                    }else if(dgen==2||dgen==3){
                        cell.setCellValue("2 y 3 D�as");
                    }else if (dgen>=4){
                        cell.setCellValue("4 y m�s d�as");
                    }
                 }else{
                      cell.setCellValue("");
                 }
                
            } 
            model.LogProcesosSvc.finallyProceso( "Reporte OC de Vacios con anticipo sin reporte", this.hashCode(), usuario, "PROCESO EXITOSO" );
            /************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream(Ruta);
            wb.write(fo);
            //System.out.println("paso");
            fo.close();
            
            
        }catch( Exception ex ){
            ex.printStackTrace();
            //Capturo errores finalizando proceso
            try{
                 model.LogProcesosSvc.finallyProceso( "Reporte OC de Vacios con anticipo sin reporte",this.hashCode(), usuario, "ERROR : " + ex.getMessage() );

            } catch( Exception f ){
                f.printStackTrace();
                try{
                     model.LogProcesosSvc.finallyProceso( "Reporte OC de Vacios con anticipo sin reporte",this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                } catch( Exception p ){ p.printStackTrace(); }
            }
            
        }finally{
            super.destroy();
        }
       
    }
    
}

/***************************************************************************
 Entregado a elavalle 12 Febrero 2007
 ***************************************************************************/
