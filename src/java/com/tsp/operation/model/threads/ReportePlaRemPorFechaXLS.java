/*****************************************************************************
 * Nombre clase :                   HExportarPresupuestoXLS.java             *
 * Descripcion :                    Hilo que permite la creacion del reporte *
 *                                  de cumplidos en un                       *
 *                                  archivo en excel                         *
 * Autor :                          Ing. Leonardo Parody                     *
 * Modificado :                     Ing. Juan Manuel Escandon Perez          *
 * Modificado Octubre 2006 :        LREALES                                  *
 * Fecha :                          21 de septiembre de 2005, 10:01 AM       *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 ****************************************************************************/
package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.ReporteCumplidoAction;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;


public class ReportePlaRemPorFechaXLS extends Thread{
    
    private String mensaje="";
    String FechaI;
    String FechaF;
    String Agencia;
    String user;
    String Fecha;
    String []LOV;
    String usuario;
    ReporteCumplido datos = new ReporteCumplido();
    Model model;
    
    public void start(String FechaI,String FechaF,String Agencia,String user,String Fecha, String []LOV, String usuario ){
        this.FechaI = FechaI;
        this.FechaF = FechaF;
        this.Agencia = Agencia;
        this.user = user;
        this.Fecha = Fecha;
        this.LOV = LOV;
        this.usuario = usuario;
        super.start();
    }
    
    public synchronized void run(){
        
        try{
            
            Model model = new Model();
            
            model.LogProcesosSvc.InsertProceso("Reporte de Cumplidos", this.hashCode(), "Reporte de Cumplidos", user);
            List reporte = model.cumplidoService.ReporteCumplido(FechaI, FechaF, Agencia, usuario );
            // if (reporte!=null && reporte.size()>0){
            
            this.setMensaje("Reporte de "+FechaI+" a "+FechaF+" Realizado");
            
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            
            model.DirectorioSvc.create( user );

            String NombreArchivos =  model.DirectorioSvc.getUrl() + user + "/ReporteCumplidos_"+FechaI+"-"+FechaF+"-"+Agencia+"-"+FechaFormated+".xls";
            POIWrite xls = new POIWrite(NombreArchivos, user, Fecha);
            
            HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLUE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            
            
            xls.obtenerHoja("BASE");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.adicionarCelda(0,0, "REPORTE CUMPLIDOS", header);
            xls.combinarCeldas(0, 0, 0, 2);
            
            xls.adicionarCelda(1,0, "Fecha Inicial :"   , fechatitle);
            xls.adicionarCelda(1,1, FechaI , fecha);
            xls.adicionarCelda(1,2, "Fecha Final :"   , fechatitle);
            xls.adicionarCelda(1,3, FechaF, fecha);
            xls.adicionarCelda(2,0, "Agencia Despacho :"   , fechatitle);
            xls.adicionarCelda(2,1, model.agenciaService.obtenerAgencia(Agencia).getNombre(), texto);
            xls.adicionarCelda(3,0, "Fecha y hora Proceso :"   , fechatitle);
            xls.adicionarCelda(3,1, FechaFormated1, fecha);
            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            /*Realiza el proceso de verificacion para solo mostrar las columnas seleccionadas por el usuario*/
            for( int i = 0; i < LOV.length; i++ ){
                
                if( LOV[i].equals("numpla") ){
                    xls.adicionarCelda(fila ,col++ , "Num. Planilla"     , titulo );
                }                
                if( LOV[i].equals("fecdesp") ){
                    xls.adicionarCelda(fila ,col++ , "Fech Despacho"     , titulo );
                }
                if( LOV[i].equals("origen") ){
                    xls.adicionarCelda(fila ,col++ , "Agc Planilla "     , titulo );
                }
                if( LOV[i].equals("destino") ){
                    xls.adicionarCelda(fila ,col++ , "Origen"     , titulo );
                }
                if( LOV[i].equals("agcdesppla") ){
                    xls.adicionarCelda(fila ,col++ , "Destino"      , titulo );
                }
                if( LOV[i].equals("feccumpla") ){
                    xls.adicionarCelda(fila ,col++ , "Fech Cump Pla"     , titulo );
                }
                if( LOV[i].equals("fecentpla") ){
                    xls.adicionarCelda(fila ,col++ , "Fecha Ent Trafico Pla"      , titulo );
                }
                if( LOV[i].equals("diffec") ){
                    xls.adicionarCelda(fila ,col++ , "Dif Cump  - Ent Pla"  , titulo );
                }
                if( LOV[i].equals("cantpla") ){
                    xls.adicionarCelda(fila ,col++ , "Cant Orig Pla"     , titulo );
                }
                if( LOV[i].equals("cantcumppla") ){
                    xls.adicionarCelda(fila ,col++ , "Cant Cump Pla"     , titulo );
                }
                if( LOV[i].equals("difcantpla") ){
                    xls.adicionarCelda(fila ,col++ , "Dif Cant Pla"      , titulo );
                }
                if( LOV[i].equals("agccumppla") ){
                    xls.adicionarCelda(fila ,col++ , "Agc Cump Pla"      , titulo );
                }
                if( LOV[i].equals("usucumppla") ){
                    xls.adicionarCelda(fila ,col++ , "Usu Cump Pla"      , titulo );
                }                
                if( LOV[i].equals("numdisp") ){
                    xls.adicionarCelda(fila ,col++ , "Num Discrepa"      , titulo );
                }
                if( LOV[i].equals("numrem") ){
                    xls.adicionarCelda(fila ,col++ , "Numero Remesa"     , titulo );
                }
                if( LOV[i].equals("fecrem") ){
                    xls.adicionarCelda(fila ,col++ , "Fecha Remesa"      , titulo );
                }
                if( LOV[i].equals("agcrem") ){
                    xls.adicionarCelda(fila ,col++ , "Agc Remesa"        , titulo);
                }
                if( LOV[i].equals("orirem") ){
                    xls.adicionarCelda(fila ,col++ , "Agc Ori Rem"       , titulo );
                }
                if( LOV[i].equals("agcdesprem") ){
                    xls.adicionarCelda(fila ,col++ , "Agc Desp Rem"      , titulo );
                }
                if( LOV[i].equals("cliente") ){
                    xls.adicionarCelda(fila ,col++ , "Cliente"      , titulo );
                }
                if( LOV[i].equals("feccumprem") ){
                    xls.adicionarCelda(fila ,col++ , "Fech Cump Rem"     , titulo );
                }
                if( LOV[i].equals("cantorirem") ){
                    xls.adicionarCelda(fila ,col++ , "Cant Ori Rem"      , titulo );
                }
                if( LOV[i].equals("cantcumprem") ){
                    xls.adicionarCelda(fila ,col++ , "Cant Cum Rem"      , titulo);
                }
                if( LOV[i].equals("difcantrem") ){
                    xls.adicionarCelda(fila ,col++ , "Dif Cant Rem"      , titulo );
                }
                if( LOV[i].equals("agccumprem") ){
                    xls.adicionarCelda(fila ,col++ , "Agc Cump Rem"      , titulo );
                }
                if( LOV[i].equals("usucumprem") ){
                    xls.adicionarCelda(fila ,col++ , "usu Cump Rem"      , titulo );
                }
                if( LOV[i].equals("docint") ){
                    xls.adicionarCelda(fila ,col++ , "Nro Doc Int"       , titulo );
                }
                if( LOV[i].equals("docintxcump") ){
                    xls.adicionarCelda(fila ,col++ , "Nro Doc Int x Cump"  , titulo );
                }
                
                // LREALES - 09 OCTUBRE 2006
                if( LOV[i].equals("tienedisc") ){
                    xls.adicionarCelda(fila ,col++ , "Tiene Discrepancia"     , titulo );
                }
                if( LOV[i].equals("placa") ){
                    xls.adicionarCelda(fila ,col++ , "Placa"     , titulo );
                }
                if( LOV[i].equals("agefac") ){
                    xls.adicionarCelda(fila ,col++ , "Agc Facturacion"     , titulo );
                }
                
            }
            
            Iterator it = reporte.iterator();
            while(it.hasNext()){
                fila++;
                col = 0;
                datos = (ReporteCumplido)it.next();
                /*Realiza el proceso de verificacion para solo mostrar las columnas seleccionadas por el usuario*/
                for( int i = 0; i < LOV.length; i++ ){                    
                    if( LOV[i].equals("numpla") ){
                        xls.adicionarCelda(fila ,col++ , datos.getPlanilla(), texto );
                    }                    
                    if( LOV[i].equals("fecdesp") ){
                        xls.adicionarCelda(fila ,col++ , datos.getFecha_despacho(), fecha );
                    }
                    if( LOV[i].equals("origen") ){
                        xls.adicionarCelda(fila ,col++ , datos.getAgc_planilla(), texto );
                    }
                    if( LOV[i].equals("destino") ){
                        xls.adicionarCelda(fila ,col++ , datos.getOrigen_planilla(), texto );
                    }
                    if( LOV[i].equals("agcdesppla") ){
                        xls.adicionarCelda(fila ,col++ , datos.getDestino_planilla(), texto );
                    }
                    if( LOV[i].equals("feccumpla") ){
                        xls.adicionarCelda(fila ,col++ , datos.getFec_cumplido_planilla(), texto );
                    }
                    if( LOV[i].equals("fecentpla") ){
                        xls.adicionarCelda(fila ,col++ , datos.getFecha_entrega(), texto );
                    }
                    if( LOV[i].equals("diffec") ){
                        xls.adicionarCelda(fila ,col++ , datos.getDiferencia_fecha(), texto );
                    }
                    if( LOV[i].equals("cantpla") ){
                        xls.adicionarCelda(fila ,col++ , datos.getCantidad_origen(), numero );
                    }
                    if( LOV[i].equals("cantcumppla") ){
                        xls.adicionarCelda(fila ,col++ , datos.getCantidad_cumplida_pla(), numero );
                    }
                    if( LOV[i].equals("difcantpla") ){
                        xls.adicionarCelda(fila ,col++ , datos.getDiferencia_cantidad(), numero );
                    }
                    if( LOV[i].equals("agccumppla") ){
                        xls.adicionarCelda(fila ,col++ , datos.getAgencia_cumplido(), texto );
                    }
                    if( LOV[i].equals("usucumppla") ){
                        xls.adicionarCelda(fila ,col++ , datos.getUsuario_cumplido_pla(), texto );
                    }   
                    
                    // LREALES - 09 OCTUBRE 2006
                    if( LOV[i].equals("numdisp") ){
                        xls.adicionarCelda(fila ,col++ , datos.getDiscrepancias(), texto );
                    }
                    
                    if( LOV[i].equals("numrem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getRemesa(), texto );
                    }
                    if( LOV[i].equals("fecrem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getFecha_remesa(), texto );
                    }
                    if( LOV[i].equals("agcrem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getAgencia_remesa(), texto );
                    }
                    if( LOV[i].equals("orirem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getOrigen_remesa(), texto );
                    }
                    if( LOV[i].equals("agcdesprem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getDestino_remesa(), texto );
                    }
                    if( LOV[i].equals("cliente") ){
                        xls.adicionarCelda(fila ,col++ , datos.getCliente(), texto );
                    }
                    if( LOV[i].equals("feccumprem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getFec_cumplido_remesa(), texto );
                    }
                    if( LOV[i].equals("cantorirem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getCantidad_remesa(), numero );
                    }
                    if( LOV[i].equals("cantcumprem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getCantidad_cumplida_rem(), numero );
                    }
                    if( LOV[i].equals("difcantrem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getDif_cantidad_remesa(), numero );
                    }
                    if( LOV[i].equals("agccumprem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getAgencia_cumplido_rem(), texto );
                    }
                    if( LOV[i].equals("usucumprem") ){
                        xls.adicionarCelda(fila ,col++ , datos.getUsuario_cumplido_rem(), texto );
                    }
                    if( LOV[i].equals("docint") ){
                        xls.adicionarCelda(fila ,col++ , datos.getDocumentos_cumplidos(), texto );
                    }
                    if( LOV[i].equals("docintxcump") ){
                        xls.adicionarCelda(fila ,col++ , datos.getDocumentosxcumplir(), texto );
                    }
                    
                    // LREALES - 09 OCTUBRE 2006
                    if( LOV[i].equals("tienedisc") ){
                        xls.adicionarCelda(fila ,col++ , datos.getTienedisc(), texto );
                    }
                    if( LOV[i].equals("placa") ){
                        xls.adicionarCelda(fila ,col++ , datos.getPlaca(), texto );
                    }
                    if( LOV[i].equals("agefac") ){
                        xls.adicionarCelda(fila ,col++ , datos.getAgefac(), texto );
                    }
                
                }
            }
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso("Reporte de Cumplidos", this.hashCode(), user, "PROCESO EXITOSO");
          
            
        }catch (Exception ex){
            ////System.out.println("Error : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso("Reporte de Cumplidos", this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso("Reporte de Cumplidos", this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    
    public void setMensaje(String Mensaje){
        
        this.mensaje = Mensaje;
        
    }
    public String getMensaje(){
        return mensaje;
    }
    
}