/*************************************************************
 * Nombre: ReporteInventarioDiscrepanciaCliente.java
 * Descripci�n: Hilo para crear el reporte de productos en inventario
 *              por discrepancia
 * Autor: Osvaldo P�rez
 * Fecha: 05-06-2006, 11:39 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  Osvaldo P�rez
 */
public class ReporteInventarioDiscrepanciaCliente extends Thread{
    
    private Vector datos;
    private String path;
    private String usuario;
    private String procesoName;
    private String des;
    private String cliente;
    private String ubicacion;
    private Date d;
    private Model model = new Model();
    
    public ReporteInventarioDiscrepanciaCliente() {
    }
    
    public void start(Vector datos, String u, String cliente, String ubicacion) {
        this.datos = datos;
        this.procesoName = "Reporte Inventario Discrepancia";
        this.des = "Productos de cliente en inventario por discrepancia";
        this.usuario = u;  
        this.cliente=cliente;
        this.ubicacion=ubicacion;
        this.d = new Date();
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            Util u = new Util();
            //
            
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            path = rb.getString("ruta");
                        
            File file = new File(path +"/exportar/migracion/"+usuario);
            file.mkdirs();
                        
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll("/", "-");
            fecha=fecha.replaceAll(":","_");
            
            String nombreArch= "InventarioDiscrepancia-" + fecha + ".xls";
            String       Hoja  = "InventarioDiscrepancia";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            ////System.out.println("RUTA: " + Ruta);
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for (int col=0; col<40 ; col++){
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 20 ) ) );                
            }
            sheet.setColumnWidth( (short)0, (short) ( ( 50 * 8 ) / ( (double) 1 / 10 ) ) );
            sheet.setColumnWidth( (short)2, (short) ( ( 50 * 8 ) / ( (double) 1 / 10 ) ) );
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.ORANGE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor((short)(0x1));
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setFillForegroundColor(HSSFColor.ORANGE.index);
            estiloX.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x1));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
             HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            /****************************************************/
            
            
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            //sheet.createFreezePane(0,5);
                        
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            
            for (int j=0;j<4;j++) {
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
            
            
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Art�culos en inventario por discrepancia");
            
             for(int i=2;i<6;i++){
                for (int j=0;j<4;j++) {
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);
                }                
            }
            
            //FECHA
            row = sheet.getRow((short)(2));  
            cell = row.createCell((short)(0));            
            cell = row.getCell((short)(0));  
            cell.setCellStyle(estilo6);
            cell.setCellValue("FECHA: ");
            cell = row.createCell((short)(1));            
            cell = row.getCell((short)(1));            
            cell.setCellStyle(estilo6);
            cell.setCellValue(Util.getFechaActual_String(7));
            
            //CLIENTE
            row = sheet.getRow((short)(3));  
            cell = row.createCell((short)(0));            
            cell = row.getCell((short)(0));  
            cell.setCellStyle(estilo6);
            cell.setCellValue("CLIENTE: ");
            cell = row.createCell((short)(1));            
            cell = row.getCell((short)(1));            
            cell.setCellStyle(estilo6);
            cell.setCellValue(cliente);
            
            
            //UBICACION
            row = sheet.getRow((short)(4));  
            cell = row.createCell((short)(0));            
            cell = row.getCell((short)(0));  
            cell.setCellStyle(estilo6);
            cell.setCellValue("UBICACION: ");
            cell = row.createCell((short)(1));            
            cell = row.getCell((short)(1));            
            cell.setCellStyle(estilo6);
            cell.setCellValue(ubicacion);
            
            /*************************************************************************************/
            
            /***** RECORRER LOS DATOS ******/
            int Fila = 6;
            
            row = sheet.createRow((short)6);
            row = sheet.getRow( (short)(6) );            
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CODIGO");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);            
            cell.setCellValue("DESCRIPCION");
            
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CANTIDAD");
            
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("NOTA DEBITO");
                                     
            
            for (int i=0; i<datos.size(); i++){
                
                Producto p=(Producto) datos.elementAt(i);
                
                Fila++;
                row  = sheet.createRow((short)(Fila));
                
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue(p.getCodigo());
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue(p.getDescripcion());
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo5);
                cell.setCellValue(p.getUnidad());
                
                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo4);
                cell.setCellValue(p.getNota_debito());
                
                
                
            
        }//end for datos
        
        /************************************************************************************/
        /**** GUARDAR DATOS EN EL ARCHIVO  ***/
        FileOutputStream fo = new FileOutputStream(Ruta);
        wb.write(fo);
        fo.close();
    }catch(Exception e){        
        ////System.out.println(e.getMessage());
        
    }finally{
        super.destroy();
    }
}

}



