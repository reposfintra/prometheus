/***************************************************************************
 * Nombre clase : ............... HReporteCitaCarguePlaca.java             *
 * Descripcion :................. Hilo que permite generar un reporte de   *
 *                                novedades y indicadores de cumplimiento  *
 *                                de citas de cargue                       *
 * Autor :....................... Ing. Karen Reales                        *
 * Fecha :........................ 16 de Febrero de 2006, 11:38   AM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/


package com.tsp.operation.model.threads;
import java.util.*;
import java.io.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import java.sql.*;
/**
 *
 * @author Mario Fontalvo
 */
public class HReporteCitaCargue extends Thread{
    private Vector reporte;
    private String fec1;
    private String fec2;
    private String user;
    Model model;
    /** Creates a new instance of ReporteRegistroTiempoXLS */
    public HReporteCitaCargue() {
    }
    public void start( String fec1,String fec2,String user) {
        this.fec1 = fec1;
        this.fec2= fec2;
        this.user = user;
        
        super.start();
    }
    
    public synchronized void run(){
        Vector cargue;
        Vector descargue;
        
        try{
            model= new Model();
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta") + "/exportar/migracion";
            
            String ruta = path + "/" + user + "/";
            cargue=new Vector();
            descargue=new Vector();
            CitaCargue ccargue = new CitaCargue();
            CitaCargue dcargue = new CitaCargue();
            ////System.out.println("Empezamos...");
            try{
                ////System.out.println("Buscamos los vectores...");
                //  model.LogProcesosSvc.InsertProceso("Cita cargue", this.hashCode(), "Reporte Cita Cargue", user);
                model.citacargueService.reporteCitaCargue(fec1, fec2);
                ////System.out.println("Termine buscando los vectores...");
                cargue = model.citacargueService.getCargue();
                descargue = model.citacargueService.getDescargue();
                //model.citacargueService.causasCitaCargue();
                
                //SE BUSCAN LAS CANTIDADES MANUALES Y AUTOMATICAS DEL CARGUE
                
                model.citacargueService.cantCargues(fec1, fec2, "C");
                if(model.citacargueService.getCita()!=null){
                    ccargue =model.citacargueService.getCita();
                }
                //SE BUSCAN LAS CANTIDADES MANUALES Y AUTOMATICAS DEL DESCARGUE
                
                model.citacargueService.cantCargues(fec1, fec2, "D");
                if(model.citacargueService.getCita()!=null){
                    dcargue =model.citacargueService.getCita();
                }
                

            }catch(SQLException e){
                ////System.out.println("Error llenando el informe");
                throw new SQLException("ESTE ES EL ERROR... " + e.getMessage() + " " + e.getErrorCode());
            }
            
            fec1 = fec1.substring(0,10).replaceAll("-","");
            fec2 = fec2.substring(0,10).replaceAll("-","");
            
            String nombreArch= "ReporteCitaCargue["+fec1+"]["+fec2+"].xls";
            String       Hoja  = "CITAS CARGUE";
            
            String       Ruta  =ruta+ nombreArch;
            // Definicion de Estilos para la hoja de excel
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( Ruta);
            
            HSSFCellStyle fecha   = xls.nuevoEstilo("Verdana", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle texto   = xls.nuevoEstilo("Verdana", 10 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle texto3  = xls.nuevoEstilo("Verdana", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle texto2  = xls.nuevoEstilo("Verdana", 9 ,  true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle total   = xls.nuevoEstilo("Verdana", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
            HSSFCellStyle titulo2 = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.WHITE.index, HSSFColor.BLUE.index, HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero  = xls.nuevoEstilo("Verdana", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle titulo  = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);
            
            xls.obtenerHoja("ReporteCitasCargue");
            
            int fila = 1;
            int col  = 0;
            xls.combinarCeldas(fila, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO"             , texto  );
            xls.combinarCeldas(2, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "INDICADORES DE CUMPLIMIENTO DE CITAS DE CARGUE"             , texto  );
            xls.combinarCeldas(3, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "FECHA INICIO: "+fec1+" FECHA FIN:"+fec2             , texto  );
            
            xls.cambiarAnchoColumna(0, 10000);
            xls.cambiarAnchoColumna(1, 10000);
            xls.cambiarAnchoColumna(2, 10000);
            
            fila++;
            xls.adicionarCelda(fila,0 , "TOTAL CITAS CARGUE REALIZADAS" , titulo2 );
            xls.adicionarCelda(fila,1 , "TOTAL CITAS CARGUE AUTOMATICAS", titulo2 );
            
            fila++;
            xls.adicionarCelda(fila ,0, ccargue.getCantCargueM(), texto3);
            xls.adicionarCelda(fila ,1, ccargue.getCantCargueA(), texto3);
            fila++;
            xls.adicionarCelda(fila,0 , "PORCENTAJE CARGUES REALIZADOS" , texto );
            xls.adicionarCelda(fila,1 , "PORCENTAJE CARGUES AUTOMATICOS", texto );
            fila++;
            xls.adicionarCelda(fila ,0, ccargue.getPorcCargueM()+"%", texto3);
            xls.adicionarCelda(fila ,1, ccargue.getPorcCargueA()+"%", texto3);
            
            
            fila+=3;
            xls.adicionarCelda(fila,0, "TOTAL CITAS DESCARGUE REALIZADAS" , titulo2 );
            xls.adicionarCelda(fila,1, "TOTAL CITAS DESCARGUE AUTOMATICAS", titulo2 );
            fila++;
            xls.adicionarCelda(fila ,0, dcargue.getCantCargueM(), texto3);
            xls.adicionarCelda(fila ,1, dcargue.getCantCargueA(), texto3);
            fila++;
            xls.adicionarCelda(fila,0 , "PORCENTAJE DESCARGUES REALIZADOS" , texto );
            xls.adicionarCelda(fila,1 , "PORCENTAJE DESCARGUES AUTOMATICOS", texto );
            fila++;
            xls.adicionarCelda(fila ,0, dcargue.getPorcCargueM()+"%", texto3);
            xls.adicionarCelda(fila ,1, dcargue.getPorcCargueA()+"%", texto3);
            
            
            
            fila+=2;
            xls.adicionarCelda(fila,0 , "CUMPLIMIENTO CITA CARGUE" , titulo2 );
            xls.adicionarCelda(fila,1 , "CUMPLIMIENTO CITA DESCARGUE", titulo2 );
            
            
            
            for(int i =0; i<cargue.size();i++){
                CitaCargue c = (CitaCargue) cargue.elementAt(i);
                fila++;
                col++;
                xls.adicionarCelda(fila ,0, c.getCumplido_cargue()+"%", texto3);
                xls.adicionarCelda(fila ,1, c.getCumplido_dcargue()+"%", texto3);
                
            }
            
            
            fila = fila + 2;
            xls.adicionarCelda(fila++ ,0   , "NOVEDADES CITA CARGUE"             , texto  );
            fila = fila++;
            xls.adicionarCelda(fila   ,0 , "CAUSA" , titulo2 );
            xls.adicionarCelda(fila   ,1 , "PARTICIPACION CARGUE", titulo2 );
            
            //CAUSAS
            Vector causas=new Vector();
            try{
                ////System.out.println("Buscamos los vectores DE CAUSAS ...");
                model.citacargueService.causasCitaCargue("C", fec1, fec2);
                ////System.out.println("Termine buscando los vectores DE CAUSAS...");
                causas = model.citacargueService.getCausas();
                
            }catch(SQLException e){
                ////System.out.println("Error llenando el informe");
                throw new SQLException("ESTE ES EL ERROR... " + e.getMessage() + " " + e.getErrorCode());
            }
            
            for(int i = 0;i<causas.size();i++){
                
                fila++;
                col = 0;
                
                TblGeneral tg = (TblGeneral) causas.elementAt(i);
                
                xls.adicionarCelda(fila ,col++ , tg.getDescripcion(), texto3);
                xls.adicionarCelda(fila ,col++ , tg.getPorcentaje()+"%", texto3);
            }
            
            fila = fila + 2;
            xls.adicionarCelda(fila++ ,0   , "NOVEDADES CITA DESCARGUE"             , texto  );
            fila = fila++;
            xls.adicionarCelda(fila   ,0 , "CAUSA" , titulo2 );
            xls.adicionarCelda(fila   ,1 , "PARTICIPACION DESCARGUE", titulo2 );
            //CAUSAS DESCARGUE
            causas=new Vector();
            try{
                ////System.out.println("Buscamos los vectores DE CAUSAS ...");
                model.citacargueService.causasCitaCargue("D", fec1, fec2);
                ////System.out.println("Termine buscando los vectores DE CAUSAS...");
                causas = model.citacargueService.getCausas();
                
            }catch(SQLException e){
                ////System.out.println("Error llenando el informe");
                throw new SQLException("ESTE ES EL ERROR... " + e.getMessage() + " " + e.getErrorCode());
            }
            
            for(int i = 0;i<causas.size();i++){
                
                fila++;
                col = 0;
                
                TblGeneral tg = (TblGeneral) causas.elementAt(i);
                
                xls.adicionarCelda(fila ,col++ , tg.getDescripcion(), texto3);
                xls.adicionarCelda(fila ,col++ , tg.getPorcentaje()+"%", texto3);
            }
            
            Vector placas = new Vector();
            try{
                ////System.out.println("Buscamos los vectores DE PLACAS...");
                model.citacargueService.reporteCitaCarguePlaca(fec1, fec2);
                ////System.out.println("Termine buscando los vectores DE PLACAS...");
                placas = model.citacargueService.getPlacas();
                
            }catch(SQLException e){
                ////System.out.println("Error llenando el informe");
                throw new SQLException("ESTE ES EL ERROR... " + e.getMessage() + " " + e.getErrorCode());
            }
            xls.obtenerHoja("ReporteCitasCargue Placa");
            
            fila = 1;
            col  = 0;
            xls.combinarCeldas(fila, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO"             , texto  );
            xls.combinarCeldas(2, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "INDICADORES DE INCUMPLIMIENTO DE CITAS DE CARGUE POR PLACA"             , texto  );
            xls.combinarCeldas(3, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "FECHA INICIO: "+fec1+" FECHA FIN:"+fec2             , texto  );
            xls.cambiarAnchoColumna(0, 6000);
            xls.cambiarAnchoColumna(1, 10000);
            xls.cambiarAnchoColumna(2, 10000);
            xls.cambiarAnchoColumna(3, 10000);
            xls.cambiarAnchoColumna(4, 10000);
            xls.cambiarAnchoColumna(5, 10000);
            fila++;
            xls.adicionarCelda(fila,0 , "PLACA" , titulo2 );
            xls.adicionarCelda(fila,1 , "PROPIETARIO" , titulo2 );
            xls.adicionarCelda(fila,2 , "PORC. INCUMPLIMIENTO CARGUE", titulo2 );
            xls.adicionarCelda(fila,3 , "CANT. CARGUE INCUMPLIDOS", titulo2 );
            xls.adicionarCelda(fila,4 , "PORC. INCUMPLIMIENTO DESCARGUE", titulo2 );
            xls.adicionarCelda(fila,5 , "CANT. DESCARGUE INCUMPLIDOS", titulo2 );
            for(int i = 0;i<placas.size();i++){
                
                fila++;
                col = 0;
                
                CitaCargue c = (CitaCargue) placas.elementAt(i);
                
                xls.adicionarCelda(fila ,col++ ,c.getPlaca(), texto3);
                xls.adicionarCelda(fila ,col++ ,c.getPropietario(), texto3);
                xls.adicionarCelda(fila ,col++ ,c.getCumplido_cargue()+"%", texto3);
                xls.adicionarCelda(fila ,col++ ,c.getInCargue(), texto3);
                xls.adicionarCelda(fila ,col++ ,c.getCumplido_dcargue()+"%", texto3);
                xls.adicionarCelda(fila ,col++ ,c.getInDCargue(), texto3);
            }
            
            xls.cerrarLibro();
        }catch(Exception e){
            ////System.out.println("Hay un error: "+e.getMessage());
            /*try{
                model.LogProcesosSvc.finallyProceso("Cita Cargue", this.hashCode(),user,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso("Cita Cargue",this.hashCode(),user,"ERROR :");
                }catch(Exception p){    }
            }*/
        }
    }
    
    public static void main(String a [])throws SQLException{
        HReporteCitaCargue hilo = new HReporteCitaCargue();
        hilo.start("2006-01-01 00:00","2006-02-28 00:00","KREALES");
    }
}
