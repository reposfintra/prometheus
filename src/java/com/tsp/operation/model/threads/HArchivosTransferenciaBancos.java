   /***************************************
    * Nombre Clase ............. HArchivosTransferenciaBancos.java
    * Descripci�n  .. . . . . .  Permite Generar los archivos para transferencia al banco
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  05/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.LogProcesosService.*;
import java.lang.*;
import java.io.*;


import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;
import java.text.*;
import java.util.Date;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Rectangle;



import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;
import com.lowagie.text.Font;
import javax.servlet.*;


import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
//import com.sun.xml.internal.xsom.impl.scd.Iterators;
import com.tsp.finanzas.contab.model.DAO.ComprobantesDAO;
import com.tsp.operation.encrypted.PGPFileProcessor;
import java.nio.channels.FileChannel;



public class HArchivosTransferenciaBancos  extends Thread {

      private  boolean estadoProceso = true;
      private  String planillasConReanticipos = "";
      String  url_logo="fintrapdf.gif";


      Calendar c = new GregorianCalendar();
      String dia = Integer.toString(c.get(Calendar.DATE));
      String mes = Integer.toString(c.get(Calendar.MONTH));
      String annio = Integer.toString(c.get(Calendar.YEAR));


// Archivo de Transferencia MSF265 para migrar a MIMS:
    private   FileOutputStream fw265;
    private  BufferedWriter    bf265;
    private  PrintWriter       linea265;
    private  List          Popietario_secuencias;
    private  File f;


 // Archivo de Transferencia
    private   FileOutputStream fw;
    private  BufferedWriter    bf;
    private  PrintWriter       linea;


    private  String URL_TEM_TRANS     = "";
    private  String URL_TEM_MSF265    = "";



    private  String        procesoName;
    private  Model         model;
    private  Usuario       usuario ;
    private  List          listaTransferencia;
    private  List          listaAgrupada;

    private  String[]      anticipos;


    private String   AAMMDD                = "yyMMdd";
    private String   AAAAMMDD              = "yyyyMMdd";
    private String   SPACE                 = " ";
    private String   CERO                  = "0";
    private String   DERECHA               = "R";
    private String   IZQUIERDA             = "L";
    private String   CTA_AHORR0            = "CA";
    private String   CTA_CORRIENTE         = "CC";
    private String   CTA_EFECTIVO          = "EF";
    private String   BANCOLOMBIA_CREDIPAGO = "CPAG";

    private int      TOTALDETALLE          = 0;
    private double   TOTALDEBITO           = 0;
    private double   TOTALCREDITO          = 0;

    private String   NIT_PROVEEDOR         = "";
    private String   NOMBRE_PROVEEDOR      = "";
    private String   CTA_PROVEEDOR         = "";
    private String   TIPO_CTA_PROVEEDOR    = "";
    private String   BANCO_PROVEEDOR       = "";
    private String   DESC_BANCO_PROVEEDOR  = "";


    private String   TRANSFERENCIA         = "";
    private String   BANCOLOMBIA_CPAG_TIPO_TRANSACTION= "320";



// CONFIGURACION DE BANCOS:

    // 1.  Bancolombia:
           private String   BANCOLOMBIA                   = "07";
           private String   TABLA_BANCOLOMBIA_BANCO       = "BANCOLOMBI";   // BANCOS ESTIPULADOS POR BANCOLOMBIA
           private String   TABLA_BANCOLOMBIA_CIUDAD      = "CIUBANCOLO";   // CODIGO DE CIUDAD ESTIPULADOS POR BANCOLOMBIA

           private String   BANCOLOMBIA_TIPO_REGISTRO     = "1";
           private String   BANCOLOMBIA_TIPO_REGISTRO_DET = "6";
           private String   BANCOLOMBIA_TIPO_TRANSACTION  = "220";
           private String   BANCOLOMBIA_PROPOSITO         = "PAGFINTRA";
           private String   BANCOLOMBIA_CTA_AHORR0        = "37";
           private String   BANCOLOMBIA_CTA_CORRIENTE     = "27";
           private String   BANCOLOMBIA_CTA_EFECTIVO      = "26";



    // 2. Occidente
          private String   OCCIDENTE                      = "23";
          private String   OCCIDENTE_TIPO_REGISTRO        = "1";
          private String   OCCIDENTE_TIPO_REGISTRO_DET    = "2";
          private String   OCCIDENTE_TIPO_REGISTRO_FIN    = "3";

          private String   OCCIDENTE_CTA_AHORR0           = "A";
          private String   OCCIDENTE_CTA_CORRIENTE        = "C";
          private String   OCCIDENTE_CTA_EFECTIVO         = SPACE;
          private String   OCCIDENTE_CONCEPTO             = "PAGOS FINTRA";
          private String   TABLA_OCCIDENTE_BANCO          = "BOCCIDENTE";


          private String   DDMMAAAA              = "ddMMyyyy";
          private String   CUPO_ROTATIVO         = "CROT";


    // 3. Avvillas
          private String   AVVILLAS                   = "52";
          private String   TABLA_AVVILLAS_BANCO       = "BOCCIDENTE";   // Son los mismos codigos de bancos para occidente
          private String   AVVILLAS_TIPO_REGISTRO_HEAD= "1";
          private String   AVVILLAS_TIPO_REGISTRO_DET = "2";
          private String   AVVILLAS_TIPO_REGISTRO_FIN = "4";
          private String   AVVILLAS_TIPO_REGISTROS    = "PPD";
          private String   AVVILLAS_TIPO_TRANSACTION  = "PP";   //Pago a proveedores
          private String   AVVILLAS_CANAL             = "4";    //internet
          private String   AVVILLAS_NIT               = "03";
          private String   AVVILLAS_CEDULA            = "01";
          private String   PLAZA_BARRANQUILLA         = "0004"; //Codigo para la ciudad de barranquilla
          //tipos de cuentas
          private String   AVVILLAS_CTA_CORRIENTE     = "0";
          private String   AVVILLAS_CTA_AHORROS       = "1";
          private String   AVVILLAS_CUPO_ROTATIVO     = "2";
          //Codigos de transaccion
          private String   AVVILLAS_CREDITO_A_AH      = "32";
          private String   AVVILLAS_CREDITO_A_CC      = "22";

    // 4. Colpatria
          private String   COLPATRIA                      = "19";
          private String   COLPATRIA_TIPO_REGISTRO_HEAD   = "01";
          private String   COLPATRIA_TIPO_REGISTRO_DET    = "02";
          private String   COLPATRIA_TIPO_REGISTRO_FIN    = "03";
          //private String   COLPATRIA_CTA_AHORR0S          = "0";
          private String   COLPATRIA_CTA_AHORR0S          = "2";
          private String   COLPATRIA_CTA_CORRIENTE        = "1";
          private String   COLPATRIA_CTA_EFECTIVO         = SPACE;
          private String   COLPATRIA_TIPO_TRANSACCION     = "02";
          private String   COLPATRIA_CONCEPTO             = "PAGOFINTRA";
          private String   TABLA_COLPATRIA_BANCO          = "BANCOLOMBI";

          private   String  CUENTA_ANTICIPO     = "2205050406";
          private   String  CUENTA_LIQUIDACION  = "2205050404";
          
          
          // 5.  Bancolombia formato PAB
           private String   BANCOLOMBIAPAB                   = "7B";
           private String   BANCOLOMBIAPAB_TIPO_APLICACION   = "I"; // I=inmediata

          //6. Archivo encriptado configuracion.
            private static final String PASSPHRASE ="fintra123456";
            private static String E_INPUT = "";
            private static String E_OUTPUT = "";
            private static String E_KEY_FILE = "";
            private static String NAME_FILE="";
            private String tipoOperacion="";













    public HArchivosTransferenciaBancos() {
    }





    public void init(){
            TOTALDETALLE          = 0;
            TOTALDEBITO           = 0;
            TOTALCREDITO          = 0;

            NOMBRE_PROVEEDOR      = "";
            NIT_PROVEEDOR         = "";
            CTA_PROVEEDOR         = "";
            TIPO_CTA_PROVEEDOR    = "";
            BANCO_PROVEEDOR       = "";
            DESC_BANCO_PROVEEDOR  = "";

            URL_TEM_TRANS         = "";
            URL_TEM_MSF265        = "";

            TRANSFERENCIA         = "";

    }





    /**
     * M�todo inicializa  el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void start(Model modelo, Usuario user,String[] anticipos, List lista, String nit, String name, String banco,String desbanco, String cta, String tCta) throws Exception{
         try{
                init();
                this.model                 = modelo;
                this.usuario               = user;
                this.listaTransferencia    = lista;
                this.anticipos             = anticipos;
                this.listaAgrupada         = new LinkedList();

                this.NIT_PROVEEDOR         = nit;
                this.NOMBRE_PROVEEDOR      = name;
                this.BANCO_PROVEEDOR       = banco;
                this.DESC_BANCO_PROVEEDOR  = desbanco;
                this.CTA_PROVEEDOR         = cta;
                this.TIPO_CTA_PROVEEDOR    = tCta;

                this.procesoName = "TRANSFERENCIA BANCO " + banco;

            super.start();

        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
     /**
     * Metodo que  permite ejecutar el SQL
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */





     /**
     * Metodo que  determina si hay rutina para el banco seleccionado
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public String validarFormatosRealizados(String codebanco)
    {
          String msj = "No hay formato definido(rutina) para el banco " + codebanco;
         if( codebanco.equals(  BANCOLOMBIA ) )
             msj="";
         if( codebanco.equals(  BANCOLOMBIAPAB ) )
             msj="";
         if( codebanco.equals(  OCCIDENTE   ) )
             msj="";
         if( codebanco.equals(  AVVILLAS    ) )
             msj="";
         if( codebanco.equals(  COLPATRIA   ) )
             msj="";
         if( codebanco.equals(  "CG"   ) )  msj="";

         if( codebanco.equals(  "FC"   ) )  msj="";
         if( codebanco.equals(  "FD"   ) )  msj="";
         if( codebanco.equals(  "CT"   ) )  msj="";
        return msj;
    }






     /**
     * Metodo que  escribe cabecera archivo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void writeHeader(String codeBanco)throws Exception{
        try{


            int sec = 1;


         // BANCOLOMBIA
            if(codeBanco.equals(BANCOLOMBIA)){
                     String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO                                           +  // 1.1  Tipo de Registro
                                     rellenar(this.NIT_PROVEEDOR,               CERO,  10, IZQUIERDA )   +  // 1.2  Nit entidad que envia
                                     rellenar(this.NOMBRE_PROVEEDOR,            SPACE, 16, DERECHA   )   +  // 1.3  Nombre entidad que envia
                                     BANCOLOMBIA_TIPO_TRANSACTION                                        +  // 1.4  Clase de transaccion, segun formato es 220
                                     rellenar(this.BANCOLOMBIA_PROPOSITO ,      SPACE, 10, DERECHA   )   +  // 1.5  Proposito descripci�n
                                     this.getFecha(AAMMDD)                                               +  // 1.6  Fecha transacci�n  AAMMDD
                                     this.convertLetra(sec)                                              +  // 1.7  Secuencia del archivo del dia en letra A.B.C....
                                     this.getFecha(AAMMDD)                                               +  // 1.8  Fecha aplicaci�n  AAMMDD
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  6 , IZQUIERDA )   +  // 1.9  N�mero de registros de detalle
                                     rellenar(String.valueOf((int)TOTALDEBITO ),CERO,  12, IZQUIERDA )   +  // 1.10 Sumatoria debito
                                     rellenar(String.valueOf((int)TOTALCREDITO),CERO,  12, IZQUIERDA )   +  // 1.10 Sumatoria debito
                                     rellenar(this.CTA_PROVEEDOR,               CERO,  11, IZQUIERDA )   +  // 1.11 Cta Cliente
                                     equivalenciaTipoCta( this.BANCO_PROVEEDOR, this.TIPO_CTA_PROVEEDOR );  // 1.12 Tipo Cta  S : aho  /  D : cte

                    linea.println( dato );
            }



         // OCCIDENTE
            if(codeBanco.equals( OCCIDENTE )){
                String dato =
                                     OCCIDENTE_TIPO_REGISTRO                                           +  // 1.1 Tipo de Registro
                                     rellenar( CERO,                            CERO,  4,  DERECHA   ) +  // 1.2 Consecutivo
                                     this.getFecha(AAAAMMDD)                                           +  // 1.3 Fecha del archivo, formato YYYYMMDD
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  4 , IZQUIERDA ) +  // 1.4 N�mero de registros de detalle
                                     rellenar(String.valueOf((int)TOTALCREDITO) + "00" , CERO,  18, IZQUIERDA ) +  // 1.5 Valor total de pago
                                     rellenar(this.CTA_PROVEEDOR,               CERO,  16, IZQUIERDA ) +  // 1.6 Cta Cliente
                                     rellenar(String.valueOf(0),              CERO,   6, IZQUIERDA ) +  // 1.7 Secuencia
                                     rellenar( CERO,                            CERO, 142, DERECHA   ) ;  // 1.8 Ceros

                linea.println( dato );
            }
            if (codeBanco.equals(AVVILLAS)) {
                //Falta colocar si se rellena a izq o derecha
                String dato =
                        AVVILLAS_TIPO_REGISTRO_HEAD +
                        rellenar(this.CTA_PROVEEDOR, SPACE, 17, DERECHA) + // Cuenta origen
                        equivalenciaTipoCta(this.BANCO_PROVEEDOR, this.TIPO_CTA_PROVEEDOR) + // Tipo Cuenta origen
                        AVVILLAS_TIPO_TRANSACTION + //pago proveedores
                        this.getFecha(AAAAMMDD) +
                        rellenar(this.NIT_PROVEEDOR+"1", CERO, 15, IZQUIERDA) + // Nit entidad que envia
                        AVVILLAS_NIT +
                        rellenar(this.NOMBRE_PROVEEDOR, SPACE, 16, DERECHA) +
                        PLAZA_BARRANQUILLA +
                        this.AVVILLAS_TIPO_REGISTROS +
                        rellenar(String.valueOf(0), CERO, 6, IZQUIERDA) +
                        this.AVVILLAS_CANAL;

                linea.println(dato);
            }

            // COLPATRIA
            if (codeBanco.equals(COLPATRIA)) {
                String dato =
                        //rellenar("1", CERO, 5, DERECHA) + // Consecutivo
                        rellenar("1", CERO, 5, IZQUIERDA) + // Consecutivo
                        COLPATRIA_TIPO_REGISTRO_HEAD + // Tipo de Registro
                        this.getFecha(DDMMAAAA) + // Fecha del archivo
                        rellenar(this.NIT_PROVEEDOR+"1", CERO, 11, IZQUIERDA) + // Nit entidad que envia
                        rellenar(COLPATRIA_CONCEPTO, SPACE, 10, DERECHA) + // Clave: codigo de identificacion de la empresa
                        rellenar(String.valueOf(TOTALDETALLE+2), CERO, 6, IZQUIERDA) + // Numero de registros de detalle
                        "0196" + // OFICINA DE PAGO
                        rellenar(this.CTA_PROVEEDOR, CERO, 12, IZQUIERDA) + // Cuenta origen
                        rellenar(SPACE, SPACE, 142, DERECHA);

                linea.println(dato);
            }
            
            
            
            
             // BANCOLOMBIA FORMATO PAB
            if(codeBanco.equals(BANCOLOMBIAPAB)){
                     String secuenciaPAB = model.AnticiposPagosTercerosSvc.obtenerSecuenciaBanco(BANCOLOMBIAPAB, usuario.getLogin());
                     String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO                                           +  // 1.1  Tipo de Registro
                                     rellenar(this.NIT_PROVEEDOR,               CERO,  15, IZQUIERDA )   +  // 1.2  Nit entidad que envia
                                     BANCOLOMBIAPAB_TIPO_APLICACION                                      +  // 1.3  Tipo de aplicacion
                                     rellenar(SPACE,SPACE, 15, DERECHA   )                               +  // 1.4  Filler                             
                                     BANCOLOMBIA_TIPO_TRANSACTION                                        +  // 1.5  Clase de transaccion, segun formato es 220 pago proveedores
                                     rellenar(this.BANCOLOMBIA_PROPOSITO ,      SPACE, 10, DERECHA   )   +  // 1.6  Proposito descripci�n
                                     this.getFecha(AAAAMMDD)                                             +  // 1.7  Fecha transacci�n  AAAAMMDD                             
                                     rellenar(secuenciaPAB,                     SPACE, 2, DERECHA    )   +  // 1.8  Secuencia del archivo del dia en letra A.B.C....
                                     this.getFecha(AAAAMMDD)                                             +  // 1.9  Fecha aplicaci�n  AAAAMMDD
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  6 , IZQUIERDA )   +  // 1.10  N�mero de registros de detalle
                                     rellenar(String.valueOf((int)TOTALDEBITO ) + "00",CERO,  17, IZQUIERDA )+  // 1.11 Sumatoria debito
                                     rellenar(String.valueOf((int)TOTALCREDITO) + "00",CERO,  17, IZQUIERDA )+  // 1.12 Sumatoria credito
                                     rellenar(this.CTA_PROVEEDOR,               CERO,  11, IZQUIERDA )   +  // 1.13 Cta Cliente
                                     equivalenciaTipoCta( BANCOLOMBIA         , this.TIPO_CTA_PROVEEDOR )+  // 1.14 Tipo Cta  S : aho  /  D : cte
                                     rellenar(SPACE,SPACE, 149, DERECHA);                                   // 1.15  Filler
                             

                    linea.println( dato );
            }


        }catch(Exception e){
            throw new Exception(" writeHeader "+e.getMessage());
        }
    }






     /**
     * Metodo que  ecribe en el archivo de transferencia a banaco
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void writeTransfer(String codeBanco, AnticiposTerceros  trans)throws Exception{
        try{

         // BANCOLOMBIA
            if( codeBanco.equals(BANCOLOMBIA ) )    bancolombia( trans );

        // OCCIDENTE
            if( codeBanco.equals( OCCIDENTE ) )    occidente( trans );

        // AVVILLAS
            if( codeBanco.equals( AVVILLAS ) )    avvillas( trans );

        // COLPATRIA
            if( codeBanco.equals( COLPATRIA ) )    colpatria( trans );
            
            // BANCOLOMBIA PAB
            if( codeBanco.equals(BANCOLOMBIAPAB ) )    bancolombiaPAB( trans );

        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }




    /**
     * Metodo que  ecribe bloque final
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void writeFinal(String codeBanco)throws Exception{
        try{

         // OCCIDENTE
            if(codeBanco.equals( OCCIDENTE )){
                String dato =
                                     OCCIDENTE_TIPO_REGISTRO_FIN                                         +  // 1.1 Tipo de Registro final
                                     "9999"                                                              +  // 1.2 Secuencia
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  4 , IZQUIERDA )   +  // 1.3 N�mero de pagos
                                     rellenar(String.valueOf((int)TOTALCREDITO) +"00",CERO,  18, IZQUIERDA )   +  // 1.4 Valor total de pago
                                     rellenar( CERO,                            CERO, 172, DERECHA   )   ;  // 1.5 Ceros
                linea.println( dato );
            }
        // AVVILLAS
            if(codeBanco.equals( AVVILLAS )){
                String dato =
                                     AVVILLAS_TIPO_REGISTRO_FIN                                          +  // Tipo de Registro final
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  8 , IZQUIERDA )   +  // Número de pagos
                                     rellenar(String.valueOf((int)TOTALCREDITO) +"00",CERO,  18, IZQUIERDA );//Valor total de pago
                linea.println( dato );
            }

        // COLPATRIA
            if(codeBanco.equals( COLPATRIA )){
                String dato =
                                     rellenar(String.valueOf(TOTALDETALLE+2),     CERO,  5 , IZQUIERDA ) +  // Secuencia
                                     COLPATRIA_TIPO_REGISTRO_FIN                                         +  // Tipo de Registro final
                                     rellenar(String.valueOf(TOTALDETALLE+2),     CERO,  6 , IZQUIERDA ) +  // Número de pagos
                                     rellenar( SPACE,                            SPACE, 187, DERECHA   );
                linea.println( dato );
            }



        } catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }






    /**
     * Metodo que  ecribe en formato BANCOLOMBIA
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void bancolombia(AnticiposTerceros  transf)throws Exception{
           try{


         // 2. DETALLE DE TRANSACCION:

               String tipoCta          = transf.getTipo_cuenta();


            // Codigo del  banco del beneficiario
               String codigoBanco     =  CERO;
               Hashtable  infoBanco   =  model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_BANCOLOMBIA_BANCO, transf.getBanco() );
               if(infoBanco==null)
                     throw new Exception ( "No hay informaci�n del banco  " + transf.getBanco() + " en formato bancolombia");
               else
                    codigoBanco  =   (String)infoBanco.get("descripcion");


            // Indicador de pago: sucursal
               String indicador_pago   = "S";

            // Tipo de transacion:
               String tipo_transaction = equivalenciaTipoTransaction( BANCOLOMBIA, tipoCta );

            // Valor
               int    valor            = (int)transf.getVlrConsignar();

               String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO_DET                                      +  // 1.1  Tipo de Registro detalle
                                     rellenar( transf.getNit_cuenta(),          CERO,  15, IZQUIERDA  ) +  // 1.2  Nit beneficiario
                                     rellenar( transf.getNombre_cuenta(),       SPACE, 18, DERECHA    ) +  // 1.3  Nombre del beneficiario
                                     rellenar( codigoBanco,                     CERO,   9, IZQUIERDA  ) +  // 1.4  Codigo banco
                                     rellenar( transf.getCuenta(),              CERO,  17, IZQUIERDA  ) +  // 1.5  N�mero de la Cta
                                     indicador_pago                                                     +  // 1.6  Lugar de pago
                                     tipo_transaction                                                   +  // 1.7  Tipo de Transaction
                                     rellenar( String.valueOf(valor),           CERO,  10, IZQUIERDA  ) +  // 1.8  Valor
                                     rellenar( this.BANCOLOMBIA_PROPOSITO ,     SPACE,  9, DERECHA    ) +  // 1.9  Proposito descripci�n
                                     rellenar( transf.getFactura_mims(),        SPACE, 12, DERECHA    ) +  // 1.10 referencia
                                     SPACE                                                              ;  // 1.11 Espacio en blanco

              linea.println( dato );


           } catch(Exception e){
             throw new Exception( " bancolombia " + e.getMessage());
          }
    }




    /**
     * Metodo que  ecribe en formato OCCIDENTE
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void occidente(AnticiposTerceros  transf)throws Exception{
           try{


         // 2. DETALLE DE TRANSACCION:

               String tipoCta          = transf.getTipo_cuenta();

            // N�mero de la cuenta, si es efectivo 0, de lo contrario la cuenta del beneficiario
               String cuenta           = ( tipoCta.equals( CTA_EFECTIVO ))? CERO : transf.getCuenta() ;


            // Codigo del banco  -- Definir codigos
               String codigoBanco      = CERO;
               Hashtable  infoBanco    =  model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_OCCIDENTE_BANCO, transf.getBanco() );
               if(infoBanco==null)
                     throw new Exception ( "No hay informaci�n del banco  " + transf.getBanco() + " en formato occidente");
               else
                    codigoBanco  =   (String)infoBanco.get("descripcion");


           //  Forma de pago:
               String formaPago        = ( tipoCta.equals( CTA_EFECTIVO ) )? "4" : ( ( transf.getBanco().equals("BANCO OCCIDENTE"))?"2":"3"  );

           //  Tipo de transacion:
               String tipo_transaction = equivalenciaTipoTransaction( OCCIDENTE, tipoCta );

           //  Valor
               int  valor   = (int) transf.getVlrConsignar();


           // Comprobante -- Definir
              String comprobante       = transf.getNit_cuenta();//(transf.getReanticipo().equals("N") )?CUENTA_ANTICIPO : CUENTA_LIQUIDACION ; // CUENTA CONTABLE


               String dato =    OCCIDENTE_TIPO_REGISTRO_DET                                                  +  // 1.1   Tipo Registro detalle
                                rellenar( String.valueOf(transf.getSecuencia()),     CERO,   4, IZQUIERDA  ) +  // 1.2   Consecutivo del registro
                                rellenar( this.CTA_PROVEEDOR,                        CERO,  16, IZQUIERDA  ) +  // 1.3   Cta Beneficiario
                                rellenar( transf.getNombre_cuenta(),                 SPACE, 30, DERECHA    ) +  // 1.4   Nombre del beneficiario
                                rellenar( transf.getNit_cuenta(),                    CERO,  11, IZQUIERDA  ) +  // 1.5   Nit beneficiario
                                rellenar( codigoBanco,                               CERO,   4, IZQUIERDA  ) +  // 1.6   Codigo banco beneficiario
                                this.getFecha(AAAAMMDD)                                                      +  // 1.7   Fecha a realizar el pago
                                formaPago                                                                    +  // 1.8   Forma de pago
                                rellenar( String.valueOf(valor) + "00",              CERO,  15, IZQUIERDA  ) +  // 1.9   Valor
                                rellenar( cuenta,                                    SPACE, 16, DERECHA    ) +  // 1.10  N�mero de la Cta
                                rellenar( comprobante,                               SPACE, 12, IZQUIERDA  ) +  // 1.11  Comprobante
                                tipo_transaction                                                             +  // 1.12  Tipo de transacction
                                rellenar( OCCIDENTE_CONCEPTO + transf.getFactura_mims(), SPACE,  80, DERECHA   ) ;  // 1.13  Concepto de pago


              linea.println( dato );


           } catch(Exception e){
             throw new Exception( " occidente " + e.getMessage());
          }
    }













     /**
     * Metodo que  devuelve la equivalencia tipo de transacci�n de acuerdo al banco
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public String equivalenciaTipoTransaction(String codeBanco, String tipo){
        String equivale = tipo;

        // BANCOLOMBIA
            if(codeBanco.equals(BANCOLOMBIA)){
               if( tipo.equals( CTA_AHORR0     ))  equivale = BANCOLOMBIA_CTA_AHORR0;
               if((tipo.equals( CTA_CORRIENTE  )) || (tipo.equals( BANCOLOMBIA_CREDIPAGO)) )  equivale = BANCOLOMBIA_CTA_CORRIENTE;
               if( tipo.equals( CTA_EFECTIVO   ))  equivale = BANCOLOMBIA_CTA_EFECTIVO;
            }


        // OCCIDENTE
            if(codeBanco.equals( OCCIDENTE )){
               if( tipo.equals( CTA_AHORR0     ))  equivale = OCCIDENTE_CTA_AHORR0;
               if( tipo.equals( CTA_CORRIENTE  ))  equivale = OCCIDENTE_CTA_CORRIENTE;
               if( tipo.equals( CTA_EFECTIVO   ))  equivale = OCCIDENTE_CTA_EFECTIVO;
            }

        // AVVILLAS
            if(codeBanco.equals(AVVILLAS)){
               if( tipo.equals( CTA_AHORR0     ))  equivale = AVVILLAS_CREDITO_A_AH;
               if( tipo.equals( CTA_CORRIENTE  ))  equivale = AVVILLAS_CREDITO_A_CC;
               if( tipo.equals( CTA_EFECTIVO   ))  equivale = AVVILLAS_CREDITO_A_CC;
            }

        return equivale;
    }


    /**
     * Metodo que  devuelve la equivalencia tipo de cuenta de acuerdo al banco
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public String equivalenciaTipoCta(String codeBanco, String tipo){
        String equivale = tipo;

        // BANCOLOMBIA
            if(codeBanco.equals(BANCOLOMBIA)){
               if( tipo.equals(CTA_AHORR0))     equivale = "S";
               if( tipo.equals(CTA_CORRIENTE) || tipo.equals(BANCOLOMBIA_CREDIPAGO))  equivale = "D";
            }

        // AVVILLAS
            if(codeBanco.equals(AVVILLAS)){
               if( tipo.equals(CTA_AHORR0))     equivale = AVVILLAS_CTA_AHORROS;
               if( tipo.equals(CTA_CORRIENTE))  equivale = AVVILLAS_CTA_CORRIENTE;
               if( tipo.equals(CUPO_ROTATIVO))  equivale = AVVILLAS_CUPO_ROTATIVO;
            }

        // COLPATRIA
            if(codeBanco.equals(COLPATRIA)){
               if( tipo.equals(CTA_AHORR0))     equivale = COLPATRIA_CTA_AHORR0S;
               if( tipo.equals(CTA_CORRIENTE))  equivale = COLPATRIA_CTA_CORRIENTE;
            }


        return equivale;
    }




    /**
     * Metodo para sacar la equivalencia de un caracter decimal
     * en un caracter de notacion
     * @autor mfontalvo
     */
    public static String convertLetra(int numero){
        return  String.valueOf(  (char) (numero + 65) );
    }



     /**
    * M�todo que rellena los campos
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/
    public  String rellenar(String cadena, String caracter, int tope, String posicion)throws Exception{
       try{
           int lon = cadena.length();
           if(tope>lon){
             for(int i=lon;i<tope;i++){
                 if(posicion.equals( DERECHA))    cadena += caracter;
                 else                                 cadena  = caracter + cadena;
             }
           }
           else
               cadena = Trunc(cadena, tope );

       }catch(Exception e){
           throw new Exception(e.getMessage());
       }
       return cadena;
    }



   /**
    * M�todo que trunca cadena
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/
   public static String Trunc(String Cadena, int Longitud){
       return (Cadena==null?Cadena: (Cadena.length()>=Longitud? Cadena.substring(0,Longitud):Cadena  )  );
   }





     /**
     * Metodo que crea el archivo
     * modificado:. egonzalez
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void createFile() throws Exception {
        try {

            String nameFile = getNombreArchivo(this.DESC_BANCO_PROVEEDOR.replace(" ", "_"));
             model.DirectorioSvc.create(this.usuario.getLogin());
            System.out.println(" crear archivo " + this.usuario.getLogin() + "/" + nameFile);
            String ruta = model.DirectorioSvc.getUrl() + this.usuario.getLogin() + "/" + nameFile;

            URL_TEM_TRANS = ruta;
            //Configuracion de variables 
            NAME_FILE=nameFile;
            E_INPUT = ruta;
            E_OUTPUT = ruta + ".pgp";
            E_KEY_FILE = model.DirectorioSvc.getRuta() + "/pgp/pubring.pkr";

            this.fw = new FileOutputStream(ruta);
            this.bf = new BufferedWriter(new OutputStreamWriter(fw, "ISO-8859-1"));
            this.linea = new PrintWriter(this.bf);

        } catch (Exception e) {
            throw new Exception(e.getMessage());
         }
     }




     /**
     * Metodo que guarda el archivo de transferencia
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void saveTransfer(){
        this.linea.close();
    }




    /**
     * Metodo que crea el archivo para migrar a mims MSF265
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
     public void createFile265()throws Exception{
         try{

             String hora         =  Util.getFechaActual_String(6).replaceAll("/|:","").replaceAll(" ","_");
             String ruta265      =  model.DirectorioSvc.getUrl() + this.usuario.getLogin()  +"/MSF265_"+  hora   +".txt";

             URL_TEM_MSF265      = ruta265;

             this.fw265          = new FileOutputStream   ( ruta265     );
             this.bf265          = new BufferedWriter( new OutputStreamWriter(fw265,"ISO-8859-1" ) );
             this.linea265       = new PrintWriter   ( this.bf265  );

         }catch(Exception e){
             throw new Exception( e.getMessage() );
         }
     }



     /**
     * Metodo que guarda el archivo para migrar a mims
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void save265(){
        this.linea265.close();
    }





    /**
     * Metodo que borra los archivos en caso de error
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
     public void deleteFiles()throws Exception{
         try{

             File  fT =  new File(URL_TEM_TRANS);   fT.delete();
             File  fM =  new File(URL_TEM_MSF265);  fM.delete();

         }catch(Exception e){
             throw new Exception( e.getMessage() );
         }
     }




    /**
    * M�todo que permite crear nombre del archivo
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/
    public String getNombreArchivo(String desBanco)throws Exception{
        String name    ="";
        try{
            String hora    =  Util.getFechaActual_String(6).replaceAll("/|:","").replaceAll(" ","_");
                   name    =  desBanco +"_"+ hora + ".txt";
        }catch(Exception e){
           throw new Exception("getNombreArchivo " + e.getMessage());
        }
        return name;
    }




     /**
    * M�todo que permite dar la fecha actual dependiendo el formato
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/
    public String getFecha(String formato)throws Exception{
       String Fecha = "";
       try{

            SimpleDateFormat FMT = null;
            FMT = new SimpleDateFormat(formato);
            Fecha = FMT.format(new Date());

       }catch(Exception e){
           throw new Exception("getFecha " + e.getMessage());
       }
       return Fecha.toUpperCase() ;
    }



    /**
     * Metodo que  ecribe al archivo MSF265 para MIMS
     * @autor: ....... Fernel Villacob
     * @Modificado ... Julio Barros Rueda
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void write265(AnticiposTerceros  trans)throws Exception{
        try{


             String  GRABADOR           = "79458875";        //  Pablo Rosales
             String  CUENTA             = (trans.getReanticipo().equals("N") )?CUENTA_ANTICIPO : CUENTA_LIQUIDACION ;   // CUENTA CONTABLE REGISTRO


             String  FACTURA            =  trans.getFactura_mims();

             String  FECHA              =  Util.getFechaActual_String(5) +"/"+  Util.getFechaActual_String(3) +"/"+ Util.getFechaActual_String(1).substring(2,4);
             String  COMA               = ",";

             double  valor              = (int) Math.round( trans.getVlr());

             String  banco              = "";
             String  sucursal           = "";
             String  mims               = "";
             double  vlr                = 0;


          // PROPIETARIO:
          /*   Hashtable bancoPro      =  model.AnticiposPagosTercerosSvc.getBancoNIT( trans.getPla_owner() );
             if( bancoPro!= null  ){
                 banco     = (String) bancoPro.get("banco");
                 sucursal  = (String) bancoPro.get("sucursal");
                 mims      = (String) bancoPro.get("mims");
             }
             vlr   =  valor * -1 ;

            String  datoPropietario   = rellenar(banco      ,    SPACE,  15, DERECHA )           + COMA +
                                        rellenar(sucursal   ,    SPACE,  15, DERECHA )           + COMA +
                                        GRABADOR              + COMA +
                                        mims                  + COMA +
                                        FACTURA               + COMA +
                                        FECHA                 + COMA +
                                        FECHA                 + COMA +
                                        String.valueOf( vlr ) + COMA +
                                        SPACE                 + COMA +
                                        DESCRIPCION           + COMA +
                                        String.valueOf( vlr ) + COMA +
                                        SPACE                 + COMA +
                                        GRABADOR              + COMA +
                                        SPACE                 + COMA +
                                        CUENTA
                                        ;*/

        //  TERCERO:

             banco     = "";
             sucursal  = "";
             mims      = "";
             vlr       = 0;
             Hashtable bancoTer      =  model.AnticiposPagosTercerosSvc.getBancoNIT( trans.getProveedor_anticipo()  );
             if( bancoTer!= null  ){
                 banco     = (String) bancoTer.get("banco");
                 sucursal  = (String) bancoTer.get("sucursal");
                 mims      = (String) bancoTer.get("mims");
             }
             vlr   =  valor;

             String  DESCRIPCION        = "PP Fintra Liq " + trans.getFactura_mims()+" Por $"+valor;

             String SPACE2 = "";

             String  datoProveedor   =
                                        banco.trim()                  + COMA +
                                        sucursal.trim()               + COMA +
                                        GRABADOR.trim()               + COMA +
                                        mims.trim()                   + COMA +
                                        FACTURA.trim()                + COMA +
                                        FECHA.trim()                  + COMA +
                                        FECHA.trim()                  + COMA +
                                        String.valueOf( vlr ).trim()  + COMA +
                                        SPACE2.trim()                 + COMA +
                                        DESCRIPCION.trim()            + COMA +
                                        String.valueOf( vlr ).trim()  + COMA +
                                        SPACE2.trim()                 + COMA +
                                        GRABADOR.trim()               + COMA +
                                        SPACE2.trim()                 + COMA +
                                        CUENTA.trim()
                                        ;

           //  linea265.println( datoPropietario );
             linea265.println( datoProveedor   );


        } catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }


     /**
     * Metodo que  permite ejecutar el SQL
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
      public void ejecutar(String sql)throws Exception{
        try{
            TransaccionService  svc =  new  TransaccionService(this.usuario.getBd());
            svc.crearStatement();
            svc.getSt().addBatch(sql);
            svc.execute();

        }catch(Exception e){

            System.out.println("SQL: " + sql );

            throw new Exception( e.getMessage() );
        }
    }


     /**
     * M�todo que ejecuta el proceso de  generaci�n del archivo
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public synchronized void run(){
       try{
            String comentario="EXITOSO";
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(), this.procesoName , this.usuario.getLogin() );
            String control = validarFormatosRealizados(this.BANCO_PROVEEDOR);
            //buscamos los permisos de usuarios para generar los pagos...
            String tipoTrasfrencia=model.AnticiposPagosTercerosSvc.buscarPermisoTransferencia(usuario);
       
           if (control.equals("")) {
               if (tipoTrasfrencia.equalsIgnoreCase("TRANSFERENCIA")) {
                   tipoOperacion=tipoTrasfrencia;
                   listaAgrupada = model.AnticiposPagosTercerosSvc.getApruparTransferencias(anticipos);
               }
               if (tipoTrasfrencia.equalsIgnoreCase("PRONTOPAGO")) {
                   tipoOperacion=tipoTrasfrencia;
                   listaAgrupada = model.AnticiposPagosTercerosSvc.getAprupar(anticipos);

               }
               if (tipoTrasfrencia.equalsIgnoreCase("TODOS")) {
               }

                 
                  if(  listaAgrupada.size()>0 ){
                           // Buscamos la transferencia:
                              TRANSFERENCIA = model.AnticiposPagosTercerosSvc.getSerie();
                              if( !TRANSFERENCIA.equals("") ){
                                       // Definimos factura
                                          this.TOTALDETALLE  = listaAgrupada.size();
                                          for(int i=0;i<listaAgrupada.size();i++){
                                                AnticiposTerceros  trans  = (AnticiposTerceros) listaAgrupada.get(i);
                                                trans.setSecuencia      (i+1);
                                                trans.setFactura_mims   (  TRANSFERENCIA +"_"+ trans.getSecuencia()  );
                                                trans.setTransferencia  ( this.TRANSFERENCIA       );
                                               this.TOTALCREDITO += trans.getVlrConsignar() - validarMontoRepetidoCabecera(trans.getCuenta(),trans.getNit_cuenta(), trans.getVlrConsignar());
                                          }
                                         try{
                                          // ARCHIVOS:
                                          // Escribimos en el archivo para la transferencia:
                                              createFile();
                                             obtenerTipoTransaccion(BANCO_PROVEEDOR, TIPO_CTA_PROVEEDOR);
                                              writeHeader(this.BANCO_PROVEEDOR);
                                              for(int i=0;i<listaAgrupada.size();i++){
                                                    AnticiposTerceros  trans  = (AnticiposTerceros) listaAgrupada.get(i);
                                                    writeTransfer(this.BANCO_PROVEEDOR, trans);
                                              }
                                              writeFinal(this.BANCO_PROVEEDOR);
                                              saveTransfer();
                                           // Archivo para migrar a mims  MSF265:
                                               createFile265();
                                               for(int i=0;i<listaAgrupada.size();i++)
                                               {
                                                    AnticiposTerceros  trans  = (AnticiposTerceros) listaAgrupada.get(i);
                                                    write265( trans );


                                                    /***********creacion de carta pdf*************/


                                               }
                                               save265();
                                            // BASE DE DATOS:
                                            // Seteamos datos cuenta y facturas:
                                               for(int i=0;i<listaAgrupada.size();i++){
                                                     AnticiposTerceros  grupo  = (AnticiposTerceros) listaAgrupada.get(i);
                                                     for(int j=0;j<listaTransferencia.size();j++){
                                                            AnticiposTerceros  trans  = (AnticiposTerceros) listaTransferencia.get(j);
                                                            trans.setBanco_transferencia  ( this.BANCO_PROVEEDOR     );
                                                            trans.setCuenta_transferencia ( this.CTA_PROVEEDOR       );
                                                            trans.setTcta_transferencia   ( this.TIPO_CTA_PROVEEDOR  );
                                                            trans.setTransferencia        ( this.TRANSFERENCIA       );
                                                            if(
                                                                  grupo.getProveedor_anticipo().equals( trans.getProveedor_anticipo()  )
                                                              &&  grupo.getBanco().equals             ( trans.getBanco()               )
                                                              /*&&  grupo.getNombre_cuenta().equals     ( trans.getNombre_cuenta()     )*/
                                                              &&  grupo.getCuenta().equals            ( trans.getCuenta()              )
                                                              &&  grupo.getTipo_cuenta().equals       ( trans.getTipo_cuenta()         )
                                                              &&  grupo.getNit_cuenta().equals        ( trans.getNit_cuenta()          )
                                                              &&  grupo.getPla_owner().equals         ( trans.getPla_owner()           )
                                                            )
                                                               trans.setFactura_mims( grupo.getFactura_mims() );



                                                      }

                                               }
                                               //System.out.println("Inicio del Hilo ******************************");
                                                GenerarChequesAnticipos(anticipos);
                                               //28-11-2006
                                              //System.out.println("Codigo de Mario -----------------------------------------------------------xxxx");
                                              String[] sql = model.AnticiposPagosTercerosSvc.transferir(listaTransferencia, this.usuario.getLogin() ).split(";");
                                              for (int i = 0; i < sql.length; i++) {
                                                  ejecutar(sql[i]);
                                               }
                                            
                                              ////////////////////////////////// 03-12-2006 jbarros
                                              String ids = "";
                                              for(int i=0;i<anticipos.length;i++){
                                                  if(!ids.equals(""))  ids +=",";
                                                  ids += "'" + anticipos[i] + "'";
                                              }
                                              Popietario_secuencias = model.AnticiposPagosTercerosSvc.Listar_NitPro_Secuencia(ids);
                                              System.out.println(" para armar el exel");
                                              for(int i=0;i<Popietario_secuencias.size();i++){
                                                  //System.out.println("Entro Correos ---------------------->");
                                                  Hashtable  Rep = (Hashtable) Popietario_secuencias.get(i);
                                                  System.out.println("Secuencia  ("+Rep.get("Secuencia")+")");
                                                  if( !(""+Rep.get("Secuencia")).equals("") ){
                                                      //EnviarEmailProveedor(""+Rep.get("Documento"),""+Rep.get("Secuencia"));
                                                      System.out.println("Entro  -->");
                                                      model.ExtractoPPSvc.updateExtracto(""+Rep.get("Secuencia"));
                                                  }
                                              }
                                              
                                             //bloque para encriptar el archivo plano para bancolombia por el momento.
                                             if (DESC_BANCO_PROVEEDOR.equals("BANCOLOMBIA")) {
                                                 EncriptarArchivo();
                                             }

                                              
                                              
                                              //System.out.println("termino   ------------------------------>");
                             }catch(Exception e){
                                deleteFiles();
                                throw new Exception( e.getMessage() );
                             }


                                          /*********************carta pdf bancos y excel*************************/
                                          /*Jpinedo 2011-06-15*/
                                          if((this.BANCO_PROVEEDOR.equals("FC")||(this.BANCO_PROVEEDOR.equals("FD"))))
                                          {
                                          this.exportarCartaPdf(usuario.getLogin(), listaAgrupada);
                                          this.exportarExcel(listaAgrupada, usuario.getLogin());
                                          }
                                             System.out.println("/*********************Enviar SMS transferidos*************************/");
                                           /*********************Enviar SMS transferidos*************************/
                                        listaAgrupada =model.AnticiposPagosTercerosSvc.getAnticipos(anticipos);
                                         this.Enviar_SMS(listaAgrupada);



                       }
                       else
                          comentario = "No hay serie de transferencia...";
                  }
                  else
                      comentario = "No se agruparon los registros o no tiene permisos de transferencia...";
            }
            else
                comentario = control;
            System.out.println("pasa a escribir en el log de prosesos");
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);
       }catch(Exception e){
           try{
               model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage());
           }catch(Exception f){ }
       }
    }


 // Julio Barros 15-11-2006
    /**
     * Metodo que  Envia un email al proveedor del nit dado con el detalle de la secuencia
     * @autor: ....... Julio Ernesto Barros Rueda
     * @throws ....... Exception
     * @version ...... 1.0
     */
    private void EnviarEmailProveedor(String nit,String secuencia) throws Exception{
        String retorno = "";
        try {
            retorno = model.proveedorService.obtenerEMailPorNit(nit);
            System.out.println("retorno   "+retorno);
            if (!retorno.trim().equals("")){
                ////////////////////////////////////////////////////////////////
                model.ExtractoPPSvc.buscarExtractoDetallePP(secuencia);
                Vector datos = model.ExtractoPPSvc.getExtractoDetallePPS();
                if (datos.size()>0){
                    Extracto p = (Extracto) datos.get(0);
                    String nitt  =""+p.getNit();
                    //String fechap=""+p.getFecha();
                    String fechap=""+p.getCreation_date();
                    System.out.println(" creation date del mail     ----   >> "+fechap);
                    model.ExtractoPPSvc.buscarExtractoDetallePP(nitt,fechap,1);
                    model.ExtractoPPSvc.buscarExtractoDetallePP(nitt,fechap,2);
                    model.ExtractoPPSvc.buscarExtractoDetallePP(nitt,fechap,3);
                    Vector  liquidaciones      = model.ExtractoPPSvc.getExtractoDetallePP1();
                    Vector  ajustesPropietario = model.ExtractoPPSvc.getExtractoDetallePP2();
                    Vector  ajustesPlaca       = model.ExtractoPPSvc.getExtractoDetallePP0();

                    System.out.println(" vec1 "+liquidaciones.size()+"   vec2 "+ajustesPropietario.size()+"  vec3 "+ajustesPlaca.size());
                    ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                    String path = rb.getString("ruta") + "/exportar/migracion";
                    String ruta = path + "/" + usuario.getLogin();
                    f = new File(ruta);
                    if ( !f.exists() ){
                        f.mkdirs();
                    }
                    String lafecha = Util.getFechaActual_String(6);
                    lafecha = lafecha.replaceAll("/", "").replaceAll(":","").replaceAll(" ","_");
                    com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( ruta + "/Mail"+nit+"-"+secuencia+".xls");
                    System.out.println("ruta  "+ruta + "/Mail"+nit+"-"+secuencia+".xls");
                    // Definicion de Estilos para la hoja de excel
                    //HSSFCellStyle fecha   = xls.nuevoEstilo("Arial", 8  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
                    HSSFCellStyle texto   = xls.nuevoEstilo("Arial", 13 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
                    //HSSFCellStyle texto3  = xls.nuevoEstilo("Arial", 8  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                    HSSFCellStyle texto2  = xls.nuevoEstilo("Arial", 9 , false    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                    HSSFCellStyle total   = xls.nuevoEstilo("Arial", 9 , false    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
                    HSSFCellStyle titulo2 = xls.nuevoEstilo("Arial", 11 , true    , false, "text"       , HSSFColor.WHITE.index , HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER);
                    //HSSFCellStyle numero  = xls.nuevoEstilo("Arial", 8  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                    HSSFCellStyle titulo  = xls.nuevoEstilo("Arial", 8  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);
                    HSSFCellStyle numero  = xls.nuevoEstilo("Arial", 9 , false , false, "#,##0.00"     , xls.NONE , xls.NONE  , HSSFCellStyle.ALIGN_RIGHT);
                    xls.obtenerHoja("Extracto");
                    int fila = 1;
                    int col  = 1;
                    xls.adicionarCelda(fila++ ,col+2  , "           EXTRACTO FINTRA" , texto );
                    String  msjEstado          =  "";
                    double totalPlanillas      = 0;
                    double totalDescuentoPro   = 0;
                    double totalImpuestosPro   = 0;
                    double totalDescuentoPla   = 0;
                    double totalImpuestosPla   = 0;
                    int   planillasConEerrores = 0;
                    int   cent                 = 0;
                    String    Dcoument         = "";
                    float     valorF           = 0;
                    float totalsaldo           = 0;
                    if (liquidaciones!=null && !liquidaciones.isEmpty()){
                         ExtractoDetalle liquidacion1 = (ExtractoDetalle) liquidaciones.get(0);
                         Dcoument = liquidacion1.getDocumento();
                    }else if(ajustesPlaca!=null && !ajustesPlaca.isEmpty()){
                         ExtractoDetalle ajustesPlaca1 = (ExtractoDetalle) liquidaciones.get(0);
                         Dcoument = ajustesPlaca1.getDocumento();
                    }else if(ajustesPropietario!=null && !ajustesPropietario.isEmpty()){
                         ExtractoDetalle ajustesPropietario1 = (ExtractoDetalle) liquidaciones.get(0);
                         Dcoument = ajustesPropietario1.getDocumento();
                    }
                    if (liquidaciones!=null && !liquidaciones.isEmpty()){
                        System.out.println("liquidaciones   "+liquidaciones.size());
                       for (int i = 0; i<liquidaciones.size(); i++){
                          ExtractoDetalle liquidacion = (ExtractoDetalle) liquidaciones.get(i);
                          cent=0;
                              model.ExtractoPPSvc.buscarExtractoPP(liquidacion.getNit(),liquidacion.getFecha());
                              Extracto     Ext            = model.ExtractoPPSvc.getExtractoPPU();
                             if (i==0) {
                                    valorF = model.ExtractoPPSvc.retornar_vlr_pp_e(Ext.getFecha()) ;
                                    xls.combinarCeldas(fila, col, fila, col+8);
                                    xls.adicionarCelda(fila++ ,col   , "Liqudacion   "+liquidacion.getSecuencia() , titulo2 );
                                    fila++;
                                    xls.combinarCeldas(fila, col, fila, col+1);
                                    xls.adicionarCelda(fila++ ,col   , "Propietario   "+Ext.getNit() , total );
                                    xls.adicionarCelda(fila   ,col+4 , "Nombre        "+Ext.getNombre_trans(), total );
                                    xls.cambiarAnchoColumna(col, 5000);
                                    xls.combinarCeldas(fila, col, fila, col+1);
                                    xls.adicionarCelda(fila--   ,col   , "Sucursal      "+Ext.getSucursal(), total );
                                    xls.adicionarCelda(fila++   ,col+4 , "Banco         "+Ext.getBanco()  , total );
                                    xls.cambiarAnchoColumna(col, 5000);
                                    fila++;
                             }//para La cabecera
                             ///////////////////////////////////////////////
                             fila++;
                             xls.combinarCeldas(fila, col, fila, col+8);
                             xls.adicionarCelda(fila ,col   ,"", titulo2 );
                             ///////////////////////////////////////////////
                             fila++;
                             String temp01="";
                             String temp02="";
                             xls.combinarCeldas(fila, col, fila, col+1);
                             xls.adicionarCelda(fila ,col   , "OC         "+liquidacion.getDocumento() , total );
                             xls.adicionarCelda(fila ,col+3 , "Placa      "+liquidacion.getPlaveh() , total );
                             if( !liquidacion.getUnit_vlr().equals (" ") ){
                                 temp01=liquidacion.getUnit_vlr();
                             }
                             if( !liquidacion.getPesoreal().equals (" ") ){
                                 temp02=""+Utility.customFormat( Float.parseFloat( liquidacion.getPesoreal() ) );
                             }
                             xls.adicionarCelda(fila   ,col+7 , temp01+"        "+temp02 , total );
                             fila++;
                             //xls.combinarCeldas(fila, col, fila, col+1);
                             xls.adicionarCelda(fila ,col   , "Cliente    "+liquidacion.getNomc() , total );
                             fila++;
                             xls.adicionarCelda(fila ,col   , "Ruta       "+liquidacion.getDespla(), total );
                             temp01="";
                             if(!liquidacion.getStapla().equals("C")&&!liquidacion.getStapla().equals("A")){
                                 temp01="NO CUMPLIDA";
                             }else if(liquidacion.getStapla().equals("C")){
                                 temp01="CUMPLIDA";
                             }
                             xls.adicionarCelda(fila ,col+7 , "           "+temp01 , total );
                             fila++;
                             xls.adicionarCelda(fila ,col   , "F.Despacho "+liquidacion.getFecdsp(), total );
                             fila++;
                             xls.adicionarCelda(fila ,col   , "Remesa     "+liquidacion.getNumrem(), total );
                             fila++;
                             xls.adicionarCelda(fila ,col   , "Factura    "+liquidacion.getDocumento(), total );
                             double tv = 0;
                             double trfte = 0;
                             double trica = 0;
                             fila++;
                             //Titulo del detalle
                             xls.adicionarCelda(fila ,col   , "Item       ", total );
                             xls.adicionarCelda(fila ,col+1 , "Descripcion", total );
                             xls.adicionarCelda(fila ,col+4 , "Valor      ", total );
                             xls.adicionarCelda(fila ,col+6 , "Retefuente ", total );
                             xls.adicionarCelda(fila ,col+8, "Reteica    ", total );
                             xls.cambiarAnchoColumna(col, 5000);
                             while ( cent == 0) {
                                 fila++;
                                 String Comp="";
                                 liquidacion = (ExtractoDetalle) liquidaciones.get(i);
                                 //Columnas del detalle
                                 xls.adicionarCelda(fila ,col   , ""+liquidacion.getConcepto(), total );
                                 xls.adicionarCelda(fila ,col+1 , ""+liquidacion.getDescripcion(), total );
                                 xls.adicionarCelda(fila ,col+4 , ""+Utility.customFormat(liquidacion.getVlr() ), numero );
                                 xls.adicionarCelda(fila ,col+6 , ""+Utility.customFormat(liquidacion.getRetefuente() ), numero );
                                 xls.adicionarCelda(fila ,col+8, ""+Utility.customFormat(liquidacion.getReteica()  ), numero );
                                 xls.cambiarAnchoColumna(col, 5000);
                                 tv    += liquidacion.getVlr() ;
                                 trfte += liquidacion.getRetefuente();
                                 trica += liquidacion.getReteica();
                                 if ( (i+1) < liquidaciones.size()){
                                        Comp = ((ExtractoDetalle) liquidaciones.get(i+1)).getDocumento();
                                 }else{
                                        Comp="";
                                 }
                                 if ( liquidacion.getDocumento().equals( Comp) ) {
                                        i++;
			         }else{//fin if
				    cent++;
                                    fila++;
                                    //Totales de la factura
                                    xls.adicionarCelda(fila ,col   , "TOTAL FACTURAS", total );
                                    xls.adicionarCelda(fila ,col+4 , ""+Utility.customFormat(tv), numero );
                                    xls.adicionarCelda(fila ,col+6 , ""+Utility.customFormat(trfte), numero );
                                    xls.adicionarCelda(fila ,col+8, ""+Utility.customFormat(trica), numero );
                                    fila++;
                                    xls.adicionarCelda(fila ,col   , "TOTAL A PAGAR", total );
                                    xls.adicionarCelda(fila ,col+8 , ""+Utility.customFormat( tv+trfte+trica ), numero );
                                    xls.cambiarAnchoColumna(col, 5000);
                                    totalPlanillas += tv+trfte+trica;
                                  }//fin else
                               }//fin while del op
                       }
                        fila++;
                    }//end liquidaciones
                    if (ajustesPlaca!=null && !ajustesPlaca.isEmpty()){
                            fila++;
                            xls.combinarCeldas(fila, col, fila, col+8);
                            xls.adicionarCelda(fila++ ,col   , "Facturas Placas " , titulo2 );
                            fila++;
                            //Titulo del detalle
                            xls.adicionarCelda(fila ,col   , "Placa      ", total );
                            xls.adicionarCelda(fila ,col+1 , "Factura    ", total );
                            xls.adicionarCelda(fila ,col+4 , "Item       ", total );
                            xls.adicionarCelda(fila ,col+5 , "Descripcion", total );
                            xls.adicionarCelda(fila ,col+7 , "Valor", total );
                            xls.adicionarCelda(fila ,col+8 , "Impuestos  ", total );
                            xls.cambiarAnchoColumna(col, 5000);
                            for (int j = 0; j<ajustesPlaca.size(); j++){
                                ExtractoDetalle descuento = (ExtractoDetalle) ajustesPlaca.get(j);
                                totalDescuentoPla += descuento.getVlr();
                                totalImpuestosPla += descuento.getImpuestos();
                                fila++;
                        	xls.adicionarCelda(fila ,col   , ""+descuento.getDocumento(), total );
                                xls.adicionarCelda(fila ,col+1 , ""+descuento.getFactura(), total );
                                xls.adicionarCelda(fila ,col+4 , ""+descuento.getConcepto(), total );
                                xls.adicionarCelda(fila ,col+5 , ""+descuento.getDescripcion(), total );
                                xls.adicionarCelda(fila ,col+7 , ""+Utility.customFormat(descuento.getVlr()), numero );
                                xls.adicionarCelda(fila ,col+8 , ""+Utility.customFormat(descuento.getImpuestos()) , numero );
                            } // end for de descuentos
			    fila++;
                            xls.adicionarCelda(fila ,col   , "Total Facturas Placa", total );
                            xls.adicionarCelda(fila ,col+7 , ""+Utility.customFormat(totalDescuentoPla), numero );
                            xls.adicionarCelda(fila ,col+8 , ""+Utility.customFormat(totalImpuestosPla ), numero );
                            xls.cambiarAnchoColumna(col, 5000);
  		    } // end if de descuentos propietario
                    if (ajustesPropietario!=null && !ajustesPropietario.isEmpty()){
                            fila++;
                            xls.combinarCeldas(fila, col, fila, col+8);
                            xls.adicionarCelda(fila++ ,col   , "Facturas de Propietario " , titulo2 );
                            fila++;
                            //Titulo del detalle
                            xls.adicionarCelda(fila ,col   , "Factura    ", total );
                            xls.adicionarCelda(fila ,col+1 , "Item       ", total );
                            xls.adicionarCelda(fila ,col+2 , "Descripcion", total );
                            xls.adicionarCelda(fila ,col+4 , "Valor      ", total );
                            xls.adicionarCelda(fila ,col+6 , "Impuestos  ", total );
                            xls.adicionarCelda(fila ,col+8 , "Saldo      ", total );
                            for (int k = 0; k<ajustesPropietario.size(); k++){
                                    ExtractoDetalle descuento = (ExtractoDetalle) ajustesPropietario.get(k);
                                    totalDescuentoPro += descuento.getVlr();
                                    totalImpuestosPro += descuento.getImpuestos();
                                    totalsaldo        += descuento.getVlr_pp_item();
                                    fila++;
                                    xls.adicionarCelda(fila ,col   , ""+descuento.getFactura(), total );
                                    xls.adicionarCelda(fila ,col+1 , ""+descuento.getConcepto() , total );
                                    xls.adicionarCelda(fila ,col+2 , ""+descuento.getDescripcion(), total );
                                    xls.adicionarCelda(fila ,col+4 , ""+Utility.customFormat(descuento.getVlr()), numero );
                                    xls.adicionarCelda(fila ,col+6 , ""+Utility.customFormat(descuento.getImpuestos()), numero );
                                    xls.adicionarCelda(fila ,col+8 , ""+Utility.customFormat( descuento.getVlr_pp_item() ), numero );
                                    xls.cambiarAnchoColumna(col, 5000);
                            } // end for de descuentos
			    fila++;
                            xls.adicionarCelda(fila ,col   , "Total Facturas Propietario", total );
                            xls.adicionarCelda(fila ,col+4 , ""+Utility.customFormat(totalDescuentoPro), numero );
                            xls.adicionarCelda(fila ,col+6 , ""+Utility.customFormat(totalImpuestosPro ), numero );
                            xls.adicionarCelda(fila ,col+8, ""+Utility.customFormat(totalsaldo ), numero );
                            xls.cambiarAnchoColumna(col, 5000);
  		    } // end if de descuentos
                    if (ajustesPlaca!=null && !ajustesPlaca.isEmpty()){
                            fila++;
                            xls.combinarCeldas(fila, col, fila, col+4);
                            xls.adicionarCelda(fila++ ,col   , "Resumen de Placas " , titulo2 );
                            fila++;
                            //Titulo del detalle
                            //Titulo del detalle
                            xls.adicionarCelda(fila ,col+3 , "Placa      ", total );
                            xls.adicionarCelda(fila ,col+4 , "Total      ", texto2 );
                            String ant = null;
                            double total_a_mostrar = 0;
                            int  i = 0;
                            for (int l = 0; l<ajustesPropietario.size(); l++){
                                    ExtractoDetalle descuento = (ExtractoDetalle) ajustesPropietario.get(l);
                                    double subtotal = descuento.getVlr();
                                    total_a_mostrar +=  subtotal;
                                    if( (ant!=null && !ant.equals(descuento.getDocumento()))) {
                                            fila++;
                                            i++;
                                            xls.adicionarCelda(fila ,col+3 , ""+ant, total );
                                            xls.adicionarCelda(fila ,col+4 , ""+ Utility.customFormat(total_a_mostrar) , numero );
                                            total_a_mostrar = 0;
                                    }
                                    ant = descuento.getDocumento();
                            }
		    } //edn resumen placa
                    fila++;
                    xls.combinarCeldas(fila, col+2, fila, col+6);
                    xls.adicionarCelda(fila++ ,col+2   , "Datos Generales de Liquidaci�n " , titulo2 );
                    fila++;
                    xls.adicionarCelda(fila ,col+2   , "Total Planillas    ", total );
                    xls.adicionarCelda(fila ,col+6   , ""+Utility.customFormat(totalPlanillas) , numero );
                    fila++;
                    xls.adicionarCelda(fila ,col+3   , "Factura Placas     ", total );
                    xls.adicionarCelda(fila ,col+6   , ""+Utility.customFormat(totalDescuentoPla + totalImpuestosPla) , numero );
                    fila++;
                    xls.adicionarCelda(fila ,col+3   , "Factura Propietario", total );
                    xls.adicionarCelda(fila ,col+6   , ""+Utility.customFormat( totalsaldo ) , numero );
                    fila++;
                    xls.adicionarCelda(fila ,col+3   , "Total Facturas     ", total );
                    xls.adicionarCelda(fila ,col+6   , ""+Utility.customFormat(totalDescuentoPla + totalImpuestosPla + totalsaldo ) , numero );
                    fila++;
                    xls.combinarCeldas(fila, col+2, fila, col+3);
                    xls.adicionarCelda(fila ,col+2   , "Total Liqudacion   ", titulo2 );
                    xls.combinarCeldas(fila, col+5, fila, col+2);
                    xls.adicionarCelda(fila ,col+6   , ""+Utility.customFormat(totalPlanillas+ totalDescuentoPla + totalImpuestosPla + totalsaldo ) , numero );
                    fila++;
                    fila++;
                    xls.combinarCeldas(fila, col+2, fila, col+3);
                    xls.adicionarCelda(fila ,col+2   , "Total Liquidado    " , titulo2 );
                    xls.combinarCeldas(fila, col+5, fila, col+2);
                    xls.adicionarCelda(fila ,col+6   , ""+Utility.customFormat(valorF) , numero );
                    xls.cerrarLibro();
                    System.out.println("terminacion exel");
                }//para datios mayor a cero
                //////////////////////////////////////////////
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion";
                String ruta = path + "/" + usuario.getLogin();
                ruta = ruta + "/Mail"+nit+"-"+secuencia+".xls";
                File f = new File(ruta);
                System.out.println("ruta --> "+ruta);
                //////////////////
                System.out.println("A mandar "+secuencia);
                com.tsp.operation.model.beans.SendMail mail = new com.tsp.operation.model.beans.SendMail();
                mail.setEmailfrom("jgomezfintra@metrotel.net.co");
                mail.setSendername ( "FINTRA" );
                mail.setEmailto(retorno);
                mail.setEmailsubject("Cancelaci�n Pronto Pago ");
                mail.setNombrearchivo("Mail"+nit+"-"+secuencia+".xls");
                mail.setEmailbody(  "                                         TRANSPORTES SANCHEZ POLO S.A. \n"+
                                    "                                     FORMATO AUTORIZACI�N CESI�N DE PAGOS \n"+
                                    "\n"+
                                    "CESI�N  DE  DERECHOS  ECON�MICOS SOBRE EL  SALDO  DE  MANIFIESTO  DE CARGA  DE  TRANSPORTES SANCHEZ POLO S.A. QUIEN \n"+
                                    "SUSCRIBE  ESTE DOCUMENTO EN CALIDAD DE PROPIETARIO  DE UN VEH�CULO  DE SERVICIO  PUBLICO  DE TRANSPORTE DE CARGA, Y \n"+
                                    "QUE SE IDENTIFICA COMO APARECE AL PIE DE SU FIRMA, DECLARA LO SIGUIENTE: \n"+
                                    "\n"+
                                    "\n"+
                                    "1.	QUE  PRESTA SERVICIO  DE  TRANSPORTE EN LA MODALIDAD DE  ENCARGO A TERCEROS, TRASPORTANDO  CARGA  QUE LE HA SIDO \n"+
                                    "ENCOMENDADA POR TRANSPORTES SANCHEZ POLO S.A. EN ESA MODALIDAD DE  EJECUCI�N DEL CONTRATO  DE  TRANSPORTE, SEG�N SE \n"+
                                    "ENTIENDE  Y  EST�  REGULADA  POR EL C�DIGO DE COMERCIO Y LOS CONVENIOS QUE SE HAYAN PACTADO CON TRANSPORTES SANCHEZ \n"+
                                    "POLO S.A. \n"+
                                    "2.	QUE  EN DESARROLLO DE ESTA ACTIVIDAD, ES BENEFICIARIO DE UNA CIERTA SUMA POR VALOR DE FLETES DE TRANSPORTE, SUMA \n"+
                                    "QUE EST�  CONTENIDA Y  REPRESENTADA  EN  EL VALOR DEL SALDO POR FLETE DE EL O LOS MANIFIESTOS DE CARGA (PLANILLA DE \n"+
                                    "TRANSPORTE) QUE SE ESTIPULAN EN EL DOCUMENTO ADJUNTO: \n"+
                                    "\n"+
                                    "Mail"+nit+"-"+secuencia+".xls \n"+
                                    "\n"+
                                    "3.	QUE  MEDIANTE  EL  PRESENTE  DOCUMENTO,  CEDE   DE  MANERA  INTEGRA  IRREVOCABLE   Y  DEFINITIVA,  EL  VALOR  DE \n"+
                                    "LOS DERECHOS  ECON�MICOS  QUE PUEDA  TENER  EN  LOS  MANIFIESTOS  DE  CARGA  MENCIONADOS ANTERIORMENTE, Y QUE EST�N \n"+
                                    "REPRESENTADO  DE  HECHO  EN  EL  VALOR  DEL  SALDO  DE  FLETES  DE  TRANSPORTE  QUE  QUEDEN  A  SU FAVOR. EN DICHOS \n"+
                                    "MANIFIESTOS  LUEGO  DE  LAS  DEDUCCIONES  POR  ANTICIPOS HECHOS A SU FAVOR U OTROS CONCEPTOS QUE LE PUEDA ADEUDAR A \n"+
                                    "TRANSPORTES SANCHEZ POLO S.A. \n"+
                                    "4.OPCI�N DE  CESI�N  DE  LA  TOTALIDAD  DE  LOS FLETES PENDIENTES DE PAGO A LA FECHA: EN CASO DE QUE NO IDENTIFIQUE \n"+
                                    "ESPEC�FICAMENTE  CUALES  MANIFIESTOS  DE CARGA SER�N OBJETO DE ESTA CESI�N DE DERECHOS ECON�MICOS, SE ENTENDER� QUE \n"+
                                    "ESTOY  CEDIENDO  TODOS  LOS  DERECHOS ECON�MICOS(FLETES)QUE ME PUEDAN CORRESPONDER SOBRE  ABSOLUTAMENTE  TODOS  LOS \n"+
                                    "MANIFIESTOS  DE  CARGA QUE A LA FECHA EST�N PENDIENTES DE PAGO POR PARTE DE TRANSPORTES SANCHEZ  POLO S.A., SIEMPRE \n"+
                                    "QUE   SE   ENCUENTREN   DEBIDAMENTE  CUMPLIDOS  POR  LA  PRESTACI�N  A  SATISFACCI�N  DEL  SERVICIO  DE TRANSPORTE. \n"+
                                    "LO  ANTERIOR  IMPLICA  QUE  AUTORIZO  A  TRANSPORTES  SANCHEZ  POLO S.A. PARA QUE HAGA UN BARRIDO EN SUS REGISTROS, \n"+
                                    "DE  TODOS  LOS  SALDOS  QUE  TENGA  A  MI  FAVOR  POR  MANIFIESTOS  DE  CARGA  EXPEDIDOS  A MI FAVOR QUE A LA FECHA \n"+
                                    "EST�N EN LA FORMA PREVISTA EN LA CL�USULA ANTERIOR Y EN LA CL�USULA SIGUIENTE. \n"+
                                    "5.	LA  CESI�N  DE DERECHOS  ECON�MICOS  QUE  SE HA ESTIPULADO EN LAS CL�USULAS ANTERIORES, SE EFECT�A MEDIANTE ESTE \n"+
                                    "DOCUMENTO  A  FAVOR  DEL  BENEFICIO  DE  LA  SOCIEDAD  DE  FINTRA S.A., IDENTIFICADA CON NIT. 802022016-1 EN VIRTUD \n"+
                                    "QUE DICHA SOCIEDAD ME  ENTREGARA  EL VALOR  DEL  SALDO  DE  LOS  MANIFIESTOS  DE  CARGA  MENOS  ALGUNOS  DESCUENTOS \n"+
                                    "COMERCIALES ACEPTADOS. \n"+
                                    "6.QUIEN  SUSCRIBE  ESTA  CESI�N  DE  DERECHOS,  DECLARA  QUE  LOS  DERECHOS  ECON�MICOS   SON   CEDIDOS   POR  ESTE \n"+
                                    "DOCUMENTO   Y  RESPONSABLE   SOLIDARIAMENTE  POR  LA  EXISTENCIA  DE  ESOS  DERECHOS  ECON�MICOS; Y POR LO TANTO SE \n"+
                                    "DECLARA  DEUDOR  DE  LA  SOCIEDAD  FINTRA S.A.  EN  EL  MISMO   MONTO  QUE  LE  HA  SIDO  ENTREGADO  POR  RAZ�N  DE \n"+
                                    "LA PRESENTE  CESI�N  EN AQUELLOS CASOS EN QUE POR CUALQUIER MOTIVO, NO LE SEA REEMBOLSADO A LA SOCIEDAD FINTRA S.A. \n"+
                                    "EL  VALOR  QUE  LE HA ENTREGADO AL SUSCRITO PROPIETARIO EN RAZ�N DE LA PRESENTE CESI�N. POR LO TANTO, SE ACEPTA QUE \n"+
                                    "EN  TALES  EVENTOS  EL  PRESENTE  DOCUMENTO  PRESTE  MERITO  EJECUTIVO PARA EL REEMBOLSO DE LAS SUMAS ENTREGADAS AL \n"+
                                    "PROPIETARIO,  JUNTO  CON  LAS  COPIAS  O CERTIFICACIONES  DE  DEP�SITO  DE  LA   ENTIDAD  BANCARIA  EN  LA  QUE  SE \n"+
                                    "HA CONSIGNADO AL SUSCRITO PROPIETARIO LA SUMA ENTREGADA EN LA PRESENTE CESI�N DE DERECHOS. \n"+
                                    "7.LA  SIGUIENTE  CESI�N  DE  DERECHOS  Y  SUS  CONDICIONES SE CONVIERTEN DE MANERA LIBRE Y POR EXPRESA VOLUNTAD DEL \n"+
                                    "SUSCRITO PROPIETARIO. \n"+
                                    "8.AUTORIZACI�N PARA ENTREGA DE DINERO SEG�N DECLARACI�N PREEXISTENTE QUE HA HECHO A TRANSPORTES SANCHEZ POLO S.A. : \n"+
                                    "EN  CASO  DE  QUE  LAS  SUMAS  DE DINERO QUE SE ME DEBAN POR LA PRESTACI�N DE SERVICIOS DE TRANSPORTES EST�N SIENDO \n"+
                                    "CANCELADAS  CON  CHEQUES  O TRANSFERENCIAS  A NOMBRE DE UN TERCERO, POR SOLICITUD EXPRESA DEL PREEXISTENTE QUE HAYA \n"+
                                    "HECHO EL  SUSCRITO  A TRANSPORTES SANCHEZ POLO S.A., DECLARO IGUALMENTE QUE LA SOCIEDAD BENEFICIARIA DE LA PRESENTE \n"+
                                    "CESI�N DE DERECHOS, LA CUAL HA SIDO IDENTIFICADA EN LA CL�USULA 5, QUEDA AUTORIZADA A GIRAR EL PAGO DEL VALOR DE LA \n"+
                                    "PRESENTE \n"+
                                    "CESI�N DE  DERECHOS, A NOMBRE DEL TERCERO QUE HA SIDO AUTORIZADO PREVIAMENTE POR MI EN TRANSPORTES SANCHEZ POLO S.A.\n"+
                                    "PARA  QUE LOS PAGOS A MI DEBIDOS SE GENEREN A NOMBRE DE DICHO TERCERO; Y EL PAGO HECHO DE ESTA FORMA SE CONSIDERAR� \n"+
                                    "UN PAGO VALIDO Y COMO SI HUBIERA SIDO HECHO DIRECTAMENTE AL SUSCRITO. \n"+
                                    "\n"+
                                    "\n"+
                                    "\n"+
                                    "	FIRMA EN CALIDAD DE PROPIETARIO DE VEH�CULO Y CEDENTE: \n"+
                                    "\n"+
                                    "	____________________________________________ \n");
                mail.setRecstatus ("A");
                mail.setEmailcode ("");
                mail.setEmailcopyto ("");
                mail.setTipo("E");
                /////////////////////////
                model.sendMailService.sendMail(mail,f);
                /////////////////////////
            } //if del mail
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error al Mandar el correo"+ex.getMessage());
        }
        //return retorno;
    }

    /**
     * Metodo para generar los egresos de los reanticipos por pronto pago
     * @autor mfontalvo
     */
    public void GenerarChequesAnticipos (String [] idAnticipos ) throws Exception{
        if ( idAnticipos!=null ){
            for (int i=0; i<idAnticipos.length; i++){
                String []id = { idAnticipos[i] };
                System.out.println("id "+id);
                List lista = model.ImprimirChequeSvc.searchChequesProntoPago(usuario.getDstrct() , usuario.getId_agencia() , usuario.getBase(),usuario.getLogin(), id);
                if (lista!=null && !lista.isEmpty()){
                    System.out.println("lista  "+lista.size());
                    for (int j = 0; j < lista.size(); j++ ){
                        try{
                            //System.out.println("----------->>  "+j);
                            ImpresionCheque cheque = (ImpresionCheque) lista.get(j);
                            String sql = model.ImprimirChequeSvc.updateRecordProntoPago(cheque,  usuario.getLogin());
                            if ( cheque.getCheque() == null ){
                                throw new Exception("No se pudo generar el numero del cheque, por favor verifique la serie..." +
                                "<br>Banco   : " + cheque.getBanco() +
                                "<br>Sucursal: " + cheque.getAgenciaBanco());
                            }

                            System.out.println("SQL : "+ sql);

                            TransaccionService  svc =  new  TransaccionService(this.usuario.getBd());
                            svc.crearStatement();
                            svc.getSt().addBatch(sql);
                            svc.execute();
                        }catch(Exception e){
                            throw new Exception( e.getMessage() );
                        }
                    }
                }
            }
        }
        //System.out.println("Fin de Codigo Mario------------------------------------------------------");
    }
    /**
     * Metodo para generar un registro de detalle en el archivo de transferencia para AVVILLAS
     * @param transf bean AnticiposTerceros con la informacion para generar el registro
     * @throws Exception
     */
    public void avvillas(AnticiposTerceros transf) throws Exception {
        try {

            // DETALLE DE TRANSACCION:

            String tipoCta = transf.getTipo_cuenta();

            // Codigo del  banco del beneficiario
            String codigoBanco = CERO;
            Hashtable infoBanco = model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_AVVILLAS_BANCO, transf.getBanco());
            if (infoBanco == null) {
                throw new Exception("No hay información del banco  " + transf.getBanco() + " en formato avvillas");
            } else {
                codigoBanco = (String) infoBanco.get("descripcion");
            }

            String tipo_transaction = equivalenciaTipoTransaction(AVVILLAS, tipoCta);
            String tipoCuentaDestino = equivalenciaTipoCta(AVVILLAS,tipoCta);

            // Valor
            int valor = (int) transf.getVlrConsignar();

            String dato =
                    AVVILLAS_TIPO_REGISTRO_DET +
                    tipo_transaction +
                    rellenar(codigoBanco, CERO, 4, IZQUIERDA) + // Codigo del banco destino
                    PLAZA_BARRANQUILLA + // codigo para barranquilla
                    rellenar(transf.getNit_cuenta(), SPACE, 15, IZQUIERDA) + // nit beneficiario
                    AVVILLAS_NIT + // tipo= nit
                    rellenar(transf.getCuenta(), SPACE, 17, DERECHA) +
                    tipoCuentaDestino +
                    rellenar(transf.getNombre_cuenta(), SPACE, 22, DERECHA) +
                    "0" + // ind_mas_addendas 0=una addenda, 1=mas de una addenda
                    rellenar(String.valueOf(valor) + "00", CERO, 18, IZQUIERDA) +
                    "1"; // flag_valida 1=valida identificacion, 0=no valida

            linea.println(dato);


        } catch (Exception e) {
            throw new Exception(" Error en avvillas " + e.getMessage());
        }
    }


    /**
     * Metodo para generar un registro de detalle en el archivo de transferencia para COLPATRIA
     * @param transf bean AnticiposTerceros con la informacion para generar el registro
     * @throws Exception
     */
    public void colpatria(AnticiposTerceros transf) throws Exception {
        try {

            // DETALLE DE TRANSACCION:

            String codigoBanco = CERO;
            Hashtable infoBanco = model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_COLPATRIA_BANCO, transf.getBanco());
            if (infoBanco == null) {
                throw new Exception("No hay información del banco  " + transf.getBanco() + " en formato colpatria");
            } else {
                codigoBanco = (String) infoBanco.get("descripcion");
            }

            String codigoTransaccion = "";
            String cuentaColpatria = "";
            String cuentaOtroBanco = "";
            if(transf.getBanco().equals("COLPATRIA")){
                codigoTransaccion="902";
                cuentaColpatria = rellenar(transf.getCuenta(), CERO, 12, IZQUIERDA);
                cuentaOtroBanco = rellenar(CERO, CERO, 17, IZQUIERDA);
            }else{
                codigoTransaccion="911";
                cuentaColpatria = rellenar(CERO, CERO, 12, IZQUIERDA);
                cuentaOtroBanco = rellenar(transf.getCuenta(), CERO, 17, IZQUIERDA);
            }

            String tipoCuentaDestino = equivalenciaTipoCta(COLPATRIA,transf.getTipo_cuenta());

            //  Valor
            int valor = (int) transf.getVlrConsignar();

            String dato =
                    rellenar(String.valueOf(transf.getSecuencia()+1), CERO, 5, IZQUIERDA) + // Consecutivo del registro
                    COLPATRIA_TIPO_REGISTRO_DET + // Tipo Registro detalle
                    cuentaColpatria + // Cta Beneficiario si es a colpatria
                    rellenar(transf.getNit_cuenta(), CERO, 11, IZQUIERDA) + // Nit beneficiario
                    rellenar(transf.getNombre_cuenta(), SPACE, 40, DERECHA) + // Nombre del beneficiario
                    codigoTransaccion + // Codigo de transaccion 911=abono en otra entidad
                    COLPATRIA_TIPO_TRANSACCION + // Tipo de cargo a aplicar
                    rellenar(String.valueOf(valor) + "00", CERO, 15, IZQUIERDA) + // Valor
                    rellenar(SPACE, SPACE, 10, DERECHA) + // numero de factura
                    rellenar(CERO, CERO, 6, IZQUIERDA) + // Numero control de pago
                    rellenar(CERO, CERO, 15, IZQUIERDA) + // Retencion en la fuente
                    rellenar(CERO, CERO, 15, IZQUIERDA) + // iva
                    this.getFecha(DDMMAAAA) + // Fecha a realizar el pago
                    rellenar(CERO, CERO, 10, IZQUIERDA) + //
                    rellenar(CERO, CERO, 15, IZQUIERDA) + //
                    rellenar(codigoBanco, CERO, 8, IZQUIERDA) + // Codigo banco beneficiario
                    cuentaOtroBanco + // Número de la Cta
                    tipoCuentaDestino +
                    "N" + //tipo de documento N=nit
                    rellenar(SPACE, SPACE, 4, IZQUIERDA) +
                    rellenar(SPACE, SPACE, 80, IZQUIERDA); // adenda

            linea.println(dato);


        } catch (Exception e) {
            throw new Exception(" occidente " + e.getMessage());
        }
    }





    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons + "_" + fmt.format(new Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }



   /***********************************************carta pdf anticipos ************************************************************/
protected boolean exportarCartaPdf(String userlogin,List  listaAgrupada) throws Exception {


           int lmt=12;

           boolean generado = true;
            String directorio = "";String documento_detalle="";
            ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "CartaBanco"+this.DESC_BANCO_PROVEEDOR.replace(" ","_" ), "pdf");
            Font fuente = new Font(Font.HELVETICA, 11);
            Font fuenteB = new Font(Font.HELVETICA, 11, Font.BOLD);
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));

            documento.setFooter(this.getMyFooter());
            documento.open();
            documento.newPage();



            Image img= Image.getInstance(rb.getString("ruta")+"/images/"+url_logo);
            img.scaleToFit(200, 200);
            documento.add(img);
            documento.add(Chunk.NEWLINE);
            documento.add(new Paragraph("802.022.016-1.",fuenteB));
            documento.add(Chunk.NEWLINE);
            documento.add(new Paragraph("Barranquilla, "+dia+" de "+Utility.NombreMes(Integer.parseInt(mes)+1)+" de "+annio,fuente));
            documento.add(Chunk.NEWLINE);
            documento.add(new Paragraph("Se�ores"));
            documento.add(new Paragraph("FIDUCIARIA CORFICOLOMBIANA S.A.",fuenteB));
            documento.add(new Paragraph("Att  Adelaida Ortiz Orellano",fuente));
            documento.add(new Paragraph("Gerente Regional",fuente));
            documento.add(new Paragraph("Ciudad",fuente));
            documento.add(Chunk.NEWLINE);

            documento.add(new Paragraph("Solicitamos retiro por restituci�n de aportes a nombre de "+this.AsuntoCarta(this.BANCO_PROVEEDOR)+" para realizar las siguientes transferencias:",fuente));
            documento.add(Chunk.NEWLINE);

           /*----------------------------------lista Anticipos--------------------------------------*/

            if(listaAgrupada.size()>lmt)
            {
                int x=0;

                    x=0;
                    PdfPTable tabla_datos_pro = this.TrasferenciasAnticipos(listaAgrupada,0,lmt);
                    tabla_datos_pro.setWidthPercentage(100);
                    documento.add(tabla_datos_pro);

                    documento.newPage();
                    documento.add(Chunk.NEWLINE); documento.add(Chunk.NEWLINE);
                    tabla_datos_pro = this.TrasferenciasAnticipos(listaAgrupada,lmt,listaAgrupada.size());
                    tabla_datos_pro.setWidthPercentage(100);
                    documento.add(tabla_datos_pro);
            }
            else
            {
            PdfPTable tabla_datos_pro = this.TrasferenciasAnticipos(listaAgrupada);
            tabla_datos_pro.setWidthPercentage(100);
            documento.add(tabla_datos_pro);
            }


            PdfPTable tabla_total = this.Total(listaAgrupada);
            tabla_total.setWidthPercentage(100);
            documento.add(tabla_total);


            documento.add(Chunk.NEWLINE); documento.add(Chunk.NEWLINE);
            documento.add(new Paragraph("Atentamente,",fuente));
            documento.add(Chunk.NEWLINE);  documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);

            /*----------------------------------firmas carta--------------------------------------*/
            PdfPTable tabla_frimas = this.datosFirmas();
           tabla_frimas.setWidthPercentage(100);
           documento.add(tabla_frimas);
            documento.close();

        }
   catch (Exception e)
        {
            generado = false;
            System.out.println("error al generar pdf solicitud de aval: " + e.toString());
            e.printStackTrace();
        }
        System.out.println("fin elaboracion pdf solicitud de aval");
        //Envio de Correo

        return generado;
    }
   /*********************************************************************************************************************/


   private Document createDoc() {
        Document doc = new Document(PageSize.LETTER, 60, 40, 40, 40);//
        doc.addAuthor ("Fintravalores S.A");
        doc.addSubject ("Trasferencias");
        return doc;
    }







    /************************************** trasferencias***************************************************/
  protected PdfPTable TrasferenciasAnticipos( List  listaAgrupada) throws DocumentException {

        double vlr=0,ip=0;

        PdfPTable tabla_temp = new PdfPTable(6);
      float[] medidaCeldas = {0.340f, 0.110f, 0.190f, 0.100f, 0.150f,0.130f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.TIMES_ROMAN, 9);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();

        // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TITULAR", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);





        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("NIT", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("BANCO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TIPO CTA", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("CUENTA", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("VALOR", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

       for (int i=0;i<listaAgrupada.size();i++)
       {
           AnticiposTerceros  ant  = (AnticiposTerceros) listaAgrupada.get(i);
           vlr=vlr+ant.getVlr();


            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(ant.getNombre_cuenta(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(ant.getNit_cuenta(), fuente));
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(ant.getBanco(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(ant.getTipo_cuenta(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(ant.getCuenta(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Utility.customFormat(ant.getVlrConsignar()), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tabla_temp.addCell(celda_temp);


        }

        return tabla_temp;
    }





  /************************************** trasferencias***************************************************/
  protected PdfPTable TrasferenciasAnticipos( List  listaAgrupada ,int x, int y) throws DocumentException {

        double vlr=0,ip=0;

        PdfPTable tabla_temp = new PdfPTable(6);
       float[] medidaCeldas = {0.340f, 0.110f, 0.190f, 0.100f, 0.150f,0.130f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();

        // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TITULAR", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("NIT", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("BANCO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TIPO CTA", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("CUENTA", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("VALOR", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

       for (int i=x;i<y;i++)
       {
           AnticiposTerceros  ant  = (AnticiposTerceros) listaAgrupada.get(i);
           vlr=vlr+ant.getVlr();


            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(ant.getNombre_cuenta(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(ant.getNit_cuenta(), fuente));
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(ant.getBanco(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(ant.getTipo_cuenta(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(ant.getCuenta(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Utility.customFormat(ant.getVlrConsignar()), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tabla_temp.addCell(celda_temp);


        }


        return tabla_temp;
    }








  /************************************** tabla total***************************************************/
  protected PdfPTable Total( List  listaAgrupada ) throws DocumentException {

        double vlr=0,ip=0;

        PdfPTable tabla_temp = new PdfPTable(6);
       float[] medidaCeldas = {0.340f, 0.110f, 0.190f, 0.100f, 0.150f,0.130f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

      for (int i=0;i<listaAgrupada.size();i++)
       {
           AnticiposTerceros  ant  = (AnticiposTerceros) listaAgrupada.get(i);
           vlr=vlr+ant.getVlrConsignar();
       }



         celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(6);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        // fila 3 + ajustesPropietario.size()
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL ", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(vlr), fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }





     /*--------------------------------------------------------------*/
        protected PdfPTable datosFirmas() throws DocumentException {



        PdfPTable tabla_temp = new PdfPTable(3);
        float[] medidaCeldas = {0.400f,  0.200f,0.400f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 10);
        Font fuenteB = new Font(Font.HELVETICA, 10, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();


        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("JOSE LUIS GOMEZ OLARTE", fuente));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);



        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);



        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("ALVARO PABON", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("85.465.887", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
       celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("", fuente));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("72.292.060", fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
       celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }



        public HeaderFooter getMyFooter() throws ServletException {
        try {
             Phrase p = new Phrase();
            Font fuente = new Font(Font.HELVETICA, 9);
            Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
             p.clear();


             p.add(new Paragraph("Carrera 53 # 79 - 01 Local 205 Barranquilla, Colombia\n",fuenteB));
             p.add(new Paragraph("PBX: 57 5 3679900 FAX: 57 5 3679906\n",fuenteB));
             p.add(new Paragraph("www.fintravalores.com\n",fuenteB));
             HeaderFooter header = new HeaderFooter(p, false);
             header.setBorder(Rectangle.NO_BORDER);
             header.setAlignment(Paragraph.ALIGN_CENTER);

             return header;
         } catch (Exception ex) {
            throw new ServletException("Header Error");
         }
     }



public String AsuntoCarta(String banco)
    {
    String as="FIDUCIARIA CORFICOLOMBIANA S.A. FIDEICOMISO FINTRAVALORES No de encargo 906110";
    if(banco.equals("FC"))
    {
       as="FIDUCIARIA CORFICOLOMBIANA S.A. FIDEICOMISO FINTRAVALORES - CONSORCIO MULTISERVICIOS No de encargo 906306";
    }
    return as;
   }




/*****************************Archivo Excel*******************************************/

         private void exportarExcel(List listaAgrupada,String userlogin) throws Exception{
          double vlr=0,ip=0;

        try {
            if(listaAgrupada!=null && listaAgrupada.size()>0){
           String ruta = this.directorioArchivo(userlogin, "listado_"+this.DESC_BANCO_PROVEEDOR.replace(" ","_" ), "xls");
               ExcelApplication excel = this.instanciar("Transferencias");
                int filaact = 0;


                    int col = 0;


                    if((listaAgrupada!=null)&&(listaAgrupada.size()>0)){
                        excel.cambiarAnchoColumna(1, 10000);//10000-->10cm
                        excel.cambiarAnchoColumna(2, 3000);
                        excel.setDataCell(filaact, 1, "TRANSFERENCIAS");
                        excel.setCellStyle(filaact, 1, excel.getStyle("estilo2"));
                        filaact++;
                        excel.setDataCell(filaact, 1, "TITULAR");
                        excel.setCellStyle(filaact, 1, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 2, "NIT");
                        excel.setCellStyle(filaact, 2, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 3, "BANCO");
                        excel.cambiarAnchoColumna(3, 5000);
                        excel.setCellStyle(filaact, 3, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 4, "TIPO CUENTA");
                        excel.cambiarAnchoColumna(4, 3000);
                        excel.setCellStyle(filaact, 4, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 5, "CUENTA");
                        excel.setCellStyle(filaact, 5, excel.getStyle("estilo2"));
                        excel.cambiarAnchoColumna(5, 5000);
                        excel.setDataCell(filaact, 6, "VALOR");
                        excel.cambiarAnchoColumna(6, 5000);
                        excel.setCellStyle(filaact, 6, excel.getStyle("estilo2"));
                        filaact++;
                        for (int i = 0; i < listaAgrupada.size(); i++) {
                           AnticiposTerceros  ant  = (AnticiposTerceros) listaAgrupada.get(i);
                            vlr=vlr+ant.getVlrConsignar();
                          //escr

                            excel.setDataCell(filaact, 1,ant.getNombre_cuenta());
                            excel.setCellStyle(filaact, 1, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 2, ant.getNit_cuenta());
                            excel.setCellStyle(filaact, 2, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 3, ant.getBanco());
                            excel.setCellStyle(filaact, 3, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 4, ant.getTipo_cuenta());
                            excel.setCellStyle(filaact, 4, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 5, ant.getCuenta());
                            excel.setCellStyle(filaact, 5, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 6, Utility.customFormat(ant.getVlrConsignar()));
                            excel.setCellStyle(filaact, 6, excel.getStyle("estilo3"));







                            ant=null;
                            filaact++;

                        excel.setDataCell(filaact, 1, "");
                        excel.setDataCell(filaact, 2, "");
                        excel.setDataCell(filaact, 3, "");
                        excel.setDataCell(filaact, 4, "");
                        excel.setDataCell(filaact, 5, "Total");


                        excel.setCellStyle(filaact, 5, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 6, Utility.customFormat(vlr));
                        excel.setCellStyle(filaact, 6, excel.getStyle("estilo2"));
                        }
                        filaact++;

                    }


                excel.saveToFile(ruta);
            }
        }
        catch (Exception e) {
            throw new Exception("Error al general el excel de codigos contables: "+e.toString());
        }
    }

     /**
     * Crea un objeto tipo ExcelApplication
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private ExcelApplication instanciar(String descripcion) throws Exception{
        ExcelApplication excel = new ExcelApplication();


        try{
            excel.createSheet(descripcion);
            excel.createFont("Titulo", "Arial", (short)1, true, (short)12);
            excel.createFont("Subtitulo", "Verdana", (short)1, true, (short)10);
            excel.createFont("Contenido", "Verdana", (short)0, false, (short)10);

            excel.createColor((short)11, (byte)255, (byte)255, (byte)255);//Blanco
            excel.createColor((short)9, (byte)26, (byte)126, (byte)0);//Verde dark
            excel.createColor((short)10, (byte)171, (byte)243, (byte)169);//Verde light

            excel.createStyle("estilo1", excel.getFont("Titulo"), (short)10, true, "@",HSSFCellStyle.ALIGN_CENTER);
            excel.createStyle("estilo2", excel.getFont("Subtitulo"), (short)9, true, "@",HSSFCellStyle.ALIGN_CENTER);
            excel.createStyle("estilo3", excel.getFont("Contenido"), (short)11, true, "@",HSSFCellStyle.ALIGN_CENTER);





        }
        catch(Exception e){
            throw new Exception("Error al instanciar el objeto: "+e.toString());
        }
        return excel;
    }







/**
     * Crea un objeto tipo ExcelApplication
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private int Enviar_SMS(List listaAgrupada) throws Exception{
       NitSot nit = new NitSot();
       NitDAO nitdao= new NitDAO();
       String mensaje="";
       String comentario="";
       int ret=-1;
       Sms sms = new Sms();
       System.out.println("Enviar");
      // sms.setCell("3016741089");



        try{
           if(listaAgrupada!=null && listaAgrupada.size()>0)
           {


               for (int i = 0; i < listaAgrupada.size(); i++)
               {
                  AnticiposTerceros  ant  = (AnticiposTerceros) listaAgrupada.get(i);         
                  nit=nitdao.searchNit(ant.getNit_cuenta());

                            if(nit.getCellular().equals(""))
                            {
                               nit=nitdao.searchNit(ant.getConductor());                      
                               if(nit.getCellular().equals(""))
                               {

                                   nit=nitdao.searchNit(ant.getPla_owner());
                               }
                            }

                             nit.setCellular(nit.getCellular().replace("-",""));

                            sms.setCell(nit.getCellular()); sms.setNit(nit.getCedula());
                            sms.setTipo("ATP");
                            if(!nit.getCellular().equals(""))
                            {
                                   System.out.println(nit.getCellular());                                                                                                      
                                   sms.setSMS(getMensajeAnticvipo(ant));
                                   sms= model.Smservice.envia_SMS_HTTP(sms);
                            }
                            else
                            {
                                 sms.setEstado("INVALID_DESTINATION");
                                 sms.setFechaEnvio("0099-01-01 00:00:00");

                            }
                          model.Smservice.InsertarSMS(sms);
               }
           }

        }
        catch(Exception e)
        {
             model.Smservice.InsertarSMS(sms);
            throw new Exception("Error al instanciar el objeto: "+e.toString());
        }
        return 1;
    }


public String getMensajeAnticvipo(AnticiposTerceros anticipo)
{
  Calendar c = new GregorianCalendar();
  String dia = Integer.toString(c.get(Calendar.DATE));
  String mes = Integer.toString(c.get(Calendar.MONTH));
  String anio = Integer.toString(c.get(Calendar.YEAR));
  if((Integer.parseInt(mes)+1)<10)
  {
      mes="0"+(Integer.parseInt(mes)+1);
  }
 else
  {
       mes=String.valueOf(Integer.parseInt(mes)+1);
  }



  if((Integer.parseInt(dia))<10)
  {
      dia="0"+(Integer.parseInt(mes)+1);
  }


  String mesx="";
  String fecha=dia+"/"+mes+"/"+anio;
   String mensaje="";
  String ach="";

    if(anticipo.getBanco().equals("BANCOLOMBIA"))
    {
        mensaje="Fintra abono cuenta "+  anticipo.getBanco()+" "+anticipo.getTipo_cuenta()+" "+anticipo.getCuenta()+" anticipo OC "+anticipo.getPlanilla()+","+
                "valor: $"+Utility.customFormat(anticipo.getVlrConsignar())+
                " en caso de inconveniente contactar al (5)3679901 opcion 3. "+fecha;
    }
    else
    {
      anticipo.setBanco(anticipo.getBanco().replaceAll("BANCO", "BCO"));
      mensaje="Fintra abona a las 18:30,$"+Utility.customFormat(anticipo.getVlrConsignar())+" en " +  anticipo.getBanco()+" "+
               anticipo.getTipo_cuenta()+" "+anticipo.getCuenta()+
              " anticipo OC "+anticipo.getPlanilla()+", en caso de inconveniente contactar al (5)3679901 opcion 3. "+fecha;
    }

    return mensaje;

    }



 /**
     * Metodo que ecribe los las transferencias a realizar en formato BANCOLOMBIA PAB
     * @autor Diana Arrieta
     * @throws Exception
     */
    public void bancolombiaPAB(AnticiposTerceros  transf)throws Exception{
           try{

               // 2. DETALLE DE TRANSACCION:

               String tipoCta = transf.getTipo_cuenta();


               // Codigo del  banco del beneficiario
               String codigoBanco = CERO;
               Hashtable infoBanco = model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_BANCOLOMBIA_BANCO, transf.getBanco());
               if (infoBanco == null) {
                   throw new Exception("No hay informaci�n del banco  " + transf.getBanco() + " en formato bancolombia");
               } else {
                   codigoBanco = (String) infoBanco.get("descripcion");
               }


               // Indicador de pago: sucursal
               String indicador_pago = "S";

               // Tipo de transacion:
               String tipo_transaction = equivalenciaTipoTransaction(BANCOLOMBIA, tipoCta);

               // Valor que le restamos 1 si ya tiene una trasnferencia en los ultimos cuatro dias
               int valor = (int) transf.getVlrConsignar() - validarMontoRepetidoDetalle(transf);

               String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO_DET                                      +  // 1  Tipo de Registro detalle
                                     rellenar( transf.getNit_cuenta(),          SPACE,  15, DERECHA   ) +  // 2  Nit beneficiario
                                     rellenar( transf.getNombre_cuenta(),       SPACE, 30, DERECHA    ) +  // 3  Nombre del beneficiario
                                     rellenar( codigoBanco,                     CERO,   9, IZQUIERDA  ) +  // 4  Codigo banco
                                     rellenar( transf.getCuenta(),              SPACE,  17, DERECHA   ) +  // 5  N�mero de la Cta
                                     indicador_pago                                                     +  // 6  Lugar de pago
                                     tipo_transaction                                                   +  // 7  Tipo de Transaction
                                     rellenar( String.valueOf(valor) + "00",    CERO,  17, IZQUIERDA  ) +  // 8  Valor
                                     this.getFecha(AAAAMMDD)                                            +  // 9  Fecha aplicaci�n  AAAAMMDD
                                     rellenar( this.BANCOLOMBIA_PROPOSITO ,     SPACE,  9, DERECHA    ) +  // 10  Proposito descripci�n
                                     rellenar( transf.getFactura_mims(),        SPACE, 12, DERECHA    ) +  // 11 referencia                       
                                     rellenar( SPACE,                           SPACE,  1, DERECHA    ) +  // 12 Tipo de documento de identificaci�n
                                     rellenar( SPACE,                           SPACE,  5, DERECHA    ) +  // 13 Oficina de entrega
                                     rellenar( SPACE,                           SPACE, 15, DERECHA    ) +  // 14 N�mero de Fax
                                     rellenar( SPACE,                           SPACE, 80, DERECHA    ) +  // 15 E-mail beneficiario
                                     rellenar( SPACE,                           SPACE, 15, DERECHA    ) +  // 16 N�mero identificaci�n del autorizado
                                     rellenar( SPACE,                           SPACE, 27, DERECHA    );   // 17 Filler

              linea.println( dato );


           } catch(Exception e){
             throw new Exception( " bancolombia " + e.getMessage());
          }
    }
    
    
        /**
     * Obtiene el tipo de tranasaccion para un banco
     * @param codeBanco
     * @param listaAnticipos 
     */
    public void obtenerTipoTransaccion(String codeBanco, String tipo){
        // BANCOLOMBIA
        if(codeBanco.equals(BANCOLOMBIAPAB)){
            if(tipo.equals(BANCOLOMBIA_CREDIPAGO)){
                BANCOLOMBIA_TIPO_TRANSACTION = BANCOLOMBIA_CPAG_TIPO_TRANSACTION;
            }
        }
    }

    public void EncriptarArchivo() throws Exception {
        PGPFileProcessor p = new PGPFileProcessor();
        p.setInputFileName(E_INPUT);
        p.setOutputFileName(E_OUTPUT);
        p.setPassphrase(PASSPHRASE);
        p.setPublicKeyFileName(E_KEY_FILE);
        if (p.encrypt()) {
              
            String archivoSalida = model.DirectorioSvc.getRuta() + "/copiatsp/" + NAME_FILE;
            File filein = new File(E_INPUT);
            File filecopy=new File(archivoSalida);
            copyFile(filein, filecopy);
            filein.delete();

        } else {

            System.err.println("Error generando archivo plano encriptado.");
}

    }

    /*
     * Metodo para validar si hay que hacer un ajuste al valor a consignar en la cabecera 
     *autor: egonzalez 
     *fecha: 2014-02-12
     * 
     */
    
    public int validarMontoRepetidoCabecera(String cuenta,String nit_cuenta, double valorConsignacion) throws Exception {
        //  boolean estado = false;
        ArrayList lista = model.AnticiposPagosTercerosSvc.obtenerUtimasTrasferencias(cuenta, nit_cuenta);
        int contador = 0;
        if (!lista.isEmpty()) {

            for (int i = 0; i < lista.size(); i++) {
                AnticiposTerceros tercero = (AnticiposTerceros) lista.get(i);

                if (valorConsignacion == tercero.getVlrConsignar()) {
                    contador++;
                    //        estado= true;
                }
            }

        } else {

            return contador;

        }
        
        //Aqui vamos a validar que el valor calculado no se repita solo cuando es es mayor que cero 
        if (contador > 0) {
            contador = validarValorCalculado(valorConsignacion - contador, lista, contador);
        }

        

        return contador;
    }
    
    /*
     * Metodo para validar si hay que hacer un ajuste al valor a consignar en la cabecera 
     *autor: egonzalez 
     *fecha: 2014-02-12
     * 
     */
    
    public int validarMontoRepetidoDetalle(AnticiposTerceros transf) throws Exception {

        //metodo que validad si hay que sumar el peso al detalle del archivo
        //retorna true si hayq ue sumar el peso 
        int peso = validarMontoRepetidoCabecera(transf.getCuenta(),transf.getNit_cuenta(), transf.getVlrConsignar());
        //aqui vamos a llamr un procedimiento que genre un comprobante con el peso descontado
        if (peso > 0) {
            ajustarValorConsignacion(peso, transf);
        }
        return peso;
    }
    
    /*
     * Metodo para actualizar el valor de consignacion  para que cuadre el egreso 
     *autor: egonzalez 
     *fecha: 2014-02-17
     * 
     */
      
    public void ajustarValorConsignacion(int peso, AnticiposTerceros trans) throws Exception {
      
        if (tipoOperacion.equals("PRONTOPAGO")) {//pronto pago
            String sql = model.AnticiposPagosTercerosSvc.updatePesoAjuste(trans, peso, usuario);
            //ejecutamos la actualizacion.
            System.out.println("QueryProntoPago: " + sql);
            ejecutar(sql);

        } else if (tipoOperacion.equals("TRANSFERENCIA")) {//transferencia
            String sql = model.AnticiposPagosTercerosSvc.updatePesoAjusteTrasferencias(trans, peso, usuario);
            System.out.println("QueryTransferencia: " + sql);
            ejecutar(sql);
        }

    }
    
    public int validarValorCalculado(double valorCalculado, ArrayList lista, int peso) throws Exception {

        int contador = 0;
        for (int i = 0; i < lista.size(); i++) {
            AnticiposTerceros at = (AnticiposTerceros) lista.get(i);
            if (valorCalculado == at.getVlrConsignar()) {
                contador++;
            }
        }

        valorCalculado = valorCalculado - contador;
        peso = peso + contador;

        if (contador > 0) {

            validarValorCalculado(valorCalculado, lista, peso);

        }

        return peso;

    }
    
     public void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel origen = null;
        FileChannel destino = null;
        try {
            origen = new FileInputStream(sourceFile).getChannel();
            destino = new FileOutputStream(destFile).getChannel();

            long count = 0;
            long size = origen.size();
            while ((count += destino.transferFrom(origen, count, size - count)) < size);
        } finally {
            if (origen != null) {
                origen.close();
            }
            if (destino != null) {
                destino.close();
            }
        }
    }

}

//Entregado a Jbarros 21 Febrero



/*Fintra abon� cuenta BANCOLOMBIA CA 00000000000 anticipo OC 9001010, valor: $1000000 en caso de inconveniente contactar al
(5) 3679901 opci�n 3 dd/mm/a�os*/