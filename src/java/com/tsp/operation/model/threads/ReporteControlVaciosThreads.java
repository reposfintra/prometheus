/*************************************************************
 * Nombre: ReporteControlVaciosThreads.java
 * Descripci�n: Hilo para crear el reporte de control de vacios
 * Autor: Ing. Jose de la rosa
 * Fecha: 12 de agosto de 2006, 12:05 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class ReporteControlVaciosThreads extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String fecini = "";
    String fecfin = "";
    String dstrct = "";
    Model model;
    
    /** Creates a new instance of ReporteControlVaciosThreads */
    public ReporteControlVaciosThreads ( Model model ) {
        this.model = model;
    }
        
    public void start( String fecini, String fecfin, String id, String dstrct){        
        this.procesoName = "Reporte de Control de Vacios";
        this.des = "Reporte de Control de Vacios";
        this.id = id;
        this.fecini = fecini;
        this.fecfin = fecfin;
        this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
            String fecha_actual = Util.getFechaActual_String(6);
            
            //registramos el proeso en el log de procesos
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            
            //obtenemos la lista de facturas creadas segun parametros definidos
            Vector lista = model.planillaService.planillasVacias (fecini, fecfin);
            if (lista!=null && lista.size()>0){
                Usuario us = (Usuario) model.usuarioService.obtenerUsuario(id);
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                
                File file = new File(path);
                file.mkdirs();
                
                String a�o = fecha_actual.substring(0,4);
                String mes = fecha_actual.substring(5,7);
                String dia = fecha_actual.substring(8,10);
                String hora = fecha_actual.substring(11,13);
                String min = fecha_actual.substring(14,16);
                String seg = fecha_actual.substring(17,19);
                
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite(path+"/CONTROL_VACIOS " +a�o + "-" + mes + "-" + dia +".xls");
                
                // Definicion de estilos: fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado                                           
                HSSFCellStyle fecha = xls.nuevoEstilo(      "verdana", 12, false , false, "yyyy/mm/dd", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_RIGHT );
                HSSFCellStyle texto = xls.nuevoEstilo(      "verdana", 12, false, false, "text", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle numero = xls.nuevoEstilo(     "verdana", 12, false, false, "", xls.NONE, xls.NONE, HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle negrita = xls.nuevoEstilo(    "verdana", 12, true, false, "text", HSSFColor.BLACK.index, xls.NONE, xls.NONE);
                HSSFCellStyle header = xls.nuevoEstilo(     "verdana", 18, true, false, "text", HSSFColor.LIME.index,xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo = xls.nuevoEstilo(     "verdana", 16, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle subtitulo = xls.nuevoEstilo(  "verdana", 14, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle cabecera = xls.nuevoEstilo(   "verdana", 12, true, false, "text", HSSFColor.WHITE.index, HSSFColor.INDIGO.index, HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja("BASE");
                //formateo al abrir hoja (tamano)
                xls.cambiarMagnificacion(3,4);
                
                //Titulo (cabecera)
                xls.adicionarCelda(0, 0, "TRANSPORTES SANCHEZ POLO S.A", header);
                xls.combinarCeldas(0, 0, 0, 5);  
                xls.adicionarCelda(2, 0, "REPORTE CONTROL DE VACIOS", titulo);
                xls.combinarCeldas(2, 0, 0, 5);
                xls.adicionarCelda(3, 0, "Fecha Inicial:", subtitulo);
                xls.adicionarCelda(3, 1, fecini, negrita);
                xls.adicionarCelda(3, 2, "Fecha Final:", subtitulo);
                xls.adicionarCelda(3, 3, fecfin, negrita);
                xls.adicionarCelda(4, 0, "Elaborado Por: ", subtitulo);
                xls.adicionarCelda(4, 1, us.getNombre(), negrita);
                xls.adicionarCelda(4, 2, "Agencia:", subtitulo);
                xls.adicionarCelda(4, 3, model.ciudadservice.obtenerNombreCiudad(us.getId_agencia()), negrita);
                xls.adicionarCelda(4, 4, "Distrito:", subtitulo);
                xls.adicionarCelda(4, 5, dstrct, negrita);
                xls.adicionarCelda(5, 0, "Fecha Reporte", subtitulo);
                xls.adicionarCelda(5, 1, fecha_actual, negrita);
                
                int fila = 7;
                int col  = 0;
                
                
                //definicion de titulos de la tabla
                xls.adicionarCelda(fila ,col++ , "Planilla del Vacio"           , cabecera );
                xls.adicionarCelda(fila ,col++ , "Placa"                        , cabecera );
                xls.adicionarCelda(fila ,col++ , "Remesa del Vacio"             , cabecera );
                xls.adicionarCelda(fila ,col++ , "Fecha del Vacio"              , cabecera );
                xls.adicionarCelda(fila ,col++ , "Conductor del Vacio"          , cabecera );
                xls.adicionarCelda(fila ,col++ , "Cliente"                      , cabecera );
                xls.adicionarCelda(fila ,col++ , "Origen del Vacio"             , cabecera );
                xls.adicionarCelda(fila ,col++ , "Destino del Vacio"            , cabecera );
                xls.adicionarCelda(fila ,col++ , "Tiene Reporte en Trafico"     , cabecera );
                
                xls.adicionarCelda(fila ,col++ , "Planilla LLena"               , cabecera );
                xls.adicionarCelda(fila ,col++ , "Fecha Planilla LLena"         , cabecera );
                xls.adicionarCelda(fila ,col++ , "Conductor Planilla LLena"     , cabecera );
                xls.adicionarCelda(fila ,col++ , "Cliente Planilla LLena"       , cabecera );
                xls.adicionarCelda(fila ,col++ , "Origen Planilla LLena"        , cabecera );
                xls.adicionarCelda(fila ,col++ , "Destino Planilla LLena"       , cabecera );
                
                
                //definicion de la tabla con los datos obtenidos de la consulta
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    fila++;
                    col = 0;
                    Planilla pla = (Planilla) it.next(); 
                    
                    xls.adicionarCelda(fila ,col++ , pla.getNumpla ()             , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getPlaveh ()             , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getNumrem ()             , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getFecdsp ()             , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getNomcon ()            , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getNomcliente ()         , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getNomori ()             , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getNomdest ()            , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getStatus_220 ()         , texto );
                    
                    xls.adicionarCelda(fila ,col++ , pla.getGroup_code ()         , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getFeccum ()             , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getCedcon ()             , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getClientes ()           , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getOripla ()             , texto );
                    xls.adicionarCelda(fila ,col++ , pla.getDespla ()             , texto );
                }
                xls.cerrarLibro();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "Proceso Exitoso!");
            }else {
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "No se encontraron Planillas En MIMS...");
            }
            model.cxpDocService.setEnproceso();
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
                model.cxpDocService.setEnproceso();
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
