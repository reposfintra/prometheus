/*
 * MigracionFacturasClientesThread.java
 *
 * Created on 25 de agosto de 2006, 02:56 PM
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.Corrida;
import com.tsp.operation.model.beans.Plarem;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import com.tsp.util.LogWriter;
import com.tsp.util.*;
/**
 *
 * @author  ALVARO
 */
public class MigracionFacturasClientesThread extends Thread {
     Model model;
    Vector datos;
    Usuario usuario;
    SimpleDateFormat fmt;    
    String fechaInicio;
    String fechaFinal;
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, letra, numero, porcentaje, letraCentrada, numeroNegrita,porcentajew, letraCentradaw,letrawarning ;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris, cRojo ;
    private PrintWriter pw;
    private LogWriter logWriter;
    /** Creates a new instance of MigracionFacturasClientesThread */
    public MigracionFacturasClientesThread(String fechainicio,String fechafinal,Model model, Usuario usuario)  throws Exception{
        this.fechaInicio=fechainicio;
        this.fechaFinal=fechafinal;
        this.usuario = usuario;
        this.model   = model;
        
        
        java.util.Date ahora = new java.util.Date();
        java.text.SimpleDateFormat fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh.mm.ss");
        String fechaTimestamp = fechaHora.format(ahora);
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta");
        //Creaci�n del archivo log del usuario
        File file = new File( ruta + "/exportar/migracion/" + usuario.getLogin() );
        file.mkdirs();
         
        pw = new PrintWriter( System.err, true );        
        String logFile = ruta +"/exportar/migracion/" + usuario.getLogin() + "/MigracionFacturasClientes-" + fechaTimestamp + ".log";
        pw = new PrintWriter( new FileWriter( logFile, true ), true );
        logWriter = new LogWriter( "MigracionFacturasClientes", LogWriter.INFO, pw );
        logWriter.setPrintWriter( pw );
         
        
        super.start();
    }
    public synchronized void run(){
        try{
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  PROCESO DE MIGRACION DE FACTURAS CLI MIMS - POSTGRES   ",LogWriter.INFO);
            logWriter.log("*  Usuario  :  "+usuario.getLogin()+"                     ",LogWriter.INFO);
            logWriter.log("*  Fecha    :  "+Util.getFechaActual_String(6)+"          ",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            model.LogProcesosSvc.InsertProceso("MigracionFacturasClientes",this.hashCode(), "Iniciado", usuario.getLogin());
            logWriter.log("INICIO DEL PROCESO ",LogWriter.INFO);
            
            
           /* 
            // creacion del diractorio del usuario
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );*/
            
            if(model.mfcs.buscarFacturasClientesCabeceraOracle(fechaInicio,fechaFinal)){
                logWriter.log( "Consulta de las facturas cabecera clientes de mims arrojo: "+model.mfcs.getVFacturaCabecera().size(), LogWriter.INFO);
                
                if(model.mfcs.buscarFacturasClientesDetallesOracle()){
                    logWriter.log( "Consulta de las facturas detalle clientes de mims arrojo: "+model.mfcs.getVFacturaDetalle().size(), LogWriter.INFO );
                
                    if(model.mfcs.ingresarFacturaCabecera(usuario.getLogin(),usuario.getDstrct(),usuario.getBase())){
                        logWriter.log( "Ingreso con exito las facturas cabecera en postgres: "+model.mfcs.getVFacturaCabecera().size(), LogWriter.INFO );
                                        
                        if(model.mfcs. ingresarFacturaDetalle(usuario.getLogin(),usuario.getDstrct(),usuario.getBase())){
                            logWriter.log( "Ingreso con exito las facturas detalles en postgres: "+model.mfcs.getVFacturaDetalle().size(), LogWriter.INFO );
                        
                            model.LogProcesosSvc.finallyProceso("MigracionFacturasClientes", this.hashCode(), usuario.getLogin(), "Exitoso");    
                        }
                        else{
                            logWriter.log( "Error al insertar los detalles de facturas clientes a postgres  ", LogWriter.ERROR );
                
                            model.LogProcesosSvc.finallyProceso("MigracionFacturasClientes", this.hashCode(), usuario.getLogin(), "Error al insertar los detalles de facturas clientes a postgres");
                        }
                    }
                    else{
                        logWriter.log( "Error al insertar las cabeceras de facturas clientes a postgres  ", LogWriter.ERROR );
                
                        model.LogProcesosSvc.finallyProceso("MigracionFacturasClientes", this.hashCode(), usuario.getLogin(), "Error al insertar las cabeceras de facturas clientes a postgres");
                    }
                    
                    
                }
                else{
                    logWriter.log( "Error en la consulta de las facturas detalles clientes de mims  ", LogWriter.ERROR );
                
                    model.LogProcesosSvc.finallyProceso("MigracionFacturasClientes", this.hashCode(), usuario.getLogin(), "No se pudo generar el archivo, error en la consulta de las facturas detalles clientes de mims");
                }
            }
            else{
                logWriter.log( "Error en la consulta de las facturas cabecera clientes de mims  ", LogWriter.ERROR );
                
                model.LogProcesosSvc.finallyProceso("MigracionFacturasClientes", this.hashCode(), usuario.getLogin(), "No se pudo generar el archivo, error en la consulta de las facturas cabecera clientes de mims");
            }
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            model.LogProcesosSvc.finallyProceso("MigracionFacturasClientes", this.hashCode(), usuario.getLogin(), "Proceso Exitoso!");
                        
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso("MigracionFacturasClientes", this.hashCode(), usuario.getLogin(),  ex.getMessage());
            } catch (Exception e){}            
        }
        
    }
    
    
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);
            cRojo       = xls.obtenerColor(255,0,0);
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAzul.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo3      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo4      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            letra        = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada= xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero       = xls.nuevoEstilo("Tahoma", 8 , false , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 8 , true  , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            porcentaje   = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            porcentaje  = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada = xls.nuevoEstilo("Tahoma", 8 , false , false, "text" , xls.NONE , xls.NONE , xls.NONE);
            letraCentradaw = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            letrawarning = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cRojo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
    }    
    
    

    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            xls.adicionarCelda(2,0, "USUARIO", titulo1);
            xls.adicionarCelda(2,1, usuario.getLogin() , titulo1);
            xls.adicionarCelda(3,0, "CEDULA" , titulo1);
            xls.adicionarCelda(3,1, usuario.getCedula(), titulo1);
            xls.adicionarCelda(4,0, "RANGO DE FECHAS:", titulo1);
            xls.adicionarCelda(4,1, "("+model.PorcentajeSvc.getFecha1()+" / "+model.PorcentajeSvc.getFecha2()+")" , titulo1);
                        
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
 /**
     * CompararRemesas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void CompararFacturas() throws Exception {
        try{
             int fila = 6;
            fila++;
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("MIGRACION_FACTURAS_MIMS_POSTGRES_" + fmt.format( new Date() ) +".txt", "MIGRACION_FACTURAS_MIMS_POSTGRES");
            
            
            String [] cabecera = { "Postgres Planilla", "Postgres Remesa", "Postgres %", "Mims Planilla", "Mims Remesa", "Mims %", "Igual?"};
            short  [] dimensiones = new short [] { 5000, 5000, 3000, 5000 , 5000, 3000,3000};
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                xls.cambiarAnchoColumna(i, dimensiones[i] );
            }
            Vector VPostgres;
            Vector VOracle;
            VPostgres = (Vector)model.PorcentajeSvc.getVPostgres().clone();
            ////System.out.println("VPostgres:"+VPostgres.size());
            VOracle = (Vector)model.PorcentajeSvc.getVOracle().clone();
            ////System.out.println("VOracle:"+VOracle.size());
            
            int j=0,l=0;
            
            Vector Postgresfinal= new Vector();
            Vector Oraclefinal= new Vector();
            
             ////System.out.println("Bloque para conseguir el vector de postgres");
            /*Bloque para conseguir el vector de postgres*/
            for(int i=j;i<VPostgres.size();i++){
                Vector v1= new Vector();
                Vector v2= new Vector();
                
                Plarem p1 = (Plarem)VPostgres.get(i);
                
                /*postgres...*/
                for(j=i;j<VPostgres.size();j++){
                    Plarem p2 = (Plarem)VPostgres.get(j);
                    if(p1.getNumpla().equals(p2.getNumpla())){
                        v1.add(p2);
                        i=j;
                        ////System.out.println("vector1:"+p2.getNumpla()+" "+p2.getNumrem());
                    }
                    else{
                        i=j-1;
                        break;
                    }
                }
                /*fin postgres*/
                /*oracle*/
                for(int k=l;k<VOracle.size();k++){
                    ////System.out.println("entro");
                    Plarem p3 = (Plarem)VOracle.get(k);
                    if(p1.getNumpla().equals(p3.getNumpla())){
                        v2.add(p3);
                        ////System.out.println("vector2:"+p3.getNumpla()+" "+p3.getNumrem());
                        l=k;
                        if(k+1==VOracle.size()){
                           l=k+1;   
                        }
                    }
                    else{
                        l=k;
                        break;
                    }
                }
              
                /*fin oracle*/
                /*comienza la */
                Vector guardados=new Vector();
                String pla="";
                for(int g=0;g<v1.size();g++){
                    Plarem p4 = (Plarem)v1.get(g);
                    pla= p4.getNumpla();
                        int r=0;
                        for(int f=0;f<v2.size();f++){
                            Plarem p5 = (Plarem)v2.get(f);
                            if((p4.getNumpla().equals(p5.getNumpla()))&&(p4.getNumrem().equals(p5.getNumrem()))){
                                r++;
                                v2.remove(f);
                            }                
                        }
                        if(r>0){
                            Postgresfinal.add(p4);    
                        }
                        else{
                            guardados.add(p4);
                        }
                }
                for(int w=0;w<guardados.size();w++){
                    Plarem p6 = (Plarem)guardados.get(w);
                    Postgresfinal.add(p6);
                }
                /*for(int f=0;f<v2.size();f++){
                    Plarem q = new Plarem();
                    q.setNumpla(pla);
                    q.setNumrem("");
                    q.setPorcent("");
                    Postgresfinal.add(q);
                }*/
            }
            for(int f=l;f<VOracle.size();f++){
                    Plarem qq = (Plarem)VOracle.get(f);;
                    Plarem q = new Plarem();
                    q.setNumpla(qq.getNumpla());
                    q.setNumrem("");
                    q.setPorcent("");
                    Postgresfinal.add(q);
            }
            /*fin bloque de postgres*/
            
            j=0;
            l=0;
            ////System.out.println("Bloque para conseguir el vector de oracle");
            
            /*Bloque para conseguir el vector de oracle*/
            for(int i=j;i<VOracle.size();i++){
                Vector v1= new Vector();
                Vector v2= new Vector();
                
                Plarem p1 = (Plarem)VOracle.get(i);
                
                /*postgres...*/
                for(j=i;j<VOracle.size();j++){
                    Plarem p2 = (Plarem)VOracle.get(j);
                    if(p1.getNumpla().equals(p2.getNumpla())){
                        v1.add(p2);
                        i=j;
                        ////System.out.println("vector1:"+p2.getNumpla()+" "+p2.getNumrem());
                    }
                    else{
                        i=j-1;
                        break;
                    }
                }
                /*fin postgres*/
                /*oracle*/
                for(int k=l;k<VPostgres.size();k++){
                    ////System.out.println("entro");
                    Plarem p3 = (Plarem)VPostgres.get(k);
                    if(p1.getNumpla().equals(p3.getNumpla())){
                        v2.add(p3);
                        ////System.out.println("vector2:"+p3.getNumpla()+" "+p3.getNumrem());
                        l=k;
                        if(k+1==VPostgres.size()){
                            l=k+1;    
                        }
                    }
                    else{
                        l=k;
                        if(k+1==VPostgres.size()){
                            l=k+1;    
                        }
                        break;
                    }
                }
                ////System.out.println("******fin oracle **********");
                /*fin oracle*/
                /*comienza la */
                Vector guardados=new Vector();
                String pla="";
                for(int g=0;g<v1.size();g++){
                    Plarem p4 = (Plarem)v1.get(g);
                    pla=p4.getNumpla();
                        int r=0;
                        for(int f=0;f<v2.size();f++){
                            Plarem p5 = (Plarem)v2.get(f);
                            if((p4.getNumpla().equals(p5.getNumpla()))&&(p4.getNumrem().equals(p5.getNumrem()))){
                                r++;
                                v2.remove(f);
                            }                
                        }
                        if(r>0){
                            Oraclefinal.add(p4);    
                        }
                        else{
                            guardados.add(p4);
                        }
                }
                for(int f=0;f<v2.size();f++){
                    Plarem q = new Plarem();
                    q.setNumpla(pla);
                    q.setNumrem("");
                    q.setPorcent("");
                    Oraclefinal.add(q);
                }
                /*for(int w=0;w<guardados.size();w++){
                    Plarem p6 = (Plarem)guardados.get(w);
                    Oraclefinal.add(p6);
                }*/
                
            }
            
            ////System.out.println("");
            for(int f=Oraclefinal.size();f<Postgresfinal.size();f++){
                    Plarem qq = (Plarem)Postgresfinal.get(f);;
                    Plarem q = new Plarem();
                    q.setNumpla(qq.getNumpla());
                    q.setNumrem("");
                    q.setPorcent("");
                    Oraclefinal.add(q);
            }
            /*fin bloque de postgres*/
            ////System.out.println("----------------- tama�o post:"+Postgresfinal.size());
            for(int z3=0;z3<Postgresfinal.size();z3++){
                 Plarem po = (Plarem)Postgresfinal.get(z3); 
                
                 ////System.out.println(""+po.getNumpla()+" "+po.getNumrem());
                 
            }
            ////System.out.println("----------------- tama�o Orac:"+Oraclefinal.size());
            for(int z3=0;z3<Oraclefinal.size();z3++){
                
                 Plarem pa = (Plarem)Oraclefinal.get(z3); 
                 ////System.out.println(""+pa.getNumpla()+" "+pa.getNumrem());
                 
            }
            ////System.out.println("*******************************************");
            /*Para sacar las que estan ok!!!*/
            Vector vok=new Vector();
            j=0;
            l=0;
           
            /*for(int i=j;i<Postgresfinal.size();i++){
                Vector v1= new Vector();
                Vector v2= new Vector();
                ////System.out.println("bandera1");
                Plarem p1 = (Plarem)Postgresfinal.get(i);
                ////System.out.println("bandera2");
                for(j=i;j<Postgresfinal.size();j++){
                    Plarem p2 = (Plarem)Postgresfinal.get(j);
                    if(p1.getNumpla().equals(p2.getNumpla())){
                        v1.add(p2);
                        i=j;
                        ////System.out.println("vector1:"+p2.getNumpla()+" "+p2.getNumrem());
                    }
                    else{
                        i=j-1;
                        break;
                    }
                }
               ////System.out.println("bandera3");
                for(int k=l;k<Oraclefinal.size();k++){
                    Plarem p3 = (Plarem)Oraclefinal.get(k);
                    if(p1.getNumpla().equals(p3.getNumpla())){
                        v2.add(p3);
                        ////System.out.println("vector2:"+p3.getNumpla()+" "+p3.getNumrem());
                       
                    }
                    else{
                        l=k;
                        break;
                    }
                }
               ////System.out.println("bandera4");
                int ok=0;
                String p1anilla="";
              
                for(int g=0;g<v1.size();g++){
                    Plarem p4 = (Plarem)v1.get(g);
                    Plarem p5 = (Plarem)v2.get(g);
                            if(((p4.getNumpla().equals(p5.getNumpla()))&&(p4.getNumrem().equals(p5.getNumrem()))&&(p4.getPorcent().equals(p5. getPorcent())))){
                                ok=0;
                                p1anilla=p4.getNumpla();
                                ////System.out.println("Son iguales!!!");
                            }
                            else{
                                ////System.out.println("Encontro diferente!!!");
                                p1anilla="";
                                ok=1;
                                break;
                            }
                       
                }
                ////System.out.println("bandera5");
                if(ok==0){
                    vok.add(p1anilla);
                }
                ////System.out.println("bandera6");
            }*/
            
            /*////System.out.println("elimina...");
            for(int z3=vok.size()-1;z3>=0;z3--){
                for(int z4=Postgresfinal.size()-1;z4>=0;z4--){
                Plarem po = (Plarem)Postgresfinal.get(z4); 
                    if(po.getNumpla().equals((String)vok.get(z3))){
                        ////System.out.println("elimino de post");
                        Postgresfinal.remove(z4);
                    }  
                }    
                for(int z4=Oraclefinal.size()-1;z4>=0;z4--){
                Plarem po = (Plarem)Oraclefinal.get(z4); 
                    if(po.getNumpla().equals((String)vok.get(z3))){
                        ////System.out.println("elimino de oracle");
                        Oraclefinal.remove(z4);
                    }  
                }  
            }*/
          
                                
          ////System.out.println("***********CREACION EXCEL************");
           
            /*fin ok*/
            
            /*for(int z3=0;z3<Postgresfinal.size();z3++){
                 Plarem po = (Plarem)Postgresfinal.get(z3); 
                 Plarem pa = (Plarem)Oraclefinal.get(z3);
                 if(po.getNumrem().equals("")){
                     po.setNumpla(""); 
                 }
                 if(pa.getNumrem().equals("")){
                     pa.setNumpla(""); 
                 }
                 
            }*/
           /*for(int z3=0;z3<Postgresfinal.size();z3++){
                 Plarem po = (Plarem)Postgresfinal.get(z3); 
                 Plarem pa = (Plarem)Oraclefinal.get(z3);
                 ////System.out.println(""+po.getNumpla()+" "+po.getNumrem()+"/ "+pa.getNumpla()+" "+pa.getNumrem());
                 
           }   */  
           fila++;
            for ( int i = 0; i<Postgresfinal.size(); i++, fila++){
                
                Plarem p1 = (Plarem)Postgresfinal.get(i);
                Plarem p2 = (Plarem)Oraclefinal.get(i);
          
                
                xls.adicionarCelda(fila  , 0 , (p1.getNumrem()!="")?p1.getNumpla():"", (p1.getNumrem()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 1 , p1.getNumrem(), (p1.getNumrem()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 2 , p1.getPorcent(), (p1.getNumrem()!="")?porcentaje:letraCentradaw);
                xls.adicionarCelda(fila  , 3 , (p2.getNumrem()!="")?p2.getNumpla():"", (p2.getNumrem()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 4 , p2.getNumrem(), (p2.getNumrem()!="")?letraCentrada:letraCentradaw);
                xls.adicionarCelda(fila  , 5 , p2.getPorcent(), (p2.getNumrem()!="")?porcentaje:letraCentradaw); 
            
                xls.adicionarCelda(fila  , 6 ,(p1.getPorcent().equals(p2.getPorcent()))?"Si":"No" ,(p1.getPorcent().equals(p2.getPorcent()))?letraCentrada:letrawarning); 
             
            }       
            this.cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en ExportarTransferencias ...\n" + ex.getMessage() );
        }
    }
    /*public static void main(String a []){
        ExportarPorcentajes b = new ExportarPorcentajes("33","33");
        try{
            b.CompararRemesas();
        }catch(Exception e){
            e.printStackTrace();
        }
       
    }*/
}