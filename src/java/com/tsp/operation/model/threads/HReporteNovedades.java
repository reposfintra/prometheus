/***************************************************************************
 * Nombre clase : ............... HReporteNovedades.java                  *
 * Descripcion :................. Hilo que permite generar un reporte de   *
 *                                informacion de las novedades     *
 * Autor :....................... Ing. Karen Reales                        *
 * Fecha :........................ 29 de Enero de 2007, 08:38   AM         *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/

package com.tsp.operation.model.threads;
import java.util.*;
import java.io.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import java.sql.*;

public class HReporteNovedades extends Thread{
    
    private Vector reporte;
    private String user;
    private String mes;
    private Model model;
    
    /** Creates a new instance of ReporteRegistroTiempoXLS */
    public HReporteNovedades() {
    }
    
    public void start( Vector vec, String user, String mes) {
        this.reporte=vec;
        this.mes= mes;
        this.user = user;
        model = new Model();
        super.start();
    }
    
    public synchronized void run(){
        
        try{
            model.LogProcesosSvc.InsertProceso("Generacion Reporte Novedades LG", this.hashCode(), "Inicio de reporte Novedades LG" ,this.user);
            
            //  //System.out.println("Inicio...");
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta") + "/exportar/migracion";
            
            String ruta = path + "/" + user + "/";
            String nombreMes="";
            if(mes.equals("01")){
                nombreMes="ENERO";
            }
            if(mes.equals("02")){
                nombreMes="FEBRERO";
            }
            if(mes.equals("03")){
                nombreMes="MARZO";
            }
            if(mes.equals("04")){
                nombreMes="ABRIL";
            }
            if(mes.equals("05")){
                nombreMes="MAYO";
            }
            if(mes.equals("06")){
                nombreMes="JUNIO";
            }
            if(mes.equals("07")){
                nombreMes="JULIO";
            }
            if(mes.equals("08")){
                nombreMes="AGOSTO";
            }
            if(mes.equals("09")){
                nombreMes="SEPTIEMBRE";
            }
            if(mes.equals("10")){
                nombreMes="OCTUBRE";
            }
            if(mes.equals("11")){
                nombreMes="NOVIEMBRE";
            }
            if(mes.equals("12")){
                nombreMes="DICIEMBRE";
            }
            
            String nombreArch= "ReporteNovedadesLg"+nombreMes+".xls";
            String       Hoja  = "Novedades";
            String       Ruta  =ruta+ nombreArch;
            
            
            // Definicion de Estilos para la hoja de excel
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( Ruta);
            
            HSSFCellStyle fecha   = xls.nuevoEstilo("Verdana", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER,1 );
            HSSFCellStyle texto   = xls.nuevoEstilo("Verdana", 12 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle texto3  = xls.nuevoEstilo("Verdana", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle texto2  = xls.nuevoEstilo("Verdana", 9 ,  true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle total   = xls.nuevoEstilo("Verdana", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT,1 );
            HSSFCellStyle titulo2 = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.BLACK.index, HSSFColor.YELLOW.index, HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle titulo1 = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.WHITE.index, HSSFColor.BLUE.index, HSSFCellStyle.ALIGN_CENTER,2);
            HSSFCellStyle numero  = xls.nuevoEstilo("Verdana", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT,1);
            HSSFCellStyle titulo  = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle textoIzq  = xls.nuevoEstilo("Verdana", 9 ,  false    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT,1);
            xls.obtenerHoja("Novedades");
            
            int fila = 1;
            int col  = 0;
            xls.combinarCeldas(fila, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO S.A"             , texto  );
            xls.combinarCeldas(2, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "LG Electronics Colombia Ltda."             , texto  );
            xls.combinarCeldas(3, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , nombreMes           , texto  );
            
            fila++;
            xls.combinarCeldas(fila, 0, fila, 3);
            xls.adicionarCelda(fila,0 , "TRANSPORTADORA" , titulo1 );
            xls.adicionarCelda(fila,1 , "" , titulo1 );
            xls.adicionarCelda(fila,2 , "" , titulo1 );
            xls.adicionarCelda(fila,3 , "" , titulo1 );
            
            xls.combinarCeldas(fila, 4, fila, 17);
            xls.adicionarCelda(fila,4 , "INFORMACION DE LA FACTURA Y DESPACHO" , titulo1 );
            xls.adicionarCelda(fila,5 , "" , titulo1 );
            xls.adicionarCelda(fila,6 , "" , titulo1 );
            xls.adicionarCelda(fila,7 , "" , titulo1 );
            xls.adicionarCelda(fila,8 , "" , titulo1 );
            xls.adicionarCelda(fila,9 , "" , titulo1 );
            xls.adicionarCelda(fila,10 , "" , titulo1 );
            xls.adicionarCelda(fila,11 , "" , titulo1 );
            xls.adicionarCelda(fila,12 , "" , titulo1 );
            xls.adicionarCelda(fila,13, "" , titulo1 );
            xls.adicionarCelda(fila,14, "" , titulo1 );
            xls.adicionarCelda(fila,15, "" , titulo1 );
            xls.adicionarCelda(fila,16, "" , titulo1 );
            xls.adicionarCelda(fila,17, "" , titulo1 );
            
            
            xls.combinarCeldas(fila, 18, fila, 23);
            xls.adicionarCelda(fila,18, "FACTURAS CON DEVOLUCION" , titulo1 );
            xls.adicionarCelda(fila,19, "" , titulo1 );
            xls.adicionarCelda(fila,20, "" , titulo1 );
            xls.adicionarCelda(fila,21, "" , titulo1 );
            xls.adicionarCelda(fila,22, "" , titulo1 );
            xls.adicionarCelda(fila,23, "" , titulo1 );
            
            xls.combinarCeldas(fila, 23, fila, 26);
            xls.adicionarCelda(fila,23 , "INFORMACION DE LA TRANSPORTADORA" , titulo1 );
            xls.adicionarCelda(fila,24, "" , titulo1 );
            xls.adicionarCelda(fila,25, "" , titulo1 );
            xls.adicionarCelda(fila,26, "" , titulo1 );
            
            
            xls.combinarCeldas(fila, 27, fila, 32);
            xls.adicionarCelda(fila,27 , "DATOS DE  SALIDA DE ALMACENADORA" , titulo1 );
            xls.adicionarCelda(fila,28, "" , titulo1 );
            xls.adicionarCelda(fila,29, "" , titulo1 );
            xls.adicionarCelda(fila,30, "" , titulo1 );
            xls.adicionarCelda(fila,31, "" , titulo1 );
            xls.adicionarCelda(fila,32, "" , titulo1 );
            xls.adicionarCelda(fila,33, "" , titulo1 );
            
            xls.combinarCeldas(fila, 33, fila, 38);
            xls.adicionarCelda(fila,34, "SOLUCION A LA NOVEDAD" , titulo1 );
            xls.adicionarCelda(fila,35, "" , titulo1 );
            xls.adicionarCelda(fila,36, "" , titulo1 );
            xls.adicionarCelda(fila,37, "" , titulo1 );
            xls.adicionarCelda(fila,38, "" , titulo1 );
            
            
            xls.cambiarAnchoColumna(0, 4000);
            xls.cambiarAnchoColumna(1, 4000);
            for(int i =2; i<=39;i++){
                xls.cambiarAnchoColumna(i, 6000);
            }
            
            fila++;
            


            xls.adicionarCelda(fila,0 , "FECHA DE NOVEDAD (AAAA-MM-DD)" , titulo2 );
            xls.adicionarCelda(fila,1 , "TRANSPORTADORA" , titulo2 );
            xls.adicionarCelda(fila,2 , "CIUDAD DONDE ESTA FISICA LA MERCANCIA" , titulo2 );
            xls.adicionarCelda(fila,3 , "REMESA" , titulo2 );
            xls.adicionarCelda(fila,4 , "DESP" , titulo2 );
            xls.adicionarCelda(fila,5 , "ORDER_NO" , titulo2 );
            xls.adicionarCelda(fila,6 , "FACTURA" , titulo2 );
            xls.adicionarCelda(fila,7 , "FECHA FACT" , titulo2 );
            xls.adicionarCelda(fila,8 , "PED VEND" , titulo2 );
            xls.adicionarCelda(fila,9 , "DESTINATARIO" , titulo2 );
            xls.adicionarCelda(fila,10 , "PRODUCTO" , titulo2 );
            xls.adicionarCelda(fila,11 , "QTY FAC" , titulo2 );
            xls.adicionarCelda(fila,12 , "DIRECCION DE ENTREGA" , titulo2 );
            xls.adicionarCelda(fila,13 , "CIUDAD DESTINO" , titulo2 );
            xls.adicionarCelda(fila,14 , "OBSERVACIONES DEL DESPACHO" , titulo2 );
            xls.adicionarCelda(fila,15 , "ALMACENADORA ORIGEN" , titulo2 );
            xls.adicionarCelda(fila,16 , "ORDEN  DE COMPRA" , titulo2 );
            xls.adicionarCelda(fila,17, "FECHA VENC O DE COMPRA" , titulo2 );
            xls.adicionarCelda(fila,18 , "GR" , titulo2 );
            xls.adicionarCelda(fila,19 , "FECHA GR" , titulo2 );
            xls.adicionarCelda(fila,20 , "BOG APLICADA" , titulo2 );
            xls.adicionarCelda(fila,21 , "QTY" , titulo2 );
            xls.adicionarCelda(fila,22 , "STATUS" , titulo2 );
            xls.adicionarCelda(fila,23 , "REMESA" , titulo2 );
            xls.adicionarCelda(fila,24 , "No DE NOVEDAD" , titulo2 );
            xls.adicionarCelda(fila,25 , "CANT NOVEDAD" , titulo2 );
            xls.adicionarCelda(fila,26 , "MOTIVO NOVEDAD" , titulo2 );
            xls.adicionarCelda(fila,27, "FECHA SALIDA BOG" , titulo2 );
            xls.adicionarCelda(fila,28 , "TRANSPORTE" , titulo2 );
            xls.adicionarCelda(fila,29 , "CMM" , titulo2 );
            xls.adicionarCelda(fila,30 , "CANT DESP" , titulo2 );
            xls.adicionarCelda(fila,31 , "FECHA DE ENTREGA" , titulo2 );
            xls.adicionarCelda(fila,32 , "CANTIDAD ENTREGADA" , titulo2 );
            xls.adicionarCelda(fila,33 , "FECHA SOLUCION (AAAA-MM-DD)" , titulo2 );
            xls.adicionarCelda(fila,34 , "SOLUCION" , titulo2 );
            xls.adicionarCelda(fila,35 , "COD DE SOLUCION" , titulo2 );
            xls.adicionarCelda(fila,36 , "NUEVA FACT" , titulo2 );
            xls.adicionarCelda(fila,37 , "FECHA DE INFORME DE LA SOLUCION A LA TRANSPORTADORA" , titulo2 );
            xls.adicionarCelda(fila,38 , "FECHA DE INGRESO DE LA DEV A ALMAVIVA" , titulo2 );
            
            
           for(int i =0; i<reporte.size();i++){
                Instrucciones_Despacho inst = (Instrucciones_Despacho) reporte.elementAt(i);
                fila++;
                
                xls.adicionarCelda(fila,0 , inst.getFecha_novedad() , numero );
                xls.adicionarCelda(fila,1 , "Transportes Sanchez Polo", texto3 );
                xls.adicionarCelda(fila,2 , inst.getUbicacion_mercancia() , texto3 );
                xls.adicionarCelda(fila,3 , inst.getNumrem() , texto3 );
                xls.adicionarCelda(fila,4 , inst.getDesp() , texto3 );
                xls.adicionarCelda(fila,5 , inst.getOrder_no(), texto3 );
                xls.adicionarCelda(fila,6 , inst.getFactura() , textoIzq );
                xls.adicionarCelda(fila,7 , inst.getFecha_factura() , texto3 );
                xls.adicionarCelda(fila,8 , "", texto3 );
                xls.adicionarCelda(fila,9 , inst.getShip_to_name() , texto3 );
                xls.adicionarCelda(fila,10 , inst.getModelo(), numero );
                xls.adicionarCelda(fila,11 , "" , numero );
                xls.adicionarCelda(fila,12 ,inst.getAddr1() , texto3 );
                xls.adicionarCelda(fila,13 , inst.getCiudad() , texto3 );
                xls.adicionarCelda(fila,14 , inst.getObservacion(), texto3 );
                xls.adicionarCelda(fila,15 , inst.getBodega() , texto3 );
                xls.adicionarCelda(fila,16 , "" , texto3 );
                xls.adicionarCelda(fila,17, "" , textoIzq );
                xls.adicionarCelda(fila,18 , "" , textoIzq );
                xls.adicionarCelda(fila,19 , "", texto3 );
                xls.adicionarCelda(fila,20 , "" , texto3 );
                xls.adicionarCelda(fila,21 , "", texto3 );
                xls.adicionarCelda(fila,22 , "" , texto3 );
                xls.adicionarCelda(fila,23 , inst.getNumrem(), texto3 );
                xls.adicionarCelda(fila,24 , inst.getCodigo_novedad() , texto3 );
                xls.adicionarCelda(fila,25 , inst.getCantida_novedad() , texto3 );
                xls.adicionarCelda(fila,26 , inst.getMotivo_novedad(), texto3 );
                xls.adicionarCelda(fila,27, "", texto3 );
                xls.adicionarCelda(fila,28 , "" , texto3 );
                xls.adicionarCelda(fila,29 ,"" , texto3 );
                xls.adicionarCelda(fila,30 , "", texto3 );
                xls.adicionarCelda(fila,31 , "", texto3 );
                xls.adicionarCelda(fila,32 , "" , texto3 );
                xls.adicionarCelda(fila,33 , inst.getFecha_solucion(), texto3 );
                xls.adicionarCelda(fila,34 , inst.getNom_solucion_novedad(), texto3 );
                xls.adicionarCelda(fila,35 , inst.getSolucion_novedad(), numero );
                xls.adicionarCelda(fila,36 , "" , texto3 );
                xls.adicionarCelda(fila,37 ,inst.getFecha_solucion() , texto3 );
                xls.adicionarCelda(fila,38 , "" , texto3 );
                
            }
            
            
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso( "Generacion Reporte Novedades LG", this.hashCode(), user, "PROCESO FINALIZADO CON EXITO!" );
        }catch(Exception e){
            //System.out.println("Hay un error: "+e.getMessage());
            
            try{
                
                model.LogProcesosSvc.finallyProceso( "Generacion Reporte Novedades LG", this.hashCode(), user, "ERROR : " + e.getMessage() );
                
            } catch( Exception f ){
                
                f.printStackTrace();
                
                try{
                    
                    model.LogProcesosSvc.finallyProceso( "Generacion Reporte Novedades LG",this.hashCode(), user, "ERROR : " + f.getMessage() );
                    
                } catch( Exception p ){ p.printStackTrace(); }
                
            }
        }
    }
    
    public static void main(String a [])throws SQLException{
        HReporteNovedades hilo = new HReporteNovedades();
        Model model = new Model();
        String fecha = "200701";
        String condicion="";
        model.instrucciones_despachoService.reporteNovedad(condicion,fecha,"01");
        hilo.start(model.instrucciones_despachoService.getLista(), "KREALES", "01");
    }
}
