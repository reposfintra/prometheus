/**************************************************************************
 * Nombre clase: HListarPrefacturaTercm.java                                     *
 * Descripci�n: Clase que envia los datos a excel                          *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 1 de octubre de 2005, 08:20 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/
 
package com.tsp.operation.model.threads;

import com.tsp.util.*;
/**
 *
 * @author  Igomez
 */


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;

import java.util.*;
import java.lang.*;
import java.io.*;


public class HListarPrefacturaTercm extends Thread{

   Model    model;
   Usuario  usuario;
   String   NumPrefactura;
   //Log Procesos
   private String procesoName;
   private String des;
   
    public void init(Model modelo, Usuario usu, String numerFactura){
        this.model         = modelo;
        this.usuario       = usu;
        this.NumPrefactura = numerFactura;
        this.procesoName = "Prefactura Tercm";
        this.des = "Prefactura Tercm";
        super.start();
    }
    
    
    public synchronized void run(){
        try{
            
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, usuario.getLogin());
            model.DescuentoTercmSvc.BuscarPreFactura(NumPrefactura);
            List lista = model.DescuentoTercmSvc.getPrefacturaCreada();
           //if (lista!=null && lista.size()>0){
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion";

                String ruta = path + "/" + usuario.getLogin() + "/";// + model.movplaService.obtenerFechaActualEnFormato("YYYYMM") ;

                File f = new File(ruta); 
                if ( !f.exists() ){
                    f.mkdirs();
                    ////System.out.println("directorios creados en: "+f.getAbsolutePath());
                }
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( ruta + "/Prefactura"+NumPrefactura+".xls");
                // Definicion de Estilos para la hoja de excel
               
                HSSFCellStyle fecha   = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
                HSSFCellStyle texto   = xls.nuevoEstilo("Book Antiqua", 14 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
                HSSFCellStyle texto3  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto2  = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle total   = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
                HSSFCellStyle titulo2 = xls.nuevoEstilo("Book Antiqua", 12 , true    , false, "text"       , HSSFColor.WHITE.index , HSSFColor.BLACK.index, HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle titulo  = xls.nuevoEstilo("Book Antiqua", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);

                xls.obtenerHoja("Prefactura");
                
                int fila = 1;
                int col  = 0;
                xls.adicionarCelda(fila++ ,col  , "TRANSPORTE SANCHEZ POLO" , texto );
                xls.combinarCeldas(fila, col, fila, col+5); 
                xls.adicionarCelda(fila++ ,col   , "Reporte Prefactura TERCM"   , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "Boleta #"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Fecha de Impresion"                 , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Placa"               , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Planilla"              , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Valor"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "#Prefactura"                   , titulo );
                
                Iterator it = lista.iterator();

                while(it.hasNext()){

                    fila++;
                    col = 0;
                     
                    DescuentoTercm tercm = (DescuentoTercm) it.next();
                    xls.adicionarCelda(fila ,col++ , tercm.getBoleta()  , texto3 );
                    xls.adicionarCelda(fila ,col++ , tercm.getFecha()   , fecha );
                    xls.adicionarCelda(fila ,col++ , tercm.getPlaca()   , texto3 );  
                    xls.adicionarCelda(fila ,col++ , tercm.getOC()      , texto3 );
                    xls.adicionarCelda(fila ,col++ ,  Double.parseDouble(tercm.getValorPost())      , numero );
                    xls.adicionarCelda(fila ,col++ , tercm.getPrefactura()  , texto3 );
                    
                }
                     
                
                xls.cerrarLibro();
                
            //}
             ////System.out.println("Listo..");
             model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){            
            ////System.out.println("Error : " + ex.getMessage());
           try{            
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),usuario.getLogin() ,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{               
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),usuario.getLogin(),"ERROR :");
                }catch(Exception p){    }
              } 
           }
        
    }
}
