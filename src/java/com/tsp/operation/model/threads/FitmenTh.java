/******************************************************************
* Nombre                        FitmenTh.java
* Descripci�n                   Clase Threads para la tabla fitmen
* Autor                         ricardo rosero
* Fecha                         10/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.ReporteCumplidoAction;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;

/**
 *
 * @author  rrosero
 */
public class FitmenTh extends Thread{
    //atributos
    private Vector informe;
    private Vector resumen;
    private String user;
    private String mensaje;
    private String fechai;
    private String fechaf;

    /** Creates a new instance of FitmenTh */
    public FitmenTh() {
    }
    
    /**
     * funcion que inicializa el hilo
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public void start(Vector informe, String user, String fechai, String fechaf){
            this.informe = informe;            
            this.user = user;
            this.fechaf = fechaf;
            this.fechai = fechai;
            
            super.start();
    }

    /**
     * funcion que ejecuta el hilo
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public synchronized void run(){
            try{
                    //Fecha del sistema             
                    Calendar FechaHoy = Calendar.getInstance();
                    Date d = FechaHoy.getTime();
                    SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
                    SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
                    SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");            
                    String FechaFormated = s.format(d);
                    String FechaFormated1 = s1.format(d);
                    String Fecha = fec.format(d);


                    //obtener cabecera de ruta
                    ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                    String path = rb.getString("ruta");
                    //armas la ruta
                    String Ruta1  = path + "/exportar/migracion/" + user + "/";
                    //crear la ruta
                    File file = new File(Ruta1);
                    file.mkdirs();

                    POIWrite xls = new POIWrite(Ruta1 +"ReporteFitmen_" + FechaFormated + ".xls", user, Fecha);
                    String nom_archivo = "ReporteFitmen_" + FechaFormated + ".xls";
                    this.mensaje = nom_archivo;

                    //Definici�n de Estilos
                    HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                    HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
                    HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);

                    HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 10, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                    HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                    HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 10, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                    HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
                    HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
                    HSSFCellStyle acpm_hd = xls.nuevoEstilo("Arial", 10, true , false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                    HSSFCellStyle texto3  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text", xls.NONE , xls.NONE , xls.NONE);
                    HSSFCellStyle celda = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text", xls.NONE , xls.NONE , xls.NONE);

                    header1.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
                    header1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    header1.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                    header2.setAlignment(HSSFCellStyle.ALIGN_CENTER);

                    acpm_hd.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                    acpm_hd.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
                    acpm_hd.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
                    acpm_hd.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
                    acpm_hd.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);

                    texto3.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
                    texto3.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
                    texto3.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
                    texto3.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
                    texto3.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
                    celda.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);


                    xls.obtenerHoja("Informe Fitmen");
                    xls.cambiarMagnificacion(3,4);

                    // cabecera

                    xls.combinarCeldas(2, 0, 2, 10);
                    xls.combinarCeldas(4, 0, 4, 10);
                    xls.combinarCeldas(6, 0, 6, 10);
                    xls.adicionarCelda(2, 0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
                    xls.adicionarCelda(4, 0, "Reporte de la Tabla Fitmen", header2);
                    xls.adicionarCelda(6, 0, "Reporte de fecha: " + Fecha , texto2);

                    //resumen
                    /*    
                    xls.adicionarCelda(0, 13, "RESUMEN ACPM", acpm_hd);
                    xls.combinarCeldas(0, 13, 0, 14);
                    xls.adicionarCelda(0, 15, "", celda);
                    */
                    // subtitulos


                    int fila = 8;
                    int col  = 0;

                    xls.adicionarCelda(fila, col++, "TRAILER", header1);
                    xls.adicionarCelda(fila, col++, "PLANILLA", header1);
                    xls.adicionarCelda(fila, col++, "CABEZOTE", header1);
                    xls.adicionarCelda(fila, col++, "FECHA-DESP", header1);
                    xls.adicionarCelda(fila, col++, "ORIGEN", header1);
                    xls.adicionarCelda(fila, col++, "DESTINO", header1);
                    xls.adicionarCelda(fila, col++, "AGENCIA-ORIGEN", header1);
                    xls.adicionarCelda(fila, col++, "AGENCIA-DEST", header1);
                    xls.adicionarCelda(fila, col++, "PLANILLA-ANT", header1);
                    xls.adicionarCelda(fila, col++, "FECHA-CUMPLIDO", header1);
                    xls.adicionarCelda(fila, col++, "FECHA-CUMPLIDO-ANT", header1);

                    fila = 9;
                    for(int i=0; i<informe.size(); i++){
                        Fitmen fit = (Fitmen) informe.elementAt(i);
                        col = 0;
                        xls.adicionarCelda(fila, col++, fit.getPlatlr(), texto);
                        xls.adicionarCelda(fila, col++, fit.getNumpla(), texto);
                        xls.adicionarCelda(fila, col++, fit.getPlaveh(), fecha);
                        xls.adicionarCelda(fila, col++, fit.getFecdsp(), texto);
                        xls.adicionarCelda(fila, col++, fit.getOripla(), texto);
                        xls.adicionarCelda(fila, col++, fit.getDespla(), texto);
                        xls.adicionarCelda(fila, col++, fit.getAgcplao(), texto);
                        xls.adicionarCelda(fila, col++, fit.getAgcplad(), texto);
                        xls.adicionarCelda(fila, col++, fit.getNumpla_ant(), texto);
                        xls.adicionarCelda(fila, col++, fit.getFeccum(), texto);
                        xls.adicionarCelda(fila, col++, fit.getFeccum_ant(), texto);
                        fila++;                                
                    }
                    /*
                    fila = 1;
                    for(int i=0; i<resumen.size(); i++){
                        Vector ln = (Vector) resumen.get(i);
                        String prov = (String) ln.get(0);
                        String ttl = (String) ln.get(1);
                        col = 13;
                        xls.adicionarCelda(fila, col++, prov, acpm_hd);
                        xls.adicionarCelda(fila, col++, ttl, texto3);
                        fila++;
                    }
                    */
                    // datos


                    xls.cerrarLibro();

            }catch (Exception ex){
                            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            }

    }

    /**
     * Getter for property mensaje.
     * @return Value of property mensaje.
     */
    public java.lang.String getMensaje() {
        return mensaje;
    }

    /**
     * Setter for property mensaje.
     * @param mensaje New value of property mensaje.
     */
    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }


}
