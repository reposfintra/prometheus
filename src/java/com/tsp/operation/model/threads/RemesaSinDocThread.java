/***************************************************************************
 * Nombre clase : ............... RemesaSinDocThread.java                  *
 * Descripcion :................. Hilo que permite la creacion del reporte *
 *                                de viajes en un archivo en excel         *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 18 de noviembre de 2005, 10:36 AM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;

public class RemesaSinDocThread extends Thread{
    private List Lista;
    private String fechainicio;
    private String fechafin;
    private String id;
    
    /** Creates a new instance of RemesaSinDocThread */
    public RemesaSinDocThread() {
    }
    
    public void start(List Lista, String fechai, String fechaf, String id) {
        this.Lista = Lista;
        this.fechainicio = fechai;
        this.fechafin = fechaf;
        this.id = id;
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            
            File file = new File(path + "/exportar/migracion/"+id);
            file.mkdirs();
            
            String nombreArch= "RemesasSinDocumentos["+fechainicio+"]["+fechafin+"].xls";
            String       Hoja  = "RemesaSinDocumentos";
            String       Ruta  = path + "/exportar/migracion/"+ id +"/" + nombreArch;
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x1));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x0));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setDataFormat(wb.createDataFormat().getFormat("####-##-## ##:##:##"));
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente4);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo6.setDataFormat(wb.createDataFormat().getFormat("###,##"));
            
            
            /****************************************************/
            row  = sheet.createRow((short)(0));
            
            for (int j=0;j<10;j++) {
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
            }
            
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            
            sheet.createFreezePane(0,5);
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO");
            
            
            row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("Reporte de remesas sin documentos");
            
            /*************************************************************************************/
            int Fila = 4;
            
            row  = sheet.createRow((short)(Fila));
            
            sheet.setColumnWidth((short)0, (short)3000 );
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("REMESA");
            
            sheet.setColumnWidth((short)1, (short)7000 );
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA REMESA");
            
            sheet.setColumnWidth((short)2, (short)5000 );
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CLIENTE");
            
            sheet.setColumnWidth((short)3, (short)6000 );
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("ORIGEN");
            
            sheet.setColumnWidth((short)4, (short)6000 );
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESTINO");
            
            sheet.setColumnWidth((short)5, (short)5000 );
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PESO REAL");
            
            sheet.setColumnWidth((short)6, (short)4000 );
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("UNIDAD");
            
            sheet.setColumnWidth((short)7, (short)6000 );
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AGENCIA");
            
            sheet.setColumnWidth((short)8, (short)7000 );
            cell = row.createCell((short)(8));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA CREACION");
            
            sheet.setColumnWidth((short)9, (short)5000 );
            cell = row.createCell((short)(9));
            cell.setCellStyle(estilo3);
            cell.setCellValue("USUARIO");
            
            
            Iterator It = this.Lista.iterator();
            while(It.hasNext()) {
                
                RemesaSinDoc  re = (RemesaSinDoc) It.next();
                Fila++;
                row  = sheet.createRow((short)(Fila));
                
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getNumrem()!=null)?re.getNumrem():"");
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getFecharemesa()!=null)?re.getFecharemesa():"");
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getCliente()!=null)?re.getCliente():"");
                
                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getOrigen()!=null)?re.getOrigen():"");
                
                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getDestino()!=null)?re.getDestino():"");
                
                cell = row.createCell((short)(5));
                cell.setCellStyle(estilo6);
                cell.setCellValue(re.getCantidad());
                
                cell = row.createCell((short)(6));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getUnidad()!=null)?re.getUnidad():"");
                
                cell = row.createCell((short)(7));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getAgencia()!=null)?re.getAgencia():"");
                
                cell = row.createCell((short)(8));
                cell.setCellStyle(estilo5);
                cell.setCellValue((re.getFechacreacion()!=null)?re.getFechacreacion():"");
                
                cell = row.createCell((short)(9));
                cell.setCellStyle(estilo4);
                cell.setCellValue((re.getUsuariocreacion()!=null)?re.getUsuariocreacion():"");
                
                
            }
            /************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream(Ruta);
            wb.write(fo);
            fo.close();
        }catch(Exception e){
            ////System.out.println(e.getMessage());
        }finally{
            super.destroy();
        }
    }
    
}
