/*
 * HReporteOcsConAnticipoSinReporte.java
 *
 * Created on 7 de marzo de 2007, 09:13 AM
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.ReporteOCAnticipo;
import com.tsp.operation.model.Model;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;


/**
 *
 * @author  equipo
 */
public class HReporteOcsConAnticipoSinReporte extends Thread{
    

    String finicial;
    String ffinal;
    Model model;
    Usuario usuario;
    SimpleDateFormat fmt;
    String processName = "OC.con.Anticipos.sin.Reporte";
    
    
    
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;
    
    
    /** Creates a new instance of HReporteOcsConAnticipoSinReporte */
    public HReporteOcsConAnticipoSinReporte() {
    }
    
    public void start(Model model, String finicial, String ffinal, Usuario usuario){
        this.finicial = finicial;
        this.ffinal   = ffinal;
        this.usuario  = usuario;
        this.model    = model;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del Reporte de Ocs con Anticipos sin reporte en trafico.", usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                //System.out.println("Error HReporteOcsConAnticipoSinReporte ...\n" + e.getMessage());
            }
        }
    }
    
    
    public void generarRUTA() throws Exception{
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            /*cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);*/
            
            
            // estilos
            header         = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo3        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo4        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo5        = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
        
    }
    
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            xls.adicionarCelda(2,0, "PERIODO", titulo1);
            xls.adicionarCelda(2,1, finicial + " - " + ffinal  , titulo1);
            xls.adicionarCelda(3,0, "USUARIO", titulo1);
            xls.adicionarCelda(3,1, usuario.getNombre() , titulo1);
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ReporteOcs_" + fmt.format( new Date() ) +".xls", "REPORTE DE OCS CON ANTICIPOS SIN REPORTE EN TRAFICO ");
            fila = 6;
            
            Vector vector = model.reporteOcAnt.getVector();
            if (vector!=null && !vector.isEmpty()){
                


                    // encabezado
                String [] cabecera = { 
                    "Agencia Generadora OC", "Despachador", "OC", "Valor OC", "Contador OC", "Placa", "Cedula Propietario", "Nombre Propietario",  "Tipo Viaje", "Nro. Psto. Ctrl.",
                    "Ruta", "Agencia Origen", "Agencia Destino", "Fec. Generacion OC", "Fec. Impresion OC", "Días Gen. OC", "Otras OC Asociadas",

                    "Fec. Cumplido", "Cedula Usr. Cumpl.", "Cumplida por",

                    "Agencia Generadora Ant.",  "Fecha Gen. Ant.", "Cedula Gen. Ant.", "Generador Anticipo", "Anticipo", "Cheque", "Fec.Imp.Cheque","Banco", "Sucursal",

                    "Est. Relacion", "Est. Remesa", "OT", "Valor OT", "Descripcion OT", "Cod. Cliente", "Cliente", "Nit. Cliente", "Fact Cliente", "Fec. Ultimo Pago",
                    "Clasificación", "Rango Demora", "Calificación" 
                };
                short  [] dimensiones = new short [] { 
                    6000, 5500, 3500, 4000, 3500, 3500, 5000, 6000, 3500, 3500,
                    3000, 6000, 6000, 4000, 4000, 3000, 7000,
                    
                    4000, 4000, 5500,
                    
                    6000, 4000, 4000, 5500, 4000, 3500, 4000, 6000, 6000,
                    
                    5000, 5000, 3500, 4000, 9000, 3500, 9000, 4000, 4000, 4000, 
                    5000, 4000, 4000
                };
                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }                
                fila++;
                
                
                for ( int i = 0; i< vector.size(); i++, fila++){
                    int col = 0;
                    ReporteOCAnticipo rp = (ReporteOCAnticipo) vector.get(i);

                    // datos de la planilla
                    xls.adicionarCelda(fila  , col++ , rp.getAgcpla()                       , letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getDespachador()                  , letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getNumpla()                       , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , Double.parseDouble(rp.getVlrpla2())  , dinero );
                    xls.adicionarCelda(fila  , col++ , Double.parseDouble(rp.getContaroc()) , numeroCentrado );
                    xls.adicionarCelda(fila  , col++ , rp.getPlaca()                        , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getNitpro()                       , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getNompro()                       , letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getTipo_viaje()                   , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , Double.parseDouble(rp.getPuestocontrol()), numeroCentrado );
                    xls.adicionarCelda(fila  , col++ , rp.getRuta_pla()                     , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getAgc_origen()                   , letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getAgc_destino()                  , letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getCreation_date()                , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , (rp.getPrinter_date().equals("0099-01-01")?"":rp.getPrinter_date() ) , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , Double.parseDouble(rp.getDif())      , numero  );
                    xls.adicionarCelda(fila  , col++ , rp.getOcasociadas()                  , letraCentrada  );

                    // datos del cumplido
                    xls.adicionarCelda(fila  , col++ , (rp.getFechacumplido().equals("0099-01-01")?"":rp.getFechacumplido() ) , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getCedulacumplido()               , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getNombrecumplido()               , letra          );

                    //  datos del anticipo 
                    xls.adicionarCelda(fila  , col++ , rp.getAgenciaanticipo()              , letra  );
                    xls.adicionarCelda(fila  , col++ , (rp.getFechaanticipo().equals("0099-01-01")?"":rp.getFechaanticipo() ), letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getCedulaanticipo()               , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getNombreanticipo()               , letra  );
                    xls.adicionarCelda(fila  , col++ , Double.parseDouble(rp.getVlr())      , dinero );

                    // datos del cheque
                    xls.adicionarCelda(fila  , col++ , rp.getDocument()                       , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , (rp.getFecha_cheque().equals("0099-01-01")?"":rp.getFecha_cheque() ) , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getBanco()                          , letra );
                    xls.adicionarCelda(fila  , col++ , rp.getSucursal()                       , letra );

                    // datos de la remesa
                    xls.adicionarCelda(fila  , col++ , rp.getEstado_relacion()              , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getEstado_remesa()                , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getNumrem()                       , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , Double.parseDouble(rp.getVlrrem2())  , dinero  );
                    xls.adicionarCelda(fila  , col++ , rp.getDescripcion()                  , letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getCodCliente()                   , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getNomCliente()                   , letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getCliente()                      , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getFacturacliente()               , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , (rp.getFecha_pago_remesa().equals("0099-01-01")?"":rp.getFecha_pago_remesa() ) , letraCentrada  );

                    // otros datos
                    xls.adicionarCelda(fila  , col++ , rp.getClasificacion()                   , numeroCentrado  );
                    xls.adicionarCelda(fila  , col++ , rp.getRango()                           , letraCentrada   );
                    xls.adicionarCelda(fila  , col++ , Double.parseDouble(rp.getCalificacion()), dinero          );
                }            

                
                
            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;                
            }
            
            
            this.cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
}
