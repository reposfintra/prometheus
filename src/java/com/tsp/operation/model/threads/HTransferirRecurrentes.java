/*************************************************************
 * Nombre: TransferirRecurrentes.java
 * Descripci�n: Hilo para transferencia de Recurrentes a Facturas
 * Autor: Osvaldo P�rez
 * Fecha: 7 de Julio de 2006
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/


/*
 * TransferirRecurrentes.java
 *
 * Created on 7 de julio de 2006, 11:01 AM
 */

package com.tsp.operation.model.threads;


import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.DAOS.RXPDocDAO;

/**
 *
 * @author  Osvaldo
 */
public class HTransferirRecurrentes extends Thread{
    
    private String ruta;
    private String usuario;
    private String procesoName;
    private String des;    
    private String base;
    private String nit;
    private Date d;
    private Model model = new Model();
    
    private  PrintWriter pw;
    private  String path;
    private  String filename;
    
    private LogWriter   logTrans;
    /** Creates a new instance of TransferirRecurrentes */
    public HTransferirRecurrentes() {
    }
    
    public void start( String usuario, String base, String nit) {
        
        this.procesoName = "Transferencia de Recurrentes";
        this.des = "Transfiere la cuentas recurrentes a cuentas por pagar";
        this.usuario = usuario;
        this.base = base;        
        this.nit = nit;
        this.d = new Date();
        super.start();
    }
    
    public synchronized void run(){
        
        try{
            String fecha = "";                        
            
            Util u = new Util();
            String comentario="EXITOSO";
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode()," Transferencia de Recurrentes", this.usuario );
            
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            path = ruta + "/exportar/migracion/" + this.usuario;
            
            File archivo = new File( path );
            if (!archivo.exists()) archivo.mkdirs();            
            
            initLog();  // preparando archivo de log
            
            
            RXPDocDAO r = new RXPDocDAO();
            String[] log = r.transferir(usuario, base, nit);
                                    
            
            logTrans.log("Facturas transferidas "+log[1], logTrans.INFO);     
            logTrans.log("Facturas NO transferidas "+log[2], logTrans.INFO);     
            logTrans.log(log[0], logTrans.INFO);     
                        
            closeLog(); // cerrando archivo de log
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,comentario);
        }catch(Exception e){
            
            try{
            
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,"ERROR Hilo: " + e.getMessage());
            }catch(Exception ex){}
           
        }
    }
    
     /**
     * Inicia el log de importaciones
     * @throws Exception .
     */    
    public void initLog ()throws Exception{
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");               
        pw     = new PrintWriter (new BufferedWriter(new FileWriter(path + "/logTransferenciaRecurrentes_"+ fmt.format(new Date()) +".txt")));
        logTrans = new LogWriter("TransferenciaRecurrentes", LogWriter.INFO , pw ); 
        logTrans.log( "Proceso Inicializado", logTrans.INFO );
    }
    
    /**
     * Cierra el log general de importaciones
     * @throws Exception .
     */    
    public void closeLog() throws Exception{        
        logTrans.log( "Proceso Finalizado", logTrans.INFO );
        pw.close();
    }
    
    public static void main(String[] a){
        HTransferirRecurrentes t = new HTransferirRecurrentes();
        t.start("ADMIN", "COL", "");
    }
}
