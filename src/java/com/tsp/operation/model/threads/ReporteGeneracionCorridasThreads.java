/*
 * ReporteGeneracionCorridasThreads.java
 *
 * Created on 29 de septiembre de 2006, 02:55 PM
 */

package com.tsp.operation.model.threads;


import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author  JDELAROSA
 */
public class ReporteGeneracionCorridasThreads extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String dstrct = "";
    String corrida = "";
    String banco = "";
    String sucursal = "";
    String aprobadas = "";
    String pagadas = "";
    Model model;
    
    /** Creates a new instance of ReporteGeneracionCorridasThreads */
    public ReporteGeneracionCorridasThreads (Model model) {
        this.model = model;
    }
    
    public void start(String usuario, String dstrct, String corrida, String banco, String sucursal, String aprobadas, String pagadas){        
        this.id = usuario;
        this.dstrct = dstrct;
        this.procesoName = "Reporte de Generacion de Corridas en EXCEL";
        this.des = "Reporte de Generacion de Corridas en EXCEL creadas por "+usuario;
        this.corrida = corrida;
        this.banco = banco;
        this.sucursal = sucursal;
        this.aprobadas = aprobadas;
        this.pagadas = pagadas;
        super.start();
    }
    
    public synchronized void run(){
        try{
            String fecha_actual = Util.getFechaActual_String(6);
            
            //registramos el proeso en el log de procesos
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            if( corrida.equals("") )
                model.extractoService.listaCorridaBanco(dstrct, banco, sucursal, aprobadas, pagadas);
            else{
                LinkedList corridaBanco = new LinkedList();
                Corrida corri = new Corrida();
                corri.setBanco( banco ); 
                corri.setSucursal( sucursal );
                corri.setCorrida( corrida );
                corridaBanco.add(corri);
                List listado = model.extractoService.setCorridaBanco( corridaBanco );
            }
            //obtenemos la lista de facturas creadas segun parametros definidos
            List listado = model.extractoService.getCorridaBanco();
            if ( listado != null && listado.size() > 0 ){
                Usuario us = (Usuario) model.usuarioService.obtenerUsuario(id);
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                File file = new File(path);
                file.mkdirs();
                
                String a�o = fecha_actual.substring(0,4);
                String mes = fecha_actual.substring(5,7);
                String dia = fecha_actual.substring(8,10);
                String hora = fecha_actual.substring(11,13);
                String min = fecha_actual.substring(14,16);
                String seg = fecha_actual.substring(17,19);
                
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite(path+"/DETALLE CORRIDAS " +corrida+"_"+banco+"_"+sucursal+ ".xls");
                
                // Definicion de estilos: fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado                                           
                HSSFCellStyle fecha = xls.nuevoEstilo(      "verdana", 12, false , false, "yyyy/mm/dd", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_RIGHT );
                HSSFCellStyle texto = xls.nuevoEstilo(      "verdana", 12, false, false, "text", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle numero = xls.nuevoEstilo(     "verdana", 12, false, false, "_(#,##0.00_);(@_)", xls.NONE, xls.NONE, HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle negrita = xls.nuevoEstilo(    "verdana", 12, true, false, "text", HSSFColor.BLACK.index, xls.NONE, xls.NONE);
                HSSFCellStyle header = xls.nuevoEstilo(     "verdana", 18, true, false, "text", HSSFColor.LIME.index,xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo = xls.nuevoEstilo(     "verdana", 16, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle subtitulo = xls.nuevoEstilo(  "verdana", 14, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle cabecera = xls.nuevoEstilo(   "verdana", 12, true, false, "text", HSSFColor.WHITE.index, HSSFColor.INDIGO.index, HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja("BASE");
                //formateo al abrir hoja (tamano)
                xls.cambiarMagnificacion(3,4);
                
                //Titulo (cabecera)
                xls.adicionarCelda(0, 0, "TRANSPORTES SANCHEZ POLO S.A", header);
                xls.combinarCeldas(0, 0, 0, 5);  
                xls.adicionarCelda(2, 0, "REPORTE DETALLADOS DE CORRIDAS", titulo);
                xls.combinarCeldas(2, 0, 0, 5);
                xls.adicionarCelda(3, 0, "DISTRITO:", subtitulo);
                xls.adicionarCelda(3, 1, dstrct.toUpperCase (), negrita);
                xls.adicionarCelda(4, 0, "ELABORADO POR : ", subtitulo);
                xls.adicionarCelda(4, 1, us.getNombre().toUpperCase (), negrita);
                xls.adicionarCelda(5, 0, "BRANCH CODE :", subtitulo);
                xls.adicionarCelda(5, 1, banco.toUpperCase (), negrita);
                xls.adicionarCelda(6, 0, "BANK ACCT NO :", subtitulo);
                xls.adicionarCelda(6, 1, sucursal.toUpperCase (), negrita);
                xls.adicionarCelda(7, 0, "CHEQUE RUN NO :", subtitulo);
                xls.adicionarCelda(7, 1, corrida, negrita);
                
                int fila = 7;
                int col  = 0;
                
                //definicion de titulos de la tabla
                xls.adicionarCelda(fila ,col++ , "NRO CORRIDA"                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "NIT"                          , cabecera );
                xls.adicionarCelda(fila ,col++ , "NOMBRE SUPPLIER"              , cabecera );
                xls.adicionarCelda(fila ,col++ , "PLACA/SUPPLIER"               , cabecera );
                xls.adicionarCelda(fila ,col++ , "FACTURA"                      , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA FACTURA"                , cabecera );
                xls.adicionarCelda(fila ,col++ , "VENCIMIENTO"                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "DESCRIPCION"                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "VALOR FACTURA"                , cabecera );
                xls.adicionarCelda(fila ,col++ , "IVA"                          , cabecera );
                xls.adicionarCelda(fila ,col++ , "RETEIVA"                      , cabecera );
                xls.adicionarCelda(fila ,col++ , "RETEICA"                      , cabecera );
                xls.adicionarCelda(fila ,col++ , "RETEFUENTE"                   , cabecera );
                xls.adicionarCelda(fila ,col++ , "NETO FACTURA"                 , cabecera );
                xls.adicionarCelda(fila ,col++ , "MONEDA"                       , cabecera );
                xls.adicionarCelda(fila ,col++ , "OC"                           , cabecera );
                xls.adicionarCelda(fila ,col++ , "OT"                           , cabecera );
                xls.adicionarCelda(fila ,col++ , "NRO CHEQUE"                   , cabecera );
                xls.adicionarCelda(fila ,col++ , "CUENTA CONT"                  , cabecera );
                for (int k = 0; k < listado.size (); k++ ){
                    Corrida corri = (Corrida) listado.get (k);
                    Vector lista = model.extractoService.obtenerCorridasXLS( dstrct, corri.getBanco(), corri.getSucursal(), corri.getCorrida(), aprobadas, pagadas );
                    if ( lista != null && lista.size() > 0 ){
                        //definicion de la tabla con los datos obtenidos de la consulta
                        for (int i = 0; i < lista.size (); i++ ){
                            fila++;
                            col = 0;
                            Vector Vcorrida = (Vector) lista.get(i);
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (18)          , texto );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (0)           , texto );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (1)           , texto );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (2)           , texto );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (3)           , texto );                    
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (4)           , texto );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (5)           , texto );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (6)           , texto );
                            xls.adicionarCelda(fila ,col++ , ( (String)Vcorrida.get (7)  )!=null? Double.parseDouble ( (String) Vcorrida.get (7)  ):0   , numero );
                            xls.adicionarCelda(fila ,col++ , ( (String)Vcorrida.get (8)  )!=null? Double.parseDouble ( (String) Vcorrida.get (8)  ):0   , numero );
                            xls.adicionarCelda(fila ,col++ , ( (String)Vcorrida.get (9)  )!=null? Double.parseDouble ( (String) Vcorrida.get (9)  ):0   , numero );
                            xls.adicionarCelda(fila ,col++ , ( (String)Vcorrida.get (10) )!=null? Double.parseDouble ( (String) Vcorrida.get (10) ):0   , numero );
                            xls.adicionarCelda(fila ,col++ , ( (String)Vcorrida.get (11) )!=null? Double.parseDouble ( (String) Vcorrida.get (11) ):0   , numero );
                            xls.adicionarCelda(fila ,col++ , ( (String)Vcorrida.get (12) )!=null? Double.parseDouble ( (String) Vcorrida.get (12) ):0   , numero );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (17)          , texto );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (14)          , texto );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (13)          , texto );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (15)          , texto );
                            xls.adicionarCelda(fila ,col++ , (String) Vcorrida.get (16)          , texto );
                        }
                    }
                }
                xls.cerrarLibro();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "Proceso Exitoso!");
            }else {
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "No se encontraron Corridas...");
            }
            model.cxpDocService.setEnproceso();
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
                model.cxpDocService.setEnproceso();
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
}
