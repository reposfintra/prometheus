package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.POIWrite;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;
// @author  navi

public class HReporteFacturasEca extends Thread{
       
    private  Model        model;
    private  Usuario      usuario ;
    private  String       procesoName;
    
    private String          url;
    private POIWrite        Excel; 
    private String          hoja;
    private int             fila;
    private int             columna; 
    
    private HSSFCellStyle   texto, negrilla, numero, titulo, porcentaje;
    private HSSFCellStyle   grupoA, grupoB, grupoC, grupoD;
  
    private String[] id_accion;
    private List listaPrefactura,listaPrefacturaReal;
    private String proxima_prefactura;
    public HReporteFacturasEca() {    }     
    
    public void start(Model modelo,  Usuario user,String[] id_accion1,List listaPrefactura1,String proxima_prefactura1,String factura_eca) throws Exception{
         try{            
            this.model       = modelo;
            this.usuario     = user;
            this.procesoName = "CONSULTA DE FACTURA ";
            //proxima_prefactura=proxima_prefactura1;
            //id_accion=id_accion1;
            //listaPrefactura=listaPrefactura1;
            //Prefactura prefactura ;
            //listaPrefacturaReal=new LinkedList();
            /*for (int i=0;i<listaPrefactura.size();i++){
                prefactura = (Prefactura)listaPrefactura.get(i);                
                for (int j=0;j<id_accion1.length;j++){
                    if ((prefactura.getId_accion()).equals(id_accion1[j])){
                        listaPrefacturaReal.add(prefactura);
                    }                    
                }
                
            }*/
            
            super.start();
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
     public synchronized void run(){
       try{
           
           String comentario="EXITOSO";
           model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode()," Reporte Facturas ", this.usuario.getLogin() ); 
                      
           configXLS();
           createHoja("Consulta");
           titulo();
           
           ImpuestoContrato impuestoContrato = model.applusService.getImpuestoContrato();
			
           double porcentajeIva =  impuestoContrato.getPorcentaje1();
           //System.out.println("porcentajeIva : "+porcentajeIva );
           List  listaPrefactura  = listaPrefacturaReal;//model.applusService.getPrefactura();
           for(int i=0;i<listaPrefactura.size();i++){
                Prefactura prefactura = (Prefactura)listaPrefactura.get(i);                
                addPrefactura(prefactura,porcentajeIva);
           } 
           
           Excel.cerrarLibro();   
           
           model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);           
           
             
       }catch(Exception e){
           try{
               model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage()); 
           }
           catch(Exception f){ }   
       }
       
    }
    
    public void titulo()throws Exception{
        try{    
            
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "FINTRAVALORES",           this.negrilla ); incFila();
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "CONSULTA DE FACTURA",   this.negrilla ); incFila();
              
            incFila();
                        
            Excel.combinarCeldas( fila, 0,  fila,5);   Excel.adicionarCelda( this.fila, 0,  "FACTURA  "+proxima_prefactura,          this.grupoA );
                       
            incFila();
            setearColumna();
            
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "ORDEN",             this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "ACCION",           this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "NUM OS",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "MATERIAL",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "MANO OBRA",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "OTROS",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "SUBTOTAL",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "BONIFICACION",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "SUBTOTAL",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "IVA",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "TOTAL",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "NOMBRE CLIENTE",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "DIRECCION",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CIUDAD",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "ACCIONES",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CONSECUTIVO",      this.titulo); incColumna();
            
            incFila();
            setearColumna();            
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    public void addPrefactura(Prefactura prefactura,double porcentajeIva)throws Exception{
        try{  
                        
            //Prefactura prefactura = (Prefactura)it.next();
            String id_orden = prefactura.getId_orden();
            String id_accion = prefactura.getId_accion();
            String consecutivo = prefactura.getConsecutivo();
            String id_cliente = prefactura.getId_cliente();
            String nombre_cliente = prefactura.getNombre_cliente();
            String direccion = prefactura.getDireccion();
            String ciudad = prefactura.getCiudad();
            String acciones = prefactura.getAcciones();

            double total_prev1_mat = prefactura.getTotal_prev1_mat();
            double total_prev1_mob = prefactura.getTotal_prev1_mob();
            double total_prev1_otr = prefactura.getTotal_prev1_otr();
            String num_os = prefactura.getNumOs();
            double total_prev1 = total_prev1_mat + total_prev1_mob + total_prev1_otr;
            double comision_total = prefactura.getComision_total();
            double subtotal = total_prev1 + comision_total;
            double iva = Util.redondear2(subtotal*porcentajeIva/100, 2);

            String b=String.valueOf(iva);
            double ivaDouble=Double.valueOf(b);
            b=String.valueOf(subtotal);
            double subtotalDouble=Double.valueOf(b);
            double valorItem = subtotalDouble + ivaDouble;
                        
            Excel.adicionarCelda( this.fila, this.columna, prefactura.getId_orden()           ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, prefactura.getId_accion()               ,this.texto);  incColumna();            
           
            Excel.adicionarCelda( this.fila, this.columna, prefactura.getNumOs() ,this.texto); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, Util.customFormat(total_prev1_mat) ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.customFormat(total_prev1_mob) ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.customFormat(total_prev1_otr) ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.customFormat(total_prev1) ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.customFormat(comision_total) ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.customFormat(subtotal) ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.FormatoMiles(ivaDouble) ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.FormatoMiles(valorItem) ,this.texto); incColumna();
                        
            Excel.adicionarCelda( this.fila, this.columna, nombre_cliente ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, direccion ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, ciudad ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, acciones ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, consecutivo ,this.texto); incColumna();
                                    
            incFila();
            setearColumna();            
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
     public void configXLS() throws Exception{
       try{          
            DirectorioService dd = new DirectorioService();
            dd.create( this.usuario.getLogin()  );
            url = dd.getUrl() +  this.usuario.getLogin() + "/CONSULTAFAC"+ Util.getFechaActual_String(6).replaceAll("/|:| ","") + ".xls";
            
            this.Excel  = new POIWrite(url);
            createStyle();
            
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }

    public void createStyle()throws Exception{
       try{
             HSSFColor cGris       = Excel.obtenerColor(192,192,192);
                        
          this.texto        = Excel.nuevoEstilo("Tahoma", 7,   false , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);
          this.numero       = Excel.nuevoEstilo("Tahoma", 8 ,  false , false, "_($* #,##0.00_);(@_)"      , Excel.NONE , Excel.NONE , Excel.NONE);
          this.titulo       = Excel.nuevoEstilo("Tahoma", 8,   true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLACK.index  , HSSFCellStyle.ALIGN_CENTER);                        
          this.negrilla     = Excel.nuevoEstilo("Arial", 11,   true  , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);                     
          
          this.porcentaje   = Excel.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , Excel.NONE , Excel.NONE , Excel.NONE);
              
          this.grupoA       = Excel.nuevoEstilo("Tahoma",10,   true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.TEAL.index        , HSSFCellStyle.ALIGN_CENTER);              
          this.grupoB       = Excel.nuevoEstilo("Tahoma",10,   true  , false, "text"        , HSSFColor.WHITE.index , cGris.getIndex()            , HSSFCellStyle.ALIGN_CENTER);              
          this.grupoC       = Excel.nuevoEstilo("Tahoma",10,   true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.LIGHT_BLUE.index  , HSSFCellStyle.ALIGN_CENTER);              
          this.grupoD       = Excel.nuevoEstilo("Tahoma",10,   true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index      , HSSFCellStyle.ALIGN_CENTER);              
          
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }

    public void createHoja(String nombreHoja)throws Exception{
        try{            
            hoja = nombreHoja;
            this.Excel.obtenerHoja(hoja);             
            setearFila();
            setearColumna();            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
 
    public void incFila()      {  this.fila++;      }
    public void incColumna()   {  this.columna++;   }    
    public void setearFila()   {  this.fila=0;      }
    public void setearColumna(){  this.columna=0;   }   
    
}
