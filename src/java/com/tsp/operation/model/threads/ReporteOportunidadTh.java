/********************************************************************
 *      Nombre Clase.................   ReporteOportunidad.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   27.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;



public class ReporteOportunidadTh extends Thread{
    
    private Vector reporte;
    private Vector reporteCMSA;
    private Vector detalles;
    private String user;    
    private String fechai;
    private String fechaf;
    
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Vector reporte, Vector reporteCMSA, Vector detalles, String user, String fechai, String fechaf){
        this.reporte = reporte;
        this.reporteCMSA = reporteCMSA;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.detalles = detalles;
        
        this.model   = modelo;
        this.procesoName = "Oportunidad en la Generaci�n de los reportes";
        this.des = "Reporte de oportunidad en la Generaci�n de los reportes:  " + fechai + " - " + fechaf;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteDeOportunidad_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReporteDeOportunidad_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                                                
            xls.obtenerHoja("REPORTE DE OPORTUNIDAD");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, 6);
            xls.combinarCeldas(3, 0, 3, 6);
            xls.combinarCeldas(4, 0, 4, 6);
            xls.adicionarCelda(0, 0, "REPORTE DE OPORTUNIDAD", header2);
            xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
                            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila, col++, "USUARIO", header1);
            xls.adicionarCelda(fila, col++, "LOGIN", header1);
            xls.adicionarCelda(fila, col++, "REGISTROS", header1);
            xls.adicionarCelda(fila, col++, "OPORTUNOS", header1);
            xls.adicionarCelda(fila, col++, "INOPORTUNOS", header1);
            xls.adicionarCelda(fila, col++, "% OPORTUNOS", header1);
            xls.adicionarCelda(fila, col++, "% INOPORTUNOS", header1);
            
            
            fila++;
            
            for( int i=0; i<reporte.size(); i++){
                col = 0;
                
                RepGral obj = (RepGral) reporte.elementAt(i);
                
                xls.adicionarCelda(fila, col++, obj.getUsuario(), texto2);                
                xls.adicionarCelda(fila, col++, obj.getLogin(), texto2);
                xls.adicionarCelda(fila, col++, obj.getRegistros(), entero);
                xls.adicionarCelda(fila, col++, obj.getOportunos(), entero);
                xls.adicionarCelda(fila, col++, obj.getInoportunos(), entero);
                xls.adicionarCelda(fila, col++, obj.getPorcent_opor(), flotante);
                xls.adicionarCelda(fila, col++, obj.getPorcent_inop(), flotante);
                
                fila++;
            }         
            
            xls.obtenerHoja("DETALLES");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, 17);
            xls.combinarCeldas(3, 0, 3, 17);
            xls.combinarCeldas(4, 0, 4, 17);
            xls.adicionarCelda(0, 0, "REPORTE DE OPORTUNIDAD", header2);
            xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
            fila = 6;
            col  = 0;
            
            xls.adicionarCelda(fila, col++, "PLANILLA", header1);
            xls.adicionarCelda(fila, col++, "CLIENTE", header1);
            xls.adicionarCelda(fila, col++, "OBSERVACION", header1);
            xls.adicionarCelda(fila, col++, "TIPO PROCEDENCIA", header1);
            xls.adicionarCelda(fila, col++, "UBICACION PROCEDENCIA", header1);
            xls.adicionarCelda(fila, col++, "TIPO REPORTE", header1);
            xls.adicionarCelda(fila, col++, "ULT. ACTUALIZACION", header1);
            xls.adicionarCelda(fila, col++, "FECHA REPORTE", header1);
            xls.adicionarCelda(fila, col++, "DIFERENCIA (MINS)", header1);
            xls.adicionarCelda(fila, col++, "OPORTUNO", header1);
            xls.adicionarCelda(fila, col++, "ACTUALIZO", header1);
            xls.adicionarCelda(fila, col++, "FECHA CREACION", header1);
            xls.adicionarCelda(fila, col++, "ZONA", header1);
            xls.adicionarCelda(fila, col++, "USUARIO DE CREACION ", header1);
            xls.adicionarCelda(fila, col++, "BASE", header1);
            xls.adicionarCelda(fila, col++, "FECHA REPORTE PLANEADO", header1);
            xls.adicionarCelda(fila, col++, "CAUSA", header1);
            xls.adicionarCelda(fila, col++, "CLASIFICACION", header1);
            
            fila++;
            
            for(int i=0; i<this.detalles.size(); i++){
                Vector vec = (Vector) detalles.elementAt(i);
                RepMovTrafico rmt = (RepMovTrafico) vec.elementAt(0);
                String usuario = (String) vec.elementAt(1);
                String cliente = (String) vec.elementAt(2);
                String usuario_up = (String) vec.elementAt(4);
                
                col = 0;
                
                xls.adicionarCelda(fila, col++, rmt.getNumpla(), texto2);
                xls.adicionarCelda(fila, col++, cliente, texto2);
                xls.adicionarCelda(fila, col++, rmt.getObservacion(), texto2);
                xls.adicionarCelda(fila, col++, rmt.getTipo_procedencia(), texto2);
                xls.adicionarCelda(fila, col++, rmt.getUbicacion_procedencia(), texto2);
                xls.adicionarCelda(fila, col++, rmt.getTipo_reporte(), texto2);
                xls.adicionarCelda(fila, col++, rmt.getLast_update(), texto2);
                long mins = com.tsp.util.Util.minutosTranscurridos(rmt.getLast_update(), rmt.getFechaReporte());
                xls.adicionarCelda(fila, col++, rmt.getFechaReporte(), texto2);
                xls.adicionarCelda(fila, col++, String.valueOf(mins), texto2);
                xls.adicionarCelda(fila, col++, mins>=-60 && mins<=60 ? "SI" : "NO", texto2);
                xls.adicionarCelda(fila, col++, usuario_up, texto2);
                xls.adicionarCelda(fila, col++, rmt.getCreation_date(), texto2);
                xls.adicionarCelda(fila, col++, rmt.getZona(), texto2);
                xls.adicionarCelda(fila, col++, usuario, texto2);
                xls.adicionarCelda(fila, col++, rmt.getBase(), texto2);
                xls.adicionarCelda(fila, col++, rmt.getFechareporte_planeado().equals("0099-01-01 00:00:00") ? "" : rmt.getFechareporte_planeado() , texto2);
                xls.adicionarCelda(fila, col++, rmt.getCausa(), texto2);
                xls.adicionarCelda(fila, col++, rmt.getClasificacion(), texto2);
                
                fila++;
            }

            xls.obtenerHoja("CMSA");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, 6);
            xls.combinarCeldas(3, 0, 3, 6);
            xls.combinarCeldas(4, 0, 4, 6);
            xls.adicionarCelda(0, 0, "REPORTE DE OPORTUNIDAD CMSA", header2);
            xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
                            
            // subtitulos
            
            fila = 6;
            col  = 0;
            
            xls.adicionarCelda(fila, col++, "USUARIO", header1);
            xls.adicionarCelda(fila, col++, "LOGIN", header1);
            xls.adicionarCelda(fila, col++, "REGISTROS", header1);
            xls.adicionarCelda(fila, col++, "OPORTUNOS", header1);
            xls.adicionarCelda(fila, col++, "INOPORTUNOS", header1);
            xls.adicionarCelda(fila, col++, "% OPORTUNOS", header1);
            xls.adicionarCelda(fila, col++, "% INOPORTUNOS", header1);
            
            
            fila++;
            
            for( int i=0; i<reporteCMSA.size(); i++){
                col = 0;
                
                RepGral obj = (RepGral) reporteCMSA.elementAt(i);
                
                xls.adicionarCelda(fila, col++, obj.getUsuario(), texto2);                
                xls.adicionarCelda(fila, col++, obj.getLogin(), texto2);
                xls.adicionarCelda(fila, col++, obj.getRegistros(), entero);
                xls.adicionarCelda(fila, col++, obj.getOportunos(), entero);
                xls.adicionarCelda(fila, col++, obj.getInoportunos(), entero);
                xls.adicionarCelda(fila, col++, obj.getPorcent_opor(), flotante);
                xls.adicionarCelda(fila, col++, obj.getPorcent_inop(), flotante);
                
                fila++;
            }
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO EXITOSO");
            
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}