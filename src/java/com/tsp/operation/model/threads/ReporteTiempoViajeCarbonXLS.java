/***********************************************************************************
 * Nombre clase : ............... ReporteTiempoViajeCarbonXLS.java                 *
 * Descripcion :................. hilo que ejecuta el                              *
 *                                programa de TiempoViajeCarbon                    *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 21 de noviembre de 2005, 08:55 AM                *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.threads;


import java.io.*;
import jxl.write.*;
import com.tsp.operation.model.beans.JXLWrite;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;

/**
 *
 * @author  dbastidas
 */
public class ReporteTiempoViajeCarbonXLS extends Thread {
    private String FechaI;
    private String FechaF;
    private String usuario;
    private String procesoName;
    private String des;
    
    Model model;
    TiempoViajeCarbon tviajecarbon = new TiempoViajeCarbon();
    //Formulas
    String Viaje_Vacio ="S@-Q@";
    String Enturne="S@-R@";
    String EspEnturne = "SI(AA@>$AN$7,1,0)";
    String Poenturne = "SI(AB@=1,AA@,\"\")";
    String Tcarge = "T@-S@";
    //TRANSITO + CARGUE
    String Tran_carge = "BUSCARV(BE@,$AC$3:$AD$#,2,0)";//Modificado 23-08-2006
    String TranMayor = "SI(AD@>=0,SI(AD@>AE@,1,0),0)";
    String pro_limitetrafico = "SI(AF@=1,AD@,\"\")";
    String llegaday = "U@-T@";
    String llegadapuert = "V@-U@";
    String totalleno = "AI@+AH@";
    String espera = "SI(W@-V@>=0,W@-V@,0)";
    String desefectivo = "SI(X@-W@>=0,X@-W@,0)";
    String salida = "SI(Y@-X@>=0,Y@-X@,0)";
    String esperamincarge = "SI(AD@-$AN$4>0,AD@-$AN$4,0)";
    String esperatotal = "AK@+AN@";
    String vehencima = "SI(AO@>$AN$3,1,0)";
    String fecopc = "SI(Y@-ENTERO(Y@)<=$AQ$5,ENTERO(Y@)-1,ENTERO(Y@))";
    String esperaptomayor = "SI(AV@=\"N\",0,SI(AK@>$AN$5,1,0))";
    String esperaminmayor = "SI(AN@>$AN$6,1,0)";
    String tip_flota = "No Fija";
    String hllegadapto = "V@-ENTERO(V@)";
    String incluir = "Y";
    String capacidadcga = "$AW$6-M@";
    String dif_neto = "N@-AW@";
    String ragodispeso = "SI(AX@<-5,-6,SI(AX@>5,6,REDONDEAR(AX@,0)))"; // .MAS
    String dis_pesominpos = "SI(O@>=0,O@,)";
    String dis_pesominneg = "SI(O@<0,O@,)";
    String dis_pesoptomin = "SI(O@<-5,-6,SI(O@>5,6,REDONDEAR(O@,0)))"; // .MAS
    String fecllegadadesc = "ENTERO(V@)";
    String horallegada = "HORA(AU@)";
    //String RUTA2 = "BUSCARV(P@,$CM$1:$CN$12385,2,0)"; Se busca en tablagen tabla_type ='ALIASJ'
    
    String fecdescargue = "SI(BD@>BF@,BC@+1,BC@)";
    String tiempoespdesc = "SI(BD@=BF@,W@-V@,SI(W@-(BG@+BF@/24)>=0,W@-(BG@+BF@/24),0))";
    String esperadescargue = "BUSCARV(BE@,$AC$3:$AH$#,3,0)";//Modificado 23-08-2006
    String esperamayor2 = "SI(AV@=\"N\",0,SI(BH@>BI@,1,0))";
    String promlindes = "SI(BJ@=1,BH@,\"\")";
    String htiempoespera = "HORA(BH@)";
    String responsable = "Y";
    String tiemesperareal = "HORA(AK@)";
    String fecsalida = "ENTERO(X@)";
    String hsaldes = "HORA(X@)";
    String tiem_viajevacio = "SI(REDONDEAR(Z@,4)>=$CH$7,$CH$7,SI(REDONDEAR(Z@,4)>=$CH$6,$CH$6," +
    "SI(REDONDEAR(Z@,4)>=$CH$5,$CH$5,SI(REDONDEAR(Z@,4)>=$CH$4,$CH$4," +
    "SI(REDONDEAR(Z@,4)>=$CH$3,$CH$3,$CH$2)))))";
    String tiempo_viajelleno = "SI(REDONDEAR(AJ@,4)>=$CI$7,$CI$7,SI(REDONDEAR(AJ@,4)>=$CI$6,$CI$6," +
    "SI(REDONDEAR(AJ@,4)>=$CI$5,$CI$5,SI(REDONDEAR(Z@,4)>=$CI$4,$CI$4," +
    "SI(REDONDEAR(Z@,4)>=$CI$3,$CI$3,$CI$2)))))";
    String tiempo_cargue = "SI(REDONDEAR(AD@,6)>=$CJ$7,$CJ$7,SI(REDONDEAR(AD@,6)>=$CJ$6,$CJ$6," +
    "SI(REDONDEAR(AD@,6)>=$CJ$5,$CJ$5,SI(REDONDEAR(AD@,6)>=$CJ$4,$CJ$4," +
    "SI(REDONDEAR(AD@,6)>=$CJ$3,$CJ$3,$CJ$2)))))";
    String tiempo_descargue = "SI(REDONDEAR(AK@+AL@,6)>=$CK$7,$CK$7,SI(REDONDEAR(AK@+AL@,6)>=$CK$6,$CK$6," +
    "SI(REDONDEAR(AK@+AL@,6)>=$CK$5,$CK$5,SI(REDONDEAR(AK@+AL@,6)>=$CK$4,$CK$4," +
    "SI(REDONDEAR(AK@+AL@,6)>=$CK$3,$CK$3,$CK$2)))))";
    String viaje_vacio_lleno = "Z@+AJ@";
    String viajellenomayor = "SI(BU@>BUSCARV(BE@,$AC$3:$AH$#,6,0),1,0)";//Modificado 23-08-2006
    String prom_vaciolleno = "SI(BV@=1,BU@,\"\")";
    String vaciomayor = "SI(Z@>BUSCARV(BE@,$AC$3:$AH$#,4,0),1,0)";//Modificado 23-08-2006
    String prom_vaciomayor = "SI(BX@=1,Z@,\"\")";
    String llenomayor = "SI(AJ@>BUSCARV(BE@,$AC$3:$AH$#,5,0),1,0)";//Modificado 23-08-2006
    String prom_tllenomayor = "SI(BZ@=1,AJ@,\"\")";
    String salidamin = "ENTERO(T@)";
    String hsalidamin = "HORA(T@)";
    String hllegadamin = "HORA(R@)";
    String llegadamin = "ENTERO(R@)";
    String hesperamin = "HORA(AN@)";
    
    public void start(String FechaI,String FechaF,String user){
        
        this.FechaI = FechaI;
        this.FechaF = FechaF;
        this.usuario = user;
        this.procesoName = "Reporte Tiempo Viaje Carbon";
        this.des = "Tiempos Viaje "+FechaI+" - "+FechaF;
        super.start();
    }
    
    public synchronized void run(){
        
        
        try{
            
            Model model = new Model();
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, this.usuario);
            Vector    standares =  model.tiempoviajecarbonService.buscarSJ();
            
            List    reporte = (List) model.tiempoviajecarbonService.tiempoViajeCarbonXLS(FechaI, FechaF);
            
            if (reporte!=null && reporte.size()>0){
                
                
                
                Calendar FechaHoy = Calendar.getInstance();
                Date d = FechaHoy.getTime();
                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
                String FechaFormated = s.format(d);
                
                
                //obtener cabecera de ruta
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta");
                //armas la ruta
                String Ruta  = path + "/exportar/migracion/" + usuario + "/";
                //String Ruta  = "C:/TiempoCarbon/";
                //crear la ruta
                File file = new File(Ruta);
                file.mkdirs();
                
                ////System.out.println("Inicia XLS "+ reporte.size());
                JXLWrite xls = new JXLWrite(Ruta +"TiemposViajeCarbon"+FechaI+"_"+FechaF+".xls");
                
                try {
                    WritableCellFormat fecha        = xls.nuevoEstilo("Arial", 10, false , false, new jxl.write.DateFormat("dd/mm/yyyy")  , null , null , null );
                    WritableCellFormat fecha_hora   = xls.nuevoEstilo("Arial", 10, false , false, new jxl.write.DateFormat("dd/mm/yyyy hh:mm")  , null , null , null );
                    WritableCellFormat texto        = xls.nuevoEstilo("Arial", 10, false , false, null , null , null , null);
                    WritableCellFormat numdec       = xls.nuevoEstilo("Arial", 10, false , false, new jxl.write.NumberFormat("#.##")  , null , null , null);
                    WritableCellFormat numdec1       = xls.nuevoEstilo("Arial", 10, false , false, new jxl.write.DateFormat("#.##0")  , null , null , null);
                    WritableCellFormat numero       = xls.nuevoEstilo("Arial", 10, false , false, new jxl.write.NumberFormat("#")  , null , null , null);
                    WritableCellFormat hora_min     = xls.nuevoEstilo("Arial", 10, false , false, new jxl.write.DateFormat("[h]:mm")   , null , null , null);
                    WritableCellFormat dia_hora_min = xls.nuevoEstilo("Arial", 10, false , false, new jxl.write.DateFormat("dd hh:mm")  , null , null , null);
                    
                    WritableCellFormat header      = xls.nuevoEstilo("Arial", 12, true  , false, null , Colour.BLUE, null , null);
                    WritableCellFormat azul      = xls.nuevoEstilo("Arial", 10, true  , false, null  , Colour.BLACK , Colour.SKY_BLUE, null);
                    WritableCellFormat verde      = xls.nuevoEstilo("Arial", 10, true  , false, null  , Colour.BLACK , Colour.LIGHT_GREEN, null);
                    WritableCellFormat morado      = xls.nuevoEstilo("Arial", 10, true  , false, null  , Colour.BLACK , Colour.LAVENDER, null);
                    WritableCellFormat rosado     = xls.nuevoEstilo("Arial", 10, true  , false, null  , Colour.BLACK , Colour.PINK, null);
                    WritableCellFormat amarillo1      = xls.nuevoEstilo("Arial", 10, true  , false, null  , Colour.BLACK , Colour.YELLOW, null);
                    WritableCellFormat naranja      = xls.nuevoEstilo("Arial", 10, true  , false, null  , Colour.BLACK , Colour.LIGHT_ORANGE, null);
                    WritableCellFormat amarillo      = xls.nuevoEstilo("Arial", 10, true  , false, null  , Colour.BLACK , Colour.VERY_LIGHT_YELLOW, null);
                    
                    
                    xls.obtenerHoja("TIEMPOS");
                    xls.cambiarMagnificacion(80);
                    
                    // cabecera
                    xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", header);
                    xls.adicionarCelda(1,0, "REPORTE TIEMPOS DE VIAJES", header);
                    xls.combinarCeldas(0, 0, 0, 5);
                    xls.combinarCeldas(1, 0, 0, 5);
                    
                    //comfigurar los anchos
                    xls.cambiarAnchoColumna(1, 10);
                    xls.cambiarAnchoColumna(2, 10);
                    
                    xls.adicionarCelda(1,29, "TRANSITO +  CARGUE",  texto);
                    xls.adicionarCelda(1,30, "ESPERA EN DESCARGUE",  texto);
                    xls.adicionarCelda(1,31, "Viaje Vacio"   , texto);
                    xls.adicionarCelda(1,32, "Viaje Lleno"   , texto);
                    xls.adicionarCelda(1,33, "Viaje Lleno + Vacio"   , texto);
                    
                    int pos=2;
                    //buscar los standares
                    for(int i=0 ; i < standares.size(); i++){
                        Hashtable info = (Hashtable) standares.get(i);
                        if(  !((String) info.get("tiempos")).equals("") ){
                            xls.adicionarCelda(pos,28, (String) info.get("descripcion") , texto);
                            String tiempos[] = ((String) info.get("tiempos")).split(",");
                            xls.adicionarFormula(pos,29, tiempos[0]+"/24" ,  hora_min);
                            xls.adicionarFormula(pos,30, tiempos[1]+"/24" ,  hora_min);
                            xls.adicionarFormula(pos,31, tiempos[2]+"/24" ,  hora_min);
                            xls.adicionarFormula(pos,32, tiempos[3]+"/24" ,  hora_min);
                            xls.adicionarFormula(pos,33, "("+tiempos[2]+"+"+tiempos[3]+")/24" ,  hora_min);
                            pos++;
                        }
                    }
                    //Remplazo en la formula hasta donde realiza la busqueda
                    Tran_carge = Tran_carge.replace("#", ""+pos);
                    esperadescargue = esperadescargue.replace("#", ""+pos);
                    viajellenomayor = viajellenomayor.replace("#", ""+pos);
                    vaciomayor = vaciomayor.replace("#", ""+pos);
                    llenomayor = llenomayor.replace("#", ""+pos);
                    
                    
                    
                    
                    //Valore de limites
                    xls.adicionarCelda(2,38, "limite horas perdidas para poder hacer m�s de un viaje diario" , texto);
                    xls.adicionarFormula(2,39, "1.5/24"   , hora_min);
                    xls.adicionarCelda(3,38, "minutos programados duracion operaci�n cargue" , texto);
                    xls.adicionarFormula(3,39, "1/24"   , hora_min);
                    xls.adicionarCelda(4,38, "Tiempo Espera Puerto" , texto);
                    xls.adicionarFormula(4,39, "1/24"   , hora_min);
                    xls.adicionarCelda(5,38, "Tiempo Espera Mina" , texto);
                    xls.adicionarFormula(5,39, "1.25/24"   , hora_min);
                    xls.adicionarCelda(6,38, "Tiempo Enturne" , texto);
                    xls.adicionarFormula(6,39, "1.25/24"   , hora_min);
                    
                    //HOra de corte
                    xls.adicionarCelda(4,41, "Hora Corte" , texto);
                    xls.adicionarFormula(4,42, "7/24"   , hora_min);
                    xls.adicionarCelda(5,41, "Vehiculos con mas de 01:30:00 horas perdidas"   , texto); //valor de limte en horas
                    xls.adicionarCelda(4,45, "Fija > viajes"   , texto);
                    xls.adicionarCelda(5,45, "16"   , numero);
                    
                    xls.adicionarCelda(4,48, "Peso Maximo"   , texto);
                    xls.adicionarFormula(5,48, "53/1"   , numero);
                    
                    //CLases de tiempos
                    xls.adicionarCelda(0,85, "Clase Tiempo Viaje Vacio"   , texto);
                    xls.adicionarFormula(1,85, "5.5/24"   , hora_min);
                    xls.adicionarFormula(2 , 85 ,"CH2+1/24" , hora_min );
                    xls.adicionarFormula(3 , 85 ,"CH3+1/24" , hora_min );
                    xls.adicionarFormula(4 , 85 ,"CH4+1/24" , hora_min );
                    xls.adicionarFormula(5 , 85 ,"CH5+1/24" , hora_min );
                    xls.adicionarFormula(6 , 85 ,"CH6+1/24" , hora_min );
                    
                    xls.adicionarCelda(0,86, "Clase Tiempo Viaje Lleno"   , texto);
                    xls.adicionarFormula(1, 86, "5.5/24", hora_min);
                    xls.adicionarFormula(2 , 86 ,"CI2+1/24" , hora_min );
                    xls.adicionarFormula(3 , 86 ,"CI3+1/24" , hora_min );
                    xls.adicionarFormula(4 , 86 ,"CI4+1/24" , hora_min );
                    xls.adicionarFormula(5 , 86 ,"CI5+1/24" , hora_min );
                    xls.adicionarFormula(6 , 86 ,"CI6+1/24" , hora_min );
                    
                    xls.adicionarCelda(0,87, "Clase Tiempo Cargue"   , texto);
                    xls.adicionarFormula(1,87, "(12+40/60)/24"   , hora_min);
                    xls.adicionarFormula(2 , 87 ,"CJ2+5/60/24" , hora_min );
                    xls.adicionarFormula(3 , 87 ,"CJ3+5/60/24" , hora_min );
                    xls.adicionarFormula(4 , 87 ,"CJ4+5/60/24" , hora_min );
                    xls.adicionarFormula(5 , 87 ,"CJ5+5/60/24" , hora_min );
                    xls.adicionarFormula(6 , 87 ,"CJ6+5/60/24" , hora_min );
                    
                    xls.adicionarCelda(0,88, "Clase Tiempo Descargue"   , texto);
                    xls.adicionarFormula(1,88, "(12+30/60)/24"   , hora_min);
                    xls.adicionarFormula(2 , 88 ,"CK2+5/60/24" , hora_min );
                    xls.adicionarFormula(3 , 88 ,"CK3+5/60/24" , hora_min );
                    xls.adicionarFormula(4 , 88 ,"CK4+5/60/24" , hora_min );
                    xls.adicionarFormula(5 , 88 ,"CK5+5/60/24" , hora_min );
                    xls.adicionarFormula(6 , 88 ,"CK6+5/60/24" , hora_min );
                   
                    
                    // subtitulos
                    int fila = pos+3;
                    int col  = 0;
                    
                    xls.adicionarCelda(fila ,col++ , "Remision"     , azul );
                    xls.adicionarCelda(fila ,col++ , "Fec_ingre"     , azul );
                    xls.adicionarCelda(fila ,col++ , "Placa"     , azul );
                    xls.adicionarCelda(fila ,col++ , "Trailer"     , azul );
                    xls.adicionarCelda(fila ,col++ , "CedCon"      , verde );
                    xls.adicionarCelda(fila ,col++ , "Conductor"     , verde );
                    xls.adicionarCelda(fila ,col++ , "CedProp"      , verde );
                    xls.adicionarCelda(fila ,col++ , "Propietario"  , verde );
                    xls.adicionarCelda(fila ,col++ , "P.Lleno Pto"     , verde );
                    xls.adicionarCelda(fila ,col++ , "P.Vacio Pto"     , verde );
                    xls.adicionarCelda(fila ,col++ , "P.Neto Pto"      , verde );
                    xls.adicionarCelda(fila ,col++ , "P.Lleno Min"      , verde );
                    xls.adicionarCelda(fila ,col++ , "P.Vacio Min"      , verde );
                    xls.adicionarCelda(fila ,col++ , "P.Neto Min"      , verde );
                    xls.adicionarCelda(fila ,col++ , "Dif_Pesos"     , verde );
                    xls.adicionarCelda(fila ,col++ , "Ruta"      , azul );
                    xls.adicionarCelda(fila ,col++ , "Inicio Viaje"        , azul);
                    xls.adicionarCelda(fila ,col++ , "Llegada a mina"       , azul );
                    xls.adicionarCelda(fila ,col++ , "Entrada cargue"      , azul );
                    xls.adicionarCelda(fila ,col++ , "Salida Mina"      , azul );
                    xls.adicionarCelda(fila ,col++ , "Llegada la Y"     , azul );
                    xls.adicionarCelda(fila ,col++ , "Entrada Puerto"      , azul );
                    xls.adicionarCelda(fila ,col++ , "Entrada Descar"      , azul);
                    xls.adicionarCelda(fila ,col++ , "Salida Descar"      , azul );
                    xls.adicionarCelda(fila ,col++ , "Salida Puerto"      , azul );
                    
                    xls.adicionarCelda(fila ,col++ , "T.Viaje Vacio"      , morado );
                    xls.adicionarCelda(fila ,col++ , "T.Enturne"       , morado );
                    xls.adicionarCelda(fila ,col++ , "Espera Enturne > 1 hora"  , morado );
                    xls.adicionarCelda(fila ,col++ , "T.promedio de enturne > 1 hora"  , morado );
                    xls.adicionarCelda(fila ,col++ , "T.Cargue"  , rosado );
                    xls.adicionarCelda(fila ,col++ , "T.TRANSITO + CARGUE"  , rosado );
                    xls.adicionarCelda(fila ,col++ , "T.TRANSITO MAYOR"  , rosado );
                    xls.adicionarCelda(fila ,col++ , "T.Promedio > T.limite de tr�nsito"  , rosado );
                    xls.adicionarCelda(fila ,col++ , "T.Llegada LaY"  , azul );
                    xls.adicionarCelda(fila ,col++ , "T.Llegada Puer"  , azul );
                    xls.adicionarCelda(fila ,col++ , "T.TotalLleno"  , morado );
                    xls.adicionarCelda(fila ,col++ , "T.Espera"  , amarillo );
                    xls.adicionarCelda(fila ,col++ , "T.Descar Efectivo"  , azul );
                    xls.adicionarCelda(fila ,col++ , "T.Salida"  , azul );
                    xls.adicionarCelda(fila ,col++ , "T.Espera Mina Cargue"  , amarillo );
                    xls.adicionarCelda(fila ,col++ , "T.Espera Total"  , amarillo1 );
                    xls.adicionarCelda(fila ,col++ , "Vehiculos por encima"  , amarillo1 );
                    xls.adicionarCelda(fila ,col++ , "FECHA OPCION"  , amarillo );
                    xls.adicionarCelda(fila ,col++ , "Espera Puerto Mayor"  , amarillo );
                    xls.adicionarCelda(fila ,col++ , "Espera Mina Mayor"  , amarillo );
                    xls.adicionarCelda(fila ,col++ , "TIPO FLOTA"  , amarillo );
                    xls.adicionarCelda(fila ,col++ , "Hora llegada Puerto"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Incluir"  , amarillo );
                    xls.adicionarCelda(fila ,col++ , "Capacidad carga"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Diferencia Vs P.Neto Min"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Rango Diferencia Peso"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Dif.Peso_Puerto-Mina Positivo"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Dif.Peso_Puerto-Mina Negativo"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Rango_Dif Peso_Puerto-Mina"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Fecha Llegada Descargue"  , naranja );
                    xls.adicionarCelda(fila ,col++ , "Rango Hora Llegada"  , naranja );
                    xls.adicionarCelda(fila ,col++ , "RUTA 2"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Rango Hora Llegada 2"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Fecha Descargue"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Tiempo Espera Puerto Descargue"  , azul );
                    xls.adicionarCelda(fila ,col++ , "ESPERA EN DESCARGUE"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Espera Mayor (2)"  , azul );
                    xls.adicionarCelda(fila ,col++ , "T.promedio> t.limite descargue"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Rango Hora Tiempo espera"  , azul );
                    xls.adicionarCelda(fila ,col++ , "RESPONSABLE"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Rango Tiempo Espera Puerto Real"  , azul );
                    xls.adicionarCelda(fila ,col++ , "Fecha Salida"  , naranja );
                    xls.adicionarCelda(fila ,col++ , "Rango Hora Salida Descargue"  , naranja );
                    xls.adicionarCelda(fila ,col++ , "Rango Tiempo Viaje Vacio"  , morado );
                    xls.adicionarCelda(fila ,col++ , "Rango Tiempo Viaje Lleno"  , morado );
                    xls.adicionarCelda(fila ,col++ , "Rango Tiempo Cargue"  , morado );
                    xls.adicionarCelda(fila ,col++ , "Rango Tiempo Descargue"  , morado );
                    xls.adicionarCelda(fila ,col++ , "TViajeVacio_Lleno"  , morado );
                    xls.adicionarCelda(fila ,col++ , "TVVacioLlenoMayor"  , morado );
                    xls.adicionarCelda(fila ,col++ , "T.promeido>tvacio+lleno"  , morado );
                    xls.adicionarCelda(fila ,col++ , "TVVacioMayor"  , morado );
                    xls.adicionarCelda(fila ,col++ , "T.promedio>TvacioMayor"  , morado );
                    xls.adicionarCelda(fila ,col++ , "TVLlenoMayor"  , morado );
                    xls.adicionarCelda(fila ,col++ , "T.promedio>TllenoMayor"  , morado );
                    xls.adicionarCelda(fila ,col++ , "Fecha Salida Mina"  , verde );
                    xls.adicionarCelda(fila ,col++ , "Rango Hora Salida Mina"  , verde );
                    xls.adicionarCelda(fila ,col++ , "Rango Hora Llegada Mina"  , verde );
                    xls.adicionarCelda(fila ,col++ , "Fecha Llegada Mina"  , verde );
                    xls.adicionarCelda(fila ,col++ , "Rango Hora TEspera Mina"  , verde );
                    xls.adicionarCelda(fila ,col++ , "Fecha Modificaci�n"  , azul );
                    
                    // datos
                    int it = 0;
                    while(it < reporte.size()){
                        fila++;
                        col = 0;
                        
                        tviajecarbon = (TiempoViajeCarbon) reporte.get(it);
                        
                        
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getRemision() , texto );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getFec_ingre().replaceAll("-","/").substring(0,16), fecha );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getPlaca() , texto );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getTrayler(), texto );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getCedcon(), numero );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getConductor(), texto );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getCedpro(), numero );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getPropietario(), texto );
                        
                        xls.adicionarFormula(fila ,col++ , tviajecarbon.getLleno_pto()+"*1",numdec1  );
                        xls.adicionarFormula(fila ,col++ , tviajecarbon.getVacio_pto()+"*1", numdec1 );
                        xls.adicionarFormula(fila ,col++ , tviajecarbon.getNeto_pto()+"*1", numdec1 );
                        xls.adicionarFormula(fila ,col++ , tviajecarbon.getLleno_min()+"*1", numdec1 );
                        xls.adicionarFormula(fila ,col++ , tviajecarbon.getVacio_min()+"*1", numdec1 );
                        xls.adicionarFormula(fila ,col++ , tviajecarbon.getNeto_min()+"*1", texto );
                        xls.adicionarFormula(fila ,col++ , tviajecarbon.getDif_pesos()+"*1", numdec1 );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getRuta() , texto );
                        
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getInicio_viaje(), fecha_hora );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getLlegada_mina(), fecha_hora );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getEntrada_cargue() , fecha_hora );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getSalida_mina() , fecha_hora );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getLlegada_y() , fecha_hora );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getEntrada_puerto() , fecha_hora );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getEntrada_descarga() , fecha_hora );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getSalida_descar() , fecha_hora );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getSalida_puerto(), fecha_hora );
                        
                        xls.adicionarFormula(fila ,col++ , Viaje_Vacio.replaceAll("@", ""+(fila+1) ), hora_min );
                        xls.adicionarFormula(fila ,col++ , Enturne.replaceAll("@", ""+(fila+1) ), dia_hora_min );
                        xls.adicionarFormula(fila ,col++ ,EspEnturne.replaceAll("@", ""+(fila+1) ) , texto );
                        xls.adicionarFormula(fila ,col++ ,Poenturne.replaceAll("@", ""+(fila+1) ) , hora_min );
                        xls.adicionarFormula(fila ,col++ ,Tcarge.replaceAll("@", ""+(fila+1) ) , hora_min );
                        xls.adicionarFormula(fila ,col++ ,Tran_carge.replaceAll("@", ""+(fila+1) ) , hora_min );
                        xls.adicionarFormula(fila ,col++ ,TranMayor.replaceAll("@", ""+(fila+1) ) , texto );
                        xls.adicionarFormula(fila ,col++ ,pro_limitetrafico.replaceAll("@", ""+(fila+1) ) , hora_min ); //T.Promedio > T.limite de tr�nsito
                        xls.adicionarFormula(fila ,col++ ,llegaday.replaceAll("@", ""+(fila+1) ) , hora_min );
                        xls.adicionarFormula(fila ,col++ ,llegadapuert.replaceAll("@", ""+(fila+1) ) , hora_min );
                        xls.adicionarFormula(fila ,col++ ,totalleno.replaceAll("@", ""+(fila+1) ) , hora_min );
                        
                        xls.adicionarFormula(fila ,col++ ,espera.replaceAll("@", ""+(fila+1) ) , dia_hora_min );
                        xls.adicionarFormula(fila ,col++ ,desefectivo.replaceAll("@", ""+(fila+1) ) , dia_hora_min );
                        xls.adicionarFormula(fila ,col++ ,salida.replaceAll("@", ""+(fila+1) ) , dia_hora_min );
                        xls.adicionarFormula(fila ,col++ , esperamincarge.replaceAll("@", ""+(fila+1) ), hora_min );
                        xls.adicionarFormula(fila ,col++ , esperatotal.replaceAll("@", ""+(fila+1) ),hora_min );
                        xls.adicionarFormula(fila ,col++ , vehencima.replaceAll("@", ""+(fila+1) ),texto );
                        xls.adicionarFormula(fila ,col++ , fecopc.replaceAll("@", ""+(fila+1) ), fecha );
                        xls.adicionarFormula(fila ,col++ , esperaptomayor.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarFormula(fila ,col++ , esperaminmayor.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarCelda(fila ,col++ , tip_flota , fecha_hora );
                        xls.adicionarFormula(fila ,col++ , hllegadapto.replaceAll("@", ""+(fila+1) ), hora_min );
                        xls.adicionarCelda(fila ,col++ , incluir , fecha_hora );
                        xls.adicionarFormula(fila ,col++ , capacidadcga.replaceAll("@", ""+(fila+1) ), numdec );
                        
                        
                        xls.adicionarFormula(fila ,col++ , dif_neto.replaceAll("@", ""+(fila+1) ), numdec );
                        xls.adicionarFormula(fila ,col++ , ragodispeso.replaceAll("@", ""+(fila+1) ), numdec );
                        xls.adicionarFormula(fila ,col++ , dis_pesominpos.replaceAll("@", ""+(fila+1) ), numdec );
                        xls.adicionarFormula(fila ,col++ , dis_pesominneg.replaceAll("@", ""+(fila+1) ), numdec );
                        xls.adicionarFormula(fila ,col++ , dis_pesoptomin.replaceAll("@", ""+(fila+1) ), numero );
                        xls.adicionarFormula(fila ,col++ , fecllegadadesc.replaceAll("@", ""+(fila+1) ), fecha );
                        xls.adicionarFormula(fila ,col++ , horallegada.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getRuta2(), texto );
                        
                        
                        xls.adicionarCelda(fila ,col++ ,tviajecarbon.getRangohllegada(), texto );
                        
                        xls.adicionarFormula(fila ,col++ , fecdescargue.replaceAll("@", ""+(fila+1) ), fecha );
                        xls.adicionarFormula(fila ,col++ , tiempoespdesc.replaceAll("@", ""+(fila+1) ), dia_hora_min );
                        xls.adicionarFormula(fila ,col++ , esperadescargue.replaceAll("@", ""+(fila+1) ), dia_hora_min );
                        
                        xls.adicionarFormula(fila ,col++ , esperamayor2.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarFormula(fila ,col++ , promlindes.replaceAll("@", ""+(fila+1) ), hora_min );
                        xls.adicionarFormula(fila ,col++ , htiempoespera.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarCelda(fila ,col++ , responsable, texto );
                        xls.adicionarFormula(fila ,col++ , tiemesperareal.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarFormula(fila ,col++ , fecsalida.replaceAll("@", ""+(fila+1) ), fecha );
                        xls.adicionarFormula(fila ,col++ , hsaldes.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarFormula(fila ,col++ , tiem_viajevacio.replaceAll("@", ""+(fila+1) ), hora_min );
                        xls.adicionarFormula(fila ,col++ , tiempo_viajelleno.replaceAll("@", ""+(fila+1) ), hora_min );
                        xls.adicionarFormula(fila ,col++ , tiempo_cargue.replaceAll("@", ""+(fila+1) ), hora_min );
                        xls.adicionarFormula(fila ,col++ , tiempo_descargue.replaceAll("@", ""+(fila+1) ), hora_min );
                        xls.adicionarFormula(fila ,col++ , viaje_vacio_lleno.replaceAll("@", ""+(fila+1) ), hora_min );
                        xls.adicionarFormula(fila ,col++ , viajellenomayor.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarFormula(fila ,col++ , prom_vaciolleno.replaceAll("@", ""+(fila+1) ), dia_hora_min );
                        xls.adicionarFormula(fila ,col++ , vaciomayor.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarFormula(fila ,col++ , prom_vaciomayor.replaceAll("@", ""+(fila+1) ), dia_hora_min );
                        xls.adicionarFormula(fila ,col++ , llenomayor.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarFormula(fila ,col++ , prom_tllenomayor.replaceAll("@", ""+(fila+1) ), dia_hora_min );
                        xls.adicionarFormula(fila ,col++ , salidamin.replaceAll("@", ""+(fila+1) ), fecha );
                        xls.adicionarFormula(fila ,col++ , hsalidamin.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarFormula(fila ,col++ , hllegadamin.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarFormula(fila ,col++ , llegadamin.replaceAll("@", ""+(fila+1) ), fecha );
                        xls.adicionarFormula(fila ,col++ , hesperamin.replaceAll("@", ""+(fila+1) ), texto );
                        xls.adicionarCelda(fila ,col++ , tviajecarbon.getLast_update().replaceAll("-","/").substring(0,10), fecha );
                        it++;
                        
                    }
                    System.gc();
                }
                catch (Exception ex){
                    try{
                        model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.usuario ,"ERROR :" + ex.getMessage());
                    }
                    catch(Exception f){
                        try{
                            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usuario,"ERROR :");
                        }catch(Exception p){    }
                    }
                }
                finally{
                    xls.cerrarLibro();
                    
                }
            }
            ////System.out.println("Listo..");
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.usuario, "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("Error : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.usuario ,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usuario,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    
    public static void main(String a []){
        ReporteTiempoViajeCarbonXLS proc = new ReporteTiempoViajeCarbonXLS();
        proc.start("2006-08-16","2006-08-17","DBASTIDAS");
    }
}
