/*
 * Nombre        HExportarExcel_TiemposDeViajesConductores.java
 * Descripci�n   Genera el reporte de tiempos de viaje de conductores en formato excel
 * Autor         Alejandro Payares
 * Fecha         26 de enero de 2006, 01:00 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.ReporteExcelSOT;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Utility;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Iterator;

// EXCEL
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

/**
 * Genera el reporte de tiempos de viaje de conductores en formato excel
 * @author Alejandro Payares
 */
public class HExportarExcel_TiemposDeViajesConductores extends ReporteExcelSOT {
    
    /**
     * El numero de parametros para el reporte, usado para determinar el tama�o del vector
     * de parametros
     */
    public static final int NUMERO_DE_PARAMETROS = 9;
    /**
     * la posici�n en el vector de parametros de la fecha de inicio
     */
    public static final int FECHA_INICIO         = 0;
    /**
     * la posici�n en el vector de parametros de fecha de fin
     */
    public static final int FECHA_FIN            = 1;
    /**
     * la posici�n en el vector de parametros de la placa
     */
    public static final int PLACA                = 2;
    /**
     * la posici�n en el vector de parametros del cliente
     */
    public static final int CLIENTE              = 3;
    /**
     * la posici�n en el vector de parametros del origen
     */
    public static final int ORIGEN               = 4;
    /**
     * la posici�n en el vector de parametros del destino
     */
    public static final int DESTINO              = 5;
    /**
     * la posici�n en el vector de parametros del nombre de la ciudad de origen
     */
    public static final int NOMBRE_ORIGEN        = 6;
    /**
     * la posici�n en el vector de parametros del nombre de la ciudad de destino
     */
    public static final int NOMBRE_DESTINO       = 7;
    /**
     * la posici�n en el vector de parametros del nombre del cliente
     */
    public static final int NOMBRE_CLIENTE       = 8;
    
    
    
    /**
     * El modelo de la aplicaci�n
     */
    private   Model       model;
    /**
     * El nombre de la unidad fisica donde ser� guardado el reporte.
     */
    private   String      unidad;
    /**
     * El nombre del archivo donde ser� guardado el reporte
     */
    private   String      fileName;
    /**
     * El usuario en sesi�n
     */
    private   Usuario     usuario;
    /**
     * Un arreglo que contiene los parametros de consulta del reporte
     */
    private   String[]    parametrosReporte;
    /**
     * Variable que indica la fila dentro de la hoja deexcel en donde se est� escribiendo la informaci�n
     */
    private int filaActual = -1;
    /**
     * Un arreglo que contiene los nombres de los campos fijos del reporte
     */
    private  String campos [] = {"planilla","origen","destino","placa","conductor","nombrecliente","fechaSalida"};
    
    /**
     * Crea una nueva instancia de HExportarExcel_TiemposDeViajesConductores
     * @autor Alejandro Payares
     * @param model EL modelo de la aplicaci�n
     * @param user El usuario en sesi�n
     * @param argumentos Los argumentos de consulta del reporte
     * @throws Exception Si algun erro ocurre
     */
    public HExportarExcel_TiemposDeViajesConductores(Model model, Usuario user, String[] argumentos) throws Exception{
        String hoy    = Utility.getDate(6).replaceAll("/","").replaceAll(":","_");
        this.fileName = "Tiempos_"+hoy;
        this.model       = model;
        this.usuario     = user;
        this.parametrosReporte = argumentos;
        String dir       = user.getLogin();
        this.unidad      = model.DirectorioSvc.getUrl() + dir;
        //this.unidad = "c:";
        this.model.DirectorioSvc.create(dir);
        this.path        = unidad +"/"+ fileName + ".xls";
        initExcel(fileName);
    }
    
    /**
     * Es el encargado de iniciar el hijo que genera el reporte en el archivo excel
     */
    public void generar(){
        new Thread(this,fileName).start();
    }
    
    /**
     * Crea el encabezado del reporte
     * @throws Exception si alg�n error ocurre
     */
    public void crearEncabezado()throws Exception{
        int columna = 0;
        crearCelda(++filaActual, columna);
        HSSFCellStyle estilo = clonarEstilo(estiloBlancoGrande);
        quitarBordeAEstilo(estilo);
        cell.setCellStyle(estilo);
        cell.setCellValue("TRANSPORTES SANCHEZ POLO S.A");
        for( int i=1; i< 5; i++ ){
            crearCelda(filaActual, i);
        }
        crearCeldaCombinada(filaActual, columna, filaActual, 6, false);
        
        //this.prepared(++filaActual, 5);
        crearCelda(++filaActual, columna);
        estilo = clonarEstilo(estiloBlancoGrande);
        quitarBordeAEstilo(estilo);
        cell.setCellStyle(estilo);
        cell.setCellValue("REPORTE DE TIEMPOS DE VIAJES DE CONDUCTORES");
        for( int i=1; i< 5; i++ ){
            crearCelda(filaActual, i);
        }
        crearCeldaCombinada(filaActual, columna, filaActual, 6, false);
        
        //this.prepared(++filaActual, 7);
        //this.prepared(filaActual+1, 7);
        
        // la fecha del reporte
        crearCelda(++filaActual, columna);
        cell.setCellStyle(this.estiloAzulGrande);
        cell.setCellValue("Fecha del reporte");
        crearCelda(filaActual+1, columna);
        cell.setCellStyle(this.estiloBlanco);
        cell.setCellValue(Utility.getDate(6));
        
        // la fecha de inicio de consulta
        crearCelda(filaActual, ++columna);
        cell.setCellStyle(this.estiloAzulGrande);
        cell.setCellValue("Fecha inicial");
        crearCelda(filaActual+1, columna);
        cell.setCellStyle(this.estiloBlanco);
        cell.setCellValue(parametrosReporte[FECHA_INICIO]);
        
        // la fecha de inicio de consulta
        crearCelda(filaActual, ++columna);
        cell.setCellStyle(this.estiloAzulGrande);
        cell.setCellValue("Fecha final");
        crearCelda(filaActual+1, columna);
        cell.setCellStyle(this.estiloBlanco);
        cell.setCellValue(parametrosReporte[FECHA_FIN]);
        
        if ( parametrosReporte[PLACA] != null) {
            crearCelda(filaActual, ++columna);
            cell.setCellStyle(this.estiloAzulGrande);
            cell.setCellValue("Placa");
            crearCelda(filaActual+1, columna);
            cell.setCellStyle(this.estiloBlanco);
            cell.setCellValue(parametrosReporte[PLACA]);
        }
        if ( parametrosReporte[CLIENTE] != null) {
            crearCelda(filaActual, ++columna);
            cell.setCellStyle(this.estiloAzulGrande);
            cell.setCellValue("Cliente");
            crearCelda(filaActual+1, columna);
            cell.setCellStyle(this.estiloBlanco);
            cell.setCellValue(parametrosReporte[NOMBRE_CLIENTE]);
        }
        if ( parametrosReporte[ORIGEN] != null) {
            crearCelda(filaActual, ++columna);
            cell.setCellStyle(this.estiloAzulGrande);
            cell.setCellValue("Origen");
            crearCelda(filaActual+1, columna);
            cell.setCellStyle(this.estiloBlanco);
            cell.setCellValue(parametrosReporte[NOMBRE_ORIGEN]);
        }
        if ( parametrosReporte[DESTINO] != null) {
            crearCelda(filaActual, ++columna);
            cell.setCellStyle(this.estiloAzulGrande);
            cell.setCellValue("Destino");
            crearCelda(filaActual+1, columna);
            cell.setCellStyle(this.estiloBlanco);
            cell.setCellValue(parametrosReporte[NOMBRE_DESTINO]);
        }
        filaActual+=2;
    }
    
    /**
     * Crea los titulos del reporte
     * @throws Exception Si algun error ocurre
     */
    public void crearTitulos() throws Exception {
        int diasMaximos = model.repTiempoDeViajeConductores.obtenerNumeroMaximoDeDias();
        String titulos [] = {"Planilla","Origen","Destino","Placa","Conductor","Cliente","Fecha y hora de salida"};
        filaActual++;
        int columna = -1;
        int camposDelDia = 6;
        HSSFCellStyle estilo = clonarEstilo(estiloBlanco);
        quitarBordeAEstilo(estilo);
        for(int i=0; i<titulos.length; i++ ){
            this.crearCelda(filaActual, ++columna);
            cell.setCellStyle(estilo);
            cell.setCellValue("");
        }
        for( int i=1; i<=diasMaximos; i++ ){
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+1);
            cell.setCellStyle(this.estiloAmarilloGrande);
            cell.setCellValue("DIA "+i);
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+2);
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+3);
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+4);
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+5);
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+6);
            this.crearCeldaCombinada(filaActual, (columna+(i-1)*camposDelDia)+1, filaActual, (columna+(i-1)*camposDelDia)+6, true);
        }
        filaActual++;
        columna = -1;
        for(int i=0; i<titulos.length; i++ ){
            this.crearCelda(filaActual, ++columna);
            cell.setCellStyle(this.estiloAzul);
            cell.setCellValue(titulos[i]);
        }
        for( int i=1; i<=diasMaximos; i++ ){
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+1);
            cell.setCellStyle(this.estiloAzul);
            cell.setCellValue("Primer rep");
            
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+2);
            cell.setCellStyle(this.estiloAzul);
            cell.setCellValue("Ultimo rep");
            
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+3);
            cell.setCellStyle(this.estiloAzul);
            cell.setCellValue("Tiempo trabajo");
            
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+4);
            cell.setCellStyle(this.estiloAzul);
            cell.setCellValue("Demora");
            
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+5);
            cell.setCellStyle(this.estiloAzul);
            cell.setCellValue("Descripcion demora");
            
            this.crearCelda(filaActual, (columna+(i-1)*camposDelDia)+6);
            cell.setCellStyle(this.estiloAzul);
            cell.setCellValue("Total");
        }
    }
    
    /**
     * Metodo encargado de ejecutar la generaci�n del reporte.
     */
    public void run() {
        try {
            model.repTiempoDeViajeConductores.buscarDatosReporte(
            parametrosReporte[FECHA_INICIO],
            parametrosReporte[FECHA_FIN],
            parametrosReporte[CLIENTE], parametrosReporte[PLACA],
            parametrosReporte[ORIGEN],
            parametrosReporte[DESTINO]
            );
            LinkedList datos = model.repTiempoDeViajeConductores.obtenerDatosReporte();
            this.crearEncabezado();
            //this.prepared(++filaActual, 1);
            this.crearCelda(++filaActual, 0);
            cell.setCellStyle(this.estiloNormalGrande);
            cell.setCellValue(datos.size()+" Registros encontrados");
            crearTitulos();
            int diasMaximos = model.repTiempoDeViajeConductores.obtenerNumeroMaximoDeDias();
            Iterator devolIt = datos.iterator();
            String camposDia [] = {"inicio","fin","tiempo","demora","descripciones","total"};
            while( devolIt.hasNext() ) {
                Hashtable fila = (Hashtable) devolIt.next();
                filaActual++;
                int columna = -1;
                for( int i = 0; i < campos.length; i++ ) {
                    String str = ((String)fila.get(campos[i]));
                    str = str == null? "":str;
                    str = str.equals("0099-01-01")?"":str;
                    crearCelda(filaActual, ++columna);
                    cell.setCellStyle(this.estiloBlanco);
                    cell.setCellValue(str.equals("")?"-":str);
                    
                }
                for( int i=1; i<=diasMaximos; i++){
                    Hashtable datosDelDia = (Hashtable)fila.get("dia"+i);
                    if ( datosDelDia == null ){
                        for( int j=1; j<= camposDia.length; j++ ){
                            this.crearCelda(filaActual, (columna+(i-1)*camposDia.length)+j);
                            cell.setCellStyle(this.estiloBlanco);
                            cell.setCellValue("");
                        }
                    }
                    else {
                        for( int j=1; j<= camposDia.length; j++ ){
                            this.crearCelda(filaActual, (columna+(i-1)*camposDia.length)+j);
                            if ( camposDia[j-1].equals("demora") || camposDia[j-1].equals("descripciones") ) {
                                cell.setCellStyle(this.estiloLetraRoja);
                            }
                            else {
                                cell.setCellStyle(this.estiloBlanco);
                            }
                            cell.setCellValue(""+datosDelDia.get(camposDia[j-1]));
                        }
                    }
                }
            }        
            model.repTiempoDeViajeConductores.borrarDatos();
            save();
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
    }
    
}
