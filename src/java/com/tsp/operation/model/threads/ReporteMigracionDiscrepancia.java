/*
 * ReporteMigracionDiscrepancia.java
 *
 * Created on 14 de octubre de 2005, 11:39 AM
 */

package com.tsp.operation.model.threads;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.*;
/**
 *
 * @author  dbastidas
 */
public class ReporteMigracionDiscrepancia extends Thread{
    public ReporteMigracionDiscrepancia() {
    }
    
    private String procesoName;
    private String des;
    private String id="";
    private FileWriter fw;
    private BufferedWriter bffw;
    private PrintWriter pntw;
    private MigracionDiscrepancia migdis;
    private  String fecha_actual;
    private Model model = new Model();
    
    /** Creates a new instance of ReporteMigracionDiscrepancia */
    public Vector info = null;
    public void start(String id){
        ////System.out.println("entro por hilo");
        this.id = id;
        this.procesoName = "Migrar Discrepancia";
        this.des = "Migrar la discrepancia";
        
              
        try {
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            info = model.migradiscreService.MigrarDiscrepancia();
            ////System.out.println("Tama�o "+info);
            String nombre_archivo = "";
            //String ruta = "c:/Diogenes/";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            ruta = ruta + "/exportar/migracion/"+this.id;
            File f = new File(ruta);////System.out.println(ruta);
            f.mkdirs();
            fecha_actual = Util.getFechaActual_String(6);           
            nombre_archivo = ruta + "/ANULAOC2" + fecha_actual.replace(':','_').replace('/','_') + ".csv";            ////System.out.println("1");
            ////System.out.println("NOMBRE ARCHIVO " + nombre_archivo);
            File archivo = new File(ruta.trim());
            ////System.out.println("Fecha "+fecha_actual);
            archivo.mkdirs();
            FileOutputStream file = new FileOutputStream(nombre_archivo.trim());
            fw = new FileWriter(nombre_archivo);
            bffw = new BufferedWriter(fw);
            pntw = new PrintWriter(bffw);
        }
        catch(Exception e){
            ////System.out.println(e.toString());
        }
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            this.writeFile();
            model.migradiscreService.marcarMigracion(fecha_actual.replace('/','-'), this.id);
            ////System.out.println("Fin Proceso");
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                Model model = new Model();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                try{
                    Model model = new Model();
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
    public void writeFile() throws Exception{
    try{
            
        String linea = "";
        String sep=",";
        ////System.out.println("recorro el vector");
        for (int i= 0; i < info.size(); i++) {
             migdis = (MigracionDiscrepancia) info.elementAt(i);
             linea = migdis.getOC() +
                          sep+ migdis.getItem()+
                          sep+ migdis.getRaised_by()+
                          sep+ migdis.getRaised_date()+
                          sep+ migdis.getDr_medium_ind()+
                          sep+ migdis.getSupp_contated()+
                          sep+ migdis.getSupp_contact()+
                          sep+ migdis.getDocumento()+
                          sep+ migdis.getRaised_date()+
                          sep+ migdis.getDiscrep_qty()+
                          sep+ migdis.getDiscrep_oum()+
                          sep+ migdis.getQi_code()+
                          sep+ migdis.getDr_desc()+
                          sep+ migdis.getDr_ref()+
                          sep+ migdis.getDiscrp_type1()+
                          sep+ migdis.getDiscrp_type2()+
                          sep+ migdis.getDiscrp_type3()+
                          sep+ migdis.getDiscrp_type4()+
                          sep+ migdis.getDiscrp_type5()+
                          sep+ migdis.getDiscrp_type6()+
                          sep+ migdis.getHold_pmt_ind()+
                          sep+ migdis.getUpd_stats_ind()+
                          sep+ migdis.getCr_replace_ind()+
                          sep+ migdis.getReplace_qty()+
                          sep+ migdis.getCredit_value();
                 
            pntw.println(linea);  
            ////System.out.println("escribo la linea");
        }
        pntw.close();
        }
        catch(Exception e){
            ////System.out.println(e.toString());
        }
    }
}
