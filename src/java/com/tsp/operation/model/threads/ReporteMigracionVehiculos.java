/*************************************************************
 * Nombre: ReporteMigracionCumplidos.java
 * Descripci�n: Hilo para crear el ReporteMigracionVehiculos
 * Autor: Ing. Jose de la rosa
 * Fecha: 24 de octubre de 2005, 11:39 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author  Fily
 */
public class ReporteMigracionVehiculos extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String fechaI = "";
    String fechaF = "";
    private PrintWriter pw;
    private LogWriter   logTrans;
    private String path;
    public ReporteMigracionVehiculos () {
    }
    
    public void start (String id, String FechaI,String FechaF){
        this.id = id;
        this.fechaI = FechaI;
        this.fechaF = FechaF;
        this.procesoName = "Vehiculo Migrado";
        this.des = "Migracion de vehiculos por fechas";
        super.start ();
    }
    
    public synchronized void run (){
        try{
            
            Model model = new Model ();
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            path = ruta + "/exportar/migracion/" + id;

            File archivo = new File( path );
            if (!archivo.exists()) archivo.mkdirs();
            initLog();
            
            model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, this.id);
            model.transitoService.BusquedaVehiculos(fechaI, fechaF);
            Vector lista = (Vector) model.transitoService.getVector();
            Date hoy = new Date ();
            SimpleDateFormat s = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
            String fecha_hoy = s.format (hoy);
            String a�o = fecha_hoy.substring(0,4);
            String mes = fecha_hoy.substring(5,7);
            String dia = fecha_hoy.substring(8,10);
            if (lista!=null && lista.size ()>0){
                
                ResourceBundle rbT = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
                String pathT = rbT.getString ("ruta");
                File fileT = new File (pathT + "/exportar/migracion/" + id + "/");
                fileT.mkdirs ();
                String NombreArchivo = pathT + "/exportar/migracion/" + id + "/890103161"+mes+a�o+"VEHICULOS"+".txt";
                PrintStream archivoT = new PrintStream (NombreArchivo);
                
                
                Iterator it = lista.iterator ();
                String datos1;
                String datos2;
                String datos3;
                String datos4;
                String datos5;
                String datos6;
                String datos7;
                String datos8;
                String datos9;
                String datos10;
                String datos11;
                String datos12;
                String datos13;
                String datos14;
                String datos15;
                String datos16;
                String datos17;
                String datos18;
               
               
                while(it.hasNext ()){
                    BeanGeneral Bean = (BeanGeneral) it.next ();
                    if(Bean.getValor_01().equals("")){
                        logTrans.log("No se encuentra numero de la placa del vehiculo", logTrans.INFO);
                    }
                    datos1= this.TruncarDatos( Bean.getValor_01() , 6);
                   
                    if(Bean.getValor_02().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Marca del vehiculo", logTrans.INFO);
                    }
                    datos2= this.TruncarDatos( Bean.getValor_02() , 2);
                    
                    if(Bean.getValor_04().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Linea del vehiculo", logTrans.INFO);
                    }
                    datos3= this.TruncarDatos( Bean.getValor_04() , 3);
                    
                    if(Bean.getValor_03().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Modelo del vehiculo", logTrans.INFO);
                    }
                    datos4= this.TruncarDatos( Bean.getValor_03() , 4);
                    
                    if(Bean.getValor_18().equals("")){
                        logTrans.log("El numero de manifiesto = "+Bean.getValor_01()+" no tiene no tiene Modelo Repontenciado del vehiculo", logTrans.INFO);
                    }
                    datos5= this.TruncarDatos( Bean.getValor_18() , 4);
                    
                    if(Bean.getValor_05().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Numero de Serie el vehiculo", logTrans.INFO);
                    }
                    datos6= this.TruncarDatos( Bean.getValor_05() , 25);
                    
                    
                    if(Bean.getValor_06().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Color del vehiculo", logTrans.INFO);
                    }
                    datos7= this.TruncarDatos( Bean.getValor_06() , 3);
                    
                    if(Bean.getValor_07().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Tipo de Carroceria del vehiculo", logTrans.INFO);
                    }
                    datos8= this.TruncarDatos( Bean.getValor_07() , 3);
                    
                    if(Bean.getValor_08().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Peso del Vehiculo cuando esta vacios", logTrans.INFO);
                    }
                    datos9= this.TruncarDatos( Bean.getValor_08() , 5);
                    
                    if(Bean.getValor_09().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Numero de Registro Nacional de Carga", logTrans.INFO);
                    }
                    datos10= this.TruncarDatos( Bean.getValor_09() , 8);
                    
                    if(Bean.getValor_10().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene # Poliza Soat", logTrans.INFO);
                    }
                    datos11= this.TruncarDatos( Bean.getValor_10() , 20);
                    
                    if(Bean.getValor_11().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Tipo de identifiacion de aseguradora", logTrans.INFO);
                    }
                    datos12= this.TruncarDatos( Bean.getValor_11() , 1);
                    
                    if(Bean.getValor_14().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Numero de Identificacion de aseguradora", logTrans.INFO);
                    }
                    datos13= this.TruncarDatos( Bean.getValor_14() , 11);
                    
                    if(Bean.getValor_15().equals("0099/01/01")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Fecha de Venciemiento de Poliza", logTrans.INFO);
                    }
                    datos14=  Bean.getValor_15();
                    
                    if(Bean.getValor_16().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Tipo de Identificacion de Propietario", logTrans.INFO);
                    }
                    datos15= this.TruncarDatos( Bean.getValor_16() , 1);
                    
                    if(Bean.getValor_12().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene # de Identificacion de Propietario", logTrans.INFO);
                    }
                    datos16= this.TruncarDatos( Bean.getValor_12() , 11);
                    
                    if(Bean.getValor_17().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene Tipo de Indentificacion de Tenedor", logTrans.INFO);
                    }
                    datos17= this.TruncarDatos( Bean.getValor_17() , 1);
                    
                    if(Bean.getValor_13().equals("")){
                        logTrans.log("El vehiculo con la placa # = "+Bean.getValor_01()+" no tiene # de Identificacion de Tenedor", logTrans.INFO);
                    }
                    datos18= this.TruncarDatos( Bean.getValor_13() , 11);
                    
                      
                   archivoT.println (datos1 +" \t "+datos2+" \t "+datos3+" \t "+ datos4+ " \t "+datos5 +
                                     " \t "+ datos6+  " \t "+datos7+ " \t "+datos8+ " \t "+datos9 +
                                     " \t "+datos10+" \t "+datos11+" \t "+datos12+" \t "+datos13+" \t "+datos14+
                                     " \t "+datos15+" \t "+datos16+ " \t "+datos17+" \t "+datos18);
                 
                }
                closeLog();
            }
            
            model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                Model model = new Model ();
                model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (),this.id,"ERROR :" + e.getMessage ());
            }
            catch(Exception f){
                try{
                    Model model = new Model ();
                    model.LogProcesosSvc.finallyProceso (this.procesoName,this.hashCode (),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
    /*funciones para llenar */
    public String agregarEspacios(String valor, int longitud ) throws Exception {
       int tama�oValor = valor.length();
       int tama�o = longitud - tama�oValor;
       String vacios = "";
       String relleno = "";
        for(int i= 0;i< tama�o; i++){
            vacios = vacios+" "; 
        }
        relleno =  valor + vacios ;
        
        return relleno;
       
    }  
    
    public String TruncarDatos(String valor, int longitud ) throws Exception {
       String valorFinal = "";
       int tama�oValor = valor.length();
       String valorTruncado = "";
       if(tama�oValor > longitud ){
          valorTruncado = valor.substring(0, longitud);
       }else{
           valorTruncado = valor;
       }
       valorFinal = this.agregarEspacios(valorTruncado, longitud);
       return valorFinal;
       
    }  
     
     
    public void initLog()throws Exception{
        
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");
        pw     = new PrintWriter(new BufferedWriter(new FileWriter(path + "/logMigracionVehiculos_.txt")));
        logTrans = new LogWriter("Vehiculos_migrqaciones", LogWriter.INFO , pw );
        logTrans.log( "Proceso Inicializado", logTrans.INFO );
    }
    
    public void closeLog() throws Exception{
        logTrans.log( "Proceso Finalizado", logTrans.INFO );
        pw.close();
    }
    
}
