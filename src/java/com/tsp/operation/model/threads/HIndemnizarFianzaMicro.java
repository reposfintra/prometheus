/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.threads;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.GarantiasComunitariasDAO;
import com.tsp.operation.model.DAOS.Tipo_impuestoDAO;
import com.tsp.operation.model.DAOS.impl.GarantiasComunitariasImpl;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Tipo_impuesto;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.NegociosGenService;
import com.tsp.util.LogWriter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mcastillo
 */
public class HIndemnizarFianzaMicro implements Runnable{

    Model model;
    NegociosGenService negserv;
    GarantiasComunitariasDAO dao;
    String empresa_fianza, mora, periodoCorte, procesoName;
    boolean acelerarPagare, gac;
    JsonArray asJsonArray;
    Usuario usuario;
    
    private  PrintWriter pw;
    private  String path;
    private LogWriter   logTrans;
    public HIndemnizarFianzaMicro(String empresa_fianza, String mora, String periodoCorte, boolean acelerarPagare, boolean gac, JsonArray asJsonArray, Usuario usuario) {
        this.negserv = new NegociosGenService(usuario.getBd());
        this.dao = new GarantiasComunitariasImpl(usuario.getBd());
        this.procesoName = "Indemnización Fianza Micro";
        this.empresa_fianza = empresa_fianza;
        this.mora = mora;
        this.periodoCorte = periodoCorte;
        this.acelerarPagare = acelerarPagare;
        this.gac = gac;
        this.asJsonArray = asJsonArray;
        this.usuario = usuario;
    }
    
    @Override
    public void run() {
        try {
            String comentario="PROCESO EXITOSO";
            model = new Model(usuario.getBd());
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(), this.procesoName, this.usuario.getLogin() );
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            path = ruta + "/exportar/migracion/" + this.usuario.getLogin();
            File archivo = new File( path );
            if (!archivo.exists()) archivo.mkdirs();        
            initLog();  // preparando archivo de log
            this.indemnizarFacturasFianzaMC(this.empresa_fianza, this.mora, this.periodoCorte, this.acelerarPagare, this.asJsonArray, this.usuario);
            closeLog(); // cerrando archivo de log
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);
        } catch (Throwable ex) {
            try {
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo IndemnizarFianzaMicro: " + ex.getMessage());
            } catch (Throwable e) {
                System.out.println("error en hilo indemnizar fianza micro: " + e.toString());
                Logger.getLogger(HIndemnizarFianzaMicro.class.getName()).log(Level.SEVERE, null, e);
            }           
        }
    }
    
    
    /**
     * indemniza las facturas de fianza
     * @throws Exception
     */
    private void indemnizarFacturasFianzaMC(String empresa_fianza, String mora, String periodoCorte, boolean acelerarPagare, JsonArray asJsonArray, Usuario usuario) throws Exception {
        try { 
                if(acelerarPagare){
                    causarInteresesCatPendientes(empresa_fianza, asJsonArray, usuario);          
                 }else{
                     TransaccionService tservice = new TransaccionService(usuario.getBd());
                     tservice.crearStatement();
                     for (int i = 0; i < asJsonArray.size(); i++) {
                         JsonObject objects = (JsonObject) asJsonArray.get(i);
                         tservice.getSt().addBatch(dao.getInsertDataBaseJson("GUARDAR_FACTURAS", empresa_fianza, objects, usuario));
                     }
                     tservice.execute();
                     tservice.closeAll();
                 }      

                String[] splitIC =  dao.crearICIndemnizacionFianza(acelerarPagare, usuario).split(";");               
                if(splitIC[0].equals("OK")){
                    logTrans.log(splitIC[1], logTrans.INFO);  
                    String[] split =  dao.crearCxCIndemnizacionFianza(acelerarPagare, usuario).split(";");
                    if(split[0].equals("OK")){
                          logTrans.log("CxC Generada "+split[1], logTrans.INFO);  
                          generarPdfCxCGarantias(acelerarPagare, mora, periodoCorte, split[1]);
                    }else{
                        logTrans.log("Error generando la cxc ", logTrans.ERROR);  
                    }
                }else{
                     logTrans.log("Error al generar IC ", logTrans.ERROR);  
                }
        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Causa los intereses del numero de cuotas digitado
     * @throws Exception
     */
    private void causarInteresesCatPendientes(String empresa_fianza, JsonArray asJsonArray, Usuario usuario) throws Exception {
       
        TransaccionService tService = new TransaccionService(usuario.getBd());
        tService.crearStatement();       
        ArrayList<BeanGeneral> interesesPendList;
        ArrayList<BeanGeneral> catPendList;
        for (int j = 0; j < asJsonArray.size(); j++) {
            JsonObject objects = (JsonObject) asJsonArray.get(j);
            String negocio = objects.get("negocio").getAsString();
            String periodo_foto = objects.get("periodo_foto").getAsString();          
            interesesPendList = new ArrayList<BeanGeneral>();
            interesesPendList = negserv.interesesPendientes(negocio); 
 
                try {
                    //Generamos Facturas MI Pendientes para el negocio especificado
                    for (int i = 0; i < interesesPendList.size(); i++) {

                        //Seteamos el usuario que anticipa los intereses
                        interesesPendList.get(i).setValor_19(usuario.getLogin());

                        interesesPendList.get(i).setValor_04(interesesPendList.get(i).getValor_12());
                        double valor = (Double.parseDouble(interesesPendList.get(i).getValor_05()) - Double.parseDouble(interesesPendList.get(i).getValor_06()));

                        String documento = negserv.UpCP(interesesPendList.get(i).getValor_01());

                        tService.getSt().addBatch(negserv.ingresarCXCMicrocreditoPost(interesesPendList.get(i), valor, documento));
                        tService.getSt().addBatch(negserv.ingresarDetalleCXCMicrocreditoPost(interesesPendList.get(i), valor, documento, "1"));

                        valor = Double.parseDouble(interesesPendList.get(i).getValor_06()) + valor;
                        tService.getSt().addBatch(negserv.updateInteresCausado(valor, interesesPendList.get(i).getValor_12(), interesesPendList.get(i).getValor_13(), interesesPendList.get(i).getValor_07()));

                    }
                    
                    catPendList = new ArrayList<BeanGeneral>();
                    catPendList = negserv.catPendientes(negocio);
                    //Generamos Facturas CAT Pendientes para el negocio especificado
                    for (int i = 0; i < catPendList.size(); i++) {

                        catPendList.get(i).setValor_04(catPendList.get(i).getValor_12());
                        catPendList.get(i).setValor_19(usuario.getLogin());

                        Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(usuario.getBd());
                        Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(catPendList.get(i).getValor_09(), catPendList.get(i).getValor_06(), catPendList.get(i).getValor_12());

                        String documento = negserv.UpCP(catPendList.get(i).getValor_01());

                        double ivacat = (Double.parseDouble(catPendList.get(i).getValor_05()) * impuesto.getPorcentaje1()) / (100 + impuesto.getPorcentaje1());
                        double catneto = Double.parseDouble(catPendList.get(i).getValor_05()) - ivacat;

                        tService.getSt().addBatch(negserv.ingresarCXCMicrocreditoPost(catPendList.get(i), Double.parseDouble(catPendList.get(i).getValor_05()), documento));
                        tService.getSt().addBatch(negserv.ingresarDetalleCXCMicrocreditoPost(catPendList.get(i), catneto, documento, "1"));

                        catPendList.get(i).setValor_08(impuesto.getCod_cuenta_contable());

                        tService.getSt().addBatch(negserv.ingresarDetalleCXCMicrocreditoPost(catPendList.get(i), ivacat, documento, "2"));
                        tService.getSt().addBatch(negserv.updateDocumentoCat(documento, catPendList.get(i).getValor_13(), catPendList.get(i).getValor_07()));

                    }
                    
                    tService.getSt().addBatch(dao.getInsertFacturasXindemnizarMicro("GUARDAR_FACTURAS_NEGOCIO_ALL", objects, empresa_fianza, usuario));

                } catch (Exception e) {
                    System.out.println("error en action: " + e.toString());
                    e.printStackTrace();
                }
                
     }
         
     tService.execute();
        
 }
    
        private void generarPdfCxCGarantias(boolean acelerarPagare, String mora, String periodoCorte, String numero_cxc) {    

        try {
                       
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            String directorioArchivos = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin() + "/";//se establece la ruta de la imagen            
            this.createDir(directorioArchivos);
            dao.generarCxCGarantias(acelerarPagare, numero_cxc, directorioArchivos, periodoCorte, mora, usuario.getLogin());           
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
        
    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
     /**
     * Inicia el log de importaciones
     * @throws Exception .
     */    
    public void initLog ()throws Exception{
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");               
        pw     = new PrintWriter (new BufferedWriter(new FileWriter(path + "/logIndemnizacionFianzaMicro_"+ fmt.format(new Date()) +".txt")));
        logTrans = new LogWriter("IndemnizacionFianzaMicro", LogWriter.INFO , pw ); 
        logTrans.log( "Proceso Inicializado", logTrans.INFO );
    }
    
    /**
     * Cierra el log general de importaciones
     * @throws Exception .
     */    
    public void closeLog() throws Exception{        
        logTrans.log( "Proceso Finalizado", logTrans.INFO );
        pw.close();
    }
    
}
