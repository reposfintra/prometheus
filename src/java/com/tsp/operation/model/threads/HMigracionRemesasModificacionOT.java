/*
 * HMigracionRemesasAnuladas.java
 *
 * Created on 16 de julio de 2005, 04:37 PM
 */

package com.tsp.operation.model.threads;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import java.util.Vector;
import java.io.*;
import org.apache.log4j.Logger;


/**
 *
 * @author  Henry
 */
public class HMigracionRemesasModificacionOT extends Thread{
    static Logger logger = Logger.getLogger(HMigracionRemesasModificacionOT.class);
    private Vector remesasOT;
    
    private FileWriter fw;
    private BufferedWriter bffw;
    private PrintWriter pntw;
    
    private String usuario;
    private String path;
    
    private String fechaActual;
    private String fechaIni;
    
    /** Creates a new instance of HMigracionRemesasAnuladas */
    public HMigracionRemesasModificacionOT() {
    }
    
    public void start(String fechaIni, String fechaActual,String u) {
        
        this.usuario = u;
        this.fechaActual=fechaActual;
        this.fechaIni=fechaIni;
        
        
        super.start();
    }
    
    public synchronized void run(){
        Model model= new Model();
        
        try{
            
            model.LogProcesosSvc.InsertProceso("Generacion MODIF620", this.hashCode(), "Migracion Modificacion Remesas", usuario);
            
            logger.info("BUSCANDO DATOS DE OTS");
            remesasOT= model.migracionOTsModifService.obtenerRemesasModificacionOTs(fechaIni, fechaActual);
            logger.info("FIN BUSCANDO DATOS DE OTS");
            try{
                //sandrameg 190905
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                path = rb.getString("ruta");
                
                String nombre_archivo = "";
                String ruta = path + "/exportar/migracion/" + usuario + "/";
                String fecha_actual = Util.getFechaActual_String(6);
                //ruta = ruta + fecha_actual.substring(0,4) + fecha_actual.substring(5,7);
                nombre_archivo = ruta + "/MODIF620 " + fecha_actual.replace(':','_').replace('/','_') + ".csv";
                File archivo = new File(ruta.trim());
                
                archivo.mkdirs();
                FileOutputStream file = new FileOutputStream(nombre_archivo.trim());
                
                fw = new FileWriter(nombre_archivo);
                bffw = new BufferedWriter(fw);
                pntw = new PrintWriter(bffw);
            }
            catch(java.io.IOException e){
                ////System.out.println(e.toString());
            }
            
            this.writeFile();
            
            //Migracion Remesa no facturadas
            HMigracionRemesaNF hremnf = new HMigracionRemesaNF();
            hremnf.start(model.migracionOTsModifService.obtenerRemesasNF(fechaIni, fechaActual),usuario);
            model.LogProcesosSvc.finallyProceso("Generacion MODIF620", this.hashCode(), usuario, "PROCESO EXITOSO");
            
        }catch(Exception ex){
            try{
                model.LogProcesosSvc.finallyProceso("Generacion MODIF620", this.hashCode(),usuario,"ERROR :" + ex.getMessage());
                
            }catch ( SQLException e) {
                ////System.out.println("Error guardando el proceso");
            }
            ex.printStackTrace();
            
        }
    }
    
    protected void writeFile() {
        String linea = "";
        logger.info("SE VA A ESCRIBIR EL ARCHIVO SE ENCONTRARON "+remesasOT.size()+" DATOS");
        for(int i=0; i<remesasOT.size(); i++){
            
            Vector datos = (Vector) remesasOT.elementAt(i);
            ////System.out.println(datos.size());
            double val = 0;
            
            if (!datos.elementAt(42).toString().equals(""))
                val = Double.parseDouble(""+datos.elementAt(42));
            
            linea = datos.elementAt(0) + "," + datos.elementAt(1) + "," +
            datos.elementAt(2) + "," + datos.elementAt(3) + "," +
            datos.elementAt(4) + "," + datos.elementAt(5) + "," +
            datos.elementAt(6) + "," + datos.elementAt(7) + "," +
            datos.elementAt(8) + "," + datos.elementAt(9) + "," +
            datos.elementAt(10) + "," + datos.elementAt(11) + "," +
            datos.elementAt(12) + "," + datos.elementAt(13) + "," +
            datos.elementAt(14) + "," + datos.elementAt(15) + "," +
            datos.elementAt(16) + "," + datos.elementAt(17) + "," +
            datos.elementAt(18) + "," + datos.elementAt(19) + "," +
            datos.elementAt(20) + "," + datos.elementAt(21) + "," +
            datos.elementAt(22) + "," + datos.elementAt(23) + "," +
            datos.elementAt(24) + "," + datos.elementAt(25) + "," +
            datos.elementAt(26) + "," + datos.elementAt(27) + "," +
            datos.elementAt(28) + "," + datos.elementAt(29) + "," +
            datos.elementAt(30) + "," + datos.elementAt(31) + "," +
            datos.elementAt(32) + "," + datos.elementAt(33) + "," +
            datos.elementAt(34) + "," + datos.elementAt(35) + "," +
            datos.elementAt(36) + "," + datos.elementAt(37) + "," +
            datos.elementAt(38) + "," + datos.elementAt(39) + "," +
            datos.elementAt(40) + "," + datos.elementAt(41) + "," +
            datos.elementAt(42) + "," + datos.elementAt(43) + "," +
            datos.elementAt(44) + "," + datos.elementAt(45) + "," +
            datos.elementAt(46) + "," + datos.elementAt(47) + "," +
            (int)Util.redondear(val,0) + "," + datos.elementAt(49) + "," +
            datos.elementAt(50) + "," + datos.elementAt(51) + "," +
            datos.elementAt(52) + "," + datos.elementAt(53) + "," +
            datos.elementAt(54) + "," + datos.elementAt(55) + "," +
            datos.elementAt(56) + "," + datos.elementAt(57);
            logger.info("SE ESCRIBIO "+linea);
            pntw.println(linea);
        }
        pntw.close();
    }
    
}
