/******************************************************************
* Nombre                        EstCargaPlacaTh.java
* Descripci�n                   Clase Action para la tabla fitmen
* Autor                         ricardo rosero
* Fecha                         13/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.threads;
import java.awt.event.*;
import javax.swing.Timer;
import java.util.Calendar;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.sql.*;


/**
 *
 * @author  Ricardo Rosero
 */
public class EstCargaPlacaTh extends Thread implements Runnable, ActionListener{
    // Atributos
    // objeto que estar� pendiente de invocar el hilo que ejecuta el proceso
    // todos los fines de mes a las 11:59 pm
    private Timer verificadorDeDias;
    private static final int SEGUNDO = 1000;
    private static final int MINUTO = 60 * SEGUNDO;
    private static final int HORA = 60 * MINUTO; 
    private static final int DIA = 24 * HORA;
    private Object bloqueador = new Object();

    
    public EstCargaPlacaTh() {
        // se crea el verificador de dias para que se ejecute cada 24 horas
        verificadorDeDias = new Timer(DIA,this);
        // establecemos que siempre se repetira este proceso
        verificadorDeDias.setRepeats(true);

        // es necesario que el el programa verifique que dia es hoy solamente
        // a las 11:59 pm es decir a las 23:59.
        // Para hacer eso calculamos la diferencia de tiempo que hay desde
        // ahora, es decir la primera vez que se ejecuta el programa hasta
        // las 11:59 pm proxima.
        Calendar ahora = Calendar.getInstance();
        Calendar las11y59DeHoy = Calendar.getInstance();
        las11y59DeHoy.set(Calendar.HOUR_OF_DAY,23);
        las11y59DeHoy.set(Calendar.MINUTE,59); 
        las11y59DeHoy.set(Calendar.SECOND,59);
        long tiempoRestante = las11y59DeHoy.getTimeInMillis() - ahora.getTimeInMillis();
        ////System.out.println("timepo restante = "+tiempoRestante);
        verificadorDeDias.setInitialDelay((int)tiempoRestante);
        verificadorDeDias.start();
        ////System.out.println("esperando por siempre");
        esperarPorSiempre();
        ////System.out.println("fin del programa"); 
    }

    /**
    * esperarPorSiempre
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... Exception
    * @version ...... 1.0
    */   
    private void esperarPorSiempre(){
        synchronized(bloqueador){
            try {
                bloqueador.wait();
            }
            catch( Exception ex ){
                ex.printStackTrace();
            }
        }
    }

    /**
    * run
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... Exception
    * @version ...... 1.0
    */   
    public void run(){
        // Aqui se coloca lo que se ejecutar� los fines de mes
        
        try{
            Model model = new Model ();
            model.ecpService.eliminarEstCargaPlaca();
            model.ecpService.insertar(); 
            
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        
        ////System.out.println("me llamaron a las: "+Calendar.getInstance());
    }

    /**
    * actionPerformed
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ java.awt.event.ActionEvent actionEvent
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void actionPerformed(java.awt.event.ActionEvent actionEvent) {
        Calendar ahora = Calendar.getInstance(); //ahora.getActualMaximum(Calendar.DAY_OF_MONTH) ) {
        if ( ahora.get(Calendar.DAY_OF_MONTH) == ahora.getActualMaximum(Calendar.DAY_OF_MONTH) ) {
            Thread proceso = new Thread(this);
            proceso.start();
        }
    }

    /**
     * Metodo ejecutable de java
     */
    public static void main( String [] args ){
        new EstCargaPlacaTh();
    }

}