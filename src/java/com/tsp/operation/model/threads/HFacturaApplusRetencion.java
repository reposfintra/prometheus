/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;



import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;

import java.util.*;
import java.text.*;




/**
 *
 * @author Alvaro
 */
public class HFacturaApplusRetencion extends Thread{


    private Model   model;
    private Usuario usuario;
    private String  dstrct;
    private String  fechaInicial;
    private String  fechaFinal;
    private String  tipoFactura;

    /** Creates a new instance of HFacturaApplus */
    public HFacturaApplusRetencion () {
    }

    public void start(Model model, Usuario usuario, String distrito,
                      String fechaInicial,String fechaFinal , String tipoFactura){

        this.usuario = usuario;
        this.model = model;
        this.dstrct = distrito;
        this.fechaInicial = fechaInicial;
        this.fechaFinal = fechaFinal;
        this.tipoFactura = tipoFactura;

        super.start();
    }


    public synchronized void run(){
        try{
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del reporte de prefactura : " + prefactura, usuario.getLogin());

            Vector comandos_sql =new Vector();
            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();



            List listaFacturaApplus =  model.applusService.getFacturaApplus(fechaInicial,fechaFinal,tipoFactura);

            if (listaFacturaApplus.size() != 0){

                FacturaApplus facturaApplus = new FacturaApplus();
                Iterator it = listaFacturaApplus.iterator();

                String comandoSQL = "";

                while (it.hasNext()  ) {

                    facturaApplus = (FacturaApplus)it.next();

                    // Datos comunes a la cabecera y a los items


                    String id_contratista = facturaApplus.getId_contratista();
                    String prefactura     = facturaApplus.getPrefactura();
                    String fact_conformada= facturaApplus.getFact_conformada();

                    // Cambiar el nit al de APPLUS
                    String proveedor = "9002335631";//090805
                    String tipo_documento = "FAP";
                    // Buscar un consecutivo

                    SerieGeneral serie = model.serieGeneralService.getSerie("FINV", "OP", "FAPAR");
                    model.serieGeneralService.setSerie("FINV", "OP", "FAPAR");

                    String documento      = serie.getUltimo_prefijo_numero();
                    String user_update = usuario.getLogin();
                    String creation_user = user_update;
                    String base = "COL";



                    // CREACION DE LA CABECERA EN CXP_DOC

                    // Datos de la cabecera


                    String descripcion    = "Factura por retencion";
                    String agencia        = "BQ";
                    Proveedor objeto_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
                    //String handle_code = objeto_proveedor.getC_hc();
                    
                    String handle_code = "MR";
                    
                    String aprobador   = "JGOMEZ";
                    String usuario_aprobacion = "JGOMEZ";
                    String banco = objeto_proveedor.getC_branch_code();
                    String sucursal = objeto_proveedor.getC_bank_account();
                    String moneda = "PES";

                    double vlr_mat = facturaApplus.getVlr_mat();
                    double vlr_mob = facturaApplus.getVlr_mob();
                    double vlr_otr = facturaApplus.getVlr_otr();


                    double vlr_rmat           = -facturaApplus.getVlr_rmat();
                    double vlr_rmob           = -facturaApplus.getVlr_rmob();
                    double vlr_rotr           = -facturaApplus.getVlr_rotr();


                    double vlr_neto  =  vlr_rmat + vlr_rmob + vlr_rotr ;
                    double vlr_total_abonos = 0;
                    double vlr_saldo = vlr_neto;
                    double vlr_neto_me = vlr_neto;
                    double vlr_total_abonos_me = 0;
                    double vlr_saldo_me = vlr_neto;
                    double tasa = 1;

                    String observacion = "";
                    String clase_documento = "4";
                    // String moneda_banco = objeto_proveedor.getC_currency_bank();
                    String moneda_banco = "PES";
                    String clase_documento_rel = "4";
                    Date fecha_hoy = new Date();
                    DateFormat formato;
                    formato = new SimpleDateFormat("yyyy-MM-dd");
                    String fecha_documento = formato.format(fecha_hoy);
                    Date fecha_pago = fecha_hoy ;
                    String fecha_vencimiento = fecha_documento;

                    comandoSQL = model.applusService.setCxp_doc(dstrct, proveedor, tipo_documento, documento, descripcion,
                            agencia, handle_code, aprobador, usuario_aprobacion, banco, sucursal,
                            moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                            vlr_total_abonos_me, vlr_saldo_me, tasa, observacion, user_update,
                            creation_user, base, clase_documento, moneda_banco,
                            fecha_documento, fecha_vencimiento, clase_documento_rel,
                            creation_date, creation_date);

                    comandos_sql.add(comandoSQL);


                    // CREACION DE LOS ITEMS EN CXP_ITEMS_DOC

                    // Datos comunes a todos los items

                    int item = 0;

                    model.applusService.buscaContratista(id_contratista);
                    Contratista contratista = model.applusService.getContratista();
                    String auxiliar = contratista.getNit();

                    String prefactura_relacionada = "Contratista : " + id_contratista +
                                                    "  Prefactura : " + prefactura +
                                                    "  Factura conformada : " + fact_conformada + "  ";

                    // Item de valor retencion material

                    if (vlr_rmat != 0) {

                        descripcion = prefactura_relacionada + "Retencion por material, Base retencion : " +
                                      Util.FormatoMiles(vlr_mat);
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         descripcion, vlr_rmat, vlr_rmat, "23050702",
                                         user_update, creation_user, base, "108", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }

                     // Item de valor retencion mano de obra
                    if (vlr_rmob != 0) {
                        descripcion =  prefactura_relacionada + "Retencion por mano de obra, Base retencion : " + Util.FormatoMiles(vlr_mob);
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         descripcion, vlr_rmob, vlr_rmob, "23050702",
                                         user_update, creation_user, base, "109", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }

                    // Item de valor retencion otros
                    if (vlr_rotr != 0) {
                        descripcion =  prefactura_relacionada + "Retencion por otros, Base retencion : " + Util.FormatoMiles(vlr_otr);
                        comandoSQL = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),
                                         descripcion, vlr_rotr, vlr_rotr, "23050702",
                                         user_update, creation_user, base, "110", auxiliar,
                                         creation_date, creation_date);
                        comandos_sql.add(comandoSQL);
                    }


                    // ACTUALIZA LAS ACCIONES CON EL NUMERO DE FACTURA Y FECHA DE FACTURA DE APPLUS

                    comandoSQL = model.applusService.setFacturaApplus(documento,creation_date, id_contratista, prefactura,tipoFactura);
                    comandos_sql.add(comandoSQL);

                }
                // Grabando todo a la base de datos.
                model.applusService.ejecutarSQL(comandos_sql);
            }


            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HFacturaApplusRetencion ...\n" + e.getMessage());
            }
        }
    }


}
