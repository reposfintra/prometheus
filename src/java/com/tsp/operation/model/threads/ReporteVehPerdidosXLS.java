/*****************************************************************************
 * Nombre clase :                   ReporteVehPerdidosXLS.java               *
 * Descripcion :                    Hilo que permite la creacion del reporte *
 *                                  de vehiculos perdidos en un              *
 *                                  archivo en excel                         *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          10 de enero de 2006, 11:42 AM            *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 ****************************************************************************/

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import java.sql.*;

public class ReporteVehPerdidosXLS extends Thread{
    private List lista;
    private String id;
    private String procesoName;
    private String des;
    private Model model;
    /** Creates a new instance of ReporteVehPerdidosXLS */
    public ReporteVehPerdidosXLS() {
    }
    
    public void start(List Lista, String id) {
        this.model = new Model();
        this.lista = Lista;
        this.id = id;
        this.procesoName = "Reporte de Vehiculos Perdidos";
        this.des = "Reporte que permite generar un archivo de Excel, con las estadisticas de los Vehiculos Perdidos";
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            
            File file = new File(path + "/exportar/migracion/"+id);
            file.mkdirs();
            
            this.model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, this.id);
            
            String nombreArch= "ReporteVehPerdidos.xls";
            String       Hoja  = "ReportesVehPerdidos";
            String       Ruta  = path + "/exportar/migracion/"+ id +"/" + nombreArch;
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFRow      row   = null;
            HSSFCell     cell  = null;
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x1));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.BLACK.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x0));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            //estilo5.setDataFormat(wb.createDataFormat().getFormat("####-##-## ##:##:##"));
            
            /****************************************************/
            row  = sheet.createRow((short)(0));
            
            for (int j=0;j<10;j++) {
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
            }
            
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            
            sheet.createFreezePane(0,5);
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO - Reporte Vehiculos Perdidos");
            
            
            /*row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("Reporte de Vehiculos Perdidos");*/
            
            /*************************************************************************************/
            
            int Fila = 4;
            
            row  = sheet.createRow((short)(Fila));
            
            sheet.setColumnWidth((short)0, (short)3000 );
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLACA");
            
            sheet.setColumnWidth((short)1, (short)3000 );
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CLASE");
            
            sheet.setColumnWidth((short)2, (short)7000 );
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA DIPONIBILIDAD");
            
            sheet.setColumnWidth((short)3, (short)5000 );
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLANILLA");
            
            
            sheet.setColumnWidth((short)4, (short)8000 );
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PERDIDO EN PROCESO :");
            
            sheet.setColumnWidth((short)5, (short)6000 );
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("ORIGEN");
            
            sheet.setColumnWidth((short)6, (short)6000 );
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESTINO");
            
            sheet.setColumnWidth((short)7, (short)6000 );
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AGENCIA");
            
            Iterator It = this.lista.iterator();
            while(It.hasNext()) {
                ReporteVehPerdidos  ve = (ReporteVehPerdidos) It.next();
                Fila++;
                row  = sheet.createRow((short)(Fila));
                
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue((ve.getPlaca()!=null)?ve.getPlaca():"");
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue((ve.getClase()!=null)?ve.getClase():"");
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo5);
                cell.setCellValue((ve.getFecha_disponibilidad()!=null)?ve.getFecha_disponibilidad():"");
                
                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo4);
                cell.setCellValue((ve.getNumpla()!=null)?ve.getNumpla():"");
                
                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo4);
                cell.setCellValue((ve.getEstado()!=null)?ve.getEstado():"");
                
                cell = row.createCell((short)(5));
                cell.setCellStyle(estilo4);
                cell.setCellValue((ve.getOrigen()!=null)?ve.getOrigen():"");
                
                cell = row.createCell((short)(6));
                cell.setCellStyle(estilo4);
                cell.setCellValue((ve.getDestino()!=null)?ve.getDestino():"");
                
                cell = row.createCell((short)(7));
                cell.setCellStyle(estilo4);
                cell.setCellValue((ve.getAgencia()!=null)?ve.getAgencia():"");
                
            }
            
            model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), this.id, "PROCESO EXITOSO");
            
             /************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream(Ruta);
            wb.write(fo);
            fo.close();
        }catch(Exception e){
            ////System.out.println(e.getMessage());
            try{ 
                model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (),this.id,"ERROR :" + e.getMessage ());  
            }catch(SQLException ex){}
        }finally{
            super.destroy();
        }
    }
    
}
