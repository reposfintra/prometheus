/***************************************
 * Nombre       HFacturasAdicionalOP.java 
 * Autor        FERNEL VILLACOB DIAZ
 * Fecha        29/11/2005
 * versi�n      1.0
 * Copyright    Transportes Sanchez Polo S.A.
 *******************************************/





package com.tsp.operation.model.threads;




import java.io.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;



public class HFacturasAdicionalOP  extends Thread{
    
    private Model         model;
    private String        usuario;
    private String        distrito;
    private String        procesoName;
    
    private String        PROCEDENCIA = "ADICION";
    
    public Usuario user;
    
    public HFacturasAdicionalOP() {
        procesoName = "ADICION FACTURAS OP";
    }
    
    
    
    
    
    
    public void start(Model modelo, Usuario user, String distrito) throws Exception{
        try{
            this.model           = modelo;
            this.usuario         = user.getLogin();
            this.user            = user;
            this.distrito        = distrito;
            super.start();
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    /**
     * M�todo para ejecutarlo Automatico
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public  void Automatico()throws Exception{
        try{
        
            this.model           =  new Model();
            this.usuario         =  "ADMIN";
            this.distrito        =  "FINV";
            this.procesoName     =  "ADICION FACTURAS OP - AUTOMATICO";
            super.start();
            
        }catch(Exception e){
            throw new Exception( e.getMessage() ); 
        }
    }
    
    
    
    
    
    
    public static void main(String[] args)throws Exception{
        try{
             HFacturasAdicionalOP  variable =  new  HFacturasAdicionalOP();
             variable.Automatico();            
        }catch(Exception e){
           throw new Exception( e.getMessage() );  
        }
    }
    
    
    
    
    
    
    
    
    /**
     * M�todo que ejecuta el proceso
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public synchronized void run(){
        try{
             
            String comentario  = "Proceso Exitoso!";
            String fecha       = Util.getFechaActual_String(6).replaceAll("/|:","");
            
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), fecha ,this.usuario);
                        
            
         // Creamos directorio del usuario.
            model.DirectorioSvc.create(this.usuario );
            
            
         // GENERACION FACTURAS ADICIONAL A  DE OPS:
            
            // Creamos los archivoa a generar.
            String file      = model.DirectorioSvc.getUrl() + this.usuario + "/FACT_ADICIONAL"     + fecha +".xls";   // Excel con facturas generadas
            String fileError = model.DirectorioSvc.getUrl() + this.usuario + "/FACT_ADICIONAL"     + fecha +".txt";   // Planilla que no se pudo generar facturas por Error
           
            
            
           // Buscamos las facturas. 
            model.AdicionOPSvc.generarFacturasAdicionOP(distrito);    
            List lista = model.AdicionOPSvc.getListOCs();
            
            if (lista != null && lista.size() > 0){                
                model.OpSvc.insertList(lista, this.user, file, fileError, PROCEDENCIA);
            }else
                comentario += " -> No se encontraron movimientos de planillas validos";            
            
            
            
            
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usuario,comentario);
            
             
        }catch(Exception e){
            model.LiqOCSvc.setEnproceso();
            try{model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usuario,"ERROR Hilo: " + e.getMessage()); }
            catch(Exception f){}
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
