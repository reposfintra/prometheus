/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.tsp.operation.model.threads;


import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;

import java.util.*;
import java.text.*;

import java.io.*;
import com.tsp.util.LogWriter;


/**
 *
 * @author Alvaro
 */
public class HGenerarFacturaEcaOpav extends Thread {

    private Model model;
    private Usuario usuario;


    /** Creates a new instance of HPrefacturaDetalle */
    public HGenerarFacturaEcaOpav () {
    }

    public void start(Model model, Usuario usuario){

        this.usuario = usuario;
        this.model = model;

        super.start();
    }


    public synchronized void run(){
        try{
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del reporte de prefactura : " + prefactura, usuario.getLogin());

            System.out.println("Paso 0)");


            // -------------------------------------------------------------------------
            // DEFINICION DEL LOG DE ERROR
            // Variables
            //
            PrintWriter pw;
            LogWriter logWriter;
            java.text.SimpleDateFormat formatoFecha;
            String fechaDocumento;
            java.util.Date fecha;

            // Abriendo un log de error
            String user         = usuario.getLogin();
            ResourceBundle rb   = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaInformes = rb.getString("ruta")+ "/exportar/migracion/"+user;

            File file = new File(rutaInformes);
            file.mkdirs();
            pw        = new PrintWriter(System.err, true);

            Calendar fechaProceso  = Calendar.getInstance();
            fecha                  = fechaProceso.getTime();
            formatoFecha           = new java.text.SimpleDateFormat("yyyy-MM-dd HHmm");
            fechaDocumento         = formatoFecha.format(fecha);

            String logFile   = rutaInformes + "/Generacion de registros de facturas (Reporte de Fiducia) "+fechaDocumento+".txt";

            pw        = new PrintWriter(new FileWriter(logFile, true), true);
            logWriter = new LogWriter("RF", LogWriter.INFO, pw);
            logWriter.setPrintWriter(pw);

            // FIN DEFINICION DEL LOG DE ERROR
            // -------------------------------------------------------------------------


            logWriter.log("****************************************************************",LogWriter.INFO);
            logWriter.log("*  PARA GENERACION DE REGISTROS DE FACTURA (REPORTE FIDUCIA    *",LogWriter.INFO);
            logWriter.log("*  FechaProceso :  "+fechaDocumento+"                             *",LogWriter.INFO);
            logWriter.log("****************************************************************" +"\n",LogWriter.INFO);





            Vector comandos_sql =new Vector();
            String comandoSQL = "";
            List listaSubclientePorFacturar = null;
            
            // Elabora una lista de solicitudes que se van a facturar
            model.consorcioService.buscaSolicitudParcialPorFacturar();
            logWriter.log("LISTA DE SOLICITUDES PARCIALES POR FACTURAR CREADA \n",LogWriter.INFO);

            List listaSolicitudParcialPorFacturar =  model.consorcioService.getSolicitudParcialPorFacturar();


            System.out.println("Paso 1)");

            if (listaSolicitudParcialPorFacturar.size() != 0){


                SolicitudParcialPorFacturar solicitudParcialPorFacturar =  new SolicitudParcialPorFacturar();
                SubclientePorFacturar subclientePorFacturar = new SubclientePorFacturar();
                FacturaConsorcio facturaConsorcio = new FacturaConsorcio();
                SerieGeneral serie = new SerieGeneral();
                TotalesConsorcio totalesConsorcio = new TotalesConsorcio();


                Iterator it = listaSolicitudParcialPorFacturar.iterator();
                while (it.hasNext()) {


                  // Una solicitud parcial para facturar
                  solicitudParcialPorFacturar = (SolicitudParcialPorFacturar) it.next();


                  // Elabora una lista de subclientes a facturar por cada solicitud parcial
                  String id_solicitud = solicitudParcialPorFacturar.getId_solicitud();
                  int parcial         = solicitudParcialPorFacturar.getParcial();

                  logWriter.log("INICIA PROCESO PARA SOLICITUD : " +id_solicitud + " - " + parcial+ "\n",LogWriter.INFO);



                  model.consorcioService.buscaSubclientePorFacturar(id_solicitud, parcial);
                  listaSubclientePorFacturar = model.consorcioService.getSubclientePorFacturar();

                  Iterator it0 = listaSubclientePorFacturar.iterator();
                  while (it0.hasNext()) {
                      
                      // UN SUBCLIENTE A FACTURAR

                      String errorEnSubcliente = "NO";
                      subclientePorFacturar = (SubclientePorFacturar) it0.next();

                      logWriter.log("   Inicia factura para subcliente : " + subclientePorFacturar.getId_cliente() + "\n",LogWriter.INFO);

                      serie = model.serieGeneralService.getSerie("FINV","OP","FMSOP");
                      model.serieGeneralService.setSerie("FINV","OP","FMSOP");
                      String documento_inicial = serie.getUltimo_prefijo_numero();

                      // Controla totales de la factura para evitar perdida por redondeo. Inicializa los valores totales y las variables que acumulan
                      totalesConsorcio.setValorInicial(subclientePorFacturar.getVal_a_financiar(),3);
                      totalesConsorcio.setValorInicial(subclientePorFacturar.getIntereses(),4);
                      totalesConsorcio.setValorInicial(subclientePorFacturar.getVal_con_financiacion(),5);
                      totalesConsorcio.setValorTotal(0.00, 3);
                      totalesConsorcio.setValorTotal(0.00, 4);
                      totalesConsorcio.setValorTotal(0.00, 5);




                      // Genera registros de facturas de la fiducia.
                      for(int numeroCuota = 1 ; numeroCuota <=  subclientePorFacturar.getPeriodo() ; numeroCuota++) {

                          String documento = documento_inicial + "_" + Integer.toString(numeroCuota);
                          logWriter.log("    Inicia creacion factura : " + documento + "\n",LogWriter.INFO);
                          String error = model.consorcioService.creaFacturaFiducia(subclientePorFacturar,facturaConsorcio, documento, numeroCuota, totalesConsorcio );

                          if(error.equalsIgnoreCase("NO")){
                             comandoSQL =  model.consorcioService.insertarFacturaConsorcio(facturaConsorcio);
                             logWriter.log("    Factura agregada al batch de SQL \n",LogWriter.INFO);
                             comandos_sql.add(comandoSQL);
                          }
                          else {
                             errorEnSubcliente = "SI";
                             logWriter.log("ERROR: Se genero error en la factura \n",LogWriter.INFO);

                             break;
                          }
                      } // Final del for que genera tantas facturas como cuotas haya


                      if(errorEnSubcliente.equalsIgnoreCase("NO")){
                          // Grabando todo a la base de datos.
                          comandoSQL = model.consorcioService.setSolicitudSubcliente(subclientePorFacturar, documento_inicial);
                          logWriter.log("    Actualizacion de subclientes con la factura agregada al batch de SQL \n",LogWriter.INFO);

                          // System.out.println(comandoSQL+ "\n");
                          comandos_sql.add(comandoSQL);

                          String estado = model.consorcioService.ejecutarBatchSQL(comandos_sql);

                          if (estado.equalsIgnoreCase("ERROR")){
                              logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. Se efectuo rollback \n",LogWriter.INFO);
                          }
                          else{
                              logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                          }
                      }
                      else {
                          logWriter.log("ERROR: Subcliente generado con error. Se descarto toda actualizacion para la solicitud \n",LogWriter.INFO);
                      }

                      // Inicializa el vector para el proximo proceso del subcliente
                      if(comandos_sql.size() > 0) { comandos_sql.removeAllElements(); }

                  } // Final del while que recorre la lista de subclientes de una solicitud a facturar



                  logWriter.log("FINAL  PROCESO PARA SOLICITUD : " +id_solicitud + " - " + parcial+ "\n",LogWriter.INFO);


                } // Final del while que recorre la lista de solicitudes a facturar



            } //




            logWriter.log("****************************************************************",LogWriter.INFO);
            logWriter.log("*  FINAL DEL PROCESO " + fechaDocumento + "                           *",LogWriter.INFO);
            logWriter.log("****************************************************************",LogWriter.INFO);




            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HGenerarFacturaEca ...\n"  + e.getMessage());
            }
        }
    }


    public void expandirTotalPrev1(){




    }







}
