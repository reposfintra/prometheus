/******************************************************************
* Nombre ......................ReporteInventarioSoftwareXLS.java
* Descripci�n..................Clase utilizada para generar un reporte de excel que contiene todos los archivos del sitio
* Autor........................Ing. Armando Oviedo
* Fecha........................26/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;
/**
 *
 * @author  Armando Oviedo
 */
public class ReporteInventarioSoftwareXLS extends Thread{
    
    private String procesoName;
    private String des;
    String id="";
    Usuario user;
    Vector listaprogramas;
    
    /** Creates a new instance of ReporteInventarioSoftwareXLS */
    public ReporteInventarioSoftwareXLS() {
    }
    
    /**
     * M�todo que inicia el hilo para generar el reporte de sitio completo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @param.......String id
     **/ 
    public void start(Usuario user, Vector listaProgramas){                
        this.id= user.getLogin();
        this.user = user;
        this.procesoName = "Inventario De Software";
        this.des = "Inventario detallado de archivos que se encuentran en el sitio slt";
        this.listaprogramas = listaProgramas;
        super.start();
    }
    
    /**
     * M�todo que inicia el hilo para generar el reporte de sitio completo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @param.......String id
     **/ 
    public synchronized void run(){
        try{         
            Model model = new Model();
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");            
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);                                    
            String fecha_actual = Util.getFechaActual_String(6);
            if(listaprogramas!=null && listaprogramas.size()>0){                
                    String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                    String a�o = fecha_actual.substring(0,4);
                    String mes = fecha_actual.substring(5,7);
                    String dia = fecha_actual.substring(8,10);
                    String hora = fecha_actual.substring(11,13);
                    String min = fecha_actual.substring(14,16);
                    String seg = fecha_actual.substring(17,19);
                    File file = new File(path);
                    file.mkdirs();                    
                    POIWrite xls = new POIWrite(path+"/InventarioSoftware"+a�o+mes+dia+" "+hora+min+seg+".xls","","");
                    // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
                    HSSFCellStyle fecha  = xls.nuevoEstilo("verdana", 12, false , false, "yyyy/mm/dd hh:mm:ss"  , xls.NONE , xls.NONE , xls.NONE );
                    HSSFCellStyle texto  = xls.nuevoEstilo("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                    HSSFCellStyle textopags  = xls.nuevoEstilo("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);                    
                    HSSFCellStyle numero = xls.nuevoEstilo("verdana", 12, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                    HSSFCellStyle negrita      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE);
                    HSSFCellStyle header      = xls.nuevoEstilo("verdana", 18, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
                    HSSFCellStyle titulo      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER);
                    HSSFCellStyle fechatitle  = xls.nuevoEstilo("verdana", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );


                    xls.obtenerHoja("Inventario Software");
                    xls.cambiarMagnificacion(3,4);

                    // cabecera

                    xls.adicionarCelda(0,0, "INVENTARIO DE PROGRAMAS", header);
                    xls.combinarCeldas(0, 0, 0, 7);

                    // subtitulos

                    xls.adicionarCelda(2,0, "Fecha Proceso: "   , negrita);
                    xls.adicionarCelda(2,1, fecha_actual , fecha);
                    xls.adicionarCelda(3,0, "Elaborado Por: "   , negrita);
                    xls.adicionarCelda(3,1, id , fecha);
                    
                    int fila = 5;
                    int col  = 0;   
                    int tamano= 0;                    
                    
                    xls.adicionarCelda(fila ,col++ , "Equipo", titulo );
                    xls.adicionarCelda(fila ,col++ , "Sitio", titulo );
                    xls.adicionarCelda(fila ,col++ , "Nombre Programa", titulo );
                    xls.adicionarCelda(fila ,col++ , "Extensi�n Del Archivo", titulo );
                    xls.adicionarCelda(fila ,col++ , "Fecha Modificaci�n", titulo );
                    xls.adicionarCelda(fila ,col++ , "A�o", titulo );
                    xls.adicionarCelda(fila ,col++ , "Mes", titulo );
                    xls.adicionarCelda(fila ,col++ , "Dia", titulo );
                    xls.adicionarCelda(fila ,col++ , "Tama�o (bytes)", titulo );
                    xls.adicionarCelda(fila ,col++ , "Nivel 1", titulo );
                    
                    for(int i=0;i<listaprogramas.size();i++){
                        fila++;
                        col = 0;
                        GregorianCalendar gc = new GregorianCalendar();
                        InventarioSoftware invsoft = (InventarioSoftware)(listaprogramas.elementAt(i));
                        gc.setTimeInMillis(invsoft.getFecha().getTime());
                       
                        xls.adicionarCelda(fila ,col++ , invsoft.getEquipo(), textopags );
                        xls.adicionarCelda(fila ,col++ , invsoft.getSitio(), textopags );
                        xls.adicionarCelda(fila ,col++ , invsoft.getNombre(), textopags );
                        xls.adicionarCelda(fila ,col++ , invsoft.getExtension(), texto );
                        xls.adicionarCelda(fila ,col++ , invsoft.getFecha(), fecha );
                        xls.adicionarCelda(fila ,col++ , gc.get(Calendar.YEAR), texto );
                        xls.adicionarCelda(fila ,col++ , (gc.get(Calendar.MONTH)+1), texto );
                        xls.adicionarCelda(fila ,col++ , gc.get(Calendar.DATE), texto );
                        xls.adicionarCelda(fila ,col++ , invsoft.getTamano(), texto );                        
                        String profundidad[] = invsoft.getProfundidad();
                        xls.adicionarCelda(fila ,col++ , profundidad[0]+"/"+profundidad[1]+"/"+profundidad[2] , texto );                        
                        for(int j=3;j<profundidad.length-1;j++){
                            xls.adicionarCelda(5 ,(j+5) , "Nivel "+(j-1) , titulo );
                            xls.adicionarCelda(fila ,col++ , profundidad[j] , texto );                                                    
                        }
                    }                    
                    xls.cerrarLibro();                    
            }   
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "PROCESO EXITOSO");
        }catch(Exception e){
            e.printStackTrace();
            try{
                Model model = new Model();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
            }catch(Exception ex){
                try{
                    Model model = new Model();
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
}
