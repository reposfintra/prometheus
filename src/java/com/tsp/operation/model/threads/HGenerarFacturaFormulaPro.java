/* * HGenerarFacturaFormulaPro.java * Created on 27 de mayo de 2009, 16:34 */
package com.tsp.operation.model.threads;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.util.Utility;
/** * @author  Fintra */
public class HGenerarFacturaFormulaPro  extends Thread{    
    private Model   model;
    private Usuario usuario;
    private String  dstrct;
    public HGenerarFacturaFormulaPro() {    }   
    
    public void start(Model model, Usuario usuario, String distrito){
        this.usuario = usuario;
        this.model = model;
        this.dstrct = distrito;
        super.start();
    }
    
    public synchronized void run(){  
        //.out.println("run");
        SerieGeneral serie = null;
        serie = new SerieGeneral();
                
        LogProcesosService log = new LogProcesosService(usuario.getBd());        
        try{            
            String hoy  =  Utility.getHoy("-");
            String ayer =  Utility.convertirFecha( hoy, -1);
            String comentario="EXITOSO";
            log.InsertProceso("FormulaProvintegral", this.hashCode(), hoy ,usuario.getLogin()); 
            
            Vector comandos_sql =new Vector();            
            String comandoSQL = "";            
            List cxps=model.negociosApplusService.obtainCxpsContratista();            
            String proveedor = "9000742958"; 
            String auxiliar  = proveedor;
            String tipo_documento = "FAP";
            String documento="";
            String descripcion    ="";
            String agencia        = "BQ";
            
            Proveedor objeto_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
            
            String handle_code = "CF";//TEM

            String aprobador   = "JGOMEZ";
            String usuario_aprobacion = "JGOMEZ";
            String banco = objeto_proveedor.getC_branch_code();
            String sucursal = objeto_proveedor.getC_bank_account();
            String moneda = "PES";
            String observacion="";
            Double vlr_neto ;
            Double abono=Double.valueOf("0");
            Double tasita=Double.valueOf("1");
            
            String user_update = usuario.getLogin();
            String creation_user = user_update;
            String base = "COL";
            
            String clase_documento = "4";
            
            String moneda_banco = "PES";
            String clase_documento_rel = "4";

            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();
            //.out.println("antes de for");
            for (int i=0;i<cxps.size();i++ ){
                
                FacturaContratista facturaContratista = (FacturaContratista)cxps.get(i);
                    
                    serie = model.serieGeneralService.getSerie("FINV","OP","FAPPF");
                    //.out.println("serie"+serie);
                    model.serieGeneralService.setSerie("FINV","OP","FAPPF");
                    documento= serie.getUltimo_prefijo_numero(); 
                    descripcion    = "Factura por formula en cxp : " + facturaContratista.getFacturaContratista()+ " del contratista " + facturaContratista.getIdContratista() +".";
                    vlr_neto   =Double.valueOf(facturaContratista.getValor());
                             
                    
                    observacion=descripcion;
                    
                    comandoSQL = model.applusService.setCxp_doc(dstrct, proveedor, tipo_documento, documento, descripcion,
                            agencia, handle_code, aprobador, usuario_aprobacion, banco, sucursal,
                            moneda, vlr_neto, abono, vlr_neto, vlr_neto,
                            abono, vlr_neto, tasita, observacion, user_update,
                            creation_user, base, clase_documento, moneda_banco,
                            facturaContratista.getFecFacContratista(), facturaContratista.getFecFacContratista(), clase_documento_rel,
                            creation_date, creation_date);
                    //.out.println("comandoSQL1"+comandoSQL);
                    comandos_sql.add(comandoSQL);
                                        
                    comandoSQL  = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,"1",  descripcion,
                                         vlr_neto, vlr_neto, "28150501",
                                         user_update, creation_user, base, "089", auxiliar,
                                         creation_date, creation_date);
                    //.out.println("comandoSQL2"+comandoSQL);
                    comandos_sql.add(comandoSQL);
                    
                    comandoSQL  = model.negociosApplusService.updateAccord(facturaContratista.getFacturaContratista(),documento);
                    comandos_sql.add(comandoSQL);
                    //.out.println("comandoSQL3"+comandoSQL);

                    
                //}
                //comandos_sql.add(cruceFacIng);
                
            }           
            model.applusService.ejecutarSQL(comandos_sql);
            //.out.println("despues de sql");
            //comandoSQL =model.applusService.setFacturaContratista(null,null, null, null);
            //.out.println("fin de run");
               
            log.finallyProceso("FormulaProvintegral", this.hashCode(), usuario.getLogin(),comentario);
            
            // Grabando todo a la base de datos.
            //model.applusService.ejecutarSQL(comandos_sql);
        }catch (Exception ex){
            System.out.println("error en h:"+ex.toString()+"__"+ex.getMessage());            
            try{
                log.finallyProceso("FormulaProvintegral",this.hashCode(), usuario.getLogin(),"ERROR Hilo: " + ex.getMessage()); 
            }catch(Exception w){
                System.out.println("errorrinho"+w.toString());
            }
        }
    }

}