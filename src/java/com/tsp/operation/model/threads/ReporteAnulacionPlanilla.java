/***************************************************************************
 * Nombre clase : ............... ReporteAnulacionPlanilla.java            *
 * Descripcion :................. Hilo que permite la creacion del reporte *
 *                                de viajes en un archivo en excel         *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 23 de noviembre de 2005, 03:16 PM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import org.apache.log4j.*;

public class ReporteAnulacionPlanilla extends Thread{
    Logger logger = Logger.getLogger(this.getClass());
    
    private List Lista;
    private String fechainicio;
    private String fechafin;
    private String id;
    private Model model;
    
    /** Creates a new instance of ReporteAnulacionPlanilla */
    public ReporteAnulacionPlanilla() {
    }
    
    public void start(List Lista, String fechai, String fechaf, String id) {
        this.Lista = Lista;
        this.fechainicio = fechai;
        this.fechafin = fechaf;
        this.id = id;
        super.start();
    }
    
    public synchronized void run(){
        try{
            //model.LogProcesosSvc.InsertProceso( "Reporte Planillas Anuladas", this.hashCode(), "Reporte Planillas Anuladas", id );
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            
            File file = new File(path + "/exportar/migracion/"+id);
            file.mkdirs();
            
            String nombreArch= "PlanillasAnuladas["+fechainicio+"]["+fechafin+"].xls";
            String       Hoja  = "PlanillasAnuladas";
            String       Ruta  = path + "/exportar/migracion/"+ id +"/" + nombreArch;
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x1));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x1));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            //estilo5.setDataFormat(wb.createDataFormat().getFormat("####-##-## ##:##:##"));
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            String format = "#,##0.00";
            estilo6.setFont(fuente4);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo6.setDataFormat(wb.createDataFormat().getFormat("###,##"));
            estilo6.setAlignment((short) estilo6.ALIGN_RIGHT);
            
            
            /****************************************************/
            row  = sheet.createRow((short)(0));
            
            for (int j=0;j<10;j++) {
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
            }
            
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            
            sheet.createFreezePane(0,5);
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO");
            
            
            row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("Reporte de planillas anuladas");
            
            /*************************************************************************************/
            int Fila = 4;
            
            row  = sheet.createRow((short)(Fila));
            
            sheet.setColumnWidth((short)0, (short)5000 );
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLANILLA");
            
            sheet.setColumnWidth((short)1, (short)5000 );
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLACA");
            
            sheet.setColumnWidth((short)2, (short)5000 );
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("REMESA");
            
            sheet.setColumnWidth((short)3, (short)9000 );
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CLIENTE");
            
            sheet.setColumnWidth((short)4, (short)10000 );
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESCRIPCION");
            
            sheet.setColumnWidth((short)5, (short)7000 );
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA ANULACION");
            
            sheet.setColumnWidth((short)6, (short)7000 );
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA CREACION");
            
            sheet.setColumnWidth((short)7, (short)6000 );
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("DIFERENCIA");
            
            sheet.setColumnWidth((short)8, (short)7000 );
            cell = row.createCell((short)(8));
            cell.setCellStyle(estilo3);
            cell.setCellValue("USUARIO ANULACION");
            
            sheet.setColumnWidth((short)9, (short)7000 );
            cell = row.createCell((short)(9));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR ANTICIPO EN PESOS");
            
            sheet.setColumnWidth((short)10, (short)5000 );
            cell = row.createCell((short)(10));
            cell.setCellStyle(estilo3);
            cell.setCellValue("BANCO");
            
            sheet.setColumnWidth((short)11, (short)5000 );
            cell = row.createCell((short)(11));
            cell.setCellStyle(estilo3);
            cell.setCellValue("SUCURSAL");
            
            sheet.setColumnWidth((short)12, (short)5000 );
            cell = row.createCell((short)(12));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AGENCIA");
            
            sheet.setColumnWidth((short)13, (short)7000 );
            cell = row.createCell((short)(13));
            cell.setCellStyle(estilo3);
            cell.setCellValue("ESTADO ANTICIPO");
            
            sheet.setColumnWidth((short)14, (short)8000 );
            cell = row.createCell((short)(14));
            cell.setCellStyle(estilo3);
            cell.setCellValue("ESTADO DE IMPRESION");
            
            sheet.setColumnWidth((short)15, (short)8000 );
            cell = row.createCell((short)(15));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CAUSA ANULACION");
            
            sheet.setColumnWidth((short)16, (short)7000 );
            cell = row.createCell((short)(16));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FORMA RECUPERACION");
            
            sheet.setColumnWidth((short)17, (short)7000 );
            cell = row.createCell((short)(17));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLANILLA CUMPLIDA");
            
            sheet.setColumnWidth((short)18, (short)7000 );
            cell = row.createCell((short)(18));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA CUMPLIDO");
            
            sheet.setColumnWidth((short)19, (short)7000 );
            cell = row.createCell((short)(19));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA PROYEC. LLEGADA CHEQUE");
            
            sheet.setColumnWidth((short)20, (short)7000 );
            cell = row.createCell((short)(20));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AGEN. DESPACHADORA OC");
            
            sheet.setColumnWidth((short)21, (short)7000 );
            cell = row.createCell((short)(21));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA IMPRESION OC");
            
            
            sheet.setColumnWidth((short)22, (short)7000 );
            cell = row.createCell((short)(22));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR OC");
            
            
            sheet.setColumnWidth((short)23, (short)7000 );
            cell = row.createCell((short)(23));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OBSERVACIONES DE LA ANULACION");
            
            
            /*sheet.setColumnWidth((short)26, (short)7000 );
            cell = row.createCell((short)(26));
            cell.setCellStyle(estilo3);
            cell.setCellValue("NUMERO FACTURA ANTICIPO");*/
            
            
            sheet.setColumnWidth((short)24, (short)7000 );
            cell = row.createCell((short)(24));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CHEQUE ANTICIPO");
            
            
            sheet.setColumnWidth((short)25, (short)7000 );
            cell = row.createCell((short)(25));
            cell.setCellStyle(estilo3);
            cell.setCellValue("ESTADO CHEQUE");
            
            
            sheet.setColumnWidth((short)26, (short)7000 );
            cell = row.createCell((short)(26));
            cell.setCellStyle(estilo3);
            cell.setCellValue("NOMBRE GENERADOR ANTICIPO");
            
            
            sheet.setColumnWidth((short)27, (short)7000 );
            cell = row.createCell((short)(27));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AGENCIA GENERADOR ANTICIPO");
            
            
            sheet.setColumnWidth((short)28, (short)7000 );
            cell = row.createCell((short)(28));
            cell.setCellStyle(estilo3);
            cell.setCellValue("SUPP TO PAY");
            
            
            sheet.setColumnWidth((short)29, (short)7000 );
            cell = row.createCell((short)(29));
            cell.setCellStyle(estilo3);
            cell.setCellValue("SUPPLIER NAME");
            
            
            sheet.setColumnWidth((short)30, (short)7000 );
            cell = row.createCell((short)(30));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CEDULA DEL PROPIETARIO");
            
            
            sheet.setColumnWidth((short)31, (short)7000 );
            cell = row.createCell((short)(31));
            cell.setCellStyle(estilo3);
            cell.setCellValue("BANCO DEL PROPIETARIO");
            
            
            sheet.setColumnWidth((short)32, (short)7000 );
            cell = row.createCell((short)(32));
            cell.setCellStyle(estilo3);
            cell.setCellValue("SUCURSAL DEL PROPIETARIO");
            
            
            sheet.setColumnWidth((short)33, (short)7000 );
            cell = row.createCell((short)(33));
            cell.setCellStyle(estilo3);
            cell.setCellValue("INGRESO");
            
            
            sheet.setColumnWidth((short)34, (short)7000 );
            cell = row.createCell((short)(34));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR INGRESO");
            
            
            sheet.setColumnWidth((short)35, (short)7000 );
            cell = row.createCell((short)(35));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR ANTICIPO");
            
            
            sheet.setColumnWidth((short)36, (short)7000 );
            cell = row.createCell((short)(36));
            cell.setCellStyle(estilo3);
            cell.setCellValue("MONEDA");
            
            
            sheet.setColumnWidth((short)37, (short)7000 );
            cell = row.createCell((short)(37));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA ANTICIPO");
            
            
            sheet.setColumnWidth((short)38, (short)7000 );
            cell = row.createCell((short)(38));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR PENDIENTE POR RECUPERAR");
            
            
            sheet.setColumnWidth((short)39, (short)7000 );
            cell = row.createCell((short)(39));
            cell.setCellStyle(estilo3);
            cell.setCellValue("TIENE ANTICIPO");
            
            
            
            Iterator It = this.Lista.iterator();
            while(It.hasNext()) {
                
                AnulacionPlanilla pa = (AnulacionPlanilla) It.next();
                Fila++;
                row  = sheet.createRow((short)(Fila));
                
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getNumpla()!=null)?pa.getNumpla():"");
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getPlaca()!=null)?pa.getPlaca():"");
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getRemesa()!=null)?pa.getRemesa():"");
                
                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getCliente()!=null)?pa.getCliente():"");
                
                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getDescripcion()!=null)?pa.getDescripcion():"");
                
                cell = row.createCell((short)(5));
                cell.setCellStyle(estilo5);
                cell.setCellValue(pa.getFechaanulacion());
                
                cell = row.createCell((short)(6));
                cell.setCellStyle(estilo5);
                cell.setCellValue(pa.getFechacreacion());
                
                cell = row.createCell((short)(7));
                cell.setCellStyle(estilo4);
                cell.setCellValue(pa.getDiferencia());
                
                cell = row.createCell((short)(8));
                cell.setCellStyle(estilo4);
                cell.setCellValue(pa.getUsuario());
                
                cell = row.createCell((short)(9));
                cell.setCellStyle(estilo6);
                cell.setCellValue(pa.getValoranticipo());
                
                cell = row.createCell((short)(10));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getBanco()!=null)?pa.getBanco():"");
                
                cell = row.createCell((short)(11));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getSucursal()!=null)?pa.getSucursal():"");
                
                cell = row.createCell((short)(12));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getAgencia()!=null)?pa.getAgencia():"");
                
                cell = row.createCell((short)(13));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getEstadoanulacion()!=null)?pa.getEstadoanulacion():"");
                
                cell = row.createCell((short)(14));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getEstadoimpresion()!=null)?pa.getEstadoimpresion():"");
                
                cell = row.createCell((short)(15));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getCausaanulacion()!=null)?pa.getCausaanulacion():"");
                
                cell = row.createCell((short)(16));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getFormarecuperacion()!=null)?pa.getFormarecuperacion():"");
                
                cell = row.createCell((short)(17));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getCumplimiento()!=null)?pa.getCumplimiento():"");
                
                cell = row.createCell((short)(18));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getFechacumplimiento()!=null)?pa.getFechacumplimiento():"");
                
                cell = row.createCell((short)(19));
                cell.setCellStyle(estilo4);
                //cell.setCellValue((pa.getFechaproyectada()!=null)?pa.getFechaproyectada():"");
                cell.setCellValue("");
                
                cell = row.createCell((short)(20));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getCiudad()!=null)?pa.getCiudad():"");
                
                cell = row.createCell((short)(21));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getFechaimpresionoc()!=null)?pa.getFechaimpresionoc():"");
                
                cell = row.createCell((short)(22));
                cell.setCellStyle(estilo6);
                cell.setCellValue(pa.getValoroc());
                
                cell = row.createCell((short)(23));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getMotivoanulacion()!=null)?pa.getMotivoanulacion():"");
                
                /* cell = row.createCell((short)(26));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getFacturacliente()!=null)?pa.getFacturacliente():"");*/
                
                cell = row.createCell((short)(24));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getChequeanticipo()!=null)?pa.getChequeanticipo():"");
                
                cell = row.createCell((short)(25));
                cell.setCellStyle(estilo4);
                //cell.setCellValue((pa.getEstadocheque()!=null)?pa.getEstadocheque():"");
                cell.setCellValue("");
                
                cell = row.createCell((short)(26));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getNombreanticipo()!=null)?pa.getNombreanticipo():"");
                
                cell = row.createCell((short)(27));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getAgencia()!=null)?pa.getAgencia():"");
                
                cell = row.createCell((short)(28));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getSupptopay()!=null)?pa.getSupptopay():"");
                
                cell = row.createCell((short)(29));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getSuppliername()!=null)?pa.getSuppliername():"");
                
                cell = row.createCell((short)(30));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getCedulapropietario()!=null)?pa.getCedulapropietario():"");
                
                cell = row.createCell((short)(31));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getBancopropietario()!=null)?pa.getBancopropietario():"");
                
                cell = row.createCell((short)(32));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getSucursalpropietario()!=null)?pa.getSucursalpropietario():"");
                
                cell = row.createCell((short)(33));
                cell.setCellStyle(estilo4);
                cell.setCellValue("---");
                
                cell = row.createCell((short)(34));
                cell.setCellStyle(estilo4);
                cell.setCellValue("---");
                
                cell = row.createCell((short)(35));
                cell.setCellStyle(estilo6);
                cell.setCellValue(pa.getValorforaneoanticipo());
                
                cell = row.createCell((short)(36));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getMoneda()!=null)?pa.getMoneda():"");
                
                cell = row.createCell((short)(37));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getFechaanticipo()!=null)?pa.getFechaanticipo():"");
                
                cell = row.createCell((short)(38));
                cell.setCellStyle(estilo6);
                cell.setCellValue(pa.getValorrecuperar());
                
                cell = row.createCell((short)(39));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getTieneanticipo()!=null)?pa.getTieneanticipo():"");
                
            }
            /**************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream(Ruta);
            wb.write(fo);
            fo.close();
            //model.LogProcesosSvc.finallyProceso( "Reporte Planillas Anuladas", this.hashCode(), id, "PROCESO EXITOSO" );
        }catch( Exception ex ){
            ex.printStackTrace();
            //Capturo errores finalizando proceso
            /*try{
                model.LogProcesosSvc.finallyProceso( "Reporte Planillas Anuladas", this.hashCode(), id, "ERROR : " + ex.getMessage() );
            } catch( Exception f ){
                f.printStackTrace();
                try{
                    model.LogProcesosSvc.finallyProceso( "Reporte Planillas Anuladas",this.hashCode(), id, "ERROR : " + f.getMessage() );
                } catch( Exception p ){ p.printStackTrace(); }
            }*/
            
        }finally{
            super.destroy();
        }
    }
    
}
