/********************************************************************
 *      Nombre Clase.................   ReporteGeneracion.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   30.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;



public class ReporteGeneracionTh extends Thread{
    
    private Vector reporte;
    private String user;    
    private String fechai;
    private String fechaf;
    private Vector ttls;
    
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Vector reporte, Vector totales, String user, String fechai, String fechaf){
        this.reporte = reporte;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.ttls = totales;
        
        this.model   = modelo;
        this.procesoName = "Reporte de n�meros de reportes generados";
        this.des = "Reporte de n�meros de reportes generados:  " + fechai + " - " + fechaf;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteDeGeneracion_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReporteDeGeneracion_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                                                
            
            xls.obtenerHoja("REPORTE DE GENERACION");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, 4);
            xls.combinarCeldas(3, 0, 3, 4);
            xls.combinarCeldas(4, 0, 4, 4);
            xls.adicionarCelda(0, 0, "REPORTE DE GENERACION", header2);
            xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila, col++, "USUARIO", header1);
            xls.adicionarCelda(fila, col++, "TTL REPORTES", header1);
            xls.adicionarCelda(fila, col++, "TTL REGISTRADOS", header1);
            xls.adicionarCelda(fila, col++, "TTL NO REGISTRADOS", header1);
            xls.adicionarCelda(fila, col++, "% REGISTRADOS", header1);
            
            fila++;
            
            for( int i=0; i<ttls.size(); i++){
                col = 0;
                
                Hashtable ht = (Hashtable) ttls.elementAt(i);
                
                xls.adicionarCelda(fila, col++, ht.get("usuario").toString(), texto2);                
                xls.adicionarCelda(fila, col++, ht.get("total").toString(), entero);
                xls.adicionarCelda(fila, col++, ht.get("R").toString(), entero);
                xls.adicionarCelda(fila, col++, ht.get("NR").toString(), entero);
                xls.adicionarCelda(fila, col++, ht.get("PR").toString(), flotante);
                
                fila++;
            } 
            
            //DETALLES DEL REPORTE
            
            xls.obtenerHoja("DETALLES");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, 5);
            xls.combinarCeldas(3, 0, 3, 5);
            xls.combinarCeldas(4, 0, 4, 5);
            xls.adicionarCelda(0, 0, "REPORTE DE GENERACION", header2);
            xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
                            
            // subtitulos
            
            
            fila = 6;
            col  = 0;
            
            xls.adicionarCelda(fila, col++, "USUARIO", header1);
            xls.adicionarCelda(fila, col++, "PLANILLA", header1);
            xls.adicionarCelda(fila, col++, "PUESTO DE CONTROL", header1);
            xls.adicionarCelda(fila, col++, "FECHA DEL REPORTE", header1);
            xls.adicionarCelda(fila, col++, "ZONA", header1);
            xls.adicionarCelda(fila, col++, "GRABADO", header1);
            
            
            fila++;
            
            for( int i=0; i<reporte.size(); i++){
                col = 0;
                
                ReporteGeneracion rg = (ReporteGeneracion) reporte.elementAt(i);
                
                xls.adicionarCelda(fila, col++, rg.getUsuario(), texto2);                
                xls.adicionarCelda(fila, col++, rg.getPlanilla(), texto2);
                xls.adicionarCelda(fila, col++, rg.getNom_pc(), texto2);
                xls.adicionarCelda(fila, col++, rg.getFecha(), texto2);
                xls.adicionarCelda(fila, col++, rg.getNom_zona(), texto2);
                xls.adicionarCelda(fila, col++, rg.getGrabado(), texto2);
                
                fila++;
            }         
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO EXITOSO");
            
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}