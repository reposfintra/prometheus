/********************************************************************
 *      Nombre Clase.................   VGSeriePlanilla.java
 *      Descripci�n..................   Vigilante de serie de planillas 
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   9 de noviembre de 2006, 04:11 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;

import org.apache.log4j.*;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class VGSeriePlanillaTh extends Thread{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private String dstrct;
    private Model modelo;
    
    /** Crea una nueva instancia de  VGSeriePlanilla */
    public VGSeriePlanillaTh() {
    }
    
    public void start(Model modelo, String dstrct){
        this.modelo = modelo;
        this.dstrct = dstrct;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            double porcent = modelo.reporteGeneralService.sentinelaSeriePlanillas(dstrct);
            Email mail = new Email();
            
            logger.info("PORCENTAJE: " + porcent);
            
            if( porcent == -1 ){
                /* No encuentra una serie autorizada */
                mail.setEmailfrom("procesos@mail.tsp.com");
                mail.setEmailto(modelo.usuarioService.buscarEmailPerfil("ADMIN"));
                mail.setEmailsubject("Vigilante de Series de Planilla");
                mail.setEmailbody("El Vigilante de Series de Planilla no ha encontrado " +
                        "informaci�n pertinente al rango de serie de planilla autorizado por el Ministerio de Transporte");
                modelo.emailService.saveMail(mail);
            } else if ( porcent >= 80 ){
                /* La serie autorizada ha superado el 80% */
                mail.setEmailfrom("procesos@mail.tsp.com");
                mail.setEmailto("manifiesto@mail.tsp.com");
                mail.setEmailsubject("Vigilante de Series de Planilla");
                mail.setEmailbody("El Vigilante de Series de Planilla ha encontrado " +
                        "que el porcentaje utilizado del del rango autorizado de serie por el Ministerio de Transporte" +
                        "es del " + porcent + ". Favor tener en cuenta esta informaci�n para solicitar mas al Ministerio." );
                modelo.emailService.saveMail(mail);
                logger.info("EL PORCENTAJE ES MAYOR O IGUAL AL 80% SE ENVIO UN CORREO");
            }
            
        }catch (Exception ex){            
            ex.printStackTrace();
        }
        
    }    
    
}
