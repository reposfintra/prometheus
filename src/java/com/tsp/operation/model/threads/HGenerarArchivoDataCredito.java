/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.ReporteDataCreditoService;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

/**
 *
 * @author maltamiranda
 */
public class HGenerarArchivoDataCredito extends Thread {
    private String info;
    private Model model;
    private Usuario usuario;
    private String processName,ruta;

    public void start(Model model, Usuario usuario,String processName ){
        this.usuario = usuario;
        this.model = model;
        this.processName=processName;
        super.start();
    }

    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Exportacion a EXCEL", usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error "+this.processName +"...\n" + e.getMessage());
            }
        }
    }


    public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }

    private void generarArchivo() throws Exception {
        ReporteDataCreditoService rdc = new ReporteDataCreditoService();
        info=rdc.generarReporte();
        String  url   =  ruta + "/" + processName + (new SimpleDateFormat("yyyMMdd_hhmmss")).format( new Date())+".txt";
        PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
        pw.print(info);
        pw.close();
    }

}
