/*
 * ReporteControlDiscrepanciaThreads.java
 *
 * Created on 14 de diciembre de 2006, 03:04 PM
 */

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class ReporteControlDiscrepanciaThreads extends Thread {
    
    private String procesoName;
    private String des;
    String id = "";
    String distrito = "";
    String fecha_inicial = "";
    String fecha_final = "";
    String tipo = "";
    String numpla = "";
    Model model;
    
    /** Creates a new instance of ReporteControlDiscrepanciaThreads */
    public ReporteControlDiscrepanciaThreads ( Model model ) {
        this.model = model;
    }
    
    public void start(String usuario, String distrito, String fecha_inicial, String fecha_final, String tipo, String numpla){
        this.id = usuario;
        this.distrito = distrito ;
        this.procesoName = "Reporte de Control de Discrepancia en EXCEL";
        this.des = "Reporte de Control de Discrepancia en EXCEL creadas por "+usuario;
        this.fecha_inicial = fecha_inicial;
        this.fecha_final = fecha_final;
        this.tipo = tipo;
        this.numpla = numpla;
        super.start();
    }
        
    
    public synchronized void run(){
        try{
            String fecha_actual = Util.getFechaActual_String(6);
            
            //registramos el proeso en el log de procesos
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            Vector listado = new Vector();
            if( numpla.equals("") )
                listado = model.reporteGeneralService.reporteControlDiscrepancia (distrito, fecha_inicial, fecha_final, tipo);
            if ( listado != null && listado.size() > 0 ){
                //System.out.println("");
                Usuario us = (Usuario) model.usuarioService.obtenerUsuario(id);
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                File file = new File(path);
                file.mkdirs();
                
                String a�o = fecha_actual.substring(0,4);
                String mes = fecha_actual.substring(5,7);
                String dia = fecha_actual.substring(8,10);
                String hora = fecha_actual.substring(11,13);
                String min = fecha_actual.substring(14,16);
                String seg = fecha_actual.substring(17,19);
                
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite(path+"/REPORTE CONTROL DISCREPANCIA " +dia+"_"+mes+"_"+a�o+ ".xls");
                
                // Definicion de estilos: fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado                                           
                HSSFCellStyle fecha = xls.nuevoEstilo(      "verdana", 12, false , false, "yyyy/mm/dd", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_RIGHT );
                HSSFCellStyle texto = xls.nuevoEstilo(      "verdana", 12, false, false, "text", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle numero = xls.nuevoEstilo(     "verdana", 12, false, false, "_(#,##0.00_);(@_)", xls.NONE, xls.NONE, HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle negrita = xls.nuevoEstilo(    "verdana", 12, true, false, "text", HSSFColor.BLACK.index, xls.NONE, xls.NONE);
                HSSFCellStyle header = xls.nuevoEstilo(     "verdana", 18, true, false, "text", HSSFColor.LIME.index,xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo = xls.nuevoEstilo(     "verdana", 16, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle subtitulo = xls.nuevoEstilo(  "verdana", 14, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle cabecera = xls.nuevoEstilo(   "verdana", 12, true, false, "text", HSSFColor.WHITE.index, HSSFColor.INDIGO.index, HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja("BASE");
                //formateo al abrir hoja (tamano)
                xls.cambiarMagnificacion(3,4);
                
                //Titulo (cabecera)
                xls.adicionarCelda(0, 0, "TRANSPORTES SANCHEZ POLO S.A", header);
                xls.combinarCeldas(0, 0, 0, 5);  
                xls.adicionarCelda(2, 0, "REPORTE CONTROL DISCREPANCIA", titulo);
                xls.combinarCeldas(2, 0, 0, 5);
                xls.adicionarCelda(3, 0, "Fecha Inicial:", subtitulo);
                xls.adicionarCelda(3, 1, fecha_inicial, negrita);
                xls.adicionarCelda(3, 2, "Fecha Final:", subtitulo);
                xls.adicionarCelda(3, 3, fecha_final, negrita);
                xls.adicionarCelda(4, 0, "Elaborado Por: ", subtitulo);
                xls.adicionarCelda(4, 1, us.getNombre(), negrita);
                xls.adicionarCelda(4, 2, "Agencia:", subtitulo);
                xls.adicionarCelda(4, 3, model.ciudadservice.obtenerNombreCiudad(us.getId_agencia()), negrita);
                xls.adicionarCelda(4, 4, "Distrito:", subtitulo);
                xls.adicionarCelda(4, 5, distrito, negrita);
                xls.adicionarCelda(5, 0, "Fecha Reporte", subtitulo);
                xls.adicionarCelda(5, 1, fecha_actual, negrita);
                
                int fila = 7;
                int col  = 0;
                
                //definicion de titulos de la tabla
                xls.adicionarCelda(fila ,col++ , "AGENCIA DESPACHADORA"     , cabecera );
                xls.adicionarCelda(fila ,col++ , "AGENCIA RESPONSABLE"      , cabecera );
                xls.adicionarCelda(fila ,col++ , "CLIENTE"                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "OT"                       , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA DISCREPANCIA"       , cabecera );
                xls.adicionarCelda(fila ,col++ , "NRO. DISCREPANCIA"        , cabecera );
                xls.adicionarCelda(fila ,col++ , "DESC DISCREPANCIA"        , cabecera );
                xls.adicionarCelda(fila ,col++ , "PLACA"                    , cabecera );
                xls.adicionarCelda(fila ,col++ , "OC"                       , cabecera );
                xls.adicionarCelda(fila ,col++ , "CANT DISC"                , cabecera );
                
                xls.adicionarCelda(fila ,col++ , "VALOR"                    , cabecera );
                
                xls.adicionarCelda(fila ,col++ , "UNI MEDIDA"               , cabecera );
                xls.adicionarCelda(fila ,col++ , "TIPO DISC"                , cabecera );
                xls.adicionarCelda(fila ,col++ , "NOMBRE Y APELLIDO "       , cabecera );
                xls.adicionarCelda(fila ,col++ , "AG GEN DISC"              , cabecera );
                xls.adicionarCelda(fila ,col++ , "RETENIDO PAGO"            , cabecera );
                xls.adicionarCelda(fila ,col++ , "OBSERVACIONES"            , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA DE RESP"            , cabecera );
                xls.adicionarCelda(fila ,col++ , "TEXTO 1"                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "RUTA OC"                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "TIPO OT"                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "RECURSO"                  , cabecera );
                xls.adicionarCelda(fila ,col++ , "TIPO RECURSO"             , cabecera );
                xls.adicionarCelda(fila ,col++ , "OC TRAMO 1"               , cabecera );
                xls.adicionarCelda(fila ,col++ , "PLACA TRAMO 1"            , cabecera );
                xls.adicionarCelda(fila ,col++ , "RUTA TRAMO 1"             , cabecera );
                xls.adicionarCelda(fila ,col++ , "D�A"                      , cabecera );
                xls.adicionarCelda(fila ,col++ , "RANGO D�A"                , cabecera );
                xls.adicionarCelda(fila ,col++ , "CLASIFICACI�N (%)"        , cabecera );
                xls.adicionarCelda(fila ,col++ , "ESTADO DISCREPANCIA"      , cabecera );
                xls.adicionarCelda(fila ,col++ , "FECHA CIERRE"             , cabecera );
                
                //definicion de la tabla con los datos obtenidos de la consulta
                for (int i = 0; i < listado.size (); i++ ){
                    fila++;
                    col = 0;
                    Hashtable discrepancia = (Hashtable)listado.get ( i );
                    double cantidad = Double.parseDouble( (String)discrepancia.get ("cantidad") );
                    double valor = Double.parseDouble( (String)discrepancia.get ("valor") );
                    int diferencia = Integer.parseInt( (String)discrepancia.get ("diferencia") );
                    double clasificacion = Double.parseDouble( (String)discrepancia.get ("clasificacion") );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("agencia_despacho")          , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("agencia_cliente")           , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("nombre_cliente")            , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("remesa")                    , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("fecha_discrepancia")        , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("num_discre")                , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("desc_discrepancia")         , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("placa")                     , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("planilla")                  , texto );
                    xls.adicionarCelda(fila ,col++ , cantidad                                               , numero);
                    xls.adicionarCelda(fila ,col++ , valor                                                  , numero);
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("unidad")                    , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("tipo_discrepancia")         , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("usuario")                   , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("agencia_discrepancia")      , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("retenido_pago")             , texto );
                    xls.adicionarCelda(fila ,col++ , ""                                                     , texto );
                    xls.adicionarCelda(fila ,col++ , ""                                                     , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("observacion")               , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("ruta_oc")                   , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("tipo_ot")                   , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("recurso")                   , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("tipo_recurso")              , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("planilla_tramo1")           , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("placa_tramo1")              , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("ruta1_tramo1")              , texto );
                    xls.adicionarCelda(fila ,col++ , diferencia                                             , numero);
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("rango")                     , texto );
                    xls.adicionarCelda(fila ,col++ , clasificacion                                          , numero );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("estado")                    , texto );
                    xls.adicionarCelda(fila ,col++ , (String)discrepancia.get ("fecha_cierre")              , texto );
                }
                xls.cerrarLibro();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "Proceso Exitoso!");
            }else {
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "No se encontraron Datos...");
            }
            model.cxpDocService.setEnproceso();
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
                model.cxpDocService.setEnproceso();
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
