/*************************************************************
 * Nombre: ReportePlaneacionViajesXLS.java
 * Descripci�n: Hilo para crear el reporte de planeaci�n
 *              viaje.
 * Autor: Ing. Jose de la rosa
 * Fecha: 26 de enero de 2006, 06:21 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.finanzas.presupuesto.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class ReportePlaneacionViajesXLS extends Thread{
    private String procesoName;
    private String des;
    Usuario id;
    String fechaini="";
    String fechafin="";
    String ffin="";
    String Nmes="";
    String diac="";
    String distrito="";
    String vec_standar[]= {"127003","127006","127009","127010","127017","127012"};
    int vec_dia[]=new int[31];
    int vec_viajes[]=new int[31];
    int tplan[]=new int [31];
    int teje[]=new int [31];
    
    int ejecutado []=new int [31];
    float vec_peso[]=new float[31];
    Vector vecpto = new Vector();
    com.tsp.operation.model.Model model;
    
    /** Creates a new instance of ReportePlaneacionViajesXLS */
    public ReportePlaneacionViajesXLS() {
    }
    public void start(Usuario usuario, String Nmes,String fechaini, String fechafin,String ffin,String diac,String distrito){
        this.id = usuario;
        this.fechaini = fechaini;
        this.fechafin = fechafin;
        this.Nmes = Nmes;
        this.distrito = distrito;
        this.ffin = ffin;
        this.diac = diac;
        this.procesoName = "PLANEACI�N "+Nmes;
        this.des = "LA PLANEACION DE EJECUCI�N :"+Nmes;
        model = new com.tsp.operation.model.Model(usuario.getBd());
        super.start();
    }
    
    public synchronized void run(){
        try{
            com.tsp.finanzas.presupuesto.model.Model modelfin = new com.tsp.finanzas.presupuesto.model.Model(id.getBd());
            ////System.out.println("entro hilo");
            model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, this.id.getLogin());
            ////System.out.println("1");
            String fecha_actual = Util.getFechaActual_String(6);
            
            //Declaraci�n de variables
            int fila = 5;
            int col  = 0;
            int m=0;
            int nrocol = 0;
            double toneladas=0;
            int ejestd = 0;
            int planstd = 0;
            int difstd=0;
            int n = Integer.parseInt(ffin);
            int dif = 0;
            
            
            //DEFINICIONES DE VECTORES
            for(int i = 1; i <= n ; i++){
                vec_viajes[i-1]=0;
                vec_dia[i-1]=0;
                vec_peso[i-1]=0;
                teje[i-1]=0;
                tplan[i-1]=0;
                ejecutado[i-1]=0;
                
            }
            //comienzo conexion
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta") + "/exportar/migracion/"+id;
            String dia = fecha_actual.substring(8,10);
            File file = new File(path);
            file.mkdirs();
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite(path+"/PLANEACION"+Nmes+" "+diac+".xls");
            // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
            HSSFWorkbook wb = new HSSFWorkbook();
            
            HSSFCellStyle fecha  = xls.nuevoEstilo("verdana", 10, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("verdana", 10, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle numero = xls.nuevoEstilo("verdana", 10, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle numero_negrita = xls.nuevoEstilo("verdana", 10, true , false, "#,##0"            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle numero_porciento = xls.nuevoEstilo("verdana", 10, true , false, "#,##0%"            ,  HSSFColor.WHITE.index ,HSSFColor.BLUE_GREY.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero_negrita_sin = xls.nuevoEstilo("verdana", 10, true , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle negrita      = xls.nuevoEstilo("verdana", 10, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE,1);
            HSSFCellStyle header      = xls.nuevoEstilo("verdana", 13, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLUE.index , HSSFCellStyle.ALIGN_CENTER);
            //  HSSFCellStyle titulo      = xls.nuevoEstilo("verdana", 10, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("verdana", 10, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );
            // HSSFCellStyle titulo1      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.AQUA.index , HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle titulo      = xls.nuevoEstilo("verdana", 10, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLUE.index , HSSFCellStyle.ALIGN_CENTER,1);
            //HSSFCellStyle titulo3      = xls.nuevoEstilo("verdana", 10, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.DARK_YELLOW.index , HSSFCellStyle.ALIGN_CENTER,1);
            //HSSFCellStyle titulo4      = xls.nuevoEstilo("verdana", 10, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREY_25_PERCENT.index , HSSFCellStyle.ALIGN_CENTER,1);
            //HSSFCellStyle titulo5      = xls.nuevoEstilo("verdana", 10, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.LIME.index , HSSFCellStyle.ALIGN_CENTER,1);
            //HSSFCellStyle titulo6      = xls.nuevoEstilo("verdana", 10, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREY_50_PERCENT.index , HSSFCellStyle.ALIGN_CENTER,1);
            //HSSFCellStyle titulo7      = xls.nuevoEstilo("verdana", 10, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.SKY_BLUE.index , HSSFCellStyle.ALIGN_CENTER,1);
            
            //primera hoja detalle
            xls.obtenerHoja("DETALLES");
            xls.cambiarMagnificacion(3,4);
            // cabecera
            ////System.out.println("3");
            xls.adicionarCelda(0,0, "REPORTE CUMPLIMIENTO, MES DE: "+Nmes  , header);
            xls.combinarCeldas(0, 0, 0, 7);
            
            // ENCABEZADO
            xls.adicionarCelda(2,0, "Fecha Proceso: "                                  , negrita);
            xls.adicionarCelda(2,1, fecha_actual                                       , fecha);
            xls.adicionarCelda(3,0, "Elaborado Por: "                                  , negrita);
            xls.adicionarCelda(3,1, id.getLogin()                                      , fecha);
            
            //SUBTITULOS
            xls.adicionarCelda(4,1, "CMU"  , titulo);
            xls.combinarCeldas(4, 1, 4, 3);
            xls.adicionarCelda(fila ,col++ , "Dia"                                     , titulo );
            xls.adicionarCelda(fila ,col++ , "Planeado"                                , titulo );
            xls.adicionarCelda(fila ,col++ , "Ejecutado"                               , titulo );
            xls.adicionarCelda(fila ,col++ , "Diferencia"                              , titulo );
            xls.adicionarCelda(4,4, "C.D.J"  , titulo);
            xls.combinarCeldas(4, 4, 4, 6);
            xls.adicionarCelda(fila ,col++ , "Planeado"                                , titulo );
            xls.adicionarCelda(fila ,col++ , "Ejecutado"                               , titulo );
            xls.adicionarCelda(fila ,col++ , "Diferencia"                              , titulo );
            xls.adicionarCelda(4,7, "C.NORTE"  , titulo);
            xls.combinarCeldas(4, 7, 4, 9);
            xls.adicionarCelda(fila ,col++ , "Planeado"                                , titulo );
            xls.adicionarCelda(fila ,col++ , "Ejecutado"                               , titulo );
            xls.adicionarCelda(fila ,col++ , "Diferencia"                              , titulo );
            xls.adicionarCelda(4,10, "CALENTURITAS"  , titulo);
            xls.combinarCeldas(4, 10, 4, 12);
            xls.adicionarCelda(fila ,col++ , "Planeado"                                , titulo );
            xls.adicionarCelda(fila ,col++ , "Ejecutado"                               , titulo );
            xls.adicionarCelda(fila ,col++ , "Diferencia"                              , titulo );
            xls.adicionarCelda(4,13, "C.D.C"  , titulo);
            xls.combinarCeldas(4, 13, 4, 15);
            xls.adicionarCelda(fila ,col++ , "Planeado"                                , titulo );
            xls.adicionarCelda(fila ,col++ , "Ejecutado"                               , titulo );
            xls.adicionarCelda(fila ,col++ , "Diferencia"                              , titulo );
            xls.adicionarCelda(4,16, "Total Puerto"  , titulo);
            xls.combinarCeldas(4, 16, 4, 18);
            xls.adicionarCelda(fila ,col++ , "Planeado"                                , titulo );
            xls.adicionarCelda(fila ,col++ , "Ejecutado"                               , titulo );
            xls.adicionarCelda(fila ,col++ , "Diferencia"                              , titulo );
            xls.adicionarCelda(4,19, "Carbon Andes"  , titulo);
            xls.combinarCeldas(4, 19, 4, 21);
            xls.adicionarCelda(fila ,col++ , "Planeado"                                , titulo );
            xls.adicionarCelda(fila ,col++ , "Ejecutado"                               , titulo );
            xls.adicionarCelda(fila ,col++ , "Diferencia"                              , titulo );
            col=0;
            nrocol=0;
            for(int i = 1; i <= 6 ; i++){
                planstd = 0;
                ejestd = 0;
                dif = 0;
                
                //iniacializo vector de viajes ejecutado
                for(int k = 1; k <= n ; k++){
                    ejecutado[k-1]=0;
                }
                //SUB TOTAL DEL PUERTO
                if(i == 6){
                    for(m=1;m<=tplan.length;m++){
                        fila++;
                        col=nrocol;
                        xls.adicionarCelda(fila ,col++ ,   tplan[m-1]                             , numero );
                        xls.adicionarCelda(fila ,col++ ,   teje[m-1]                          , numero );
                        dif+=(teje[m-1] - tplan[m-1]);
                        xls.adicionarCelda(fila ,col++ ,      dif        , numero_negrita );
                        //realizo la suma total por  standar
                        planstd+=tplan[m-1];
                        ejestd+=teje[m-1];
                        
                    }
                    nrocol=(i==1)?1:nrocol;
                    fila++;
                    xls.adicionarCelda(fila ,nrocol++ ,   planstd                             , numero );
                    xls.adicionarCelda(fila ,nrocol++ ,   ejestd                          , numero );
                    xls.adicionarCelda(fila ,nrocol++ ,   ((planstd*35.5)!=0)?((ejestd*35.5)/(planstd*35.5)):0           , numero_porciento );
                    fila++;
                    nrocol = nrocol-3;
                    xls.adicionarCelda(fila ,nrocol++ ,   (planstd*35.5)                             , numero_negrita );
                    xls.adicionarCelda(fila ,nrocol++ ,   (ejestd*35.5)                          , numero_negrita );
                    xls.adicionarCelda(fila ,nrocol++ ,  ""                          , null );
                    fila++;
                    nrocol = nrocol-3;
                    xls.adicionarCelda(fila ,nrocol++ ,   "Diferencia"                          , titulo );
                    xls.adicionarCelda(fila ,nrocol++ ,   (planstd*35.5)-(ejestd*35.5)                        , numero_negrita );
                    xls.adicionarCelda(fila ,nrocol++ ,  ""                          , null );
                    //inicializo variables
                    fila = 5;
                    planstd = 0;
                    ejestd = 0;
                    dif = 0;
                }
                //Vector de dias ejecutados
                List lista = (List) model.planillaService.reporteViajesXLS( fechaini, fechafin, vec_standar[i-1] );
                if (lista!=null && lista.size()>0){
                    Iterator it = lista.iterator();
                    while(it.hasNext()){
                        Planilla plan = (Planilla) it.next();
                        m = Integer.parseInt(plan.getFeccum());
                        vec_viajes[m-1]+=plan.getDias();
                        ejecutado[m-1]=plan.getDias();
                        toneladas+=plan.getPesoreal();
                    }
                }
                
                //Vector dias presupuestados
                int planeado[] = modelfin.DPtoSvc.buscarptoCarbon( fechaini, fechaini, vec_standar[i-1], distrito);
                for(m=1;m<=planeado.length;m++){
                    vec_dia[m-1] += planeado[m-1];
                    if (i <= 5){
                        tplan[m-1] +=planeado[m-1];
                        teje[m-1]  +=ejecutado[m-1];
                    }
                }
                //escribo en la hoja de excel
                for(m=1;m<=n;m++){
                    fila++;
                    col=nrocol;
                    if (i==1){
                        xls.adicionarCelda(fila ,col++ , " D�a "+m                           , texto );
                    }
                    xls.adicionarCelda(fila ,col++ ,   planeado[m-1]                    , numero );
                    dif+=(ejecutado[m-1] - planeado[m-1]);
                    xls.adicionarCelda(fila ,col++ ,   ejecutado[m-1]                       , numero );
                    xls.adicionarCelda(fila ,col++ ,    dif        , numero_negrita );
                    //realizo la suma total por  standar
                    planstd+=planeado[m-1];
                    ejestd+=ejecutado[m-1];
                    
                }
                nrocol=(i==1)?1:nrocol;
                fila++;
                xls.adicionarCelda(fila ,nrocol++ ,   planstd                             , numero );
                xls.adicionarCelda(fila ,nrocol++ ,   ejestd                          , numero );
                xls.adicionarCelda(fila ,nrocol++ ,   ((planstd*35.5)!=0)?((ejestd*35.5)/(planstd*35.5)):0           , numero_porciento );
                fila++;
                nrocol = nrocol-3;
                xls.adicionarCelda(fila ,nrocol++ ,   (planstd*35.5)                          , numero_negrita );
                xls.adicionarCelda(fila ,nrocol++ ,   (ejestd*35.5)                          , numero_negrita );
                xls.adicionarCelda(fila ,nrocol++ ,  ""                          , null );
                fila++;
                nrocol = nrocol-3;
                xls.adicionarCelda(fila ,nrocol++ ,   "Diferencia"                          , titulo );
                xls.adicionarCelda(fila ,nrocol++ ,   (planstd*35.5)-(ejestd*35.5)                        , numero_negrita );
                xls.adicionarCelda(fila ,nrocol++ ,  ""                          , null );
                fila = 5;
                nrocol = col;
                
                
                
            }
            
            fila=5;
            col=0;
            //segunda hoja de resumen
            xls.obtenerHoja ("RESUMEN");
            xls.cambiarMagnificacion (3,4);
            // cabecera
            ////System.out.println ("3");
            xls.adicionarCelda (0,0, "REPORTE PLANEACION VIAJES, MES DE: "+Nmes  , header);
            xls.combinarCeldas (0, 0, 0, 7);
            ////System.out.println ("6");
             
            // ENCABEZADO
            xls.adicionarCelda (2,0, "Fecha Proceso: "                                  , negrita);
            xls.adicionarCelda (2,1, fecha_actual                                       , fecha);
            xls.adicionarCelda (3,0, "Elaborado Por: "                                  , negrita);
            xls.adicionarCelda (3,1, id.getLogin()                                      , fecha);
            ////System.out.println ("antes de imprimir");
            // subtitulos
            xls.adicionarCelda (fila ,col++ , "Total"                                   , titulo );
            xls.adicionarCelda (fila ,col++ , "Planeado"                                , titulo );
            xls.adicionarCelda (fila ,col++ , "Ejecutado"                               , titulo );
            xls.adicionarCelda (fila ,col++ , "Diferencia"                              , titulo );
            xls.adicionarCelda (fila ,col++ , "%"                                       , titulo );
            // datos
            
            int  sw=0,cont_plan=0,cont_eje=0,diferencia=0;
            for(int j = 1; j <= n ; j++){
                m=0;
                fila++;
                col = 0;
                cont_plan+=vec_dia[j-1];
                cont_eje+=(vec_viajes[j-1]+teje[j-1]);
                diferencia+=(vec_viajes[j-1]+teje[j-1])-vec_dia[j-1];
                
                
                xls.adicionarCelda (fila ,col++ , " D�a "+j                                  , texto );
                xls.adicionarCelda (fila ,col++ ,   vec_dia[j-1]                             , numero );
                xls.adicionarCelda (fila ,col++ ,   (vec_viajes[j-1]+teje[j-1])              , numero );
                xls.adicionarCelda (fila ,col++ ,   diferencia                               , numero_negrita );
                xls.adicionarCelda (fila ,col++ ,   vec_dia[j-1]!=0?(((double) (vec_viajes[j-1]+teje[j-1])*100)/vec_dia[j-1])-100:0 , numero );
                
            }
            col = 0;
            fila++;
            fila++;
            xls.adicionarCelda (fila ,col++ , "Viajes"                                      , negrita );
            xls.adicionarCelda (fila ,col++ , cont_plan                                     , numero );
            xls.adicionarCelda (fila ,col++ , cont_eje                                      , numero );
            xls.adicionarCelda (fila ,col++ , cont_plan-cont_eje                            , numero );
             
            col = 0;
            fila++;
            xls.adicionarCelda (fila ,col++ , "Ton"                                         , negrita );
            xls.adicionarCelda (fila ,col++ , cont_plan*35.5                                , numero );
            xls.adicionarCelda (fila ,col++ , toneladas                                     , numero );
            xls.adicionarCelda (fila ,col++ , (cont_plan*35.5 )-toneladas                   , numero );
            xls.cerrarLibro();

            model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), this.id.getLogin(), "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id.getLogin(),"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id.getLogin(),"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
