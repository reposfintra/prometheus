   /***************************************
    * Nombre Clase ............. HArchivosTransferenciaBsCaptaciones.java
    * Descripci�n  .. . . . . .  Permite Generar los archivos para transferencia al banco
    * Autor  . . . . . . . . . . JESUS PINEDO VILLAFA�E
    * Fecha . . . . . . . . . .  23/05/2012
    * versi�n . . . . . . . . .  1.0
    * Copyright ...geotech S.A.
    *******************************************/




package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.LogProcesosService.*;
import java.lang.*;
import java.io.*;

import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;
import java.text.*;
import java.util.Date;



import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Rectangle;



import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;
import com.lowagie.text.Font;
import javax.servlet.*;


import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;



public class HArchivosTransferenciasCaptaciones extends Thread {



       String  url_logo="fintrapdf.gif";


      Calendar c = new GregorianCalendar();
      String dia = Integer.toString(c.get(Calendar.DATE));
      String mes = Integer.toString(c.get(Calendar.MONTH));
      String annio = Integer.toString(c.get(Calendar.YEAR));

      private String   DDMMAAAA              = "ddMMyyyy";



// Archivo de Transferencia MSF265 para migrar a MIMS:
    private  FileWriter        fw265;
    private  BufferedWriter    bf265;
    private  PrintWriter       linea265;
    private  List          Popietario_secuencias;
    private  File f;


 // Archivo de Transferencia
    private  FileWriter        fw;
    private  BufferedWriter    bf;
    private  PrintWriter       linea;


    private  String URL_TEM_TRANS     = "";
    private  String URL_TEM_MSF265    = "";



    private  String        procesoName;
    private  Model         model;
    private  Usuario       usuario ;
    private  List          listaTransferencia;
    private ArrayList<MovimientosCaptaciones>         listaAgrupada;

    private  String[]      retiros;
    private String   CUPO_ROTATIVO         = "CROT";

    private String   AAMMDD                = "yyMMdd";
    private String   AAAAMMDD              = "yyyyMMdd";
    private String   SPACE                 = " ";
    private String   CERO                  = "0";
    private String   DERECHA               = "R";
    private String   IZQUIERDA             = "L";
    private String   CTA_AHORR0            = "CA";
    private String   CTA_CORRIENTE         = "CC";
    private String   CTA_EFECTIVO          = "EF";
    private String   CPAG                  = "CPAG";

    private int      TOTALDETALLE          = 0;
    private double   TOTALDEBITO           = 0;
    private double   TOTALCREDITO          = 0;

    private String   NIT_PROVEEDOR         = "";
    private String   NOMBRE_PROVEEDOR      = "";
    private String   CTA_PROVEEDOR         = "";
    private String   TIPO_CTA_PROVEEDOR    = "";
    private String   BANCO_PROVEEDOR       = "";
    private String   DESC_BANCO_PROVEEDOR  = "";


    private String   TRANSFERENCIA         = "";
    private boolean PRESTAMO               = false; //TMolina 2008-09-13

// CONFIGURACION DE BANCOS:

    // 1.  Bancolombia:
           private String   BANCOLOMBIA                   = "07";
           private String   TABLA_BANCOLOMBIA_BANCO       = "BANCOLOMBI";   // BANCOS ESTIPULADOS POR BANCOLOMBIA
           private String   TABLA_BANCOLOMBIA_CIUDAD      = "CIUBANCOLO";   // CODIGO DE CIUDAD ESTIPULADOS POR BANCOLOMBIA

           private String   BANCOLOMBIA_TIPO_REGISTRO     = "1";
           private String   BANCOLOMBIA_TIPO_REGISTRO_DET = "6";
           private String   BANCOLOMBIA_TIPO_TRANSACTION  = "220";
           private String   BANCOLOMBIA_PROPOSITO         = "PAGFINTRA";
           private String   BANCOLOMBIA_CTA_AHORR0        = "37";
           private String   BANCOLOMBIA_CTA_CORRIENTE     = "27";
           private String   BANCOLOMBIA_CTA_EFECTIVO      = "26";



    // 2. Occidente
          private String   OCCIDENTE                      = "23";
          private String   OCCIDENTE_TIPO_REGISTRO        = "1";
          private String   OCCIDENTE_TIPO_REGISTRO_DET    = "2";
          private String   OCCIDENTE_TIPO_REGISTRO_FIN    = "3";

          private String   OCCIDENTE_CTA_AHORR0           = "A";
          private String   OCCIDENTE_CTA_CORRIENTE        = "C";
          private String   OCCIDENTE_CTA_EFECTIVO         = SPACE;
          private String   OCCIDENTE_CONCEPTO             = "PAGOS FINTRA";
          private String   TABLA_OCCIDENTE_BANCO          = "BOCCIDENTE";

          private   String  CUENTA_ANTICIPO     = "2205050406";
          private   String  CUENTA_LIQUIDACION  = "2205050404";


          // 3. Avvillas
          private String   AVVILLAS                   = "52";
          private String   TABLA_AVVILLAS_BANCO       = "BOCCIDENTE";   // Son los mismos codigos de bancos para occidente
          private String   AVVILLAS_TIPO_REGISTRO_HEAD= "1";
          private String   AVVILLAS_TIPO_REGISTRO_DET = "2";
          private String   AVVILLAS_TIPO_REGISTRO_FIN = "4";
          private String   AVVILLAS_TIPO_REGISTROS    = "PPD";
          private String   AVVILLAS_TIPO_TRANSACTION  = "PP";   //Pago a proveedores
          private String   AVVILLAS_CANAL             = "4";    //internet
          private String   AVVILLAS_NIT               = "03";
          private String   AVVILLAS_CEDULA            = "01";
          private String   PLAZA_BARRANQUILLA         = "0004"; //Codigo para la ciudad de barranquilla
          //tipos de cuentas
          private String   AVVILLAS_CTA_CORRIENTE     = "0";
          private String   AVVILLAS_CTA_AHORROS       = "1";
          private String   AVVILLAS_CUPO_ROTATIVO     = "2";
          //Codigos de transaccion
          private String   AVVILLAS_CREDITO_A_AH      = "32";
          private String   AVVILLAS_CREDITO_A_CC      = "22";

    // 4. Colpatria
          private String   COLPATRIA                      = "19";
          private String   COLPATRIA_TIPO_REGISTRO_HEAD   = "01";
          private String   COLPATRIA_TIPO_REGISTRO_DET    = "02";
          private String   COLPATRIA_TIPO_REGISTRO_FIN    = "03";
          //private String   COLPATRIA_CTA_AHORR0S          = "0";
          private String   COLPATRIA_CTA_AHORR0S          = "2";
          private String   COLPATRIA_CTA_CORRIENTE        = "1";
          private String   COLPATRIA_CTA_EFECTIVO         = SPACE;
          private String   COLPATRIA_TIPO_TRANSACCION     = "02";
          private String   COLPATRIA_CONCEPTO             = "PAGOFINTRA";
          private String   TABLA_COLPATRIA_BANCO          = "BANCOLOMBI";


    // 5.  Bancolombia formato PAB
           private String   BANCOLOMBIAPAB                   = "7B";
           private String   BANCOLOMBIAPAB_TIPO_APLICACION   = "I"; // I=inmediata
           private String   BANCOLOMBIA_CPAG_TIPO_TRANSACTION= "320";





    public HArchivosTransferenciasCaptaciones() {
    }





    public void init(){
            TOTALDETALLE          = 0;
            TOTALDEBITO           = 0;
            TOTALCREDITO          = 0;

            NOMBRE_PROVEEDOR      = "";
            NIT_PROVEEDOR         = "";
            CTA_PROVEEDOR         = "";
            TIPO_CTA_PROVEEDOR    = "";
            BANCO_PROVEEDOR       = "";
            DESC_BANCO_PROVEEDOR  = "";

            URL_TEM_TRANS         = "";
            URL_TEM_MSF265        = "";

            TRANSFERENCIA         = "";

            PRESTAMO              = false; //TMolina 2008-09-13
    }





    /**
     * M�todo inicializa  el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void start(Model modelo, Usuario user,String[] retiros, String nit, String name, String banco,String desbanco, String cta, String tCta) throws Exception{
         try{
                init();
                this.model                 = modelo;
                this.usuario               = user;
                this.retiros             = retiros;
                this.listaAgrupada         = new ArrayList<MovimientosCaptaciones>();

                String[] str; //Mod Tmolina 2008-09-13
                str           = name.split(",");


                this.NIT_PROVEEDOR         = nit;
                this.NOMBRE_PROVEEDOR      = str[0];//name; //TMolina 2008-09-13
                this.BANCO_PROVEEDOR       = banco;
                this.DESC_BANCO_PROVEEDOR  = desbanco;
                this.CTA_PROVEEDOR         = model.Negociossvc.getAccountToTxt(cta);
                this.TIPO_CTA_PROVEEDOR    = tCta;


                this.procesoName = "TRANSFERENCIA BANCO " + banco;

            super.start();

        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
     /**
     * Metodo que  permite ejecutar el SQL
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */





     /**
     * Metodo que  determina si hay rutina para el banco seleccionado
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public String validarFormatosRealizados(String codebanco){
         String msj = "No hay formato definido(rutina) para el banco " + codebanco;
         if( codebanco.equals(  BANCOLOMBIA ) )  msj="";
         if( codebanco.equals(  BANCOLOMBIAPAB ) )
             msj="";
         if( codebanco.equals(  OCCIDENTE   ) )  msj="";
         if( codebanco.equals(  AVVILLAS    ) )
             msj="";
         if( codebanco.equals(  COLPATRIA   ) )
             msj="";
         if( codebanco.equals(  "CG"   ) )  msj="";

         if( codebanco.equals(  "FC"   ) )  msj="";
         if( codebanco.equals(  "FD"   ) )
             msj="";
        return msj;
    }






     /**
     * Metodo que  escribe cabecera archivo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void writeHeader(String codeBanco)throws Exception{
        try{


            int sec = 1;


         // BANCOLOMBIA
            if(codeBanco.equals(BANCOLOMBIA)){
                     String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO                                           +  // 1.1  Tipo de Registro
                                     rellenar(this.NIT_PROVEEDOR,               CERO,  10, IZQUIERDA )   +  // 1.2  Nit entidad que envia
                                     rellenar(this.NOMBRE_PROVEEDOR,            SPACE, 16, DERECHA   )   +  // 1.3  Nombre entidad que envia
                                     BANCOLOMBIA_TIPO_TRANSACTION                                        +  // 1.4  Clase de transaccion, segun formato es 220
                                     rellenar(this.BANCOLOMBIA_PROPOSITO ,      SPACE, 10, DERECHA   )   +  // 1.5  Proposito descripci�n
                                     this.getFecha(AAMMDD)                                               +  // 1.6  Fecha transacci�n  AAMMDD
                                     this.convertLetra(sec)                                              +  // 1.7  Secuencia del archivo del dia en letra A.B.C....
                                     this.getFecha(AAMMDD)                                               +  // 1.8  Fecha aplicaci�n  AAMMDD
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  6 , IZQUIERDA )   +  // 1.9  N�mero de registros de detalle
                                     rellenar(String.valueOf((int)TOTALDEBITO ),CERO,  12, IZQUIERDA )   +  // 1.10 Sumatoria debito
                                     rellenar(String.valueOf((int)TOTALCREDITO),CERO,  12, IZQUIERDA )   +  // 1.10 Sumatoria debito
                                     rellenar(this.CTA_PROVEEDOR,               CERO,  11, IZQUIERDA )   +  // 1.11 Cta Cliente
                                     equivalenciaTipoCta( this.BANCO_PROVEEDOR, this.TIPO_CTA_PROVEEDOR );  // 1.12 Tipo Cta  S : aho  /  D : cte

                    linea.println( dato );
                    //System.out.println("DatoHeader---------"+dato);
            }


         // BANCOLOMBIA FORMATO PAB
            if(codeBanco.equals(BANCOLOMBIAPAB)){
                     String secuenciaPAB = model.AnticiposPagosTercerosSvc.obtenerSecuenciaBanco(BANCOLOMBIAPAB, usuario.getLogin());
                     String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO                                           +  // 1.1  Tipo de Registro
                                     rellenar(this.NIT_PROVEEDOR,               CERO,  15, IZQUIERDA )   +  // 1.2  Nit entidad que envia
                                     BANCOLOMBIAPAB_TIPO_APLICACION                                      +  // 1.3  Tipo de aplicacion
                                     rellenar(SPACE,SPACE, 15, DERECHA   )                               +  // 1.4  Filler
                                     BANCOLOMBIA_TIPO_TRANSACTION                                        +  // 1.5  Clase de transaccion, segun formato es 220 pago proveedores
                                     rellenar(this.BANCOLOMBIA_PROPOSITO ,      SPACE, 10, DERECHA   )   +  // 1.6  Proposito descripci�n
                                     this.getFecha(AAAAMMDD)                                             +  // 1.7  Fecha transacci�n  AAAAMMDD
                                     rellenar(secuenciaPAB,                     SPACE, 2, DERECHA    )   +  // 1.8  Secuencia del archivo del dia en letra A.B.C....
                                     this.getFecha(AAAAMMDD)                                             +  // 1.9  Fecha aplicaci�n  AAAAMMDD
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  6 , IZQUIERDA )   +  // 1.10  N�mero de registros de detalle
                                     rellenar(String.valueOf((int)TOTALDEBITO ) + "00",CERO,  17, IZQUIERDA )+  // 1.11 Sumatoria debito
                                     rellenar(String.valueOf((int)TOTALCREDITO) + "00",CERO,  17, IZQUIERDA )+  // 1.12 Sumatoria credito
                                     rellenar(this.CTA_PROVEEDOR,               CERO,  11, IZQUIERDA )   +  // 1.13 Cta Cliente
                                     equivalenciaTipoCta( BANCOLOMBIA         , this.TIPO_CTA_PROVEEDOR )+  // 1.14 Tipo Cta  S : aho  /  D : cte
                                     rellenar(SPACE,SPACE, 149, DERECHA);                                   // 1.15  Filler


                    linea.println( dato );
            }

         // OCCIDENTE
            if(codeBanco.equals( OCCIDENTE )){
                String dato =
                                     OCCIDENTE_TIPO_REGISTRO                                           +  // 1.1 Tipo de Registro
                                     rellenar( CERO,                            CERO,  4,  DERECHA   ) +  // 1.2 Consecutivo
                                     this.getFecha(AAAAMMDD)                                           +  // 1.3 Fecha del archivo, formato YYYYMMDD
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  4 , IZQUIERDA ) +  // 1.4 N�mero de registros de detalle
                                     rellenar(String.valueOf((int)TOTALCREDITO) + "00" , CERO,  18, IZQUIERDA ) +  // 1.5 Valor total de pago
                                     rellenar(this.CTA_PROVEEDOR,               CERO,  16, IZQUIERDA ) +  // 1.6 Cta Cliente
                                     rellenar(String.valueOf(sec),              CERO,   6, IZQUIERDA ) +  // 1.7 Secuencia
                                     rellenar( CERO,                            CERO, 142, DERECHA   ) ;  // 1.8 Ceros

                linea.println( dato );
            }


             if (codeBanco.equals(AVVILLAS)) {
                String dato =
                        AVVILLAS_TIPO_REGISTRO_HEAD +
                        rellenar(this.CTA_PROVEEDOR, SPACE, 17, DERECHA) + // Cuenta origen
                        equivalenciaTipoCta(this.BANCO_PROVEEDOR, this.TIPO_CTA_PROVEEDOR) + // Tipo Cuenta origen
                        AVVILLAS_TIPO_TRANSACTION + //pago proveedores
                        this.getFecha(AAAAMMDD) +
                        rellenar(this.NIT_PROVEEDOR+"1", CERO, 15, IZQUIERDA) + // Nit entidad que envia
                        AVVILLAS_NIT +
                        rellenar(this.NOMBRE_PROVEEDOR, SPACE, 16, DERECHA) +
                        PLAZA_BARRANQUILLA +
                        this.AVVILLAS_TIPO_REGISTROS +
                        rellenar(String.valueOf(0), CERO, 6, IZQUIERDA) +
                        this.AVVILLAS_CANAL;

                linea.println(dato);
            }

            // COLPATRIA
            if (codeBanco.equals(COLPATRIA)) {
                String dato =
                        //rellenar("1", CERO, 5, DERECHA) + // Consecutivo
                        rellenar("1", CERO, 5, IZQUIERDA) + // Consecutivo
                        COLPATRIA_TIPO_REGISTRO_HEAD + // Tipo de Registro
                        this.getFecha(DDMMAAAA) + // Fecha del archivo
                        rellenar(this.NIT_PROVEEDOR+"1", CERO, 11, IZQUIERDA) + // Nit entidad que envia
                        rellenar(COLPATRIA_CONCEPTO, SPACE, 10, DERECHA) + // Clave: codigo de identificacion de la empresa
                        rellenar(String.valueOf(TOTALDETALLE+2), CERO, 6, IZQUIERDA) + // Numero de registros de detalle
                        "0196" + // OFICINA DE PAGO
                        rellenar(this.CTA_PROVEEDOR, CERO, 12, IZQUIERDA) + // Cuenta origen
                        rellenar(SPACE, SPACE, 142, DERECHA);

                linea.println(dato);
            }


          //System.out.println("Escribio Biennnn Occ");
        }catch(Exception e){
            throw new Exception(" writeHeader "+e.getMessage());
        }
    }






     /**
     * Metodo que  ecribe en el archivo de transferencia a banaco
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void writeTransfer(String codeBanco, MovimientosCaptaciones  trans)throws Exception{
        try{

         // BANCOLOMBIA
            if( codeBanco.equals(BANCOLOMBIA ) )    bancolombia( trans );

         // BANCOLOMBIA PAB
            if( codeBanco.equals(BANCOLOMBIAPAB ) )    bancolombiaPAB( trans );

        // OCCIDENTE
            if( codeBanco.equals( OCCIDENTE ) )    occidente( trans );


            // AVVILLAS
            if( codeBanco.equals( AVVILLAS ) )    avvillas( trans );

        // COLPATRIA
            if( codeBanco.equals( COLPATRIA ) )    colpatria( trans );
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }




    /**
     * Metodo que  ecribe bloque final
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void writeFinal(String codeBanco)throws Exception{
        try{

         // OCCIDENTE
            if(codeBanco.equals( OCCIDENTE )){
                String dato =
                                     OCCIDENTE_TIPO_REGISTRO_FIN                                         +  // 1.1 Tipo de Registro final
                                     "9999"                                                              +  // 1.2 Secuencia
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  4 , IZQUIERDA )   +  // 1.3 N�mero de pagos
                                     rellenar(String.valueOf((int)TOTALCREDITO) +"00",CERO,  18, IZQUIERDA )   +  // 1.4 Valor total de pago
                                     rellenar( CERO,                            CERO, 172, DERECHA   )   ;  // 1.5 Ceros
                linea.println( dato );
            }

             // AVVILLAS
            if(codeBanco.equals( AVVILLAS )){
                String dato =
                                     AVVILLAS_TIPO_REGISTRO_FIN                                          +  // Tipo de Registro final
                                     rellenar(String.valueOf(TOTALDETALLE),     CERO,  8 , IZQUIERDA )   +  // Número de pagos
                                     rellenar(String.valueOf((int)TOTALCREDITO) +"00",CERO,  18, IZQUIERDA );//Valor total de pago
                linea.println( dato );
            }

        // COLPATRIA
            if(codeBanco.equals( COLPATRIA )){
                String dato =
                                     rellenar(String.valueOf(TOTALDETALLE+2),     CERO,  5 , IZQUIERDA ) +  // Secuencia
                                     COLPATRIA_TIPO_REGISTRO_FIN                                         +  // Tipo de Registro final
                                     rellenar(String.valueOf(TOTALDETALLE+2),     CERO,  6 , IZQUIERDA ) +  // Número de pagos
                                     rellenar( SPACE,                            SPACE, 187, DERECHA   );
                linea.println( dato );
            }



        } catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }






    /**
     * Metodo que  ecribe en formato BANCOLOMBIA
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void bancolombia(MovimientosCaptaciones  transf)throws Exception{
           try{


         // 2. DETALLE DE TRANSACCION:

               String tipoCta          = transf.getTipo_cuenta();


            // Codigo del  banco del beneficiario
               String codigoBanco     =  CERO;
               Hashtable  infoBanco   =  model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_BANCOLOMBIA_BANCO, transf.getBanco() );
               if(infoBanco==null)
                     throw new Exception ( "No hay informaci�n del banco  " + transf.getBanco() + " en formato bancolombia");
               else
                    codigoBanco  =   (String)infoBanco.get("descripcion");

            // Indicador de pago: sucursal
               String indicador_pago   = "S";

            // Tipo de transacion:
               String tipo_transaction = equivalenciaTipoTransaction( BANCOLOMBIA, tipoCta );

            // Valor
               int    valor            = (int)transf.getRetiro();

               String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO_DET                                      +  // 1.1  Tipo de Registro detalle
                                     rellenar( transf.getNit_cuenta(),          CERO,  15, IZQUIERDA  ) +  // 1.2  Nit beneficiario
                                     rellenar( transf.getTitular_cuenta(),       SPACE, 18, DERECHA    ) +  // 1.3  Nombre del beneficiario
                                     rellenar( codigoBanco,                     CERO,   9, IZQUIERDA  ) +  // 1.4  Codigo banco
                                     rellenar( transf.getCuenta(),              CERO,  17, IZQUIERDA  ) +  // 1.5  N�mero de la Cta
                                     indicador_pago                                                     +  // 1.6  Lugar de pago
                                     tipo_transaction                                                   +  // 1.7  Tipo de Transaction
                                     rellenar( String.valueOf(valor),           CERO,  10, IZQUIERDA  ) +  // 1.8  Valor
                                     rellenar( this.BANCOLOMBIA_PROPOSITO ,     SPACE,  9, DERECHA    ) +  // 1.9  Proposito descripci�n
                                     rellenar( transf.getLote_transferencia(),        SPACE, 12, DERECHA    ) +  // 1.10 NEGOCIO OPCIONAL
                                     SPACE                                                              ;  // 1.11 Espacio en blanco

              linea.println( dato );
              //System.out.println("Dato---------"+dato);
           } catch(Exception e){
             throw new Exception( " bancolombia " + e.getMessage());
          }
    }




    /**
     * Metodo que ecribe los las transferencias a realizar en formato BANCOLOMBIA PAB
     * @autor Diana Arrieta
     * @throws Exception
     */
    public void bancolombiaPAB(MovimientosCaptaciones  transf)throws Exception{
           try{

               // 2. DETALLE DE TRANSACCION:

               String tipoCta = transf.getTipo_cuenta();


               // Codigo del  banco del beneficiario
               String codigoBanco = CERO;
               Hashtable infoBanco = model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_BANCOLOMBIA_BANCO, transf.getBanco());
               if (infoBanco == null) {
                   throw new Exception("No hay informaci�n del banco  " + transf.getBanco() + " en formato bancolombia");
               } else {
                   codigoBanco = (String) infoBanco.get("descripcion");
               }


               // Indicador de pago: sucursal
               String indicador_pago = " ";

               // Tipo de transacion:
               String tipo_transaction = equivalenciaTipoTransaction(BANCOLOMBIA, tipoCta);
               // Valor
               int valor = (int) transf.getRetiro();
               String dato =
                                     BANCOLOMBIA_TIPO_REGISTRO_DET                                      +  // 1  Tipo de Registro detalle
                                     rellenar( transf.getNit_cuenta(),          SPACE,  15, DERECHA   ) +  // 2  Nit beneficiario
                                     rellenar( transf.getTitular_cuenta(),       SPACE, 30, DERECHA    ) +  // 3  Nombre del beneficiario
                                     rellenar( codigoBanco,                     CERO,   9, IZQUIERDA  ) +  // 4  Codigo banco
                                     rellenar( transf.getCuenta(),              SPACE,  17, DERECHA   ) +  // 5  N�mero de la Cta
                                     indicador_pago                                                     +  // 6  Lugar de pago
                                     tipo_transaction                                                   +  // 7  Tipo de Transaction
                                     rellenar( String.valueOf(valor) + "00",    CERO,  17, IZQUIERDA  ) +  // 8  Valor
                                     this.getFecha(AAAAMMDD)                                            +  // 9  Fecha aplicaci�n  AAAAMMDD
                                     rellenar( this.BANCOLOMBIA_PROPOSITO ,     SPACE,  9, DERECHA    ) +  // 10  Proposito descripci�n
                                     rellenar( transf.getLote_transferencia(),                              SPACE, 12, DERECHA    ) +  // 11 referencia
                                     rellenar( SPACE,                           SPACE,  1, DERECHA    ) +  // 12 Tipo de documento de identificaci�n
                                     rellenar( SPACE,                           SPACE,  5, DERECHA    ) +  // 13 Oficina de entrega
                                     rellenar( SPACE,                           SPACE, 15, DERECHA    ) +  // 14 N�mero de Fax
                                     rellenar( SPACE,                           SPACE, 80, DERECHA    ) +  // 15 E-mail beneficiario
                                     rellenar( SPACE,                           SPACE, 15, DERECHA    ) +  // 16 N�mero identificaci�n del autorizado
                                     rellenar( SPACE,                           SPACE, 27, DERECHA    );   // 17 Filler

              linea.println( dato );


           } catch(Exception e){
             throw new Exception( " bancolombia " + e.getMessage());
          }
    }



    /**
     * Metodo que  ecribe en formato OCCIDENTE
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void occidente(MovimientosCaptaciones  transf)throws Exception{
           try{


         // 2. DETALLE DE TRANSACCION:

               String tipoCta          = transf.getTipo_cuenta();

            // N�mero de la cuenta, si es efectivo 0, de lo contrario la cuenta del beneficiario
               String cuenta           = ( tipoCta.equals( CTA_EFECTIVO ))? CERO : transf.getCuenta() ;


            // Codigo del banco  -- Definir codigos
               String codigoBanco      = CERO;
               Hashtable  infoBanco    =  model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_OCCIDENTE_BANCO, transf.getBanco() );
               if(infoBanco==null)
                     throw new Exception ( "No hay informaci�n del banco  " + transf.getBanco() + " en formato occidente");
               else
                    codigoBanco  =   (String)infoBanco.get("descripcion");


           //  Forma de pago:
               String formaPago        = ( tipoCta.equals( CTA_EFECTIVO ) )? "4" : ( ( transf.getBanco().equals("BANCO OCCIDENTE"))?"2":"3"  );

           //  Tipo de transacion:
               String tipo_transaction = equivalenciaTipoTransaction( OCCIDENTE, tipoCta );

           //  Valor
               int  valor   = (int) transf.getRetiro();


           // Comprobante -- Definir
              String comprobante       = transf.getNit_cuenta();//(transf.getReanticipo().equals("N") )?CUENTA_ANTICIPO : CUENTA_LIQUIDACION ; // CUENTA CONTABLE


               String dato =    OCCIDENTE_TIPO_REGISTRO_DET                                                  +  // 1.1   Tipo Registro detalle
                               // rellenar( String.valueOf(transf.getSecuencia()),     CERO,   4, IZQUIERDA  ) +  // 1.2   Consecutivo del registro
                                rellenar( this.CTA_PROVEEDOR,                        CERO,  16, IZQUIERDA  ) +  // 1.3   Cta Beneficiario
                                rellenar( transf.getTitular_cuenta(),                 SPACE, 30, DERECHA    ) +  // 1.4   Nombre del beneficiario
                                rellenar( transf.getNit_cuenta(),                    CERO,  11, IZQUIERDA  ) +  // 1.5   Nit beneficiario
                                rellenar( codigoBanco,                               CERO,   4, IZQUIERDA  ) +  // 1.6   Codigo banco beneficiario
                                this.getFecha(AAAAMMDD)                                                      +  // 1.7   Fecha a realizar el pago
                                formaPago                                                                    +  // 1.8   Forma de pago
                                rellenar( String.valueOf(valor) + "00",              CERO,  15, IZQUIERDA  ) +  // 1.9   Valor
                                rellenar( cuenta,                                    SPACE, 16, DERECHA    ) +  // 1.10  N�mero de la Cta
                                rellenar( comprobante,                               SPACE, 12, IZQUIERDA  ) +  // 1.11  Comprobante
                                tipo_transaction                                                             +  // 1.12  Tipo de transacction
                                rellenar( OCCIDENTE_CONCEPTO,                        SPACE,  80, DERECHA   ) ;  // 1.13  Concepto de pago


              linea.println( dato );


           } catch(Exception e){
             throw new Exception( " occidente " + e.getMessage());
          }
    }



    /**
     * Metodo para generar un registro de detalle en el archivo de transferencia para AVVILLAS
     * @param transf bean AnticiposTerceros con la informacion para generar el registro
     * @throws Exception
     */
    public void avvillas(MovimientosCaptaciones transf) throws Exception {
        try {

            // DETALLE DE TRANSACCION:

            String tipoCta = transf.getTipo_cuenta();

            // Codigo del  banco del beneficiario
            String codigoBanco = CERO;
            Hashtable infoBanco = model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_AVVILLAS_BANCO, transf.getBanco());
            if (infoBanco == null) {
                throw new Exception("No hay información del banco  " + transf.getBanco() + " en formato avvillas");
            } else {
                codigoBanco = (String) infoBanco.get("descripcion");
            }

            String tipo_transaction = equivalenciaTipoTransaction(AVVILLAS, tipoCta);
            String tipoCuentaDestino = equivalenciaTipoCta(AVVILLAS,tipoCta);

            // Valor
            int valor = (int) transf.getRetiro();

            String dato =
                    AVVILLAS_TIPO_REGISTRO_DET +
                    tipo_transaction +
                    rellenar(codigoBanco, CERO, 4, IZQUIERDA) + // Codigo del banco destino
                    PLAZA_BARRANQUILLA + // codigo para barranquilla
                    rellenar(transf.getNit_cuenta(), SPACE, 15, IZQUIERDA) + // nit beneficiario
                    AVVILLAS_NIT + // tipo= nit
                    rellenar(transf.getCuenta(), SPACE, 17, DERECHA) +
                    tipoCuentaDestino +
                    rellenar(transf.getTitular_cuenta(), SPACE, 22, DERECHA) +
                    "0" + // ind_mas_addendas 0=una addenda, 1=mas de una addenda
                    rellenar(String.valueOf(valor) + "00", CERO, 18, IZQUIERDA) +
                    "1"; // flag_valida 1=valida identificacion, 0=no valida

            linea.println(dato);


        } catch (Exception e) {
            throw new Exception(" Error en avvillas " + e.getMessage());
        }
    }


    /**
     * Metodo para generar un registro de detalle en el archivo de transferencia para COLPATRIA
     * @param transf bean AnticiposTerceros con la informacion para generar el registro
     * @throws Exception
     */
    public void colpatria(MovimientosCaptaciones transf) throws Exception {
        try {

            // DETALLE DE TRANSACCION:

            String codigoBanco = CERO;
            Hashtable infoBanco = model.AnticiposPagosTercerosSvc.getInfoBanco(TABLA_COLPATRIA_BANCO, transf.getBanco());
            if (infoBanco == null) {
                throw new Exception("No hay información del banco  " + transf.getBanco() + " en formato colpatria");
            } else {
                codigoBanco = (String) infoBanco.get("descripcion");
            }

            String codigoTransaccion = "";
            String cuentaColpatria = "";
            String cuentaOtroBanco = "";
            if(transf.getBanco().equals("COLPATRIA")){
                codigoTransaccion="902";
                cuentaColpatria = rellenar(transf.getCuenta(), CERO, 12, IZQUIERDA);
                cuentaOtroBanco = rellenar(CERO, CERO, 17, IZQUIERDA);
            }else{
                codigoTransaccion="911";
                cuentaColpatria = rellenar(CERO, CERO, 12, IZQUIERDA);
                cuentaOtroBanco = rellenar(transf.getCuenta(), CERO, 17, IZQUIERDA);
            }

            String tipoCuentaDestino = equivalenciaTipoCta(COLPATRIA,transf.getTipo_cuenta());

            //  Valor
            int valor = (int) transf.getRetiro();

            String dato =
                    //rellenar(String.valueOf(transf.getSecuencia()+1), CERO, 5, IZQUIERDA) + // Consecutivo del registro
                    COLPATRIA_TIPO_REGISTRO_DET + // Tipo Registro detalle
                    cuentaColpatria + // Cta Beneficiario si es a colpatria
                    rellenar(transf.getNit_cuenta(), CERO, 11, IZQUIERDA) + // Nit beneficiario
                    rellenar(transf.getTitular_cuenta(), SPACE, 40, DERECHA) + // Nombre del beneficiario
                    codigoTransaccion + // Codigo de transaccion 911=abono en otra entidad
                    COLPATRIA_TIPO_TRANSACCION + // Tipo de cargo a aplicar
                    rellenar(String.valueOf(valor) + "00", CERO, 15, IZQUIERDA) + // Valor
                    rellenar(SPACE, SPACE, 10, DERECHA) + // numero de factura
                    rellenar(CERO, CERO, 6, IZQUIERDA) + // Numero control de pago
                    rellenar(CERO, CERO, 15, IZQUIERDA) + // Retencion en la fuente
                    rellenar(CERO, CERO, 15, IZQUIERDA) + // iva
                    this.getFecha(DDMMAAAA) + // Fecha a realizar el pago
                    rellenar(CERO, CERO, 10, IZQUIERDA) + //
                    rellenar(CERO, CERO, 15, IZQUIERDA) + //
                    rellenar(codigoBanco, CERO, 8, IZQUIERDA) + // Codigo banco beneficiario
                    cuentaOtroBanco + // Número de la Cta
                    tipoCuentaDestino +
                    "N" + //tipo de documento N=nit
                    rellenar(SPACE, SPACE, 4, IZQUIERDA) +
                    rellenar(SPACE, SPACE, 80, IZQUIERDA); // adenda

            linea.println(dato);


        } catch (Exception e) {
            throw new Exception(" occidente " + e.getMessage());
        }
    }


     /**
     * Metodo que  devuelve la equivalencia tipo de transacci�n de acuerdo al banco
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public String equivalenciaTipoTransaction(String codeBanco, String tipo){
        String equivale = tipo;

        // BANCOLOMBIA
            if(codeBanco.equals(BANCOLOMBIA)){
               if( tipo.equals( CTA_AHORR0     ))  equivale = BANCOLOMBIA_CTA_AHORR0;
               if( tipo.equals( CTA_CORRIENTE  ))  equivale = BANCOLOMBIA_CTA_CORRIENTE;
               if( tipo.equals( CTA_EFECTIVO   ))  equivale = BANCOLOMBIA_CTA_EFECTIVO;
            }


        // OCCIDENTE
            if(codeBanco.equals( OCCIDENTE )){
               if( tipo.equals( CTA_AHORR0     ))  equivale = OCCIDENTE_CTA_AHORR0;
               if( tipo.equals( CTA_CORRIENTE  ))  equivale = OCCIDENTE_CTA_CORRIENTE;
               if( tipo.equals( CTA_EFECTIVO   ))  equivale = OCCIDENTE_CTA_EFECTIVO;
            }

        return equivale;
    }


    /**
     * Metodo que  devuelve la equivalencia tipo de cuenta de acuerdo al banco
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public String equivalenciaTipoCta(String codeBanco, String tipo){
        String equivale = tipo;

        // BANCOLOMBIA
            if(codeBanco.equals(BANCOLOMBIA)){
               if( tipo.equals(CTA_AHORR0))     equivale = "S";
               if( tipo.equals(CTA_CORRIENTE))  equivale = "D";
               if(tipo.equals(CPAG))   equivale="D";
            }

                // AVVILLAS
            if(codeBanco.equals(AVVILLAS)){
               if( tipo.equals(CTA_AHORR0))     equivale = AVVILLAS_CTA_AHORROS;
               if( tipo.equals(CTA_CORRIENTE))  equivale = AVVILLAS_CTA_CORRIENTE;
               if( tipo.equals(CUPO_ROTATIVO))  equivale = AVVILLAS_CUPO_ROTATIVO;
            }

        // COLPATRIA
            if(codeBanco.equals(COLPATRIA)){
               if( tipo.equals(CTA_AHORR0))     equivale = COLPATRIA_CTA_AHORR0S;
               if( tipo.equals(CTA_CORRIENTE))  equivale = COLPATRIA_CTA_CORRIENTE;
            }

        return equivale;
    }




    /**
     * Metodo para sacar la equivalencia de un caracter decimal
     * en un caracter de notacion
     * @autor mfontalvo
     */
    public static String convertLetra(int numero){
        return  String.valueOf(  (char) (numero + 65) );
    }



     /**
    * M�todo que rellena los campos
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/
    public  String rellenar(String cadena, String caracter, int tope, String posicion)throws Exception{
       try{
           int lon = cadena.length();
           if(tope>lon){
             for(int i=lon;i<tope;i++){
                 if(posicion.equals( DERECHA))    cadena += caracter;
                 else                                 cadena  = caracter + cadena;
             }
           }
           else
               cadena = Trunc(cadena, tope );

       }catch(Exception e){
           throw new Exception(e.getMessage());
       }
       return cadena;
    }



   /**
    * M�todo que trunca cadena
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/
   public static String Trunc(String Cadena, int Longitud){
       return (Cadena==null?Cadena: (Cadena.length()>=Longitud? Cadena.substring(0,Longitud):Cadena  )  );
   }





     /**
     * Metodo que crea el archivo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
     public void createFile()throws Exception{
         try{

             String nameFile     =  getNombreArchivo( this.DESC_BANCO_PROVEEDOR );
             model.DirectorioSvc.create(this.usuario.getLogin());
             System.out.println(" crear archivo "+ this.usuario.getLogin()+"/"+ nameFile);
             String ruta         =  model.DirectorioSvc.getUrl() + this.usuario.getLogin()  +"/"+ nameFile;

             URL_TEM_TRANS       =  ruta;

             this.fw             = new FileWriter    (ruta   );
             this.bf             = new BufferedWriter(this.fw);
             this.linea          = new PrintWriter   (this.bf);
         }catch(Exception e){
             throw new Exception( e.getMessage() );
         }
     }




     /**
     * Metodo que guarda el archivo de transferencia
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void saveTransfer(){
        this.linea.close();
    }




    /**
     * Metodo que crea el archivo para migrar a mims MSF265
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
     public void createFile265()throws Exception{
         try{

             String hora         =  Util.getFechaActual_String(6).replaceAll("/|:","").replaceAll(" ","_");
             String ruta265      =  model.DirectorioSvc.getUrl() + this.usuario.getLogin()  +"/MSF265_"+  hora   +".txt";

             URL_TEM_MSF265      = ruta265;

             this.fw265          = new FileWriter    ( ruta265     );
             this.bf265          = new BufferedWriter( this.fw265  );
             this.linea265       = new PrintWriter   ( this.bf265  );

         }catch(Exception e){
             throw new Exception( e.getMessage() );
         }
     }



     /**
     * Metodo que guarda el archivo para migrar a mims
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void save265(){
        this.linea265.close();
    }





    /**
     * Metodo que borra los archivos en caso de error
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
     public void deleteFiles()throws Exception{
         try{

             File  fT =  new File(URL_TEM_TRANS);   fT.delete();
             File  fM =  new File(URL_TEM_MSF265);  fM.delete();

         }catch(Exception e){
             throw new Exception( e.getMessage() );
         }
     }




    /**
    * M�todo que permite crear nombre del archivo
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/
    public String getNombreArchivo(String desBanco)throws Exception{
        String name    ="";
        try{
            String hora    =  Util.getFechaActual_String(6).replaceAll("/|:","").replaceAll(" ","_");
                   name    =  desBanco +"_"+ hora + ".txt";
        }catch(Exception e){
           throw new Exception("getNombreArchivo " + e.getMessage());
        }
        return name;
    }




     /**
    * M�todo que permite dar la fecha actual dependiendo el formato
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/
    public String getFecha(String formato)throws Exception{
       String Fecha = "";
       try{

            SimpleDateFormat FMT = null;
            FMT = new SimpleDateFormat(formato);
            Fecha = FMT.format(new Date());

       }catch(Exception e){
           throw new Exception("getFecha " + e.getMessage());
       }
       return Fecha.toUpperCase() ;
    }



    /**
     * Metodo que  ecribe al archivo MSF265 para MIMS
     * @autor: ....... Fernel Villacob
     * @Modificado ... Julio Barros Rueda
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void write265(AnticiposTerceros  trans)throws Exception{
        try{


             String  GRABADOR           = "79458875";        //  Pablo Rosales
             String  CUENTA             = (trans.getReanticipo().equals("N") )?CUENTA_ANTICIPO : CUENTA_LIQUIDACION ;   // CUENTA CONTABLE REGISTRO


             String  FACTURA            =  trans.getFactura_mims();

             String  FECHA              =  Util.getFechaActual_String(5) +"/"+  Util.getFechaActual_String(3) +"/"+ Util.getFechaActual_String(1).substring(2,4);
             String  COMA               = ",";

             double  valor              = (int) Math.round( trans.getVlr());

             String  banco              = "";
             String  sucursal           = "";
             String  mims               = "";
             double  vlr                = 0;
             banco     = "";
             sucursal  = "";
             mims      = "";
             vlr       = 0;
             Hashtable bancoTer      =  model.AnticiposPagosTercerosSvc.getBancoNIT( trans.getProveedor_anticipo()  );
             if( bancoTer!= null  ){
                 banco     = (String) bancoTer.get("banco");
                 sucursal  = (String) bancoTer.get("sucursal");
                 mims      = (String) bancoTer.get("mims");
             }
             vlr   =  valor;

             String  DESCRIPCION        = "PP Fintra Liq " + trans.getFactura_mims()+" Por $"+valor;

             String SPACE2 = "";

             String  datoProveedor   =
                                        banco.trim()                  + COMA +
                                        sucursal.trim()               + COMA +
                                        GRABADOR.trim()               + COMA +
                                        mims.trim()                   + COMA +
                                        FACTURA.trim()                + COMA +
                                        FECHA.trim()                  + COMA +
                                        FECHA.trim()                  + COMA +
                                        String.valueOf( vlr ).trim()  + COMA +
                                        SPACE2.trim()                 + COMA +
                                        DESCRIPCION.trim()            + COMA +
                                        String.valueOf( vlr ).trim()  + COMA +
                                        SPACE2.trim()                 + COMA +
                                        GRABADOR.trim()               + COMA +
                                        SPACE2.trim()                 + COMA +
                                        CUENTA.trim()
                                        ;

           //  linea265.println( datoPropietario );
             linea265.println( datoProveedor   );


        } catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }


     /**
     * Metodo que  permite ejecutar el SQL
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
      public void ejecutar(String sql)throws Exception{
        try{
            TransaccionService  svc =  new  TransaccionService(usuario.getBd());
            svc.crearStatement();
            svc.getSt().addBatch(sql);
            svc.execute();

        }catch(Exception e){

            System.out.println("SQL: " + sql );

            throw new Exception( e.getMessage() );
        }
    }


     /**
     * M�todo que ejecuta el proceso de  generaci�n del archivo
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public synchronized void run(){
       try{
            String comentario="EXITOSO";
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(), this.procesoName , this.usuario.getLogin() );
            String control = validarFormatosRealizados(this.BANCO_PROVEEDOR);
            if(control.equals("")){  
            listaAgrupada = model.captacionInversionistaService.agruparRetiros(retiros);
                  
                  if(  listaAgrupada.size()>0 ){
                           // Buscamos la transferencia:
                              TRANSFERENCIA = model.AnticiposPagosTercerosSvc.getSerie();
                              if( !TRANSFERENCIA.equals("") ){
                                       // Definimos factura
                                          this.TOTALDETALLE  = listaAgrupada.size();
                                          for(int i=0;i<listaAgrupada.size();i++){
                                               MovimientosCaptaciones  trans  =  listaAgrupada.get(i);
                                                //trans.setSecuencia      (i+1);
                                                //trans.setFactura_mims   (  TRANSFERENCIA +"_"+ trans.getSecuencia()  );
                                               // trans.setTransferencia  ( this.TRANSFERENCIA       );
                                                this.TOTALCREDITO +=  trans.getRetiro();
                                          }
                                         try{
                                          // ARCHIVOS:
                                          // Escribimos en el archivo para la transferencia:
                                              createFile();
                                              obtenerTipoTransaccion(BANCO_PROVEEDOR, TIPO_CTA_PROVEEDOR);
                                              writeHeader(this.BANCO_PROVEEDOR);
                                              for(int i=0;i<listaAgrupada.size();i++){
                                                   //System.out.println("para ------------------------------>");
                                                    MovimientosCaptaciones  trans  =  listaAgrupada.get(i);
                                                    writeTransfer(this.BANCO_PROVEEDOR, trans);
                                              }
                                              writeFinal(this.BANCO_PROVEEDOR);
                                              saveTransfer();

                             }
                                         catch(Exception e)
                             {
                                deleteFiles();
                                throw new Exception( e.getMessage() );
                             }
                       }
                       else
                          comentario = "No hay serie de transferencia...";
                  }
                  else
                      comentario = "No se agruparon los registros...";
            }
            else
                comentario = control;
            //System.out.println("pasa a escribir en el log de prosesos");
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);
       }catch(Exception e){
           try{
               model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage());
           }catch(Exception f){ }
       }
    }












    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons + "_" + fmt.format(new Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }













    /**
     * Obtiene el tipo de tranasaccion para un banco
     * @param codeBanco
     * @param listaAnticipos
     */
    public void obtenerTipoTransaccion(String codeBanco, String tipo){
        // BANCOLOMBIA
        if(codeBanco.equals(BANCOLOMBIAPAB)){
            if(tipo.equals(CPAG)){
                BANCOLOMBIA_TIPO_TRANSACTION = BANCOLOMBIA_CPAG_TIPO_TRANSACTION;
            }
        }
    }






}
//Entregado a Jbarros 21 Febrero
