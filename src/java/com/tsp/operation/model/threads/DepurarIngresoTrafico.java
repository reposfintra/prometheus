/***********************************************************************************
 * Nombre       DepurarIngresoTrafico.java 
 * Autor        Diogenes Bastidas
 * Fecha        10 de agosto de 2006, 07:36 PM
 * Versi�n      1.0
 * Coyright     Transportes Sanchez Polo S.A.
 ***********************************************************************************/


package com.tsp.operation.model.threads;

import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;


public class DepurarIngresoTrafico extends Thread{
    private String dstrct;
    private String usuario;
    private Model model = new Model();
    
    public void start(String dstrct, String Usuario){
        this.dstrct = dstrct;
        this.usuario = Usuario;
        super.start();
    }
    
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso("DEPURAR INGRESO TRAFICO ", this.hashCode(), "DEPURAR INGRESO TRAFICO ", this.usuario);
            
            String fecha_actual = Util.getFechaActual_String(6);
            String fecha = fecha_actual.substring(0,10).replace("/","-");
            String hora = fecha_actual.substring(11,16).replace(":","-");
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            
            File file = new File(path + "/exportar/migracion/" + this.usuario + "/");
            file.mkdirs();
            String NombreArchivo = path + "/exportar/migracion/" + this.usuario + "/DepuracionSalida_"+fecha+"_"+hora+".txt";
            PrintStream archivo = new PrintStream(NombreArchivo);
            
            Vector vecurb = model.rmtService.obtenerUrbanoUrbano();
            Vector vecurbno = model.rmtService.obtenerUrbanoNoAplica();
            Vector vecvac = model.rmtService.obtenerVacios();
            Vector vecsjsintrf =  model.rmtService.obtenerSJSintrafico();
            //uno los vectores en uno solo
            for (int i = 0; i < vecurbno.size(); i++){
               vecurb.add(vecurbno.get(i));
            }
            for (int i = 0; i < vecvac.size(); i++){
               vecurb.add(vecvac.get(i));
            }
            for (int i = 0; i < vecsjsintrf.size(); i++){
               vecurb.add(vecsjsintrf.get(i));
            }
            archivo.println( " Dstrct, Planilla, Placa, Cedcon,Nomcond,Cedprop, Nomprop,Origen,Nomorigen, Destino,"+
                             " Nomdestino,Zona,Nomzona,Escolta, Caravana,Fecha_despacho,Pto_control_ultreporte,Nompto_control_ultreporte," +
                             " Fecha_ult_reporte,Pto_control_proxreporte,Nompto_control_proxreporte,Fecha_prox_reporte,Ult_observacion," +
                             " Last_update, User_update, Creation_date, Creation_user, Base,Demora,Via,Fecha_salida,Cel_cond,Cliente,"+
                             " Tipo_despacho,Tipo_reporte,Nom_reporte,Nom_ciudad,Cod_ciudad, Cedcon_anterior,Nomcond_anterior,"+
                             " Cedprop_anterior, Nomprop_anterior, Placa_anterior, Via_anterior, Cadena, Tipo eliminacion" );
            for (int i = 0; i < vecurb.size(); i++){
                Ingreso_Trafico ingtrf = (Ingreso_Trafico) vecurb.get(i);
                model.rmtService.eliminarIngresoTrafico(ingtrf.getPlanilla());
                archivo.println( this.linea(ingtrf)  );
            }
            
            
            
            model.LogProcesosSvc.finallyProceso("DEPURAR INGRESO TRAFICO ", this.hashCode(), this.usuario, "PROCESO EXITOSO");
        }catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso("DEPURAR INGRESO TRAFICO ", this.hashCode(),this.usuario,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso("DEPURAR INGRESO TRAFICO ",this.hashCode(),this.usuario,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
    public String linea (Ingreso_Trafico ing){
        String linea =  ing.getDstrct() +","+
                        ing.getPlanilla() +","+
                        ing.getPlaca( ) +","+
                        ing.getCedcon( ) +","+
                        ing.getNomcond( ) +","+
                        ing.getCedprop( ) +","+
                        ing.getNomprop() +","+
                        ing.getOrigen( ) +","+
                        ing.getNomorigen(  ) +","+
                        ing.getDestino( ) +","+
                        ing.getNomdestino(  ) +","+
                        ing.getZona(  ) +","+
                        ing.getNomzona(  ) +","+
                        ing.getEscolta( ) +","+
                        ing.getCaravana(  ) +","+
                        ing.getFecha_despacho(  ) +","+
                        ing.getPto_control_ultreporte(  ) +","+
                        ing.getNompto_control_ultreporte(  ) +","+
                        ing.getFecha_ult_reporte(  ) +","+
                        ing.getPto_control_proxreporte() +","+
                        ing.getNompto_control_proxreporte(  ) +","+
                        ing.getFecha_prox_reporte(  ) +","+
                        ing.getUlt_observacion(  ) +","+
                        ing.getLast_update(  ) +","+
                        ing.getUser_update(  ) +","+
                        ing.getCreation_date(  ) +","+
                        ing.getCreation_user(  ) +","+
                        ing.getBase( ) +","+
                        ing.getDemora( ) +","+
                        ing.getVia(  ) +","+
                        ing.getFecha_salida(  ) +","+
                        ing.getCel_cond(  ) +","+
                        ing.getCliente(  ) +","+
                        ing.getTipo_despacho( ) +","+
                        ing.getTipo_reporte(  ) +","+
                        ing.getNom_reporte() +","+
                        ing.getNom_ciudad( ) +","+
                        ing.getCod_ciudad( ) +","+
                        ing.getCedcon_anterior(  ) +","+
                        ing.getNomcond_anterior( ) +","+
                        ing.getCedprop_anterior(  ) +","+
                        ing.getNomprop_anterior(  ) +","+
                        ing.getPlaca_anterior(  ) +","+        
                        ing.getVia_anterior(  ) +","+
                        ing.getCadena() +","+
                        ing.getTipo_eliminacion(); 
        return linea;        
    }
    
    
    
     public static void main(String a []){
        DepurarIngresoTrafico proc = new DepurarIngresoTrafico();
        proc.start("FINV","DBASTIDAS");
    }
    
    
}
