/*
 * HReporteAnticiposPagados.java
 *
 * Created on 19 octubre de 2006, 03:53 PM
 */

package com.tsp.operation.model.threads;
import com.aspose.cells.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;
/**
 *
 * @author  ffernandez
 */
public class HReporteICA extends Thread{
    String user;
    String FechaI1;
    String FechaF1;
    String procesoName;
    String des;
    //String dstrct;
    Model model = new Model();
    
    public void start(String FechaI, String FechaF, String usuario){
        this.user = usuario;
        this.procesoName = "Reporte ICA";
        this.des = "Reporte Documentos Digitalizados del ICA";
        this.FechaI1 = FechaI;
        this.FechaF1 = FechaF;
        //this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
            model.reporteGeneralService.ReoporteIca(FechaI1, FechaF1);
            Vector vector =  model.reporteGeneralService.getVector();
           
            
            
            Date fecha = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String now = format.format(fecha);
            String fechaCrea = now.substring(0,10);

            Workbook libro = new Workbook();

            Worksheets hojas = libro.getWorksheets();

            Worksheet hoja = hojas.getSheet(0);

            hoja.setName("HOJA1");

            Font fuente = new Font();
            fuente.setBold(true);
            fuente.setName("arial");
            
            //CELDA TITULO
            Cell celda = hoja.getCell(1,1);
            Style estiloEmpresa = libro.createStyle();
            estiloEmpresa.setColor( com.aspose.cells.Color.GRAY);
            celda.setValue("TRASPORTES SANCHEZ POLO ");
            celda.setStyle(estiloEmpresa);
            estiloEmpresa.setFont ( fuente ); 
            estiloEmpresa.getBorderColor(2);
            //hoja.autoFitColumns();
            
           
            //CELDA FECHA INICIO
            celda = hoja.getCell(2,1);
            celda.setValue("FECHA INICIO  : "+FechaI1);
            celda.setStyle(estiloEmpresa);
            estiloEmpresa.setFont ( fuente ); 
            estiloEmpresa.getBorderColor(2);
            
            //CELDA FECHA FINAL
            celda = hoja.getCell(3,1);
            celda.setValue("FECHA FINAL  : "+FechaF1);
            celda.setStyle(estiloEmpresa);
            estiloEmpresa.setFont ( fuente ); 
            estiloEmpresa.getBorderColor(2);
            //COLUMNA DE DATOS
              
            int columnas = 0;
            int filas = 5;
            
              
            Style estiloDatos = libro.createStyle();
            estiloDatos.setColor(com.aspose.cells.Color.GRAY);
            estiloDatos.setFont ( fuente ); 
            
           
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("ITEMS");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("CLIENTE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("OT");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("VALOR OT");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("FACTURA");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("NOM_CIUDAD");
            celda.setStyle(estiloDatos);
            hoja.setColumnWidth( columnas,12 );
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("RUTA_OT");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("PAIS_DESTINO");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("DESCRIPCION_SJ");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("STD_JOB");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("CREACION");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("NOMBRE_CLIENTE");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("INV_AMOUNT_L");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("INV_AMOUNT_F");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("FECHA_OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("VALOR_OC");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("RUTA_PLANILLA");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("PLACA");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("CODIGO_PROPIETARIO");
            celda.setStyle(estiloDatos);
            
            celda = hoja.getCell(filas, columnas++);
            celda.setValue("NOMBRE_PROPIETARIO");
            celda.setStyle(estiloDatos);
            
             columnas = 1;
             int contador = 1;
             String RemesaAnt= "";
             String RemesaAct= "";
             String valorFact= "";
            for(int i=0; i<vector.size();i++ ){
                RemesaAct= "";
                columnas =0;
                valorFact = "";
                filas++ ;
          
                BeanGeneral rep = (BeanGeneral) vector.get(i);
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+contador);
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_01());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_02());
                RemesaAct = rep.getValor_02();
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_03());
                                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_04());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_05());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_06());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_07());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_08());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_09());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_10());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_11());
                
                valorFact = rep.getValor_12();
                if(valorFact.equals("")){
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue(""+rep.getValor_03());
                
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue(""+rep.getValor_03());  
                }
                else if(RemesaAct.equals(RemesaAnt)){
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue("0");

                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue("0");
                
                }
               
                else {  
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue(""+rep.getValor_12());
                
                    celda = hoja.getCell(filas, columnas++);
                    celda.setValue(""+rep.getValor_13());
                }
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_14());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_15());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_16());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_17());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_18());
                
                celda = hoja.getCell(filas, columnas++);
                celda.setValue(""+rep.getValor_19());
                
                 celda = hoja.getCell(filas, columnas++);
                 celda.setValue(""+rep.getValor_20());
               
                 contador++;
                 RemesaAnt= rep.getValor_02();
            }
         
            //hoja.autoFitColumns();
           
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
            libro.save(Ruta1 +"ReporteICA" + hoy + ".xls");
             
        
          
        System.out.println("YA");
          
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
   
    public static void main(String[]sfhgsd) throws Exception{
        HReporteICA h = new HReporteICA();
        h.start("2007-04-01", "2007-04-17", "HOSORIO");
    }
    
}
