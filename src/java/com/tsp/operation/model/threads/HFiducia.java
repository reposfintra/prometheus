/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.threads;


import java.util.*;
import java.text.*;
import java.io.*;

import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.services.ConstanteService;
import com.tsp.util.LogWriter;
import com.tsp.util.Util;
import com.aspose.cells.License;
import com.aspose.cells.*;





import java.io.File;
import java.util.List;





/**
 *
 * @author Alvaro
 */
public class HFiducia extends Thread {
    

    private Model   model;
    private Usuario usuario;
    private String  distrito;
    private String[] id_factura;
    private String[] fiducia;
    private LogWriter logWriter;    
    private String retorno = "normal"; 
    
    
    


    
        /** Creates a new instance of HPrefacturaDetalle */
    public HFiducia () {
    }

    public void start(Model model, Usuario usuario, String distrito, String[] id_factura, String[] fiducia, LogWriter logWriter ){

        this.usuario        = usuario;
        this.model          = model;
        this.distrito         = distrito;
        this.id_factura     = id_factura;
        this.fiducia        = fiducia;

        this.logWriter      = logWriter;

        super.start();
    }

    


    public synchronized void run(){
        
            
                
                try {    
                    
                    // model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del reporte de prefactura : " + prefactura, usuario.getLogin());                    

                
                   String  TIPO_DOCUMENTO_FAC      = model.constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAC", "fintra");
                   String  SIGLA_PM                = model.constanteService.getValor("FINV", "SIGLA_PM", "fintra");
                   String TIPO_DOCUMENTO_ICR       = model.constanteService.getValor("FINV","TIPO DOCUMENTO ICR","fintra");
                    

                   String login = usuario.getLogin();
                   java.util.Date fecha = new java.util.Date();
                   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                   String fechaCreacion = sdf.format(fecha);

                   String estado = "";                                                  // Contiene el estado de la actualizacion de la base de datos en lote ""= OK , "ERROR"= Se presento error
                   String comandoSQL = "";                                              // Almacena un string SQL
                   Vector listaComandoSQL =new Vector();                                // Almacena una lista de String SQL que actualizaran la BD en modo transaccion
                   
                   if( id_factura!= null){

                        //try {

                            // PROCESAR CADA FACTURA SELECCIONADA
                            for(int i=0;i<id_factura.length;i++){

                                
                                String factura_indice = id_factura[i];
                                String[] arrayParametro = factura_indice.split("-");
                                String facturaMaestra = arrayParametro[0].trim();
                                String indiceFiducia = arrayParametro[1].trim();
                                int   f =  Integer.parseInt(indiceFiducia);
                                
                                String fiduciaSeleccionada = fiducia[f-1];
                                String error = "NO";                            // Para controlar si hay algun error en la construccion de los items de las facturas y suspender el proceso de esa NM
                                
                                // S� la NM se queda en fintra 
                                if(retorno.equalsIgnoreCase("PM") || retorno.equalsIgnoreCase("RM")) {
                                    
                                    comandoSQL =  model.fiduciaService.retornarFiducia(distrito, facturaMaestra, TIPO_DOCUMENTO_FAC, login, logWriter);
                                    /*} else {
                                    // Actualizacion de la factura NM para dejarla con referencia a fintra
                                       comandoSQL = model.fiduciaService.actualizarFacturaRefNM(distrito, facturaMaestra, TIPO_DOCUMENTO_FAC, login, logWriter);
                                    }*/
                                   if(comandoSQL.equals("")) {
                                       logWriter.log("No se pudo crear el comprobante SM de la:  "  + facturaMaestra);
                                       logWriter.log("Error en la conformacion del script de la actualizacion de la PM para referenciarla a fintra");    
                                       listaComandoSQL.clear();
                                       error = "SI";
                                   }
                                } else if(retorno.equalsIgnoreCase("PMF") || retorno.equalsIgnoreCase("NMF")) {
                                    comandoSQL =  model.fiduciaService.fintraaFiducia(distrito, facturaMaestra, TIPO_DOCUMENTO_FAC, login, fiduciaSeleccionada);
                                    if(comandoSQL.equals("")) {
                                       logWriter.log("No se pudo crear el comprobante DM de la:  "  + facturaMaestra);
                                       logWriter.log("Error en la conformacion del script de la actualizacion de la PM para referenciarla a "+fiduciaSeleccionada);    
                                       listaComandoSQL.clear();
                                       error = "SI";
                                   }
                                } else {
                                    
                                String  CUENTA_EXTEMPORANEA = model.constanteService.getValor("FINV",fiduciaSeleccionada + " CUENTA EXTEMPORANEA PM","fintra");
                                String  CUENTA_INTERESES = model.constanteService.getValor("FINV",fiduciaSeleccionada + " CUENTA INTERESES PM","fintra");
                                String  CUENTA_CAPITAL = model.constanteService.getValor("FINV",fiduciaSeleccionada + " CUENTA CAPITAL PM","fintra");   
                                
                                String  HANDLE_CODE    = model.constanteService.getValor("FINV",fiduciaSeleccionada + " HANDLE CODE FACTURA PM", "fintra");    
                                String  HANDLE_CODE_PM = model.constanteService.getValor("FINV",fiduciaSeleccionada + " HANDLE CODE INGRESO PM", "fintra"); 
                                
                                
                                String  CUENTA_INGRESO_CABECERA = model.constanteService.getValor("FINV",fiduciaSeleccionada + " CUENTA INGRESO CABECERA PM","fintra");
                                String CUENTA_INGRESO_DETALLE = (fiduciaSeleccionada.equals("FIDFIV") || fiduciaSeleccionada.equals("FIDFUP"))
                                        ? model.constanteService.getCuentaHCIngreso(facturaMaestra)
                                        : model.constanteService.getValor("FINV",fiduciaSeleccionada + " CUENTA INGRESO DETALLE PM","fintra");

                                int numero_item = 0;
                                
                                List listaItemDetalle = new LinkedList();
                                
                                // Creacion del detalle de la factura para el concepto de Valor Extemporaneo
                                
                                numero_item++;
                                String descripcion_concepto = "Valor extemporaneo" ;          
                                String condicion = "descripcion like 'Valor extemporaneo%'" ;   
                                
                                listaItemDetalle = model.fiduciaService.seleccionaConceptoNM (distrito,   facturaMaestra,  numero_item,  descripcion_concepto,  condicion,  TIPO_DOCUMENTO_FAC,  SIGLA_PM,  login,  fechaCreacion,
                                                                 HANDLE_CODE ,  CUENTA_EXTEMPORANEA,   logWriter); 
                                
                                
                                if ( listaItemDetalle.size() == 0 ) {
                                    numero_item--;
                                }
                                else {
                                       for(int j=0;(j<listaItemDetalle.size() & error.equals("NO")  )  ;j++){
                                           
                                           FacturaDetalleCxC itemDetalle = (FacturaDetalleCxC) listaItemDetalle.get(j);
                                            comandoSQL = model.fiduciaService.insertarFacturaDetallePM(itemDetalle, logWriter);

                                            if(comandoSQL.equals("")) {
                                                logWriter.log("No se pudo crear el item para la factura PM de la:  "  + facturaMaestra + " Factura especifica : " + itemDetalle.getDocumento() );
                                                logWriter.log("Error en la conformacion del script del detalle de la factura PM " + facturaMaestra + " Valor Extemporaneo");
                                                listaComandoSQL.clear();
                                                error = "SI";
                                            }
                                            else {
                                                listaComandoSQL.add(comandoSQL);
                                            }   
                                       }
                                }                                      
                                   
                                
                                // Creacion del detalle de la factura para el concepto de Intereses
                                
                                if (error.equals("NO")){
                                
                                    numero_item++;
                                    descripcion_concepto = "Intereses" ;          
                                    condicion = "descripcion like 'Intereses%'" ;                                
                                    listaItemDetalle = model.fiduciaService.seleccionaConceptoNM (distrito,   facturaMaestra,  numero_item,  descripcion_concepto,  condicion,  TIPO_DOCUMENTO_FAC,  SIGLA_PM,  login,  fechaCreacion,
                                                                                             HANDLE_CODE ,  CUENTA_INTERESES,  logWriter);                               

                                    if ( listaItemDetalle.size() == 0  ) {
                                        numero_item--;
                                    }
                                    else {
                                           
                                           for(int j=0;(j<listaItemDetalle.size() & error.equals("NO")  )  ;j++){

                                                FacturaDetalleCxC itemDetalle = (FacturaDetalleCxC) listaItemDetalle.get(j);                                    

                                                comandoSQL = model.fiduciaService.insertarFacturaDetallePM(itemDetalle, logWriter);

                                                if(comandoSQL.equals("")) {
                                                    logWriter.log("No se pudo crear la factura PM de la:  "  + facturaMaestra+ " Factura especifica : " + itemDetalle.getDocumento());
                                                    logWriter.log("Error en la conformacion del script del detalle de la factura PM "  + facturaMaestra + " Valor Intereses");
                                                    listaComandoSQL.clear();
                                                    error = "SI";
                                                }
                                                else {
                                                    listaComandoSQL.add(comandoSQL);
                                                }
                                           }
                                    }                                    
                                }
   
                                
                                // Creacion del detalle de la factura para el concepto de Capital
                                
                                
                                if (error.equals("NO")){                                

                                    numero_item++;
                                    descripcion_concepto = "Capital" ;          
                                    condicion = "(descripcion not like 'Intereses%' and descripcion not like 'Valor extemporaneo%')" ;                                
                                    listaItemDetalle = model.fiduciaService.seleccionaConceptoNM (distrito,   facturaMaestra,  numero_item,  descripcion_concepto,  condicion,  TIPO_DOCUMENTO_FAC,  SIGLA_PM,  login,  fechaCreacion,
                                                                                             HANDLE_CODE ,  CUENTA_CAPITAL, logWriter);                               

                                    if ( listaItemDetalle.size() == 0  ) {
                                        numero_item--;
                                    }
                                    else {
                                        
                                           for(int j=0;(j<listaItemDetalle.size() & error.equals("NO")  )  ;j++){

                                                FacturaDetalleCxC itemDetalle = (FacturaDetalleCxC) listaItemDetalle.get(j);                                            
                                        
                                                comandoSQL = model.fiduciaService.insertarFacturaDetallePM(itemDetalle, logWriter);

                                                if(comandoSQL.equals("")) {
                                                    logWriter.log("No se pudo crear la factura PM de la:  "  + facturaMaestra + " Factura especifica : " + itemDetalle.getDocumento());
                                                    logWriter.log("Error en la conformacion del script del detalle de la factura PM "  + facturaMaestra + " Valor Capital");
                                                    listaComandoSQL.clear();
                                                    error = "SI";;
                                                }
                                                else {
                                                    listaComandoSQL.add(comandoSQL);
                                                }
                                           }
                                    }
                                    
                                }
                                
                                // Creacion de la cabecera de la factura
                                
                                if (error.equals("SI")){
                                    continue;
                                }
                                else {
                                    comandoSQL = model.fiduciaService.insertarFacturaPM(distrito, facturaMaestra, numero_item, fiduciaSeleccionada, TIPO_DOCUMENTO_FAC, SIGLA_PM, login, fechaCreacion, HANDLE_CODE, logWriter);

                                    if(comandoSQL.equals("")) {
                                        logWriter.log("No se pudo crear la factura PM de la:  "  + facturaMaestra);
                                        logWriter.log("Error en la conformacion del script de la cabecera de la factura PM "  + facturaMaestra );
                                        listaComandoSQL.clear();
                                        error = "SI";
                                    }
                                    else {
                                        listaComandoSQL.add(comandoSQL);
                                    }
                                }

                                
                                // Creacion de la cabecera del ingreso

                                if (error.equals("SI")){  
                                    continue;
                                }
                                else {
                                    
                                    comandoSQL = model.fiduciaService.insertarIngresoPM(distrito,   facturaMaestra,  TIPO_DOCUMENTO_FAC,  TIPO_DOCUMENTO_ICR,  login,  fechaCreacion,
                                                CUENTA_INGRESO_CABECERA,  HANDLE_CODE_PM ,  logWriter);

                                    if(comandoSQL.equals("")) {
                                        logWriter.log("No se pudo crear la factura PM de la:  "  + facturaMaestra);
                                        logWriter.log("Error en la conformacion del script del ingreso de la cabecera con el que se cancela la NM "   + facturaMaestra  );  
                                        listaComandoSQL.clear();
                                        error = "SI";
                                    }
                                    else {
                                        listaComandoSQL.add(comandoSQL);
                                    }
                                }
                                
                                // Creacion deL detalle del ingreso
                                
                                if (error.equals("SI")){  
                                    continue;
                                }
                                else {
                                    
                                   comandoSQL = model.fiduciaService.insertarIngresoDetallePM( distrito,   facturaMaestra,  TIPO_DOCUMENTO_FAC,  TIPO_DOCUMENTO_ICR,  login,  fechaCreacion,
                                                CUENTA_INGRESO_DETALLE,  logWriter);

                                   if(comandoSQL.equals("")) {
                                       logWriter.log("No se pudo crear la factura PM de la:  "  + facturaMaestra);
                                       logWriter.log("Error en la conformacion del script del ingreso del detalle con el que se cancela la NM "  + facturaMaestra );   
                                       listaComandoSQL.clear();
                                       error = "SI";
                                   }
                                   else {
                                       listaComandoSQL.add(comandoSQL);
                                   }
                                }


                                // Actualizacion de la factura NM para dejarla con saldo cero
                                
                                if (error.equals("SI")){  
                                    continue;
                                }
                                else {
                                    
                                   comandoSQL = model.fiduciaService.actualizarFacturaNM( distrito,  facturaMaestra,  TIPO_DOCUMENTO_FAC,  login,  logWriter);

                                   if(comandoSQL.equals("")) {
                                       logWriter.log("No se pudo crear la factura PM de la:  "  + facturaMaestra);
                                       logWriter.log("Error en la conformacion del script de la actualizacion de la NM para cancelarla");    
                                       listaComandoSQL.clear();
                                       error = "SI";
                                   }
                                   else {
                                       listaComandoSQL.add(comandoSQL);
                                   }  
                                }
                                
                                
                                // Ejecuta el batch para actualizar en productivo

                                if (error.equals("SI")){
                                    continue;
                                }
                                else {
                                
                                    // Ejecutar batch de transaccion
                                    estado = model.consorcioService.ejecutarBatchSQL(listaComandoSQL);
                                    if (estado.equalsIgnoreCase("ERROR")){
                                        // Al final no se pudo actualizar en productivo
                                        logWriter.log("Los datos no fueron registrados. Se genero un rollback sobre la base de datos ");
                                        if(listaComandoSQL.size() > 0) {
                                            // Existen script SQL que se iban a procesar
                                            for(int j=0; i<listaComandoSQL.size() ; i++) {
                                                // Imprimiendo los SQL que estan por ejecutarse
                                                String sql = (String) listaComandoSQL.get(j);
                                                logWriter.log(sql);
                                            }
                                        }
                                    }
                                }
                                    
                                
                                listaComandoSQL.clear();
                                }
                            } // Fin de procesar  facturas


                        }

                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");   

                }
                catch (Exception e){
                    Util.imprimirTrace(e);
                    try{
                        //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
                    }catch (Exception ex){
                        System.out.println("Error HFiducia ...\n" + ex.getMessage());
                    }
                }        
        
        
    }

    public void setRetorno(String bln) {
        retorno = bln;
    }

          
    
    
}
