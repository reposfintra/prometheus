/*
 * ReporteDiarioDespacho.java
 *
 * Created on 5 de septiembre de 2005, 11:08 AM
 */

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  Administrador
 */
public class SendMail extends Thread{
    
    private Email email;
    /** Creates a new instance of ReporteDiarioDespacho */
    public SendMail() {
    }
    
    public void start(Email e) {
        this.email = e;
        super.start();
    }
    
    public synchronized void run(){
        ////System.out.println("Corro el prog de senmail");
        Email send = new Email();
        if(email==null){
            ////System.out.println("El OBJETO Q RECIVO ES NULO ");
        }
        try{
            //ESTO ES POR INTERNET DIRECTO,
            send.send("198.63.51.247", email);
            ////System.out.println("Termine de enviar el email");
        }catch(Exception e){
            ////System.out.println("ENCONTRE UN ERROR : " +e.getMessage());
            if(e.getMessage().indexOf("unreachable")>=0){
                ////System.out.println("No pude enviarlo con el primer host lo envio por el segundo");
                try{
                    send.send("proxy.tsp.com", email);
                    ////System.out.println("Termine de enviar el email");
                }catch(Exception e1){
                    ////System.out.println("ENCONTRE UN SEGUNDO ERROR : " +e.getMessage());
                }
            }
        }finally{
            //            super.destroy();
        }
    }
    
}
