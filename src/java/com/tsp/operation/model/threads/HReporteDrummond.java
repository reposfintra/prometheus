/**************************************************************************
 * Nombre clase: hReporteDrummond.java                                     *
 * Descripci�n: Clase que envia los datos a excel                          *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 1 de octubre de 2005, 08:20 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/
 

package com.tsp.operation.model.threads;

import com.tsp.util.*;
/**
 *
 * @author  Igomez
 */


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;

import java.util.*;
import java.lang.*;
import java.io.*;


public class HReporteDrummond extends Thread{

   Model   model;
   Usuario  usuario;
   String  FecIni;
   String  FecFin;
   String  Cliente;
   
   //Log Procesos
   private String procesoName;
   private String des;
   
    public void init(Model modelo, Usuario usu , String Fecini, String Fecfin, String cliente){
        this.model   = modelo;
        this.usuario = usu;
        this.FecIni  = Fecini;
        this.FecFin  = Fecfin;
        this.Cliente = cliente; 
        this.procesoName = "Reporte Drummond";
        this.des = "Reporte Drummond "+Fecini+" - "+Fecfin;
        super.start();
    }
    
    
    public synchronized void run(){
        try{
            
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, usuario.getLogin());  
            model.ReporteDrummondSvc.searchReporte(FecIni, FecFin, Cliente);
            List lista = model.ReporteDrummondSvc.getReporte();
            
           //if (lista!=null && lista.size()>0){
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion";

                String ruta = path + "/" + usuario.getLogin() + "/";// + model.movplaService.obtenerFechaActualEnFormato("YYYYMM") ;

                File f = new File(ruta); 
                if ( !f.exists() ){
                    f.mkdirs();
                    ////System.out.println("directorios creados en: "+f.getAbsolutePath());
                }
                
                
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( ruta + "/Procesada_"+ Cliente + ".xls");
                ////System.out.println(" RUTA ---------- " + ruta + "Procesada_"+ Cliente + ".xls");
               
                // Definicion de Estilos para la hoja de excel
               
                HSSFCellStyle fecha   = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
                HSSFCellStyle texto   = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
                HSSFCellStyle texto3  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto2  = xls.nuevoEstilo("Book Antiqua", 9 ,  true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle total   = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
                HSSFCellStyle titulo2 = xls.nuevoEstilo("Book Antiqua", 9  , true    , false, "text"       , HSSFColor.BLUE.index, HSSFColor.YELLOW.index, HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle titulo  = xls.nuevoEstilo("Book Antiqua", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);

                xls.obtenerHoja("Reporte" + Cliente);
                
                int fila = 1;
                int col  = 0;
                xls.cambiarAnchoColumna(col+1, 4000);
                xls.cambiarAnchoColumna(col+6, 4000);
                xls.combinarCeldas(fila, col, fila, col+3);
                xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO"             , texto  );
                xls.adicionarCelda(fila++ ,col   , "SUCURSAL: BARRANQUILLA "             , texto  );
               
                fila++;
                xls.adicionarCelda(fila   ,col++ , "DESPACHO #"        , titulo2 ); 
                xls.adicionarCelda(fila   ,col++ , "REMISI�N"          , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "FECHA DESPACHO"    , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "EMPRESA/ DPTO"     , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "ORIGEN"            , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "DESTINO"           , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "DO."               , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "PO."               , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "CONTENIDO"         , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "TIPO VEHICULO"     , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "COLOR"             , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "PLACA"             , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "CONDUCTOR"         , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "CEDULA"            , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "CELULAR"           , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "SOLICITANTE DLTD"  , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "TEL-EXT-CEL"       , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "FECHA ENTREGA"     , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "COST CENTER"       , titulo2 );
                xls.adicionarCelda(fila   ,col++ , "PO."               , titulo2 );
               
                Iterator it = lista.iterator();

                while(it.hasNext()){

                    fila++;
                    col = 0;
                     
                    ReporteDrummond ReportDrum = (ReporteDrummond) it.next();
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getDespacho()       , texto3);
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getRemision()       , texto3);
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getFecDespacho()    , texto3);
                    col++;
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getOrigen()         , texto3);
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getDestino()        , texto3);
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getDO()             , texto3);
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getPO()             , texto3);
                    col++;
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getTipoVehiculo()   , texto3);
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getColor()          , texto3);
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getPlaca()          , texto2);
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getConductor()      , texto3);
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getCedula()         , texto3);
                    xls.adicionarCelda(fila ,col++ , ReportDrum.getCelular()        , texto3);
          

                }
                     
                
                xls.cerrarLibro();
                
            //}
             ////System.out.println("Listo..");
             model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){            
            ////System.out.println("Error : " + ex.getMessage());
           try{            
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),usuario.getLogin() ,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{               
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),usuario.getLogin(),"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
