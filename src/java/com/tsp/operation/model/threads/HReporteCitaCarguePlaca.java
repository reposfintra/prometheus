/***************************************************************************
 * Nombre clase : ............... HReporteCitaCarguePlaca.java             *
 * Descripcion :................. Hilo que permite generar un reporte de   *
 *                                Placas con su fecha de planeacion de     *
 *                                 cargue                                  *
 * Autor :....................... Ing. Karen Reales                        *
 * Fecha :........................ 15 de Febrero de 2006, 11:38   AM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/

package com.tsp.operation.model.threads;
import java.util.*;
import java.io.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import java.sql.*;

public class HReporteCitaCarguePlaca extends Thread{
    private Vector reporte;
    private String fec1;
    private String fec2;
    private String user;
    Model model;
    /** Creates a new instance of ReporteRegistroTiempoXLS */
    public HReporteCitaCarguePlaca() {
    }
    public void start( String fec1,String user) {
        this.fec1 = fec1;
        this.user = user;
        
        super.start();
    }
    
    public synchronized void run(){
        Vector cargue;
        Vector descargue;
        
        try{
            model= new Model();
        
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta") + "/exportar/migracion";
            
            String ruta = path + "/" + user + "/";
            cargue=new Vector();
            descargue=new Vector();
            
            ////System.out.println("Empezamos...");
            try{
                ////System.out.println("Buscamos los vectores...");
              //  model.LogProcesosSvc.InsertProceso("Cita cargue", this.hashCode(), "Reporte Cita Cargue", user);
                model.citacargueService.reportePlacasCitaCargue(fec1);
                ////System.out.println("Termine buscando los vectores...");
                cargue = model.citacargueService.getCargue();
                 
            }catch(SQLException e){
                ////System.out.println("Error llenando el informe");
                throw new SQLException("ESTE ES EL ERROR... " + e.getMessage() + " " + e.getErrorCode());
            }
            
            fec1 = fec1.substring(0,10).replaceAll("-","");
            String nombreArch= "ReporteCitaCargue["+fec1+"].xls";
            String       Hoja  = "CITAS CARGUE DIA "+fec1;
            
            String       Ruta  =ruta+ nombreArch;
            // Definicion de Estilos para la hoja de excel
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( Ruta);
            
            HSSFCellStyle fecha   = xls.nuevoEstilo("Verdana", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle texto   = xls.nuevoEstilo("Verdana", 10 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle texto3  = xls.nuevoEstilo("Verdana", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle texto2  = xls.nuevoEstilo("Verdana", 9 ,  true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle total   = xls.nuevoEstilo("Verdana", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
            HSSFCellStyle titulo2 = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.WHITE.index, HSSFColor.BLUE.index, HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero  = xls.nuevoEstilo("Verdana", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle titulo  = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);
            
            xls.obtenerHoja("Reporte Citas de Cargue");
            
            int fila = 1;
            int col  = 0;
            xls.combinarCeldas(fila, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO"             , texto  );
            xls.combinarCeldas(2, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "REPORTE CITAS DE CARGUE"             , texto  );
            xls.combinarCeldas(3, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "FECHA: "+fec1            , texto  );
            
            xls.cambiarAnchoColumna(0, 6000);
            xls.cambiarAnchoColumna(1, 10000);
            fila++;
            xls.adicionarCelda(fila,0 , "PLaca" , titulo2 );
            xls.adicionarCelda(fila,1 , "Fecha de Cargue" , titulo2 );
            
            
            
            for(int i =0; i<cargue.size();i++){
		CitaCargue c = (CitaCargue) cargue.elementAt(i);
                fila++;
                col++;
                xls.adicionarCelda(fila ,0, c.getPlaca(), texto3);
                xls.adicionarCelda(fila ,1, c.getFecha_pla(), texto3);
            
            }
            
            
            
            xls.cerrarLibro();
        }catch(Exception e){
            ////System.out.println("Hay un error: "+e.getMessage());
            /*try{
                model.LogProcesosSvc.finallyProceso("Cita Cargue", this.hashCode(),user,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso("Cita Cargue",this.hashCode(),user,"ERROR :");
                }catch(Exception p){    }
            }*/
        }
    }
    
     public static void main(String a [])throws SQLException{
        HReporteCitaCarguePlaca hilo = new HReporteCitaCarguePlaca();
        hilo.start("2006-01-01 00:00","KREALES");
    }
}
