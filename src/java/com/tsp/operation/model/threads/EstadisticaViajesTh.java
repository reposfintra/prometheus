/********************************************************************
 *      Nombre Clase.................   EstadisticaViajesTh.java
 *      Descripci�n..................   Hilo para la obtencion de estadistica de viajes de proveedor/conductor
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   15.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import com.tsp.util.UtilFinanzas;
import java.util.Vector;
import java.util.Hashtable;
import java.text.*;




public class EstadisticaViajesTh extends Thread{
    
    private int ano;
    private String mes;
    private Usuario user;
    private String cia;
    private boolean update_C;
    
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public synchronized void start(Model modelo, int ano, String mes, Usuario user, String cia, boolean update_C){
        this.ano = ano;
        this.mes = mes;
        this.user = user;
        this.cia = cia;
        this.update_C = update_C;
        
        this.model   = modelo;
        this.procesoName = "Generacion de Estadisticas Viajes Conductor-Propietario";
        this.des = "Generacion de Estadisticas Viajes Conductor-Propietario:  " + ano + " - " + mes;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user.getLogin());
            
            model.estadViajesSvc.obtenerViajes(ano, mes, cia);
            Vector vec = model.estadViajesSvc.getVector();
            
            for(int i=0; i<vec.size(); i++){
                EstadViajes obj = new EstadViajes();
                obj = (EstadViajes) vec.elementAt(i);;
                obj.setDstrct(cia);
                obj.setBase(user.getBase());
                obj.setCreation_user(user.getLogin());
                obj.setMes(mes);
                obj.setAno(ano);
                
                model.estadViajesSvc.setNviajes(obj);
                boolean existe = model.estadViajesSvc.existe();
                
                if( !existe ){
                    model.estadViajesSvc.insertar();
                } else {
                    int temp = obj.getCant_mes();
                    obj.setCant_mes(0);
                    model.estadViajesSvc.setNviajes(obj);
                    model.estadViajesSvc.actualizar();
                    obj.setCant_mes(temp);
                    model.estadViajesSvc.setNviajes(obj);
                    model.estadViajesSvc.actualizar();
                }
            }
            
            /**
             * Actualizaci�n del campo nviajes en el archivo conductir
             */
            if( this.update_C ){
                
                /* Calculo del per�odo de inicio */
                int ano = Util.AnoActual();
                int mes = Util.MesActual();
                
                int anoStart = ano;
                int meses = mes - 6;
                int mesStart = meses;
                if ( meses<1 ){
                    anoStart = anoStart - 1;
                    mesStart = 12 + meses;
                }
                
                //////System.out.println(".................. ( " + ano + mes + " ) Per�odo Inicio: " + anoStart + "-" + mesStart);
                
                model.estadViajesSvc.listar(anoStart, "C", cia);
                vec = model.estadViajesSvc.getVector();
                
                String nit = "";
                int viajes = 0; /* Contador del n�mero de viajes del conductor */
                for( int i=0; i<vec.size(); i++){
                    EstadViajes obj = new EstadViajes();
                    obj = (EstadViajes) vec.elementAt(i);
                    
                    if( i==0 ){
                        nit = obj.getNit();
                    }
                    
                    
                    if( nit.compareTo(obj.getNit())!=0){
                        //////System.out.println("................. NIT: " + nit  + " - VIAJES : " + viajes);
                        model.estadViajesSvc.actualizarViajes(nit, viajes, user.getLogin());
                        nit = obj.getNit();
                        viajes = 0;
                    }
                    
                    int sum_aux = 0;
                    int[] cant_meses = obj.getCant_meses();
                    
                    if( obj.getAno()==anoStart ){
                        for( int j=(mesStart-1); j<12; j++){
                            sum_aux += cant_meses[j];
                        }
                        //////System.out.println("............. nit: " + nit + "- viajes ano: " + obj.getAno() + " : " + sum_aux);
                    } else {
                        for( int j=0; j<mes; j++){
                            sum_aux += cant_meses[j];
                        }
                        //////System.out.println("............. nit: " + nit + "- viajes ano: " + obj.getAno() + " : " + sum_aux);                        
                    }
                    viajes += sum_aux;
                }
            }

            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "PROCESO FINALIZADO CON EXITO.");
            
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}