/**
 * Nombre        HReporteMABE.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         30 de octubre de 2006, 05:39 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.threads;


import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

public class HReporteMABE extends Thread {
    
    String  ano;
    String  mes;
    String  tipo;
    String  cliente;
    com.tsp.finanzas.presupuesto.model.Model  modelpto;
    com.tsp.operation.model.Model model;
    Usuario usuario;
    SimpleDateFormat fmt;
    
    
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, porcentaje, letraCentrada, numeroNegrita;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;
    
    
    /** Crea una nueva instancia de  HReporteMABE */
    public HReporteMABE() {
    }
    
    public void start(String cliente, String ano, String mes, String tipo, Usuario usuario,
    com.tsp.finanzas.presupuesto.model.Model modelpto,
    com.tsp.operation.model.Model model
    ){
        this.ano      = ano;
        this.mes      = mes;
        this.modelpto = modelpto;
        this.model    = model;
        this.tipo     = tipo;
        this.usuario  = usuario;
        this.cliente  = cliente;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso("HReporteMABE", this.hashCode(), "Generacion de Reporte MABE", usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso("HReporteMABE", this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso("HReporteMABE", this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error ReporteMABE ...\n" + e.getMessage());
            }
        }
    }
    
    
    public void generarRUTA() throws Exception{
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);
            
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAzul.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo3      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo4      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo5      = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            letra        = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE,1);
            letraCentrada= xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER,1);
            numero       = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE,1);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE,1);
            porcentaje   = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
        
    }
    
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            xls.adicionarCelda(2,0, "PERIODO", titulo1);
            xls.adicionarCelda(2,1, ano+mes  , titulo1);
            xls.adicionarCelda(3,0, "USUARIO", titulo1);
            xls.adicionarCelda(3,1, usuario.getNombre() , titulo1);
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ReporteMABE_" + fmt.format( new Date() ) +".xls", "REPORTE MABE");
            fila = 6;
            
            if (tipo.equals("AMB")){
                generarBloque( this.cliente, this.ano, this.mes, "IMP" );
                fila+=3;
                generarBloque( this.cliente, this.ano, this.mes, "EXP" );
            }else{
                generarBloque( this.cliente, this.ano, this.mes, this.tipo );
            }
            
            this.cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en ExportarResumenPrestamos ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    public String getString(TreeMap lista, String key, String def){
        return (lista.get(key)!=null?(String) lista.get(key):def);
    }
    
    public void generarBloque(String cliente, String ano, String mes, String tipo) throws Exception{
        try{
            modelpto.ReportesSvc.obtenerDocumentos(cliente, ano, mes, tipo);
            
            TreeMap datos      = modelpto.ReportesSvc.getXLSReferencias();
            TreeMap referencia = (TreeMap) datos.get("referencias");
            TreeMap origenes   = (TreeMap) datos.get("origenes");
            TreeMap destinos   = (TreeMap) datos.get("destinos");
            Iterator     itOri = null, itDes = null;
            System.gc();
            
            
            String subtitulo   = (tipo.equals("IMP")?"IMPORTACIONES":"EXPORTACIONES");
            int col = 0;
            xls.adicionarCelda(fila  ,col, subtitulo, titulo5);
            xls.combinarCeldas(fila , col, fila, col+5);
            fila++;
            
            
            
            
            if (origenes.isEmpty()){
                xls.adicionarCelda(fila  ,col, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , col, fila, col+5);
                fila++;
                
            } else {
                
                // encabezado del reporte
                xls.adicionarCelda(fila  ,col  , "DOCUMENTO", titulo4);
                xls.adicionarCelda(fila+1,col  , "", titulo4);
                xls.combinarCeldas(fila  ,col  , fila+1, col);
                xls.cambiarAnchoColumna(col, 4000);
                col++;
                
                xls.adicionarCelda(fila  ,col, "DESTINOS" , titulo4);
                xls.adicionarCelda(fila+1,col, "", titulo4);
                xls.combinarCeldas(fila  ,col, fila+1, col);
                xls.cambiarAnchoColumna(col, 5000);
                col++;
                
                
                xls.combinarCeldas(fila, col, fila, (col+origenes.size()-1));
                xls.combinarCeldas(fila, col+origenes.size()  , fila, (col+origenes.size()*2-1));
                xls.combinarCeldas(fila, col+origenes.size()*2, fila, (col+origenes.size()*3-1));
                fila++;
                
                
                // columnas dinamicas del encabezado
                itOri = origenes.values().iterator();
                while (itOri.hasNext()){
                    String ag = (String) itOri.next();
                    xls.adicionarCelda(fila-1, col, "ENTREGADOS", titulo4);
                    xls.adicionarCelda(fila-1, col + (origenes.size()  ), "CARGADOS", titulo4); // ejecutado
                    xls.adicionarCelda(fila-1, col + (origenes.size()*2), "SALDOS", titulo4); // saldo
                    xls.adicionarCelda(fila, col, ag, titulo4); // presupuestado
                    xls.adicionarCelda(fila, col + (origenes.size()  ), ag, titulo4); // ejecutado
                    xls.adicionarCelda(fila, col + (origenes.size()*2), ag, titulo4); // saldo
                    xls.cambiarAnchoColumna(col , 5000);
                    xls.cambiarAnchoColumna(col + (origenes.size()  ), 5000);
                    xls.cambiarAnchoColumna(col + (origenes.size()*2), 5000);
                    col++;
                }
                
                
                // grabacion de datos del reporte
                System.gc();
                fila++;
                Iterator itRef = referencia.values().iterator();
                while (itRef.hasNext()){
                    TreeMap doc = (TreeMap) itRef.next();
                    xls.adicionarCelda(fila, 0 , getString(doc,"documento", ""), letraCentrada);
                    
                    
                    itDes = destinos.values().iterator();
                    while (itDes.hasNext()){
                        col = 1;
                        String destino = (String) itDes.next();
                        
                        if (destinoValido( destino, origenes, doc )) {
                            
                            xls.adicionarCelda(fila, col ,destino ,letra);
                            col++;
                            
                            itOri = origenes.values().iterator();
                            while (itOri.hasNext()){
                                String origen = (String) itOri.next();
                                String key = origen + "-" + destino;
                                xls.adicionarCelda(fila,col                    ,Double.parseDouble(getString(doc, key +"-PTO", "0")),numero);
                                xls.adicionarCelda(fila,col+(origenes.size()  ),Double.parseDouble(getString(doc, key +"-EJE", "0")),numero);
                                xls.adicionarCelda(fila,col+(origenes.size()*2),Double.parseDouble(getString(doc, key +"-SAL", "0")),numero);
                                col++;
                            }
                            fila++;
                        }
                    }
                    
                    fila ++;
                }
            }
            
            System.gc();
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    private boolean destinoValido(String destino, TreeMap origenes, TreeMap doc) {
        Iterator itOri = origenes.values().iterator();
        while (itOri.hasNext()){
            String origen = (String) itOri.next();
            String key = origen + "-" + destino;
            if (!getString(doc, key +"-PTO", "").equals("")) return true;
            if (!getString(doc, key +"-EJE", "").equals("")) return true;
            if (!getString(doc, key +"-SAL", "").equals("")) return true;
        }
        return false;
    }
    
    
    /*
    public static void main(String []args) throws Exception {
        HReporteMABE h = new HReporteMABE();
        Usuario u = new Usuario();
        u.setLogin("FVILLACOB");
        u.setNombre("Mario Fontalvo");
        h.start("000031", "2006", "10", "AMB", u, new com.tsp.finanzas.presupuesto.model.Model(), new com.tsp.operation.model.Model());
    }
    */
    
    
    
    
}
