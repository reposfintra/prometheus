/*************************************************************
 * Nombre: ReporteMigracionCumplidos.java
 * Descripci�n: Hilo para crear el reporte de migraci�n Cumplidos.
 * Autor: Ing. Jose de la rosa
 * Fecha: 24 de octubre de 2005, 11:39 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author  Jose
 */
public class ReporteMigracionCumplidos extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String fecha = "";
    public ReporteMigracionCumplidos () {
    }
    
    public void start (String id, String fecha){
        this.id = id;
        this.fecha = fecha;
        this.procesoName = "Cumplido Migrado";
        this.des = "Migraci�n de cumplidos por fecha";
        super.start ();
    }
    
    public synchronized void run (){
        try{
            Model model = new Model ();
            model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, this.id);
            Vector lista = (Vector) model.cumplidoService.listarCumplidosMigrados (fecha);
            Date hoy = new Date ();
            SimpleDateFormat s = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
            String fecha_hoy = s.format (hoy);
            String a�o = fecha_hoy.substring(0,4);
            String mes = fecha_hoy.substring(5,7);
            String dia = fecha_hoy.substring(8,10);
            if (lista!=null && lista.size ()>0){
                ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
                String path = rb.getString ("ruta");
                File file = new File (path + "/exportar/migracion/" + id + "/");
                file.mkdirs ();
                String NombreArchivo = path + "/exportar/migracion/" + id + "/MSO150o3 "+a�o+"-"+mes+dia+".csv";
                PrintStream archivo = new PrintStream (NombreArchivo);
                Iterator it = lista.iterator ();
                while(it.hasNext ()){
                    Cumplido cum = (Cumplido) it.next ();
                    archivo.println (cum.getPlanilla ()+","+cum.getTipo_doc ()+","+cum.getCrea_user ()+","+ cum.getCantidad () +",MAIN");
                    model.cumplidoService.updateCumplido ( fecha_hoy , id, cum.getTipo_doc (), cum.getPlanilla () );
                }
            }
            model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                Model model = new Model ();
                model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (),this.id,"ERROR :" + e.getMessage ());
            }
            catch(Exception f){
                try{
                    Model model = new Model ();
                    model.LogProcesosSvc.finallyProceso (this.procesoName,this.hashCode (),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
