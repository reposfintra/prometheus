package com.tsp.operation.model;

import com.tsp.operation.model.beans.Serie;
import java.sql.*;
import com.tsp.util.connectionpool.PoolManager;
import java.util.Calendar;
import java.util.GregorianCalendar;
import com.tsp.util.Util;


public class SerieDAO {

    private static final String SQL_OBTENER_SERIE_POR_DOCUMENT_TYPE = "select * from series where document_type = ?";
    private static final String SQL_REINICIAR_SERIE = "update series set last_number = '01', last_update = now() where document_type = ?";
    private static final String SQL_ACTUALIZAR_SERIE = "update series set last_number = ?, last_update = now() where document_type = ?";
    private String nombreConexion;
    
    public SerieDAO() {
    }
    public SerieDAO(String dataBaseName) {
        this.nombreConexion = dataBaseName;
    }

    public Serie obtenerSerie(String document_type) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            Serie s = null;
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.nombreConexion );
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement(SQL_OBTENER_SERIE_POR_DOCUMENT_TYPE);
                ps.setString(1,document_type);
                rs = ps.executeQuery();
                if (rs.next()){                    
                    Calendar f = Util.crearCalendar(rs.getString("last_update"));
                    Calendar h = new GregorianCalendar();
			  if ( f.get(f.YEAR) < h.get(h.YEAR) || (f.get(f.YEAR) == h.get(h.YEAR) && f.get(f.MONTH) < h.get(h.MONTH)) ){
                    //if ( f.get(f.MONTH) != h.get(h.MONTH) || f.get(f.DATE) != h.get(h.DATE) ){
                        PreparedStatement ps2 = con.prepareStatement(SQL_REINICIAR_SERIE);
                        ps2.setString(1,document_type);
                        ps2.execute();
                    }
                }
                rs = ps.executeQuery();
                if (rs.next()){
                    s = new Serie();
                    s.setDocument_type(rs.getString("document_type"));
                    s.setLast_number(rs.getString("last_number"));
                    s.setLast_update(rs.getString("last_update"));
                    s.setPrefix(rs.getString("prefix"));
                }
                //********************************** esto es si no pueden haber dos Nro de corrida iguales en un mismo dia
                /*if ( Calendar.getInstance().get(Calendar.DATE) == 1 && Util.sonElMismoDia(Util.crearCalendar(s.getLast_update()),new GregorianCalendar()) ){
                    ps = con.prepareStatement(SQL_REINICIAR_SERIE);
                    ps.execute();
                }*/
                return s;
            }
        
        }catch(SQLException e){
            throw new SQLException("ERROR OBTENIENDO SERIE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return null;
    }

    public void actualizarSerie(Serie s) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.nombreConexion );
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement(SQL_ACTUALIZAR_SERIE);
                ps.setString(1,s.getLast_number());
                ps.setString(2,s.getDocument_type());
                ps.execute();
            }
         }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA SERIE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
}
