package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;

public class Proveedor_AnticipoService{
    
    private Proveedor_AnticipoDAO proveedor;
    
    Proveedor_AnticipoService(){
        
        proveedor = new Proveedor_AnticipoDAO();
    }
    
    public boolean existProveedor(String nit, String sucursal )throws SQLException{
        
        return proveedor.existProveedor(nit, sucursal);
        
    }
    
    public List getProveedoresAnticipo(String dstrct )throws SQLException{
        
        List proveedores=null;
        
        proveedor.searchProveedorAnticipo(dstrct);
        proveedores = proveedor.getProveedorAnticipo();
        
        return proveedores;
        
    }
    
    public boolean existProveedoresAnticipo(String dstrct )throws SQLException{
        
        return proveedor.existProveedorAnticipo(dstrct);
        
    }
    
    public void insertProveedor(Proveedor_Anticipo pa, String base)throws SQLException{
        try{
            proveedor.setProveedor(pa);
            proveedor.insertProveedor(base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
     public void anularProveedor(Proveedor_Anticipo pa)throws SQLException{
        try{
            proveedor.setProveedor(pa);
            proveedor.anularProveedor();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public Proveedor_Anticipo getProveedor( )throws SQLException{
        
        return proveedor.getProveedor();
        
    }
    public void buscaProveedor(String nit,String sucursal)throws SQLException{
        try{
            proveedor.searchProveedor(nit,sucursal);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void updateProveedor(Proveedor_Anticipo pa)throws SQLException{
        try{
            proveedor.setProveedor(pa);
            proveedor.updateProveedor();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void consultaProveedor(String ciudad, String nombre, String nit)throws SQLException{
        try{
            proveedor.consultar(ciudad, nombre, nit);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    public Vector getProveedores(){
        return proveedor.getProveedores();
    }
    
    /**Diogenes */
    public boolean existProveedorAnulado(String nit, String  sucursal)throws SQLException{
            try{
                    return proveedor.existProveedorAnulado(nit, sucursal);
            }
            catch(SQLException e){
                throw new SQLException(e.getMessage());
            }
    }
    public void activarProveedor(Proveedor_Anticipo pa)throws SQLException{
        try{
            proveedor.setProveedor(pa);
            proveedor.activarProveedor();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
}





