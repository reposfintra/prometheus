/*
 * RegistroTiempoDAO.java
 *
 * Created on 01 de Diciembre de 2004, 2:52
 */

package com.tsp.operation.model;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Mario fontalvo
 */
public class RegistroTiempoDAO {
    private static final String SQL_BUSCAR_PLANILLA = "" +
    "            SELECT P.NUMPLA, P.FECPLA, P.ORIPLA, P.DESPLA, P.PLAVEH, P.CEDCON, P.AGCPLA  " +
    "             FROM PLANILLA P," +
    "		  REMESA R," +
    "		  PLAREM PR" +
    "             WHERE R.REMISION = ?" +
    "	     AND   R.NUMREM = PR.NUMREM   " +
    "             AND   PR.NUMPLA = P.NUMPLA" +
    "            and P.reg_status='' ;";
  
    private static final String SQL_BUSCAR_CIUDAD   = 
            " SELECT NOMCIU FROM CIUDAD WHERE CODCIU = ?            ";
    
    private static final String SQL_BUSCAR_DELAY    =
            " SELECT A.DELAY_CODE, A.DESCRIPTION                                 "+
            " FROM TBLDELAY AS A, SJDELAY AS B                                   "+
            " WHERE A.DELAY_CODE = B.DELAY_CODE AND  A.DSTRCT = B.DSTRCT         "+
            " AND B.SJ IN ( SELECT C.STD_JOB_NO FROM remesa AS C, plarem AS D  "+
            "               WHERE C.NUMREM = D.NUMREM AND D.NUMPLA = ?  )        "+
            " ORDER  BY A.DELAY_CODE                                             ";
    
    public static final String SQL_BUSCAR_TBLTIEMPO = 
            " SELECT A.DSTRCT, A.SJ, A.CF_CODE, A.TIME_CODE, A.TIME_DESCRIPTION, A.SECUENCE, A.NMONIC_CODE "+
            " FROM TBLTIEMPO AS A                                                                          "+
            " WHERE A.SJ IN ( SELECT B.STD_JOB_NO FROM remesa AS B, plarem AS C                          "+
            "                 WHERE B.NUMREM = C.NUMREM AND C.NUMPLA = ?  )                                "+
            " ORDER BY A.CF_CODE, A.SECUENCE                                                               "; 
    
    public static final String SQL_BUSCAR_TIEMPOPLANILLA = 
            " SELECT A.PLA, A.DATE_TIME_TRAFFIC, A.DELAY_CODE, A.OBSERVATION, A.TIME_DELAY  "+
            " FROM PLANILLA_TIEMPO AS A, TBLTIEMPO AS B                                     "+
            " WHERE A.DSTRCT = B.DSTRCT       AND A.SJ=B.SJ   AND A.CF_CODE= B.CF_CODE      "+
            " AND A.TIME_CODE = B.TIME_CODE AND A.SECUENCE = B.SECUENCE                     "+
            " AND B.DSTRCT = ?    AND B.TIME_CODE = ?       "+
            " AND B.SECUENCE = ?  AND A.PLA = ? And A.reg_status=''  ";
            
    
    public static final String SQL_INSERTAR  =  "INSERT INTO PLANILLA_TIEMPO VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    public static final String SQL_MODIFICAR =  
        " UPDATE PLANILLA_TIEMPO "+
        " SET DATE_TIME_TRAFFIC = ? , DELAY_CODE = ? , OBSERVATION = ? , TIME_DELAY = ?, LAST_UPDATE = ? , USER_UPDATE = ? "+
        " WHERE DSTRCT = ? AND SJ = ? AND CF_CODE = ? AND TIME_CODE = ? AND SECUENCE = ? AND PLA = ?                       ";
   
    
    /**********************************************************************************************/
    // CONSULTAS PARA EL REPORTE A EXCEL
    private static final String SQL_BUSCAR_GRUPO_PLANILLAS = 
            " SELECT NUMPLA, FECPLA, ORIPLA, DESPLA, PLAVEH, CEDCON, AGCPLA "+ 
            " FROM PLANILLA                                                "+
            " WHERE  FECPLA BETWEEN ? AND ?   ORDER BY FECPLA, NUMPLA       ";
    
    private static final String SQL_BUSCAR_OPERACIONES =
            " SELECT TIME_CODE_1, TIME_CODE_2              "+
            " FROM TIEMPO                                  "+
            " WHERE DSTRCT = ? AND SJ = ? AND CF_CODE = ?  "+
            " ORDER BY SECUENCE                            ";
    
   
    /** Creates a new instance of RegistroTiempoDAO */
    public RegistroTiempoDAO() {
    }
    
    
    String BuscarNombreCiudad(String Codigo) throws Exception {
        PreparedStatement st  = null;
        ResultSet rs          = null;
        String    Nombre      = new String ("");
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_CIUDAD);
            st.setString(1 ,  Codigo.toUpperCase());
            rs = st.executeQuery();
            while(rs.next())
                Nombre =  rs.getString(1);
        }
        catch(Exception e) {
            throw new Exception("Error en rutina Buscar Ciudad [RegistroTiempoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();            
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Nombre;    	
    }       
    
    // BUSCA LOS DATOS DE UNA PLANILLA ESPECIFICA
    DatosPlanilla BuscarDatosPlanilla(String CodigoPlanilla)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        DatosPlanilla     Registro = null;

        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_PLANILLA);
            st.setString(1 ,  CodigoPlanilla.toUpperCase());
            rs = st.executeQuery();
            while(rs.next())
            {
                DatosPlanilla datos = new DatosPlanilla();
                Registro = datos.load(rs);                
                Registro.setDescripcionOrigenPlanilla (BuscarNombreCiudad(Registro.getCodigoOrigenPlanilla()));
                Registro.setDescripcionDestinoPlanilla(BuscarNombreCiudad(Registro.getCodigoDestinoPlanilla()));
                Registro.setDescripcionAgenciaPlanilla(BuscarNombreCiudad(Registro.getCodigoAgenciaPlanilla()));
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarDatosPlanilla [RegistroTiempoDAO]... \n"+e.getMessage()); 
        }
        finally {
            if(st!=null)  st.close();            
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Registro;    	        
    }
    
    // BUSCA TODOS LOS CODIGO DELAY ASOCIADA A LA PLANILLA ATRAVES DEL SJ
    List BuscarDelay(String CodigoPlanilla)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();

        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_DELAY);
            st.setString(1, CodigoPlanilla);
            rs = st.executeQuery();
            while(rs.next()){
                DatosDelay datos = new DatosDelay();
                Lista.add(datos.load(rs));
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarDelay [RegistroTiempoDAO]... \n"+e.getMessage()); 
        }
        finally {
            if(st!=null)  st.close();            
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Lista;    	                
    }
    // PRIMERO OBTENEMOS TODOS LOS REGISTRO DE TBLTIEMPO PARA LA PLANILLA
    List BuscarRegistrosTBLTiempo(String CodigoPlanilla)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();

        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_TBLTIEMPO);
            st.setString(1, CodigoPlanilla);
            rs = st.executeQuery();
            while(rs.next()){
                PlanillaTiempo datos = new PlanillaTiempo();
                datos.load_TBLTiempo(rs);
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarRegistrosTBLTiempo [RegistroTiempoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Lista;
    }
    // COMO SEGUNDO PASO BUSCAMOS EN PLANILLA_TIEMPO TODOS REGISTRO DE LA PLANILLA
    // ASOCIADA A LOS DE TBLTIEMPO
    List BuscarTiempoPlanilla(List TBLTiempo, String Planilla)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();

        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            
            Iterator it = TBLTiempo.iterator();
            while(it.hasNext()){
                PlanillaTiempo datos = (PlanillaTiempo) it.next();
                datos.setPlanilla(Planilla);
                st = conPostgres.prepareStatement(SQL_BUSCAR_TIEMPOPLANILLA);
                st.setString(1,datos.getDstrct());
                st.setString(2,datos.getTimeCode());
                st.setString(3,datos.getSequence());
                st.setString(4,Planilla);       
                ////System.out.println("Tiempos de la planilla "+st);
                rs = st.executeQuery();
                while(rs.next()){
                    datos.load_PlanillaTiempo(rs);                    
                }   
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarTiempoPlanilla [RegistroTiempoDAO]... \n"+e.getMessage()); 
        }
        finally {
            if(st!=null)  st.close();            
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Lista;    	                
    }   
    
    void Insertar(String Distrito, String Planilla, String SJ, String CF_Code, String Time_Code, String Secuence, String DTT, String Delay_Code, String Observation, String TimeDelay, String Usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {            
            st = conPostgres.prepareStatement(SQL_INSERTAR);
            st.setString(1 ,  "");
            st.setString(2 ,  Distrito);
            st.setString(3 ,  Planilla);
            st.setString(4 ,  SJ);
            st.setString(5 ,  CF_Code);
            st.setString(6 ,  Time_Code);
            st.setString(7 ,  Secuence);
            st.setString(8 ,  DTT); 
            st.setString(9 ,  Delay_Code);
            st.setString(10 , Observation.toUpperCase());
            st.setString(11 , Util.getFechaActual_String(6));
            st.setString(12,  Usuario);
            st.setString(13 , Util.getFechaActual_String(6));
            st.setString(14,  Usuario); 
            st.setString(15,  TimeDelay);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Insertar [RegistroTiempoDao]... \n"+e.getMessage());
        }
        finally{
            if(st!=null)  st.close();                        
            poolManager.freeConnection("fintra",conPostgres);
        }        
    }
        
    void Modificar(String Distrito, String Planilla, String SJ, String CF_Code, String Time_Code, String Sequence, String DTT, String Delay_Code, String Observation, String TimeDelay, String Usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_MODIFICAR);
            st.setString(1 ,  DTT);
            st.setString(2 ,  Delay_Code);
            st.setString(3 ,  Observation);
            st.setString(4 ,  TimeDelay);
            st.setString(5 ,  Util.getFechaActual_String(6));
            st.setString(6 ,  Usuario);
            st.setString(7 ,  Distrito);
            st.setString(8 ,  SJ);
            st.setString(9 ,  CF_Code);
            st.setString(10,  Time_Code);
            st.setString(11,  Sequence);
            st.setString(12,  Planilla);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Modificar [RegistroTiempoDao]... \n"+e.getMessage());
        }
        finally{
            if(st!=null)  st.close();                        
            poolManager.freeConnection("fintra",conPostgres);
        }        
    }
    
    
   /******************************************************************************************************/
   /*   Procedimiento para el reporte  (Todas las planillas sus registros de tiempos  ******************/
   //   y las operacioens entre cada time_code 
    
    List BuscarOperaciones(List ListaTiempoPlanillas)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();

        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            String Distrito = "";
            String Sj       = ""; 
            String CF_Code  = "";            
            Iterator it = ListaTiempoPlanillas.iterator();
            while(it.hasNext()){
                PlanillaTiempo Pla = (PlanillaTiempo)it.next();
                Distrito = Pla.getDstrct();
                Sj       = Pla.getSJ();
                CF_Code  = Pla.getCFCode();
                break;
            }
            
            st = conPostgres.prepareStatement(SQL_BUSCAR_OPERACIONES);
            st.setString(1, Distrito);
            st.setString(2, Sj);
            st.setString(3, CF_Code);            
            rs = st.executeQuery();
            while(rs.next()){
                OperacionTiempo datos = OperacionTiempo.load(rs);
                datos.setDatos(ListaTiempoPlanillas);
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarOperaciones [RegistroTiempoDAO]... \n"+e.getMessage()); 
        }
        finally {
            if(st!=null)  st.close();            
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Lista;    	                
    }
    
    List BuscarPlanillas(String FechaInicial, String FechaFinal)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();  

        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_GRUPO_PLANILLAS);
            st.setString(1 ,  FechaInicial);
            st.setString(2 ,  FechaFinal);
            rs = st.executeQuery();
            while(rs.next())
            {
                DatosPlanilla Registro =  DatosPlanilla.load(rs);
                  Registro.setDescripcionOrigenPlanilla (BuscarNombreCiudad(Registro.getCodigoOrigenPlanilla()));
                  Registro.setDescripcionDestinoPlanilla(BuscarNombreCiudad(Registro.getCodigoDestinoPlanilla()));
                  Registro.setDescripcionAgenciaPlanilla(BuscarNombreCiudad(Registro.getCodigoAgenciaPlanilla()));                
                List    TBLTiempos  = BuscarRegistrosTBLTiempo(Registro.getNumeroPlanilla());
                List    Tiempos     = BuscarTiempoPlanilla(TBLTiempos,Registro.getNumeroPlanilla());
                List    Operaciones = BuscarOperaciones(Tiempos);
                
                Planillas Plan = new Planillas();
                Plan.setDatos(Registro);
                Plan.setTiempos(Tiempos);
                Plan.setOperaciones(Operaciones);
                Lista.add(Plan);                 
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarPlanillas [RegistroTiempoDAO]... \n"+e.getMessage()); 
        }
        finally {
            if(st!=null)  st.close();            
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Lista;    	        
    }
    
    
    
}
