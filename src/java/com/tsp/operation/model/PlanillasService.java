
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;



public class PlanillasService {
  
   private PlanillasDAO  PlanillaDataAccess;
   private List          listadoOC;
   private Planillas     planilla;
    
    
 // Inicializamos las variables 
    public PlanillasService() {
       PlanillaDataAccess = new PlanillasDAO();
       listadoOC = null;
       planilla  = null;
    }
    
    
    
//--- Buscamos los datos de la oc
  public String searchOC(String distrito,String oc,String base, String numrem)throws Exception{
     String comentario="";
     try{  
       planilla= PlanillaDataAccess.queryConsultaOC(distrito,oc,base,numrem);
     }catch(Exception e){
        throw new Exception( e.getMessage() );
     }
     if(planilla==null) comentario="No hay planilla correspondiente al n�mero ["+ oc +"] para el distrito [" +distrito +"], verifique el n�mero ingresado....";
     return comentario;
  }
    
  
  
 
//--- Buscamos el listado de oc pertenecientes al rango
  public String searchListOC(int tipo,String distrito,String fecha1, String fecha2) throws SQLException{
     String comentario="";
     try{  
         this.listadoOC = PlanillaDataAccess.queryConsultaListadoOC(tipo,distrito,fecha1,fecha2);
     }catch(Exception e){
        throw new SQLException( e.getMessage() );
     }
     if(listadoOC==null) comentario="No hay planillas registradas entre ["+ fecha1 +" - "+ fecha2 +"]  para el distrito [" +distrito +"]";
     return comentario;
  }
  
  
  
  
//--- Devolvemos el Objeto planilla
    public Planillas getPlanilla(){
        return this.planilla;
    }
    
    
//--- Devolvemos la lista
    public List getList(){
        return this.listadoOC;
    }
    /**
     * Metodo consultarOC, busca la informacion correspondiente a una planilla,
     * @param: distrito, oc, base
     * @see:   consultarOC - planillasDAO
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    
    public void consultarOC(String distrito,String oc,String base)throws Exception{
        try{
            planilla= PlanillaDataAccess.consultarOC(distrito,oc,base);
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
    }
    
 
    
}// FIN SERVICE
