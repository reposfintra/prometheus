/*
 * PeajeDAO.java
 *
 * Created on 3 de diciembre de 2004, 05:57 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  KREALES
 */
public class AutorizacionFleteDAO {
    
    private AutorizacionFlete aflete;
    
    
    /** Creates a new instance of PeajeDAO */
    public AutorizacionFleteDAO() {
    }
    /**
     * Getter for property aflete.
     * @return Value of property aflete.
     */
    public com.tsp.operation.model.beans.AutorizacionFlete getAflete() {
        return aflete;
    }
    
    /**
     * Setter for property aflete.
     * @param aflete New value of property aflete.
     */
    public void setAflete(com.tsp.operation.model.beans.AutorizacionFlete aflete) {
        this.aflete = aflete;
    }
    
    
    public String insert()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        String sql ="";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into cambios_fletes(dstrct,agency_id,planilla,placa,pla_owner,cedcond,fecha_req,standard,nuevoflete,unidad,justificacion,despachador,autorizador,email_autorizador,creation_user,base,viejoflete,email_desp,solicitud)VALUES (?,?,?,?,?,?,'now()',?,?,?,?,?,?,?,?,?,?,?,?)");
                st.setString(1,aflete.getDstrct());
                st.setString(2,aflete.getAgency_id());
                st.setString(3,aflete.getPlanilla());
                st.setString(4,aflete.getPlaca());
                st.setString(5,aflete.getPla_owner());
                st.setString(6,aflete.getCedcond());
                st.setString(7,aflete.getStandard());
                st.setFloat(8,aflete.getNuevoflete());
                st.setString(9,aflete.getUnidad());
                st.setString(10,aflete.getJustificacion());
                st.setString(11,aflete.getDespachador());
                st.setString(12,aflete.getAutorizador());
                st.setString(13,aflete.getEmail_autorizador());
                st.setString(14,aflete.getCreation_user());
                st.setString(15,aflete.getBase());
                st.setFloat(16,aflete.getViejoflete());
                st.setString(17, aflete.getEmailDesp());
                st.setString(18, aflete.getNumsol());
                sql = st.toString();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LA AUTORIZACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
        
    }
    
    public String insertSendMail()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        String sql ="";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into sendmail(emailfrom,emailto,emailsubject,emailbody,sendername,emailcopyto)VALUES (?,?,?,?,?,'')");
                st.setString(1,aflete.getMyEmail());
                st.setString(2,aflete.getEmail_autorizador());
                st.setString(3,aflete.getSubject());
                st.setString(4,aflete.getBody());
                st.setString(5,aflete.getMyName());
                
                sql = st.toString();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LA AUTORIZACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
        
    }
    
    public void selecionar(String planilla)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        String sql ="";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  a.nombre as agencia," +
                "	c.planilla," +
                "	c.placa," +
                "	n.nombre as nomprop, " +
                "	c.cedcond," +
                "	To_char(c.fecha_req,'YYYY-MM-DD')as fecha_req," +
                "	c.standard," +
                "	c.viejoflete," +
                "	c.nuevoflete," +
                "	c.unidad," +
                "	c.justificacion," +
                "	c.despachador," +
                "       c.email_desp," +
                "       c.solicitud," +
                "       c.estado_solicitud" +
                " from 	cambios_fletes c," +
                "	agencia a," +
                "	nit n" +
                " where 	planilla=? " +
                "	and a.id_agencia = c.agency_id" +
                "	and n.cedula = c.pla_owner");
                st.setString(1, planilla);
                rs = st.executeQuery();
                aflete=null;
                if(rs.next()){
                    aflete = new AutorizacionFlete();
                    aflete.setAgency_id(rs.getString("agencia"));
                    aflete.setCedcond(rs.getString("cedcond"));
                    aflete.setDespachador(rs.getString("despachador"));
                    aflete.setFecha_req(rs.getString("fecha_req"));
                    aflete.setJustificacion(rs.getString("justificacion"));
                    aflete.setNuevoflete(rs.getFloat("nuevoflete"));
                    aflete.setViejoflete(rs.getFloat("viejoflete"));
                    aflete.setPla_owner(rs.getString("nomprop"));
                    aflete.setPlaca(rs.getString("placa"));
                    aflete.setPlanilla(rs.getString("planilla"));
                    aflete.setStandard(rs.getString("standard"));
                    aflete.setUnidad(rs.getString("unidad"));
                    aflete.setEmailDesp(rs.getString("email_desp"));
                    aflete.setNumsol(rs.getString("solicitud"));
                    if(rs.getString("estado_solicitud").equals("C"))
                        aflete.setAutorizada(true);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LA AUTORIZACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        // return sql;
        
    }
    
    public String getSolicitud()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        String serie ="NO HAY SERIE";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select prefix||last_number as serie from series where document_type = 'SAUT' and last_number<=serial_fished_no");
                rs = st.executeQuery();
                if(rs.next()){
                    serie = rs.getString("serie");
                }
                st = con.prepareStatement("UPDATE SERIES SET last_number = last_number+1 WHERE document_type = 'SAUT' and last_number<=serial_fished_no");
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR CUANDO SE OB " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return serie;
        
    }
    
    public String getMaxPlanilla()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        String cod = "VAC1";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select max(substr(planilla,4))as max, count(planilla) as cant from cambios_fletes where planilla like 'VAC%'");
                rs = st.executeQuery();
                if(rs.next()){
                    if(rs.getInt("cant")>0){
                        int max = rs.getInt("max")+1;
                        cod = "VAC"+max;
                    }
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LA AUTORIZACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return cod;
        
    }
    
    public void cumplirSolicitud(String solicitud)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        String sql ="";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update cambios_fletes set estado_solicitud = 'C'  where solicitud=?");
                st.setString(1,solicitud);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL CUMPLIMIENTO DE LA SOLICITUD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    public boolean estaCumplida(String solicitud)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select estado_solicitud from  cambios_fletes where solicitud=? and estado_solicitud='C'");
                st.setString(1,solicitud);
                rs = st.executeQuery();
                if(rs.next()){
                    sw  = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL CUMPLIMIENTO DE LA SOLICITUD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    public void insertControlActualizacion(String comando,String usuario)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        String sql ="";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into control_actualizaciones (comando,creation_user) values (?,?)");
                st.setString(1,comando);
                st.setString(2,usuario);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR INSERTANDO CONTROL DE ACTUALIZACIONES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
}
