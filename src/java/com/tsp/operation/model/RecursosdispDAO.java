//*************************************** clase modificada
/*
 * RecusosdispDAO.java
 *
 * Created on 16 de junio de 2005, 11:24 PM
 */
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.Wgroup_PlacaDAO;

/**
 *
 * @author  Jm
 */
public class RecursosdispDAO extends MainDAO{
    Recursosdisp rd;
    Vector recursosd;
    Vector asignados;
    
    private static String update = " UPDATE recursos_disp                                               "+
    " SET dstrct = ?, numpla = ?, origen = ?, destino = ?,               "+
    " placa = ?, recurso = ?, tiporecurso = ?,                           "+
    " fecha_disp = ?, tiempo = ?, cod_cliente = ? , creation_user = ?,   "+
    " std_job_no_rec = ? , no_viajes = ?, work_group = ?,                "+
    " agencia = ?                                                        "+
    " WHERE dstrct = ? AND  fecha_disp = ?  AND PLACA = ?                ";
    
    private static String updateRA=" UPDATE recursos_disp                                               "+
    " SET dstrct = ?, numpla = ?, origen = ?, destino = ?,               "+
    " placa = ?, recurso = ?, tiporecurso = ?,                           "+
    " fecha_disp = ?, tiempo = ?, cod_cliente = ? , equipo_aso = ?,      "+
    " tipo_aso = ?, creation_user = ?,                                   "+
    " std_job_no_rec = ?, no_viajes = ? , work_group = ? , agencia = ?   "+
    " WHERE dstrct = ? AND  fecha_disp = ?  AND PLACA = ?                 ";
    
    
    private static String SQL_MARCA_FRONTERA = " UPDATE recursos_disp "+
    " SET numpla_req=?, frontera='S', disponibilidad_frontera='N'     "+
    " WHERE dstrct = ? AND  fecha_disp = ?  AND PLACA = ?                ";
    
    private static String SEARCHTRAILER     = " SELECT * FROM RECURSOS_DISP WHERE PLACA = ? AND FRONTERA  <>   'S'      ";
    
    
    
    /*private static String update = " UPDATE recursos_disp                                               "+
    " SET dstrct = ?, numpla = ?, origen = ?, destino = ?,               "+
    " placa = ?, recurso = ?, tiporecurso = ?,                           "+
    " fecha_disp = ?, tiempo = ?, cod_cliente = ? , creation_user = ?,   "+
    " std_job_no_rec = ? , no_viajes = ?                                 "+
    " WHERE dstrct = ? AND  fecha_disp = ?  AND PLACA = ?                ";
     
    private static String updateRA=" UPDATE recursos_disp                "+
    " SET dstrct = ?, numpla = ?, origen = ?, destino = ?,               "+
    " placa = ?, recurso = ?, tiporecurso = ?,                           "+
    " fecha_disp = ?, tiempo = ?, cod_cliente = ? , equipo_aso = ?,      "+
    " tipo_aso = ?, creation_user = ?,                                    " +
    " std_job_no_rec = ? , no_viajes = ?                                  "+
    " WHERE dstrct = ? AND  fecha_disp = ?  AND PLACA = ?                 ";*/
    
    private static String SEARCH = " SELECT * FROM RECURSOS_DISP WHERE PLACA = ?                        ";
    
    /*Jescandon 11-01-06*/
    
    private static String List_Viajes =     "   SELECT COUNT(plaveh) AS viajes, plaveh as placa                         "+
    "   FROM planilla                                                           "+
    "   WHERE fecdsp BETWEEN now() - CAST('30 DAYS' AS INTERVAL ) AND now()     "+
    "   GROUP BY plaveh                                                         "+
    "   ORDER BY plaveh                                                         ";
    
    private static String Insert_record =   "   INSERT INTO record_placa ( dstrct, no_viajes , placa, creation_user )    "+
    "   VALUES ( ?, ?, ?, ?  )                                                  ";
    
    private static String Update_record =   "   UPDATE record_placa SET no_viajes = ?, user_update = ?, last_update = now() "+
    "   WHERE placa = ?                                                             ";
    
    private static String Search_record =   "   SELECT placa, no_viajes FROM record_placa WHERE placa = ?               ";
    
    /*Jescandon 13-01-06*/
    private static String Search_wgroup =   "    SELECT std_job_no, placa FROM                                              "+
    "    wgroup_placa wp                                                            "+
    "    LEFT OUTER JOIN wgroup_standart ws on ( ws.work_group = wp.work_group )    "+
    "    WHERE wp.placa = ?                                                         ";
    
    /** Creates a new instance of RecusosdispDAO */
    public RecursosdispDAO() {
        super("RecursosDispDAO.xml");
    }
    
    /**
     * Getter for property rd.
     * @return Value of property rd.
     */
    public Recursosdisp getRd() {
        return rd;
    }
    
    /**
     * Setter for property rd.
     * @param rd New value of property rd.
     */
    public void setRd(Recursosdisp rd) {
        this.rd = rd;
    }
    
    /**
     * Getter for property recursosd.
     * @return Value of property recursosd.
     */
    public java.util.Vector getRecursosd() {
        return recursosd;
    }
    
    /**
     * Setter for property recursosd.
     * @param recursosd New value of property recursosd.
     */
    public void setRecursosd(java.util.Vector recursosd) {
        this.recursosd = recursosd;
    }
    
    public void list( String distrito )throws SQLException{
        String consulta = "select * from recursos_disp where dstrct = ?";
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, distrito);
                rs = st.executeQuery();
                recursosd = new Vector();
                while(rs.next()){
                    rd = new Recursosdisp();
                    rd.setDistrito(rs.getString("dstrct"));
                    rd.setReg_status(rs.getString("estado"));
                    rd.setNumpla(rs.getString("numpla"));
                    rd.setOrigen(rs.getString("origen"));
                    rd.setDestino(rs.getString("destino"));
                    rd.setPlaca(rs.getString("placa"));
                    rd.setClase(rs.getString("recurso"));
                    rd.setTipo(rs.getString("tiporecurso"));
                    rd.setFecha_disp(rs.getTimestamp("fecha_disp"));
                    rd.setFecha_asig(rs.getTimestamp("fecha_asig"));
                    rd.setTiempo(rs.getFloat("tiempo"));
                    rd.setTipo_asig(rs.getString("tipo_asig"));
                    rd.setCod_cliente(rs.getString("cod_cliente"));
                    rd.setEquipo_aso(rs.getString("equipo_aso"));
                    rd.setTipo_aso(rs.getString("tipo_aso"));
                    rd.setFecha_creacion(rs.getTimestamp("fecha_creacion"));
                    rd.setFecha_actualizacion(rs.getTimestamp("fecha_actualizacion"));
                    rd.setUsuario_creacion(rs.getString("usuario_creacion"));
                    rd.setUsuario_actualizacion(rs.getString("usuario_actualizacion"));
                    
                    recursosd.add(rd);
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
    
    
    
    
    
    public void DeleteAll()throws SQLException{
        String consulta = "DELETE FROM recursos_disp";
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DELETEALL ->" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void DeleteAct( String d , String fi, String ff  )throws SQLException{
        String consulta = "DELETE FROM recursos_disp WHERE dstrct  = ? AND creation_date BETWEEN ? AND ?";
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, d);
                st.setString(2, fi);
                st.setString(3, ff);
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DELETEACT ->" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void DeleteRD( String placa )throws SQLException{
        String consulta = "DELETE FROM recursos_disp WHERE placa = ?";
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, placa);
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DELETERD ->" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /////alpy
    /********************************** Miembros de clase nuevos ******************************/
    
    /*
     */
    
    
    private static final String consultaTodos = "select * from recursos_disp";
    private static final String consultaTieneRutaFavorita = "select tiporecurso from recursos_disp,ruta_fav where recursos_disp.placa = ruta_fav.placa and ruta_fav.placa = ?";
    private static final String consultaPuedeViajarEnRutaFavorita = "select tiporecurso from recursos_disp,ruta_fav where recursos_disp.placa = ruta_fav.placa and recursos_disp.placa = ? and ruta_fav.origen = ? and ruta_fav.destino = ?";
    private static final String consultaTieneClienteFavorito = "select tiporecurso from recursos_disp,cliente_fav where recursos_disp.placa = cliente_fav.placa and cliente_fav.placa = ?";
    private static final String consultaPuedeTransportarCliente = "select tiporecurso from recursos_disp,cliente_fav where recursos_disp.placa = cliente_fav.placa and recursos_disp.placa = ? and cliente_fav.codcliente = ?";
    private static final String consultaAsignarRecurso = "update recursos_disp set reg_status = ?,tipo_asig = ?, fecha_asig = ?, last_update = now(), dstrct_code_req = ?, num_sec_req = ?, std_job_no_req = ?, numpla_req = ?, fecha_dispo_req = ?, cod_cliente = ? where recurso = ? and dstrct = ? and placa = ? and fecha_disp = ?";//modif-distrito
    private static final String consultaAsignarReq = "update req_cliente set estado = 'A', fecha_asing = ?, fecha_posibleentrega = ?, fecha_actualizacion = 'now()' where dstrct_code = ? and num_sec = ? and std_job_no = ? and fecha_dispo = ? and numpla = ?";
    private static final String consultaObtenerDuracionVia =
    "select duracion from via where origen = ? and destino = ?";
    private static final String sqlCrearNuevoRecursoDisponible =
    "insert into recursos_disp" +
    " (dstrct,placa,fecha_disp,reg_status,numpla,origen,destino,tiporecurso,recurso,tiempo," +
    "equipo_aso,tipo_aso,creation_user,user_update,std_job_no_rec)" +
    " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";//modif -distrito-tipo_recurso
    
    public Vector obtenerRecursosNoAsignados( ReqCliente req, int KI, int KF ) throws SQLException {
        return listarPorWhere( true,
        "select  " +
        " position(? in recursos_disp.work_group)as post," +
        " recursos_disp.* , ciudad.agasoc," +
        " CASE WHEN position(? in recursos_disp.work_group)>0 then true else false end," +
        " CASE WHEN d.agasoc = ? THEN true else false end AS retorno," +
        " D.NOMCIU," +
        " case when inter.dato is not null then true else false end as inter" +
        " from 	recursos_disp" +
        "   LEFT OUTER JOIN (SELECT dato," +
        "                           std_job_no " +
        "				 FROM   wgroup_standart, " +
        "					tablagen " +
        "				 where  table_type='WG' " +
        "					and table_code=work_group " +
        "					and dato='IN') as inter on (inter.std_job_no=?), " +
        "	ciudad," +
        "	ciudad d" +
        "   where  	tipo_asig = ? " +
        "	  and recursos_disp.destino = ciudad.codciu " +
        "   and ciudad.agasoc = ? " +
        "	and (? between fecha_disp - CAST( '"+KI+" HOUR' AS INTERVAL)  " +
        "        and fecha_disp  + CAST( '"+KF+" HOUR' AS INTERVAL) OR (recursos_disp.equipo_propio='S'))" +
        "	and recursos_disp.origen=d.codciu" +
        "   and (position(? in recursos_disp.work_group)>0 or recursos_disp.work_group='') " +
        "   AND CASE WHEN position(? in recursos_disp.work_group)=0 then     " +
        "		case when ? = '' THEN" +
        "			CASE WHEN d.agasoc = ? " +
        "			THEN true " +
        "			else false end              " +
        "		else true end              " +
        "		else TRUE " +
        "	    end = TRUE  " +
        "	and case when ? != ''" +
        "	    then placa = ? else  " +
        "	         disponibilidad_frontera='S' " +
        "	     end  " +
        " order by post desc, retorno DESC,recursos_disp.equipo_propio DESC,no_viajes desc",
        new Object[] { req.getstd_job_no(),req.getstd_job_no(),req.isInternacional()?req.getFrontera():req.getAgaAsocDest(), req.getstd_job_no(),"", req.getAgaAsoc(),req.getfecha_dispo(),req.getstd_job_no(),req.getstd_job_no(),req.getId_rec_tra(),req.isInternacional()?req.getFrontera():req.getAgaAsocDest(),req.getId_rec_tra(),req.getId_rec_tra()}
        , new Class[] {String.class, String.class, String.class,String.class, String.class, String.class,String.class,String.class,String.class,String.class,String.class,String.class,String.class} );
    }
    
    public Recursosdisp obtenerRecurso(String distrito, String placa, String fechaDisponibilidad) throws SQLException {
        Vector vec = listarPorWhere( true, "select *, false as inter from recursos_disp inner join ciudad on (ciudad.codciu =recursos_disp.destino) where dstrct=? and placa=? and fecha_disp=? order by fecha_disp", new String[] {distrito, placa, fechaDisponibilidad}
        , new Class[] {String.class, String.class, String.class} );
        if (!vec.isEmpty()){
            return (Recursosdisp) vec.firstElement();
        }
        return null;
    }
    
    private Vector listarPorWhere( boolean whereEsConsulta, String where, Object[] parametros, Class[] tiposParametros ) throws
    SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector recursos = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra");
            if ( con != null ) {
                st = con.prepareStatement( whereEsConsulta? where:consultaTodos + " " + where );
                for ( int i = 0; i < tiposParametros.length; i++ ) {
                    if ( tiposParametros[0] == String.class ) {
                        st.setString( i + 1, parametros[i].toString() );
                    }
                    else {
                        st.setInt( i + 1,
                        ( ( Integer ) parametros[i] ).intValue() );
                    }
                }
                //System.out.println("QUERY QUE BUSCA LOS RECURSOS: "+st);
                rs = st.executeQuery();
                
                while ( rs.next() ) {
                    rd = new Recursosdisp();
                    rd.setDistrito( rs.getString( "dstrct" ) );///modif -distrito
                    rd.setPlaca( rs.getString( "placa" ) );
                    rd.setFecha_disp( rs.getTimestamp( "fecha_disp" ) );
                    rd.setaFecha_disp( rs.getString( "fecha_disp" ) );
                    rd.setReg_status( rs.getString( "reg_status" ) );
                    rd.setNumpla( rs.getString( "numpla" ) );
                    rd.setOrigen( rs.getString( "origen" ) );
                    rd.setDestino( rs.getString( "destino" ) );
                    rd.setAgaAsoc(rs.getString("agasoc"));
                    rd.setClase( rs.getString( "tiporecurso" ) );//modif -tipo_recurso-
                    rd.setTipo( rs.getString( "recurso" ) );
                    rd.setaFecha_asig( rs.getString( "fecha_asig" ) );
                    rd.setTiempo( rs.getFloat( "tiempo" ) );
                    rd.setTipo_asig( rs.getString( "tipo_asig" ) );
                    rd.setCod_cliente( rs.getString( "cod_cliente" ) );
                    rd.setEquipo_aso( rs.getString( "equipo_aso" ) );
                    rd.setTipo_aso( rs.getString( "tipo_aso" ) );
                    rd.setDstrct_code_req( rs.getString( "dstrct_code_req" ) );
                    rd.setNum_sec_req( rs.getInt( "num_sec_req" ) );
                    rd.setStd_job_no_req( rs.getString( "std_job_no_req" ) );
                    rd.setNumpla_req( rs.getString( "numpla_req" ) );
                    rd.setFecha_dispo_req( rs.getString( "fecha_dispo_req" ) );
                    rd.setFecha_creacion( rs.getTimestamp( "creation_date" ) );
                    rd.setFecha_actualizacion( rs.getTimestamp("last_update" ) );
                    rd.setUsuario_creacion( rs.getString( "creation_user" ) );
                    rd.setUsuario_actualizacion( rs.getString("user_update" ) );
                    rd.setStd_job_no_rec(rs.getString("std_job_no_rec"));
                    rd.setWork_gorup(rs.getString("work_group"));
                    rd.setNo_Viajes(rs.getInt("no_viajes"));
                    rd.setInternacional(rs.getBoolean("inter"));
                    rd.setAgencia(rs.getString("agencia"));
                    recursos.add( rd );
                }
                
            }
            
        }
        catch ( SQLException e ) {
            e.printStackTrace();
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        return recursos;
    }
    
    public boolean puedeViajarEnRuta( String placa, String origen, String destino ) throws
    SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra");
            if ( con != null ) {
                st = con.prepareStatement( consultaTieneRutaFavorita );
                st.setString( 1, placa );
                rs = st.executeQuery();
                if ( !rs.next() ) { // si esta consulta no devuelve nada es porque no hay restriccion de ruta favorita para ese recurso
                    sw= true;
                }
                st = con.prepareStatement( consultaPuedeViajarEnRutaFavorita );
                st.setString( 1, placa );
                st.setString( 2, origen );
                st.setString( 3, destino );
                rs = st.executeQuery();
                sw= rs.next(); // si esta consulta devuelve algo es porque si existe esa ruta favorita para ese recurso
            }
            
        }
        catch ( SQLException e ) {
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE BUSCAR RUTA FAVORITA " +
            e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        return sw;
    }
    
    public boolean puedeTransportarCliente( String placa, String cliente ) throws
    SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw= false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra");
            ////System.out.println( "con: " + con );
            if ( con != null ) {
                st = con.prepareStatement( consultaTieneClienteFavorito );
                st.setString( 1, placa );
                rs = st.executeQuery();
                if ( !rs.next() ) { // si esta consulta no devuelve nada es porque no hay restriccion de cliente favorito para ese recurso
                    sw= true;
                }
                st = con.prepareStatement( consultaPuedeTransportarCliente );
                st.setString( 1, placa );
                st.setString( 2, cliente );
                rs = st.executeQuery();
                sw= rs.next(); // si esta consulta devuelve algo es porque ese recurso si puede transportar al cliente
            }
            
        }
        catch ( SQLException e ) {
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE BUSCAR CLIENTE FAVORITO " +
            e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        return sw;
    }
    
    
    public void  realizarAsignacion( Recursosdisp cabezote, Recursosdisp trailer,
    ReqCliente requerimiento, String tipoAsig ) throws SQLException {
        RegistroAsignacion reg = null;
        
        if ( cabezote != null ) {
            ////System.out.println("Realizando asignaci�n de un cabezote, PLACA:"+cabezote.getPlaca());
            
            asignarRecursoARequerimiento( cabezote, requerimiento, tipoAsig );
            String tipoAso = trailer == null ? "" : trailer.getTipo();
            String recursoAso = trailer == null ? "" : trailer.getPlaca();
            
            guardarRecurso( cabezote, tipoAso, recursoAso );
            
            guardarRegistroAsignacion( cabezote, requerimiento, recursoAso, tipoAso );
            
        }
        if ( trailer != null ) {
            asignarRecursoARequerimiento( trailer, requerimiento, tipoAsig );
            
            String tipoAso = cabezote == null ? "" : cabezote.getClase();
            String recursoAso = cabezote == null ? "" : cabezote.getTipo();
            
            guardarRecurso( trailer, tipoAso, recursoAso );
            //  guardarRegistroAsignacion( trailer, requerimiento, recursoAso, tipoAso );
        }
        if ( cabezote == null && trailer == null ){
            ////System.out.println("No se pudo realizar asignaci�n de cabezote="+cabezote+", trailer="+trailer);
        }
    }
    
    /**
     * guardarRegistroAsignacion
     */
    private RegistroAsignacion guardarRegistroAsignacion( Recursosdisp recurso, ReqCliente requerimiento,
    String recursoAso, String tipoAso ) throws SQLException {
        
        RegistroAsignacion reg = new RegistroAsignacion();
        
        reg.setClase_req( requerimiento.getclase_req() );
        reg.setCliente_req( requerimiento.getcliente() );
        reg.setCod_cliente_rec( recurso.getCod_cliente() );
        reg.setCreation_user( recurso.getUsuario_creacion() );
        reg.setDestino_rec( recurso.getDestino() );
        reg.setDestino_req( requerimiento.getdestino() );
        reg.setDistri_rec( recurso.getDistrito() );
        reg.setDistri_req( requerimiento.getdstrct_code() );
        reg.setEquipo_aso_rec( recursoAso );
        reg.setEstado_req( recurso.getTipo_asig() );//********************
        reg.setFecha_asig_rec(requerimiento.getfecha_asign()  );//LA FECHA DE ASIGNACION DEL RECURSO ES LA MISMA QUE LA DE ASIGNACION DEL REQUERIMIENTO.
        reg.setFecha_asing_req( requerimiento.getfecha_asign() );
        reg.setFecha_disp_rec( recurso.getaFecha_disp() );
        reg.setFecha_disp_req( requerimiento.getfecha_dispo() );
        reg.setFecha_posibleentrega_req( requerimiento.getFecha_posibleentrega() );
        reg.setId_rec_cab_req( requerimiento.getId_rec_cab() );
        reg.setId_rec_tra_req( requerimiento.getId_rec_tra() );
        reg.setNuevafecha_dispo_req( recurso.getaFecha_disp() );
        reg.setNuevo_origen( requerimiento.getdestino() );
        reg.setNumpla_rec( recurso.getNumpla() );
        reg.setNumpla_req( requerimiento.getnumpla() );
        reg.setNum_sec_req( requerimiento.getnum_sec() );
        reg.setOrigen_rec( recurso.getOrigen() );
        reg.setOrigen_req( requerimiento.getorigen() );
        reg.setPlaca_rec( recurso.getPlaca() );
        reg.setPrioridad1_req( requerimiento.getprioridad1() );
        reg.setPrioridad2_req( requerimiento.getprioridad2() );
        reg.setPrioridad3_req( requerimiento.getprioridad3() );
        reg.setPrioridad4_req( requerimiento.getprioridad4() );
        reg.setPrioridad5_req( requerimiento.getprioridad5() );
        reg.setRecurso1_req( requerimiento.getrecurso1() );
        reg.setRecurso2_req( requerimiento.getrecurso2() );
        reg.setRecurso3_req( requerimiento.getrecurso3() );
        reg.setRecurso4_req( requerimiento.getrecurso4() );
        reg.setRecurso5_req( requerimiento.getrecurso5() );
        reg.setReg_status_rec( "A" );
        reg.setStd_job_no_req( requerimiento.getstd_job_no() );
        reg.setTiempo_rec( recurso.getaTiempo() );
        reg.setTipo_asig_rec( recurso.getTipo_asig() );
        reg.setTipo_asing_req( requerimiento.gettipo_asign() );
        reg.setTipo_aso_rec( tipoAso );
        reg.setTipo_rec1_req( requerimiento.getTipo_recurso1() );
        reg.setTipo_rec2_req( requerimiento.getTipo_recurso2() );
        reg.setTipo_rec3_req( requerimiento.getTipo_recurso3() );
        reg.setTipo_rec4_req( requerimiento.getTipo_recurso4() );
        reg.setTipo_rec5_req( requerimiento.getTipo_recurso5() );
        reg.setTipo_recurso_rec( recurso.getClase() );
        reg.setRecurso_rec(recurso.getTipo());
        reg.setUser_update( recurso.getUsuario_actualizacion() );
        reg.setStd_job_desc_rec(recurso.getReg_status());///AL RECURSO LE ASIGNAMOS EL STANDARD CON EL Q HABIA VIAJADO
        reg.setStd_job_desc_req(requerimiento.getestado());
        if ( asignados != null ){
            asignados.addElement(reg);
        }
        return reg;
    }
    
    /**
     * guardarRecurso
     *
     * @param cabezote Recursosdisp
     * @param fechaPosibleEntrega String
     * @param tipoAso String
     * @param recursoAso String
     */
    private void guardarRecurso( Recursosdisp r, String tipoAso,
    String recursoAso ) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra");
            ////System.out.println( "con: " + con );
            if ( con != null ) {
                
                ps = con.prepareStatement( this.sqlCrearNuevoRecursoDisponible );
                ps.setString( 1, r.getDistrito() );
                ps.setString( 2, r.getPlaca() );
                ps.setString( 3, r.getNuevaFechaDisponibilidad() );
                ps.setString( 4, "" );
                ps.setString( 5, "" );//EL NUMPLA PARA EL NUEVO RECURSO SE DEJA VACIO.
                ps.setString( 6, r.getOrigen() );
                ps.setString( 7, r.getDestino() );
                ps.setString( 8, r.getClase() );
                ps.setString( 9, r.getTipo() );
                ps.setFloat( 10, r.getaTiempo() );
                ps.setString( 11, recursoAso );
                ps.setString( 12, tipoAso );
                ps.setString( 13, r.getUsuario_creacion() );
                ps.setString( 14, r.getUsuario_actualizacion() );
                ps.setString( 15, r.getStd_job_no_rec());
                //////System.out.println("guardar recurso: "+ps);
                ps.execute();
            }
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
        }
        finally {
            if ( ps != null ) {
                try {
                    ps.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
    }
    
    /**
     * Preguntas pendientes:
     * - En el campo fecha_asig va la hora actual en que se realiza la asignacion o
     * la hora que indica el requerimiento en que debe ser aignado?
     * @param recurso Recursosdisp
     * @param requerimiento ReqCliente
     * @throws SQLException
     */
    private void asignarRecursoARequerimiento( Recursosdisp recurso, ReqCliente requerimiento, String tipoAsig ) throws
    SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra");
            if ( con != null ) {
                
                /*
                 * SE BUSCA LA DURACION DE LA VIA DEL REQUERIMIENTO QUE VAMOS A ASIGNAR.
                 */
                PreparedStatement st3 = con.prepareStatement( consultaObtenerDuracionVia );
                st3.setString( 1, requerimiento.getorigen() );
                st3.setString( 2, requerimiento.getdestino() );
                rs = st3.executeQuery();
                int duracion = 24; //************************************* ESTO ES POR SEGURIDAD
                if ( rs.next() ){
                    duracion = (int)rs.getFloat( "duracion" );// este valor no era entero
                }
                else {
                    ////System.out.println("Via no encontrada: origen="+requerimiento.getorigen()+" - destino="+requerimiento.getdestino());
                    ////System.out.println("VIA NO ENCONTRADA");
                }
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db" );
                int KI = Integer.parseInt( rb.getString( "KI" ) );
                if ( recurso.getFechaInicio() == null ){
                    
                    Calendar inicioRango = ( Calendar ) Util.crearCalendar(recurso.getaFecha_disp());
                    //inicioRango.add(inicioRango.HOUR, KI);
                    recurso.setFechaInicio(inicioRango);
                }
                // esta fecha es calculada cuando se valida el rango de recurso respecto a la fecha del requerimiento
                // -> com.slt.AsignarYSincronizar.requerimientoEntraEnRangoDeRecurso
                Calendar cRec = recurso.getFechaInicio();
                Calendar cReq = Util.crearCalendar( requerimiento.getfecha_dispo() );
                Calendar cMayor = cRec.after( cReq ) ? cRec : cReq;
                
                ////System.out.println("fecha del recurso: "+cRec.get(Calendar.YEAR)+"/"+cRec.get(Calendar.MONTH)+"/"+cRec.get(Calendar.DATE)+" - "+cRec.get(Calendar.HOUR)+":"+cRec.get(Calendar.MINUTE));
                ////System.out.println("fecha del requerimiento : "+cReq.get(Calendar.YEAR)+"/"+cReq.get(Calendar.MONTH)+"/"+cReq.get(Calendar.DATE)+" - "+cReq.get(Calendar.HOUR)+":"+cReq.get(Calendar.MINUTE));
                ////System.out.println("fecha mayor: "+cMayor.get(Calendar.YEAR)+"/"+cMayor.get(Calendar.MONTH)+"/"+cMayor.get(Calendar.DATE)+" - "+cMayor.get(Calendar.HOUR)+":"+cMayor.get(Calendar.MINUTE));
                
                String horaSalida = Util.crearStringFecha( cMayor );
                ////System.out.println("hora salida: "+horaSalida);
                
                
                st = con.prepareStatement( "update recursos_disp set reg_status = ?,tipo_asig = ?, fecha_asig = ?, last_update = now(), dstrct_code_req = ?, num_sec_req = ?, std_job_no_req = ?, numpla_req = ?, fecha_dispo_req = ?, cod_cliente = ? where recurso = ? and dstrct = ? and placa = ? and fecha_disp = ?" );
                st.setString(1, tipoAsig );
                st.setString(2, tipoAsig );
                st.setString( 3, horaSalida );
                st.setString( 4, recurso.getDistrito() );
                st.setInt( 5, requerimiento.getnum_sec() );
                st.setString( 6, requerimiento.getstd_job_no() );
                st.setString( 7, requerimiento.getnumpla() );
                st.setString( 8, requerimiento.getfecha_dispo() );
                st.setString( 9,  recurso.getCod_cliente());
                st.setString( 10, recurso.getTipo() );
                st.setString( 11, recurso.getDistrito() );
                st.setString( 12, recurso.getPlaca() );
                st.setString( 13, recurso.getaFecha_disp() );
                ////System.out.println("asignado recurso: "+st);
                st.execute();
                
                /*
                 *SE SUMA A LA FECHA DE ASIGNACION EL VALOR QUE TIENE LA VIA.
                 */
                cMayor.add(Calendar.HOUR, duracion);
                String fechaPosibleEntrega = Util.crearStringFecha( cMayor );
                
                PreparedStatement st2 = con.prepareStatement( "update req_cliente set estado = 'A', fecha_asing = ?, fecha_posibleentrega = ?, fecha_actualizacion = 'now()' where dstrct_code = ? and num_sec = ? and std_job_no = ? and fecha_dispo = ? and numpla = ?" );
                st2.setString( 1, horaSalida );
                st2.setString( 2, fechaPosibleEntrega );
                st2.setString( 3, requerimiento.getdstrct_code() );
                st2.setInt( 4, requerimiento.getnum_sec() );
                st2.setString( 5, requerimiento.getstd_job_no() );
                st2.setString( 6, requerimiento.getfecha_dispo() );
                st2.setString(7, requerimiento.getnumpla() );
                st2.execute();
                
                cMayor = Util.crearCalendar( fechaPosibleEntrega );
                cMayor.add( Calendar.HOUR, KI );
                
                
                /*
                 *SE BUSCA EL STANDARD DEL RECURSO.
                 */
                
                //   st = con.prepareStatement("select std_job_desc,stdjob.std_job_no as numero from planilla,plarem,remesa,stdjob where planilla.numpla = ? and planilla.numpla = plarem.numpla and plarem.numrem = remesa.numrem and remesa.std_job_no = stdjob.std_job_no");
                st = con.prepareStatement("select std_job_desc,stdjob.std_job_no as numero from stdjob where std_job_no = ?");
                st.setString(1, recurso.getStd_job_no_rec());
                ////System.out.println(st);
                rs = st.executeQuery();
                String standard = rs.next()? (rs.getString("std_job_desc").length()>1?rs.getString("std_job_desc"):rs.getString("numero")):"(estandar no encontrado)";
                /*
                 *SE ACTUALIZAN LOS DATOS DEL RECURSO
                 */
                
                recurso.setTipo_asig(tipoAsig);
                recurso.setDstrct_code_req( requerimiento.getdstrct_code() );
                recurso.setNum_sec_req( requerimiento.getnum_sec() );
                recurso.setStd_job_no_req( requerimiento.getstd_job_no() );
                recurso.setNumpla_req( requerimiento.getnumpla() );
                recurso.setFecha_dispo_req( requerimiento.getfecha_dispo() );
                recurso.setCod_cliente(requerimiento.getcliente());
                recurso.setNuevaFechaDisponibilidad( Util.crearStringFecha( cMayor ) );
                recurso.setOrigen( requerimiento.getorigen() );
                recurso.setDestino( requerimiento.getdestino() );
                recurso.setaFecha_asig( requerimiento.getfecha_asign() );
                recurso.setReg_status(standard);
                //  recurso.setStd_job_no_rec(requerimiento.getstd_job_no());
                
                /*
                 *SE BUSCA EL STANDARD DEL REQUERIMIENTO
                 */
                
                //   st = con.prepareStatement("select std_job_desc,stdjob.std_job_no as numero from planilla,plarem,remesa,stdjob where planilla.numpla = ? and planilla.numpla = plarem.numpla and plarem.numrem = remesa.numrem and remesa.std_job_no = stdjob.std_job_no");
                st = con.prepareStatement("select std_job_desc,stdjob.std_job_no as numero from stdjob where std_job_no = ?");
                st.setString(1, requerimiento.getstd_job_no());
                ////System.out.println(st);
                rs = st.executeQuery();
                standard = rs.next()? (rs.getString("std_job_desc").length()>1?rs.getString("std_job_desc"):rs.getString("numero")):"(estandar no encontrado)";
                
                
                requerimiento.setfecha_posibleentrega( fechaPosibleEntrega );
                requerimiento.setfecha_asign( horaSalida );
                requerimiento.setestado(standard);
                
            }
            
            
        }
        catch ( SQLException e ) {
            e.printStackTrace();
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE ASIGNAR RECURSO A REQUERIMIENTO " +
            e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
        
    }
    
    /**
     * Getter for property asignados.
     * @return Value of property asignados.
     */
    public java.util.Vector getAsignados() {
        return asignados;
    }
    
    /**
     * Setter for property asignados.
     * @param asignados New value of property asignados.
     */
    public void setAsignados(java.util.Vector asignados) {
        this.asignados = asignados;
    }
    
    public String buscarRecursosxTipo( String tipo ) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        PoolManager poolManager = null;
        String recursos="";
        ResultSet rs = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                
                ps = con.prepareStatement( "Select recurso from recursos where ind = ?" );
                ps.setString( 1,tipo);
                rs = ps.executeQuery();
                while(rs.next()){
                    
                    recursos = recursos+ rs.getString("recurso")+";";
                }
            }
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
        }
        finally {
            if ( ps != null ) {
                try {
                    ps.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
        return recursos;
    }
    
    /***********12-12-05*****************/
    
    public boolean BuscarRecurso(String placa)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SEARCH);
                st.setString(1,placa);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS RECURSOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    /*Jescandon 11-01-05*/
    /**
     * Metodo list_viajes, lista todas los vehiculos con sus viajes correspondientes en el ultimo mes
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public List list_viajes() throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.List_Viajes);
            ////System.out.println("LISTA Lista Viajes " + st);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    rd = new Recursosdisp();
                    rd.setNo_Viajes(rs.getInt("viajes"));
                    rd.setPlaca(rs.getString("placa"));
                    rd.setUsuario_creacion("kreales");
                    lista.add(rd);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina list_viajes [RecursosdispDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return lista;
    }
    
    /**
     * Metodo Update_record, Actualiza un registro de la tabla record_tabla dada una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     *@param : int Numero de Viajes , String Usuario de Modificacion, String Numero de placa
     * @version : 1.0
     */
    public void Update_record( int nviajes,  String user, String placa ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        try{
            st = con.prepareStatement(this.Update_record);
            st.setInt(1, nviajes);
            st.setString(2, user);
            st.setString(3, placa);
            ////System.out.println(st+"---update----");
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("Error en rutina Update_record [RecursosdispDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
    }
    
    
    /**
     * Metodo Insert_record, Inserta un registro de la tabla record_tabla dada una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String distrito, int Numero de Viajes ,  String Numero de placa, String Usuario de Creacion
     * @version : 1.0
     */
    public void Insert_record( String distrito ,int nviajes,  String placa, String user ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        try{
            st = con.prepareStatement(this.Insert_record);
            st.setString(1, distrito);
            st.setInt(2, nviajes );
            st.setString(3, placa);
            st.setString(4, user);
            ////System.out.println(st+"----insert---");
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("Error en rutina Insert_record [RecursosdispDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        
    }
    
    /**
     * Metodo Search_record, Busca un registro de la tabla record_tabla dada una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String Numero de placa
     * @version : 1.0
     */
    public void Search_record( String placa ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        try{
            st = con.prepareStatement(this.Search_record);
            st.setString(1, placa);
            rs = st.executeQuery();
            if(rs.next()){
                rd.setPlaca(rs.getString("placa"));
                rd.setNo_Viajes(rs.getInt("no_viajes"));
            }
            else{
                rd.setPlaca(placa);
                rd.setNo_Viajes(0);
            }
            
            
            
        }catch(Exception e){
            throw new SQLException("Error en rutina Search_record  [RecursosdispDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
    }
    
    /**
     * Metodo Search_r, Indica si un registro de la tabla record_tabla existe dada una placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String Numero de placa
     * @version : 1.0
     */
    public boolean Search_r( String placa ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag         = false;
        try{
            st = con.prepareStatement(this.Search_record);
            st.setString(1, placa);
            rs = st.executeQuery();
            if(rs.next())
                flag = true;
            
        }catch(Exception e){
            throw new SQLException("Error en rutina Search_record  [RecursosdispDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return flag;
    }
    
    /* 13-01-06  */
    /**
     * Metodo Search_Wgroup, Busca todos los standares relacionados a una placa a travez de los
     * work_group
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String Numero de placa
     * @version : 1.0
     */
    public String Search_Wgroup( String placa ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        String std_job       = "";
        try{
            st = con.prepareStatement(this.Search_wgroup);
            st.setString(1, placa);
            rs = st.executeQuery();
            while(rs.next()){
                std_job += rs.getString("std_job_no") + ",";
            }
            if( !std_job.equals(""))
                std_job = std_job.substring(0,std_job.length()-1);
            
        }catch(Exception e){
            throw new SQLException("Error en rutina Search_Wgroup  [RecursosdispDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return std_job;
    }
    
    /* M�todo obtenerRecursosByWhere, obtiene registros de recursos_disp
     *       que contengan el dato en la columna dada
     * @author: Osvaldo P�rez Ferrer
     * @param: colum, columna por la que se va a filtar
     * @param: dato, cadena a buscar
     * @throws: En caso de que un error de base de datos ocurra.
     */
    public void obtenerRecursosByWhere(String where, String dstrct) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        
        Wgroup_PlacaDAO w = new Wgroup_PlacaDAO();
        
        try{
            st = this.crearPreparedStatement("SQL_WHERE");
            st.setString( 1, dstrct );
            sql = st.toString();
            sql = sql.replaceAll( "#WHERE#", "WHERE "+where);
            
            recursosd = new Vector();
            
            rs = st.executeQuery(sql);
            List l = new LinkedList();
            
            while( rs.next() ){
                rd = new Recursosdisp();
                String placa = (rs.getString( "placa" ) != null)? rs.getString( "placa" ) : "";
                rd.setPlaca( placa );
                rd.setNumpla( rs.getString( "numpla" ) );
                rd.setRecurso( rs.getString( "recurso" ) );
                rd.setFecha_disp( rs.getTimestamp( "fecha_disp" ) );
                rd.setOrigen( rs.getString( "origen" ) );
                rd.setDestino( rs.getString( "destino" ) );
                rd.setFecha_asig( rs.getTimestamp( "fecha_asig" ) );
                rd.setStd_job_no_req( rs.getString( "std_job_no_req" ) );
                
                String wg = "";
                try{
                    l = w.wGroupXPlaca(placa);
                    for(int i =0; i<l.size();i++){
                        Wgroup_Placa wgplaca = (Wgroup_Placa) l.get(i);
                        wg += ","+wgplaca.getWgroup();
                    }
                    wg = wg.substring( 1 , wg.length() );
                }catch(Exception e){}
                
                rd.setWork_gorup( wg );
                
                recursosd.add( rd );
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR en obtenerRecursosByWhere " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            this.desconectar("SQL_WHERE");
        }
    }
    
     /* M�todo updateRecursoAsignar, actualiza el recurso disponible
      *         despu�s de asignado
      * @author: Osvaldo P�rez Ferrer
      * @param: placa, placa del recurso
      * @param: fechadisp, fecha del recurso a actualizar
      * @param: destino, nuevo destino del recurso
      * @param: fecha, nueva fecha_disp para el recurso
      * @param: usuario, usuario que asign� recurso
      * @throws: En caso de que un error de base de datos ocurra.
      */
    public void updateRecursoAsignar(String placa, String fechadisp, String destino, String fecha, String usuario) throws SQLException{
        
        PreparedStatement st = null;
        
        try{
            st = this.crearPreparedStatement("QUERY_RECURSO_ASIGNADO");
            st.setString( 1, destino );
            st.setString( 2, usuario );
            st.setString( 3, fecha );
            st.setString( 4, placa );
            st.setString( 5, fechadisp );
            
            st.executeUpdate();
            
        }catch(SQLException e){
            throw new SQLException("ERROR en obtenerRecursosByWhere " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR en updateRecursoAsignar " + e.getMessage());
                }
            }
            this.desconectar("QUERY_RECURSO_ASIGNADO");
        }
    }
    
    /* M�todo obtenerRecursos, obtiene recursos de la tabla recursos
     * @author: Osvaldo P�rez Ferrer
     * @throws: En caso de que un error de base de datos ocurra.
     */
    public Vector obtenerRecursos() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        
        Vector v = new Vector();
        try{
            st = this.crearPreparedStatement("QUERY_OBTENER_RECURSOS");
            
            rs = st.executeQuery();
            
            Recursosdisp r;
            
            while( rs.next() ){
                r = new Recursosdisp();
                r.setRecurso( rs.getString( "recurso" ) );
                r.setClase( rs.getString( "descri" ) );
                v.add(r);
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR en obtenerRecursosByWhere " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR en obtenerRecursos " + e.getMessage());
                }
            }
            this.desconectar("QUERY_OBTENER_RECURSOS");
        }
        return v;
    }
    public boolean BuscarRecursoTrailer(String placa)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SEARCHTRAILER);
                st.setString(1,placa);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS RECURSOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    /**
     * Metodo  insertFrontera, inserta los recursos disponibles por frontera
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertFrontera()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into recursos_disp (dstrct, numpla, origen, destino, placa, recurso, tiporecurso,fecha_disp, tiempo, cod_cliente ,creation_user,std_job_no_rec, no_viajes, work_group,agencia, equipo_propio,frontera, disponibilidad_frontera,equipo_aso) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'S','N',?)");
                st.setString(1, rd.getDistrito());
                st.setString(2, rd.getNumpla());
                st.setString(3, rd.getOrigen());
                st.setString(4, rd.getDestino());
                st.setString(5, rd.getPlaca());
                st.setString(6, rd.getClase());
                st.setString(7, rd.getTipo());
                st.setTimestamp(8,rd.getFecha_disp());
                st.setFloat(9, rd.getTiempo());
                st.setString(10,rd.getCod_cliente());
                st.setString(11, rd.getUsuario_creacion());
                st.setString(12, rd.getStd_job_no_rec());
                st.setInt(13, rd.getNo_Viajes());
                st.setString(14, rd.getWork_gorup());
                st.setString(15, rd.getAgencia());
                st.setString(16, rd.getEquipo_propio());
                st.setString(17, rd.getEquipo_aso());
                
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL RECURSO DISPONIBLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo  marcarFrontera
     * @param:
     * @autor : Ing. Karen 02-10-2006
     * @version : 1.0
     */
    public void marcarFrontera()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_MARCA_FRONTERA);
                st.setString(1, rd.getNumpla_req());
                st.setString(2, rd.getDistrito());
                st.setTimestamp(3,rd.getFecha_disp());
                st.setString(4, rd.getPlaca());
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL RECURSO DISPONIBLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }    
    
    public void insert()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into recursos_disp (dstrct, numpla, origen, destino, placa, recurso, tiporecurso,fecha_disp, tiempo, cod_cliente ,creation_user,std_job_no_rec, no_viajes, work_group, agencia) values (?,?,?,?,?,?,?,?,?,?,?,?,?, ?, ?)");
                st.setString(1, rd.getDistrito());
                st.setString(2, rd.getNumpla());
                st.setString(3, rd.getOrigen());
                st.setString(4, rd.getDestino());
                st.setString(5, rd.getPlaca());
                st.setString(6, rd.getClase());
                st.setString(7, rd.getTipo());
                st.setTimestamp(8,rd.getFecha_disp());
                st.setFloat(9, rd.getTiempo());
                st.setString(10,rd.getCod_cliente());
                st.setString(11, rd.getUsuario_creacion());
                st.setString(12, rd.getStd_job_no_rec());
                st.setInt(13, rd.getNo_Viajes());
                st.setString(14, rd.getWork_gorup());
                
                st.setString(15, (rd.getAgencia()!=null )?rd.getAgencia():"" );//Jescandon 09-10-06
                ////System.out.println("ST " + st);
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL RECURSO DISPONIBLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void insertEquipoA()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into recursos_disp (dstrct, numpla, origen, destino,placa, recurso,tiporecurso,fecha_disp,tiempo,cod_cliente,equipo_aso,tipo_aso,creation_date,last_update,creation_user,std_job_no_rec,no_viajes, work_group, agencia) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?,?,?)");
                st.setString(1, rd.getDistrito());
                st.setString(2, rd.getNumpla());
                st.setString(3, rd.getOrigen());
                st.setString(4, rd.getDestino());
                st.setString(5, rd.getPlaca());
                st.setString(6, rd.getClase());
                st.setString(7, rd.getTipo());
                st.setTimestamp(8,rd.getFecha_disp());
                st.setFloat(9, rd.getTiempo());
                st.setString(10,rd.getCod_cliente());
                st.setString(11, rd.getEquipo_aso());
                st.setString(12, rd.getTipo_aso());
                st.setTimestamp(13,rd.getFecha_creacion());
                st.setTimestamp(14,rd.getFecha_actualizacion());
                st.setString(15, rd.getUsuario_creacion());
                st.setString(16, rd.getStd_job_no_rec());
                st.setInt(17, rd.getNo_Viajes());
                st.setString(18, rd.getWork_gorup());
                
                st.setString(19, (rd.getAgencia()!=null )?rd.getAgencia():"" );
                ////System.out.println("ST " + st);
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL RECURSO ASOCIADO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void Update()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(update);
                st.setString(1, rd.getDistrito());
                st.setString(2, rd.getNumpla());
                st.setString(3, rd.getOrigen());
                st.setString(4, rd.getDestino());
                st.setString(5, rd.getPlaca());
                st.setString(6, rd.getClase());
                st.setString(7, rd.getTipo());
                st.setTimestamp(8,rd.getFecha_disp());
                st.setFloat(9, rd.getTiempo());
                st.setString(10,rd.getCod_cliente());
                st.setString(11, rd.getUsuario_creacion());
                st.setString(12, rd.getStd_job_no_rec());
                st.setInt(13, rd.getNo_Viajes());
                st.setString(14, rd.getWork_gorup());
                
                st.setString(15, (rd.getAgencia()!=null )?rd.getAgencia():"" );
                
                st.setString(16, rd.getDistrito());
                st.setTimestamp(17,rd.getFecha_disp());
                st.setString(18, rd.getPlaca());
                
                
                ////System.out.println("ST " + st);
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL RECURSO DISPONIBLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void UpdateRA()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(updateRA);
                st.setString(1, rd.getDistrito());
                st.setString(2, rd.getNumpla());
                st.setString(3, rd.getOrigen());
                st.setString(4, rd.getDestino());
                st.setString(5, rd.getPlaca());
                st.setString(6, rd.getClase());
                st.setString(7, rd.getTipo());
                st.setTimestamp(8,rd.getFecha_disp());
                st.setFloat(9, rd.getTiempo());
                st.setString(10,rd.getCod_cliente());
                st.setString(11, rd.getEquipo_aso());
                st.setString(12, rd.getTipo_aso());
                st.setString(13, rd.getUsuario_creacion());
                st.setString(14, rd.getStd_job_no_rec());
                st.setInt(15, rd.getNo_Viajes());
                st.setString(16, rd.getWork_gorup());
                st.setString(17, (rd.getAgencia()!=null )?rd.getAgencia():"" );
                st.setString(18, rd.getDistrito());
                st.setTimestamp(19,rd.getFecha_disp());
                st.setString(20, rd.getPlaca());
                
                
                ////System.out.println("ST " + st);
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL RECURSO DISPONIBLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
}
