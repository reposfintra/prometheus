 
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;


public class LogProcesosService {
    
    private LogProcesosDAO  LogProcesosDataAccess;
    private List lista;
   
    
    
    //Inicializamos las variables  
//    public LogProcesosService() {
//        LogProcesosDataAccess = new LogProcesosDAO();
//        lista=null;
//    }
    public LogProcesosService(String dataBaseName) {
        LogProcesosDataAccess = new LogProcesosDAO(dataBaseName);
        lista=null;
    }
    
    
    //Buscamos los procesos setiamos la lista
    public String  searchLogProcesos(String estado, String fecha1,String fecha2, String user) throws SQLException{
        String comentario="";
        try{
           this.lista=LogProcesosDataAccess.QueryLogProcesos(estado,fecha1,fecha2, user); 
        }catch(Exception e){
           throw new SQLException(e.getMessage());
        }
        if(lista==null)
            comentario ="No se encontr� proceso de tipo " + estado;
        return comentario;
    }
    
    
    
    // Devolvemos la lista
    public List getList(){
        return this.lista;
    }
    
    
    // finalizamos el proceso
    public void finallyProceso(String proceso,int id,String usuario, String comentario)throws SQLException {
      try{
          LogProcesosDataAccess.FinalizarProceso(proceso,id,usuario, comentario);
      }catch(Exception e){
          throw new SQLException(e.getMessage());
      }
    }
    
    //finalizamos el proceso metodo 2
    public void finallyProceso2(String proceso,int id,String usuario, String comentario)throws SQLException {
      try{
          LogProcesosDataAccess.FinalizarProceso2(proceso,id,usuario, comentario);
      }catch(Exception e){
          throw new SQLException(e.getMessage());
      }
    }
    
    
     // finalizamos el proceso
    public void updateProceso(String proceso,int id,String usuario, String comentario)throws SQLException {
      try{
          LogProcesosDataAccess.ActualizarProceso(proceso,id,usuario, comentario);
      }catch(Exception e){
          throw new SQLException(e.getMessage());
      }
    }
    
    
    
    //Anulamos el proceso
     public void AnularProceso(int id)throws SQLException {
       try{
          LogProcesosDataAccess.AnularProceso( id );
       }catch(Exception e){
          throw new SQLException(e.getMessage());
       }
     }
     
     
     //Insertamos los procesos, pero creamos la tabla de procesos si no Existe
      public void InsertProceso(String proceso,int id,String descripcion, String usuario)throws SQLException {
         // try{create();}catch(Exception e){}
          try{  
              ////System.out.println("Proceso ("+proceso + ") Code ("+id+") Usuario ("+usuario+") descripcion ("+descripcion+")" );
              LogProcesosDataAccess.InsertarProceso(proceso, id,descripcion,usuario);
          }catch(Exception e){              
             throw new SQLException(e.getMessage());             
          }
      }
     

      public void create(){
          try{  
              LogProcesosDataAccess.crearLOGPROCESO();
          }catch(Exception f){}   
      }
     
}//FIN SERVICE
