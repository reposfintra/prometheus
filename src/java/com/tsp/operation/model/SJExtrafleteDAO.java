/*
 * SJExtrafleteDAO.java
 *
 * Created on 15 de julio de 2005, 9:44
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;

/**
 *
 * @author  Henry
 */
public class SJExtrafleteDAO extends MainDAO {
    //Diogenes 2006-04-11
    private static final String SQL_BUSCAR_EXTRAFLETES =
    "SELECT  a.codigo,         " +
    "      SUM (a.vlr_costo) as vlr_costo,  " +
    "      a.moneda_costo,  " +
    "      a.tipo_costo,  " +
    "      SUM(a.vlr_ingreso) as vlr_ingreso, " +
    "      a.cantidad,   " +
    "      b.descripcion  " +
    " FROM                " +
    "      costo_reembolsables a,  " +
    "      codextraflete b   " +
    " WHERE                  " +
    "      a.dstrct = ?      " +
    "  AND a.numpla = ?      " +
    "  AND a.std_job_no = ?  " +
    "  AND a.reg_status <> 'A'" +
    "  AND b.codextraflete = a.codigo " +
    "  GROUP BY a.codigo, " +
    "           a.moneda_costo,  " +
    "           a.tipo_costo,    " +
    "           a.cantidad,      " +
    "           b.descripcion    " +
    "  order by descripcion";
    
    private static final String SQL_INSERTAR_SJEXTRA  =  "INSERT INTO SJEXTRAFLETE VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
    
     private static final String SQL_INSERTAR_COSTOREM  =  "INSERT INTO costo_reembolsables (codigo,dstrct,proveedor,numrem,numpla,codcli,std_job_no,element_gasto,vlr_costo,moneda_costo,fecdsp,tipo_costo,vlr_ingreso,usuario_aprobacion,cantidad,creation_date,reembolsable,agencia,cuenta1,cuenta2,nit ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    
    private static final String SQL_ELIMINAR_SJEXTRA  =  "DELETE FROM SJEXTRAFLETE WHERE COD_EXTRAFLETE=? AND CODCLI=? AND STD_JOB_NO=?";
    
    private static final String SQL_ACTUALIZAR_SJE    =  "UPDATE SJEXTRAFLETE SET VLR_COSTO=?, MONEDA_COSTO=?, VF_COSTO=?, "+
    "REEMBOLSABLE=?, VLR_INGRESO=?, MONEDA_INGRESO=?, VF_INGRESO=?,  "+
    "CLASE_VALOR=?, PORCENTAJE=? WHERE COD_EXTRAFLETE=? AND CODCLI=? AND STD_JOB_NO=?";
    
    private static final String SQL_OBTENER_SJEXTRA   =  "SELECT * FROM SJEXTRAFLETE WHERE CODCLI=? AND STD_JOB_NO=? AND COD_EXTRAFLETE=?";
    
    private static final String SQL_LISTAR_SJEXTRA    =  
    "SELECT " +
    "     DISTINCT COD_EXTRAFLETE," +
    "     S.CODCLI," +
    "     STD_JOB_NO," +
    "     VLR_COSTO,"+
    "     MONEDA_COSTO," +
    "     VF_COSTO," +
    "     REEMBOLSABLE," +
    "     VLR_INGRESO," +
    "     MONEDA_INGRESO,"+
    "     VF_INGRESO," +
    "     CLASE_VALOR," +
    "     PORCENTAJE " +
    "FROM " +
    "     SJEXTRAFLETE S " +
    "     LEFT JOIN CLIENTE C on (C.CODCLI=S.CODCLI) "+
    "WHERE " +
    "     S.CODCLI LIKE ? OR C.NOMCLI LIKE ? ORDER BY S.CODCLI";
    
    private static final String SQL_EXTRAFLETES    =  "SELECT 	C.CODEXTRAFLETE, " +
    "    	C.DESCRIPCION, " +
    "    	C.AJUSTE, " +
    "    	C.UNIDADES,	 " +
    "    	S.CODCLI, " +
    "    	S.STD_JOB_NO, " +
    "    	S.VLR_COSTO, " +
    "    	S.MONEDA_COSTO, " +
    "    	S.VF_COSTO, " +
    "    	S.REEMBOLSABLE, " +
    "    	S.VLR_INGRESO, " +
    "    	S.MONEDA_INGRESO, " +
    "    	S.VF_INGRESO, " +
    "    	S.CLASE_VALOR AS PV, " +
    "    	S.PORCENTAJE, " +
    "        c.cod_descuento, " +
    "        c.elemento, " +
    "        c.tipo," +
    "	cli.nit," +
    "	t.account," +
    "	c.cuenta," +
    "	STD.ACCOUNT_CODE_C," +
    "	TG.TABLE_CODE," +
    "	character_length(STD.ACCOUNT_CODE_C)-4," +
    "	CASE WHEN S.REEMBOLSABLE ='S' then c.cuenta ELSE SUBSTRING(STD.ACCOUNT_CODE_C,1,1)||TG.TABLE_CODE||SUBSTRING(STD.ACCOUNT_CODE_C,4,character_length(SUBSTRING(STD.ACCOUNT_CODE_C,4))-4)||t.account END as cuenta1" +
    "     FROM SJEXTRAFLETE S" +
    "	  INNER JOIN CODEXTRAFLETE C ON (C.CODEXTRAFLETE = S.COD_EXTRAFLETE )" +
    "	  INNER JOIN CLIENTE CLI ON (cli.codcli  = '000'|| substring(s.std_job_no,1,3))" +
    "          INNER JOIN TBLCON T ON (T.CONCEPT_CODE = C.CODEXTRAFLETE)" +
    "	  INNER JOIN STDJOB STD ON (STD.STD_JOB_NO = S.STD_JOB_NO)" +
    "	  INNER JOIN TABLAGEN TG ON (TG.TABLE_TYPE  ='TAGECONT' AND POSITION (? IN TG.REFERENCIA)>0)" +
    "     WHERE  S.STD_JOB_NO =?" +
    "	    and c.tipo = 'E'";
    
    
    
    private static final String SQL_COSTOS    =  "SELECT 	C.CODEXTRAFLETE, " +
    "    	C.DESCRIPCION, " +
    "    	C.AJUSTE, " +
    "    	C.UNIDADES,	 " +
    "    	S.CODCLI, " +
    "    	S.STD_JOB_NO, " +
    "    	S.VLR_COSTO, " +
    "    	S.MONEDA_COSTO, " +
    "    	S.VF_COSTO, " +
    "    	S.REEMBOLSABLE, " +
    "    	S.VLR_INGRESO, " +
    "    	S.MONEDA_INGRESO, " +
    "    	S.VF_INGRESO, " +
    "    	S.CLASE_VALOR AS PV, " +
    "    	S.PORCENTAJE, " +
    "        c.cod_descuento, " +
    "        c.elemento, " +
    "        c.tipo," +
    "	cli.nit," +
    "	t.account," +
    "	c.cuenta," +
    "	STD.ACCOUNT_CODE_C," +
    "	TG.TABLE_CODE," +
    "	character_length(STD.ACCOUNT_CODE_C)-4," +
    "	CASE WHEN S.REEMBOLSABLE ='S' then c.cuenta ELSE SUBSTRING(STD.ACCOUNT_CODE_C,1,1)||TG.TABLE_CODE||SUBSTRING(STD.ACCOUNT_CODE_C,4,character_length(SUBSTRING(STD.ACCOUNT_CODE_C,4))-4)||t.account END as cuenta1" +
    "     FROM SJEXTRAFLETE S" +
    "	  INNER JOIN CODEXTRAFLETE C ON (C.CODEXTRAFLETE = S.COD_EXTRAFLETE )" +
    "	  INNER JOIN CLIENTE CLI ON (cli.codcli  = '000'|| substring(s.std_job_no,1,3))" +
    "          INNER JOIN TBLCON T ON (T.CONCEPT_CODE = C.CODEXTRAFLETE)" +
    "	  INNER JOIN STDJOB STD ON (STD.STD_JOB_NO = S.STD_JOB_NO)" +
    "	  INNER JOIN TABLAGEN TG ON (TG.TABLE_TYPE  ='TAGECONT' AND POSITION (? IN TG.REFERENCIA)>0)" +
    "     WHERE  S.STD_JOB_NO =?" +
    "	    and c.tipo = 'R'";
    
    private static final String SQL_EXTRAFLETE    =  "SELECT 	C.CODEXTRAFLETE," +
    "	C.DESCRIPCION," +
    "	C.AJUSTE," +
    "	C.UNIDADES,	" +
    "	S.CODCLI," +
    "	S.STD_JOB_NO," +
    "	S.VLR_COSTO," +
    "	S.MONEDA_COSTO," +
    "	S.VF_COSTO," +
    "	S.REEMBOLSABLE," +
    "	S.VLR_INGRESO," +
    "	S.MONEDA_INGRESO," +
    "	S.VF_INGRESO," +
    "	S.CLASE_VALOR AS PV," +
    "	S.PORCENTAJE," +
    "   c.cod_descuento" +
    " FROM 	SJEXTRAFLETE S," +
    "	CODEXTRAFLETE C" +
    " WHERE   S.COD_EXTRAFLETE = C.CODEXTRAFLETE" +
    "	AND S.STD_JOB_NO = ?" +
    "   AND C.CODEXTRAFLETE=?";
    
    private static final String SQL_EXTRA_PLA ="select m.planilla," +
    "       m.concept_code,		" +
    "       m.cantidad," +
    "       m.vlr," +
    "       c.codextraflete" +
    " from   movpla m," +
    "       CODEXTRAFLETE c" +
    " where  planilla = ?" +
    "       AND c.cod_descuento=m.concept_code" +
    "       and c.codextraflete = ?" +
    "       and m.reg_status=''";
    private static final String SQL_EXTRA_TOTAL ="select sum(m.vlr)as total" +
    " from   movpla m," +
    "       CODEXTRAFLETE c" +
    " where  planilla = ?" +
    "       AND c.cod_descuento=m.concept_code";
    
    private static final String SQL_COSTOS_PLA =    "SELECT 	*" +
    " FROM  costo_reembolsables" +
    " WHERE dstrct='FINV' " +
    "   and tipo_costo=? and" +
    "   numpla = ? ";
    
    private static final String SQL_EXTRAFLETES_APLICADOS    = "SELECT S.CODEXTRAFLETE,  " +
    "        	S.DESCRIPCION,  " +
    "        	S.AJUSTE,  " +
    "        	S.UNIDADES,	  " +
    "        	S.CODCLI,          	" +
    "           S.STD_JOB_NO,  " +
    "        	CASE WHEN CR.VLR_INGRESO IS NULL THEN  S.VLR_COSTO ELSE CR.VLR_COSTO END AS VLR_COSTO,  " +
    "        	S.MONEDA_COSTO,  " +
    "        	S.VF_COSTO,  " +
    "        	S.REEMBOLSABLE,  " +
    "        	CASE WHEN CR.VLR_INGRESO IS NULL THEN  S.VLR_INGRESO ELSE CR.VLR_INGRESO END AS VLR_INGRESO,  " +
    "        	S.MONEDA_INGRESO,  " +
    "        	S.VF_INGRESO,  " +
    "        	S.CLASE_VALOR AS PV,  " +
    "        	S.PORCENTAJE,  " +
    "               S.cod_descuento, " +
    "               cr.proveedor, " +
    "       	cr.usuario_aprobacion, " +
    "              case WHEN CR.VLR_INGRESO IS NULL THEN false else true end as utilizado, " +
    "               CASE WHEN  CR.CANTIDAD IS NULL THEN 1 ELSE CR.CANTIDAD END AS CANTIDAD ," +
    "		cli.nit, t.account," +
    "	c.cuenta," +
    "	STD.ACCOUNT_CODE_C," +
    "	TG.TABLE_CODE," +
    "	character_length(STD.ACCOUNT_CODE_C)-4," +
    "	CASE WHEN S.REEMBOLSABLE ='S' then c.cuenta ELSE SUBSTRING(STD.ACCOUNT_CODE_C,1,1)||TG.TABLE_CODE||SUBSTRING(STD.ACCOUNT_CODE_C,4,character_length(SUBSTRING(STD.ACCOUNT_CODE_C,4))-4)||t.account END as cuenta1" +
    "         FROM  (	select *  " +
    "    		from  SJEXTRAFLETE S,  " +
    "    		      CODEXTRAFLETE C  " +
    "    		WHERE S.COD_EXTRAFLETE = C.CODEXTRAFLETE  " +
    "    		      AND S.STD_JOB_NO =? " +
    "    		      and c.tipo = ?) S " +
    "	 INNER JOIN CODEXTRAFLETE C ON (C.CODEXTRAFLETE = S.COD_EXTRAFLETE )" +
    "	  INNER JOIN CLIENTE CLI ON (cli.codcli  = '000'|| substring(s.std_job_no,1,3))" +
    "          INNER JOIN TBLCON T ON (T.CONCEPT_CODE = C.CODEXTRAFLETE)" +
    "	  INNER JOIN STDJOB STD ON (STD.STD_JOB_NO = S.STD_JOB_NO)" +
    "	  INNER JOIN TABLAGEN TG ON (TG.TABLE_TYPE  ='TAGECONT' AND POSITION (? IN TG.REFERENCIA)>0)" +
    "     LEFT OUTER JOIN costo_reembolsables cr ON (CR.dstrct = 'FINV' AND CR.TIPO_COSTO = S.TIPO AND CR.CODIGO =S.CODEXTRAFLETE AND CR.NUMPLA=? and cr.numrem  =? and CR.reg_status<>'A') ;";
    
    private static final String SQL_ANULAR =    "UPDATE costo_reembolsables SET reg_status = 'A' WHERE numpla=? ";
    private static final String SQL_ANULAR_COSTO =    "UPDATE costo_reembolsables SET reg_status = 'A' WHERE dstrct=? and tipo_costo=? and  codigo=? and  numpla=? and reg_status <>'A'";
    
    private static final String SQL_ACTUALIZAR_APLICADOS =    "UPDATE costo_reembolsables " +
    "   SET proveedor = ?," +
    "       vlr_costo=?," +
    "       vlr_ingreso=?," +
    "       usuario_aprobacion=?," +
    "       cantidad=? " +
    "    WHERE dstrct=?" +
    "           and  tipo_costo=?" +
    "           and codigo=?" +
    "           and numpla=? " +
    "           and numrem = ?";
    
    private SJExtraflete sj;
    private Vector fletes;
    private Vector c_reembolsables;
    private float vlrtotalE=0;
    private float vlrtotalCR=0;
    
    /** Creates a new instance of SJExtrafleteDAO */
    public SJExtrafleteDAO() {
        super("SJExtrafleteDAO.xml");
    }
    public void setSJExtraflete(SJExtraflete sjExtra){
        sj = sjExtra;
    }
    public void insertarExtraflete() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_INSERTAR_SJEXTRA);
                st.setString(1, sj.getCod_extraflete());
                st.setString(2, sj.getCodcli());
                st.setString(3, sj.getStd_job_no());
                st.setDouble(4, sj.getValor_costo());
                st.setString(5, sj.getMoneda_costo());
                st.setString(6, sj.getVf_costo());
                st.setString(7, sj.getReembolsable());
                st.setDouble(8, sj.getValor_ingreso());
                st.setString(9, sj.getMoneda_ingreso());
                st.setString(10, sj.getVf_ingreso());
                st.setString(11, sj.getClase_valor());
                st.setFloat(12, sj.getPorcentaje());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR SJEXTRAFLETE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public void updateExtraflete() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_ACTUALIZAR_SJE);
                st.setDouble(1, sj.getValor_costo());
                st.setString(2, sj.getMoneda_costo());
                st.setString(3, sj.getVf_costo());
                st.setString(4, sj.getReembolsable());
                st.setDouble(5, sj.getValor_ingreso());
                st.setString(6, sj.getMoneda_ingreso());
                st.setString(7, sj.getVf_ingreso());
                st.setString(8, sj.getClase_valor());
                st.setFloat(9, sj.getPorcentaje());
                st.setString(10, sj.getCod_extraflete());
                st.setString(11, sj.getCodcli());
                st.setString(12, sj.getStd_job_no());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR SJEXTRAFLETE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public void deleteExtraflete() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_ELIMINAR_SJEXTRA);
                st.setString(1, sj.getCod_extraflete());
                st.setString(2, sj.getCodcli());
                st.setString(3, sj.getStd_job_no());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ELIMINAR SJEXTRAFLETE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public SJExtraflete obtenerSJExtraflete(String codcli, String std_job_no, String codextra) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        sj = new SJExtraflete();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_OBTENER_SJEXTRA);
                st.setString(1, codcli);
                st.setString(2, std_job_no);
                st.setString(3, codextra);
                rs = st.executeQuery();
                if(rs.next()){
                    sj.setCod_extraflete(rs.getString(1));
                    sj.setCodcli(rs.getString(2));
                    sj.setStd_job_no(rs.getString(3));
                    sj.setValor_costo(rs.getDouble(4));
                    sj.setMoneda_costo(rs.getString(5));
                    sj.setVf_costo(rs.getString(6));
                    sj.setReembolsable(rs.getString(7));
                    sj.setValor_ingreso(rs.getDouble(8));
                    sj.setMoneda_ingreso(rs.getString(9));
                    sj.setVf_ingreso(rs.getString(10));
                    sj.setClase_valor(rs.getString(11));
                    sj.setPorcentaje(rs.getFloat(12));
                    sj.setReg_status(rs.getString(13));
                    sj.setCreation_date(rs.getString(14));
                    sj.setLastUpdate(rs.getString(15));
                    sj.setCreation_user(rs.getString(16));
                    sj.setBase(rs.getString(17));
                }
            }
            return sj;
        }catch(SQLException e){
            throw new SQLException("ERROR AL OBTENER SJEXTRAFLETE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public Vector listarSJExtraflete(String var) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector sjFlete = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_LISTAR_SJEXTRA);
                st.setString(1, var+"%");
                st.setString(2, var+"%");
                //System.out.println(st);
                rs = st.executeQuery();
                while (rs.next()) {
                    sj = new SJExtraflete();
                    sj.setCod_extraflete(rs.getString(1));
                    sj.setCodcli(rs.getString(2));
                    sj.setStd_job_no(rs.getString(3));
                    sj.setValor_costo(rs.getDouble(4));
                    sj.setMoneda_costo(rs.getString(5));
                    sj.setVf_costo(rs.getString(6));
                    sj.setReembolsable(rs.getString(7));
                    sj.setValor_ingreso(rs.getDouble(8));
                    sj.setMoneda_ingreso(rs.getString(9));
                    sj.setVf_ingreso(rs.getString(10));
                    sj.setClase_valor(rs.getString(11));
                    sj.setPorcentaje(rs.getFloat(12));
                    sjFlete.addElement(sj);
                }
            }
            return sjFlete;
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR SJEXTRAFLETE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public boolean existeSJExtraflete(String cod, String std_job_no, String codextra) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean existe = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_OBTENER_SJEXTRA);
                st.setString(1, cod);
                st.setString(2, std_job_no);
                st.setString(3, codextra);
                rs = st.executeQuery();
                return rs.next();
                
            }
            return existe;
        }catch(SQLException e){
            throw new SQLException("ERROR AL CONSULTAR SJEXTRAFLETE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    public void listaFletes(String no_sj, String agencia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        fletes = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_EXTRAFLETES);
                st.setString(1,agencia);
                st.setString(2,no_sj);
                //System.out.println("Consulta Extraflete "+st);
                rs = st.executeQuery();
                while (rs.next()) {
                    sj = new SJExtraflete();
                    sj.setCod_extraflete(rs.getString("CODEXTRAFLETE"));
                    sj.setDescripcion(rs.getString("DESCRIPCION"));
                    sj.setAjuste(rs.getString("AJUSTE"));
                    sj.setUnidades(rs.getString("UNIDADES"));
                    sj.setCodcli(rs.getString("CODCLI"));
                    sj.setStd_job_no(rs.getString("STD_JOB_NO"));
                    sj.setValor_costo(rs.getFloat("VLR_COSTO"));
                    sj.setMoneda_costo(rs.getString("MONEDA_COSTO"));
                    sj.setVf_costo(rs.getString("VF_COSTO"));
                    sj.setReembolsable(rs.getString("REEMBOLSABLE"));
                    sj.setValor_ingreso(rs.getFloat("VLR_INGRESO"));
                    sj.setMoneda_ingreso(rs.getString("MONEDA_INGRESO"));
                    sj.setVf_ingreso(rs.getString("VF_INGRESO"));
                    sj.setClase_valor(rs.getString("PV"));
                    sj.setPorcentaje(rs.getFloat("PORCENTAJE"));
                    sj.setCantidad(1);
                    sj.setSelec(false);
                    sj.setConcepto(rs.getString("cod_descuento"));
                    sj.setTipo("E");
                    sj.setAgencia(agencia);
                    sj.setNit(rs.getString("nit"));
                    sj.setCuenta1(rs.getString("cuenta1"));
                    sj.setCuenta2(rs.getString("cuenta"));
                    
                    fletes.addElement(sj);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR FLETES" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    public void listaCostos(String no_sj, String agencia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        c_reembolsables= new Vector();
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_COSTOS);
                st.setString(1,agencia);
                st.setString(2,no_sj);
                //System.out.println("Consulta COSTOS "+st);
                rs = st.executeQuery();
                while (rs.next()) {
                    sj = new SJExtraflete();
                    sj.setCod_extraflete(rs.getString("CODEXTRAFLETE"));
                    sj.setDescripcion(rs.getString("DESCRIPCION"));
                    sj.setAjuste(rs.getString("AJUSTE"));
                    sj.setUnidades(rs.getString("UNIDADES"));
                    sj.setCodcli(rs.getString("CODCLI"));
                    sj.setStd_job_no(rs.getString("STD_JOB_NO"));
                    sj.setValor_costo(rs.getFloat("VLR_COSTO"));
                    sj.setMoneda_costo(rs.getString("MONEDA_COSTO"));
                    sj.setVf_costo(rs.getString("VF_COSTO"));
                    sj.setReembolsable(rs.getString("REEMBOLSABLE"));
                    sj.setValor_ingreso(rs.getFloat("VLR_INGRESO"));
                    sj.setMoneda_ingreso(rs.getString("MONEDA_INGRESO"));
                    sj.setVf_ingreso(rs.getString("VF_INGRESO"));
                    sj.setClase_valor(rs.getString("PV"));
                    sj.setPorcentaje(rs.getFloat("PORCENTAJE"));
                    sj.setCantidad(1);
                    sj.setSelec(false);
                    sj.setConcepto(rs.getString("cod_descuento"));
                    sj.setElemento(rs.getString("elemento"));
                    sj.setTipo("R");
                    sj.setAgencia(agencia);
                    sj.setNit(rs.getString("nit"));
                    sj.setCuenta1(rs.getString("cuenta1"));
                    sj.setCuenta2(rs.getString("cuenta"));
                    c_reembolsables.addElement(sj);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR COSTOS REEMBOLSABLES " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Getter for property fletes.
     * @return Value of property fletes.
     */
    public java.util.Vector getFletes() {
        return fletes;
    }
    
    /**
     * Setter for property fletes.
     * @param fletes New value of property fletes.
     */
    public void setFletes(java.util.Vector fletes) {
        this.fletes = fletes;
    }
    public boolean existeExtraflete( String codextra) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean existe = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("Select cod_extraflete from sjextraflete where cod_extraflete=? ");
                st.setString(1, codextra);
                rs = st.executeQuery();
                if(rs.next())
                    existe = true;
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL CONSULTAR SJEXTRAFLETE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return existe;
    }
    public void searchExtraflete( String sj_no,String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_EXTRAFLETE);
                st.setString(1, sj_no);
                st.setString(2, cod);
                rs = st.executeQuery();
                if(rs.next()){
                    sj = new SJExtraflete();
                    sj.setCod_extraflete(rs.getString("CODEXTRAFLETE"));
                    sj.setDescripcion(rs.getString("DESCRIPCION"));
                    sj.setAjuste(rs.getString("AJUSTE"));
                    sj.setUnidades(rs.getString("UNIDADES"));
                    sj.setCodcli(rs.getString("CODCLI"));
                    sj.setStd_job_no(rs.getString("STD_JOB_NO"));
                    sj.setValor_costo(rs.getFloat("VLR_COSTO"));
                    sj.setMoneda_costo(rs.getString("MONEDA_COSTO"));
                    sj.setVf_costo(rs.getString("VF_COSTO"));
                    sj.setReembolsable(rs.getString("REEMBOLSABLE"));
                    sj.setValor_ingreso(rs.getFloat("VLR_INGRESO"));
                    sj.setMoneda_ingreso(rs.getString("MONEDA_INGRESO"));
                    sj.setVf_ingreso(rs.getString("VF_INGRESO"));
                    sj.setClase_valor(rs.getString("PV"));
                    sj.setPorcentaje(rs.getFloat("PORCENTAJE"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL CONSULTAR SJEXTRAFLETE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Getter for property sj.
     * @return Value of property sj.
     */
    public com.tsp.operation.model.beans.SJExtraflete getSj() {
        return sj;
    }
    
    /**
     * Setter for property sj.
     * @param sj New value of property sj.
     */
    public void setSj(com.tsp.operation.model.beans.SJExtraflete sj) {
        this.sj = sj;
    }
    
    public void searchExtrafletePla( String planilla,String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        sj= null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_EXTRA_PLA);
                st.setString(1, planilla);
                st.setString(2, cod);
                rs = st.executeQuery();
                if(rs.next()){
                    sj = new SJExtraflete();
                    sj.setCod_extraflete(rs.getString("CODEXTRAFLETE"));
                    sj.setCantidad(rs.getFloat("cantidad"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL CONSULTAR SJEXTRAFLETES DE LAS PLANILLAS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public float valorTotalFlete( String planilla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        float total = 0;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_EXTRA_TOTAL);
                st.setString(1, planilla);
                rs = st.executeQuery();
                if(rs.next()){
                    total = rs.getFloat("total");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL SUMAR LOS FLETES " + e.getMessage());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return total;
    }
    
    /**
     * Getter for property c_reembolsables.
     * @return Value of property c_reembolsables.
     */
    public java.util.Vector getC_reembolsables() {
        return c_reembolsables;
    }
    
    /**
     * Setter for property c_reembolsables.
     * @param c_reembolsables New value of property c_reembolsables.
     */
    public void setC_reembolsables(java.util.Vector c_reembolsables) {
        this.c_reembolsables = c_reembolsables;
    }
    
    /*Karen*/
    public String insertarCostoReembolsable() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                //"INSERT INTO costo_reembolsables (dstrct,proveedor,numrem,numpla,codcli,std_job_no,element_gasto,vlr_costo,moneda_costo,fecdsp) VALUES(?,?,?,?,?,?,?,?,?,?)";
                st = con.prepareStatement(SQL_INSERTAR_COSTOREM);
                st.setString(1, sj.getCod_extraflete());
                st.setString(2, sj.getDistrito());
                st.setString(3, sj.getProveedor());
                st.setString(4, sj.getNumrem());
                st.setString(5, sj.getNumpla());
                st.setString(6, sj.getCodcli());
                st.setString(7, sj.getStd_job_no());
                st.setString(8, sj.getElemento());
                st.setDouble(9, sj.getValor_costo());
                st.setString(10, sj.getMoneda_costo());
                st.setString(11, sj.getFecdsp());
                st.setString(12, sj.getTipo());
                st.setDouble(13, sj.getValor_ingreso());
                st.setString(14, sj.getAprobador());
                st.setFloat(15, sj.getCantidad());
                st.setString(16,sj.getCreation_date()==null?""+new java.sql.Timestamp(System.currentTimeMillis()+11):sj.getCreation_date());
                st.setString(17,sj.getReembolsable());
                st.setString(18,sj.getAgencia());
                st.setString(19,sj.getCuenta1());
                st.setString(20,sj.getCuenta2());
                st.setString(21,sj.getNit());
                
                //st.executeUpdate();
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR SJEXTRAFLETE" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sql;
    }    
    /**
     * Metodo que anula los registros de extrafletes y costos reembolsables
     * relacionados a una planilla.
     * @param: Numero de la planilla
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public String anularCosto(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ANULAR);
                st.setString(1, numpla);
                //st.executeUpdate();
                sql = st.toString();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR COSTOS. " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sql;
    }
    /**
     * Metodo que lista los registros de extrafletes y costos reembolsables
     * relacionados a una planilla y remesa.
     * @param: Numero de la planilla
     * @param: Numero de la remesa
     * @param: Tipo
     * @param: Std job
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public void listaFletesAplicados(String numpla,String numrem,String tipo, String stdjob, String agencia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        if(tipo.equals("E")){
            fletes = new Vector();
        }
        else if (tipo.equals("R")){
            c_reembolsables=new  Vector();
        }
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_EXTRAFLETES_APLICADOS);
                st.setString(1,stdjob);
                st.setString(2,tipo);
                st.setString(3,agencia);
                st.setString(4,numpla);
                st.setString(5,numrem);
                //System.out.println("Consulta Extrafletes APLICADOS "+st);
                
                rs = st.executeQuery();
                while (rs.next()) {
                    sj = new SJExtraflete();
                    sj.setCod_extraflete(rs.getString("CODEXTRAFLETE"));
                    sj.setDescripcion(rs.getString("DESCRIPCION"));
                    sj.setAjuste(rs.getString("AJUSTE"));
                    sj.setUnidades(rs.getString("UNIDADES"));
                    sj.setCodcli(rs.getString("CODCLI"));
                    sj.setStd_job_no(rs.getString("STD_JOB_NO"));
                    sj.setValor_costo(rs.getFloat("VLR_COSTO"));
                    sj.setMoneda_costo(rs.getString("MONEDA_COSTO"));
                    sj.setVf_costo(rs.getString("VF_COSTO"));
                    sj.setReembolsable(rs.getString("REEMBOLSABLE"));
                    sj.setValor_ingreso(rs.getFloat("VLR_INGRESO"));
                    sj.setMoneda_ingreso(rs.getString("MONEDA_INGRESO"));
                    sj.setVf_ingreso(rs.getString("VF_INGRESO"));
                    sj.setClase_valor(rs.getString("PV"));
                    sj.setPorcentaje(rs.getFloat("PORCENTAJE"));
                    sj.setCantidad(rs.getFloat("cantidad"));
                    sj.setSelec(rs.getBoolean("utilizado"));
                    sj.setConcepto(rs.getString("cod_descuento"));
                    sj.setTipo(tipo);
                    sj.setProveedor(rs.getString("proveedor")==null?"":rs.getString("proveedor"));
                    sj.setAprobador(rs.getString("usuario_aprobacion"));
                    sj.setVlrtotal(rs.getFloat("VLR_COSTO")*rs.getFloat("cantidad"));
                    if(tipo.equals("E")){
                        if(rs.getBoolean("utilizado")){
                            this.vlrtotalE= vlrtotalE+(rs.getFloat("VLR_COSTO")*rs.getFloat("cantidad"));
                        }
                        fletes.addElement(sj);
                    }
                    else if (tipo.equals("R")){
                        c_reembolsables.addElement(sj);
                        if(rs.getBoolean("utilizado")){
                            this.vlrtotalCR= vlrtotalCR+(rs.getFloat("VLR_COSTO")*rs.getFloat("cantidad"));
                        }
                    }
                    sj.setAgencia(agencia);
                    sj.setNit(rs.getString("nit"));
                    sj.setCuenta1(rs.getString("cuenta1"));
                    sj.setCuenta2(rs.getString("cuenta"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR FLETES" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Getter for property vlrtotalE.
     * @return Value of property vlrtotalE.
     */
    public float getVlrtotalE() {
        return vlrtotalE;
    }
    
    /**
     * Setter for property vlrtotalE.
     * @param vlrtotalE New value of property vlrtotalE.
     */
    public void setVlrtotalE(float vlrtotalE) {
        this.vlrtotalE = vlrtotalE;
    }
    
    /**
     * Getter for property vlrtotalCR.
     * @return Value of property vlrtotalCR.
     */
    public float getVlrtotalCR() {
        return vlrtotalCR;
    }
    
    /**
     * Setter for property vlrtotalCR.
     * @param vlrtotalCR New value of property vlrtotalCR.
     */
    public void setVlrtotalCR(float vlrtotalCR) {
        this.vlrtotalCR = vlrtotalCR;
    }
    /**
     * Metodo que actualiza los registros de extrafletes y costos reembolsables
     * relacionados a una planilla y remesa.
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     *@return : Retorna el sql del update,
     */
    public String ActualizarAplicados() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_ACTUALIZAR_APLICADOS);
                st.setString(1, sj.getProveedor());
                st.setDouble(2, sj.getValor_costo());
                st.setDouble(3, sj.getValor_ingreso());
                st.setString(4, sj.getAprobador());
                st.setFloat(5, sj.getCantidad());
                st.setString(6, sj.getDistrito());
                st.setString(7, sj.getTipo());
                st.setString(8, sj.getCod_extraflete());
                st.setString(9, sj.getNumpla());
                st.setString(10, sj.getNumrem());
                //st.executeUpdate();
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR SJEXTRAFLETE APLICADO A LA PLANILLA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sql;
    }
    /**
     * Metodo que anula un registro dado en la tabla de costos_reembolsables
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public String anularCosto() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                // private static final String SQL_ANULAR_COSTO =    "UPDATE costo_reembolsables SET reg_status = 'A' WHERE dstrct=? and tipo_costo=? and  codigo=? and  numpla=? ";
                st = con.prepareStatement(SQL_ANULAR_COSTO);
                st.setString(1, sj.getDistrito());
                st.setString(2, sj.getTipo());
                st.setString(3, sj.getCod_extraflete());
                st.setString(4, sj.getNumpla());
                //st.executeUpdate();
                sql = st.toString();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR COSTOS. " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sql;
    }
    /**
     * Funcion que busca un costo reembolsable o un extraflete relacionado a una planilla
     * para saber si ha sido o no aplicada, retorna true o false
     * @return : true si encuentra el costo, false de lo contrario
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public boolean estaCostoAplicado() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select * from costo_reembolsables where dstrct=? and  tipo_costo=? and codigo=? and  numpla=? and numrem = ? and reg_status <>'A'");
                st.setString(1, sj.getDistrito());
                st.setString(2, sj.getTipo());
                st.setString(3, sj.getCod_extraflete());
                st.setString(4, sj.getNumpla());
                st.setString(5, sj.getNumrem());
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO COSTOS APLICADOS " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    /**
     * Metodo buscarExtrafletes, lista los exatrafletes dependiendo de una planilla, stdjob y dstrict,
     * @param: distrito, numpla, standar
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarExtrafletes(String dstrct, String numpla, String stdjob ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        fletes = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_BUSCAR_EXTRAFLETES);
                st.setString(1, dstrct);
                st.setString(2, numpla);
                st.setString(3, stdjob);
                //System.out.println("BUSCar Extraflete "+st);
                rs = st.executeQuery();
                while (rs.next()) {
                    sj = new SJExtraflete();
                    sj.setCod_extraflete(rs.getString("codigo"));
                    sj.setDescripcion(rs.getString("DESCRIPCION"));
                    sj.setValor_costo(rs.getFloat("VLR_COSTO"));
                    sj.setMoneda_costo(rs.getString("MONEDA_COSTO"));
                    sj.setValor_ingreso(rs.getFloat("VLR_INGRESO"));
                    sj.setCantidad(rs.getInt("cantidad"));
                    sj.setTipo(rs.getString("tipo_costo"));
                    fletes.addElement(sj);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR EXTRAFLETES" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }     
    
    /**
     * Metodo listaCostosPlanilla, lista todos los extrafletes y costos reembolsables
     * aplicados a una planilla.
     * @param: Numero de la planilla
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public void listaCostosPlanilla(String planilla, String tipo) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        c_reembolsables= new Vector();
        
        try{
            st = this.crearPreparedStatement("SQL_COSTOS_PLA");
            st.setString(1, tipo);
            st.setString(2,planilla);
            
            //System.out.println("Consulta COSTOS "+st);
            rs = st.executeQuery();
            while (rs.next()) {
                sj = new SJExtraflete();
                sj.setCod_extraflete(rs.getString("codigo"));
                sj.setValor_costo(rs.getFloat("VLR_COSTO"));
                sj.setNumpla(rs.getString("numpla"));
                sj.setNumrem(rs.getString("numrem"));
                sj.setDistrito(rs.getString("dstrct"));
                sj.setFecdsp(rs.getString("fecdsp"));
                sj.setCodcli(rs.getString("CODCLI"));
                sj.setProveedor(rs.getString("proveedor"));
                sj.setStd_job_no(rs.getString("std_job_no"));
                sj.setElemento(rs.getString("element_gasto"));
                sj.setMoneda_costo(rs.getString("moneda_costo"));
                sj.setTipo(rs.getString("tipo_costo"));
                sj.setValor_ingreso(rs.getFloat("vlr_ingreso"));
                //nuevos campos
                sj.setAgencia(rs.getString("agencia"));
                sj.setNit(rs.getString("nit"));
                sj.setCuenta1(rs.getString("cuenta1"));
                sj.setCuenta2(rs.getString("cuenta2"));
                sj.setReembolsable(rs.getString("reembolsable"));
                c_reembolsables.addElement(sj);
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR COSTOS DE LA PLANILLA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_COSTOS_PLA");
        }
    }
}