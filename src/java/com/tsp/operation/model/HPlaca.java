/*
 * HPlaca.java
 *
 * Created on 21 de diciembre de 2004, 09:16 AM
 */

package com.tsp.operation.model;
import java.io.*;
import java.util.*;
import com.tsp.operation.model.beans.*;


/**
 *
 * @author  mcelin
 */
public class HPlaca extends Thread{
    private Model  model;
    private String Fecha;
    
    /** Creates a new instance of HPlaca */
    public void start(Model model, String Fecha) {
        this.model = model;
        this.Fecha = Fecha;
        super.start();
    }
    
    public synchronized void run(){
        try {
            
            
            String          Ruta       = "/exportar/masivo/CORTES/MS8205_"+Fecha.replaceAll("-","")+".txt";
            String          Ruta600    = "/exportar/masivo/CORTES/M600.csv";
            String          Ruta200    = "/exportar/masivo/CORTES/M200.csv";
            
            /*String          Ruta       = "C:/MS230828_"+Fecha.replaceAll("-","")+".txt";
            String          Ruta600    = "C:/M600.csv";
            String          Ruta200    = "C:/M200.csv";*/
            
            int             PInicial   = 22;
            int             PFinal     = PInicial + 6;
            int             CInicial   = 43;
            int             CFinal     = CInicial + 8;
            long            Fila       = 1;
            
            FileInputStream fptr     = null;
            DataInputStream f        = null;
            String          Linea    = null;
            List            Placas   = new LinkedList();
            List            Placas2  = new LinkedList();
            
            
            fptr = new FileInputStream(Ruta);
            f    = new DataInputStream(fptr);
            do {
                Placa datos = new Placa();
                Linea = f.readLine();
                if(Linea!=null){
                    datos.setPlaca(Linea.substring(PInicial, PFinal));
                    datos.setConductor(Linea.substring(CInicial, CFinal));
                    Placas.add(datos);
                }
            }while(Linea!=null);
            fptr.close();
            
            //Recorro la lista Placa y saco las placas que no esten en oracle para adicionarlas
            //en una nueva lista llamada Placa2
            Iterator it = Placas.iterator();
            while(it.hasNext()) {
                Placa datos  = (Placa) it.next();
                if(!model.placaService.Existe_Placa_En_Oracle(datos.getPlaca()))
                    
                    Placas2.add(datos);
            }
            
            if(Placas2.size()>0) {
                //Busco en Postgres los Datos de las placas que se encuantran en la lista Placa2
                PrintWriter fp = new PrintWriter(new BufferedWriter(new FileWriter(Ruta600)));
                Linea = "";
                
                it = Placas2.iterator();
                while(it.hasNext()) {
                    Placa datos  = (Placa) it.next();
                    Placa Registro = model.placaService.Buscar_Placa_Postgres(datos.getPlaca());
                    
                    Linea =
                    datos.getPlaca()               +","+
                    "SS"             +","+
                    "EQUIPO CARBON"             +","+
                    "DS"                                                             +","+ //(Registro.getEstadoequipo()!=null?Registro.getEstadoequipo():"") +","+
                    "SS0335"     +","+
                    "SM" +","+
                    "C99999981"                                                      +","+
                    (Registro.getTenedor()!=null?Registro.getTenedor():"")           +","+
                    (datos.getConductor()!=null?datos.getConductor():"")             +","+
                    (Registro.getSerial()!=null?Registro.getSerial():"")             +","+
                    (Registro.getTara()!=null?Registro.getTara():"")                 +","+
                    (Registro.getVolumen()!=null?Registro.getVolumen():"")           +","+
                    (Registro.getMarca()!=null?Registro.getMarca():"")               +","+
                    (Registro.getModelo()!=null?Registro.getModelo():"")             +","+
                    (Registro.getLargo()!=null?Registro.getLargo():"")               +","+
                    (Registro.getAlto()!=null?Registro.getAlto():"")                 +","+
                    (Registro.getLlantas()!=null?Registro.getLlantas():"")           +","+
                    (Registro.getEnganche()!=null?Registro.getEnganche():"")         +","+
                    " "                                                              +","+
                    " "                                                              +","+
                    "30"                                                             +","+
                    (Registro.getColor()!=null?Registro.getColor():"")               +","+
                    (Registro.getNoejes()!=null?Registro.getNoejes():"")             +","+
                    "FV"                                                             +","+
                    (Registro.getAncho()!=null?Registro.getAncho():"")               +","+
                    (Registro.getEmpresaafil()!=null?Registro.getEmpresaafil():"")   +","+
                    (Registro.getPiso()!=null?Registro.getPiso():"")                 +","+
                    (Registro.getCargue()!=null?Registro.getCargue():"")             +","+
                    " "                                                              +","+
                    " "                                                              +","+
                    "MOTOR"                                                          +","+
                    (Registro.getNomotor()!=null?Registro.getNomotor():"")           +","+
                    "CA"                                                             +",";
                    fp.println(Linea);
                }
                fp.close();
                
                
                /****************************************************************************/
                
                //Busco en Postgres los Datos de las placas que se encuantran en la lista Placa2
                fp = new PrintWriter(new BufferedWriter(new FileWriter(Ruta200)));
                Linea = "";
                
                it = Placas2.iterator();
                while(it.hasNext()) {
                    Placa datos  = (Placa) it.next();
                    Placa datos2 = model.placaService.Buscar_Pro_Con(datos.getPlaca());
                    ProConBen Registro = model.placaService.Buscar_Pro_Con_Ben_Ban(datos.getPlaca(), datos2.getPropietario(), datos.getConductor());
                    
                    Linea = (datos.getPlaca()!=null?datos.getPlaca():"")                     +","+
                    (Registro.getPais()!=null?Registro.getPais():"")                 +","+
                    (Registro.getNombreP()!=null?Registro.getNombreP():"")           +","+
                    (Registro.getDireccionP()!=null?Registro.getDireccionP():"")     +","+
                    " "                                                              +","+
                    (Registro.getNombreC()!=null?Registro.getNombreC():"")           +","+
                    (Registro.getDireccionC()!=null?Registro.getDireccionC():"")     +","+
                    " "                                                              +","+
                    (Registro.getTelefono()!=null?Registro.getTelefono():"")         +","+
                    (Registro.getBeneficiario()!=null?Registro.getBeneficiario():"") +","+
                    " "                                                              +","+
                    " "                                                              +","+
                    "PL"                                                             +","+
                    "FC"                                                             +","+
                    " "                                                              +","+
                    " "                                                              +","+
                    (Registro.getBanco()!=null?Registro.getBanco():"")               +","+
                    (Registro.getCuenta()!=null?Registro.getCuenta():"")             +","+
                    (Registro.getMoneda()!=null?Registro.getMoneda():"")             +",";
                    fp.println(Linea);
                }
                fp.close();
            }
            
        }
        catch(Exception e){
        }
    }
}
