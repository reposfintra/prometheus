/*
 * RemiDestDAO.java
 *
 * Created on 9 de diciembre de 2004, 09:58 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.RemiDest;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  KREALES
 */
public class RemiDestDAO extends MainDAO {

    private List remitentes;
    private List destinatarios;
    private RemiDest rd;
    private TreeMap ciudades;
    private Vector vecRd;
    
    private String BUSCAR_CIUDAD = "SELECT 	" +
    "           distinct substring(codigo,5,2)as ciudad, " +
    "           c.nomciu " +
    " FROM 	remidest, " +
    "           ciudad c " +
    " WHERE      codigo like ? AND " +
    "           substring(codigo,4,1)='D' AND" +
    "           c.codciu = substring(codigo,5,2)";
    
     private String BUSCAR_CIUDAD_R = "SELECT 	" +
    "           distinct substring(codigo,5,2)as ciudad, " +
    "           c.nomciu " +
    " FROM 	remidest, " +
    "           ciudad c " +
    " WHERE      codigo like ? AND " +
    "           substring(codigo,4,1)='R' AND" +
    "           c.codciu = substring(codigo,5,2)" +
    "           and c.codciu = ?";

    private static final String INSERTAR_REM_DEST =
    " Insert into remidest " +
    " (estado,codigo,nombre,tipo," +
    "  codcli,creation_date,last_update," +
    "  direccion,ciudad,base," +
    "  creation_user,dstrct,referencia,nombre_contacto,telefono) VALUES" +
    " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    private String BUSCAR_DESTINATARIO="SELECT 	* " +
    " FROM 	remidest" +
    " WHERE 	codigo like ? AND " +
    "           substring(codigo,4,1)='D' AND" +
    "           substring(codigo,5,2)=?" +
    "           and upper(nombre) like ?" +
    "           order by nombre";
    
    
    private String BUSCAR_REMITENTE="SELECT 	* " +
    " FROM 	remidest" +
    " WHERE 	codigo like ? AND " +
    "           substring(codigo,4,1)='R' AND" +
    "           substring(codigo,5,2)=?";
    
    private static final String UPDATE_REMI_DEST = 
    " Update remidest " +
    " set estado=?, nombre=?, direccion=?," +
    "     user_update=?, reg_status=?, last_update='now()', referencia = ?, telefono =?, nombre_contacto=? " +
    " where codigo=? ";
    
    private static final String BUSCAR_REM_DEST = 
    "Select * from remidest where codigo=?";
    
        private String LISTAR_REMESADEST="SELECT 	rd.*  " +
    "     FROM 	remesadest rm" +
    "	inner join remidest rd on (rd.codigo = rm.codigo) " +
    "     WHERE 	rm.numrem = ?" +
    "		and substring(rm.codigo,4,1)=? " +
    "     order by nombre";
    
   private String SEARCH_REMIDEST="SELECT 	rd.*  " +
    "     FROM remidest rd " +
    "     WHERE RD.codigo  =? ";
    
    public TreeMap getCiudades(){
        
        return ciudades;
    }
    /** Creates a new instance of RemiDestDAO */
     public RemiDestDAO() {
          super("RemiDestDAO.xml");
    }
    public List getListR(){
        return remitentes;
    }
    public List getListD(){
        return destinatarios;
    }
    
    public void searchCiudades(String codigo)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ciudades = null;
        ciudades = new TreeMap();
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(BUSCAR_CIUDAD);
                st.setString(1,codigo+"%");
                rs = st.executeQuery();
                ciudades.put("Seleccione una ciudad","");
                while(rs.next()){
                    ciudades.put(rs.getString("nomciu"), rs.getString("ciudad"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CIUDADES DE LOS DESTINATARIOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public void searchCiudadesr(String codigo, String origen)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ciudades = null;
        ciudades = new TreeMap();
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(BUSCAR_CIUDAD_R);
                st.setString(1,codigo+"%");
                st.setString(2,origen);
                rs = st.executeQuery();
               // ciudades.put("Seleccione una ciudad","");
                while(rs.next()){
                    ciudades.put(rs.getString("nomciu"), rs.getString("ciudad"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CIUDADES DE LOS DESTINATARIOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void searchListaR(String codcli )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        remitentes = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select * from remidest where substring(codigo,1,3)like ? and tipo='RE'");
                st.setString(1,codcli+"%");
                
                rs = st.executeQuery();
                remitentes = new LinkedList();
                
                while(rs.next()){
                    remitentes.add(RemiDest.load(rs));
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REMITENTES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public void searchListaR(String codcli, String ciudad)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        remitentes = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(BUSCAR_REMITENTE);
                st.setString(1,codcli+"%");
                st.setString(2, ciudad);
                
                rs = st.executeQuery();
                remitentes = new LinkedList();
                
                while(rs.next()){
                    remitentes.add(RemiDest.load(rs));
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REMITENTES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
     public void searchListaD(String codcli )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        destinatarios = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select * from remidest where substring(codigo,1,3)like ? and tipo='DE' order by nombre");
                st.setString(1,codcli+"%");
                
                rs = st.executeQuery();
                destinatarios = new LinkedList();
                
                while(rs.next()){
                    destinatarios.add(RemiDest.load(rs));
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REMITENTES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
  
    
    
    public String searchNombre(String codcli )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        destinatarios = null;
        String nombre="No se econtro";
        PoolManager poolManager = null;

        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select nombre from remidest where codigo=?");
                st.setString(1,codcli);
                rs = st.executeQuery();
                if(rs.next()){
                    nombre = rs.getString("nombre");
                }
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REMITENTES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
            
        }
        return nombre;
    }
    /**
     * Metodo insertarRemitenteDestinatario, recibe el objeto Remitente Destinatario
     * y lo ingresa a la base de datos
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : RemiDest RemitenteDestinatario
     * @version : 1.0
     */
    public void insertarRemitenteDestinatario(RemiDest remdest)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;       
        PoolManager poolManager = null;        
        try {            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
               st = con.prepareStatement(INSERTAR_REM_DEST);  
               
               st.setString(1, remdest.getEstado());
               st.setString(2, remdest.getCodigo());
               st.setString(3, remdest.getNombre());
               st.setString(4, remdest.getTipo());
               st.setString(5, remdest.getCodcli());
               st.setString(6, "now()");
               st.setString(7, "now()");
               st.setString(8, remdest.getDireccion());
               st.setString(9, remdest.getCiudad());
               st.setString(10, remdest.getBase());
               st.setString(11, remdest.getCration_user());
               st.setString(12, remdest.getDstrct());
               st.setString(13, remdest.getReferencia());
               st.setString(14, remdest.getNombre_contacto());
               st.setString(15, remdest.getTelefono());
               ////System.out.println(st);
               st.execute();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL REGISTRO REMDEST " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
            
        }        
    }
    /**
     * Metodo existeRemitenteDestinatario, determina si un codigo de remtiente o destinatario 
     * existe o no en la base de datos
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : Strimg codigo
     * @return : true si el codigo existe � false si no existe
     * @version : 1.0
     */
    public boolean existeRemitenteDestinatario(String codigo)throws SQLException{        
        Connection con=null;
        PreparedStatement st = null;       
        PoolManager poolManager = null;  
        ResultSet rs = null;
        try {            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
               st = con.prepareStatement("select codigo from remidest where codigo=?");
               st.setString(1, codigo);
               rs = st.executeQuery();
               return rs.next();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REMITENTES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
            
        }        
    }    
    /**
     * Metodo updateRemitenteDestinatario, recibe el objeto Remitente Destinatario 
     * y actualiza el registro con los nuevos valored del objeto en la BD
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : RemiDest RemitenteDestinatario
     * @version : 1.0
     */
    public void updateRemitenteDestinatario(RemiDest remdest)throws SQLException{        
        Connection con=null;
        PreparedStatement st = null;       
        PoolManager poolManager = null;        
        try {            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
               st = con.prepareStatement(UPDATE_REMI_DEST);   
               st.setString(1, remdest.getEstado());               
               st.setString(2, remdest.getNombre());               
               st.setString(3, remdest.getDireccion());               
               st.setString(4, remdest.getUser_update());               
               st.setString(5, remdest.getReg_status());
               st.setString(6, remdest.getReferencia());
               st.setString(7, remdest.getTelefono());
               st.setString(8, remdest.getNombre_contacto());
               st.setString(9, remdest.getCodigo());
               
               ////System.out.println("QUERY UPDT: "+st);
               st.execute();
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL REGISTRO REMDEST " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
            
        }        
    }    
    /**
     * Metodo listarRemitentesDestinatarios, busca los remitentes y los destinatarios
     * que cumplan con el criterio de busqueda
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String codigo
     * @version : 1.0
     */
    public void listarRemitentesDestinatarios(String codigo)throws SQLException{        
        Connection con=null;
        PreparedStatement st = null;       
        PoolManager poolManager = null;        
        ResultSet rs = null;
        rd = null;
        vecRd = new Vector();        
        try {            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con!=null){
               st = con.prepareStatement("Select " +
                                         "     R.codigo," +
                                         "     R.tipo," +
                                         "     R.codcli," +
                                         "     C.nomcli," +
                                         "     R.estado," +
                                         "     R.nombre," +
                                         "     R.direccion, " +
                                         "     B.nomciu as ciudad, " +
                                         "     R.referencia, " +
                                         "     R.telefono, " +
                                         "     R.nombre_contacto " +
                                         "from " +
                                         "     remidest R, Cliente C, ciudad B " +
                                         "where " +
                                         "     codigo like ? and " +
                                         "     R.estado='A' and" +
                                         "     R.codcli = C.codcli and" +
                                         "     R.ciudad = B.codciu order by C.nomcli,R.nombre");   
               st.setString(1, codigo+"%");
               
               rs = st.executeQuery();
               while (rs.next()) {
                   rd = new RemiDest();                                      
                   rd.setCodigo(rs.getString("codigo"));
                   rd.setDireccion(rs.getString("direccion"));
                   rd.setCodcli(rs.getString("codcli"));
                   rd.setCiudad(rs.getString("ciudad"));                   
                   rd.setNombre(rs.getString("nombre"));
                   rd.setTipo(rs.getString("tipo"));  
                   rd.setNomcli(rs.getString("nomcli"));
                   rd.setEstado(rs.getString("estado"));
                   rd.setReferencia(rs.getString("referencia"));
                   rd.setTelefono(rs.getString("telefono"));
                   rd.setNombre_contacto(rs.getString("nombre_contacto"));
                   vecRd.addElement(rd);
               }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL REGISTRO REMDEST " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }

        }
    }

    /**
     * Getter for property vecRd.
     * @return Value of property vecRd.
     */
    public java.util.Vector getVecRd() {
        return vecRd;
    }
    
     // LOS SIGUIENTES M�TODOS Y/O ATRIBUTOS FUERON COPIADOS DE LA APLICACI�N SOT
    // PARA IMPLEMENTAR EL MODULO DE ADMINISTRACI�N DE USUARIOS.
    // Alejandro Payares - 13.12.05 - 10:50 am
    
    private TreeMap tDestinatarios;
    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RemiDestDAO.class);
    
    
    
    /**
     * Crea una lista de objetos de destinatarios.
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     */
    public void destinatarioSearch() throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        destinatarios = null;
        try {
            // Buscar los clientes y meterlos en una lista
            pstmt = this.crearPreparedStatement("SQL_DESTINATARIO_SEARCH");
            //pstmt.setString(1, placaID );
            rs = pstmt.executeQuery();
            destinatarios = new LinkedList();
            while (rs.next()) {
                destinatarios.add(RemiDest.load(rs));
            }
        } finally {
            if (rs != null) rs.close();
            if ( pstmt != null ) pstmt.close();
            this.desconectar("SQL_DESTINATARIO_SEARCH");
        }
    }
    
    /**
     * Crea una lista de objetos de destinatarios asociados a un determinado
     * cliente.
     * @param codCliente El c&oacute;digo del cliente.
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     * @autor Alejandro Payares.
     */
    public void destinatarioSearch(String codCliente)throws SQLException{
        destinatarioSearch(codCliente,false);
    }
    
    /**
     * Crea una lista o TreeMap de objetos de destinatarios asociados a un determinado
     * cliente.
     * @param codCliente El c&oacute;digo del cliente.
     * @param guardarEnTreeMap bandera para saber si se quieren guardar los detinatarios en un TreeMap o no
     * si la bandera es falsa se guardan en una lista.
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     * @autor Alejandro Payares.
     */
    public void destinatarioSearch(String codCliente, boolean guardarEnTreeMap) throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        destinatarios = null;
        try {
            pstmt = this.crearPreparedStatement("SQL_OBTENER_DESTINATARIOS_CLIENTE");
            pstmt.setString(1, codCliente );
            rs = pstmt.executeQuery();
            if( rs.next() ){
                if ( guardarEnTreeMap ) {
                    tDestinatarios = new TreeMap();
                }
                else {
                    destinatarios = new LinkedList();
                }
                do {
                    if ( guardarEnTreeMap ) {
                        tDestinatarios.put(rs.getString("nombre"), rs.getString("codigodest"));
                    }
                    else {
                        destinatarios.add(RemiDest.load(rs));
                    }
                }
                while( rs.next() );
            }            
        }
        catch( Exception ex ){
            logger.error("ERROR BUSCANDO DESTINATARIOS: "+ex.getMessage()+"\n"+this.getStackTrace(ex));
            throw new SQLException("ERROR BUSCANDO DESTINATARIOS: "+ex.getMessage());
        }
        finally {
            if (rs != null) rs.close();
            if ( pstmt != null ) pstmt.close();
            this.desconectar("SQL_OBTENER_DESTINATARIOS_CLIENTE");
        }
    }
    
    public TreeMap getTgetDestinatarios(){
        return tDestinatarios;
    }
    
    
    /**
     * Devuelve los resultados de busqueda de destinatarios.
     * @return Lista de destinatarios
     */
    public List getDestinatarios() {
        return destinatarios;
    }
     /**
     * Setter for property vecRd.
     * 
     */
    public void setVecRd(Vector v) {
        vecRd=v;
    }
     /**
     * Metodo searchListaD, busca un vector de destinatarios 
     * dado el cliente y una ciudad
     * @autor : Ing. Karen Reales
     * @param : String codclo, ciudad
     * @version : 1.0
     */
    public void searchListaD(String codcli,String ciudad, String nombre )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        destinatarios = null;
        PoolManager poolManager = null;
        vecRd = new Vector();
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(BUSCAR_DESTINATARIO);
                st.setString(1,codcli+"%");
                st.setString(2, ciudad);
                st.setString(3, "%"+nombre.toUpperCase()+"%");
                ////System.out.println("query: "+st.toString());
                rs = st.executeQuery();
                while(rs.next()){
                    RemiDest rd = RemiDest.load(rs);
                    rd.setDireccion(rs.getString("direccion"));
                    vecRd.addElement(rd);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS DESTINATARIOS DE UNA CIUDAD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    /**
     * Metodo listaDestinatariosPla, busca un vector de destinatarios 
     * dado el cliente y una ciudad
     * @autor : Ing. Karen Reales
     * @param : String codclo, ciudad
     * @version : 1.0
     */
    public void listaDestinatariosPla(String numrem )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        destinatarios = null;
        PoolManager poolManager = null;
        vecRd = new Vector();
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(LISTAR_REMESADEST);
                st.setString(1,numrem);
                st.setString(2, "D");
                ////System.out.println("SQL remidest "+st.toString());
                rs = st.executeQuery();
                while(rs.next()){
                    RemiDest rd = RemiDest.load(rs);
                    rd.setDireccion(rs.getString("direccion"));
                    vecRd.addElement(rd);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS DESTINATARIOS DE UNA CIUDAD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
     /**
     * Metodo searchRemiDest, busca los datos de un destinatario especifico
     * @autor : Ing. Karen Reales
     * @param : String codigo del destinatario
     * @version : 1.0
     */
    public void searchRemiDest(String codigo )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SEARCH_REMIDEST);
                st.setString(1,codigo);
                ////System.out.println("SQL remidest "+st.toString());
                rs = st.executeQuery();
                while(rs.next()){
                    rd = RemiDest.load(rs);
                    rd.setDireccion(rs.getString("direccion"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN DESTINATARIO ESPECIFICO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
        
    /**
     * Getter for property rd.
     * @return Value of property rd.
     */
    public com.tsp.operation.model.beans.RemiDest getRd() {
        return rd;
    }
    
    /**
     * Setter for property rd.
     * @param rd New value of property rd.
     */
    public void setRd(com.tsp.operation.model.beans.RemiDest rd) {
        this.rd = rd;
    }
    
}
