/*
 * PlaremDAO.java
 *
 * Created on 20 de junio de 2005, 09:55 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  Jm
 */
public class PlaremDAO {
    Plarem plrem;
    Vector plr;
    private static final String QUERY_TENER_DATOS_PLAREM  =" select NUMPLA,       " +
    "NUMREM,        " +
    "porcent,        " +
    "account_code_i AS paralaremesa,        " +
    "account_code_c AS paralaplanilla,        " +
    "porcent_contabilizacion AS porcentajecontabilizacion " +
    "from PLAREM " +
    "where NUMPLA = ? " +
    "and numrem = ? ";
    /** Creates a new instance of PlaremDAO */
    public PlaremDAO() {
    }
    
    /**
     * Getter for property plr.
     * @return Value of property plr.
     */
    public java.util.Vector getPlr() {
        return plr;
    }
    
    /**
     * Setter for property plr.
     * @param plr New value of property plr.
     */
    public void setPlr(java.util.Vector plr) {
        this.plr = plr;
    }
    
    /**
     * Getter for property plrem.
     * @return Value of property plrem.
     */
    public Plarem getPlrem() {
        return plrem;
    }
    
    /**
     * Setter for property plrem.
     * @param plrem New value of property plrem.
     */
    public void setPlrem(Plarem plrem) {
        this.plrem = plrem;
    }
     public void list( String numpla )throws SQLException{
        String consulta = "select NUMPLA, NUMREM from PLAREM where NUMPLA = ?" ;
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, numpla);
                rs= st.executeQuery();
                while(rs.next()){
                    plrem = new Plarem();
                    plrem.setNumpla(rs.getString("numpla"));
                    plrem.setNumrem(rs.getString("numrem"));
                
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR PLAREM " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }

    } 
     
    public Plarem obtenerDatosPlarem(String numpla, String numrem)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        plrem = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(QUERY_TENER_DATOS_PLAREM);
                st.setString(1, numpla);
                st.setString(2, numrem);
                rs= st.executeQuery();
               
                if(rs.next()){
                    plrem = new Plarem();
                    plrem.setNumpla(rs.getString("numpla"));
                    
                    plrem.setNumrem(rs.getString("numrem"));
                    
                    plrem.setPorcent(rs.getString("porcent"));
                    
                    plrem.setParalaremesa(rs.getString("paralaremesa"));
                    
                    plrem.setParalaplanilla(rs.getString("paralaplanilla"));
                    
                    plrem.setPorcentaje_contabilizacion(rs.getString("porcentajecontabilizacion"));
                    
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL MOSTRAR LO DATOS DE PLAREM " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return plrem;
    } 
     
    
     
     public String account_code_c( String numpla )throws SQLException{
        String consulta = "SELECT account_code_c FROM plarem WHERE numpla = ?" ;
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, numpla);
                rs= st.executeQuery();
                if ( rs.next() ){
                    sql = rs.getString("account_code_c");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR PLAREM " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    } 
}
