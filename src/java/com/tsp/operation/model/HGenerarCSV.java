/*
 * HGenerarCSV.java
 *
 * Created on 22 de julio de 2005, 11:38 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import java.util.Vector;
import java.io.*;

/**
 *
 * @author  Tito Andr�s Maturana
 */
public class HGenerarCSV extends Thread{
    private Vector lineas;
    
    private FileWriter fw;
    private BufferedWriter bffw;
    private PrintWriter pntw;
    private String ruta;
    private String archivo;
    
    /** Creates a new instance of HGenerarCSV */
    public HGenerarCSV() {
    }
    
    /**
     * Getter for property archivo.
     * @return Value of property archivo.
     */
    public java.lang.String getArchivo() {
        return archivo;
    }
    
    /**
     * Setter for property archivo.
     * @param archivo New value of property archivo.
     */
    public void setArchivo(java.lang.String archivo) {
        this.archivo = archivo;
    }
    
    /**
     * Getter for property bffw.
     * @return Value of property bffw.
     */
    public java.io.BufferedWriter getBffw() {
        return bffw;
    }
    
    /**
     * Setter for property bffw.
     * @param bffw New value of property bffw.
     */
    public void setBffw(java.io.BufferedWriter bffw) {
        this.bffw = bffw;
    }
    
    /**
     * Getter for property fw.
     * @return Value of property fw.
     */
    public java.io.FileWriter getFw() {
        return fw;
    }
    
    /**
     * Setter for property fw.
     * @param fw New value of property fw.
     */
    public void setFw(java.io.FileWriter fw) {
        this.fw = fw;
    }
    
    /**
     * Getter for property lineas.
     * @return Value of property lineas.
     */
    public java.util.Vector getLineas() {
        return lineas;
    }
    
    /**
     * Setter for property lineas.
     * @param lineas New value of property lineas.
     */
    public void setLineas(java.util.Vector lineas) {
        this.lineas = lineas;
    }
    
    /**
     * Getter for property pntw.
     * @return Value of property pntw.
     */
    public java.io.PrintWriter getPntw() {
        return pntw;
    }
    
    /**
     * Setter for property pntw.
     * @param pntw New value of property pntw.
     */
    public void setPntw(java.io.PrintWriter pntw) {
        this.pntw = pntw;
    }
    
    /**
     * Getter for property ruta.
     * @return Value of property ruta.
     */
    public java.lang.String getRuta() {
        return ruta;
    }
    
    /**
     * Setter for property ruta.
     * @param ruta New value of property ruta.
     */
    public void setRuta(java.lang.String ruta) {
        this.ruta = ruta;
    }
    
    public void start(Vector lineas, String ruta, String archivo) {
        this.lineas = lineas;
        this.ruta = ruta;
        this.archivo = archivo;
        
        try{
           /* String nombre_archivo = "";
            String ruta = "c:/exportar/migracion/";
            String fecha_actual = Util.getFechaActual_String(6);
            
            ruta += fecha_actual.substring(0,4) + fecha_actual.substring(5,7);
            nombre_archivo = ruta + "/anula620 " + fecha_actual.replace(':','_').replace('/','_') + ".csv";
            */
            File archiv = new File(ruta.trim());
            ////System.out.println("RUTA " + archiv); 
            archiv.mkdirs();
            ////System.out.println("ARCHIVO " + archivo);
            FileOutputStream file = new FileOutputStream(archivo.trim());
            
            fw = new FileWriter(archivo);
            bffw = new BufferedWriter(fw);
            pntw = new PrintWriter(bffw);
        }
        catch(java.io.IOException e){
            ////System.out.println(e.toString());
        }
        
        super.start();
    }
    
    public synchronized void run(){
       this.writeFile();
    }
    
    protected void writeFile() {    	
    	String linea = "";          
    	    	
        for(int i=0; i<this.lineas.size(); i++){            
            linea = (String) this.lineas.elementAt(i);
            ////System.out.println("linea " + i + " : " + linea);
            pntw.println(linea);
        }

        pntw.close();
        	    	    	
    }
    
}
