/*
 * ViaService.java
 *
 * Created on 9 de diciembre de 2004, 04:19 PM
 */

package com.tsp.operation.model;
import java.util.*;
import java.sql.*;

/**
 *
 * @author  ARMANDO
 */
public class ViaService {
    
    private ViaDAO viaDataAccess;
    private Vector vecVias;
    /** Creates a new instance of ViaService */
    public ViaService() {
        viaDataAccess = new ViaDAO();
    }
    
    public void getViasRel(String codvia)throws SQLException{
        viaDataAccess.getViasRel(codvia);
    }
    
    public void getViasRelPg(String codvia)throws SQLException{
        viaDataAccess.getViasRelPg(codvia);
    }
    
    public TreeMap getCbxVias(){
        return viaDataAccess.getCbxVias();
    }
    
    public String getDescripcion(String via) throws SQLException{
        return viaDataAccess.getDescripcion(via);
    }
    
    public void getVias() throws SQLException{
        viaDataAccess.getVias();
    }
    
    public void getViasPg() throws SQLException{
        viaDataAccess.getViasPg();
    }
    public Vector obtenerPCVia(String via) throws SQLException{
        this.getViasXRutaFrecuencia(via);
        return viaDataAccess.getVecVias();
    }
    /**
     * M�todo que retorna la ruta de una via
     * @autor.......kreales
     * @param.......el codigo de la via
     * @throws......SQLException
     * @version.....1.0.
     * @return.......String de la ruta
     **/
    public String getRuta(String via) throws SQLException{
        return viaDataAccess.getRuta(via);
    }
    /**
     * M�todo que retorna la ruta de una via
     * @autor.......Jose de la rosa
     * @param.......el origen, destino
     * @throws......SQLException
     * @version.....1.0.
     * @return
     **/
    public void getViasTreeMaps(String origen, String destino) throws SQLException{
        viaDataAccess.getViasTreeMaps(origen, destino);
    }
    /**
     * M�todo que buscar y llena un TreeMap con las ciudades que comprenden una via
     * @autor:     Henry A. Osorio Gonzalez
     * @param:     String origen.  Origen de la Ruta
     * @param:     String destino. Destino de la Ruta
     * @param:     String freq.    Frecuencia de la Ruta
     * @throws:    SQLException
     * @version:   1.0.
     **/
    public void getViasXRutaFrecuencia(String via) throws SQLException {
        viaDataAccess.getViasXRutaFrecuencia(via);
    }
    
    public Vector getVecVias() {        
        return viaDataAccess.getVecVias();
    }
   
    
    /**
     * Obtiene todas las vias donde el c�digo de la ciudad se encuentra registrado en el campo via.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param codciu C�digo de la ciudad
     * @throws SQLException
     * @version 1.0
     */
    public void viasCiudad(String codciu) throws SQLException{
        this.viaDataAccess.viasCiudad(codciu);
    }
    
    /**
     * Actualiza el tiempo y la via
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param ori C�digo de la ciudad origen
     * @param des C�digo de la ciudad destino
     * @param sec C�digo de la secuencia
     * @param via Via
     * @param tiempo Tiempo
     * @throws SQLException
     * @version 1.0
     */  
    public void updateVia(String ori, String des, String sec, String via, float tiempo) throws SQLException{
        this.viaDataAccess.updateVia(ori, des, sec, via, tiempo);
    }
    
    /**
     * Actualiza la m�xima duraci�n
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param ori C�digo de la ciudad origen
     * @param des C�digo de la ciudad destino
     * @param sec C�digo de la secuencia
     * @param dur Duracion
     * @throws SQLException
     * @version 1.0
     */
    public void updateDuracion(String ori, String des, String sec, float dur) throws SQLException{
        this.viaDataAccess.updateDuracion(ori, des, sec, dur);
    }
    
    /**
     * Obtiene todas las v�as con el mismo origen y destino
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param ori C�digo de la ciudad origen
     * @param des C�digo de la ciudad destino
     * @throws SQLException
     * @version 1.0
     */
    public void obtenerVias(String ori, String dest) throws SQLException{
        this.viaDataAccess.obtenerVias(ori, dest);
    }
    
    /**
     * Metodo <tt>listarDestinos</tt>, obtiene los destinos de las v�as
     *  dado un origen
     * @autor Ing. Andr�s Maturana
     * @param origen (String)
     * @version : 1.0
     */
    public void listarDestinos(String orig)throws SQLException{
        this.viaDataAccess.listarDestinos(orig);
    }
    
    /**
     * Setter for property cbxVias
     * @autor Ing. Andr�s Maturana
     * @param origen (String)
     * @version : 1.0
     */
    public void setCbxVias(TreeMap cbxVias){
        this.viaDataAccess.setCbxVias(cbxVias);
    }

    
}