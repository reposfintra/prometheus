/*
 * CiudadDAO.java
 *
 * Created on 1 de diciembre de 2004, 02:58 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  KREALES
 */
public class UnidadDAO {
       private Unidad unidad;
    private Vector unidadess;
    /** Creates a new instance of CiudadDAO */
    public UnidadDAO() {
    }
    //private List ciudades;
    private TreeMap unidades;
    
    public TreeMap getUnidades(){
        
        return unidades;
    }
    
    public void searchUnidades()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        unidades = null;
        unidades = new TreeMap();
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select * from unidademp order by descripcion");
                rs = st.executeQuery();
                unidades.put("NINGUNA", "");
                while(rs.next()){
                    unidades.put(rs.getString("descripcion"), rs.getString("codigo"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS UNIDADES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public boolean existCiudades()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from ciudad ");
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROVEEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
     //Jose
    public void listUnidad()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                //st = con.prepareStatement("select * from unidad where rec_status != 'A' order by cod_unidad");
                 st = con.prepareStatement("SELECT table_code AS cod_unidad, " +
                                          "       descripcion AS desc_unidad, " +
                                          "       referencia AS tipo " +
                                          "FROM   tablagen " +
                                          "WHERE  table_type = 'TUNIDAD'" +                                          
                                          "       AND reg_status != 'A'");
                rs= st.executeQuery();
                unidadess = new Vector();
                while(rs.next()){
                    unidad = new Unidad();
                    unidad.setCodigo(rs.getString("cod_unidad"));
                    unidad.setDescripcion(rs.getString("desc_unidad"));
                    unidad.setTipo(rs.getString("tipo"));
                    unidadess.add(unidad);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA UNIDAD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }

    public Vector listarUnidades ()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        unidades = null;
        PoolManager poolManager = null;
        
        try {            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                //st = con.prepareStatement("select * from unidad where rec_status!='A' order by cod_unidad");
                st = con.prepareStatement("SELECT table_code AS cod_unidad, " +
                                          "       descripcion AS desc_unidad, " +
                                          "       referencia AS tipo " +
                                          "FROM   tablagen " +
                                          "WHERE  table_type = 'TUNIDAD'" +                                          
                                          "       AND reg_status != 'A'");

                rs = st.executeQuery();
                unidadess = new Vector();
                
                while(rs.next()){
                    unidad = new Unidad();
                    unidad.setCodigo(rs.getString("cod_unidad"));
                    unidad.setDescripcion(rs.getString("desc_unidad"));
                    unidad.setTipo(rs.getString("tipo"));
                    unidadess.add(unidad);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS UNIDADES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return unidadess;
    }
    
//JOSE 211005    
    public void searchUnidad(String cod, String tipo)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                //st = con.prepareStatement("select * from unidad where cod_unidad=? and tipo = ? and rec_status != 'A'");
                 st = con.prepareStatement("SELECT table_code AS cod_unidad, " +
                                          "       descripcion AS desc_unidad, " +
                                          "       referencia AS tipo " +
                                          "FROM   tablagen " +
                                          "WHERE   table_type = 'TUNIDAD'" +
                                          "       AND table_code = ? " +
                                          "       AND referencia = ? " +
                                          "       AND reg_status != 'A'");
                 st.setString(1,cod);
                st.setString(2,tipo);
                rs= st.executeQuery();
                unidadess = new Vector();
                while(rs.next()){
                    unidad = new Unidad();
                    unidad.setCodigo(rs.getString("cod_unidad"));
                    unidad.setDescripcion(rs.getString("desc_unidad"));
                    unidad.setTipo(rs.getString("tipo"));
                    unidadess.add(unidad);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA UNIDAD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public Unidad getUnidad() {
        return unidad;
    }
    /**
     * obtiene el objeto unidad del codigo buscado.
     * @autor Ing. Jose de la rosa
     * @param Codigo de la unidad.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
public void buscarUnidad (String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        unidad = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement ("SELECT table_code AS cod_unidad, " +
                "       descripcion AS desc_unidad, " +
                "       referencia AS tipo " +
                "FROM   tablagen " +
                "WHERE   table_type = 'TUNIDAD'" +
                "       AND table_code = ? " +
                "       AND reg_status != 'A'");
                st.setString (1,cod);
                rs= st.executeQuery ();
                unidadess = new Vector ();
                while(rs.next ()){
                    unidad = new Unidad ();
                    unidad.setCodigo (rs.getString ("cod_unidad"));
                    unidad.setDescripcion (rs.getString ("desc_unidad"));
                    unidad.setTipo (rs.getString ("tipo"));
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DE LA UNIDAD " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
        
    }
}
