/******************************************************************
* Nombre ......................SeriesDAO.java
* Descripci�n..................clase que maneja las consultas para las series
* Autor........................Armando Oviedo
* modificado: Ing. Iv�n Devia Acosta
* Fecha........................04/11/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/


package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import com.tsp.operation.model.DAOS.MainDAO;

public class SeriesDAO extends MainDAO{
    
    private Series serie;
    //Armando O
    private Vector listaGrupos;
    
    public SeriesDAO(){
        super("SeriesDAO.xml");
    }
    public SeriesDAO(String dataBaseName){
        super("SeriesDAO.xml", dataBaseName);
    }

    //private static final String SQL_BUSCAR_DOCUMENTO = "SELECT DSTRCT, BANCO, DOCUMENT_TYPE, DOCUMENT_NAME FROM TBLDOC ORDER BY DSTRCT";
    private static final String SQL_BUSCAR_DOCUMENTO = 
    "SELECT DISTINCT DSTRCT, " +
    "BANCO, " +
    "DOCUMENT_TYPE," +
    "DOCUMENT_NAME " +
    "FROM TBLDOC ORDER BY DSTRCT, DOCUMENT_NAME ";
    
    private static final String SQL_BUSCAR_CIUDADES  = 
    "SELECT " +
    "   DSTRCT," +
    "   ID_AGENCIA, " +
    "   NOMBRE " +
    "FROM AGENCIA " +
    "ORDER BY DSTRCT, NOMBRE";
    
    private static final String SQL_BUSCAR_BANCO     =
    "SELECT DISTINCT DSTRCT,                       "+
    "       AGENCY_ID,                             "+
    "       BRANCH_CODE                            "+
    "FROM BANCO                                    "+
    "      ORDER BY DSTRCT, AGENCY_ID, BRANCH_CODE ";
    
    
    private static final String SQL_BUSCAR_AGENCIA   =
    "SELECT DISTINCT DSTRCT,                       "+
    "       AGENCY_ID,                             "+
    "       BRANCH_CODE,                           "+
    "       BANK_ACCOUNT_NO                        "+
    "FROM BANCO                                    "+
    "      ORDER BY DSTRCT, AGENCY_ID, BRANCH_CODE ";
        
    private static final String SQL_BUSCAR_CUENTA    =
    "SELECT DISTINCT DSTRCT,                       "+
    "       AGENCY_ID,                             "+
    "       BRANCH_CODE,                           "+
    "       BANK_ACCOUNT_NO,                       "+
    "       ACCOUNT_NUMBER                         "+
    "FROM BANCO                                    "+
    "      ORDER BY DSTRCT, AGENCY_ID, BRANCH_CODE  ";
    
    
    private static final String SQL_INSERTAR         =
    "INSERT INTO SERIES       "+
    "(                        "+
    "  dstrct             ,   "+
    "  agency_id          ,   "+
    "  document_type      ,   "+
    "  branch_code        ,   "+
    "  bank_account_no    ,   "+
    "  account_number     ,   "+
    "  prefix             ,   "+
    "  serial_initial_no  ,   "+
    "  serial_fished_no   ,   "+
    "  last_number        ,   "+
    "  last_update        ,   "+
    "  user_update        ,   "+
    "  creation_date      ,   "+
    "  creation_user      ,   "+
    "  base                   "+
    ")                        "+
    "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    
    
    private static final String SQL_ANULAR_SERIE     =
    "UPDATE SERIES SET REG_STATUS = 'A'"+
    "WHERE DSTRCT = ? AND              "+
    "      AGENCY_ID = ? AND           "+
    "      DOCUMENT_TYPE = ? AND       "+
    "      BRANCH_CODE   = ? AND       "+
    "      BANK_ACCOUNT_NO = ? AND     "+
    "      ACCOUNT_NUMBER = ? AND      "+
    "      SERIAL_INITIAL_NO =?        ";
    
    private static final String SQL_NO_ANULAR_SERIE  =
    "UPDATE SERIES SET REG_STATUS = ' '"+
    "WHERE DSTRCT = ? AND              "+
    "      AGENCY_ID = ? AND           "+
    "      DOCUMENT_TYPE = ? AND       "+
    "      BRANCH_CODE   = ? AND       "+
    "      BANK_ACCOUNT_NO = ? AND     "+
    "      ACCOUNT_NUMBER = ? AND      "+
    "      SERIAL_INITIAL_NO =?        ";
    
    
    
    private static final String SQL_BUSCAR_SERIES_ONE =
    /*"SELECT REG_STATUS,            "+
    "       DSTRCT,                "+
    "       AGENCY_ID,             "+
    "       DOCUMENT_TYPE,         "+
    "       BRANCH_CODE,           "+
    "       BANK_ACCOUNT_NO,       "+
    "       ACCOUNT_NUMBER,        "+
    "       PREFIX,                "+
    "       SERIAL_INITIAL_NO,     "+
    "       SERIAL_FISHED_NO,      "+
    "       LAST_NUMBER            "+
    "FROM SERIES                   "+
    "WHERE DSTRCT = ? AND          "+
    "      AGENCY_ID = ? AND       "+
    "      DOCUMENT_TYPE = ? AND   "+
    "      BRANCH_CODE   = ? AND   "+
    "      BANK_ACCOUNT_NO = ? AND "+
    "      ACCOUNT_NUMBER = ? AND  "+
    "      SERIAL_INITIAL_NO =?    ";*/
    
    "SELECT *               "+
    "FROM SERIES a          "  +
    "WHERE a.DSTRCT = ? AND "  + 
    "   a.AGENCY_ID = ? AND    "  + 
    "   a.DOCUMENT_TYPE = ? AND"+   
    "   a.PREFIX = ? AND"   +    
    "   a.BRANCH_CODE   = ? AND" +   
    "   a.BANK_ACCOUNT_NO = ? AND" +   
    "   a.ACCOUNT_NUMBER = ? AND"   +  
    "   (  a.SERIAL_INITIAL_NO::interval BETWEEN ?::interval AND ?::interval OR"+ 
    "   a.serial_fished_no::interval  BETWEEN ?::interval AND ?::interval  OR"+
    "   ?::interval BETWEEN a.SERIAL_INITIAL_NO::interval AND a.serial_fished_no::interval OR"+	
    "   ?::interval BETWEEN a.SERIAL_INITIAL_NO::interval AND  a.serial_fished_no::interval)";
    
    
    
    private static final String SQL_BUSCAR_SERIES_ALL =
    "SELECT A.REG_STATUS,                    "+
    "       A.DSTRCT,                        "+
    "       A.AGENCY_ID,                     "+
    "       B.NOMBRE,                        "+
    "       A.DOCUMENT_TYPE,                 "+
    "       C.DOCUMENT_NAME,                 "+
    "       A.BRANCH_CODE,                   "+
    "       A.BANK_ACCOUNT_NO,               "+
    "       ACCOUNT_NUMBER,                  "+
    "       A.PREFIX,                        "+
    "       A.SERIAL_INITIAL_NO,             "+
    "       A.SERIAL_FISHED_NO,              "+
    "       A.LAST_NUMBER,                   "+//AMATURANA 17.04.2007
    "       A.CONCEPTO,                      "+
    "       A.ID                             "+
    "FROM SERIES  A,                         "+
    "     AGENCIA B,                         "+
    "     TBLDOC  C                          "+
    "WHERE A.AGENCY_ID = ? AND               "+
    "      A.DOCUMENT_TYPE = ? AND           "+
    "      A.AGENCY_ID = B.ID_AGENCIA AND    "+
    "      A.DOCUMENT_TYPE = C.DOCUMENT_TYPE AND " +
    "      A.REG_STATUS <> 'A'                   "+
    "ORDER BY A.BRANCH_CODE,A.PREFIX";
    
    
    private static final String SQL_MODIFICAR_SERIE   = "UPDATE SERIES SET PREFIX = ?,           "+
    "                  SERIAL_FISHED_NO = ?, "+
    "                  LAST_NUMBER = ?,      "+
    "                  LAST_UPDATE = ?,      "+
    "                  USER_UPDATE = ?       "+
    "WHERE DSTRCT = ? AND                    "+
    "      AGENCY_ID = ? AND                 "+
    "      DOCUMENT_TYPE = ? AND             "+
    "      BRANCH_CODE   = ? AND             "+
    "      BANK_ACCOUNT_NO = ? AND           "+
    "      ACCOUNT_NUMBER = ? AND            "+
    "      SERIAL_INITIAL_NO = ?             ";
    
        /****************SQL QUERIES***************************/
    private static final String SQL_GET_SERIE           = "SELECT prefix,last_number FROM series WHERE document_type = ? AND reg_status != 'A' AND last_number <= serial_fished_no";
    private static final String SQL_UPDATE_SERIE        = "UPDATE series SET last_number = last_number +1, last_update = now(), user_update = ? WHERE document_type = ? AND reg_status != 'A' AND last_number <= serial_fished_no";
   
    private static final String SQL_EstadoSeriesCheck   = "SELECT dstrct, agency_id, document_type, branch_code, bank_account_no,serial_initial_no, serial_fished_no, last_number, creation_date, creation_user from series where reg_status <> 'A' and document_type='004'";
    private static final String SQL_GetSeriesFromGroup  = "SELECT serial_initial_no, serial_fished_no, last_number FROM SERIES WHERE dstrct=? AND agency_id=? AND document_type=? AND branch_code=? AND bank_account_no=? AND reg_status!='A' ORDER BY serial_initial_no";
    private static final String SQL_GetDocumentName     = "SELECT document_name FROM tbldoc WHERE document_type=?";
    private static final String SQL_ENVIARMAIL          = "INSERT INTO sendmail(emailfrom, emailto, emailcopyto, emailsubject, emailbody, lastupdat, sendername) VALUES(?,?,?,?,?,?,?)";    
    
    /******************************************************/
    
    /**
     * M�todo utilizado para enviar un mail como advertencia a la finalizaci�n de series
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String mailto, mailcopyto, mailbody;
     **/ 
    public void sendMailMessage(String mailto, String mailcopyto, String mailbody) throws SQLException{
        //Enviar mail....                
        PreparedStatement ps = null;                
        Connection con = null;
        PoolManager pm = null;
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection(this.getDatabaseName());
            ps = con.prepareStatement(SQL_ENVIARMAIL);
            ps.setString(1, "lizpo@yahoo.com");
            ps.setString(2, mailto);
            ps.setString(3, mailcopyto);
            ps.setString(4, "Urgente: Series Finalizando");
            ps.setString(5, mailbody);                
            ps.setString(6, "now()");
            ps.setString(7, "FINV");
            ps.executeUpdate();        
            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{            
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }            
            if ( con != null){
                pm.freeConnection(this.getDatabaseName(), con );
            }
        }
    }
    
    /**
     * M�todo que guetea el nombre de un documento
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String tipodocumento (doctype)
     **/ 
    public String getNombreDocumento(String doctype) throws Exception{
        String nombre = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        PoolManager pm = null;
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection(this.getDatabaseName());
            ps = con.prepareStatement(SQL_GetDocumentName);
            ps.setString(1,doctype);
            rs = ps.executeQuery();
            while(rs.next()){
                nombre = rs.getString(1);
            }                            
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{            
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }            
            if ( con != null){
                pm.freeConnection(this.getDatabaseName(), con );
            }
        }
        return nombre;
    }
    
    /**
     * M�todo que busca y llena un vector con todos los grupos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void BuscarTodosGrupos() throws Exception{                
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        PoolManager pm = null;
        listaGrupos = new Vector();
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection(this.getDatabaseName());
            ps = con.prepareStatement(SQL_EstadoSeriesCheck);
            rs = ps.executeQuery();                    
            while(rs.next()){
                Series tmp = new Series();
                tmp = tmp.loadV(rs);
                if(!existeGrupo(tmp)){
                    listaGrupos.add(tmp);
                    tmp.setNombre_Doucmento(getNombreDocumento(tmp.getDocument_type()));
                    BuscarSeries(tmp);
                }                              
            }            
        }catch(Exception ex){
                ex.printStackTrace();
                throw new Exception("Error en BuscarTodasSeries. "+ex.getMessage());
        }finally{            
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }            
            if ( con != null){
                pm.freeConnection(this.getDatabaseName(), con );
            }
        }
    }
    
    /**
     * M�todo que busca todas las series de un grupo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......Series s
     **/ 
    public void BuscarSeries(Series s) throws Exception{
        PreparedStatement ps = null;
        PoolManager poolManager = null;
        ResultSet rs;          
        Connection con = null;
        try{
            poolManager= PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(listaGrupos!=null){                                         
                ps = con.prepareStatement(SQL_GetSeriesFromGroup);
                ps.setString(1, s.getDistrito());
                ps.setString(2, s.getAgency_id());
                ps.setString(3, s.getDocument_type());
                ps.setString(4, s.getBranch_code());
                ps.setString(5, s.getBank_account_no());  
                
                rs = ps.executeQuery();
                while(rs.next()){
                    SerieGroupValidation tmp = new SerieGroupValidation();
                    tmp = tmp.load(rs);     
                    int restantes = Integer.parseInt(tmp.getFinalSerialNo()) - tmp.getLastNumber();
                    
                    
                    tmp.setRestantes(restantes);
                    s.addListaSeriesElement(tmp);                                        
                }                               
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en BuscarTodasSeries. "+ex.getMessage());
        }finally{            
            if(ps != null){
                try{
                    ps.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }            
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
    }        
    
    /**
     * M�todo que comprueba si existe un grupo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......Serie s
     **/ 
    public boolean existeGrupo(Series s){
	boolean existe = false;
	for(int i=0;i<listaGrupos.size();i++){
            Series tmp = new Series();
            tmp = (Series)(listaGrupos.elementAt(i));
            if(tmp.getDistrito().equalsIgnoreCase(s.getDistrito()) && tmp.getAgency_id().equalsIgnoreCase(s.getAgency_id()) && tmp.getDocument_type().equalsIgnoreCase(s.getDocument_type()) && tmp.getBranch_code().equalsIgnoreCase(s.getBranch_code()) && tmp.getBank_account_no().equalsIgnoreCase(s.getBank_account_no())){
                existe = true;
            }
        }
	return existe;        
    }
    
    /**
     * M�todo que obtiene la lista de grupos
     * @autor.......Armando Oviedo               
     * @version.....1.0.     
     **/ 
    public Vector getListaGrupos(){
        return this.listaGrupos;
    }
   
    //ACA TERMINA
    
    void Modificar(String Distrito, String Ciudad, String Documento, String Banco, String Agencia_Banco, String Cuenta, String Prefijo, String N_Inicial, String N_Final, String L_Numero, String Usuario) throws SQLException {
        PreparedStatement st    = null;
        String            Fecha = Util.getFechaActual_String(6);
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_MODIFICAR_SERIE);
            st.setString(1,  Prefijo);
            st.setString(2,  N_Final);
            st.setString(3,  L_Numero);
            st.setString(4,  Fecha);
            st.setString(5,  Usuario);
            st.setString(6,  Distrito);
            st.setString(7,  Ciudad);
            st.setString(8,  Documento);
            st.setString(9,  Banco);
            st.setString(10, Agencia_Banco);
            st.setString(11, Cuenta);
            st.setString(12, N_Inicial);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en la rutina Modificar [SeriesDAO]...\n"+e.getMessage());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (conPostgres!= null){
                poolManager.freeConnection(this.getDatabaseName(), conPostgres);
            }
        }
    }
    
    
    void Insertar(String Distrito, String Ciudad, String Documento, String Banco, String Agencia_Banco, String Cuenta, String Prefijo, String N_Inicial, String N_Final, String L_Numero, String Usuario, String base) throws SQLException {
        PreparedStatement st    = null;
        String            Fecha = Util.getFechaActual_String(6);
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_INSERTAR);
            //  st.setString(1,  " ");
            st.setString(1,  Distrito);
            st.setString(2,  Ciudad);
            st.setString(3,  Documento);
            st.setString(4,  Banco);
            st.setString(5,  Agencia_Banco);
            st.setString(6,  Cuenta);
            st.setString(7,  Prefijo);
            st.setString(8,  N_Inicial);
            st.setString(9,  N_Final);
            st.setString(10, L_Numero);
            st.setString(11, Fecha);
            st.setString(12, Usuario);
            st.setString(13, Fecha);
            st.setString(14, Usuario);
            st.setString(15, base);
            
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en la rutina Insertar [SeriesDAO]...\n"+e.getMessage()+"\n ?:"+Ciudad);
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (conPostgres!= null){
                poolManager.freeConnection(this.getDatabaseName(), conPostgres);
            }
        }
    }
    
    
    
    /*
    public static void main(String[] arg)throws Exception{
        try{
            SeriesDAO x = new SeriesDAO();
            x.Insertar("FER", "BQ", "01", "COLPATRIA", "BARRANQUILLA", "001", "FE", "0000", "9999", "0000", "fer", "NC");
        }catch(Exception e){
           throw new Exception(e.getMessage());
        }
    }*/
    
    
    String  Buscar_Ciudades_Js() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        String Variable      = "var ciudades = [";
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_CIUDADES);
            
            rs = st.executeQuery();
            String aux="?";
            while(rs.next()) {
                if(!aux.equals(rs.getString(1))) {
                    if(!aux.equals("?"))
                        Variable+="], ['"+rs.getString(1)+"'";
                    else
                        Variable+="['"+rs.getString(1)+"'";
                }
                Variable+=", '"+rs.getString(2)+"~"+rs.getString(3)+"'";
                aux=rs.getString(1);
            }
            Variable+="]]; \n";
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Ciudades_JS todos [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Variable;
    }
    
    boolean Existe_Series(String Distrito, String Ciudad, String Documento,String prefijo, String Banco, String Agencia_Banco, String Cuenta, String N_Inicial, String N_Final) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean Existe       = false;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_SERIES_ONE);
            st.setString(1, Distrito);
            st.setString(2, Ciudad);
            st.setString(3, Documento);
            st.setString(4, prefijo);
            st.setString(5, Banco);
            st.setString(6, Agencia_Banco);
            st.setString(7, Cuenta);
            st.setString(8, N_Inicial);
            st.setString(9, N_Final);
            st.setString(10, N_Inicial);
            st.setString(11, N_Final);
            st.setString(12, N_Inicial);
            st.setString(13, N_Final);
            
            rs = st.executeQuery();
            while(rs.next()){
                Existe = true;
                break;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Serie ONE [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Existe;
    }
    
    void Anular_Series(String Distrito, String Ciudad, String Documento, String Banco, String Agencia_Banco, String Cuenta, String N_Inicial) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_ANULAR_SERIE);
            st.setString(1, Distrito);
            st.setString(2, Ciudad);
            st.setString(3, Documento);
            st.setString(4, Banco);
            st.setString(5, Agencia_Banco);
            st.setString(6, Cuenta);
            st.setString(7, N_Inicial);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Anular_Serie [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
    }
    
    void No_Anular_Series(String Distrito, String Ciudad, String Documento, String Banco, String Agencia_Banco, String Cuenta, String N_Inicial) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_NO_ANULAR_SERIE);
            st.setString(1, Distrito);
            st.setString(2, Ciudad);
            st.setString(3, Documento);
            st.setString(4, Banco);
            st.setString(5, Agencia_Banco);
            st.setString(6, Cuenta);
            st.setString(7, N_Inicial);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Anular_Serie [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
    }
    
    List Buscar_Series(String Distrito, String Ciudad, String Documento, String Banco, String Agencia_Banco, String N_Inicial) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        List Registros       = new LinkedList();
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_SERIES_ONE);
            st.setString(1, Distrito);
            st.setString(2, Ciudad);
            st.setString(3, Documento);
            st.setString(4, Banco);
            st.setString(5, Agencia_Banco);
            st.setString(6, N_Inicial);
            
            rs = st.executeQuery();
            while(rs.next()){
                Series datos = new Series();
                Registros.add(datos.load(rs));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Serie ONE [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Registros;
    }
    
    List Buscar_Series(String Ciudad, String Documento) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        List Registros       = new LinkedList();
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_SERIES_ALL);
            st.setString(1, Ciudad);
            st.setString(2, Documento);
            
            rs = st.executeQuery();
            while(rs.next()){
                Series datos = new Series();
                Registros.add(datos.load(rs));
               
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Serie ALL [SeriesDAO]... \n"+e.getMessage()+"Ciudad: "+Ciudad+" - Documento: "+Documento);
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Registros;
    }
    
    List Buscar_Ciudades() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        List Registros       = new LinkedList();
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_CIUDADES);
            
            rs = st.executeQuery();
            while(rs.next()){
                Ciudad datos = new Ciudad();
                datos.setNombre(rs.getString("NOMBRE"));
                datos.setCodigo(rs.getString("ID_AGENCIA"));
                Registros.add(datos);
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Ciudades [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Registros;
    }
    
    String Buscar_Tipo_Documento_Js() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        String Variable      = "var documentos = [";
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_DOCUMENTO);
            
            rs = st.executeQuery();
            String aux="?";
            while(rs.next()) {
                if(!aux.equals(rs.getString(1))) {
                    if(!aux.equals("?"))
                        Variable+="], ['"+rs.getString(1)+"'";
                    else
                        Variable+="['"+rs.getString(1)+"'";
                }
                Variable+=", '"+rs.getString(2)+"~"+rs.getString(3)+"~"+rs.getString(4)+"'";
                aux=rs.getString(1);
            }
            Variable+="]]; \n";
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Ciudades_JS todos [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Variable;
    }
    
    List Buscar_Tipo_Documento() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        List Registros       = new LinkedList();
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_DOCUMENTO);
            
            rs = st.executeQuery();
            while(rs.next()){
                TipoDocumento datos = new TipoDocumento();
                Registros.add(datos.load(rs));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Ciudades_JS todos [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Registros;
    }
    
    List Buscar_Banco() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        List Registros       = new LinkedList();
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_BANCO);
            
            rs = st.executeQuery();
            while(rs.next()){
                Banco datos = new Banco();
                Registros.add(datos.load(rs));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Banco todos [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Registros;
    }
    
    String Buscar_Banco_JS() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        String Variable      = "var banco = [";
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_BANCO);
            
            rs = st.executeQuery();
            String aux1 ="?";
            String aux2 ="?";
            while(rs.next()){
                if(!aux1.equals(rs.getString(1))) {
                    if(!aux1.equals("?"))
                        Variable+="]], ['"+rs.getString(1)+"'";
                    else
                        Variable+="['"+rs.getString(1)+"'";
                }
                if(!aux2.equals(rs.getString(2))) {
                    if(!aux2.equals("?"))
                        Variable+="], ['"+rs.getString(2)+"','"+rs.getString(3)+"'";
                    else
                        Variable+=", ['"+rs.getString(2)+"','"+rs.getString(3)+"'";
                }
                else
                    Variable+=",'"+rs.getString(3)+"'";
                aux1=rs.getString(1);
                aux2=rs.getString(2);
            }
            Variable+="]]]; \n";
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Banco_JS todos [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Variable;
    }
    
    String Buscar_Agencia_JS() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        String Variable      = "var agencia = [";
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_AGENCIA);
            
            rs = st.executeQuery();
            String aux1 ="?";
            String aux2 ="?";
            String aux3 ="?";
            while(rs.next()){
                if((!aux1.equals(rs.getString(1))) || (!aux2.equals(rs.getString(2))) || (!aux3.equals(rs.getString(3)))) {
                    if(!aux1.equals("?"))
                        Variable+="], ['"+rs.getString(1)+"~"+rs.getString(2)+"~"+rs.getString(3)+"'";
                    else
                        Variable+="['"+rs.getString(1)+"~"+rs.getString(2)+"~"+rs.getString(3)+"'";
                }
                Variable+=",'"+rs.getString(4)+"'";
                aux1=rs.getString(1);
                aux2=rs.getString(2);
                aux3=rs.getString(3);
            }
            Variable+="]]; \n";
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Banco_JS todos [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Variable;
    }
    
    String Buscar_Cuenta_Js() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        String Variable      = "var cuenta = [";
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_CUENTA);
            
            rs = st.executeQuery();
            String aux1 ="?";
            String aux2 ="?";
            String aux3 ="?";
            String aux4 ="?";
            while(rs.next()){
                if((!aux1.equals(rs.getString(1))) || (!aux2.equals(rs.getString(2))) ||
                (!aux3.equals(rs.getString(3))) || (!aux4.equals(rs.getString(4)))) {
                    if(!aux1.equals("?"))
                        Variable+="], ['"+rs.getString(1)+"~"+rs.getString(2)+"~"+rs.getString(3)+"~"+rs.getString(4)+"'";
                    else
                        Variable+="['"+rs.getString(1)+"~"+rs.getString(2)+"~"+rs.getString(3)+"~"+rs.getString(4)+"'";
                }
                Variable+=",'"+rs.getString(5)+"'";
                aux1=rs.getString(1);
                aux2=rs.getString(2);
                aux3=rs.getString(3);
                aux4=rs.getString(4);
            }
            Variable+="]]; \n";
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina Buscar_Banco_JS todos [SeriesDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
        return Variable;
    }
    
    synchronized void searchSerie(String document_type, Usuario usuario)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PreparedStatement st1 = null;
        ResultSet rs = null,rs1 = null;
        String id=null;
        PoolManager poolManager = null;
        int consecutivo=0;
        serie=null;
        boolean sw=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("SELECT * from series where reg_status ='' and document_type =? and serial_fished_no>last_number and dstrct=?");
                st.setString(1,document_type);
                st.setString(2,usuario.getDstrct());
                rs = st.executeQuery();
                
                if(rs.next()){
                    consecutivo = rs.getInt("Last_number");
                    while(!sw){
                        if(document_type.equals ("001")){
                            st1 = con.prepareStatement("SELECT numpla FROM planilla WHERE numpla = ?");
                            st1.setString(1, rs.getString("Prefix")+consecutivo);
                            
                            rs1 = st1.executeQuery();
                            if(rs1.next())
                                consecutivo++;
                            else
                                sw=true;
                        }
                        else if(document_type.equals ("002")){
                            st1 = con.prepareStatement("SELECT numrem FROM remesa WHERE numrem = ?");
                            st1.setString(1, rs.getString("Prefix")+consecutivo);
                            
                            rs1 = st1.executeQuery();
                            if(rs1.next())
                                consecutivo++;
                            else
                                sw=true;
                        }
                        else{
                            sw=true;
                        }
                    }
                    serie=new Series();
                    serie.setLast_number(consecutivo);
                    serie.setDstrct(rs.getString("dstrct"));
                    serie.setAgency_id(rs.getString("Agency_id"));
                    serie.setDocument_type(rs.getString("Document_type"));
                    serie.setBranch_code(rs.getString("Branch_code"));
                    serie.setBank_account_no(rs.getString("Bank_account_no"));
                    serie.setPrefix(rs.getString("Prefix"));
                    serie.setSerial_initial_no(rs.getString("Serial_initial_no"));
                    serie.setSerial_fished_no(rs.getString("Serial_fished_no"));
                    serie.setLast_update(rs.getTimestamp("Last_update"));
                    serie.setUser_update(rs.getString("User_update"));
                    serie.setCreation_date(rs.getTimestamp("Creation_date"));
                    serie.setCreation_user(rs.getString("Creation_user"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SERIE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    
    Series getSerie()throws SQLException{
        return serie;
    }
    
    public String updateSerie(Series serie, Usuario usuario)throws SQLException{
        String sql ="";
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                int consecutivo = serie.getLast_number()+1;
                st = con.prepareStatement("update series set last_number=?, last_update='now()', user_update=? where dstrct=? and document_type=? and serial_initial_no=?");
                st.setInt(1,consecutivo);
                st.setString(2,usuario.getLogin());
                st.setString(3,serie.getDstrct());
                st.setString(4,serie.getDocument_type());
                st.setString(5,serie.getSerial_initial_no());
                //System.out.println(st.toString());
                //st1.executeUpdate();
                sql = st.toString();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SERIE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sql;
    }
    
    //AGREGADO POR KAREN REALES
    public void buscaSerie(String banco, String cuenta, int ultnum)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select * from series where document_type = '004' and  branch_code =? and bank_account_no =?");
                st.setString(1,banco);
                st.setString(2,cuenta);
                
                rs = st.executeQuery();
                if(rs.next()){
                    serie=new Series();
                    serie.setLast_number(ultnum);
                    serie.setDstrct(rs.getString("dstrct"));
                    serie.setAgency_id(rs.getString("Agency_id"));
                    serie.setDocument_type(rs.getString("Document_type"));
                    serie.setBranch_code(rs.getString("Branch_code"));
                    serie.setBank_account_no(rs.getString("Bank_account_no"));
                    serie.setPrefix(rs.getString("Prefix"));
                    serie.setSerial_initial_no(rs.getString("Serial_initial_no"));
                    serie.setSerial_fished_no(rs.getString("Serial_fished_no"));
                    serie.setLast_update(rs.getTimestamp("Last_update"));
                    serie.setUser_update(rs.getString("User_update"));
                    serie.setCreation_date(rs.getTimestamp("Creation_date"));
                    serie.setCreation_user(rs.getString("Creation_user"));
                    serie.setCuenta(rs.getString("account_number"));
                    //serie.setLast_number(rs.getInt("Last_number"));
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SERIE DE LOS CHEQUES  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    
    /**
     * Metodo asignarSerie, permite asignar un numero de serie
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void asignarSeries(Series serie, Usuario usuario)throws SQLException{
        String sql ="";
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                int consecutivo = serie.getLast_number()+1;
                st = con.prepareStatement("update series set last_number=?, last_update='now()', user_update=? where dstrct=? and document_type=? and serial_initial_no=?");
                st.setInt(1,consecutivo);
                st.setString(2,usuario.getLogin());
                st.setString(3,serie.getDstrct());
                st.setString(4,serie.getDocument_type());
                st.setString(5,serie.getSerial_initial_no());
                
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SERIE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    
    public String updateSerieCheque(Series serie, Usuario usuario)throws SQLException{
        String sql ="";
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                int consecutivo = serie.getLast_number()+1;
                st = con.prepareStatement("update series set last_number=?, last_update='now()', user_update=? where dstrct=? and branch_code=? and bank_account_no=? and account_number=?");
                st.setInt(1,consecutivo);
                st.setString(2,usuario.getLogin());
                st.setString(3,serie.getDstrct());
                st.setString(4,serie.getBranch_code());
                st.setString(5,serie.getBank_account_no());
                st.setString(6,serie.getCuenta());
                //System.out.println(st.toString());
                //st1.executeUpdate();
                sql = st.toString();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SERIE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sql;
    }    
     /***********************************************************
     * M�todo obtenerSerie, retorna prefijo y last_number de la
     *           serie correspondiente al document_type
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param:  String document_type
     * @return: String[] {'prefijo','last_numer'}
     *          null si no existe o esta finalizada
     * @throws: En caso de un error de BD
     ***********************************************************/
    public String[] obtenerSerie( String document_type )throws SQLException{
        
        String[] serieyprefijo = null;
        Connection con=null;
        PoolManager poolManager = null;
        PreparedStatement st    = null;
        ResultSet rs            = null;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_GET_SERIE);
                
                st.setString( 1, document_type );
                ////System.out.println("series "+st.toString());
                rs = st.executeQuery();
                if( rs.next() ){
                    
                    serieyprefijo = new String[2];
                    
                    serieyprefijo[0] = rs.getString("prefix");
                    serieyprefijo[1] = (rs.getInt("last_number")+"");
                }
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.formatoGrabaEn, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            poolManager.freeConnection(this.getDatabaseName(), con);
        }
        
        return serieyprefijo;
        
    }
    
    /***********************************************************
     * M�todo actualizarSerie, actualiza la serie correspondiente
     *        al document_type, incrementando el last_number en 1
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param:  String usuario
     * @param:  String document_type
     * @throws: En caso de un error de BD
     ***********************************************************/
    public void actualizarSerie( String usuario, String document_type ) throws SQLException{
        
        PreparedStatement st    = null;
        Connection con=null;
        PoolManager poolManager = null;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_UPDATE_SERIE);
                st.setString( 1, usuario );
                st.setString( 2, document_type );
                st.executeUpdate();
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.actualizarSerie, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            poolManager.freeConnection(this.getDatabaseName(), con);
        }
        
    }
    //amaturana 18/04/2007
    public String loadUsuarios_Js() throws SQLException{
        String var = "\n var CamposJSUsers = [ ";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LOAD_USUARIOS";
        
        try{
            ps = this.crearPreparedStatement(query);
            
            rs = ps.executeQuery();
            
            while( rs.next() ){
                var += "\n '" + rs.getString("idusuario") + "~" + rs.getString("nombre").toUpperCase() +"',";
            }
            
        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE loadUsuarios_Js " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (ps != null){ ps.close(); }
            this.desconectar(query);
        }
        
        var = var.substring(0,var.length()-1) + "];";
        return var;
    }
   
    //amaturana 18/04/2007
    void insertarSerieCheque(String Distrito, String Ciudad, String Documento, String Banco, String Agencia_Banco, String Cuenta, String Prefijo, String N_Inicial, String N_Final, String L_Numero, String Usuario, String base, String[] usuarios, String concept_code) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERTAR";
        Connection con = null;
        
        try{
            con = this.conectar(query);
            st = con.prepareStatement(this.obtenerSQL(query));            
            st.setString(1,  Distrito);
            st.setString(2,  Ciudad);
            st.setString(3,  Documento);
            st.setString(4,  Banco);
            st.setString(5,  Agencia_Banco);
            st.setString(6,  Cuenta);
            st.setString(7,  Prefijo);
            st.setString(8,  N_Inicial);
            st.setString(9,  N_Final);
            st.setString(10, L_Numero);
            st.setString(11, Usuario);
            st.setString(12, Usuario);
            st.setString(13, base);
            st.setString(14, concept_code);
            
            st.executeUpdate();
            
            st.close();
            st = con.prepareStatement(this.obtenerSQL("SQL_LOAD_ID"));
            rs = st.executeQuery();
            int id = 1;
            if( rs.next() ){
              id = rs.getInt("id");  
            }
            
            st.close();
            st = con.prepareStatement(this.obtenerSQL("SQL_INSERT_SERIE_USUARIO"));
            for( int i=0; i<usuarios.length; i++){
                st.clearParameters();
                st.setString(1, Distrito);
                st.setString(2, String.valueOf(id) );
                st.setString(3, concept_code);
                st.setString(4, usuarios[i]);
                st.setString(5, Usuario);
                st.setString(6, Usuario);
                st.setString(7, base);
                st.executeUpdate();
            }
            
        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertarSerieCheque " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(query);
        }
    }
    
    //amaturana 18/04/2007
    //modif amaturana 20/04/2007
    public String loadUsuariosSerie_Js(String id) throws SQLException{
        String var = "\n var CamposJSUserSerie = [ ";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GET_USUARIO_ID";
        
        try{
            ps = this.crearPreparedStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            
            while( rs.next() ){
                var += "\n '" + rs.getString("idusuario") + "~" + rs.getString("nombre") +"',";
            }
            
        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE loadUsuariosSerie_Js " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (ps != null){ ps.close(); }
            this.desconectar(query);
        }
        
        var = var.substring(0,var.length()-1) + "];";
        return var;
    }
    
    //amaturana 18/04/2007
    void modificarSerieCheque(String Distrito, String Ciudad, String Documento, String Banco, String Agencia_Banco, String Cuenta, String Prefijo, String N_Inicial, String N_Final, String L_Numero, String Usuario, String[] usuarios, String concept_code, int id, String base) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE";
        Connection con = null;
        
        try{
            con = this.conectar(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1,  Prefijo);
            st.setString(2,  N_Final);
            st.setString(3,  L_Numero);
            st.setString(4,  Usuario);
            st.setString(5,  concept_code);
            st.setInt(6,  id);
            
            st.executeUpdate();
            
            st.close();
            st = con.prepareStatement(this.obtenerSQL("SQL_DELETE_USUARIOS"));
            st.setString(1, String.valueOf(id));
            st.executeUpdate();
            
            st.close();
            st = con.prepareStatement(this.obtenerSQL("SQL_INSERT_SERIE_USUARIO"));
            for( int i=0; i<usuarios.length; i++){
                st.clearParameters();
                st.setString(1, Distrito);
                st.setString(2, String.valueOf(id) );
                st.setString(3, concept_code);
                st.setString(4, usuarios[i]);
                st.setString(5, Usuario);
                st.setString(6, Usuario);
                st.setString(7, base);
                st.executeUpdate();
            }
            
        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertarSerieCheque " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(query);
        }        
    }
    
    
    //amaturana 20/04/2007
    public void anularSerieCheque(int id) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR";
        
        try{
            ps = this.crearPreparedStatement(query);
            ps.setInt(1, id);
            ps.setString(2, String.valueOf(id));
            ps.executeUpdate();
            
        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE anularSerieCheque " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (ps != null){ ps.close(); }
            this.desconectar(query);
        }
    }

    /***********************************************************
     * Método obtenerPrefix, retorna prefijo de la
     *           serie correspondiente al document_type
     * @author: Ing. Iris Vargas
     * @param:  String document_type
     * @return: String prefijo
     *          null si no existe o esta finalizada
     * @throws: En caso de un error de BD
     ***********************************************************/
    public String obtenerPrefix(String document_type) throws SQLException {

        String prefijo = null;
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_GET_PREFIX";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, document_type);
                rs = st.executeQuery();
                if (rs.next()) {
                    prefijo = rs.getString("prefix");
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR AL obtenerPrefix(String document_type) " + e.getMessage() + "" + e.getErrorCode());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return prefijo;

    }
    
}
