/*
 * NitService.java
 *
 * Created on 26 de mayo de 2005, 03:47 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  kreales
 */
public class NitService {
    
    private NitDAO nitDao;
    private NitSot nit;
    /** Creates a new instance of NitService */
    public NitService() {
        nitDao = new NitDAO();
    }
    public NitService(String dataBaseName) {
        nitDao = new NitDAO(dataBaseName);
    }
    public void insertProp()throws SQLException{
        nitDao.insertProp();
        
    }
    public void updateProp()throws SQLException{
        nitDao.updateProp();
        
    }
    
    public void deleteProp()throws SQLException{
        nitDao.deleteProp();
        
    }
    
    public boolean estaProp()throws SQLException{
        return nitDao.estaProp();
    }
    public void listaPropietarios()throws SQLException{
        nitDao.listaPropietarios();
        
    }
    
    public java.util.Vector getPropietarios() {
        return nitDao.getPropietarios();
    }
    
    public com.tsp.operation.model.beans.Nit getProp() {
        return nitDao.getProp();
    }
    
    public void setProp(com.tsp.operation.model.beans.Nit prop) {
        nitDao.setProp(prop);
    }
    //AMENDEZ 20050708
    //--- buscamos un conductor especifico en la tabla conductor
    public boolean searchNit(String id) throws SQLException{
        boolean sw = false;
        if ((nit=nitDao.searchNit(id))!=null)
            sw = true;
        return sw;
    }
    
    //AMENDEZ 20050708
    // inserción en la tabla nit
    public void insertNit(NitSot nit)throws SQLException{
        nitDao.insertNit(nit);
    }
    
    //AMENDEZ 20050708
    // modificamos en la tabla nit
    public void updateNit(NitSot nit)throws SQLException{
        nitDao.updateNit(nit);
    }
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public com.tsp.operation.model.beans.NitSot getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(com.tsp.operation.model.beans.NitSot nit) {
        this.nit = nit;
    }
    public String obtenerNombre(String cedcon) throws SQLException {
        return nitDao.obtenerNombre(cedcon);
    }
     /**
     * Metodo buscarUsuarioPorNit_Nombre, metodo que busca un usuario por cudula o por nombre
     * @param:
     * @autor : Ing. Jdelarosa
     * @version : 1.0
     */
    public void buscarUsuarioPorNit_Nombre(String ced_nom) throws SQLException {
        nitDao.buscarUsuarioPorNit_Nombre(ced_nom);
    }
    
        /**
     * Setter for property propietarios.
     * @param propietarios New value of property propietarios.
     */
    public void setPropietarios(java.util.Vector propietarios) {
        nitDao.setPropietarios(propietarios);
    }
}
