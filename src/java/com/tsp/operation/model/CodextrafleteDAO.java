/*
 * codextrafleteDAO.java
 *
 * Created on 10 de julio de 2005, 05:48 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  INTEL
 */
public class CodextrafleteDAO {
    
    /** Creates a new instance of codextrafleteDAO */
    public CodextrafleteDAO() {
    }
    
    private Codextraflete codextra;
    private Codextraflete Codextra;
    private List lista;
     public void setCodigoextraflete(Codextraflete codextra){
        this.codextra = codextra;
    }
    
  // Verifica si existe un codigo de extra flete Anulado...  
  // Return ( True --> si existe o False si no...  
     
  public boolean existCodigoExtraflete(String codigo )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(
                 "Select  *"
                +"from codextraflete "
                +"where codextraflete = ?" 
                +" and reg_status = 'A' ");
                
                st.setString(1,codigo);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO EXTRA FLETE" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }   
  
  
   public void insertCodextraflete()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(
                 "insert into codextraflete "
                +"(reg_status,codextraflete,descripcion, cuenta, agencia, elemento, ajuste, unidades, creation_date,creation_user, base, tipo)"
                +" values('',?,?,?,?,?,?,?,'now()',?,?, ?)");
              
                st.setString(1, codextra.getCodextraflete());
                st.setString(2, codextra.getDescripcion());
                st.setString(3, codextra.getCuenta());
                st.setString(4, codextra.getAgencia());
                st.setString(5, codextra.getElemento());
                st.setString(6, codextra.getAjuste());
                st.setString(7, codextra.getUnidades());
                st.setString(8, codextra.getCreation_user());
                st.setString(9, codextra.getBase()); 
                st.setString(10, codextra.getTipo());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE CODIGOS DE EXTRA FLETE" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
  

     public void updateCodextraflete()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(
                " update codextraflete " +
                " set reg_status='', descripcion=?, cuenta=?, agencia=?, elemento=?, ajuste=?, unidades=?, user_update=?,last_update='now()'" +
                " where codextraflete = ? ");
                st.setString(1, codextra.getDescripcion());
                st.setString(2, codextra.getCuenta());
                st.setString(3, codextra.getAgencia());
                st.setString(4, codextra.getElemento());
                st.setString(5, codextra.getAjuste());
                st.setString(6, codextra.getUnidades());
                st.setString(7, codextra.getCreation_user());
                st.setString(8, codextra.getCodextraflete());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL CODIGO EXTRA FLETE" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
 
   
    public Codextraflete getCodextraflete() throws SQLException{
         return Codextra;
    }   
     
    public void consultarCodextraflete()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Codextra=null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(
                 "SELECT codextraflete, descripcion, cuenta, agencia, elemento, ajuste, unidades, tipo" 
                +" from codextraflete "
                +" where codextraflete = ?" 
                +" and reg_status = '' ");
                st.setString(1, codextra.getCodextraflete());
                rs = st.executeQuery();
             
                if(rs.next())
                  Codextra = (Codextraflete.load(rs));
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
   
    public void deleteCodextraflete()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(
                "update codextraflete " +
                "set reg_status='A', user_update=?, last_update='now()'" +
                "where codextraflete = ? ");
                st.setString(1, codextra.getCreation_user());
                st.setString(2, codextra.getCodextraflete());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ELIMINACION DEL CODIGO EXTRA FLETE" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
  
    
     public List listarCodextraflete()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        lista = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(
                 "SELECT codextraflete, descripcion, cuenta, agencia, elemento, ajuste, unidades, tipo " 
                +" from codextraflete "
                +" where reg_status <> 'A'" 
                +" order by codextraflete ");
                rs = st.executeQuery();
                lista = new LinkedList();                 
                while(rs.next())
                  lista.add(Codextraflete.load(rs));
          }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
      return lista;  
    } 
       /**
     * Determina si un extraflete ya existe y cual es su estado
     * @autor Ing. Andr�s Maturana
     * @param codigo C�digo del extraflete
     * @throws <code>SQLException</code>
     * @version 1.0
     */
     public String existCodExtraflete(String codigo )throws SQLException{
         
         Connection con=null;
         PreparedStatement st = null;
         ResultSet rs = null;
         PoolManager poolManager = null;
         String sw = null;
         
         try {
             
             poolManager = PoolManager.getInstance();
             con = poolManager.getConnection("fintra");
             if(con!=null){
                 st = con.prepareStatement(
                 "Select  reg_status "
                 +"from codextraflete "
                 +"where codextraflete = ?");
                 
                 st.setString(1,codigo);
                 rs = st.executeQuery();
                 
                 if(rs.next()){
                     sw = rs.getString("reg_status");
                 }
             }
             
         }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO EXTRA FLETE" + e.getMessage() + " " + e.getErrorCode());
         }
         finally{
             if (st != null){
                 try{
                     st.close();
                 }
                 catch(SQLException e){
                     throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                 }
             }
             
             if (con != null){
                 poolManager.freeConnection("fintra", con);
             }
         }
         return sw;
         
     }
}

