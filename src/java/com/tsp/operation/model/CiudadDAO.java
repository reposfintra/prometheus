 /*
  * CiudadDAO.java
  *
  * Created on 1 de diciembre de 2004, 02:58 PM
  */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  KREALES
 */
public class CiudadDAO extends MainDAO {
    public CiudadDAO() {
        super("CiudadDAO.xml");
    }
    
    public CiudadDAO(String dataBaseName) {
        super("CiudadDAO.xml", dataBaseName);
    }
    
    private TreeMap ciudades;
    private Ciudad ciudad;
    private List Ciudades;
    private Vector VecCiudades;
    //apayares 12.12.05
    private LinkedList ciudadList;
    
    private TreeMap vias;
    
    
        private static String SQL_INSERTAR = "INSERT INTO ciudad (  codciu, " +
    "                                                           nomciu, " +
    "                                                           pais, " +
    "                                                           coddpt," +
    "                                                           reg_status, " +
    "                                                           last_update, " +
    "                                                           user_update, " +
    "                                                           creation_date, " +
    "                                                           creation_user, " +
    "                                                           aplica, " +
    "                                                           codica, " +
    "                                                           zona, " +
    "                                                           frontera, " +
    "                                                           agasoc, " +
    "                                                           base, " +
    "                                                           tipociu," +
    "                                                           indicativo," +
    "                                                           frontera_asoc, " +
    "                                                           tiene_rep_urbano," +
    "                                                           zona_urb    " +
    " ) " +
    "                                       VALUES (?,?,?,?,'','now()',?,'now()',?,?,?,?,?,?,?,?,?,?,?,?)";
      //Sandra 03-02-2006
    private static String SQL_MODIFICAR = " UPDATE  ciudad " +
    "                                       SET     nomciu=?," +
    "                                               last_update='now()', " +
    "                                               user_update=?, " +
    "                                               aplica=?, " +
    "                                               codica=?, " +
    "                                               zona=?, " +
    "                                               tipociu=?, " +
    "                                               indicativo=?, " +
    "                                               coddpt = ?," +
    "                                               frontera_asoc = ?," +
    "                                               tiene_rep_urbano = ?," +
    "                                               zona_urb = ?," +
    "                                               agasoc = ?," +
    "                                               frontera = ? "+
    "                                       WHERE   pais=? " +
    "                                               AND coddpt=? " +
    "                                               AND codciu=?";

    private static String SQL_BUSCAR = "SELECT  c.codciu, " +
    "                                           c.nomciu, " +
    "                                           p.country_code, " +
    "                                           p.country_name, " +
    "                                           e.department_code AS departament_code, " +
    "                                           e.department_name AS departament_name, " +
    "                                           c.reg_status, "+
    "                                           c.last_update, " +
    "                                           c.user_update, " +
    "                                           c.creation_date, " +
    "                                           c.creation_user, "+
    "                                           c.aplica, " +
    "                                           c.codica, " +
    "                                           c.zona, " +
    "                                           c.frontera, " +
    "                                           c.agasoc, " +
    "                                           cnomag.nomciu AS nomagasoc, " +
    "                                           c.base, " +
    "                                           c.tipociu, " +
    "                                           c.indicativo, " +
    "                                           z.desczona," +
    "                                           c.frontera_asoc," +
    "                                           c.tiene_rep_urbano," +
    "                                           c.zona_urb     " +
    "                                   FROM    ciudad c " +
    "                                           INNER JOIN estado e on(e.department_code=c.coddpt) " +
    "                                           INNER JOIN pais p on(p.country_code=c.pais) " +
    "                                           INNER JOIN zona z on(z.codzona = c.zona) "+
    "                                           LEFT OUTER JOIN ciudad cnomag on(c.agasoc = cnomag.codciu) " +
    "                                   WHERE   c.codciu= ?" +
    "                                           AND c.coddpt= ? " +
    "                                           AND c.reg_status='' " +
    "                                           AND c.pais= ? " +
    "                                           AND e.rec_status='' " +
    "                                           AND p.reg_status='' ";
    
    private static final  String Select_Consulta = "select c.codciu, c.nomciu from ciudad c, estado e, pais p   where c.reg_status='' and e.rec_status='' and p.reg_status='' and c.coddpt= ? and c.pais= ? and "+
    "p.country_code=c.pais and e.department_code=c.coddpt order by c.nomciu";
    
    private static final  String Select_ConsultaGeneral = "select c.codciu, c.nomciu, p.country_code, p.country_name, e.department_code, " +
    " e.department_name, c.reg_status, c.last_update, c.user_update, c.creation_date, c.creation_user, " +
    " c.aplica, c.codica, c.zona, c.frontera, c.agasoc, cnomag.nomciu as nomagasoc, c.base, c.tipociu, z.desczona "+
    " from ciudad c " +
    " inner join estado e on(e.department_code=c.coddpt) " +
    " inner join pais p on(p.country_code=c.pais) " +
    " inner join zona z on(z.codzona = c.zona) " +
    " left outer join ciudad cnomag on(c.agasoc = cnomag.codciu)" +
    " where  c.reg_status='' " +
    " and e.rec_status='' " +
    " and p.reg_status='' " +
    " order by p.country_name, c.nomciu";
    
    //JuanM
    private static String SQLAgencia =              "   SELECT CODCIU, NOMCIU FROM                          "+
    "   (SELECT DISTINCT AGASOC FROM CIUDAD)A, CIUDAD C     "+
    "   WHERE A.AGASOC = C.CODCIU ORDER BY CODCIU           ";
    
    //Nuevo Diogenes
    private static String BuscarIndicativo = "select indicativo from ciudad where codciu = ? ";
    
    private final String SQL_OBTENER_NOMBRE_CIUDAD = "  SELECT  nomciu " +
    "                                                   FROM    ciudad " +
    "                                                   WHERE   codciu = ?";
    
    private static final  String SELECT_CONSULTA = "SELECT  c.codciu, " +
    "                                                       c.nomciu " +
    "                                               FROM    ciudad c, " +
    "                                                       estado e, " +
    "                                                       pais p   " +
    "                                               WHERE   c.reg_status ='' " +
    "                                                       AND e.rec_status='' " +
    "                                                       AND p.reg_status='' " +
    "                                                       AND c.coddpt= ? " +
    "                                                       AND c.pais= ? " +
    "                                                       AND p.country_code = c.pais " +
    "                                                       AND e.department_code=c.coddpt" +
    "                                               ORDER BY c.nomciu";
    
    private static final  String SELECT_CONSULTAGENERAL = "SELECT   c.codciu, " +
    "                                                               c.nomciu, " +
    "                                                               p.country_code, " +
    "                                                               p.country_name, " +
    "                                                               e.department_code, " +
    "                                                               e.department_name, " +
    "                                                               c.reg_status, " +
    "                                                               c.last_update, " +
    "                                                               c.user_update, " +
    "                                                               c.creation_date, " +
    "                                                               c.creation_user, " +
    "                                                               c.aplica, " +
    "                                                               c.codica, " +
    "                                                               c.zona, " +
    "                                                               c.frontera, " +
    "                                                               c.agasoc, " +
    "                                                               cnomag.nomciu AS nomagasoc, " +
    "                                                               c.base, " +
    "                                                               c.tipociu, " +
    "                                                               z.desczona "+
    "                                                       FROM    ciudad c " +
    "                                                               INNER JOIN estado e on(e.department_code=c.coddpt) " +
    "                                                               INNER JOIN pais p on(p.country_code=c.pais) " +
    "                                                               INNER JOIN zona z on(z.codzona = c.zona) " +
    "                                                               LEFT OUTER JOIN ciudad cnomag on(c.agasoc = cnomag.codciu)" +
    "                                                       WHERE   c.reg_status='' " +
    "                                                               AND e.rec_status='' " +
    "                                                               AND p.reg_status='' " +
    "                                                       ORDER BY p.country_name, " +
    "                                                               c.nomciu";
    
    
    //jescandon
    private static String SQL_AGENCIA =" SELECT  codciu, " +
    "                                           nomciu " +
    "                                   FROM    (SELECT DISTINCT agasoc " +
    "                                           FROM ciudad)a, " +
    "                                           ciudad c     "+
    "                                   WHERE   a.agasoc = c.codciu " +
    "                                   ORDER BY codciu";
    
    //dbastidas
    private static String BUSCAR_INDICATIVO = " SELECT  indicativo " +
    "                                           FROM    ciudad " +
    "                                           WHERE   codciu = ? ";
    
    private static String SQL_OBTENER_CIUDAD = "SELECT  nomciu, " +
    "                                                   codciu " +
    "                                           FROM    ciudad " +
    "                                           ORDER BY nomciu";
    
    
    
    private static String SQL_EXISTE_CIUDAD = " SELECT  c.codciu " +
    "                                           FROM    ciudad c, " +
    "                                                   estado e, " +
    "                                                   pais p  " +
    "                                           WHERE   c.reg_status=''" +
    "                                                   AND e.rec_status='' " +
    "                                                   AND p.reg_status='' " +
    "                                                   AND p.country_code=c.pais " +
    "                                                   AND e.department_code=c.coddpt " +
    "                                                   AND c.pais= ? " +
    "                                                   AND c.coddpt= ? " +
    "                                                   AND c.codciu= ?";
    
    private static String SQL_EXCANULADA = "SELECT  codciu " +
    "                                       FROM    ciudad " +
    "                                       WHERE   reg_status='A' " +
    "                                               AND codciu= ?";
    
    private static String SQL_CIUXNOMBRE = "SELECT  c.codciu " +
    "                                       FROM    ciudad c, " +
    "                                               estado e, " +
    "                                               pais p   " +
    "                                       WHERE   c.reg_status='' " +
    "                                               AND e.rec_status='' " +
    "                                               AND p.reg_status='' " +
    "                                               AND p.country_code=c.pais " +
    "                                               AND e.department_code=c.coddpt " +
    "                                               AND c.nomciu= ?  " +
    "                                               AND e.department_name= ? " +
    "                                               AND p.country_name= ? ";
    
   
    
    private static String SQL_BUSCAR1 = "SELECT c.codciu, " +
    "                                           c.nomciu, " +
    "                                           p.country_name, " +
    "                                           e.department_name AS departament_name, " +
    "                                           p.country_code, " +
    "                                           e.department_code AS departament_code " +
    "                                   FROM    ciudad c, " +
    "                                           estado e, " +
    "                                           pais p, " +
    "                                           zona z " +
    "                                   WHERE   c.reg_status='' " +
    "                                           AND e.rec_status='' " +
    "                                           AND p.reg_status='' " +
    "                                           AND p.country_code=c.pais " +
    "                                           AND e.department_code=c.coddpt " +
    "                                           AND z.codzona = c.zona " +
    "                                           AND ( p.country_name LIKE ? OR p.country_code LIKE ? )	"+
    "						AND ( e.department_name LIKE ? OR e.department_code LIKE ? )"+
    "                                           AND ( c.nomciu LIKE ? OR c.codciu LIKE ? )"+
    "                                   ORDER BY p.country_name, c.nomciu";
    
     private static String SQL_ACTIVAR = "UPDATE ciudad " +
    "                                   SET     reg_status='', " +
    "                                           last_update='now()', " +
    "                                           user_update=?, " +
    "                                           nomciu=?, " +
    "                                           pais=?, " +
    "                                           coddpt=?," +
    "                                           aplica=?, " +
    "                                           codica=?, " +
    "                                           zona=?, " +
    "                                           frontera=?, " +
    "                                           agasoc=?, " +
    "                                           base=?, " +
    "                                           tipociu=?," +
    "                                           indicativo=?,"+
    "                                           frontera_asoc = ?," +
    "                                           tiene_rep_urbano = ?," +
    "                                           zona_urb = ? "+
    "                                   WHERE   codciu=?";

    private static String SQL_ANULAR = "UPDATE  ciudad " +
    "                                   SET     reg_status='A', " +
    "                                           user_update=? " +
    "                                   WHERE   pais=? " +
    "                                           AND coddpt=? " +
    "                                           AND codciu= ?";
    
    private static String SQL_EXCXESTADO = "SELECT  c.codciu " +
    "                                       FROM    ciudad c, " +
    "                                               estado e, " +
    "                                               pais p   " +
    "                                       WHERE   c.reg_status='' " +
    "                                               AND e.rec_status='' " +
    "                                               AND p.reg_status='' " +
    "                                               AND p.country_code=c.pais " +
    "                                               AND e.department_code=c.coddpt " +
    "                                               AND c.pais = ? " +
    "                                               AND c.coddpt=?";
    
    private static String SQL_LISTAR = "SELECT  c.codciu, " +
    "                                           c.nomciu " +
    "                                   FROM    ciudad c, " +
    "                                           estado e, " +
    "                                           pais p, " +
    "                                           zona z " +
    "                                   WHERE   c.reg_status='' " +
    "                                           AND e.rec_status='' " +
    "                                           AND p.reg_status='' " +
    "                                           AND p.country_code=c.pais " +
    "                                           AND e.department_code=c.coddpt " +
    "                                           AND z.codzona = c.zona " +
    "                                           AND e.department_code= ? " +
    "                                           AND p.country_code= ?  " +
    "                                   ORDER BY c.nomciu";
    
    private static String SQL_LCXPAIS = "SELECT c.codciu, " +
    "                                           c.nomciu,   " +
    "                                           p.country_code, " +
    "                                           p.country_name " +
    "                                   FROM    ciudad c, " +
    "                                           pais p, " +
    "                                           zona z " +
    "                                   WHERE   c.reg_status='' " +
    "                                           AND p.reg_status='' " +
    "                                           AND p.country_code=c.pais " +
    "                                           AND z.codzona = c.zona " +
    "                                           AND p.country_code= ?  " +
    "                                   ORDER BY c.nomciu";
    
    
     private static String SQL_LCXPAISC = "SELECT c.codciu, " +
    "                                           c.nomciu,   " +
    "                                           p.country_code, " +
    "                                           p.country_name " +
    "                                   FROM    ciudad c, " +
    "                                           pais p, " +
    "                                           zona z " +
    "                                   WHERE   c.reg_status='' " +
    "                                           AND p.reg_status='' " +
    "                                           AND p.country_code=c.pais " +
    "                                           AND z.codzona = c.zona " +
    "                                           AND p.country_code= ?  " +
    "                                           AND c.nomciu not like 'PC %' " +                                                    
    "                                   ORDER BY c.nomciu";
    
    private static String SQL_BCXPAIS = "SELECT c.codciu, " +
    "                                           c.nomciu " +
    "                                   FROM    ciudad c, " +
    "                                           pais p," +
    "                                           zona z" +
    "                                   WHERE   c.reg_status='' " +
    "                                           AND p.reg_status=''" +
    "                                           AND p.country_code=c.pais " +
    "                                           AND z.codzona = c.zona " +
    "                                           AND c.codciu= ?" +
    "                                           AND c.pais= ?";
    
    private static String SQL_NOMCIUDAD =   "SELECT nomciu " +
    "                                       FROM    ciudad  " +
    "                                       WHERE codciu = ?";
    
    private static String SQL_BUSCAR2 = " SELECT    codciu," +
    "                                               nomciu " +
    "                                     FROM      ciudad  " +
    "                                     WHERE     (codciu LIKE ? OR nomciu LIKE ?)";
    
    private static String SQL_VERIFICAR = " SELECT  codciu " +
    "                                       FROM    ciudad  " +
    "                                       WHERE   codciu=?";
    
    private static String SQL_BUSCAR3 = "SELECT c.codciu, " +
    "                                           c.nomciu," +
    "                                           c.zona, " +
    "                                           c.frontera, " +
    "                                           c.agasoc, " +
    "                                           c.base, " +
    "                                           c.tipociu," +
    "                                           c.tiene_rep_urbano, " +
    "                                           c.pais " +
    "                                   FROM    ciudad c " +
    "                                   WHERE   c.codciu= ?";
    /*Karen 02-05-2006*/
    private static String SLQ_FRONTERAS =
    "   SELECT CODCIU, NOMCIU FROM CIUDAD                         "+
    "   WHERE pais=? AND frontera in ('S','Y') ";
    /**
     * Holds value of property out.
     */
    private PrintWriter out;
    
    /**
     * Getter for property ciudades
     * @return Value of property ciudades
     */
    public TreeMap getCiudades(){
        return ciudades;
    }
    
    /**
     * Metodo <tt>searchCiudades()</tt>, obtiene la lista de las ciudades
     *  registradas en el sistema
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public void searchCiudades()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ciudades = null;
        ciudades = new TreeMap();
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            
            if(con!=null){
                st = con.prepareStatement(SQL_OBTENER_CIUDAD);
                rs = st.executeQuery();
                
                while(rs.next()){
                    ciudades.put(rs.getString("nomciu"), rs.getString("codciu"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CIUDADES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    
    /**
     * Metodo <tt>existCiudades</tt>, verifica la existencia de las ciudades
     * @autor : Ing. Karen Reales
     * @return boolean
     * @version : 1.0
     */
    public boolean existCiudades()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_OBTENER_CIUDAD);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR (existCiudades)" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sw;
    }
    
    /**
     * Metodo <tt>obtenerNombreCiudad</tt>, obtiene el nombre de una ciudad
     * @autor : Ing. Karen Reales
     * @param codigo de la ciudad (String)
     * @return String
     * @version : 1.0
     */
    public String obtenerNombreCiudad(String codigo) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( this.getDatabaseName());
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement( SQL_OBTENER_NOMBRE_CIUDAD );
                ps.setString( 1, codigo );
                rs = ps.executeQuery();
                if (rs.next()){
                    return rs.getString(1);
                }
            }
            return "(no encontrada)";
        }
        catch(Exception e){
            throw new SQLException( "ERROR DURANTE LA OBTENCION DEL NOMBRE DE LA CIUDAD " + e.getMessage());
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( this.getDatabaseName(), con );
            }
        }
        
    }
    
    /**
     * Setter for property ciudad
     * @param p New value of property ciudad
     */
    public void setCiudad(Ciudad ciudad){
        this.ciudad = ciudad;
    }
    
    
    
    /**
     * Metodo <tt>existeCiudad</tt>, verifica la existencia de una ciudad
     * @autor : Ing. Diogenes Bastidas
     * @param codigo del pais (String), codigo del departamento (String),
     *    codigo de la ciudad (String)
     * @return boolean
     * @version : 1.0
     */
    
    public boolean existeCiudad(String cod_pais, String cod_depto, String codciu ) throws SQLException {
        boolean sw = false;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement(SQL_EXISTE_CIUDAD);
                st.setString(1, cod_pais);
                st.setString(2, cod_depto);
                st.setString(3, codciu);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR [existeCiudad] " + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
        return sw;
    }
    
    /**
     * Metodo <tt>existeCiudadAnulado</tt>, verifica la existencia de una ciudad anulada
     * @autor : Ing. Diogenes Bastidas
     * @param codigo del pais (String), codigo del departamento (String),
     *    codigo de la ciudad (String)
     * @return boolean
     * @version : 1.0
     */
    public boolean existeCiudadAnulado(String cod_pais, String cod_depto, String codciu ) throws SQLException {
        boolean sw = false;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement(SQL_EXCANULADA);
                st.setString(1, codciu);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR [existeCiudadAnulado]" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
        return sw;
    }
    
    /**
     * Metodo <tt>existeCiudadnom</tt>, verifica la existencia de una ciudad dado el nombre
     * @autor : Ing. Diogenes Bastidas
     * @param nombre del pais (String), nombre del departamento (String),
     *    nombre de la ciudad (String)
     * @return boolean
     * @version : 1.0
     */
    public boolean existeCiudadnom(String nom_pais, String nom_depto, String nomciu ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement(SQL_CIUXNOMBRE);
                st.setString(1, nomciu);
                st.setString(2, nom_depto);
                st.setString(3, nom_pais);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR [existeCiudadnom]" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
        return sw;
    }
    
    /**
     * Metodo <tt>listarCiudad</tt>, obtiene la lista de las ciudades dado pais y estado
     * @autor : Ing. Diogenes Bastidas
     * @param codigo del pais (String), codigo del departamento (String)
     * @version : 1.0
     */
    public void listarCiudad(String pais, String estado)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        VecCiudades = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(SELECT_CONSULTA);
                st.setString(1,estado);
                st.setString(2,pais);
                rs = st.executeQuery();
                
                VecCiudades =  new Vector();
                
                while (rs.next()){
                    Ciudad ciudad = new Ciudad();
                    ciudad.setcodciu(rs.getString( "codciu") );
                    ciudad.setnomciu(rs.getString( "nomciu") );
                    VecCiudades.add(ciudad);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR [ listarCiudad ] " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
    }
    
    /**
     * Metodo <tt>listarCiudadGeneral</tt>, obtiene la lista de las ciudades del sistema
     * @autor : Ing. Diogenes Bastidas
     * @version : 1.0
     */
    public void listarCiudadGeneral( )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        VecCiudades = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(SELECT_CONSULTAGENERAL);
                rs = st.executeQuery();
                
                VecCiudades =  new Vector();
                
                while (rs.next()){
                    VecCiudades.add(ciudad.load4(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR [ listarCiudadGeneral ] " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
    }
    
    /**
     * Getter for property Ciudades
     * @return Value of property Ciudades
     */
    public List obtenerCiudades(){
        return Ciudades;
    }
    
    /**
     * Getter for property VecCiudades
     * @return Value of property VecCiudades
     */
    public Vector obtCiudades(){
        return VecCiudades;
    }
    
    /**
     * Getter for property ciudad
     * @return Value of property ciudad
     */
    public Ciudad obtenerCiudad()throws SQLException{
        return ciudad;
    }
    
   
    /**
     * Metodo <tt>buscarCiudadesXnom</tt>, obtiene las ciudades dado unos parametros de busqueda
     * @autor : Ing. Diogenes Bastidas
     * @param nombre del pais (String), nombre del departamento (String), nombre de la ciudad (String)
     * @version : 1.0
     */
    public void buscarCiudadesXnom(String nom_pais, String nom_depto, String nomciu ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        VecCiudades = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement(SQL_BUSCAR1);
                st.setString(1, nom_pais);
                st.setString(2, nom_pais);
                st.setString(3, nom_depto);
                st.setString(4, nom_depto);
                st.setString(5, nomciu);
                st.setString(6, nomciu);
                rs = st.executeQuery();
                
                VecCiudades =  new Vector();
                
                while (rs.next()){
                    
                    Ciudad c = new Ciudad();
                    c.setcodciu(rs.getString( "codciu") );
                    c.setnomciu(rs.getString( "nomciu") );
                    c.setpais_name( rs.getString("country_name") );
                    c.setdepartament_name( rs.getString("departament_name" ) );
                    
                    c.setpais_code(rs.getString("country_code"));
                    c.setdepartament_code(rs.getString("departament_code"));
                    
                    VecCiudades.add(c);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR [ buscarCiudadesxNombre ]" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
    }
    
    /**
     * Metodo <tt>activarCiudad</tt>, activa una ciudad antes anulada, actualiza el rec_status = ''
     * @autor : Ing. Diogenes Bastidas
     * @version : 1.0
     */
    public void activarCiudad() throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(SQL_ACTIVAR);
                st.setString(1,  ciudad.getUser_update() );
                st.setString(2, ciudad.getNomCiu() );
                st.setString(3, ciudad.getpais_code() );
                st.setString(4, ciudad.getdepartament_code() );
                st.setString(5, ciudad.getAplIca() );
                st.setString(6, ciudad.getCodIca() );
                st.setString(7, ciudad.getZona() );
                st.setString(8, ciudad.getFrontera() );
                st.setString(9, ciudad.getAgAsoc() );
                st.setString(10, ciudad.getBase() );
                st.setString(11, ciudad.getTipociu() );
                st.setInt(12, ciudad.getIndicativo() );
                st.setString(13, ciudad.getFrontera_asoc());
                st.setString(14, ciudad.getTiene_rep_urbano());
                st.setString(15, ciudad.getZona_urb());
                
                st.setString(16, ciudad.getcodciu() );
                ////System.out.println("ST ACTIVACION " + st);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL MODIFICAR LA CIUDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
    }
    
  
    
    /**
     * Metodo <tt>anularCiudad</tt>, anular ciudad, actualiza el rec_status = 'A'
     * @autor : Ing. Diogenes Bastidas
     * @version : 1.0
     */
    public void anularCiudad() throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(SQL_ANULAR);
                st.setString(1, ciudad.getUser_update() );
                st.setString(2, ciudad.getpais_code() );
                st.setString(3, ciudad.getdepartament_code() );
                st.setString(4, ciudad.getcodciu());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR LA CIUDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        
    }
    
    /**
     * Metodo <tt>existeCiudadesxEstado</tt>, verifica la existencia de una ciudad
     *   dado el pais y el estado
     * @autor : Ing. Diogenes Bastidas
     * @param codigo del pais (String), codigo del estado (String)
     * @version : 1.0
     */
    public boolean existeCiudadesxEstado(String cpais, String cest)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement(SQL_EXCXESTADO);
                st.setString(1, cpais);
                st.setString(2, cest);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR [ existeCiudadesxEstado ] " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        return sw;
    }
    
    /**
     * Metodo <tt>listarCiudades</tt>, obtiene la lista de las ciudades dado pais y estado
     * @autor : Ing. Diogenes Bastidas
     * @param codigo del pais (String), codigo del estado (String)
     * @version : 1.0
     */
    public void listarCiudades(String pais, String estado)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Ciudades = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(SQL_LISTAR);
                st.setString(1,estado);
                st.setString(2,pais);
                //////System.out.println("ST " + st);
                rs = st.executeQuery();
                
                Ciudades =  new LinkedList();
                
                while (rs.next()){
                    Ciudad ciudad = new Ciudad();
                    ciudad.setcodciu(rs.getString( "codciu") );
                    ciudad.setnomciu(rs.getString( "nomciu") );
                    Ciudades.add(ciudad);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR [ listarCiudades ] " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        
    }
    
    /**
     * Metodo <tt>listarCiudadesxpais</tt>, obtiene la lista de las ciudades dado pais
     * @autor : Ing. Jesus Cuesta
     * @param codigo del pais (String)
     * @version : 1.0
     */
    public void listarCiudadesxpais(String pais)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Ciudades = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(SQL_LCXPAIS);
                st.setString(1,pais);
                rs = st.executeQuery();
                
                Ciudades =  new LinkedList();
                
                while (rs.next()){
                    Ciudad ciudad = new Ciudad();
                    ciudad.setcodciu(rs.getString( "codciu") );
                    ciudad.setnomciu(rs.getString( "nomciu") );
                    ciudad.setpais_code( rs.getString("country_code") );
                    ciudad.setpais_name( rs.getString("country_name") );
                    Ciudades.add(ciudad);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR [ listarCiudadesxpais] " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        
    }
    
    public void listarCiudadesxpaisC(String pais)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Ciudades = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(SQL_LCXPAISC);
                st.setString(1,pais);
                rs = st.executeQuery();
                
                Ciudades =  new LinkedList();
                
                while (rs.next()){
                    Ciudad ciudad = new Ciudad();
                    ciudad.setcodciu(rs.getString( "codciu") );
                    ciudad.setnomciu(rs.getString( "nomciu") );
                    ciudad.setpais_code( rs.getString("country_code") );
                    ciudad.setpais_name( rs.getString("country_name") );
                    Ciudades.add(ciudad);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR [ listarCiudadesxpais] " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        
    }
    
    /**
     * Metodo <tt>listarAgencias</tt>, obtiene las agencias registradas en agasoc
     * @autor : Ing. Juan Escandon
     * @return List
     * @version : 1.0
     */
    public List listarAgencias() throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQL_AGENCIA);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    Ciudad c = new Ciudad();
                    c.setCodCiu(rs.getString("codciu"));
                    c.setNomCiu(rs.getString("nomciu"));
                    lista.add(c);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina ListarAgencias [CiudadDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return lista;
    }
    
    /**
     * Metodo <tt>listarCiudadesxPais</tt>, obtiene las agencias
     * @autor : Ing. Jesus Cuesta
     * @param codigo del pais (String), codigo de la ciudad (String)
     * @version : 1.0
     */
    public void buscarCiudadxPais(String cod_pais, String codciu ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement(SQL_BCXPAIS);
                st.setString(1, codciu);
                st.setString(2, cod_pais);
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    ciudad = new Ciudad();
                    ciudad.setcodciu(rs.getString( "codciu") );
                    ciudad.setnomciu(rs.getString( "nomciu") );
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA CIUDAD" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
    }
    
    /**
     * Metodo <tt>buscarNomCiudadxCodigo</tt>, obtiene el nombre de una ciudad dado un codigo
     * @autor : Ing. Jesus Cuesta
     * @param codigo de la ciudad (String)
     * @version : 1.0
     */
    public String buscarNomCiudadxCodigo(String codciu) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String ciu = "";
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement(SQL_NOMCIUDAD);
                
                st.setString(1, codciu);
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    ciu = rs.getString(1);
                }
            }
            return ciu;
        }catch(SQLException e){
            throw new SQLException("ERROR [ buscarNomCiudadxCodigo ]" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
    }
    
    /**
     * Metodo <tt>buscarCiudadesGeneral</tt>, obtiene la lista de las ciudades
     * @autor : Ing. Henry Osorio
     * @param  (String)
     * @version : 1.0
     */
    public void buscarCiudadesGeneral(String var) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        VecCiudades = new Vector();
        ciudad = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement(SQL_BUSCAR2);
                st.setString(1, var+"%");
                st.setString(2, var+"%");
                rs = st.executeQuery();
                while (rs.next()){
                    ciudad = new Ciudad();
                    ciudad.setNomCiu(rs.getString("nomciu"));
                    ciudad.setCodCiu(rs.getString("codciu"));
                    VecCiudades.addElement(ciudad);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR [ buscarCiudadesGeneral ]" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
    }
    
    /**
     * Metodo <tt>existeCiudad</tt>, verifica la existencia de una ciudad dado un codigo
     * @autor : Ing. Henry Osorio
     * @param  codigo de la ciudad (String)
     * @version : 1.0
     */
    public boolean existeCiudad( String codciu ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement(SQL_VERIFICAR);
                st.setString(1, codciu);
                rs = st.executeQuery();
                return rs.next();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR existeCiudad( String codciu ) " + e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
        return sw;
    }
    
    /**
     * Metodo buscarIndicativoCiudad, busca el indicativo de la ciudad
     * de acuerdo al codigo de la ciudad
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : String codigo ciudad
     * @version : 1.0
     */
    
    public int buscarIndicativoCiudad(String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        VecCiudades = new Vector();
        ciudad = null;
        int indicativo = 0;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement( BUSCAR_INDICATIVO );
                st.setString(1, cod);
                rs = st.executeQuery();
                while (rs.next()){
                    indicativo = rs.getInt("indicativo");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR EL INDICATIVO DE LA CIUDAD " + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
        return indicativo;
    }
    
    /**
     * Metodo que busca una ciudad dado su codigo
     * @autor: Karen Reales
     * @param: codigo de la ciudad
     * @throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarCiudad( String codciu ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement(SQL_BUSCAR3);
                st.setString(1, codciu);
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    ciudad = new Ciudad();
                    ciudad.setAgAsoc(rs.getString("agasoc"));
                    ciudad.setCodCiu(rs.getString("codciu"));
                    ciudad.setNomCiu(rs.getString("nomciu"));
                    ciudad.setZona(rs.getString("zona"));
                    ciudad.setReporteUrbano(false);
                    if(!rs.getString("tiene_rep_urbano").equals("N"))
                        ciudad.setReporteUrbano(true);
                    ciudad.setPais(rs.getString("pais"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA CIUDAD" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
    }
    
    // Los siguientes metodos y atributos fueron agregados de la clase CiudadDAO
    // de la aplicaci�n SOT. Alejandro Payares, 12.12.05
    
    /**
     * Obtiene una ciudad dado su codigo.
     * @param codciu. Codigo de la ciudad
     * @return la ciudad buscada
     * @throws SQLException si aparece un error en la base de datos
     */
    public Ciudad obtenerCiudad(String codciu) throws SQLException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        Ciudad ciudad = null;
        try {
            ps = this.crearPreparedStatement("SQL_OBTENER_CIUDAD");
            ps.setString(1, codciu );
            //System.out.println("Queryyy-->>"+ps.toString());
            rs = ps.executeQuery();
            if ( rs.next() ) {
                ciudad = Ciudad.load(rs) ;
            }
        } finally {
            if (rs != null) rs.close();
            if ( ps != null ) ps.close();
            desconectar("SQL_OBTENER_CIUDAD");
            return ciudad;
        }
    }
    
    /**
     * Obtiene las ciudades registradas
     * @autor Alejandro Payares
     * @throws SQLException si aparece un error en la base de datos
     */
    public void getCiudadSearch() throws SQLException {
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        Ciudad ciudad = null;
        
        try {
            
            pstmt = this.crearPreparedStatement("SQL_OBTENER_CIUDADES_NO_PC");
            pstmt.setString(1, "A" );
            rs = pstmt.executeQuery();
            ciudadList = new LinkedList();
            while (rs.next()) {
                ciudadList.add( Ciudad.load(rs) );
            }
        } finally {
            if (rs != null) rs.close();
            if ( pstmt != null ) pstmt.close();
            this.desconectar("SQL_OBTENER_CIUDADES_NO_PC");
        }
    }
    
    /**
     * Getter for property ciudadList
     * @return Value of property ciudadList
     */
    public List getCiudadList() {
        return ciudadList;
    }
    
    
    
    
    public void ActivarCiudad() throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement("update ciudad set  rec_status='', last_update='now()', user_update=? where pais=? and coddpt=? and codciu=?");
                st.setString(1,  ciudad.getUser_update() );
                st.setString(2, ciudad.getpais_code() );
                st.setString(3, ciudad.getdepartament_code() );
                st.setString(4, ciudad.getcodciu() );
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL MODIFICAR LA CIUDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
        
    }
    /**
     * Busca la lista de fronteras de un pais en la tabla ciudad
     * donde el campo frontera = Y o S
     * @param pais. Codigo del pais
     * @throws SQLException si aparece un error en la base de datos
     */
    public void listarFronteras(String pais) throws SQLException {
        Connection con= null;
        PreparedStatement ps= null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ciudades = new TreeMap();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                ps = con.prepareStatement(this.SLQ_FRONTERAS);
                ps.setString(1, pais );
                rs = ps.executeQuery();
                ciudades.put("Seleccione un item","");
                while( rs.next() ) {
                    ciudades.put(rs.getString("nomciu"),rs.getString("codciu"));
                    
                }
            }
        } finally {
            if (ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
    }
    /**
     * Metodo <tt>insertarCiudad</tt>, registra una ciudad en el sistema
     * @autor : Ing. Diogenes Bastidas
     * @version : 1.0
     */
    public void insertarCiudad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(SQL_INSERTAR);
                st.setString(1, ciudad.getcodciu() );
                st.setString(2, ciudad.getnomciu() );
                st.setString(3, ciudad.getpais_code() );
                st.setString(4, ciudad.getdepartament_code() );
                st.setString(5, ciudad.getUser_update() );
                st.setString(6, ciudad.getCreation_user() );
                st.setString(7, ciudad.getAplIca());
                st.setString(8, ciudad.getCodIca());
                st.setString(9, ciudad.getZona());
                st.setString(10, ciudad.getFrontera());
                st.setString(11, ciudad.getAgAsoc());
                st.setString(12, ciudad.getBase());
                st.setString(13, ciudad.getTipociu());
                st.setInt(14, ciudad.getIndicativo());
                st.setString(15, ciudad.getFrontera_asoc());
                st.setString(16, ciudad.getTiene_rep_urbano());
                st.setString(17, ciudad.getZona_urb());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR LA CIUDAD" + e.getMessage()+ " " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
    }
    
    /**
     * Metodo <tt>buscarCiudad</tt>, obtiene la ciudad dado pais, estado, ciudad
     * @autor : Ing. Diogenes Bastidas
     * @param codigo del pais (String), departamento (String), codigo de la ciudad (String)
     * @version : 1.0
     */
    public void buscarCiudad(String cod_pais, String cod_depto, String codciu ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null ){
                st = con.prepareStatement(SQL_BUSCAR);
                st.setString(1, codciu);
                st.setString(3, cod_pais);
                st.setString(2, cod_depto);
                rs = st.executeQuery();
                
                if (rs.next()){
                    ciudad = Ciudad.load3(rs);
                    ciudad.setFrontera_asoc(rs.getString("frontera_asoc"));
                    ciudad.setZona_urb(rs.getString("zona_urb"));
                    ciudad.setTiene_rep_urbano(rs.getString("tiene_rep_urbano"));
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR LA CIUDAD" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection(this.getDatabaseName(), con);
                }
            }
        }
    }
    
     /**
     * Metodo <tt>modificarCiudad</tt>, actualizar informacion de una ciudad
     * @autor : Ing. Diogenes Bastidas
     * @version : 1.0
     */
    public void modificarCiudad(String antest) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if (con != null){
                st = con.prepareStatement(SQL_MODIFICAR);
                st.setString(1, ciudad.getnomciu() );
                st.setString(2, ciudad.getUser_update() );
                st.setString(3, ciudad.getAplIca());
                st.setString(4, ciudad.getCodIca());
                st.setString(5, ciudad.getZona());
                st.setString(6, ciudad.getTipociu());
                st.setInt(7, ciudad.getIndicativo());
                st.setString(8, ciudad.getdepartament_code() );
                st.setString(9, ciudad.getFrontera_asoc());
                st.setString(10, ciudad.getTiene_rep_urbano());
                st.setString(11, ciudad.getZona_urb() );
                st.setString(12, ciudad.getAgAsoc() ); 
                st.setString(13, ciudad.getFrontera() );
                st.setString(14, ciudad.getpais_code() );
                st.setString(15, antest );
                st.setString(16, ciudad.getcodciu() );
                //System.out.println("Modificar  " + st);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL MODIFICAR LA CIUDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection(this.getDatabaseName(), con );
            }
        }
    }
    
    /**
     * Metodo <tt>buscarCiudad</tt>, obtiene la ciudad dado pais, estado, ciudad
     * @autor : Ing. Diogenes Bastidas
     * @param codigo del pais (String), departamento (String), codigo de la ciudad (String)
     * @version : 1.0
     */
    public void ciudadesIntermedias(String origen, String destino ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null,rs2=null;
        Hashtable h =new Hashtable();
        try{
            
            con = this.conectar("SQL_OBTENER_VIAS");
            if (con != null ){
                st = con.prepareStatement(this.obtenerSQL("SQL_OBTENER_VIAS"));
                st.setString(1, origen);
                st.setString(2, destino);
                rs = st.executeQuery();
                
                VecCiudades=new Vector();
                while (rs.next()){
                    String via2 = rs.getString("via");
                
                    
                    for (int i=2; i < via2.length() -4 ; i += 2 ){
                        
                        if(via2.length()>i+2){
                            
                            String ciudad  = via2.substring( i  , i+2 );
                
                            
                            st = con.prepareStatement(this.obtenerSQL("SQL_OBTENER_CIUDAD"));
                            st.setString(1,ciudad);
                            rs2 = st.executeQuery();
                            
                
                            
                            if(rs2.next()){
                                if(!rs2.getString("tipociu").equals("PC")){
                
                                    Ciudad c = new Ciudad();
                                    c.setCodCiu(rs2.getString("codciu"));
                                    c.setNomCiu(rs2.getString("nomciu"));
                                    
                                    if(h.get(rs2.getString("codciu"))==null){
                                        VecCiudades.add(c);
                                    }
                                    h.put(rs2.getString("codciu"),rs2.getString("nomciu"));
                                }
                            }
                            
                        }
                        
                    }
                    
                }
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL BUSCAR LA LISTA DE CIUDADES INTERMEDIAS " + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    this.desconectar("SQL_OBTENER_VIAS");
                }
            }
        }
    }
    
    public void  viasxCiudad(String origen, String destino, String ciudade ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null,rs2=null;
        vias = new TreeMap();
        try{
            
            con = this.conectar("SQL_OBTENER_VIASXCIUDAD");
            if (con != null ){
                
                String vec [] = ciudade.split(",");
                String condicion = "";
                for(int i =0; i< vec.length; i++){
                    String ciudad  = vec[i];
                    condicion = condicion + " AND POSITION ('"+ciudad+"' in via )>=1";
                }
                
                String query =this.obtenerSQL("SQL_OBTENER_VIASXCIUDAD").replaceAll("#CONDICION#",condicion);
                
                st = con.prepareStatement(query);
                st.setString(1, origen);
                st.setString(2, destino);
                rs = st.executeQuery();
                //System.out.println("Query "+st);
                while (rs.next()){
                    vias.put(rs.getString("descripcion"),rs.getString("origen")+rs.getString("destino")+rs.getString("secuencia"));
                }
                
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR LAS VIAS ASOCIADAS A LAS CIUDADES " + ciudades + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    this.desconectar("SQL_OBTENER_VIASXCIUDAD");
                }
            }
        }
        
    }
    
    /**
     * Getter for property vias.
     * @return Value of property vias.
     */
    public java.util.TreeMap getVias() {
        return vias;
    }    
    
    /**
     * Setter for property vias.
     * @param vias New value of property vias.
     */
    public void setVias(java.util.TreeMap vias) {
        this.vias = vias;
    }




     /**
     * M�todo que lista de los anticipos (no prontopagos) que no se an enviado por correo.
     * @autor   Jpinedo
     * @throws  Exception
     * @version 09-Jun-2011
     **/
        public String getCodDtp(String ciudad) throws Exception{
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_GET_DPT_CIUDAD";
        String dtp="";
        try{
            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, ciudad);
            rs=st.executeQuery();
            while( rs.next() )
            {
                dtp=rs.getString("coddpt");
            }

        }catch(Exception e){
            throw new Exception( " obtener departamento de ciudad" + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return dtp;

    }


            /**
     * Genera una lista de departamentos
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listadoDeps() throws Exception{
        ArrayList cadena = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_DEP";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while(rs.next()){
                cadena.add(rs.getString("department_code")+";_;"+rs.getString("department_name"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar departamentos: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return cadena;
    }



    /**
     * Genera una lista de ciudades
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listadoCiudades(String dept) throws Exception{
        ArrayList cadena = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_CIU";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setString(1, dept);
            rs=ps.executeQuery();
            while(rs.next()){
                cadena.add(rs.getString("codciu")+";_;"+rs.getString("nomciu"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar ciudades: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return cadena;
    }

}

