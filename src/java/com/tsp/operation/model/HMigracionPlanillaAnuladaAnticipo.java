/*
 * HMigracionRemesasAnuladas.java
 *
 * Created on 16 de julio de 2005, 04:37 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import java.util.Vector;
import java.io.*;

/**
 *
 * @author  
 */
public class HMigracionPlanillaAnuladaAnticipo extends Thread{
    private Vector con;
    private Vector sin;
    
    private FileWriter fw;
    private BufferedWriter bffw;
    private PrintWriter pntw;
    private FileWriter fw2;
    private BufferedWriter bffw2;
    private PrintWriter pntw2;
    
    private String usuario;
    private String path;
    
    
    /** Creates a new instance of HMigracionRemesasAnuladas */
    public HMigracionPlanillaAnuladaAnticipo() {
    }
    
    public void start(Vector con, Vector sin, String u) {
        this.con = con;
        this.sin = sin;
        this.usuario = u;
        
        try{
            //sandrameg 190905
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            path = rb.getString("ruta");
            
            //Planilla anulada sin anticipo
            String nombre_archivo = "";
            String ruta = path + "/exportar/migracion/" + usuario + "/";
            String fecha_actual = Util.getFechaActual_String(6);            
            //ruta = ruta + fecha_actual.substring(0,4) + fecha_actual.substring(5,7);
            nombre_archivo = ruta + "ANULAOC1 " + fecha_actual.replace(':','_').replace('/','_') + ".csv";            
            File archivo = new File(ruta.trim());
                
            archivo.mkdirs();
            FileOutputStream file = new FileOutputStream(nombre_archivo.trim());
            
            fw = new FileWriter(nombre_archivo);
            bffw = new BufferedWriter(fw);
            pntw = new PrintWriter(bffw);
            
            //planilla anulada con anticipo            
            String nombre_archivo2 = "";
            String ruta2 = path + "/exportar/migracion/" + usuario + "/";            
            //ruta2 = ruta2 + fecha_actual.substring(0,4) + fecha_actual.substring(5,7);
            nombre_archivo2 = ruta2 + "ANULAOC2 " + fecha_actual.replace(':','_').replace('/','_') + ".csv";            
            File archivo2 = new File(ruta2.trim());
                
            archivo2.mkdirs();
            FileOutputStream file2 = new FileOutputStream(nombre_archivo2.trim());
            
            fw2 = new FileWriter(nombre_archivo2);
            bffw2 = new BufferedWriter(fw2);
            pntw2 = new PrintWriter(bffw2);
        }
        catch(java.io.IOException e){
            ////System.out.println(e.toString());
        }
        
        super.start();
    }
    
    public synchronized void run(){
       this.writeFile();
    }
    
    protected void writeFile() {    	
    	String linea = "";   
        for(int i=0; i<sin.size(); i++){
            //Vector datos = (Vector) con.elementAt(i);
            linea = (String) sin.elementAt(i);
            pntw.println(linea);
        }
    	    	
        for(int i=0; i<con.size(); i++){
            Vector datos = (Vector) con.elementAt(i);
            linea = datos.elementAt(0) + "," + datos.elementAt(1) + "," +
                    datos.elementAt(2) + "," + datos.elementAt(3) + "," +
                    datos.elementAt(4) + "," + datos.elementAt(5) + "," + 
                    datos.elementAt(6) + "," + datos.elementAt(7) + "," +
                    datos.elementAt(8) + "," + datos.elementAt(9) + "," +
                    datos.elementAt(10) + "," + datos.elementAt(11) + "," +
                    datos.elementAt(12) + "," + datos.elementAt(13) + "," +
                    datos.elementAt(14) + "," + datos.elementAt(15) + "," +
                    datos.elementAt(16) + "," + datos.elementAt(17) + "," +
                    datos.elementAt(18) + "," + datos.elementAt(19) + "," +
                    datos.elementAt(20) + "," + datos.elementAt(21) + "," +
                    datos.elementAt(22);
            pntw2.println(linea);
        }
        pntw.close(); 
        pntw2.close();
    }    
}
