package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class Proveedor_AnticipoDAO{
    
    Proveedor_AnticipoDAO(){
        
    }
    
    private List proveedorAnticipo;
    private Proveedor_Anticipo pa;
    private Vector proveedores;
    private static final String SELECT_CONSULTA ="select p.dstrct," +
    "       c.nomciu," +
    "       p.nit," +
    "       p.sucursal, " +
    "       n.nombre " +
    "from   proveedor_anticipo p," +
    "       ciudad c, " +
    "       nit n " +
    "where  c.codciu = p.city_code and " +
    "       n.cedula = p.nit       and" +
    "       p.nit like ?         and" +
    "       n.nombre like ?     and" +
    "       c.nomciu  like ?     and" +
    "       p.reg_status = ''";
    
    List getProveedorAnticipo(){
        
        return proveedorAnticipo;
    }
    public Vector getProveedores(){
        
        return proveedores;
    }
    public void setProveedor(Proveedor_Anticipo pa){
        this.pa = pa;
    }
    public Proveedor_Anticipo getProveedor() throws SQLException{
        return pa;
    }
    
    public void consultar(String ciudad, String nombre, String nit )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        proveedores= null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SELECT_CONSULTA);
                st.setString(1,nit+"%");
                st.setString(2,nombre+"%");
                st.setString(3,ciudad+"%");
                
                rs = st.executeQuery();
                proveedores = new Vector();
                
                while(rs.next()){
                    pa = new Proveedor_Anticipo();
                    pa.setDstrct(rs.getString("dstrct"));
                    pa.setCiudad(rs.getString("nomciu"));
                    pa.setNit(rs.getString("nit"));
                    pa.setNombre(rs.getString("nombre"));
                    pa.setCodigo(rs.getString("sucursal"));
                    proveedores.add(pa);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROOVEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void searchProveedorAnticipo(String dstrct )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        proveedorAnticipo = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select p.*, n.nombre, c.nomciu as ciudad from proveedor_anticipo p, nit n, ciudad c where p.nit=n.cedula and c.codciu=p.city_code and p.dstrct = ? and p.reg_status=''");
                st.setString(1,dstrct);
                
                rs = st.executeQuery();
                proveedorAnticipo = new LinkedList();
                
                while(rs.next()){
                    proveedorAnticipo.add(Proveedor_Anticipo.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROOVEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void searchProveedor(String nit,String sucursal)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        pa = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select p.*, n.nombre, c.nomciu as ciudad from proveedor_anticipo p, nit n, ciudad c where p.nit=n.cedula and c.codciu=p.city_code and p.nit = ? and p.sucursal = ?");
                st.setString(1,nit);
                st.setString(2,sucursal);
                rs = st.executeQuery();
                
                if(rs.next()){
                   pa=Proveedor_Anticipo.load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROOVEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
    public boolean existProveedorAnticipo(String dstrct )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        proveedorAnticipo = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from proveedor_anticipo where dstrct = ? ");
                st.setString(1,dstrct);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROVEEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    public boolean existProveedor(String nit, String  sucursal)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        proveedorAnticipo = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from proveedor_anticipo where nit = ? and sucursal = ? ");
                st.setString(1,nit);
                st.setString(2,sucursal); 
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROVEEDOR  DE ANTICIPO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    
    
    public void insertProveedor(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into proveedor_anticipo (dstrct,city_code,nit,creation_date,creation_user,codigo_migracion,porcentaje,sucursal,base) values(?,?,?,'now()',?,?,?,?,?)");
                st.setString(1, pa.getDstrct());
                st.setString(2,pa.getCity_code());
                st.setString(3, pa.getNit());
                st.setString(4,pa.getCreation_user());
                st.setString(5,pa.getCod_Migracion());
                st.setFloat(6, pa.getPorcentaje());
                st.setString(7, pa.getCodigo());
                st.setString(8, base);
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS PROVEEDOR_ACPM " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public void updateProveedor()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update proveedor_anticipo set city_code=?, user_update=?, dstrct=? where nit=? and sucursal=?");
                st.setString(1,pa.getCity_code());
                st.setString(2, pa.getCreation_user());
                st.setString(3,pa.getDstrct());
                st.setString(4,pa.getNit());
                st.setString(5, pa.getCodigo());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL PROVEEDOR ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public void anularProveedor()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update proveedor_anticipo set reg_status='A' where nit=? and sucursal=?");
                st.setString(1,pa.getNit());
                st.setString(2,pa.getCodigo());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL PROVEEDOR ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
   /********************Diogenes ******************************************
    */
    public boolean existProveedorAnulado(String nit, String  sucursal)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        proveedorAnticipo = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from proveedor_anticipo where nit = ? and sucursal = ? and reg_status='A' ");
                st.setString(1,nit);
                st.setString(2,sucursal);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROVEEDOR  DE ANTICIPO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    public void activarProveedor()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update proveedor_anticipo set city_code=?, user_update=?, dstrct=?,reg_status='' where nit=? and sucursal=?");
                st.setString(1,pa.getCity_code());
                st.setString(2, pa.getCreation_user());
                st.setString(3,pa.getDstrct());
                st.setString(4,pa.getNit());
                st.setString(5, pa.getCodigo());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL PROVEEDOR ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }

    
    
}





