/*
 * RemesaDestDAO.java
 *
 * Created on 7 de diciembre de 2004, 05:28 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  KREALES
 */
public class RemesaDestDAO {
    
    private List remdes;
    private List remitentes;
    private List destinatarios;
    private RemesaDest rd;
    /** Creates a new instance of RemesaDestDAO */
    public RemesaDestDAO() {
    }
    public void setRemDes(RemesaDest rd){
        this.rd=rd;
    }
    public RemesaDest getRemDes(){
        return this.rd;
    }
    public List getList(){
        return remdes;
    }
    public List getRemitentes(){
        return remitentes;
    }
    public List getDestinatarios(){
        return destinatarios;
    }
    public void searchLista(String numrem )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        remdes = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select * from remesadest where numrem=? and reg_status=''");
                st.setString(1,numrem);
                
                rs = st.executeQuery();
                remdes = new LinkedList();
                
                while(rs.next()){
                    remdes.add(RemesaDest.load(rs));
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMSAS-DESTINATARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public boolean estaDest(String numrem, String dest )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        remdes = null;
        PoolManager poolManager = null;
        boolean sw= false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select * from remesadest where numrem=?  and  codigo= ?");
                st.setString(1,numrem);
                st.setString(2,dest);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw= true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMSAS-DESTINATARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public void searchRemitentes(String numrem )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        remitentes = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select rd.nombre from remesadest r, remidest rd where r.numrem=? and r.reg_status='' and r.tipo='RE' and r.codigo = rd.codigo");
                st.setString(1,numrem);
                
                rs = st.executeQuery();
                remitentes = new LinkedList();
                
                while(rs.next()){
                    rd = new RemesaDest();
                    rd.setNombre(rs.getString("nombre"));
                    remitentes.add(rd);
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMSAS-DESTINATARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void searchDestinatarios(String numrem )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        destinatarios= null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select rd.nombre from remesadest r, remidest rd where r.numrem=? and r.reg_status='' and r.tipo='DE' and r.codigo = rd.codigo");
                st.setString(1,numrem);
                
                rs = st.executeQuery();
                destinatarios = new LinkedList();
                
                while(rs.next()){
                    rd = new RemesaDest();
                    rd.setNombre(rs.getString("nombre"));
                    destinatarios.add(rd);
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMSAS-DESTINATARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public String insert(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into remesadest (dstrct, codigo, numrem, tipo, creation_user,base) values(?,?,?,?,?,?)");
                st.setString(1,rd.getDstrct());
                st.setString(2, rd.getCodigo());
                st.setString(3, rd.getNumrem());
                st.setString(4, rd.getTipo());
                st.setString(5, rd.getUsuario());
                st.setString(6, base);
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS REMESAS DESTINO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
        
    }
    public void update()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("delete from remesadest where codigo=? and numrem=?");
                st.setString(1, rd.getCodigo());
                st.setString(2, rd.getNumrem());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LAS REMESAS DESTINATARIOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public void anular()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update remesadest set reg_status='A' where norem=?");
                st.setString(1,rd.getNumrem());
                
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LAS REMESAS DESTINATARIOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
}
