
/************************************************************************************
 * Nombre clase : ............... ImpresionChequeDAO.java                           *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )   *
 *                                los cuales contienen los metodos que interactuan  *
 *                                con la tabla para generar Cheques                 *
 * Autor :....................... Ing. Fernel Villacob                              *
 * Fecha :....................... 26 de Enero de 2006                               *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ************************************************************************************/




package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
//import com.tsp.operation.model.DAOS.OpDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.services.TasaService;
import com.tsp.operation.model.DAOS.ChequeXFacturaDAO;



public class ImpresionChequeDAO extends MainDAO{
    
    
    //____________________________________________________________________________________________
    //                                          ATRIBUTOS
    //____________________________________________________________________________________________
    
    
    
    
     private String  SEPARADOR    = "<BR>";
     
     
     
    
    //____________________________________________________________________________________________
    //                                          METODOS
    //____________________________________________________________________________________________
    
     
     
     
     
     
    public ImpresionChequeDAO() {
        super("ImpresionChequeDAO.xml");
    }
    public ImpresionChequeDAO(String dataBaseName) {
        super("ImpresionChequeDAO.xml", dataBaseName);
    }
    
    
    synchronized String ActualizarMOVOC(ImpresionCheque cheque, String usuario)throws Exception {
        
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        String            sql     = "";
        
        String            query   = "SQL_UPDATE_MOVOC";
        String            query2  = "SQL_INSERT_EGRESO";
        String            query3  = "SQL_INSERT_EGRESODET";
        
        
        try{
            
            String  fecha = Utility.getDate(4);  // YYYY-MM-DD
            TasaService       svc     =  new  TasaService(this.getDatabaseName());
            ChequeXFacturaDAO dao     =  new  ChequeXFacturaDAO(this.getDatabaseName());
            
            
            
            String  num         = cheque.getCheque();            
            
            /*
            String  num         = cheque.getCheque();
            String  iniSerie    = cheque.getInicioSerie();
            int     lastNumber  = cheque.getNumber(); 
            
            Series serie = this.getSeries(cheque.getDistrito(), cheque.getAgencia(), cheque.getBanco(), cheque.getAgenciaBanco(), cheque.getCuenta());
            if( serie!=null){
                num         =  serie.getPrefijo() + serie.getLast_number();
                iniSerie    =  serie.getSerial_initial_no();
                lastNumber  =  serie.getLast_number();
            }
            
            cheque.setCheque      ( num        );
            cheque.setInicioSerie ( iniSerie   );
            cheque.setNumber      ( lastNumber );
            */
            
            
            // MOVPLA:
            
            String[] vec = cheque.getPlanilla().split( SEPARADOR );
            st= this.crearPreparedStatement(query);
            for(int i=0;i<vec.length;i++){
                String oc = vec[i];                  
                // set
                st.setString(1,  num     );
                st.setString(2,  usuario );
                st.setString(3,  fecha   );

                // where
                st.setString(4,  oc      );
                st.setString(5,  cheque.getDistrito()     );
                st.setString(6,  cheque.getBanco()        );
                st.setString(7,  cheque.getAgenciaBanco() );
                
                st.setString(8,  cheque.getCedula()       );     //Beneficiario.           
                st.setString(9,  usuario                  );
                             
                sql += st.toString();
                
            }
            
           
            
            String   monedaCheque = cheque.getMoneda();
            String   monedaLocal  = dao.getMonedaLocal(cheque.getDistrito());
            
            
            double    vlrLocal = cheque.getValor();
            double    vlrME    = cheque.getValor();
            double    tasa     = 1; 
            
            //System.out.println("MONEDAS :" +monedaCheque +"|"+ monedaLocal  );
            
            if(!monedaCheque.equals(monedaLocal) ){
                  Tasa  obj    =  svc.buscarValorTasa(monedaLocal,  monedaCheque , monedaLocal , fecha);
                  if(obj!=null){
                        tasa      =  obj.getValor_tasa();
                        vlrLocal  =  vlrME *  tasa;
                        vlrLocal  =  ( monedaLocal.equals("PES")  ||  monedaLocal.equals("BOL")  ) ? (int)Math.round(vlrLocal) : Util.roundByDecimal(vlrLocal, 2);                        
                  }
              }     
            
            
            
            // EGRESO
            st = this.crearPreparedStatement(query2);
            st.setString(1,  cheque.getDistrito()       );
            st.setString(2,  cheque.getBanco()          );
            st.setString(3,  cheque.getAgenciaBanco()   );
            st.setString(4,  num                        );
            st.setString(5,  cheque.getPropietario ()   );
            st.setString(6,  cheque.getBeneficiario()   );
            st.setString(7,  cheque.getAgencia()        );
            st.setString(8,  "01"                       );
            st.setDouble(9,  vlrLocal                   );
            st.setDouble(10, vlrME                      );
            st.setString(11, cheque.getMoneda()         );
            st.setString(12, cheque.getUsuario()        );
            st.setString(13, "004"                      );
            st.setString(14, cheque.getUsuario()        );
            st.setString(15, cheque.getCedula()         );
            st.setString(16, cheque.getProveedor_anticipo() );
            st.setDouble(17, tasa                       );
            st.setString(18, cheque.getProveedor_anticipo().equals("890103161")?"S":"N"   );
             sql = sql + st.toString();
             
            
           // EGRESODET
            String[] vecVlr  = cheque.getValores().split( SEPARADOR );
            st = this.crearPreparedStatement(query3);
            for(int i=0;i<vec.length;i++){
                String oc    = vec[i];
                double valor = Double.parseDouble( vecVlr[i] );
                
                    vlrLocal  =  valor  * tasa;
                    vlrLocal  =  ( monedaLocal.equals("PES")  ||  monedaLocal.equals("BOL")  ) ? (int)Math.round(vlrLocal) : Util.roundByDecimal(vlrLocal, 2);                        
                    vlrME     =  valor ;

                
                    st.setString(1,  cheque.getDistrito()     );
                    st.setString(2,  cheque.getBanco()        );
                    st.setString(3,  cheque.getAgenciaBanco() );
                    st.setString(4,  num                      );
                    st.setString(5,  Utility.rellenar( String.valueOf( i+1 ) ,3) );
                    st.setString(6,  "01"               );
                    st.setDouble(7,  vlrLocal           );
                    st.setDouble(8,  vlrME              );
                    st.setString(9,  cheque.getMoneda() );
                    st.setString(10, oc                 );
                    st.setString(11, cheque.getUsuario());
                    st.setString(12, "CHEQUE ANTICIPO DE LA PLANILLA "+oc);
                    st.setDouble(13, tasa                       );
                    st.setString(14, "001"                      );//jose 2007-02-27
                    st.setString(15, "C"                        );//jose 2007-02-27
                    st.setString(16, oc                         );//jose 2007-02-27
                    
               sql = sql + st.toString() ;
            }   
           
            
            
        }catch(Exception e){
            throw new SQLException("No se pudo actualizar en MOVOC "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
            this.desconectar(query2);
            this.desconectar(query3);
        }
        return sql;
    }
    
    
    
    
    
    /**
     * M�todo que carga el sql para la actualizacion de la Serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
    synchronized String ActualizarSeries(ImpresionCheque cheque, String usuario)throws Exception {
        
        
        PreparedStatement st      = null;
        String            sql     = "";
        String            query   = "SQL_UPDATE_SERIE";
        try{
          
            
            //--- realizamos la actualizacion
            st= this.crearPreparedStatement(query);
            st.setInt   (1, cheque.getNumber() +  1  );
            st.setString(2, usuario                  );
            st.setString(3, cheque.getDistrito()     );
            st.setString(4, cheque.getBanco()        );
            st.setString(5, cheque.getAgenciaBanco() );
            st.setString(6, cheque.getInicioSerie()  );
            
            sql = st.toString();
            ////System.out.println("Actualizar Serie : "+st.toString());
            
            
        }catch(Exception e){
            throw new SQLException("No se pudo actualizar la SERIE " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return sql;
    }
    
    
    
    
    
    /**
     * M�todo que finaliza la serie
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
    void FinalizarSeries(ImpresionCheque cheque, String usuario)throws Exception {
       
        PreparedStatement st      = null;
        String            query   = "SQL_UPDATE_FINALIZAR_SERIE";
        try{            
           
            st= this.crearPreparedStatement(query);
            st.setString(1, usuario);
            st.setString(2, cheque.getDistrito());
            st.setString(3, cheque.getBanco());
            st.setString(4, cheque.getAgenciaBanco());
            st.setString(5, cheque.getInicioSerie() );
            
            
            
            st.executeUpdate();
            ////System.out.println("Finalizar serie: "+st.toString());
        }catch(Exception e){
            throw new SQLException("No se pudo finalizar la SERIE "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
    }
    
    
   
    
    /**
     * M�todo que buscar nombre del usuario
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String user
     * @version..... 1.0.
     **/
    String  getUserName(String user) throws Exception{        
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        String            query   = "SQL_USER_NAME";
        String            name    = "";        
        try{
            st = this.crearPreparedStatement(query);
            st.setString(1, user);
            rs = st.executeQuery();
            if(rs.next())
                name = rs.getString(1);
                
        }catch(Exception e){
            throw new SQLException("Error en la busqueda del nombre del usuario --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return name;
    }
    
    
    
    /**
     * M�todo que buscar nombre de la agencia
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String codigo
     * @version..... 1.0.
     **/
    String  getAgencyName(String codigo) throws Exception{        
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        String            query   = "SQL_AGENCY_NAME";
        String            name    = "";
        try{
            st = this.crearPreparedStatement(query);
            st.setString(1, codigo);
            rs = st.executeQuery();
            if(rs.next())
                name = rs.getString(1);
                
        }catch(Exception e){
            throw new SQLException("Error en la busqueda del nombre de la agencia --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return name;
    }
    
    
    
    
    
    
    
    
    
    
    
    
     
    /**
     * M�todo que carga el objeto cheque con los datos del registro de  la consulta
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ResultSet rs 
     * @version..... 1.0.
     **/
        
    
    
    
    /**
     * M�todo que setea valores nulos
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter...String val
     * @version..... 1.0.
     **/
    public String reset(String val){
        if(val==null)
            val = "";
        return val;
    }
    
    
    
    
    
    
     /**
     * M�todo que carga el listado de bancos para la planilla
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String planilla
     * @version..... 1.0.
     **/
    List  ListBancos(String distrito, String planilla) throws Exception{
        
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        String            query   = "SQL_SEARCH_BANCOS";
        
        try{
            st= this.crearPreparedStatement(query);
            st.setString(1, planilla);
            st.setString(2, distrito);
            ////System.out.println("Bancos "+st.toString());
            rs=st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    Banco  banco = new Banco();
                    banco.setBanco(rs.getString(1));
                    banco.setNombre_Agencia(rs.getString(2));
                    banco.setCuenta( rs.getString(3));
                    banco.setDescripcion(rs.getString(1)+" "+rs.getString(3));
                    listado.add(banco);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de los cheques --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return listado;
        
    }
    
    
    
    
     /**
     * M�todo que carga la serie pra el banco y sucursal
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String agencia, String banco, String agenciaBanco, String cuenta
     * @version..... 1.0.
     **/
    Series  getSeries(String distrito, String agencia, String banco, String agenciaBanco, String cuenta) throws Exception{
        
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        Series            serie   = null;
        String            query   = "SQL_SEARCH_SERIES";
        try{
            st=  this.crearPreparedStatement(query);
            st.setString(1, distrito);
            st.setString(2, banco);
            st.setString(3, agenciaBanco);
            ////System.out.println("Serie: "+st.toString());
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next()){                    
                    if( Integer.parseInt(rs.getString(2)) >=   rs.getInt(3)  ){
                        serie = new Series();
                        serie.setPrefijo           ( rs.getString(1)        );
                        serie.setSerial_fished_no  ( rs.getString(2)        );   // Tope
                        serie.setLast_number       ( rs.getInt(3)           );   // proximo a imprimir
                        serie.setSerial_initial_no ( rs.getString("INICIO") );
                        break;
                    }
                }
            
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de los cheques --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return serie;
        
    }
    
    
    
    
     /**
     * M�todo que carga el esquema de impresion para el banco
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String banco
     * @version..... 1.0.
     **/
    List  searchEsquema(String distrito, String banco)throws Exception {
       
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        String            query   = "SQL_SEARCH_ESQUEMA";
        try{
            st=  this.crearPreparedStatement(query);
            st.setString(1, distrito);
            st.setString(2, banco);
            rs = st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    EsquemaCheque esquema = new EsquemaCheque();
                    esquema.setDistrito( rs.getString(1));
                    esquema.setBanco(rs.getString(2));
                    esquema.setCPI(rs.getInt(3));
                    esquema.setLinea(rs.getInt(4));
                    esquema.setColumna(rs.getInt(5));
                    esquema.setDescripcion(rs.getString(6));
                    listado.add(esquema);
                }
            }
        }catch(Exception e){
            throw new SQLException(" No se pudieron listar los registros en Esquema Cheque -->" + e.getMessage() );
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return listado;
    }
    
    
    
    
    
    
    
    //KAREN REALES
    
    /**
     * M�todo que carga datos del banco para el  banco
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String bank
     * @version..... 1.0.
     **/
    Banco  searchBanco(String bank) throws Exception{
       
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        Banco             banco    = new Banco();
        String            query    = "SQL_SEARCH_BANCO";
        try{
            String vec []= bank.split("/");
            String codigo = vec[0];
            String cuenta = vec[1];
            
            st= this.crearPreparedStatement(query);
            st.setString(1, codigo);
            st.setString(2, cuenta);
            rs=st.executeQuery();
            if(rs.next()){                
                banco.setBanco(rs.getString(1));
                banco.setNombre_Agencia(rs.getString(2));
                banco.setCuenta( rs.getString(3));
                banco.setDescripcion(rs.getString(1)+" "+rs.getString(3));
            }
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de los cheques --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();            
            this.desconectar(query);
        }
        return banco;
    }
    
    
    
     /**
     * M�todo que cambia el banco para la planilla en la tabla movpla
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String planilla, String banco
     * @version..... 1.0.
     **/
    public void  cambiarBanco(String planilla, String bancoAnt, String bancoNew, double vlrtasa, String moneda) throws Exception{
        
       
        ImpresionCheque   cheque = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        String            query   = "SQL_UPDATE_MOVPLA";
        
        try{
            
            
            // banco Anterior:
            String vecAnt []    = bancoAnt.split("/");
            String codigoAnt    = vecAnt[0];
            String cuentaAnt    = vecAnt[1];
            
            // banco Nuevo:
            String vecNew []  = bancoNew.split("/");
            String codigoNew  = vecNew[0];
            String cuentaNew  = vecNew[1];
            
            
            
            
            st= st= this.crearPreparedStatement(query);
            // set:
            st.setString(1, codigoNew    );
            st.setString(2, cuentaNew    );
            st.setDouble(3, vlrtasa    );
            st.setString(4, moneda   );
            // where
            st.setString(5, planilla     );
            st.setString(6, codigoAnt    );
            st.setString(7, cuentaAnt    );
            
            //System.out.println("QUERY CHEQUE "+st.toString());
            st.executeUpdate();
            
            
            
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de un cheque --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        
        
    }
    
    /**
     * M�todo que carga el listado de cheques a imprimir
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String agencia, String base, String user
     * @version..... 1.0.
     **/
    List ListCheques(String distrito, String agencia, String base, String user) throws Exception{
        
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        String            query   = "SQL_SEARCH_CHEQUES_USUARIO";
        
        try{     
         
            
            user                      = user.toUpperCase();      
            
            
          //SE BUSCAN LOS CHEQUES HECHOS POR EL USUARIO
            st = this.crearPreparedStatement(query);
            st.setString(1, distrito    );
            st.setString(2, user);
            st.setString(3, distrito);
            rs=st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                
                String  nombreUser   = this.getUserName  ( user    );
                String  nombreAgency = this.getAgencyName( agencia );
                
                int cont=1;
                while(rs.next()){
                    ImpresionCheque cheque = this.loadCheque(rs);
                        cheque.setId      ( cont      );
                        cheque.setDistrito( distrito  );
                        cheque.setAgencia ( agencia   );
                        cheque.setUsuario ( user      );
                        
                        cheque.setNombreAgencia ( nombreAgency );
                        cheque.setNombreUsurio  ( nombreUser   );                        
                        
                    cont++;
                    listado.add(cheque);
                }
            }
            
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de los cheques --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return listado;
        
    }
    
    /**
     * M�todo que carga el listado de cheques a imprimir por pronto pago de fintra
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String agencia, String base, String user
     * @version..... 1.0.
     **/
    List ListChequesPorProntoPago (String distrito, String agencia, String base, String user, String []ids ) throws Exception{
        
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        String            query   = "SQL_SEARCH_CHEQUES_PROVEEDOR_ANTICIPO";
        
        try{     
            user = user.toUpperCase();    
            String lista = "";
            for (int i = 0; i< ids.length; i++){
                lista +=  (i!=0?",":"") + "'" + ids[i] + "'";
            }
            String sql = this.obtenerSQL(query).replaceAll("#IDS#",lista);
            
            Connection con =  this.conectar(query);
            st = con.prepareStatement(sql);
            st.setString(1, distrito    );
            st.setString(2, "802022016"); // proveedor anticipo
            st.setString(3, distrito);
            rs=st.executeQuery();
            
            
            if(rs!=null){
                listado = new LinkedList();
                
                String  nombreUser   = this.getUserName  ( user    );
                String  nombreAgency = this.getAgencyName( agencia );

                int cont=1;
                while(rs.next()){
                    ImpresionCheque cheque = this.loadCheque(rs);
                        cheque.setId      ( cont      );
                        cheque.setDistrito( distrito  );
                        cheque.setAgencia ( agencia   );
                        cheque.setUsuario ( user      );
                        
                        cheque.setNombreAgencia ( nombreAgency );
                        cheque.setNombreUsurio  ( nombreUser   );                        
                        
                    cont++;
                    listado.add(cheque);
                }
            }

        }catch(Exception e){
            throw new SQLException("Error en la busqueda de los cheques --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return listado;
        
    }

    
    /**
     * M�todo que verifica si existe el banco al usuario
     * @autor....... jdelarosa
     * @throws...... Exception
     * @parameter... String user
     * @version..... 1.0.
     **/
    boolean getBancoUsuario (String banco, String sucursal, String user) throws Exception{        
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        String            query   = "SQL_USER_BRANCH";
        boolean           name    = false;        
        try{
            st = this.crearPreparedStatement(query);
            st.setString(1, banco);
            st.setString(2, sucursal);
            st.setString(3, user);
            rs = st.executeQuery();
            name = rs.next();
                
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de los bancos del usuario --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return name;
    }
    
    



    synchronized String ActualizarMOVOC_PRONTO_PAGO(ImpresionCheque cheque, String usuario)throws Exception {
        
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        String            sql     = "";
        
        String            query   = "SQL_UPDATE_MOVOC_PP";
        String            query2  = "SQL_INSERT_EGRESO";
        String            query3  = "SQL_INSERT_EGRESODET";
        
        
        try{
            
            String  fecha = Utility.getDate(4);  // YYYY-MM-DD
            TasaService       svc     =  new  TasaService(this.getDatabaseName());
            ChequeXFacturaDAO dao     = new ChequeXFacturaDAO(this.getDatabaseName());
            
            
            String  num         = cheque.getCheque();
            String  iniSerie    = cheque.getInicioSerie();
            int     lastNumber  = cheque.getNumber(); 
            
            Series serie = this.getSeries(cheque.getDistrito(), cheque.getAgencia(), cheque.getBanco(), cheque.getAgenciaBanco(), cheque.getCuenta());
            if( serie!=null){
                num         =  serie.getPrefijo() + serie.getLast_number();
                iniSerie    =  serie.getSerial_initial_no();
                lastNumber  =  serie.getLast_number();
            }
            
            cheque.setCheque      ( num        );
            cheque.setInicioSerie ( iniSerie   );
            cheque.setNumber      ( lastNumber );
            
            
            
            
            ////System.out.println("ACTUALIZACIONES *************************");
            
            // MOVPLA:
            
            String[] vec = cheque.getPlanilla().split( SEPARADOR );
            st= this.crearPreparedStatement(query);
            for(int i=0;i<vec.length;i++){
                String oc = vec[i];                  
                // set
                st.setString(1,  num     );
                st.setString(2,  usuario );
                st.setString(3,  fecha   );

                // where
                st.setString(4,  oc      );
                st.setString(5,  cheque.getDistrito()     );
                st.setString(6,  cheque.getBanco()        );
                st.setString(7,  cheque.getAgenciaBanco() );
                
                st.setString(8,  cheque.getCedula()       );     //Beneficiario.           
                
                sql += st.toString();
                
            }
            
           
            
            String   monedaCheque = cheque.getMoneda();
            String   monedaLocal  = dao.getMonedaLocal(cheque.getDistrito());
            
            
            double    vlrLocal = cheque.getValor();
            double    vlrME    = cheque.getValor();
            double    tasa     = 1; 
            
            //System.out.println("MONEDAS :" +monedaCheque +"|"+ monedaLocal  );
            
            if(!monedaCheque.equals(monedaLocal) ){
                  Tasa  obj    =  svc.buscarValorTasa(monedaLocal,  monedaCheque , monedaLocal , fecha);
                  if(obj!=null){
                        tasa      =  obj.getValor_tasa();
                        vlrLocal  =  vlrME *  tasa;
                        vlrLocal  =  ( monedaLocal.equals("PES")  ||  monedaLocal.equals("BOL")  ) ? (int)Math.round(vlrLocal) : Util.roundByDecimal(vlrLocal, 2);                        
                  }
              }     
            
            
            
            // EGRESO
            st = this.crearPreparedStatement(query2);
            st.setString(1,  cheque.getDistrito()       );
            st.setString(2,  cheque.getBanco()          );
            st.setString(3,  cheque.getAgenciaBanco()   );
            st.setString(4,  num                        );
            st.setString(5,  cheque.getPropietario ()   );
            st.setString(6,  cheque.getBeneficiario()   );
            st.setString(7,  cheque.getAgencia()        );
            st.setString(8,  "01"                       );
            st.setDouble(9,  vlrLocal                   );
            st.setDouble(10, vlrME                      );
            st.setString(11, cheque.getMoneda()         );
            st.setString(12, cheque.getUsuario()        );
            st.setString(13, "004"                      );
            st.setString(14, cheque.getUsuario()        );
            st.setString(15, cheque.getCedula()         );
            st.setString(16, cheque.getProveedor_anticipo() );
            st.setDouble(17, tasa                       );
             sql = sql + st.toString();
             
            
           // EGRESODET
            String[] vecVlr  = cheque.getValores().split( SEPARADOR );
            st = this.crearPreparedStatement(query3);
            for(int i=0;i<vec.length;i++){
                String oc    = vec[i];
                double valor = Double.parseDouble( vecVlr[i] );
                
                    vlrLocal  =  valor  * tasa;
                    vlrLocal  =  ( monedaLocal.equals("PES")  ||  monedaLocal.equals("BOL")  ) ? (int)Math.round(vlrLocal) : Util.roundByDecimal(vlrLocal, 2);                        
                    vlrME     =  valor ;

                
                    st.setString(1,  cheque.getDistrito()     );
                    st.setString(2,  cheque.getBanco()        );
                    st.setString(3,  cheque.getAgenciaBanco() );
                    st.setString(4,  num                      );
                    st.setString(5,  Utility.rellenar( String.valueOf( i+1 ) ,3) );
                    st.setString(6,  "01"               );
                    st.setDouble(7,  vlrLocal           );
                    st.setDouble(8,  vlrME              );
                    st.setString(9,  cheque.getMoneda() );
                    st.setString(10, oc                 );
                    st.setString(11, cheque.getUsuario());
                    st.setString(12, "CHEQUE ANTICIPO DE LA PLANILLA "+oc);
                    st.setDouble(13, tasa                       );
                    st.setString(14, "001"                      );//jose 2007-02-27
                    st.setString(15, "C"                        );//jose 2007-02-27
                    st.setString(16, oc                         );//jose 2007-02-27
                    
               sql = sql + st.toString() ;
            }   
            ////System.out.println( sql + "\n");
            
            
        }catch(Exception e){
            throw new SQLException("No se pudo actualizar en MOVOC "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
            this.desconectar(query2);
            this.desconectar(query3);
        }
        return sql;
    }
    
    public ImpresionCheque loadCheque(ResultSet rs)throws Exception{
        ImpresionCheque cheque = new ImpresionCheque();
        try{
            
            cheque.setPlanilla          ( reset(rs.getString("planilla")     ));
            cheque.setBanco             ( reset(rs.getString("banco")        ));
            cheque.setAgenciaBanco      ( reset(rs.getString("sucursal")     ));
            cheque.setMoneda            ( reset(rs.getString("moneda")       ));
            cheque.setValor             (       rs.getDouble("valor")         );
            cheque.setCedula            ( reset(rs.getString("cedula")       ));
            cheque.setBeneficiario      ( reset(rs.getString("beneficiario") ));
            cheque.setValores           ( reset(rs.getString("valor")        ));
            cheque.setPropietario       ( reset(rs.getString("propietario") ));
            cheque.setProveedor_anticipo( reset(rs.getString("proveedor_anticipo")  ));
            cheque.setNombre_propietario( reset(rs.getString("nombre_propietario")  ));
            
            /*JEscandon 2006-12-13*/
            /*0099-01-01 00:00:00 = false ( No impreso ) - true = Impreso */
            boolean flag = ( reset(rs.getString("printer_date")).equals("0099-01-01 00:00:00"))?false:true;
            cheque.setEstado_impresion_oc(flag);
            cheque.setFecha_imp_oc(reset(rs.getString("printer_date"))); 
            cheque.setAuxiliar(reset(rs.getString("printer_date")) + "/" + reset(rs.getString("planilla")));
            
                        
        }catch(Exception e){
            throw new Exception("DAO: loadCheque --->" + e.getMessage());
        }
        return cheque;
    }
     /**
     * M�todo que carga el esquema de impresion para el banco
     * @autor....... Osvaldo P�rez
     * @throws...... Exception
     * @parameter... String distrito, String banco
     * @version..... 1.0.
     **/
    List searchEsquemaCMS(String banco)throws Exception {
       
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        String            query   = "SQL_SEARCH_ESQUEMA_CMS";
        try{
            st=  this.crearPreparedStatement(query);            
            st.setString(1, banco);
            
            System.out.println(st.toString());
            
            rs = st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    EsquemaCheque esquema = new EsquemaCheque();
                    
                    esquema.setBanco( rs.getString("banco") );
                    esquema.setCampo( rs.getString("campo") );
                    esquema.setTop(   rs.getDouble("top") );
                    esquema.setLeft(  rs.getDouble("leftt") );
                    
                    listado.add(esquema);
                }
            }
        }catch(Exception e){
            throw new SQLException(" ERROR en ImpresionChequeDAO.searchEsquemaCMS -->" + e.getMessage() );
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return listado;
    }
}// DAO
