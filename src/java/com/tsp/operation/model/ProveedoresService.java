/*
 * ProveedoresServicio.java
 *
 * Created on 19 de abril de 2005, 04:54 PM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  kreales
 */
public class ProveedoresService {
    
    private ProveedoresDAO proveedor;
    /** Creates a new instance of ProveedoresServicio */
    public ProveedoresService() {
        proveedor = new ProveedoresDAO();
    }
    public ProveedoresService(String dataBaseName) {
        proveedor = new ProveedoresDAO(dataBaseName);
    }
    public Proveedores getProveedor(){
        return proveedor.getProveedore();
    }
    public void setProveedor(Proveedores p){
        proveedor.setProveedores(p);
    }
    public void insertProveedor(String base)throws SQLException{
        proveedor.insertProveedor(base);
        
    }
    public void updateProveedor()throws SQLException{
        proveedor.updateProveedor();
    }
    public void anularProveedor()throws SQLException{
        proveedor.anularProveedor();
    }
    public void vecProveedor(String base)throws SQLException{
        proveedor.vecProveedor(base);
    }
    public void searchProveedor(String nit, String sucursal)throws SQLException{
        proveedor.searchProveedor(nit, sucursal);
    }
    public boolean existNit(String nit)throws SQLException{
        return proveedor.existNit(nit);
    }
    public Vector getProveedores(){
        
        return proveedor.getProveedores();
    }
    public void searchAnticipos(String base)throws SQLException{
        proveedor.searchAnticipos(base);
    }
    public Vector getAnticipos(){
        return proveedor.getAnticipos();
    }
    public boolean estaAnticipoProvee(String nit, String sucursal, String codigo)throws SQLException{
        
        return proveedor.estaAnticipoProvee(nit,sucursal,codigo);
    }
    public void listaAnticipoProvee(String  codigo)throws SQLException{
        proveedor.listaAnticipoProvee(codigo);
    }
    public void deleteAnticipoProvee(String  codigo, String nit, String sucursal)throws SQLException{
        proveedor.deleteAnticipoProvee(codigo, nit, sucursal);
    }
    
}
