/*
 * TransaccionService.java
 *
 * Created on 21 de enero de 2005, 04:23 PM *
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.TransaccionDAO2;

/**
 *
 * @author  KREALES
 */
public class TransaccionService2 {
    
    TransaccionDAO2 t;
    /** Creates a new instance of TransaccionService */
    public TransaccionService2() {
        t = new TransaccionDAO2();
    }
    public TransaccionService2(String dataBaseName) {
        t = new TransaccionDAO2(dataBaseName);
    }
    
   /* public Statement getSt(){
        return t.getSt();
    }*/
    /*public void crearStatement()throws SQLException{
        t.crearStatement();
    }*/
     public void execute(String SQL)throws SQLException{
         t.execute(SQL);
     }
     /*public void closeAll () throws Exception{
        t.cerrarTodo();
    }*/
   /*  public void executeSinDes()throws SQLException{
         t.executeSinDes();
     }*/
     
     
 /*     public void crearStatementJNDI()throws SQLException{
        t.crearStatementJNDI();
    }
    public void executeJNDI()throws SQLException{
         t.executeJNDI();
    }
    public void closeAllJNDI () throws Exception{
        t.cerrarTodoJNDI();
    }
    public void addBatch(String sql)throws Exception{
         t.addBatch(sql);
    }    
      /**
     * Getter for property conexion.
     * @return Value of property conexion.
     */
 /*   public java.sql.Connection getConexion() {
        return t.getConexion();
    }
    
    /**
     * Setter for property conexion.
     * @param conexion New value of property conexion.
     */
  /*  public void setConexion(java.sql.Connection conexion) {
        t.setConexion(conexion);
    }
    
    /**
     * Getter for property sql.
     * @return Value of property sql.
     */
    public java.lang.String getSql() {
        return t.getSql();
    }    
    
    /**
     * Setter for property sql.
     * @param sql New value of property sql.
     */
    public void setSql(java.lang.String sql) {
        this.t.setSql( sql ); 
    }  
    
    /*
     public void closeAllJNDIsw () throws Exception{
        t.cerrarTodoJNDIsw();
     }*/
    
    
}
