
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;



public class EsquemaChequeDAO extends MainDAO{
    
    
    
    
    
    
    
    
    //____________________________________________________________________________________________
    //                                          METODOS
    //____________________________________________________________________________________________
    
    
    
    
    public EsquemaChequeDAO() { 
       super("EsquemaChequeDAO.xml");
    }
    public EsquemaChequeDAO(String dataBaseName) { 
       super("EsquemaChequeDAO.xml", dataBaseName);
    }
    
    
    
    
   /**
   * M�todo que permite insertar un esquema de impresion para banco
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/
    void insert(EsquemaCheque esquema, String usuario,String base)throws Exception {
        PreparedStatement  st      =  null;
        String             query   = "SQL_INSERT";
        try{
            
            st = this.crearPreparedStatement(query);
            st.setString (1, esquema.getDistrito()    );
            st.setString (2, esquema.getBanco()       );
            st.setInt    (3, esquema.getCPI()         );
            st.setInt    (4, esquema.getLinea()       );
            st.setInt    (5, esquema.getColumna()     );
            st.setString (6, esquema.getDescripcion() );
            st.setString (7, usuario  );
            st.setString (8, base     );
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException(" No se pudo insertar en esquema cheque -->" + e.getMessage() );
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
    }
    
    
    
    
    
    
   /**
   * M�todo que permite buscar el listado de esquema de impresion para banco
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/
    List  search()throws Exception {
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        String            query   = "SQL_SEARCH";
        try{ 
            
            st = this.crearPreparedStatement(query);
            rs = st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    EsquemaCheque esquema = new EsquemaCheque();
                    esquema.setDistrito( rs.getString(1));
                    esquema.setBanco(rs.getString(2));
                    esquema.setCPI(rs.getInt(3));
                    esquema.setLinea(rs.getInt(4));
                    esquema.setColumna(rs.getInt(5));
                    esquema.setDescripcion(rs.getString(6));
                    listado.add(esquema);
                }
            }
            
        }catch(Exception e){
            throw new SQLException(" No se pudieron listar los registros en Esquema Cheque -->" + e.getMessage() );
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return listado;
    }
    
    
    
   /**
   * M�todo que permite buscar el listado de bancos
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/
    List  searchBanck()throws Exception {
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              listado  = null;
        String            query    = "SQL_SEARCH_BANK";
        try{
            
            st = this.crearPreparedStatement(query);
            rs = st.executeQuery();
            if( rs!=null ){
                listado = new LinkedList();
                while(rs.next()){
                    CodeValue banco = new CodeValue();
                    banco.setCodigo(rs.getString(1));
                    listado.add(banco);
                }
            }
            
        }catch(Exception e){
            throw new SQLException(" No se pudieron listar los bancos -->" + e.getMessage() );
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return listado;
    }
    
    
    
    
    /**
   * M�todo que permite ANULAR el esquema de impresion
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/
    void delete(EsquemaCheque esquema)throws Exception {
        PreparedStatement st     =  null;
        String            query  = "SQL_DELETE";
        try{
            
            st = this.crearPreparedStatement(query);
            st.setString(1, esquema.getDistrito());
            st.setString(2, esquema.getBanco());
            st.setString(3, esquema.getDescripcion());
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException(" No se pudo eliminar el esquema  -->" + e.getMessage() );
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
    }
    
    
    
   /**
   * M�todo que permite actualizar el esquema de impresion
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/
    void update(EsquemaCheque esquema)throws Exception {
        PreparedStatement  st     =  null;
        String             query  = "SQL_UPDATE";
        try{
            
            st = this.crearPreparedStatement(query);
            st.setInt(1, esquema.getCPI());
            st.setInt(2, esquema.getLinea());
            st.setInt(3, esquema.getColumna());
            st.setString(4, esquema.getDistrito());
            st.setString(5, esquema.getBanco());
            st.setString(6, esquema.getDescripcion());
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException(" No se pudo eliminar el esquema  -->" + e.getMessage() );
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
    }
    
    
    
    
   /**
   * M�todo que permite buscar un esquema de impresion
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/
    EsquemaCheque  searchEsquema(EsquemaCheque objeto)throws Exception {
        PreparedStatement st      =  null;
        ResultSet         rs      =  null;
        EsquemaCheque     esquema =  null;
        String            query   = "SQL_SEARCH_ESQUEMA";
        try{
            
            st = this.crearPreparedStatement(query);
            st.setString(1, objeto.getDistrito());
            st.setString(2, objeto.getBanco());
            st.setString(3, objeto.getDescripcion());
            rs = st.executeQuery();
            
            if(rs.next()){
                esquema = new EsquemaCheque();
                esquema.setDistrito( rs.getString(1));
                esquema.setBanco(rs.getString(2));
                esquema.setCPI(rs.getInt(3));
                esquema.setLinea(rs.getInt(4));
                esquema.setColumna(rs.getInt(5));
                esquema.setDescripcion(rs.getString(6));
            }
            
        }catch(Exception e){
            throw new SQLException(" No se pudo encontrar el Esquema Cheque -->" + e.getMessage() );
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return esquema;
    }
    
    
    
    
    
    
    
} //end DAO
