/*
 * CarroceriaDAO.java
 *
 * Created on 9 de noviembre de 2004, 02:06 PM
 */

package com.tsp.operation.model;

import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.util.*;
/**
 *
 * @author  AMENDEZ
 */
public class CarroceriaDAO {
    private TreeMap cbxCarr;
    
    public static final String SQL_LISTAR =
    "SELECT * FROM carroceria ORDER BY descripcion";
        
    /** Creates a new instance of CarroceriaDAO */
    public CarroceriaDAO() {
    }
    
    public TreeMap getCbxCarr(){
        return this.cbxCarr;
    }
    
    public void setCbxCarr(TreeMap carr){
        this.cbxCarr = carr;
    }
    
    public void listar() throws SQLException{
        Connection con = null;
        Statement sttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        cbxCarr = null;
        cbxCarr = new TreeMap();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            sttm = con.createStatement();
            rs = sttm.executeQuery(SQL_LISTAR);
            while (rs.next()) {
                cbxCarr.put(rs.getString(2), rs.getString(1));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (sttm != null){
                try{
                   sttm.close();
                }
                catch(SQLException e){
                   throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);                
            }
        }     
    }
     public String getNombreCarroceria(String codigo) throws SQLException{
        Connection con = null;
        Statement sttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        String carr = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            sttm = con.createStatement();
            rs = sttm.executeQuery("SELECT descripcion FROM carroceria where codigo='"+codigo+"'");
            if (rs.next()) {
                carr = rs.getString(1);
            }
            return carr;
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA LISTAR MARCA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (sttm != null){
                try{
                   sttm.close();
                }
                catch(SQLException e){
                   throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);                
            }
        }     
    }
    
}
