/*
 * SdelayService.java
 *
 * Created on 3 de diciembre de 2004, 04:27 PM
 */

package com.tsp.operation.model;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  KREALES
 */
public class SdelayService {
    
    private SdelayDAO sd;
    /** Creates a new instance of SdelayService */
    public SdelayService() {
        
        sd=new SdelayDAO();
    }
    
    public List getSjs(String dstrct )throws SQLException{
        
        List sdjs=null;
        
        sd.searchSjs(dstrct);
        sdjs= sd.getList();
        
        return sdjs;
        
    }
    
    
    public Sjdelay get( )throws SQLException{
        
        return sd.getSj();
        
    }
    public boolean exist(String sj, String cf )throws SQLException{
        
        return sd.exist(sj, cf);
        
    }
    public boolean existList(String dstr )throws SQLException{
        
        return sd.existList(dstr);
        
    }
    
    
    public void buscar(String sj, String cf)throws SQLException{
        try{
            sd.search(sj,cf);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void insert(Sjdelay sj)throws SQLException{
        try{
            sd.set(sj);
            sd.insert();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void anular(Sjdelay sj)throws SQLException{
        try{
            sd.set(sj);
            sd.anular();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void listDelays(String dstrct)throws SQLException{
        sd.listDelays(dstrct);
    }
    public TreeMap getDemoras(){
        return sd.getDemoras();
    }
    
    
}
