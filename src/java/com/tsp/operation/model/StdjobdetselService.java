package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

public class StdjobdetselService{
    
    private StdjobdetselDAO standard;
    
    StdjobdetselService(){
        
        standard = new StdjobdetselDAO();
    }
    
    public List getStandards()throws SQLException{
        
        return standard.getStandards();
        
    }
    public void buscaStdSel() throws SQLException{
        try{
            standard.searchStdSel();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean estaStandardJob(String sj )throws SQLException{
        return standard.estaStandardJob(sj);
    }
    public List getStandardsProy(String proy, String base )throws SQLException{
        
        List standards=null;
        
        standard.searchStandardProy(proy, base);
        standards = standard.getStandardProy();
        
        return standards;
        
    }
    
    public Stdjobdetsel getStandardDetSel()throws SQLException{
        
        return  standard.getStandardDetSel();
        
    }
    public void buscaStandardDetSel(String sj) throws SQLException{
        try{
            standard.searchStandardDetSel(sj);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscaStandard(String sj) throws SQLException{
        try{
            standard.searchStandard(sj);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void consultaStandardDetSel(String sj, String cliente) throws SQLException{
        try{
            standard.consultarStandardJob(sj, cliente);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void consultaStandardCli( String cliente) throws SQLException{
        try{
            standard.searchStandardCli(cliente);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void datosStandard(String sj) throws SQLException{
        try{
            standard.datosStdjob(sj);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscarStandardJob(String sj) throws SQLException{
        try{
            standard.buscarStandardJob(sj);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public List getCostos() throws SQLException{
        return standard.getCosto();
    }
    
    public Vector getStdjob() throws SQLException{
        return standard.getStdjob();
    }
    public boolean existStandardsProy(String proy )throws SQLException{
        
        return standard.existStandardProy(proy);
    }
    public void insertStandard(String base)throws SQLException{
        standard.insertStdJob(base);
    }
    public void updateStandard()throws SQLException{
        standard.updateStdJob();
    }
    public void setStandard(Stdjobdetsel std)throws SQLException{
        standard.setStdjob(std);
    }
    
    public void deleteStandard(Stdjobdetsel std)throws SQLException{
        standard.setStdjob(std);
        standard.deleteStdJob();
    }
    
    public boolean existStandards(String sj )throws SQLException{
        
        return standard.existStandard(sj);
    }
    
    public void searchRutasCliente(String client)throws SQLException{
        standard.searchRutasCliente(client);
    }
    public void searchDestinosOrigen(String client, String ori)throws SQLException{
        standard.searchDestinosOrigen(client, ori);
    }
    public TreeMap getCiudadesOri(){
        
        return standard.getCiudadesOri();
    }
    public TreeMap getCiudadesDest(){
        
        return standard.getCiudadesDest();
    }
    public void setCiudadesDest(TreeMap t){
        
        standard.setCiudadesDest(t);
    }
    public void setCiudadesOri(TreeMap t) {
        standard.setCiudadesOri(t);
    }
    public TreeMap getStdjobTree(){
        
        return standard.getStdjobTree();
    }
    public void setStdjobTree(TreeMap t){
        
        standard.setStdjobTree(t);
    }
    public void searchStandaresOrDest(String client, String ori, String dest)throws SQLException{
        standard.searchStandaresOrDest(client, ori, dest);
    }
    public void searchStdJob(String sj)throws SQLException{
        standard.searchStdJob(sj);
    }
    
    public void searchCliente(String client)throws SQLException{
        standard.searchCliente(client);
    }
    
    //Tito Andr�s Maturana 02.11.2005
    public TreeMap getTreeMapStdSel() throws SQLException{
        this.standard.getTreeMapStdSel();
        return this.standard.getStdjobTree();
    }
    public void buscarStdjobcarbon() throws SQLException{    
        standard.getTreeMapStdSel();    
    }
    
        public void consultaStandardDetSel( String sj, String cliente, String estado, String tipo ) throws SQLException{
        try{
            standard.consultarStandardJob( sj, cliente, estado, tipo );
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
        


    
      public void setStdjob( Vector stdjob ) throws SQLException{
        standard.setStdjob( stdjob );
    }
}