/************************************************************************
 * Nombre clase: PlacaDAO.java                                          *
 * Descripci�n: Clase que maneja las consultas de las placas.           *
 * Autor: KREALES                                                       *
 * Fecha: Created on 17 de noviembre de 2004, 03:25 PM                  *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import java.text.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class PlacaDAO {
    
    Placa veh;
    Vector vehiculos;
    boolean flag;
    
    Vector vPlacas;//David 23.11.05
    
    private static final String SQL_HISTORIAL_PROPIETARIO = "INSERT INTO historial_mod_prop_placa(reg_status,dstrct,placa,nit,nombre,creation_date,creation_user,last_update,user_update,base) VALUES('','FINV',?,?,?,'now()',?,'now()',?,'COL')";
    
    //private static final String Base = "spo";
    
    private static final String SQL_BUSCAR_PLACA_ORACLE   = "SELECT * FROM MSF200 WHERE SUPPLIER_NO= ? ";
    
     //idevia 19.04.07
    private static final String SQL_PLACANOAPROBADA = "SELECT" +
    "     x.placa," +
    "     x.aprobado, " +
    "     B.nombre as conductor, " +
    "     c.nombre as propietario," +
    "     get_nombreusuario(x.usuariocrea) AS usuariocrea" +
    "  FROM    " +
    "  (SELECT a.placa, " +
    "       a.propietario, " +
    "       a.conductor,  " +
    "        a.usuariocrea," +
    "        a.aprobado, " +
    "        b.id_agencia  " +
    "  FROM placa a, " +
    "      usuarios b " +
    "  WHERE " +
    "     a.usuario_aprobacion = '' " +
    "     AND upper(b.idusuario) = upper(a.usuariocrea) " +
    "     AND b.id_agencia in (SELECT id_agencia FROM usuario_aprobacion WHERE usuario_aprobacion = ? AND tabla = 'PLACA' ))x " +
    "   LEFT JOIN NIT B  ON (B.cedula = x.conductor) " +
    "   LEFT JOIN NIT C  ON (C.cedula = x.propietario)" +
    "  ORDER BY x.placa";
    
    //Le agrego un miltro m�s a la consulta [BASE='spo'] por order explicita de AP el d�a 31 de Marzo de 2005 a las 12:09 PM
    private static final String SQL_BUSCAR_PLACA_POSTGRES = "SELECT PLACA,        "+
    "       CLASE,        "+
    "       NOMBRE,       "+
    "       ESTADOEQUIPO, "+
    "       RECURSO,      "+
    "       LOCALIZACION, "+
    "       propietario,  "+ //"       TENEDOR,      "+
    "       CONDUCTOR,    "+
    "       SERIAL,       "+
    "       TARA,         "+
    "       VOLUMEN,      "+
    "       MARCA,        "+
    "       MODELO,       "+
    "       LARGO,        "+
    "       ALTO,         "+
    "       LLANTAS,      "+
    "       ENGANCHE,     "+
    "       COLOR,        "+
    "       NOEJES,       "+
    "       ANCHO,        "+
    "       EMPRESAAFIL,  "+
    "       PISO,         "+
    "       CARGUE,       "+
    "       NOMOTOR,      "+
    "       PROPIETARIO   "+
    "FROM  PLACA          "+
    "WHERE PLACA= ?       ";
    
    //Le agrego un miltro m�s a la consulta [BASE='spo'] por order explicita de AP el d�a 31 de Marzo de 2005 a las 12:09 PM
    private static final String SQL_BUSCAR_PRO_CON        = "SELECT PROPIETARIO,  "+
    "       CONDUCTOR     "+
    "FROM PLACA           "+
    "WHERE PLACA= ?       ";
    
    //Le agrego un miltro m�s a la consulta [BASE='spo'] por order explicita de AP el d�a 31 de Marzo de 2005 a las 12:09 PM
    private static final String SQL_BUSCAR_DATOS_PRO_CON  = "SELECT A.CODPAIS,                           "+
    "       A.NOMBRE,                            "+
    "       A.DIRECCION,                         "+
    "       B.NOMBRE,                            "+
    "       B.DIRECCION,                         "+
    "       A.TELEFONO,                          "+
    "       C.PAYMENT_NAME,                      "+
    "       D.BRANCH_CODE,                       "+
    "       D.BANK_ACCOUNT_NO,                   "+
    "       D.CURRENCY                           "+
    "FROM NIT                 A,                 "+
    "     (SELECT NOMBRE,                        "+
    "             DIRECCION                      "+
    "      FROM NIT                              "+
    "      WHERE CEDULA = ? ) B,                 "+
    "     PROVEEDOR           C,                 "+
    "     BANCO               D                  "+
    "WHERE A.CEDULA          = ?             AND "+
    "      A.CEDULA          = C.NIT         AND "+
    "      C.BRANCH_CODE     = D.BRANCH_CODE AND "+
    "      C.BANK_ACCOUNT_NO = D.BANK_ACCOUNT_NO ";
    
    //POSDATA: El cambio lo realice directamente del equipo de KREALES
    //Henry
    private static final String SQL_PLACA_COND =   "SELECT placa FROM placa WHERE conductor = ?";
    private static final String SQL_PLACA_PROP =   "SELECT placa FROM placa WHERE propietario = ?";
    
    
    
    /*Andres MArtinez*/
    private static final String SELECT_TIPOXPLACA ="SELECT grupo_equipo FROM placa WHERE placa = ?";
    
    
    private static final String SELECT_PLACA =
   /* "SELECT " +
    "         p.*, b.nombre,b.nomciu,b.country_name,coalesce(r.tipo_recurso,'') AS tipo_recurso ,a.*     " +
    "    FROM " +
    "         (SELECT * FROM  placa WHERE  " +
    "        placa =?) p  " +
    "	 LEFT JOIN recursos r ON (r.equipo = p.recurso)" +
    "	 LEFT JOIN (SELECT cedula, nombre,veto as vetop from nit)a  on p.propietario = a.cedula " +
    "         LEFT JOIN (SELECT " +
    "                          cedula,nombre,nomciu,country_name  " +
    "                    FROM  " +
    "                          nit  " +
    "                          left join ciudad c on (c.codciu=nit.codciu) " +
    "                          left join pais pa on (pa.country_code=nit.codpais) " +
    "                    )b  on p.empresaafil = b.cedula  ;";*/
    "SELECT p.*, " +
    "       get_nombrenit(a.cedula) AS nombre, " +
    "       a.codciu, " +
    "       get_nombreciudad( a.codciu )AS nomciu, " +
    "       get_nombrepais( get_paisciudad(a.codciu) ) AS country_name, " +
    "       coalesce(r.tipo_recurso,'') AS tipo_recurso , " +
    "       a.*      " +
    "FROM   (SELECT * FROM  placa WHERE placa =?) p   " +
    "       LEFT JOIN recursos r ON (r.equipo = p.recurso) " +
    "       LEFT JOIN ( SELECT cedula, nombre, codciu, veto as vetop " +
    "                   FROM nit) a ON (a.cedula = p.propietario)";
    /*"SELECT p.estado,p.placa, p.condicion, p.tarjetaoper, p.venctarjetaoper, p.marca, p.clase, p.capacidad, p.carroceria,  " +
    "    p.modelo,p.color, p.nomotor, p.nochasis, p.noejes, p.agencia, p.dimcarroceria, p.venseguroobliga, p.homologado, p.atitulo,  " +
    "    p.empresaafil, p.propietario, p.conductor, p.tenedor, p.tipo, p.tara, p.largo, p.alto, p.llantas, p.enganche, p.ancho, p.piso,  " +
    "    p.cargue, p.volumen, p.fechaultact, p.usuario, p.recurso, p.grupoid, a.nombre, p.grupo, p.estadoequipo, p.localizacion,p.id_mims  " +
    "    FROM placa p LEFT JOIN (SELECT cedula, nombre from nit)a  on p.propietario = a.cedula" +
    "    WHERE placa = ? " ;*/
    
    //modificado el 05/01/2006 por operez
    
    
    
    
    
    private static final String NIT_EXIST = "SELECT nombre FROM nit WHERE cedula = ?";
    
    private static final String SQL_PLACAS_POR_ID="select placa, conductor from placa where placa like upper(?)";
    
    private static final String SLQ_EXISTE_PLACA="select placa from placa where placa= upper (?)";
    
    private static final String SQL_METADATA = "select * from placa limit(0)";    
  
    private static final String SQL_APROBACION ="UPDATE placa " +
    " SET aprobado = ? , " +
    "    usuario_aprobacion = ?," +
    "    fecha_aprobacion = 'now()'" +
    " WHERE " +
    "   placa = ? ";
    
    private static final String SQL_BUSCARFOTO="select document from doc.tblimagen where dstrct='FINV' and  activity_type='005' and document_type='031' and document=?";
    
    private static final String SELECT_PLACA_TIPO =
    "SELECT  p.*, get_nombrenit(a.cedula) AS nombre, a.codciu, get_nombreciudad( a.codciu )AS nomciu, " +
    "             get_nombrepais( get_paisciudad(a.codciu) ) AS country_name, coalesce(r.tipo_recurso,'') AS tipo_recurso , a.*      " +
    "FROM (SELECT * FROM  placa WHERE placa =? and grupo_equipo = ?) p   " +
    "     LEFT JOIN recursos r ON (r.equipo = p.recurso) " +
    "     LEFT JOIN (SELECT cedula, nombre, codciu, veto as vetop FROM nit) a ON (a.cedula = p.propietario)";
    
    private static final String SQL_CAMBIAR_ESTADO = " UPDATE placa " +
    " SET estado = ? #VETO#  " +
    " WHERE placa = ? ";
    
    private static final String SQL_BUSCAR_PLACA =  "SELECT placa,    " +
    "   get_nombrenit(conductor) AS conductor,    " +
    "   get_nombrenit(propietario) AS propietario," +
    "   estado       " +
    " FROM           " +
    "       placa    " +
    " WHERE          " +
    "      placa=  ? ";
    
   
    
    
private static final String INSERT_PLACA =
    "INSERT INTO placa (estado, placa, condicion, tarjetaoper, venctarjetaoper, marca, clase, capacidad, carroceria, " +
    "modelo, color, nomotor, nochasis, noejes, agencia, dimcarroceria, venseguroobliga, homologado, atitulo, " +
    "empresaafil, propietario, conductor, tenedor, tipo, tara, largo, alto, llantas, ancho, piso, " +
    "cargue, volumen, fechaultact, usuario, fechacrea, usuariocrea, recurso, grupoid, nombre, grupo, estadoequipo, localizacion, "+
    "numero_rin,placa_trailer,grado_riesgo,reg_nal_carga,fecvenreg,poliza_andina,fecvenandina,capacidad_trailer,tarempresa, " +
    "fecvenempresa,tarprop,fecvenprop,tarhabil,fecvenhabil,polizasoat,certemgases,fecvegases,ciu_tarjeta,resp_civil,fecvres_civil,grupo_equipo,descuento_equipo,clasificacion_equipo,estado_escolta,"+
    "operador_gps,gps_passwd, capac_gal, capac_mts, veto)" +
    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," +
    "        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'L',?,?,?,?,?)";
    
    private static final String UPDATE_PLACA =
    "UPDATE placa SET estado = ?, condicion = ?, tarjetaoper = ?, venctarjetaoper = ?, marca = ?, clase = ?, capacidad = ?, " +
    "carroceria = ?, modelo = ?, color = ?, nomotor = ?, nochasis = ?, noejes = ?, agencia = ?, dimcarroceria = ?, venseguroobliga = ?, " +
    "homologado = ?, atitulo = ?, trailer = 'A', empresaafil = ?, propietario = ?, conductor = ?, tenedor = ?, tipo = ?, serial = 'A', " +
    "tara = ?, largo = ?, alto = ?, llantas = ?, enganche = 'A', ancho = ?, piso = ?, cargue = ?, volumen = ?, fechaultact = ?, usuario = ?, " +
    "recurso = ?, grupoid = ?, grupo = ?, estadoequipo = ?, localizacion = ?, numero_rin = ?, placa_trailer = ?, grado_riesgo = ?, " +
    "reg_nal_carga = ?, fecvenreg = ?, poliza_andina = ?, fecvenandina = ?, capacidad_trailer = ?, tarempresa = ?, fecvenempresa = ?, " +
    "tarprop = ?, fecvenprop = ?, tarhabil = ?, fecvenhabil = ?, ciu_tarjeta = ?, fecvres_civil = ?, resp_civil =  ?, descuento_equipo = ?, clasificacion_equipo = ?, " +
    "polizasoat = ?, certemgases = ?, fecvegases = ?, grupo_equipo = ?, nombre = ?, operador_gps = ?, gps_passwd = ?," +
    "capac_gal = ?, capac_mts = ?, veto = ? "+
    " WHERE placa = ?";
    
    //Actualizacion de Placa 23 - 04 - 2007 - Historial
    private static final String UPDATE_PLACA2 =
    "UPDATE placa SET estado = ?, condicion = ?, tarjetaoper = ?, venctarjetaoper = ?, marca = ?, clase = ?, capacidad = ?, " +
    "carroceria = ?, modelo = ?, color = ?, nomotor = ?, nochasis = ?, noejes = ?, agencia = ?, dimcarroceria = ?, venseguroobliga = ?, " +
    "homologado = ?, atitulo = ?, trailer = 'A', empresaafil = ?, propietario = ?, conductor = ?, tenedor = ?, tipo = ?, serial = 'A', " +
    "tara = ?, largo = ?, alto = ?, llantas = ?, enganche = 'A', ancho = ?, piso = ?, cargue = ?, volumen = ?, fechaultact = ?, usuario = ?, " +
    "recurso = ?, grupoid = ?, grupo = ?, estadoequipo = ?, localizacion = ?, numero_rin = ?, placa_trailer = ?, grado_riesgo = ?, " +
    "reg_nal_carga = ?, fecvenreg = ?, poliza_andina = ?, fecvenandina = ?, capacidad_trailer = ?, tarempresa = ?, fecvenempresa = ?, " +
    "tarprop = ?, fecvenprop = ?, tarhabil = ?, fecvenhabil = ?, ciu_tarjeta = ?, fecvres_civil = ?, resp_civil =  ?, descuento_equipo = ?, clasificacion_equipo = ?, " +
    "polizasoat = ?, certemgases = ?, fecvegases = ?, grupo_equipo = ?, nombre = ?, operador_gps = ?, gps_passwd = ?," +
    "capac_gal = ?, capac_mts = ?, veto = ? "+
    " WHERE placa = ?";

    private static final String BUSCAR_PLACA =
    "SELECT  p.*, get_nombrenit(a.cedula) AS nombre, a.codciu, get_nombreciudad( a.codciu )AS nomciu, " +
    "             get_nombrepais( get_paisciudad(a.codciu) ) AS country_name, coalesce(r.tipo_recurso,'') AS tipo_recurso , a.*      " +
    "FROM (SELECT * FROM  placa WHERE placa =? ) p   " +
    "     LEFT JOIN recursos r ON (r.equipo = p.recurso) " +
    "     LEFT JOIN (SELECT cedula, nombre, codciu, veto as vetop FROM nit) a ON (a.cedula = p.propietario)";  
    
    //luigi...
    private static final String SQL_PLACA_DELETE = "UPDATE placa SET estado = 'I' WHERE placa = ?";
    private static final String SQL_BUSCAR_ESTADO = "SELECT estado FROM placa WHERE placa = ?";
    private static final String SQL_CAMBIAR_ESTADO_ANULADO = "UPDATE placa SET estado = 'A' WHERE placa = ?";
    
     //Modificacion Enrique De Lavalle 2007-04-24
    private static final String SQL_EXISTE_RECURSO="select equipo from recursos where equipo= upper (?)";
    private static final String SQL_OBTENER_TPLACA_REC="select tipo_recurso from recursos where equipo = upper (?)";
    
 /** Creates a new instance of PLACADAO */
    private Placa placa;
    
    PlacaDAO() {
        
    }

/**
     * buscaPlacaTipo, busca tda la informacion en la tabla de placa
     * @autor Ing. Jose de la rosa
     * @param: placa, tipo
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void buscarTipo(String noPlaca, String tipo) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        placa = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(SELECT_PLACA_TIPO);
            psttm.setString(1, noPlaca);
            psttm.setString(2, tipo);
            rs = psttm.executeQuery();
            if (rs.next()){
                placa = new Placa();
                placa.setReg_status(rs.getString("estado"));
                placa.setPlaca(rs.getString("placa"));
                placa.setCondicion(rs.getString("condicion"));
                placa.setTarjetaoper(rs.getString("tarjetaoper"));
                String venCtar = rs.getString("venctarjetaoper").trim();
                placa.setVenctarjetaoper((!venCtar.equals("0099-01-01")) ? venCtar :"");
                placa.setMarca(rs.getString("marca"));
                placa.setClase(rs.getString("clase"));
                placa.setCapacidad(rs.getString("capacidad"));
                placa.setCarroceria(rs.getString("carroceria"));
                placa.setModelo(rs.getString("modelo"));
                placa.setColor(rs.getString("color"));
                placa.setNomotor(rs.getString("nomotor"));
                placa.setNochasis(rs.getString("nochasis"));
                placa.setNoejes(rs.getString("noejes"));
                placa.setAgencia(rs.getString("agencia"));
                placa.setDimcarroceria(rs.getString("dimcarroceria"));
                String venCO = rs.getString("venseguroobliga").trim();
                placa.setVenseguroobliga((!venCO.trim().equals("0099-01-01")) ? venCO:"");
                placa.setHomologado(rs.getString("homologado"));
                placa.setAtitulo(rs.getString("atitulo"));
                placa.setEmpresaafil(rs.getString("empresaafil"));
                placa.setPropietario(rs.getString("propietario"));
                placa.setConductor(rs.getString("conductor"));
                placa.setTenedor(rs.getString("tenedor"));
                placa.setTipo(rs.getString("tipo"));
                placa.setTara(rs.getString("tara"));
                placa.setLargo(rs.getString("largo"));
                placa.setAlto(rs.getString("alto"));
                placa.setLlantas(rs.getString("llantas"));
                placa.setAncho(rs.getString("ancho"));
                placa.setPiso(rs.getString("piso"));
                placa.setCargue(rs.getString("cargue"));
                placa.setVolumen(rs.getString("volumen"));
                placa.setRecurso(rs.getString("recurso"));
                placa.setGrupoid(rs.getString("grupoid"));
                placa.setNombre(rs.getString("nombre"));
                placa.setGrupo(rs.getString("grupo"));
                placa.setEstadoequipo(rs.getString("estadoequipo"));
                placa.setLocalizacion(rs.getString("localizacion"));
                placa.setPolizasoat(rs.getString("polizasoat"));
                placa.setFechacrea((!rs.getString("fechacrea").equals("0099-01-01")) ? rs.getString("fechacrea"):"");
                placa.setUsuariocrea(rs.getString("usuariocrea"));
                //Nuevo 20-09-2005
                placa.setNumero_rin(rs.getString("numero_rin"));
                placa.setPlaca_trailer(rs.getString("placa_trailer"));
                placa.setGrado_riesgo(rs.getString("grado_riesgo"));
                placa.setReg_nal_carga(rs.getString("reg_nal_carga"));
                placa.setFecvenreg((!rs.getString("fecvenreg").trim().equals("0099-01-01")) ? rs.getString("fecvenreg"):"");
                placa.setPoliza_andina((!rs.getString("poliza_andina").trim().equals("0099-01-01")) ? rs.getString("poliza_andina"):"");
                placa.setFecvenandina((!rs.getString("fecvenandina").trim().equals("0099-01-01")) ? rs.getString("fecvenandina"):"");
                placa.setCapacidad_trailer(rs.getString("capacidad_trailer"));
                placa.setTarempresa(rs.getString("tarempresa"));
                placa.setFecvenempresa((!rs.getString("fecvenempresa").trim().equals("0099-01-01")) ? rs.getString("fecvenempresa"):"");
                placa.setTarprop(rs.getString("tarprop"));
                placa.setFecvenprop((!rs.getString("fecvenprop").trim().trim().equals("0099-01-01")) ? rs.getString("fecvenprop"):"");
                placa.setTarhabil(rs.getString("tarhabil"));
                placa.setFecvenhabil((!rs.getString("fecvenhabil").trim().equals("0099-01-01")) ? rs.getString("fecvenhabil"):"");
                placa.setCertgases(rs.getString("certemgases"));
                placa.setFecvengases((!rs.getString("fecvegases").trim().equals("0099-01-01")) ? rs.getString("fecvegases"):"");
                placa.setVprop(rs.getString("propietario"));
                placa.setEmpAfiliada(rs.getString("nombre"));
                placa.setCiudadAfiliada(rs.getString("nomciu"));
                placa.setPaisAfiliada(rs.getString("country_name"));
                //dbastidas 2006-01-17
                placa.setAprobado(rs.getString("aprobado"));
                placa.setFecvresp_civil(!rs.getString("fecvres_civil").equals("0099-01-01 00:00:00")? rs.getString("fecvres_civil"):"");
                placa.setResp_civil(rs.getString("resp_civil"));
                placa.setCiudad_tarjeta(rs.getString("ciu_tarjeta"));
                placa.setGrupo_equipo(rs.getString("grupo_equipo"));
                placa.setDescuento_equipo(rs.getString("descuento_equipo"));
                placa.setClasificacion_equipo(rs.getString("clasificacion_equipo"));
                
                //Osvaldo
                placa.setOperador_gps( rs.getString("operador_gps") );
                placa.setPasswd_seguimiento( rs.getString("gps_passwd") );
                placa.setTiene_gps( placa.getOperador_gps().equals("")? "NO" : "SI" );
                
                //AMATURANA 6.1.2007
                placa.setCapac_gal( rs.getString("capac_gal") );
                placa.setCapac_mts( rs.getString("capac_mts") );
                placa.setVeto( rs.getString("veto") );
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }



/**
     * Metodo actualizaPlaca, permite actualizar los datos de una placa a la BD.
     * @autor : Desconocido
     * @param : Objeto placa
     * @version : 1.0
     */
    public void actualizar(Placa placa) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(UPDATE_PLACA);
            
            psttm.setString(1, placa.getReg_status());
            psttm.setString(2, placa.getCondicion());
            psttm.setString(3, placa.getTarjetaoper());
            psttm.setString(4, placa.getVenctarjetaoper());
            psttm.setString(5, placa.getMarca());
            psttm.setString(6, placa.getClase());
            psttm.setString(7, placa.getCapacidad());
            psttm.setString(8, placa.getCarroceria());
            psttm.setString(9, placa.getModelo());
            psttm.setString(10, placa.getColor());
            psttm.setString(11, placa.getNomotor());
            psttm.setString(12, placa.getNochasis());
            psttm.setString(13, placa.getNoejes());
            psttm.setString(14, placa.getAgencia());
            psttm.setString(15, placa.getDimcarroceria());
            psttm.setString(16, placa.getVenseguroobliga());
            psttm.setString(17, placa.getHomologado());
            psttm.setString(18, placa.getAtitulo());
            psttm.setString(19, placa.getEmpresaafil());
            psttm.setString(20, placa.getPropietario());
            psttm.setString(21, placa.getConductor());
            psttm.setString(22, placa.getTenedor());
            psttm.setString(23, placa.getTipo());
            psttm.setString(24, placa.getTara());
            psttm.setString(25, placa.getLargo());
            psttm.setString(26, placa.getAlto());
            psttm.setString(27, placa.getLlantas());
            psttm.setString(28, placa.getAncho());
            psttm.setString(29, placa.getPiso());
            psttm.setString(30, placa.getCargue());
            psttm.setString(31, placa.getVolumen());
            psttm.setString(32, placa.getFechaultact());
            psttm.setString(33, placa.getUsuario());
            psttm.setString(34, placa.getRecurso());
            psttm.setString(35, placa.getGrupoid());
            psttm.setString(36, placa.getGrupo());
            psttm.setString(37, placa.getEstadoequipo());
            psttm.setString(38, placa.getLocalizacion());
            //Nuevo 20-09-2005
            psttm.setString(39, placa.getNumero_rin());
            psttm.setString(40,placa.getPlaca_trailer());
            psttm.setInt(41,placa.getGrado_riesgo());
            psttm.setString(42,placa.getReg_nal_carga());
            psttm.setString(43,placa.getFecvenreg());
            psttm.setString(44,placa.getPoliza_andina());
            psttm.setString(45,placa.getFecvenandina());
            psttm.setString(46,placa.getCapacidad_trailer());
            psttm.setString(47,placa.getTarempresa());
            psttm.setString(48,placa.getFecvenempresa());
            psttm.setString(49,placa.getTarprop());
            psttm.setString(50,placa.getFecvenprop());
            psttm.setString(51,placa.getTarhabil());
            psttm.setString(52,placa.getFecvenhabil());
            //jose 2006-02-15
            psttm.setString(53,placa.getCiudad_tarjeta());
            psttm.setString(54,placa.getFecvresp_civil());
            psttm.setString(55,placa.getResp_civil());
            psttm.setString(56,placa.getDescuento_equipo());
            psttm.setString(57,placa.getClasificacion_equipo());
            psttm.setString(58,placa.getPolizasoat());
            psttm.setString(59,placa.getCertgases());
            psttm.setString(60,placa.getFecvengases());
            //OJO
            psttm.setString(61,placa.getGrupo_equipo());
            psttm.setString(62,placa.getNombre());
            psttm.setString(63, placa.getOperador_gps());//Osvaldo
            psttm.setString(64, placa.getPasswd_seguimiento());//Osvaldo
            //***************** AMATURANA
            psttm.setString(65,placa.getCapac_gal());
            psttm.setString(66,placa.getCapac_mts());
            //*************************************
           //luigi
            psttm.setString(67, placa.getVeto());
            psttm.setString(68, placa.getPlaca());
            
            
            psttm.executeUpdate();
            AutorizacionFleteDAO aflete = new AutorizacionFleteDAO();
            aflete.insertControlActualizacion(psttm.toString(), placa.getUsuariocrea());
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }






/**
     * Metodo insertaPlaca, permite ingresar los datos de una placa a la BD.
     * @autor : Desconocido
     * @param : Objeto Placa
     * @version : 1.0
     */
    public void insertar() throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(INSERT_PLACA);
            psttm.setString(1, placa.getReg_status());
            psttm.setString(2, placa.getPlaca());
            psttm.setString(3, placa.getCondicion());
            psttm.setString(4, placa.getTarjetaoper());
            psttm.setString(5, placa.getVenctarjetaoper());
            psttm.setString(6, placa.getMarca());
            psttm.setString(7, placa.getClase());
            psttm.setString(8, placa.getCapacidad());
            psttm.setString(9, placa.getCarroceria());
            psttm.setString(10, placa.getModelo());
            psttm.setString(11, placa.getColor());
            psttm.setString(12, placa.getNomotor());
            psttm.setString(13, placa.getNochasis());
            psttm.setString(14, placa.getNoejes());
            psttm.setString(15, placa.getAgencia());
            psttm.setString(16, placa.getDimcarroceria());
            psttm.setString(17, placa.getVenseguroobliga());
            psttm.setString(18, placa.getHomologado());
            psttm.setString(19, placa.getAtitulo());
            psttm.setString(20, placa.getEmpresaafil());
            psttm.setString(21, placa.getPropietario());
            psttm.setString(22, placa.getConductor());
            psttm.setString(23, placa.getTenedor());
            psttm.setString(24, placa.getTipo());
            psttm.setString(25, placa.getTara());
            psttm.setString(26, placa.getLargo());
            psttm.setString(27, placa.getAlto());
            psttm.setString(28, placa.getLlantas());
            psttm.setString(29, placa.getAncho());
            psttm.setString(30, placa.getPiso());
            psttm.setString(31, placa.getCargue());
            psttm.setString(32, placa.getVolumen());
            psttm.setString(33, placa.getFechaultact());
            psttm.setString(34, placa.getUsuario());
            psttm.setString(35, placa.getFechacrea());
            psttm.setString(36, placa.getUsuariocrea());
            psttm.setString(37, placa.getRecurso());
            psttm.setString(38, placa.getGrupoid());
            psttm.setString(39, placa.getNombre());
            psttm.setString(40, placa.getGrupo());
            psttm.setString(41, placa.getEstadoequipo());
            psttm.setString(42, placa.getLocalizacion());
            //nuevo
            psttm.setString(43, placa.getNumero_rin());
            psttm.setString(44,placa.getPlaca_trailer());
            psttm.setInt(45,placa.getGrado_riesgo());
            psttm.setString(46,placa.getReg_nal_carga());
            psttm.setString(47,placa.getFecvenreg());
            psttm.setString(48,placa.getPoliza_andina());
            psttm.setString(49,placa.getFecvenandina());
            psttm.setString(50,placa.getCapacidad_trailer());
            psttm.setString(51,placa.getTarempresa());
            psttm.setString(52,placa.getFecvenempresa());
            psttm.setString(53,placa.getTarprop());
            psttm.setString(54,placa.getFecvenprop());
            psttm.setString(55,placa.getTarhabil());
            psttm.setString(56,placa.getFecvenhabil());
            psttm.setString(57,placa.getPolizasoat());
            psttm.setString(58,placa.getCertgases());
            psttm.setString(59,placa.getFecvengases());
            //jose 2006-02-15
            psttm.setString(60,placa.getCiudad_tarjeta());
            psttm.setString(61,placa.getResp_civil());
            psttm.setString(62,placa.getFecvresp_civil());
            psttm.setString(63,placa.getGrupo_equipo());
            psttm.setString(64,placa.getDescuento_equipo());
            psttm.setString(65,placa.getClasificacion_equipo());
            psttm.setString(66,placa.getOperador_gps());
            psttm.setString(67,placa.getPasswd_seguimiento());
            //***************** AMATURANA
            psttm.setString(68,placa.getCapac_gal());
            psttm.setString(69,placa.getCapac_mts());
            //*************************************
            psttm.setString(70,placa.getVeto());
            //psttm.setString(66, placa.getEstadoEscolta());
            //System.out.println( psttm.toString() );
            
            psttm.executeUpdate();
                        
            
            AutorizacionFleteDAO aflete = new AutorizacionFleteDAO();
            aflete.insertControlActualizacion(psttm.toString(), placa.getUsuariocrea());
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
   
    
    
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public Placa getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param veh New value of property placa.
     */
    public void setPlaca(Placa placa) {
        this.placa = placa;
    }
    
    /**
     * Metodo reiniciarPlaca, coloca el objeto de placa en null.
     * @autor : Desconocido
     * @param : placa
     * @version : 1.0
     */
    public void reiniciar(){
        this.placa = null;
    }
    
   
    
    
    /**
     * Metodo buscarConductorPropietario, permite una busqueda de placas por cedula conductor o propietario.
     * @autor : Henry Osorio
     * @param : cedula conductor, placa
     * @version : 1.0
     */
    public void buscar(String noPlaca) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        placa = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(SELECT_PLACA);
            psttm.setString(1, noPlaca);
            rs = psttm.executeQuery();
            if (rs.next()){
                placa = new Placa();
                placa.setReg_status(rs.getString("estado"));
                placa.setPlaca(rs.getString("placa"));
                placa.setCondicion(rs.getString("condicion"));
                placa.setTarjetaoper(rs.getString("tarjetaoper"));
                String venCtar = rs.getString("venctarjetaoper").trim();
                placa.setVenctarjetaoper((!venCtar.equals("0099-01-01")) ? venCtar :"");
                placa.setMarca(rs.getString("marca"));
                placa.setClase(rs.getString("clase"));
                placa.setCapacidad(rs.getString("capacidad"));
                placa.setCarroceria(rs.getString("carroceria"));
                placa.setModelo(rs.getString("modelo"));
                placa.setColor(rs.getString("color"));
                placa.setNomotor(rs.getString("nomotor"));
                placa.setNochasis(rs.getString("nochasis"));
                placa.setNoejes(rs.getString("noejes"));
                placa.setAgencia(rs.getString("agencia"));
                placa.setDimcarroceria(rs.getString("dimcarroceria"));
                String venCO = rs.getString("venseguroobliga").trim();
                placa.setVenseguroobliga((!venCO.trim().equals("0099-01-01")) ? venCO:"");
                placa.setHomologado(rs.getString("homologado"));
                placa.setAtitulo(rs.getString("atitulo"));
                placa.setEmpresaafil(rs.getString("empresaafil"));
                placa.setPropietario(rs.getString("propietario"));
                placa.setConductor(rs.getString("conductor"));
                placa.setTenedor(rs.getString("tenedor"));
                placa.setTipo(rs.getString("tipo"));
                placa.setTara(rs.getString("tara"));
                placa.setLargo(rs.getString("largo"));
                placa.setAlto(rs.getString("alto"));
                placa.setLlantas(rs.getString("llantas"));
                placa.setAncho(rs.getString("ancho"));
                placa.setPiso(rs.getString("piso"));
                placa.setCargue(rs.getString("cargue"));
                placa.setVolumen(rs.getString("volumen"));
                placa.setRecurso(rs.getString("recurso"));
                placa.setGrupoid(rs.getString("grupoid"));
                placa.setNombre(rs.getString("nombre"));
                placa.setGrupo(rs.getString("grupo"));
                placa.setEstadoequipo(rs.getString("estadoequipo"));
                placa.setLocalizacion(rs.getString("localizacion"));
                placa.setPolizasoat(rs.getString("polizasoat"));
                placa.setFechacrea((!rs.getString("fechacrea").equals("0099-01-01 00:00:00")) ? rs.getString("fechacrea"):"");
                placa.setUsuariocrea(rs.getString("usuariocrea"));
                //Nuevo 20-09-2005
                placa.setNumero_rin(rs.getString("numero_rin"));
                placa.setPlaca_trailer(rs.getString("placa_trailer"));
                placa.setGrado_riesgo(rs.getString("grado_riesgo"));
                placa.setReg_nal_carga(rs.getString("reg_nal_carga"));
                placa.setFecvenreg((!rs.getString("fecvenreg").trim().equals("0099-01-01")) ? rs.getString("fecvenreg"):"");
                placa.setPoliza_andina((!rs.getString("poliza_andina").trim().equals("0099-01-01")) ? rs.getString("poliza_andina"):"");
                placa.setFecvenandina((!rs.getString("fecvenandina").trim().equals("0099-01-01")) ? rs.getString("fecvenandina"):"");
                placa.setCapacidad_trailer(rs.getString("capacidad_trailer"));
                placa.setTarempresa(rs.getString("tarempresa"));
                placa.setFecvenempresa((!rs.getString("fecvenempresa").trim().equals("0099-01-01")) ? rs.getString("fecvenempresa"):"");
                placa.setTarprop(rs.getString("tarprop"));
                placa.setFecvenprop((!rs.getString("fecvenprop").trim().trim().equals("0099-01-01")) ? rs.getString("fecvenprop"):"");
                placa.setTarhabil(rs.getString("tarhabil"));
                placa.setFecvenhabil((!rs.getString("fecvenhabil").trim().equals("0099-01-01")) ? rs.getString("fecvenhabil"):"");
                placa.setCertgases(rs.getString("certemgases"));
                placa.setFecvengases((!rs.getString("fecvegases").trim().equals("0099-01-01")) ? rs.getString("fecvegases"):"");
                placa.setVprop(rs.getString("propietario"));
                placa.setEmpAfiliada(rs.getString("nombre"));
                placa.setCiudadAfiliada(rs.getString("nomciu"));
                placa.setPaisAfiliada(rs.getString("country_name"));
                //dbastidas 2006-01-17
                placa.setAprobado(rs.getString("aprobado"));
                placa.setFecvresp_civil(!rs.getString("fecvres_civil").equals("0099-01-01 00:00:00")? rs.getString("fecvres_civil"):"");
                placa.setResp_civil(rs.getString("resp_civil"));
                placa.setCiudad_tarjeta(rs.getString("ciu_tarjeta"));
                placa.setGrupo_equipo(rs.getString("grupo_equipo"));
                placa.setDescuento_equipo(rs.getString("descuento_equipo"));
                placa.setClasificacion_equipo(rs.getString("clasificacion_equipo"));
                //dbastidas 2006-04-18
                placa.setFechaultact(!rs.getString("fechaultact").equals("0099-01-01 00:00:00")? rs.getString("fechaultact"):"");
                placa.setUsuario(rs.getString("usuariocrea"));
                placa.setVeto(rs.getString("veto"));
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /*public void buscar(String noPlaca) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        placa = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(SELECT_PLACA);
            psttm.setString(1, noPlaca);
            rs = psttm.executeQuery();
            if (rs.next()){
                placa = new Placa();
                placa.setReg_status(rs.getString(1));
                placa.setPlaca(rs.getString(2));
                placa.setCondicion(rs.getString(3));
                placa.setTarjetaoper(rs.getString(4));
                placa.setVenctarjetaoper(rs.getString(5));
                placa.setMarca(rs.getString(6));
                placa.setClase(rs.getString(7));
                placa.setCapacidad(rs.getString(8));
                placa.setCarroceria(rs.getString(9));
                placa.setModelo(rs.getString(10));
                placa.setColor(rs.getString(11));
                placa.setNomotor(rs.getString(12));
                placa.setNochasis(rs.getString(13));
                placa.setNoejes(rs.getString(14));
                placa.setAgencia(rs.getString(15));
                placa.setDimcarroceria(rs.getString(16));
                placa.setVenseguroobliga(rs.getString(17));
                placa.setHomologado(rs.getString(18));
                placa.setAtitulo(rs.getString(19));
                placa.setEmpresaafil(rs.getString(20));
                placa.setPropietario(rs.getString(21));
                placa.setConductor(rs.getString(22));
                placa.setTenedor(rs.getString(23));
                placa.setTipo(rs.getString(24));
                placa.setTara(rs.getString(25));
                placa.setLargo(rs.getString(26));
                placa.setAlto(rs.getString(27));
                placa.setLlantas(rs.getString(28));
                placa.setAncho(rs.getString(30));
                placa.setPiso(rs.getString(31));
                placa.setCargue(rs.getString(32));
                placa.setVolumen(rs.getString(33));
                placa.setRecurso(rs.getString(36));
                placa.setGrupoid(rs.getString(37));
                placa.setNombre(rs.getString(38));
                placa.setGrupo(rs.getString(39));
                placa.setEstadoequipo(rs.getString(40));
                placa.setLocalizacion(rs.getString(41));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
     
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }*/
    
    /**
     * Metodo placaExist, permite saber si existe una placa.
     * @autor : Desconocido
     * @param : Placa
     * @version : 1.0
     */
    public boolean placaExist(String noPlaca) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement("select placa from placa where placa =?");
            psttm.setString(1, noPlaca);
            rs = psttm.executeQuery();
            if (rs.next()){
                sw = true;
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Metodo nombreNit, permite obtener el nombre de un usuario dada una cedula.
     * @autor : Desconocido
     * @param : nit
     * @version : 1.0
     */
    public String nombreNit(String cedula) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        String nombre = "No se encontro";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement("select nombre from nit where cedula = ?");
            psttm.setString(1,cedula);
            rs = psttm.executeQuery();
            if (rs.next()){
                nombre = rs.getString("nombre");
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return nombre;
    }
    
    /**
     * Metodo nombreProp, permite obtener el nombre de un propietario dada una cedula.
     * @autor : Desconocido
     * @param : cudula
     * @version : 1.0
     */
    public String nombreProp(String cedula) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        String nombre = "No se encontro";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement("select p_nombre||' '||s_nombre||' '||p_apellido||' '||s_apellido as nombre from identificacion_prop where cedula = ?");
            psttm.setString(1,cedula);
            rs = psttm.executeQuery();
            if (rs.next()){
                nombre = rs.getString("nombre");
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NOMBRE DEL PROPIETARIO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return nombre;
    }
    
    /**
     * Metodo nitExist, permite saber si el nit existe en la BD.
     * @autor : Desconocido
     * @param : nit
     * @version : 1.0
     */
    public boolean nitExist(String nit) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(NIT_EXIST);
            psttm.setString(1, nit);
            rs = psttm.executeQuery();
            if (rs.next()){
                sw = true;
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEl NIT: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Metodo Buscar_Pro_Con, permite buscar un propietario.
     * @autor : Ing. Miguel Angel
     * @param : Placa
     * @version : 1.0
     */
    Placa Buscar_Pro_Con(String Placa) throws SQLException {
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        PoolManager       poolManager = null;
        Placa             Registro    = new Placa();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con==null)
                throw new SQLException("Error en conexi�n...");
            st = con.prepareStatement(SQL_BUSCAR_PRO_CON);
            st.setString(1,Placa);
            rs = st.executeQuery();
            while(rs.next()){
                Registro.setPropietario(rs.getString(1));
                Registro.setConductor(rs.getString(2));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en Buscar_Pro_Con_Ben_Ban [PlacaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st  != null) st.close();
            if(rs  != null) rs.close();
            if(con != null)
                poolManager.freeConnection("fintra", con);
        }
        return Registro;
    }
    
    /**
     * Metodo Buscar_Pro_Con_Ben_Ban.
     * @autor : Desconocido
     * @see Buscar_Pro_Con_Ben_Ban - PlacaDAO
     * @param : Placa
     * @version : 1.0
     */
    ProConBen Buscar_Pro_Con_Ben_Ban(String Placa, String Propietario, String Conductor) throws SQLException {
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        PoolManager       poolManager = null;
        ProConBen         Registros   = new ProConBen();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con==null)
                throw new SQLException("Error en conexi�n...");
            st = con.prepareStatement(SQL_BUSCAR_DATOS_PRO_CON);
            st.setString(1, Conductor);
            st.setString(2, Propietario);
            ////System.out.println("SQL= "+st.toString());
            rs = st.executeQuery();
            while(rs.next()){
                Registros.setPlaca(Placa);
                Registros.setPais(rs.getString(1));
                Registros.setNombreP(rs.getString(2));
                Registros.setDirteccionP(rs.getString(3));
                Registros.setNombreC(rs.getString(4));
                Registros.setDireccionC(rs.getString(5));
                Registros.setTelefono(rs.getString(6));
                Registros.setBeneficiario(rs.getString(7));
                Registros.setBanco(rs.getString(8));
                Registros.setCuenta(rs.getString(9));
                Registros.setMoneda(rs.getString(10));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en Buscar_Pro_Con_Ben_Ban [PlacaDAO]... \n SQL=> ["+st.toString()+"]\n"+e.getMessage());
        }
        finally {
            if(st  != null) st.close();
            if(rs  != null) rs.close();
            if(con != null)
                poolManager.freeConnection("fintra", con);
        }
        return Registros;
    }
    
    /**
     * Metodo Buscar_Placa_Postgres, permite buscar una placa en postgres.
     * @autor : Desconocido
     * @param : Placa
     * @version : 1.0
     */
    Placa Buscar_Placa_Postgres(String Placa) throws SQLException{
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        PoolManager       poolManager = null;
        Placa             Registro    = new Placa();
        
        try {
            poolManager = PoolManager.getInstance();
            con         = poolManager.getConnection("fintra");
            if(con==null)
                throw new SQLException("Error en conexi�n...");
            st = con.prepareStatement(SQL_BUSCAR_PLACA_POSTGRES);
            st.setString(1,Placa);
            rs = st.executeQuery();
            while(rs.next()){
                Registro.setPlaca(rs.getString(1));
                Registro.setClase(rs.getString(2));
                Registro.setNombre(rs.getString(3));
                Registro.setEstadoequipo(rs.getString(4));
                Registro.setRecurso(rs.getString(5));
                Registro.setLocalizacion(rs.getString(6));
                Registro.setTenedor(rs.getString(7));
                Registro.setConductor(rs.getString(8));
                Registro.setSerial(rs.getString(9));
                Registro.setTara(rs.getString(10));
                Registro.setVolumen(rs.getString(11));
                Registro.setMarca(rs.getString(12));
                Registro.setModelo(rs.getString(13));
                Registro.setLargo(rs.getString(14));
                Registro.setAlto(rs.getString(15));
                Registro.setLlantas(rs.getString(16));
                Registro.setEnganche(rs.getString(17));
                Registro.setColor(rs.getString(18));
                Registro.setNoejes(rs.getString(19));
                Registro.setAncho(rs.getString(20));
                Registro.setEmpresaafil(rs.getString(21));
                Registro.setPiso(rs.getString(22));
                Registro.setCargue(rs.getString(23));
                Registro.setNomotor(rs.getString(24));
                Registro.setPropietario(rs.getString(25));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en Buscar_Placa_Postgres [PlacaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st  != null) st.close();
            if(rs  != null) rs.close();
            if(con != null)
                poolManager.freeConnection("fintra", con);
        }
        return Registro;
    }
    
    /**
     * Metodo Buscar_Placa_Oracle, permite buscar una placa en oracle.
     * @autor : Ing. Miguel Angel
     * @param : Placa
     * @version : 1.0
     */
    boolean Buscar_Placa_Oracle(String Placa) throws SQLException {
        boolean           Esta        = false;
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        PoolManager       poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con         = poolManager.getConnection("oracle");
            if(con==null)
                throw new SQLException("Error en conexi�n...");
            st = con.prepareStatement(SQL_BUSCAR_PLACA_ORACLE);
            st.setString(1, Placa);
            rs = st.executeQuery();
            while(rs.next())
                Esta = true;
        }
        catch(SQLException e) {
            throw new SQLException("Error en Buscar_Placa_Oracle [PlacaDAO]... \n"+e.getMessage()+"\n"+SQL_BUSCAR_PLACA_ORACLE+"\n"+Placa);
        }
        finally {
            if(st  != null) st.close();
            if(rs  != null) rs.close();
            if(con != null)
                poolManager.freeConnection("oracle", con);
        }
        return Esta;
    }
    
    /**
     * Metodo placaVetado, permite saber si la placa se encuentra vetada.
     * @autor : Desconocido
     * @param : placa
     * @version : 1.0
     */
    public boolean placaVetado(String placa) throws SQLException{
        
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                psttm = con.prepareStatement("select veto from placa where placa = ? and veto ='V' ");
                psttm.setString(1, placa);
                rs = psttm.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA VERIFICACION DEL VETO DE UNA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Getter for property veh.
     * @return Value of property veh.
     */
    public Placa getVeh() {
        return veh;
    }
    
    /**
     * Setter for property veh.
     * @param veh New value of property veh.
     */
    public void setVeh(Placa veh) {
        this.veh = veh;
    }
    
    /**
     * Getter for property vehiculos.
     * @return Value of property vehiculos.
     */
    public java.util.Vector getVehiculos() {
        return vehiculos;
    }
    
    /**
     * Setter for property vehiculos.
     * @param vehiculos New value of property vehiculos.
     */
    public void setVehiculos(java.util.Vector vehiculos) {
        this.vehiculos = vehiculos;
    }
    
    
    /**
     * Metodo isTrailer, permite saber si la placa es una tractomula.
     * @autor : Desconocido
     * @param : plaveh
     * @version : 1.0
     */
    public boolean isTractomula( String plaveh )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean esta=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select 	p.placa, " +
                "	p.recurso," +
                "        r.tipo_recurso" +
                " from    placa p," +
                "	recursos r" +
                " where   r.equipo = p.recurso" +
                "        and placa = ?");
                st.setString(1, plaveh);
                rs= st.executeQuery();
                if(rs.next()){
                    if("C".equals(rs.getString("tipo_recurso")))
                        esta = true;
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LAS PLACAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return esta;
    }
    
    /**
     * Metodo isTrailer, permite saber si la placa es una tractomula.
     * @autor : Desconocido
     * @see isTrailer - PlacaDAO
     * @param : plaveh
     * @version : 1.0
     */
    public boolean isTrailer( String plaveh )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean esta=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select 	p.placa, " +
                "	p.recurso," +
                "        r.tipo_recurso" +
                " from    placa p," +
                "	recursos r" +
                " where   r.equipo = p.recurso" +
                "        and placa = ?");
                st.setString(1, plaveh);
                ////System.out.println("Consulta trailer "+st);
                rs= st.executeQuery();
                if(rs.next()){
                    if("T".equals(rs.getString("tipo_recurso")))
                        esta = true;
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LAS PLACAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return esta;
    }
    
    /**
     * Getter for property flag.
     * @return Value of property flag.
     */
    public boolean isFlag() {
        return flag;
    }
    
    /**
     * Setter for property flag.
     * @param flag New value of property flag.
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }
    
    /**
     * Metodo actualizarCond, funcion que permite actualizar la cedula de conductor de una placa.
     * @autor : Desconocido
     * @param : cedula conductor, placa
     * @version : 1.0
     */
    public void actualizarCond(String cedcon, String placa) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement("update placa set conductor=?  where placa =?");
            
            psttm.setString(1, cedcon);
            psttm.setString(2, placa);
            
            
            psttm.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL CONDUCTOR DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo buscarConductorPropietario, permite una busqueda de placas por cedula conductor o propietario.
     * @autor : Henry Osorio
     * @param : cedula conductor, placa
     * @version : 1.0
     */
    public void buscarPlacasConductor(String busq, String cedula) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        vehiculos = new Vector();
        String sql = "";
        if(busq.equals("Propietario"))
            sql = this.SQL_PLACA_PROP;
        else if(busq.equals("Conductor"))
            sql = this.SQL_PLACA_COND;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(sql);
            psttm.setString(1, cedula);
            ////System.out.println("Query"+psttm);
            rs = psttm.executeQuery();
            while (rs.next()){
                vehiculos.addElement(rs.getString("placa"));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    
    
    /**
     * Metodo vPlacas , Metodo que retorna un Vector de Vectores con las placas dado Un Striing con el caracter inicial de cualquier placa
     * @autor : Ing. David Lamadrid
     * @param : String id
     * @version : 1.0
     */
    public void vPlacas(String id) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vPlacas = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_PLACAS_POR_ID);
                st.setString(1, id+"%");
                
                ////System.out.println("Query: "+st.toString());
                rs = st.executeQuery();
                while(rs.next()) {
                    Vector placa = new Vector();
                    placa.add(rs.getString("placa"));
                    placa.add(rs.getString("conductor"));
                    vPlacas.add(placa);
                }
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL INSERTAR EL ESTADO" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Metodo existePlaca , Metodo que retorna un boolean true cuando existe una placa y false cuando no existe
     * @autor : Ing. David Lamadrid
     * @param : String id
     * @version : 1.0
     */
    public boolean existePlaca(String id) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SLQ_EXISTE_PLACA);
                st.setString(1, id);
                
                ////System.out.println("Query: "+st.toString());
                rs = st.executeQuery();
                while(rs.next()) {
                    sw=true;
                }
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL INSERTAR EL ESTADO" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    /**
     * Getter for property vPlacas.
     * @return Value of property vPlacas.
     */
    public java.util.Vector getVPlacas() {
        return vPlacas;
    }
    
    /**
     * Setter for property vPlacas.
     * @param vPlacas New value of property vPlacas.
     */
    public void setVPlacas(java.util.Vector vPlacas) {
        this.vPlacas = vPlacas;
    }
    /**
     * Metodo consultaTxt , Metodo que Realiza una consulta de un registro para a�adirlo a una linea
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : String ruta, String usuario, String campo, String tabla, String placa
     * @version : 1.0
     */
    public void consultaTxt(String ruta, String base, String tabla, String placa, String campo) throws SQLException {
        ////System.out.println("ESTOY EN EL DAO");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                ////System.out.println("VOY A EJECUTAR EL QUERY");
                st = con.prepareStatement("SELECT * FROM "+tabla+" WHERE  "+campo+" = ?");
                st.setString(1, placa);
                ////System.out.println("Query  "+st);
                rs = st.executeQuery();
                ////System.out.println("YA EJECUTE EL QUERY");
                String linea = "";
                while (rs.next()){
                    ////System.out.println("VOY A METER LOS DATOS EN LA LINEA");
                    linea += rs.getString(1)+","+rs.getString(2)+","+rs.getString(3)+","+rs.getString(4)+
                    ","+rs.getDate(5)+","+rs.getString(6)+","+rs.getString(7)+","+rs.getString(8)+","+rs.getString(9)
                    +","+rs.getString(10)+","+rs.getString(11)+","+rs.getString(12)+","+rs.getString(13)+","+rs.getString(14)
                    +","+rs.getString(15)+","+rs.getString(16)+","+rs.getDate(17)+","+rs.getString(18)+","+rs.getString(19)
                    +","+rs.getString(20)+","+rs.getString(21)+","+rs.getString(22)+","+rs.getString(23)+","+rs.getString(24)
                    +","+rs.getString(25)+","+rs.getString(26)+","+rs.getString(27)+","+rs.getString(28)+","+rs.getString(29)
                    +","+rs.getString(30)+","+rs.getString(31)+","+rs.getString(32)+","+rs.getString(33)+","+rs.getString(34)
                    +","+rs.getString(35)+","+rs.getTimestamp(36)+","+rs.getString(37)+","+rs.getTimestamp(38)+","+rs.getString(39)
                    +","+rs.getString(40)+","+rs.getString(41)+","+rs.getString(42)+","+rs.getString(43)+","+rs.getString(44)+","+rs.getString(45)
                    +","+rs.getString(46)+","+rs.getString(47)+","+rs.getString(48)+","+rs.getString(49)+","+rs.getString(50)+","+rs.getString(51)
                    +","+rs.getString(52)+","+rs.getString(53)+","+rs.getDouble(54)+","+rs.getString(55)+","+rs.getString(56)+","+rs.getDate(57)
                    +","+rs.getString(58)+","+rs.getDate(59)+","+rs.getString(60)+","+rs.getDate(61)+","+rs.getString(62)+","+rs.getDate(63)
                    +","+rs.getString(64)+","+rs.getDate(65)+","+rs.getString(66)+","+rs.getDate(67)+","+rs.getString(68)+","+rs.getString(69)
                    +","+rs.getString(70)+","+rs.getString(71)+","+rs.getString(72)+","+rs.getString(73)+","+rs.getString(74)+","+rs.getString(75)+","+rs.getString(76)
                    +","+rs.getString(77)+","+rs.getString(78)+","+rs.getString(79)+","+rs.getString(80)+","+rs.getTimestamp(81)
                    +","+rs.getString(82);
                    ////System.out.println("METI LOS DATOS");
                    
                }
                ////System.out.println("VOY A METER LOS DATOS EN EL ARCHIVO"+"n/  linea = "+ linea);
                this.archivotxt(linea, ruta, base);
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL REALIZAR LA CONSULTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo archivoTxt , M�todo que a�ade una linea con los datos de un registro a un archivo de auditor�a
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : String ruta,  String base, String linea
     * @version : 1.0
     */
    private void archivotxt(String linea, String ruta, String base) throws SQLException{
        LinkedList lineasrs = new LinkedList();
        Calendar FechaHoy = Calendar.getInstance();
        java.util.Date d = FechaHoy.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyMMdd");
        String FechaFormated1 = s1.format(d);
        String lineas="";
        int cent = 0;
        String archivo = ruta + "\\placa" + base +"_"+ FechaFormated1 + ".txt";
        ////System.out.println("ARCHIVO  "+ archivo);
        try{
            
            FileWriter x = new FileWriter(archivo, true);
            x.write("\n"+linea);
            x.close();
        }
        catch(IOException e){
            ////System.out.println("No se pudo abrir el archivo");
        }
    }
    /**
     * Obtiene el ResultSetMetaData de la tabla placa
     * @autor Ing. Tito Andr�s Maturana
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public ResultSetMetaData obtenerMetadata() throws SQLException{
        
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                psttm = con.prepareStatement(SQL_METADATA);
                
                rs = psttm.executeQuery();
                
                return rs.getMetaData();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL METADATA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return null;
    }    
    
    /**
     * aprobarRegistrio, aprueba los registros de la tabla placa
     * @autor Ing. Diogenes Bastidas Morales
     * @param: usuario, estado, placa
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void aprobarRegistrio(String usuario, String estado, String placa ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_APROBACION);
                st.setString(1, estado);
                st.setString(2, usuario);
                st.setString(3, placa);
                ////System.out.println(st);
                
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL APROBAR LA PLACA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    
    /**
     * buscaTipoXPlaca, busca el tipo de una placa
     * @autor Ing. Andres Martinez
     * @param: placa
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public String buscaTipoXPlaca(String placa_no) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        String grupo_equipo="";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(SELECT_TIPOXPLACA);
            psttm.setString(1, placa_no);
            
            rs = psttm.executeQuery();
            if (rs.next()){
                
                grupo_equipo=rs.getString("grupo_equipo");
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return grupo_equipo;
    }
    
    
    /**
     * Metodo cambiarEstadoPlaca, cambia el estado, de una placa
     * Activo o Inactivo
     * param: placa, estado
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void cambiarEstadoPlaca(String doc, String estado )throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            st= conPostgres.prepareStatement(this.SQL_CAMBIAR_ESTADO);
            st.setString(1,  estado);
            st.setString(2,  doc);
            
            String sql = st.toString();
            
            sql = estado.equals("A")? sql.replaceAll("#VETO#", " , veto = 'N' ") : sql.replaceAll("#VETO#", " , veto = 'S' ");
            
            st.executeUpdate( sql );
        }catch(Exception e){
            throw new SQLException("No se pudo modificar la Placa "+ placa.getPlaca() + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    /**
     * Metodo buscarPlacaCGA, metodo que busca la placa y setea en el
     * objeto solo el numero de la placa los nombres de propietario y conductor, el estado
     * param: placa, estado
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarPlacaCGA(String placa) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        veh = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(this.SQL_BUSCAR_PLACA);
            psttm.setString(1, placa);
            ////System.out.println("Query"+psttm);
            rs = psttm.executeQuery();
            if (rs.next()){
                veh = new Placa();
                veh.setPlaca(rs.getString("placa"));
                veh.setConductor(rs.getString("conductor"));
                veh.setPropietario(rs.getString("propietario"));
                veh.setEstado(rs.getString("estado"));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     * Metodo listVeh, permite obtener el numero de placa y el recurso dado una parametro.
     * @autor : Desconocido
     * @param : plaveh
     * @version : 1.0
     */
    public void listVeh( String plaveh )throws SQLException{
        String consulta =  "SELECT PLACA, RECURSO,AGENCIA,PROPIETARIO FROM PLACA WHERE PLACA = ?";
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        veh = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, plaveh);
                
                rs= st.executeQuery();
                while(rs.next()){
                    veh = new Placa();
                    veh.setPlaca(rs.getString("placa"));
                    veh.setRecurso(rs.getString("recurso"));
                    veh.setPropietario(rs.getString("propietario"));
                    veh.setAgencia(rs.getString("agencia"));
                }
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LAS PLACAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
  
  
    /**
     * buscarPlaca, busca tda la informacion en la tabla de placa
     * @autor Ing. Osvaldo P�rez Ferrer
     * @param: placa
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void buscarPlaca(String noPlaca) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        placa = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(BUSCAR_PLACA);
            psttm.setString(1, noPlaca);
            
            rs = psttm.executeQuery();
            if (rs.next()){
                placa = new Placa();
                placa.setReg_status(rs.getString("estado"));
                placa.setPlaca(rs.getString("placa"));
                placa.setCondicion(rs.getString("condicion"));
                placa.setTarjetaoper(rs.getString("tarjetaoper"));
                String venCtar = rs.getString("venctarjetaoper").trim();
                placa.setVenctarjetaoper((!venCtar.equals("0099-01-01")) ? venCtar :"");
                placa.setMarca(rs.getString("marca"));
                placa.setClase(rs.getString("clase"));
                placa.setCapacidad(rs.getString("capacidad"));
                placa.setCarroceria(rs.getString("carroceria"));
                placa.setModelo(rs.getString("modelo"));
                placa.setColor(rs.getString("color"));
                placa.setNomotor(rs.getString("nomotor"));
                placa.setNochasis(rs.getString("nochasis"));
                placa.setNoejes(rs.getString("noejes"));
                placa.setAgencia(rs.getString("agencia"));
                placa.setDimcarroceria(rs.getString("dimcarroceria"));
                String venCO = rs.getString("venseguroobliga").trim();
                placa.setVenseguroobliga((!venCO.trim().equals("0099-01-01")) ? venCO:"");
                placa.setHomologado(rs.getString("homologado"));
                placa.setAtitulo(rs.getString("atitulo"));
                placa.setEmpresaafil(rs.getString("empresaafil"));
                placa.setPropietario(rs.getString("propietario"));
                placa.setConductor(rs.getString("conductor"));
                placa.setTenedor(rs.getString("tenedor"));
                placa.setTipo(rs.getString("tipo"));
                placa.setTara(rs.getString("tara"));
                placa.setLargo(rs.getString("largo"));
                placa.setAlto(rs.getString("alto"));
                placa.setLlantas(rs.getString("llantas"));
                placa.setAncho(rs.getString("ancho"));
                placa.setPiso(rs.getString("piso"));
                placa.setCargue(rs.getString("cargue"));
                placa.setVolumen(rs.getString("volumen"));
                placa.setRecurso(rs.getString("recurso"));
                placa.setGrupoid(rs.getString("grupoid"));
                placa.setNombre(rs.getString("nombre"));
                placa.setGrupo(rs.getString("grupo"));
                placa.setEstadoequipo(rs.getString("estadoequipo"));
                placa.setLocalizacion(rs.getString("localizacion"));
                placa.setPolizasoat(rs.getString("polizasoat"));
                placa.setFechacrea((!rs.getString("fechacrea").equals("0099-01-01")) ? rs.getString("fechacrea"):"");
                placa.setUsuariocrea(rs.getString("usuariocrea"));
                //Nuevo 20-09-2005
                placa.setNumero_rin(rs.getString("numero_rin"));
                placa.setPlaca_trailer(rs.getString("placa_trailer"));
                placa.setGrado_riesgo(rs.getString("grado_riesgo"));
                placa.setReg_nal_carga(rs.getString("reg_nal_carga"));
                placa.setFecvenreg((!rs.getString("fecvenreg").trim().equals("0099-01-01")) ? rs.getString("fecvenreg"):"");
                placa.setPoliza_andina((!rs.getString("poliza_andina").trim().equals("0099-01-01")) ? rs.getString("poliza_andina"):"");
                placa.setFecvenandina((!rs.getString("fecvenandina").trim().equals("0099-01-01")) ? rs.getString("fecvenandina"):"");
                placa.setCapacidad_trailer(rs.getString("capacidad_trailer"));
                placa.setTarempresa(rs.getString("tarempresa"));
                placa.setFecvenempresa((!rs.getString("fecvenempresa").trim().equals("0099-01-01")) ? rs.getString("fecvenempresa"):"");
                placa.setTarprop(rs.getString("tarprop"));
                placa.setFecvenprop((!rs.getString("fecvenprop").trim().trim().equals("0099-01-01")) ? rs.getString("fecvenprop"):"");
                placa.setTarhabil(rs.getString("tarhabil"));
                placa.setFecvenhabil((!rs.getString("fecvenhabil").trim().equals("0099-01-01")) ? rs.getString("fecvenhabil"):"");
                placa.setCertgases(rs.getString("certemgases"));
                placa.setFecvengases((!rs.getString("fecvegases").trim().equals("0099-01-01")) ? rs.getString("fecvegases"):"");
                placa.setVprop(rs.getString("propietario"));
                placa.setEmpAfiliada(rs.getString("nombre"));
                placa.setCiudadAfiliada(rs.getString("nomciu"));
                placa.setPaisAfiliada(rs.getString("country_name"));
                //dbastidas 2006-01-17
                placa.setAprobado(rs.getString("aprobado"));
                placa.setFecvresp_civil(!rs.getString("fecvres_civil").equals("0099-01-01 00:00:00")? rs.getString("fecvres_civil"):"");
                placa.setResp_civil(rs.getString("resp_civil"));
                placa.setCiudad_tarjeta(rs.getString("ciu_tarjeta"));
                placa.setGrupo_equipo(rs.getString("grupo_equipo"));
                placa.setDescuento_equipo(rs.getString("descuento_equipo"));
                placa.setClasificacion_equipo(rs.getString("clasificacion_equipo"));
                
                //Osvaldo
                placa.setOperador_gps( rs.getString("operador_gps") );
                placa.setPasswd_seguimiento( rs.getString("gps_passwd") );
                placa.setTiene_gps( placa.getOperador_gps().equals("")? "NO" : "SI" );
                placa.setVeto( rs.getString("veto") );
                
                //AMATURANA 6.1.2007
                placa.setCapac_gal( rs.getString("capac_gal") );
                placa.setCapac_mts( rs.getString("capac_mts") );
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo eliminarPlaca
     * param: placa, estado
     * @autor : Ing. Luigi
     * @version : 1.0
     */
    public void eliminarPlaca(String placa)throws SQLException {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            st= conPostgres.prepareStatement(this.SQL_PLACA_DELETE);
            st.setString(1,  placa);
            st.executeUpdate();
        }catch(Exception e){
            throw new SQLException("No se pudo Eliminar la Placa " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /**
     * Metodo buscarEstadoPlaca
     * param: placa
     * @autor : Ing. Luigi
     * @version : 1.0
     */
    public String buscarEstadoPlaca(String placa)throws SQLException {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        ResultSet rs = null;
        String estado = "";
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            st= conPostgres.prepareStatement(this.SQL_BUSCAR_ESTADO);
            st.setString(1,  placa);
            rs = st.executeQuery();
                
            if(rs.next()){
                estado = rs.getString("estado");
            }
            
        }catch(Exception e){
            throw new SQLException("No se pudo Eliminar la Placa " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return estado;
    }
    
    /**
     * Metodo cambiarEstadoPlaca
     * param: placa, estado
     * @autor : Ing. Luigi
     * @version : 1.0
     */
    public void cambiarEstadoPlacaAnulado(String placa)throws SQLException {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            st= conPostgres.prepareStatement(this.SQL_CAMBIAR_ESTADO_ANULADO);
            st.setString(1,  placa);
            st.executeUpdate();
        }catch(Exception e){
            throw new SQLException("No se pudo Cambiar el estado de la Placa " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /**
     * isGrupoDespacho, retorna true o false, si el grupo de la placa puede despachar o no
     * @autor Ing. Karen Reales
     * @param: cliente y grupo
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public boolean isGrupoDespacho(String cliente, String grupo) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null,rs2=null;
        boolean sw=false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement("select * from tablagen where table_type='GROUPCLIEN' and table_code like ?");
            psttm.setString(1, grupo+"%");
            rs = psttm.executeQuery();
            if (rs.next()){
                psttm = con.prepareStatement("select * from tablagen where table_type='GROUPCLIEN' and table_code=?");
                psttm.setString(1, grupo+cliente);
                rs2 = psttm.executeQuery();
                if(rs2.next()){
                    sw=true;
                }
            }
            else{
                sw=true;
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL GRUPO EN TABLAGEN" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
     /**
     * Metodo searchPlaca, permite buscar los datos de una placa.
     * @autor : Desconocido
     * @param : placaVeh
     * @version : 1.0
     */
    public void searchPlaca(String placaVeh )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        placa=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SELECT_PLACA);
                st.setString(1,placaVeh.toUpperCase());
                //System.out.println("Placa "+st.toString());
                rs = st.executeQuery();
                
                if(rs.next()){
                    placa = new Placa();
                    placa.setPlaca(rs.getString("placa"));
                    placa.setPropietario(rs.getString("propietario"));
                    placa.setVencSOAT(rs.getDate("venseguroobliga"));
                    placa.setProveedor(rs.getString("id_mims"));
                    placa.setConductor(rs.getString("conductor"));
                    placa.setClasificacion(rs.getString("clasificacion"));
                    placa.setNombre(rs.getString("nombre"));
                    placa.setVeto(rs.getString("veto"));
                    placa.setTractomula(false);
                    placa.setTraile(false);
                    placa.setVetado(false);
                    placa.setTieneFoto(false);
                    placa.setGrupoid(rs.getString("GRUPOID"));
                    
                    //NUMERO DE RIN Y NUMERO DE EJES PARA LA ORDEN DE CARGUE
                    placa.setNoejes(rs.getString("noejes"));
                    placa.setNumero_rin(rs.getString("numero_rin"));
                    
                    if("C".equals(rs.getString("tipo_recurso"))){
                        placa.setTractomula(true);
                    }
                    if("T".equals(rs.getString("tipo_recurso"))){
                        placa.setTraile(true);
                    }
                    if("S".equals(rs.getString("veto"))){
                        placa.setVetado(true);
                    }
                    if("S".equals(rs.getString("vetop"))){
                        placa.setVetado(true);
                    }
                    st = con.prepareStatement(SQL_BUSCARFOTO);
                    st.setString(1,placaVeh.toUpperCase());
                    rs2 = st.executeQuery();
                    if(rs2.next()){
                        placa.setTieneFoto(true);
                    }
                    
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }

/**
     * listarPlacaNoAprobada, lista las placa no aprobados de acuerdo al usuario
     * asignado aprobar las placas.
     * @autor Ing. Diogenes Bastidas Morales
     * mod: Ing. Iv�n Devia
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void listarPlacaNoAprobada(String login )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vPlacas = new Vector();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_PLACANOAPROBADA);
                st.setString(1, login);
                rs = st.executeQuery();
                
                while(rs.next()) {
                    placa = new Placa();
                    placa.setPlaca(rs.getString("placa"));
                    placa.setPropietario(rs.getString("propietario"));
                    placa.setConductor(rs.getString("conductor"));
                    placa.setAprobado(rs.getString("aprobado"));
                    placa.setUsuariocrea(rs.getString("usuariocrea"));
                    vPlacas.add(placa);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL USUARIO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo actualizaPlaca, permite actualizar los datos de una placa a la BD y guarda registro de modificacionde propietario.
     * @autor : Ing Enrique De Lavalle.
     * @param : Objeto placa
     * @version : 1.0
     */
    public void actualizar(Placa placa, String nitP, String nombreP) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(UPDATE_PLACA2);
            
            psttm.setString(1, placa.getReg_status());
            psttm.setString(2, placa.getCondicion());
            psttm.setString(3, placa.getTarjetaoper());
            psttm.setString(4, placa.getVenctarjetaoper());
            psttm.setString(5, placa.getMarca());
            psttm.setString(6, placa.getClase());
            psttm.setString(7, placa.getCapacidad());
            psttm.setString(8, placa.getCarroceria());
            psttm.setString(9, placa.getModelo());
            psttm.setString(10, placa.getColor());
            psttm.setString(11, placa.getNomotor());
            psttm.setString(12, placa.getNochasis());
            psttm.setString(13, placa.getNoejes());
            psttm.setString(14, placa.getAgencia());
            psttm.setString(15, placa.getDimcarroceria());
            psttm.setString(16, placa.getVenseguroobliga());
            psttm.setString(17, placa.getHomologado());
            psttm.setString(18, placa.getAtitulo());
            psttm.setString(19, placa.getEmpresaafil());
            psttm.setString(20, placa.getPropietario());
            psttm.setString(21, placa.getConductor());
            psttm.setString(22, placa.getTenedor());
            psttm.setString(23, placa.getTipo());
            psttm.setString(24, placa.getTara());
            psttm.setString(25, placa.getLargo());
            psttm.setString(26, placa.getAlto());
            psttm.setString(27, placa.getLlantas());
            psttm.setString(28, placa.getAncho());
            psttm.setString(29, placa.getPiso());
            psttm.setString(30, placa.getCargue());
            psttm.setString(31, placa.getVolumen());
            psttm.setString(32, placa.getFechaultact());
            psttm.setString(33, placa.getUsuario());
            psttm.setString(34, placa.getRecurso());
            psttm.setString(35, placa.getGrupoid());
            psttm.setString(36, placa.getGrupo());
            psttm.setString(37, placa.getEstadoequipo());
            psttm.setString(38, placa.getLocalizacion());
            //Nuevo 20-09-2005
            psttm.setString(39, placa.getNumero_rin());
            psttm.setString(40,placa.getPlaca_trailer());
            psttm.setInt(41,placa.getGrado_riesgo());
            psttm.setString(42,placa.getReg_nal_carga());
            psttm.setString(43,placa.getFecvenreg());
            psttm.setString(44,placa.getPoliza_andina());
            psttm.setString(45,placa.getFecvenandina());
            psttm.setString(46,placa.getCapacidad_trailer());
            psttm.setString(47,placa.getTarempresa());
            psttm.setString(48,placa.getFecvenempresa());
            psttm.setString(49,placa.getTarprop());
            psttm.setString(50,placa.getFecvenprop());
            psttm.setString(51,placa.getTarhabil());
            psttm.setString(52,placa.getFecvenhabil());
            //jose 2006-02-15
            psttm.setString(53,placa.getCiudad_tarjeta());
            psttm.setString(54,placa.getFecvresp_civil());
            psttm.setString(55,placa.getResp_civil());
            psttm.setString(56,placa.getDescuento_equipo());
            psttm.setString(57,placa.getClasificacion_equipo());
            psttm.setString(58,placa.getPolizasoat());
            psttm.setString(59,placa.getCertgases());
            psttm.setString(60,placa.getFecvengases());
            //OJO
            psttm.setString(61,placa.getGrupo_equipo());
            psttm.setString(62,placa.getNombre());
            psttm.setString(63, placa.getOperador_gps());//Osvaldo
            psttm.setString(64, placa.getPasswd_seguimiento());//Osvaldo
            //***************** AMATURANA
            psttm.setString(65,placa.getCapac_gal());
            psttm.setString(66,placa.getCapac_mts());
            //*************************************
           //luigi
            psttm.setString(67, placa.getVeto());
            psttm.setString(68, placa.getPlaca());
            
            
            psttm.executeUpdate();
            AutorizacionFleteDAO aflete = new AutorizacionFleteDAO();
            aflete.insertControlActualizacion(psttm.toString(), placa.getUsuariocrea());
            
            this.historialPropietario(placa.getPlaca(),nitP, nombreP, placa.getUsuariocrea());
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    
    /**
     * Guarda registro en historial para las modificaciones de sede de pago del proveedor.
     * @autor Ing. Enrique De Lavalle Rizo. 2007/04/18
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */ 
    public void historialPropietario(String placa,String nitP,String nombreP, String user) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(SQL_HISTORIAL_PROPIETARIO);
            psttm.setString(1, placa);
            psttm.setString(2, nitP);
            psttm.setString(3, nombreP);
            psttm.setString(4, user);
            psttm.setString(5, user);            
            psttm.executeUpdate();     
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE AL GUARDAR HISTORIAL DE LA MODIFICACION DEL PROPIETARIO DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
      /**
     * Metodo existeRecurso , Metodo que retorna un boolean true cuando existe el recurso ingresado para la placa
     * @autor : Ing. Enrique De Lavalle
     * @param : String idRecurso
     * @version : 1.0
     */
    public boolean existeRecurso(String idRecurso) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_EXISTE_RECURSO);
                st.setString(1, idRecurso);
                
                ////System.out.println("Query: "+st.toString());
                rs = st.executeQuery();
                while(rs.next()) {
                    sw=true;
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR AL INSERTAR EL ESTADO" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    e.printStackTrace();
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    
    /**
     * Metodo existeRecurso , Metodo que retorna un boolean true cuando existe el recurso ingresado para la placa
     * @autor : Ing. Enrique De Lavalle
     * @param : String idRecurso
     * @version : 1.0
     */
    public String obtenerTipoPlacaRecurso(String idRecurso) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String tipoPlaca = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_OBTENER_TPLACA_REC);
                st.setString(1, idRecurso);
                
                ////System.out.println("Query: "+st.toString());
                rs = st.executeQuery();
                while(rs.next()) {
                    tipoPlaca = rs.getString("tipo_recurso");
                }
            }
        }catch(SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR AL INSERTAR EL ESTADO" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    e.printStackTrace();
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return tipoPlaca;
    }

    
    
}
