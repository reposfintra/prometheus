/******************************************************************
* Nombre ......................SeriesService.java
* Descripci�n..................Clase que maneja los servicios para las series
* Autor........................Armando Oviedo
* Fecha........................04/11/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/


package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

public class SeriesService{

    private SeriesDAO SeriesDataAccess;
    private String    CiudadesJs;
    private List      Ciudades;
    private String    DocumentoJs;
    private List      Documento;
    private List      Banco;
    private List      Registros;
    private String    BancoJs;
    private String    AgenciaJs;
    private String    CuentaJs;
    private Series    serie;

    //AMATURANA 13.04.2007
    private TreeMap conceptos;
    private String UsuariosJs;
    private String UsuarioSerieJs;

    public SeriesService(){
        SeriesDataAccess = new SeriesDAO();
    }
    public SeriesService(String dataBaseName){
        SeriesDataAccess = new SeriesDAO(dataBaseName);
    }




    public void obtenerSerie( String document_type, Usuario usuario)throws SQLException{
        try{
            SeriesDataAccess.searchSerie(document_type,usuario);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }

    }
    public String asignarSerie( Series serie, Usuario usuario)throws SQLException{
        try{
            return SeriesDataAccess.updateSerie(serie,usuario);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }

    }
    public void Insertar(String base) throws SQLException {
        try {
            SeriesDataAccess.Insertar(serie.getDistrito(), serie.getCodigo_Ciudad(), serie.getCodigo_Documento(), serie.getBanco(), serie.getAgencia_Banco(), serie.getCuenta(), serie.getPrefijo(), serie.getN_Inicial(), serie.getN_Final(), serie.getL_Numero(), serie.getUsuario(),base);
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Insertar [SeriesService]...\n"+e.getMessage());
        }
    }

    public void Modificar() throws SQLException {
        try {
            SeriesDataAccess.Modificar(serie.getDistrito(), serie.getCodigo_Ciudad(), serie.getCodigo_Documento(), serie.getBanco(), serie.getAgencia_Banco(), serie.getCuenta(), serie.getPrefijo(), serie.getN_Inicial(), serie.getN_Final(), serie.getL_Numero(), serie.getUsuario());
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Modificar [SeriesService]...\n"+e.getMessage());
        }
    }

    /*public void Buscar_Series_one() throws SQLException {
        Registros = new LinkedList();
        try {
            Registros = SeriesDataAccess.Buscar_Series(serie.getDistrito(), serie.getCodigo_Ciudad(), serie.getDocumento(), serie.getBanco(), serie.getAgencia_Banco(), serie.getN_Inicial());
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Buscar_Series_one [SeriesService]...\n"+e.getMessage());
        }
    }*/

    public void Buscar_Series_all() throws SQLException {
        try {
            Registros = (List) SeriesDataAccess.Buscar_Series(serie.getCodigo_Ciudad(), serie.getCodigo_Documento());
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Buscar_Series_all [SeriesService]...\n"+e.getMessage());
        }
    }
     public boolean Existe_Series() throws SQLException {
        boolean Existe = false;
        try {
            Existe = SeriesDataAccess.Existe_Series(serie.getDistrito(), serie.getCodigo_Ciudad(), serie.getCodigo_Documento(),serie.getPrefijo(), serie.getBanco(), serie.getAgencia_Banco(), serie.getCuenta(), serie.getN_Inicial(), serie.getN_Final());
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Existe_Series [SeriesService]...\n"+e.getMessage());
        }
        return Existe;
    }

    public void Anular_Series(String Distrito, String Ciudad, String Documento, String Banco, String Agencia, String Cuenta, String N_Inicial) throws SQLException {
        try {
            SeriesDataAccess.Anular_Series(Distrito, Ciudad, Documento, Banco, Agencia, Cuenta, N_Inicial);
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Anular_Serie [SeriesService]...\n"+e.getMessage());
        }
    }

    public void No_Anular_Series(String Distrito, String Ciudad, String Documento, String Banco, String Agencia, String Cuenta, String N_Inicial) throws SQLException {
        try {
            SeriesDataAccess.No_Anular_Series(Distrito, Ciudad, Documento, Banco, Agencia, Cuenta, N_Inicial);
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Anular_Serie [SeriesService]...\n"+e.getMessage());
        }
    }

    public void Buscar_Documento_Js() throws SQLException {
        try {
            DocumentoJs = SeriesDataAccess.Buscar_Tipo_Documento_Js();
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Buscar_Documento_Js [SeriesService]...\n"+e.getMessage());
        }
    }

    public void Buscar_Documento() throws SQLException {
        try {
            Documento = SeriesDataAccess.Buscar_Tipo_Documento();
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Buscar_Documento [SeriesService]...\n"+e.getMessage());
        }
    }

    public void Buscar_Banco() throws SQLException {
        try {
            Banco = SeriesDataAccess.Buscar_Banco();
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Buscar_Banco [SeriesService]...\n"+e.getMessage());
        }
    }

    public void Buscar_Banco_Js() throws SQLException {
        try {
            BancoJs = SeriesDataAccess.Buscar_Banco_JS();
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Buscar_Banco_JS [SeriesService]...\n"+e.getMessage());
        }
    }

    public void Buscar_Agencia_Js() throws SQLException {
        try {
            AgenciaJs = SeriesDataAccess.Buscar_Agencia_JS();
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Buscar_Agencia_JS [SeriesService]...\n"+e.getMessage());
        }
    }

    public void Buscar_Ciudades_Js() throws SQLException {
        try {
            CiudadesJs = SeriesDataAccess.Buscar_Ciudades_Js();
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Buscar_Ciudades_JS [SeriesService]...\n"+e.getMessage());
        }
    }

    public void Buscar_Ciudades() throws SQLException {
        try {
            Ciudades = SeriesDataAccess.Buscar_Ciudades();
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Buscar_Ciudades [SeriesService]...\n"+e.getMessage());
        }
    }

    public void Buscar_Cuenta_Js() throws SQLException {
        try {
            CuentaJs = SeriesDataAccess.Buscar_Cuenta_Js();
        }
        catch(SQLException e) { e.printStackTrace();
            throw new SQLException("Error en la rutina Buscar_Cuneta_JS [SeriesService]...\n"+e.getMessage());
        }
    }

    public void Reinicial() {
        this.Registros = new LinkedList();
        this.serie     = Series.Reiniciar();
    }

    //Setter
    public void setSerie(Series valor) {
        this.serie = valor;
    }

    public void setRegistros(List valor) {
        this.Registros = valor;
    }


    public Series getSerie( )throws SQLException{

        return SeriesDataAccess.getSerie();

    }

    //Getter
    public Series getserie() {
        return this.serie;
    }

    public String getCiudadesJs() {
        return this.CiudadesJs;
    }

    public List getCiudades() {
        return this.Ciudades;
    }

    public List getRegistros() {
        return this.Registros;
    }

    public String getDocumentoJs() {
        return this.DocumentoJs;
    }

    public List getDocumento() {
        return this.Documento;
    }

    public List getBanco() {
        return this.Banco;
    }

    public String getBancoJs() {
        return this.BancoJs;
    }

    public String getAgenciaJs() {
        return this.AgenciaJs;
    }

    public String getCunetaJs() {
        return this.CuentaJs;
    }
    //KAREN REALES
    public void buscaSerie(String banco, String cuenta, int ultnum)throws SQLException{
        SeriesDataAccess.buscaSerie( banco, cuenta, ultnum);
    }

    /**
     * Metodo asignarSerie, permite asignar un numero de serie
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void asignarSeries(Series serie, Usuario usuario)throws SQLException{
        try{
            SeriesDataAccess.asignarSeries(serie,usuario);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }

    }
    public String updateSerieCheque(Series serie, Usuario usuario)throws SQLException{
        return SeriesDataAccess.updateSerieCheque(serie, usuario);
    }

    //********  Armando Oviedo

    /**
     * M�todo que busca y llena un vector con todos los grupos
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @see.........SeriesDAO.java
     * @version.....1.0.
     **/
    public void BuscarTodosGrupos() throws Exception{
        SeriesDataAccess.BuscarTodosGrupos();
    }

    /**
     * M�todo que obtiene la lista de grupos
     * @autor.......Armando Oviedo
     * @see.........getListaGrupos - SeriesDAO.java
     * @version.....1.0.
     **/
    public Vector getListaGrupos() throws Exception{
        return SeriesDataAccess.getListaGrupos();
    }

    /**
     * M�todo utilizado para enviar un mail como advertencia a la finalizaci�n de series
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @see.........sendMailMessage - SeriesDAO.java
     * @param.......String mailto, mailcopyto, mailbody;
     **/
    public void sendMailMessage(String mailto, String mailcopyto, String mailbody) throws SQLException{
        SeriesDataAccess.sendMailMessage(mailto, mailcopyto, mailbody);
    }

    /**
     * M�todo devuelve el nombre de un documento
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @see.........SeriesDAO.java
     * @param.......String tipodocumento (doctype)
     **/
    public String getNombreDocumento(String doctype) throws Exception{
        return SeriesDataAccess.getNombreDocumento(doctype);
    }

     /**
     * M�todo que devuelve el estado y el valor restante de la serie
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @see.........SeriesDAO.java
     * @param....... beans serie
     **/
    public String getActualState(Series s){
        String state = "";
        String rest ="";
        Vector ls = s.getListaSeries();
        if(ls.size()==1){
            SerieGroupValidation tmp = (SerieGroupValidation)(ls.elementAt(0));
            int restantes = tmp.getRestantes();
            if(restantes <= 10) state = "Rojo";
            else if(restantes < 30) state = "Amarillo";
            else state = "Verde";

            rest = String.valueOf(restantes);
        }
        else{
            int cont=0;
            for(int i=0;i<ls.size();i++){
                SerieGroupValidation tmp = (SerieGroupValidation)(ls.elementAt(i));
                cont += tmp.getRestantes();
            }
            if(cont <= 10) state = "Rojo";
            else if(cont < 30) state = "Amarillo";
            else state = "Verde";

            rest = String.valueOf(cont);
        }
        return state+","+rest;
    }
    //*********

    /**
     * Getter for property conceptos.
     * @return Value of property conceptos.
     */
    public java.util.TreeMap getConceptos() {
        return conceptos;
    }

    /**
     * Setter for property conceptos.
     * @param conceptos New value of property conceptos.
     */
    public void setConceptos(java.util.TreeMap conceptos) {
        this.conceptos = conceptos;
    }

    /**
     * Getter for property UsuariosJs.
     * @return Value of property UsuariosJs.
     */
    public java.lang.String getUsuariosJs() {
        return UsuariosJs;
    }

    /**
     * Setter for property UsuariosJs.
     * @param UsuariosJs New value of property UsuariosJs.
     */
    public void setUsuariosJs(java.lang.String UsuariosJs) {
        this.UsuariosJs = UsuariosJs;
    }

     /**
     * Getter for property UsuarioSerieJs.
     * @return Value of property UsuarioSerieJs.
     */
    public java.lang.String getUsuarioSerieJs() {
        return UsuarioSerieJs;
    }

    /**
     * Setter for property UsuarioSerieJs.
     * @param UsuarioSerieJs New value of property UsuarioSerieJs.
     */
    public void setUsuarioSerieJs(java.lang.String UsuarioSerieJs) {
        this.UsuarioSerieJs = UsuarioSerieJs;
    }

    //amaturana 18/04/2007
    public void loadUsuariosJs() throws SQLException{
        this.setUsuariosJs(this.SeriesDataAccess.loadUsuarios_Js());
    }

    //amaturana 18/04/2007
    public void insertarSerieCheque(String base) throws SQLException {
        SeriesDataAccess.insertarSerieCheque(
            serie.getDistrito(),
            serie.getCodigo_Ciudad(),
            serie.getCodigo_Documento(),
            serie.getBanco(),
            serie.getAgencia_Banco(),
            serie.getCuenta(),
            serie.getPrefijo(),
            serie.getN_Inicial(),
            serie.getN_Final(),
            serie.getL_Numero(),
            serie.getUsuario(),
            base,
            serie.getUsuarios(),
            serie.getConcepto());
    }

    //amaturana 18/04/2007
    public void loadUsuariosSerie_Js(String id) throws SQLException{
        this.setUsuarioSerieJs(this.SeriesDataAccess.loadUsuariosSerie_Js(id));
    }

    //amaturana 18/04/2007
    public void modificarSerieCheque(String base) throws SQLException {
        SeriesDataAccess.modificarSerieCheque(
            serie.getDistrito(),
            serie.getCodigo_Ciudad(),
            serie.getCodigo_Documento(),
            serie.getBanco(),
            serie.getAgencia_Banco(),
            serie.getCuenta(),
            serie.getPrefijo(),
            serie.getN_Inicial(),
            serie.getN_Final(),
            serie.getL_Numero(),
            serie.getUsuario(),
            serie.getUsuarios(),
            serie.getConcepto(),
            serie.getId(),
            base);
    }

    //amaturana 20/04/2007
    public void anularSerieCheque(int id) throws SQLException{
        this.SeriesDataAccess.anularSerieCheque(id);
    }

     public void actualizarSerie( String usuario, String document_type ) throws SQLException{//20101209
        this.SeriesDataAccess.actualizarSerie(usuario, document_type);
    }

     /***********************************************************
     * Metodo obtenerPrefix, retorna prefijo de la
     *           serie correspondiente al document_type
     * @author: Ing. Iris Vargas
     * @param:  String document_type
     * @return: String prefijo
     *          null si no existe o esta finalizada
     * @throws: En caso de un error de BD
     ***********************************************************/
    public String obtenerPrefix(String document_type) throws SQLException {
        return this.SeriesDataAccess.obtenerPrefix(document_type);
    }

}





