/*
 * CarroceriaService.java
 *
 * Created on 9 de noviembre de 2004, 02:53 PM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  AMENDEZ
 */
public class ColorService {
    
    private ColorDAO colorDataAccess;
    /** Creates a new instance of CarroceriaService */
    public ColorService() {
        colorDataAccess = new ColorDAO();
    }
    
    public TreeMap listar() throws SQLException{
        colorDataAccess.listar();
        return colorDataAccess.getCbxColor();
    }
    public String getNombreColor(String codigo) throws SQLException{
        return colorDataAccess.getNombreColor(codigo);
    }
}