/********************************************************************
 *      Nombre Clase.................   BancoDAO.java
 *      Descripci�n..................   DAO del archivo banco
 *      Autor........................
 *      Fecha........................
 *      Versi�n......................
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model;


import java.sql.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.beans.Banco;
import java.util.Vector;

public class BancoDAO extends com.tsp.operation.model.DAOS.MainDAO{
    //Jose
    private Banco banc;
    private Vector VecBancos;
    
    
    public BancoDAO () {
        super ( "BancoDAO.xml" );
    }
    public BancoDAO (String dataBaseName) {
        super ( "BancoDAO.xml", dataBaseName );
    }
    
    /**
     * Obtiene la propiedad banc
     * @autor Ing. Jose De La Rosa
     * @version 1.0
     */
    public Banco getBannco () {
        return banc;
    }
    
    /**
     * Setea la propiedad banc
     * @autor Ing. Jose De La Rosa
     * @version 1.0
     */
    public void setBannco (Banco banc) {
        this.banc = banc;
    }
    
    /**
     * Obtiene la propiedad VecBancos
     * @autor Ing. Jose De La Rosa
     * @version 1.0
     */
    public Vector getBancos () {
        return VecBancos;
    }
    
    /**
     * Setea la propiedad VecBancos
     * @autor Ing. Jose De La Rosa
     * @version 1.0
     */
    public void setBancos (Vector VecBancos) {
        this.VecBancos = VecBancos;
    }
    
    /**
     * Ingresa un registro en el archivo banco.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void insertarBanco () throws SQLException {
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_INSERT_BANCO");
            st.setString (1, banc.getDstrct ());
            st.setString (2, banc.getBanco ());
            st.setString (3, banc.getBank_account_no ());
            st.setString (4, banc.getCodigo_Agencia ());
            st.setString (5, banc.getCuenta ());
            st.setString (6, banc.getMoneda ());
            st.setString (7, banc.getCodigo_cuenta ());
            st.setString (8, banc.getUsuario_modificacion ());
            st.setString (9, banc.getUsuario_creacion ());
            st.setString (10, banc.getBase ());
            st.setString (11, banc.getPosbancaria ());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL INGRESAR UN BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_INSERT_BANCO");
        }
    }
    
    /**
     * Verifica si existe un registro en el archivo de banco.
     * @param dstrct C�digo del distrito
     * @param branch_code Nombre del banco
     * @param bank_account_no Nombre de la sucursal
     * @param account_number N�mero de la cuenta
     * @returns <code>true</code> si existe, <code>false</code> si no.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeBanco (String dstrct, String branch_code, String bank_account_no) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_EXISTE_BANCO");
            st.setString (1, dstrct);
            st.setString (2, branch_code);
            st.setString (3, bank_account_no);
            rs = st.executeQuery ();
            if (rs.next ()){
                sw = true;
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UN BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_EXISTE_BANCO");
        }
        return sw;
    }
    
    /**
     * Obtiene un registro del archivo de banco.
     * @param dstrct C�digo del distrito
     * @param branch_code Nombre del banco
     * @param bank_account_no Nombre de la sucursal
     * @param account_number N�mero de la cuenta
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public Banco obtenerBanco (String dstrct, String branch_code, String bank_account_no) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        Banco b = null;
        try {
            st = crearPreparedStatement ("SQL_OBTENER_BANCO");
            st.setString (1,dstrct);
            st.setString (2,branch_code);
            st.setString (3,bank_account_no);
            rs = st.executeQuery();
            if (rs.next()){
                b = new Banco ();
                b.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                b.setMoneda (rs.getString ("currency"));
                b.setCodigo_Agencia (rs.getString ("agency_id"));//jose de la rosa
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER UN BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_OBTENER_BANCO");
        }
        return b;
    }
    
    /*public Vector obtenerBancos() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector datos = new Vector();
        try {
     
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement(SQL_OBTENER_BANCOS);
                ////System.out.println(ps);
                rs = ps.executeQuery();
                while (rs.next()){
                    Banco b = new Banco();
                    b.setDstrct(rs.getString("dstrct"));
                    b.setBanco(rs.getString("branch_code"));
                    b.setBank_account_no(rs.getString("bank_account_no"));
                    b.setCodigo_Agencia(rs.getString("agency_id"));
                    b.setCuenta(rs.getString("account_number"));
                    b.setCodigo_cuenta(rs.getString("codigo_cuenta"));
                    b.setMoneda(rs.getString("currency"));
                    b.setNombre_Agencia(rs.getString("nombre"));
                    datos.addElement(b);
                    ////System.out.println("bancos -> "+b);
                }
            }
     
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
     
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return datos;
    }*/
    
    /**
     * Obtiene un arreglo de los resgitros del archivo de banco.
     * @returns Arreglo con los bancos.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public Vector obtenerBancos () throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector datos = new Vector ();
        try {
            st = crearPreparedStatement ("SQL_OBTENER_BANCOS");
            rs = st.executeQuery ();
            while (rs.next ()){
                Banco b = new Banco ();
                b.setDstrct (rs.getString ("dstrct"));
                b.setBanco (rs.getString ("branch_code"));
                b.setBank_account_no (rs.getString ("bank_account_no"));
                b.setCodigo_Agencia (rs.getString ("agency_id"));
                b.setCuenta (rs.getString ("account_number"));
                b.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                b.setMoneda (rs.getString ("currency"));
                datos.addElement (b);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER UN BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_OBTENER_BANCOS");
        }
        return datos;
    }
    
    /**
     * Obtiene un registro del archivo de banco.
     * @param dstrct C�digo del distrito
     * @param branch_code Nombre del banco
     * @param bank_account_no Nombre de la sucursal
     * @param account_number N�mero de la cuenta
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void searchBanco (String dstrct, String branch_code, String bank_account_no, String account_number)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        banc = null;
        try {
            st = crearPreparedStatement ("SQL_SEARCH_BANCO");
            st.setString (1, dstrct);
            st.setString (2, branch_code);
            st.setString (3, bank_account_no);
            st.setString (4, account_number);
            rs= st.executeQuery ();
            while(rs.next ()){
                banc = new Banco ();
                banc.setDstrct (rs.getString ("dstrct"));
                banc.setBanco (rs.getString ("branch_code"));
                banc.setBank_account_no (rs.getString ("bank_account_no"));
                banc.setCodigo_Agencia (rs.getString ("agency_id"));
                banc.setCuenta (rs.getString ("account_number"));
                banc.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                banc.setMoneda (rs.getString ("currency"));
                banc.setPosbancaria (rs.getString ("posicion_bancaria"));//Tito Andr�s 27.01.2006
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UN BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SEARCH_BANCO");
        }
    }
    /*
    public void searchDetalleBanco(String distrito, String banco, String sucursal, String cuenta, String agencia, String ccuenta) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        VecBancos=null;
     
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                st = con.prepareStatement(SQL_SERCH_DETALLE_BANCO);
                st.setString(1, distrito+"%");
                st.setString(2, banco+"%");
                st.setString(3, sucursal+"%");
                st.setString(4, cuenta+"%");
                st.setString(5, agencia+"%");
                st.setString(6, ccuenta+"%");
                rs = st.executeQuery();
                VecBancos = new Vector();
                while(rs.next()){
                    Banco b = new Banco();
                    b.setDstrct(rs.getString("dstrct"));
                    b.setBanco(rs.getString("branch_code"));
                    b.setBank_account_no(rs.getString("bank_account_no"));
                    b.setCodigo_Agencia(rs.getString("agency_id"));
                    b.setCuenta(rs.getString("account_number"));
                    b.setCodigo_cuenta(rs.getString("codigo_cuenta"));
                    b.setMoneda(rs.getString("currency"));
                    b.setNombre_Agencia(rs.getString("nombre"));
                    VecBancos.add(b);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS BANCOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }*/
    
    /**
     * Obtiene un registro del archivo de banco.
     * @param distrito C�digo del distrito
     * @param banco Nombre del banco
     * @param sucursal Nombre de la sucursal
     * @param cuenta N�mero de la cuenta
     * @param agencia C�digo de la agencia
     * @param ccuenta C�digo de la cuenta
     * @param posbanc Activo para posicion bancaria (S o N)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void searchDetalleBanco (String distrito, String banco, String sucursal, String cuenta, String agencia, String ccuenta, String posbanc) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        VecBancos=null;
        try {
            st = crearPreparedStatement ("SQL_SERCH_DETALLE_BANCO");
            st.setString (1, distrito+"%");
            st.setString (2, banco+"%");
            st.setString (3, sucursal+"%");
            st.setString (4, cuenta+"%");
            st.setString (5, agencia+"%");
            st.setString (6, ccuenta+"%");
            st.setString (7, posbanc+"%");
            rs = st.executeQuery ();
            VecBancos = new Vector ();
            while(rs.next ()){
                Banco b = new Banco ();
                b.setDstrct (rs.getString ("dstrct"));
                b.setBanco (rs.getString ("branch_code"));
                b.setBank_account_no (rs.getString ("bank_account_no"));
                b.setCodigo_Agencia (rs.getString ("agency_id"));
                b.setCuenta (rs.getString ("account_number"));
                b.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                b.setMoneda (rs.getString ("currency"));
                b.setNombre_Agencia (rs.getString ("nombre"));
                b.setPosbancaria (rs.getString ("posicion_bancaria"));
                VecBancos.add (b);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UN BANCO POR DETALLE, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH_DETALLE_BANCO");
        }
    }
    
    /**
     * Actualiza un registro del archivo de banco.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void updateBanco () throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_UPDATE_BANCO");
            st.setString (1, banc.getMoneda ());
            st.setString (2, banc.getCodigo_Agencia ());
            st.setString (3, banc.getCodigo_cuenta ());
            st.setString (4, banc.getUsuario_modificacion ());
            st.setString (5, banc.getPosbancaria ());
            st.setString (6, banc.getCuenta ());
            st.setString (7, banc.getDstrct ());
            st.setString (8, banc.getBanco ());
            st.setString (9, banc.getBank_account_no ());
            System.out.println(st.toString());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR UN BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    e.printStackTrace();
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                    
                }
            }
            desconectar ("SQL_UPDATE_BANCO");
        }
    }
    
    /**
     * Anula un registro del archivo de banco.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void anularBanco () throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_ANULAR_BANCO");
            st.setString (1, banc.getUsuario_modificacion ());
            st.setString (2, banc.getDstrct ());
            st.setString (3, banc.getBanco ());
            st.setString (4, banc.getBank_account_no ());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ANULAR UN BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_ANULAR_BANCO");
        }
    }
    
    /**
     * Obtiene los registro del archivo de banco asociados a un nombre de banco.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param nombre Nombre del  banco
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public Vector obtenerBancosPorNombre (String nombre) throws SQLException{
        Vector datos = new Vector ();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_BANCO_NOMBRE");
            st.setString (1, nombre);
            rs = st.executeQuery ();
            while (rs.next ()){
                Banco b = new Banco ();
                b.setDstrct (rs.getString ("dstrct"));
                b.setBanco (rs.getString ("branch_code"));
                b.setBank_account_no (rs.getString ("bank_account_no"));
                b.setCodigo_Agencia (rs.getString ("agency_id"));
                b.setCuenta (rs.getString ("account_number"));
                b.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                b.setMoneda (rs.getString ("currency"));
                datos.addElement (b);
                //////System.out.println("bancos -> "+b);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER BANCOS POR NOMBRE, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_BANCO_NOMBRE");
        }
        return datos;
    }
    
    /**
     * Obtiene las sucursales de los bancos activos para posici�n banacaria
     * teniendo en cuenta la agencia y el banco.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.1
     */
    public void obtenerSucursalesPorAgencia (String agencia, String banco, String dstrct) throws SQLException {
        this.VecBancos = new Vector ();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if( agencia.length ()!=0 ){
                st = crearPreparedStatement ("SQL_SUCURSALES_AGENCIA_PB");
                st.setString (1, agencia);
                st.setString (2, banco);
                st.setString (3, dstrct);
            } else {
                st = crearPreparedStatement ("SQL_SUCURSALES_AGENCIAALL_PB");
                st.setString (1, banco);
                st.setString (2, dstrct);
            }
            rs = st.executeQuery ();
            while (rs.next ()){
                Banco b = new Banco ();
                b.setDstrct (rs.getString ("dstrct"));
                b.setBanco (rs.getString ("branch_code"));
                b.setBank_account_no (rs.getString ("bank_account_no"));
                b.setCodigo_Agencia (rs.getString ("agency_id"));
                b.setCuenta (rs.getString ("account_number"));
                b.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                b.setMoneda (rs.getString ("currency"));
                b.setNombre_Agencia (rs.getString ("nomagencia"));
                this.VecBancos.add (b);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER SUCURSALES POR AGENCIA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if( agencia.length ()!=0 )
                desconectar ("SQL_SUCURSALES_AGENCIA_PB");
            else
                desconectar ("SQL_SUCURSALES_AGENCIAALL_PB");
        }
    }
    
    /**
     * Obtiene los bancos activos para posici�n banacaria pertenecientes a un distrito.
     * teniendo en cuenta la agencia y el banco.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param dstrct Distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.1
     */
    public void obtenerBancosAgenciaCia (String agencia, String dstrct) throws SQLException {
        this.VecBancos = new Vector ();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if( agencia.length ()!=0 ){
                st = crearPreparedStatement ("SQL_BANCOS_AGENCIA_DTRCT_PB");
                
                st.setString (1, agencia);
                st.setString (2, dstrct);
            } else {
                st = crearPreparedStatement ("SQL_BANCOS_AGENCIAALL_DTRCT_PB");
                st.setString (1, dstrct);
            }
            rs = st.executeQuery ();
            while (rs.next ()){
                Banco b = new Banco ();
                b.setDstrct (rs.getString ("dstrct"));
                b.setBanco (rs.getString ("branch_code"));
                b.setBank_account_no (rs.getString ("bank_account_no"));
                b.setCodigo_Agencia (rs.getString ("agency_id"));
                b.setCuenta (rs.getString ("account_number"));
                b.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                b.setMoneda (rs.getString ("currency"));
                b.setNombre_Agencia (rs.getString ("nomagencia"));
                this.VecBancos.add (b);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER LOS BANCOS ACTIVOS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if( agencia.length ()!=0 )
                desconectar ("SQL_BANCOS_AGENCIA_DTRCT_PB");
            else
                desconectar ("SQL_BANCOS_AGENCIAALL_DTRCT_PB");
        }
    }
    
    /**
     * Verifica si existe un registro en el archivo de banco.
     * @autor Ing. Andr�s Maturana D.
     * @param dstrct C�digo del distrito
     * @param branch_code Nombre del banco
     * @param bank_account_no Nombre de la sucursal
     * @param account_number N�mero de la cuenta
     * @returns <code>true</code> si existe, <code>false</code> si no.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String estadoBanco (String dstrct, String branch_code, String bank_account_no, String account_number) throws SQLException {
        String reg_status = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_ESTADO_BANCO");
            st.setString (1, dstrct);
            st.setString (2, branch_code);
            st.setString (3, bank_account_no);
            st.setString (4, account_number);
            rs = st.executeQuery ();
            if (rs.next ()){
                reg_status = rs.getString ("reg_status");
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER EL ESTADO DEL BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_ESTADO_BANCO");
        }
        return reg_status;
    }
    /**
     * Obtiene los bancos activos para posici�n banacaria pertenecientes a un distrito - sin imporatar la posici�n bancaria.
     * teniendo en cuenta la agencia y el banco.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param dstrct Distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.1
     */
    public void obtenerBancos (String agencia, String dstrct) throws SQLException {
        this.VecBancos = new Vector ();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_BANCOS_AGENCIA");
            st.setString (1, agencia + "%");
            st.setString (2, dstrct);
            rs = st.executeQuery ();
            while (rs.next ()){
                Banco b = new Banco ();
                b.setDstrct (rs.getString ("dstrct"));
                b.setBanco (rs.getString ("branch_code"));
                b.setBank_account_no (rs.getString ("bank_account_no"));
                b.setCodigo_Agencia (rs.getString ("agency_id"));
                b.setCuenta (rs.getString ("account_number"));
                b.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                b.setMoneda (rs.getString ("currency"));
                this.VecBancos.add (b);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER LOS BANCOS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_BANCOS_AGENCIA");
        }
    }
    
    /**
     * Obtiene las sucursales de los bancos sin importar la posici�n banacaria
     * teniendo en cuenta la agencia y el banco.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param banco Nombre del banco
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.1
     */
    public void obtenerSucursales (String agencia, String banco, String dstrct) throws SQLException {
        this.VecBancos = new Vector ();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_SUCURSALES_AGENCIA");
            st.setString (1, agencia + "%");
            st.setString (2, banco + "%");
            st.setString (3, dstrct);
            rs = st.executeQuery ();
            while (rs.next ()){
                Banco b = new Banco ();
                b.setDstrct (rs.getString ("dstrct"));
                b.setBanco (rs.getString ("branch_code"));
                b.setBank_account_no (rs.getString ("bank_account_no"));
                b.setCodigo_Agencia (rs.getString ("agency_id"));
                b.setCuenta (rs.getString ("account_number"));
                b.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                b.setMoneda (rs.getString ("currency"));
                this.VecBancos.add (b);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER LAS SUCURSAL, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SUCURSALES_AGENCIA");
        }
    }
    /**
     * Obtiene los bancos de un distrito
     * @autor Ing. David Pi�a Lopez
     * @param dstrct C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtenerBancos ( String dstrct ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        Banco b = null;
        this.VecBancos = new Vector ();
        try {
            st = crearPreparedStatement ("SQL_GET_BANCOS");
            st.setString ( 1, dstrct );            
            rs = st.executeQuery ();
            while( rs.next () ){
                b = new Banco ();
                b.setBanco ( rs.getString ( "branch_code" ) );
                this.VecBancos.add( b );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER LOS BANCOS obtenerBancos ( String dstrct ) , "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_GET_BANCOS");
        }        
    }
     /**
     * Obtiene un banco-sucursal
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del distrito
     * @param banco Nombre del banco
     * @param sucursal Nomnre de la sucursal
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtenerBancoSucursal(String dstrct, String banco, String sucursal) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        Banco b = null;
        
        try {
            st = crearPreparedStatement("SQL_OBTENER_BANCO");
            st.setString(1, dstrct);
            st.setString(2, banco);
            st.setString(3, sucursal);
            rs = st.executeQuery();
            
            if (rs.next()){                
                b = new Banco ();
                b.setDstrct (rs.getString ("dstrct"));
                b.setBanco (rs.getString ("branch_code"));
                b.setBank_account_no (rs.getString ("bank_account_no"));
                b.setCodigo_Agencia (rs.getString ("agency_id"));
                b.setCuenta (rs.getString ("account_number"));
                b.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                b.setMoneda (rs.getString ("currency"));
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LA MONEDA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_OBTENER_BANCO");
        }
        
        this.setBannco(b);
    }
    //David 23.12.05
    /**
     * Este m�todo genera un Vector con los Bancos pertenecientes a una agencia
     * @param String agencia
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public Vector obtenerBancosPorAgencia (String agencia) throws SQLException{
        Vector datos            = new Vector ();
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String sql              = "";
        try {
            
            if( agencia.toUpperCase().equals("OP")){
                sql = "SQL_ALL_BANCOS";
                st  = crearPreparedStatement(sql);
            }
            else{
                sql = "BANCOS_POR_AGENCIA";
                st  = crearPreparedStatement(sql);
                st.setString(1, agencia);
            }
            rs = st.executeQuery ();
            while (rs.next ()){
                Banco b = new Banco ();
                b.setDstrct (rs.getString ("dstrct"));
                b.setBanco (rs.getString ("branch_code"));
                b.setBank_account_no (rs.getString ("bank_account_no"));
                b.setCodigo_Agencia (rs.getString ("agency_id"));
                b.setCuenta (rs.getString ("account_number"));
                b.setCodigo_cuenta (rs.getString ("codigo_cuenta"));
                b.setMoneda (rs.getString ("currency"));
                datos.addElement (b);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER BANCOS POR AGENCIA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (sql);
        }
        return datos;
    }
    
       /**
     * LUIGI........
     * Verifica si existe un registro en el archivo de cuentas.
     * @param cuenta
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeCuenta (String account_number) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("EXISTE_CUENTA");
            st.setString (1, account_number);
            rs = st.executeQuery ();
            if (rs.next ()){
                sw = true;
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UNA CUENTA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("EXISTE_CUENTA");
        }
        return sw;
    }
    
     /**
     * LUIGI........
     * Verifica si existe un registro existe como registro anulado.
     * @param cuenta
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String bancoAnulado (String distrito, String banco, String sucursal) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String sw = "";
        try {
            st = crearPreparedStatement ("SQL_ESTADO_BANCO_2");
            st.setString (1, distrito);
            st.setString (2, banco);
            st.setString (3, sucursal);
            rs = st.executeQuery ();
            if (rs.next ()){
                sw = rs.getString(1);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR EL BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_ESTADO_BANCO_2");
        }
        return sw;
    }
    
    
    /**
     * LUIGI........
     * Retira la anulacion de un banco en especifico.
     * @param cuenta
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String bancoAnuladoCambiar (String distrito, String banco, String sucursal) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String sw = "";
        try {
            st = crearPreparedStatement ("SQL_ACTUALIZAR_ESTADO_BANCO");
            st.setString (1, distrito);
            st.setString (2, banco);
            st.setString (3, sucursal);
            st.executeUpdate ();
   
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR EL BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_ACTUALIZAR_ESTADO_BANCO");
        }
        return sw;
    }
    
    public boolean bancoExiste (String dstrct, String branch_code, String bank_account_no) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_EXISTE_BANCO_2");
            st.setString (1, dstrct);
            st.setString (2, branch_code);
            st.setString (3, bank_account_no);
            rs = st.executeQuery ();
            if (rs.next ()){
                sw = true;
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UN BANCO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_EXISTE_BANCO_2");
        }
        return sw;
    }

    public String obtenerCuentaHC(String handle_code) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String cuenta = "";
        try {
            st = crearPreparedStatement ("SQL_OBTENER_CUENTA_HC");
            st.setString (1, handle_code);
            
            rs = st.executeQuery ();
            if (rs.next ()){
                cuenta = rs.getString(1);
            }
   
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL OBTENER_CUENTA_HC, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL OBTENER_CUENTA_HC" + e.getMessage () );
                }
            }
            desconectar ("SQL_OBTENER_CUENTA_HC");
        }
        return cuenta;
    }

    String obtenerCuentaGasto(String documento) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String cuenta = "";
        try {
            st = crearPreparedStatement ("SQL_OBTENER_CUENTA_GASTO");
            st.setString (1, documento);
            
            rs = st.executeQuery ();
            if (rs.next ()){
                cuenta = rs.getString(1);
            }
   
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL SQL_OBTENER_CUENTA_GASTO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL SQL_OBTENER_CUENTA_GASTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_OBTENER_CUENTA_GASTO");
        }
        return cuenta;
    }
    
}
