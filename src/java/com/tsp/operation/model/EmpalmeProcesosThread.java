/*
 * EmpalmeProcesosThread.java
 *
 * Created on 26 de junio de 2005, 12:41 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
//import com.tsp.operation.model.*;

/**
 *
 * @author  Sandrameg
 */
public class EmpalmeProcesosThread extends Thread {
    
    private AsignarYSincronizar ays;
    private ReqClienteThread rct;
    private ReqClienteThread rctf;
    private RecTSPThread rtsp;
    private String usuario;
    private String distrito; 
    private Model  model;
    
    public EmpalmeProcesosThread(String u, String d ) {
        usuario = u;
        distrito = d;
    }   
    
    public void star () {        
        super.start();
    }
    
    public void run() {// Este es el punto de entrada del hilo                
        try{
            model.LogProcesosSvc.InsertProceso("Empalme", this.hashCode(), "Asignacion y programacion Diaria OP", this.usuario);
        }catch (Exception ex ){
            ex.getMessage();
        }
        try {
            rct = new ReqClienteThread();
            rct.star(usuario, "", "", distrito, "act_pto");
            ////System.out.println("REALIZA HILO REQCLIENTE PTO");
            rct.join();
            ////System.out.println("SALE JOIN REQCLIENTE PTO");
            
            rctf = new ReqClienteThread();
            rctf.star(usuario, "", "", distrito, "act_fron");
            ////System.out.println("REALIZA HILO REQCLIENTE FRON");
            rctf.join();
            ////System.out.println("SALE JOIN REQCLIENTE FRON");
            
            rtsp = new RecTSPThread();
            rtsp.star(usuario, "", distrito, "act");
            ////System.out.println("REALIZA HILO RECTSP");
            rtsp.join();
            ////System.out.println("SALE JOIN HILO RECTSP");
            
            ////System.out.println("REALIZA AYS");
            AsignarYSincronizar ays = new AsignarYSincronizar("kreales");
            ////System.out.println("SALE AYS");
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso("Empalme", this.hashCode(),this.usuario,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso("Empalme",this.hashCode(),this.usuario,"ERROR :");
                }catch(Exception p){    }
            }
        }
        try{
            model.LogProcesosSvc.finallyProceso("Empalme", this.hashCode(), this.usuario, "PROCESO EXITOSO");
        }catch (Exception exe ){ exe.getMessage();}
        ////System.out.println("FIN HILO oooooooo");
    }
}

