/********************************************************************
 *      Nombre Clase.................   MigrarOTFitDefService.java
 *      Descripci�n..................   Servicio que genera el archivo de migraci�n Fitmen-Defitmen
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   21.07.2006
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  Tito Andr�s Maturana
 */
public class MigrarOTFitDefService {
    private MigrarOTFitDefDAO migrarOTFitDefDAO;
    private Vector lineas;
    
    /** Creates a new instance of MigrarOTFitDefService */
    public MigrarOTFitDefService() {
        migrarOTFitDefDAO = new MigrarOTFitDefDAO();
    }
    
    /**
     * Genera la informaci�n para el archivo de migraci�n Fitmen-Defitmens
     * @autor Ing. Tito Andr�s Maturana D.
     * @param cia C�digo Distrito
     * @throws SQLException si ocurre un error con la conexi�n con la Base de Datos
     * @returns Arreglo con los fitmens y defitmens
     * @version 1.0.
     **/
    public Vector migrarOTFitDef(String cia) throws SQLException{
        Vector lineas = new Vector();
        
        this.migrarOTFitDefDAO.migrarOTFitDef(cia);
        lineas = this.migrarOTFitDefDAO.getLineas();
        
        return lineas;
    }
    
}
