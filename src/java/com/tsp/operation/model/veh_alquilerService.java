/*
 * veh_alquilerService.java
 *
 * Created on 4 de septiembre de 2005, 11:07 AM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.*;

/**
 *
 * @author  Jm
 */
public class veh_alquilerService {
    private veh_alquilerDAO    VADataAccess;
    private List               ListaVA;
    private veh_alquiler       Datos;
    
    /** Creates a new instance of veh_alquilerService */
    public veh_alquilerService() {
        VADataAccess = new veh_alquilerDAO();
        ListaVA = new LinkedList();
        Datos = new veh_alquiler();
    }
    
    public void INSERT(String distrito, String placa, String std_job_no, String usuario) throws Exception {
        try{
            VADataAccess.INSERT_VA(distrito, placa, std_job_no, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT [veh_alquilerService]...\n"+e.getMessage());
        }
    }
    
    public void UPDATEESTADO(String reg_status, String placa, String std_job_no, String usuario) throws Exception {
        try{
            VADataAccess.UPDATE_VA(reg_status, placa, std_job_no, usuario);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEESTADO [veh_alquilerService]...\n"+e.getMessage());
        }
    }
    
    public void SEARCH(String placa, String std_job_no )throws Exception {
        try{
            this.ReiniciarDato();
            this.Datos = VADataAccess.SEARCH_VA(placa, std_job_no);
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH [veh_alquilerService]...\n"+e.getMessage());
        }
    }
    
    public boolean EXISTE(String placa, String std_job_no )throws Exception {
        try{
            return VADataAccess.BUSCAR_VA(placa, std_job_no);
        }
        catch(Exception e){
            throw new Exception("Error en EXISTE [veh_alquilerService]...\n"+e.getMessage());
        }
    }
    
    public void DELETE(String placa, String std_job_no ) throws Exception {
        try{
            VADataAccess.DELETE_VA(placa, std_job_no);
        }
        catch(Exception e){
            throw new Exception("Error en DELETE [veh_alquilerService]...\n"+e.getMessage());
        }
    }
    
    public void LIST() throws Exception {
        try{
            this.ReiniciarLista();
            this.ListaVA = VADataAccess.LIST();
        }
        catch(Exception e){
            throw new Exception("Error en LIST [veh_alquilerService]...\n"+e.getMessage());
        }
    }
    
    public void UPDATE(String placa, String std_job_no, String usuario, String nplaca, String nstd_job_no) throws Exception {
        try{
            VADataAccess.MODIFICAR_VA(placa, std_job_no, usuario, nplaca, nstd_job_no);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE [veh_alquilerService]...\n"+e.getMessage());
        }
    }
    
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    public void ReiniciarLista(){
        this.ListaVA = null;
    }
    
    public veh_alquiler getDato(){
        return this.Datos;
    }
    
    public List getList(){
        return this.ListaVA;
    }
    
    
}
