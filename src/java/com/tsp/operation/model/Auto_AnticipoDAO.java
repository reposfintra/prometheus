/*
 * Auto_AnticipoDAO.java
 *
 * Created on 18 de junio de 2005, 12:14 PM
 */

package com.tsp.operation.model;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  Sandrameg
 */
public class Auto_AnticipoDAO {
    
    private Auto_Anticipo autoant;
    
    /** Creates a new instance of Auto_AnticipoDAO */
    public Auto_AnticipoDAO() {
    }
    
    public Auto_Anticipo getAuto_Anticipo(){
        return this.autoant;
    }
    
    public void setAuto_Anticipo( Auto_Anticipo aa ){
        this.autoant = aa;
    }
    
    public void insert()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("insert into auto_anticipo (placa, efectivo, acpm, ticket_a, ticket_b, ticket_c, creation_user, user_update,base, sj) values (?,?,?,?,?,?,?,?,?,?)");
                st.setString(1, autoant.getPlaca());
                st.setDouble(2, autoant.getEfectivo());
                st.setDouble(3, autoant.getacpm());
                st.setInt(4, autoant.getTicket_a());
                st.setInt(5, autoant.getTicket_b());
                st.setInt(6, autoant.getTicket_c());
                st.setString(7, autoant.getCreation_user());
                st.setString(8, autoant.getUser_update());
                st.setString(9, autoant.getBase());
                st.setString(10, autoant.getSj());
                st.executeUpdate();
                ////System.out.println(st.toString());
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL AUTO ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void update()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update auto_anticipo set efectivo = ?, acpm = ?, ticket_a = ?, ticket_b = ?, ticket_c = ?, last_update='now()', user_update=? where placa= ? and sj =?");
                st.setDouble(1, autoant.getEfectivo());
                st.setDouble(2, autoant.getacpm());
                st.setInt(3, autoant.getTicket_a());
                st.setInt(4, autoant.getTicket_b());
                st.setInt(5, autoant.getTicket_c());
                st.setString(6, autoant.getUser_update());
                st.setString(7, autoant.getPlaca());
                st.setString(8, autoant.getSj());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACION DEL AUTO ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void nullify()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update auto_anticipo set reg_status='A', last_update='now()', user_update=? where placa= ?");
                st.setString(1, autoant.getUser_update());
                st.setString(2, autoant.getPlaca());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL AUTO ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void buscar(String placa)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        autoant = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            ////System.out.println(" placa " + placa);
            if(con!=null){
                st = con.prepareStatement("Select * from auto_anticipo where placa = ? and reg_status=''");
                st.setString(1,placa);
                rs = st.executeQuery();
                
                if(rs.next()){
                    autoant = new Auto_Anticipo();
                    autoant.setPlaca(rs.getString("placa"));
                    autoant.setEfectivo(rs.getDouble("efectivo"));
                    autoant.setacpm(rs.getDouble("acpm"));
                    autoant.setTicket_a(rs.getInt("ticket_a"));
                    autoant.setTicket_b(rs.getInt("ticket_b"));
                    autoant.setTicket_c(rs.getInt("ticket_c"));
                    autoant.setSj(rs.getString("sj"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DEL AUTO ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public void buscarPSj(String placa,String sj)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        autoant = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            ////System.out.println(" placa " + placa);
            if(con!=null){
                st = con.prepareStatement("Select * from auto_anticipo where placa = ? and reg_status='' and sj =?");
                st.setString(1,placa);
                st.setString(2,sj);
                rs = st.executeQuery();
                
                if(rs.next()){
                    autoant = new Auto_Anticipo();
                    autoant.setPlaca(rs.getString("placa"));
                    autoant.setEfectivo(rs.getDouble("efectivo"));
                    autoant.setacpm(rs.getDouble("acpm"));
                    autoant.setTicket_a(rs.getInt("ticket_a"));
                    autoant.setTicket_b(rs.getInt("ticket_b"));
                    autoant.setTicket_c(rs.getInt("ticket_c"));
                    autoant.setSj(rs.getString("sj"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DEL AUTO ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
}



