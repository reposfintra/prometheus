/*
 * Auto_AnticipoService.java
 *
 * Created on 18 de junio de 2005, 01:01 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  Sandrameg
 */
public class Auto_AnticipoService {
    
    private Auto_AnticipoDAO autoant;
    /** Creates a new instance of Auto_AnticipoService */
    public Auto_AnticipoService() {
        autoant = new Auto_AnticipoDAO();
    }
    
    public Auto_Anticipo getAuto_Anticipo(){
        return autoant.getAuto_Anticipo();
    }
    
    public void insert(Auto_Anticipo aa)throws SQLException{
        autoant.setAuto_Anticipo(aa);
        autoant.insert();        
    }
    
    public void update(Auto_Anticipo aa)throws SQLException{
        autoant.setAuto_Anticipo(aa);
        autoant.update();   
    }
    
    public void nullify(Auto_Anticipo aa)throws SQLException{
        autoant.setAuto_Anticipo(aa);
        autoant.nullify();   
    }
    
    public void buscar(String placa)throws SQLException{
        autoant.buscar(placa);
    }
    public void buscarPSj(String placa,String sj)throws SQLException{
        autoant.buscarPSj(placa, sj);
    }
}

