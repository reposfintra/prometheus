/***************************************************************************
 * Nombre clase : ............... HReportePredo.java                       *
 * Descripcion :................. Hilo que permite generar un reporte de   *
 *                                la planeacion en cada una de las agencias*
 * Autor :....................... Ing. Karen Reales                        *
 * Fecha :........................ 4 de Marzo de 2006, 11:38   AM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/

package com.tsp.operation.model;
import java.util.*;
import java.io.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import java.sql.SQLException;

public class HReportePredo extends Thread{
    private Model model;
    private Vector reporte;
    private String fec1;
    private String fec2;
    private String usuario;
    /** Creates a new instance of ReporteRegistroTiempoXLS */
    public HReportePredo() {
    }
    public void start(String fec1,String fec2, String usuario) {
        model=new Model();
        this.reporte= reporte;
        this.fec1 = fec1;
        this.fec2= fec2;
        this.usuario=usuario;
        try{
            model.LogProcesosSvc.InsertProceso("Generacion Reporte Planeacion", this.hashCode(), "Reporte Planeacion",usuario);
        }catch(Exception e){
            ////System.out.println("Error insertando el log");
        }
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            ////System.out.println("Empezamos...");
            /*
             *Creamos el archivo donde se va a guardar el reporte.
             */
            String nombreArch= "ReportePlaneacion["+fec1.replaceAll("-","")+"]["+fec2.replaceAll("-","")+"].xls";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta") + "/exportar/migracion";
            String Ruta  =path + "/"+usuario+"/"+nombreArch;
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( Ruta);
            
            HSSFCellStyle fecha   = xls.nuevoEstilo("Verdana", 8  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle hora   = xls.nuevoEstilo("Verdana", 8 , false   , false, "dd h:mm" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
            HSSFCellStyle texto   = xls.nuevoEstilo("Verdana", 8 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle texto3  = xls.nuevoEstilo("Verdana", 8  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle texto2  = xls.nuevoEstilo("Verdana", 8 ,  true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle total   = xls.nuevoEstilo("Verdana", 8 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
            HSSFCellStyle titulo2 = xls.nuevoEstilo("Verdana", 8  , true    , false, "text"       , HSSFColor.WHITE.index, HSSFColor.BLUE.index, HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero  = xls.nuevoEstilo("Verdana", 8  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle titulo  = xls.nuevoEstilo("Verdana", 8  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);
            
            HSSFCellStyle t_amarillo  = xls.nuevoEstilo("Verdana", 8  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.LIGHT_YELLOW.index, HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle t_lavanda  = xls.nuevoEstilo("Verdana", 8 , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.LAVENDER.index, HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle req  = xls.nuevoEstilo("Verdana", 8 , false    , false, "yyyy/mm/dd"       , HSSFColor.BLACK.index , HSSFColor.LIME.index, HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle rec  = xls.nuevoEstilo("Verdana", 8 , false    , false, "yyyy/mm/dd"       , HSSFColor.BLACK.index , HSSFColor.TAN.index, HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle req_bold  = xls.nuevoEstilo("Verdana", 10 , true    , true, "text"       , HSSFColor.BLACK.index , HSSFColor.LIME.index, HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle rec_bold  = xls.nuevoEstilo("Verdana",10, true    , true, "text"       , HSSFColor.BLACK.index , HSSFColor.TAN.index, HSSFCellStyle.ALIGN_LEFT);
            
            HSSFCellStyle asig  = xls.nuevoEstilo("Verdana", 8 , false    , false, "yyyy/mm/dd"       , HSSFColor.BLACK.index  , HSSFColor.TURQUOISE.index, HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle rojo  = xls.nuevoEstilo("Verdana", 8 , true    , false, "yyyy/mm/dd"       , HSSFColor.BLACK.index  , HSSFColor.RED.index, HSSFCellStyle.ALIGN_LEFT);
            
            /*
             *Se crea la primera hoja con los requerimientos y recursos no asignados
             */
            xls.obtenerHoja("NO ASIGNADOS");
            
            int fila = 1;
            int col  = 0;
            xls.combinarCeldas(fila, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO"             , texto  );
            xls.combinarCeldas(2, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "PLANEACION Y EJECUCION DIARIA DE LA OPERACION"        , texto  );
            xls.combinarCeldas(3, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "REQUERIMIENTOS SIN ASIGNAR Y RECURSOS DISPONIBLES"        , texto  );
            xls.combinarCeldas(4, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "FECHA INICIO: "+fec1+" FECHA FIN:"+fec2             , texto  );
            model.ipredoSvc.llenarInforme();
            reporte = model.ipredoSvc.getDatos();
            fila+=1;
            xls.combinarCeldas(fila, 0, fila, 5);
            xls.adicionarCelda(fila ,col   , "REQUERIMIENTOS"             , req_bold  );
            xls.combinarCeldas(fila,6, fila,11);
            xls.adicionarCelda(fila ,6   , "RECURSOS"             , rec_bold  );
            
            fila++;
            xls.cambiarAnchoColumna(0, 6000);
            xls.adicionarCelda(fila,0 , "Fecha planeacion" , titulo2 );
            xls.cambiarAnchoColumna(1, 10000);
            xls.adicionarCelda(fila,1 , "Cliente" , titulo2 );
            xls.cambiarAnchoColumna(2, 10000);
            xls.adicionarCelda(fila,2 , "Recurso", titulo2 );
            xls.cambiarAnchoColumna(3, 7000);
            xls.adicionarCelda(fila,3 , "Fecha de Disponibilidad", titulo2 );
            xls.cambiarAnchoColumna(4, 6000);
            xls.adicionarCelda(fila,4 , "Origen del Requerimiento", titulo2 );
            xls.cambiarAnchoColumna(5, 6000);
            xls.adicionarCelda(fila,5 , "Destino del Requerimiento", titulo2 );
            xls.adicionarCelda(fila,6 , "Placa", titulo2 );
            xls.cambiarAnchoColumna(7, 4500);
            xls.adicionarCelda(fila,7 , "Clase del Recurso", titulo2 );
            xls.cambiarAnchoColumna(8, 8000);
            xls.adicionarCelda(fila,8 , "Origen del Recurso", titulo2 );
            xls.cambiarAnchoColumna(9, 8000);
            xls.adicionarCelda(fila,9 , "Destino del Recurso", titulo2 );
            xls.cambiarAnchoColumna(10, 8500);
            xls.adicionarCelda(fila,10 , "Cliente del Recurso", titulo2 );
            xls.cambiarAnchoColumna(11, 5500);
            xls.adicionarCelda(fila,11 , "Fecha de Disponibilidad", titulo2 );
            
            for (int k  = 0; k<reporte.size();k++){
                
                ReportePlaneacion rep= (ReportePlaneacion) reporte.elementAt(k);
                
                fila++;
                HSSFCellStyle color_req=texto3;
                String fechaDispReq ="";
                if(rep.getCliente()!=null){
                    if( !rep.getCliente().equals("")){
                        color_req=req;
                        fechaDispReq=""+rep.getFechadispC();
                    }
                }
                HSSFCellStyle color_rec=texto3;
                String fechaDispRec ="";
                if(rep.getPlaca()!=null){
                    if( !rep.getPlaca().equals("")){
                        color_rec=rec;
                        fechaDispRec=""+rep.getFechadispR();
                        
                    }
                }
                HSSFCellStyle color_asig=fecha;
                if(rep.getFechaasig()!=null){
                    color_asig=asig;
                    
                }
                
                xls.adicionarCelda(fila,0 , rep.getFecha_dispxag() ,fecha);
                xls.adicionarCelda(fila,1 , rep.getCliente() , color_req);
                xls.adicionarCelda(fila,2 , rep.getRecurso_req(), color_req );
                xls.adicionarCelda(fila,3 , fechaDispReq,color_req);
                xls.adicionarCelda(fila,4 , rep.getOrigenC(), color_req );
                xls.adicionarCelda(fila,5 , rep.getDestinoC(),color_req);
                xls.adicionarCelda(fila,6 , rep.getPlaca(), color_rec );
                xls.adicionarCelda(fila,7 , rep.getClase(), color_rec );
                xls.adicionarCelda(fila,8 , rep.getOrigenR(),color_rec);
                xls.adicionarCelda(fila,9 , rep.getDestinoR(), color_rec);
                xls.adicionarCelda(fila,10 ,rep.getClienteR(), color_rec );
                xls.adicionarCelda(fila,11 ,fechaDispRec, color_rec);
               
                
            }
            /*
             *Obtenemos todas la agencias del distrito.
             */
            Vector ag = model.agenciaService.obtenerAgencias("FINV");
            
            /*
             *Reccorremos las agencias para buscar lo asignado y no asignado en cada una.
             */
            for(int a=0; a<ag.size();a++){
                
                Agencia agen = (Agencia) ag.elementAt(a);
                String codage = agen.getId_agencia();
                String nomage = agen.getNombre().toUpperCase();
                
                
                /*
                 * Buscamos lo planeado, asignado y ejecutado en la agencia.
                 */
                try{
                    model.ipredoSvc.llenarInforme(fec1, fec2, codage, "FECHAS");
                    
                }catch(SQLException e){
                    throw new SQLException( "ERROR BUSCANDO EL INFORME DE LA AGENCIA "+nomage+" : "+e.getMessage());
                    
                }
                reporte = model.ipredoSvc.getDatos();
                ////System.out.println("Tama�o del reporte "+reporte.size());
                if(reporte.size()>0){
                    xls.obtenerHoja(nomage);
                    fila = 1;
                    col  = 0;
                    xls.combinarCeldas(fila, col, fila, col+3);
                    xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO"             , texto  );
                    xls.combinarCeldas(2, col, fila, col+3);
                    xls.adicionarCelda(fila++ ,col   , "PLANEACION Y EJECUCION DIARIA DE LA OPERACION AGENCIA: "+  nomage           , texto  );
                    xls.combinarCeldas(3, col, fila, col+3);
                    xls.adicionarCelda(fila++ ,col   , "FECHA INICIO: "+fec1+" FECHA FIN:"+fec2             , texto  );
                    
                    fila+=2;
                    xls.combinarCeldas(fila, 0, fila, 12);
                    xls.adicionarCelda(fila ,col   , "PLANEADO"             , t_amarillo  );
                    xls.combinarCeldas(fila,13, fila,18);
                    xls.adicionarCelda(fila ,13   , "EJECUTADO"             , t_lavanda  );
                    
                    fila+=1;
                    xls.combinarCeldas(fila, 0, fila, 5);
                    xls.adicionarCelda(fila ,col   , "REQUERIMIENTOS"             , req_bold  );
                    xls.combinarCeldas(fila,6, fila,12);
                    xls.adicionarCelda(fila ,6   , "RECURSOS"             , rec_bold  );
                    xls.combinarCeldas(fila,13, fila,18);
                    xls.adicionarCelda(fila ,13   , ""             , t_lavanda  );
                    
                    fila++;
                    xls.cambiarAnchoColumna(0, 6000);
                    xls.adicionarCelda(fila,0 , "Fecha planeacion" , titulo2 );
                    xls.cambiarAnchoColumna(1, 10000);
                    xls.adicionarCelda(fila,1 , "Cliente" , titulo2 );
                    xls.cambiarAnchoColumna(2, 10000);
                    xls.adicionarCelda(fila,2 , "Recurso", titulo2 );
                    xls.cambiarAnchoColumna(3, 7000);
                    xls.adicionarCelda(fila,3 , "Fecha de Disponibilidad", titulo2 );
                    xls.cambiarAnchoColumna(4, 6000);
                    xls.adicionarCelda(fila,4 , "Origen del Requerimiento", titulo2 );
                    xls.cambiarAnchoColumna(5, 6000);
                    xls.adicionarCelda(fila,5 , "Destino del Requerimiento", titulo2 );
                    xls.adicionarCelda(fila,6 , "Placa", titulo2 );
                    xls.cambiarAnchoColumna(7, 4000);
                    xls.adicionarCelda(fila,7 , "Clase del Recurso", titulo2 );
                    xls.cambiarAnchoColumna(8, 8000);
                    xls.adicionarCelda(fila,8 , "Origen del Recurso", titulo2 );
                    xls.cambiarAnchoColumna(9, 8000);
                    xls.adicionarCelda(fila,9 , "Destino del Recurso", titulo2 );
                    xls.cambiarAnchoColumna(10, 8000);
                    xls.adicionarCelda(fila,10 , "Cliente del Recurso", titulo2 );
                    xls.cambiarAnchoColumna(11, 5000);
                    xls.adicionarCelda(fila,11 , "Fecha de Disponibilidad", titulo2 );
                    xls.cambiarAnchoColumna(12, 5100);
                    xls.adicionarCelda(fila,12 , "Fecha de Asignacion", titulo2 );
                    xls.cambiarAnchoColumna(13, 5000);
                    xls.adicionarCelda(fila,13 , "Fecha del Despacho", titulo2 );
                    xls.cambiarAnchoColumna(14, 5000);
                    xls.adicionarCelda(fila,14 , "Numero de planilla", titulo2 );
                    xls.cambiarAnchoColumna(15, 7000);
                    xls.adicionarCelda(fila,15 , "Cliente", titulo2 );
                    xls.cambiarAnchoColumna(16, 5000);
                    xls.adicionarCelda(fila,16 , "Standard Job", titulo2 );
                    xls.cambiarAnchoColumna(17, 5000);
                    xls.adicionarCelda(fila,17 , "Origen de la Planilla", titulo2 );
                    xls.cambiarAnchoColumna(18, 5000);
                    xls.adicionarCelda(fila,18 , "Destino de la Planilla", titulo2 );
                    // xls.cambiarAnchoColumna(19, 5000);
                    // xls.adicionarCelda(fila,19 , "Demora Disponibilidad Cliente - Asignacion", titulo2 );
                    // xls.cambiarAnchoColumna(20, 5000);
                    // xls.adicionarCelda(fila,20 , "Demora Disponibilidad Recurso - Asignacion", titulo2 );
                    // xls.cambiarAnchoColumna(21, 5000);
                    // xls.adicionarCelda(fila,21 , "Demora Asignacion - Ejecucion", titulo2 );
                    
                    
                    ////System.out.println("Tamano del reporte de la agencia : "+nomage+" : "+reporte.size());
                    
                    
                    double asignado=0;
                    double no_asignado=0;
                    double ejecutados=0;
                    double ejecutadoOk=0;
                    for (int k  = 0; k<reporte.size();k++){
                        
                        ReportePlaneacion rep= (ReportePlaneacion) reporte.elementAt(k);
                        
                        fila++;
                        HSSFCellStyle color_req=texto3;
                        String fechaDispReq ="";
                        if(rep.getCliente()!=null){
                            if( !rep.getCliente().equals("")){
                                color_req=req;
                                fechaDispReq=""+rep.getFechadispC();
                                if(rep.getFechaasig()!=null){
                                    color_req=asig;
                                }
                                if(rep.isAplazado()){
                                    color_req=rojo;
                                }
                            }
                        }
                        HSSFCellStyle color_rec=texto3;
                        String fechaDispRec ="";
                        if(rep.getPlaca()!=null){
                            if( !rep.getPlaca().equals("")){
                                color_rec=rec;
                                fechaDispRec=""+rep.getFechadispR();
                                if(rep.getFechaasig()!=null){
                                    color_rec=asig;
                                }
                                if(rep.isAplazado()){
                                    color_rec=rojo;
                                }
                            }
                        }
                        HSSFCellStyle color_asig=fecha;
                        if(rep.getFechaasig()!=null){
                            color_asig=asig;
                            if(rep.isAplazado()){
                                color_asig=rojo;
                            }
                        }
                        
                        xls.adicionarCelda(fila,0 , rep.getFecha_dispxag() ,fecha);
                        xls.adicionarCelda(fila,1 , rep.getCliente() , color_req);
                        xls.adicionarCelda(fila,2 , rep.getRecurso_req(), color_req );
                        xls.adicionarCelda(fila,3 , fechaDispReq,color_req);
                        xls.adicionarCelda(fila,4 , rep.getOrigenC(), color_req );
                        xls.adicionarCelda(fila,5 , rep.getDestinoC(),color_req);
                        xls.adicionarCelda(fila,6 , rep.getPlaca(), color_rec );
                        xls.adicionarCelda(fila,7 , rep.getClase(), color_rec );
                        xls.adicionarCelda(fila,8 , rep.getOrigenR(),color_rec);
                        xls.adicionarCelda(fila,9 , rep.getDestinoR(), color_rec);
                        xls.adicionarCelda(fila,10 ,rep.getClienteR(), color_rec );
                        xls.adicionarCelda(fila,11 ,fechaDispRec, color_rec);
                        xls.adicionarCelda(fila,12 ,rep.getFechaasig()!=null?""+rep.getFechaasig():"",color_asig);
                        xls.adicionarCelda(fila,13 ,rep.getFecdspejec()!=null?""+rep.getFecdspejec():"", fecha );
                        xls.adicionarCelda(fila,14 ,rep.getNumplaejec(), texto3 );
                        xls.adicionarCelda(fila,15 ,rep.getCliejec(), texto3 );
                        xls.adicionarCelda(fila,16 ,rep.getStdejec(), texto3 );
                        xls.adicionarCelda(fila,17 ,rep.getOriEjec(), texto3 );
                        xls.adicionarCelda(fila,18 ,rep.getDesEjec(), texto3 );
                        //      xls.adicionarCelda(fila,19 , "", texto3 );
                        //    xls.adicionarCelda(fila,20 , "", texto3 );
                        //  xls.adicionarCelda(fila,21 ,"", texto3 );
                    }
                }
            }
            
            xls.cerrarLibro();
            
            
            // end while de planillas
                /*try{
                 
                    //PLANEADO
                    Fila=Fila+2;
                    row  = sheet.createRow((short)(Fila));
                    cell = row.createCell((short)(0));
                    xls.setCellStyle(estilo3);
                    xls.setCellValue("Total Planeado");
                 
                    cell = row.createCell((short)(1));
                    xls.setCellStyle(estilo4);
                    xls.setCellValue(no_asignado);
                 
                    Fila++;
                    row  = sheet.createRow((short)(Fila));
                 
                    cell = row.createCell((short)(0));
                    xls.setCellStyle(estilo3);
                    xls.setCellValue("Total Asignado");
                 
                    cell = row.createCell((short)(1));
                    xls.setCellStyle(estilo4);
                    xls.setCellValue(asignado);
                 
                 
                 
                 
                    double pa_na = (asignado/(asignado+no_asignado))*100;
                 
                    double rend=0;
                    if(pa_na>0){
                        rend= Util.redondear(pa_na,2);
                    }
                    //double
                 
                    cell = row.createCell((short)(2));
                    xls.setCellStyle(estilo3);
                    xls.setCellValue("Porcentaje de Eficacia");
                 
                    cell = row.createCell((short)(3));
                    xls.setCellStyle(estilo4);
                    xls.setCellValue(rend+"%");
                 
                 
                    //EJECUTADO
                    Fila++;
                 
                    row  = sheet.createRow((short)(Fila));
                 
                    cell = row.createCell((short)(0));
                    xls.setCellStyle(estilo3);
                    xls.setCellValue("Total Ejecutados");
                 
                    cell = row.createCell((short)(1));
                    xls.setCellStyle(estilo4);
                    xls.setCellValue(ejecutados);
                 
                    double pa_efic = (asignado/(ejecutados))*100;
                 
                    rend=0;
                 
                    if(pa_efic>0){
                        rend= Util.redondear(pa_efic,2);
                    }
                 
                    cell = row.createCell((short)(2));
                    xls.setCellStyle(estilo3);
                    xls.setCellValue("Porcentaje de Eficiencia");
                 
                    cell = row.createCell((short)(3));
                    xls.setCellStyle(estilo4);
                    xls.setCellValue(rend+"%");
                 
                 
                    //EJECUTADO OK
                    Fila++;
                    row  = sheet.createRow((short)(Fila));
                 
                    cell = row.createCell((short)(0));
                    xls.setCellStyle(estilo3);
                    xls.setCellValue("Total Asignados y Ejecutados ");
                 
                    cell = row.createCell((short)(1));
                    xls.setCellStyle(estilo4);
                    xls.setCellValue(ejecutadoOk);
                 
                    cell = row.createCell((short)(2));
                    xls.setCellStyle(estilo3);
                    xls.setCellValue("Porcentaje de Planeacion");
                 
                    cell = row.createCell((short)(3));
                    xls.setCellStyle(estilo4);
                    xls.setCellValue("0%");
                 
                    double pa_pla = (asignado/(ejecutadoOk))*100;
                    rend=0;
                 
                    if(pa_pla>0){
                        rend= Util.redondear(pa_pla,2);
                    }
                    cell = row.createCell((short)(3));
                    xls.setCellStyle(estilo4);
                    xls.setCellValue(rend+"%");
                 
                    Fila+=2;
                 
                }catch(Exception e){
                    ////System.out.println("Error Al llenar datos de porcentajes " +e.getMessage());
                }*/
            
            model.LogProcesosSvc.finallyProceso("Generacion Reporte Planeacion", this.hashCode(),usuario,"PROCESO FINALIZADO CON EXITO." );
        }catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso("Generacion Reporte Planeacion", this.hashCode(),usuario,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                ////System.out.println("Hay un error: "+f.getMessage());
            }
            ////System.out.println("Hay un error: "+e.getMessage());
        }
    }
    
    public Calendar pasarACalendar(java.sql.Timestamp fecha){
        long milis = fecha.getTime();
        Calendar a = Calendar.getInstance();
        a.clear();
        a.setTimeInMillis(milis);
        return a;
    }
    public static void main(String a [])throws SQLException{
        HReportePredo hilo = new HReportePredo();
        hilo.start("2006-04-01","2006-04-30","KREALES");
    }
    
    
}
