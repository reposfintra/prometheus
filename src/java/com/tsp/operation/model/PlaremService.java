/*
 * PlaremService.java
 *
 * Created on 20 de junio de 2005, 10:08 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Jm
 */
public class PlaremService {
    PlaremDAO plrDAO;
    /** Creates a new instance of PlaremService */
    public PlaremService() {
        plrDAO = new PlaremDAO();
    }
    public java.util.Vector getPlrem() {
        return plrDAO.getPlr();
    }
    
    /**
     * Setter for property plr.
     * @param plr New value of property plr.
     */
    public void setPlrem(java.util.Vector plr) {
        plrDAO.setPlr(plr);
    }
    
    /**
     * Getter for property plrem.
     * @return Value of property plrem.
     */
    public Plarem getPlr() {
        return plrDAO.getPlrem();
    }
    
    /**
     * Setter for property plrem.
     * @param plrem New value of property plrem.
     */
    public void setPlr(Plarem plrem) {
        plrDAO.setPlrem(plrem);
    }
    public void list( String numpla )throws SQLException{
        plrDAO.list(numpla);
    }
    
    public Plarem obtenerDatosPlarem(String numpla, String numrem)throws SQLException{
        return plrDAO.obtenerDatosPlarem(numpla, numrem);
    }
    
    public String account_code_c( String numpla )throws SQLException{
        return plrDAO.account_code_c( numpla );
    }
}

