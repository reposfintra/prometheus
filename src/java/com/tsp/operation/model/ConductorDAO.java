/***********************************************************************************
 * Nombre clase :  ConductorDAO                   
 * Descripcion :   Clase que maneja los DAO ( Data Access Object )  
 *                 los cuales contienen los metodos que interactuan 
 *                 con la BD.                                       
 * Autor :                         Ing. Fernell Villacob          
 * Fecha :                          14 de octubre de 2005, 09:30 AM                
 * Version :  1.0          
 * Copyright : Fintravalores S.A.                         
 ***********************************************************************************/
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;




public class ConductorDAO {
    
    //____________________________________________________________________________________________
    //                                          ATRIBUTOS
    //____________________________________________________________________________________________
    
    
    
    // SELECT
    
    private Vector conductores1;
    private Conductor conductor1;
    
    private static final String SEARCH_CONDUCTOR="SELECT                "+
    "     estado,          "+
    "     cedula,          "+
    "     nopase,          "+
    "     categoriapase ,  "+
    "     vigenciapase ,   "+
    "     nopasaporte,     "+
    "     pasaportevence,  "+
    "     gruposangre ,    "+
    "     lugarnacido ,    "+
    "     fechanacido ,    "+
    "     senalesparti,    "+
    "     totalviajes ,    "+
    "     fechaingreso ,   "+
    "     nomreferencia ,  "+
    "     dirreferencia ,  "+
    "     telreferencia ,  "+
    "     ciureferencia ,  "+
    "     nomultpatron ,   "+
    "     dirultpatron ,   "+
    "     telultpatron ,   "+
    "     ciuultpatron ,   "+
    "     docadjunto       "+
    " FROM  CONDUCTOR  WHERE CEDULA = ? ";
    
    
    private static final String SEARCH_NIT="SELECT *,nombre1||','||nombre2||','||apellido1||','||apellido2 as nomcompleto FROM  NIT  WHERE CEDULA = ?  ";
    
    private static final String QUERY_CIUDAD="SELECT codciu ,nomciu,coddpt FROM CIUDAD  order by nomciu";
    
    private static final String QUERY_DEPARTAMENTO="SELECT department_code as departament_code,department_name as departament_name, country_code as country FROM estado  order by department_name";
    
    private static final String QUERY_PAIS="SELECT country_code,country_name ,country FROM PAIS order by country_name";
    
    
    
    // DELETE  (En realidad es una modificación del campo reg_status)
    
    private static final String DELETE_CONDUCTOR="UPDATE CONDUCTOR SET ESTADO='A' WHERE CEDULA = ? ";
    
    private static final String DELETE_NIT="UPDATE  NIT SET ESTADO='A'  WHERE CEDULA = ? ";
    
    
    // INSERT
    
    private static final String INSERT_CONDUCTOR="INSERT INTO CONDUCTOR   "+
    "( estado              , "+
    "  cedula              , "+
    "  nopase              , "+
    "  categoriapase       , "+
    "  vigenciapase        , "+
    "  nopasaporte         , "+
    "  pasaportevence      , "+
    "  gruposangre         , "+
    "  lugarnacido         , "+
    "  fechanacido         , "+
    "  senalesparti        , "+
    "  nomreferencia       , "+
    "  dirreferencia       , "+
    "  telreferencia       , "+
    "  ciureferencia       , "+
    "  nomultpatron        , "+
    "  dirultpatron        , "+
    "  telultpatron        , "+
    "  ciuultpatron        , "+
    "  docadjunto          , "+
    "  usuariocrea         , "+
    "  fechacreado          "+
    ") "+
    "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now()) ";
    
    private static final String INSERT_NIT="INSERT INTO NIT      "+
    "( estado           , "+
    "  cedula           , "+
    "  id_mims          , "+
    "  nombre           , "+
    "  direccion        , "+
    "  codciu           , "+
    "  coddpto          , "+
    "  codpais          , "+
    "  telefono         , "+
    "  celular          , "+
    "  e_mail           , "+
    "  sexo             , "+
    "  fechanac         , "+
    "  usuariocrea      , "+
    "  fechacrea          "+
    ") "+
    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
    
    
    
    //  UPDATE
    
    private static final String UPDATE_CONDUCTOR=" UPDATE CONDUCTOR  SET       "+
    "  estado           = ?     , "+
    "  cedula           = ?     , "+
    "  nopase           = ?     , "+
    "  categoriapase    = ?     , "+
    "  vigenciapase     = ?     , "+
    "  nopasaporte      = ?     , "+
    "  pasaportevence   = ?     , "+
    "  gruposangre      = ?     , "+
    "  lugarnacido      = ?     , "+
    "  fechanacido      = ?     , "+
    "  senalesparti     = ?     , "+
    "  nomreferencia    = ?     , "+
    "  dirreferencia    = ?     , "+
    "  telreferencia    = ?     , "+
    "  ciureferencia    = ?     , "+
    "  nomultpatron     = ?     , "+
    "  dirultpatron     = ?     , "+
    "  telultpatron     = ?     , "+
    "  ciuultpatron     = ?     , "+
    "  docadjunto       = ?     , "+
    "  usuario          = ?       "+
    "WHERE cedula       = ?      ";
    
    
    private static final String UPDATE_NIT="UPDATE NIT  SET           "+
    "  estado         = ?    , "+
    "  cedula         = ?    , "+
    "  id_mims        = ?    , "+
    "  nombre         = ?    , "+
    "  direccion      = ?    , "+
    "  codciu         = ?    , "+
    "  coddpto        = ?    , "+
    "  codpais        = ?    , "+
    "  telefono       = ?    , "+
    "  celular        = ?    , "+
    "  e_mail         = ?    , "+
    "  sexo           = ?    , "+
    "  fechanac       = ?    , "+
    "  usuario        = ?      "+
    "WHERE  cedula    = ?      ";
    
    /********MOdificacion Diogenes 121005****/
    private static final String insertarconductor="INSERT INTO CONDUCTOR   "+
    "( estado              , "+
    "  cedula              , "+
    "  nopase              , "+
    "  categoriapase       , "+
    "  vigenciapase        , "+
    "  nopasaporte         , "+
    "  pasaportevence      , "+
    "  gruposangre         , "+   
    "  docadjunto          , "+
    "  res_pase            , "+
    "  nrojudicial         ," +
    "  vencejudicial       ," +
    "  nomeps              ," +
    "  nroeps              ," +
    "  fecafieps           ," +
    "  nomarp              ," +
    "  fecafiarp           ," +
    "  nrovisa             ," +
    "  vencevisa           ," +
    "  nrolibtripulante    ," +
    "  vencelibtripulante  ," +
    "  pension             ," +
    "  fecafipension       ," +
    "  usuario             ,"+
    "  usuariocrea         , "+
    "  fechacreado         ," +
    "   fechaingreso         "+
    ") "+
    "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),now()) ";
    
    private static final String modificarconductor=" UPDATE CONDUCTOR  SET       "+
    "  nopase      = ?     , "+
    "  categoriapase  = ?   , "+
    "  vigenciapase  = ?      , "+
    "  nopasaporte   = ?      , "+
    "  pasaportevence  = ?    , "+
    "  gruposangre      = ?   , "+
    "  docadjunto      = ?    , "+
    "  res_pase        = ?    , " +
    "  nrojudicial      = ?   ," +
    "  vencejudicial    = ?   ," +
    "  nomeps           = ?   ," +
    "  nroeps           = ?   ," +
    "  fecafieps        = ?   ," +
    "  nomarp           = ?   ," +
    "  fecafiarp        = ?   ," +
    "  nrovisa           = ?  ," +
    "  vencevisa       = ?    ," +
    "  nrolibtripulante  = ?  ," +
    "  vencelibtripulante = ? ," +
    //"  griesgo          = ?   ," +
    "  pension          = ?   ," +
    "  fecafipension    = ?   ," +
    "  usuario          = ?   ,"+
    "  fechaultact   = now()   "+
    "  WHERE cedula       = ?   ";
    private static final String buscarconductor="  SELECT               "+
    "  estado              , "+
    "  cedula              , "+
    "  nopase              , "+
    "  categoriapase       , "+
    "  vigenciapase        , "+
    "  nopasaporte         , "+
    "  pasaportevence      , "+
    "  gruposangre         , "+
    "  senalesparti        , "+
    "  totalviajes         , "+
    "  fechaingreso        , "+
    "  docadjunto          , "+
    "  res_pase            , " +
    "  nrojudicial         ," +
    "  vencejudicial       ," +
    "  nomeps              ," +
    "  nroeps              ," +
    "  fecafieps           ," +
    "  nomarp              ," +
    "  fecafiarp           ," +
    "  nrovisa             ," +
    "  vencevisa           ," +
    "  nrolibtripulante    ," +
    "  vencelibtripulante  ," +
    "  griesgo ,            " +
    "  pension ,            " +
    "  fecafipension,      " +
    "  huella_der,         " +
    "  huella_izq,         " +
    "  COALESCE ( get_destablagen(res_pase,'RESPASE' ),'')  AS desrespase          " +
    " FROM  CONDUCTOR  WHERE CEDULA = ? ";
   
    
    private static final String existe = "Select * from conductor where cedula = ? and estado = 'A'";
    private static final String SLQ_BUSCAFOTO="select document from doc.tblimagen where dstrct='FINV' and  activity_type='003' and document_type='032' and document=?";
    //David 28.11.05
    private static final String EXISTE_CONDUCTOR = "Select cedula from conductor where cedula = ? ";
    private static final String SQL_CONDUCTORES_POR_NOMBRE="select c.cedula,n.nombre from conductor as c,nit as n where c.cedula = n.cedula  and (c.cedula like ? or n.nombre like ? )order by n.nombre";
    
    private static final String SQL_BUSCA_CONDUCTOR="SELECT cedula, get_nombrenit(cedula) AS nombre, estado FROM conductor WHERE cedula = ? ";
    private static final String SQL_BUSCA_NIT="SELECT cedula,  nombre, estado FROM nit WHERE cedula = ? ";
    private static final String SQL_CAMBIAR_ESTADO = " UPDATE conductor " +
                                                     " SET estado = ?  " +
                                                     " WHERE cedula = ?; #VETO# ";
     private static final String SQL_CAMBIAR_ESTADO_NIT = " UPDATE nit " +
                                                     " SET estado = ?,  " +
                                                     "     veto = ?     "+
                                                     " WHERE cedula = ? ";
    
    //andres 2006-04-03
    private static final String SQL_CONDUC_POR_NOMBRE="select a.cedula,b.nombre from conductor as a LEFT OUTER JOIN nit b ON ( b.cedula = a.cedula ) where a.cedula = ?";
    
    //____________________________________________________________________________________________
    //                                          METODOS
    //____________________________________________________________________________________________
    
    private Conductor conductor;
    private Vector conductores;
    private Nit prop;
    private Vector propietarios;
    
    public ConductorDAO() {}
    
    
    // _________________________________________________________________________________________
    //   SELECT
    
    
    
    
    //--- busqueda especifica de conductor
    
    Conductor buscarConductor(String id)throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        Conductor conductor=null;
        try{
            st= conPostgres.prepareStatement(SEARCH_CONDUCTOR);
            st.setString(1, id);
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next()){
                    conductor = new Conductor();
                    conductor.setEstado(rs.getString(1));
                    conductor.setCedula(rs.getString(2));
                    // conductor.setSupplierNo(rs.getString(3));
                    conductor.setPassNo(rs.getString(3));
                    conductor.setPassCat(rs.getString(4));
                    conductor.setPassExpiryDate(rs.getString(5));
                    conductor.setPassport(rs.getString(6));
                    conductor.setPassportExpiryDate(rs.getString(7));
                    conductor.setRh(rs.getString(8));
                    conductor.setBirthPlace(rs.getString(9));
                    conductor.setDateBirth(rs.getString(10));
                    conductor.setParticularSign(rs.getString(11));
                    conductor.setTripTotal(rs.getString(12));
                    conductor.setInitDate(rs.getString(13));
                    conductor.setRefName(rs.getString(14));
                    conductor.setAddressRef(rs.getString(15));
                    conductor.setPhoneRef(rs.getString(16));
                    conductor.setCityRef(rs.getString(17));
                    conductor.setNameLastBoss(rs.getString(18));
                    conductor.setAddressBoss(rs.getString(19));
                    conductor.setPhoneBoss(rs.getString(20));
                    conductor.setCityBoss(rs.getString(21));
                    conductor.setDocument(rs.getString(22));
                    break;
                }
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda especifica del conductor..." + id + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return conductor;
    }
    
    
    
    
    // busqueda especifica del nit
    
    Nit buscarNit(String id) throws Exception{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        Nit nit=null;
        try{
            st= conPostgres.prepareStatement(SEARCH_NIT);
            st.setString(1, id);
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next()){
                    nit = new Nit();
                    nit.setEstado(rs.getString(1));
                    nit.setNit( rs.getString(2) );
                    nit.setIdMims( rs.getString(3) );
                    nit.setName( rs.getString("nomcompleto") );
                    nit.setPnombre(rs.getString("nombre"));
                    nit.setAddress( rs.getString(5) );
                    nit.setAgCode( rs.getString(6) );
                    nit.setDptCode( rs.getString(7) );
                    nit.setCountryCode( rs.getString(8) );
                    nit.setPhone( rs.getString(9) );
                    nit.setCellular( rs.getString(10) );
                    nit.setEMail( rs.getString(11) );
                    nit.setSex( rs.getString(16) );
                    nit.setFechaNacido(rs.getString(17) );
                    break;
                }
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda especifica del nit..." + id + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return nit;
        
    }
    
    
    
    
    List ciudades() throws Exception{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        List ciudades = null;
        
        try{
            st= conPostgres.prepareStatement(QUERY_CIUDAD);
            rs=st.executeQuery();
            if(rs!=null){
                ciudades = new LinkedList();
                while(rs.next()){
                    CodeValue  obj = new CodeValue();
                    obj.setCodigo(rs.getString(1));
                    obj.setDescripcion(rs.getString(2));
                    obj.setReferencia(rs.getString(3));
                    ciudades.add(obj);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de la ciudades -->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return ciudades;
        
    }
    
    
    
    
    List departamentos() throws Exception{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        List departamentos=null;
        try{
            st= conPostgres.prepareStatement(QUERY_DEPARTAMENTO);
            rs=st.executeQuery();
            if(rs!=null){
                departamentos = new LinkedList();
                while(rs.next()){
                    CodeValue dpto = new CodeValue();
                    dpto.setCodigo(rs.getString(1));
                    dpto.setDescripcion(rs.getString(2));
                    dpto.setReferencia(rs.getString(3));
                    departamentos.add(dpto);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de los departamentos...  ---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return departamentos;
        
    }
    
    
    
    
    List paises() throws Exception{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        List paises=null;
        try{
            st= conPostgres.prepareStatement(QUERY_PAIS);
            rs=st.executeQuery();
            if(rs!=null){
                paises = new LinkedList();
                while(rs.next()){
                    CodeValue pais = new CodeValue();
                    pais.setCodigo(rs.getString(1));
                    pais.setDescripcion(rs.getString(2));
                    pais.setReferencia(rs.getString(3));
                    paises.add(pais);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de los departamentos... ---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return paises;
        
    }
    
    
    
    
    //_________________________________________________________________________________
    //  DELETE
    
    
    void eliminar(String id)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        PreparedStatement st2=null;
        try{
            if(!id.equals("")) {
                st= conPostgres.prepareStatement(DELETE_CONDUCTOR);
                st.setString(1, id);
                st.executeUpdate();
                
                st2= conPostgres.prepareStatement(DELETE_NIT);
                st2.setString(1, id);
                st2.executeUpdate();
            }
        }catch(Exception e){
            throw new SQLException("No se pudo eliminar el conductor "+ id + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(st2!=null) st2.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
      
    
   
    
    
    
    
    void insertNit(Nit nit, String usuario)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            st= conPostgres.prepareStatement(INSERT_NIT);
            st.setString(1, nit.getEstado() );
            st.setString(2, nit.getNit().toUpperCase());
            st.setString(3, nit.getIdMims());
            st.setString(4, nit.getName());
            st.setString(5, nit.getAddress());
            st.setString(6, nit.getAgCode());
            st.setString(7, nit.getDptCode());
            st.setString(8, nit.getCountryCode());
            st.setString(9, nit.getPhone());
            st.setString(10,nit.getCellular());
            st.setString(11, nit.getEMail());
            st.setString(12, nit.getSex());
            st.setString(13, nit.getFechaNacido());
            st.setString(14, usuario);
            st.executeUpdate();
        }catch(Exception e){
            throw new SQLException("No se pudo insertar el conductor  "+ nit.getNit() + " en nit  " + " -->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
        
    
    
    void updateNit(Nit nit, String usuario)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            if(!nit.getNit().equals("") ) {
                st= conPostgres.prepareStatement(UPDATE_NIT);
                st.setString(1, nit.getEstado());
                st.setString(2, nit.getNit());
                st.setString(3, nit.getIdMims());
                st.setString(4, nit.getName());
                st.setString(5, nit.getAddress());
                st.setString(6, nit.getAgCode());
                st.setString(7, nit.getDptCode());
                st.setString(8, nit.getCountryCode());
                st.setString(9, nit.getPhone());
                st.setString(10, nit.getCellular());
                st.setString(11, nit.getEMail());
                st.setString(12, nit.getSex());
                st.setString(13, nit.getFechaNacido() );
                st.setString(14, usuario);
                st.setString(15, nit.getNit());
                st.executeUpdate();
            }
        }catch(Exception e){
            throw new SQLException("No se pudo modificar el conductor "+ nit.getNit() +" en nit" + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    public Conductor getConductor(){
        return this.conductor;
    }
    public Vector getConductores(){
        return this.conductores;
    }
    
    boolean nitExist(String cedula) throws SQLException{
        
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                psttm = con.prepareStatement("SELECT n.cedula FROM nit n where  n.cedula=?");
                psttm.setString(1, cedula);
                rs = psttm.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLACA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    //Modificacdo por jose 28/09/2006
    public void consultaConductor(String nit)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        conductor = null;
        conductores = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select DISTINCT n.cedula, n.nombre from nit n where  upper(n.nombre) like upper(?) OR upper(n.cedula) LIKE upper(?)");
                st.setString(1,nit+"%");
                st.setString(2,nit+"%");
                rs = st.executeQuery();
                conductores = new Vector();
                while(rs.next()){
                    conductor = new Conductor();
                    conductor.setNombre(rs.getString("nombre"));
                    conductor.setCedula(rs.getString("cedula"));
                    conductores.add(conductor);
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NIT" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    public boolean nitVetado(String cedula) throws SQLException{
        
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                psttm = con.prepareStatement("select veto from nit where cedula = ? and veto ='V' ");
                psttm.setString(1, cedula);
                rs = psttm.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA VERIFICACION DEL VETO DE UN CONDUCTOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    /*******Modificaciones DIOGENES 121005***********/
    /*******Modificaciones DIOGENES 121005***********/
    public void buscar_Conductor(String id)throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        conductor = new Conductor();
        try{
            st= conPostgres.prepareStatement(buscarconductor);
            st.setString(1, id);
            ////System.out.println(st);
            rs=st.executeQuery();
            if(rs.next()){
                conductor.setEstado(rs.getString("estado"));
                conductor.setCedula(rs.getString("cedula"));
                conductor.setPassNo(rs.getString("nopase"));
                conductor.setPassCat(rs.getString("categoriapase"));
                conductor.setPassExpiryDate(rs.getString("vigenciapase")); ////System.out.println("Vigencia Pase "+rs.getString("vigenciapase"));
                conductor.setPassport(rs.getString("nopasaporte"));
                conductor.setPassportExpiryDate(rs.getString("pasaportevence")); ////System.out.println("Vence Pasaporte "+rs.getString("pasaportevence"));
                conductor.setRh(rs.getString("gruposangre"));
                conductor.setParticularSign(rs.getString("senalesparti"));
                conductor.setTripTotal(rs.getString("totalviajes"));
                conductor.setInitDate(rs.getString("fechaingreso"));
                conductor.setDocument(rs.getString("docadjunto"));
                conductor.setRes_pase(rs.getString("res_pase"));
                conductor.setNrojudicial(rs.getString("nrojudicial"));
                conductor.setVencejudicial(rs.getString("vencejudicial"));
                conductor.setNomeps(rs.getString("nomeps"));
                conductor.setNroeps( rs.getString("nroeps"));
                conductor.setFecafieps( rs.getString("fecafieps"));
                conductor.setNomarp( rs.getString("nomarp"));
                conductor.setFecafiarp(rs.getString("fecafiarp"));
                conductor.setNrovisa(rs.getString("nrovisa"));
                conductor.setVencevisa(rs.getString("vencevisa"));
                conductor.setNrolibtripulante(rs.getString("nrolibtripulante"));
                conductor.setVencelibtripulante(rs.getString("vencelibtripulante"));
                conductor.setGradoriesgo(rs.getString("griesgo"));
            }
            
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda especifica del conductor..." + id + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    public void insertConductor(Conductor conductor)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            
            st= conPostgres.prepareStatement(insertarconductor);
            st.setString(1,  conductor.getEstado());
            st.setString(2,  conductor.getCedula());
            st.setString(3,  conductor.getPassNo());
            st.setString(4, conductor.getPassCat());
            st.setString(5, conductor.getPassExpiryDate());
            st.setString(6, conductor.getPassport());
            st.setString(7, conductor.getPassportExpiryDate());
            st.setString(8, conductor.getRh());
            st.setInt(9, Integer.parseInt(conductor.getTripTotal()));
            st.setString(10, conductor.getInitDate());
            st.setString(11, conductor.getDocument());
            st.setString(12, conductor.getRes_pase());
            st.setString(13, conductor.getNrojudicial());
            st.setString(14, conductor.getVencejudicial());
            st.setString(15, conductor.getNomeps());
            st.setString(16, conductor.getNroeps());
            st.setString(17, conductor.getFecafieps());
            st.setString(18, conductor.getNomarp());
            st.setString(19, conductor.getFecafiarp());
            st.setString(20, conductor.getNrovisa());
            st.setString(21, conductor.getVencevisa());
            st.setString(22, conductor.getNrolibtripulante());
            st.setString(23, conductor.getVencelibtripulante());
            st.setString(24, conductor.getGradoriesgo());
            st.setString(25, conductor.getUsuariocrea());
            st.setString(26, conductor.getUsuariocrea());
            //            ////System.out.println("Insertar "+ st);
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("No se pudo insertar el conductor "+ conductor.getCedula()+ "-->" + e.getMessage() );
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    public void updateConductor(Conductor conductor)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            if(!conductor.getCedula().equals("") ) {
                st= conPostgres.prepareStatement(modificarconductor);
                st.setString(1,  conductor.getEstado());
                st.setString(2,  conductor.getPassNo());
                st.setString(3, conductor.getPassCat());
                st.setString(4, conductor.getPassExpiryDate());
                st.setString(5, conductor.getPassport());
                st.setString(6, conductor.getPassportExpiryDate());
                st.setString(7, conductor.getRh());
                st.setInt(8, Integer.parseInt(conductor.getTripTotal()));
                st.setString(9, conductor.getInitDate());
                st.setString(10, conductor.getDocument());
                st.setString(11, conductor.getRes_pase());
                st.setString(12, conductor.getNrojudicial());
                st.setString(13, conductor.getVencejudicial());
                st.setString(14, conductor.getNomeps());
                st.setString(15, conductor.getNroeps());
                st.setString(16, conductor.getFecafieps());
                st.setString(17, conductor.getNomarp());
                st.setString(18, conductor.getFecafiarp());
                st.setString(19, conductor.getNrovisa());
                st.setString(20, conductor.getVencevisa());
                st.setString(21, conductor.getNrolibtripulante());
                st.setString(22, conductor.getVencelibtripulante());
                st.setString(23, conductor.getGradoriesgo());
                st.setString(24, conductor.getUsuario());
                st.setString(25, conductor.getCedula() );
                ////System.out.println("Modificar"+st);
                st.executeUpdate();
            }
        }catch(Exception e){
            throw new SQLException("No se pudo modificar el conductor "+ conductor.getCedula() + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    public void reset(){
        conductor = null;
    }
    public boolean existerConductorAnul(String id)throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        PreparedStatement st=null;
        ResultSet rs=null;
        boolean est = false;
        try{
            st= conPostgres.prepareStatement(existe);
            st.setString(1, id);
            
            rs=st.executeQuery();
            if(rs.next()){
                est=true;
            }
            
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda especifica del conductor..." + id + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return est;
    }
    
    /**
     * Metodo listaDespacho  , Metodo que setea un vector de objetos tipo Conductor dado el nombre o cedula de conductor
     * @param : String id;
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    
    public void  obtConductoresPorNombre(String id) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.conductores = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                
                st = con.prepareStatement(this.SQL_CONDUCTORES_POR_NOMBRE);
                // select cedula,get_nombrenit(cedula) as nombre from conductor where cedula like 'A' order by nombre
                st.setString(1, id+"%");
                st.setString(2, id+"%");
                rs = st.executeQuery();
                
                while (rs.next()) {
                    Vector vConductor= new Vector();
                    vConductor.add(rs.getString("cedula"));
                    vConductor.add(rs.getString("nombre"));
                    conductores.add(vConductor);
                }
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Metodo existerConductor  , Metodo que retorna true si existe el conductor dada la cedula
     * @param : String id;
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    
    public boolean existeConductor(String id) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.EXISTE_CONDUCTOR);
                st.setString(1, id);
                
                ////System.out.println("Query: "+st.toString());
                rs = st.executeQuery();
                while(rs.next()) {
                    sw=true;
                }
                ////System.out.println("Existe en conductor switch "+sw);
            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL INSERTAR EL ESTADO" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    /*******Modificaciones DIOGENES 121005***********/
    
    /**
     * Metodo buscar_ConductorCGA, busca un conductor sin tener en cuenta su estado
     * param: ducumento del conductor
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscar_ConductorCGA(String id)throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        conductor = null;
        try{
            st= conPostgres.prepareStatement(buscarconductor);
            st.setString(1, id);
            ////System.out.println(st);
            rs=st.executeQuery();
            if(rs.next()){
                conductor = new Conductor();
                conductor.setEstado(rs.getString("estado"));
                conductor.setCedula(rs.getString("cedula"));
                conductor.setPassNo(rs.getString("nopase"));
                conductor.setPassCat(rs.getString("categoriapase"));
                conductor.setPassExpiryDate(rs.getString("vigenciapase")); 
                conductor.setPassport(rs.getString("nopasaporte"));
                conductor.setPassportExpiryDate(rs.getString("pasaportevence"));
                conductor.setRh(rs.getString("gruposangre"));
                conductor.setParticularSign(rs.getString("senalesparti"));
                conductor.setTripTotal(rs.getString("totalviajes"));
                conductor.setInitDate(rs.getString("fechaingreso"));
                conductor.setDocument(rs.getString("docadjunto"));
                conductor.setRes_pase(rs.getString("res_pase"));
                conductor.setNrojudicial(rs.getString("nrojudicial"));
                conductor.setVencejudicial(rs.getString("vencejudicial"));
                conductor.setNomeps(rs.getString("nomeps"));
                conductor.setNroeps( rs.getString("nroeps"));
                conductor.setFecafieps( rs.getString("fecafieps"));
                conductor.setNomarp( rs.getString("nomarp"));
                conductor.setFecafiarp(rs.getString("fecafiarp"));
                conductor.setNrovisa(rs.getString("nrovisa"));
                conductor.setVencevisa(rs.getString("vencevisa"));
                conductor.setNrolibtripulante(rs.getString("nrolibtripulante"));
                conductor.setVencelibtripulante(rs.getString("vencelibtripulante"));
                conductor.setGradoriesgo(rs.getString("griesgo"));
                conductor.setPension(rs.getString("pension"));
                conductor.setFecafipension(rs.getString("fecafipension"));
                conductor.setHuella_der(rs.getString("huella_der"));
                conductor.setHuella_izq(rs.getString("huella_izq"));
                conductor.setDesrespase(rs.getString("desrespase"));
            }
            
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda especifica del conductor..." + id + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /**
     * Metodo insertConductorCGA, inserta un registro a la tabla conductor,
     * param: objeto de tipo conductor
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void insertConductorCGA(Conductor conductor)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            
            st= conPostgres.prepareStatement(insertarconductor);
            st.setString(1,  conductor.getEstado());
            st.setString(2,  conductor.getCedula());
            st.setString(3,  conductor.getPassNo());
            st.setString(4, conductor.getPassCat());
            st.setString(5, conductor.getPassExpiryDate());
            st.setString(6, conductor.getPassport());
            st.setString(7, conductor.getPassportExpiryDate());
            st.setString(8, conductor.getRh());
            //st.setInt(9, Integer.parseInt(conductor.getTripTotal()));
            //st.setString(10, conductor.getInitDate());
            st.setString(9, conductor.getDocument());
            st.setString(10, conductor.getRes_pase());
            st.setString(11, conductor.getNrojudicial());
            st.setString(12, conductor.getVencejudicial());
            st.setString(13, conductor.getNomeps());
            st.setString(14, conductor.getNroeps());
            st.setString(15, conductor.getFecafieps());
            st.setString(16, conductor.getNomarp());
            st.setString(17, conductor.getFecafiarp());
            st.setString(18, conductor.getNrovisa());
            st.setString(19, conductor.getVencevisa());
            st.setString(20, conductor.getNrolibtripulante());
            st.setString(21, conductor.getVencelibtripulante());
            //st.setInt(22, Integer.parseInt(conductor.getGradoriesgo()));
            st.setString(22, conductor.getPension());
            st.setString(23, conductor.getFecafipension());
            st.setString(24, conductor.getUsuariocrea());
            st.setString(25, conductor.getUsuariocrea());
            ////System.out.println("Insertar "+ st);
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("No se pudo insertar el conductor "+ conductor.getCedula()+ "-->" + e.getMessage() );
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    /**
     * Metodo updateConductorCGA, modifica un registro a la tabla conductor,
     * param: objeto tipo conductor
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void updateConductorCGA(Conductor conductor)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            if(!conductor.getCedula().equals("") ) {
                st= conPostgres.prepareStatement(modificarconductor);
                st.setString(1,  conductor.getPassNo());
                st.setString(2, conductor.getPassCat());
                st.setString(3, conductor.getPassExpiryDate());
                st.setString(4, conductor.getPassport());
                st.setString(5, conductor.getPassportExpiryDate());
                st.setString(6, conductor.getRh());
                st.setString(7, conductor.getDocument());
                st.setString(8, conductor.getRes_pase());
                st.setString(9, conductor.getNrojudicial());
                st.setString(10, conductor.getVencejudicial());
                st.setString(11, conductor.getNomeps());
                st.setString(12, conductor.getNroeps());
                st.setString(13, conductor.getFecafieps());
                st.setString(14, conductor.getNomarp());
                st.setString(15, conductor.getFecafiarp());
                st.setString(16, conductor.getNrovisa());
                st.setString(17, conductor.getVencevisa());
                st.setString(18, conductor.getNrolibtripulante());
                st.setString(19, conductor.getVencelibtripulante());
               // st.setInt(20, Integer.parseInt(conductor.getGradoriesgo()));
                st.setString(20, conductor.getPension());
                st.setString(21, conductor.getFecafipension());
                st.setString(22, conductor.getUsuario());
                st.setString(23, conductor.getCedula() );
                ////System.out.println("Modificar"+st);
                st.executeUpdate();
            }
        }catch(Exception e){
            throw new SQLException("No se pudo modificar el conductor "+ conductor.getCedula() + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /**
     * Metodo existerConductorAnulCGA, returna true si existe el conductor
     * anulado, sino false
     * param: documento conductor
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existerConductorAnulCGA(String id)throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        PreparedStatement st=null;
        ResultSet rs=null;
        boolean est = false;
        try{
            st= conPostgres.prepareStatement(existe);
            st.setString(1, id);
            
            rs=st.executeQuery();
            if(rs.next()){
                est=true;
            }
            
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda especifica del conductor..." + id + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return est;
    }
    
    /**
     * Metodo cambiarEstadoConductor, cambia el estado, de un conductor
     * Activo o Inactivo
     * param: documento conductor, estado
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void cambiarEstadoConductor(String doc, String estado )throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            st= conPostgres.prepareStatement(this.SQL_CAMBIAR_ESTADO);                        
            
            st.setString(1,  estado);
            st.setString(2,  doc);
            
            String sql = st.toString();
            
            sql = estado.equals("A")? sql.replaceAll("#VETO#", " UPDATE nit SET veto = 'N' WHERE cedula = '"+doc+"';") : sql.replaceAll("#VETO#", " UPDATE nit SET veto = 'S' WHERE cedula = '"+doc+"';");            
            
            st.executeUpdate( sql );
        }catch(Exception e){
            throw new SQLException("No se pudo modificar el conductor "+ conductor.getCedula() + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    /**
     * Metodo cambiarEstadoConductor, cambia el estado, de un conductor
     * Activo o Inactivo
     * param: documento conductor, estado
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void cambiarEstadoNit(String doc, String estado )throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        try{
            st= conPostgres.prepareStatement(this.SQL_CAMBIAR_ESTADO_NIT);                        
            
            st.setString(1,  estado);
            st.setString(2,  estado.equals("A")?"N":"S");
            st.setString(3,  doc);
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("No se pudo modificar el Nit "+ conductor.getCedula() + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /**
     * Metodo buscar_ConductorCGA, busca un conductor, pero solo muestra la cedula y el nombre
     * param: documento del conductor
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarConductorCGA(String id)throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        conductor = null;
        try{
            st= conPostgres.prepareStatement(this.SQL_BUSCA_CONDUCTOR);
            st.setString(1, id);
            ////System.out.println(st);
            rs=st.executeQuery();
            if(rs.next()){ 
                conductor = new Conductor();
                conductor.setCedula(rs.getString("cedula"));
                conductor.setNombre(rs.getString("nombre"));
                conductor.setEstado(rs.getString("estado"));

            }
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda especifica del conductor..." + id + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /**
     * Metodo existeConductorCGA, busca un conductor retirna true si esta de lo contrario false
     * param: documento del conductor
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeConductorCGA(String id)throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        boolean sw = false;
        try{
            st= conPostgres.prepareStatement(this.SQL_BUSCA_CONDUCTOR);
            st.setString(1, id);
            ////System.out.println(st);
            rs=st.executeQuery();
            if(rs.next()){ 
                sw = true;
            }
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda especifica del conductor..." + id + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return sw;
    }
    /**
     * Metodo obtConductorPorCedula, 
     * descripcion: metodo service que para obtener el nombre del conductor por una cedula
     * param: cedula del conductor
     * @autor : Ing. Andres Martinez
     * @version : 1.0
     */ 
     public String obtConductorPorCedula(String id) throws SQLException {
        String nom ="";
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.SQL_CONDUC_POR_NOMBRE);
                st.setString(1, id);


                rs = st.executeQuery();
                while(rs.next()) {
                    nom= rs.getString("nombre");
                }

            }
        }catch(SQLException e) {
            throw new SQLException("ERROR AL INSERTAR EL ESTADO" + e.getMessage()+"" + e.getErrorCode());
        }finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return nom;
     }
     void searchConductor(String nit )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null,rs2=null;
        PoolManager poolManager = null;
        conductor = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT c.cedula, n.nombre, n.nombre1, n.nombre2, n.apellido1, apellido2, coalesce(c.vigenciapase,'0099-01-01')as vigenciapase, n.celular, n.veto FROM nit n left join conductor c on (c.cedula=n.cedula) where   n.cedula=?");
                st.setString(1,nit);
                rs = st.executeQuery();
                if(rs.next()){
                    conductor = new Conductor();
                    conductor.setCedula(rs.getString("cedula"));
                    ////System.out.println("n1 "+rs.getString("nombre1")+rs.getString("nombre2")+"  a1 "+rs.getString("apellido1")+ "  n1 "+rs.getString("apellido2"));
                    ////System.out.println("nombre"+rs.getString("nombre"));
                    if(!rs.getString("nombre1").equals("") || !rs.getString("nombre2").equals("") || !rs.getString("apellido1").equals("") || !rs.getString("apellido2").equals("")  ){
                        conductor.setNombre(rs.getString("nombre1")+" "+rs.getString("nombre2")+ " "+ rs.getString("apellido1")+" "+ rs.getString("apellido2"));
                    }
                    if(rs.getString("nombre1").equals("") && rs.getString("nombre2").equals("") && rs.getString("apellido1").equals("") && rs.getString("apellido2").equals("")  ){
                        conductor.setNombre(rs.getString("nombre"));
                    }
                    else {
                        conductor.setNombre(rs.getString("nombre1")+" "+rs.getString("nombre2")+ " "+ rs.getString("apellido1")+" "+ rs.getString("apellido2"));
                    }
                    //System.out.println("CONDUCTOR ******   "+conductor.getNombre());
                    conductor.setVecPase(rs.getDate("vigenciapase"));
                    conductor.setCelular(rs.getString("celular"));
                    conductor.setTieneFoto(false);
                    if("S".equals(rs.getString("veto"))){
                        conductor.setVetado(true);
                    }
                    st = con.prepareStatement(SLQ_BUSCAFOTO);
                    st.setString(1,nit);
                    rs2 = st.executeQuery();
                    if(rs2.next()){
                        conductor.setTieneFoto(true);
                    }
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NIT" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
     
     /***************************************************
     *Metodo que retorna los propietarios de nit que
     *tienen registro en porveedores.
     *@autor: Ing Enrique De Lavalle.
     *@param: String nit     
     **************************************************/
     public Vector getPropietarios(){
        return this.conductores1;
    }

    /***************************************************
     *Metodo que guarda los propietarios de nit que
     *tienen registro en porveedores.
     *@autor: Ing Enrique De Lavalle.
     *@param: String nit     
     **************************************************/       
    public void consultarPropietarios(String nit)throws SQLException{
        
      Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        conductor1 = null;
        conductores1 = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT DISTINCT n.cedula, n.nombre FROM nit n, proveedor p WHERE (upper(n.nombre) LIKE upper(?) OR upper(n.cedula) LIKE upper(?)) AND n.clasificacion = '00P00' AND n.cedula = p.nit");
                st.setString(1,nit+"%");
                st.setString(2,nit+"%");
                rs = st.executeQuery();
                conductores1 = new Vector();
                while(rs.next()){
                    conductor1 = new Conductor();                    
                    conductor1.setNombre(rs.getString("nombre"));
                    conductor1.setCedula(rs.getString("cedula"));
                    conductores1.add(conductor1);
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NIT" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    } 
 
    
    /**
     * Metodo buscar_ConductorCGA, busca un conductor, pero solo muestra la cedula y el nombre
     * param: documento del conductor
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarNitCGA(String id)throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        conductor = null;
        try{
            st= conPostgres.prepareStatement(this.SQL_BUSCA_NIT);
            st.setString(1, id);
            ////System.out.println(st);
            rs=st.executeQuery();
            if(rs.next()){ 
                conductor = new Conductor();
                conductor.setCedula(rs.getString("cedula"));
                conductor.setNombre(rs.getString("nombre"));
                conductor.setEstado(rs.getString("estado"));

            }
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda especifica del nit..." + id + "---" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
}// FIN DAO
