/*
 * CaravanaDAO.java
 *
 * Created on 20 de diciembre de 2004, 0:53
 */

package com.tsp.operation.model;
import com.tsp.operation.model.beans.*;
import java.sql.*;
import com.tsp.util.connectionpool.PoolManager;
import java.util.*;

/**
 * Clase para manipular las caravanas en el sot (CREACION, ELIMINACION, ACTUALIZACION)
 * @author  amendez
 */
public class CaravanaDAO {
    private static final String CREAR_CARAVANA = 
    "INSERT INTO caravanas (codigo, planilla, usuario, cia, creation_date) VALUES (?, ?, ?, ?, now())";
    
    private static final String OBTENER_NUM_CARAVANA =
    "SELECT nextval('seq_caravanas')";
    
    //BUSCA LA CARAVANA RELACIONANDA A UN PLAN DE VIAJE
    private static final String SEARCH_CARAVANA =
    "SELECT DISTINCT codigo FROM caravanas WHERE planilla = ? AND cia = ? AND estado <> 'A'";
    
    //BUSCA TODOS LOS PLANES DE VIAJE RELACIONADOS A UNA CARAVANA
    private static final String OBTENER_PLANVIAJE_CARAVANA =
    "SELECT codigo, planilla, cia, creation_date FROM caravanas WHERE codigo = ? AND estado <> 'A' ORDER BY planilla";
    
    //ELIMINAR PLANILLA
    private static final String ELIMINAR_PLANILLA_CARAVANA =
    "UPDATE caravanas SET estado = 'A', cancel_date = now(), cancel_user = ?, motivo_cancel = ? WHERE codigo = ? AND planilla = ? AND cia = ? AND creation_date = ?";
    
    //QUERY PARA OBTENER LA INFORMACION COMUN DE UNA CARAVANA
    private static final String SQL_INFO_COMUN = 
    "SELECT al1, at1, al2, at2, al3, at3, "+ 
           "pl1, pt1, pl2, pt2, pl3, pt3, "+ 
           "qlo, qto, qld, qtd, ql1, qt1, "+ 
           "ql2, qt2, ql3, qt3, tl1, tt1, "+ 
           "tl2, tt2, tl3, tt3 "+
      "FROM planViaje "+
     "WHERE planilla = ? "+
       "AND cia = ?";
    
    //CUADO EL OBJETO NO HA OBTENIDO UN NUMERO DE CARAVANA
    private static final int SIN_NUM_CARAVANA = 0;
    
    //DISPONIBLE PARA ADICICIONAR PLANILLAS A LA CARAVANA
    private static final int DISP_ADICION_PLANILLA = 1;
    
    private String numCaravana;
    
    private String codCaravana;
    
    private int estado;
    
    private TreeMap cbxCaravana;
    
    private Vector planilla;
    
    /** Crea una nueva instancia de CaravanaDAO 
     *@return CaravanaDAO
     */
    public CaravanaDAO() {
        estado = SIN_NUM_CARAVANA;
    }
    
    /**
     *Metodo crear una caravana, inserta un registro en la tabla caravas
     *@param caravana un bean de tipo Caravana 
     */
    public void crearCaravana(Caravana caravana) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(CREAR_CARAVANA);
            psttm.setString(1, caravana.getCodigo());
            psttm.setString(2, caravana.getPlanilla());
            psttm.setString(3, caravana.getUsuario());
            psttm.setString(4, caravana.getCia());
            psttm.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DE LA CARAVANA: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();   
            if (con != null) poolManager.freeConnection("fintra", con);
        }     
    }
    
    /**
     *Metodo para obtener el concecutivo de una caravana,
     *guarda el concutivo de la caravana en el campo numCaravana.
     */
    public void obtenerNumCaravana() throws SQLException{
        Connection con = null;
        Statement sttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null; 
        if (estado == SIN_NUM_CARAVANA){
            numCaravana = null;
            try{
                poolManager = PoolManager.getInstance();
                con = poolManager.getConnection("fintra");
                if ((con == null) || con.isClosed())
                    throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
                sttm = con.createStatement();
                rs = sttm.executeQuery(OBTENER_NUM_CARAVANA);

                if (rs.next()){
                    numCaravana = rs.getString(1);
                    estado = DISP_ADICION_PLANILLA; 
                }

            }
            catch(SQLException e){
                throw new SQLException("ERROR OBTENIENDO EL NUMERO DE CARAVANA: " + e.getMessage() + " " + e.getErrorCode());
            }
            finally{
                if (sttm != null) sttm.close();
                if (con != null) poolManager.freeConnection("fintra", con);                
            }
        }
    }
    
    /**
     *Metodo para verificar si una planilla pertenece a una caravana.
     *Si la planilla pertenece a una caravana guarda el numero de la caravana
     *en el campo codCaravana.
     *@param distrito Distrito al que pertenece la planilla.
     *@param planilla Numero de la planilla a buscar.
     */
    public void searchCaravana(String distrito, String planilla) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null; 
       
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SEARCH_CARAVANA);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
            rs = psttm.executeQuery();

            if (rs.next()){
                codCaravana = rs.getString(1); 
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA CARAVANA: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     *Obtiene los planes de viaje relacionados a una caravana
     *a excepcion del plan de viae pasado como parametro en  planilla.
     *Guarda el listado de planes de viajes relacionados en el campo cbxCaravana. 
     *@param caravana Codigo de la carava a la cual se le va a buscar planes de viaje relacionados.
     *@param planilla Planilla que debebe excluirse de la busqueda de planes de vieje relacionados.
     *@param distrito distrito al que pertenece la caravana. 
     */
    public void getPlanViajeCaravana(String caravana, String planilla, String distrito) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null; 
        cbxCaravana = null;
        String cia = null;
        String numpla = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(OBTENER_PLANVIAJE_CARAVANA);
            psttm.setString(1, caravana);
            rs = psttm.executeQuery();
            cbxCaravana = new TreeMap();
            while(rs.next()){
                cia = rs.getString(1);
                numpla = rs.getString(2);
                if (!numpla.equals(planilla) && !cia.equals(distrito)){
                   cbxCaravana.put(rs.getString(2), rs.getString(2)); 
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR OBTENIENDO EL NUMERO DE CARAVANA: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     *Verifica si un plan de viaje pertenece a una caravana.
     *@param distrito Distrito al que pertenece el plan de viaje.
     *@param planilla Numero del plan de viaje.
     *@return devuelve true si el plan de viaje esta relacionado a una caravana en caso contrario false.
     */
    public boolean planViajeExist(String distrito, String planilla) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null; 
        boolean sw = false;       
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SEARCH_CARAVANA);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
           
            rs = psttm.executeQuery();

            if (rs.next()){
                sw = true;
            }
            else
                codCaravana = "";

        }
        catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA CARAVANA: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
        return sw;
    }
    
    /**
     *Genera unlistado de todos los planes de viaje relacionados a una caravana
     *que no hallan sido eliinados de la caravana (estado <> 'A'). Modifica el campo
     *cvna, que es un listado de beans de tipo Caravana que contiene todos los planes de viaje 
     *reacionados a la caravana. 
     *@param caravana Codigo de la caravana.
     */
    public void listarPvjCaravana(String caravana) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null; 
        planilla = null;
        Caravana cvna = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(OBTENER_PLANVIAJE_CARAVANA);
            psttm.setString(1, caravana);
            rs = psttm.executeQuery();
            planilla = new Vector();
            while(rs.next()){
                cvna = new Caravana();
                cvna.setCodigo(rs.getString(1));
                cvna.setPlanilla(rs.getString(2));
                cvna.setCia(rs.getString(3));
                cvna.setCreation_date(rs.getString(4));
                planilla.add(cvna);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR OBTENIENDO LISTADO DE PLANILLAS RELACIONADOS A LA CARAVANA: " + caravana + " " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);                
        }
    }
    
    /**
     *Elimina la planilla de una caravana. 
     *Pone el valor del campo estado de la tabla caravanas en 'A'.
     *@param distrito Distrito al que pertenece la caravana.
     *@param planilla Numero del plan de viaje.
     *@param caravana Codigo de la caravana.
     *@param usuario Id del usuario que hace la eliinacion.
     *@param motivo Razon por la cual se hace la eliminacion.
     *@param creation_date Fecha en la que se adiciono el plan de viaje a la caravana (creation_date de la tabla caravanas)
     */
    public void elimnarPlaCaravana(String distrito, String planilla, String caravana, String usuario, String motivo, String creation_date) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
      
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(ELIMINAR_PLANILLA_CARAVANA);
            psttm.setString(1, usuario);
            psttm.setString(2, motivo);
            psttm.setString(3, caravana);
            psttm.setString(4, planilla);
            psttm.setString(5, distrito);
            psttm.setString(6, creation_date);
            psttm.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("ERROR ELIMINANDO PLANILLA DE LA CARAVANA: " + caravana + " " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     *Obtiene la informacion comun en una caravana como:
     *alientacion, pernoctacion, parqueo y tanqueo.
     *@param caravana Codigo de la caravana.
     *@return Devuelve un bean de tipo PlanViaje con la informacion
     *de alimentacion, pernoctacion, tanqueo y parqueo.
     */
    public PlanViaje getCaravanaInfoComun(String caravana) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        String planilla = null;
        String cia = null;
        PlanViaje planViaje = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(OBTENER_PLANVIAJE_CARAVANA);
            psttm.setString(1, caravana);
            rs = psttm.executeQuery();
            if (rs.next()){
                planilla = rs.getString(2);
                cia = rs.getString(3);
            }
            
            psttm = con.prepareStatement(SQL_INFO_COMUN);
            psttm.setString(1, planilla);
            psttm.setString(2, cia);
            rs = psttm.executeQuery();
            if (rs.next()){
               planViaje = new PlanViaje();
               planViaje.setAl1(rs.getString(1));
               planViaje.setAt1(rs.getString(2));
               planViaje.setAl2(rs.getString(3));
               planViaje.setAt2(rs.getString(4));
               planViaje.setAl3(rs.getString(5));
               planViaje.setAt3(rs.getString(6));
               planViaje.setPl1(rs.getString(7));
               planViaje.setPt1(rs.getString(8));
               planViaje.setPl2(rs.getString(9));
               planViaje.setPt2(rs.getString(10));
               planViaje.setPl3(rs.getString(11));
               planViaje.setPt3(rs.getString(12));
               planViaje.setQlo(rs.getString(13));
               planViaje.setQto(rs.getString(14));
               planViaje.setQld(rs.getString(15));
               planViaje.setQtd(rs.getString(16));
               planViaje.setQl1(rs.getString(17));
               planViaje.setQt1(rs.getString(18));
               planViaje.setQl2(rs.getString(19));
               planViaje.setQt2(rs.getString(20));
               planViaje.setQl3(rs.getString(21));
               planViaje.setQt3(rs.getString(22));
               planViaje.setTl1(rs.getString(23));
               planViaje.setTt1(rs.getString(24));
               planViaje.setTl2(rs.getString(25));
               planViaje.setTt2(rs.getString(26));
               planViaje.setTl3(rs.getString(27));
               planViaje.setTt3(rs.getString(28));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO INFORMACION COMUN DE LA CARAVANA: "+ e.getMessage());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
        return planViaje;
    }
    
    public void finCreacionCaravana(){
        setEstado(SIN_NUM_CARAVANA);
    }
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public int getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property numCaravana.
     * @return Value of property numCaravana.
     */
    public String getNumCaravana() {
        return numCaravana;
    }
    
    /**
     * Setter for property numCaravana.
     * @param numCaravana New value of property numCaravana.
     */
    public void setNumCaravana(String numCaravana) {
        this.numCaravana = numCaravana;
    }
    
    /**
     * Getter for property codCaravana.
     * @return Value of property codCaravana.
     */
    public java.lang.String getCodCaravana() {
        return codCaravana;
    }
    
    /**
     * Setter for property codCaravana.
     * @param codCaravana New value of property codCaravana.
     */
    public void setCodCaravana(java.lang.String codCaravana) {
        this.codCaravana = codCaravana;
    }
    
    /**
     * Getter for property cbxCaravana.
     * @return Value of property cbxCaravana.
     */
    public java.util.TreeMap getCbxCaravana() {
        return cbxCaravana;
    }
    
    /**
     * Setter for property cbxCaravana.
     * @param cbxCaravana New value of property cbxCaravana.
     */
    public void setCbxCaravana(java.util.TreeMap cbxCaravana) {
        this.cbxCaravana = cbxCaravana;
    }
    
    /**
     * Getter for property planilla. 
     * @return Value of property planilla.
     */
    public java.util.Vector getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.util.Vector planilla) {
        this.planilla = planilla;
    }
    
}
