/*
 * CaravanaDAO.java
 *
 * Created on 20 de diciembre de 2004, 0:53
 */

package com.tsp.operation.model;
import com.tsp.operation.model.beans.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author  Armando
 */
public class CaravanaService {
    private CaravanaDAO caravanaDataAccess;
    
    public CaravanaService() {
        caravanaDataAccess = new CaravanaDAO();
    }
    
    public void crearCaravana(Caravana caravana) throws SQLException{
        caravanaDataAccess.crearCaravana(caravana);
    }
    
    public void obtenerNumCaravana() throws SQLException{
        caravanaDataAccess.obtenerNumCaravana();
    }
    
    public String getNumCaravana(){
        return caravanaDataAccess.getNumCaravana();
    }
     
    public void finCreacionCaravana(){
        caravanaDataAccess.finCreacionCaravana();
    }
    
    public void searchCaravana(String distrito, String planilla) throws SQLException{
        caravanaDataAccess.searchCaravana(distrito, planilla);
    }
    
    public boolean planViajeExist(String distrito, String planilla) throws SQLException{
        return caravanaDataAccess.planViajeExist(distrito, planilla);
    }
    
    public String getCodCaravana(){
        return caravanaDataAccess.getCodCaravana();
    }
    
    public TreeMap getCbxCaravana(){
        return caravanaDataAccess.getCbxCaravana();
    }
    
    public void getPlanViajeCaravana(String caravana, String planilla, String distrito) throws SQLException{
        caravanaDataAccess.getPlanViajeCaravana(caravana, planilla, distrito);
    }
    
    public void setCbxCaravana(TreeMap caravanas){
        caravanaDataAccess.setCbxCaravana(caravanas);
    }
    
    public java.util.Vector getPlanilla() {
        return caravanaDataAccess.getPlanilla();
    }
    
    public void listarPvjCaravana(String caravana) throws SQLException{
        caravanaDataAccess.listarPvjCaravana(caravana);
    }
    
    public PlanViaje getCaravanaInfoComun(String caravana) throws SQLException{
        return caravanaDataAccess.getCaravanaInfoComun(caravana);
    }
    
    public void elimnarPlaCaravana(String distrito, String planilla, String caravana, String usuario, String motivo, String creation_date) throws SQLException{
        caravanaDataAccess.elimnarPlaCaravana(distrito, planilla, caravana, usuario, motivo, creation_date);
    }
}