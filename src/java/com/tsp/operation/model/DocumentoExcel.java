/*
 * Nombre        DocumentoExcel.java
 * Descripci�n   Clase para implementar las configuraciones tipicas de un reporte en excel de SOT.
 * Autor         Alejandro Payares
 * Fecha         6 de octubre de 2005, 09:07 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.model;

import java.io.FileOutputStream;

// EXCEL
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

/**
 * Clase para implementar las funciones b�sicas necesarias para crear un archivo
 * Excel usando el proyecto Apache POI.
 * @author  Alejandro Payares
 */
public abstract class DocumentoExcel implements Runnable {
    
    /**
     * El libro de excel
     */
    protected   HSSFWorkbook   libroExcel;
    /**
     * la hoja actual de excel
     */
    protected   HSSFSheet      hojaExcel;
    /**
     * una fila de la hoja
     */
    protected   HSSFRow        row;
    /**
     * una celda de la fila
     */
    protected   HSSFCell       cell;
    /**
     * la ruta donde se crear� el archivo
     */
    protected   String  path;
    /**
     * el ancho del borde
     */
    protected   short borde = HSSFCellStyle.BORDER_THIN;
    /**
     * el estilo blanco por default de una hoja de excel
     */
    protected   HSSFCellStyle estiloBlanco;
    /**
     * El estilo blanco con letra grande (12 puntos)
     */
    protected   HSSFCellStyle estiloBlancoGrande;
    
    
    /** Creates a new instance of DocumentoExcel */
    public DocumentoExcel() {
    }
    
    /**
     * Inicializa los elementos que participan en la creaci�n del reporte.
     * @param fileName El nombre del archivo donde se guardar� el reporte
     */
    public void initExcel(String fileName){
        this.libroExcel = new HSSFWorkbook();
        this.hojaExcel  = libroExcel.createSheet(fileName);
        this.row        = null;
        this.cell       = null;
        crearColores();
        crearEstilos();
        HSSFFont font = libroExcel.createFont();
        font.setFontHeightInPoints((short)10);        //tamano letra
        font.setFontName("Arial");                    //Tipo letra
        font.setColor(HSSFColor.BLACK.index);
        estiloBlanco = libroExcel.createCellStyle();
        ponerBordeAEstilo(estiloBlanco,borde);
        configurarEstilo(estiloBlanco, font, false, HSSFColor.WHITE.index);
        
        font = libroExcel.createFont();
        font.setFontHeightInPoints((short)13);        //tamano letra
        font.setFontName("Arial");                    //Tipo letra
        font.setColor(HSSFColor.BLACK.index);                    //color letra blanca
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        estiloBlancoGrande = libroExcel.createCellStyle();
        ponerBordeAEstilo(estiloBlancoGrande,borde);
        configurarEstilo(estiloBlancoGrande, font, false, HSSFColor.WHITE.index);
        
    }
    
    /**
     * Crea los colores necesarios para el reporte
     */
    protected abstract void crearColores();
    
    /**
     * Crea los estilos necesarios para el reporte
     */
    protected abstract void crearEstilos();
    
    /**
     * Permite modificar un estilo con los par�metros recibidos. Evita el tener que realizar
     * est�s configuraciones repetidas veces para los distintos estilos que usa un documento
     * Excel
     * @param estilo El estilo que ser� modificado
     * @param font el tipo de fuente que ser� colocado al estilo
     * @param centrado una bandera para indicar si ser� centrado o no
     * @param color el color que ser� colocado al estilo
     */
    protected void configurarEstilo(HSSFCellStyle estilo, HSSFFont font, boolean centrado, short color ){
        estilo.setFont(font);
        estilo.setFillForegroundColor(color);
        estilo.setFillPattern((short) HSSFCellStyle.SOLID_FOREGROUND);
        estilo.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
        ponerBordeAEstilo(estilo, borde);
        if ( centrado ){
            estilo.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            estilo.setVerticalAlignment(estilo.VERTICAL_CENTER);
        }
    }
    
    /**
     * crea una celda en la fila y la columna dada
     * @param fila la fila donde se crear� la celda
     * @param columna la columna donde se crear� la celda
     * @throws Exception si alg�n error ocurre
     */
    protected void crearCelda(int fila, int columna) throws Exception{
        row = hojaExcel.getRow(fila) == null? hojaExcel.createRow((short)fila):hojaExcel.getRow(fila);
        cell = row.createCell((short)columna);
        row = hojaExcel.getRow((short)fila);
    }
    
    /**
     * prepara varias celdas para la escritura
     * @param fila la fila donde se preparar�n las celdas
     * @param columnas las columnas de las celdas
     * @throws Exception si alg�n error ocurre
     */
    protected void prepared(int fila, int columnas) throws Exception{
        row = hojaExcel.createRow((short)fila);
        for(int j=0;j<columnas;j++)
            cell = row.createCell((short)j);
        row = hojaExcel.getRow((short)fila);
    }
    
    /**
     * Guarda el documento en la ruta y nombre de archivo espec�ficado previamente.
     * @throws Exception si alg�n error ocurre
     */
    public void save() throws Exception{
        try{
            FileOutputStream fo = new FileOutputStream(path);
            libroExcel.write(fo);
            fo.close();
            ////System.out.println("Documento excell guardado: "+path);
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    /**
     * Permite crear una celda combinada o merged en el rango de coordenadas dada
     * @param filaInicial la fila inicial de la celda combinada
     * @param colInicial la columna inicial de la celda combinada
     * @param filaFinal la fila final de la celda combinada
     * @param colFinal la columna final de la celda combinada
     * @param conBorde indica si la celda combinada tendr� o no borde
     * @throws Exception si alg�n error ocurre
     */
    protected void crearCeldaCombinada(int filaInicial, int colInicial, int filaFinal, int colFinal, boolean conBorde) throws Exception{
        Region reg = new Region(filaInicial,(short)colInicial,filaFinal,(short)colFinal);
        hojaExcel.addMergedRegion(reg);
        
        if ( conBorde ){
            ponerBordeARegion(reg,HSSFCellStyle.BORDER_THIN);
        }
    }
    
    
    /**
     * Permite crear una copia exacta de un estilo
     * @param estilo el estilo a clonar
     * @return el estilo clonado
     */
    protected HSSFCellStyle clonarEstilo(HSSFCellStyle estilo){
        HSSFCellStyle clon = libroExcel.createCellStyle();
        clon.setAlignment(estilo.getAlignment());
        clon.setBorderBottom(estilo.getBorderBottom());
        clon.setBorderLeft(estilo.getBorderLeft());
        clon.setBorderRight(estilo.getBorderRight());
        clon.setBorderTop(estilo.getBorderTop());
        clon.setBottomBorderColor(estilo.getBottomBorderColor());
        clon.setDataFormat(estilo.getDataFormat());
        clon.setFillBackgroundColor(estilo.getFillBackgroundColor());
        clon.setFillForegroundColor(estilo.getFillForegroundColor());
        clon.setFillPattern(estilo.getFillPattern());
        clon.setFont(libroExcel.getFontAt(estilo.getFontIndex()));
        clon.setHidden(estilo.getHidden());
        clon.setIndention(estilo.getIndention());
        clon.setLeftBorderColor(estilo.getLeftBorderColor());
        clon.setLocked(estilo.getLocked());
        clon.setRightBorderColor(estilo.getRightBorderColor());
        clon.setRotation(estilo.getRotation());
        clon.setTopBorderColor(estilo.getTopBorderColor());
        clon.setVerticalAlignment(estilo.getVerticalAlignment());
        clon.setWrapText(estilo.getWrapText());
        
        return clon;
    }
    
    /**
     * Permite quitarle el borde a un estilo
     * @param estilo el estilo al que le ser� quitado el borde
     */
    public static void quitarBordeAEstilo(HSSFCellStyle estilo){
        ponerBordeAEstilo(estilo, HSSFCellStyle.BORDER_NONE );
    }
    
    /**
     * Permite ponerle borde a un estilo
     * @param estilo el estilo al que ser� puesto el borde
     * @param borde el borde que ser� puesto al estilo
     */
    public static void ponerBordeAEstilo(HSSFCellStyle estilo, short borde ){
        estilo.setBorderBottom(borde);
        estilo.setBorderLeft(borde);
        estilo.setBorderTop(borde);
        estilo.setBorderRight(borde);
    }
    
    
    
    /**
     * Permite ponerle bode a una regi�n
     * @param reg la region a la que ser� puesto el borde
     * @param borde el borde que ser� puesto a la regi�n
     */
    protected void ponerBordeARegion(Region reg, short borde){
        try {
            HSSFRow fila = hojaExcel.getRow(reg.getRowFrom());
            HSSFCell celda = fila.getCell(reg.getColumnFrom());
            //////System.out.println("celda (0,0) #"+celda.hashCode());
            HSSFCellStyle estilo = clonarEstilo(celda.getCellStyle());
            
        /*
         __
         |
         
         */
            estilo.setBorderLeft(borde);
            estilo.setLeftBorderColor(HSSFColor.BLACK.index);
            estilo.setBorderTop(borde);
            estilo.setTopBorderColor(HSSFColor.BLACK.index);
            celda.setCellStyle(estilo);
            //////System.out.println("Estilo de celda (0,0) #"+celda.hashCode()+" despues de aplicar bordes izquierdo y superior: ");
            //mostrarBordesDeEstilo(estilo);
            
         /*
         ___->
          
          
          */
            String str = "";
            for(int i=reg.getColumnFrom()+1; i<=reg.getColumnTo(); i++){
                celda = fila.getCell((short)i);
                //////System.out.println(str);
                estilo = clonarEstilo(celda.getCellStyle());
                estilo.setTopBorderColor(HSSFColor.BLACK.index);
                estilo.setBorderTop(borde);
                celda.setCellStyle(estilo);
                //////System.out.println("Estilo de "+str+" despues de aplicar borde superior: ");
                //mostrarBordesDeEstilo(estilo);
            }
        /*
         __
          |
         
         */
            estilo.setRightBorderColor(HSSFColor.BLACK.index);
            estilo.setBorderRight(borde);
            //////System.out.println("Estilo de "+str+" despues de aplicar borde derecho: ");
            //mostrarBordesDeEstilo(estilo);
        /*
          |
          |
          v
         
         */
            
            for( int i=reg.getRowFrom()+1; i<=reg.getRowTo(); i++ ){
                fila = hojaExcel.getRow(i);
                celda = fila.getCell(reg.getColumnTo());
                str = "celda ("+fila.getRowNum()+","+i+") #"+celda.hashCode();
                //  ////System.out.println(str);
                estilo = clonarEstilo(celda.getCellStyle());
                estilo.setRightBorderColor(HSSFColor.BLACK.index);
                estilo.setBorderRight(borde);
                celda.setCellStyle(estilo);
                //////System.out.println("Estilo de "+str+" despues de aplicar borde derecho: ");
                //mostrarBordesDeEstilo(estilo);
            }
            
        /*
         
         __|
         
         */
            estilo.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo.setBorderBottom(borde);
            //    ////System.out.println("Estilo de "+str+" despues de aplicar borde inferior: ");
            //mostrarBordesDeEstilo(estilo);
            
        /*
         
         
         <-___
         
         */
            for( int i=reg.getColumnTo()-1; i >= reg.getColumnFrom(); i-- ){
                celda = fila.getCell((short)i);
                str = "celda ("+fila.getRowNum()+","+i+") #"+celda.hashCode();
                //      ////System.out.println(str);
                estilo = clonarEstilo(celda.getCellStyle());
                estilo.setBottomBorderColor(HSSFColor.BLACK.index);
                estilo.setBorderBottom(borde);
                celda.setCellStyle(estilo);
                //    ////System.out.println("Estilo de "+str+" despues de aplicar borde inferior: ");
                //mostrarBordesDeEstilo(estilo);
            }
            
        /*
         
         |_
         
         */
            estilo.setLeftBorderColor(HSSFColor.BLACK.index);
            estilo.setBorderLeft(borde);
            //////System.out.println("Estilo de "+str+" despues de aplicar borde izquierdo: ");
            //mostrarBordesDeEstilo(estilo);
        /*
          /\
          |
          |
         
         */
            for( int i=reg.getRowTo()-1; i > reg.getRowFrom(); i-- ){
                fila = hojaExcel.getRow(i);
                celda = fila.getCell(reg.getColumnFrom());
                str = "celda ("+fila.getRowNum()+","+i+") #"+celda.hashCode();
                //  ////System.out.println(str);
                estilo = clonarEstilo(celda.getCellStyle());
                estilo.setLeftBorderColor(HSSFColor.BLACK.index);
                estilo.setBorderLeft(borde);
                celda.setCellStyle(estilo);
                //////System.out.println("Estilo de "+str+" despues de aplicar borde izquierdo: ");
                //mostrarBordesDeEstilo(estilo);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    
}
