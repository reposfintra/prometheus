 /*
  * MainRD.java
  *
  * Created on 18 de junio de 2005, 12:34 PM
  * modificado: egonzalez2014
  */

/**
 *
 * @author  Jm
 */
package com.tsp.operation.model;
import java.util.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import java.net.URL;


public class MainRD {
    private static Model model = new Model();
    private static Properties propTime = new Properties();
    // Variables Auxiliares
    private static String fecha_ini = "";
    private static String fecha_fin = "";
    private static String fecha_actual = "";
    private static String fini_busq;
    private static String ffin_busq;
    private static String hoy;
    private static String ayer;
    private static java.sql.Timestamp fec_c;
    private static java.sql.Timestamp fec_a;
    private static java.sql.Timestamp fecha_despacho;
    //Vectores de Consulta
    private static Vector planilla = new Vector();
    private static Vector planilla1 = new Vector();
    private static Vector planilla2 = new Vector();
    private static Vector planilla3 = new Vector();
    private static Vector moventrega = new Vector();
    private static Vector movtrafico = new Vector();
    private static Vector recusosD = new Vector();
    private static boolean flagmov = true; //Indica true si hay movimientos de trafico o de entrega false de lo contrario
    //BEANS
    private static Planilla pl,plaux;
    private static Movtrafico mv;
    private static Tramo tr;
    private static Plarem plr;
    private static Remesa r;
    private static Placa p;
    private static BDOracle oracle;
    private static Tiporec tp;
    private static Recursosdisp rd,rd_aso;
    private static Util u = new Util();
    //Datos de la Tabla Recursos Disponibles
    private static String distrito;
    private static String numpla;
    private static String origen;
    private static String destino;
    private static String placa, placatrailer="";
    private static String clase;
    private static String tipo;
    private static String reg_status;
    private static java.sql.Timestamp fecha_disp;
    private static float tiempo;
    private static String tipo_aso="";
    private static String cod_cliente;
    private static String equipo_aso="";
    private static String recurso;
    private static String asso_rec;
    private static String cod_tipo;
    private static String user_name;
    private static String date_creation;
    private static String clase_aso;
    private static String std_job_no_rec;
    
    private static String agencia; //Jescandon 09-10-06
    //Aux
    
    private static Calendar yesterday;
    private static Calendar fecdes;
    ////sandrameg
    private static String nu = "";
    private static String fi = "";
    private static String ff = "";
    private static String dis = "";
    private static String tip = "";
    
    //Variables de Control
    private static int ControlRegistros;
    private static int RegistrosNoInsertados;
    private static boolean flag;
    private List ListaIncosistencias = new LinkedList();
    
    /** Creates a new instance of MainRD */
    public MainRD( String u, String fi, String dis, String tip ) {
        this.nu = u;
        this.fi = fi;
        this.dis = dis;
        this.tip = tip;
        this.ControlRegistros =  0;
        this.RegistrosNoInsertados = 0;
    }
    public MainRD(){}
    
    /**
     * @param args the command line arguments
     */
    public void principalRecTSP() throws  java.io.IOException,java.sql.SQLException,Exception {
        
        MainRD principal = new MainRD(nu, fi, dis, tip);
        
        boolean aux;
        ////System.out.println("INICIO DE PROCESO");
        try{
            
            if ( tip.equals("nuevo")){
                
                principal.AsignarPropiedadesNuevo(fi, nu, dis);
                //Se Borran Todos los Registros de la Tabla Recursos_disp
                //model.rdSvc.DeleteAll();
            }
            else{
                ////System.out.println("Proceso Actualizacion");
                principal.CargarPropiedades();
                principal.AsignarPropiedades();
                //Se Borran los registros que se encuentren en el rango de fechas de actualizacion
            }
            
            principal.InsertarVehExterno(distrito,fini_busq, ffin_busq);
            ////System.out.println("INSERTO VEH EXTERNOS");
            planilla = principal.RecolectarInfPlanilla(distrito,fini_busq, ffin_busq);
            
            for(int i = 0; i<planilla.size(); i++){
                flag = true;
                pl = (Planilla)planilla.elementAt(i);
                aux=true;
                
                try{
                    model.trSvc.list(pl.getOrigenpla(), pl.getDestinopla());
                    
                    //Incosistencia tramo no existe en el archivo tramo
                    
                }catch(Exception e){
                    ////System.out.println("Error: "+e.getMessage());
                }
                if ( model.trSvc.getTr()==null ){
                    ////System.out.println("No existe la via..");
                    this.RegistrosNoInsertados++;
                    principal.RegistrarInconsistencia(1, pl.getNumpla(), pl.getOrigenpla()+"-"+pl.getDestinopla(), pl.getPlaveh());
                }
                else{
                    try{
                        tr = model.trSvc.getTr();
                        if(tr!=null){
                            ////System.out.println("El tramo no es NULO");
                            try{
                                reg_status = pl.getReg_status();
                                numpla = pl.getNumpla();
                                origen = tr.getOrigen();
                                destino = tr.getDestino();
                                tiempo = tr.getTiempo();
                                placa = pl.getPlaveh();
                                placatrailer = pl.getPlatr();
                                agencia = pl.getAgcpla();
                                
                            }catch(Exception e){
                                ////System.out.println("Error "+e.getMessage());
                            }
                            
                            String res = u.dateTimeFormat(pl.getFechaPLlegada());
                            
                            
                            
                            if( tiempo == 0.0 ){
                                ////System.out.println("Encontre la inconsistencia del tiempo");
                                flag = false;
                                this.RegistrosNoInsertados++;
                                principal.RegistrarInconsistencia(3, origen + "-" + destino, String.valueOf(tiempo), pl.getPlaveh());
                                
                            }
                            else{
                                //Incosistencia de Datos en fechaposllegada en el Archivo Planilla
                                if ( res.equalsIgnoreCase("1900-01-01 00:00:00")){
                                    flag = false;
                                    this.RegistrosNoInsertados++;
                                    principal.RegistrarInconsistencia(2, pl.getNumpla(), res, pl.getPlaveh());
                                }
                                else{
                                    if(!res.equalsIgnoreCase("0099-01-01 00:00:00") ){
                                        fecha_disp = pl.getFechaPLlegada();
                                    }
                                    
                                    else{
                                        String s = u.FechaDisp(u.ConvertirToString(pl.getFechadespacho()), tiempo);
                                        fecha_disp = u.ConvertiraTimestamp(s);
                                    }
                                    
                                    principal.BuscarCodCliente(numpla);
                                    boolean fv = principal.BuscarClaseTipoRecurso(placa);
                                    if( fv == false){
                                        ////System.out.println("Encontre la inconsistencia de la Placa");
                                        flag = false;
                                        this.RegistrosNoInsertados++;
                                        principal.RegistrarInconsistencia(4, pl.getNumpla(), placa, pl.getPlaveh() );
                                        
                                    }
                                    else{
                                        if(!reg_status.equals("A")){
                                            if ( !placatrailer.equals("") && !placatrailer.equals("NA")){
                                                principal.BuscarClaseTipoRecursoAsocioado(placatrailer);
                                                principal.InsertarRDAsociado();
                                                
                                            }
                                            else{
                                                ////System.out.println("INSERTO RECURSOS");
                                                principal.InsertarRD();
                                            }
                                        }
                                        else{
                                            model.rdSvc.DeleteRD(placa);
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            ////System.out.println("Es nulo el tramo...");
                        }
                    }catch(Exception e){
                        ////System.out.println("Error: "+e.getMessage());
                        
                    }
                    
                }
                
                
            }
            try{
                principal.ReAsignarPropiedades();
            }catch ( Exception e ) {
                throw new Exception("ERROR "+e.getMessage());
            }
            principal.CrearLog();
            ////System.out.println("FIN DE PROCESO");
            
        }catch ( Exception e ) {
            throw new Exception("ERROR "+e.getMessage());
        }
        
        
    }
    
    public void CargarPropiedades() throws IOException{
        InputStream is = getClass().getResourceAsStream("StoreInf.properties");
        propTime.load(is);
    }
    
    public void AsignarPropiedades() throws IOException{
        fecha_ini = propTime.getProperty("fecha_fin");
        fini_busq = fecha_ini.substring(0,10);
        fecha_actual = u.getFechaActual_String(6);
        hoy = u.getFechaActual_String(4);
        fecha_fin = fecha_actual;
        ffin_busq = fecha_fin.substring(0,10);
        yesterday = u.JCalendar(hoy);
        yesterday = u.AddCalendar(yesterday, -2);
        ayer = u.crearStringFechaDate(yesterday);
        user_name = "ADMIN"; //propTime.getProperty("usuario");
        distrito = "FINV";
        fec_c = u.toTimestamp(u.ConvertiraDate(fecha_actual));
        fec_a = fec_c;
    }
    
    public void AsignarPropiedadesNuevo(String fi, String us, String ds) throws IOException{
        fecha_ini = fi;
        hoy = u.getFechaActual_String(4);
        yesterday = u.JCalendar(hoy);
        yesterday = u.AddCalendar(yesterday, -2);
        ayer = u.crearStringFechaDate(yesterday);
        fini_busq = fecha_ini.substring(0,10);
        fecha_fin = hoy;
        ffin_busq = fecha_fin.substring(0,10);
        fecha_actual = u.getFechaActual_String(6);
        user_name = us;
        distrito = ds;
        fec_c = u.toTimestamp(u.ConvertiraDate(fecha_actual));
        fec_a = fec_c;
    }
    
    public void ReAsignarPropiedades() throws IOException{
        String actual = u.getFechaActual_String(6);
        propTime.setProperty("fecha_ini", actual);
        propTime.setProperty("fecha_fin", fecha_actual);
        //propTime.store(new FileOutputStream("C:/Tomcat5/webapps/slt2/WEB-INF/classes/com/tsp/operation/model/StoreInf.properties"),"");
        URL u = getClass().getResource("StoreInf.properties");
        propTime.store(new FileOutputStream(u.getPath()),"");
    }
    
    
    //Recolecta la informacion de la tabla planilla de acuerdo a un rango de fechas
    //Se Crean dos vectores uno con los registros creados en rango y otro con los modificados
    public Vector RecolectarInfPlanilla( String d,String fecha_i, String fecha_f) throws Exception{
        ////System.out.println("INICIO DE PROCESO DE RECOLECCION DE REGISTROS DESDE PLANILLAS");
        model.plSvc.listRecursosDisp(d,fecha_i,fecha_f);
        Vector pln = model.plSvc.getPlanillas2();
        ////System.out.println("FIN DE PROCESO DE RECOLECCION DE REGISTROS DESDE PLANILLAS");
        return pln;
        
    }
    
    //Inserta un Registro en la Tabla de Recursos Disponibles
    public void InsertarRD() throws Exception{
        rd = new Recursosdisp();
        rd.setDistrito(distrito);
        rd.setNumpla(numpla);
        rd.setOrigen(origen);
        rd.setDestino(destino);
        rd.setPlaca(placa);
        rd.setClase(clase);
        rd.setTipo(tipo);
        rd.setFecha_disp(fecha_disp);
        rd.setTiempo(tiempo);
        rd.setCod_cliente(cod_cliente);
        rd.setFecha_creacion(u.ConvertiraTimestamp(fecha_actual));
        rd.setFecha_actualizacion(u.ConvertiraTimestamp(fecha_actual));
        rd.setUsuario_creacion(user_name);
        rd.setStd_job_no_rec(std_job_no_rec);
        
        rd.setAgencia(agencia);//Jescandon 09-10-06
        model.rdSvc.setRdisp(rd);
        
        /*Jescandon*/
        model.rdSvc.Search_record(placa);
        Recursosdisp aux = model.rdSvc.getRdisp();
        rd.setNo_Viajes(aux.getNo_Viajes());
        /**/
        
        /*Jescandon  13-01-06*/
        String std = model.rdSvc.Search_Wgroup(placa);
        rd.setWork_gorup(std);
        /**/
        
        if( !model.rdSvc.BuscarRecurso(placa)){
            model.rdSvc.insert();
            this.ControlRegistros++;
        }
        else{
            model.rdSvc.Update();
            this.ControlRegistros++;
        }
        
        
        
    }
    
    //Inserta un Registro y otro que corresponde al recurso asociado en la Tabla de Recursos Disponibles
    public void InsertarRDAsociado() throws Exception{
        
        rd = new Recursosdisp();
        rd.setDistrito(distrito);
        rd.setNumpla(numpla);
        rd.setOrigen(origen);
        rd.setDestino(destino);
        rd.setPlaca(placa);
        rd.setClase(clase);
        rd.setTipo(tipo);
        rd.setFecha_disp(fecha_disp);
        rd.setTiempo(tiempo);
        rd.setTipo_aso(tipo_aso);
        rd.setEquipo_aso(placatrailer);
        rd.setCod_cliente(cod_cliente);
        rd.setFecha_creacion(u.ConvertiraTimestamp(fecha_actual));
        rd.setFecha_actualizacion(u.ConvertiraTimestamp(fecha_actual));
        rd.setUsuario_creacion(user_name);
        rd.setStd_job_no_rec(std_job_no_rec);
        rd.setAgencia(agencia);//Jescandon 09-10-06
        
        model.rdSvc.setRdisp(rd);
        
        /*Jescandon*/
        model.rdSvc.Search_record(placa);
        Recursosdisp aux = model.rdSvc.getRdisp();
        rd.setNo_Viajes(aux.getNo_Viajes());
        /**/
        
        /*Jescandon  13-01-06*/
        String std = model.rdSvc.Search_Wgroup(placa);
        rd.setWork_gorup(std);
        /**/
        
        if( !model.rdSvc.BuscarRecurso(placa)){
            model.rdSvc.insertEquipoA();
            this.ControlRegistros++;
        }
        else{
            model.rdSvc.UpdateRA();
            this.ControlRegistros++;
        }
        
        rd_aso = new Recursosdisp();
        rd_aso.setDistrito(distrito);
        rd_aso.setNumpla(numpla);
        rd_aso.setOrigen(origen);
        rd_aso.setDestino(destino);
        rd_aso.setPlaca(placatrailer);
        rd_aso.setClase(clase_aso);
        rd_aso.setTipo(tipo_aso);
        rd_aso.setFecha_disp(fecha_disp);
        rd_aso.setTiempo(tiempo);
        rd_aso.setTipo_aso(tipo);
        rd_aso.setEquipo_aso(placa);
        rd_aso.setCod_cliente(cod_cliente);
        rd_aso.setFecha_creacion(u.ConvertiraTimestamp(fecha_actual));
        ////System.out.println(u.ConvertiraTimestamp(fecha_actual));
        rd_aso.setFecha_actualizacion(u.ConvertiraTimestamp(fecha_actual));
        rd_aso.setUsuario_creacion(user_name);
        rd_aso.setStd_job_no_rec(std_job_no_rec);
        rd_aso.setAgencia(agencia);//Jescandon 09-10-06
        model.rdSvc.setRdisp(rd_aso);
        
        /*Jescandon*/
        model.rdSvc.Search_record(placatrailer);
        aux = model.rdSvc.getRdisp();
        rd_aso.setNo_Viajes(aux.getNo_Viajes());
        /**/
        
        /*Jescandon  13-01-06*/
        String stdaux = model.rdSvc.Search_Wgroup(placa);
        rd_aso.setWork_gorup(stdaux);
        /**/
        
        if (!model.rdSvc.BuscarRecursoTrailer(placatrailer)){
            model.rdSvc.insertEquipoA();
            this.ControlRegistros++;
        }
        else{
            model.rdSvc.UpdateRA();
            this.ControlRegistros++;
        }
        
    }
    /*****************************************************************************************
     * Metodo: InsertarVehExterno
     * parametros: distrito, fecha inicial, fecha final.
     * autor: Ing. Andres MartinezInserta
     * descripcion: Inserta o modifica un Vehiculo Externo en la Tabla de Recursos Disponibles
     ******************************************************************************************/    
    public void InsertarVehExterno( String d, String fi, String ff ) throws Exception{
        model.planillaService.ListaVehExternos(d, fi, ff );
        Vector ve = model.planillaService.getPlanillasVehExternos();
        Iterator it  = ve.iterator();
        while(it.hasNext()){
            Planilla pl = (Planilla)it.next();
            rd = new Recursosdisp();
            rd.setDistrito(pl.getDistrito());
            rd.setNumpla("");
            rd.setOrigen("");
            rd.setDestino(pl.getDestinoRelacionado());
            rd.setPlaca(pl.getPlaveh());
            rd.setClase("");
            rd.setTipo("");
            rd.setFecha_disp(pl.getFechadespacho());
            rd.setTiempo(0);
            rd.setCod_cliente("");
            rd.setFecha_creacion(u.ConvertiraTimestamp(fecha_actual));
            rd.setFecha_actualizacion(u.ConvertiraTimestamp(fecha_actual));
            rd.setUsuario_creacion(user_name);
            rd.setStd_job_no_rec("");
            model.rdSvc.setRdisp(rd);
            
            /*Jescandon*/
            model.rdSvc.Search_record(pl.getPlaveh());
            Recursosdisp aux = model.rdSvc.getRdisp();
            rd.setNo_Viajes(aux.getNo_Viajes());
            /**/
            
            /*Jescandon  13-01-06*/
            String std = model.rdSvc.Search_Wgroup(pl.getPlaveh());
            rd.setWork_gorup(std);
            /**/
            
            if( !model.rdSvc.BuscarRecurso(pl.getPlaveh())){
                model.rdSvc.insert();
                this.ControlRegistros++;
            }
            else
                model.rdSvc.Update();
            
        }
        
    }
    
    
    //Verificacion movimientos de trafico de una planilla
  /*  public boolean ListMovTrafico( Planilla p ) throws Exception{
        distrito = p.getDistrito();
        //Movimientos de Entrega;
        model.mvSvc.listME(p.getNumpla());
        mv = model.mvSvc.getMovimientos();
        //Verificacion de movimientos de entrega
        Calendar fechaentrega = u.JCalendar(mv.getFechaentrega());
        Calendar fechai = u.JCalendar(ayer);
        Calendar fechaf = u.JCalendar(hoy);
   
        if( mv != null ){
             if( fechaentrega.after(fechai) && fechaentrega.before(fechaf) )
                 flagmov = true;
             else
                 flagmov = false;
   
         }
   
   
        //Otros movimientos de Trafico;
        model.mvSvc.listMT(p.getNumpla());
        movtrafico = model.mvSvc.getMov();
   
        if(model.mvSvc.getMov().size() > 0 ){
            flagmov = true;
        }
        else{
            flagmov = false;
        }
   
        Calendar fechaPosL = u.JCalendar(u.ConvertirToString(p.getFechaPLlegada()));
   
        //Verificacion si no hay ninguna clase de movimientos
        if ((moventrega.size() == 0) && ( movtrafico.size() == 0)){
            /*if(u.MayorIgualString(u.ConvertirToString(p.getFechaPLlegada()),ayer)){
                flagmov = true;
            }
            else{
                flagmov = false;
            }*/
        /*    if( fechaPosL.after(fechai) && fechaPosL.before(fechaf))
                flagmov = true;
            else
                flagmov = false;
        }
        return flagmov;*/
    
    //  }/*
    
    //Busca el Codigo del Cliente
    public void BuscarCodCliente( String n ) throws Exception{
        model.rSvc.list(n);
        r = model.rSvc.getRm();
        cod_cliente = r.getStd_job_no();
        cod_cliente = u.cliente(cod_cliente);
        std_job_no_rec = r.getStd_job_no();
    }
    
    //Busca la Clase y el Tipo de Recurso
    public boolean BuscarClaseTipoRecurso( String n ) throws Exception{
        boolean flagVeh = true;
        model.vehSvc.listVeh(n);
        p = model.vehSvc.getV();
        if( p == null)
            flagVeh = false;
        else{
            recurso = p.getRecurso();
//            model.bdSvc.list(recurso);
//            oracle = model.bdSvc.getB();
            asso_rec = oracle.getAsso_rec();
            clase = u.clase(asso_rec);
            cod_tipo = u.codTipo(asso_rec);
            model.tpSvc.list(cod_tipo);
            tp = model.tpSvc.getTprec();
            tipo = tp.getTipo();
        }
        return flagVeh;
    }
    
    //Busca la Clase y tipo del recurso asociado
    public void BuscarClaseTipoRecursoAsocioado( String n ) throws Exception{
        try{
            model.vehSvc.listVeh(n);
            p = model.vehSvc.getV();
            equipo_aso = p.getPlaca();
            recurso = p.getRecurso();
//            model.bdSvc.list(recurso);
//            oracle = model.bdSvc.getB();
            asso_rec = oracle.getAsso_rec();
            cod_tipo = u.codTipo(asso_rec);
            clase_aso = u.clase(asso_rec);
            model.tpSvc.list(cod_tipo);
            tp = model.tpSvc.getTprec();
            tipo_aso = tp.getTipo();
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
    public Vector VectorFiltrado( Vector datos )throws IOException{
        ////System.out.println("INICIO FILTRACION DATOS");
        Vector nuevo = new Vector();
        try{
            File file = new File("/exportar/Logs");
            file.mkdirs();
            String NombreArchivo = "/exportar/Logs/vectorfiltrado.txt";
            PrintStream archivo = new PrintStream(NombreArchivo);
            archivo.println("Placa;Fecdespacho;Numpla");
            
            for( int i=0; i<datos.size(); i++ ){
                Planilla p = (Planilla) datos.elementAt(i);
                Planilla mayor = p;
                for( int j=i+1; j<datos.size(); j++ ){
                    Planilla p2 = (Planilla) datos.elementAt(j);
                    if (( p.equals(p2) && p.compareTo(p2) < 0) || ( p.equals(p2) && p.compareTo(p2) == 0)){
                        mayor = p2;
                    }
                }
                nuevo.addElement(mayor);
                archivo.println(mayor.getPlaveh()+";"+mayor.getFecdsp()+";"+mayor.getNumpla());
            }
            ////System.out.println("FIN FILTRACION DATOS");
        }catch(IOException e){
            throw new IOException("Error en vector filtrado");
        }
        return nuevo;
        
    }
    
    //Registra las incosistencias
    public void RegistrarInconsistencia( int Tipo, String np, String reg, String pla ){
        Inconsistencia inc = new Inconsistencia();
        String ITramo = "EL TRAMO NO EXISTE EN EL ARCHIVO TRAMO";
        String IFechaposllegada = "LA FECHA DE POSIBLE LLEGADA NO ES VALIDA";
        String ITiempo = "EL TIEMPO EN EL ARCHIVO VIA NO ES VALIDO";
        String IPlaca = "LA PLACA DEL VEHICULO NO EXISTE EN EL ARCHIVO PLACA";
        String INovalida = "EL RECURSO NO ES RECURSO DISPONIBLE";
        String Descripcion ="";
        String Columna ="";
        String Archivo = "";
        if( Tipo == 1 ){
            Descripcion = ITramo;
            Columna = "Origen - Destino";
            Archivo = "Via";
        }
        else if( Tipo == 2){
            Descripcion = IFechaposllegada;
            Columna = "Fechaposllegada";
            Archivo = "Planilla";
        }
        else if( Tipo == 3){
            Descripcion = ITiempo;
            Columna = "Tiempo";
            Archivo = "Via";
        }
        else if( Tipo == 4){
            Descripcion = IPlaca;
            Columna = "Placa";
            Archivo = "Placa";
        }
        else if( Tipo == 5){
            Descripcion = INovalida;
            Columna = "Numpla";
            Archivo = "Planilla";
        }
        
        inc.load(Tipo, Descripcion, np, reg, Columna, Archivo, pla );
        
        this.ListaIncosistencias.add(inc);
        
        
    }
    
    public void CrearLog() throws Exception{
        ////System.out.println("CREAR LOG");
        ////System.out.println("REGISTROS INSERTADOS " + ControlRegistros + "RegistrosNoInsertados " + RegistrosNoInsertados);
        try{
            LogExcel hilo = new LogExcel();
            
            //System.out.println("CONTROL DE REGISTROS ");
            
            hilo.star(ListaIncosistencias, ControlRegistros, RegistrosNoInsertados, nu);
        } catch(Exception e){ ////System.out.println("ERROR: Creacion LOGEXCEL"+e.getMessage());
        }
    }
}



