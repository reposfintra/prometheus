/*
 * HMigracionRemesasAnuladas.java
 *
 * Created on 16 de julio de 2005, 04:37 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import java.util.Vector;
import java.io.*;

/**
 *
 * @author  
 */
public class HMigracionRemesasOTPadre extends Thread{
    private Vector remesasOT;
    
    private FileWriter fw;
    private BufferedWriter bffw;
    private PrintWriter pntw;
    
    private String path;
    private String usuario;
    
    
    /** Creates a new instance of HMigracionRemesasAnuladas */
    public HMigracionRemesasOTPadre() {
    }
    
    public void start(Vector remesasOT, String u) {
        this.remesasOT = remesasOT;
        this.usuario = u;
        
        try{
            
            //sandrameg 190905
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            path = rb.getString("ruta");
            
            String nombre_archivo = "";
            String ruta = path + "/exportar/migracion/" + usuario + "/";
            String fecha_actual = Util.getFechaActual_String(6);            
            //ruta = ruta + fecha_actual.substring(0,4) + fecha_actual.substring(5,7);
            nombre_archivo = ruta + "otpadre " + fecha_actual.replace(':','_').replace('/','_') + ".csv";            
            File archivo = new File(ruta.trim());                
            archivo.mkdirs();
            FileOutputStream file = new FileOutputStream(nombre_archivo.trim());
            
            fw = new FileWriter(nombre_archivo);
            bffw = new BufferedWriter(fw);
            pntw = new PrintWriter(bffw);
        }
        catch(java.io.IOException e){
            ////System.out.println(e.toString());
        }
        
        super.start();
    }
    
    public synchronized void run(){
       this.writeFile();
    }
    
    protected void writeFile() {    	
    	String linea = "";          
    	    	
        for(int i=0; i<remesasOT.size(); i++){
            Vector datos = (Vector) remesasOT.elementAt(i);
            linea = datos.elementAt(0) + "," + datos.elementAt(1) + "," +
                    datos.elementAt(2) + "," + datos.elementAt(3);
            pntw.println(linea);
        }
        pntw.close();        	    	    	
    }    

}
