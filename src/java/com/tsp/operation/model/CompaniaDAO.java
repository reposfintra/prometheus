/*
 * CompaniaDAO.java
 *
 * Created on 20 de enero de 2005, 04:21 PM
 */

package com.tsp.operation.model;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.util.Util;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  KREALES
 */
public class CompaniaDAO extends MainDAO {
    
    private List compa�ias;
    private Compania cia;
    
    /** Creates a new instance of CompaniaDAO */
    public CompaniaDAO() {
        super("CompaniaDAO.xml");
    }
    public CompaniaDAO(String dataBaseName) {
        super("CompaniaDAO.xml", dataBaseName);
    }
    
    public List getCompa�ias(){
        return this.compa�ias;
    }
    public Compania getCia(){
        return this.cia;
    }
    public void setCia(Compania cia){
        this.cia=cia;
    }
    
    public void listar() throws SQLException{
        compa�ias=null;
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select * from cia where reg_status=''");
                rs = st.executeQuery();
                
                compa�ias = new LinkedList();
                
                while(rs.next()){
                    cia= new Compania();
                    cia.setcodigo_empresa(rs.getString("codigo_empresa"));
                    cia.setcodigo_regional(rs.getString("codigo_regional"));
                    cia.setdefault_project(rs.getString("default_project"));
                    cia.setdescription(rs.getString("description"));
                    cia.setdstrct(rs.getString("dstrct"));
                    cia.setfecha_ven_poliza(rs.getString("fecha_ven_poliza"));
                    cia.setnit(rs.getString("nit"));
                    cia.setnombre_aseguradora(rs.getString("nombre_aseguradora"));
                    cia.setpoliza_aseguradora(rs.getString("poliza_aseguradora"));
                    cia.setresolucion(rs.getString("resolucion"));
                    cia.setuser_update(rs.getString("user_update"));
                    compa�ias.add(cia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA LISTA DE COMPA�IAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void buscar(String distrito)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        cia=null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select * from cia where dstrct=? and reg_status=''");
                st.setString(1,distrito);
                rs = st.executeQuery();
                
                if(rs.next()){
                    cia= new Compania();
                    cia.setcodigo_empresa(rs.getString("codigo_empresa"));
                    cia.setcodigo_regional(rs.getString("codigo_regional"));
                    cia.setdefault_project(rs.getString("default_project"));
                    cia.setdescription(rs.getString("description"));
                    cia.setdstrct(rs.getString("dstrct"));
                    cia.setfecha_ven_poliza(rs.getString("fecha_ven_poliza"));
                    cia.setnit(rs.getString("nit"));
                    cia.setnombre_aseguradora(rs.getString("nombre_aseguradora"));
                    cia.setpoliza_aseguradora(rs.getString("poliza_aseguradora"));
                    cia.setresolucion(rs.getString("resolucion"));
                    cia.setuser_update(rs.getString("user_update"));
                    cia.setpeso_lleno_max(rs.getFloat("peso_lleno_max"));
                    cia.sethora(rs.getString("hora_minera"));
                    cia.setMoneda(rs.getString("moneda"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DE LA COMPA�IA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
                con.close();
            }
        }
    }
    
    
    public void insert()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into cia (codigo_empresa,codigo_regional,default_project,description,dstrct,fecha_ven_poliza,nit,nombre_aseguradora,poliza_aseguradora,resolucion,user_update,peso_lleno_max,anticipo_max,hora_minera) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                st.setString(1, cia.getcodigo_empresa() );
                st.setString(2, cia.getcodigo_regional() );
                st.setString(3, cia.getdefault_project());
                st.setString(4, cia.getdescription());
                st.setString(5, cia.getdstrct());
                st.setString(6, cia.getfecha_ven_poliza());
                st.setString(7, cia.getnit());
                st.setString(8, cia.getnombre_aseguradora());
                st.setString(9, cia.getpoliza_aseguradora());
                st.setString(10, cia.getresolucion() );
                st.setString(11, cia.getuser_update());
                st.setFloat(12, cia.getpeso_lleno_max());
                st.setFloat(13, cia.getanticipo_max());
                st.setString(14, cia.gethora());
                ////System.out.println(st.toString());
                st.executeUpdate();
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE COMPA�IAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void update()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update cia set codigo_empresa=? ,codigo_regional=? ,default_project=? ,description=?,fecha_ven_poliza=?,nit=?,nombre_aseguradora=?,poliza_aseguradora=?,resolucion=?, user_update=?,peso_lleno_max=?,anticipo_max=?,hora_minera=? where dstrct=?");
                st.setString(1,cia.getcodigo_empresa() );
                st.setString(2,cia.getcodigo_regional() );
                st.setString(3, cia.getdefault_project());
                st.setString(4, cia.getdescription());
                st.setString(5, cia.getfecha_ven_poliza());
                st.setString(6, cia.getnit());
                st.setString(7, cia.getnombre_aseguradora());
                st.setString(8, cia.getpoliza_aseguradora());
                st.setString(9, cia.getresolucion() );
                st.setString(10, cia.getuser_update());
                st.setFloat(11, cia.getpeso_lleno_max());
                st.setFloat(12, cia.getanticipo_max());
                st.setString(13, cia.gethora());
                st.setString(14, cia.getdstrct());
                st.executeUpdate();
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL UPDATE DE COMPA�IAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    //Henry 05.11.05
     public void anularCompa�ia()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update cia set reg_status='A' where dstrct=?");                
                st.setString(1, cia.getdstrct());
                st.executeUpdate();
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL UPDATE DE COMPA�IAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public void anular()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update cia set reg_status='A' where dstrct=?");
                st.setString(1, cia.getdstrct());
                st.executeUpdate();
                
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE COMPA�IAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    // LOS SIGUIENTES M�TODOS Y/O ATRIBUTOS FUERON COPIADOS DE LA APLICACI�N SOT
    // PARA IMPLEMENTAR EL MODULO DE ADMINISTRACI�N DE USUARIOS.
    // Alejandro Payares - nov 18 de 2005 - 11:10 am
    
    /**
     *QUERY PARA LISTAR LOS DISTRITO
     */
    private static final String SQL_CIA =
    "SELECT dstrct FROM cia";
    
    private TreeMap cbxCia;
    private TreeMap bases;
    
    
    public TreeMap getCbxCia(){
        return this.cbxCia;
    }
    
    public void setCbxCia(TreeMap cias){
        this.cbxCia = cias;
    }
    
    public void listarDistritos() throws SQLException{
        PreparedStatement sttm = null;
        ResultSet rs = null;
        cbxCia = null;
        cbxCia = new TreeMap();
        try{
            sttm = this.crearPreparedStatement("SQL_CIA");
            rs = sttm.executeQuery();
            while (rs.next()) {
                cbxCia.put(rs.getString(1), rs.getString(1));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LOS DISTRITOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (sttm != null){ sttm.close(); }
            this.desconectar("SQL_CIA");
        }
    }
    
    public void listarBases() throws SQLException{
        PreparedStatement sttm = null;
        ResultSet rs = null;
        bases = new TreeMap();
        try{
            sttm = this.crearPreparedStatement("SQL_BASES");
            rs = sttm.executeQuery();
            while (rs.next()) {
                bases.put(rs.getString(1), rs.getString(1));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LAS BASES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (sttm != null){ sttm.close(); }
            this.desconectar("SQL_BASES");
        }
    }
    
    public TreeMap getBases(){
        return this.bases;
    }
    
     public float maxValorMercancia(String dstrct) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        float valor=0;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select lte_vlr_mercancia from cia where dstrct = ? and reg_status=''");
                st.setString(1, dstrct);
                rs = st.executeQuery();
                if(rs.next()){
                    valor = rs.getFloat("lte_vlr_mercancia");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL LIMITE DEL VALOR DE LA MERCANCIA  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return valor;
    }
    
}
