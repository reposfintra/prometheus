/*
 * veh_alquilerDAO.java
 *
 * Created on 4 de septiembre de 2005, 10:06 AM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
/**
 *
 * @author  Jm
 */
public class veh_alquilerDAO {
    private static String SQLINSERT =   " INSERT INTO VEH_ALQUILER ( DSTRCT, PLACA, STD_JOB_NO, CREATION_USER ) "+
    " VALUES ( ?, ?, ?, ? )    ";
    
    
    private static String SQLDELETE =   " DELETE FROM VEH_ALQUILER WHERE PLACA = ? AND STD_JOB_NO = ? ";
    
    private static String SQLUPDATE =   " UPDATE VEH_ALQUILER SET REG_STATUS = ?, USER_UPDATE = ?  WHERE PLACA = ? AND STD_JOB_NO = ? ";
    
    private static String SQLBUSCAR =   " SELECT PLACA, STD_JOB_NO, REG_STATUS FROM VEH_ALQUILER WHERE PLACA = ? AND STD_JOB_NO = ? ";
    
    private static String SQLLISTAR =   " SELECT VA.PLACA, VA.STD_JOB_NO, VA.REG_STATUS, ST.STD_JOB_DESC FROM VEH_ALQUILER VA LEFT OUTER JOIN STDJOB ST ON ( VA.STD_JOB_NO = ST.STD_JOB_NO ) ";
    
    private static String SQLMODIFCAR = " UPDATE VEH_ALQUILER SET PLACA = ?, STD_JOB_NO = ?,  USER_UPDATE = ? WHERE PLACA = ? AND STD_JOB_NO = ? ";
    
    /** Creates a new instance of veh_alquilerDAO */
    public veh_alquilerDAO() {
    }
    
    /*INSERT */
    public void INSERT_VA(String distrito, String placa, String std_job_no, String usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLINSERT);
            st.setString(1, distrito);
            st.setString(2, placa);
            st.setString(3, std_job_no);
            st.setString(4, usuario);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_VA [veh_alquilerDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /* UPDATE */
    public void UPDATE_VA(String reg_status, String placa, String std_job_no, String usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLUPDATE);
            st.setString(1, reg_status);
            st.setString(2, usuario);
            st.setString(3, placa);
            st.setString(4, std_job_no);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATE_VA [veh_alquilerDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /*BUSCAR*/
    public veh_alquiler SEARCH_VA(String placa, String std_job_no ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        veh_alquiler datos = null;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLBUSCAR);
            st.setString(1, placa);
            st.setString(2, std_job_no);
            rs = st.executeQuery();
            while(rs.next()){
                datos = veh_alquiler.load(rs);
                break;
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina SEARCH_VA [veh_alquilerDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return datos;
    }
    
    public boolean BUSCAR_VA(String placa, String std_job_no ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag         = false;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLBUSCAR);
            st.setString(1, placa);
            st.setString(2, std_job_no);
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina BUSCAR_VA [veh_alquilerDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return flag;
    }
    
    public void DELETE_VA(String placa, String std_job_no ) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLDELETE);
            st.setString(1,  placa);
            st.setString(2, std_job_no);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina DELETE_VA [veh_alquilerDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    public List LIST() throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLLISTAR);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(veh_alquiler.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LIST [veh_alquilerDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return lista;
    }
    
    /* MODIFICAR */
    public void MODIFICAR_VA(String placa, String std_job_no, String usuario, String nplaca, String nstd_job_no) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLMODIFCAR);
            st.setString(1, placa);
            st.setString(2, std_job_no);
            st.setString(3, usuario);
            st.setString(4, nplaca);
            st.setString(5, nstd_job_no);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina MODIFICAR_VA [veh_alquilerDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
}
