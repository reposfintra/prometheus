/*
 * EgresoService.java
 *
 * Created on 11 de enero de 2005, 09:29 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class EgresoService {
    
    EgresoDAO egreso;
    
    
    TreeMap listaTipoEgresos;
    TreeMap listaTipoConceptos;
    
    /** Creates a new instance of EgresoService */
    public EgresoService() {
        egreso = new EgresoDAO();
    }
    public EgresoService(String dataBaseName) {
        egreso = new EgresoDAO(dataBaseName);
    }
    
    public Egreso getEgreso(){
        return egreso.getEgreso();
    }
    
    public Vector getEgresos(){
        return egreso.getEgresos();
    }
    
    public List getEgresosDet(){
        return egreso.getEgresosDet();
    }
    public void consultarEgreso (String distrito,String cuenta,String banco,String planilla,String nit,String nombre,String fechaini,String fechafin )throws SQLException{
       egreso.consultaEgreso(distrito, cuenta, banco, planilla, nit, nombre, fechaini, fechafin);
    }
    
    public void buscarEgreso (String doc_no )throws SQLException{
       egreso.buscarEgreso(doc_no);
    }
    public void buscarEgresoDet (String doc_no )throws SQLException{
       egreso.buscarEgresosDet(doc_no);
    }
    public String insertEgresoxAnticipo()throws SQLException{
        return egreso.insertEgresoxAnticipo();
    }
    public String insertItemxAnticipo()throws SQLException{
        return egreso.insertItemxAnticipo();
    }
    public void setCheque(com.tsp.operation.model.beans.ImpresionCheque cheque) {
        egreso.setCheque(cheque);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    ///MARIO 201005    
    // Registro de cheques entregados
    
    /**
     * Elemento para guardar el listado de cheques
     * @autor ............. Mario Fontalvo
     */
    private List listaCheques;
    
    /**
     * Procedimiento para buscar los cheques segun los el filtro definido
     * por el usuario, el tipo indica si son cheques entregados, enviados
     * o recibidos
     *
     * @autor ............. Mario Fontalvo
     * @Fecha ..............20051020
     * @see ............... com.tsp.operation.model.egresoService.BuscarCheques
     * @param Tipo ........ Indica el Tipo de Busqqueda
     * @param Agencia ..... Filtrar resultados por este parametro
     * @param Banco ....... Filtrar resultados por este parametro
     * @param Sucursal .... Filtrar resultados por este parametro
     * @throws Exception .
     */
    public void BuscarCheques(int Tipo, String Agencia, String Banco, String Sucursal ) throws Exception{
        try{
            listaCheques = egreso.BuscarCheques( Tipo,  new String[] { Banco, Sucursal, Agencia } ) ;
        }catch (Exception e){
            throw new Exception("Error en la rutina BuscarCheques [MigracionService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Procedimiento para actulizar la fecha de envia, entregadoso recibidos
     * segun sea el tipo
     *
     * @autor ............. Mario Fontalvo
     * @Fecha ..............20051020
     * @see ............... com.tsp.operation.model.egresoService.ActualizarFechasCheques
     * @param Distrito .... Distrito del cheque
     * @param Documento ... Cheque
     * @param Usuario   ... Usuario en session
     * @param Fecha ....... fecha a asignar
     * @param Tipo ........ Tipo de Modificacion
     * @param Banco ....... Banco del Cheque
     * @param Sucursal .... Sucursal del cheque
     * @throws Exception .
     */
    public void ActualizarFechasCheques(int Tipo, String Distrito, String Banco, String Sucursal, String Documento, String Usuario, String Fecha ) throws Exception{
        try{
            String [] params = {  Usuario, Fecha, "#FECHA#", Distrito, Banco, Sucursal, Documento};
            egreso.ActualizarFechasCheques( Tipo, params ) ;
        }catch (Exception e){
            throw new Exception("Error en la rutina ActualizarFechasCheques [MigracionService]...\n"+e.getMessage());
        }
    }

    /**
     * Metodo para obtener el listado de Cheques
     * @return Listado
     */
    
    public List getListaCheques(){
        return listaCheques;
    }
    
    /*     
     * Descripcion :    Funcion que permite obtener todos los egresos
     * @autor :         David Pi�a Lopez               
     * @version :       1.0
     */
    public void buscarEgresos() throws Exception{
        egreso.buscarEgresos();
    }
    /*     
     * Descripcion :    Funcion que obtiene la cabecera de un egreso
     * @autor :         David Pi�a Lopez               
     * @version :       1.0
     */
    public void obtenerCabeceraEgreso() throws Exception{
        egreso.obtenerCabeceraEgreso();
    }
    /*     
     * Descripcion :    Funcion que obtiene el detalle de un egreso
     * @autor :         David Pi�a Lopez               
     * @version :       1.0
     */
    public void obtenerDetalleEgreso() throws Exception{
        egreso.obtenerDetalleEgreso();
    }
    public void setEgreso ( Egreso eg )throws SQLException{
       egreso.setEgreso( eg );
    }
    /*     
     * Descripcion :    Funcion que permite obtener todos los egresos
     * @autor :         David Pi�a Lopez               
     * @version :       1.0
     */
    public void buscarEgresosCab( String fecini, String fecfin , String usuario, String tipo) throws Exception{
        egreso.buscarEgresosCab( fecini, fecfin, usuario, tipo ); 
    }
    /**
     * Funcion que permite obtener un lista de cheques a partir de un banco, una sucursal y un rango de fechas     
     *
     * @autor ............. Ing. Juan M. Escand�n P.
     * @Fecha ..............2007-02-15
     * @see ............... com.tsp.operation.model.egresoService.buscarChequesRangoFechas
     * @param distrito ..... Filtrar resultados por este parametro
     * @param banco .......  Filtrar resultados por este parametro
     * @param sucursal ....  Filtrar resultados por este parametro
     * @param rango_ini .... Filtrar resultados por este parametro
     * @param rango_fin .... Filtrar resultados por este parametro
     * @throws Exception .
     */
    public void buscarChequesRangoFechas(String distrito, String banco, String sucursal, String rango_ini, String rango_fin ) throws Exception{
        try{
            listaCheques = egreso.buscarChequesRangoFechas( new String[] { distrito, banco, sucursal, rango_ini, rango_fin } ) ;
        }catch (Exception e){
            throw new Exception("Error en la rutina buscarChequesRangoFechas [EgresoService]...\n"+e.getMessage());
        }
    }


    public void reiniciarListaChk(){
        listaCheques = null;
    }

    /**
     * Getter for property listaTipoEgresos.
     * @return Value of property listaTipoEgresos.
     */
    public java.util.TreeMap getListaTipoEgresos() {
        return listaTipoEgresos;
    }
    
    /**
     * Setter for property listaTipoEgresos.
     * @param listaTipoEgresos New value of property listaTipoEgresos.
     */
    public void setListaTipoEgresos(java.util.TreeMap listaTipoEgresos) {
        this.listaTipoEgresos = listaTipoEgresos;
    }
    
    /**
     * Getter for property listaTipoConceptos.
     * @return Value of property listaTipoConceptos.
     */
    public java.util.TreeMap getListaTipoConceptos() {
        return listaTipoConceptos;
    }

        public void setListaTipoConceptos(java.util.TreeMap listaTipoConceptos) {
        this.listaTipoConceptos = listaTipoConceptos;
    }
        /**
     * Setter for property listaTipoConceptos.
     * @param listaTipoConceptos New value of property listaTipoConceptos.
     */
    public double getVlr(String neg) throws Exception{
        return egreso.getVlr(neg) ;
    }

}
