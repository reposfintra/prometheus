/*
 * MovtraficoDAO.java
 *
 * Created on 18 de junio de 2005, 08:27 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  Jm
 */
public class MovtraficoDAO {
    Movtrafico mv;
    Vector movimientos;
    /** Creates a new instance of MovtraficoDAO */
    public MovtraficoDAO() {
    }
    
    /**
     * Getter for property movimientos.
     * @return Value of property movimientos.
     */
    public java.util.Vector getMovimientos() {
        return movimientos;
    }
    
    /**
     * Setter for property movimientos.
     * @param movimientos New value of property movimientos.
     */
    public void setMovimientos(java.util.Vector movimientos) {
        this.movimientos = movimientos;
    }
    
    /**
     * Getter for property mv.
     * @return Value of property mv.
     */
    public Movtrafico getMv() {
        return mv;
    }
    
    /**
     * Setter for property mv.
     * @param mv New value of property mv.
     */
    public void setMv(Movtrafico mv) {
        this.mv = mv;
    }
    //Lista de los Movimientos de Entrega tipomtr = 'E'
    public void listMovEntrega( String pla )throws SQLException{
        String consulta = "select cia, tipomtr, planilla, feinftr, hoinftr from trafimo where tipomtr ='E' and planilla = ?";
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, pla);
                rs= st.executeQuery();
                while(rs.next()){
                    mv = new Movtrafico();
                    mv.setDistrito(rs.getString("cia"));
                    mv.setTipomov(rs.getString("tipomtr"));
                    mv.setNumpla(rs.getString("planilla"));
                    mv.setFechaentrega(rs.getString("feinftr"));
                    mv.setHoraentrega(rs.getString("hoinftr"));
                    
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LOS MOVIMIENTOS DE ENTREGA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    //Lista los movimietos de trafico diferentes a movimientos de entrega
    
    public void listMov(String p)throws SQLException{
        String consulta = "select cia, tipomtr, planilla, feinftr, hoinftr from trafimo where tipomtr <> 'E' and tipomtr <> '' and planilla = ?";
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, p);
                rs= st.executeQuery();
                movimientos = new Vector();
                while(rs.next()){
                    mv = new Movtrafico();
                    mv.setDistrito(rs.getString("cia"));
                    mv.setTipomov(rs.getString("tipomtr"));
                    mv.setNumpla(rs.getString("planilla"));
                    mv.setFechaentrega(rs.getString("feinftr"));
                    mv.setHoraentrega(rs.getString("hoinftr"));
                    
                    movimientos.add(mv);
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LOS MOVIMIENTOS DE TRAFICO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
}
