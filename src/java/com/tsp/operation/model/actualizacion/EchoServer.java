/*
 * EchoServer.java
 *
 * Created on 14 de octubre de 2005, 11:30
 */

/**
 *
 * @author  DRIGO
 */

package com.tsp.operation.model.actualizacion;
import java.io.*;
import java.net.*;

public class EchoServer
{  
    static private final int DEFAULT_PORT = 8189;
    static private final String SIGN_OFF_TOKEN = "BYE";

    public static BufferedReader getBufferedReader(Socket socket) throws IOException {
	InputStream inputStream = socket.getInputStream();
	InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	return bufferedReader;
    }

    public static void main(String[] args )
    {  
	ServerSocket serverSocket = null;
	Socket clientSocket = null;
	try {  
	    ////System.out.println("Creatin ServerSocket on " + DEFAULT_PORT);
	    serverSocket = new ServerSocket(DEFAULT_PORT);
	    while(true) {
		////System.out.println("Looking for a client. CQ, CQ, CQ!");
		clientSocket = serverSocket.accept(); // blocks until client connects
		////System.out.println("Client connected");
		BufferedReader in = getBufferedReader(clientSocket);
		OutputStream outputStream = clientSocket.getOutputStream();
		PrintWriter out = new PrintWriter(outputStream, true);
	  
		boolean done = false;
		while (!done) {  
		    String line = in.readLine();
		    if (line == null) {
			done = true;
		    } else {  
			////System.out.println(line); // Prints on server side
			ActualizacionThread act = new ActualizacionThread();
                        act.start(line);
		    }
		}
		continue; // Go looking for new client
	    }
	} catch (Exception e) {  
	    ////System.out.println("socket "+e);
	} catch (Throwable t) {
	    ////System.out.println("Caught throwable t: " + t);
	}
	finally {
	    if(serverSocket != null) {
		////System.out.println("Finally clause");
		try {
		    ////System.out.println("Cleaning up, closing sockets");
		    serverSocket.close();
		    if(clientSocket != null) {
			clientSocket.close();
		    }
		} catch (IOException eio) { }
	    }
	}
    }
}
