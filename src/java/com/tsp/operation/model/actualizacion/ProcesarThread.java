/*
 * ProcesarThread.java
 *
 * Created on 19 de octubre de 2005, 03:13 PM
 */

/**
 *
 * @author  R.SALAZAR
 */
package com.tsp.operation.model.actualizacion;
import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;

public class ProcesarThread extends Thread {
    String comandos;
    /** Creates a new instance of ProcesarThread */
    public void start(){
        super.start();
    }
    public synchronized void run(){
        try{
            ActualizacionDao actdao = new ActualizacionDao();
            actdao.ejecutar();
            
        }catch(Exception e){
            ////System.out.println("ACTTHREAD "+e.getMessage());
        }finally{
            
        }
    }
}