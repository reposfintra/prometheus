/*
 * TransaccionService.java
 *
 * Created on 21 de enero de 2005, 04:23 PM
 */
package com.tsp.operation.model.actualizacion;

import java.io.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author  KREALES
 */
public class TransaccionService {
    
    TransaccionDAO t;
    /** Creates a new instance of TransaccionService */
    public TransaccionService(String dataBaseName) {
        t = new TransaccionDAO(dataBaseName);
    }
    
    public Statement getSt(){
        return t.getSt();
    }
    public void crearStatement()throws SQLException{
        t.crearStatement();
    }
     public void execute()throws SQLException{
         t.execute();
     }
    
}
