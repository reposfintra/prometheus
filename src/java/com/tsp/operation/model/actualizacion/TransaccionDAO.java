/*
 * Transaccion.java
 *
 * Created on 21 de enero de 2005, 04:08 PM
 */

package com.tsp.operation.model.actualizacion;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  KREALES
 */

public class TransaccionDAO {
    
    /** Creates a new instance of Transaccion */
    
    
    Connection con=null;
    Statement st = null;
    ResultSet rs = null;
    PoolManager poolManager = null;
    String dataBaseName;
    
    public TransaccionDAO(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }
    public Statement getSt(){
        return st;
    }
    public void crearStatement()throws SQLException{
        poolManager = PoolManager.getInstance();
        con = poolManager.getConnection(this.dataBaseName);
        st = con.createStatement();
    }
    
    public void execute()throws SQLException{
        try{
            
            if(st!=null){
                boolean autocommit = con.getAutoCommit();
                con.setAutoCommit(false);
                st.toString();
                st.executeBatch();
                con.commit();
                con.setAutoCommit(autocommit);
            }
            
        }
        catch(SQLException e){
            con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
            
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
        
    }
    
    
}
