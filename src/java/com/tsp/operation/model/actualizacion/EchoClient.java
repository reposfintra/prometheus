package com.tsp.operation.model.actualizacion;

import java.io.*;
import java.net.*;

public class EchoClient {
    public void mandar(String mensaje) throws IOException {
        Socket echoSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        ////System.out.println("mandar");
        try {
            echoSocket = new Socket("equipo10.tsp.com", 8189);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                                        echoSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: taranis.");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for "
                               + "the connection to: taranis.");
            System.exit(1);
        }

	BufferedReader stdIn = new BufferedReader(
                                   new InputStreamReader(System.in));
	
	out.println(mensaje);
	out.close();
	in.close();
	stdIn.close();
	echoSocket.close();
    }
}