/*
 * ActualizacionDao.java
 *
 * Created on 17 de octubre de 2005, 9:30
 */

package com.tsp.operation.model.actualizacion;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.util.connectionpool.*;

//import com.vcc.util.connectionpool.PoolManager;

/**
 *
 * @author  DRIGO
 */
public class ActualizacionDao {
    
    /** Creates a new instance of ActualizacionDao */
    public ActualizacionDao() {
    }
    public String comandos() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        String s="";
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select comando from control_actualizaciones where fecha_procesado = '0099-01-01 00:00:00'" );
                rs = st.executeQuery();
                
                while (rs.next()){
                    s += rs.getString(1)+";";
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR VERIFICAR SI EXISTE CONTRATO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        } 
        ////System.out.println("comandos:"+s);
        return s;
    }
    
    public void pendientes(String comandos) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        String[] s= comandos.split(";");
        
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                
                for(int i = 0 ; i<s.length ; i++){
                    st = con.prepareStatement("insert into pendiente_actualizar values('',?,'','now()','now()')" );
                    st.setString(1,s[i]);
                    st.executeUpdate();
                }
                try{
                    //CREACION DEL ARCHIVO PARA QUE EL PROGRAMA TOME LOS DATOS.
                    ////System.out.println("creando archivo");
                    File file = new File("c:/actualizaciones/");
                    file.mkdirs();
                    String NombreArchivo = "c:/actualizaciones/PENDIENTE_POR_EJECUTAR.txt";
                    PrintStream archivo = new PrintStream(NombreArchivo);
                    archivo.close();
                    ////System.out.println(" archivo creado");
                    
                }catch(IOException ex){
                    ////System.out.println("Error creando el archivo "+ex.getMessage());
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR PENDIENTES" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        } 
        ////System.out.println("comandos:"+s);
        
        
    }
    
    
    public void ejecutar() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PreparedStatement st2 = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        String comando;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("Select comando,OID from pendiente_actualizar where error = ''" );
                rs = st.executeQuery();
                while(rs.next()){
                    comando = rs.getString("comando");
                    try{
                        st2 = con.prepareStatement(comando);
                        st2.executeUpdate();
                        st2 = con.prepareStatement("delete from pendiente_actualizar where oid = '"+rs.getString("OID")+"'");
                        ////System.out.println("borrando"+st2);
                        st2.executeUpdate();
                    }
                    catch(SQLException e){
                        st2 = con.prepareStatement("update pendiente_actualizar set error ='"+e.getMessage()+"' where oid = '"+rs.getString("OID")+"'");
                        ////System.out.println("borrando"+st2);
                        st2.executeUpdate();
                        throw new SQLException("ERROR AL EJECUTAR" + e.getMessage() ); 
                    }
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EJECUTAR" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        } 
        
        
    }
    
    public void borrarPendientes() throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("delete from control_actualizaciones where fecha_procesado = '0099-01-01 00:00:00'" );
                st.executeUpdate();
                
                
            }
        }catch(SQLException e){
             throw new SQLException("ERROR " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        } 
        
    }
    
}
