/*
 * ActualizacionThread.java
 *
 * Created on 18 de octubre de 2005, 10:02 AM
 */

/**
 *
 * @author  R.SALAZAR
 */
package com.tsp.operation.model.actualizacion;
import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;

public class ActualizacionThread extends Thread {
    String comandos;
    /** Creates a new instance of ActualizacionThread */
    public ActualizacionThread() {
    }
    
    public void start(String com){
        comandos = com;
        super.start();
    }
    public synchronized void run(){
        try{
            ActualizacionDao actdao = new ActualizacionDao();
            actdao.pendientes(comandos);
            
        }catch(Exception e){
            ////System.out.println("ACTTHREAD "+e.getMessage());
        }finally{
            //super.destroy();
        }
    }
}