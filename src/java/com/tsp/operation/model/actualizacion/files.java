/*
 * files.java
 *
 * Created on 15 de octubre de 2005, 9:05
 */

/**
 *
 * @author  DRIGO
 */
package com.tsp.operation.model.actualizacion;

import java.util.*;
import java.io.*;
import java.sql.*;
public class files {
    
    /** Creates a new instance of files */
    public files() {
    }
    public static void main(String[] args){
        File f1 = new File("c:/actualizaciones/PENDIENTE_POR_ENVIAR.txt");
        File f2 = new File("c:/actualizaciones/PENDIENTE_POR_EJECUTAR.txt");
        ActualizacionDao actdao = new ActualizacionDao();
        String comandos;
        if(f1.exists()){
            ////System.out.println("file true");
            try{
                EchoClient ec = new EchoClient();
                comandos=actdao.comandos();
                ec.mandar(comandos);
                f1.delete();
                actdao.borrarPendientes();
            }
            catch (Exception e  ){

            }    
        }
        if(f2.exists()){
            ProcesarThread proc = new ProcesarThread();
            proc.start();
            f2.delete();
        }        
    }
}
