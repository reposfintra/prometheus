
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;


public class MigracionConductorPropietarioDAO {
   
    
    
   // private static final String  BASE = "spo";
    
    
   //____________________________________________________________________________________________  
   //                                          ATRIBUTOS 
   //____________________________________________________________________________________________    
   
     private static final String SEARCH_810  = " SELECT  employee_id  FROM  msf810  WHERE  employee_id =? ";
     private static final String SEARCH_760  = " SELECT  employee_id  FROM  msf760  WHERE  employee_id =? ";
    
     private static final String SEARCH_PROPIETARIO = "SELECT  PROPIETARIO FROM PLACA WHERE PLACA= ?";
     
     private static final String SEARCH_CEDULA_PROPIETARIO=" SELECT                                   "+
                                                           "       C.nitpro    AS  cedula_propietario "+
                                                           " FROM                                     "+
                                                           "       remesa    A,                       "+
                                                           "       plarem    B,                       "+        
                                                           "       planilla  C                        "+
                                                           " WHERE                                    "+
                                                           "      A.remision  = ?                     "+
                                                           " and  B.numrem    = A.numrem              "+
                                                           " and  B.base      = A.base                "+
                                                           " and  C.numpla    = B.numpla              "+
                                                           " and  C.base      = B.base       " +
                                                           " and  a.reg_status !='A'         ";
      
    private static final String SEARCH_200="SELECT * FROM MSF200  WHERE  SUPPLIER_NO= ? "; 
     
    
     
   //____________________________________________________________________________________________  
   //                                          METODOS 
   //____________________________________________________________________________________________    
   
    
    public MigracionConductorPropietarioDAO() {}
    
    
    
    boolean exist200(String cedula)throws SQLException{
      PoolManager poolManager = PoolManager.getInstance();
      Connection conOracle    = poolManager.getConnection("oracle");
      if (conOracle == null)
          throw new SQLException("Sin conexion");
      PreparedStatement st=null;
      ResultSet rs=null;
      boolean estado=false;
      try{
            st=conOracle.prepareStatement(SEARCH_200);
            st.setString(1, cedula);
            rs=st.executeQuery(); 
            int cont=0;
            if(rs!=null){
              while(rs.next())
                cont++;
            }
            if(cont>0)
                estado=true;
      }
      catch(SQLException e){
          throw new SQLException("No se pudo buscar la cedula "+ cedula +" en MSF200  -->" + e.getMessage());
      }  
      finally{
         if(st!=null) st.close();
         if(rs!=null) rs.close();
         poolManager.freeConnection("oracle",conOracle);
      } 
     return estado;  
    }
    
    boolean exist810(String cedula)throws SQLException{
      PoolManager poolManager = PoolManager.getInstance();
      Connection conOracle = poolManager.getConnection("oracle");
      if (conOracle == null)
          throw new SQLException("Sin conexion");
      PreparedStatement st=null;
      ResultSet rs=null;
      boolean estado=false;
      try{
            st=conOracle.prepareStatement(SEARCH_810);
            st.setString(1, cedula);
            rs=st.executeQuery(); 
            int cont=0;
            if(rs!=null){
              while(rs.next())
                cont++;
            }
            if(cont>0)
                estado=true;
      }
      catch(SQLException e){
          throw new SQLException("No se pudo buscar la cedula "+ cedula +" en MSF810  -->" + e.getMessage());
      }  
      finally{
         if(st!=null) st.close();
         if(rs!=null) rs.close();
         poolManager.freeConnection("oracle",conOracle);
      } 
     return estado;  
    }
        
   
   
   
  boolean exist760(String cedula) throws SQLException{
      PoolManager poolManager = PoolManager.getInstance();
      Connection conOracle = poolManager.getConnection("oracle");
      if (conOracle == null)
          throw new SQLException("Sin conexion");
      PreparedStatement st=null;
      ResultSet rs=null;
      boolean estado=false;
      try{
            st=conOracle.prepareStatement(SEARCH_760);
            st.setString(1, cedula);
            rs=st.executeQuery(); 
            int cont=0;
            if(rs!=null){
              while(rs.next())
                 cont++;
            }
            if(cont>0)
                estado=true;
      }
      catch(SQLException e){
          throw new SQLException("No se pudo buscar la cedula "+ cedula +" en MSF760  -->" + e.getMessage());
      }  
      finally{
         if(st!=null) st.close();
         if(rs!=null) rs.close();
         poolManager.freeConnection("oracle",conOracle);
      } 
     return estado; 
}
    
 
  String searchPropietario(String placa) throws SQLException{
      PoolManager poolManager = PoolManager.getInstance();
      Connection con = poolManager.getConnection("fintra");
      if (con == null)
          throw new SQLException("Sin conexion");
      PreparedStatement st=null;
      ResultSet rs=null;
      String id="";
      try{
            st=con.prepareStatement(SEARCH_PROPIETARIO);
            st.setString(1, placa);
            rs=st.executeQuery();
            if(rs!=null){
               while(rs.next()){
                 id = rs.getString(1);
                 break;
               }
            }
      }
      catch(SQLException e){
          throw new SQLException("No se pudo buscar el propietario de la placa "+ placa);
      }  
      finally{
         if(st!=null) st.close();
         if(rs!=null) rs.close();
         poolManager.freeConnection("fintra",con);
      } 
      return id;
}
 
  
  
String searchCCPropietario(String remision) throws SQLException{
      PoolManager poolManager = PoolManager.getInstance();
      Connection con = poolManager.getConnection("fintra");
      if (con == null)
          throw new SQLException("Sin conexion");
      PreparedStatement st=null;
      ResultSet rs=null;
      String id="";
      try{
            st=con.prepareStatement(SEARCH_CEDULA_PROPIETARIO);
            st.setString(1, remision);
            rs=st.executeQuery();
            if(rs!=null){
               while(rs.next()){
                 id = rs.getString(1);
                 break;
               }
            }
      }
      catch(SQLException e){
          throw new SQLException("No se pudo buscar la cedula del propietario de la remision "+ remision);
      }  
      finally{
         if(st!=null) st.close();
         if(rs!=null) rs.close();
         poolManager.freeConnection("fintra",con);
      } 
      return id;
}  
  
  
   
    
}//FIN DAO
