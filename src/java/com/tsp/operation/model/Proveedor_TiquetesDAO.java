package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;



public class Proveedor_TiquetesDAO{
    
    Proveedor_TiquetesDAO(){
        
    }
    
    private List proveedorTIQUETES;
    private Proveedor_Tiquetes pt;
    private Vector proveedores;
    private static final String SELECT_CONSULTA ="select p.dstrct," +
    "       c.nomciu," +
    "       p.nit," +
    "       p.sucursal, " +
    "       n.nombre " +
    "from   proveedor_tiquetes p," +
    "       ciudad c, " +
    "       nit n " +
    "where  c.codciu = p.city_code and " +
    "       n.cedula = p.nit       and" +
    "       p.nit like ?         and" +
    "       n.nombre like ?     and" +
    "       c.nomciu  like ?     and" +
    "       p.reg_status = ''";
    
    public List getProveedorTIQUETES(){
        return proveedorTIQUETES;
    }
    public Vector getProveedores(){
        
        return proveedores;
    }
    public void setProveedor(Proveedor_Tiquetes pt){
        this.pt = pt;
    }
    public boolean existProveedor(String nit, String sucursal)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from proveedor_tiquetes where nit = ? and reg_status='' and sucursal = ? ");
                st.setString(1,nit);
                st.setString(2,sucursal);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROVEEDOR  DE ANTICIPO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    public void consultar(String ciudad, String nombre, String nit )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        proveedores= null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SELECT_CONSULTA);
                st.setString(1,nit+"%");
                st.setString(2,nombre+"%");
                st.setString(3,ciudad+"%");
                
                
                rs = st.executeQuery();
                proveedores = new Vector();
                
                while(rs.next()){
                    pt = new Proveedor_Tiquetes();
                    pt.setDstrct(rs.getString("dstrct"));
                    pt.setCiudad(rs.getString("nomciu"));
                    pt.setNit(rs.getString("nit"));
                    pt.setNombre(rs.getString("nombre"));
                    pt.setCodigo(rs.getString("sucursal"));
                    proveedores.add(pt);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROOVEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    void searchProveedorTIQUETES(String dstrct )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        proveedorTIQUETES = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select p.*, n.nombre, c.nomciu as ciudad from proveedor_tiquetes p, nit n, ciudad c where p.nit=n.cedula and c.codciu=p.city_code and p.dstrct= ? and p.reg_status=''");
                st.setString(1,dstrct);
                
                rs = st.executeQuery();
                proveedorTIQUETES = new LinkedList();
                
                while(rs.next()){
                    proveedorTIQUETES.add(Proveedor_Tiquetes.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROVEEDOR DE TIQUETES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    boolean existProveedorTiquetes(String dstrct )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from proveedor_tiquetes where dstrct = ? ");
                st.setString(1,dstrct);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROVEEDOR" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    public void searchProveedor(String nit, String sucursal )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        pt=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select p.*, n.nombre, c.nomciu as ciudad from proveedor_tiquetes p, nit n, ciudad c where p.nit=n.cedula and c.codciu=p.city_code and p.nit = ? and p.sucursal=? and p.reg_status=''");
                st.setString(1, nit);
                st.setString(2, sucursal);
                rs = st.executeQuery();
                
                if(rs.next()){
                    pt = Proveedor_Tiquetes.load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROVEEDOR DE TIQUETES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public Proveedor_Tiquetes getProveedor() throws SQLException{
        return pt;
    }
    
    public void anularProveedor()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update proveedor_tiquetes set reg_status='A' where nit=? and sucursal=? ");
                st.setString(1,pt.getNit());
                st.setString(2,pt.getCodigo());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL PROVEEDOR TIQUETES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    
    public void updateProveedor()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update proveedor_tiquetes set city_code=?, user_update=?, dstrct=? where nit=? and sucursal=?");
                st.setString(1,pt.getCity_code());
                st.setString(2, pt.getCreation_user());
                st.setString(3,pt.getDstrct());
                st.setString(4,pt.getNit());
                st.setString(5,pt.getCodigo());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL PROVEEDOR TIQUETES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    public void insertProveedor(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into proveedor_tiquetes (dstrct,city_code,nit,creation_date,creation_user,codigo_migracion,porcentaje,sucursal,base) values(?,?,?,'now()',?,?,?,?,?)");
                st.setString(1, pt.getDstrct());
                st.setString(2,pt.getCity_code());
                st.setString(3, pt.getNit());
                st.setString(4,pt.getCreation_user());
                st.setString(5,pt.getCod_Migracion());
                st.setFloat(6, pt.getPorcentaje());
                st.setString(7, pt.getCodigo());
                st.setString(8, base);
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS PROVEEDOR_TIQUETES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    /*Diogenes
     *retorna true si existe anulado el tiquete del proveedor 
     */
    public boolean existProveedorAnulado(String nit, String sucursal)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from proveedor_tiquetes where nit = ? and reg_status='A' and sucursal = ? ");
                st.setString(1,nit);
                st.setString(2,sucursal);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PROVEEDOR  DE ANTICIPO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    /*Activa el proveedor*/
    public void activarProveedor()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update proveedor_tiquetes set city_code=?, user_update=?, dstrct=?, reg_status='' where nit=? and sucursal=?");
                st.setString(1,pt.getCity_code());
                st.setString(2, pt.getCreation_user());
                st.setString(3,pt.getDstrct());
                st.setString(4,pt.getNit());
                st.setString(5,pt.getCodigo());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTIVACION DEL PROVEEDOR TIQUETES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
}





