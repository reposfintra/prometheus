/*
 * PlanillaauxService.java
 *
 * Created on 22 de noviembre de 2004, 02:06 PM
 */

package com.tsp.operation.model;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class PlaauxService {
    
    private PlaauxDAO planilla;
    /** Creates a new instance of PlanillaauxService */
    public PlaauxService() {
        planilla = new PlaauxDAO();
    }
    
    public String  insertPlaaux(Plaaux plaaux, String base)throws SQLException{
        try{
            planilla.setPlaAux(plaaux);
            return planilla.insertPlaAux(base);
            
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String updatePlaaux(Plaaux plaaux)throws SQLException{
        try{
            planilla.setPlaAux(plaaux);
            return planilla.updatePlaAux();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
     public String anularPlanilla(Plaaux pla)throws SQLException{
        try{
            planilla.setPlaAux(pla);
            return planilla.anularPlaaux();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
}
