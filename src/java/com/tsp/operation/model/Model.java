package com.tsp.operation.model;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;

public class Model {
    public transient FacturasCorficolombianaService facturascorficolombianaSvc;

    public transient ElectricaribeOtService ElectricaribeOtSvc;

    public transient ConsorcioService consorcioService;

    public transient ElectricaribeOfertaService ElectricaribeOfertaSvc;//091103

    public transient CxpsApplusService cxpsApplusService;

    public transient EstadoFinancieroService estadoFinancieroService;

    public transient ProcesarRecaudosFenalcoService procesarRecaudosFenalcoService;

    public transient ElectricaribeVerService electricaribeVerSvc;

    public transient DatosEcaService datosEcaService;

    //Ivan Herazo: Servicio para Gestion-Ejecucion de Consultas
    public transient AvalesService avalesService;

    public transient ConsultaService consultaService;

    public transient SerieGeneralService serieGeneralService;

    public transient  RegistroTiempoService RegistroTiempoSvc;
    public transient  PlacaService placaService;
    public transient ConductorService conductorService;
    public transient Proveedor_AcpmService proveedoracpmService;
    public transient Proveedor_AnticipoService proveedoranticipoService;
    public transient Proveedor_TiquetesService proveedortiquetesService;
    public transient SeriesService seriesService;
    public transient StdjobcostoService stdjobcostoService;
    public transient StdjobdetselService stdjobdetselService;
    public transient TramoService tramoService;
    public transient PlanillaService planillaService;
    public transient TbltiempoService tbltiempoService;
    public transient UsuarioService usuarioService;
    public transient PlaauxService plaauxService;
    public transient RemesaService remesaService;
    public transient RemPlaService remplaService;
    public transient Planilla_TiempoService pla_tiempoService;
    public transient PlkgruService plkgruService;
    public transient StdjobplkcostoService stdjobplkcostoService;
    public transient MovPlaService movplaService;
    public transient CiudadService ciudadService;
    public transient SdelayService sdelayService;
    public transient PeajeService peajeService;
    public transient ColorService colorService;
    public transient MarcaService marcaService;
    public transient CarroceriaService carrService;
    public transient AgenciaService agenciaService;
    public transient RemesaDestService rdService;
    public transient RemiDestService remidestService;
    public transient SeriesService  SeriesSvc;
    public transient PlanillasService PlanillasSvc;
    public transient RemisionOCService RemisionOCSvc;
    public transient RemisionService RemisionSvc;
    public transient EsquemaChequeService  EsquemaChequeSvc;
    public transient RemesaService RemesaSvc;
    public transient MonedaService monedaSvc;
    public transient ImpresionChequeService ImprimirChequeSvc;
    public transient RMCantidadEnLetras Convercion;
//    public transient MigracionConductorPropietarioService  MigracionConductorSvc;
    public transient EgresoService egresoService;
    public transient PlanillaImpresionService PlanillaImpresionSvc;
    public transient CompaniaService ciaService;
    public transient TransaccionService tService;
    public transient TransaccionService2 tService2;
    public transient TiempoService tiempoService;
    public transient LogServidorService logservice;
    public transient Usuario usuario;
    public transient AnticiposService anticiposService;
    public transient ProveedoresService proveedoresService;
    public transient NitService nitService ;
    public transient UnidadService unidadService ;
    public transient BancoUsuarioService buService;
    public transient AutorizacionFleteService afleteService;
    public transient SerieService servicioSerie;
    public transient BancoService servicioBanco;

    //Sandrameg
    public transient Conciliacion conciliacion;
    public transient PropietarioService propService;
    public transient Auto_AnticipoService autoAnticipoService;
    public transient ReporteOCVacioAnticipoService reporteOCVacioAnticipoService;

    //AMendez 20050708
    public transient PlanViajeService planViajeService;
    public transient CaravanaService caravanaService;
    public transient ViaService viaService;
    public transient HReporteService hReporteService;

    //Alejandro
    public transient StdjobService servicioStdjob;
    public transient RegistroAsignacionServices regAsigService;
    public transient ReporteInformacionAlClienteService reporteInfoClienteService;
    public transient ReporteUbicacionVehicularService reporteUbicacionVehService;
    public transient GrupoEquipoService grupoEquipoService;
    public transient TablaGenManagerService tablaGenService;
    public transient ReporteDevolucionesService reporteDevolucionesService;
    public transient MovimientoAduanaService movAduanaService;
    public transient ReporteTiemposDeViajeConductoresService repTiempoDeViajeConductores;
    public transient ReporteTiemposPuestosDeControlService repTiemposPCService;
    public transient ConfigurarReporteClientesService confReportService;
    public transient ConsultasGeneralesDeReportesService consultasGrlesReportesService;
    public transient GruposReporteService gruposReporteService;
    public transient ReporteOCAnticipoService reporteOcAnt;


    //Planeacion
    public transient StdjobServices stdjobServices;
    public transient ReqClienteServices reqclienteServices;
    public transient InformePredoService ipredoSvc;
    public transient RecursosdispService rdSvc;
    public transient MovtraficoService  mvSvc;
    public transient PlanillaService plSvc;
    public transient TramoService trSvc;
    public transient TiporecService tpSvc;
    public transient PlacaService vehSvc;
    public transient PlaremService plremSvc;
    public transient RemesaService rSvc;
    //public transient BDOracleService bdSvc;

    //Diogenes
    public transient LogProcesosService LogProcesosSvc;
    public transient VerificacionDocService veridocService;
    public transient CausaDiscrepanciaService causadiscreService;
    public transient MigracionDiscrepanciaService migradiscreService;
    public transient ActividadPlanService actplanService;
    public transient TiempoViajeCarbonService tiempoviajecarbonService;
    public transient ControlactAduaneraService controlactAduaneraService;
    public transient EMailService emailService;
    public transient ConductorService ConductorSvc;
    public transient UsuarioAprobacionService usuaprobacionService;
    public transient RetroactivoService retroactivoService;
    public transient IngresoService ingresoService;
    public transient PlanllegadacargueService planllegadacargue;
    public transient FormularioService formularioService;


    //Jose
    public transient DiscrepanciaService discrepanciaService;
    public transient SancionService sancionService;
    public transient ProductoService productoService;
    public transient Tipo_sancionService tipo_sancionService;
    public transient ResponsableService responsableService;
    public transient Cumplidos_DocumentosService cumplidos_DocumentosService;
    public transient Flota_directaService flota_directaService;
    public transient Acuerdo_especialService acuerdo_especialService;
    public transient Ingreso_especialService ingreso_especialService;
    public transient Descuento_claseService descuento_claseService;
    public transient EquipoPropioService equipoPropioService;
    public transient DescuentoService descuentoService;
    public transient Concepto_equipoService concepto_equipoService;
    public transient Cliente_ubicacionService cliente_ubicacionService;
    public transient JustificacionService justificacionService;
    public transient ReporteGeneralService reporteGeneralService;
    public transient Ingreso_detalleService ingreso_detalleService;
    public transient Esquema_formatoService esquema_formatoService;
    public transient Formato_tablaService formato_tablaService;
    public transient EquivalenciaCargaService equivalenciaCargaService;



    //TRAFICO
    public transient ActividadService actividadSvc;
    public transient CiudadService ciudadservice;
    public transient ClienteActividadService clientactSvc;
    public transient Codigo_demoraService codigo_demoraService;
    public transient Codigo_discrepanciaService codigo_discrepanciaService;
    public transient ContactoService contactoService;
    public transient com.tsp.operation.model.services.CaravanaService caravanaSVC;
    public transient DemorasService demorasSvc;
    public transient EscoltaVehiculoService escoltaVehiculoSVC;
    public transient EstadoService estadoservice;
    public transient IdentidadService identidadService;
    public transient InfoActividadService infoactService;
    public transient Jerarquia_tiempoService jerarquia_tiempoService;
    public transient MenuService menuService;
    public transient NovedadService novedadService;
    public transient PaisService paisservice;
    public transient PerfilService perfilService;
    public transient RutaService rutaService;
    public transient TasaService tasaService;
    public transient TipoDocumentoService tdocumentoService;
    public transient Tipo_contactoService tipo_contactoService;
    public transient Tipo_ubicacionService tipo_ubicacionService;
    public transient Tramo_TicketService tiketService;
    public transient UbicacionService ubService;
    public transient UnidadTrfService unidadTrfService;
    public transient ZonaService zonaService;
    public transient ZonaUsuarioService zonaUsuarioService;
    public transient ActividadTrfService actividadTrfService;
    public transient ResponsableActTrfService responsableActTrfService;
    public transient RepMovTraficoService rmtService;
    public transient campos_jspService camposjsp;
    public transient PerfilVistaUsuarioService perfilvistausService;
    public transient JspService jspService;
    public transient Perfil_vistaService perfil_vistaService;
    public transient AsignacionUsuariosClientesService AsigUsuCliSvc;
    public transient TraficoService traficoService;

    //Fernell
    public transient LiquidarOCService LiqOCSvc;
    public transient DirectorioService DirectorioSvc;
    public transient ImagenService  ImagenSvc;
    public transient ProyectoService ProyectoSvc;
    public transient OpService OpSvc;
   // public transient MigrarOP266Service MigrarOP266Svc;
    public transient PlanDeViajeService PlanViajeSvc;
    public transient ExtractosService ExtractosSvc;
    public transient ChequeXFacturaService ChequeXFacturaSvc;
    public transient AdminSoftwareService  AdminSoft;
    public transient RevisionSoftwareService RevisionSoft;
    public transient ConsultaSoftwareService  ConsultaSoft;
    public transient ArchivoMovimientoService      ArchivoMovimientoSvc;
    public transient LiberarFacturasXCorridasService  LiberarFacturasSvc;
    public transient ChequeFacturasCorridaService  ChequeFactXCorridasSvc;
    public transient TransferenciaCorridaService   TransferenciaSvc;
    public transient  ReportePlanillasViaServices  ReportePlanillasViaSvc;
    public transient AnticiposPagosTercerosServices  AnticiposPagosTercerosSvc;
    public transient CuentaBancoService              CuentaBancoSvc;
    public transient ReporteContableAnticiposServices   ReporteContableAnticiposSvc;
    public transient ConsultaAnticiposTercerosServices  ConsultaAnticiposTercerosSvc;
    public transient StatusBarraServices  StatusBarraSvc;
    public transient FormatoServices      FormatoSvc;
    public transient  AdicionOPServices   AdicionOPSvc;

    //Henry
    public transient StdjobService stdjobService;
    public transient SJExtrafleteService sjextrafleteService;
    public transient CodextrafleteService codextrafleteService;
    public transient MonedaService monedaService;
    public transient MigracionPlanillaAnuladaAnticipoService migracionPlanillaAnuladaService;
    public transient MigracionRemesasOTPadreService migracionOTPadreService;
    public transient MigracionRemesasModificacionOTService migracionOTsModifService;
//    public transient RptNumeroMovimientosService rptNumeroMovSVC;
    public transient ClienteService clienteService;
    public transient CXPDocService cxpDocService;
    public transient CXPItemDocService cxpItemDocService;
    public transient CXPObservacionItemService cxpObservacionItemService;
    public transient ReferenciaService referenciaService;
    public transient ProveedorEscoltaService proveedorEscoltaService;
    public transient MigracionProveedorEscoltaService migracionProveedorEsc;
    public transient FlotaUtilizadaService flotaUtilizadaService;
    public transient CorridaService corridaService;
    public transient HojaVidaService hojaVidaService;
    public transient ExtractoService extractoService;

    //Tito Andr�s
    public transient MigracionRemesasAnuladasService mraSvc;
    public transient MigrarOTFitDefService migFitDefSvc;
    public transient ProveedorService proveedorService;
    public transient TblEstadoService tbl_estadoSvc;
    public transient DocumentoService documentoSvc;
    public transient DocumentoActividadService actividad_documentoSvc;
    public transient AplicacionService appSvc;
    public transient DocumentoAplicacionService apl_docSvc;
    public transient ConceptoService otros_conceptosSvc;
    public transient ReporteRetroactivoService repRetroactivosSvc;
    public transient ReporteNitPlacaUpdatesService repNitPlacasUpSvc;
    public transient PrecintosService precintosSvc;
    public transient WGrpStdjobService wgroup_stdjobSvc;
//    public transient PosBancariaService posbancariaSvc;
    public transient EstadViajesService estadViajesSvc;
    public transient RepUtilidadService rep_utilidadSvc;
    public transient IndicadoresService indicadoresSvc;
    public transient RXPDocService factrecurrService;
    public transient RXPImpDocService factrecurrImpService;

    //Ju@n M
    public transient veh_externosService VExternosSvc;
    public transient veh_alquilerService VAlquilerSvc;
    public transient ReporteEgresoService ReporteEgresoSvc;
    public transient remesa_doctoService RemDocSvc;
    public transient Tipo_proveedorService TproveedorSvc;
    public transient Tipo_impuestoService TimpuestoSvc;
    public transient Stdjob_tbldocService stdjob_tbldocService;
    public transient ReporteViajeService ReporteViajeSvc;
    public transient RemesaSinDocService RemesaSinDocSvc;
    public transient AnulacionPlanillaService AnulacionPlanillaSvc;
    public transient FlotaService FlotaSvc;
//    public transient ReportesService  ReportesSvc;
    //public transient DistribucionPresupuestoService DPtoSvc;
    public transient HojaControlViajeService hojaControlViajeSvc;
    public transient RemesaImpresionService remesaImpresionSvc;
    public transient ReporteVehPerdidosService reporteVehPerdidosSvc;
    public transient Wgroup_PlacaService wgroup_placaSvc;
    public transient BalancePruebaService balancePrueba ;
    public transient Caja_lecService cajaLectoraSvc;
    public transient RegistroTarjetaService registroTarjetaSvc;

    public transient SerieDocAnuladoService docAnuladoSvc;


    //Rodrigo
    public transient CumplidoService cumplidoService;
    public transient Cumplido_docService cumplido_docService;

    //David
    public transient AdminHistoricoService adminH;
    public transient DespachoManualService  despachoManualService;
    public transient RetrasoSalidaService retrasoSService;
    public transient CXPImpDocService  cxpImpDocService ;
    public transient TurnosService turnosService;
    public transient ConsultaUsuarioService consultaUsuarioService;
    public transient TablasUsuarioService tablasUsuarioService;
    public transient ImpoExpoService impoExpoService;


    //Parody
    public transient ReportesCumplidosService reportecumplidosService;
    public transient TLBActividadesService tLBActividadesServices;
    public transient CXPDocService cXP_DocService;
    public transient ObjetosService objetosservice;
    public transient ReportePlacaFotoService reporteplacanitfotoService;
    public transient ReporteMovTraficoCerromatosoService reporteMovTraficoCerromatosoService;
    public transient RelacionGrupoSubgrupoPlacaService relaciongruposubgrupoplaca;
    public transient CuentaValidaService cuentavalidaservice;

    //Mario
    public transient ImportacionService ImportacionSvc;
    public transient SoporteClientesService  SoporteClienteSvc;
    public transient TblConceptosService ConceptosSvc;
    public transient SJCostoService          SJCostoSvc;
    public transient PlanillaCostosService   PlanillaCostosSvc;
    public transient ImportacionCXPService   ImportacionCXPSvc;
    public transient ReportePPPService reportePPPSvc;



    //KAREN
    public transient Causas_AnulacionService causas_anulacionService;
    public transient Tipo_Recuperacion_AnticipoService trecuperacionaService;
    public transient TiempoViajeService TiempoViajeSvc;
    public transient Instrucciones_DespachoService instrucciones_despachoService;


    //Ivan
    public transient ReporteDrummondService ReporteDrummondSvc;
    public transient DescuentoTercmService DescuentoTercmSvc;
    public transient UbicacionVehicularClientesService UbicacionVehicularClientesSvc;
    public transient UbicacionVehicularPropietarioService UbicaVehicuPorpSvc;
    public transient ConciliacionService ConciliacionSvc;
    public transient RepInfoOtMimPosSvc repInfoOtMimPosSvc;
    public transient ReportePODService ReportePODSvc;
    public transient ReporteDevolucionesDiscrepService reporteDevolucionesDiscrepService;
    public transient ReporteBancoTransferenciaService ReporteBTSvc;
    public transient ReversarViajeTraficoService ReversarViajeTraficoSvc;
    public transient StandarService StandarSvc;
    //Armando
    public transient DescuentoEquipoService descuentoequiposvc;
    public transient TblGeneralService tblgensvc;
    public transient TblGeneralDatoService tblgendatosvc;
    public transient VehNoRetornadoService vehnrsvc;
    public transient InventarioSoftwareReporteService repinvsvc;
    public transient ConceptoPagoService ConceptoPagosvc;
    public transient SendMailService sendMailService;

    //Ricardo Rosero
    public transient TblGeneralProgService tbl_GeneralProgService;
    public transient FitmenService fitmenService;
    public transient EstCargaPlacaService ecpService;
    public transient ReporteDevolucionService repdevService;

    public transient DespachoService despachoService;

    //kreales
    public transient CitaCargueService citacargueService;

    //Lreales
    public transient ConvertirPlanillaManualService convertirPlaManService;
    public transient ReporteFacturaDestinatarioService reporteFacDesService;
    public transient PublicacionService publicacionService;
    public transient ImprimirOrdenDeCargaService imprimirOrdenService;
    public transient ReporteRemesasPorFacturarService remesasPorFacturarService;
    public transient PlanillaManualPlanViajeService planillaManualPlanViajeService;
    public transient ReporteProveedoresFintraService reporteProveedoresFintraService;
    public transient ReporteDemorasService reporteDemorasService;
    public transient ReporteFacturasClientesService reporteFacturasClientesService;
    public transient ConsultarModificarFacturaClienteService consultarModificarFacturaClienteService;
    public transient ReporteCarbonService reporteCarbonService;

    //Prestamos
    public transient IngresoPrestamoService  IngPrestamoSvc;
    public transient PrestamoService         PrestamoSvc;
    public transient CuotasService           CuotasSvc;
    public transient ResumenPrestamoService  ResumenPrtSvc;

    //amartinez
    public transient FacturaService facturaService;
    public transient DescuentoTercmWebService DescuentoTercmWebSvc;
    public transient PorcentajeService PorcentajeSvc;
    public transient ComparacionPlanillasService comparacionplanillasservice;
    public transient MigracionFacturasClientesService  mfcs;
    public transient HImportacionCFService himportacioncfservice;

    //jbarros
    public transient ExtractoPPService ExtractoPPSvc;
    public transient CaptacionesFintraService CaptacionesFintraSvc;

    public transient FenalcoFintraService FenalcoFintraSvc;


    //ffernandez
    public transient PlanllegadacargueService planllegadacargueService;
    public transient PlanillaSinCumplidoService planillaSinCumplidoService;
    public transient Control_excepcionService control_excepcionService;

    public transient AdicionAjustesPlanillasServices AdicionAjustesPlanillasSvc;
    public transient TrasladoService trasladoService;

    public transient PerfilesGenService PerfilesGenService;
    public transient MantenimientoFestivosService mantenimientoFestivosService;

    public transient ConsultaExtractoService ConsultaExtractoSvc;
    public transient FacturaProveedorAnticipoService facturaProveedorAnticipoService;
    public transient ReimpresionService ReimpresionSvc;
    public transient PrechequeService prechequeSvc;

    //Luigi
    public transient VetoService vetoSvc;
    public transient TransitoService transitoService;

    public transient ImportacionCXCService   ImportacionCXCSvc;

     public transient Banco_PropietarioService banco_PropietarioService;

     public transient CambioDestinoService CambioDestinoSvc;

     //Ing Roberto Rocha
    public transient ConsultarAvalesService Avales ;
    public transient NegociosGenService Negociossvc ;

    public transient creacionCompraCarteraService creacionCompraCarteraSvc;

    public transient PdfImprimirService PdfImprimirSvc;
    public transient FacturaObservacionService FactObsvc ;

    public transient AnticipoGasolinaService AnticiposGasolinaService;

    public transient MensajeSistemaService mensajeSistemaService;

    public transient PreanticipoService preanticipoService;

    public transient modificacionComprobanteService modificacionComprobantService;


        //jemartinez
    public transient ProvisionFenalcoService provisionFenalcoService;

    public transient InteresesFenalcoService interesesFenalcoService;

    public transient ProcesoCorficolombianaService procesoCorficolombianaService;

    public transient ArchivosCorficolombianaService archivosCorficolombianaService;

    public transient NegociosApplusService negociosApplusService;
    public transient ApplusService applusService;

    public transient ImporteTextoService importeTextoService ;
     public transient SiniestrosService FenalcoService ;

    //Abapon
    public transient ProntoPagoService prontoPagoService;
    public transient CorficolombianaService corficolombianaSvc;
    public transient ConstanteService constanteService;
    public transient FiduciaService fiduciaService;

    public transient GestionConveniosService gestionConveniosSvc;
    public transient SectorSubsectorService sectorsubsectorSvc;

    public transient Smservice Smservice;//jpinedo
     public transient CaptacionInversionistaService captacionInversionistaService;
     
     
     
      public transient PdfImprimirPagareService pdfImprimirPagareService;//Ing. Jose Avila - 24/07/2012
      public transient PdfImprimirFacturaAval pdfImprimirFacturaAval; //Ing. Jose Avila - 27/07/2012
      
      public transient GestionSolicitudAvalService gsaserv;

    /** Creates a new instance of Model */
    public  Model() {
        try{
            corficolombianaSvc  =   new CorficolombianaService();
            //Ivan Herazo: Servicio para Gestion-Ejecucion de Consultas
            consultaService     =   new ConsultaService();

            buService           =   new BancoUsuarioService();
            unidadService       =   new UnidadService();
            nitService          =   new NitService();
            proveedoresService  =   new ProveedoresService();
            anticiposService    =   new AnticiposService();
            logservice          =   new LogServidorService();
            tiempoService       =   new TiempoService();
            tService            =   new TransaccionService();
            tService2           =   new TransaccionService2();

            ciaService          =   new CompaniaService();
            egresoService       =   new EgresoService();
            monedaSvc           =   new MonedaService();
            EsquemaChequeSvc    =   new EsquemaChequeService();
            remidestService     =   new RemiDestService();
            rdService           =   new RemesaDestService();
            RegistroTiempoSvc   =   new RegistroTiempoService();
            colorService        =   new ColorService();
            marcaService        =   new MarcaService();
            carrService         =   new CarroceriaService();
            agenciaService      =   new AgenciaService();
            peajeService        =   new PeajeService();
            sdelayService       =   new SdelayService();
            ciudadService       =   new CiudadService();
            movplaService       =   new MovPlaService();
            stdjobplkcostoService = new StdjobplkcostoService();
            plkgruService       =   new PlkgruService();
            pla_tiempoService   =   new Planilla_TiempoService();
            remplaService       =   new RemPlaService();
            remesaService       =   new RemesaService();
            plaauxService       =   new PlaauxService();
            usuarioService      =   new UsuarioService();
            tbltiempoService    =   new TbltiempoService();
            planillaService     =   new PlanillaService();
       //     placaService        =   new PlacaService();
            conductorService    =   new ConductorService();
            proveedoracpmService        =   new Proveedor_AcpmService();
            proveedoranticipoService    =   new Proveedor_AnticipoService();
            proveedortiquetesService    =   new Proveedor_TiquetesService();
            seriesService       =   new SeriesService();
            stdjobcostoService  =   new StdjobcostoService();
            stdjobdetselService =   new StdjobdetselService();
            tramoService        =   new TramoService();
            RemisionOCSvc         =   new RemisionOCService();
            PlanillasSvc        =   new PlanillasService();
            SeriesSvc           =   new SeriesService();
            RemesaSvc           =   new RemesaService();
            EsquemaChequeSvc    =   new EsquemaChequeService();
            ImprimirChequeSvc   =   new ImpresionChequeService();
            Convercion          =   new RMCantidadEnLetras();
     //       MigracionConductorSvc   =   new MigracionConductorPropietarioService();
            PlanillaImpresionSvc    =   new PlanillaImpresionService();
            RemisionSvc  = new RemisionService();
            usuario = new Usuario();
            afleteService=new AutorizacionFleteService();

            //TRAFICO
            ubService = new UbicacionService();
            tiketService = new Tramo_TicketService();
            rutaService = new RutaService();
            ciudadservice      =   new CiudadService();
            paisservice         =   new PaisService();
            estadoservice       =   new EstadoService();
            perfilService = new PerfilService();
            menuService = new MenuService();
            tipo_ubicacionService = new Tipo_ubicacionService();
            codigo_demoraService = new Codigo_demoraService();
            codigo_discrepanciaService = new Codigo_discrepanciaService();
            contactoService = new ContactoService();
            zonaService = new ZonaService();
            tdocumentoService = new TipoDocumentoService();
            tipo_contactoService = new Tipo_contactoService();
            unidadTrfService = new UnidadTrfService();
            identidadService = new IdentidadService();
            novedadService = new NovedadService();
            zonaUsuarioService = new ZonaUsuarioService();
            demorasSvc = new  DemorasService();
            caravanaSVC = new com.tsp.operation.model.services.CaravanaService();
            escoltaVehiculoSVC = new EscoltaVehiculoService();
            actividadSvc = new ActividadService();
            zonaService = new ZonaService();
            tasaService = new TasaService();
            clientactSvc = new ClienteActividadService();
            infoactService = new InfoActividadService();
            jerarquia_tiempoService = new Jerarquia_tiempoService();
            actividadTrfService = new ActividadTrfService();
            responsableActTrfService = new ResponsableActTrfService();
            rmtService = new RepMovTraficoService();
            camposjsp = new campos_jspService();
            perfilvistausService = new PerfilVistaUsuarioService();
            jspService = new JspService();
            perfil_vistaService = new Perfil_vistaService();
            AsigUsuCliSvc    = new AsignacionUsuariosClientesService();
            traficoService  = new TraficoService();

            //Sandrameg
            conciliacion = new Conciliacion();
            propService = new PropietarioService();
            autoAnticipoService = new Auto_AnticipoService();
            servicioSerie = new SerieService();
            servicioBanco = new BancoService();
            clienteService = new ClienteService();

            //AMendez 20050708
            planViajeService = new PlanViajeService();
            caravanaService = new CaravanaService();
            viaService = new ViaService();
            hReporteService = new HReporteService();

            //Alejandro
            servicioStdjob= new StdjobService();
            regAsigService = new RegistroAsignacionServices();
            reporteInfoClienteService = new ReporteInformacionAlClienteService();
            reporteUbicacionVehService = new ReporteUbicacionVehicularService();
            grupoEquipoService = new GrupoEquipoService();
            tablaGenService = new TablaGenManagerService();
            reporteDevolucionesService = new ReporteDevolucionesService();
            movAduanaService = new MovimientoAduanaService();
            repTiempoDeViajeConductores = new ReporteTiemposDeViajeConductoresService();
            repTiemposPCService = new ReporteTiemposPuestosDeControlService();
            confReportService = new ConfigurarReporteClientesService();
            consultasGrlesReportesService = new ConsultasGeneralesDeReportesService();
            gruposReporteService = new GruposReporteService();

            //planeacion
            stdjobServices = new StdjobServices();
            reqclienteServices = new ReqClienteServices();
            ipredoSvc = new InformePredoService();
           // LogProcesosSvc = new LogProcesosService();
            rdSvc = new RecursosdispService();
            mvSvc = new MovtraficoService();
            plSvc = new PlanillaService();
            trSvc = new TramoService();
            tpSvc = new TiporecService();
      //      vehSvc = new PlacaService();
            plremSvc = new PlaremService();
            rSvc = new RemesaService();
      //      bdSvc = new BDOracleService();

            //Rodrigo
            cumplidoService = new CumplidoService();
            cumplido_docService = new Cumplido_docService();

            //Tito Andr�s
            mraSvc = new MigracionRemesasAnuladasService();
            migFitDefSvc = new MigrarOTFitDefService();
            proveedorService = new ProveedorService();
            tbl_estadoSvc = new TblEstadoService();
            actividad_documentoSvc = new DocumentoActividadService();
            documentoSvc = new DocumentoService();
            appSvc = new AplicacionService();
            apl_docSvc = new DocumentoAplicacionService();
            otros_conceptosSvc = new ConceptoService();
            repRetroactivosSvc = new ReporteRetroactivoService();
            repNitPlacasUpSvc = new ReporteNitPlacaUpdatesService();
            precintosSvc = new PrecintosService();
            wgroup_stdjobSvc = new WGrpStdjobService();
    //        posbancariaSvc = new PosBancariaService();
            estadViajesSvc = new EstadViajesService();
            rep_utilidadSvc = new RepUtilidadService();
            indicadoresSvc = new IndicadoresService();
            factrecurrService = new RXPDocService();
            factrecurrImpService = new RXPImpDocService();

            //Jose
            discrepanciaService = new DiscrepanciaService();
            sancionService = new SancionService();
            productoService = new ProductoService();
            tipo_sancionService = new Tipo_sancionService();
            responsableService = new ResponsableService();
            cumplidos_DocumentosService = new Cumplidos_DocumentosService();
            flota_directaService = new Flota_directaService() ;
            acuerdo_especialService = new Acuerdo_especialService();
            ingreso_especialService = new Ingreso_especialService();
            descuento_claseService = new Descuento_claseService();
            descuentoService = new DescuentoService();
            equipoPropioService    = new EquipoPropioService();
            concepto_equipoService = new Concepto_equipoService();
            cliente_ubicacionService = new Cliente_ubicacionService();
            justificacionService = new JustificacionService();
            reporteGeneralService = new ReporteGeneralService();
            ingreso_detalleService = new Ingreso_detalleService();
            esquema_formatoService = new Esquema_formatoService ();
            formato_tablaService = new Formato_tablaService ();
            equivalenciaCargaService = new EquivalenciaCargaService();

            //Fernell
            LiqOCSvc = new LiquidarOCService();
            DirectorioSvc = new DirectorioService();
            ImagenSvc           =  new ImagenService();
            ProyectoSvc = new ProyectoService();
            OpSvc = new OpService();
            PlanViajeSvc = new PlanDeViajeService();
          //  MigrarOP266Svc      =   new MigrarOP266Service();
            ExtractosSvc = new ExtractosService();
            ChequeXFacturaSvc= new ChequeXFacturaService();
            AdminSoft  = new AdminSoftwareService();
            RevisionSoft  = new  RevisionSoftwareService();
            ConsultaSoft      = new  ConsultaSoftwareService();
            ArchivoMovimientoSvc    =  new ArchivoMovimientoService();
            LiberarFacturasSvc      = new LiberarFacturasXCorridasService();
            ChequeFactXCorridasSvc  =  new ChequeFacturasCorridaService();
            TransferenciaSvc        =  new TransferenciaCorridaService();
            ReportePlanillasViaSvc  =  new ReportePlanillasViaServices();
            AnticiposPagosTercerosSvc  =  new AnticiposPagosTercerosServices();
            CuentaBancoSvc             =  new CuentaBancoService();
            ReporteContableAnticiposSvc =  new ReporteContableAnticiposServices();
            ConsultaAnticiposTercerosSvc    =  new ConsultaAnticiposTercerosServices();
            StatusBarraSvc               = new StatusBarraServices();
            FormatoSvc          =   new FormatoServices();
            AdicionOPSvc        =   new  AdicionOPServices();

            //Henry
            stdjobService        =  new StdjobService();
            sjextrafleteService  =  new SJExtrafleteService();
            codextrafleteService =  new CodextrafleteService();
            monedaService        =  new MonedaService();
            migracionOTPadreService         = new MigracionRemesasOTPadreService();
            migracionPlanillaAnuladaService = new MigracionPlanillaAnuladaAnticipoService();
            migracionOTsModifService        = new MigracionRemesasModificacionOTService();
    //        rptNumeroMovSVC    =  new RptNumeroMovimientosService();
            cxpDocService      =  new CXPDocService();
            cxpItemDocService  =  new CXPItemDocService();
            cxpObservacionItemService = new CXPObservacionItemService();
            referenciaService        =  new ReferenciaService();
            proveedorEscoltaService  =  new ProveedorEscoltaService();
            migracionProveedorEsc    =  new MigracionProveedorEscoltaService();
            flotaUtilizadaService    =  new FlotaUtilizadaService();
            corridaService           =  new CorridaService();
            hojaVidaService          =  new HojaVidaService();
            extractoService          =  new ExtractoService();

            //Ju@nM
            VExternosSvc = new veh_externosService();
            VAlquilerSvc = new veh_alquilerService();
            ReporteEgresoSvc = new ReporteEgresoService();
            RemDocSvc = new remesa_doctoService();
            TproveedorSvc = new Tipo_proveedorService();
            TimpuestoSvc = new Tipo_impuestoService();
            stdjob_tbldocService = new Stdjob_tbldocService();
            ReporteViajeSvc = new ReporteViajeService();
            RemesaSinDocSvc = new RemesaSinDocService();
            AnulacionPlanillaSvc = new AnulacionPlanillaService();
            FlotaSvc = new FlotaService();
          //  ReportesSvc      = new ReportesService();
          //  DPtoSvc          = new DistribucionPresupuestoService();
            hojaControlViajeSvc = new HojaControlViajeService();
            remesaImpresionSvc = new RemesaImpresionService();
            reporteVehPerdidosSvc = new ReporteVehPerdidosService();
            wgroup_placaSvc = new Wgroup_PlacaService();
            sendMailService = new SendMailService();
            balancePrueba = new  BalancePruebaService();
            cajaLectoraSvc = new Caja_lecService();
            registroTarjetaSvc = new RegistroTarjetaService();

            //Diogenes
            veridocService = new VerificacionDocService();
            causadiscreService = new CausaDiscrepanciaService();
            migradiscreService = new MigracionDiscrepanciaService();
            actplanService = new ActividadPlanService();
            tiempoviajecarbonService = new TiempoViajeCarbonService();
            controlactAduaneraService = new ControlactAduaneraService();
            emailService = new EMailService();
            ConductorSvc = new ConductorService();
            usuaprobacionService =  new UsuarioAprobacionService();
            retroactivoService   =  new RetroactivoService();
            ingresoService   =  new IngresoService();
            planllegadacargue = new PlanllegadacargueService();
            formularioService = new     FormularioService();

            //David
            adminH = new AdminHistoricoService();
            despachoManualService = new DespachoManualService();
            retrasoSService = new RetrasoSalidaService();
            cxpImpDocService = new CXPImpDocService();
            turnosService = new TurnosService();
            consultaUsuarioService = new ConsultaUsuarioService();
            tablasUsuarioService = new TablasUsuarioService();
            impoExpoService  = new ImpoExpoService();

            //Parody
            reportecumplidosService = new ReportesCumplidosService();
            tLBActividadesServices = new TLBActividadesService();
            objetosservice= new ObjetosService();
            cXP_DocService = new CXPDocService();
            reporteplacanitfotoService = new ReportePlacaFotoService();
            reporteMovTraficoCerromatosoService = new ReporteMovTraficoCerromatosoService();
            relaciongruposubgrupoplaca = new RelacionGrupoSubgrupoPlacaService();
            cuentavalidaservice = new CuentaValidaService();

            //Mario
            ImportacionSvc = new ImportacionService();
            SoporteClienteSvc   = new SoporteClientesService();
            ConceptosSvc        = new TblConceptosService();
            ImportacionCXPSvc   = new ImportacionCXPService();

            SJCostoSvc          = new SJCostoService();
            PlanillaCostosSvc   = new  PlanillaCostosService();
            reportePPPSvc =  new ReportePPPService ();

            //karen
            causas_anulacionService = new Causas_AnulacionService();
            trecuperacionaService   = new Tipo_Recuperacion_AnticipoService();
            TiempoViajeSvc          = new TiempoViajeService();
            instrucciones_despachoService= new Instrucciones_DespachoService();

            //Ivan
            ReporteDrummondSvc    =  new ReporteDrummondService();
            DescuentoTercmSvc     =  new DescuentoTercmService();
            UbicacionVehicularClientesSvc = new UbicacionVehicularClientesService();
            UbicaVehicuPorpSvc    =  new UbicacionVehicularPropietarioService();
            ConciliacionSvc       =  new ConciliacionService();
            repInfoOtMimPosSvc = new RepInfoOtMimPosSvc();
            ReportePODSvc      = new ReportePODService();
            reporteDevolucionesDiscrepService = new ReporteDevolucionesDiscrepService();
            ReporteBTSvc                      = new ReporteBancoTransferenciaService();
            ReversarViajeTraficoSvc  = new ReversarViajeTraficoService();
            StandarSvc               = new StandarService();

            //Armando
            descuentoequiposvc = new DescuentoEquipoService();
            tblgensvc = new TblGeneralService();
            tblgendatosvc = new TblGeneralDatoService();
            vehnrsvc = new VehNoRetornadoService();
            repinvsvc= new InventarioSoftwareReporteService();
            ConceptoPagosvc = new ConceptoPagoService();

            //Ricardo Rosero
            tbl_GeneralProgService = new TblGeneralProgService();
            fitmenService = new FitmenService();
            ecpService = new EstCargaPlacaService();
            repdevService = new ReporteDevolucionService();

            despachoService =new DespachoService();
            citacargueService = new CitaCargueService();

            //Lreales
            convertirPlaManService = new ConvertirPlanillaManualService();
            reporteFacDesService = new ReporteFacturaDestinatarioService();
            publicacionService = new PublicacionService();
            imprimirOrdenService = new ImprimirOrdenDeCargaService();
            remesasPorFacturarService = new ReporteRemesasPorFacturarService();
            planillaManualPlanViajeService = new PlanillaManualPlanViajeService();
            reporteProveedoresFintraService = new ReporteProveedoresFintraService();
            reporteDemorasService = new ReporteDemorasService();
            reporteFacturasClientesService = new ReporteFacturasClientesService();
            consultarModificarFacturaClienteService = new ConsultarModificarFacturaClienteService();
            reporteCarbonService = new ReporteCarbonService();

            //Prestamos
            IngPrestamoSvc = new IngresoPrestamoService();
            PrestamoSvc    = new PrestamoService();
            CuotasSvc      = new CuotasService();
            ResumenPrtSvc  = new ResumenPrestamoService();

            //amartinez
            facturaService = new    FacturaService();
            DescuentoTercmWebSvc  = new DescuentoTercmWebService();
            PorcentajeSvc = new PorcentajeService();
            comparacionplanillasservice = new ComparacionPlanillasService();
            mfcs = new MigracionFacturasClientesService();
            himportacioncfservice  = new HImportacionCFService();

            //Jbarros
//            ExtractoPPSvc = new ExtractoPPService();
            CaptacionesFintraSvc = new CaptacionesFintraService();
            FenalcoFintraSvc = new FenalcoFintraService();

            //ffernandez
            planllegadacargue = new PlanllegadacargueService();
            planillaSinCumplidoService = new PlanillaSinCumplidoService();
            reporteOcAnt = new ReporteOCAnticipoService();
            control_excepcionService = new Control_excepcionService();
            transitoService = new TransitoService();

            AdicionAjustesPlanillasSvc    = new AdicionAjustesPlanillasServices();
            reporteOCVacioAnticipoService =  new ReporteOCVacioAnticipoService();
            trasladoService               =  new TrasladoService();
            PerfilesGenService = new PerfilesGenService();
            mantenimientoFestivosService = new MantenimientoFestivosService();
            ConsultaExtractoSvc = new ConsultaExtractoService();
            facturaProveedorAnticipoService= new FacturaProveedorAnticipoService();
            ReimpresionSvc           = new ReimpresionService();
            prechequeSvc = new PrechequeService();

            ImportacionCXCSvc   = new ImportacionCXCService();

            //luigi
            vetoSvc      = new VetoService();

            docAnuladoSvc = new SerieDocAnuladoService();

            banco_PropietarioService = new Banco_PropietarioService();

            CambioDestinoSvc = new CambioDestinoService();

            //Ing Roberto Rocha
            Avales = new ConsultarAvalesService();
            Negociossvc= new NegociosGenService();

            creacionCompraCarteraSvc= new creacionCompraCarteraService() ;

            PdfImprimirSvc=new PdfImprimirService() ;
            FactObsvc = new FacturaObservacionService ();

            AnticiposGasolinaService = new AnticipoGasolinaService ();

            mensajeSistemaService=new MensajeSistemaService();

            preanticipoService=new PreanticipoService ();

            modificacionComprobantService=new modificacionComprobanteService() ;

            //jemartinez

            provisionFenalcoService = new ProvisionFenalcoService();

            interesesFenalcoService=new InteresesFenalcoService();

           // procesoCorficolombianaService = new ProcesoCorficolombianaService();

            archivosCorficolombianaService=new ArchivosCorficolombianaService();

            negociosApplusService=new NegociosApplusService() ;
            applusService = new ApplusService();

            serieGeneralService  = new SerieGeneralService();

            importeTextoService = new ImporteTextoService()  ;

            // Pronto pagos
            prontoPagoService = new ProntoPagoService();

            avalesService= new AvalesService();

            datosEcaService=new DatosEcaService();

           // electricaribeVerSvc=new ElectricaribeVerService ();

            estadoFinancieroService=new EstadoFinancieroService() ;

            cxpsApplusService=new CxpsApplusService();

            procesarRecaudosFenalcoService= new ProcesarRecaudosFenalcoService();

            ElectricaribeOfertaSvc= new ElectricaribeOfertaService();

            ElectricaribeOtSvc=new ElectricaribeOtService();

            consorcioService=new ConsorcioService();

            facturascorficolombianaSvc=new FacturasCorficolombianaService();

            sectorsubsectorSvc  = new SectorSubsectorService();
            gestionConveniosSvc = new GestionConveniosService();
            constanteService = new ConstanteService();
            fiduciaService = new FiduciaService();

 	    //jpinedo
            Smservice = new Smservice();
            FenalcoService = new SiniestrosService();
            captacionInversionistaService= new CaptacionInversionistaService() ;
            
           //  pdfImprimirPagareService = new PdfImprimirPagareService(); //Ing. Jose Avila - 24/07/2012
             pdfImprimirFacturaAval = new PdfImprimirFacturaAval(); //Ing. Jose Avila - 27/07/2012
             
        }catch(Exception e){
        }

    }
    
    public  Model(String dataBaseName) {
        try {
            consultaService = new ConsultaService(dataBaseName);
            agenciaService = new AgenciaService(dataBaseName);
            tablaGenService = new TablaGenManagerService(dataBaseName);
            paisservice = new PaisService(dataBaseName);
            menuService = new MenuService(dataBaseName);
            tdocumentoService = new TipoDocumentoService(dataBaseName);
            servicioBanco = new BancoService(dataBaseName);
            proveedorService = new ProveedorService(dataBaseName);
            documentoSvc = new DocumentoService(dataBaseName);
            monedaService = new MonedaService(dataBaseName);
            TimpuestoSvc = new Tipo_impuestoService(dataBaseName);
            cxpDocService = new CXPDocService(dataBaseName);
            cXP_DocService = new CXPDocService(dataBaseName);
            tblgensvc = new TblGeneralService(dataBaseName);
            Negociossvc = new NegociosGenService(dataBaseName);
            mensajeSistemaService = new MensajeSistemaService(dataBaseName);
            electricaribeVerSvc = new ElectricaribeVerService(dataBaseName);
            gestionConveniosSvc = new GestionConveniosService(dataBaseName);
            negociosApplusService = new NegociosApplusService(dataBaseName);
            LogProcesosSvc = new LogProcesosService(dataBaseName);
            ConceptoPagosvc = new ConceptoPagoService(dataBaseName);
            tService = new TransaccionService(dataBaseName);
            monedaSvc = new MonedaService(dataBaseName);
            ciaService = new CompaniaService(dataBaseName);
            tasaService = new TasaService(dataBaseName);
            publicacionService = new PublicacionService(dataBaseName);
            applusService = new ApplusService(dataBaseName);
            consorcioService = new ConsorcioService(dataBaseName);
            serieGeneralService = new SerieGeneralService(dataBaseName);
            reporteDemorasService = new ReporteDemorasService(dataBaseName);
            constanteService = new ConstanteService(dataBaseName);
            ImportacionSvc = new ImportacionService(dataBaseName);
            datosEcaService = new DatosEcaService(dataBaseName);
            ingresoService = new IngresoService(dataBaseName);
            fiduciaService = new FiduciaService(dataBaseName);
            reporteFacturasClientesService = new ReporteFacturasClientesService(dataBaseName);
            facturaService = new FacturaService(dataBaseName);
            clienteService = new ClienteService(dataBaseName);
            ciudadService = new CiudadService(dataBaseName);
            ciudadservice = new CiudadService(dataBaseName);
            ChequeFactXCorridasSvc = new ChequeFacturasCorridaService(dataBaseName);
            egresoService = new EgresoService(dataBaseName);
            identidadService = new IdentidadService(dataBaseName);
            vetoSvc = new VetoService(dataBaseName);
            estadoservice = new EstadoService(dataBaseName);
            estadoFinancieroService = new EstadoFinancieroService(dataBaseName);
            ingreso_detalleService = new Ingreso_detalleService(dataBaseName);
            ExtractosSvc = new ExtractosService(dataBaseName);
            corridaService = new CorridaService(dataBaseName);
            LiberarFacturasSvc = new LiberarFacturasXCorridasService(dataBaseName);
            TransferenciaSvc = new TransferenciaCorridaService(dataBaseName);
            AnticiposPagosTercerosSvc = new AnticiposPagosTercerosServices(dataBaseName);
            despachoService = new DespachoService(dataBaseName);
            seriesService = new SeriesService(dataBaseName);
            SeriesSvc = new SeriesService(dataBaseName);
            ChequeXFacturaSvc = new ChequeXFacturaService(dataBaseName);
            cxpObservacionItemService = new CXPObservacionItemService(dataBaseName);
            ArchivoMovimientoSvc = new ArchivoMovimientoService(dataBaseName);
            CuentaBancoSvc = new CuentaBancoService(dataBaseName);
            ReporteContableAnticiposSvc = new ReporteContableAnticiposServices(dataBaseName);
            ConsultaAnticiposTercerosSvc = new ConsultaAnticiposTercerosServices(dataBaseName);
            cxpItemDocService = new CXPItemDocService(dataBaseName);
            extractoService = new ExtractoService(dataBaseName);
            FactObsvc = new FacturaObservacionService(dataBaseName);
            modificacionComprobantService = new modificacionComprobanteService(dataBaseName);
            cxpsApplusService = new CxpsApplusService(dataBaseName);
            ElectricaribeOfertaSvc = new ElectricaribeOfertaService(dataBaseName);
            ElectricaribeOtSvc = new ElectricaribeOtService(dataBaseName);
            servicioSerie = new SerieService(dataBaseName);
            plSvc = new PlanillaService(dataBaseName);
            rSvc = new RemesaService(dataBaseName);
            RemesaSvc = new RemesaService(dataBaseName);
            planillaService = new PlanillaService(dataBaseName);
            remesaService = new RemesaService(dataBaseName);
            buService = new BancoUsuarioService(dataBaseName);
            migracionOTPadreService = new MigracionRemesasOTPadreService(dataBaseName);
            proveedoresService = new ProveedoresService(dataBaseName);
            nitService = new NitService(dataBaseName);
            anticiposService = new AnticiposService(dataBaseName);
            EsquemaChequeSvc = new EsquemaChequeService(dataBaseName);
            ImprimirChequeSvc = new ImpresionChequeService(dataBaseName);
            tService2 = new TransaccionService2(dataBaseName);
            conciliacion = new Conciliacion(dataBaseName);
            reporteInfoClienteService = new ReporteInformacionAlClienteService(dataBaseName);
            propService = new PropietarioService(dataBaseName);
            ReporteEgresoSvc = new ReporteEgresoService(dataBaseName);
            ConsultaExtractoSvc = new ConsultaExtractoService(dataBaseName);
            formato_tablaService = new Formato_tablaService(dataBaseName);
            banco_PropietarioService = new Banco_PropietarioService(dataBaseName);
            Smservice = new Smservice(dataBaseName);
            captacionInversionistaService = new CaptacionInversionistaService(dataBaseName);
            OpSvc = new OpService(dataBaseName);
            FenalcoService = new SiniestrosService(dataBaseName);
            pdfImprimirPagareService = new PdfImprimirPagareService(dataBaseName); //Ing. Jose Avila - 24/07/2012
            pdfImprimirFacturaAval = new PdfImprimirFacturaAval(dataBaseName); //Ing. Jose Avila - 27/07/2012
            AnticiposGasolinaService = new AnticipoGasolinaService(dataBaseName);
            preanticipoService = new PreanticipoService(dataBaseName);
            interesesFenalcoService = new InteresesFenalcoService(dataBaseName);
            procesarRecaudosFenalcoService = new ProcesarRecaudosFenalcoService(dataBaseName);
            StandarSvc = new StandarService(dataBaseName);
            emailService = new EMailService(dataBaseName);
            PlanillaImpresionSvc = new PlanillaImpresionService();
            hReporteService = new HReporteService();
            usuaprobacionService = new UsuarioAprobacionService(dataBaseName);
            CaptacionesFintraSvc = new CaptacionesFintraService(dataBaseName);
            ImportacionCXPSvc = new ImportacionCXPService(dataBaseName);
            FenalcoFintraSvc = new FenalcoFintraService(dataBaseName);
            ReporteBTSvc = new ReporteBancoTransferenciaService(dataBaseName);
            comparacionplanillasservice = new ComparacionPlanillasService(dataBaseName);
            reporteOcAnt = new ReporteOCAnticipoService(dataBaseName);
            control_excepcionService = new Control_excepcionService(dataBaseName);
            AdicionAjustesPlanillasSvc = new AdicionAjustesPlanillasServices(dataBaseName);
            reporteOCVacioAnticipoService = new ReporteOCVacioAnticipoService(dataBaseName);
            trasladoService = new TrasladoService(dataBaseName);
            facturaProveedorAnticipoService = new FacturaProveedorAnticipoService(dataBaseName);
            ReimpresionSvc = new ReimpresionService(dataBaseName);
            prechequeSvc = new PrechequeService(dataBaseName);
            ImportacionCXCSvc = new ImportacionCXCService(dataBaseName);
            docAnuladoSvc = new SerieDocAnuladoService(dataBaseName);
            CambioDestinoSvc = new CambioDestinoService(dataBaseName);
            Avales = new ConsultarAvalesService(dataBaseName);
            creacionCompraCarteraSvc = new creacionCompraCarteraService(dataBaseName);
            sectorsubsectorSvc = new SectorSubsectorService(dataBaseName);
            avalesService = new AvalesService(dataBaseName);
            prontoPagoService = new ProntoPagoService(dataBaseName);
            
            provisionFenalcoService = new ProvisionFenalcoService();
            procesoCorficolombianaService = new ProcesoCorficolombianaService(dataBaseName);
            archivosCorficolombianaService = new ArchivosCorficolombianaService();
            importeTextoService = new ImporteTextoService();
            facturascorficolombianaSvc = new FacturasCorficolombianaService();
            
//            LiqOCSvc = new LiquidarOCService();            
            
            PdfImprimirSvc = new PdfImprimirService();
            TproveedorSvc = new Tipo_proveedorService();
            hojaVidaService = new HojaVidaService();
            cxpImpDocService = new CXPImpDocService();
            migracionOTsModifService = new MigracionRemesasModificacionOTService();
            corficolombianaSvc = new CorficolombianaService();
            unidadService = new UnidadService();
            logservice = new LogServidorService();
            tiempoService = new TiempoService();
            remidestService = new RemiDestService();
            rdService = new RemesaDestService();
            RegistroTiempoSvc = new RegistroTiempoService();
            colorService = new ColorService();
            marcaService = new MarcaService();
            carrService = new CarroceriaService();
            peajeService = new PeajeService();
            sdelayService = new SdelayService();
            movplaService = new MovPlaService();
            stdjobplkcostoService = new StdjobplkcostoService();
            plkgruService = new PlkgruService();
            pla_tiempoService = new Planilla_TiempoService();
            remplaService = new RemPlaService();
            plaauxService = new PlaauxService();
            usuarioService = new UsuarioService();
            tbltiempoService = new TbltiempoService();
            conductorService = new ConductorService();
            proveedoracpmService = new Proveedor_AcpmService();
            proveedoranticipoService = new Proveedor_AnticipoService();
            proveedortiquetesService = new Proveedor_TiquetesService();
            stdjobcostoService = new StdjobcostoService();
            stdjobdetselService = new StdjobdetselService();
            tramoService = new TramoService();
            RemisionOCSvc = new RemisionOCService();
            PlanillasSvc = new PlanillasService();
            Convercion = new RMCantidadEnLetras();
            RemisionSvc = new RemisionService();
            usuario = new Usuario();
            afleteService = new AutorizacionFleteService();
            ubService = new UbicacionService();
            tiketService = new Tramo_TicketService();
            rutaService = new RutaService();
            perfilService = new PerfilService();
            tipo_ubicacionService = new Tipo_ubicacionService();
            codigo_demoraService = new Codigo_demoraService();
            codigo_discrepanciaService = new Codigo_discrepanciaService();
            contactoService = new ContactoService();
            zonaService = new ZonaService();
            tipo_contactoService = new Tipo_contactoService();
            unidadTrfService = new UnidadTrfService();
            novedadService = new NovedadService();
            zonaUsuarioService = new ZonaUsuarioService();
            demorasSvc = new DemorasService();
            caravanaSVC = new com.tsp.operation.model.services.CaravanaService();
            escoltaVehiculoSVC = new EscoltaVehiculoService();
            actividadSvc = new ActividadService();
            zonaService = new ZonaService();
            clientactSvc = new ClienteActividadService();
            infoactService = new InfoActividadService();
            jerarquia_tiempoService = new Jerarquia_tiempoService();
            actividadTrfService = new ActividadTrfService();
            responsableActTrfService = new ResponsableActTrfService();
            rmtService = new RepMovTraficoService();
            camposjsp = new campos_jspService();
            perfilvistausService = new PerfilVistaUsuarioService();
            jspService = new JspService();
            perfil_vistaService = new Perfil_vistaService();
            AsigUsuCliSvc = new AsignacionUsuariosClientesService();
            traficoService = new TraficoService();
            autoAnticipoService = new Auto_AnticipoService();
            planViajeService = new PlanViajeService();
            caravanaService = new CaravanaService();
            viaService = new ViaService();
            servicioStdjob = new StdjobService();
            regAsigService = new RegistroAsignacionServices();
            reporteUbicacionVehService = new ReporteUbicacionVehicularService();
            grupoEquipoService = new GrupoEquipoService();
            reporteDevolucionesService = new ReporteDevolucionesService();
            movAduanaService = new MovimientoAduanaService();
            repTiempoDeViajeConductores = new ReporteTiemposDeViajeConductoresService();
            repTiemposPCService = new ReporteTiemposPuestosDeControlService();
            confReportService = new ConfigurarReporteClientesService();
            consultasGrlesReportesService = new ConsultasGeneralesDeReportesService();
            gruposReporteService = new GruposReporteService();
            stdjobServices = new StdjobServices();
            reqclienteServices = new ReqClienteServices();
            ipredoSvc = new InformePredoService();
            rdSvc = new RecursosdispService();
            mvSvc = new MovtraficoService();
            trSvc = new TramoService();
            tpSvc = new TiporecService();
            plremSvc = new PlaremService();
            cumplidoService = new CumplidoService();
            cumplido_docService = new Cumplido_docService();
            mraSvc = new MigracionRemesasAnuladasService();
            migFitDefSvc = new MigrarOTFitDefService();
            tbl_estadoSvc = new TblEstadoService();
            actividad_documentoSvc = new DocumentoActividadService();
            appSvc = new AplicacionService();
            apl_docSvc = new DocumentoAplicacionService();
            otros_conceptosSvc = new ConceptoService();
            repRetroactivosSvc = new ReporteRetroactivoService();
            repNitPlacasUpSvc = new ReporteNitPlacaUpdatesService();
            precintosSvc = new PrecintosService();
            wgroup_stdjobSvc = new WGrpStdjobService();
            estadViajesSvc = new EstadViajesService();
            rep_utilidadSvc = new RepUtilidadService();
            indicadoresSvc = new IndicadoresService();
            factrecurrService = new RXPDocService();
            factrecurrImpService = new RXPImpDocService();
            discrepanciaService = new DiscrepanciaService();
            sancionService = new SancionService();
            productoService = new ProductoService();
            tipo_sancionService = new Tipo_sancionService();
            responsableService = new ResponsableService();
            cumplidos_DocumentosService = new Cumplidos_DocumentosService();
            flota_directaService = new Flota_directaService();
            acuerdo_especialService = new Acuerdo_especialService();
            ingreso_especialService = new Ingreso_especialService();
            descuento_claseService = new Descuento_claseService();
            descuentoService = new DescuentoService();
            equipoPropioService = new EquipoPropioService();
            concepto_equipoService = new Concepto_equipoService();
            cliente_ubicacionService = new Cliente_ubicacionService();
            justificacionService = new JustificacionService();
            reporteGeneralService = new ReporteGeneralService();
            esquema_formatoService = new Esquema_formatoService();
            equivalenciaCargaService = new EquivalenciaCargaService();
            DirectorioSvc = new DirectorioService();
            ImagenSvc = new ImagenService();
            ProyectoSvc = new ProyectoService();
            PlanViajeSvc = new PlanDeViajeService();
            AdminSoft = new AdminSoftwareService();
            RevisionSoft = new RevisionSoftwareService();
            ConsultaSoft = new ConsultaSoftwareService();
            ReportePlanillasViaSvc = new ReportePlanillasViaServices();
            StatusBarraSvc = new StatusBarraServices();
            FormatoSvc = new FormatoServices();
            AdicionOPSvc = new AdicionOPServices();
            stdjobService = new StdjobService();
            sjextrafleteService = new SJExtrafleteService();
            codextrafleteService = new CodextrafleteService();
            migracionPlanillaAnuladaService = new MigracionPlanillaAnuladaAnticipoService();
            referenciaService = new ReferenciaService();
            proveedorEscoltaService = new ProveedorEscoltaService();
            migracionProveedorEsc = new MigracionProveedorEscoltaService();
            flotaUtilizadaService = new FlotaUtilizadaService();
            VExternosSvc = new veh_externosService();
            VAlquilerSvc = new veh_alquilerService();
            RemDocSvc = new remesa_doctoService();
            stdjob_tbldocService = new Stdjob_tbldocService();
            ReporteViajeSvc = new ReporteViajeService();
            RemesaSinDocSvc = new RemesaSinDocService();
            AnulacionPlanillaSvc = new AnulacionPlanillaService();
            FlotaSvc = new FlotaService();
            hojaControlViajeSvc = new HojaControlViajeService();
            remesaImpresionSvc = new RemesaImpresionService();
            reporteVehPerdidosSvc = new ReporteVehPerdidosService();
            wgroup_placaSvc = new Wgroup_PlacaService();
            sendMailService = new SendMailService();
            balancePrueba = new BalancePruebaService();
            cajaLectoraSvc = new Caja_lecService();
            registroTarjetaSvc = new RegistroTarjetaService();
            veridocService = new VerificacionDocService();
            causadiscreService = new CausaDiscrepanciaService();
            migradiscreService = new MigracionDiscrepanciaService();
            actplanService = new ActividadPlanService();
            tiempoviajecarbonService = new TiempoViajeCarbonService();
            controlactAduaneraService = new ControlactAduaneraService();
            ConductorSvc = new ConductorService();
            retroactivoService = new RetroactivoService();
            planllegadacargue = new PlanllegadacargueService();
            formularioService = new FormularioService();
            adminH = new AdminHistoricoService();
            despachoManualService = new DespachoManualService();
            retrasoSService = new RetrasoSalidaService();
            turnosService = new TurnosService();
            consultaUsuarioService = new ConsultaUsuarioService();
            tablasUsuarioService = new TablasUsuarioService();
            impoExpoService = new ImpoExpoService();
            reportecumplidosService = new ReportesCumplidosService();
            tLBActividadesServices = new TLBActividadesService();
            objetosservice = new ObjetosService();
            reporteplacanitfotoService = new ReportePlacaFotoService();
            reporteMovTraficoCerromatosoService = new ReporteMovTraficoCerromatosoService();
            relaciongruposubgrupoplaca = new RelacionGrupoSubgrupoPlacaService();
            cuentavalidaservice = new CuentaValidaService();
            SoporteClienteSvc = new SoporteClientesService();
            ConceptosSvc = new TblConceptosService();
            SJCostoSvc = new SJCostoService();
            PlanillaCostosSvc = new PlanillaCostosService();
            reportePPPSvc = new ReportePPPService();
            causas_anulacionService = new Causas_AnulacionService();
            trecuperacionaService = new Tipo_Recuperacion_AnticipoService();
            TiempoViajeSvc = new TiempoViajeService();
            instrucciones_despachoService = new Instrucciones_DespachoService();
            ReporteDrummondSvc = new ReporteDrummondService();
            DescuentoTercmSvc = new DescuentoTercmService();
            UbicacionVehicularClientesSvc = new UbicacionVehicularClientesService();
            UbicaVehicuPorpSvc = new UbicacionVehicularPropietarioService();
            ConciliacionSvc = new ConciliacionService();
            repInfoOtMimPosSvc = new RepInfoOtMimPosSvc();
            ReportePODSvc = new ReportePODService();
            reporteDevolucionesDiscrepService = new ReporteDevolucionesDiscrepService();
            ReversarViajeTraficoSvc = new ReversarViajeTraficoService();
            descuentoequiposvc = new DescuentoEquipoService();
            tblgendatosvc = new TblGeneralDatoService();
            vehnrsvc = new VehNoRetornadoService();
            repinvsvc = new InventarioSoftwareReporteService();
            tbl_GeneralProgService = new TblGeneralProgService();
            fitmenService = new FitmenService();
            ecpService = new EstCargaPlacaService();
            repdevService = new ReporteDevolucionService();
            citacargueService = new CitaCargueService();
            convertirPlaManService = new ConvertirPlanillaManualService();
            reporteFacDesService = new ReporteFacturaDestinatarioService();
            imprimirOrdenService = new ImprimirOrdenDeCargaService();
            remesasPorFacturarService = new ReporteRemesasPorFacturarService();
            planillaManualPlanViajeService = new PlanillaManualPlanViajeService();
            reporteProveedoresFintraService = new ReporteProveedoresFintraService();
            consultarModificarFacturaClienteService = new ConsultarModificarFacturaClienteService();
            reporteCarbonService = new ReporteCarbonService();
            IngPrestamoSvc = new IngresoPrestamoService();
            PrestamoSvc = new PrestamoService();
            CuotasSvc = new CuotasService();
            ResumenPrtSvc = new ResumenPrestamoService();
            DescuentoTercmWebSvc = new DescuentoTercmWebService();
            PorcentajeSvc = new PorcentajeService();
            mfcs = new MigracionFacturasClientesService();
            himportacioncfservice = new HImportacionCFService();
            ExtractoPPSvc = new ExtractoPPService(dataBaseName);
            planllegadacargue = new PlanllegadacargueService();
            planillaSinCumplidoService = new PlanillaSinCumplidoService();
            transitoService = new TransitoService();
            PerfilesGenService = new PerfilesGenService();
            mantenimientoFestivosService = new MantenimientoFestivosService();
            gsaserv=new GestionSolicitudAvalService(dataBaseName);
        } catch (Exception e) {  }
    }
}
