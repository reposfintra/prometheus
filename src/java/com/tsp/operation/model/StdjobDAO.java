package com.tsp.operation.model;

import java.sql.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.beans.StdJob;
import java.util.*;

public class StdjobDAO {
    
    private final String SQL_OBTENER_WORPGROUP = "select work_group,porcentaje_maximo_anticipo,std_job_desc from stdjob,remesa where remesa.std_job_no = stdjob.std_job_no and remesa.numrem = ?";
    private StdJob stdjob;
    private static final  String Select_Consulta = "SELECT * FROM stdjob WHERE dstrct_code = ? AND std_job_no = ?";
    //HENRY
    private static final String SQL_LISTAR_STDJOB_CLIENTE = "SELECT DISTINCT  S.STD_JOB_NO," +
    "		 S.STD_JOB_DESC," +
    "		 S.ORIGIN_CODE, " +
    "		 S.DESTINATION_CODE," +
    "		 CO.NOMCIU AS ORIGIN_NAME, " +
    "		 CD.NOMCIU AS DESTINATION_NAME" +
    "	FROM     STDJOB S," +
    "		 CIUDAD CO," +
    "		 CIUDAD CD" +
    "	WHERE    STD_JOB_NO LIKE ? " +
    "		AND CO.CODCIU =S.ORIGIN_CODE" +
    "		AND CD.CODCIU =S.DESTINATION_CODE";
    
    private static final String SQL_RUTA_STD_JOB          = "SELECT ORIGIN_CODE, DESTINATION_CODE FROM STDJOB WHERE STD_JOB_NO=?";
    private static final String SQL_LISTAR_STDJOB_RUTA    = "SELECT * FROM STDJOB WHERE ORIGIN_CODE=? AND DESTINATION_CODE=?";
    
    //TMaturana 19.01.06
    private static final String SQL_ORIGEN_SJCLIENTE =
            "SELECT DISTINCT(COALESCE(origin_code, '')) AS origin_code, " +
            "       COALESCE(get_nombreciudad(origin_code), 'No se encontr�.') AS origin_name " +
            "FROM stdjob sj WHERE '000'||SUBSTRING(std_job_no FROM 1 FOR 3) = ?";
    
    private static final String SQL_DESTINO_SJCLIENTE = 
            "SELECT DISTINCT(COALESCE(destination_code, '')) AS destination_code, " +
            "       COALESCE(get_nombreciudad(destination_code), 'No se encontr�.') AS destination_name " +
            "FROM stdjob sj WHERE '000'||SUBSTRING(std_job_no FROM 1 FOR 3) = ? AND origin_code=?";
    
    private static final String SQL_STDJOB_RUTA_CLIENTE =
            "SELECT std_job_no, " +
            "       std_job_desc " +
            "FROM stdjob sj " +
            "WHERE  '000'||SUBSTRING(std_job_no FROM 1 FOR 3) = ? " +
            "       AND origin_code=? AND destination_code=?";
    //jose 2006-04-05
    private static final String SQL_STDJOB_VACIOS = "" +
    "select std_job_no, std_job_desc, "+
    "  recurso1, tipo_recurso1, prioridad1, "+
    "  recurso2, tipo_recurso2, prioridad2, "+
    "  recurso3, tipo_recurso3, prioridad3, "+
    "  recurso4, tipo_recurso4, prioridad4, "+
    "  recurso5, tipo_recurso5, prioridad5 "+
    "from "+
    "  stdjob "+
    "where "+
    " NOT  "+
    "( "+
    "  ( "+
    "    ( recurso1 != '' AND tipo_recurso1 != '' AND prioridad1 != '')  AND "+
    "    ( ( recurso2 = '' AND tipo_recurso2 = '' AND prioridad2 = '') AND ( recurso3 = '' AND tipo_recurso3 = '' AND prioridad3 = '') AND ( recurso4 = '' AND tipo_recurso4 = '' AND prioridad4 = '') AND ( recurso5 = '' AND tipo_recurso5 = '' AND prioridad5 = '') ) "+
    "  ) "+
    "  OR "+
    "  ( "+
    "    (  ( recurso1 != '' AND tipo_recurso1 != '' AND prioridad1 != '') AND ( recurso2 != '' AND tipo_recurso2 != '' AND prioridad2 != '') ) AND "+
    "    (  ( recurso3 = '' AND tipo_recurso3 = '' AND prioridad3 = '') AND ( recurso4 = '' AND tipo_recurso4 = '' AND prioridad4 = '') AND ( recurso5 = '' AND tipo_recurso5 = '' AND prioridad5 = '') ) "+
    "  ) "+
    "  OR "+
    "  ( "+
    "    (  ( recurso1 != '' AND tipo_recurso1 != '' AND prioridad1 != '') AND ( recurso2 != '' AND tipo_recurso2 != '' AND prioridad2 != '') AND ( recurso3 != '' AND tipo_recurso3 != '' AND prioridad3 != '') ) AND "+
    "    (  ( recurso4 = '' AND tipo_recurso4 = '' AND prioridad4 = '') AND ( recurso5 = '' AND tipo_recurso5 = '' AND prioridad5 = '') ) "+
    "  ) "+
    "  OR "+
    "  ( "+
    "    ( recurso1 != '' AND tipo_recurso1 != '' AND prioridad1 != '') AND ( recurso2 != '' AND tipo_recurso2 != '' AND prioridad2 != '') AND ( recurso3 != '' AND tipo_recurso3 != '' AND prioridad3 != '') AND ( recurso4 != '' AND tipo_recurso4 != '' AND prioridad4 != '')  AND "+
    "    ( recurso5 = '' AND tipo_recurso5 = '' AND prioridad5 = '') "+
    "  ) "+
    "  OR "+
    "  ( "+
    "    ( recurso1 != '' AND tipo_recurso1 != '' AND prioridad1 != '') AND ( recurso2 != '' AND tipo_recurso2 != '' AND prioridad2 != '') AND ( recurso3 != '' AND tipo_recurso3 != '' AND prioridad3 != '') AND ( recurso4 != '' AND tipo_recurso4 != '' AND prioridad4 != '') AND ( recurso5 != '' AND tipo_recurso5 != '' AND prioridad5 != '') "+
    "  ) "+
    " )";
    
    //jose 2006-04-05
    private static final String SQL_STDJOB_LLENOS = "" +
    "select std_job_no, std_job_desc, "+
    "  recurso1, tipo_recurso1, prioridad1, "+
    "  recurso2, tipo_recurso2, prioridad2, "+
    "  recurso3, tipo_recurso3, prioridad3, "+
    "  recurso4, tipo_recurso4, prioridad4, "+
    "  recurso5, tipo_recurso5, prioridad5 "+
    "from "+
    "  stdjob "+
    "where "+
    "( "+
    "  ( "+
    "    ( recurso1 != '' AND tipo_recurso1 != '' AND prioridad1 != '')  AND "+
    "    ( ( recurso2 = '' AND tipo_recurso2 = '' AND prioridad2 = '') AND ( recurso3 = '' AND tipo_recurso3 = '' AND prioridad3 = '') AND ( recurso4 = '' AND tipo_recurso4 = '' AND prioridad4 = '') AND ( recurso5 = '' AND tipo_recurso5 = '' AND prioridad5 = '') ) "+
    "  ) "+
    "  OR "+
    "  ( "+
    "    (  ( recurso1 != '' AND tipo_recurso1 != '' AND prioridad1 != '') AND ( recurso2 != '' AND tipo_recurso2 != '' AND prioridad2 != '') ) AND "+
    "    (  ( recurso3 = '' AND tipo_recurso3 = '' AND prioridad3 = '') AND ( recurso4 = '' AND tipo_recurso4 = '' AND prioridad4 = '') AND ( recurso5 = '' AND tipo_recurso5 = '' AND prioridad5 = '') ) "+
    "  ) "+
    "  OR "+
    "  ( "+
    "    (  ( recurso1 != '' AND tipo_recurso1 != '' AND prioridad1 != '') AND ( recurso2 != '' AND tipo_recurso2 != '' AND prioridad2 != '') AND ( recurso3 != '' AND tipo_recurso3 != '' AND prioridad3 != '') ) AND "+
    "    (  ( recurso4 = '' AND tipo_recurso4 = '' AND prioridad4 = '') AND ( recurso5 = '' AND tipo_recurso5 = '' AND prioridad5 = '') ) "+
    "  ) "+
    "  OR "+
    "  ( "+
    "    ( recurso1 != '' AND tipo_recurso1 != '' AND prioridad1 != '') AND ( recurso2 != '' AND tipo_recurso2 != '' AND prioridad2 != '') AND ( recurso3 != '' AND tipo_recurso3 != '' AND prioridad3 != '') AND ( recurso4 != '' AND tipo_recurso4 != '' AND prioridad4 != '')  AND "+
    "    ( recurso5 = '' AND tipo_recurso5 = '' AND prioridad5 = '') "+
    "  ) "+
    "  OR "+
    "  ( "+
    "    ( recurso1 != '' AND tipo_recurso1 != '' AND prioridad1 != '') AND ( recurso2 != '' AND tipo_recurso2 != '' AND prioridad2 != '') AND ( recurso3 != '' AND tipo_recurso3 != '' AND prioridad3 != '') AND ( recurso4 != '' AND tipo_recurso4 != '' AND prioridad4 != '') AND ( recurso5 != '' AND tipo_recurso5 != '' AND prioridad5 != '') "+
    "  ) "+
    " )";    
    private TreeMap origin = new TreeMap();
    private TreeMap destination = new TreeMap();
    private TreeMap stdjobs = new TreeMap();
    
    public StdjobDAO() {
    }
    
    public StdJob obtenerStdJob(String numrem) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra");
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement( SQL_OBTENER_WORPGROUP );
                ps.setString( 1, numrem );
                rs = ps.executeQuery();
                if (rs.next()){
                    StdJob s = new StdJob();
                    s.setPorcentaje_maximo_anticipo(rs.getFloat("porcentaje_maximo_anticipo"));
                    s.setWork_group(rs.getString("work_group"));
                    s.setstd_job_desc(rs.getString("std_job_desc"));
                    return s;
                    
                }
            }
            return null;
        }
        catch(Exception e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
        
    }
    public StdJob BuscarStdjob(String dstrct, String std_job_no )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        StdJob stdjob = new StdJob();
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(Select_Consulta);
                st.setString(1,dstrct);
                st.setString(2,std_job_no);
                
                rs = st.executeQuery();
                
                
                while (rs.next()){
                    stdjob.setdstrct_code( rs.getString("dstrct_code") );
                    stdjob.setstd_job_no( rs.getString("std_job_no") );
                    stdjob.setorigin_code( rs.getString("origin_code") );
                    stdjob.setorigin_name( rs.getString("origin_name") );
                    stdjob.setdestination_code( rs.getString("destination_code") );
                    stdjob.setdestination_name( rs.getString("destination_name") );
                    stdjob.setTipo_recurso1( rs.getString("tipo_recurso1"));
                    stdjob.setTipo_recurso2( rs.getString("tipo_recurso2"));
                    stdjob.setTipo_recurso3( rs.getString("tipo_recurso3"));
                    stdjob.setTipo_recurso4( rs.getString("tipo_recurso4"));
                    stdjob.setTipo_recurso5( rs.getString("tipo_recurso5"));
                    stdjob.setrecurso1( rs.getString("recurso1") );
                    stdjob.setrecurso2( rs.getString("recurso2") );
                    stdjob.setrecurso3( rs.getString("recurso3") );
                    stdjob.setrecurso4( rs.getString("recurso4") );
                    stdjob.setrecurso5( rs.getString("recurso5") );
                    stdjob.setprioridad1( rs.getString("prioridad1") );
                    stdjob.setprioridad2( rs.getString("prioridad2") );
                    stdjob.setprioridad3( rs.getString("prioridad3") );
                    stdjob.setprioridad4( rs.getString("prioridad4") );
                    stdjob.setprioridad5( rs.getString("prioridad5") );
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA Stdjob" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return stdjob;
    }
    public Vector obtenerStdJobsCliente(String cod) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector stClientes = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra");
            if ( con != null ) {
                ps = con.prepareStatement(this.SQL_LISTAR_STDJOB_CLIENTE );
                ps.setString(1, cod+"%");
                ////System.out.println(ps);
                rs = ps.executeQuery();
                while(rs.next()){
                    StdJob std = new StdJob();
                    std.setstd_job_no(rs.getString("std_job_no"));
                    std.setstd_job_desc(rs.getString("std_job_desc"));
                    std.setorigin_code(rs.getString("origin_code"));
                    std.setdestination_code(rs.getString("destination_code"));
                    std.setorigin_name(rs.getString("origin_name"));
                    std.setdestination_name(rs.getString("destination_name"));
                    stClientes.addElement(std);
                }
            }
            return stClientes;
        }
        catch(SQLException e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( ps != null ) {
                try {
                    ps.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
        
    }
    public Vector obtenerStdJobsRuta(String origen, String dest) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector stClientes = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra");
            if ( con != null ) {
                ps = con.prepareStatement(this.SQL_LISTAR_STDJOB_RUTA);
                ps.setString(1, origen);
                ps.setString(2, dest);
                ////System.out.println(ps);
                rs = ps.executeQuery();
                while(rs.next()){
                    StdJob std = new StdJob();
                    std.setstd_job_no(rs.getString("std_job_no"));
                    std.setstd_job_desc(rs.getString("std_job_desc"));
                    std.setorigin_code(rs.getString("origin_code"));
                    std.setdestination_code(rs.getString("destination_code"));
                    std.setorigin_name(rs.getString("origin_name"));
                    std.setdestination_name(rs.getString("destination_name"));
                    stClientes.addElement(std);
                }
            }
            return stClientes;
        }
        catch(SQLException e){
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( ps != null ) {
                try {
                    ps.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
        
    }
    public String obtenerRutaStdjob(String std_job_no )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String ruta = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_RUTA_STD_JOB);
                st.setString(1,std_job_no);
                rs = st.executeQuery();
                if (rs.next()){
                    ruta = rs.getString("origin_code")+"-"+rs.getString("destination_code");
                }
            }
            return ruta;
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA Stdjob" + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     *Funcion que retorna true o false si  la agencia de origen
     *es igual a la agencia de destino.
     *@autor: Karen Reales
     *@param: Ruta
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public boolean esUrbano(String ruta)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw= false;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select 	origen.agasoc," +
                "	destino.agasoc," +
                "	CASE WHEN origen.agasoc=destino.agasoc then true else false end as valor" +
                " from 	(select agasoc  from ciudad where codciu = substrinG(?,1,2))origen" +
                "	left join (select agasoc from ciudad where codciu = substrinG(?,3,2))destino" +
                "	on (origen.agasoc=destino.agasoc)");
                st.setString(1,ruta);
                st.setString(2, ruta);
                ////System.out.println("Query de urbano "+st.toString());
                rs = st.executeQuery();
                if(rs.next()){
                    sw= rs.getBoolean("valor");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR VALIDANDO SI UNA RUTA ES URBANA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
     /**
     * Obtiene los c�digos y nombres de todas las ciudades registradas como origen 
     * en los Standard Jobs del cliente indicado.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cliente C�digo del cliente
     * @return <code>TreeMap</code> con los c�digos y nombres de las ciudades
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void origenesStdJobsCliente(String cliente)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.origin = new TreeMap();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_ORIGEN_SJCLIENTE);
                st.setString(1, cliente);
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    String code = rs.getString("origin_code");
                    String name = rs.getString("origin_name");
                    
                    this.origin.put(name, code);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR OBTENIENDO LOS ORIGENES DE LOS STDJOBS DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Getter for property origin.
     * @return Value of property origin.
     */
    public java.util.TreeMap getOrigin() {
        return origin;
    }
    
    /**
     * Setter for property origin.
     * @param origin New value of property origin.
     */
    public void setOrigin(java.util.TreeMap origin) {
        this.origin = origin;
    }
    
    /**
     * Getter for property destination.
     * @return Value of property destination.
     */
    public java.util.TreeMap getDestination() {
        return destination;
    }
    
    /**
     * Setter for property destination.
     * @param destination New value of property destination.
     */
    public void setDestination(java.util.TreeMap destination) {
        this.destination = destination;
    }
    
    /**
     * Obtiene los c�digos y nombres de todas las ciudades registradas como destino 
     * en los Standard Jobs del cliente indicado.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cliente C�digo del cliente
     * @param origen C�digo de la ciudad origen
     * @return <code>TreeMap</code> con los c�digos y nombres de las ciudades
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void destinosStdJobsCliente(String cliente, String origin)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.destination = new TreeMap();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_DESTINO_SJCLIENTE);
                st.setString(1, cliente);
                st.setString(2, origin);
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    String code = rs.getString("destination_code");
                    String name = rs.getString("destination_name");
                    
                    this.destination.put(name, code);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR OBTENIENDO LOS DESTINOS DE LOS STDJOBS DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Obtiene los n�meros y descripciones de todos los Standard Jobs con una ciudad de origen
     * y destino indicada.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cliente C�digo del cliente
     * @param origen C�digo de la ciudad origen
     * @param destino C�digo de la ciudad destino
     * @return <code>TreeMap</code> con los c�digos y nombres de las ciudades
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void listarStdJobsCliente(String cliente, String origin, String destino)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.stdjobs = new TreeMap();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_STDJOB_RUTA_CLIENTE);
                st.setString(1, cliente);
                st.setString(2, origin);
                st.setString(3, destino);
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    String code = rs.getString("std_job_no");
                    String name = rs.getString("std_job_desc");
                    
                    this.stdjobs.put(name, code);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR OBTENIENDO LOS STD JOBS DE LOS STDJOBS DE UN CLIENTE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Getter for property stdjobs.
     * @return Value of property stdjobs.
     */
    public java.util.TreeMap getStdjobs() {
        return stdjobs;
    }
    
    /**
     * Setter for property stdjobs.
     * @param stdjobs New value of property stdjobs.
     */
    public void setStdjobs(java.util.TreeMap stdjobs) {
        this.stdjobs = stdjobs;
    }
    
    /* Metodo: obtenerStandaresVacios, permite listar los campos standares vacios
 * @autor : Ing. Jose de la rosa
 * @param : 
 * @version : 1.0
 */
    public List obtenerStandaresVacios() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        List standar = new LinkedList();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_STDJOB_VACIOS);
                rs = st.executeQuery();
                while(rs.next()){
                    stdjob = new StdJob();
                    stdjob.setstd_job_no ( rs.getString("std_job_no")!=null? rs.getString("std_job_no") : "" );
                    stdjob.setstd_job_desc ( rs.getString("std_job_desc")!=null? rs.getString("std_job_desc") : "" );
                    stdjob.setTipo_recurso1 ( rs.getString("tipo_recurso1")!=null? rs.getString("tipo_recurso1") : "" );
                    stdjob.setTipo_recurso2 ( rs.getString("tipo_recurso2")!=null? rs.getString("tipo_recurso2") : "" );
                    stdjob.setTipo_recurso3 ( rs.getString("tipo_recurso3")!=null? rs.getString("tipo_recurso3") : "" );
                    stdjob.setTipo_recurso4 ( rs.getString("tipo_recurso4")!=null? rs.getString("tipo_recurso4") : "" );
                    stdjob.setTipo_recurso5 ( rs.getString("tipo_recurso5")!=null? rs.getString("tipo_recurso5") : "" );
                    stdjob.setrecurso1 ( rs.getString("recurso1")!=null? rs.getString("recurso1") : "" );
                    stdjob.setrecurso2 ( rs.getString("recurso2")!=null? rs.getString("recurso2") : "" );
                    stdjob.setrecurso3 ( rs.getString("recurso3")!=null? rs.getString("recurso3") : "" );
                    stdjob.setrecurso4 ( rs.getString("recurso4")!=null? rs.getString("recurso4") : "" );
                    stdjob.setrecurso5 ( rs.getString("recurso5")!=null? rs.getString("recurso5") : "" );
                    stdjob.setprioridad1 ( rs.getString("prioridad1")!=null? rs.getString("prioridad1") : "" );
                    stdjob.setprioridad2 ( rs.getString("prioridad2")!=null? rs.getString("prioridad2") : "" );
                    stdjob.setprioridad3 ( rs.getString("prioridad3")!=null? rs.getString("prioridad3") : "" );
                    stdjob.setprioridad4 ( rs.getString("prioridad4")!=null? rs.getString("prioridad4") : "" );
                    stdjob.setprioridad5 ( rs.getString("prioridad5")!=null? rs.getString("prioridad5") : "" );
                    standar.add(stdjob);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CAMPOS DE DESPACHOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return standar;
    }    
    
/* Metodo: obtenerStandaresLLenos, permite listar los campos standares llenos
 * @autor : Ing. Jose de la rosa
 * @param : 
 * @version : 1.0
 */
    public List obtenerStandaresLLenos() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        List standar = new LinkedList();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_STDJOB_LLENOS);
                rs = st.executeQuery();
                while(rs.next()){
                    stdjob = new StdJob();
                    stdjob.setstd_job_no ( rs.getString("std_job_no")!=null? rs.getString("std_job_no") : "" );
                    stdjob.setstd_job_desc ( rs.getString("std_job_desc")!=null? rs.getString("std_job_desc") : "" );
                    stdjob.setTipo_recurso1 ( rs.getString("tipo_recurso1")!=null? rs.getString("tipo_recurso1") : "" );
                    stdjob.setTipo_recurso2 ( rs.getString("tipo_recurso2")!=null? rs.getString("tipo_recurso2") : "" );
                    stdjob.setTipo_recurso3 ( rs.getString("tipo_recurso3")!=null? rs.getString("tipo_recurso3") : "" );
                    stdjob.setTipo_recurso4 ( rs.getString("tipo_recurso4")!=null? rs.getString("tipo_recurso4") : "" );
                    stdjob.setTipo_recurso5 ( rs.getString("tipo_recurso5")!=null? rs.getString("tipo_recurso5") : "" );
                    stdjob.setrecurso1 ( rs.getString("recurso1")!=null? rs.getString("recurso1") : "" );
                    stdjob.setrecurso2 ( rs.getString("recurso2")!=null? rs.getString("recurso2") : "" );
                    stdjob.setrecurso3 ( rs.getString("recurso3")!=null? rs.getString("recurso3") : "" );
                    stdjob.setrecurso4 ( rs.getString("recurso4")!=null? rs.getString("recurso4") : "" );
                    stdjob.setrecurso5 ( rs.getString("recurso5")!=null? rs.getString("recurso5") : "" );
                    stdjob.setprioridad1 ( rs.getString("prioridad1")!=null? rs.getString("prioridad1") : "" );
                    stdjob.setprioridad2 ( rs.getString("prioridad2")!=null? rs.getString("prioridad2") : "" );
                    stdjob.setprioridad3 ( rs.getString("prioridad3")!=null? rs.getString("prioridad3") : "" );
                    stdjob.setprioridad4 ( rs.getString("prioridad4")!=null? rs.getString("prioridad4") : "" );
                    stdjob.setprioridad5 ( rs.getString("prioridad5")!=null? rs.getString("prioridad5") : "" );
                    standar.add(stdjob);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CAMPOS DE DESPACHOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return standar;
    }    
}
