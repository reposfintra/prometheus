/*
 * Transaccion.java
 *
 * Created on 21 de enero de 2005, 04:08 PM
 */

package com.tsp.operation.model;

import java.sql.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;

/**
 *
 * @author  KREALES
 */
public class TransaccionDAO extends MainDAO {
    
    /** Creates a new instance of Transaccion */
    
    
    Connection con=null;
    Statement st = null;
    ResultSet rs = null;
    PoolManager poolManager = null;
    
    @Deprecated
    public TransaccionDAO() {
        super("");
    }
    public TransaccionDAO(String dataBaseName) {
        super("",dataBaseName);
    }
    
    public Statement getSt(){
        return st;
    }
    public void crearStatement()throws SQLException{
        con   = this.conectarJNDI();
        st = con.createStatement();
    }
    
    public void crearStatement(String dataBaseName)throws Exception{
        con   = this.conectarJNDIDBLink( dataBaseName );
        st = con.createStatement();
    }
    
    public void crearStatement2()throws SQLException{
        poolManager = PoolManager.getInstance();
        con = poolManager.getConnection(this.getDatabaseName()); 
        st = con.createStatement();
    }
    
    public void execute()throws SQLException{
        try{
            
            if(st!=null){
                boolean autocommit = con.getAutoCommit();
                con.setAutoCommit(false);
                st.toString();
                st.executeBatch();
                con.commit();
                con.setAutoCommit(autocommit);
            }
            
        }
        catch(SQLException e){
            con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
            
        }
        finally{
            if (st != null){
                try{
                    st.close();
                    //System.out.println(" cerrar st");
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if ( con != null && !(con.isClosed())){
            con.close();
           // System.out.println(" cerrar con");
            //this.desconectar(con);
            }

        }
        
        
        
    }
    
    public void execute2()throws SQLException{
        try{
            
            if(st!=null){
                boolean autocommit = con.getAutoCommit();
                con.setAutoCommit(false);
                st.toString();
                st.executeBatch();
                con.commit();
                con.setAutoCommit(autocommit);
            }
            
        }
        catch(SQLException e){
            con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
            
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
        
    }
    
     public void cerrarTodo() throws Exception {
        try{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if ( con != null && !(con.isClosed())){
            con.close();
            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
        
    }
     
     
     public void cerrarTodo2() throws Exception {
        try{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
        
    }
     
     /*public void execute(String SQL)throws Exception{
        java.sql.Connection con = null;
        Statement st   = null;
        try{
            //con = this.conectarBDJNDI("fintra");
            con=this.conectar("default");
            st = con.createStatement();            
            if(st!=null){
                st.addBatch(SQL);
                boolean autocommit = con.getAutoCommit();
                con.setAutoCommit(false);
                st.executeBatch();
                con.commit();
                con.setAutoCommit(autocommit);
            }
        }
        catch(SQLException e){
            con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
            
        }
        finally{
            if (st  != null){ try{ st.close(); st=null;   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            //if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar("default"); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           
        }
            
    }*/
   
}