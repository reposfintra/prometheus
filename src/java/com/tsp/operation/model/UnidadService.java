/*
 * CiudadService.java
 *
 * Created on 1 de diciembre de 2004, 03:06 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class UnidadService {
    
    private UnidadDAO unidad;
    
    /** Creates a new instance of CiudadService */
    public UnidadService() {
        unidad= new UnidadDAO();
    }
    
    public TreeMap getUnidades()throws SQLException{
        
        TreeMap unidades=null;
        
        unidad.searchUnidades();
        unidades = unidad.getUnidades();
        
        return unidades;
        
    }
    
    //Jose
    public void listUnidad()throws SQLException{
        unidad.listUnidad();
    }
    
    public Vector listarUnidades() throws SQLException{
        try{
            return unidad.listarUnidades();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
      
    public void serchUnidad(String cod, String uni)throws SQLException{
        unidad.searchUnidad(cod, uni);
    }
    
    public Unidad getUnidad()throws SQLException{
        return unidad.getUnidad();
    }
    /**
     * obtiene el objeto unidad del codigo buscado.
     * @autor Ing. Jose de la rosa
     * @param Codigo de la unidad.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void buscarUnidad (String cod)throws SQLException{
        unidad.buscarUnidad (cod);
    }


    
}
