package com.tsp.operation.model;

import java.sql.SQLException;
import com.tsp.operation.model.StdjobDAO;
import com.tsp.operation.model.beans.StdJob;
import java.util.*;

public class StdjobService {

    private StdjobDAO dao;

    public StdjobService() {
        dao = new StdjobDAO();
    }

    public StdJob obtenerStdJob(String numrem) throws SQLException {
        return dao.obtenerStdJob(numrem);
    }
     public Vector obtenerStdJobClientes(String codcli) throws SQLException {        
        ////System.out.println("CODIGO CLIENTE"+codcli);
        codcli = codcli.substring(3);
        return dao.obtenerStdJobsCliente(codcli);
    }
    public String obtenerRutaStdJob(String std_no) throws SQLException{
        return dao.obtenerRutaStdjob(std_no);
    }
    public Vector obtenerStdJobRuta(String ruta) throws SQLException {        
        int pos = ruta.indexOf("-");
        String origen = ruta.substring(2,pos);
        String dest = ruta.substring(pos+1);       
        ////System.out.println("Origen: "+origen+" Destino: "+dest);
        return dao.obtenerStdJobsRuta(origen, dest);
    }
    /**
     *Funcion que retorna true o false si  la agencia de origen
     *es igual a la agencia de destino.
     *@autor: Karen Reales
     *@param: Ruta
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public boolean esUrbano(String ruta)throws SQLException{
        return dao.esUrbano(ruta);
    }
    
    /**
     * Obtiene los c�digos y nombres de todas las ciudades registradas como origen 
     * en los Standard Jobs del cliente indicado.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cliente C�digo del cliente
     * @return <code>TreeMap</code> con los c�digos y nombres de las ciudades
     * @see com.tsp.operation.model.StdjobDAO#origenesStdJobsCliente(String)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void origenesStdJobsCliente(String cliente)throws SQLException{
        dao.origenesStdJobsCliente(cliente);
    }
    
    /**
     * Obtiene el valor de la propiedad origin.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @return Valor de la propiedad origin.
     * @see com.tsp.operation.model.StdjobDAO#getOrigin()
     * @version 1.0
     */
    public TreeMap getOrigenesStdJobsCliente(){
        return dao.getOrigin();
    }
    
    /**
     * Establece el valor de la propiedad origin.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param tm Valor de la propiedad origin.
     * @see com.tsp.operation.model.StdjobDAO#setOrigin(TreeMap)
     * @version 1.0
     */
    public void setOrigenesStdJobsCliente(TreeMap tm){
        dao.setOrigin(tm);
    }
    
    /**
     * Obtiene los c�digos y nombres de todas las ciudades registradas como destino 
     * en los Standard Jobs del cliente indicado.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cliente C�digo del cliente
     * @param origen C�digo de la ciudad origen
     * @return <code>TreeMap</code> con los c�digos y nombres de las ciudades
     * @see com.tsp.operation.model.StdjobDAO#destinosStdJobsCliente(String, String)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void destinosStdJobsCliente(String cliente, String origin)throws SQLException{
        dao.destinosStdJobsCliente(cliente, origin);
    }
    
    /**
     * Obtiene el valor de la propiedad destination.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @return Valor de la propiedad destination.
     * @see com.tsp.operation.model.StdjobDAO#getDestination()
     * @version 1.0
     */
    public TreeMap getDestinosStdJobsCliente(){
        return dao.getDestination();
    }
    
    /**
     * Establece el valor de la propiedad destination.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param tm Valor de la propiedad destination.
     * @see com.tsp.operation.model.StdjobDAO#setDestination(TreeMap)
     * @version 1.0
     */
    public void setDestinosStdJobsCliente(TreeMap tm){
        dao.setDestination(tm);
    }
    
    /**
     * Obtiene los n�meros y descripciones de todos los Standard Jobs con una ciudad de origen
     * y destino indicada.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cliente C�digo del cliente
     * @param origen C�digo de la ciudad origen
     * @param destino C�digo de la ciudad destino
     * @return <code>TreeMap</code> con los c�digos y nombres de las ciudades
     * @see com.tsp.operation.model.StdjobDAO#listarStdJobsCliente(String, String, String)
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void listarStdJobsCliente(String cliente, String origin, String destino)throws SQLException{
        dao.listarStdJobsCliente(cliente, origin, destino);
    }
    
    /**
     * Obtiene el valor de la propiedad stdjobs.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @return Valor de la propiedad stdjobs.
     * @see com.tsp.operation.model.StdjobDAO#getStdjobs()
     * @version 1.0
     */
    public TreeMap getStdJobsCliente(){
        return dao.getStdjobs();
    }
    
    /**
     * Establece el valor de la propiedad stdjobs.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param tm Valor de la propiedad stdjobs.
     * @see com.tsp.operation.model.StdjobDAO#setStdJobsCliente(TreeMap)
     * @version 1.0
     */
    public void setStdJobsCliente(TreeMap tm){
        dao.setStdjobs(tm);
    }
     //Jose 05.04.2006
    /* Metodo: obtenerDespacho, permite listar los campos asociados al trailer
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicio y fecha fin
     * @see obtenerCamposTrailer - planillaDAO
     * @version : 1.0
     */
    public List obtenerStandaresVacios() throws SQLException{
        try{
            return dao.obtenerStandaresVacios();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    } 
    
    /* Metodo: obtenerDespacho, permite listar los campos asociados al trailer
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicio y fecha fin
     * @see obtenerCamposTrailer - planillaDAO
     * @version : 1.0
     */
    public List obtenerStandaresLLenos() throws SQLException{
        try{
            return dao.obtenerStandaresLLenos();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }   
}
