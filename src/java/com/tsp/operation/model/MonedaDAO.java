/*
 * MonedaDAO.java
 *
 * Created on 29 de diciembre de 2004, 10:09 AM
 */

package com.tsp.operation.model;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  KREALES
 */
public class MonedaDAO {
    
    Moneda m;
    Moneda moneda;
    private static final String SQL_LISTAR_MONEDAS = "SELECT * FROM MONEDAS ORDER BY NOMMONEDA";
     
    //Diogenes
    
    /*
     * query para insertar lo datos a la tabla moneda
     */
    private static final  String insertar = "insert into monedas (inicialesmoneda,codmoneda,nommoneda,creation_date,creation_user)"+
                                             "values (?,?,?,To_Char(now(),'YYYY-MM-DD HH24:MI:SS'),?)";

    /*
     * query para modificar un registro de la tabla
     */
    private static final String modificar = " update                    "+
                                            "      monedas              "+
                                            " set                       "+  
                                            "    nommoneda = ?,         "+
                                            "    last_update = To_Char(now(),'YYYY-MM-DD HH24:MI:SS')::TIMESTAMP, "+
                                            "    user_update = ?,       "+
                                            "    rec_status = ?         "+
                                            " where                     "+
                                            "      codmoneda = ?       ";
    /*
     * query para anular moneda
     */

    private static final String anular = " update                   "+
                                         "      monedas             "+
                                         " set                      "+
                                         "    rec_status = 'A',    "+
                                         "    last_update = To_Char(now(),'YYYY-MM-DD HH24:MI:SS')::TIMESTAMP,"+
                                         "    user_update = ?       "+
                                         " where                    "+
                                         "      codmoneda = ?      ";
     /*
     * query para buscar moneda por el nombre
     */

    private static final String buscarxmon = " select *              "+
                                           " from                    "+
                                           "     monedas              "+
                                           " where                   "+
                                           "       nommoneda like ?  "+
                                           "   and rec_status = ''    ";

     /*
     * query para buscar moneda por el codigo
     */
    
   private static final String busmon = " select *              "+    
                                         " from                  "+
                                         "     monedas            "+   
                                         " where                 "+ 
                                         "       codmoneda = ?  "+
                                         "   and rec_status = ''  ";

    /*
     * query para verificar si exite la inical de la moneda
     */
   private static final String inicial = "select * from monedas where inicialesmoneda = ? and rec_status = '' ";
    private String dataBaseName = "fintra";
    
    /** Creates a new instance of MonedaDAO */
    public MonedaDAO() {
    }
    public MonedaDAO(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }
    
    public Moneda getMoneda(){
        
        return this.m;
    }
    //ESTA FUNCION RETORNA EL VALOR DE LA MONEDA DADA EN FOR_CURRENCY A LOCAL_CURRENCY.
    
    public void searchMoneda(String local_currency, String for_currency, float valor )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        m = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(dataBaseName);
            if(con!=null){
                st = con.prepareStatement("select *, ?/(rate) as valor from tblmon where local_currency =? and for_currency =?");
                st.setFloat(1,valor);
                st.setString(2,local_currency);
                st.setString(3,for_currency);
                rs = st.executeQuery();
                 
                if(rs.next()){
                    m= Moneda.load(rs);
                    m.setCambio(rs.getFloat("valor"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CAMBIOS DE MONEDA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(dataBaseName, con);
            }
        }
        
    }
    public Vector listarMonedas()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        m = null;
        PoolManager poolManager = null;
        Vector monedas = new Vector();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(dataBaseName);
            if(con!=null){
                st = con.prepareStatement(this.SQL_LISTAR_MONEDAS); 
                //////System.out.println("Moneda QRY:"+st);
                rs = st.executeQuery();                 
                while (rs.next()){
                    m = new Moneda();
                    m.setCodMoneda(rs.getString("codmoneda"));
                    m.setNomMoneda(rs.getString("nommoneda"));
                    monedas.addElement(m);
                }
            }
            return monedas;
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR MONEDA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(dataBaseName, con);
            }
        }
        
    }
    
    ////sandrameg 050905
    
     private static final String existe = " select *              "+    
                                         " from                  "+
                                         "     monedas            "+   
                                         " where                 "+ 
                                         "       codmoneda = ?  ";


     public void buscarMonedaxCodigo(String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;

        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(dataBaseName);
            if (con != null){
                st = con.prepareStatement(existe);
                st.setString(1, cod );
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    m = new Moneda();
                    m.setCodMoneda(rs.getString("codmoneda"));
                    m.setNomMoneda(rs.getString("nommoneda"));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR MONEDA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection(dataBaseName, con );
            }
        }  
    }
     
    public boolean existeMoneda(String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(dataBaseName);
            if (con != null){
                st = con.prepareStatement(existe);
                st.setString(1, cod );
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true; 
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR VERIFICAR SI EXISTE MONEDA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection(dataBaseName, con );
            }
        }  
        return sw;
    }     

    //******************************<Diogenes>***********************************************
     /*
     * Procedimiento que setea el objeto moneda
     * parametros: Objeto tipo Moneda
     * 
     */
    public void  setMoneda (Moneda moneda){

        this.moneda=moneda;

    }
     /*
     * Procedimiento que ejcuta sentencias de creacion, modificacion insert
     * dependiendo del tipo de consulta que se mande a ejecutar
     * parametros:
     *  Tipo   -> tipo de consulta
     *  args[] -> parametros del query
     *  conn   -> conexion opcional
     */
    public void insertarMoneda() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(dataBaseName);
            if (con != null){
                st = con.prepareStatement(insertar);
                st.setString(1, moneda.getInicial() );
                st.setString(2, moneda.getCodMoneda() );
                st.setString(3, moneda.getNomMoneda() );
                st.setString(4, moneda.getCreation_user() );
                ////System.out.println(st);
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR LA MONEDA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection(dataBaseName, con );
            }
        }      
    } 

    public void modificarMoneda() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;

        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(dataBaseName);
            if (con != null){
                st = con.prepareStatement(modificar);
                st.setString(1, moneda.getNomMoneda() );
                st.setString(2, moneda.getUser_update() );
                st.setString(3, moneda.getEstado() );
                st.setString(4, moneda.getCodMoneda() );
                
                ////System.out.println("Modificar "+st);
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR MONEDA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection(dataBaseName, con );
            }
        }  
    }
    
    public void anularMoneda() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;

        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(dataBaseName);
            if (con != null){
                st = con.prepareStatement(anular);
                st.setString(1, moneda.getUser_update() );
                st.setString(2, moneda.getCodMoneda() );
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR MONEDA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection(dataBaseName, con );
            }
        }  
    } 
    
    public Vector buscarXMonedas(String des) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        Vector VecMoneda = new Vector();

        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(dataBaseName);
            if (con != null){
                st = con.prepareStatement(buscarxmon);
                st.setString(1, des );
                
                VecMoneda = new Vector(); 
                rs = st.executeQuery();
                ////System.out.println(st);
                while (rs.next()){
                         m = new Moneda();
                         m.setCodMoneda(rs.getString("codmoneda"));
                         m.setNomMoneda(rs.getString("nommoneda"));
                         m.setInicial(rs.getString("inicialesmoneda"));
                         VecMoneda.add(m); 
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR MONEDA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection(dataBaseName, con );
            }
        } 
        return VecMoneda;
    }
    
    public void buscarMoneda(String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;

        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(dataBaseName);
            if (con != null){
                st = con.prepareStatement(busmon);
                st.setString(1, cod );
                
                rs = st.executeQuery();
                
                while (rs.next()){
                        m = new Moneda();
                        m.setCodMoneda(rs.getString("codmoneda"));
                        m.setNomMoneda(rs.getString("nommoneda"));
                        m.setInicial(rs.getString("inicialesmoneda"));
                        m=m;
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR MONEDA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection(dataBaseName, con );
            }
        }  
    }
    public boolean existeInicial(String ini) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(dataBaseName);
            if (con != null){
                st = con.prepareStatement(inicial);
                st.setString(1, ini );
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true; 
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR VERIFICAR SI EXISTE MONEDA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection(dataBaseName, con );
            }
        }  
        return sw;
    }  

}
