/*
 * MonedaService.java
 *
 * Created on 29 de diciembre de 2004, 03:18 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class MonedaService {
    
    MonedaDAO moneda;
    
    private Vector monedas = new Vector();
    /** Creates a new instance of MonedaService */
    public MonedaService() {
        moneda = new MonedaDAO();
    }
    public MonedaService(String dataBaseName) {
        moneda = new MonedaDAO(dataBaseName);
    }
    
    public Moneda getMoneda ()throws SQLException{
        return moneda.getMoneda();
    }
    public void buscarMoneda(String moneda, String moneda_cam, float valorCambiar)throws SQLException{
        this.moneda.searchMoneda(moneda, moneda_cam, valorCambiar);
    }   
    public Vector listarMonedas() throws SQLException {
        return moneda.listarMonedas();
    }
    ///
    public void buscarMonedaxCodigo(String cod) throws SQLException {
        moneda.buscarMonedaxCodigo(cod);
    }
    public boolean existeMoneda(String cod) throws SQLException {
        return moneda.existeMoneda(cod);
    }
     //*******************************Diogenes**************
    public void insertarMoneda(Moneda mon) throws SQLException {
            try{
                    moneda.setMoneda(mon);
                    moneda.insertarMoneda();
            }catch(SQLException e){
                    throw new SQLException(e.getMessage());
             }
    }
    
    public void modificarMoneda(Moneda mon) throws SQLException {
            try{
                    moneda.setMoneda(mon);
                    moneda.modificarMoneda();
            }catch(SQLException e){
                    throw new SQLException(e.getMessage());
             }
    }
    
    public void anularMoneda(Moneda mon) throws SQLException {
            try{
                    moneda.setMoneda(mon);
                    moneda.anularMoneda();
            }catch(SQLException e){
                    throw new SQLException(e.getMessage());
             }
    }
    
    public Vector buscarXMonedas(String des) throws SQLException {
            try{
                    return moneda.buscarXMonedas(des);
            }catch(SQLException e){
                    throw new SQLException(e.getMessage());
             }
    }
    public void buscarMoneda(String cod) throws SQLException {
            try{
                    moneda.buscarMoneda(cod);
            }catch(SQLException e){
                    throw new SQLException(e.getMessage());
             }
    }
    public boolean existeInicial(String ini) throws SQLException {
            try{
                    return moneda.existeInicial(ini);
            }catch(SQLException e){
                    throw new SQLException(e.getMessage());
             }
    }
    
    
    //David 23.12.05
     /**
     * Este m�todo retorna un TreeMap con los diferentes tipos de monedas de la bd
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public TreeMap obtenerMonedas() throws SQLException{
        TreeMap monedas = new TreeMap();
        
        Vector vmonedas = this.listarMonedas();
        
        for(int i=0; i< vmonedas.size(); i++){
                Moneda moneda = (Moneda)vmonedas.elementAt(i);
               // if( banc.getBanco().matches(banco) )
                        monedas.put(moneda.getNomMoneda(),moneda.getCodMoneda());
        } 
        return monedas;    
    }
    
    /**
     * Carga la propiedad monedas, la cual contiene un listado de registro del archivo monedas
     * @autor Tito Andr�s Maturana D.
     * @throws SQLException Si presenta un error en la conexion con la Base de Datos
     * version 1.0
     */
    public void cargarMonedas() throws SQLException{
        this.monedas=this.listarMonedas();
    }
    
    /**
     * Getter for property monedas.
     * @return Value of property monedas.
     */
    public java.util.Vector getMonedas() {
        return monedas;
    }
    
    /**
     * Setter for property monedas.
     * @param monedas New value of property monedas.
     */
    public void setMonedas(java.util.Vector monedas) {
        this.monedas = monedas;
    }
    
}
