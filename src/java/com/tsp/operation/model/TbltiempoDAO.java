/*
 * TBLTIEMPODAO.java
 *
 * Created on 19 de noviembre de 2004, 09:07 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  KREALES
 */
public class TbltiempoDAO {
    
    private List tbltiempos;
    private Tbltiempo tbltiempo;
    private Vector tiempos;
    /** Creates a new instance of TBLTIEMPODAO */
    public TbltiempoDAO() {
    }
    
    List list(){
        
        return tbltiempos;
    }
    Vector getTiempos(){
        
        return tiempos;
    }
    void searchTblTiempos(String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tbltiempos = null;
        PoolManager poolManager = null;
        tiempos = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  * from tbltiempo where sj = ? and presentacion='S' and presentacion_es='E' order by secuence ");
                st.setString(1,sj);
                
                rs = st.executeQuery();
                tbltiempos = new LinkedList();
                tiempos = new Vector();
                while(rs.next()){
                    tbltiempos.add(Tbltiempo.load(rs));
                    tiempos.add(Tbltiempo.load(rs));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIEMPOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    void searchTblTiemposAll(String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tbltiempos = null;
        PoolManager poolManager = null;
        tiempos = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  * from tbltiempo where sj = ?  order by secuence ");
                st.setString(1,sj);
                
                rs = st.executeQuery();
                tbltiempos = new LinkedList();
                tiempos = new Vector();
                while(rs.next()){
                    tbltiempos.add(Tbltiempo.load(rs));
                    tiempos.add(Tbltiempo.load(rs));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIEMPOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    int cantidadTiempo(String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tbltiempos = null;
        PoolManager poolManager = null;
        int cantidad=0;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  count(*) as cantidad from tbltiempo where sj = ? and presentacion='S' and presentacion_es='E' ");
                st.setString(1,sj);
                
                rs = st.executeQuery();
                tbltiempos = new LinkedList();
                tiempos = new Vector();
                if(rs.next()){
                    cantidad=rs.getInt("cantidad");
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL CONTEO DE LOS TIEMPOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return cantidad;
        
    }
    void searchTblTiemposSal(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tbltiempos = null;
        PoolManager poolManager = null;
        tiempos = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  * from tbltiempo where presentacion_es='S' and presentacion='S' and base =? order by secuence ");
                st.setString(1, base);
                rs = st.executeQuery();
                tbltiempos = new LinkedList();
                tiempos = new Vector();
                while(rs.next()){
                    tbltiempos.add(Tbltiempo.load(rs));
                    tiempos.add(Tbltiempo.load(rs));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIEMPOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    void searchTblTiemposEnt(String standard, String base )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tbltiempos = null;
        PoolManager poolManager = null;
        tiempos = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  * from tbltiempo where presentacion_es='E' and presentacion='S' and base =? and sj =? order by secuence ");
                st.setString(1, base);
                st.setString(2, standard);
                rs = st.executeQuery();
                tbltiempos = new LinkedList();
                tiempos = new Vector();
                while(rs.next()){
                    tbltiempos.add(Tbltiempo.load(rs));
                    tiempos.add(Tbltiempo.load(rs));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIEMPOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    int cantidadTiempo()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tbltiempos = null;
        PoolManager poolManager = null;
        int cantidad=0;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("Select  count(*) as cantidad from tbltiempo where presentacion_es='S' and presentacion='S'");
                
                rs = st.executeQuery();
                tbltiempos = new LinkedList();
                
                if(rs.next()){
                    cantidad=rs.getInt("cantidad");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL CONTEO DE LOS TIEMPOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return cantidad;
        
    }
    
    
    
}
