/*
 * Planilla_TiempoDAO.java
 *
 * Created on 24 de noviembre de 2004, 11:00 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  KREALES
 */
public class Planilla_TiempoDAO extends MainDAO{
    
    private Planilla_Tiempo pla_tiempo;
    
    /** Creates a new instance of Planilla_TiempoDAO */
    public Planilla_TiempoDAO() {
        super("Planilla_TiempoDAO.xml");
    }
    
    public void setPlaTiempo(Planilla_Tiempo pla_tiempo){
        this.pla_tiempo = pla_tiempo;
    }
    public Planilla_Tiempo  getPlaTiempo(){
        return this.pla_tiempo;
    }
    public String insertTiempo(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into planilla_tiempo (dstrct,pla,sj,cf_code,time_code,secuence,date_time_traffic,creation_date,creation_user,base) values(?,?,?,?,?,?,?,'now()',?,?)");
                st.setString(1,pla_tiempo.getDstrct());
                st.setString(2,pla_tiempo.getPla());
                st.setString(3,pla_tiempo.getSj());
                st.setString(4,pla_tiempo.getCf_code());
                st.setString(5,pla_tiempo.getTime_code());
                st.setInt(6,pla_tiempo.getSecuence());
                st.setString(7,pla_tiempo.getDate_time_traffic());
                st.setString(8,pla_tiempo.getCreation_user());
                st.setString(9,base);
                //st.executeUpdate();
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS PLANILLA-TIEMPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
        
    }
    /**
     *Metodo que anula los registros de tiempos relacionados con la 
     *planilla.
     *@autor: Karen Reales
     *@return : Comando para ser ejecutado
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String anularPlaTiempo()throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        try {
            st = this.crearPreparedStatement("SQL_ANULAR");
            st.setString(1,pla_tiempo.getPla());
            sql = st.toString();
            // st.executeUpdate();
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LAS PLANILLAS TIEMPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_ANULAR");
        }
        return sql ;
    }
    public void buscaUltimaFecha(String planilla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        pla_tiempo = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select date_time_traffic from planilla_tiempo where pla = ? order by secuence desc");
                st.setString(1,planilla);
                
                rs = st.executeQuery();
                
                
                if(rs.next()){
                    pla_tiempo = new Planilla_Tiempo();
                    pla_tiempo.setDate_time_traffic(rs.getString("date_time_traffic"));
                    //Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA ULTIMA FECHA  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public String updatePlaTiempo()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update planilla_tiempo set date_time_traffic=?, user_update=? where pla=? and time_code=? ");
                st.setString(1, pla_tiempo.getDate_time_traffic());
                st.setString(2, pla_tiempo.getCreation_user());
                st.setString(3,pla_tiempo.getPla());
                st.setString(4, pla_tiempo.getTime_code());
                sql = st.toString();
                //st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACION DE LAS PLANILLAS TIEMPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
}
