/*
 * BDOracleService.java
 *
 * Created on 20 de junio de 2005, 11:21 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.operation.model.beans.*;


/**
 *
 * @author  Jm
 */
public class BDOracleService {
    BDOracleDAO bdDAO;
    /** Creates a new instance of BDOracleService */
    public BDOracleService() {
        bdDAO = new BDOracleDAO();
    }
    public java.util.Vector getBd() {
        return bdDAO.getB();
    }
    
    /**
     * Setter for property b.
     * @param b New value of property b.
     */
    public void setBd(java.util.Vector b) {
        bdDAO.setB(b);
    }
    
    /**
     * Getter for property bd.
     * @return Value of property bd.
     */
    public com.tsp.operation.model.beans.BDOracle getB() {
        return bdDAO.getBd();
    }
    
    public void setB(com.tsp.operation.model.beans.BDOracle bd) {
        bdDAO.setBd(bd);
    }
    public void list( String recurso )throws SQLException{
        bdDAO.list(recurso);
    }
}
