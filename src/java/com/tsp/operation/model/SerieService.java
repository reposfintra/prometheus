package com.tsp.operation.model;

import com.tsp.operation.model.SerieDAO;
import com.tsp.operation.model.beans.Serie;
import java.sql.SQLException;

public class SerieService {

    private SerieDAO dao;

    public SerieService() {
        dao = new SerieDAO();
    }
    public SerieService(String dataBaseName) {
        dao = new SerieDAO(dataBaseName);
    }

    public Serie obtenerSerie(String document_type) throws SQLException {
        return dao.obtenerSerie(document_type);
    }

    public void actualizarSerie(Serie s) throws SQLException {
        dao.actualizarSerie(s);
    }
}
