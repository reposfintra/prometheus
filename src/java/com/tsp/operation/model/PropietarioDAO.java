/*
 * PropietarioDAO.java
 *
 * Created on 14 de junio de 2005, 10:13 PM
 */

package com.tsp.operation.model;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  Sandrameg
 */
public class PropietarioDAO {
    
    private List lstProp;
    private Propietario prop;
    private String dataBaseName = "";
    private static String SQLNIT =  "   SELECT N.NOMBRE , N.DIRECCION, N.TELEFONO , C.NOMCIU FROM NIT N, CIUDAD C WHERE N.ID_MIMS = ?    "+
                                    "   AND N.CODCIU = C.CODCIU                                                                         ";
    //henry
    private static final String SEDE_PAGO = "select c.nomciu as nomciu from proveedor pr left join ciudad c on (pr.codciu_cuenta=c.codciu) where pr.nit=? ";
    /** Creates a new instance of PropietarioDAO */
    public PropietarioDAO() {
    }
    public PropietarioDAO(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }
    
    public Propietario getPropietario(){
        return this.prop;
    }
    
    public void setPropietario( Propietario p ){
        this.prop = p;
    }
    
    public void insert()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            
            if(con!=null){
                st = con.prepareStatement("insert into identificacion_prop (dstrct, cedula, p_nombre, s_nombre, p_apellido, s_apellido, user_update, creation_user, base) values (?,?,?,?,?,?,?,?,?)");
                st.setString(1, prop.getdstrct());
                st.setString(2, prop.getCedula());
                st.setString(3, prop.getP_nombre());
                st.setString(4, prop.getS_nombre());
                st.setString(5, prop.getP_apellido());
                st.setString(6, prop.getS_apellido());
                st.setString(7, prop.getUser_update());
                st.setString(8, prop.getCreation_user());
                st.setString(9, prop.getBase());
                System.out.println(st.toString());
                st.executeUpdate();                
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL PROPIETARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
    
    public void update()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("update identificacion_prop set p_nombre = ?, s_nombre = ?, p_apellido = ?, s_apellido = ?, last_update='now()', user_update=? where cedula = ? and dstrct= ?");
                st.setString(1, prop.getP_nombre());
                st.setString(2, prop.getS_nombre());
                st.setString(3, prop.getP_apellido());
                st.setString(4, prop.getS_apellido());
                st.setString(5, prop.getUser_update());
                st.setString(6, prop.getCedula());
                st.setString(7, prop.getdstrct());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA MODIFICACION DEL PROPIETARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
    
    public void nullify()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("update identificacion_prop set reg_status='A', last_update='now()', user_update=? where cedula = ? and dstrct= ?");
                st.setString(1, prop.getUser_update());
                st.setString(2, prop.getCedula());
                st.setString(3, prop.getdstrct());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ANULACION DEL PROPIETARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
    
    public void buscar(String distrito)throws SQLException{
    	Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        prop = null;
        
        try {
       	    poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            
            if(con!=null){
            	st = con.prepareStatement("Select * from identificacion_prop where dstrct = ? and reg_status=''");
                st.setString(1,distrito);
                rs = st.executeQuery();
                
                if(rs.next()){
                    prop = new Propietario();
                    prop.setCedula(rs.getString("cedula"));
                    prop.setP_nombre(rs.getString("p_nombre"));
                    prop.setS_nombre(rs.getString("s_nombre"));
                    prop.setP_apellido(rs.getString("p_apellido"));
                    prop.setS_apellido(rs.getString("s_apellido"));
		    prop.setdstrct(rs.getString("dstrct"));
                }
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DEL PROPIETARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }    
    
    public void buscarxCedula(String cedula)throws SQLException{
    	Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        prop = null;
        
        try {
       	    poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            
            if(con!=null){
            	st = con.prepareStatement("Select * from identificacion_prop where cedula = ? and reg_status=''");
                st.setString(1,cedula);
                rs = st.executeQuery();
                
                if(rs.next()){
                    prop = new Propietario();
                    prop.setCedula(rs.getString("cedula"));
                    prop.setP_nombre(rs.getString("p_nombre"));
                    prop.setS_nombre(rs.getString("s_nombre"));
                    prop.setP_apellido(rs.getString("p_apellido"));
                    prop.setS_apellido(rs.getString("s_apellido"));
		    prop.setdstrct(rs.getString("dstrct"));
                }
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DEL PROPIETARIO X CEDULA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
    
    public void buscarxCedulaxDistrito(String cedula, String distrito)throws SQLException{
    	Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        prop = null;
        
        try {
       	    poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            
            if(con!=null){
            	st = con.prepareStatement("Select * from identificacion_prop where cedula = ? and dstrct = ? and reg_status=''");
                st.setString(1,cedula);
                st.setString(2, distrito);
                rs = st.executeQuery();
                
                if(rs.next()){
                    prop = new Propietario();
                    prop.setCedula(rs.getString("cedula"));
                    prop.setP_nombre(rs.getString("p_nombre"));
                    prop.setS_nombre(rs.getString("s_nombre"));
                    prop.setP_apellido(rs.getString("p_apellido"));
                    prop.setS_apellido(rs.getString("s_apellido"));
		    prop.setdstrct(rs.getString("dstrct"));
                }
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DEL PROPIETARIO X CEDULA Y DISTRITO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }    
    
    ////////////////////////////////////////Juan Consulta NIT
    public NitSot buscarPropietarioNIT(String id_mims)throws SQLException{
    	Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        NitSot nitS = null;
        
        try {
       	    poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            
            if(con!=null){
            	st = con.prepareStatement(this.SQLNIT);
                st.setString(1,id_mims);
                rs = st.executeQuery();
                
                if(rs.next()){
                    nitS = new NitSot();
                    nitS.setNombre(rs.getString("nombre"));
                    nitS.setDireccion(rs.getString("direccion"));
                    nitS.setTelefono(rs.getString("telefono"));
                    nitS.setCodciu(rs.getString("nomciu"));
                }
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DEL PROPIETARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        return nitS;
    }  
    
    ////Modificacion por placa 151005
     public void buscarxCedulaNit(String cedula)throws SQLException{
     Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        prop = null;

        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);

            if(con!=null){
             st = con.prepareStatement("select distinct " +
                                          "       cedula, " +
                                          "       direccion," +                                          
                                          "       C.nomciu as expced, " +
                                          "       F.nomciu as ciudad, " +
                                          "       N.nombre, " +
                                          "       telefono," +
                                          "       celular " +
                                          "from   " +
                                          "       nit N" +
                                          "       left join ciudad C on (N.expced=C.codciu)" +
                                          "       left join ciudad F on (N.codciu=F.codciu)" +
                                          "where " +
                                          "       cedula=?");
                st.setString(1,cedula);
                rs = st.executeQuery(); 
                boolean sw = false;
                if(rs.next()){
                    prop = new Propietario();
                    prop.setCedula(rs.getString("cedula"));
                    prop.setP_nombre(rs.getString("nombre"));
                    prop.setExpedicionced(rs.getString("expced"));                    
                    prop.setDireccion(rs.getString("direccion"));  
                    prop.setCiudad(rs.getString("ciudad"));
                    prop.setTelefono(rs.getString("telefono"));
                    prop.setCelular(rs.getString("celular"));
                    sw = true;
                }
                if (sw==true){
                    st = con.prepareStatement(SEDE_PAGO);
                    st.setString(1, cedula);
                    rs = st.executeQuery();
                    if (rs.next())
                        prop.setSedepago(rs.getString("nomciu"));
                    else
                        prop.setSedepago("");
                }
            }

        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DEL PROPIETARIO X CEDULA NIT" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }

            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
}
