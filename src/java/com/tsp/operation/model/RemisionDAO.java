
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

class RemisionDAO extends MainDAO {    
  
    //Diogenes 2006-04-11
    private static final String QUERY_MOVIMIENTO_OC_ANULADO=
    "SELECT  "+
    "       A.agency_id,       "+
    "      A.document_type,   "+
    "      A.document,        "+
    "      A.item,            "+
    "      c.concept_desc,    "+
    "      SUM(A.vlr) AS vlr," +
    "      SUM(A.vlr_for) AS vlr_for,         "+
    "      A.currency ,    " +
    "      A.branch_code," +
    "      A.bank_account_no," +
    "      A.date_doc"+
    " FROM                           "+
    "      MOVPLA         A," +
    "      TBLCON 	C          "+
    " WHERE                          "+
    "        A.reg_status='A'         "+
    "   AND  A.dstrct        =?      "+
    "   AND  A.document_type = '001' "+
    "   AND  A.concept_code in('01','02','03') "+
    "   AND  A.planilla      =? " +
    "   AND  C.dstrct = a.dstrct" +
    "   AND  C.concept_code = a.concept_code     "+
    "  group by  A.agency_id,  " +
    "          A.document_type,   " +
    "          A.document,        " +
    "          A.item,            " +
    "          c.concept_desc,    " +
    "          A.vlr_for,         " +
    "          A.currency ,     " +
    "          A.branch_code, " +
    "          A.bank_account_no," +
    "          A.date_doc       ";
    private static final String QUERY_MOVIMIENTO_OC="SELECT  "+
    "       A.agency_id,       "+
    "      A.document_type,   "+
    "      A.document,        "+
    "      A.item,            "+
    "      c.concept_desc,    "+
    "      SUM(A.vlr) AS vlr," +
    "      SUM(A.vlr_for) AS vlr_for,         "+
    "      A.currency ,    " +
    "      A.branch_code," +
    "      A.bank_account_no," +
    "      A.date_doc"+
    " FROM                           "+
    "      MOVPLA         A," +
    "      TBLCON 	C          "+
    " WHERE                          "+
    "        A.reg_status=''         "+
    "   AND  A.dstrct        =?      "+
    /*  "   AND  A.agency_id     =?      "+ 03-Ene-2006 Henry*/
    "   AND  A.document_type = '001' "+
    "   AND  A.concept_code in('01','02','03') "+
    "   AND  A.planilla      =? " +
    "   AND  C.dstrct = a.dstrct" +
    "   AND  C.concept_code = a.concept_code     "+
    "  group by  A.agency_id,  " +
    "          A.document_type,   " +
    "          A.document,        " +
    "          A.item,            " +
    "          c.concept_desc,    " +
    "          A.vlr_for,         " +
    "          A.currency ,     " +
    "          A.branch_code, " +
    "          A.bank_account_no," +
    "          A.date_doc       ";
    
    private static final String QUERY_EXISTE_SERIE=" SELECT  count(*)          "+
    " FROM                      "+
    "        SERIES             "+
    " WHERE                     "+
    "      reg_status  <> 'A'   "+
    " and  document_type ='001' ";
    
    
    
    
    private static final String  QUERY_REMISIONES= "SELECT          "+
    "   cia              AS DISTRITO,             "+
    "   numpla           AS REMISION,             "+
    "   fecpla           AS FECHA_DESPACHO,       "+
    "   agcpla           AS AGENCIA_DESPACHADORA, "+
    "   oripla           AS CODIGO_ORIGEN,        "+
    "   despla           AS CODIGO_DESTINO,       "+
    "   plaveh           AS PLACA,                "+
    "   cedcon           AS CODIGO_CONDUCTOR,     "+
    "   nitpro           AS CODIGO_PROPIETARIO,   "+
    "   pesoreal         AS CANTIDAD_CARGA,      "+
    "   unit_vlr         AS UNIDAD                "+
    " FROM                                        "+
    "      planilla                              "+
    " WHERE                                       "+
    "        reg_status  =''                      "+
    "   AND  printer_date= '0099-01-01 00:00:00'  "+
    "   AND  cia         = ?                      "+
    "   AND  agcpla      = ?     " +
    "   AND  base        = ?                 "+
    "ORDER BY numpla                              ";
    
    
    private static final String  SEARCH_ANTICIPO="SELECT                                         "+
    "   vlr                AS ANTICIPO_PESO,        "+
    "   vlr_for            AS ANTICIPO_EXTRANJERO,  "+
    "   currency           AS TIPO_MONEDA,          "+
    "   proveedor_anticipo AS PROVEEDOR,            "+
    "   sucursal           AS SUCURSAL              "+
    " FROM                                          "+
    "      MOVPLA                                   "+
    "  WHERE                                        "+
    "      reg_status=''                            "+
    " and  dstrct        = ?                        "+
    " and  agency_id     = ?                        "+
    " and  document_type = '001'                    "+
    " and  document      = ''                       "+
    " and  item          = ''                       "+
    " and  concept_code  = ?                        "+
    " and  planilla      = ?                        ";
    
    
    
    private static final String SEARCH_BANCOS=" SELECT                           "+
    "    BRANCH_CODE       AS  BANCO,  "+
    "    BANK_ACCOUNT_NO   AS  AGENCIA,"+
    "    ACCOUNT_NUMBER    AS CUENTA   "+
    " FROM                             "+
    "     BANCO                        "+
    " WHERE                            "+
    "      REG_STATUS =  ''            "+
    " AND  DSTRCT     =  ?             "+
    " AND  AGENCY_ID  =  ?             "+
    " AND  CURRENCY   =  ?    " +
    " and  base =?         ";
    
    
    
    private static final String SEARCH_SERIES=" SELECT                            "+
    "       PREFIX           AS PREFIJO,"+
    "       SERIAL_FISHED_NO AS TOPE,   "+
    "       LAST_NUMBER      AS ULTIMO  "+
    " FROM                              "+
    "       SERIES                      "+
    " WHERE                             "+
    "       REG_STATUS       =  ''      "+
    "  AND  DSTRCT           =   ?      "+
    "  AND  AGENCY_ID        =   ?      "+
    "  AND  DOCUMENT_TYPE    = '004'    "+
    "  AND  BRANCH_CODE      =   ?      "+
    "  AND  BANK_ACCOUNT_NO  =   ?      "+
    "  AND  ACCOUNT_NUMBER   =   ?      "+
    "  ORDER BY LAST_NUMBER             ";
    
    
    private static final String   SEARCH_AUXILIAR=" SELECT                                      "+
    "    acpm_gln        AS GALONES_ACPM,         "+
    "    acpm_supplier   AS NIT_PROVEEDOR_ACPM,   "+
    "    acpm_code       AS CODIGO_PROVEEDOR,     "+
    "    ticket_a        AS CANT_TICKET_A,        "+
    "    ticket_b        AS CANT_TICKET_B,        "+
    "    ticket_c        AS CANT_TICKET_C,        "+
    "    ticket_supplier AS PROVEEDOR_PEAJE       "+
    " FROM                                        "+
    "     PLAAUX                                  "+
    " WHERE                                       "+
    "        reg_status  = ''                     "+
    "   AND  dstrct      = ?                      "+
    "   AND  pla         = ?                      ";
    
    
    private static final String QUERY_VALOR_PEAJE  =" SELECT                 "+
    "          value         "+
    " FROM                   "+
    "     PEAJES             "+
    " WHERE                  "+
    "      reg_status =''    "+
    "  AND dstrct     = ?    "+
    "  AND ticket_id  = ?    ";
    
    
    public static final String  SEARCH_VALOR_ACPM=" SELECT                             "+
    "   acpm_value  AS VALOR_GALON_ACPM  "+
    " FROM                               "+
    "   PROVEEDOR_ACPM                   "+
    " WHERE                              "+
    "        reg_status       = ''       "+
    "   AND  dstrct           = ?        "+
    "   AND  nit              = ?        "+
    "   AND  codigo_proveedor = ?        ";
    
    
    private static final String  SEARCH_DATOS_REMESA="SELECT                               "+
    "   C.feccum       AS FECHA_CUMPLIDO, "+
    "   D.numrem       AS REMESA,         "+
    "   D.std_job_no   AS CONTRATO,       "+
    "   D.descripcion      AS CARGA,          "+
    "   D.cliente      AS CLIENTE,        "+
    "   C.agccum       AS AGENCIA_CUMPL,  "+
    "   D.remision     AS NO_REMISION, " +
    "   d.qty_value," +
    "   d.reg_status  "+
    " FROM                                "+
    "      plarem           C,           "+
    "      remesa           D           "+
    " WHERE                               "+
    //"        C.reg_status  =''            "+
    "     C.cia         = ?            "+
    "   AND  C.numpla      = ?         " +
    "   AND  C.numrem      =?   "+
    //"   AND  D.reg_status  =''            "+
    "   AND  D.cia         = C.cia        "+
    "   AND  D.numrem      = C.numrem     ";
    
    
    
    private static final String QUERY_NAME_PROVEEDOR="SELECT payment_name FROM PROVEEDOR WHERE reg_status=''  AND dstrct=? AND nit=?";
    private static final String QUERY_NAME_AGENCIA  ="SELECT nombre       FROM AGENCIA   WHERE dstrct=? AND id_mims=?";
    private static final String QUERY_NAME_NIT      ="SELECT nombre       FROM NIT       WHERE cedula=?";
    private static final String QUERY_NAME_CIUDAD   ="SELECT nomciu       FROM CIUDAD    WHERE codciu=?";
    private static final String QUERY_TEXTO_OC      ="SELECT texto_oc     FROM PROYECTO  WHERE reg_status='' AND dstrct=? AND project=?";
    
    
    
    //UPDATE
    
    private static final String UPDATE_OC="UPDATE planilla SET printer_date=now() WHERE reg_status='' AND cia=?  AND numpla=?";
    private static final String UPDATE_PLAREM="UPDATE plarem SET feccum=now() WHERE reg_status='' AND cia=?  AND numpla=?";
    
    private static final String UPDATE_SERIE="UPDATE SERIES              "+
    " SET last_number  =?,      "+
    "     user_update  =?,      "+
    "     last_update  = now()  "+
    "WHERE                      "+
    "      reg_status  = ''     "+
    " and  dstrct          = ?  "+
    " and  agency_id       = ?  "+
    " and  document_type ='001' "+
    " and  branch_code     = ?  "+
    " and  bank_account_no = ?  "+
    " and  account_number  = ?  ";
    
    
    
    private static final String UPDATE_FINALIZAR_SERIE="UPDATE SERIES  "+
    "   SET reg_status='A',       "+
    "     user_update  =?,        "+
    "     last_update  = now()    "+
    "WHERE                        "+
    "      reg_status  = ''       "+
    " and  dstrct          = ?    "+
    " and  agency_id       = ?    "+
    " and  document_type ='001'   "+
    " and  branch_code     = ?    "+
    " and  bank_account_no = ?    "+
    " and  account_number  = ?    ";
    
    
    
    private static final String UPDATE_MOVOC="UPDATE MOVPLA           "+
    " SET  document      = ?,    "+
    "      item          = ?,    "+
    "      user_update   = ?,    "+
    "      last_update   = now() "+
    "WHERE                       "+
    "      reg_status=''         "+
    " and  dstrct        =?      "+
    " and  agency_id     =?      "+
    " and  document_type = '001' "+
    " and  planilla      =?      "+
    " and  concept_code  =       ";
    
    //INSERT
    
    private static final String INSERT_EGRESO="INSERT INTO EGRESO "+
    "( DSTRCT,          "+
    "  BRANCH_CODE,     "+
    "  BANK_ACCOUNT_NO, "+
    "  DOCUMENT_NO,     "+
    "  NIT,             "+
    "  PAYMENT_NAME,    "+
    "  AGENCY_ID,       "+
    "  CONCEPT_CODE,    "+
    "  VLR,             "+
    "  VLR_FOR,         "+
    "  CURRENCY,        "+
    "  CREATION_DATE,   "+
    "  CREATION_USER)   "+
    "VALUES "+
    "(?,?,?,?,?,?,?,?,?,?,?,now(),?)";
    
    
    
    private static final String INSERT_DETALLE_EGRESO="INSERT INTO EGRESODET"+
    "(DSTRCT,          "+
    " BRANCH_CODE,     "+
    " BANK_ACCOUNT_NO, "+
    " DOCUMENT_NO,     "+
    " ITEM_NO,         "+
    " concept_code,    "+
    " description,     "+
    " VLR,             "+
    " VLR_FOR,         "+
    " CURRENCY,        "+
    " OC,              "+
    " creation_date,   "+
    " CREATION_USER)   "+
    "VALUES "+
    "(?,?,?,?,?,?,?,?,?,?,?,now(),?)";
    
    private static final String QUERY_VALOR_CHEQUE="SELECT VLR_FOR AS VLR " +
    " FROM MOVPLA" +
    "   WHERE PLANILLA=?" +
    "   AND DOCUMENT =?" +
    "   AND VLR_FOR>0 ";
    
    private static final String  SEARCH_DATOS_REMESA2="SELECT                               "+
    "   C.feccum       AS FECHA_CUMPLIDO, "+
    "   D.numrem       AS REMESA,         "+
    "   D.std_job_no   AS CONTRATO,       "+
    "   D.descripcion      AS CARGA,          "+
    "   D.cliente      AS CLIENTE,        "+
    "   C.agccum       AS AGENCIA_CUMPL,  "+
    "   D.remision     AS NO_REMISION     "+
    " FROM                                "+
    "      plarem           C,           "+
    "      remesa           D           "+
    " WHERE                               "+
    "        C.reg_status  =''            "+
    "   AND  C.cia         = ?            "+
    "   AND  C.numpla      = ?            "+
    //"   AND  D.reg_status  =''            "+
    "   AND  D.cia         = C.cia        "+
    "   AND  D.numrem      = C.numrem     ";
    
    
    //____________________________________________________________________________________________
    //                                          METODOS
    //____________________________________________________________________________________________
    
    
    RemisionDAO() {
        super( "RemisionDAO.xml" );
    }
    
    synchronized boolean ExisteSerie()throws SQLException {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        int cant=0;
        boolean estado=true;
        try{
            st= conPostgres.prepareStatement(QUERY_EXISTE_SERIE);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next())
                    cant=rs.getInt(1);
            }
            if(cant==0)
                estado=false;
        }catch(Exception e){
            throw new SQLException("No se pudo saber si existen SERIES disponibles para BANCOS "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return estado;
    }
    
    
    synchronized List  QueryRemisiones(String proyecto, String distrito,String agencia, String login, String base) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        List listaRemision = null;
        try{
            st= conPostgres.prepareStatement(QUERY_REMISIONES);
            st.setString(1, distrito);
            st.setString(2, agencia);
            st.setString(3, base);
            ////System.out.println("Query remisiones "+st.toString());
            rs=st.executeQuery();
            if(rs!=null){
                ////System.out.println("---------SI HAY REMISIONES");
                listaRemision = new LinkedList();
                int fer=1;
                while(rs.next()){
                    ////System.out.println("---------leyendo remisiones");
                    Remision  remision= new Remision();
                    remision.setSeleted(false);
                    remision.setPrinter(false);
                    remision.setDistrito(rs.getString(1));
                    remision.setRemision(rs.getString(2));
                    
                    ////System.out.println("Planilla : "+rs.getString(2)+"\n");
                    
                    remision.setFechaEntrada(rs.getString(3));
                    remision.setAgencia(rs.getString(4));
                    remision.setOrigen(BuscarNombre(rs.getString(1),rs.getString(5),4 ) );
                    remision.setDestino(BuscarNombre(rs.getString(1),rs.getString(6),4 ) );
                    remision.setPlaca(rs.getString(7));
                    remision.setConductor( BuscarNombre(rs.getString(1), rs.getString(8), 1 ) );
                    remision.setNit(rs.getString(8));
                    remision.setPropietario( this.nombreProp( rs.getString(9))   );
                    remision.setCantidadCarga(rs.getDouble(10));
                    remision.setUnidadValorizacion(rs.getString(11));
                    remision.setFechaHoy( Util.getFechaActual_String(5) +"/"+ Util.getFechaActual_String(3)+"/"+ Util.getFechaActual_String(1)  );
                    remision.setUsuario(login);
                    remision.setProveedorACPM( "" );
                    remision.setSucursalACPM( "");
                    remision.setProveedorpeaje( "" );
                    remision.setSucursalPeaje("");
                    remision.setProveedorAnticipo("");
                    remision.setSucursalAnticipo( "");
                    
                    //--- Anticipos
                    ValorAnticipo anticipo           = this.serachAnticipo(remision.getDistrito(), agencia, remision.getRemision(),"01");
                    ValorAnticipo anticipoACPM       = this.serachAnticipo(remision.getDistrito(), agencia, remision.getRemision(),"02");
                    ValorAnticipo anticipoPeaje      = this.serachAnticipo(remision.getDistrito(), agencia, remision.getRemision(),"03");
                    ValorAnticipo anticipoDescuento  = this.serachAnticipo(remision.getDistrito(), agencia, remision.getRemision(),"09");
                    
                    double valor       = 0;
                    double valorACPM   = 0;
                    double valorPeaje  = 0;
                    double descuento   = 0;
                    
                    String moneda="PES";
                    
                    if(anticipo!=null){
                        ////System.out.println("---------ANTICIPO NO ES NULL");
                        valor     =(anticipo.getVlr()==0)?anticipo.getVlrFor():anticipo.getVlr();
                        moneda=anticipo.getCurrency();
                        remision.setProveedorAnticipo(BuscarNombre(remision.getDistrito(), anticipo.getProveedor(),1));
                        remision.setSucursalAnticipo( anticipo.getSucursal());
                    }
                    if(anticipoACPM!=null){
                        ////System.out.println("---------ACPM NO ES NULL");
                        valorACPM =(anticipoACPM.getVlr()==0)?anticipoACPM.getVlrFor():anticipoACPM.getVlr();
                        remision.setProveedorACPM( BuscarNombre(remision.getDistrito(),anticipoACPM.getProveedor(),1) );
                        remision.setSucursalACPM( anticipoACPM.getSucursal());
                    }
                    if(anticipoPeaje!=null){
                        ////System.out.println("---------PEAJES NO ES NULL");
                        valorPeaje=(anticipoPeaje.getVlr()==0)?anticipoPeaje.getVlrFor():anticipoPeaje.getVlr();
                        remision.setProveedorpeaje( BuscarNombre(remision.getDistrito(), anticipoPeaje.getProveedor() ,1) );
                        remision.setSucursalPeaje(anticipoPeaje.getSucursal());
                    }
                    
                    if(anticipoDescuento!=null){
                        ////System.out.println("---------AJUSTE NO ES NULL");
                        descuento = (anticipoDescuento.getVlr()==0)?anticipoDescuento.getVlrFor():anticipoDescuento.getVlr();
                    }
                    double valorRed=Util.redondear(valor + descuento,0);
                    remision.setTotal( valorRed + valorACPM + valorPeaje);  // (01 +09+ 02 + 03  )
                    remision.setanticipo(valorRed);          // (01 + 09)
                    
                    remision.setTipoMoneda( moneda);
                    
                    
                    
                    //--- Banco y Serie
                    
                    Banco banco = this.searchBancos(remision.getDistrito(), agencia, remision.getTipoMoneda(),base);
                    if(banco!=null){
                        ////System.out.println("---------BANCO NO ES NULO");
                        remision.setBanco( banco.getBanco());
                        remision.setAgeBanco( banco.getNombre_Agencia());
                        remision.setCuentaBanco( banco.getCuenta() );
                        Series  serie = this.getSeries(remision.getDistrito(), agencia, remision.getBanco(), remision.getAgeBanco(), remision.getCuentaBanco());
                        ////System.out.println("Busco la serie por "+remision.getDistrito()+" "+ agencia+" "+ remision.getBanco()+" "+ remision.getAgeBanco()+" "+ remision.getCuentaBanco());
                        if(serie!=null){
                            ////System.out.println("Serie no es nula");
                            remision.setPrefix( serie.getPrefijo());
                            remision.setEgreso( serie.getPrefijo() + String.valueOf((serie.getLast_number()+1)));
                            
                            //--- Datos Auxiliares
                            Plaaux  aux = this.searchAux(remision.getDistrito(), remision.getRemision());
                            if(aux!=null){
                                ////System.out.println("Plaaux no es nula");
                                remision.setcantidadACPM((double)aux.getAcpm_gln());
                                // remision.setProveedorACPM( BuscarNombre(remision.getDistrito(),aux.getAcpm_supplier(),1) );
                                remision.setValorACPM( valorACPM);
                                
                                double cantPeaje = aux.getTicket_a() + aux.getTicket_b() + aux.getTicket_c();
                                remision.setCantidadPeajes(cantPeaje);
                                remision.setValorPeajes(valorPeaje);
                                //    remision.setProveedorpeaje( BuscarNombre(remision.getDistrito(),aux.getTicket_supplier(),1) );
                            }
                            //--- texto de la oc
                            remision.setTextoOc(BuscarTexto(remision.getDistrito(),proyecto));
                            //--- Datos de la remesa
                            DatosRem remesa = this.getDatosRemision( remision.getDistrito(), remision.getRemision());
                            if(remesa!=null){
                                remision.setFechaCumplido( remesa.getFechaCumplido().substring(0,16));
                                remision.setRemesa( remesa.getRemesa());
                                remision.setcontrato( remesa.getStj());
                                remision.setCarga( remesa.getDescripcion());
                                remision.setNoRemision( remesa.getNoRemision());
                            }
                            //if(remision.getAnticipo()>0 && remision.getBanco()!=null){
                            String letra="";
                            String[] vector =  RMCantidadEnLetras.getTexto( remision.getTotal() );
                            for(int i=0;i<vector.length;i++)
                                letra+=vector[i];
                            remision.setMonto(letra);
                            remision.setId(fer);
                            fer+=1;
                            listaRemision.add(remision);
                            ////System.out.println("Agrego una remision a la lista");
                            //}
                        }
                    }
                }
            }
            ////System.out.println("la cantidad de remisiones encontradas es: "+listaRemision.size());
        }
        catch(SQLException e){
            throw new SQLException("Error en la consulta de Remisiones " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return listaRemision;
    }
    
    
    
    ValorAnticipo serachAnticipo(String distrito, String agencia, String planilla ,String concepto)throws SQLException{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        ValorAnticipo anticipo = null;
        try{
            st= conPostgres.prepareStatement(SEARCH_ANTICIPO);
            st.setString(1, distrito);
            st.setString(2, agencia);
            st.setString(3, concepto);
            st.setString(4, planilla);
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next()){
                    anticipo = new ValorAnticipo();
                    anticipo.setVlr( rs.getDouble(1));
                    anticipo.setVlrFor(rs.getDouble(2));
                    anticipo.setCurrency( rs.getString(3));
                    anticipo.setProveedor(rs.getString(4));
                    anticipo.setSucursal(rs.getString(5));
                    break;
                }
        }catch(Exception e){
            throw new SQLException("Error en la busqueda del anticipo --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return anticipo;
    }
    
    
    
    
    Banco searchBancos(String distrito, String agencia, String moneda,String base) throws SQLException{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        Banco  banco=null;
        try{
            st= conPostgres.prepareStatement(SEARCH_BANCOS);
            st.setString(1, distrito);
            st.setString(2, agencia);
            st.setString(3, moneda);
            st.setString(4, base);
            ////System.out.println("Busca Bancos "+st.toString());
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next()){
                    banco = new Banco();
                    banco.setBanco(rs.getString(1));
                    banco.setNombre_Agencia(rs.getString(2));
                    banco.setCuenta( rs.getString(3));
                    break;
                }
        }catch(Exception e){
            throw new SQLException("Error en la busqueda del banco --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return banco;
        
    }
    
    
    
    Series  getSeries(String distrito, String agencia, String banco, String agenciaBanco, String cuenta) throws SQLException{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        Series  serie=null;
        ////System.out.print(">>>>> " + distrito +"/"+ agencia +"/"+ banco +"/"+ agenciaBanco +"/"+ cuenta);
        
        try{
            st= conPostgres.prepareStatement(SEARCH_SERIES);
            st.setString(1, distrito);
            st.setString(2, agencia);
            st.setString(3, banco);
            st.setString(4, agenciaBanco);
            st.setString(5, cuenta);
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next()){
                    serie = new Series();
                    serie.setPrefijo( rs.getString(1) );
                    serie.setSerial_fished_no( rs.getString(2) );
                    serie.setLast_number( rs.getInt(3) );
                    break;
                }
            
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de la serie para el banco "+ banco +" --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return serie;
    }
    
    
    
    Plaaux searchAux(String distrito, String planilla) throws SQLException{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        Plaaux  aux=null;
        try{
            st= conPostgres.prepareStatement(SEARCH_AUXILIAR);
            st.setString(1, distrito);
            st.setString(2, planilla);
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next()){
                    aux = new Plaaux();
                    aux.setAcpm_gln(rs.getFloat(1));
                    aux.setAcpm_supplier(rs.getString(2));
                    aux.setAcpm_code(rs.getString(3));
                    aux.setTicket_a(rs.getFloat(4));
                    aux.setTicket_b(rs.getFloat(5));
                    aux.setTicket_c(rs.getFloat(6));
                    aux.setTicket_supplier(rs.getString(7));
                    break;
                }
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de datos Auxiliares para la planilla --->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return aux;
    }
    
    
    
    double valorPeajes(String distrito,String tipo)throws SQLException {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        double valor=0;
        try{
            st= conPostgres.prepareStatement(QUERY_VALOR_PEAJE);
            st.setString(1,distrito);
            st.setString(2,tipo);
            rs=st.executeQuery();
            while(rs.next())
                valor=rs.getDouble(1);
        }catch(Exception e){
            throw new SQLException("No se pudo buscar el valor del peaje tipo: "+ tipo + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return valor;
    }
    
    
    
    double valorACPM(String distrito,String nit, String codigo)throws SQLException {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        double valor=0;
        try{
            st= conPostgres.prepareStatement(SEARCH_VALOR_ACPM);
            st.setString(1,distrito);
            st.setString(2,nit);
            st.setString(3,codigo);
            rs=st.executeQuery();
            while(rs.next())
                valor=rs.getDouble(1);
        }catch(Exception e){
            throw new SQLException("No se pudo buscar el valor del acpm "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return valor;
    }
    
    
    
    String BuscarNombre(String distrito,String nit, int tipo)throws SQLException {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        String nombre="";
        try{
            switch (tipo){
                case 1://conductor
                    st= conPostgres.prepareStatement(QUERY_NAME_NIT);
                    st.setString(1,nit);
                    break;
                case 2://propietario y proveedor acpm
                    st= conPostgres.prepareStatement(QUERY_NAME_PROVEEDOR);
                    st.setString(1,distrito);
                    st.setString(2,nit);
                    break;
                case 4://ciudades
                    st= conPostgres.prepareStatement(QUERY_NAME_CIUDAD);
                    st.setString(1,nit);
                    break;
                case 5://agencias
                    st= conPostgres.prepareStatement(QUERY_NAME_AGENCIA);
                    st.setString(1,distrito);
                    st.setString(2,nit);
                    break;
            }
            rs=st.executeQuery();
            while(rs.next())
                nombre=rs.getString(1);
        }catch(Exception e){
            throw new SQLException("Error intentando buscar el nombre correspondiente al NIT " + nit + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return nombre;
    }
    
    
    
    String BuscarTexto(String distrito,String proyecto)throws SQLException {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        String texto="";
        try{
            st= conPostgres.prepareStatement(QUERY_TEXTO_OC);
            st.setString(1,distrito);
            st.setString(2,proyecto);
            rs=st.executeQuery();
            while(rs.next())
                texto=rs.getString(1);
        }catch(Exception e){
            throw new SQLException("No se pudo buscar el TEXTO de la OC para el proyecto "+ proyecto + " y distrito " + distrito + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return texto;
    }
    
    
    
    DatosRem  getDatosRem(String distrito, String planilla, String numrem) throws SQLException{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        DatosRem  remesa=null;
        try{
            st= conPostgres.prepareStatement(SEARCH_DATOS_REMESA);
            st.setString(1, distrito);
            st.setString(2, planilla);
            st.setString(3, numrem);
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next()){
                    remesa = new DatosRem();
                    remesa.setFechaCumplido( rs.getString(1));
                    remesa.setRemesa( rs.getString(2));
                    remesa.setStj( rs.getString(3));
                    remesa.setDescripcion( rs.getString(4));
                    remesa.setCliente( rs.getString(5));
                    remesa.setAgenciaCumplido( rs.getString(6));
                    remesa.setNoRemision( rs.getString(7));
                    remesa.setTarifa(rs.getFloat("qty_value"));
                    remesa.setEstado(rs.getString("reg_status").equals("A")?"Anulada":"Activo");
                    break;
                }
            
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de datos de la remesa asociada a la planilla " + planilla +" -->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return remesa;
    }
    DatosRem  getDatosRem(String distrito, String planilla) throws SQLException{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        DatosRem  remesa=null;
        try{
            st= conPostgres.prepareStatement(SEARCH_DATOS_REMESA);
            st.setString(1, distrito);
            st.setString(2, planilla);
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next()){
                    remesa = new DatosRem();
                    remesa.setFechaCumplido( rs.getString(1));
                    remesa.setRemesa( rs.getString(2));
                    remesa.setStj( rs.getString(3));
                    remesa.setDescripcion( rs.getString(4));
                    remesa.setCliente( rs.getString(5));
                    remesa.setAgenciaCumplido( rs.getString(6));
                    remesa.setNoRemision( rs.getString(7));
                    break;
                }
            
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de datos de la remesa asociada a la planilla" + planilla +" -->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return remesa;
    }
    
    
    
    
    
    
    // UPDATE
    
    
    
    synchronized void ActualizarRemision(String remision, String distrito)throws SQLException {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        try{
            st= conPostgres.prepareStatement(UPDATE_OC);
            st.setString(1,distrito);
            st.setString(2,remision);
            st.executeUpdate();
            
            st= conPostgres.prepareStatement(UPDATE_PLAREM);
            st.setString(1,distrito);
            st.setString(2,remision);
            st.executeUpdate();
        }catch(Exception e){
            throw new SQLException("No se pudo actualizar en planilla " + e.getMessage());
        }
        finally{
            if(st!=null)st.close();
            if(rs!=null)rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    
    void ActualizarSeries(String distrito, String agencia, String banco, String ageBanco, String cuenta,String usuario)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres  == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        try{
            //--- buscamos nuevamente el last_number
            Series serie = this.getSeries(distrito, agencia, banco,ageBanco,cuenta);
            int ultimo   = serie.getLast_number();
            
            //--- realizamos la actualizacion
            st= conPostgres.prepareStatement(UPDATE_SERIE);
            st.setInt(1, ultimo+1);
            st.setString(2, usuario);
            st.setString(3, distrito);
            st.setString(4, agencia);
            st.setString(5, banco);
            st.setString(6, ageBanco);
            st.setString(7, cuenta);
            st.executeUpdate();
        }catch(Exception e){
            throw new SQLException("No se pudo actualizar la SERIE " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        
    }
    
    
    
    void FinalizarSeries(String distrito, String agencia, String banco, String ageBanco, String cuenta,String usuario)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        try{
            st= conPostgres.prepareStatement(UPDATE_FINALIZAR_SERIE);
            st.setString(1, usuario);
            st.setString(2, distrito);
            st.setString(3, agencia);
            st.setString(4, banco);
            st.setString(5, ageBanco);
            st.setString(6, cuenta);
            st.executeUpdate();
        }catch(Exception e){
            throw new SQLException("No se pudo finalizar la SERIE "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    synchronized void ActualizarMOVOC(String distrito,String agencia,String egreso,String item,String oc,String login)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        try{
            st= conPostgres.prepareStatement(UPDATE_MOVOC +"'"+item+"'");
            st.setString(1,egreso);
            st.setString(2, item);
            st.setString(3,login);
            st.setString(4,distrito);
            st.setString(5,agencia);
            st.setString(6,oc);
            
            st.executeUpdate();
        }catch(Exception e){
            throw new SQLException("No se pudo actualizar en MOVOC "+st.toString()+" "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    
    float buscarValorCheque(String oc, String documento) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        float valorCheque=0;
        String valor ="No se encontro valor";
        try{
            st= conPostgres.prepareStatement(this.QUERY_VALOR_CHEQUE);
            st.setString(1,oc);
            st.setString(2,documento);
            rs=st.executeQuery();
            if(rs.next()){
                valorCheque=rs.getFloat("vlr");
            }
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda de Movimientos para la planilla "+oc +" --> "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return valorCheque;
    }
    synchronized void ActualizarEgreso(String distrito, String banco, String ageBanco,String egreso,String concepto,String nit,String nombre, String agencia,double valor1, double valor2,String moneda,String login)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        try{
            st= conPostgres.prepareStatement(INSERT_EGRESO);
            st.setString(1, distrito);
            st.setString(2, banco);
            st.setString(3, ageBanco);
            st.setString(4, egreso);
            st.setString(5, nit);
            st.setString(6, nombre);
            st.setString(7, agencia);
            st.setString(8, concepto);
            st.setDouble(9, valor1);
            st.setDouble(10,valor2);
            st.setString(11,moneda);
            st.setString(12,login);
            st.executeUpdate();
        }catch(Exception e){
            throw new SQLException("No se pudo Actualizar en Egreso " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        
    }
    
    
    
    
    
    synchronized void ActualizarDetalleEgreso(String distrito, String banco, String ageBanco,String egreso,String item,String concepto,String oc,double valor1, double valor2,String moneda,String login)throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        try{
            st= conPostgres.prepareStatement(INSERT_DETALLE_EGRESO);
            st.setString(1, distrito);
            st.setString(2, banco);
            st.setString(3, ageBanco);
            st.setString(4, egreso);
            st.setString(5, item);
            st.setString(6, concepto);
            st.setString(7, "Anticipo Planilla "+ oc);
            st.setDouble(8, valor1);
            st.setDouble(9, valor2);
            st.setString(10,moneda);
            st.setString(11,oc);
            st.setString(12,login);
            st.executeUpdate();
        }catch(Exception e){
            throw new SQLException("No se pudo actualizar en EGRESODET " + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        
    }
    public String nombreProp(String cedula) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        String nombre = "No se encontro";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement("select p_nombre||' '||s_nombre||' '||p_apellido||' '||s_apellido as nombre from identificacion_prop where cedula = ?");
            psttm.setString(1,cedula);
            rs = psttm.executeQuery();
            if (rs.next()){
                nombre = rs.getString("nombre");
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NOMBRE DEL PROPIETARIO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return nombre;
    }
    
    DatosRem  getDatosRemision(String distrito, String planilla) throws SQLException{
        PoolManager poolManager  = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        DatosRem  remesa=null;
        try{
            st= conPostgres.prepareStatement(SEARCH_DATOS_REMESA2);
            st.setString(1, distrito);
            st.setString(2, planilla);
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next()){
                    remesa = new DatosRem();
                    remesa.setFechaCumplido( rs.getString(1));
                    remesa.setRemesa( rs.getString(2));
                    remesa.setStj( rs.getString(3));
                    remesa.setDescripcion( rs.getString(4));
                    remesa.setCliente( rs.getString(5));
                    remesa.setAgenciaCumplido( rs.getString(6));
                    remesa.setNoRemision( rs.getString(7));
                    break;
                }
            
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de datos de la remesa asociada a la planilla" + planilla +" -->" + e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return remesa;
    }
    /**
     * Metodo buscarExtrafletes, lista los exatrafletes dependiendo de una planilla, stdjob y dstrict,
     * @param: distrito, numpla, standar
     * @Modificado : Ing. Karen Reales - 2006-10-18
     * @version : 1.0
     */
    List BuscarMovimiento(String oc, String distrito, String agencia) throws SQLException{
        
        Connection conPostgres = this.conectar("QUERY_MOVIMIENTO_OC");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        List lista=null;
        try{
            st= conPostgres.prepareStatement(this.obtenerSQL("QUERY_MOVIMIENTO_OC"));
            st.setString(1,distrito);
            st.setString(2,oc);
            
            rs=st.executeQuery();
            lista = new LinkedList();
            if(rs!=null){
                
                while(rs.next()){
                    AnticipoPlanilla  movimiento = new AnticipoPlanilla();
                    movimiento.setPlanilla(oc);
                    movimiento.setAgencia(rs.getString(1));
                    movimiento.setTipoDocumento(rs.getString(2));
                    movimiento.setDocumento(rs.getString(3));
                    movimiento.setItem(rs.getString(4));
                    movimiento.setConcepto(rs.getString(5));
                    double valor=((int)rs.getDouble(6)==0)?rs.getDouble(7):rs.getDouble(6);
                    movimiento.setValor(valor);
                    movimiento.setMoneda(rs.getString(8));
                    movimiento.setBanco(rs.getString(9) );
                    movimiento.setCuenta(rs.getString(10));
                    movimiento.setAgenciaBanco(rs.getString(1) );
                    movimiento.setDate_doc(rs.getString("date_doc"));
                    movimiento.setReg_status("");
                    movimiento.setValor_for(rs.getDouble("vlr_for"));
                    if( (int)movimiento.getValor_for() == 0 ){
                        String cheque = !movimiento.getDocumento().equals ("")?" CHEQUE":" ANTICIPOS";
			movimiento.setReg_status ("ANULADO "+ cheque );
                        movimiento.setDescripcion("ANULADO VALOR: "+com.tsp.util.Util.customFormat(buscarValorCheque(oc,rs.getString(3))));
                    }
                    else
                        movimiento.setDescripcion("");
                    lista.add(movimiento);
                    
                    
                }
            }
            
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda de Movimientos para la planilla "+oc +" --> "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("QUERY_MOVIMIENTO_OC");
        }
        return lista;
    }
/*
    
    List BuscarMovimiento(String oc, String distrito, String agencia) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null, st2 = null;
        ResultSet rs=null, rs2=null;
        List lista=null;
        boolean sw = false;
        try{
            st= conPostgres.prepareStatement(QUERY_MOVIMIENTO_OC);
            st.setString(1,distrito);
            st.setString(2,oc);
            st2= conPostgres.prepareStatement(QUERY_MOVIMIENTO_OC_ANULADO);
            st2.setString(1,distrito);
            st2.setString(2,oc);
            rs2=st2.executeQuery();
            
            if(rs2.next()){
                sw=true;
            }
            rs=st.executeQuery();
            lista = new LinkedList();
            if(rs!=null){
                
                while(rs.next()){
                    AnticipoPlanilla  movimiento = new AnticipoPlanilla();
                    movimiento.setPlanilla(oc);
                    movimiento.setAgencia(rs.getString(1));
                    movimiento.setTipoDocumento(rs.getString(2));
                    movimiento.setDocumento(rs.getString(3));
                    movimiento.setItem(rs.getString(4));
                    movimiento.setConcepto(rs.getString(5));
                    double valor=((int)rs.getDouble(6)==0)?rs.getDouble(7):rs.getDouble(6);
                    movimiento.setValor(rs.getDouble(6));
                    movimiento.setValor_for(rs.getDouble(7));
                    movimiento.setMoneda(rs.getString(8));
                    movimiento.setBanco(rs.getString(9) );
                    movimiento.setCuenta(rs.getString(10));
                    movimiento.setAgenciaBanco(rs.getString(1) );
                    movimiento.setDate_doc(rs.getString("date_doc"));
                    movimiento.setReg_status("");
                    if(valor==0){
                        movimiento.setDescripcion("ANULADO VALOR: "+com.tsp.util.Util.customFormat(buscarValorCheque(oc,rs.getString(3))));
                    }
                    else
                        movimiento.setDescripcion("");
                    lista.add(movimiento);
                    rs2=st2.executeQuery();
                    if(rs2!=null && sw ){
                        if(rs2.next()){
                            movimiento.setPlanilla(oc);
                            movimiento.setAgencia(rs2.getString(1));
                            movimiento.setTipoDocumento(rs2.getString(2));
                            movimiento.setDocumento(rs2.getString(3));
                            movimiento.setItem(rs2.getString(4));
                            movimiento.setConcepto(rs2.getString(5));
                            valor=((int)rs2.getDouble(6)==0)?rs2.getDouble(7):rs2.getDouble(6);
                            movimiento.setValor(rs.getDouble(6));
                            movimiento.setValor_for(rs.getDouble(7));
                            movimiento.setMoneda(rs2.getString(8));
                            movimiento.setBanco(rs2.getString(9) );
                            movimiento.setCuenta(rs2.getString(10));
                            movimiento.setAgenciaBanco(rs2.getString(1) );
                            movimiento.setDate_doc(rs2.getString("date_doc"));
                            movimiento.setReg_status("A");
                            if(valor==0){
                                movimiento.setDescripcion("ANULADO VALOR: "+com.tsp.util.Util.customFormat(buscarValorCheque(oc,rs2.getString(3))));
                            }
                            else
                                movimiento.setDescripcion("");
                            lista.add(movimiento);
                            sw = false;
                        }
                    }
                }
            }
            rs2=st2.executeQuery();
            if(rs2!=null && sw ){
                if(rs2.next()){
                    AnticipoPlanilla  movimiento = new AnticipoPlanilla();
                    movimiento.setPlanilla(oc);
                    movimiento.setAgencia(rs2.getString(1));
                    movimiento.setTipoDocumento(rs2.getString(2));
                    movimiento.setDocumento(rs2.getString(3));
                    movimiento.setItem(rs2.getString(4));
                    movimiento.setConcepto(rs2.getString(5));
                    double valor=((int)rs2.getDouble(6)==0)?rs2.getDouble(7):rs2.getDouble(6);
                    movimiento.setValor(rs.getDouble(6));
                    movimiento.setValor_for(rs.getDouble(7));
                    movimiento.setMoneda(rs2.getString(8));
                    movimiento.setBanco(rs2.getString(9) );
                    movimiento.setCuenta(rs2.getString(10));
                    movimiento.setAgenciaBanco(rs2.getString(1) );
                    movimiento.setDate_doc(rs2.getString("date_doc"));
                    movimiento.setReg_status("A");
                    if(valor==0){
                        movimiento.setDescripcion("ANULADO VALOR: "+com.tsp.util.Util.customFormat(buscarValorCheque(oc,rs2.getString(3))));
                    }
                    else
                        movimiento.setDescripcion("");
                    lista.add(movimiento);
                }
            }
        }
        catch(Exception e){
            throw new SQLException("Error en la busqueda de Movimientos para la planilla "+oc +" --> "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            if(st2!=null) st.close();
            if(rs2!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return lista;
    }*/
    
    
    
}//FIN DAO
