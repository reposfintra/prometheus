/********************************************************************
 *      Nombre Clase.................   MigrarOTFitDefDAO.java
 *      Descripci�n..................   DAO que genera el archivo de migraci�n Fitmen-Defitmen
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   21.01.2006
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.net.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.*;
//import com.tsp.propiedades.Propiedades;

/**
 *
 * @author  Tito Andr�s Maturana
 */
public class MigrarOTFitDefDAO {
    
    //private Propiedades propiedades = new Propiedades();;
    
    
    private static final String CONSULTA =
    "SELECT plaveh, " +
    "       numrem,  " +
    "       platlr,  " +
    "       contenedores,  " +
    "       tipocont,  " +
    "       To_Char(pla.creation_date, 'dd/MM/YY') as fecha  " +
    "FROM   ( " +
    "   SELECT  plaveh,  " +
    "           platlr,  " +
    "           contenedores,  " +
    "           tipocont,  " +
    "           numpla,  " +
    "           creation_date  " +
    "   FROM    planilla  " +
    "   WHERE   fecdsp>=? AND fecdsp<'now()' AND cia=? AND base IN ('COL') " +
    ") AS pla  " +
    "JOIN   plarem AS plr ON ( plr.numpla=pla.numpla ) order by numrem" ;
    
    private static final String CONSULTA_DEFIT =
    "SELECT To_Char(cump.creation_date, 'dd/MM/YY') AS fecha, " +
    "       plaveh, " +
    "       numrem, " +
    "       platlr, " +
    "       contenedores, " +
    "       tipocont, " +
    "       cump.* " +
    "FROM (" +
    "   SELECT  creation_date, " +
    "           cod_doc " +
    "   FROM cumplido " +
    "   WHERE  dstrct=? AND tipo_doc='001' AND creation_date>=? AND creation_date<'now()') cump " +
    "JOIN planilla pl ON ( pl.numpla=cump.cod_doc AND pl.base IN ('COL') ) " +
    "JOIN plarem plr ON ( plr.numpla=pl.numpla )  ";
    
    
    private Vector lineas;
    private Properties dbProps;
    
    /** Creates a new instance of MigrarOTFitDefDAO */
    public MigrarOTFitDefDAO() {
        //InputStream is = PoolManager.getInstance().getClass().getResourceAsStream("MigOTFitDef.properties");
        //InputStream is = propiedades.getClass().getResourceAsStream("general.properties");        
        dbProps = new Properties();
        try {
            //dbProps.load(is);
        }
        catch (Exception e) {
            
        }
    }
    
    /**
     * Genera la informaci�n para el archivo de migraci�n Fitmen-Defitmens
     * @autor Ing. Tito Andr�s Maturana D.
     * @param cia C�digo Distrito
     * @throws SQLException si ocurre un error con la conexi�n con la Base de Datos
     * @returns Arreglo con los fitmens y defitmens
     * @version 1.0.
     **/
    public void migrarOTFitDef(String cia)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                String fec_ini = "";
                
                
                fec_ini = dbProps.getProperty("migracionOTFitDef");
                //////System.out.println("-Acceso a dbProps:*"+fec_ini+"*");
                if(fec_ini.length() == 0){
                    String sql = "select min(creation_date) as fec_ini from planilla where base ='COL'";
                    st = con.prepareStatement(sql);
                    rs = st.executeQuery();
                    
                    if (rs.next()) fec_ini = rs.getString("fec_ini");
                    //////System.out.println("*fec_ini: "+fec_ini);
                    //////System.out.println("*st: "+st.toString());
                }
                else{
                    fec_ini = dbProps.getProperty("migracionOTFitDef");
                    //////System.out.println("**fec_ini: "+fec_ini);
                }
                
                st = con.prepareStatement(this.CONSULTA);
                st.setString(1,fec_ini);
                st.setString(2, cia);
                ////System.out.println(".......................... FITMEN: " + st.toString());
                rs = st.executeQuery();
                
                this.lineas = new Vector();
                while(rs.next()){
                    String contenedores = rs.getString("contenedores");
                    String cont1 = "";
                    String cont2 = "";
                    String placa = rs.getString("platlr");
                    
                    if ( contenedores!=null && contenedores!=""){
                        StringTokenizer stk = new StringTokenizer(contenedores,",");
                        if ( stk.hasMoreTokens() ) cont1 = stk.nextToken();
                        if ( stk.hasMoreTokens() ) cont2 = stk.nextToken();
                    }
                    
                    String linea = "";
                    String lineaA = rs.getString("numrem") + "," + rs.getString("fecha") +
                    "," + rs.getString("plaveh") + ",B,";
                    String lineaB = ",,,";
                    String tipoContenedor = rs.getString("tipocont");
                    //////System.out.println("***plaveh:" + rs.getString("plaveh") + ", trailer:" + placa
                    //    + ", Cont 1: " + cont1 + ", Cont 2:" + cont2 + ", tipoCont: " + tipoContenedor);
                    
                    if (rs.getString("fecha")!=null){
                        if ( placa != null && placa.length() != 0 && !placa.matches("NA") ){
                            if ( cont1!=null && cont1.length() != 0 && !cont1.matches("NA") && tipoContenedor.matches("FINV") ){
                                linea = lineaA + placa + "," + rs.getString("plaveh") + ",B," + cont1 ;
                                if ( cont2!=null && cont2.length() != 0 && !cont2.matches("NA") && tipoContenedor.matches("FINV") ){
                                    linea += "," + rs.getString("plaveh") + ",B," + cont2 ;
                                }
                                else
                                    linea += lineaB;
                            }
                            else if ( cont2!=null && cont2.length() != 0 && !cont2.matches("NA") && tipoContenedor.matches("FINV") ){
                                linea = lineaA + placa + "," + rs.getString("plaveh") + ",B," + cont2 + lineaB;
                            }
                            else{
                                linea = lineaA + placa + lineaB + lineaB;
                            }
                        }
                        else if ( cont1!=null && cont1.length() != 0 && !cont1.matches("NA") && tipoContenedor.matches("FINV") ){
                            if ( cont2!=null && cont2.length() != 0 && !cont2.matches("NA") && tipoContenedor.matches("FINV") ){
                                linea = lineaA + cont1 + "," + rs.getString("plaveh") + ",B," + cont2  + lineaB;
                            }
                            else{
                                linea = lineaA + cont1 + lineaB + lineaB;
                            }
                        }
                        else if ( cont2!=null && cont2.length() != 0 && !cont2.matches("NA") && tipoContenedor.matches("FINV") ){
                            linea = lineaA + cont2 + lineaB;
                        }
                    }
                    
                    //////System.out.println("------------>linea: " + linea);
                    if(linea.length()>0)
                        this.lineas.add(linea);
                }
                
                st = con.prepareStatement(this.CONSULTA_DEFIT);
                st.setString(1,cia);
                st.setString(2,fec_ini);
                
                st.toString();
                ////System.out.println(".......................... DEFITMEN: " + st.toString());
                rs = st.executeQuery();
                
                //////System.out.println("...................... DEFITMEN: " + st.toString());
                
                while(rs.next()){
                    String contenedores = rs.getString("contenedores");
                    String cont1 = "";
                    String cont2 = "";
                    String placa = rs.getString("platlr");
                    
                    if ( contenedores!=null && contenedores!=""){
                        StringTokenizer stk = new StringTokenizer(contenedores,",");
                        if ( stk.hasMoreTokens() ) cont1 = stk.nextToken();
                        if ( stk.hasMoreTokens() ) cont2 = stk.nextToken();
                    }
                    
                    String linea = "";
                    String lineaA = rs.getString("numrem") + "," + rs.getString("fecha") +
                    "," + rs.getString("plaveh") + ",C,";
                    String lineaB = ",,,";
                    String tipoContenedor = rs.getString("tipocont");
                    //////System.out.println("***plaveh:" + rs.getString("plaveh") + ", trailer:" + placa
                    //    + ", Cont 1: " + cont1 + ", Cont 2:" + cont2 + ", tipoCont: " + tipoContenedor);
                    
                    if (rs.getString("fecha")!=null){
                        if ( placa != null && placa.length() != 0 && !placa.matches("NA") ){
                            if ( cont1!=null && cont1.length() != 0 && !cont1.matches("NA") && tipoContenedor.matches("FINV") ){
                                linea = lineaA + placa + "," + rs.getString("plaveh") + ",C," + cont1 ;
                                if ( cont2!=null && cont2.length() != 0 && !cont2.matches("NA") && tipoContenedor.matches("FINV") ){
                                    linea += "," + rs.getString("plaveh") + ",C," + cont2 ;
                                }
                                else
                                    linea += lineaB;
                            }
                            else if ( cont2!=null && cont2.length() != 0 && !cont2.matches("NA") && tipoContenedor.matches("FINV") ){
                                linea = lineaA + placa + "," + rs.getString("plaveh") + ",C," + cont2 + lineaB;
                            }
                            else{
                                linea = lineaA + placa + lineaB + lineaB;
                            }
                        }
                        else if ( cont1!=null && cont1.length() != 0 && !cont1.matches("NA") && tipoContenedor.matches("FINV") ){
                            if ( cont2!=null && cont2.length() != 0 && !cont2.matches("NA") && tipoContenedor.matches("FINV") ){
                                linea = lineaA + cont1 + "," + rs.getString("plaveh") + ",C," + cont2  + lineaB;
                            }
                            else{
                                linea = lineaA + cont1 + lineaB + lineaB;
                            }
                        }
                        else if ( cont2!=null && cont2.length() != 0 && !cont2.matches("NA") && tipoContenedor.matches("FINV") ){
                            linea = lineaA + cont2 + lineaB;
                        }
                    }
                    
                    //////System.out.println("------------>linea: " + linea);
                    if(linea.length()>0)
                        this.lineas.add(linea);
                }
                
                this.actualizarDBProperties();
                
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL FITMEN - DEFITMEN. " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Getter for property lineas.
     * @return Value of property lineas.
     */
    public java.util.Vector getLineas() {
        return lineas;
    }
    
    /**
     * Setter for property lineas.
     * @param lineas New value of property lineas.
     */
    public void setLineas(java.util.Vector lineas) {
        this.lineas = lineas;
    }
    
    /**
     * Actualiza el archivo de tipo properties donde se almacena la ultima fecha del proceso
     * @autor Ing. Tito Andr�s Maturana D.
     * @version 1.0.
     **/
    public void actualizarDBProperties(){
        try{
            dbProps.setProperty("migracionOTFitDef", Util.getFechaActual_String(6));            
            String url = null;//propiedades.getClass().getResource("general.properties").getFile();
            //////System.out.println("........................ URL: " + url);            
            //dbProps.store(new FileOutputStream(propiedades.getClass().getResource("general.properties").getFile()),"");
        }catch(Exception exc){
            //////System.out.println(exc.getMessage());
        }
    }
    
}