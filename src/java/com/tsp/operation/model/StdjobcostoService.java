package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

public class StdjobcostoService{
    
    private StdjobcostoDAO standard;
    
    public StdjobcostoService(){
        
        standard = new StdjobcostoDAO();
    }
    
    public List getRutas(String sj)throws SQLException{
        return standard.getRutas();
    }  
    public void searchRutas(String sj )throws SQLException{
        standard.searchRutas(sj);
        
    }
    
    public List getCostos(String ruta, String sj)throws SQLException{
        standard.searchCostos(ruta,sj);
        return standard.getCostos();
    }  
    
    public Stdjobcosto getStandardCosto( )throws SQLException{
        
        return standard.getStdJobCosto();
        
    }
    
    public void buscaStdJobCosto(String sj)throws SQLException{
        try{
            standard.searchStandardJobCosto(sj);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    public void buscaStdJobCosto(String sj, String ft)throws SQLException{
        try{
            standard.searchStandardJobCosto(sj, ft);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    public String estaConectado()throws SQLException{
        return standard.isConnected();
    }
    
    public boolean estaRuta(String ruta, String sj)throws SQLException{
        return standard.estaRuta(ruta,sj);
    }
    public void buscarStandardJobCostoFull(String sj, String ft )throws SQLException{
        try{
            standard.searchStandardJobCostoFull(sj, ft);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void searchMaxCostos(String ruta, String sj )throws SQLException{
        standard.searchMaxCostos(ruta, sj);
    }
    public com.tsp.operation.model.beans.Stdjobcosto getStandard() {
        return standard.getStandard();
    }
    
}





