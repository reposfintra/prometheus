/*
 * RemesaDAO.java
 *
 * Created on 23 de noviembre de 2004, 08:46 AM
 * modificado por karen reales 03-05-2006
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  KREALES
 */
public class RemesaDAO extends MainDAO {
    
    private Aduana a;
    private Vector vector;
    private Vector otro_vector;
    
    Remesa rm;
    Vector r;
    private Remesa rem;
    private List planillas;
    private List rem_print;
    private Vector remesas;
    private Vector vClientes;
    private Remesa padre;
    
    //jose 15 febrero 2007
    
    private static final String SQL_INSERT="insert into remesa (numrem,fecrem,agcrem,orirem,desrem,cliente,unidcam,tipoviaje,cia,usuario,pesoreal,unit_of_work,std_job_no,vlrrem,docuinterno,destinatario,remitente,observacion,descripcion,remision,base,facturacial,qty_packed,unit_packed,starem,qty_value,currency,VLRREM2,n_facturable,fecremori,crossdocking, aduana,codtipocarga,cadena,cod_pagador,cmc) values(?,'now()',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'now()',?,?,?,?,?,?)";
    
    private static final String SQL_REMESA_DISCREPANCIA =       "SELECT DISTINCT "+
    "	COALESCE(d.numpla,d.numpla,'') AS numpla, "+
    "	COALESCE(d.numrem,d.numrem,'') AS numrem, "+
    "	d.num_discre, "+
    "	COALESCE(tbl.document_type,tbl.document_type,'') AS document_type, "+
    "	COALESCE(tbl.document_name,tbl.document_name,'') as documento1, "+
    "	COALESCE(tbl2.document_name,tbl2.document_name,'') as doc_rel, "+
    "	COALESCE(rm.nombre,rm.nombre,'') as destinatario, "+
    "	COALESCE(rd.documento,rd.documento,'') AS documento, "+
    "	COALESCE(rd.tipo_doc_rel, rd.tipo_doc_rel,'') as nom_doc_rel, "+
    "	COALESCE(rd.tipo_doc_rel,rd.tipo_doc_rel,'') AS tipo_doc_rel, "+
    "	COALESCE(rd.documento_rel,rd.documento_rel,'') AS documento_rel "+
    "FROM "+
    "	discrepancia as d "+
    "	LEFT JOIN tbldoc tbl ON ( tbl.document_type = d.tipo_doc ) "+
    "	LEFT JOIN tbldoc tbl2 ON ( tbl2.document_type = d.tipo_doc ) "+
    "	LEFT JOIN remesa_docto rd ON ( rd.numrem = d.numrem AND rd.documento = d.documento AND rd.tipo_doc = d.tipo_doc ) "+
    "	LEFT JOIN remidest rm ON ( rm.codigo = rd.destinatario ) "+
    "WHERE "+
    "	d.numpla = ? "+
    "	AND d.numrem = ? "+
    "	AND d.tipo_doc = ? "+
    "	AND d.documento = ? "+
    "	AND d.num_discre = ? "+
    "	AND d.rec_status != 'A'";
    
        private static final String SQL_ACTUALIZAR= "update remesa set  docuinterno=?, destinatario=?,remitente=?,unidcam=?,observacion=?, facturacial=?, std_job_no=?, descripcion=?, lastupdate = 'now()', n_facturable=?,qty_packed=?,crossdocking=?,unit_packed=?,cadena=?,cod_pagador=? where numrem=?";
    
    private static final String SQL_CODCLI_REMESA = "SELECT SUBSTR(cliente, 4, 3) AS cliente FROM remesa WHERE numrem = ?";//AMATURANA
    
    
    private static final String SQL_LIST_ADUANA =
    "select      " +
    "case when r.tipoviaje = 'RM'                      THEN 'PA'     " +
    "WHEN r.tipoviaje = 'RC' AND c.pais ='CO'          THEN 'CU'     " +
    "WHEN r.tipoviaje = 'RC' AND c.pais ='VE'          THEN 'ST'       " +
    "WHEN r.tipoviaje = 'RE' AND c.pais ='CO'          THEN 'IP'        " +
    "WHEN r.tipoviaje = 'RE' AND c.pais ='EC'          THEN 'TU'                       END AS FONTERA,   " +
    "case when r.tipoviaje = 'RM'                      THEN get_nombreciudad('PA')      " +
    "WHEN r.tipoviaje = 'RC' AND c.pais ='CO'          THEN get_nombreciudad('CU')      " +
    "WHEN r.tipoviaje = 'RC' AND c.pais ='VE'          THEN get_nombreciudad('ST')      " +
    "WHEN r.tipoviaje = 'RE' AND c.pais ='CO'          THEN get_nombreciudad('IP')      " +
    "WHEN r.tipoviaje = 'RE' AND c.pais ='EC'          THEN get_nombreciudad('TU')     END AS NOMFONTERA,   " +
    "R.AGCREM as agencia,     " +
    "get_nombrecliente(R.cliente) as nomcliente,    " +
    "r.numrem,        " +
    "C.NOMCIU as origen,      " +
    "get_nombreciudad(r.desrem) as destino,   " +
    "pr.numpla,  " +
    "it.placa, " +
    "p.platlr,  " +
    "r.descripcion,   " +
    "it.nompto_control_ultreporte,   " +
    "it.ult_observacion,  " +
    "case when it.pto_control_proxreporte ='' then p.oripla else it.pto_control_proxreporte end as pto_control_proxreporte,          " +
    "v.via,        " +
    "get_ultactividad(r.cia, r.cliente, r.numrem) as ultact,     " +
    "case when it.fecha_ult_reporte='0099-01-01 00:00:00' then " +
    "   case when it.fecha_salida!='0099-01-01 00:00:00' then it.fecha_salida else p.fecdsp end " +
    "else it.fecha_ult_reporte end as fecha_ult_reporte,        " +
    "r.tipoviaje,        " +
    "r.cliente, " +
    "it.dstrct AS distrito  " +
    "from    " +
    "remesa r         " +
    "inner join plarem pr on (pr.numrem = r.numrem)   " +
    "left join planilla p on (p.numpla = pr.numpla)  " +
    "inner join ingreso_trafico it on (it.planilla = pr.numpla)       " +
    "inner join ciudad c on (c.codciu = r.orirem)        " +
    "inner join via v on (v.origen||v.destino||v.secuencia = it.via)    " +
    "inner join ciudad cd on (cd.codciu = r.desrem)        " +
    "inner join ciudad f on (f.codciu = ?)  " +
    "where    " +
    "r.cia = 'FINV'     " +
    "and aduana='S'        " +
    "and ( " +
    "case when r.tipoviaje = 'RM'  THEN 'PA'       WHEN r.tipoviaje = 'RC'  " +
    "AND c.pais ='CO'            THEN 'CU'       WHEN r.tipoviaje = 'RC'  " +
    "AND c.pais ='VE'            THEN 'ST'       WHEN r.tipoviaje = 'RE'  " +
    "AND c.pais ='CO'            THEN 'IP'       WHEN r.tipoviaje = 'RE'  " +
    "AND c.pais ='EC'            THEN 'TU'       END  " +
    ")=f.frontera_asoc    " +
    "and r.cliente like ?    " +
    "and it.tipo_reporte like ?    " +
    "and c.pais||cd.pais like ?  " +
    "and position (( case when r.tipoviaje = 'RM'  THEN 'PA'  " +
    "		WHEN r.tipoviaje = 'RC'  AND c.pais ='CO'      " +
    "		THEN 'CU'     " +
    "		WHEN r.tipoviaje = 'RC'  AND c.pais ='VE'   " +
    "		THEN 'ST'    " +
    "		WHEN r.tipoviaje = 'RE'  AND c.pais ='CO'  " +
    "		THEN 'IP'      " +
    "		WHEN r.tipoviaje = 'RE'  AND c.pais ='EC'     " +
    "		THEN 'TU'       END )  in v.via) >1  " +
    "order by " +
    "nomcliente, pr.numpla ";
    
    private static final String SQL_CONSULTA =  "  SELECT                                                                                                               "+
    "    r.numrem,                                                                                                          "+
    "     pr.numpla,                                                                                                        "+
    "     pr.feccum,                                                                                                        "+
    "     r.fecrem,                                                                                                         "+
    "     r.cia,                                                                                                            "+
    "     r.vlrrem,                                                                                                         "+
    "     r.observacion,                                                                                                    "+
    "     r.pesoreal,                                                                                                       "+
    "     r.unit_of_work,                                                                                                   "+
    "     r.std_job_no,                                                                                                     "+
    "     r.descripcion,                                                                                                    "+
    "     r.remision,                                                                                                       "+
    "     get_nombrecliente(r.cliente) AS nomcli,                                                                           "+
    "     get_nombreciudad(r.orirem)   AS origen,                                                                           "+
    "     get_nombreciudad(r.desrem)   AS destino,                                                                          "+
    "     m.nommoneda,                                                                                                      "+
    "     get_nombreciudad(p.oripla) AS origenOC,                                                                           "+
    "     get_nombreciudad(p.despla) AS destinoOC                                                                           "+
    " FROM                                                                                                                  "+
    "    remesa r                                                                                                           "+
    "     LEFT JOIN cliente  cli    ON     ( cli.codcli = r.cliente )                                                       "+
    "     LEFT JOIN plarem   pr     ON     ( pr.numrem  = r.numrem   )                                                      "+
    "     LEFT JOIN planilla p      ON     ( p.numpla   = pr.numpla  )                                                      "+
    "     #LEFTJOIN# remesa_docto rd ON     ( r.numrem   =  rd.numrem and rd.tipo_doc like ? and rd.documento like ? )       "+
    "     LEFT JOIN monedas m       ON     (  r.currency = m.codmoneda)                                                     "+
    " WHERE                                                                                                                 "+
    "     r.fecrem between ? and ?                                                                                          "+
    "     and r.std_job_no  like ?                                                                                          "+
    "     and r.reg_status  = ?                                                                                             "+
    "     and pr.feccum     like ?                                                                                          "+
    "     and cli.nomcli    like ?                                                                                          "+
    "     and r.base        = ?                                                                                             "+
    "     order by r.numrem;                                                                                                ";
    
    
    
    //Jescandon 11-10-06
    
    private final String SQL_CONSULTA_REMISION = " SELECT r.numrem,                                                       "+
    "         pr.numpla,                                                     "+
    "         pr.feccum,                                                     "+
    "         r.fecrem,                                                      "+
    "         r.cia,                                                         "+
    "         r.vlrrem,                                                      "+
    "         r.observacion,                                                 "+
    "         r.pesoreal,                                                    "+
    "         r.unit_of_work,                                                "+
    "         r.std_job_no,                                                  "+
    "         r.descripcion,                                                 "+
    "         r.remision,                                                    "+
    "         get_nombrecliente(r.cliente) AS nomcli,                        "+
    "         get_nombreciudad(orirem) AS origen,                            "+
    "         get_nombreciudad(desrem) AS destino,                           "+
    "         m.nommoneda,                                                   "+
    "         get_nombreciudad(p.oripla) AS origenOC,                        "+
    "         get_nombreciudad(p.despla) AS destinoOC                        "+
    "    FROM                                                                "+
    "         remesa r                                                       "+
    "         LEFT JOIN monedas    m   ON ( r.currency = m.codmoneda )       "+
    "         LEFT JOIN plarem     pr  ON ( pr.numrem  = r.numrem )          "+
    "         LEFT JOIN planilla   p   ON ( pr.numpla  = p.numpla  )         "+
    "   WHERE r.remision    = ?                                              "+
    "   ORDER BY r.numrem  ;                                                 ";
    
    
    private final String SQL_CONSULTA_REMESA = " SELECT r.numrem,                                                       "+
    "         pr.numpla,                                                     "+
    "         pr.feccum,                                                     "+
    "         r.fecrem,                                                      "+
    "         r.cia,                                                         "+
    "         r.vlrrem,                                                      "+
    "         r.observacion,                                                 "+
    "         r.pesoreal,                                                    "+
    "         r.unit_of_work,                                                "+
    "         r.std_job_no,                                                  "+
    "         r.descripcion,                                                 "+
    "         r.remision,                                                    "+
    "         get_nombrecliente(r.cliente) AS nomcli,                        "+
    "         get_nombreciudad(orirem) AS origen,                            "+
    "         get_nombreciudad(desrem) AS destino,                           "+
    "         m.nommoneda,                                                   "+
    "         get_nombreciudad(p.oripla) AS origenOC,                        "+
    "         get_nombreciudad(p.despla) AS destinoOC                        "+
    "    FROM                                                                "+
    "         remesa r                                                       "+
    "         LEFT JOIN monedas    m   ON ( r.currency = m.codmoneda )       "+
    "         LEFT JOIN plarem     pr  ON ( pr.numrem  = r.numrem )          "+
    "         LEFT JOIN planilla   p   ON ( pr.numpla  = p.numpla  )         "+
    "   WHERE r.numrem    = ?                                                "+
    "   ORDER BY r.numrem  ;                                                 ";
    
    //JOSE 19-09-2006
    private static final String SQL_DISCREPANCIA_REMESA = "select distinct p.numpla, r.numrem, c.nomcli, rd.tipo_doc, rd.documento, rd.tipo_doc_rel, rd.documento_rel, rm.nombre as destinatario, td1.document_name as tdoc, td2.document_name  as tdoc_rel "+
    "from planilla as p, "+
    "plarem as rp, "+
    "remesa as r, "+
    "cliente as c, "+
    "tbldoc as td1, "+
    "remesa_docto as rd "+
    "LEFT JOIN tbldoc td2 ON (rd.tipo_doc_rel = td2.document_type) "+
    "LEFT JOIN remidest rm ON (rd.destinatario = rm.codigo) "+
    "    where rp.numrem = ? and p.numpla = ? and "+
    "    rp.numpla = p.numpla and "+
    "    rp.numrem = r.numrem and "+
    "    r.cliente = c.codcli and "+
    "    rd.numrem = r.numrem and "+
    "    td1.document_type = rd.tipo_doc";
    
    private static final String SQL_OBTENER_VIA =" SELECT via FROM via WHERE origen = ? AND destino = ? AND secuencia = ? ";
    
    
    private static final String SQL_BUSCAR_INFOREMENSA = " SELECT 	r.*,  " +
    "   	cli.codcli,                   	" +
    "           cli.nomcli, " +
    "           coalesce(to_char(doc.creation_date,'yyyy-MM-dd HH24:MI'),'0099-01-01') as fec_anul," +
    "           s.currency," +
    "           s.unidad," +
    "           s.std_job_desc," +
    "	        rd.nombre as nomremitente," +
    "           case when c.cod_doc is not null then 'CUMPLIDA' else '' END as cumplido," +
    "           coalesce(c.cantidad,'0') as cantidad,        " +
    "           c.unidad as unidad_cum,          " +
    "           c.creation_user as usu_cumple," +
    "           coalesce(c.creation_date,'0099-01-01 00:00:00') as fec_cum" +
    "       FROM  	cliente cli, " +
    "            remesa r  " +
    "            LEFT JOIN stdjob s on (s.dstrct_code = ? and s.std_job_no=r.std_job_no) " +
    "            LEFT OUTER JOIN (select dstrct,creation_date,documento,tipodoc from documento_anulado)doc on (doc.dstrct = ? and doc.documento= r.numrem and doc.tipodoc = '002' )        " +
    "            LEFT JOIN remidest rd on (rd.codigo =r.remitente)" +
    "            LEFT JOIN cumplido c on (c.dstrct = 'FINV' AND c.tipo_doc = '002' AND c.cod_doc =r.numrem)" +
    "       WHERE        r.numrem= ?   " +
    "               	AND r.cliente=cli.codcli";
    
    private final String SQL_ACTUALIZAR_REMESA_IMPRESA    = "UPDATE remesa SET PRINTER_DATE= NOW() "+
    "WHERE NUMREM = ? ";
    
    private final String SQL_BUSCAR_DATOS_REM_DES         = "SELECT A.NOMBRE,          "+
    "       A.DIRECCION,       "+
    "       B.NOMCIU           "+
    "FROM REMIDEST  A,         "+
    "     CIUDAD    B          "+
    "WHERE A.CODIGO = ? AND    "+
    "      A.CIUDAD = B.CODCIU ";
    
    //MFontalvo 18.01.06
    private final String SQL_BUSCAR_REMESA_NO_IMPRESAS   =
    " SELECT                                                     " +
    "       a.numrem,                                            " +
    "       a.fecrem,                                            " +
    "       coalesce(get_nombrecliente(a.cliente),'') as nomcli, " +
    "       coalesce(get_nombreciudad(a.orirem),'') as origen,   " +
    "       coalesce(get_nombreciudad(a.desrem),'') as destino,  " +
    "       a.docuinterno,                                       " +
    "       a.observacion,                                       " +
    "       coalesce(get_nombreciudad(a.agcrem),'') as agencia,  " +
    "       a.printer_date,                                      " +
    "       a.remitente,                                         " +
    "       a.destinatario,                                      " +
    "       coalesce(b.texto_remesa,'') as texto_remesa          " +
    " FROM                                                       " +
    "       remesa     a                                         " +
    "       left join                                            " +
    "       (  select a.codcli, c.texto_remesa                   " +
    "          from   cliente a, agencia b, cia c                " +
    "          where  b.id_agencia = a.agduenia and c.dstrct = b.dstrct and a.fiduciaria = 'S' " +
    "       ) b on (b.codcli = a.cliente)                        " +
    " WHERE                                                      " +
    "       a.reg_status <> 'A'                                  " +
    "   and a.numrem    = ?                                      " +
    " ORDER BY a.agcrem, a.numrem                                " ;
    
    private final String SQL_BUSCAR_REMESAS_NO_IMPRESAS   =
    " SELECT     a.numrem,                                            " +
    "           a.fecrem,                                             " +
    "           coalesce(get_nombrecliente(a.cliente),'') as nomcli,  " +
    "           coalesce(get_nombreciudad(a.orirem),'') as origen,    " +
    "           coalesce(get_nombreciudad(a.desrem),'') as destino,   " +
    "           a.docuinterno,                                        " +
    "           a.observacion,                                        " +
    "           coalesce(get_nombreciudad(a.agcrem),'') as agencia,   " +
    "           a.printer_date,                                       " +
    "           a.remitente,                                          " +
    "           a.destinatario,                                       " +
    "           coalesce(b.texto_remesa,'') as texto_remesa           " +
    "     FROM  (SELECT * FROM remesa                                 " +
    "            WHERE printer_date  = '0099-01-01 00:00:00'          " +
    "              AND usuario       = ?                              " +
    "              AND reg_status <> 'A'                              " +
    "            ORDER BY agcrem, numrem     )  a                     " +
    "           LEFT JOIN ( SELECT a.codcli, c.texto_remesa           " +
    "                       FROM  cliente a, agencia b, cia c         " +
    "                       WHERE b.id_agencia = a.agduenia           " +
    "                         AND c.dstrct = b.dstrct                 " +
    "                         AND a.fiduciaria = 'S'   ) b on (b.codcli = a.cliente) ";
    
    
    private final String SQL_CONSULTA_DATOS=" select r.numrem," +
    "                                          pr.numpla, " +
    "                                          pr.feccum, " +
    "                                          r.fecrem, " +
    "                                          r.docuinterno, " +
    "                                          r.cliente, "+
    "                                          r.cia," +
    "                                          r.vlrrem," +
    "                                          r.observacion, " +
    "                                          r.pesoreal, " +
    "                                          r.unit_of_work, " +
    "                                          r.std_job_no," +
    "                                          r.descripcion, " +
    "                                          c.nomciu as origen, " +
    "                                          d.nomciu as destino, " +
    "                                          cli.nomcli," +
    "                                          r.remision" +
    "                                     from remesa r, " +
    "                                          ciudad c, " +
    "                                          ciudad d, " +
    "                                          plarem pr, " +
    "                                          cliente cli" +
    "                                    where r.numrem=?" +
    "                                          and r.orirem = c.codciu " +
    "                                          and r.desrem = d.codciu " +
    "                                          and pr.numrem = r.numrem" +
    "                                          and cli.codcli='000'||substr(r.std_job_no,1,3)  ";
    
    private final String SQL_CONSULTA_DATOS_REMISION=" select r.numrem," +
    "                                          pr.numpla, " +
    "                                          pr.feccum, " +
    "                                          r.fecrem, " +
    "                                          r.docuinterno, " +
    "                                          r.cliente, "+
    "                                          r.cia," +
    "                                          r.vlrrem," +
    "                                          r.observacion, " +
    "                                          r.pesoreal, " +
    "                                          r.unit_of_work, " +
    "                                          r.std_job_no," +
    "                                          r.descripcion, " +
    "                                          c.nomciu as origen, " +
    "                                          d.nomciu as destino, " +
    "                                          cli.nomcli," +
    "                                          r.remision" +
    "                                     from remesa r, " +
    "                                          ciudad c, " +
    "                                          ciudad d, " +
    "                                          plarem pr, " +
    "                                          cliente cli" +
    "                                    where r.remision=?" +
    "                                          and r.orirem = c.codciu " +
    "                                          and r.desrem = d.codciu " +
    "                                          and pr.numrem = r.numrem" +
    "                                          and cli.codcli='000'||substr(r.std_job_no,1,3)  ";
    
    
    
    private static final String SQL_OBTENER_PLANILLAS = "select * from planilla";//BETWEEN 4095 AND 12000
    private static final String SQL_OBTENER_REMESAS_DE_PLANILLA =
    "select remesa.starem," +
    " remesa.numrem, " +
    "remesa.fecrem, " +
    "remesa.fechacargue, " +
    "remesa.agcrem, " +
    "remesa.orirem, " +
    "remesa.desrem, " +
    "cliente.nomcli as cliente, " +
    "remesa.destinatario, " +
    "remesa.unidcam, " +
    "remesa.docuinterno, " +
    "remesa.facturacial, " +
    "remesa.tipoviaje, " +
    "remesa.cia, " +
    "remesa.lastupdate, " +
    "remesa.usuario, " +
    "remesa.vlrrem, " +
    "remesa.vlrrem2, " +
    "remesa.remitente, " +
    "remesa.ultreporte, " +
    "remesa.observacion, " +
    "remesa.demoras, " +
    "remesa.pesoreal, " +
    "remesa.crossdocking, " +
    "remesa.estado, " +
    "remesa.unit_of_work, " +
    "remesa.ot_padre, " +
    "remesa.ot_rela, " +
    "remesa.tieneplanilla, " +
    "remesa.fecremori, " +
    "remesa.plan_str_date, " +
    "remesa.std_job_no, " +
    "remesa.descripcion, " +
    "remesa.reg_status, " +
    "remesa.printer_date, " +
    "remesa.derechos_cedido, " +
    "remesa.remision, " +
    "remesa.creation_date, " +
    "remesa.base, " +
    "remesa.codtipocarga, " +
    "remesa.qty_value, " +
    "remesa.qty_packed, " +
    "remesa.qty_value_received, " +
    "remesa.qty_packed_received, " +
    "remesa.unit_packed, " +
    "remesa.corte " +
    "from planilla,plarem,remesa,cliente" +
    " where cliente.codcli = remesa.cliente and planilla.numpla = plarem.numpla and plarem.numrem = remesa.numrem and planilla.numpla = ?";
    
    
    
    
    
    private String  QUERY_DATOPLANILLA_REMESA =
    "  select                                                    "+
    "   b.numpla, b.plaveh, b.cedcon, c.nombre, b.contenedores   "+
    " from plarem    a,                                          "+
    "      planilla  b,                                          "+
    "      nit       c                                           "+
    " where                                                      "+
    "      a.numrem = ?                                          "+
    " and  a.numpla = b.numpla                                   "+
    " and  c.cedula = b.cedcon                                   ";
    
    //JOSE 07-03-2006
    private static final String SQL_REMESA = "select distinct p.numpla, r.numrem, c.nomcli, rd.tipo_doc, rd.documento, rd.tipo_doc_rel, rd.documento_rel, rm.nombre as destinatario, td1.document_name as tdoc, td2.document_name  as tdoc_rel "+
    "from planilla as p, "+
    "plarem as rp, "+
    "remesa as r, "+
    "cliente as c, "+
    "tbldoc as td1, "+
    "remesa_docto as rd "+
    "LEFT JOIN tbldoc td2 ON (rd.tipo_doc_rel = td2.document_type) "+
    "LEFT JOIN remidest rm ON (rd.destinatario = rm.codigo) "+
    "    where rp.numrem = ? and "+
    "    rp.numpla = p.numpla and "+
    "    rp.numrem = r.numrem and "+
    "    r.cliente = c.codcli and "+
    "    rd.numrem = r.numrem and "+
    "    td1.document_type = rd.tipo_doc";
    
    //JOSE 03.11.05
    private static final String SQL_MOSTRAR_STANDARJOB = "select std_job_no from remesa where numrem = ?";
    
    private static final String SQL_CLIENTES="select  distinct cliente ,get_nombrecliente(cliente) as nombre from plarem as p ,remesa  as r where p.numpla = ? and p.numpla <> '' and r.numrem=p.numrem";
    
    private static final String SQL_ACTIVIDAD_ADUANA =
    "SELECT  " +
    "actividad.deslarga AS nom_act, infoactividad.observacion AS obs_act, infoactividad.last_update AS fec_act " +
    "FROM 	 " +
    "infoactividad, " +
    "actividad " +
    "WHERE	 " +
    "infoactividad.dstrct = ? " +
    "AND infoactividad.codcli = ? " +
    "AND infoactividad.numrem = ? " +
    "and actividad.cod_actividad = infoactividad.cod_actividad " +
    "ORDER BY  " +
    "infoactividad.creation_date DESC LIMIT(1) " ;
    
    private static final String SQL_AUXILIAR_REMESA =
    "SELECT " +
    "* " +
    "FROM " +
    "auxdoc " +
    "WHERE " +
    "dstrct = ? " +
    "AND tipo_docu = 'REM' " +
    "AND documento = ? " +
    "AND tipo_info = ? " ;
    
    private static final String SQL_NUMPLA_PLACA_REEXP =
    "SELECT " +
    "pr.numpla, " +
    "p.plaveh " +
    "FROM " +
    "plarem pr " +
    "LEFT JOIN planilla p  ON ( p.numpla = pr.numpla ) " +
    "WHERE " +
    "pr.numrem = ? " ;
    
    private static final String SQL_TIPO_REEXP =
    "SELECT " +
    "* " +
    "FROM " +
    "tablagen " +
    "WHERE " +
    "table_type = 'TIPO_REEXP' " ;
    
    private static final String SQL_UPDATE_TRAILER =
    "UPDATE " +
    "planilla " +
    "SET " +
    "platlr = ?, last_update = 'now()' " +
    "WHERE " +
    "numpla = ? " ;
    
    private static final String SQL_INSERT_AUXDOC =
    "INSERT INTO auxdoc " +
    "( dstrct, tipo_docu, documento, tipo_info, informacion, formato, creation_user, creation_date, user_update, last_update, base, reg_status ) " +
    "VALUES " +
    "( ?, 'REM', ?, ?, ?, 'varchar(50)', ?, 'now()', ?, 'now()', ?, '' ) " ;
    
    private static final String SQL_UPDATE_AUXDOC =
    "UPDATE " +
    "auxdoc " +
    "SET " +
    "informacion = ?, user_update = ?, last_update = 'now()' " +
    "WHERE " +
    "dstrct = ? " +
    "AND tipo_docu = 'REM' " +
    "AND documento = ? " +
    "AND tipo_info = ? " ;
    
    private static final String SQL_DESTINATARIOS_CLIENTE =
    "SELECT " +
    "* " +
    "FROM " +
    "remidest " +
    "WHERE " +
    "substring( codigo, 1, 4 ) = ? " +
    "ORDER BY " +
    "nombre ASC " ;
    
    private static final String SQL_LIMPIAR_DESTINATARIOS_REMESA =
    "DELETE FROM " +
    "remesadest " +
    "WHERE " +
    "numrem = ? " ;
    
    private static final String SQL_INSERTO_NUEVOS_DESTINATARIOS =
    "INSERT INTO remesadest " +
    "( reg_status, dstrct, codigo, numrem, tipo, last_update, user_update, creation_date, creation_user, base ) " +
    "VALUES " +
    "( '', ?, ?, ?, 'DE', 'now()', ?, 'now()', ?, ? ) " ;
    
    private static final String SQL_FACTURA_COMERCIAL =
    "SELECT " +
    "* " +
    "FROM  " +
    "remesa_docto  " +
    "WHERE    " +
    "dstrct = ?    " +
    "AND numrem = ? " +
    "AND tipo_doc IN (  " +
    "SELECT  " +
    "table_code  " +
    "FROM  " +
    "tablagen  " +
    "WHERE  " +
    "table_type = 'TDOC'  " +
    "AND dato = 'FAC' " +
    ")  " +
    "AND substring( destinatario, 1, 4 ) = ? " +
    "ORDER BY  " +
    "documento " ;
    
    private static final String SQL_EXISTE_FACTURA_COMERCIAL =
    "SELECT " +
    "* " +
    "FROM  " +
    "remesa_docto  " +
    "WHERE    " +
    "dstrct = ?    " +
    "AND numrem = ? " +
    "AND tipo_doc IN (  " +
    "SELECT  " +
    "table_code  " +
    "FROM  " +
    "tablagen  " +
    "WHERE  " +
    "table_type = 'TDOC'  " +
    "AND dato = 'FAC' " +
    ")  " +
    "AND upper(documento) = ? " +
    "ORDER BY  " +
    "documento " ;
    
    private static final String SQL_UPDATE_FACTURA_COMERCIAL =
    "UPDATE " +
    "remesa_docto " +
    "SET " +
    "documento = ?, last_update = 'now()', user_update = ? " +
    "WHERE    " +
    "dstrct = ?    " +
    "AND numrem = ? " +
    "AND tipo_doc IN (  " +
    "SELECT  " +
    "table_code  " +
    "FROM  " +
    "tablagen  " +
    "WHERE  " +
    "table_type = 'TDOC'  " +
    "AND dato = 'FAC' " +
    ")  " +
    "AND upper(documento) = ? " ;
    
    private static final String SQL_INSERT_FACTURA_COMERCIAL =
    "INSERT INTO remesa_docto " +
    "( reg_status, dstrct, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, destinatario, recibido_documento, user_recibio_documento, fecha_recibido, recibido_documento_rel, user_recibio_documento_rel, fecha_recibido_rel, last_update, user_update, creation_date, creation_user, cant_ref, cant_caja ) " +
    "VALUES " +
    "( '', ?, ?, ?, ?, '', '', ?, 'N', '', '0099-01-01 00:00:00', 'N', '', '0099-01-01 00:00:00', 'now()', ?, 'now()', ?, 0, 0 ) " ;
    
    private static final String SQL_TIPO_FACTURA_COMERCIAL =
    "SELECT  " +
    "table_code  " +
    "FROM  " +
    "tablagen  " +
    "WHERE  " +
    "table_type = 'TDOC'  " +
    "AND dato = 'FAC' " ;
    
    private static final String SQL_REMESAS_A_MODIFICAR =
    "SELECT  " +
    "r.numrem, pl.numpla " +
    "FROM  " +
    "remesa_docto r " +
    "LEFT JOIN plarem pl ON ( pl.numrem = r.numrem ) " +
    "WHERE    " +
    "upper(r.documento) = ? " +
    "ORDER BY  " +
    "r.documento " ;
    
    //
    
    private static final String SQL_REMESADOCTO="select 	documento, " +
    "	documento_rel " +
    " from 	remesa_docto " +
    " where 	numrem = ?" +
    "	AND (tipo_doc  IN ('009','FAC') " +
    "	or tipo_doc_rel  IN ('009','FAC') )";
    
    private static final String SQL_REMIDEST=" select  rd.nombre," +
    "	rd.tipo" +
    "   from 	remidest rd," +
    "           remesadest rm" +
    "   where 	rm.codigo = rd.codigo " +
    "           and rm.numrem = ?" +
    "           and substring(rm.codigo,4,1)='R'";
    
    private static final String SQL_REEXPEDICION="select 	r.numrem," +
    "	p.numpla," +
    "   p.plaveh," +
    "	r.tipoviaje," +
    "	r.aduana," +
    "	orirem," +
    "	desrem," +
    "	oripla," +
    "	despla" +
    " from 	remesa r" +
    "	inner join plarem pr ON (pr.numrem = r.numrem)" +
    "	inner join planilla p  ON (p.numpla = pr.numpla and p.despla = r.desrem and p.oripla <> r.orirem)" +
    " where r.numrem = ?	";
    private static final String SQL_DESTINATARIOS=" select  rd.nombre," +
    "	rd.tipo, rd.codigo" +
    "   from 	remidest rd," +
    "           remesadest rm" +
    "   where 	rm.codigo = rd.codigo " +
    "           and rm.numrem = ?" +
    "           and substring(rm.codigo,4,1)='D'" +
    "   order by " +
    "           rd.nombre asc";
    private static final String SQL_TIEMPOTRAMO=" SELECT TIEMPO FROM TRAMO WHERE DSTRCT = ? AND ORIGIN = ? AND DESTINATION = ? ";
    private static final String SQL_BUSCARREMESA="select 	r.*, " +
    "	cli.codcli, " +
    "	cli.nomcli," +
    "	m.nommoneda," +
    "	coalesce(to_char(doc.creation_date,'yyyy-MM-dd HH24:MI'),'0099-01-01') as fec_anul" +
    " from  	cliente cli, " +
    "	remesa r " +
    "	left join monedas m on (m.codmoneda=r.currency) " +
    "	left outer join (select creation_date,documento from documento_anulado)doc on (doc.documento= r.numrem )            " +
    " where 	r.numrem=?  " +
    "	and r.cliente=cli.codcli" +
    "       and r.reg_status <>'A'";
    
    
    private static final String SQL_BUSCARREMITENTES=" select  rd.nombre," +
    "	rd.tipo," +
    "   rd.codigo" +
    "   from 	remidest rd," +
    "           remesadest rm" +
    "   where 	rm.codigo = rd.codigo " +
    "           and rm.numrem = ?" +
    "           and substring(rm.codigo,4,1)='R'";
    
    private static final String SQL_BUSCARDESTINATARIOS=" select  rd.nombre," +
    "	rd.tipo," +
    "   rd.codigo" +
    "   from 	remidest rd," +
    "           remesadest rm" +
    "   where 	rm.codigo = rd.codigo " +
    "           and rm.numrem = ?" +
    "           and substring(rm.codigo,4,1)='D'";
    
    private static final String SQL_BUSCARDOCUMENTOS= "select * from remesa_docto where numrem=?";
    
    
    /** Creates a new instance of RemesaDAO */
    public RemesaDAO() {
        super("RemesaDAO.xml");
    }
    public RemesaDAO(String dataBaseName) {
        super("RemesaDAO.xml", dataBaseName);
    }
    /**
     * Procedimiento para obtener los datos a imprimir de una remesa
     * @autor mcelin
     * @return Listado de Remesas a imprimir.
     * throws Exception.
     */
    
    public List BuscarRemesas_no_impresas( String Numrem,String usuario,int tipo)throws Exception{
        
        PreparedStatement st    = null;
        ResultSet rs            = null;
        PreparedStatement st2   = null;
        ResultSet rs2           = null;
        PreparedStatement st3   = null;
        ResultSet rs3           = null;
        
        List Registros          = new LinkedList();
        try {
            
            
            if(tipo ==1){
                st = this.crearPreparedStatement("SQL_BUSCAR_REMESA_NO_IMPRESAS");
                st.setString(1, Numrem);
            }
            else{
                st = this.crearPreparedStatement("SQL_BUSCAR_REMESAS_NO_IMPRESAS");
                st.setString(1, usuario);
            }
            
            st2 = this.crearPreparedStatement("SQL_BUSCAR_DATOS_REM_DES");
            st3 = this.crearPreparedStatement("QUERY_DATOPLANILLA_REMESA");
            
            rs = st.executeQuery();
            while(rs.next()){
                Remesa datos = new Remesa();
                datos.setNumRem      (rs.getString   ("numrem"));
                datos.setFecRem      (rs.getTimestamp("fecrem"));
                datos.setCliente     (rs.getString   ("nomcli"));
                datos.setOriRem      (rs.getString   ("origen"));
                datos.setDesRem      (rs.getString   ("destino"));
                datos.setDocInterno  (rs.getString   ("docuinterno"));
                datos.setObservacion(rs.getString   ("observacion"));
                datos.setAgcRem      (rs.getString   ("agencia"));
                datos.setPrinter_Date(rs.getString   ("printer_date"));
                datos.setFiduciaria  (rs.getString   ("texto_remesa"));
                
                
                
                String[] Remitente    = rs.getString("remitente").split(",");
                String[] Destinatario = rs.getString("destinatario").split(",");
                
                
                // busqueda de los datos del remitente y destinatario
                st2.clearParameters();
                st2.setString(1, datos.getNumrem());
                rs2 = st2.executeQuery();
                while (rs2.next()){
                    if (rs2.getString("tipo").equals("DE")){
                        datos.setDestinatario   (rs2.getString("nombre"));
                        datos.setDirDestinatario(rs2.getString("direccion"));
                        datos.setCiuDestinatario(rs2.getString("ciudad"));
                    }
                    else if ( rs2.getString("tipo").equals("RE") ){
                        datos.setRemitente   (rs2.getString("nombre"));
                        datos.setDirRemitente(rs2.getString("direccion"));
                        datos.setCiuRemitente(rs2.getString("ciudad"));
                    }
                }
                
                // busqueda de datos de la planilla
                datos.setEstado("");
                st3.clearParameters();
                st3.setString(1, datos.getNumrem() );
                rs3 = st3.executeQuery();
                while(rs3.next()){
                    datos.setOc        ( rs3.getString("numpla"));
                    datos.setPlaca     ( rs3.getString("plaveh"));
                    datos.setConductor( rs3.getString("cedcon") +" - "+ rs3.getString("nombre"));
                    datos.setContenedor( rs3.getString("contenedores"));
                    
                    // remesa pendiente
                    if (rs3.getString("reg_status").equals("P") )
                        datos.setEstado("P");
                }
                
                Registros.add(datos);
                
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en BuscarRemesas_no_impresas [RemesaDAO]..." + e.getMessage() + " - " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            if (st2 != null) st2.close();
            if (rs2 != null) rs2.close();
            if (st3 != null) st3.close();
            if (rs3 != null) rs3.close();
            
            this.desconectar("SQL_BUSCAR_REMESA_NO_IMPRESAS");
            this.desconectar("SQL_BUSCAR_REMESAS_NO_IMPRESAS");
            this.desconectar("SQL_BUSCAR_DATOS_REM_DES");
            this.desconectar("QUERY_DATOPLANILLA_REMESA");
            
        }
        return Registros;
    }
    
    
    /**
     * Metodo que consulta la remesas no impresas de varias planillas
     * @autor jescandon
     * @modificado mfontalvo
     * @param Numpla planillas a buscar
     * @throws Exception.
     * @return Lista de remesas
     */
    public List BuscarRemesas_no_impresasPorPlanillas( String Numpla )throws SQLException{
        
        Connection        con   = null;
        PreparedStatement st    = null;
        PreparedStatement st2   = null;
        PreparedStatement st3   = null;
        ResultSet rs            = null;
        ResultSet rs2           = null;
        ResultSet rs3           = null;
        List Registros          = new LinkedList();
        
        
        try {
            
            
            
            con        = this.conectar  ("SQL_BUSCAR_REMESA_NO_IMPRESAS_PLANILLAS");
            String sql = this.obtenerSQL("SQL_BUSCAR_REMESA_NO_IMPRESAS_PLANILLAS");
            sql        = sql.replaceAll("#PLANILLA#", Numpla);
            
            
            st2 = this.crearPreparedStatement("SQL_BUSCAR_DATOS_REM_DES");
            st3 = this.crearPreparedStatement("QUERY_DATOPLANILLA_REMESA");
            
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                Remesa datos = new Remesa();
                datos.setNumRem      (rs.getString   ("numrem"));
                datos.setFecRem      (rs.getTimestamp("fecrem"));
                datos.setCliente     (rs.getString   ("nomcli"));
                datos.setOriRem      (rs.getString   ("origen"));
                datos.setDesRem      (rs.getString   ("destino"));
                datos.setDocInterno  (rs.getString   ("docuinterno"));
                datos.setObservacion(rs.getString   ("observacion"));
                datos.setAgcRem      (rs.getString   ("agencia"));
                datos.setPrinter_Date(rs.getString   ("printer_date"));
                datos.setFiduciaria  (rs.getString   ("texto_remesa"));
                
                
                
                String[] Remitente    = rs.getString("remitente").split(",");
                String[] Destinatario = rs.getString("destinatario").split(",");
                
                
                // busqueda de los datos del remitente y destinatario
                st2.clearParameters();
                st2.setString(1, datos.getNumrem());
                rs2 = st2.executeQuery();
                while (rs2.next()){
                    if (rs2.getString("tipo").equals("DE")){
                        datos.setDestinatario   (rs2.getString("nombre"));
                        datos.setDirDestinatario(rs2.getString("direccion"));
                        datos.setCiuDestinatario(rs2.getString("ciudad"));
                    }
                    else if ( rs2.getString("tipo").equals("RE") ){
                        datos.setRemitente   (rs2.getString("nombre"));
                        datos.setDirRemitente(rs2.getString("direccion"));
                        datos.setCiuRemitente(rs2.getString("ciudad"));
                    }
                }
                
                
                
                // busqueda de datos de la planilla
                datos.setEstado("");
                st3.clearParameters();
                st3.setString(1, datos.getNumrem() );
                rs3 = st3.executeQuery();
                while(rs3.next()){
                    datos.setOc        ( rs3.getString("numpla"));
                    datos.setPlaca     ( rs3.getString("plaveh"));
                    datos.setConductor( rs3.getString("cedcon") +" - "+ rs3.getString("nombre"));
                    datos.setContenedor( rs3.getString("contenedores"));
                    
                    // remesa pendiente
                    if (rs3.getString("reg_status").equals("P") )
                        datos.setEstado("P");
                }
                
                Registros.add(datos);
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en BuscarRemesas_no_impresas [RemesaDAO]..." + e.getMessage() + " - " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            if (st2 != null) st2.close();
            if (rs2 != null) rs2.close();
            
            this.desconectar("SQL_BUSCAR_REMESA_NO_IMPRESAS_PLANILLAS");
            this.desconectar("SQL_BUSCAR_DATOS_REM_DES");
            this.desconectar("QUERY_DATOPLANILLA_REMESA");
            
        }
        return Registros;
    }
    
    /**
     * Tenga en cuenta que esta vaina se actualiza en plarem y no en remesa siendo que hablamos de "actualizar remesa", ya que el campo
     * printer_date esta es en plarem, porque una remesa puede tener varias planillas
     * @autor mcelin
     * @param Remesa remesa a actualizar
     */
    void Actualizar_Remesa_Impresa(String Remesa) throws SQLException {
        PreparedStatement st    = null;
        try {
            st = this.crearPreparedStatement("SQL_ACTUALIZAR_REMESA_IMPRESA");
            st.setString(1, Remesa);
            st.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("Error en BuscarRemesas_no_impresas [RemesaDAO]..." + e.getMessage() + " - " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            this.desconectar("SQL_ACTUALIZAR_REMESA_IMPRESA");
        }
    }
    
    
    /**
     * Metodo que consulta el texto extendido
     * @param Distrito, distrito a consultar
     * @param Proyecto, Proyecto a consultar
     * @throws Exception.
     */
    public String TextoExtendido(String Distrito, String Proyecto)throws SQLException{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        String            Texto = "";
        try {
            st = this.crearPreparedStatement("SQL_BUSCAR_TEXTO_EXTENDIDO");
            st.setString(1, Distrito);
            st.setString(2, Proyecto);
            rs = st.executeQuery();
            if (rs.next()){
                Texto = rs.getString(1);
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TEXTO EXTENDIDO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs!=null) rs.close();
            if (st!=null) st.close();
            this.desconectar("SQL_BUSCAR_TEXTO_EXTENDIDO");
        }
        return Texto;
    }
    
    
    
    /**
     * Procedimiento de busqueda de documentos internos de una remesa
     * @autor: mfontalvo
     * @fecha: 2005-11-08
     * @params Remesa Remesa a Consultar
     * @return String de documentos internos
     * @throws Exception.
     */
    public String BuscarDocumentosInternos(String Remesa)throws SQLException{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            doc      = "";
        
        try {
            st = this.crearPreparedStatement("SQL_BUSCAR_DOCUMENTOS_INTERNOS");
            st.setString(1, Remesa);
            rs = st.executeQuery();
            while(rs.next()){
                doc += rs.getString(1) + ",";
            }
            if (doc.equals("")) doc = " ";
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina BuscarDocumentosInternos [RemesaDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_BUSCAR_DOCUMENTOS_INTERNOS");
        }
        return doc;
    }
    
    
    
    /**
     * Metodo SearchPlanilla_Pendiente, indica si una remesa tiene asociadas planillas pendientes ( reg_status = 'P' )
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @version : 1.0
     */
    public boolean SearchPlanilla_Pendiente( String numrem )throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        boolean           flag     = false;
        try {
            st = this.crearPreparedStatement("SQL_PLANILLAS_PENDIENTES");
            st.setString(1, numrem);
            rs = st.executeQuery();
            while(rs.next()){
                if( rs.getString("reg_status").equals("P")){
                    flag = true;
                    break;
                }
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina SearchPlanilla_Pendiente [RemesaImpresionDAO]... \n"+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_PLANILLAS_PENDIENTES");
        }
        return flag;
    }
    
    public void setRemesa(Remesa rem){
        this.rem = rem;
    }
    public List getPlanillas(){
        return planillas;
    }
    public Vector getRemesas(){
        return remesas;
    }
    public List getRem_print(){
        return this.rem_print;
    }
    
    
    public String insertRemPadre(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into remesa_padre (code,description,creation_user,base,dstrct) values(?,?,?,?,?)");
                st.setString(1,rem.getNumrem());
                st.setString(2, rem.getDescripcion());
                st.setString(3, rem.getUsuario());
                st.setString(4, base);
                st.setString(5, rem.getCia());
                sql = st.toString();
                //st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    
    
    public void asignarPadre()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update remesa set ot_padre=? where numrem=?");
                st.setString(1,rem.getPadre());
                st.setString(2, rem.getNumrem());
                st.executeUpdate();
                
                try{
                    
                    st = con.prepareStatement("insert into mov_ot_padre (dstrct,  remesa,  remesa_padre,  tip_remesa,base, creation_user, user_update) values(?,?,?,'P',?,?,?)");
                    st.setString(1,rem.getDistrito());
                    st.setString(2, rem.getNumrem());
                    st.setString(3, rem.getPadre());
                    st.setString(4, rem.getBase());
                    st.setString(5, rem.getUsuario());
                    st.setString(6, rem.getUsuario());
                    st.executeUpdate();
                    
                }catch ( SQLException ex ){
                    
                    st = con.prepareStatement("update mov_ot_padre set  user_update = ?, last_update = now() where  dstrct = ? and  remesa = ? and  remesa_padre = ?" );
                    
                    st.setString(1, rem.getUsuario());
                    st.setString(2,rem.getDistrito());
                    st.setString(3, rem.getNumrem());
                    st.setString(4, rem.getPadre());
                    st.executeUpdate();
                    
                }
                
                st = con.prepareStatement("update remesa_padre set description=? where code=?");
                st.setString(1,rem.getDescripcion());
                st.setString(2, rem.getPadre());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException(" ERROR RELACIONANDO LAS REMESA CON UN PADRE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    
    public void searchRemesa(String remesa, String login )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        rem= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select r.*, cli.nomcli, cli.codcli from remesa r, cliente cli where r.numrem=? and r.reg_status='' and r.cliente=cli.codcli and r.usuario=?");
                st.setString(1,remesa);
                st.setString(2,login);
                rs = st.executeQuery();
                if(rs.next()){
                    rem=Remesa.load(rs);
                    rem.setFaccial(rs.getString("facturacial"));
                    rem.setPadre(rs.getString("ot_padre"));
                    rem.setPesoReal(rs.getFloat("pesoreal"));
                    rem.setCantreal(rs.getFloat("qty_packed"));
                    rem.setUnidad(rs.getString("unit_packed"));
                    rem.setRemitente(rs.getString("remitente"));
                    rem.setDestinatario(rs.getString("destinatario"));
                    
                    //REMITENTES
                    st = con.prepareStatement(" select  rd.nombre," +
                    "	rd.tipo" +
                    "   from 	remidest rd," +
                    "           remesadest rm" +
                    "   where 	rm.codigo = rd.codigo " +
                    "           and rm.numrem = ?" +
                    "           and substring(rm.codigo,4,1)='R'");
                    st.setString(1,rem.getNumrem());
                    rs2 = st.executeQuery();
                    String remitentes="";
                    while(rs2.next()){
                        remitentes =remitentes + rs2.getString("nombre")+"<br><br>";
                    }
                    //DESTINATARIOS
                    st = con.prepareStatement(" select  rd.nombre," +
                    "	rd.tipo" +
                    "   from 	remidest rd," +
                    "           remesadest rm" +
                    "   where 	rm.codigo = rd.codigo " +
                    "           and rm.numrem = ?" +
                    "           and substring(rm.codigo,4,1)='R'");
                    st.setString(1,rem.getNumrem());
                    rs2 = st.executeQuery();
                    String destinatarios="";
                    while(rs2.next()){
                        destinatarios =destinatarios + rs2.getString("nombre")+"<br><br>";
                    }
                    rem.setNrem(remitentes);
                    rem.setNdest(destinatarios);
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA REMESA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    public boolean estaRemision(String remision )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        rem= null;
        boolean sw= false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select remision from remesa where remision =? and reg_status =''");
                st.setString(1,remision);
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA REMESA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public boolean estaPadre(String remesa )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        rem= null;
        boolean sw= false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select code from remesa_padre where code =? and reg_status =''");
                st.setString(1,remesa);
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA REMESA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    public void searchPlanillas(String remesa )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select pla.*, r.numrem, c.nombre from plarem r, planilla pla, nit c where r.numrem=? and  pla.numpla = r.numpla and pla.cedcon=c.cedula and pla.reg_status<>'A' ");
                st.setString(1,remesa);
                rs = st.executeQuery();
                Planilla pla = null;
                planillas = new LinkedList();
                while(rs.next()){
                    pla=Planilla.load(rs);
                    pla.setNomCond(rs.getString("nombre"));
                    pla.setNumrem(rs.getString("numrem"));
                    planillas.add(pla);
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA REMESA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    public void searchPlanillasImp(String remesa )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        planillas= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select pla.*, r.numrem, c.nombre from plarem r, planilla pla, nit c where r.numrem=? and  pla.numpla = r.numpla and pla.cedcon=c.cedula and pla.reg_status='' and pla.printer_date<>'0099-01-01 00:00:00' ");
                st.setString(1,remesa);
                rs = st.executeQuery();
                Planilla pla = null;
                planillas = new LinkedList();
                while(rs.next()){
                    pla=Planilla.load(rs);
                    pla.setNomCond(rs.getString("nombre"));
                    pla.setNumrem(rs.getString("numrem"));
                    planillas.add(pla);
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA REMESA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    public Remesa getRemesa(){
        return this.rem;
    }
    /**
     *Metodo que retorna el comando para anular una remesa
     *@autor: Karen Reales
     *@return: String con comando SQL
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String anularRem()throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql="";
        boolean sw=false;
        try {
            st =this.crearPreparedStatement("SQL_ANULARREMESA");
            st.setString(1,rem.getNumrem());
            sql = st.toString();
            
            st = this.crearPreparedStatement("SQL_ESTAANULADO");
            st.setString(1,rem.getDistrito());
            st.setString(2,rem.getNumpla());
            rs = st.executeQuery();
            if(rs.next()){
                sw=true;
            }
            if(!sw){
                st = this.crearPreparedStatement("SQL_INSERTARDOCANUL");
                st.setString(1,rem.getDistrito());
                st.setString(2,rem.getNumrem());
                st.setString(3,rem.getUsuario());
                st.setString(4,rem.getUsuario());
                st.setString(5,rem.getBase());
                sql =sql+";"+st.toString();
            }
            //st.executeUpdate();
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LAS PLANILLAS- REMESAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_ANULARREMESA");
            this.desconectar("SQL_ESTAANULADO");
            if(!sw){
                this.desconectar("SQL_INSERTARDOCANUL");
            }
        }
        return sql;
    }
    
    public void consultaRemesa(String fechafin, String fechaini,  String sj, String anulada, String cumplido, String nomcli, String base,String remision, String tipo_doc, String document)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        remesas= null;
        rem = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                if(remision.equals("")){
                    String sql ="";
                    if(!tipo_doc.equals("")|| !document.equals("")){
                        sql = this.SQL_CONSULTA.replaceAll("#LEFTJOIN#", "JOIN");
                    }else{
                        sql = this.SQL_CONSULTA.replaceAll("#LEFTJOIN#", "LEFT JOIN");
                    }
                    st = con.prepareStatement(sql);
                    st.setString(1,"%"+tipo_doc+"%");
                    st.setString(2, "%"+document+"%");
                    st.setString(3, fechaini);
                    st.setString(4, fechafin);
                    st.setString(5, sj+"%");
                    st.setString(6, anulada);
                    st.setString(7, cumplido+"%");
                    st.setString(8, "%"+nomcli+"%");
                    st.setString(9, base);
                    
                }
                else{
                    
                    st = con.prepareStatement(SQL_CONSULTA_REMESA);
                    st.setString(1, remision);
                    //////System.out.println("QRY RM: "+st);
                    rs = st.executeQuery();
                    if(!rs.next()){
                        st = con.prepareStatement(SQL_CONSULTA_REMISION);
                        st.setString(1, remision);
                    }
                }
                //////System.out.println("---------------CONSULTA REMESA -"+st.toString());
                rs = st.executeQuery();
                remesas = new Vector();
                while(rs.next()){
                    rem = new Remesa();
                    rem.setNumRem(rs.getString("numrem"));
                    rem.setOc(rs.getString("numpla"));
                    rem.setFeccum(rs.getString("feccum"));
                    rem.setFecRem(rs.getDate("fecrem"));
                    rem.setDocInterno("");
                    rem.setCia(rs.getString("cia"));
                    rem.setVlrRem(rs.getFloat("vlrrem"));
                    rem.setObservacion(rs.getString("observacion"));
                    rem.setPesoReal(rs.getFloat("pesoreal"));
                    rem.setUnitOfWork(rs.getString("unit_of_work"));
                    rem.setStdJobNo(rs.getString("std_job_no"));
                    rem.setDescripcion(rs.getString("descripcion"));
                    rem.setOriRem(rs.getString("origen"));
                    rem.setDesRem(rs.getString("destino"));
                    rem.setCliente(rs.getString("nomcli"));
                    rem.setRemision(rs.getString("remision"));
                    rem.setMoneda(rs.getString("nommoneda"));
                    
                    
                    //Jescandon 10-11-06
                    rem.setOrigenOC(rs.getString("origenOC"));
                    rem.setDestinoOC(rs.getString("destinoOC"));
                    
                    remesas.add(rem);
                }
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS " + e.getMessage());
        }
        finally{
            if (st != null)st.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    public void datosRemesa(String numrem)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        remesas= null;
        rem = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_CONSULTA_DATOS);
                st.setString(1, numrem);
                
                
                rs = st.executeQuery();
                if(rs.next()){
                    rem = new Remesa();
                    rem.setNumRem(rs.getString("numrem"));
                    rem.setOc(rs.getString("numpla"));
                    rem.setFeccum(rs.getString("feccum"));
                    rem.setFecRem(rs.getDate("fecrem"));
                    rem.setDocInterno(rs.getString("docuinterno"));
                    rem.setCia(rs.getString("cia"));
                    rem.setVlrRem(rs.getFloat("vlrrem"));
                    rem.setObservacion(rs.getString("observacion"));
                    rem.setPesoReal(rs.getFloat("pesoreal"));
                    rem.setUnitOfWork(rs.getString("unit_of_work"));
                    rem.setStdJobNo(rs.getString("std_job_no"));
                    rem.setDescripcion(rs.getString("descripcion"));
                    rem.setOriRem(rs.getString("origen"));
                    rem.setDesRem(rs.getString("destino"));
                    rem.setCliente(rs.getString("nomcli"));
                    rem.setCodcli(rs.getString("cliente"));
                }
                else{
                    st = con.prepareStatement(SQL_CONSULTA_DATOS_REMISION);
                    st.setString(1, numrem);
                    //////System.out.println("Se utilizo la segunda consulta :"+st.toString());
                    rs = st.executeQuery();
                    if(rs.next()){
                        rem = new Remesa();
                        rem.setNumRem(rs.getString("numrem"));
                        rem.setOc(rs.getString("numpla"));
                        rem.setFeccum(rs.getString("feccum"));
                        rem.setFecRem(rs.getDate("fecrem"));
                        rem.setDocInterno(rs.getString("docuinterno"));
                        rem.setCia(rs.getString("cia"));
                        rem.setVlrRem(rs.getFloat("vlrrem"));
                        rem.setObservacion(rs.getString("observacion"));
                        rem.setPesoReal(rs.getFloat("pesoreal"));
                        rem.setUnitOfWork(rs.getString("unit_of_work"));
                        rem.setStdJobNo(rs.getString("std_job_no"));
                        rem.setDescripcion(rs.getString("descripcion"));
                        rem.setOriRem(rs.getString("origen"));
                        rem.setDesRem(rs.getString("destino"));
                        rem.setCliente(rs.getString("nomcli"));
                        rem.setRemision(rs.getString("remision"));
                    }
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public Vector obtenerRemesas() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            Remesa r = null;
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                st = con.prepareStatement( SQL_OBTENER_PLANILLAS );
                rs = st.executeQuery();
                Vector v = new Vector();
                while ( rs.next() ) {
                    r = crearRemesa(rs);
                    v.addElement(r);
                }
                return v;
            }
            return null;
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
            if ( con != null ) {
                con.close();
            }
            return null;
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public Vector obtenerRemesas(String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            Remesa r = null;
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                st = con.prepareStatement( SQL_OBTENER_REMESAS_DE_PLANILLA );
                st.setString(1,numpla);
                rs = st.executeQuery();
                Vector v = new Vector();
                while ( rs.next() ) {
                    r = crearRemesa(rs);
                    v.addElement(r);
                }
                return v;
            }
            return null;
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
            if ( con != null ) {
                con.close();
            }
            return null;
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
    
    private Remesa crearRemesa( ResultSet rs ) throws SQLException {
        Remesa r = new Remesa();
        r.setAgcrem(rs.getString("agcrem"));
        r.setBase(rs.getString("base"));
        r.setCia(rs.getString("cia"));
        r.setCliente(rs.getString("cliente"));
        r.setCodtipocarga(rs.getString("codtipocarga"));
        r.setCorte(rs.getString("corte"));
        r.setCreation_date(rs.getString("creation_date"));
        r.setCrossdocking(rs.getString("crossdocking"));
        r.setDemoras(rs.getString("demoras"));
        r.setDerechos_cedido(rs.getString("derechos_cedido"));
        r.setDescripcion(rs.getString("descripcion"));
        r.setDesrem(rs.getString("desrem"));
        r.setDestinatario(rs.getString("destinatario"));
        r.setDocuinterno(rs.getString("docuinterno"));
        r.setEstado(rs.getString("estado"));
        r.setFacturacial(rs.getString("facturacial"));
        r.setFechaCargue(rs.getDate("fechacargue"));
        r.setFecRem(rs.getDate("fecrem"));
        r.setFecRemOri(rs.getDate("fecremori"));
        r.setLastupdate(rs.getString("lastupdate"));
        r.setNumrem(rs.getString("numrem"));
        r.setObservacion(rs.getString("observacion"));
        r.setOrirem(rs.getString("orirem"));
        r.setOt_padre(rs.getString("ot_padre"));
        r.setOt_rela(rs.getString("ot_rela"));
        r.setPesoReal(rs.getFloat("pesoreal"));
        r.setPlan_str_date(rs.getString("plan_str_date"));
        r.setPrinter_date(rs.getString("printer_date"));
        r.setQty_packed(rs.getFloat("qty_packed"));
        r.setQty_packed_received(rs.getFloat("qty_packed_received"));
        r.setQty_value(rs.getFloat("qty_value"));
        r.setQty_value_received(rs.getFloat("qty_value_received"));
        r.setReg_status(rs.getString("reg_status"));
        r.setRemision(rs.getString("remision"));
        r.setRemitente(rs.getString("remitente"));
        r.setStarem(rs.getString("starem"));
        r.setStd_job_no(rs.getString("std_job_no"));
        r.setTieneplanilla(rs.getString("tieneplanilla"));
        r.setTipoviaje(rs.getString("tipoviaje"));
        r.setUltreporte(rs.getString("ultreporte"));
        r.setUnidcam(rs.getString("unidcam"));
        r.setUnit(rs.getString("unit_of_work"));
        r.setUnit_packed(rs.getString("unit_packed"));
        r.setUsuario(rs.getString("usuario"));
        r.setVlrrem(rs.getFloat("vlrrem"));
        r.setVlrrem2(rs.getFloat("vlrrem2"));
        return r;
    }
    
    
    //planeacion
    /**
     * Getter for property r.
     * @return Value of property r.
     */
    public java.util.Vector getR() {
        return r;
    }
    
    /**
     * Setter for property r.
     * @param r New value of property r.
     */
    public void setR(java.util.Vector r) {
        this.r = r;
    }
    
    /**
     * Getter for property rm.
     * @return Value of property rm.
     */
    public Remesa getRm() {
        return rm;
    }
    
    /**
     * Setter for property rm.
     * @param rm New value of property rm.
     */
    public void setRm(Remesa rm) {
        this.rm = rm;
    }
    
    public void list( String numpla )throws SQLException{
        String consulta = "SELECT PL.NUMPLA, R.NUMREM, R.STD_JOB_NO FROM PLAREM PL, REMESA R WHERE PL.NUMPLA = ? AND PL.NUMREM = R.NUMREM" ;
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, numpla);
                rs= st.executeQuery();
                while(rs.next()){
                    rm = new Remesa();
                    rm.setNumrem(rs.getString("numrem"));
                    rm.setStd_job_no(rs.getString("std_job_no"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR NUMREM " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    //Jose 21/10/2005
    public void consultaRemesa(String rem1) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        remesas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_REMESA);
                st.setString(1, rem1);
                rs = st.executeQuery();
                remesas = new Vector();
                while(rs.next()){
                    rem = new Remesa();
                    rem.setNumRem(rs.getString("numrem"));
                    rem.setTipo_documento(rs.getString("tipo_doc")!=null?rs.getString("tipo_doc"):"");
                    rem.setDocumento(rs.getString("documento")!=null?rs.getString("documento"):"");
                    rem.setDocumento_rel(rs.getString("documento_rel")!=null?rs.getString("documento_rel"):"");
                    rem.setTipo_doc_rel(rs.getString("tipo_doc_rel")!=null?rs.getString("tipo_doc_rel"):"");
                    rem.setNom_destinatario(rs.getString("destinatario")!=null?rs.getString("destinatario"):"");
                    rem.setNombre_tipo_doc(rs.getString("tdoc")!=null?rs.getString("tdoc"):"");
                    rem.setNombre_tipo_doc_rel(rs.getString("tdoc_rel")!=null?rs.getString("tdoc_rel"):"");
                    remesas.add(rem);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS DOCUMENTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    //Jose 19/09/2006
    public void consultaRemesaDiscrepancia(String rem1, String numpla) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        remesas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_DISCREPANCIA_REMESA);
                st.setString(1, rem1);
                st.setString(2, numpla);
                rs = st.executeQuery();
                remesas = new Vector();
                while(rs.next()){
                    rem = new Remesa();
                    rem.setNumRem(rs.getString("numrem"));
                    rem.setTipo_documento(rs.getString("tipo_doc")!=null?rs.getString("tipo_doc"):"");
                    rem.setDocumento(rs.getString("documento")!=null?rs.getString("documento"):"");
                    rem.setDocumento_rel(rs.getString("documento_rel")!=null?rs.getString("documento_rel"):"");
                    rem.setTipo_doc_rel(rs.getString("tipo_doc_rel")!=null?rs.getString("tipo_doc_rel"):"");
                    rem.setNom_destinatario(rs.getString("destinatario")!=null?rs.getString("destinatario"):"");
                    rem.setNombre_tipo_doc(rs.getString("tdoc")!=null?rs.getString("tdoc"):"");
                    rem.setNombre_tipo_doc_rel(rs.getString("tdoc_rel")!=null?rs.getString("tdoc_rel"):"");
                    remesas.add(rem);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS DOCUMENTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    
    public void consultaRemesaDiscrepancia(String planilla, String remesa, String tipo_doc, String documento, int num_discre ) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        remesas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_REMESA_DISCREPANCIA);
                st.setString(1, planilla);
                st.setString(2, remesa);
                st.setString(3, tipo_doc);
                st.setString(4, documento);
                st.setInt(5, num_discre);
                rs = st.executeQuery();
                remesas = new Vector();
                while(rs.next()){
                    rem = new Remesa();
                    rem.setNumRem(rs.getString("numrem"));
                    rem.setTipo_documento(rs.getString("document_type"));
                    rem.setDocumento(rs.getString("documento"));
                    rem.setDocumento_rel(rs.getString("documento_rel"));
                    rem.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                    rem.setNom_destinatario(rs.getString("destinatario"));
                    rem.setNombre_tipo_doc(rs.getString("documento1"));
                    rem.setNombre_tipo_doc_rel(rs.getString("doc_rel"));
                    remesas.add(rem);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS DOCUMENTOS DISCREPANCIAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public String remesaStandarJob(String remesa) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String remesas = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(this.SQL_MOSTRAR_STANDARJOB);
                st.setString(1, remesa);
                rs = st.executeQuery();
                
                if (rs.next()){
                    remesas = rs.getString( "std_job_no");
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE BUSQUEDA DEL STANDAR JOB " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return remesas;
    }
    /**
     *Metodo para buscar las remesas relacionadas a una planilla y a ninguna otra
     *@autor: Karen Reales
     *@param: numpla es el numreo de la planilla con la cual se relaciona la remesa
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarRemesasAlone(String numpla) throws SQLException{
        
        PreparedStatement st  = null;
        PreparedStatement st2 = null;
        PreparedStatement st3 = null;
        
        ResultSet         rs = null,rs1=null,rs2=null;
        remesas              =  null;
        
        String        query1 = "SQL_LISTA_REMESAS";
        String        query2 = "SQL_BUSCARREMALONE";
        String        query3 = "SQL_BUSCARREMESA";
        
        try{
            
            
            remesas = new Vector();
            
            st = this.crearPreparedStatement( query1 );
            st.setString(1, numpla);
            rs = st.executeQuery();
            
            st2 = this.crearPreparedStatement( query2 );
            st3 = this.crearPreparedStatement( query3 );
            
            while (rs.next()){
                st2.setString(1, rs.getString("numrem"));
                rs1  =  st2.executeQuery();
                
                if ( rs1.next() ){
                    st3.setString(1, rs1.getString("numrem"));
                    rs2 = st3.executeQuery();
                    if(rs2.next()){
                        rem = Remesa.load(rs2);
                    }
                    remesas.add(this.rem);
                    st3.clearParameters();
                }
                
                st2.clearParameters();
                
            }
            
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE REMESAS SOLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if(st!=  null )  st.close();
            if(st2!= null )  st2.close();
            if(st3!= null )  st3.close();
            
            if(rs!=null)  rs.close();
            if(rs1!=null) rs1.close();
            if(rs2!=null) rs2.close();
            
            this.desconectar( query1 );
            this.desconectar( query2 );
            this.desconectar( query3 );
            
        }
    }
    
    
    public void buscaRemesa( String numrem ) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        remesas=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT * FROM REMESA WHERE NUMREM=?");
                st.setString(1, numrem);
                rs = st.executeQuery();
                remesas = new Vector();
                while(rs.next()){
                    rem = new Remesa();
                    rem.setFecRem(rs.getDate("fecrem"));
                    rem.setStdJobNo(rs.getString("std_job_no"));
                    rem.setPesoReal(rs.getFloat("pesoreal"));
                    remesas.add(rem);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     *Metodo que setea un Vector con los Clientes de una remesa dada el nombre del Cliente
     *@autor:David Lamadrid
     *@param: String cliente
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void clientesRemesa( String planilla) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vClientes=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SQL_CLIENTES);
                st.setString(1, planilla);
                //////System.out.println("CONSULTA "+st);
                rs = st.executeQuery();
                this.vClientes = new Vector();
                while(rs.next()){
                    Vector fila= new Vector();
                    fila.add(rs.getString("cliente"));
                    fila.add(rs.getString("nombre"));
                    this.vClientes.add(fila);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Getter for property vClientes.
     * @return Value of property vClientes.
     */
    public java.util.Vector getVClientes() {
        return vClientes;
    }
    
    /**
     * Setter for property vClientes.
     * @param vClientes New value of property vClientes.
     */
    public void setVClientes(java.util.Vector vClientes) {
        this.vClientes = vClientes;
    }
    /**
     * Metodo consultarInfoRemesa, busca la informacion correspondiente a la remesa,
     * @param: remesa y dstrct
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void consultarInfoRemesa(String remesa,String dstrct )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        rem= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SQL_BUSCAR_INFOREMENSA);
                st.setString(1, dstrct);
                st.setString(2, dstrct);
                st.setString(3, remesa);
                //////System.out.println("");
                //////System.out.println(st);
                rs = st.executeQuery();
                if(rs.next()){
                    rem=Remesa.load(rs);
                    rem.setNumrem(rs.getString("numrem"));
                    rem.setNombre_cli(rs.getString("nomcli"));
                    rem.setCliente(rs.getString("cliente"));
                    rem.setPadre(rs.getString("ot_padre"));
                    rem.setPesoReal(rs.getFloat("pesoreal"));
                    rem.setCantreal(rs.getFloat("qty_packed"));
                    rem.setUnidad(rs.getString("unidad"));
                    rem.setRemitente(rs.getString("remitente"));
                    rem.setDestinatario(rs.getString("destinatario"));
                    rem.setTarifa(rs.getFloat("Qty_value"));
                    rem.setVlr_pesos(rs.getDouble("vlrrem2"));
                    rem.setCurrency(rs.getString("currency"));
                    rem.setCrossdocking(rs.getString("crossdocking"));
                    rem.setEstado(rs.getString("reg_status").equals("A")?"ANULADA":"ACTIVA");
                    rem.setFec_anul(rs.getString("fec_anul").equals("0099-01-01")?"No anulada":rs.getString("fec_anul"));
                    rem.setFecRem(rs.getDate("fecrem"));
                    rem.setUnit_packed(rs.getString("unit_packed"));
                    rem.setNrem(rs.getString("nomremitente"));
                    rem.setNfacturable(rs.getString("n_facturable"));
                    //20-04-2006 informacion de cumplido
                    rem.setCant_cumplida(rs.getString("cantidad"));
                    rem.setFeccum(rs.getString("fec_cum").substring(0,16));
                    rem.setUnidad_cumplida(rs.getString("unidad_cum"));
                    rem.setUsuario_cumple(rs.getString("usu_cumple"));
                    rem.setCumplida(rs.getString("cumplido"));
                    rem.setPadre(rs.getString("ot_padre"));
                    rem.setCmc(rs.getString("cmc"));
                    
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA REMESA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void setRemesas(java.util.Vector r) {
        this.remesas = r;
    }
    /**
     *Metodo que permite obtener una remesa dado el numero de la remesa
     *@autor: David Pi�a
     *@param numrem N�mero de la Remesa
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getRemesaIC( String numrem ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs  =null;
        try {
            st = this.crearPreparedStatement( "SQL_OBTENER_REMESA_IC" );
            st.setString( 1, numrem );
            rs = st.executeQuery();
            
            if (rs.next()){
                rem = new Remesa();
                rem.setNumrem( (rs.getString("numrem")!=null)?rs.getString("numrem"):"" );
                rem.setCodcli( (rs.getString("nit")!=null)?rs.getString("nit"):"" );
                rem.setNombre_cli( (rs.getString("nomcli")!=null)?rs.getString("nomcli"):"" );
                rem.setAgcrem( (rs.getString("agcrem")!=null)?rs.getString("agcrem"):"" );
                rem.setCiuRemitente( (rs.getString("nomagencia")!=null)?rs.getString("nomagencia"):"" );
                rem.setOrirem( (rs.getString("orirem")!=null)?rs.getString("orirem"):"" );
                rem.setNombre_ciu( (rs.getString("nomciu")!=null)?rs.getString("nomciu"):"" );
                rem.setDestinoRelacionado( (rs.getString("agasoc")!=null)?rs.getString("agasoc"):"" );
                rem.setNombreDestinoRelacionado( (rs.getString("nomagenciarel")!=null)?rs.getString("nomagenciarel"):"" );
                rem.setFeccum( (rs.getString("fecrem")!=null)?rs.getString("fecrem"):"" );
                
            }
        } catch( SQLException e ){
            throw new SQLException( "Error en getRemesaIC [RemesaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
        finally{
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar( "SQL_OBTENER_REMESA_IC" );
        }
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        
        return vector;
        
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector( java.util.Vector vector ) {
        
        this.vector = vector;
        
    }
    
    /**
     * Getter for property otro_vector.
     * @return Value of property otro_vector.
     */
    public java.util.Vector getOtro_vector() {
        return otro_vector;
    }
    
    /**
     * Setter for property otro_vector.
     * @param otro_vector New value of property otro_vector.
     */
    public void setOtro_vector(java.util.Vector otro_vector) {
        this.otro_vector = otro_vector;
    }
    
    /**
     *Metodo que busca los destinatarios de una remesa
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void bucarDestinatariosDeUnaRemesa( String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_DESTINATARIOS );
                st.setString( 1, numrem );
                ////System.out.println("*** bucarDestinatariosDeUnaRemesa: " + st.toString());
                rs = st.executeQuery();
                
                this.otro_vector = new Vector();
                
                while ( rs.next() ) {
                    
                    a = new Aduana();
                    a.setCodigo( rs.getString("codigo") != null ? rs.getString( "codigo" ) : "" );
                    this.otro_vector.add( a );
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE bucarDestinatariosDeUnaRemesa() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que busca los Tipos de Reexpedicion
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void bucarDestinatariosDeUnCliente( String codcli ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        codcli = codcli.substring( 3 , 6 );
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_DESTINATARIOS_CLIENTE );
                st.setString( 1, codcli + "D" );
                ////System.out.println("*** SQL_DESTINATARIOS_CLIENTE: " + st.toString());
                rs = st.executeQuery();
                
                this.vector = new Vector();
                
                while ( rs.next() ) {
                    
                    a = new Aduana();
                    a.setCodigo( rs.getString("codigo") != null ? rs.getString( "codigo" ) : "" );
                    a.setDestinatarios( rs.getString("nombre") != null ? rs.getString( "nombre" ) : "" );
                    this.vector.add( a );
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE bucarTiposReexpedicion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que busca las remesas a modificar.
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void bucarRemesasParaModificar( String factcial ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_REMESAS_A_MODIFICAR );
                st.setString( 1, factcial );
                ////System.out.println("*** SQL_REMESAS_A_MODIFICAR: " + st.toString());
                rs = st.executeQuery();
                
                this.remesas = new Vector();
                
                while ( rs.next() ) {
                    
                    a = new Aduana();
                    a.setNumrem( rs.getString("numrem") != null ? rs.getString( "numrem" ).toUpperCase() : "" );
                    a.setNumpla( rs.getString("numpla") != null ? rs.getString( "numpla" ).toUpperCase() : "" );
                    this.remesas.add( a );
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE bucarRemesasParaModificar() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que busca los Tipos de Reexpedicion
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void bucarTiposReexpedicion() throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                //TIPO DE REEXPEDICION
                st = con.prepareStatement( SQL_TIPO_REEXP );
                ////System.out.println("*** SQL_TIPO_REEXP: " + st.toString());
                rs = st.executeQuery();
                
                this.vector = new Vector();
                
                while ( rs.next() ) {
                    
                    a = new Aduana();
                    a.setTipo_reexp( rs.getString("referencia") != null ? rs.getString( "referencia" ) : "" );
                    this.vector.add( a );
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE bucarTiposReexpedicion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que mira si existe un tipo de reexpedicion
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public boolean existeTipoReexpedicion( String distrito, String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_AUXILIAR_REMESA );
                st.setString( 1, distrito );
                st.setString( 2, numrem );
                st.setString( 3, "TIPO_REEXP" );
                ////System.out.println("*** existeTipoReexpedicion: " + st.toString());
                rs = st.executeQuery();
                
                if ( rs.next() ) {
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existeTipoReexpedicion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     *Metodo que mira si existe un agente de exportacion
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public boolean existeAgenteExportacion( String distrito, String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_AUXILIAR_REMESA );
                st.setString( 1, distrito );
                st.setString( 2, numrem );
                st.setString( 3, "AGEEXP" );
                ////System.out.println("*** existeAgenteExportacion: " + st.toString());
                rs = st.executeQuery();
                
                if ( rs.next() ) {
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existeAgenteExportacion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     *Metodo que mira si existe un agente de importacion
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public boolean existeAgenteImportacion( String distrito, String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_AUXILIAR_REMESA );
                st.setString( 1, distrito );
                st.setString( 2, numrem );
                st.setString( 3, "AGEIMP" );
                ////System.out.println("*** existeAgenteImportacion: " + st.toString());
                rs = st.executeQuery();
                
                if ( rs.next() ) {
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existeAgenteImportacion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     *Metodo que mira si existe un agente de almacenadora
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public boolean existeAlmacenadora( String distrito, String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_AUXILIAR_REMESA );
                st.setString( 1, distrito );
                st.setString( 2, numrem );
                st.setString( 3, "ALMACENADO" );
                ////System.out.println("*** existeAlmacenadora: " + st.toString());
                rs = st.executeQuery();
                
                if ( rs.next() ) {
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existeAlmacenadora() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     *Metodo que mira si existe una factura comercial.
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public boolean existeFacturaComercial( String distrito, String numrem, String documento ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_EXISTE_FACTURA_COMERCIAL );
                st.setString( 1, distrito );
                st.setString( 2, numrem );
                st.setString( 3, documento );
                ////System.out.println("*** existeFacturaComercial: " + st.toString());
                rs = st.executeQuery();
                
                if ( rs.next() ) {
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existeFacturaComercial() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     *Metodo que mira si existe una factura comercial.
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public String tipoFacturaComercial() throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String tipo = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_TIPO_FACTURA_COMERCIAL );
                ////System.out.println("*** tipoFacturaComercial: " + st.toString());
                rs = st.executeQuery();
                
                if ( rs.next() ) {
                    
                    tipo = rs.getString("table_code") != null ? rs.getString( "table_code" ) : "";
                    
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE tipoFacturaComercial() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
        return tipo;
        
    }
    
    /**
     *Metodo que actualiza la tabla planilla con la nueva placa de trailer
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void guardarPlacaTrailer( String platlr, String numpla ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_UPDATE_TRAILER );
                st.setString( 1, platlr );
                st.setString( 2, numpla );
                ////System.out.println("*** SQL_UPDATE_TRAILER: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE guardarPlacaTrailer() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que elimina todos los destinatarios de una remesa
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void eliminarDestinatariosDeUnaRemesa( String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_LIMPIAR_DESTINATARIOS_REMESA );
                st.setString( 1, numrem );
                ////System.out.println("*** eliminarDestinatariosDeUnaRemesa: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE eliminarDestinatariosDeUnaRemesa() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que inserta un destinatario en remesadest
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void insertarNuevosDestinatarios( String distrito, String codigo, String numrem, String usu, String base ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_INSERTO_NUEVOS_DESTINATARIOS );
                st.setString( 1, distrito );
                st.setString( 2, codigo );
                st.setString( 3, numrem );
                st.setString( 4, usu );
                st.setString( 5, usu );
                st.setString( 6, base );
                ////System.out.println("*** insertarNuevosDestinatarios: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertarNuevosDestinatarios() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que inserta un tipo de reexpedicion en auxrem
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void insertarTipoReexpedicion( String distrito, String numrem, String tipo_reexp, String usu, String base ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_INSERT_AUXDOC );
                st.setString( 1, distrito );
                st.setString( 2, numrem );
                st.setString( 3, "TIPO_REEXP" );
                st.setString( 4, tipo_reexp );
                st.setString( 5, usu );
                st.setString( 6, usu );
                st.setString( 7, base );
                ////System.out.println("*** insertarTipoReexpedicion: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertarTipoReexpedicion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que inserta un agente de exportacion en auxrem
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void insertarAgenteExportacion( String distrito, String numrem, String ageexp, String usu, String base ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_INSERT_AUXDOC );
                st.setString( 1, distrito );
                st.setString( 2, numrem );
                st.setString( 3, "AGEEXP" );
                st.setString( 4, ageexp );
                st.setString( 5, usu );
                st.setString( 6, usu );
                st.setString( 7, base );
                ////System.out.println("*** insertarAgenteExportacion: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertarAgenteExportacion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que inserta un agente de importacion en auxrem
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void insertarAgenteImportacion( String distrito, String numrem, String ageimp, String usu, String base ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_INSERT_AUXDOC );
                st.setString( 1, distrito );
                st.setString( 2, numrem );
                st.setString( 3, "AGEIMP" );
                st.setString( 4, ageimp );
                st.setString( 5, usu );
                st.setString( 6, usu );
                st.setString( 7, base );
                ////System.out.println("*** insertarAgenteImportacion: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertarAgenteImportacion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que inserta una almacenadora en auxrem
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void insertarAlmacenadora( String distrito, String numrem, String almacenadora, String usu, String base ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_INSERT_AUXDOC );
                st.setString( 1, distrito );
                st.setString( 2, numrem );
                st.setString( 3, "ALMACENADO" );
                st.setString( 4, almacenadora );
                st.setString( 5, usu );
                st.setString( 6, usu );
                st.setString( 7, base );
                ////System.out.println("*** insertarAlmacenadora: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertarAlmacenadora() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que inserta una factura comercial en remesa_docto
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void insertarFacturaComercial( String distrito, String numrem, String factcial, String coddest, String usu ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        String tipo = tipoFacturaComercial();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_INSERT_FACTURA_COMERCIAL );
                st.setString( 1, distrito );
                st.setString( 2, numrem );
                st.setString( 3, tipo );
                st.setString( 4, factcial );
                st.setString( 5, coddest );
                st.setString( 6, usu );
                st.setString( 7, usu );
                ////System.out.println("*** insertarFacturaComercial: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertarFacturaComercial() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que actualiza la tabla planilla con la nueva placa de trailer
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void actualizarTipoReexpedicion( String tipo_reexp, String usu, String distrito, String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_UPDATE_AUXDOC );
                st.setString( 1, tipo_reexp );
                st.setString( 2, usu );
                st.setString( 3, distrito );
                st.setString( 4, numrem );
                st.setString( 5, "TIPO_REEXP" );
                ////System.out.println("*** actualizarTipoReexpedicion: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE actualizarTipoReexpedicion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que actualiza la tabla aux rem con el agente exportacion
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void actualizarAgenteExportacion( String ageexp, String usu, String distrito, String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_UPDATE_AUXDOC );
                st.setString( 1, ageexp );
                st.setString( 2, usu );
                st.setString( 3, distrito );
                st.setString( 4, numrem );
                st.setString( 5, "AGEEXP" );
                ////System.out.println("*** actualizarAgenteExportacion: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE actualizarAgenteExportacion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que actualiza la tabla auxrem el agente importacion
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void actualizarAgenteImportacion( String ageimp, String usu, String distrito, String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_UPDATE_AUXDOC );
                st.setString( 1, ageimp );
                st.setString( 2, usu );
                st.setString( 3, distrito );
                st.setString( 4, numrem );
                st.setString( 5, "AGEIMP" );
                ////System.out.println("*** actualizarAgenteImportacion: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE actualizarAgenteImportacion() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que actualiza la tabla auxrem la almacenadora
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void actualizarAlmacenadora( String almacenadora, String usu, String distrito, String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_UPDATE_AUXDOC );
                st.setString( 1, almacenadora );
                st.setString( 2, usu );
                st.setString( 3, distrito );
                st.setString( 4, numrem );
                st.setString( 5, "ALMACENADO" );
                ////System.out.println("*** actualizarAlmacenadora: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE actualizarAlmacenadora() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que actualiza en la tabla remesa_docto la factura comercial
     *@autor  : LREALES
     *@fecha  : 22 ENERO 2007
     *@throws : En caso de que un error de base de datos ocurra.
     */
    public void actualizarFacturaComercial( String factcial, String usu, String distrito, String numrem, String factvieja ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( SQL_UPDATE_FACTURA_COMERCIAL );
                st.setString( 1, factcial );
                st.setString( 2, usu );
                st.setString( 3, distrito );
                st.setString( 4, numrem );
                st.setString( 5, factvieja );
                ////System.out.println("*** actualizarFacturaComercial: " + st.toString());
                st.executeUpdate();
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE actualizarFacturaComercial() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     *Metodo que permite calcular el tiempo de viaje de una ruta
     *@autor: David Pi�a
     *@param ruta Codigo de la ruta
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public float calcularDuracion( String ruta )  throws SQLException {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        String via = "";
        float duracion = 0;
        try {
            if( ruta.length() >= 4 ){
                String ori = ruta.substring( 0, 2 );
                String des = ruta.substring( 2, 4 );
                String secuencia = (ruta.length() == 4)? "" : ruta.substring( 4, ruta.length() );
                poolManager = PoolManager.getInstance();
                con = poolManager.getConnection("fintra");
                st = con.prepareStatement( SQL_OBTENER_VIA );
                st.setString( 1, ori );
                st.setString( 2, des );
                st.setString( 3, secuencia );
                rs = st.executeQuery();
                if( rs.next() ){
                    via = rs.getString( 1 );
                }
                for (int i=0; i < via.length() -2 ; i += 2 ){
                    String tOrigen  = via.substring( i, i+2 );
                    String tDestino = via.substring( i+2, i+4 );
                    st = con.prepareStatement( SQL_TIEMPOTRAMO );
                    st.setString(1,  "FINV");
                    st.setString(2,  tOrigen);
                    st.setString(3,  tDestino);
                    rs2 = st.executeQuery();
                    while (rs2.next()){
                        duracion += rs2.getFloat(1);
                        break;
                    }
                    
                }
            }
        }catch( Exception ex ){
            throw new SQLException( "Error en RemesaDAO ---> calcularDuracion( String via )......." + ex.getMessage() );
        }finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return duracion;
        
    }
    /**
     * Getter for property padre.
     * @return Value of property padre.
     */
    public com.tsp.operation.model.beans.Remesa getPadre() {
        return padre;
    }
    
    /**
     * Setter for property padre.
     * @param padre New value of property padre.
     */
    public void setPadre(com.tsp.operation.model.beans.Remesa padre) {
        this.padre = padre;
    }
    
    /*************************************************************
     *Metodo que obtiene las remesas que tienen asociada la
     * remesa padre con el numero dado
     *@autor: Osvaldo P�rez
     *@param: padre, numero de la remesa padre de la que se desea
     *        consultar sus 'hijas'
     *@return: Vector, vector con los numeros de remesa
     *@throws: En caso de que un error de base de datos ocurra.
     *************************************************************/
    public Vector obtenerRemesasHijas( String padre ) throws Exception {
        
        Vector hijas = new Vector();
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement( " SELECT * FROM remesa WHERE ot_padre = ? AND reg_status != 'A' " );
                st.setString( 1, padre );
                rs = st.executeQuery();
                
                while( rs.next() ){
                    hijas.add( rs.getString( "numrem" ) );
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN RemesaDAO.obtenerRemesasHijas " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return hijas;
    }
    
    /*Consulta remesa*/
    
    public boolean existeTrailer(String placa)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select p.recurso, r.* from placa p left join recursos r on(p.recurso = r.equipo) where placa = ? and (r.tipo_recurso = 'T' or r.tipo_recurso = 'C' )");
                st.setString(1, placa);
                //System.out.println("query de trailer     =   "+st.toString());
                rs = st.executeQuery();
                return rs.next();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  EXISTE TRAILER" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     *Metodo que permite buscar los tiempos de los viajes de una OC
     *@autor: Jose de la rosa
     *@param fecha inicial, fecha final, cliente y si estan cumplidas
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void consultaRemesasPagador( String fecini, String fecfin, String cliente, String cumplidas, String distrito) throws SQLException {
        PreparedStatement   st  = null;
        ResultSet           rs  = null;
        Connection          con = null;
        remesas = null;
        String             query   = "SQL_REMESAS_PAGADOR";
        try {
            con         =   this.conectar(query);
            String sql  =   this.obtenerSQL( query ).replaceAll("#CUMPLIDAS#", cumplidas.equals("")?"AND reg_status = 'C'":"" );
            st          =   con.prepareStatement( sql );
            st.setString( 1, fecini+" 00:00" );
            st.setString( 2, fecfin+" 23:59" );
            st.setString( 3, cliente );
            st.setString( 4, distrito );
            rs = st.executeQuery();
            remesas = new Vector();
            while (rs.next()){
                rem = new Remesa();
                rem.setNumrem       ( (rs.getString("numrem")!=null)?rs.getString("numrem"):""      );//numero de remesa
                rem.setNombre_cli   ( (rs.getString("clientes")!=null)?rs.getString("clientes"):""  );//nombre del cliente
                rem.setOrirem       ( (rs.getString("origen")!=null)?rs.getString("origen"):""      );//origen remesa
                rem.setDesrem       ( (rs.getString("destino")!=null)?rs.getString("destino"):""    );//destino remesa
                rem.setStdJobNo     ( rs.getString("estandar")!=null?rs.getString("estandar"):""   );//estandar
                rem.setEstado       ( rs.getString("estado")!=null?rs.getString("estado"):""       );//estado
                rem.setPagador      ( rs.getString("pagador")!=null?rs.getString("pagador"):""     );//pagador
                remesas.add(rem);
            }
        } catch( SQLException e ){
            throw new SQLException( "Error en consultaRemesasPagador [RemesaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
        finally{
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar( query );
        }
    }
    
    /**
     *Metodo que permite actualizar el campo pagador de la remesa
     *@autor: Jose de la rosa
     *@param fecha inicial, fecha final, cliente y si estan cumplidas
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String updateCod_pagador( String numrem, String cod_pagador )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql="";
        try {
            st = crearPreparedStatement("SQL_UPDATE_PAGADOR");
            st.setString(1, cod_pagador);
            st.setString(2, numrem);
            sql = st.toString();
        }catch(SQLException ex){
            throw new SQLException("ERROR EN LA FUNCION updateCod_pagador ([RemesaDAO]......., "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_PAGADOR");
        }
        return sql;
    }
    
    
    
    
    public String obtenerClienteRemesa( String numrem ) throws SQLException {
        
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if( con != null ) {
                
                st = con.prepareStatement( this.SQL_CODCLI_REMESA );
                st.setString( 1, numrem );
                rs = st.executeQuery();
                
                if( rs.next() ){
                    return rs.getString("cliente");
                }
                
            }
            
        } catch( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE actualizarFacturaComercial() " + e.getMessage() );
            
        }
        finally{
            
            if ( st != null ) {
                try {
                    st.close();
                } catch( SQLException e ) {
                    e.printStackTrace();
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
        return null;
        
    }
    
    
    /**
     *Metodo para insertar una remesa en la base de datos.
     *@autor: Karen Reales
     *@param: base de la remesa indica si es web.
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String insertRem(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                String fcial = "";
                if(rem.getFaccial()!=null)
                    fcial =rem.getFaccial();
                st = con.prepareStatement(SQL_INSERT);
                st.setString(1,rem.getNumrem());
                st.setString(2, rem.getAgcRem());
                st.setString(3, rem.getOriRem());
                st.setString(4, rem.getDesRem());
                st.setString(5,"000"+rem.getCliente());
                st.setString(6, rem.getUnidcam());
                st.setString(7, rem.getTipoViaje());
                st.setString(8,rem.getCia());
                st.setString(9, rem.getUsuario());
                st.setFloat(10, rem.getPesoReal());
                st.setString(11,rem.getUnitOfWork());
                st.setString(12, rem.getStdJobNo());
                st.setFloat(13, rem.getVlrRem());
                st.setString(14, rem.getDocInterno());
                st.setString(15, rem.getDestinatario());
                st.setString(16, rem.getRemitente());
                st.setString(17, rem.getObservacion());
                st.setString(18, rem.getDescripcion());
                st.setString(19, rem.getRemision());
                st.setString(20, base);
                st.setString(21, fcial);
                st.setFloat(22, rem.getCantreal());
                st.setString(23, rem.getUnidad());
                st.setString(24, rem.getFacturable());
                st.setFloat(25, rem.getQty_value());
                st.setString(26, rem.getCurrency());
                st.setDouble(27, rem.getVlr_pesos());
                st.setString(28, rem.getNfacturable());
                st.setString(29, rem.getCrossdocking());
                st.setString(30, rem.getAduana());
                st.setString(31, rem.getTipoCarga());
                st.setString(32, rem.getCadena());
                st.setString(33, rem.getPagador());
                st.setString(34, rem.getCmc());
                sql = st.toString();
                //st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    
    public Vector obtenerTiemposRemesa( String fecini, String fecfin, String cliente, String tipo_viaje) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs  =null;
        Connection con = null;
        remesas = null;
        try {
            con = this.conectar("SQL_OBTENER_TIEMPOS_VIAJE");
            String sql = this.obtenerSQL("SQL_OBTENER_TIEMPOS_VIAJE");
            sql = ( tipo_viaje.length()!=0 ) ? sql.replaceAll("#tviaje", " AND r.tipoviaje in ( " + tipo_viaje + " )") : sql.replaceAll("#tviaje", "");
            st = con.prepareStatement(sql);
            st.setString( 1, fecini+" 00:00" );
            st.setString( 2, fecfin+" 23:59" );
            st.setString( 3, cliente+"%" );
            rs = st.executeQuery();
            remesas = new Vector();
            while (rs.next()){
                rem = new Remesa();
                rem.setNumrem       ( (rs.getString("numrem")!=null)?rs.getString("numrem"):"" );//numero de remesa
                rem.setNombre_cli   ( (rs.getString("clientes")!=null)?rs.getString("clientes"):"" );//nombre del cliente
                rem.setOrirem       ( (rs.getString("origen")!=null)?rs.getString("origen"):"" );//origen remesa
                rem.setDesrem       ( (rs.getString("destino")!=null)?rs.getString("destino"):"" );//destino remesa
                rem.setNombreOrigen( (rs.getString("pais_origen")!=null)?rs.getString("pais_origen"):"" );//pais origen de la remesa
                rem.setAgcrem       ( (rs.getString("agencia")!=null)?rs.getString("agencia"):"" );//agencia due�a
                rem.setDocumento    ( (rs.getString("factura")!=null)?rs.getString("factura"):"" );//factura de la remesa
                rem.setTipoviaje    ( (rs.getString("tipoviaje")!=null)?rs.getString("tipoviaje"):"" );//tipo de viaje
                rem.setFeccum       ( (rs.getString("fecrem")!=null)?rs.getString("fecrem"):"" );//fecha de salida de la remesa
                rem.setFec_fron     ( (rs.getString("fec_lleg_fron_exp")!=null)?rs.getString("fec_lleg_fron_exp"):"" );//fecha llegada a frontera exportacion
                rem.setFec_sal_fron( (rs.getString("fec_sal_fron_exp")!=null)?rs.getString("fec_sal_fron_exp"):"" );//fecha de salida de la frontera exportacion
                rem.setFec_lle_imp  ( (rs.getString("fec_lleg_fron_imp")!=null)?rs.getString("fec_lleg_fron_imp"):"" );//fecha llegada a frontera importacion
                rem.setFec_sal_imp  ( (rs.getString("fec_sal_fron_imp")!=null)?rs.getString("fec_sal_fron_imp"):"" );//fecha de salida de la frontera importacion
                rem.setFec_fin      ( (rs.getString("fec_destino")!=null)?rs.getString("fec_destino"):"" );//fecha finalizacion viaje
                rem.setFec_ori_fron( (rs.getString("fec_ori_fron")!=null)?rs.getString("fec_ori_fron"):"" );//dias de origen a frontera
                rem.setFec_fron_des( (rs.getString("fec_fron_des")!=null)?rs.getString("fec_fron_des"):"" );//dias de frontera a destino
                rem.setTiem_viaje   ( (rs.getString("tiem_viaje")!=null)?rs.getString("tiem_viaje"):"" );//duracion del viaje
                rem.setTiem_fron    ( (rs.getString("tiem_fron")!=null)?rs.getString("tiem_fron"):"" );//duracion en frontera
                remesas.add(rem);
            }
        } catch( SQLException e ){
            throw new SQLException( "Error en obtenerTiemposRemesa [RemesaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_OBTENER_TIEMPOS_VIAJE");
        }
        return remesas;
    }
    
    /**
     *Metodo que buscar una remesa especifica en la base de datos.
     *@autor: Karen Reales
     *@param: Numero de la remesa (String)
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchRemesa(String remesa )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        PoolManager poolManager = null;
        rem= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_BUSCARREMESA);
                st.setString(1,remesa);
                //////System.out.println( " searchRemesa -> "+st.toString() );
                rs = st.executeQuery();
                if(rs.next()){
                    rem=Remesa.load(rs);
                    
                    rem.setPadre(rs.getString("ot_padre"));
                    rem.setPesoReal(rs.getFloat("pesoreal"));
                    rem.setCantreal(rs.getFloat("qty_packed"));
                    rem.setUnidad(rs.getString("unit_packed"));
                    rem.setRemitente(rs.getString("remitente"));
                    rem.setDestinatario(rs.getString("destinatario"));
                    rem.setTarifa(rs.getFloat("Qty_value"));
                    rem.setVlr_pesos(rs.getDouble("vlrrem2"));
                    rem.setCurrency(rs.getString("currency"));
                    rem.setCrossdocking(rs.getString("crossdocking"));
                    rem.setEstado(rs.getString("reg_status").equals("A")?"ANULADA":"ACTIVA");
                    rem.setMoneda(rs.getString("nommoneda"));
                    rem.setFec_anul(rs.getString("fec_anul").equals("0099-01-01")?"No anulada":rs.getString("fec_anul"));
                    rem.setBase(rs.getString("base"));
                    rem.setFactura(rs.getString("doc_fac"));
                    rem.setPagador(rs.getString("cod_pagador"));
                    rem.setFecha_contabilidad(rs.getString("fecha_contabilizacion"));
                    rem.setReg_status(rs.getString("reg_status"));
                    //REMITENTES
                    st = con.prepareStatement(this.SQL_BUSCARREMITENTES);
                    st.setString(1,rem.getNumrem());
                    rs2 = st.executeQuery();
                    String remitentes="";
                    while(rs2.next()){
                        remitentes =remitentes + rs2.getString("nombre")+"<br><br>";
                    }
                    //DESTINATARIOS
                    st = con.prepareStatement(SQL_BUSCARDESTINATARIOS);
                    st.setString(1,rem.getNumrem());
                    rs2 = st.executeQuery();
                    String destinatarios="";
                    while(rs2.next()){
                        destinatarios =destinatarios + rs2.getString("nombre")+"<br><br>";
                    }
                    rem.setNrem(remitentes);
                    rem.setNdest(destinatarios);
                    rem.setNfacturable(rs.getString("n_facturable"));
                    
                    //BUSCO LOS DOCUMENTOS RELACIONADOS
                    st = con.prepareStatement(SQL_BUSCARDOCUMENTOS);
                    st.setString(1,rem.getNumrem());
                    rs2 = st.executeQuery();
                    String docinterno = "";
                    String fcial = "";
                    while(rs2.next()){
                        if(rs2.getString("tipo_doc").equals("008")){
                            if(rs2.getString("documento_rel").equals(""))
                                docinterno  =docinterno+ rs2.getString("documento")+",";
                            else
                                docinterno  =docinterno+ rs2.getString("documento")+"/"+rs2.getString("documento_rel")+",";
                        }
                        else if(rs2.getString("tipo_doc").equals("009")){
                            fcial  =fcial+ rs2.getString("documento")+",";
                        }
                    }
                    rem.setFaccial(fcial);
                    rem.setDocInterno(docinterno);
                    
                }
                if( rem != null && rem.getPadre() != null &&  !rem.getPadre().equals("") ){
                    
                    st = con.prepareStatement(" SELECT description FROM remesa_padre WHERE code = ? ");
                    st.setString( 1, rem.getPadre());
                    rs = st.executeQuery();
                    
                    this.padre = null;
                    if( rs.next() ){
                        // Esta es la descripcion de la Remesa Padre asociada a esta remesa
                        this.padre = new Remesa();
                        padre.setDescripcion( rs.getString("description") );
                    }
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA REMESA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     *Metodo que busca la lista de remesas para el control de aduana
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void bucarRemesasAduana(String frontera, String cliente, String estado_tra, String faccia, String tipo) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null, rs2=null;
        
        remesas=null;
        try {
            //  logger.info(frontera+" -- "+cliente+" -- "+estado_tra+"-- "+faccia+" -- "+tipo);
            
            
            con = this.conectar("SQL_REMESAS_ADUANA");
            if(con!=null){
                String reporte[] = estado_tra.split("/");
                String trafico = reporte.length>0 ? reporte[0]:"";
                st = con.prepareStatement(this.obtenerSQL("SQL_REMESAS_ADUANA"));
                st.setString(1, frontera);
                st.setString(2, cliente+"%");
                st.setString(3, trafico+"%");
                st.setString(4, tipo+"%");
                // logger.info("CONSULTA "+st);
                System.out.println("*** SQL_LIST_ADUANA: "+st.toString());
                rs = st.executeQuery();
                remesas= new Vector();
                while(rs.next()){
                    boolean sw=true;
                    for (int i=0; i<remesas.size();i++){
                        Aduana a =(Aduana) remesas.elementAt(i);
                        if(rs.getString("numrem").equals(a.getNumrem())){
                            sw=false;
                        }
                    }
                    if(sw){
                        /*
                        st = con.prepareStatement(SQL_REMESADOCTO);
                        st.setString(1,rs.getString("numrem"));
                        //System.out.println("*** SQL_REMESADOCTO: "+st.toString());
                        rs2 = st.executeQuery();
                        String documentos =" ";
                        while(rs2.next()){
                            documentos = documentos + rs2.getString("documento")+" "+ rs2.getString("documento_rel");
                        }
                         */
                        //faccia = faccia.equals("")?" ":faccia;
                        //if(documentos.indexOf(faccia)>=0){
                        Aduana a = new Aduana();
                        
                        // MODIFICADO POR LREALES EL 20 DE ENERO DE 2007
                        String distrito = rs.getString( "distrito" )!=null?rs.getString( "distrito" ).toUpperCase():"";
                        String platlr = rs.getString( "platlr" )!=null?rs.getString( "platlr" ).toUpperCase():"";
                        String ult_observacion = rs.getString( "ult_observacion" )!=null?rs.getString( "ult_observacion" ):"";
                        String fec_ult_reporte = rs.getString( "fecha_ult_reporte" )!=null?rs.getString( "fecha_ult_reporte" ):"";
                        a.setDistrito( distrito );
                        a.setPlatlr( platlr.equals("NA") ? "" : platlr );
                        a.setUlt_observacion( ult_observacion );
                        a.setFecha_ult_reporte( fec_ult_reporte.equals("0099-01-01 00:00:00") ? "" : fec_ult_reporte );
                        //
                        
                        a.setEstado("");
                        a.setTipo_viaje( rs.getString("tipoviaje")!=null?rs.getString( "tipoviaje" ).toUpperCase():"" );
                        a.setCodcliente( rs.getString("cliente")!=null?rs.getString( "cliente" ).toUpperCase():"" );
                        a.setFrontera( rs.getString("FONTERA")!=null?rs.getString( "FONTERA" ).toUpperCase():"" );
                        a.setAgencia( rs.getString("agencia")!=null?rs.getString( "agencia" ).toUpperCase():"" );
                        a.setCliente( rs.getString("nomcliente")!=null?rs.getString( "nomcliente" ).toUpperCase():"" );
                        a.setNumrem( rs.getString("numrem")!=null?rs.getString( "numrem" ).toUpperCase():"" );
                        a.setOrirem( rs.getString("origen")!=null?rs.getString( "origen" ).toUpperCase():"" );
                        a.setDestrem( rs.getString("destino")!=null?rs.getString( "destino" ).toUpperCase():"" );
                        a.setNumpla( rs.getString("numpla")!=null?rs.getString( "numpla" ).toUpperCase():"" );
                        a.setPlaca( rs.getString("placa")!=null?rs.getString( "placa" ).toUpperCase():"" );
                        //a.setStandard(rs.getString("descripcion"));
                        a.setUlt_pto_control( rs.getString("nompto_control_ultreporte")!=null?rs.getString( "nompto_control_ultreporte" ).toUpperCase():"" );
                        //REMITENTES
                        st = con.prepareStatement(SQL_REMIDEST);
                        st.setString(1,a.getNumrem());
                        rs2 = st.executeQuery();
                        //System.out.println("*** SQL_REMIDEST: "+st.toString());
                        String remitentes="";
                        while(rs2.next()){
                            remitentes =remitentes + ( rs2.getString("nombre")!=null?rs2.getString( "nombre" ).toUpperCase():"" );
                        }
                        //DESTINATARIOS
                        st = con.prepareStatement(SQL_DESTINATARIOS);
                        st.setString(1,a.getNumrem());
                        //System.out.println("*** SQL_DESTINATARIOS: "+st.toString());
                        rs2 = st.executeQuery();
                        String destinatarios="";
                        String coddest="";
                        while(rs2.next()){
                            destinatarios =destinatarios + ( rs2.getString("nombre")!=null?rs2.getString( "nombre" ).toUpperCase():"" ) + ",";
                            coddest = rs2.getString("codigo")!=null?rs2.getString( "codigo" ).toUpperCase():"";
                        }
                        a.setCodigo( coddest );
                        a.setRemitente(remitentes);
                        a.setDestinatarios( destinatarios.length() > 0 ? destinatarios.substring( 0, destinatarios.length() - 1 ) : "" );
                        
                        // MODIFICADO POR LREALES EL 20 DE ENERO DE 2007
                        //FACTURA(S) COMERCIAL(ES)
                        //a.setFaccial(documentos);
                        //a.setFaccial( rs.getString("factcial") != null ? rs.getString( "factcial" ).toUpperCase() : "" );
                        //
                        st = con.prepareStatement(SQL_FACTURA_COMERCIAL);
                        st.setString(1,a.getDistrito());
                        st.setString(2,a.getNumrem());
                        st.setString(3,a.getCodcliente().substring( 3 , 6 )+"D");
                        //System.out.println("*** SQL_FACTURA_COMERCIAL: "+st.toString());
                        rs2 = st.executeQuery();
                        String factcial="";
                        while(rs2.next()){
                            factcial = rs2.getString("documento")!=null?rs2.getString( "documento" ).toUpperCase():"";
                        }
                        a.setFaccial( factcial );
                        //
                        
                        //PLANILLAS DE REEXPEDICION
                            /*
                            st = con.prepareStatement(SQL_REEXPEDICION);
                            st.setString(1,a.getNumrem());
                            //System.out.println("*** SQL_REEXPEDICION: "+st.toString());
                            rs2 = st.executeQuery();
                            a.setPlacareex("");
                            a.setNumplareex("");
                            if(rs2.next()){
                                a.setPlacareex( rs2.getString("plaveh")!=null?rs2.getString( "plaveh" ).toUpperCase():"" );
                                a.setNumplareex( rs2.getString("numpla")!=null?rs2.getString( "numpla" ).toUpperCase():"" );
                            }
                             */
                        // REEXPEDICION
                        st = con.prepareStatement( SQL_NUMPLA_PLACA_REEXP );
                        st.setString( 1, a.getNumrem() );
                        //System.out.println("*** SQL_NUMPLA_PLACA_REEXP: "+st.toString());
                        rs2 = st.executeQuery();
                        String reexpedicion = "";
                        String np = "";
                        String pv = "";
                        while ( rs2.next() ) {
                            np = rs2.getString("numpla")!=null?rs2.getString( "numpla" ).toUpperCase():"";
                            pv = rs2.getString("plaveh")!=null?rs2.getString( "plaveh" ).toUpperCase():"";
                            if ( !np.equals( a.getNumpla() ) ) {
                                reexpedicion = reexpedicion + np + "-" + pv + " ";
                            }
                        }
                        a.setReexpedicion( reexpedicion.length() > 0 ? reexpedicion.substring( 0, reexpedicion.length() - 1 ) : "" );
                        a.setNumplareex( np );
                        a.setPlacareex( pv );
                        
                        String via = rs.getString("via")!=null?rs.getString( "via" ).toUpperCase():"";
                        // logger.info("REMESA: "+a.getNumrem()+" - Via: "+via);
                        int index = via.indexOf( rs.getString("pto_control_proxreporte")!=null?rs.getString( "pto_control_proxreporte" ).toUpperCase():"" );
                        int indexFron = via.indexOf( rs.getString("FONTERA")!=null?rs.getString( "FONTERA" ).toUpperCase():"" );
                        
                        // logger.info("ULTIMO PTO: "+rs.getString("pto_control_proxreporte")+" - POSICION: "+index);
                        String via2 = via.substring(index+2,indexFron+2<index+2?index+2:indexFron+2);
                      /*  System.out.println("VIA :"+via);
                        System.out.println("Pto control :"+ rs.getString("pto_control_proxreporte"));
                        System.out.println("FONTERA :"+ rs.getString("FONTERA"));
                        System.out.println("Index 1 " +index);
                        System.out.println("Index 2 " +indexFron);
                        System.out.println("via "+via2);*/
                        // logger.info("VIA MODIFICADA: "+via2);
                        float duracion = 0;
                        for (int i=0; i < via2.length() -2 ; i += 2 ){
                            if(via2.length()>i+4){
                                String tOrigen  = via2.substring( i  , i+2 );
                                String tDestino = via2.substring( i+2, i+4 );
                                st = con.prepareStatement(SQL_TIEMPOTRAMO);
                                st.setString(1,  "FINV");
                                st.setString(2,  tOrigen);
                                st.setString(3,  tDestino);
                                //System.out.println("*** SQL_TIEMPOTRAMO: "+st.toString());
                                rs2 = st.executeQuery();
                                while (rs2.next()){
                                    duracion += rs2.getFloat(1);
                                    //logger.info("ORIGEN "+tOrigen+" DESTINO "+tDestino+" DURACION: "+ rs2.getDouble(1));
                                    break;
                                }
                            }
                            
                        }
                        System.out.println("duracion "+duracion);
                        
                        //VERIFICAR SI CUANDO LLEGA A FORNTERA COLOCA LA FECHA O DEJA LA ESTIMADA
                        if ( !fec_ult_reporte.equals("0099-01-01 00:00:00") &&  rs.getTimestamp( "fecha_ult_reporte" )!=null) {
                            java.sql.Timestamp fecha_ult_reporte = rs.getTimestamp( "fecha_ult_reporte" );
                            Calendar fec = Calendar.getInstance();
                            fec.setTimeInMillis(fecha_ult_reporte.getTime());
                            fec.add(fec.HOUR, (int)duracion);
                            java.util.Date fecha = fec.getTime();
                            java.text.SimpleDateFormat s =  new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
                            a.setFecposllegadafron(s.format(fecha));
                        } else {
                            a.setFecposllegadafron("");
                        }
                        
                        //ACTIVIDADES
                        String nom_act = "";
                        String fec_act = "";
                        String obs_act = "";
                        st = con.prepareStatement( SQL_ACTIVIDAD_ADUANA );
                        st.setString( 1, a.getDistrito() );
                        st.setString( 2, a.getCodcliente() );
                        st.setString( 3, a.getNumrem() );
                        //System.out.println("*** SQL_ACTIVIDAD_ADUANA: " + st.toString());
                        rs2 = st.executeQuery();
                        if ( rs2.next() ) {
                            nom_act = rs2.getString("nom_act") != null ? rs2.getString( "nom_act" ).toUpperCase() : "";
                            fec_act = rs2.getString("fec_act") != null ? rs2.getString( "fec_act" ) : "";
                            obs_act = rs2.getString("obs_act") != null ? rs2.getString( "obs_act" ) : "";
                        }
                        a.setUlt_act_aduana( nom_act );
                        a.setFec_act_aduana( fec_act );
                        a.setObs_act_aduana( obs_act );
                        //a.setUlt_act_aduana( rs.getString("ultact")!=null?rs.getString( "ultact" ):"" );
                        
                        //TIPO DE REEXPEDICION
                        String tipo_reexp = "";
                        st = con.prepareStatement( SQL_AUXILIAR_REMESA );
                        st.setString( 1, a.getDistrito() );
                        st.setString( 2, a.getNumrem() );
                        st.setString( 3, "TIPO_REEXP" );
                        //System.out.println("*** TIPO DE REEXPEDICION: " + st.toString());
                        rs2 = st.executeQuery();
                        if ( rs2.next() ) {
                            tipo_reexp = rs2.getString("informacion") != null ? rs2.getString( "informacion" ) : "";
                        }
                        a.setTipo_reexp( tipo_reexp );
                        //AGENTE ADUANERO DE EXPORTACION
                        String ageexp = "";
                        st = con.prepareStatement( SQL_AUXILIAR_REMESA );
                        st.setString( 1, a.getDistrito() );
                        st.setString( 2, a.getNumrem() );
                        st.setString( 3, "AGEEXP" );
                        //System.out.println("*** AGENTE ADUANERO DE EXPORTACION: " + st.toString());
                        rs2 = st.executeQuery();
                        if ( rs2.next() ) {
                            ageexp = rs2.getString("informacion") != null ? rs2.getString( "informacion" ) : "";
                        }
                        a.setAgeexp( ageexp );
                        //AGENTE ADUANERO DE IMPORTACION
                        String ageimp = "";
                        st = con.prepareStatement( SQL_AUXILIAR_REMESA );
                        st.setString( 1, a.getDistrito() );
                        st.setString( 2, a.getNumrem() );
                        st.setString( 3, "AGEIMP" );
                        //System.out.println("*** AGENTE ADUANERO DE IMPORTACION: " + st.toString());
                        rs2 = st.executeQuery();
                        if ( rs2.next() ) {
                            ageimp = rs2.getString("informacion") != null ? rs2.getString( "informacion" ) : "";
                        }
                        a.setAgeimp( ageimp );
                        //ALMACENADORA
                        String almacenadora = "";
                        st = con.prepareStatement( SQL_AUXILIAR_REMESA );
                        st.setString( 1, a.getDistrito() );
                        st.setString( 2, a.getNumrem() );
                        st.setString( 3, "ALMACENADO" );
                        //System.out.println("*** ALMACENADORA: " + st.toString());
                        rs2 = st.executeQuery();
                        if ( rs2.next() ) {
                            almacenadora = rs2.getString("informacion") != null ? rs2.getString( "informacion" ) : "";
                        }
                        a.setAlmacenadora( almacenadora );
                        
                        remesas.add(a);
                        //}
                    }
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS DE ADUANAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                this.desconectar("SQL_REMESAS_ADUANA");
            }
        }
    }
    
    private int nextID() throws SQLException {
        PreparedStatement ps   = null;
        ResultSet         rs   = null;
        int               next = -1;
        String query = "SQL_NEXT_ID";
        try{
            Connection con = this.conectar("SQL_NEXT_ID");
            if (con == null)
                throw new SQLException("SIN CONEXION.");
            
            
            ps = con.prepareStatement( this.obtenerSQL(query));
            rs = ps.executeQuery();
            if (rs.next()){
                next = rs.getInt("last_value");
            }
            
            return next;
            
            
        } catch (SQLException ex){
            ex.printStackTrace();
            throw new SQLException(ex.getMessage());
        } finally{
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar("SQL_NEXT_ID");
        }
    }
    
    
    public String anularMovrem(String numrem, Usuario usuario) throws SQLException {
        
        PreparedStatement st = null;
        String result = "";
        ResultSet rs = null;
        try {
            Connection con = this.conectar("SQL_INSERT_MOVREM");
            if (con == null)
                throw new SQLException("SIN CONEXION.");
            
            
            
            st = con.prepareStatement( this.obtenerSQL("SQL_LISTA_MOVREM" ) );
            st.setString(1  , numrem);
            rs = st.executeQuery();
            while(rs.next()){
                int id = nextID();
                st = con.prepareStatement( this.obtenerSQL("SQL_INSERT_MOVREM" ) );
                st.setInt(1  , id);
                st.setString(2  , ""                  );
                st.setString(3  , rs.getString("dstrct"));
                st.setString(4  , rs.getString("numrem"));
                st.setString(5  , rs.getString("concepto"));
                // valor ajuste
                st.setDouble(6  , rs.getDouble("vlrrem_aj")*-1);
                st.setDouble(7  ,rs.getDouble("vlrrem2_aj")*-1);
                st.setDouble(8  ,rs.getDouble("pesoreal_aj")*-1);
                st.setDouble(9  , rs.getDouble("qty_value_aj")*-1 );
                st.setDouble(10 , rs.getDouble("tasa_aj")*-1 );
                // valores que provocan el ajuste
                st.setDouble(11  , rs.getDouble("vlrrem")*-1);
                st.setDouble(12 ,rs.getDouble("vlrrem2")*-1);
                st.setDouble(13 ,rs.getDouble("pesoreal")*-1);
                st.setDouble(14 , rs.getDouble("qty_value")*-1);
                st.setDouble(15 , rs.getDouble("tasa") *-1);
                // otros datos
                st.setString(16, rs.getString("agencia") );
                st.setString(17,"now()" );
                st.setString(18, usuario.getLogin());
                st.setString(19, usuario.getBase());
                result =result+ st.toString();
            }
            
        }catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( e.getMessage() );
        }finally{
            if ( st != null ) st.close();
            this.desconectar("SQL_INSERT_MOVREM");
        }
        return result;
    }
    
    
     /**
     *Metodo para actualizar los datos de una remesa en la base de datos.
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String updateRem()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_ACTUALIZAR);
              //  st.setFloat(1,rem.getPesoReal());
                st.setString(1,rem.getDocInterno());
                st.setString(2, rem.getDestinatario());
                st.setString(3, rem.getRemitente());
                st.setString(4, rem.getUnidcam());
                st.setString(5, rem.getObservacion());
                st.setString(6, rem.getFaccial());
                st.setString(7, rem.getStdJobNo());
                st.setString(8, rem.getDescripcion());
                st.setString(9, rem.getNfacturable());
                st.setFloat(10, rem.getCantreal());
                st.setString(11, rem.getCrossdocking());
                st.setString(12, rem.getUnidad());
                st.setString(13, rem.getCadena());
                st.setString(14, rem.getPagador());
                st.setString(15, rem.getNumrem());
                //st.executeUpdate();
                sql = st.toString();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LAS REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
}