/*
 * MovPlaDAO.java
 *
 * Created on 26 de noviembre de 2004, 02:19 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.CadenaConLongitud;
import org.apache.log4j.Logger;
import com.tsp.operation.model.DAOS.MainDAO;

/**
 *
 * @author  KREALES
 */
public class MovPlaDAO extends MainDAO {
    
    static Logger logger = Logger.getLogger(MovPlaDAO.class);
    private Movpla movpla;
    private Vector lista;
    /** Creates a new instance of MovPlaDAO */
    //ALEJO
    private static final String SQL_SELECT = "select * from movpla ";
    //private static final String SQL_ACTUALIZAR_MOVPLA = "update movpla set last_update = 'now()', fecha_migracion = 'now()' where creation_date = ? and concept_code = ? and planilla = ? and pla_owner = ? and document = ?";
    private static final String SQL_ACTUALIZAR_MOVPLA = "update movpla set last_update = 'now()', fecha_migracion = 'now()' where planilla = ? and document = ? and fecha_migracion = '0099-01-01' ";
    private final String SQL_OBTENER_VALOR = "select SUM(vlr) from movpla where dstrct=? and agency_id=? and document_type='001' and concept_code='01' and planilla=?";
    
    private static final String SQL_MIGRAR_MOVPLA = "update movpla set last_update = 'now()', fecha_migracion = '1900-01-01' where planilla = ?  and fecha_migracion = '0099-01-01' and creation_date=?";
    // MARIO 28-03-2006 08:01 PM
    
    
    String SQL_ACCOUNT =
    " SELECT coalesce(b.account_code_i,'') as ac,             " +
    "        a.numrem, a.porcent, a.std_job_no                " +
    " FROM                                                    " +
    " ( SELECT  r.numrem , pr.porcent, r.std_job_no           " +
    "   FROM    plarem pr, remesa r                           " +
    "   WHERE   pr.numpla = ? AND r.numrem = pr.numrem        " +
    " ) a LEFT JOIN stdjob b ON (b.std_job_no = a.std_job_no) " ;
    
    
    String SQL_CURRENCY_BANCO =
    " SELECT b.currency as cur FROM   banco b            " +
    " WHERE  b.branch_code = ? and b.bank_account_no = ? ";
    
    
    String SQL_ICA =
    " select dato from tablagen where table_type = 'ICA' and table_code = ? ";
    
    //kreales 01-03-2006
    private static String SQL_SEARCH_CHEQUE_PLANILLA =
    "select 	m.agency_id, " +
    "			m.branch_code," +
    "			m.bank_account_no," +
    "			m.currency," +
    "			m.creation_user," +
    "			m.document," +
    "			m.dstrct," +
    "			sum(vlr) as vlr,	" +
    "			get_nombrenit(p.cedcon) as nombre," +
    "                       m.pla_owner," +
    "                       m.supplier" +
    "               from 	movpla m" +
    "                	inner join planilla p on (p.numpla =m.planilla)" +
    "		where   m.document =?" +
    "                	and m.branch_code = ? " +
    "			and m.bank_account_no =?" +
    "                       and m.planilla = ? " +
    "			and m.reanticipo = 'N'" +
    "			and m.reg_status = '' " +
    "		group by m.agency_id," +
    "			m.branch_code," +
    "			m.bank_account_no," +
    "			m.currency," +
    "			m.creation_user," +
    "			m.document," +
    "			m.dstrct," +
    "                       m.pla_owner," +
    "                       m.supplier," +
    "			nombre;";
    /*Karen 14-03-2006*/
    private static final String SEARCH_MOV_PLA = "Select * from  movpla  where concept_code=? and planilla = ? and reg_status = ''";
    private static final String SQL_BUSQ_ANTICIPO =
    "select 	m.*" +
    " from 	movpla m" +
    " where m.concept_code='01'  " +
    "       and m.planilla =?" +
    "	and m.reg_status = '' " +
    "	AND M.CREATION_DATE = (SELECT MAX(CREATION_DATE)FROM movpla  WHERE concept_code='01' and planilla =? )";
    private String SQL_BUSCAR_VLRCONCEPT ="Select  planilla," +
    "                sum(vlr_disc) as vlr_disc," +
    "                sum(vlr) as vlr," +
    "                sum(vlr_for) as vlr_for," +
    "                currency" +
    "                from  movpla" +
    "                where concept_code=?" +
    "                and planilla = ?" +
    "                and reg_status = ''" +
    "                group by" +
    "                planilla," +
    "                currency" ;
    
    private String SQL_BUSCAR_ANTICIPO =
    "select 	m.*," +
    "                           a.svlr" +
    "          from 	movpla m," +
    "                       (" +
    "                       select 	sum(m.vlr) as svlr " +
    "                       from 	movpla m " +
    "                       where m.concept_code='01'   " +
    "                           and m.planilla =? " +
    "                           and m.reg_status = ''  ) as a" +
    "          where m.concept_code='01'   " +
    "                       and m.planilla =? " +
    "                	and m.reg_status = ''  " +
    "                	AND M.CREATION_DATE = (SELECT MAX(CREATION_DATE)FROM movpla  WHERE concept_code='01' and planilla =?)";
    
    private String SQL_REANTICIPO = "Select * from  movpla  where concept_code=? and planilla = ? and reg_status = '' and reanticipo = 'Y' ";
    private String SQL_SEARCHMOVPLA ="Select * from  movpla  where concept_code=? and planilla = ? and reg_status = '' and reanticipo = 'N'";
    private String SQL_REANTICIPO_CHEQUE = "Select * from  movpla  where  planilla = ? and document = ? and reg_status = '' and reanticipo = 'Y' ";
    private String SQL_BUSCAR_CHEQUE_IMPRESO ="select 	m.*," +
    "	p.numpla," +
    "	n.nombre" +
    " from 	movpla m," +
    "	planilla p," +
    "	nit n" +
    " where 	m.document =?" +
    "       and m.branch_code = ? " +
    "       and m.bank_account_no = ?"+
    "	and m.reg_status = ''" +
    "       and m.user_anul=''" +
    "       and m.vlr>0         "+
    "	and p.numpla=m.planilla " +
    "	and p.cedcon = n.cedula";
    
    
    private String SQL_ANULAR_CHEQUE_IMPRESO ="update movpla set  fech_anul='now()', user_anul=?  where document=? and branch_code=? and bank_account_no=?";
    
    private String SQL_ANULAR_CHEQUE_IMPRESO1 ="update egreso set  reg_status='A'  where document_no=? and branch_code=? and bank_account_no=?";
    private String SQL_ANULAR_CHEQUE_IMPRESO2 ="insert into movpla (dstrct,agency_id,document_type,concept_code,pla_owner,planilla,supplier,application_ind,vlr,currency,vlr_disc,proveedor_anticipo,creation_user,date_doc,applicated_ind,ind_vlr,vlr_for,sucursal,base,branch_code,bank_account_no,document, item, creation_date,reanticipo) "+
    " (select dstrct,agency_id,document_type,'01',pla_owner,planilla,supplier,application_ind,vlr*-1,currency,vlr_disc*-1,proveedor_anticipo, ? ,date_doc,applicated_ind,ind_vlr,vlr_for,sucursal,base,branch_code,bank_account_no,document,item, ?, reanticipo from movpla where document = ? and branch_code=? and bank_account_no=? )" ;
    private String SQL_ANULAR_EGRESO ="insert into anulacion_egreso(dstrct,branch_code,bank_account_no, document_no,tipo_documento,usuario_anulo,causa,observacion,tipo_recuperacion)values(?,?,?,?,?,?,?,?,?)";
    
    public MovPlaDAO() {
        super("MovplaDAO.xml");
    }
    public void setPlanilla(Movpla movpla){
        this.movpla = movpla;
    }
    
    public Movpla getMovPla(){
        return  movpla;
    }
    public void setMovPla(Movpla mp){
        movpla = mp;
    }
    
    /**
     * Metodo para extraer los daros de migracion de cheques
     * @autor mfontalvo
     * @return Vector.
     * @throws SQLException.
     */
    public Vector obtenerMovimientosNoMigrados() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector datos = new Vector();
        try {
                st = this.crearPreparedStatement("SQL_MIGRACION_CHEQUES");
                logger.info(  "query: " + st );
                rs = st.executeQuery();
                logger.info(  "query ejecutado" );
                while ( rs.next() ) {
                    Movpla mp = new Movpla();
                    mp.setDstrct(rs.getString("dstrct"));
                    mp.setAgency_id(rs.getString("agency_id"));
                    mp.setDocument(rs.getString("document"));
                    mp.setBranch_code(rs.getString("branch_code"));
                    mp.setBank_account_no(rs.getString("bank_account_no"));
                    mp.setPlanilla(rs.getString("planilla"));
                    mp.setVlr(rs.getFloat("vlr"));
                    datos.addElement(mp);
                    logger.info( "planilla = "+mp.getPlanilla());
                }
                return datos;
            
        } catch ( SQLException e ) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) st.close();
            this.desconectar("SQL_MIGRACION_CHEQUES");
            return datos;
        }
    }  
    
    public String insertAutReanticipo()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql ="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into autorizacion_reanticipo (dstrct,  planilla,    creation_user ) values(?,?,?)");
                st.setString(1,movpla.getDstrct());
                st.setString(2,movpla.getPlanilla());
                st.setString(3,movpla.getCreation_user());
                //st.executeUpdate();
                sql = st.toString();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS AUTORIZACION DE REANTICIPO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    public String insertMovPlaChequeNull(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql ="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into movpla (dstrct,agency_id,document_type,concept_code,vlr,currency,creation_user,branch_code,bank_account_no,document,fech_anul,user_anul,base,creation_date) values(?,?,?,?,?,?,?,?,?,?,'now()',?,?,?)");
                st.setString(1,movpla.getDstrct());
                st.setString(2,movpla.getAgency_id());
                st.setString(3,movpla.getDocument_type());
                st.setString(4,movpla.getConcept_code());
                st.setFloat(5, movpla.getVlr());
                st.setString(6,movpla.getCurrency());
                st.setString(7,movpla.getCreation_user());
                st.setString(8, movpla.getBanco());
                st.setString(9, movpla.getCuenta());
                st.setString(10, movpla.getDocument());
                st.setString(11, movpla.getCreation_user());
                st.setString(12,base);
                st.setString(13, movpla.getCreation_date());
                //st.executeUpdate();
                sql = st.toString();
                // st.clearParameters();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LOS MOVIMIENTO DE PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
     /**
     * Metodo que retorna un comando para actualizar los anticipos de una
     * planilla.
     * @autor Karen Reales
     * @throws SQLException.
     */
    public String updateMovPla()throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql="";
        try {
            st = this.crearPreparedStatement("SQL_UPDATE_ANTICIPOS");
            st.setString(1,movpla.getPla_owner());
            st.setString(2,movpla.getSupplier());
            st.setFloat(3, movpla.getVlr());
            st.setFloat(4, movpla.getVlr());
            st.setString(5,movpla.getProveedor());
            st.setString(6, movpla.getSucursal());
            st.setString(7, movpla.getBanco());
            st.setString(8, movpla.getCuenta());
            st.setString(9, movpla.getReg_status());
            st.setFloat(10, movpla.getCantidad());
            st.setString(11, movpla.getBeneficiario());
            st.setString(12,movpla.getPlanilla());
            st.setString(13, movpla.getConcept_code());
            st.setString(14, movpla.getCreation_date());
            sql = st.toString();
            logger.info( "-----UPDATE: "+sql);
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACION DE LOS MOVIMIENTO DE PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_UPDATE_ANTICIPOS");
            
        }
        return sql;
    }
    
    public String deleteMovPla()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("delete from movpla where planilla=? and concept_code=?");
                st.setString(1,movpla.getPlanilla());
                st.setString(2, movpla.getConcept_code());
                
                sql = st.toString();
                logger.info( "-----DELETE: "+sql);
            }
        }catch(SQLException e){
            throw new SQLException("ERROR MIENTRAS SE BORRAN LOS MOVIMIENTO DE PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    /**
     *Metodo que retorna un string para anular cheques y genera un nuevo registro
     *con el valor de cheque para ser impreso posteriormente
     *@autor: Karen Reales
     *@return: String con comandos SQL
     *@throws: En caso de que un error de base de datos ocurra.
     */
    
    public String anularCheque()throws SQLException{
        
        PreparedStatement st   = null;
        PreparedStatement st1  = null;
        PreparedStatement st2  = null;
        ResultSet         rs   = null;
        String            sql  = "";
        
        
        String           query1 = "SQL_ANULAR_CHEQUE_IMPRESO";
        String           query2 = "SQL_ANULAR_CHEQUE_IMPRESO1";
        String           query3 = "SQL_ANULAR_EGRESO";
        String           query4 = "SQL_LISTA_REEMPLAZO";
        String           query5 = "SQL_ANULAR_CHEQUE_IMPRESO2";
        String           query6 = "SQL_GENERACION_REEMPLAZO";
        
        try {
            
            String banco [] = movpla.getBanco().split("/");
            String branch="";
            String account="";
            if(banco.length>0)
                branch = banco[0];
            if(banco.length>1)
                account = banco[1];
            
            
            
            try{
                st = this.crearPreparedStatement(query1);
                st.setString(1, movpla.getCreation_user());
                st.setString(2,movpla.getDocument());
                st.setString(3, branch);
                st.setString(4, account);
                sql = st.toString();
            }finally{
                this.desconectar(query1);
            }
            
            
            
            try{
                st = this.crearPreparedStatement(query2);
                st.setString(1,movpla.getDocument());
                st.setString(2, branch);
                st.setString(3, account);
                sql = sql +";"+st.toString();
            }finally{
                this.desconectar(query2);
            }
            
            
            
            try{
                st = this.crearPreparedStatement(query3);
                st.setString(1, movpla.getDstrct());
                st.setString(2, branch);
                st.setString(3, account);
                st.setString(4, movpla.getDocument());
                st.setString(5, "004");
                st.setString(6, movpla.getCreation_user());
                st.setString(7, movpla.getCausa());
                st.setString(8, movpla.getObservacion());
                st.setString(9, movpla.getTipo_rec());
                sql = sql +";" + st.toString();
            }finally{
                this.desconectar(query3);
            }
            
            
            
            st = this.crearPreparedStatement(query4);
            st.setString(1,movpla.getDocument());
            st.setString(2, branch);
            st.setString(3, account);
            rs = st.executeQuery();
            int minuto =112000;
            
            st1 = this.crearPreparedStatement( query5 );
            st2 = this.crearPreparedStatement( query6 );
            
            while(rs.next()){
                minuto=minuto+10000;
                
                st1.setString(1, movpla.getCreation_user());
                st1.setString(2, ""+new java.sql.Timestamp(System.currentTimeMillis()+minuto));
                st1.setString(3, rs.getString("concept_code"));
                st1.setString(4, rs.getString("planilla"));
                st1.setString(5, rs.getString("pla_owner"));
                st1.setString(6, rs.getString("creation_date"));
                sql =sql + ";"+ st1.toString();
                st1.clearParameters();
                
                minuto=minuto+10000;
                
                st2.setString(1, movpla.getCreation_user());
                st2.setString(2, ""+new java.sql.Timestamp(System.currentTimeMillis()+minuto));
                st2.setString(3, rs.getString("concept_code"));
                st2.setString(4, rs.getString("planilla"));
                st2.setString(5, rs.getString("pla_owner"));
                st2.setString(6, rs.getString("creation_date"));
                sql = sql + ";" + st2.toString();
                
                st2.clearParameters();
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE CHEQUES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (st1 != null) st1.close();
            if (st2 != null) st2.close();
            
            this.desconectar(query4);
            this.desconectar(query5);
            this.desconectar(query6);
            
        }
        return sql;
        
    }
    
    
     /**
     *Metodo que busca el anticipo aplicado a una planolla
     *@autor: Karen Reales
     *@param: Numero de la planilla
     **@param: Codigo del concepto
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchMovPla(String numpla, String concep_code )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        
        // lista = new Vector();
        try {
            
            st = this.crearPreparedStatement("SQL_SEARCHMOVPLA");
            st.setString(1,concep_code);
            st.setString(2,numpla);
            
            rs = st.executeQuery();
            if(rs.next()){
                movpla=Movpla.load(rs);
                if(!rs.getString("document").equals(""))
                    movpla.setImpresa(true);
                
                movpla.setBanco(rs.getString("branch_code")+"/"+rs.getString("bank_account_no"));
                movpla.setBeneficiario(rs.getString("beneficiario"));
                movpla.setVlr_for(rs.getFloat("vlr_for"));
                //lista.add(movpla);
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SIN IMPRIMIR  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_SEARCHMOVPLA");
        }
        
    }
        public void searchMovPla(String numpla, String concep_code, String creation_date )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select * from  movpla  where planilla = ? and concept_code=? and reg_status = '' and creation_date = ?");
                st.setString(1,numpla);
                st.setString(2,concep_code);
                st.setString(3,creation_date);
                rs = st.executeQuery();
                if(rs.next()){
                    movpla=Movpla.load(rs);
                    if(!rs.getString("document").equals(""))
                        movpla.setImpresa(true);
                    
                    movpla.setBanco(rs.getString("branch_code")+"/"+rs.getString("bank_account_no"));
                    movpla.setBeneficiario(rs.getString("beneficiario"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SIN IMPRIMIR  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    /**
     *Metodo que busca el anticipo de una planilla si se encuentran
     *varios registos, se agrupan y se suma el valor
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: SQLException  En caso de que un error de base de datos ocurra.
     */
    public void buscarAnticipo(String numpla)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_BUSCAR_ANTICIPO);
                st.setString(1,numpla);
                st.setString(2,numpla);
                st.setString(3,numpla);
                rs = st.executeQuery();
                
                if(rs.next()){
                    movpla=Movpla.load(rs);
                    if(!rs.getString("document").equals(""))
                        movpla.setImpresa(true);
                    movpla.setVlr(rs.getFloat("svlr"));
                    movpla.setCreation_date(rs.getString("CREATION_DATE"));
                    movpla.setBanco(rs.getString("branch_code")+"/"+rs.getString("bank_account_no"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SIN IMPRIMIR  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
      /**
     * Metodo que busca una lista de anticipos y reanticipos relacionados a una
     * planilla.
     * @autor Karen Reales
     * @throws SQLException.
     */
    public void buscarAnticipos(String numpla)throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        lista = new Vector();
        try {
            
            st = this.crearPreparedStatement("SQL_LISTA_ANTICIPOS");
            st.setString(1,numpla);
            rs = st.executeQuery();
            
            while(rs.next()){
                movpla = new Movpla();
                movpla.setDocument(rs.getString("document"));
                movpla.setCreation_date(rs.getString("CREATION_DATE"));
                movpla.setVlr(rs.getFloat("vlr"));
                movpla.setReanticipo(rs.getString("reanticipo"));
                if(!rs.getString("document").equals(""))
                    movpla.setImpresa(true);
                movpla.setBanco(rs.getString("branch_code")+"/"+rs.getString("bank_account_no"));
                movpla.setBeneficiario(rs.getString("beneficiario"));
                lista.add(movpla);
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS ANTICIPOS DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_LISTA_ANTICIPOS");
            
        }
        
    }
   
    
    
    public float totalAjuste(String numpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        float total = 0;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select sum(vlr_disc)as total from  movpla  where planilla = ? and concept_code='09' and reg_status = ''");
                st.setString(1,numpla);
                rs = st.executeQuery();
                if(rs.next()){
                    total = rs.getFloat("total");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TOTAL DE AJUSTE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return total;
        
    }
    
    public void listCheques()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select 	a.* " +
                " from  	movpla a" +
                " where 	a.document not in (select document_no from egreso)" +
                "               and a.document !=''");
                rs = st.executeQuery();
                if(rs.next()){
                    movpla=Movpla.load(rs);
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SIN IMPRIMIR  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
  
    
    public boolean existenCheques(String cheque1, String cheque2, String banco, String sucursal )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        boolean sw= false;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from movpla where document between ? AND ? AND branch_code = ? AND bank_account_no = ?");
                st.setString(1,cheque1);
                st.setString(2,cheque2);
                st.setString(3,banco);
                st.setString(4,sucursal);
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CHEQUES  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    /***/
    public boolean existeUltCheque(String cheque)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        boolean sw= false;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select max(document)as ultimo from movpla where document LIKE ?");
                st.setString(1,cheque+"%");
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CHEQUES  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    public boolean existeSerie(String base,String banco, String cuenta,int ultnum)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        boolean sw= false;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT 	" +
                "       S.prefix," +
                "	s.last_number," +
                "	m.document" +
                " from 	series  s," +
                "	movpla m" +
                " where    S.document_type = '004' " +
                "      and s.branch_code =?" +
                "      and s.bank_account_no =?" +
                "      and m.document = s.prefix||s.last_number-1" +
                "      and s.last_number= ?");
                st.setString(1,banco);
                st.setString(2,cuenta);
                st.setInt(3, ultnum);
                logger.info( "Sql " +st.toString());
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SERIE DE LOS CHEQUES  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    
     /**
     *Metodo que anula en movpla, y en egreso el cheque impreso relacionado a una planilla
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String anularChequeImpreso()throws SQLException{
        
        PreparedStatement st   = null;
        PreparedStatement st2  = null;
        ResultSet         rs   = null;
        String            sql  ="";
        float val;
        
        String         query1  = "SQL_ANULAR_CHEQUE_IMPRESO";
        String         query2  = "SQL_ANULAR_CHEQUE_IMPRESO1";
        String         query3  = "SQL_ANULAR_EGRESO";
        String         query4  = "SQL_LISTA_REEMPLAZO";
        String         query5  = "SQL_ANULAR_CHEQUE_IMPRESO2";
        
        
        try {
            
            String banco [] = movpla.getBanco().split("/");
            String branch="";
            String account="";
            if(banco.length>0)
                branch = banco[0];
            if(banco.length>1)
                account = banco[1];
            
            
            
            try{
                st = this.crearPreparedStatement(query1);
                st.setString(1, movpla.getCreation_user());
                st.setString(2,movpla.getDocument());
                st.setString(3, branch);
                st.setString(4, account);
                sql = st.toString();
            }finally{
                this.desconectar( query1 );
            }
            
            
            
            try{
                st = this.crearPreparedStatement(query2);
                st.setString(1,movpla.getDocument());
                st.setString(2, branch);
                st.setString(3, account);
                sql = sql +";"+st.toString();
            }finally{
                this.desconectar( query2 );
            }
            
            
            
            try{
                st = this.crearPreparedStatement(query3);
                st.setString(1, movpla.getDstrct());
                st.setString(2, branch);
                st.setString(3, account);
                st.setString(4, movpla.getDocument());
                st.setString(5, "004");
                st.setString(6, movpla.getCreation_user());
                st.setString(7, movpla.getCausa());
                st.setString(8, movpla.getObservacion());
                st.setString(9, movpla.getTipo_rec());
                sql = sql +";"+st.toString();
            }finally{
                this.desconectar( query3 );
            }
            
            
            
            st = this.crearPreparedStatement(query4);
            st.setString(1,movpla.getDocument());
            st.setString(2, branch);
            st.setString(3, account);
            rs = st.executeQuery();
            int minuto =112000;
            
            st2 = this.crearPreparedStatement( query5 );
            
            while(rs.next()){
                minuto=minuto+10000;
                
                st2.setString(1, movpla.getCreation_user());
                st2.setString(2, ""+new java.sql.Timestamp(System.currentTimeMillis()+minuto));
                st2.setString(3, rs.getString("concept_code"));
                st2.setString(4, rs.getString("planilla"));
                st2.setString(5, rs.getString("pla_owner"));
                st2.setString(6, rs.getString("creation_date"));
                sql =sql +";"+ st2.toString();
                
                st2.clearParameters();
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE CHEQUES IMPRESO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar(query4);
            this.desconectar(query5);
        }
        return sql;
    }
    
    
  
      /**
     *Metodo que anula un anticipo de una planilla
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void anularChequeNoImpreso()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql="";
        float val;
        try {
            con = this.conectar("SQL_ANULAR_ANTICIPO");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_ANULAR_ANTICIPO"));
                st.setString(1, movpla.getCreation_user());
                st.setString(2,movpla.getPlanilla());
                st.setString(3, movpla.getCreation_date());
                st.executeUpdate();
                
                st = con.prepareStatement(this.obtenerSQL("SQL_ANULAR_ANTICIPO"));
                st.setString(1,movpla.getCreation_user());
                st.setString(2,movpla.getPlanilla());
                st.setString(3, movpla.getCreation_date());
                logger.info( st);
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE CHEQUES NO IMPRESO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                this.desconectar("SQL_ANULAR_ANTICIPO");
            }
        }
        
    }
    /*Modificado por JME 28.11.05*/
    public boolean ChequeAnulado(String cheque, String banco, String sucursal )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        boolean est = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from movpla where document = ? and user_anul<> '' and vlr>0 and branch_code = ? and bank_account_no = ? ");
                st.setString(1,cheque);
                st.setString(2,banco);
                st.setString(3,sucursal);
                rs = st.executeQuery();
                if(rs.next()){
                    est = true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN CHEQUE ANULADO  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return est;
    }/***/
    public boolean AnticipoAnulado(String planilla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        boolean est = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from movpla where planilla = ? and concept_code = '07' ");
                st.setString(1,planilla);
                rs = st.executeQuery();
                if(rs.next()){
                    est = true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN ANTICIPO ANULADO  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return est;
    }
    
   
    public float obtenerValor(String distrito, String idAgencia, String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                ps = con.prepareStatement( SQL_OBTENER_VALOR );
                ps.setString( 1, distrito );
                ps.setString( 2, idAgencia );
                ps.setString( 3, numpla );
                logger.info( "query: "+ps);
                rs = ps.executeQuery();
                if (rs.next()){
                    logger.info( "Valor devuelto por consulta: "+ps+" => "+rs.getFloat(1));
                    return rs.getFloat(1);
                }
            }
            return -1;
        }
        catch(Exception e){
            e.printStackTrace();
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( ps != null ) {
                try {
                    ps.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
        
    }
    
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.Vector getLista() {
        return lista;
    }
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.Vector lista) {
        this.lista = lista;
    }
    
   
   
    
    
    public String obtenerFechaActualEnFormato(String patron) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                ps = con.prepareStatement( "select TO_CHAR(now(),?) as hoy" );
                ps.setString( 1, patron );
                logger.info( "query: "+ps);
                rs = ps.executeQuery();
                if (rs.next()){
                    return rs.getString("hoy");
                }
            }
            return null;
        }
        catch(Exception e){
            e.printStackTrace();
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( ps != null ) {
                try {
                    ps.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
    }
    
    public void actualizarMovimientosMigrados(Vector movimientos) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                ps = con.prepareStatement( SQL_ACTUALIZAR_MOVPLA );
                for( int i=0; i<movimientos.size(); i++ ){
                    Hashtable fila = (Hashtable) movimientos.elementAt(i);
                    ps.setString(1,fila.get("creation_date").toString());
                    ps.setString(2,fila.get("concept_code").toString());
                    ps.setString(3,fila.get("planilla").toString());
                    ps.setString(4,fila.get("pla_owner").toString());
                    ps.setString(5,fila.get("document").toString());
                    logger.info( "query: "+ps);
                    ps.executeUpdate();
                    ps.clearParameters();
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " /*+ e.getErrorCode()*/ );
        }
        finally {
            if ( ps != null ) {
                try {
                    ps.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
    }
    
   
    
    /**
     *Metodo que busca los cheques generados de una planilla
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void guardarCheque(String numpla) throws  SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                st = con.prepareStatement( "select * from movpla  where fecha_migracion = ? AND document <> '' and " +
                " (  ( BRANCH_CODE !='' AND BANK_ACCOUNT_NO !='') OR ( concept_code = '01' and reanticipo = 'Y')) and " +
                "planilla = ? " +
                " order by agency_id, document ");
                st.setString(1, "0099-01-01");
                st.setString(2,numpla);
                logger.info(  "query: " + st );
                rs = st.executeQuery();
                logger.info(  "query ejecutado" );
                while ( rs.next() ) {
                    Movpla mp = new Movpla();
                    mp.setAgency_id(rs.getString("agency_id"));
                    mp.setApplicated_ind(rs.getString("applicated_ind"));
                    mp.setApplication_ind(rs.getString("application_ind"));
                    mp.setBank_account_no(rs.getString("bank_account_no"));
                    mp.setBank_account_no_remplazo(rs.getString("bank_account_no_remplazo"));
                    mp.setBase(rs.getString("base"));
                    mp.setBranch_code(rs.getString("branch_code"));
                    mp.setBranch_code_remplazo(rs.getString("branch_code_remplazo"));
                    mp.setCh_remplazo(rs.getString("ch_remplazo"));
                    mp.setConcept_code(rs.getString("concept_code"));
                    mp.setCorte(rs.getString("corte"));
                    mp.setCurrency(rs.getString("currency"));
                    mp.setDate_doc(rs.getString("date_doc"));
                    mp.setDocument(rs.getString("document"));
                    mp.setDocument_type(rs.getString("document_type"));
                    mp.setDstrct(rs.getString("dstrct"));
                    mp.setFecha_cheque(rs.getString("fecha_cheque"));
                    mp.setFecha_migracion(rs.getString("fecha_migracion"));
                    mp.setFech_anul(rs.getString("fech_anul"));
                    mp.setInd_vlr(rs.getString("ind_vlr"));
                    mp.setItem(rs.getString("item"));
                    mp.setLast_update(rs.getString("last_update"));
                    mp.setPlanilla(rs.getString("planilla"));
                    mp.setPla_owner(rs.getString("pla_owner"));
                    mp.setProveedor_anticipo(rs.getString("proveedor_anticipo"));
                    mp.setReg_status(rs.getString("reg_status"));
                    mp.setSucursal(rs.getString("sucursal"));
                    mp.setSupplier(rs.getString("supplier"));
                    mp.setVlr(rs.getFloat("vlr"));
                    mp.setVlr_disc(rs.getFloat("Vlr_disc"));
                    mp.setCreation_date(rs.getString("creation_date"));
                    mp.setReanticipo(rs.getString("reanticipo"));
                    lista.add(mp);
                }
            }
            
        }
        catch ( SQLException e ) {
            e.printStackTrace();
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
    }
    /**
     *Metodo que inicializa el vector lista
     *@autor: Karen Reales
     */
    public void iniciarLista(){
        lista = new Vector();
    }
    
      /**
     *Metodo que busca el cheque de una planilla especificar
     *@autor: Karen Reales
     *@param: Numero del cheque, Codigo de banco, Codigo agencia, Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchChequePlanilla(String cheque, String banco, String sucursal,String numpla )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        
        
        try {
            
            st = this.crearPreparedStatement("SQL_SEARCHCHEQUE");
            st.setString(1,cheque);
            st.setString(2,banco);
            st.setString(3,sucursal);
            st.setString(4,numpla);
            rs = st.executeQuery();
            if(rs.next()){
                movpla = new Movpla();
                movpla.setAgency_id(rs.getString("agency_id"));
                movpla.setBanco(rs.getString("branch_code")+"/"+rs.getString("bank_account_no"));
                movpla.setVlr(rs.getFloat("vlr"));
                movpla.setCurrency(rs.getString("currency"));
                movpla.setPla_owner(rs.getString("pla_owner"));
                movpla.setCreation_user(rs.getString("creation_user"));
                movpla.setDocument(rs.getString("document"));
                movpla.setDstrct(rs.getString("dstrct"));
                movpla.setSupplier(rs.getString("supplier"));
                movpla.setProveedor_anticipo("");
                movpla.setSucursal("");
                movpla.setProveedor("");
                movpla.setBeneficiario(rs.getString("beneficiario"));
                
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN CHEQUE SIN IMPRIMIR  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_SEARCHCHEQUE");
        }
        
    }
    /**
     *Metodo que busca un movpla dado un concepto y una planilla, si se encuentran
     *varios registos, se agrupan y se suma el valor
     *@autor: Karen Reales
     *@param: Numero de la planilla
     **@param: Codigo del concepto
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarValorConcepto(String numpla, String concep_code )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_BUSCAR_VLRCONCEPT);
                st.setString(1,concep_code);
                st.setString(2,numpla);
                
                rs = st.executeQuery();
                if(rs.next()){
                    movpla= new Movpla();
                    movpla.setPlanilla(rs.getString("planilla"));
                    movpla.setVlr(rs.getFloat("vlr"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL CONCEPTO EN MOVPLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     *Metodo que busca el valor o la suma de valores de reanticipos generados a una planilla
     *@autor: Karen Reales
     *@param: Numero de la planilla
     **@param: Codigo del concepto
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchReanticipos(String numpla, String concep_code )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        lista = new Vector();
        try {
            
            st =this.crearPreparedStatement("SQL_REANTICIPO");
            st.setString(1,concep_code);
            st.setString(2,numpla);
            
            rs = st.executeQuery();
            float vlr = 0;
            while(rs.next()){
                movpla=Movpla.load(rs);
                if(!rs.getString("document").equals(""))
                    movpla.setImpresa(true);
                
                movpla.setBanco(rs.getString("branch_code")+"/"+rs.getString("bank_account_no"));
                movpla.setBeneficiario(rs.getString("beneficiario"));
                movpla.setVlr_for(rs.getFloat("vlr_for"));
                lista.add(movpla);
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REANTICIPOS DE LA PLANILLA  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_REANTICIPO");
        }

    }
    /**
     *Metodo que busca un cheque imporeso de reanticpo
     *@autor: Karen Reales
     *@param: Numero de la planilla
     **@param: Codigo del concepto
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchChequesReanticipos(String numpla, String numcheque )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_REANTICIPO_CHEQUE);
                st.setString(1,numpla);
                st.setString(2,numcheque);
                
                rs = st.executeQuery();
                float vlr = 0;
                while(rs.next()){
                    movpla=Movpla.load(rs);
                    if(!rs.getString("document").equals(""))
                        movpla.setImpresa(true);
                    vlr =vlr+ rs.getFloat("vlr");
                    movpla.setBanco(rs.getString("branch_code")+"/"+rs.getString("bank_account_no"));
                    movpla.setVlr(vlr);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS REANTICIPOS DE LA PLANILLA  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
      /**
     *Metodo que marca en fecha de migracion los registros
     *@autor: Karen Reales
     *@param: Movpla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void migrarMovpla(String planilla, String creacion) throws SQLException {
        
        Connection con = null;
        PoolManager poolManager = null;
        PreparedStatement ps = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                ps = con.prepareStatement(SQL_MIGRAR_MOVPLA);
                ps.setString(1,planilla);
                ps.setString(2,creacion);
                logger.info( "Update movpla: "+ps);
                ps.execute();
                
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            if ( con != null ){
                con.close();
            }
        }
        finally {
            if ( ps != null ) {
                try {
                    ps.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
    }
    
    /**
     *Metodo que genera una  lista de MOVPLA dado un numero de cheque
     *@autor: Karen Reales
     *@param: String numero de cheque
     *@param: String Banco
     *@param: String Agencia del banco
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listaPlanillaCheque(String no_cheque, String branch, String account)throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql="";
        try {
            lista = new Vector();
            st = this.crearPreparedStatement("SQL_LISTA_REEMPLAZO");
            st.setString(1,no_cheque);
            st.setString(2, branch);
            st.setString(3, account);
            rs = st.executeQuery();
            while(rs.next()){
                Movpla m = new Movpla();
                m.setPlanilla(rs.getString("planilla"));
                m.setReanticipo(rs.getString("reanticipo"));
                m.setVlr(rs.getFloat("vlr"));
                m.setSupplier(rs.getString("supplier"));
                m.setProveedor_anticipo("");
                m.setPla_owner(rs.getString("pla_owner"));
                m.setSucursal("");
                lista.add(m);
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LISTA DE PLANILLAS DE UN CHEQUE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_LISTA_REEMPLAZO");
            
        }
        
        
    }
    ////////////////////////////////////////////////////////////////////////////
    // MIGRACION DE REGISTROS CONTABLES                                       //
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Metodo para extraer los daros de migracion de cheques
     * @autor mfontalvo
     * @param fechaProceso fecha y hora para restringir los datos de migracion
     * @return Vector.
     * @throws SQLException.
     */
    public Vector obtenerMovimientosNoMigrados( String fechaProceso) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector datos = new Vector();
        try {
                st = this.crearPreparedStatement("SQL_MIGRACION_CHEQUES");
                st.setString(1 , fechaProceso );
                st.setString(2 , fechaProceso );
                st.setString(3 , fechaProceso );
                st.setString(4 , fechaProceso );
                logger.info(  "query: " + st );
                rs = st.executeQuery();
                logger.info(  "query ejecutado" );
                while ( rs.next() ) {
                    Movpla mp = new Movpla();
                    mp.setDstrct(rs.getString("dstrct"));
                    mp.setAgency_id(rs.getString("agency_id"));
                    mp.setDocument(rs.getString("document"));
                    mp.setBranch_code(rs.getString("branch_code"));
                    mp.setBank_account_no(rs.getString("bank_account_no"));
                    mp.setPlanilla(rs.getString("planilla"));
                    mp.setVlr     (rs.getFloat("vlr"));
                    mp.setVlr_for (rs.getFloat("vlr_for"));
                    mp.setCurrency(rs.getString("currency") );
                    mp.setCausa( rs.getString("extra") );
                    datos.addElement(mp);
                    logger.info( "planilla = "+mp.getPlanilla());
                }
                return datos;
            
        } catch ( SQLException e ) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) st.close();
            this.desconectar("SQL_MIGRACION_CHEQUES");
            return datos;
        }
    }
    
    
   

    /**
     *Funcion que retorna el banco dado un distrito y un codigo de proveedor en MIMS
     *@autor: Karen Reales
     *@param: distrito es el distrito de la planilla que tiene el ajuste.
     *@param: proveedor codigo de proveedor del propietario due�o de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String buscarBancoProveedor(String distrito, String placa) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String banco ="";
        try {
            ps = this.crearPreparedStatement("SQL_BANCO");
            ps.setString(1,placa);
            ps.setString(2,distrito);
            rs = ps.executeQuery();
            if(rs.next()){
                banco = rs.getString(1)+"/"+rs.getString(2);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR BUSCANDO EL BANCO DEL PROVEEDOR" +e.getMessage());
        }
        finally {
            if ( ps != null ) ps.close();
            this.desconectar("SQL_BANCO");
        }
        return banco;
    }
    
    
     /**
     * Metodo para actualizar los movimientos de planilla 
     * @autor mfontalvo
     * @param mp, objeto movpla migrado
     * @param fechProceso fecha de Proceso de migracion
     * @throws Exception.
     */    
    public void actualizarMovplaMigrado(Movpla mp, String fechaProceso) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = this.crearPreparedStatement("SQL_ACTUALIZAR_MOVPLA");
            String [] extras = mp.getCausa().split("%");
            if (extras!=null){
                for (int i=0;i<extras.length;i++){
                    String []dt = extras[i].split(",");
                    ps.setString(1, dt[0] );
                    ps.setString(2, mp.getPlanilla());
                    ps.setString(3, dt[1] );
                    ps.setString(4, dt[2] );
                    logger.info( "Update movpla: "+ps);
                    ps.executeUpdate();
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        finally {
            if ( ps != null ) ps.close();
            this.desconectar("SQL_ACTUALIZAR_MOVPLA");
        }
    }

    /**
     * Metodo para formatear el account 
     * @autor mfontalvo
     * @param ac account_code
     * @return String account_code ya seteado
     */
    private String formatearAC(String ac){
        StringBuffer str = new StringBuffer(ac);
        if (ac!=null && !ac.trim().equals("")){
            str.setCharAt(0, 'C' );
            str.setCharAt(9, '9' );
        }
        return str.toString();
    }
   
    
     /**
     *Metodo que retorna el nit del propietario o el nit del beneficiario encontrado
     *en la tabla proveedor segun el propietario dado.
     *@autor: Karen Reales
     *@param: Tipo (C,T o P) y placa.
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String buscarBeneficiarioCheque(String tipo, String placa )throws SQLException{

        PreparedStatement st = null;
        ResultSet rs = null;
        String cedula="";
        try {

            st =  this.crearPreparedStatement("SQL_BENEFICIARIO");
            st.setString(1, placa);
            rs = st.executeQuery();
            if(rs.next()){
               if(tipo.equals("P")){
                  cedula = rs.getString("nit");

               }
               else if (tipo.equals("T")){
                   cedula = rs.getString("nit_beneficiario");

               }

            }


        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN CHEQUE SIN IMPRIMIR  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_BENEFICIARIO");
        }
        return cedula;
    }
    
      /**
     *Metodo que lista los datos del Movpla de una planilla
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listaAllMovPla(String numpla)throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        
        lista = new Vector();
        try {
            com.tsp.operation.model.services.TasaService tasa=  new com.tsp.operation.model.services.TasaService();
            st = this.crearPreparedStatement("SQL_LISTA_ALLMOVPLA");
            st.setString(1,numpla);
            
            rs = st.executeQuery();
            while(rs.next()){
                Movpla movpla = new Movpla();
                movpla.setDocument(rs.getString("document"));
                movpla.setConcept_code(rs.getString("concept_desc"));
                movpla.setApplication_ind(rs.getString("application_ind"));
                movpla.setInd_vlr(rs.getString("ind_vlr"));
                movpla.setVlr_disc(rs.getFloat("vlr"));
                movpla.setVlr((float)com.tsp.util.Util.redondear(rs.getDouble("valor_neto"),2));
                movpla.setCurrency(rs.getString("currency"));
                movpla.setCreation_date(rs.getString("creation_date"));
                movpla.setCreation_user(rs.getString("nombre"));
                movpla.setProveedor(rs.getString("proveedor"));
                movpla.setBranch_code(rs.getString("branch_code"));
                movpla.setBank_account_no(rs.getString("bank_account_no"));
                movpla.setFecha_migracion(rs.getString("fecha_migracion").equals("0099-01-01") || rs.getString("fecha_migracion").equals("1900-01-01")? "NO MIGRADO": rs.getString("fecha_migracion"));
                movpla.setFecha_cheque(rs.getString("fecha_contabilizacion").equals("0099-01-01 00:00:00")? "NO CONTABILIZADO": rs.getString("fecha_migracion"));
                movpla.setVlr_for(rs.getFloat("vlr_for"));
                movpla.setSigno(rs.getInt("ind_signo"));
                if(rs.getString("ind_vlr").equals("P")){
                    try{
                        tasa.buscarValorTasa("PES",rs.getString("currency"),"PES", rs.getString("fecha"));
                        if(tasa.obtenerTasa()!=null){
                            
                            double vlrTasa = tasa.obtenerTasa().getValor_tasa();
                            //System.out.println("Valor tasa "+vlrTasa);
                            movpla.setVlr_disc((float) (rs.getFloat("vlr")*vlrTasa));
                        }
                        else{
                            //System.out.println("No encontre la tasa...");
                        }
                    }catch(Exception e ){
                        e.printStackTrace();
                    }
                }
                
                
                lista.add(movpla);
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE TODOS LOS MOVPLA  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_LISTA_ALLMOVPLA");
        }
        
    }
    /**
     *Metodo para buscar un anticipo dado a una planilla y anularlo.
     *@autor: Karen Reales
     *@parametro: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void BuscarChequeNoImpreso( String numpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        try {
            
            con = this.conectar("SQL_BUSCAR_ANULA_ANTIC");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_BUSCAR_ANULA_ANTIC"));
                st.setString(1,numpla);
                // st.setString(2,numpla);
                logger.info( ""+st.toString());
                
                rs = st.executeQuery();
                if(rs.next()){
                    movpla=Movpla.load(rs);
                    movpla.setPla_owner(rs.getString("nombre"));
                    movpla.setCreation_date(rs.getString("CREATION_DATE"));
                    movpla.setBanco(rs.getString("branch_code")+ "("+rs.getString("bank_account_no")+")" );
                    movpla.setVlr_for(rs.getFloat("vlr_for"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS SIN IMPRIMIR  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                this.desconectar("SQL_BUSCAR_ANULA_ANTIC");
            }
        }
        
    }
     /*MOdificado Por kreales 25.10.06*/
    public void searchCheque(String cheque, String banco, String sucursal )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        PoolManager poolManager = null;
        
        try {
            
            st =  this.crearPreparedStatement("SQL_SEARCH_CHEQUE");
            st.setString(1,cheque);
            st.setString(2,banco);
            st.setString(3,sucursal);
            rs = st.executeQuery();
            if(rs.next()){
                movpla = new Movpla();
                movpla.setAgency_id(rs.getString("agency_id"));
                movpla.setBanco(rs.getString("branch_code")+"/"+rs.getString("bank_account_no"));
                movpla.setVlr(rs.getFloat("vlr"));
                movpla.setCurrency(rs.getString("currency"));
                movpla.setPla_owner(rs.getString("nombre"));
                movpla.setDocument(rs.getString("document"));
                movpla.setDstrct(rs.getString("dstrct"));
                movpla.setCreation_user(rs.getString("creation_user"));
                movpla.setVlr_for(rs.getFloat("vlr_for"));
                
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN CHEQUE DADO EL BANCO Y  LA SUCURSAL  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_SEARCH_CHEQUE");
        }
        
    }
     /**
     *Metodo que busca un cheque impreso de una planilla
     *@autor: Karen Reales
     *@param: Numero del cheque
     *@param: Banco
     *@param: Agencia del banco
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void BuscarChequeImpreso(String cheque, String banco, String sucursal )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        
        try {
            
            st = this.crearPreparedStatement("SQL_BUSCAR_CHEQUE_IMPRESO") ;
            st.setString(1,cheque);
            st.setString(2,banco);
            st.setString(3,sucursal);
            //System.out.println("St: "+st.toString());
            rs = st.executeQuery();
            float valor=0;
            while(rs.next()){
                movpla=Movpla.load(rs);
                valor = valor +rs.getFloat("vlr");
                movpla.setVlr(valor);
                movpla.setPla_owner(rs.getString("nombre"));
                movpla.setBanco(rs.getString("branch_code")+ "/"+rs.getString("bank_account_no") );
                movpla.setReanticipo(rs.getString("reanticipo"));
                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+100000));
                movpla.setVlr_for(rs.getFloat("vlr_for"));
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN CHEQUE YA IMPRESO  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_BUSCAR_CHEQUE_IMPRESO");
        }
        
    }
    /**
     *Metodo que busca la lista de cheques impreso de una planilla
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    
    public void buscarCheques(String numpla)throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        movpla = null;
        
        try {
            
            st = this.crearPreparedStatement("SQL_buscarCheques");
            st.setString(1,numpla);
            rs = st.executeQuery();
            lista = new Vector();
            while(rs.next()){
                movpla = new Movpla();
                movpla.setDocument(rs.getString("document"));
                //movpla.setCreation_date(rs.getString("CREATION_DATE"));
                movpla.setVlr(rs.getFloat("vlr"));
                movpla.setVlr_for(rs.getFloat("vlr_for"));
                if(!rs.getString("document").equals(""))
                    movpla.setImpresa(true);
                movpla.setBanco(rs.getString("branch_code")+"/"+rs.getString("bank_account_no"));
                lista.add(movpla);
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS ANTICIPOS DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_buscarCheques");
            
        }
        
    }
    
    
        /**
     * Procedimiento para obtener los datos que se usaran para la
     * migracion hacia los archivos ms266
     * @autor apayares
     * @modificado mfontalvo 20060317
     * @param fechaProceso fecha para restingiir los datos de migracion.
     * @return Vector, este alamacenara la informacion a importar
     * @throws Exception.
     * @see buscarBancoProveedor(String, String ).
     */
    public Vector obtenerDatosInterfaceContable(String fechaProceso) throws SQLException {
        PreparedStatement ps          = null;
        PreparedStatement psACC       = null;
        Vector            datos       = new Vector();
        ResultSet         rsAUX       = null;
        ResultSet         rs          = null;
        com.tsp.operation.model.services.TasaService       
          TasaSvc  = new com.tsp.operation.model.services.TasaService();
        
        
        try {
            
            ps    = crearPreparedStatement("SQL_DATOS_CONTABLE");
            psACC = crearPreparedStatement("SQL_ACCOUNT");
            ps.setString(1 , fechaProceso );
            ps.setString(2 , fechaProceso );
            rs    = ps.executeQuery();
            while ( rs.next() ){
                String    dbanco = buscarBancoProveedor(rs.getString("dstrct"), rs.getString("plaveh"));
                Hashtable fila  = new Hashtable();
                
                if(!dbanco.equals("")){
                    String bancos[]  = dbanco.split("/");
                    String banco     = dbanco.length()>0?bancos[0]:"";
                    String sucursal  = dbanco.length()>1?bancos[1]:"";
                    
                    // Definicion del signo del valor
                    int    signo    = rs.getInt("ind_signo");
                    double vlr      = rs.getDouble("vlr_for") * signo;
                    String currency = rs.getString("currency");
                    
                    // cambio de moneda si la moneda en WEB es PES y la moneda en mims es BOL
                    String currency_cf_code = this.getMonedaMasCF( rs.getString("cf_code") );
                    if (currency_cf_code.equals("BOL") && currency.equals("PES") ) {
                        
                        com.tsp.operation.model.beans.Tasa obj =  TasaSvc.buscarValorTasa
                        ("PES", currency, currency_cf_code, rs.getString("fecmov") );
                        if (obj!=null){
                            vlr = com.tsp.util.Util.roundByDecimal((vlr * obj.getValor_tasa()),0);
                            currency = currency_cf_code;
                        }
                    }
                    
                    
                    // Determinacion del impuesto ICA
                    int ICA = 0;
                    if ( rs.getString("ind_application").equalsIgnoreCase("V") &&
                    !rs.getString("codica").equals("")){
                        ICA = (int) Math.round( -vlr * rs.getDouble("ica") / 1000 );
                    }
                    
                    
                    // datos de control
                    fila.put("creation_date", rs.getString("cd"));
                    fila.put("concept_code" , rs.getString("cc"));
                    fila.put("planilla"     , rs.getString("pla"));
                    fila.put("pla_owner"    , rs.getString("po"));
                    fila.put("document"     , rs.getString("doc"));
                    
                    
                    // datos del archivo cabecera
                    fila.put(new Integer(1), new CadenaConLongitud(banco));
                    fila.put(new Integer(2), new CadenaConLongitud(sucursal));
                    fila.put(new Integer(3), new CadenaConLongitud(rs.getString("sietes")));
                    fila.put(new Integer(4), new CadenaConLongitud(rs.getString("plaveh")));
                    fila.put(new Integer(5), new CadenaConLongitud("W" + rs.getString("cc") + " " + rs.getString("pla") + " " + com.tsp.util.Util.DecimalToBase(rs.getString("fecfac"),36)  ));
                    fila.put(new Integer(6), new CadenaConLongitud(rs.getString("hoy")));
                    fila.put(new Integer(7), new CadenaConLongitud(""));
                    fila.put(new Integer(8), new CadenaConLongitud(String.valueOf(vlr)));
                    fila.put(new Integer(9), new CadenaConLongitud(currency ));
                    fila.put(new Integer(10), new CadenaConLongitud(""+(ICA)));
                    
                    // datos del item segun el ACCOUNT
                    int complet = 11;
                    if ("|01|15|".indexOf( rs.getString("cc") )!=-1 ){
                        fila.put(new Integer(11), new CadenaConLongitud(rs.getString("descripcion") + " " +  rs.getString("pla")));
                        fila.put(new Integer(12), new CadenaConLongitud(""+(vlr)));
                        fila.put(new Integer(13), new CadenaConLongitud(rs.getString("n")  ));
                        fila.put(new Integer(14), new CadenaConLongitud(rs.getString("sietes")));
                        fila.put(new Integer(15), new CadenaConLongitud(rs.getString("pla")));
                        fila.put(new Integer(16), new CadenaConLongitud(rs.getString("codica") ));
                        fila.put(new Integer(17), new CadenaConLongitud("22010101"));
                        complet = 18;
                    }else {
                        
                        // busqueda de los account de los estadares
                        psACC.clearParameters();
                        psACC.setString(1, rs.getString("pla") );
                        rsAUX = psACC.executeQuery();
                        
                        // SE LLENAN LOS ITEMS PARA DISTRIBUIR LOS COSTOS
                        while (rsAUX.next()){
                            int porcent = rsAUX.getInt( "porcent" );
                            fila.put(new Integer(complet++), new CadenaConLongitud(rs.getString("descripcion") + " " +  rs.getString("pla")));
                            fila.put(new Integer(complet++), new CadenaConLongitud(""+(vlr * (porcent/100) )));
                            fila.put(new Integer(complet++), new CadenaConLongitud(rs.getString("n")  ));
                            if (rsAUX.getRow()==1)
                                fila.put(new Integer(complet++), new CadenaConLongitud(rs.getString("sietes")));
                            fila.put(new Integer(complet++), new CadenaConLongitud(rs.getString("pla")));
                            fila.put(new Integer(complet++), new CadenaConLongitud(rs.getString("codica")));
                            fila.put(new Integer(complet++), new CadenaConLongitud(formatearAC(rsAUX.getString("ac"))));
                            fila.put(new Integer(complet++), new CadenaConLongitud(rsAUX.getString("numrem")));
                        }
                        rsAUX.close();
                    }
                    for( int j=complet; j <= 32; fila.put( new Integer(""+j), new CadenaConLongitud("")) , j++);
                    fila.put(new Integer(33), new CadenaConLongitud("P"));
                    datos.addElement(fila);
                }
            }

            
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally {
            if ( rs    !=null  ) rs.close();
            if ( rsAUX !=null  ) rsAUX.close();            
            if ( psACC !=null  ) psACC.close();
            if ( ps    != null ) ps.close();
            this.desconectar("SQL_DATOS_CONTABLE");
            this.desconectar("SQL_ACCOUNT");
            return datos;
        }
    }
    
    /**
     * Metodo para buscar la moneda de la +cf en mims
     * @autor mfontalvo
     * @param cf_code, codigo +cf
     * @return String, moneda de la planilla
     */
    public String getMonedaMasCF (String cf_code) throws Exception{
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        String            query  = "SQL_MONEDA_MAS_CF"; 
        String            moneda = "";
        try{
            st = this.crearPreparedStatement(query);
            st.setString(1, cf_code );
            rs = st.executeQuery();
            if (rs.next()){
                moneda = rs.getString(1);
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        finally {
            if (rs!=null) rs.close();
            if (st!=null) st.close();
            this.desconectar(query);
        }
        return moneda;
    }
   // Julio Barros 23-11-2006
    public String insertExtracto(Extracto ex)throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql ="";
        try {
            st = this.crearPreparedStatement("SQL_INSERTEXTRACTO");
            st.setString(1,ex.getReg_status());
            st.setString(2,ex.getDstrct());
            st.setString(3,ex.getNit());
            st.setString(4,ex.getFecha());
            st.setFloat(5,ex.getVlr_pp());
            st.setFloat(6,ex.getVlr_ppa());
            st.setString(7,ex.getCurrency());
            st.setString(8,ex.getBanco());
            st.setString(9,ex.getSucursal());
            st.setString(10,ex.getCreation_user()); 
            st.setString(11,ex.getCreation_date()); 
            st.setString(12,ex.getUser_update()); 
            st.setString(13,ex.getLast_update()); 
            st.setString(14,ex.getBase());
            st.setInt   (15,ex.getSecuencia());
            sql = st.toString();
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LOS MOVIMIENTO DE PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_INSERTEXTRACTO");
        }
        return sql;
    }
  
      /**
     *Metodo que inserta los movimientos de valores relacionados a la
     *planilla en la tabla movpla
     *@autor: Karen Reales
     *@param: base
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String insertMovPla(String base)throws Exception{
        String SQL_INSERTMOVPLA = this.obtenerSQL("SQL_INSERTMOVPLA");
        StringStatement st = new StringStatement(SQL_INSERTMOVPLA,true);
        try {
            st.setString(1,movpla.getDstrct());
            st.setString(2,movpla.getAgency_id());
            st.setString(3,movpla.getDocument_type());
            st.setString(4,movpla.getConcept_code());
            st.setString(5,movpla.getPla_owner());
            st.setString(6,movpla.getPlanilla());
            st.setString(7,movpla.getSupplier());
            st.setString(8, movpla.getAp_ind());
            st.setDouble(9, movpla.getVlr());
            st.setString(10,movpla.getCurrency());
            st.setDouble(11, movpla.getVlr());
            st.setString(12, movpla.getProveedor());
            st.setString(13,movpla.getCreation_user());
            st.setString(14,movpla.getInd_vlr());
            st.setDouble(15, movpla.getVlr_for());
            st.setString(16, movpla.getCreation_date());
            st.setString(17,movpla.getSucursal());
            st.setString(18, base);
            st.setString(19, movpla.getBanco());
            st.setString(20, movpla.getCuenta());
            st.setDouble(21, movpla.getCantidad());
            st.setString(22, movpla.getReanticipo());
            st.setString(23, movpla.getFecha_migracion());
            st.setString(24, movpla.getBeneficiario());
            st.setString(25, movpla.getTercero());
            return st.getSql()+";";     
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("ERROR DURANTE LA INSERCCION DE LOS MOVIMIENTO DE PLANILLAS " + e.getMessage());
        }
        finally{
            if(st != null) st =null;
        }
    }
    
      /**
     *Metodo que busca los anticipos entregados a una planilla que no
     *hallan sido impresos.
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: SQLException  En caso de que un error de base de datos ocurra.
     */
    public void buscarAnticipoSinCheque(String numpla, String nitcia)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            con = this.conectar("SQL_ANTICIPONOCHEQUE");
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL("SQL_ANTICIPONOCHEQUE"));
                st.setString(1,numpla);
                st.setString(2,nitcia);
                rs = st.executeQuery();
                lista =new Vector();
                while(rs.next()){
                    Movpla movpla = new Movpla();
                    movpla.setVlr(rs.getFloat("svlr"));
                    movpla.setCurrency(rs.getString("currency"));
                    movpla.setProveedor_anticipo(rs.getString("provee"));
                    lista.add(movpla);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS ANTICIPOS SIN CHEQUE DE UN PROVEEDOR " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                this.desconectar("SQL_ANTICIPONOCHEQUE");
            }
        }
        
    }
    
     /**
     *Metodo que inserta los movimientos de valores relacionados a la con el banco y la sucursal
     *planilla en la tabla movpla
     *@autor: Karen Reales
     *@param: base
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String insertMovPla2(String base)throws SQLException{
        //System.out.println("inset en movpla  -->  seguimiento ");
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql ="";
        try {
            st = this.crearPreparedStatement("SQL_INSERTMOVPLA");
            st.setString(1,movpla.getDstrct());
            st.setString(2,movpla.getAgency_id());
            st.setString(3,movpla.getDocument_type());
            st.setString(4,movpla.getConcept_code());
            st.setString(5,movpla.getPla_owner());
            st.setString(6,movpla.getPlanilla());
            st.setString(7,movpla.getSupplier());
            st.setString(8, movpla.getAp_ind());
            st.setFloat(9, movpla.getVlr());
            st.setString(10,movpla.getCurrency());
            st.setFloat(11, movpla.getVlr());
            st.setString(12, movpla.getProveedor());
            st.setString(13,movpla.getCreation_user());
            st.setString(14,movpla.getInd_vlr());
            st.setFloat(15, movpla.getVlr_for());
            st.setString(16, movpla.getCreation_date());
            st.setString(17,movpla.getSucursal());
            st.setString(18, base);
            
            st.setString(19, movpla.getBranch_code());
            st.setString(20, movpla.getBank_account_no());
            
            st.setFloat(21, movpla.getCantidad());
            st.setString(22, movpla.getReanticipo());
            st.setString(23, movpla.getFecha_migracion());
            st.setString(24, movpla.getBeneficiario());
            st.setString(25, movpla.getTercero());
            //st.executeUpdate();
            sql = st.toString();
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LOS MOVIMIENTO DE PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_INSERTMOVPLA");
        }
        return sql;
    }
    
    // Julio Barros 23-11-2006
     // Modificado Julio Barros 22/01/2007
      public String insertExtractoDetalle(ExtractoDetalle ex)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql ="";
        try {
            st = this.crearPreparedStatement("SQL_INSERTEXTRACTODETALLE");
            st.setString(1,ex.getReg_status());
            st.setString(2,ex.getDstrct());
            st.setString(3,ex.getNit());
            st.setString(4,ex.getFecha());
            st.setString(5,ex.getTipo_documento());
            st.setString(6,ex.getDocumento()); 
            st.setString(7,ex.getConcepto()); 
            st.setString(8,ex.getDescripcion()); 
            st.setString(9,ex.getFactura()); 
            st.setFloat (10,ex.getVlr());
            st.setFloat (11,ex.getRetefuente());
            st.setFloat (12,ex.getReteica());
            st.setFloat (13,ex.getImpuestos());
            st.setFloat (14,ex.getVlr_pp_item());
            st.setFloat (15,ex.getVlr_ppa_item());
            st.setString(16,ex.getCreation_user());
            st.setString(17,ex.getCreation_date());
            st.setString(18,ex.getUser_update());
            st.setString(19,ex.getLast_update());
            st.setString(20,ex.getBase());
            st.setInt   (21,ex.getSecuencia());
            st.setString(22,ex.getPlaca());
            sql = st.toString();
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LOS MOVIMIENTO DE PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_INSERTEXTRACTODETALLE");
        }
        return sql;
    }


/**
     * Procedimiento para obtener los datos que se usaran para la
     * migracion hacia los archivos ms266
     * @autor apayares
     * @modificado mfontalvo 20060317
     * @modificado idevia 20061214
     * @return Vector, este alamacenara la informacion a importar
     * @throws Exception.
     * @see buscarBancoProveedor(String, String ).
     */
    public Vector obtenerDatosInterfaceContable() throws SQLException {
        PreparedStatement ps          = null;
        PreparedStatement psCUR       = null;
        PreparedStatement psACC       = null;
        Vector            datos       = new Vector();
        ResultSet         rsAUX       = null;
        ResultSet         rs          = null;
        
        
        com.tsp.operation.model.services.TasaService
        TasaSvc  = new com.tsp.operation.model.services.TasaService();

        try {
            
            ps    = crearPreparedStatement("SQL_DATOS_CONTABLE");
            psCUR = crearPreparedStatement("SQL_CURRENCY_BANCO");
            psACC = crearPreparedStatement("SQL_ACCOUNT");
            rs    = ps.executeQuery();
            while ( rs.next() ){
                String    dbanco = buscarBancoProveedor(rs.getString("dstrct"), rs.getString("plaveh"));
                Hashtable fila  = new Hashtable();
                
                if(!dbanco.equals("")){
                    String bancos[]  = dbanco.split("/");
                    String banco     = dbanco.length()>0?bancos[0]:"";
                    String sucursal  = dbanco.length()>1?bancos[1]:"";
                    
                    // Definicion del signo del valor
                    int    signo = rs.getInt("ind_signo");
                    //double vlr   = rs.getDouble("vlr") * signo;
                    
                    
                    // Definicion del signo del valor
                    
                    double vlr      = rs.getDouble("vlr_for") * signo;
                    String currency = rs.getString("currency");
                    
                    // cambio de moneda si la moneda en WEB es PES y la moneda en mims es BOL
                    String currency_cf_code = this.getMonedaMasCF( rs.getString("cf_code") );
                    if (currency_cf_code.equals("BOL") && currency.equals("PES") ) {
                        
                        com.tsp.operation.model.beans.Tasa obj =  TasaSvc.buscarValorTasa
                        ("PES", currency, currency_cf_code, rs.getString("fecmov") );
                        if (obj!=null){
                            vlr = com.tsp.util.Util.roundByDecimal((vlr * obj.getValor_tasa()),0);
                            currency = currency_cf_code;
                        }
                    }
                    
                    
                    
                    
                    
                    
                    // Determinacion del impuesto ICA
                    int ICA = 0;
                    if ( rs.getString("ind_application").equalsIgnoreCase("V") &&
                    !rs.getString("codica").equals("")){
                        ICA = (int) Math.round( -vlr * rs.getDouble("ica") / 1000 );
                    }
                    
                    
                    /**************************************************
                     * Moneda del banco y sucursal                    *
                     **************************************************/
                    psCUR.clearParameters();
                    psCUR.setString(1, banco);
                    psCUR.setString(2, sucursal);
                    rsAUX = psCUR.executeQuery();
                    String moneda = (rsAUX.next()?rsAUX.getString("cur"):"");
                    rsAUX.close();
                    ////////////////////////////////////////////////////
                    
                    
                    // datos de control
                    fila.put("creation_date", rs.getString("cd"));
                    fila.put("concept_code" , rs.getString("cc"));
                    fila.put("planilla"     , rs.getString("pla"));
                    fila.put("pla_owner"    , rs.getString("po"));
                    fila.put("document"     , rs.getString("doc"));
                    
                    
                    // datos del archivo cabecera
                    fila.put(new Integer(1), new CadenaConLongitud(banco));
                    fila.put(new Integer(2), new CadenaConLongitud(sucursal));
                    fila.put(new Integer(3), new CadenaConLongitud(rs.getString("sietes")));
                    fila.put(new Integer(4), new CadenaConLongitud(rs.getString("plaveh")));
                    fila.put(new Integer(5), new CadenaConLongitud("W" + rs.getString("cc") + " " + rs.getString("pla") + " " + com.tsp.util.Util.DecimalToBase(rs.getString("fecfac"),36)  ));
                    fila.put(new Integer(6), new CadenaConLongitud(rs.getString("hoy")));
                    fila.put(new Integer(7), new CadenaConLongitud(""));
                    fila.put(new Integer(8), new CadenaConLongitud(String.valueOf(vlr)));
                    fila.put(new Integer(9), new CadenaConLongitud(moneda));
                    fila.put(new Integer(10), new CadenaConLongitud(""+(ICA)));
                    
                    // datos del item segun el ACCOUNT
                    int complet = 11;
                    if ("|01|15|".indexOf( rs.getString("cc") )!=-1 ){
                        fila.put(new Integer(11), new CadenaConLongitud(rs.getString("descripcion") + " " +  rs.getString("pla")));
                        fila.put(new Integer(12), new CadenaConLongitud(""+(vlr)));
                        fila.put(new Integer(13), new CadenaConLongitud(rs.getString("n")  ));
                        fila.put(new Integer(14), new CadenaConLongitud(rs.getString("sietes")));
                        fila.put(new Integer(15), new CadenaConLongitud(rs.getString("pla")));
                        fila.put(new Integer(16), new CadenaConLongitud(rs.getString("codica") ));
                        fila.put(new Integer(17), new CadenaConLongitud("22010101"));
                        complet = 18;
                    }else {
                        
                        // busqueda de los account de los estadares
                        psACC.clearParameters();
                        psACC.setString(1, rs.getString("pla") );
                        rsAUX = psACC.executeQuery();
                        
                        // SE LLENAN LOS ITEMS PARA DISTRIBUIR LOS COSTOS
                        while (rsAUX.next()){
                            int porcent = rsAUX.getInt( "porcent" );
                            fila.put(new Integer(complet++), new CadenaConLongitud(rs.getString("descripcion") + " " +  rs.getString("pla")));
                            fila.put(new Integer(complet++), new CadenaConLongitud(""+(vlr * (porcent/100) )));
                            fila.put(new Integer(complet++), new CadenaConLongitud(rs.getString("n")  ));
                            if (rsAUX.getRow()==1)
                                fila.put(new Integer(complet++), new CadenaConLongitud(rs.getString("sietes")));
                            fila.put(new Integer(complet++), new CadenaConLongitud(rs.getString("pla")));
                            fila.put(new Integer(complet++), new CadenaConLongitud(rs.getString("codica")));
                            fila.put(new Integer(complet++), new CadenaConLongitud(formatearAC(rsAUX.getString("ac"))));
                            fila.put(new Integer(complet++), new CadenaConLongitud(rsAUX.getString("numrem")));
                        }
                        rsAUX.close();
                    }
                    for( int j=complet; j <= 32; fila.put( new Integer(""+j), new CadenaConLongitud("")) , j++);
                    fila.put(new Integer(33), new CadenaConLongitud("P"));
                    datos.addElement(fila);
                }
            }
            
            
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally {
            if ( rs    !=null  ) rs.close();
            if ( rsAUX !=null  ) rsAUX.close();
            if ( psACC !=null  ) psACC.close();
            if ( psCUR !=null  ) psCUR.close();
            if ( ps    != null ) ps.close();
            this.desconectar("SQL_DATOS_CONTABLE");
            this.desconectar("SQL_CURRENCY_BANCO");
            this.desconectar("SQL_ACCOUNT");
            return datos;
        }
    }
     /**
     * Metodo para actualizar los movimientos de planilla 
     * @autor mfontalvo
     * @param mp, objeto movpla migrado
     * @param fechProceso fecha de Proceso de migracion
     * @throws Exception.
     */
    public void actualizarMovplaMigradoCHEQUE(Movpla mp, String fechaProceso) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = this.crearPreparedStatement("SQL_ACTUALIZAR_MOVPLA_CHEQUE");
            String [] extras = mp.getCausa().split("%");
            if (extras!=null){
                for (int i=0;i<extras.length;i++){
                    String []dt = extras[i].split(",");
                    ps.setString(1, dt[0] );
                    ps.setString(2, mp.getPlanilla());
                    ps.setString(3, dt[1] );
                    ps.setString(4, dt[2] );
                    logger.info( "Update movpla: "+ps);
                    ps.executeUpdate();
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        finally {
            if ( ps != null ) ps.close();
            this.desconectar("SQL_ACTUALIZAR_MOVPLA");
        }
    }    
    
     /**
   *Metodo que inserta los movimientos de valores relacionados a la
   *planilla en la tabla movpla
   *@autor Karen Reales, Andr�s Maturana
   *@param base
   *@throws En caso de que un error de base de datos ocurra.
   */
    public String insertMovPlaDouble(String base)throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql ="";
        try {
            st = this.crearPreparedStatement("SQL_INSERTMOVPLA");
            st.setString(1,movpla.getDstrct());
            st.setString(2,movpla.getAgency_id());
            st.setString(3,movpla.getDocument_type());
            st.setString(4,movpla.getConcept_code());
            st.setString(5,movpla.getPla_owner());
            st.setString(6,movpla.getPlanilla());
            st.setString(7,movpla.getSupplier());
            st.setString(8, movpla.getAp_ind());            
            st.setDouble(9, movpla.getVlr_ml());
            st.setString(10,movpla.getCurrency());            
            st.setDouble(11, movpla.getVlr_ml());
            st.setString(12, movpla.getProveedor());
            st.setString(13, movpla.getCreation_user());
            st.setString(14, movpla.getInd_vlr());            
            st.setDouble(15, movpla.getVlr_me());
            st.setString(16, movpla.getCreation_date());
            st.setString(17, movpla.getSucursal());
            st.setString(18, base);
            st.setString(19, movpla.getBanco());
            st.setString(20, movpla.getCuenta());
            st.setFloat(21,  movpla.getCantidad());
            st.setString(22, movpla.getReanticipo());
            st.setString(23, movpla.getFecha_migracion());
            st.setString(24, movpla.getBeneficiario());
            st.setString(25, movpla.getTercero());
            //st.executeUpdate();
            sql = st.toString();
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LOS MOVIMIENTO DE PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_INSERTMOVPLA");
        }
        return sql;
    }
     
    /**
     *Metodo que reversa en movpla el cheque
     *@autor: Karen Reales
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String registrarReversionCheque()throws SQLException{
        
        PreparedStatement st   = null;
        PreparedStatement st2  = null;
        ResultSet         rs   = null;
        String            sql  ="";
        float val;
        
        String         query1  = "SQL_ANULAR_CHEQUE_IMPRESO";
        String         query4  = "SQL_LISTA_REEMPLAZO";
        String         query5  = "SQL_ANULAR_CHEQUE_IMPRESO2";
        
        
        try {
            
            Connection con = this.conectar(query1);
            
            String banco [] = movpla.getBanco().split("/");
            String branch=movpla.getBanco();
            String account=movpla.getCuenta();
            
            
            
            st = con.prepareStatement(this.obtenerSQL(query1));
            st.setString(1, movpla.getCreation_user());
            st.setString(2,movpla.getDocument());
            st.setString(3, branch);
            st.setString(4, account);
            sql = st.toString();
            
            
            st = con.prepareStatement(this.obtenerSQL(query4));
            st.setString(1,movpla.getDocument());
            st.setString(2, branch);
            st.setString(3, account);
            System.out.println("Lista anticipos "+st.toString());
            rs = st.executeQuery();
            int minuto =112000;
            
            st2 = con.prepareStatement(this.obtenerSQL(query5));
            
            while(rs.next()){
                minuto=minuto+10000;
                
                st2.setString(1, movpla.getCreation_user());
                st2.setString(2, ""+new java.sql.Timestamp(System.currentTimeMillis()+minuto));
                st2.setString(3, rs.getString("concept_code"));
                st2.setString(4, rs.getString("planilla"));
                st2.setString(5, rs.getString("pla_owner"));
                st2.setString(6, rs.getString("creation_date"));
                sql =sql +";"+ st2.toString();
                
                st2.clearParameters();
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA REVERSION DE CHEQUES IMPRESO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar(query1);
            
        }
        return sql;
    }
    
    /**
     *Metodo que lista los datos del Movpla de una planilla y sus facturas relacionadas
     *@autor: Ing. Juan M. Escandon Perez
     *@param: Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listaAllMovPla_Factura(String numpla)throws SQLException{
        PreparedStatement st     = null;
        ResultSet rs             = null;
        movpla                   = null;
        String sql               = "SQL_LISTA_ALLMOVPLA_FACTURA";
        lista                    = new Vector();
        try {
            com.tsp.operation.model.services.TasaService tasa=  new com.tsp.operation.model.services.TasaService();
            st = this.crearPreparedStatement(sql);
            st.setString(1,numpla);
            rs = st.executeQuery();
            while(rs.next()){
                Movpla movpla = new Movpla();
                movpla.setDocument(rs.getString("document"));
                movpla.setConcept_code(rs.getString("concept_desc"));
                movpla.setApplication_ind(rs.getString("application_ind"));
                movpla.setInd_vlr(rs.getString("ind_vlr"));
                movpla.setVlr_disc(rs.getFloat("vlr"));
                movpla.setVlr((float)com.tsp.util.Util.redondear(rs.getDouble("valor_neto"),2));
                movpla.setCurrency(rs.getString("currency"));
                movpla.setCreation_date(rs.getString("creation_date"));
                movpla.setCreation_user(rs.getString("nombre"));
                movpla.setProveedor(rs.getString("proveedor"));
                movpla.setBranch_code(rs.getString("branch_code"));
                movpla.setBank_account_no(rs.getString("bank_account_no"));
                movpla.setFecha_migracion(rs.getString("fecha_migracion").equals("0099-01-01") || rs.getString("fecha_migracion").equals("1900-01-01")? "NO MIGRADO": rs.getString("fecha_migracion"));
                movpla.setFecha_cheque(rs.getString("fecha_contabilizacion").equals("0099-01-01 00:00:00")? "NO CONTABILIZADO": rs.getString("fecha_migracion"));
                movpla.setVlr_for(rs.getFloat("vlr_for"));
                movpla.setSigno(rs.getInt("ind_signo"));
                if(rs.getString("ind_vlr").equals("P")){
                    try{
                        tasa.buscarValorTasa("PES",rs.getString("currency"),"PES", rs.getString("fecha"));
                        if(tasa.obtenerTasa()!=null){
                            
                            double vlrTasa = tasa.obtenerTasa().getValor_tasa();
                            //System.out.println("Valor tasa "+vlrTasa);
                            movpla.setVlr_disc((float) (rs.getFloat("vlr")*vlrTasa));
                        }
                        else{
                            //System.out.println("No encontre la tasa...");
                        }
                    }catch(Exception e ){
                        e.printStackTrace();
                    }
                }
                
                movpla.setFactura           ( ( rs.getString("factura") != null )?rs.getString("factura"):""  );
                movpla.setEstado_factura    ( ( rs.getString("status_fra") != null )?rs.getString("status_fra"):""  );
                movpla.setProvedor_id       ( ( rs.getString("proveedor_id") != null )?rs.getString("proveedor_id"):""  );
                
                lista.add(movpla);
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE TODOS LOS MOVPLA  " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar(sql);
        }
        
    }
    
}

 /******************************************************
 Entregado a Karen 12 de Febrero 2007
 *******************************************************/

