/*
 * TiempoDAO.java
 *
 * Created on 26 de enero de 2005, 03:28 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  KREALES
 */
public class TiempoDAO {
    
    Tiempo tiempo;
    private Vector fechas;
    /** Creates a new instance of TiempoDAO */
    public TiempoDAO() {
    }
    
    
    public Vector getFechas(){
        return fechas;
    }
    public void setTiempo(Tiempo tiempo){
        this.tiempo = tiempo;
    }
    public Tiempo getTiempo(){
        return this.tiempo;
    }
    public void insert()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into tiempo (dstrct,sj,cf_code, time_code_1,time_code_2,secuence, creation_date,creation_user,reporte) values (?,?,?,?,?,?,'now()',?,?)");
                st.setString(1,tiempo.getdstrct());
                st.setString(2, tiempo.getsj());
                st.setString(3, tiempo.getcf_code());
                st.setString(4, tiempo.gettime_code_1());
                st.setString(5, tiempo.gettime_code_2());
                st.setString(6, tiempo.getsecuence());
                st.setString(7, tiempo.getcreation_user());
                st.setString(8, tiempo.getreporte());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE TIEMPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public void update()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update tiempo set time_code_1=?,time_code_2=?,secuence=?,user_update=?, reporte=? where dstrct=? and sj=? and cf_code=? and time_code_1=? and time_code_2=? ");
                st.setString(1, tiempo.gettime_code_1());
                st.setString(2, tiempo.gettime_code_2());
                st.setString(3, tiempo.getsecuence());
                st.setString(4, tiempo.getcreation_user());
                st.setString(5, tiempo.getreporte());
                st.setString(6,tiempo.getdstrct());
                st.setString(7, tiempo.getsj());
                st.setString(8, tiempo.getcf_code());
                st.setString(9, tiempo.gettime_codeUpdate1());
                st.setString(10, tiempo.gettime_codeUpdate2());
                ////System.out.println(st.toString());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LOS TIEMPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void anular()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update tiempo set reg_status='A', user_update=? where dstrct=? and sj=? and cf_code=? and time_code_1=? and time_code_2=?");
                st.setString(1, tiempo.getcreation_user());
                st.setString(2,tiempo.getdstrct());
                st.setString(3, tiempo.getsj());
                st.setString(4, tiempo.getcf_code());
                st.setString(5, tiempo.gettime_code_1());
                st.setString(6, tiempo.gettime_code_2());
                
                ////System.out.println(st.toString());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LOS TIEMPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void buscar(String distrito, String sj, String cf, String tiempo1, String tiempo2)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        tiempo=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from tiempo where dstrct=? and sj=? and cf_code=? and time_code_1=? and time_code_2=?");
                st.setString(1, distrito);
                st.setString(2, sj);
                st.setString(3, cf);
                st.setString(4, tiempo1);
                st.setString(5, tiempo2);
                rs = st.executeQuery();
                ////System.out.println(st.toString());
                if(rs.next()){
                    tiempo = new Tiempo();
                    tiempo.setcf_code(rs.getString("cf_code"));
                    tiempo.setdstrct(rs.getString("dstrct"));
                    tiempo.setreporte(rs.getString("reporte"));
                    tiempo.setsecuence(rs.getString("secuence"));
                    tiempo.setsj(rs.getString("sj"));
                    tiempo.settime_code_1(rs.getString("time_code_1"));
                    tiempo.settime_code_2(rs.getString("time_code_2"));
                    tiempo.settime_codeUpdate1(rs.getString("time_code_1"));
                    tiempo.settime_Update2(rs.getString("time_code_2"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIEMPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public boolean estaStandard( String sj, String cf)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw= false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from stdjobcosto where sj =? and cf_code=?");
                st.setString(1, sj);
                st.setString(2, cf);
                rs = st.executeQuery();
                ////System.out.println(st.toString());
                if(rs.next()){
                    sw= true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA VERIFICACION DEL STANDARD EN TIEMPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    public boolean searchFechas(String sj,String codigo)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select * from tbltiempo where sj=? and time_code=?");
                st.setString(1,sj);
                st.setString(2, codigo);
                rs = st.executeQuery();
                ////System.out.println(st.toString());
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS FECHAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public void listar()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        fechas = null;
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select * from tiempo where reg_status=''");
                rs = st.executeQuery();
                fechas = new Vector();
                while(rs.next()){
                    tiempo= new Tiempo();
                    tiempo.setcf_code(rs.getString("cf_code"));
                    tiempo.setdstrct(rs.getString("dstrct"));
                    tiempo.setreporte(rs.getString("reporte"));
                    tiempo.setsecuence(rs.getString("secuence"));
                    tiempo.setsj(rs.getString("sj"));
                    tiempo.settime_code_1(rs.getString("time_code_1"));
                    tiempo.settime_code_2(rs.getString("time_code_2"));
                    fechas.add(tiempo);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS FECHAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
}
