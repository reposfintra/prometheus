/*
 * MigracionRemesasAnuladasDAO.java
 *
 * Created on 16 de julio de 2005, 01:25 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Tito Andr�s Maturana
 */
public class MigracionRemesasAnuladasDAO {
	
	
    private Vector remesas_anuladas;
    
    private static final String SELECT_CONSULTA = 
        "select  ra.documento, To_Char(ra.creation_date,'dd/MM/YY') as creation_date," +
        " coalesce(usu.nit,'') as nit" +
        " from documento_anulado as ra left outer join usuarios as usu" +
        " on (usu.idusuario = ra.creation_user)" +
        " where ra.reg_status <> ? and ra.tipodoc='002'" +
        " order by ra.documento";
    private static final String UPDATE_CONSULTA = "update  documento_anulado set last_update='now()'," + 
        " user_update=?, reg_status='P' where documento=? and tipodoc='002'";
    
    
    
    /** Creates a new instance of MigracionRemesasAnuladasDAO */
    public MigracionRemesasAnuladasDAO() {
    }
    
    public void obtenerRemesasAnuladas() throws java.sql.SQLException{    
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SELECT_CONSULTA);
                st.setString(1,"P");
                rs = st.executeQuery();
                
                this.remesas_anuladas = new Vector();
                
                
                while(rs.next()){
                    RemesaAnulada remesa_anulada = new RemesaAnulada();
                    remesa_anulada.setNumero_remesa(rs.getString("documento"));
                    remesa_anulada.setFecha_creacion(rs.getString("creation_date"));
                    remesa_anulada.setUsuario_creacion(rs.getString("nit"));
                    remesa_anulada.setCodigo_creacion("AN");
                    
                    this.remesas_anuladas.add(remesa_anulada);                    
                }
                
            }            
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA ANULACION DE PLANILLAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }  
    }
    
    public void actualizarRegistros(String usuario)
        throws java.sql.SQLException{
        
        Connection con=null;
        PreparedStatement st = null;        
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                for(int i=0; i<this.remesas_anuladas.size(); i++){
                    RemesaAnulada remesa_anulada = (RemesaAnulada) this.remesas_anuladas.elementAt(i);
                    st = con.prepareStatement(this.UPDATE_CONSULTA);
                    st.setString(1, usuario);
                    st.setString(2, remesa_anulada.getNumero_remesa());
                    st.executeUpdate();
                }                 
            }           
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA ANULACION DE REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Getter for property remesas_anuladas.
     * @return Value of property remesas_anuladas.
     */
    public java.util.Vector getRemesas_anuladas() {
        return remesas_anuladas;
    }
    
    /**
     * Setter for property remesas_anuladas.
     * @param remesas_anuladas New value of property remesas_anuladas.
     */
    public void setRemesas_anuladas(java.util.Vector remesas_anuladas) {
        this.remesas_anuladas = remesas_anuladas;
    }
}
