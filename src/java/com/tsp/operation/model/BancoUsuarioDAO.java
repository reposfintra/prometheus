/*
 * BancoUsuarioDAO.java
 *
 * Created on 10 de junio de 2005, 02:54 PM
 */
package com.tsp.operation.model;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  kreales
 */
public class BancoUsuarioDAO {
    
    private final String SQL_ELIMINAR_BANCO_DE_USUARIO = "delete from banco_usuario where branch_code = ? and bank_account_no = ? and usuario = ? and dstrct = ?";
    private final String SQL_ELIMINAR_BANCOS_DE_USUARIO = "delete from banco_usuario where usuario = ?";
    private final String SQL_AGREGAR_BANCO_DE_USUARIO = "INSERT INTO banco_usuario (reg_status, dstrct, branch_code, bank_account_no, usuario, last_update, user_update, creation_date, creation_user, base) VALUES ('', ?, ?, ?, ?, 'now()', '', 'now()', '', ?)";
    
    private TreeMap listaBancos;
    private Banco banco;
    private String dataBaseName;
    /** Creates a new instance of BancoUsuarioDAO */
    public BancoUsuarioDAO() {
    }
    public BancoUsuarioDAO(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }
    
    /**
     * Getter for property listaBancos.
     * @return Value of property listaBancos.
     */
    public java.util.TreeMap getListaBancos() {
        return listaBancos;
    }    
    
    /**
     * Setter for property listaBancos.
     * @param listaBancos New value of property listaBancos.
     */
    public void setListaBancos(java.util.TreeMap listaBancos) {
        this.listaBancos = listaBancos;
    }
    
    /**
     * Este metodo hace casi lo mismo que buscarBancos( String user )
     * Pero con la diferencia que crea objetos bancos y los devuelve
     * en un java.util.Vector.
     */
    public Vector obtenerBancosDeUsuario(String user)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            Vector datos = new Vector();
            if(con!=null){
                st = con.prepareStatement("select a.branch_code," +
                "	a.bank_account_no, a.dstrct " +
                "from	banco a, " +
                "	banco_usuario b " +
                "where 	a.branch_code=b.branch_code and	" +
                "       a.bank_account_no=b.bank_account_no and " +
                "       b.usuario =?");
                st.setString(1,user);
                rs = st.executeQuery();
                while(rs.next()){
                    Banco b = new Banco();
                    b.setDstrct(rs.getString("dstrct"));
                    b.setBanco(rs.getString("branch_code"));
                    b.setBank_account_no(rs.getString("bank_account_no"));
                    datos.addElement(b);
                    //////System.out.println("bancos de usuario "+user+" -> "+b);
                }
            }
            return datos;
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA LISTA DE BANCOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
    
    public void eliminarBancoDeUsuario(Banco b, String usuario) throws SQLException {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            Vector datos = new Vector();
            if(con!=null){
                ////System.out.println("banco "+b+" de usuario "+usuario+" eliminado");
                //branch_code = ? and bank_account_no = ? and usuario = ? and dstrct = ?
                st = con.prepareStatement(SQL_ELIMINAR_BANCO_DE_USUARIO);
                st.setString(1,b.getBanco());
                st.setString(2,b.getBank_account_no());
                st.setString(3,usuario);
                st.setString(4,b.getDstrct());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA LISTA DE BANCOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
    
    public void eliminarBancosDeUsuario(String usuario) throws SQLException {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                //branch_code = ? and bank_account_no = ? and usuario = ? and dstrct = ?
                st = con.prepareStatement(SQL_ELIMINAR_BANCOS_DE_USUARIO);
                st.setString(1,usuario);
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ELIMINACION DE BANCOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
    
    private String obtenerBaseDeUsuario(String usuario, Connection con)throws SQLException{
        PreparedStatement ps = con.prepareStatement("select base from usuarios where idusuario = ?");
        ps.setString(1, usuario);
        ResultSet rs = ps.executeQuery();
        if ( rs.next() ){
            return rs.getString(1);
        }
        return "";
    }
    
    public void agregarBancosDeUsuario(String [] nuevos, String usuario)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                String base = obtenerBaseDeUsuario(usuario,con);
                st = con.prepareStatement(SQL_AGREGAR_BANCO_DE_USUARIO);
                for( int i=0; i<nuevos.length; i++ ){
                    String datos [] = nuevos[i].split("/");
                    st.setString(1,datos[0]);//distrito
                    st.setString(2,datos[1]);//branch_code
                    st.setString(3,datos[2]);//bank_account_no
                    st.setString(4,usuario);
                    st.setString(5,base);
                    st.executeUpdate();
                    st.clearParameters();
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA LISTA DE BANCOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
    
    
    public void buscarBancos(String user)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("select a.branch_code," +
                "	a.bank_account_no," +
                "       a.currency	" +
                "from	banco a, " +
                "	banco_usuario b " +
                "where 	a.branch_code=b.branch_code and	" +
                "       a.bank_account_no=b.bank_account_no and " +
                "       upper(b.usuario) =?");
                st.setString(1,user);
                rs = st.executeQuery();
                listaBancos= new TreeMap();
                listaBancos.put("Seleccione un item","");
                while(rs.next()){
                   listaBancos.put(rs.getString("branch_code")+"/"+rs.getString("bank_account_no"),rs.getString("branch_code")+"/"+rs.getString("bank_account_no")+"/"+rs.getString("currency"));
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA LISTA DE BANCOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    
    }
    
}
