/*
 * CarroceriaService.java
 *
 * Created on 9 de noviembre de 2004, 02:53 PM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  AMENDEZ
 */
public class MarcaService {
    
    private MarcaDAO marcaDataAccess;
    /** Creates a new instance of CarroceriaService */
    public MarcaService() {
        marcaDataAccess = new MarcaDAO();
    }
    
    public TreeMap listar() throws SQLException{ 
        marcaDataAccess.listar();
        return marcaDataAccess.getCbxMarca();  
    }
    public String getMarcaVehiculo(String codigo) throws SQLException {
        return marcaDataAccess.getMarca(codigo);
    }
    
}