/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.EstadoCuentaDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HaroldC
 */
public class EstadoCuentaImpl extends MainDAO implements EstadoCuentaDAO{

    public EstadoCuentaImpl(String dataBaseName) {
        super("EstadoCuentaDAO.xml", dataBaseName);
    }

    
    @Override
    public JsonObject EstadoCuentaApoteosys(JsonObject jsonData, String Documento) {
       
       String inputLine;
       URL apiUrl = null;
       HttpURLConnection connection = null;
       JsonObject response = new JsonObject();
       
       try {
           apiUrl = new URL("http://prometheus.fintra.co:8084/omnicanal/webresources/estado_cuenta/consultar_estado/documento="+Documento);
           connection = (HttpURLConnection) apiUrl.openConnection();
           connection.setRequestMethod("POST");                    
           connection.setRequestProperty("Content-Type", "application/json");
           //connection.setRequestProperty("token", token);
           connection.setDoOutput(false);
           connection.setDoInput(true);
           connection.setUseCaches(false);
           
           int status = connection.getResponseCode();
           if (status == 200) {

               BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
               StringBuffer content = new StringBuffer(in.readLine());               
               
               response = (JsonObject) new JsonParser().parse(content.toString());
               System.out.println(response);
               in.close();
               
           } else {
               throw new IOException("Response code " + status);
           }
       } catch (IOException ex) {
           ex.printStackTrace();
           response = (JsonObject) new JsonParser().parse("{\"error\":{\"data\":{\"comentario\": \"Error en la comunicacion con Datacredito: " + ex.getMessage() + "\"}}}");
       } finally {
           if (connection != null) connection.disconnect();
       }
       
       return response;       
       
    }     


    @Override
    public JsonObject TerceroPlaca(JsonObject jsonData, String Placa) {
       
       String inputLine;
       URL apiUrl = null;
       HttpURLConnection connection = null;
       JsonObject response = new JsonObject();
       
       try {
           apiUrl = new URL("http://prometheus.fintra.co:8084/omnicanal/webresources/placa_tercero/terplaca/plate="+Placa);
           connection = (HttpURLConnection) apiUrl.openConnection();
           connection.setRequestMethod("POST");                    
           connection.setRequestProperty("Content-Type", "application/json");
           //connection.setRequestProperty("token", token);
           connection.setDoOutput(false);
           connection.setDoInput(true);
           connection.setUseCaches(false);
           
           int status = connection.getResponseCode();
           if (status == 200) {

               BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
               StringBuffer content = new StringBuffer(in.readLine());               
               
               response = (JsonObject) new JsonParser().parse(content.toString());
               System.out.println(response);
               in.close();
               
           } else {
               throw new IOException("Response code " + status);
           }
       } catch (IOException ex) {
           ex.printStackTrace();
           response = (JsonObject) new JsonParser().parse("{\"error\":{\"data\":{\"comentario\": \"Error en la comunicacion con Datacredito: " + ex.getMessage() + "\"}}}");
       } finally {
           if (connection != null) connection.disconnect();
       }
       
       return response;       
       
    }     
    
    
    
}