/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.CorregirPlanillasDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.AnticiposTerceros;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.CorregirPlanillaBeans;
import com.tsp.operation.model.beans.ExtractoDetalleBeans;
import com.tsp.operation.model.beans.StringStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hcuello
 */
public class CorregirPlanillaImpl extends MainDAO implements CorregirPlanillasDAO {

    public CorregirPlanillaImpl(String dataBaseName) {
        super("CorregirPlanillaDAO.xml", dataBaseName);
    }

    @Override
    public ArrayList<CorregirPlanillaBeans> buscarPlanilla() throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_PLANILLAS";
        ArrayList<CorregirPlanillaBeans> listaPlanillas = null;
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            listaPlanillas = new ArrayList();
            CorregirPlanillaBeans beans = null;
            while (rs.next()) {

                beans = new CorregirPlanillaBeans();
                beans.setNit(rs.getString("nit"));
                beans.setNumero_operacion(rs.getInt("numero_operacion"));
                beans.setTipo_documento(rs.getString("tipo_documento"));
                beans.setClase(rs.getString("clase"));
                beans.setDocumento(rs.getString("documento"));
                beans.setValor_calculado(rs.getDouble("valor_calculado"));
                beans.setValor_registrado(rs.getDouble("valor_registrado"));
                beans.setDiferencia_interna(rs.getDouble("diferencia_interna"));
                beans.setPlaca(rs.getString("placa"));

                listaPlanillas.add(beans);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LAS PLANILLAS EXTRACTO DETALLE buscarPlanilla() " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return listaPlanillas;

    }

    @Override
    public ArrayList<ExtractoDetalleBeans> buscarExtractoDetalle(int numeroOperacion, String documento, String clase) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_EXTRACTO";
        if (!clase.equals("COP") && !clase.equals("SOP")) {
            query = "SQL_BUSCAR_EXTRACTO_FAC";
        }

        ArrayList<ExtractoDetalleBeans> listaExtracto = null;

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, numeroOperacion);
            ps.setString(2, documento);
            rs = ps.executeQuery();

            listaExtracto = new ArrayList();
            ExtractoDetalleBeans beans = null;

            while (rs.next()) {

                beans = new ExtractoDetalleBeans();
                beans.setConcepto(rs.getString("concepto"));
                beans.setDescripcion(rs.getString("descripcion"));
                beans.setFactura(rs.getString("factura"));
                beans.setValor(rs.getDouble("vlr"));
                beans.setRetefuente(rs.getDouble("retefuente"));
                beans.setReteica(rs.getDouble("reteica"));
                beans.setImpuestos(rs.getDouble("impuestos"));
                beans.setValor_total(rs.getDouble("vlr_ppa_item"));
                beans.setSecuencia(rs.getInt("secuencia"));
                beans.setDocumento(rs.getString("documento"));

                listaExtracto.add(beans);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LAS PLANILLAS EXTRACTO DETALLE buscarExtractoDetalle() " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return listaExtracto;

    }

    @Override
    public String actualizarExtractoDetalle(ExtractoDetalleBeans extractoDetalleBeans, String clase, String user) throws SQLException {
        String cadena = "";

        String query = "SQL_UPDATE_EXTRACTO_DET";
        if (!clase.equals("COP") && !clase.equals("SOP")) {
            query = "SQL_UPDATE_EXTRACTO_DET_FAC";
        }
        String sql = "";
        try {

            StringStatement st = null;

            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);

            st.setDouble(1, extractoDetalleBeans.getValor());
            st.setDouble(2, extractoDetalleBeans.getRetefuente());
            st.setDouble(3, extractoDetalleBeans.getReteica());
            st.setDouble(4, extractoDetalleBeans.getImpuestos());
            st.setString(5, user);

            //WHERE
            st.setInt(6, extractoDetalleBeans.getSecuencia());
            if (!clase.equals("COP") && !clase.equals("SOP")) {
                st.setString(7, extractoDetalleBeans.getFactura());
            } else {
                st.setString(7, extractoDetalleBeans.getDocumento());
            }
            st.setString(8, extractoDetalleBeans.getConcepto());

            cadena = st.getSql();

            st = null;

        } catch (Exception ex) {
            Logger.getLogger(CorregirPlanillaImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cadena;
    }

    @Override
    public String buscarDiferenciaTablas() throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DIFERENCIA_TABLAS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = null;
        ArrayList lista = null;
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));

            rs = ps.executeQuery();
            lista = new ArrayList();
            while (rs.next()) {
                obj = new JsonObject();
                obj.addProperty("secuencia", rs.getString("secuencia"));
                obj.addProperty("tabla", rs.getString("tbl"));
                obj.addProperty("vlr", rs.getString("sum"));
                lista.add(obj);
            }

            String secuencia = "";
            String tabla = "";
            String aux = "";
            String valor = "";

            JsonObject objNuevo = new JsonObject();
            JsonArray listanueva = new JsonArray();
            for (int i = 0; i < lista.size(); i++) {

                JsonObject jsonObject = (JsonObject) lista.get(i);
                secuencia = jsonObject.get("secuencia").getAsJsonPrimitive().getAsString();
                valor = jsonObject.get("vlr").getAsJsonPrimitive().getAsString();

                if (aux.equals("")) {

                    tabla = jsonObject.get("tabla").getAsJsonPrimitive().getAsString();
                    objNuevo.addProperty("secuencia", secuencia);
                    if (tabla.equals("APT")) {
                        objNuevo.addProperty("anticipo", valor);
                    } else if (tabla.equals("EXT")) {
                        objNuevo.addProperty("extracto", valor);
                    } else {
                        objNuevo.addProperty("extractodet", valor);
                    }

                    aux = secuencia;

                } else if (aux.equals(secuencia)) {

                    tabla = jsonObject.get("tabla").getAsJsonPrimitive().getAsString();
                    if (tabla.equals("APT")) {
                        objNuevo.addProperty("anticipo", valor);
                    } else if (tabla.equals("EXT")) {
                        objNuevo.addProperty("extracto", valor);
                    } else {
                        objNuevo.addProperty("extractodet", valor);
                    }

                    aux = secuencia;

                } else {

                    //agrego una linea en una lista.
                    listanueva.add(objNuevo);
                    objNuevo = new JsonObject();

                    tabla = jsonObject.get("tabla").getAsJsonPrimitive().getAsString();
                    objNuevo.addProperty("secuencia", secuencia);
                    if (tabla.equals("APT")) {
                        objNuevo.addProperty("anticipo", valor);
                    } else if (tabla.equals("EXT")) {
                        objNuevo.addProperty("extracto", valor);
                    } else {
                        objNuevo.addProperty("extractodet", valor);
                    }

                    aux = secuencia;

                }
            }

            //para el ultimo componente de la lista.
            listanueva.add(objNuevo);

            obj = new JsonObject();
            obj.add("rows", listanueva);
            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }

            return respuestaJson;
        }
    }

    @Override
    public String buscarAnticipos(int secuencia) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ANTICIPOS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = null;
        JsonArray listaJson = new JsonArray();
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, secuencia);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj = new JsonObject();
                obj.addProperty("identificador", rs.getString("id"));
                obj.addProperty("planilla", rs.getString("planilla"));
                obj.addProperty("valor", rs.getString("vlr"));
                obj.addProperty("valor_for", rs.getString("vlr_for"));
                obj.addProperty("secuencia", rs.getString("secuencia"));
                listaJson.add(obj);
            }

            obj = new JsonObject();
            obj.add("rows", listaJson);
            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }

            return respuestaJson;
        }

    }

    @Override
    public String actualizarAnticipo(String id, double valor, double valor_for,String user) throws SQLException {
        
        String cadena = "";
        String query = "SQL_UPDATE_ANTICIPO"; 
        String sql = "";
        try {

            StringStatement st = null;
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);

            st.setDouble(1,valor);
            st.setDouble(2,valor_for);
            st.setString(3, user);
            st.setString(4, id);  

            cadena = st.getSql();

            st = null;

        } catch (Exception ex) {
            Logger.getLogger(CorregirPlanillaImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cadena;       
    }

    @Override
    public String actualizarExtracto(int secuencia, double valor,String user) throws SQLException {
         
        String cadena = "";
        String query = "SQL_UPDATE_EXTRACTO"; 
        String sql = "";
        try {

            StringStatement st = null;
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);

            st.setDouble(1,valor);
            st.setString(2,user);
            st.setInt(3,secuencia);
            
            cadena = st.getSql();

            st = null;

        } catch (Exception ex) {
            Logger.getLogger(CorregirPlanillaImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cadena;       
    }

    @Override
    public ArrayList<AnticiposTerceros> buscarPlanillasAnticipo(String user, String fecha) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_PLANILLAS_ANTICIPO";
        ArrayList<AnticiposTerceros> listaPlanillas = null;
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, user);
            ps.setString(2, fecha+"%");
            
            rs = ps.executeQuery();
            listaPlanillas = new ArrayList();
            AnticiposTerceros beans = null;
            while (rs.next()) {
                beans = new AnticiposTerceros();
                beans.setUser_transferencia(rs.getString("user_transferencia"));
                beans.setTransferido(rs.getString("transferido"));
                beans.setFecha_transferencia(rs.getString("fecha_transferencia"));
                beans.setBanco_transferencia(rs.getString("banco_transferencia"));
                beans.setVlr(rs.getDouble("valor_lote"));
                beans.setSecuencia(rs.getInt("num_rows"));

                listaPlanillas.add(beans);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LAS PLANILLAS DE ANTICIPO buscarPlanillasAnticipo() " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return listaPlanillas;
    }

    @Override
    public ArrayList<AnticiposTerceros> detallePlanillaAnticipo(String user, String fecha_transf) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DETALLE_PLANILLA_ANTICIPO";
        ArrayList<AnticiposTerceros> listaPlanillas = null;
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, user);
            ps.setString(2, fecha_transf);
            
            rs = ps.executeQuery();
            listaPlanillas = new ArrayList();
            AnticiposTerceros beans = null;
            while (rs.next()) {
                beans = new AnticiposTerceros();
                beans.setId(rs.getInt("id"));
                beans.setPlanilla(rs.getString("planilla"));
                beans.setPla_owner(rs.getString("pla_owner"));
                beans.setNombre_cuenta(rs.getString("nombre_cuenta"));
                beans.setCuenta(rs.getString("cuenta"));
                beans.setVlrConsignar(rs.getDouble("vlr_consignacion"));
                beans.setUser_transferencia(rs.getString("user_transferencia"));
                beans.setTransferido(rs.getString("transferido"));
                beans.setFecha_transferencia(rs.getString("fecha_transferencia"));
                beans.setBanco_transferencia(rs.getString("banco_transferencia"));
                
                listaPlanillas.add(beans);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO EL DETALLE DE LA PLANILLA DE ANTICIPO detallePlanillaAnticipo() " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return listaPlanillas;
    }

    @Override
    public String reversarPlanillasAnticipo(String user, String fecha, String login) throws SQLException {
        String cadena = "";
        String query = "SQL_REVERSAR_PLANILLAS_ANTICIPO"; 
        String sql = "";
        try {

            StringStatement st = null;
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);

            st.setString(1, login);
            st.setString(2, user);
            st.setString(3, fecha);
           
            cadena = st.getSql();

            st = null;

        } catch (Exception ex) {
            Logger.getLogger(CorregirPlanillaImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cadena;       
    }

    @Override
    public String cargarComboUsuarios() throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_USUARIOS_ANTICIPO";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            
            while (rs.next()) {
           
                obj.addProperty(rs.getString("user"), rs.getString("user"));
            
            }
            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            
            return respuestaJson;

        }
   }
  
}
