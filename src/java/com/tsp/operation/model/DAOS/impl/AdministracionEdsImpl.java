/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;
import com.tsp.operation.model.DAOS.AdministracionEdsDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.AuditoriaVentasBeans;
import com.tsp.operation.model.beans.EDSPropietarioBeans;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.reporteColocacionBeans;
import com.tsp.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mariana
 */
public class AdministracionEdsImpl extends MainDAO implements AdministracionEdsDAO {

    public AdministracionEdsImpl(String dataBaseName) {
        super("AdministracionEdsDAO.xml", dataBaseName);
    }

    @Override
    public String guardarUsuario(String representante, String direccion, String correo, String telefono, String pais, String nit, String ciudad, String estadousu, String idusuario, String passw, String cia, String cambioclav, String base, String perfil) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_USUARIOS";
        try {

            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, representante);
            st.setString(2, direccion);
            st.setString(3, correo);
            st.setString(4, telefono);
            st.setString(5, pais);
            st.setString(6, ciudad);
            st.setString(7, nit);
            st.setString(8, "CLIENTETSP");
            st.setString(9, perfil);
            st.setString(10, estadousu);
            st.setString(11, "OP");
            st.setString(12, idusuario);
            st.setString(13, passw);
            st.setString(14, cia);
            st.setBoolean(15, Boolean.parseBoolean(cambioclav));
            st.setString(16, base);
            st.setString(17, idusuario);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String gPropietarioEds(String cia, String tipopersona, String nitem, String nombreprop, String direccion, String correo, String nit, String representante, String usuario) {
        Connection con = null;
        StringStatement st = null;
        String respuesta = "";
        //PreparedStatement st = null;
        String query = "SQL_PROPIETARIO_EDS";
        try {

            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            //ps = con.prepareStatement(this.obtenerSQL(query));

            st.setString(1, cia);
            st.setString(2, tipopersona);
            st.setString(3, nitem);
            st.setString(4, nombreprop);
            st.setString(5, direccion);
            st.setString(6, correo);
            st.setString(7, nit);
            st.setString(8, representante);
            st.setString(9, usuario);
            st.setString(10, nitem);
            //ps.executeUpdate();
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }

    }

    @Override
    public String cargarCompania() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMPANIA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("dstrct"), rs.getString("dstrct"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarCiudad(String departamento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CIUDAD";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, departamento);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("codciu"), rs.getString("nomciu"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarPais() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PAIS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("country_code"), rs.getString("country_name"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarDepartamento(String pais) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DEPARTAMENTO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, pais);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("department_code"), rs.getString("department_name"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public ArrayList<EDSPropietarioBeans> cargarEDS(String usuario, String tipousu) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOSTRAR_EDS";

        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String condicion = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            ps.setString(2, usuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setEdsid(rs.getString("ideds"));
                beanEds.setBanId(rs.getString("idban"));
                beanEds.setBanRazonsocial(rs.getString("bandera"));
                beanEds.setNombreprop(rs.getString("propietario"));
                beanEds.setEdsnombre(rs.getString("nombre_eds"));
                beanEds.setEdsnitestacion(rs.getString("nit_estacion"));
                beanEds.setCodDepartamento(rs.getString("dpto"));
                beanEds.setCodPais(rs.getString("pais"));
                beanEds.setEdsciudad(rs.getString("municipio"));
                beanEds.setCodCiudad(rs.getString("cod_municipio"));
                beanEds.setEdsdireccion(rs.getString("direccion"));
                beanEds.setEdsusuario(rs.getString("usuario_estacion"));
                beanEds.setEdsestado(rs.getString("estado"));
                beanEds.setEdsencargado(rs.getString("nombre_encargado_eds"));
                beanEds.setEdsnitencargado(rs.getString("cc_encargado"));
                beanEds.setEdstelefono(rs.getString("telefono"));
                beanEds.setEdscorreo(rs.getString("correo"));
                beanEds.setEdspass(rs.getString("clave"));

                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public ArrayList<EDSPropietarioBeans> cargarEds(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOSTRAR_EDS";

        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String condicion = "";

            condicion = " WHERE  ban.id = '" + id + "' ";

            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setIdpropietario(rs.getString("id"));
                beanEds.setBanId(rs.getString("id"));
                beanEds.setNombreprop(rs.getString("razon_social"));
                beanEds.setEdsnombre(rs.getString("nombre_eds"));
                beanEds.setEdsciudad(rs.getString("municipio"));
                beanEds.setEdsdireccion(rs.getString("direccion"));
                beanEds.setEdsusuario(rs.getString("idusuario"));

                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String guardarEds(String idpropietario, String idbandera, String nombreds, String niteds, String municipio, String direccion, String telefono, String correo, String estadouser, String idusuario, String createusuario) {
        StringStatement st = null;
        String respuesta = "";
        //PreparedStatement st = null;
        String query = "SQL_GUARDAR_EDS";
        try {

            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            //st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, "FINV");
            st.setInt(2, Integer.parseInt(idbandera));
            st.setInt(3, Integer.parseInt(idpropietario));
            st.setString(4, nombreds);
            st.setString(5, niteds);
            st.setString(6, municipio);
            st.setString(7, direccion);
            st.setString(8, telefono);
            st.setString(9, correo);
            st.setString(10, idusuario);
            st.setString(11, createusuario);
            st.setString(12, niteds);

            //st.executeUpdate();
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarProductosEds(String cia, String nombre_producto, String unidadmed) {
        StringStatement st = null;
        String respuesta = "";

        String query = "SQL_GUARDAR_PRODUCTO_EDS";
        try {

            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, cia);
            st.setString(2, nombre_producto);
            st.setString(3, unidadmed);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarUsuario(String cia, String idusuario, String usuario, String base) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_USUARIO2";
        try {

            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, idusuario);
            st.setString(2, usuario);
            st.setString(3, usuario);
            st.setString(4, base);
            st.setString(5, cia);
            st.setString(6, idusuario);
            st.setString(7, idusuario);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarUsuarioPerfil(String perfil, String idusuario, String usuario, String cia) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_USUARIO3";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, perfil);
            st.setString(2, idusuario);
            st.setString(3, usuario);
            st.setString(4, cia);
            st.setString(5, idusuario);
            st.setString(6, idusuario);
            st.setString(7, perfil);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cargarBandera() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_BANDERA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("razon_social"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarPropietario(String usuario, String tipo, Usuario user) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PROPIETARIO";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        JsonArray lista = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            obj.addProperty("id", "");
            obj.addProperty("razon_social", "Seleccione");
            obj.addProperty("identificacion", "");
            lista.add(obj);

            while (rs.next()) {
                obj = new JsonObject();
                obj.addProperty("id", rs.getInt("id"));
                obj.addProperty("razon_social", rs.getString("razon_social"));
                obj.addProperty("identificacion", rs.getString("identificacion"));

                if (user.getCedula().equals(rs.getString("identificacion"))) {
                    lista = new JsonArray();
                    lista.add(obj);
                    break;
                } else {
                    lista.add(obj);
                }
            }

        } catch (Exception ex) {
            lista = null;
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

            return new Gson().toJson(lista);
        }

    }

    @Override
    public String verificarUsuario(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICAR_IDUSUARIO";
        String respuesta = "";

        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString(1);
            }
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String actualizarEds(String nombreds, String niteds, String ciudad, String direccion, String telefono, String correo, String idusuario, String usuario, String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_EDS";

        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, nombreds);
            st.setString(2, niteds);
            st.setString(3, ciudad);
            st.setString(4, direccion);
            st.setString(5, telefono);
            st.setString(6, correo);
            st.setString(7, idusuario);
            st.setString(8, usuario);
            st.setString(9, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cambiarEstadoEds(String id, String usuario) {
        StringStatement st = null;
        String respuesta = "";

        String query = "SQL_CAMBIAR_ESTADO_EDS";

        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);

            st.setString(1, usuario);
            st.setString(2, id);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cargarComboEDS(String idpropietario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_EDS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idpropietario);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre_eds"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarproductosEds() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_SERVICIOS_EDS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public ArrayList<EDSPropietarioBeans> cargarGrillaProductosEDS() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOSTRAR_GRILLA_PRODUCTOS_EDS";

        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setProId(rs.getString("id"));
                beanEds.setUnid(rs.getString("id_unidad_medida"));
                beanEds.setProCodProducto(rs.getString("cod_producto"));
                beanEds.setProDescripcion(rs.getString("descripcion"));
                beanEds.setUniNombreUnidad(rs.getString("nombre_unidad"));
                beanEds.setProEstado(rs.getString("Estado"));

                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String cargarUnidadMedidaPrp() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_UNIDAD_MEDIDA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre_unidad"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cambiarEstadoProducto(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_PRODUCTO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarProducto(String descripcion, String uniMedida, String id) {
        StringStatement st = null;
        String respuesta = "";

        String query = "SQL_ACTUALIZAR_PRODUCTO";

        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, descripcion);
            st.setString(2, uniMedida);
            st.setString(3, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarUsuario(String nombreprop, String direccion, String correo, String telefono, String pais, String ciudad, String passw, String usuariosedit) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_USUARIOS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, nombreprop);
            st.setString(2, direccion);
            st.setString(3, correo);
            st.setString(4, telefono);
            st.setString(5, pais);
            st.setString(6, ciudad);
            // st.setString(7, nit);
            // st.setString(8, idusuario);
            st.setString(7, passw);
            st.setString(8, usuariosedit);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarUsuarioProyecto(String idusuario, String usuario, String idusuarioedit) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_USUARIO_PROYECTO";
        try {

            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, idusuario);
            st.setString(2, usuario);
            st.setString(3, idusuarioedit);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarUsuarioPerfil(String idusuario, String usuario, String idusuarioedit) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_PERFIL_USUARIO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, idusuario);
            st.setString(2, usuario);
            st.setString(3, idusuarioedit);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public ArrayList<EDSPropietarioBeans> cargarGrillaBanderas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOSTRAR_GRILLA_BANDERAS";

        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setBanId(rs.getString("id"));
                beanEds.setBancodbandera(rs.getString("cod_bandera_es"));
                beanEds.setBantippersona(rs.getString("tipo_perona"));
                beanEds.setBanNit(rs.getString("identificacion"));
                beanEds.setBanRazonsocial(rs.getString("razon_social"));
                beanEds.setBandireccion(rs.getString("direccion"));
                beanEds.setBanCorreo(rs.getString("correo"));
                beanEds.setBanDocRep(rs.getString("documento_representante_legal"));
                beanEds.setBanRepresentante(rs.getString("representante_legal"));
                beanEds.setBanestado(rs.getString("Estado"));

                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String cambiarEstadoBandera(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_BANDERA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarBanderas(String cia, String razonspcial, String nit, String tipoper, String direccion, String correo, String replegal, String docreplegal, String usuario) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_BANDERAS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, cia);
            st.setString(2, tipoper);
            st.setString(3, nit);
            st.setString(4, razonspcial);
            st.setString(5, direccion);
            st.setString(6, correo);
            st.setString(7, docreplegal);
            st.setString(8, replegal);
            st.setString(9, usuario);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarBanderas(String razonspcial, String nit, String tipoper, String direccion, String correo, String replegal, String docreplegal, String usuario, String idbandera) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_BANDERA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, tipoper);
            st.setString(2, nit);
            st.setString(3, razonspcial);
            st.setString(4, direccion);
            st.setString(5, correo);
            st.setString(6, docreplegal);
            st.setString(7, replegal);
            st.setString(8, usuario);
            st.setString(9, idbandera);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public ArrayList<EDSPropietarioBeans> cargarProductosRelEDS(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_PRODUCTOS_RELACION_EDS";

        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setConfproduc(rs.getString("producto"));
                beanEds.setConfcomision(rs.getString("descuento_fintra"));
                beanEds.setConfdescripcion(rs.getString("acuerdo_comercial"));
                beanEds.setConfporcentaje(rs.getString("porcentaje_comision"));
                beanEds.setConfvalor(rs.getString("valor_comision"));
                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public ArrayList<EDSPropietarioBeans> cargarGrillaUnidadMedida() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOSTRAR_GRILLA_UNIDAD_MEDIDA";

        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setUnid(rs.getString("id"));
                beanEds.setUniNombreUnidad(rs.getString("nombre_unidad"));
                beanEds.setUnimedicion(rs.getString("unidad_medicion"));
                beanEds.setUniestado(rs.getString("Estado"));

                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String guardarUnidadMedida(String cia, String nomunmed, String medicion) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_UNIDAD_MEDIDA";
        try {

            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, cia);
            st.setString(2, nomunmed);
            st.setString(3, medicion);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarUnidadMedida(String nomunmed, String medicion, String idum) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_UNIDAD_MEDIDA";

        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, nomunmed);
            st.setString(2, medicion);
            st.setString(3, idum);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cambiarEstadoUnidadMedida(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_UNIDAD_MEDIDA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarProveedor(String cia, String nit, String nombre, String usuario, String tipodoc,
            String tipoper, String gcontribuyente, String aretenedor, String auretefunete, String auiva,
            String auica, String base, String banco, String sedepago, String banagencia, String tcuenta,
            String ptransfer, String bancotransfer, String numcuenta, String hc, String suctransfer,
            String regimen, String nombre_cuenta, String cedula_cuenta, String digitover) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_PROVEEDOR";
        try {
            if (ptransfer.equals("2")) {
                ptransfer = "T";

            } else {
                ptransfer = "B";
            }
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, cia);
            st.setString(2, nit);
            st.setString(3, nombre);
            st.setString(4, usuario);
            st.setString(5, tipodoc);
            st.setString(6, tipoper);
            st.setString(7, gcontribuyente);
            st.setString(8, aretenedor);
            st.setString(9, auretefunete);
            st.setString(10, auiva);
            st.setString(11, auica);
            st.setString(12, base);
            st.setString(13, cedula_cuenta);//sale de la vista 
            st.setString(14, nombre_cuenta);//sale de la vista 
            st.setString(15, banco);
            st.setString(16, banagencia);
            st.setString(17, sedepago);
            st.setString(18, tcuenta);
            st.setString(19, numcuenta);
            st.setString(20, bancotransfer);
            st.setString(21, ptransfer);
            st.setString(22, hc);
            st.setString(23, suctransfer);
            st.setString(24, nit);
            st.setString(25, regimen);
            st.setString(26, digitover);
            st.setString(27, nit);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cargarBanco() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_BANCO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("branch_code"), rs.getString("branch_code"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarAgencia(String banco) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_AGENCIA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, banco);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("bank_account_no"), rs.getString("bank_account_no"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarSedePago() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_SEDE_PAGO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("agasoc"), rs.getString("nomciu"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarBancotransfer() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_BANCO_TRANSFER";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("table_code"), rs.getString("table_code"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarHC() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_HC";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("table_code"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarSucursal() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_SUCURSAL";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("codciu"), rs.getString("nomciu"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public ArrayList<EDSPropietarioBeans> cargarPropietarioEds() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PROPIETARIOS_EDS";
        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setIdpropietario(rs.getString("id"));
                beanEds.setNombreprop(rs.getString("razon_social"));
                beanEds.setTipopersona(rs.getString("tipo_persona"));
                beanEds.setNit(rs.getString("identificacion"));
                beanEds.setTipodoc(rs.getString("tipo_doc"));
                beanEds.setRepresentante(rs.getString("representante_legal"));
                beanEds.setNitem(rs.getString("documento_representante_legal"));
                beanEds.setDireccion(rs.getString("direccion"));
                beanEds.setCorreo(rs.getString("correo"));
                beanEds.setTelefono(rs.getString("telefono"));
                beanEds.setCodCiudad(rs.getString("ciudad"));
                beanEds.setCodDepartamento(rs.getString("dpto"));
                beanEds.setCodPais(rs.getString("pais"));
                beanEds.setContribuyente(rs.getString("gran_contribuyente"));
                beanEds.setTiporegimen(rs.getString("regimen"));
                beanEds.setIva(rs.getString("iva"));
                beanEds.setIca(rs.getString("ica"));
                beanEds.setRetefuente(rs.getString("retefuente"));
                beanEds.setAutoretenedor(rs.getString("agente_retenedor"));
                beanEds.setHc(rs.getString("hc"));
                beanEds.setNumcuenta(rs.getString("no_cuenta"));
                beanEds.setTipocuenta(rs.getString("tipo_cuenta"));
                beanEds.setBanco(rs.getString("branch_code"));
                beanEds.setSedebanco(rs.getString("agency_id"));
                beanEds.setBanagencia(rs.getString("bank_account_no"));
                beanEds.setTipopago(rs.getString("tipo_pago"));
                beanEds.setBancotransfer(rs.getString("banco_transfer"));
                beanEds.setSucursaltransfer(rs.getString("suc_transfer"));
                beanEds.setIdusuario(rs.getString("idusuario"));
                beanEds.setPassw(rs.getString("claveencr"));
                beanEds.setCodcli(rs.getString("codcli"));
                beanEds.setNombre_cuenta(rs.getString("nombre_cuenta"));
                beanEds.setCedula_cuenta(rs.getString("cedula_cuenta"));
                beanEds.setCantidad(rs.getString("cantidad"));

                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String actualizarPropietarioEds(String nombreprop, String direccion, String correo, String nit, String representante, String usuario, String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_PROPIETARIO";

        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, nombreprop);
            st.setString(2, direccion);
            st.setString(3, correo);
            st.setString(4, nit);
            st.setString(5, representante);
            st.setString(6, usuario);
            st.setString(7, id);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarProveedor(String nombre, String usuario, String gcontribuyente,
            String aretenedor, String auretefunete, String auiva, String auica, String banco,
            String sedepago, String banagencia, String tcuenta, String ptransfer,
            String bancotransfer, String numcuenta, String hc, String suctransfer,
            String nitedit, String regimen, String nombre_cuenta, String cedula_cuenta) {

        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_PROVEEDOR";

        try {
            if (ptransfer.equals("2")) {
                ptransfer = "T";
            } else {
                ptransfer = "B";
            }
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, nombre);
            st.setString(2, banco);
            st.setString(3, banagencia);
            st.setString(4, sedepago);
            st.setString(5, usuario);
            st.setString(6, bancotransfer);
            st.setString(7, suctransfer);
            st.setString(8, tcuenta);
            st.setString(9, numcuenta);
            st.setString(10, gcontribuyente);
            st.setString(11, aretenedor);
            st.setString(12, auretefunete);
            st.setString(13, auiva);
            st.setString(14, auica);
            st.setString(15, hc);
            st.setString(16, cedula_cuenta);//malo debe tomar la cedula de la vista...
            st.setString(17, nombre_cuenta);//malo debe tomar el nombre de la vista...
            st.setString(18, ptransfer);
            st.setString(19, regimen);
            st.setString(20, nitedit);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarCliente(String nomcliente, String base, String nit, String cia, String hc, String direccion, String telefono, String nomcontacto, String telcontacto, String emailcontacto, String dircontacto, String rif, String ciudad, String pais, String usuario) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_CLIENTE";
        try {
            query = this.obtenerSQL(query);

            st = new StringStatement(query, true);
            st.setString(1, "A");
            st.setString(2, nomcliente);
            st.setString(3, base);
            st.setString(4, nit);
            st.setString(5, cia);
            st.setString(6, hc);
            st.setString(7, direccion);
            st.setString(8, telefono);
            st.setString(9, nomcontacto);
            st.setString(10, telcontacto);
            st.setString(11, emailcontacto);
            st.setString(12, dircontacto);
            st.setString(13, hc);
            st.setString(14, rif);
            st.setString(15, ciudad);
            st.setString(16, pais);
            st.setString(17, usuario);
            st.setString(18, nit);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarCliente(String nomcliente, String hc, String direccion, String telefono, String nomcontacto, String telcontacto, String emailcontacto, String dircontacto, String ciudad, String pais, String usuario, String codcli) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_CLIENTE";
        try {
            query = this.obtenerSQL(query);

            st = new StringStatement(query, true);
            st.setString(1, nomcliente);
            st.setString(2, hc);
            st.setString(3, direccion);
            st.setString(4, telefono);
            st.setString(5, nomcontacto);
            st.setString(6, telcontacto);
            st.setString(7, emailcontacto);
            st.setString(8, dircontacto);
            st.setString(9, hc);
            st.setString(10, ciudad);
            st.setString(11, pais);
            st.setString(12, usuario);
            st.setString(13, codcli);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cargarFechaCXPEds(String eds) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_FECHA_CXC_EDS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, eds);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("fecha_venta"), rs.getString("fecha_venta"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public ArrayList<EDSPropietarioBeans> CXPEds(String eds, String fechainicio, String fechafin, String cxp) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CXP_EDS";

        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            String consulta = "";
            con = this.conectarJNDI();
            String condicion = " veds.id_eds= '" + eds + "'";
            if (!cxp.equals("")) {
                condicion = condicion + " AND cxp.documento = '" + cxp + "'";
            } else {
                condicion = condicion + " AND veds.creation_date::date  between '" + fechainicio + "' AND '" + fechafin + "'";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();

            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setCxp_documento(rs.getString("documento"));
                beanEds.setCxp_proveedor(rs.getString("proveedor"));
                beanEds.setEdsnombre(rs.getString("nombre_proveedor"));
                beanEds.setHc(rs.getString("hc"));
                beanEds.setCxp_vlr_neto(rs.getString("vlr_neto"));
                beanEds.setCxp_vlr_total_abonos(rs.getString("vlr_total_abonos"));
                beanEds.setCxp_vlr_saldo(rs.getString("vlr_saldo"));
                beanEds.setFecha_creacion(rs.getString("creation_date"));
                beanEds.setUsuarios_creacion(rs.getString("creation_user"));
                beanEds.setCxp_estado(rs.getString("estado"));
                beanEds.setEdsid(rs.getString("id"));
                beanEds.setDocumentoajuste(rs.getString("cxp_nota") != null ? "<u style='color:blue'>" + rs.getString("cxp_nota") + "</u>" : "");

                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    public ArrayList<EDSPropietarioBeans> CXPEds_detalle(String cxp) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DETALLE_CXP_EDS";

        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cxp);
            rs = ps.executeQuery();

            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setCxp_documento(rs.getString("documento_cxp"));
                beanEds.setNum_venta(rs.getString("num_venta"));
                beanEds.setFecha_venta(rs.getString("fecha_venta"));
                beanEds.setPlanilla(rs.getString("planilla"));
                beanEds.setCedula(rs.getString("cedula"));
                beanEds.setConductor(rs.getString("conductor"));
                beanEds.setPlaca(rs.getString("placa"));
                beanEds.setProducto(rs.getString("producto"));
                beanEds.setPrecio_producto(rs.getString("precio_producto"));
                beanEds.setCantidad_suministrada(rs.getString("cantidad_suministrada"));
                beanEds.setUnidad_medida(rs.getString("unidad_medida"));
                beanEds.setSubtotal(rs.getString("subtotal"));
                beanEds.setComision(rs.getString("comision"));
                beanEds.setTotal(rs.getString("total"));
                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String actualizarProveedor(String nit, String nombre, String usuario) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_PROVEEDOR_EDS";

        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, nombre);
            st.setString(2, usuario);
            st.setString(3, nit);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String verificarusuarioEDS(String nit) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICAR_IDUSUARIO_EDS";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString(1);
            }
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
        }
        return respuesta;
    }

//    @Override
//    public String verificarusuarioPrp(String nitem) {
//        Connection con = null;
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        String query = "SQL_VERIFICAR_IDUSUARIO_PROP";
//        String respuesta = "";
//        try {
//            con = this.conectarJNDI();
//            ps = con.prepareStatement(this.obtenerSQL(query));
//            ps.setString(1, nitem);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                respuesta = rs.getString(1);
//            }
//        } catch (Exception ex) {
//            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
//        } finally {
//            try {
//                if (rs != null) {
//                    rs.close();
//                }
//                if (ps != null) {
//                    ps.close();
//                }
//                if (con != null) {
//                    this.desconectar(con);
//                }
//            } catch (SQLException e) {
//            }
//        }
//        return respuesta;
//    }
    @Override
    public ArrayList<EDSPropietarioBeans> verificarusuarioPrp(String nitem, String tipodocumento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICAR_IDUSUARIO_PROP";

        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
//            String consulta = "";
//            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nitem);
            ps.setString(2, nitem);
            ps.setString(3, tipodocumento);

            rs = ps.executeQuery();
            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setIdusuario(rs.getString("idusuario"));
                beanEds.setNombreprop(rs.getString("nombre"));
                beanEds.setTipodoc(rs.getString("tipo_doc"));
                beanEds.setTipopersona(rs.getString("clasificacion"));
                beanEds.setContribuyente(rs.getString("gran_contribuyente"));
                beanEds.setAutoretenedor(rs.getString("agente_retenedor"));
                beanEds.setRetefuente(rs.getString("autoret_rfte"));
                beanEds.setIva(rs.getString("autoret_iva"));
                beanEds.setIca(rs.getString("autoret_ica"));
                beanEds.setHc(rs.getString("hc"));
                beanEds.setCedula_cuenta(rs.getString("cedula_cuenta"));
                beanEds.setNombre_cuenta(rs.getString("nombre_cuenta"));
                beanEds.setNumcuenta(rs.getString("no_cuenta"));
                beanEds.setTipocuenta(rs.getString("tipo_cuenta"));
                beanEds.setBanco(rs.getString("branch_code"));
                beanEds.setAgencia(rs.getString("bank_account_no"));
                beanEds.setSedebanco(rs.getString("agency_id"));
                beanEds.setTipopago(rs.getString("tipo_pago"));
                beanEds.setBancotransfer(rs.getString("banco_transfer"));
                beanEds.setSucursaltransfer(rs.getString("suc_transfer"));
                beanEds.setExistente(rs.getString("cliente"));
                beanEds.setCodcli(rs.getString("codcli"));
                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String cargarComboEDS() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ESTACIONES_SERVICIO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre_eds"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public ArrayList<AuditoriaVentasBeans> auditoriaVentas(String eds, String fechainicio, String fechafin, String planilla) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_VENTAS";

        ArrayList<AuditoriaVentasBeans> lista = new ArrayList<>();
        try {
            String consulta;
            con = this.conectarJNDI();
            String condicion = "";
            if (!eds.equals("")) {
                condicion = condicion + " AND estacion.id = '" + eds + "'";
            }
            if (!planilla.equals("")) {
                condicion = condicion + " AND anticipo.planilla = '" + planilla + "'";
            }
            if ((!fechafin.equals("")) && (!fechainicio.equals(""))) {
                condicion = condicion + " and anticipo.fecha_envio_fintra::date  between '" + fechainicio + "' AND '" + fechafin + "'";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();

            while (rs.next()) {
                AuditoriaVentasBeans beanEds = new AuditoriaVentasBeans();
                beanEds.setId(rs.getString("id"));
                beanEds.setPeriodo(rs.getString("periodo"));
                beanEds.setPlanilla(rs.getString("planilla"));
                beanEds.setFecha_envio_fintra(rs.getString("fecha_envio_fintra"));
                beanEds.setDiferencia_fechas(rs.getString("diferencia_fechas"));
                beanEds.setOrigen(rs.getString("origen"));
                beanEds.setDestino(rs.getString("destino"));
                beanEds.setPeriodo(rs.getString("periodo"));
                beanEds.setValor_planilla(rs.getString("valor_planilla"));
                beanEds.setValor_neto_anticipo(rs.getString("valor_neto_anticipo"));
                beanEds.setValor_descuentos_fintra(rs.getString("valor_descuentos_fintra"));
                beanEds.setValor_desembolsar(rs.getString("valor_desembolsar"));
                beanEds.setReanticipo(rs.getString("reanticipo"));
                beanEds.setFecha_pago_fintra(rs.getString("fecha_pago_fintra"));
                beanEds.setFecha_corrida(rs.getString("fecha_corrida"));
                beanEds.setId_transportadoras(rs.getString("id_transportadoras"));
                beanEds.setTransportadora(rs.getString("transportadora"));
                beanEds.setId_agencias(rs.getString("id_agencias"));
                beanEds.setNombre_agencia(rs.getString("nombre_agencia"));
                beanEds.setId_conductors(rs.getString("id_conductors"));
                beanEds.setConductor(rs.getString("conductor"));
                beanEds.setCedula_conductor(rs.getString("cedula_conductor"));
                beanEds.setId_propietarios(rs.getString("id_propietarios"));
                beanEds.setCedula_propietario(rs.getString("cedula_propietario"));
                beanEds.setPropietario(rs.getString("propietario"));
                beanEds.setId_vehiculos(rs.getString("id_vehiculos"));
                beanEds.setPlaca(rs.getString("placa"));
                beanEds.setSucursal(rs.getString("sucursal"));
                beanEds.setNum_venta(rs.getString("num_venta") != null ? "<u style='color:blue'>" + rs.getString("num_venta") + "</u>" : "");
                //beanEds.setNum_venta(rs.getString("num_venta"));
                beanEds.setFecha_vent(rs.getString("fecha_venta"));
                beanEds.setNombre_eds(rs.getString("nombre_eds"));
                beanEds.setKilometraje(rs.getString("kilometraje"));
                beanEds.setCant_reg_ventas(rs.getString("cant_reg_ventas"));
                beanEds.setTotal_venta(rs.getString("total_venta"));
                beanEds.setValor_comision_fintra(rs.getString("valor_comision_fintra"));
                beanEds.setDisponible(rs.getString("disponible"));

                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public ArrayList<AuditoriaVentasBeans> auditoriaVentas(String numventa) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_VENTAS_DETALLE";

        ArrayList<AuditoriaVentasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numventa);
            rs = ps.executeQuery();

            while (rs.next()) {
                AuditoriaVentasBeans beanEds = new AuditoriaVentasBeans();
                beanEds.setNum_venta(rs.getString("num_venta"));
                beanEds.setFecha_vent(rs.getString("fecha_venta"));
                beanEds.setProducto(rs.getString("producto"));
                beanEds.setKilometraje(rs.getString("kilometraje"));
                beanEds.setPrecio_xproducto(rs.getString("precio_xproducto"));
                beanEds.setCantidad_suministrada(rs.getString("cantidad_suministrada"));
                beanEds.setTotal_venta(rs.getString("total_venta"));

                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public ArrayList<EDSPropietarioBeans> cargarNotaCredito(String cxp, String numero_nota,String creation_date,int id_eds) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOTA_CREDITO";

        ArrayList<EDSPropietarioBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cxp);
            ps.setString(2, creation_date);
            ps.setString(3, creation_date);
            ps.setString(4, creation_date);
            ps.setInt(5, id_eds);
            ps.setString(6, numero_nota);
            rs = ps.executeQuery();

            while (rs.next()) {
                EDSPropietarioBeans beanEds = new EDSPropietarioBeans();
                beanEds.setNc_estado(rs.getString("estado"));
                beanEds.setNc_documento(rs.getString("documento"));
                beanEds.setTipo_documento_con(rs.getString("tipo_documento"));
                beanEds.setNc_proveedor(rs.getString("proveedor"));
                beanEds.setEdsnombre(rs.getString("nombre_proveedor"));
                beanEds.setHc(rs.getString("hc"));
                beanEds.setBase_numgalon_totalventa(rs.getString("base"));
                beanEds.setFactor_descuento(rs.getString("factor_descuento"));
                beanEds.setNc_vlr_neto(rs.getString("vlr_neto"));
                beanEds.setNc_vlr_total_abonos(rs.getString("vlr_total_abonos"));
                beanEds.setNc_valor_saldo(rs.getString("vlr_saldo"));
                beanEds.setFecha_creacion(rs.getString("creation_date"));
                beanEds.setUsuarios_creacion(rs.getString("creation_user"));
                lista.add(beanEds);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String actualizarCliente(String nomcliente, String direccion, String telefono, String ciudad, String pais, String usuario, String codcli) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_CLIENTE_2";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, nomcliente);
            st.setString(2, direccion);
            st.setString(3, telefono);
            st.setString(4, ciudad);
            st.setString(5, pais);
            st.setString(6, usuario);
            st.setString(7, codcli);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public ArrayList<reporteColocacionBeans> reporteColocacion(String planilla, String fechainicio, String fechafin, String eds) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_REPORTE_COLOCACION";

        ArrayList<reporteColocacionBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, planilla);
            ps.setString(2, fechainicio);
            ps.setString(3, fechafin);
            if (eds.equals("")) {
                ps.setInt(4, Integer.parseInt("0"));
            } else {
                ps.setInt(4, Integer.parseInt(eds));
            }

            rs = ps.executeQuery();

            while (rs.next()) {
                reporteColocacionBeans beancolocacion = new reporteColocacionBeans();
                beancolocacion.setId(rs.getString("id"));
                beancolocacion.setPeriodo(rs.getString("periodo"));
                beancolocacion.setTransportadora(rs.getString("transportadora"));
                beancolocacion.setNombre_agencia(rs.getString("nombre_agencia"));
                beancolocacion.setCedula_propietario(rs.getString("cedula_propietario"));
                beancolocacion.setPropietario(rs.getString("propietario"));
                beancolocacion.setPlaca(rs.getString("placa"));
                beancolocacion.setCedula_conductor(rs.getString("cedula_conductor"));
                beancolocacion.setConductor(rs.getString("conductor"));
                beancolocacion.setSucursal(rs.getString("sucursal"));
                beancolocacion.setOrigen(rs.getString("origen"));
                beancolocacion.setDestino(rs.getString("destino"));
                beancolocacion.setPlanilla(rs.getString("planilla"));
                beancolocacion.setFecha_venta(rs.getString("fecha_venta"));
                beancolocacion.setFecha_anticipo(rs.getString("fecha_anticipo"));
                beancolocacion.setTiempo_legalizacion(rs.getString("tiempo_legalizacion"));
                beancolocacion.setValor_anticipo(rs.getString("valor_anticipo"));
                beancolocacion.setDescuento_fintra(rs.getString("descuentos_fintra"));
                beancolocacion.setValor_consignado(rs.getString("valor_consignacion"));
                beancolocacion.setReanticipo(rs.getString("reanticipo"));
                beancolocacion.setNumero_venta(rs.getString("num_venta"));
                beancolocacion.setNombre_eds(rs.getString("nombre_eds"));
                beancolocacion.setKilometraje(rs.getString("kilometraje"));
                beancolocacion.setProducto(rs.getString("producto"));
                beancolocacion.setPrecioxunidad(rs.getString("precioxunidad"));
                beancolocacion.setCantidad_suministrada(rs.getString("cantidad_suministrada"));
                beancolocacion.setTotal_ventas(rs.getString("total_venta"));
                beancolocacion.setDisponible(rs.getString("disponible"));

                lista.add(beancolocacion);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

}
