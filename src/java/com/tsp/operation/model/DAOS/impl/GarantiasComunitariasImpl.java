/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lowagie.text.BadElementException;
import com.tsp.operation.model.DAOS.GarantiasComunitariasDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.ParametrosBeans;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.operation.model.TransaccionService;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import com.tsp.operation.model.beans.EstadoCuentaIngresos;
import java.util.ArrayList;

/**
 *
 * @author mcastillo
 */
public class GarantiasComunitariasImpl extends MainDAO implements GarantiasComunitariasDAO{
    
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;
    
    public GarantiasComunitariasImpl(String dataBaseName) {
        super("GarantiasComunitariasDAO.xml", dataBaseName);
    }

    @Override
    public String listarTipoDocumentos(String mostrarTodos) {
        con = null;
        rs = null;
        ps = null;
      
        query = "SQL_GET_TIPO_DOCUMENTOS";
        String respuestaJson = "{}";
        String filtro = (mostrarTodos.equals("S"))?"":" WHERE reg_status = ''";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();       
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));    
                fila.addProperty("nombre", rs.getString("nombre"));  
                fila.addProperty("descripcion", rs.getString("descripcion"));  
                fila.addProperty("reg_status", rs.getString("reg_status")); 
                fila.addProperty("cambio", rs.getString("cambio")); 
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

   @Override
    public String cargarConfigDocs() {
        con = null;
        rs = null;
        ps = null;
      
        query = "SQL_GET_CONFIG_DOCS_GARANTIAS_COMUNITARIAS";
        String respuestaJson = "{}";

        Gson gson = new Gson();       
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", "");

            ps = con.prepareStatement(query);
          
            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("tipo_doc"), rs.getString("info"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    public boolean existeConfigDoc(String id_tipo_doc) {
        con = null;
        rs = null;
        ps = null;
      
        query = "SQL_GET_CONFIG_DOCS_GARANTIAS_COMUNITARIAS";       
        String filtro = " AND id_tipo_doc="+id_tipo_doc;
        boolean resp = false;

        Gson gson = new Gson();       
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
          
            rs = ps.executeQuery();         
            while (rs.next()) {
               resp = true;
            }
            

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }
    
    @Override
    public String guardarConfigDocs(String tipo_doc, String content, Usuario usuario) {
        con = null;
        ps = null;
        query = (existeConfigDoc(tipo_doc))?"actualizarConfigDoc":"insertarConfigDoc";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, content);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(tipo_doc));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {          
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarConfigDocs \n" + e.getMessage());          
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }
    
    @Override
    public String cargarEquivalencias() {
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
       
        Gson gson = new Gson();
        query = "cargarEquivalencias";
        String respuestaJson="{}";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);           
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = null;
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("codparam", rs.getString("codparam"));
                fila.addProperty("descripcion", rs.getString("descripcion")); 
                fila.addProperty("procedencia", rs.getString("procedencia"));
                datos.add(fila);
            }
           obj.add("rows", datos);
           respuestaJson = gson.toJson(obj);
        }catch (Exception e) {
             respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }           
        }
         return respuestaJson;
    }
    
    @Override
    public String cargarComboUnidad() {
        con = null;
        ps = null;
        rs = null;
        query = "SQL_CARGAR_COMBO_UNIDADES";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
    
    @Override
    public String cargarCboEmpresasFianza() {
        con = null;
        ps = null;
        rs = null;
        query = "SQL_CARGAR_COMBO_EMRPESAS_FIANZA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("nit"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
    
    @Override
    public String cargarCboVencimientos() {
        con = null;
        ps = null;
        rs = null;
        query = "SQL_CARGAR_COMBO_VENCIMIENTOS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
        
    
    @Override
    public String cargarConfigFactor() {
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        query = "cargarConfigFactor";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));              
                fila.addProperty("numero_convenio", rs.getString("numero_convenio"));
                fila.addProperty("id_unidad_negocio", rs.getString("id_unidad_negocio"));
                fila.addProperty("unidad_negocio", rs.getString("unidad_negocio"));
                fila.addProperty("nit_empresa", rs.getString("nit_empresa"));
                fila.addProperty("nombre_empresa", rs.getString("nombre_empresa"));
                fila.addProperty("plazo_inicial", rs.getString("plazo_inicial"));
                fila.addProperty("plazo_final", rs.getString("plazo_final"));
                fila.addProperty("porcentaje_comision", rs.getString("porcentaje_comision"));
                fila.addProperty("valor_comision", rs.getString("valor_comision"));
                fila.addProperty("porcentaje_iva", rs.getString("porcentaje_iva"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                fila.addProperty("financiado", rs.getString("financiado").equals("S") ? "Si" : "No");
                fila.addProperty("porcentaje_aval", rs.getString("porcentaje_aval_clie"));
                fila.addProperty("porcentaje_aval_finan", rs.getString("porcentaje_aval_finan"));
                fila.addProperty("producto", rs.getString("producto"));
                fila.addProperty("id_producto", rs.getString("id_producto"));
                fila.addProperty("cobertura", rs.getString("cobertura"));
                fila.addProperty("id_cobertura", rs.getString("id_cobertura"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }
    
    @Override
    public boolean existePlazoFactor(String id_unidad, String plazo_inicial, String plazo_final,String nit_empresa_fianza,String producto,String cobertura) {
        con = null; rs = null; ps = null;
        boolean resp = false;
        query = "SQL_EXISTE_PLAZO_FACTOR";
        String filtro="";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query); 
            ps.setInt(1, Integer.parseInt(id_unidad));
            ps.setInt(2, Integer.parseInt(plazo_inicial));
            ps.setInt(3, Integer.parseInt(plazo_final));
            ps.setString(4, nit_empresa_fianza);
            ps.setString(5, producto);
            ps.setString(6, cobertura);
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {            
            System.out.println("Error en existePlazoFactor: " + e.toString());
            e.printStackTrace();    
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return resp;
    }
    
    @Override
    public boolean existePlazoFactor(String id, String id_unidad, String plazo_inicial, String plazo_final,String nit_empresa_fianza,String producto,String cobertura) {
        con = null; rs = null; ps = null;
        boolean resp = false;
        query = "SQL_EXISTE_PLAZO_FACTOR";
        String filtro=" AND id not in("+id+")";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query); 
            ps.setInt(1, Integer.parseInt(id_unidad));
            ps.setInt(2, Integer.parseInt(plazo_inicial));
            ps.setInt(3, Integer.parseInt(plazo_final));
            ps.setString(4, nit_empresa_fianza);
            ps.setString(5, producto);
            ps.setString(6, cobertura);
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {            
            System.out.println("Error en existePlazoFactor: " + e.toString());
            e.printStackTrace();    
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return resp;
    }   
   
    @Override
    public String guardarConfigFactor(String codigo_convenio, String id_unidad, String nit_empresa, String plazo_inicial, String plazo_final, String porc_comision, String valor_comision, String porc_iva, Usuario usuario, String financiado,String producto, String cobertura) {
        con = null;
        ps = null;
        query = "guardarConfigFactor";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);    
            ps.setString(1, codigo_convenio);      
            ps.setInt(2, Integer.parseInt(id_unidad));
            ps.setString(3, nit_empresa);          
            ps.setInt(4, Integer.parseInt(plazo_inicial));
            ps.setInt(5, Integer.parseInt(plazo_final));
            ps.setDouble(6, Double.parseDouble(porc_comision));
            ps.setDouble(7, Double.parseDouble(valor_comision));  
            ps.setDouble(8, Double.parseDouble(porc_iva));
            ps.setString(9, usuario.getLogin());          
            ps.setString(10, usuario.getDstrct());
            ps.setString(11, financiado.equals("true") ? "S": "N");
            ps.setString(12, producto); 
            ps.setString(13, cobertura); 

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarConfigFactor \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarConfigFactor(String id, String codigo_convenio, String id_unidad, String nit_empresa, String plazo_inicial, String plazo_final, String porc_comision, String valor_comision, String porc_iva, Usuario usuario, String financiado, String porcentaje_aval, String porcentaje_aval_finan,String producto, String cobertura) {
        con = null;
        ps = null;
        query = "actualizarConfigFactor";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, codigo_convenio);
            ps.setInt(2, Integer.parseInt(id_unidad));
            ps.setString(3, nit_empresa);
            ps.setInt(4, Integer.parseInt(plazo_inicial));
            ps.setInt(5, Integer.parseInt(plazo_final));
            ps.setDouble(6, Double.parseDouble(porc_comision));
            ps.setDouble(7, Double.parseDouble(valor_comision));  
            ps.setDouble(8, Double.parseDouble(porc_iva));
            ps.setDouble(9, Double.parseDouble(porcentaje_aval));
            ps.setDouble(10, Double.parseDouble(porcentaje_aval_finan));
            ps.setString(11, usuario.getLogin());          
            ps.setString(12, financiado.equals("true") ? "S": "N");           
            ps.setString(13, producto);
            ps.setString(14, cobertura);
             ps.setString(15, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarConfigFactor \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaConfigFactor(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarConfigFactor";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activaInactivaConfigFactor \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String buscarNegociosProcesarFianza(String id_unidad, String nit_empresa, String periodo,String agencia) {
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        query = "SQL_OBTENER_NEGOCIOS_PROCESAR_FIANZA";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_unidad);
            ps.setString(2, periodo);
            ps.setString(3, nit_empresa);
            ps.setString(4, agencia);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));              
                fila.addProperty("nit_cliente", rs.getString("nit_cliente"));
                fila.addProperty("nombre_cliente", rs.getString("nombre_cliente"));
                fila.addProperty("documento_rel", rs.getString("documento_relacionado"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("num_cuotas", rs.getString("plazo"));
                fila.addProperty("id_convenio", rs.getString("id_convenio"));
                fila.addProperty("fecha_vencimiento", rs.getString("fecha_vencimiento"));
                fila.addProperty("valor_negocio", rs.getString("valor_negocio"));
                fila.addProperty("valor_desembolsado", rs.getString("valor_desembolsado"));
                fila.addProperty("valor_fianza", rs.getString("valor_fianza"));              
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
        
    }
    
     @Override
    public String generarCxPFianza(String id_unidad,String agencia, String nit_empresa, String periodo, String[] x, String valor_cxp, String usuario) {
        con = null;
        ps = null;
        rs = null;
        
        if (id_unidad.equals("30") || id_unidad.equals("31")){
         query = "SQL_GENERAR_CXP_FIANZA_DEFINITIVA_FINTRA";
        }else{
        query = "SQL_GENERAR_CXP_FIANZA_DEFINITIVA";
        }
        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_unidad));
            ps.setString(2, nit_empresa); 
            ps.setString(3, periodo);
            ps.setArray(4, con.createArrayOf("text", x));
            ps.setDouble(5, Double.parseDouble(valor_cxp));
            ps.setString(6, usuario);
            ps.setString(7, agencia);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("retorno");
            }

        } catch (Exception e) {
            retorno = "ERROR";
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return retorno;
    
    }
    
    @Override
    public String getSelectDataBaseJson(String query, ParametrosBeans parametrosBeans, Usuario user) {
        con = null;
        ps = null;
        rs = null;

        String respuestaJson = "{}";
        String filtro = "", filtro_monto="";
        JsonArray lista = null;
        //JsonObject obj = new JsonObject();
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query, user.getBd());

            if (con != null) {

                switch (query) {
                    
                    case "BUSCAR_FACTURAS_INDEMNIZACION_MICRO":
                    case "BUSCAR_FACTURAS_INDEMNIZACION_MICRO_AP":
                    case "BUSCAR_FACTURAS_INDEMNIZACION_EDU_CONS":
                    case "BUSCAR_FACTURAS_INDEMNIZACION_EDU_CONS_AP":
                        
                        if (!parametrosBeans.getStringParameter5().equals("")) {
                            filtro += " AND foto.nit = '"+parametrosBeans.getStringParameter5()+"'";
                        }
                        
                        if (!parametrosBeans.getStringParameter6().equals("")) {
                            filtro += " AND foto.negasoc = '"+parametrosBeans.getStringParameter6()+"'";
                        }
                                                                 
                        if (!parametrosBeans.getStringParameter7().equals("") && !parametrosBeans.getStringParameter8().equals("")) {
                            filtro += " AND foto.valor_factura BETWEEN "+Double.parseDouble(parametrosBeans.getStringParameter7())+" AND "+ Double.parseDouble(parametrosBeans.getStringParameter8());
                        }
                        
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setString(1, parametrosBeans.getStringParameter1());
                        ps.setInt(2, Integer.parseInt(parametrosBeans.getStringParameter2()));
                        ps.setString(3, parametrosBeans.getStringParameter3());
                        ps.setInt(4, Integer.parseInt(parametrosBeans.getStringParameter4()));
                        ps.setString(5, filtro);
                        ps.setBoolean(6, Boolean.parseBoolean(parametrosBeans.getStringParameter9()));
                        ps.setBoolean(7, Boolean.parseBoolean(parametrosBeans.getStringParameter10()));
                        ps.setBoolean(8, Boolean.parseBoolean(parametrosBeans.getStringParameter11()));
                        break;
                        
                    case "FACTURAS_POR_DESISTIR":
                        
                        query = "FACTURAS_POR_DESISTIR";
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setString(1, parametrosBeans.getStringParameter1());
                        ps.setString(2, parametrosBeans.getStringParameter2());
                        break;    
                        
                    case "VER_FACTURAS_INDENIZADAS":
                        
                        query = "VER_FACTURAS_INDENIZADAS";
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        break;
                 
                }
                
                rs = ps.executeQuery();
                
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }
                //obj.add("rows", lista);
                respuestaJson = new Gson().toJson(lista);
            }
        } catch (SQLException e) {
            lista = null;
            respuestaJson = "{\"error\":\"Lo sentimosXX algo salio mal al carga la informacion.\","
                    + "\"exception\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String getInsertDataBaseJson(String query, String empresa_fianza, JsonObject objects, Usuario user) {
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            StringStatement st = null;
            st = new StringStatement(sql, true);
            st.setString(1, objects.get("periodo_foto").getAsString());
            st.setString(2, empresa_fianza);
            st.setString(3, objects.get("codcli").getAsString());
            st.setString(4, objects.get("nit_cliente").getAsString());
            st.setString(5, objects.get("nombre_cliente").getAsString());
            st.setString(6, objects.get("negocio").getAsString());
            st.setString(7, objects.get("num_pagare").getAsString());
            st.setString(8, objects.get("documento").getAsString());
            st.setString(9, objects.get("cuota").getAsString());
            st.setString(10, objects.get("fecha_vencimiento").getAsString());
            st.setString(11, objects.get("altura_mora").getAsString());
            st.setInt(12, objects.get("dias_mora").getAsInt());
            st.setDouble(13, objects.get("valor_factura").getAsDouble());
            st.setDouble(14, objects.get("valor_saldo_capital").getAsDouble());
            st.setDouble(15, objects.get("valor_saldo_mi").getAsDouble());
            st.setDouble(16, objects.get("valor_saldo_ca").getAsDouble());
            st.setDouble(17, objects.get("ixm").getAsDouble());
            st.setDouble(18, objects.get("gac").getAsDouble());
            st.setDouble(19, objects.get("total_saldo").getAsDouble()); 
            st.setInt(20, objects.get("convenio").getAsInt());
            st.setString(21, objects.get("nombre_linea_negocio").getAsString());
            st.setString(22, objects.get("cartera_en").getAsString());
            st.setString(23, user.getLogin());
            sql = st.getSql();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(GarantiasComunitariasImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }
       
    @Override
    public String crearCxCIndemnizacionFianza(boolean acelerarPagare, Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "SQL_CREAR_CXC_INDEMNIZACION";
        String retorno = "";
        try {
            con = this.conectarJNDI(query, user.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setBoolean(1, acelerarPagare);
                ps.setString(2, user.getLogin());
                rs = ps.executeQuery();
                while (rs.next()) {
                    retorno = rs.getString("retorno");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            this.deleteDocumentsCFianza(user);
            retorno = "Lo sentimos error al crear el comprobante de indemnizacion.";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return retorno;
        }
    }
    
    @Override
    public String crearICIndemnizacionFianza(boolean acelerarPagare, Usuario user) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CREAR_IC_INDEMNIZACION";
        String retorno = "";
        try {
            con = this.conectarJNDI(query, user.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setBoolean(1, acelerarPagare);
                ps.setString(2, user.getLogin());
                rs = ps.executeQuery();
                while (rs.next()) {
                    retorno = rs.getString("retorno");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            this.deleteDocumentsCFianza(user);
            retorno = "Lo sentimos error al crear IC.";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return retorno;
        }
    }
    
    @Override
    public String getInsertFacturasXindemnizarMicro(String query,JsonObject objects, String empresa_fianza, Usuario user) {
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            StringStatement st = null;
            st = new StringStatement(sql, true);
            st.setString(1, objects.get("periodo_foto").getAsString());
            st.setString(2, empresa_fianza);
            st.setString(3, objects.get("periodo_foto").getAsString());
            st.setString(4, objects.get("periodo_foto").getAsString());
            st.setString(5, objects.get("periodo_foto").getAsString());
            st.setString(6, objects.get("periodo_foto").getAsString());           
            st.setDouble(7, objects.get("ixm").getAsDouble());
            st.setDouble(8, objects.get("gac").getAsDouble());
            st.setString(9, user.getLogin());
            st.setString(10, objects.get("negocio").getAsString());
          
            sql = st.getSql();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(GarantiasComunitariasImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }
    
      /**
     * Metodo que borra las registro insertados el la tabla control
     * indemniacion.
     *
     * @param user
     * @return
     */
    private void deleteDocumentsCFianza(Usuario user) {
        Connection con = null;
        String query = "SQL_CREAR_CXC_INDEMNIZACION";

        try {

            con = this.conectarJNDI(query, user.getBd());
            con.setAutoCommit(false);
            Statement stmt = con.createStatement();

            stmt.addBatch("DELETE  FROM administrativo.control_indemnizacion_fianza  "
                    + "WHERE creation_user='" + user.getLogin() + "' AND estado_proceso='' "
                    + "AND creation_date::DATE=now()::DATE ");

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();

        } catch (SQLException ex) {
            try {
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                this.desconectar(con);
            } catch (SQLException e) {

            }

        }

    }
    
    @Override
    public String getInsertFacturaXdesistir(String query, JsonObject objects ,Usuario user) {
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            StringStatement st = null;
            st = new StringStatement(sql, true);
            st.setString(1, objects.get("periodo_foto").getAsString());
            st.setString(2, objects.get("negocio").getAsString());
            st.setString(3, objects.get("documento").getAsString());
            st.setDouble(4, objects.get("valor_desistir").getAsDouble());
            st.setDouble(5, objects.get("cuenta").getAsDouble());
            st.setString(6, objects.get("ref_4").getAsString());
            st.setString(7, objects.get("cartera_en").getAsString());
            st.setString(8, user.getLogin());
            sql = st.getSql();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(GarantiasComunitariasImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }
    
     @Override
    public String crearComprobanteDiarioDesistimiento(Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "SQL_CREAR_CDIAR_DESISTIMIENTO";
        String retorno = "";
        try {
            con = this.conectarJNDI(query, user.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, user.getLogin());
                rs = ps.executeQuery();
                while (rs.next()) {
                    retorno = rs.getString("retorno");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            this.deleteDocumentsCFianza(user);
            retorno = "Lo sentimos error al crear el comprobante de desistimiento.";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return retorno;
        }
    }
    
    @Override
    public String generarCxCGarantias(boolean acelerar_pagare, String numCxC, String ruta, String periodoCorte, String diasMora, String usuario) {
        con = null;
        ps = null;
        rs = null;      
        String sql = "";    
        query =  "obtenerInfoCxCGarantias";
        String nomarchivo = "";      
        String html_doc="", fecha_corte="";
        ResourceBundle rb = null;
        String  respuestaJson = "{}";
        try{        
           DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
	   //get current date time with Date()
	   Date date = new Date();
           nomarchivo = "cxc_garantias_comunitarias_"+dateFormat.format(date)+".pdf";
	   //System.out.println(dateFormat.format(date));
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");           
            Document document = new Document(PageSize.LEGAL, 85, 85, 72, 52);
           
            HTMLWorker htmlWorker = new HTMLWorker(document);           
        
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(ruta+"/"+nomarchivo));  
            
            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
            
            Font f2 = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL);
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
            
            Font f4 = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
          
            HeaderFooterCxC header = new HeaderFooterCxC(rb);
            pdfWriter.setPageEvent(header);

            document.open();
            document.addAuthor(usuario);
            document.addCreator("FINTRA S.A.");
            document.addCreationDate();
            document.addTitle("Cuenta de Cobro Garant�as");
            
         
            
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, periodoCorte);
            ps.setString(2, periodoCorte);
            ps.setString(3, periodoCorte);
            ps.setString(4, periodoCorte);
            ps.setString(5, periodoCorte);
            ps.setString(6, periodoCorte);
        
            rs = ps.executeQuery();

            while (rs.next()) {              
                html_doc = rs.getString("info");      
                fecha_corte = rs.getString("fecha_corte");      
            }
            
            htmlWorker.parse(new StringReader("<div>" + html_doc.replaceAll("P1", fecha_corte).replaceAll("P2","  "+ diasMora+" d�as") + "</div>"));     
                      
                         
            PdfPTable table = new PdfPTable(5);
            table.setWidths(new int[]{ 2, 4, 6, 5, 4 });
            table.setWidthPercentage(100);
            PdfPCell cell;
            //ENCABEZADOS DE LA TABLA
             // row 1, cell 1
            cell = new PdfPCell(new Phrase("No", f3));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            
            // row 1, cell 2
            cell = new PdfPCell(new Phrase("IDENTIFICACION", f3));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            
            // row 1, cell 3
            cell = new PdfPCell(new Phrase("NOMBRE CLIENTE", f3));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            //cell.setBorder(Rectangle.NO_BORDER);              
            table.addCell(cell);     
           
            // row 1, cell 4
            cell = new PdfPCell(new Phrase("VALOR A INDEMNIZAR", f3));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            
            // row 1, cell 5
            cell = new PdfPCell(new Phrase("LINEA", f3));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            DecimalFormat formato = new DecimalFormat("$###,###,###.00");  
            int secuencia = 0;
            double total_indemnizado = 0;
            query = this.obtenerSQL("obtenerInfoDetalleCxCGarantias");
            ps = con.prepareStatement(query);
            ps.setBoolean(1, acelerar_pagare);
            ps.setBoolean(2, acelerar_pagare);
            ps.setString(3, numCxC);
            ps.setString(4, usuario);            
            rs = ps.executeQuery();
            
            int i = 0;
            while (rs.next()) {  
                secuencia = i+1;               
                String cedula = rs.getString("nit");
                String nombre = rs.getString("nombre_cliente");              
                Double valor_indemnizar = rs.getDouble("valor_indemnizado");
                String linea_negocio = rs.getString("linea_negocio");
                
                // row 1, cell 1
                cell = new PdfPCell(new Phrase(String.valueOf(secuencia), f2));               
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);      
                // row 1, cell 2
                cell = new PdfPCell(new Phrase(cedula, f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);                          
                table.addCell(cell);
                // row 1, cell 3
                cell = new PdfPCell(new Phrase(nombre, f2));               
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                // row 1, cell 4
                cell = new PdfPCell(new Phrase(formato.format(valor_indemnizar), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);            
                table.addCell(cell);
                // row 1, cell 5
                cell = new PdfPCell(new Phrase(linea_negocio, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);            
                table.addCell(cell);
                
                total_indemnizado = total_indemnizado +valor_indemnizar;
            }
           
            /*for (int i = 0; i < jsonArrNeg.size(); i++) {
                secuencia = i+1;               
                String cedula = jsonArrNeg.get(i).getAsJsonObject().get("nit_cliente").getAsJsonPrimitive().getAsString();
                String nombre = jsonArrNeg.get(i).getAsJsonObject().get("nombre_cliente").getAsJsonPrimitive().getAsString();              
                Double valor_indemnizar = jsonArrNeg.get(i).getAsJsonObject().get("valor_indemnizado").getAsJsonPrimitive().getAsDouble();
                String linea_negocio = jsonArrNeg.get(i).getAsJsonObject().get("nombre_linea_negocio").getAsJsonPrimitive().getAsString();
                
                // row 1, cell 1
                cell = new PdfPCell(new Phrase(String.valueOf(secuencia), f2));               
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);      
                // row 1, cell 2
                cell = new PdfPCell(new Phrase(cedula, f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);                          
                table.addCell(cell);
                // row 1, cell 3
                cell = new PdfPCell(new Phrase(nombre, f2));               
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                // row 1, cell 4
                cell = new PdfPCell(new Phrase(formato.format(valor_indemnizar), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);            
                table.addCell(cell);
                // row 1, cell 5
                cell = new PdfPCell(new Phrase(linea_negocio, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);            
                table.addCell(cell);
                
                total_indemnizado = total_indemnizado +valor_indemnizar;            
                    
           }*/
            
         // row 1, cell 1
        cell = new PdfPCell(new Phrase(String.valueOf(secuencia+1), f2));               
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell);      
        // row 1, cell 2
        cell = new PdfPCell(new Phrase("", f2));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);                          
        table.addCell(cell);
        // row 1, cell 3
        cell = new PdfPCell(new Phrase("Total:", f3));               
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell);
        // row 1, cell 4
        cell = new PdfPCell(new Phrase(formato.format(total_indemnizado), f2));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);            
        table.addCell(cell);
        // row 1, cell 5
        cell = new PdfPCell(new Phrase("", f2));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);            
        table.addCell(cell);

        document.add(table);   
        
        PdfPTable table1 = new PdfPTable(2); 
        table1.setWidthPercentage(100);
        table1.addCell(getCell("_________________________", PdfPCell.ALIGN_LEFT, f4));
        table1.addCell(getCell("___________________________", PdfPCell.ALIGN_RIGHT, f4));    
        
        PdfPTable table2 = new PdfPTable(2);
        table2.setSpacingBefore(5f);
        table2.setWidthPercentage(100);
        table2.addCell(getCell("FINTRA S.A.", PdfPCell.ALIGN_LEFT, f4));
        table2.addCell(getCell("GARANTIAS COMUNITARIAS", PdfPCell.ALIGN_RIGHT, f4));
        
        PdfPTable table3 = new PdfPTable(2);
        table3.setSpacingBefore(5f);
        table3.setWidthPercentage(100);
        table3.addCell(getCell("NIT. _____________________", PdfPCell.ALIGN_LEFT, f4));
        table3.addCell(getCell("NIT. _______________________", PdfPCell.ALIGN_RIGHT, f4));         
            
        document.add(new Paragraph("\n\n")); 
        document.add(table1);          
        document.add(table2); 
        document.add(table3);      
        // step 5
        document.close();      
                
        respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\""+"/images/multiservicios/" + usuario + "/"+nomarchivo+"\"}";     

        }catch (Exception e) {
            try {             
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR generarCxCGarantias \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(GarantiasComunitariasImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }           
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }   
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }      
            return respuestaJson;
       }
    }
    
    @Override
    public String generarNotificacionCliente(String nit_cliente, String negocio, String mora, String rutaOrigen, Usuario user) {
        con = null;
        ps = null;
        rs = null;
        query = "SQL_GET_INFO_NOTIFICACION";
        String nomarchivo = "", htmldoc = "";
        ResourceBundle rb = null;
        String respuestaJson = "{}";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            //get current date time with Date()
            Date date = new Date();
            nomarchivo = "notificacion_cliente_" + dateFormat.format(date) + ".pdf";

            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            Document document = new Document(PageSize.LEGAL, 85, 85, 72, 52);

            HTMLWorker htmlWorker = new HTMLWorker(document);

            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(rutaOrigen + "/" + nomarchivo));

            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);

            Font f2 = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL);
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);

            HeaderFooterCxC header = new HeaderFooterCxC(rb);
            pdfWriter.setPageEvent(header);

            document.open();
            document.addAuthor(user.getLogin());
            document.addCreator("FINTRA S.A.");
            document.addCreationDate();
            document.addTitle("Notificaci�n Cliente Garantias");
                           
            String[] x = new String[] {nit_cliente, negocio, mora};         

            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, "2");
            ps.setArray(2, con.createArrayOf("text", x));           

            rs = ps.executeQuery();

            while (rs.next()) {
                htmldoc = rs.getString("info");
            }

            htmlWorker.parse(new StringReader("<div style='top=100px'>" + htmldoc + "</div>"));
            document.close();          

            respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\"" + "/images/multiservicios/" + user.getLogin() + "/" + nomarchivo + "\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR generarNotificacionCliente \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    
    @Override
    public String cargarResumenIndemnizados(String periodo) {
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        query = "SQL_REPORTE_RESUMEN_INDEMNIZADOS";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, periodo);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();          
                fila.addProperty("negocios_indemnizados", rs.getString("negocios_indemnizados"));
                fila.addProperty("valor_indemnizado", rs.getString("valor_indemnizado"));
                fila.addProperty("linea_negocio", rs.getString("linea_negocio"));             
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }
     
    @Override
    public String cargarReportesFianza(String query, String id_unidad, String empresa_fianza, String periodo, Usuario usuario) {
        con = null;
        ps = null;
        rs = null;        

        String respuestaJson = "{}";
        String filtro = "";
        JsonArray lista = null;
        JsonObject obj = new JsonObject();
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query, usuario.getBd());

            if (con != null) {

                switch (query) {
             
                    case "SQL_REPORTE_CARTERA_FIANZA":
                        switch (id_unidad) {
                            case "1":
                                query = query + "_MICRO_CREDITO";
                                break;
                            case "22":
                                query = query + "_LIBRANZA";
                                break;
                        }
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setInt(1, Integer.parseInt(id_unidad));
                        //ps.setString(2,periodo);
                        ps.setString(2, empresa_fianza);
                        break;

                    case "SQL_REPORTE_OPERACIONES_DESEMBOLSADAS_FIANZA":
                        
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setString(1,id_unidad);
                        ps.setString(2,periodo);
                        ps.setString(3, empresa_fianza);
                        break;          
                        
                    case "SQL_REPORTE_RECLAMACIONES_FIANZA":
                        
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setString(1,id_unidad);
                        ps.setString(2,periodo);
                        ps.setString(3, empresa_fianza);
                        break;          
                        
                    case "SQL_REPORTE_PREPAGOS_FIANZA":
                        
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setString(1,id_unidad);
                        ps.setString(2,periodo);
                        ps.setString(3, empresa_fianza);
                        break;          
                 
                }
                
                rs = ps.executeQuery();
                
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    
                    lista.add(jsonObject);
                }
                obj.add("rows", lista);
                respuestaJson = new Gson().toJson(obj);
            }
        } catch (SQLException e) {
            lista = null;
            respuestaJson = "{\"error\":\"Lo sentimosXX algo salio mal al carga la informacion.\","
                    + "\"exception\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarReporteReclamaciones() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String cargarReportePrepagos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String cargarReporteRecuperaciones() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String cargarDetalleFacturasNegocio(String negocio,ParametrosBeans parametrosBeans,String query) {
        con = null;
        ps = null;
        rs = null;        

        String respuestaJson = "{}";  
        JsonArray lista = null;
        JsonObject obj = new JsonObject();
        JsonObject jsonObject = null;
        try {
            
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            ps.setString(2, parametrosBeans.getStringParameter1());
            ps.setInt(3, Integer.parseInt(parametrosBeans.getStringParameter2()));
            ps.setString(4, parametrosBeans.getStringParameter3());
            ps.setInt(5, Integer.parseInt(parametrosBeans.getStringParameter4()));
            ps.setString(6, "");
            ps.setBoolean(7, Boolean.parseBoolean(parametrosBeans.getStringParameter9()));
            ps.setBoolean(8, Boolean.parseBoolean(parametrosBeans.getStringParameter10()));                    
            ps.setBoolean(9, Boolean.parseBoolean(parametrosBeans.getStringParameter11()));                    
           
            rs = ps.executeQuery();

                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    
                    lista.add(jsonObject);
                }
                obj.add("rows", lista);
                respuestaJson = new Gson().toJson(obj);
            
        } catch (SQLException e) {
            lista = null;
            respuestaJson = "{\"error\":\"Lo sentimosXX algo salio mal al carga la informacion.\","
                    + "\"exception\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }
    }
    
     @Override
    public String cargarNegociosVistoBuenoFianza(String unidad_negocio) {
        con = null;
        ps = null;
        rs = null;        

        String respuestaJson = "{}";  
        JsonArray lista = null;
        JsonObject obj = new JsonObject();
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL("SQL_OBTENER_NEGOCIOS_VISTO_BUENO_FIANZA"));
            ps.setString(1, unidad_negocio);
            ps.setString(2, unidad_negocio);

            rs = ps.executeQuery();

                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    
                    lista.add(jsonObject);
                }
                obj.add("rows", lista);
                respuestaJson = new Gson().toJson(obj);
            
        } catch (SQLException e) {
            lista = null;
            respuestaJson = "{\"error\":\"Lo sentimosXX algo salio mal al carga la informacion.\","
                    + "\"exception\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }
    }

    
     @Override
    public String generarVistoBuenoFianza(String negocio, Usuario user) {
        String sql = "";
        try {
            sql = this.obtenerSQL("SQL_UPDATE_VISTO_BUENO_FIANZA");
            StringStatement st = null;
            st = new StringStatement(sql, true);
            st.setString(1, user.getLogin());
            st.setString(2, negocio);           
            sql = st.getSql();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(GarantiasComunitariasImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }
    
    
     @Override
    public String generarVistoBuenoPoliza(String negocio, Usuario user) {
        String sql = "";
        try {
            sql = this.obtenerSQL("SQL_UPDATE_VISTO_BUENO_POLIZA");
            StringStatement st = null;
            st = new StringStatement(sql, true);
            st.setString(1, user.getLogin());
            st.setString(2, negocio);           
            sql = st.getSql();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(GarantiasComunitariasImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }

    
    public PdfPCell getCell(String text, int alignment,Font f) {
        PdfPCell cell = new PdfPCell(new Phrase(text,f));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);        
        return cell;
    }

    @Override
    public String cargarAgenciaUnidiad(int idUnidadNegocio) {
        con = null;
        ps = null;
        rs = null;
        query = "SQL_CARGAR_AGENCIA_UNIDAD";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, idUnidadNegocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String indemnizarNegocios(JsonObject obj, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
        String resultado = "";
        String filtro = "";
        TransaccionService tService = null;
        try {
            JsonArray detalle = obj.getAsJsonArray("info");
            if(!Boolean.parseBoolean(obj.get("acelerar_pagare").getAsString())){
                tService = new TransaccionService(this.getDatabaseName());
                tService.crearStatement();
                for (int i = 0; i < detalle.size(); i++) {
                    objeto = detalle.get(i).getAsJsonObject();
                    st = new StringStatement(this.obtenerSQL("SQL_INSERT_NEGOCIOS"), true);
                    st.setString(1, objeto.get("periodo_foto").getAsString() != null ? objeto.get("periodo_foto").getAsString() : "0");
                    st.setString(2, obj.get("empresa_fianza").getAsString() != null ? obj.get("empresa_fianza").getAsString() : "0");
                    st.setString(3, objeto.get("codcli").getAsString() != null ? objeto.get("codcli").getAsString() : "0");
                    st.setString(4, objeto.get("nit_cliente").getAsString() != null ? objeto.get("nit_cliente").getAsString() : "0");
                    st.setString(5, objeto.get("nombre_cliente").getAsString() != null ? objeto.get("nombre_cliente").getAsString() : "0");
                    st.setString(6, objeto.get("negocio").getAsString() != null ? objeto.get("negocio").getAsString() : "0");
                    st.setString(7, objeto.get("num_pagare").getAsString() != null ? objeto.get("num_pagare").getAsString() : "0");
                    st.setString(8, objeto.get("documento").getAsString() != null ? objeto.get("documento").getAsString() : "0");
                    st.setString(9, objeto.get("cuota").getAsString() != null ? objeto.get("cuota").getAsString() : "0");
                    st.setString(10, objeto.get("fecha_vencimiento").getAsString() != null ? objeto.get("fecha_vencimiento").getAsString() : "0");
                    st.setString(11, objeto.get("altura_mora").getAsString() != null ? objeto.get("altura_mora").getAsString() : "0");
                    st.setString(12, objeto.get("dias_mora").getAsString() != null ? objeto.get("dias_mora").getAsString() : "0");
                    st.setString(13, objeto.get("valor_factura").getAsString() != null ? objeto.get("valor_factura").getAsString() : "0");
                    st.setString(14, objeto.get("valor_saldo_capital").getAsString() != null ? objeto.get("valor_saldo_capital").getAsString() : "0");
                    st.setString(15, objeto.get("valor_saldo_mi").getAsString() != null ? objeto.get("valor_saldo_mi").getAsString() : "0");
                    st.setString(16, objeto.get("valor_saldo_ca").getAsString() != null ? objeto.get("valor_saldo_ca").getAsString() : "0");
                    st.setString(17, objeto.get("ixm").getAsString() != null ? objeto.get("ixm").getAsString() : "0");
                    st.setString(18, objeto.get("gac").getAsString() != null ? objeto.get("gac").getAsString() : "0");
                    st.setString(19, objeto.get("total_saldo").getAsString() != null ? objeto.get("total_saldo").getAsString() : "0");
                    st.setString(20, objeto.get("convenio").getAsString() != null ? objeto.get("convenio").getAsString() : "0");
                    st.setString(21, obj.get("unidad_negocio").getAsString() != null ? obj.get("unidad_negocio").getAsString() : "0");
                    st.setString(22, usuario.getLogin());
                    sqlver = st.getSql();
                    System.out.println(sqlver);
                    tService.getSt().addBatch(sqlver);
                    resultado =  "OK";
                }
                tService.execute();
            } else {
                if (!obj.get("cedula_cliente").getAsString().equals("")) {
                    filtro += " AND foto.nit = '" + obj.get("cedula_cliente").getAsString() + "'";
                }

                if (!obj.get("negocio").getAsString().equals("")) {
                    filtro += " AND foto.negasoc = '" + obj.get("negocio").getAsString() + "'";
                }

                if (!obj.get("monto_inicial").getAsString().equals("") && !obj.get("monto_inicial").getAsString().equals("")) {
                    filtro += " AND foto.valor_factura BETWEEN " + Double.parseDouble(obj.get("monto_inicial").getAsString()) + " AND " + Double.parseDouble(obj.get("monto_inicial").getAsString());
                }
                
                con = this.conectarJNDI();
                String[] negocios = new String[detalle.size()];
                for (int i = 0; i < detalle.size(); i++) {
                    objeto = detalle.get(i).getAsJsonObject();
                    negocios[i] = objeto.get("negocio").getAsString();
                }
                ps = con.prepareStatement(this.obtenerSQL("SQL_INSERT_NEGOCIOS_ACELERAR_PAGARE"));
                ps.setArray(1, con.createArrayOf("text", negocios));
                ps.setString(2, obj.get("perido_corte").getAsString() != null ? obj.get("perido_corte").getAsString() : "0");
                ps.setString(3, obj.get("unidad_negocio").getAsString() != null ? obj.get("unidad_negocio").getAsString() : "0");
                ps.setString(4, obj.get("empresa_fianza").getAsString() != null ? obj.get("empresa_fianza").getAsString() : "0");
                ps.setString(5, obj.get("mora").getAsString() != null ? obj.get("mora").getAsString() : "0");
                ps.setBoolean(6, Boolean.parseBoolean(obj.get("acelerar_pagare").getAsString()));
                ps.setBoolean(7, Boolean.parseBoolean(obj.get("aplica_gac").getAsString()));
                ps.setString(8, usuario.getLogin());
                ps.setString(9, filtro);
                ps.setBoolean(10, Boolean.parseBoolean(obj.get("aplica_ixm").getAsString()));

                rs = ps.executeQuery();
                while (rs.next()) {
                    resultado = rs.getString("respuesta");
                }
            }

            respuesta = "{\"respuesta\":\"" + resultado + "\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarNegociosIndemnizados() {
        query = "SQL_CARGAR_NEGOCIOS_INDEMNIZADOS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String verNegociosIndemnizados(String periodo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "VER_NEGOCIOS_INDEMNIZADOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, periodo);
            rs = ps.executeQuery();
            System.out.println("query : "+rs);
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String generarPdf(String usuario, String negocio){
        String directorio = "";
        String msg = "OK";
        try {
            
            ResourceBundle rb = null;
            
            //buscamos la info de la cabecera del extracto.
            EstadoCuentaIngresos epi = new EstadoCuentaIngresos();
            epi=infoCabeceraEstado(negocio);
            ArrayList<EstadoCuentaIngresos> apiArray = infoCabeceraEstadoDetalle(negocio);
            //epi=infoCabeceraEstadoDetalle(negocio);
            
            
            
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(usuario, "EstadoCuenta" + negocio, "pdf");
            Font fuente = new Font(Font.HELVETICA, 9, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font fuenteG = new Font(Font.HELVETICA, 9, Font.BOLD, new java.awt.Color(0, 0, 0));
            Font fuenteT = new Font(Font.HELVETICA, 9, Font.BOLD, new java.awt.Color(255, 255, 255));

            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.open();
            documento.newPage();
            
            
            //logo de fintra aqui
            //creamos una tabla para el logo
            PdfPTable tabla_temp = new PdfPTable(1);
            tabla_temp.setWidthPercentage(20);

            String url_logo ="logo_fintra_new.jpg";
            com.lowagie.text.Image img = com.lowagie.text.Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
            img.scaleToFit(150, 150);
            img.setBorder(1);

            PdfPCell celda_logo = new PdfPCell();
            celda_logo = new PdfPCell();
            celda_logo.setBorderWidthTop(0);
            celda_logo.setBorderWidthLeft(0);
            celda_logo.setBorderWidthRight(0);
            celda_logo.setBorderWidthBottom(0);
            celda_logo.setPhrase(new Phrase(" ", fuente));
            celda_logo.addElement(img);
            celda_logo.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            //agremos la celda con la imagen a la tabla del logo
            tabla_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tabla_temp.addCell(celda_logo);

            documento.add(tabla_temp);
            
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            
            PdfPTable table = new PdfPTable(2);
            table.setWidths(new int[]{3, 7});
            table.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            table.setWidthPercentage(70);
            PdfPCell cell;
            
            
        
      
            // informacion del negocio
            
            cell = new PdfPCell(new Phrase("Informacion del Negocio", fuenteT));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(61, 67, 102));
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Nombre Cliente", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getNombre_cliente(), fuente));
            cell.setBackgroundColor(new Color(255, 255, 255));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Nit Cliente", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getNit(), fuente));
            cell.setBackgroundColor(new Color(255, 255, 255));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getCod_neg(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Fecha Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getFecha_negocio(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            documento.add(table);
            
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            //crea aqui las cabeceras de la tabla //

            PdfPTable table2 = new PdfPTable(11);
            table2.setWidths(new int[]{3, 5, 8, 7, 7, 7, 7, 7, 7, 7, 7});
            table2.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            table2.setWidthPercentage(100);
            PdfPCell cell2;
            
            // informacion del negocio
       
            cell2 = new PdfPCell(new Phrase("Informacion de los Pagos", fuenteT)); 
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setColspan(11);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);
            
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            
            int numfilas = 0;
            cell2 = new PdfPCell(new Phrase("", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);
            
            cell2 = new PdfPCell(new Phrase("No. Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);


            cell2 = new PdfPCell(new Phrase("Fecha Vencimiento", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Fecha Ingreso", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Numero Ingreso", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Factura", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Ingreso", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Abono Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Abono IXM", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Abono GAC", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Saldo Factura", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);
            

            //Detalle
            for (int i = 0; i < apiArray.size(); i++) {
                
                numfilas = numfilas + 1;
                
                cell2 = new PdfPCell(new Phrase(String.valueOf(numfilas), fuenteT));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(61, 67, 102));
                table2.addCell(cell2);
                
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getNum_doc_fen(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                 table2.addCell(cell2);
                
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getFecha_vencimiento(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                 table2.addCell(cell2);
                
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getFecha_ingreso(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                 table2.addCell(cell2);
                
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getNum_ingreso(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                 table2.addCell(cell2);
                
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getValor_factura(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                 table2.addCell(cell2);
                
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getVlr_ingreso(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                 table2.addCell(cell2);
                 
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getValor_aplicado(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                 table2.addCell(cell2);
                 
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getIxm(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                 table2.addCell(cell2);
                 
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getGac(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                 table2.addCell(cell2);
                
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getValor_saldo(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);
            }
    


            documento.add(table2);
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            documento.add(new Paragraph(new Phrase("__________________________       __________________________            __________________________", fuenteG)));
            documento.add(new Paragraph(new Phrase("Elaborado Por                                  Revisado Por                                         Recibido Por", fuenteG)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
         return msg;
    }
    
      static class HeaderFooterCxC extends PdfPageEventHelper {

        private Image img;      

        public HeaderFooterCxC(ResourceBundle rb) {
            try {            
                String url_logo = "logoFintra.jpg";
                img = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
                img.setAbsolutePosition(70f, 930f);
                img.scalePercent(90);   
            } catch (BadElementException | IOException r) {
                System.err.println("Error al leer la imagen");
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            try {
                document.add(img);              
            } catch (DocumentException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
      
     private Document createDoc() {
        Document doc = new Document(PageSize.A4, 25, 25, 35, 30);
        return doc;
    }
      
    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons + "_" + fmt.format(new Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }
    
    private EstadoCuentaIngresos infoCabeceraEstado(String negocio) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "INFORMACION_ESTADO_CUENTA_INGRESOS";       
        EstadoCuentaIngresos epi = null; 
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();          
            
            while (rs.next()) {

                epi = new EstadoCuentaIngresos();
                epi.setCod_neg(rs.getString("cod_neg"));
                epi.setNombre_cliente(rs.getString("nombre_cliente"));
                epi.setNit(rs.getString("nit"));
                epi.setFecha_negocio(rs.getString("fecha_negocio"));
                          
              

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO INFORMACION_EXTRACTO: infoCabeceraExracto(int id_rop)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return epi;
    }
    
    private ArrayList<EstadoCuentaIngresos> infoCabeceraEstadoDetalle(String negocio) throws SQLException {
        ArrayList<EstadoCuentaIngresos> info = new ArrayList<EstadoCuentaIngresos> ();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "INFORMACION_ESTADO_CUENTA_INGRESOS_DETALLE";       
        EstadoCuentaIngresos epi = null; 
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();          
            
            while (rs.next()) {

                epi = new EstadoCuentaIngresos();
                epi.setNum_doc_fen(rs.getString("num_doc_fen"));
                epi.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                epi.setFecha_ingreso(rs.getString("fecha_ingreso"));
                epi.setNum_ingreso(rs.getString("num_ingreso"));
                epi.setValor_factura(rs.getString("valor_factura"));
                epi.setVlr_ingreso(rs.getString("vlr_ingreso"));          
                epi.setValor_aplicado(rs.getString("valor_aplicado"));
                epi.setIxm(rs.getString("ixm"));    
                epi.setGac(rs.getString("gac"));    
                epi.setValor_saldo(rs.getString("valor_saldo"));
                info.add(epi);
                
            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO INFORMACION_EXTRACTO: infoCabeceraExracto(int id_rop)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return info;
    }
    
   
   @Override
    public String cargarProveedorFianza() {
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        query = "cargarProveedorFianza";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("nit", rs.getString("nit"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }
    
    @Override
    public String buscarProveedorFianza(String campo, String texto) {
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        //query = campo.equals("nit") ? "buscarProveedorPorNit" : "buscarProveedorPorNombre";
        String s = null;
        if (campo.equals("nit")) {
            query = "buscarProveedorPorNit";
            s = texto;
        } else {
            query = "buscarProveedorPorNombre";
            s = "%" + texto.replaceAll("[^a-z]\\W+", "").toUpperCase() + "%";
        }
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, s);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("nit", rs.getString("nit"));
                fila.addProperty("nombre", rs.getString("payment_name"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String guardarProveedorFianza(String nit, Usuario usuario) {
        con = null;
        ps = null;
        query = "guardarProveedorFianza";
        String respuestaJson = "{}";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);    
            ps.setString(1, nit);
            ps.setString(2, "");
            ps.setString(3, usuario.getLogin());
            ps.setString(4, usuario.getLogin());
            ps.setString(5, usuario.getDstrct());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\" No se guard� el proveedor fianza\"}";
                throw new SQLException("ERROR guardarProveedorFianza \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cambiarEstadoProveedorFianza(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "cambiarEstadoProveedorFianza";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\" No se actualiz� el estado del proveedor fianza\"}";
                throw new SQLException("ERROR activaInactivaConfigFactor \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String cargarCboProducto() {
      con = null;
        ps = null;
        rs = null;
        query = "SQL_CARGAR_COMBO_PRODUCTO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("codigo_producto"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarCboCobertura() {
     con = null;
        ps = null;
        rs = null;
        query = "SQL_CARGAR_COMBO_COBERTURA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre_corto"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
}
