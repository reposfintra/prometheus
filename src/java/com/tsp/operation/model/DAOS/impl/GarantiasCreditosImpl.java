/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.GarantiasCreditosDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.GarantiasCreditosAutommotor;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import leerexcel.LeerExcel;

/**
 *
 * @author edgargonzalezmendoza
 */
public class GarantiasCreditosImpl extends MainDAO implements GarantiasCreditosDAO{
    
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    StringStatement st;
    
    public GarantiasCreditosImpl(String databaseName) {
        super("cargar_garantias.xml", databaseName);
    }

    @Override
    public ArrayList<String> cargarArchivoPreProceso(String ruta, Usuario u, String filename) throws Exception {
        ArrayList<String> listaQuerys = null;

        String query = "CARGAR_ARCHIVO_PRECARGA";
        String sql = "";

        sql = this.obtenerSQL(query);
        LeerExcel excel=new LeerExcel();
        List lista_datos_celda = excel.Leer_Archivo_Excel(ruta);
        String[] vectorQuerys = excel.Imprimir_Consola(lista_datos_celda, sql).split(";");
        listaQuerys = new ArrayList<>();
        listaQuerys.addAll(Arrays.asList(vectorQuerys));

        //agregamos el control de lote
        query = "CONTROL_LOTE_FASECOLDA";
        sql = this.obtenerSQL(query);
        StringStatement st = null;
        st = new StringStatement(sql, true);
        st.setString(1, filename);
        st.setString(2, u.getIdusuario());
        listaQuerys.add(st.getSql());        
        st = null;
        
        return listaQuerys;
    }

    @Override
    public String cargarMarca(String q) {
      
        con = null;
        ps = null;
        rs = null;
        String query = "BUSCAR_MARCA_VEHICULO";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1,q);
                rs = ps.executeQuery();
                lista = new JsonArray();

                jsonObject = new JsonObject(); 
                lista.add(jsonObject);

                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("value",rs.getInt("lab"));
                    jsonObject.addProperty("label",rs.getString("val") );
                    lista.add(jsonObject); 
                }
                
                if(lista.size()==0)lista.add(jsonObject);

            }
        } catch (SQLException e) {
            lista = null;

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String cargarCombox(String parametro1,String parametro2,String marca, int option) {
       
        con = null;
        ps = null;
        rs = null;
        String query = "";
        
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            
   
            con = this.conectarJNDI("BUSCAR_CLASE");

            if (con != null) {
                switch (option) {
                    case 3:
                        query = "BUSCAR_CLASE";
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setInt(1, Integer.parseInt(parametro1));
                        break;
                    case 4:
                        query = "BUSCAR_NEGOCIO_SEGURO";
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setString(1,parametro1);
                        break;
                    case 5:
                        query = "BUSCAR_ASEGURADORA";
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        break;
                    case 6:
                        query = "BUSCAR_REFERENCIA_1";
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setInt(1, Integer.parseInt(parametro1));
                        ps.setString(2, parametro2);
                        break;
                    case 7:
                        query = "BUSCAR_REFERENCIA_2";
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setString(1, marca);
                        ps.setString(2, parametro2);
                        ps.setInt(3, Integer.parseInt(parametro1));
                        break;
                    case 8:
                        query = "BUSCAR_REFERENCIA_3";
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setInt(1, Integer.parseInt(parametro1));
                        ps.setInt(2, Integer.parseInt(parametro2));
                        break;

                }
            
              
                rs = ps.executeQuery();
                
                lista = new JsonArray();
                jsonObject = new JsonObject();
                jsonObject.addProperty("id", "");
                jsonObject.addProperty("value", "Seleccione");
                lista.add(jsonObject);

                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("id", rs.getInt("id"));
                    jsonObject.addProperty("value", rs.getString("val"));
                    lista.add(jsonObject);
                }
                
                if(lista.size()==0)
                lista.add(jsonObject);

            }
        } catch (SQLException e) {
            lista = new JsonArray();
            e.printStackTrace();

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
        
    }
    

    @Override
    public String infoFasecolda(String marca,String clase, String ref1, String ref2, String ref3) {
       
        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_INFO_FASECOLDA";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, marca);
                ps.setString(2, clase);
                ps.setString(3, ref1);
                ps.setString(4, ref2);
                ps.setString(5, ref3);               
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }

    }

    @Override
    public String guardarGarantiaAutomotor(GarantiasCreditosAutommotor gca, Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "GUARDAR_GARANTIAS_FASECOLDA";
        String respuestaJson = "{}";
        try {
            con = this.conectarJNDI(query,user.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, gca.getNegocio().toUpperCase());
                ps.setString(2, gca.getIdentificacion());
                ps.setString(3, gca.getNombre().toUpperCase());
                ps.setString(4, gca.getNro_propiedad());
                ps.setString(5, gca.getChasis());
                ps.setString(6, gca.getMotor());                
                ps.setString(7, gca.getNr_poliza());
                ps.setString(8, gca.getFecha_exp());
                ps.setString(9, gca.getFecha_ven());
                ps.setString(10, gca.getNegocio_poliza());                
                ps.setString(11, gca.getCodigo_fasecolda());
                ps.setString(12, gca.getPlaca().toUpperCase());
                ps.setString(13, gca.getMarca().toUpperCase());
                ps.setString(14, gca.getClase().toUpperCase());
                ps.setString(15, gca.getServicio().toUpperCase());
                ps.setString(16, gca.getReferencia1().toUpperCase());
                ps.setString(17, gca.getReferencia2().toUpperCase());
                ps.setString(18, gca.getReferencia3().toUpperCase());
                ps.setString(19, gca.getPais().toUpperCase());
                ps.setString(20, gca.getModelo());
                ps.setString(21, gca.getAseguradora().toUpperCase());
                ps.setDouble(22, gca.getValor_fasecolda());
                ps.setString(23, user.getLogin());
         
                ps.execute();
                
                respuestaJson = "{\"respuesta\":\"OK\"}";

            }
        } catch (SQLException e) {
            respuestaJson = "{\"respuesta\":\"ERRORSQL\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }

    }

    @Override
    public String ListaGarantiaAutomotor(String negocio, Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "LISTAR_GARANTIAS_FASECOLDA";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query,user.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, negocio);                
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }

    }

    @Override
    public String actualizarGarantias(GarantiasCreditosAutommotor gca, int id, Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "UPDATE_GARANTIAS_FASECOLDA";
        String respuestaJson = "{}";
        try {
            con = this.conectarJNDI(query,user.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, gca.getEstado());
                ps.setString(2, gca.getNro_propiedad());
                ps.setString(3, gca.getChasis());
                ps.setString(4, gca.getMotor());
                ps.setString(5, gca.getCodigo_fasecolda());
                ps.setString(6, gca.getPlaca().toUpperCase());
                ps.setString(7, gca.getMarca().toUpperCase());
                ps.setString(8, gca.getClase().toUpperCase());
                ps.setString(9, gca.getServicio().toUpperCase());
                ps.setString(10, gca.getReferencia1().toUpperCase());
                ps.setString(11, gca.getReferencia2().toUpperCase());
                ps.setString(12, gca.getReferencia3().toUpperCase());
                ps.setString(13, gca.getPais().toUpperCase());
                ps.setString(14, gca.getModelo());
                ps.setString(15, gca.getAseguradora().toUpperCase());
                ps.setDouble(16, gca.getValor_fasecolda());
                ps.setString(17, user.getLogin());
                ps.setInt(18, id);

                ps.execute();
                
                respuestaJson = "{\"respuesta\":\"OK\"}";

            }
        } catch (SQLException e) {
            respuestaJson = "{\"respuesta\":\"ERRORSQL\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }

    }
    
    @Override
     public String cargaraSeguradora(Usuario usuario) {
        con = null;
        ps = null;
        rs = null;
        String seguro = "";
        String query = "BUSCAR_ASEGURADORA";
        try {
            con = this.conectarJNDI(query, usuario.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                while (rs.next()) {
                    seguro += rs.getString("val") + ": " + rs.getString("val") + "; ";
                }
            }

        } catch (SQLException ex) {

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GarantiasCreditosImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    Logger.getLogger(GarantiasCreditosImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                    Logger.getLogger(GarantiasCreditosImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return seguro;
    }

    @Override
    public String visualizarPrecarga(Usuario usuario) {
        con = null;
        ps = null;
        rs = null;
        String query = "PREVISUALIZAR_FASECOLDA";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query,usuario.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String crearMaestrosFasecolda(Usuario usuario) {
        
        con = null;
        ps = null;
        rs = null;
        String query = "CREAR_MAESTROS_FASECOLDA";
        String respuestaJson="{}";
        
        try {
            con = this.conectarJNDI(query,usuario.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1,usuario.getLogin());
                rs = ps.executeQuery();               
                if (rs.next()) {
                    if (rs.getBoolean("retorno")) {
                        respuestaJson = "{\"respuesta\":\"OK\"}";
                    } else {
                        respuestaJson = "{\"respuesta\":\"ERROR\"}";
                    }
                }

            }
        } catch (SQLException e) {
            respuestaJson = "{\"respuesta\":\"ERRORSQL\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }
    }

    @Override
    public String limpiarPrecargaFasecolda(Usuario usuario) {
         
        con = null;
        ps = null; 
        String query = "DELETE_PRECARGA_FASECOLDA";
        String respuestaJson="{}";
        
        try {
            con = this.conectarJNDI(query,usuario.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                int execute = ps.executeUpdate();               
                if (execute > 0) {
                    respuestaJson = "{\"respuesta\":\"OK\"}";
                } else {
                    respuestaJson = "{\"respuesta\":\"ERROR\"}";
                }

            }
        } catch (SQLException e) {
            respuestaJson = "{\"respuesta\":\"ERRORSQL\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }
    }

   
}
