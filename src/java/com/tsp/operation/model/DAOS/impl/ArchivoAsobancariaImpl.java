/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.ArchivoAsobancariaDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.RecaudoAsobancaria;
import com.tsp.operation.model.beans.RecaudoAsobancariaDetalle;
import com.tsp.operation.model.beans.RecaudoAsobancariaDiccionario;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author egonzalez
 */
public class ArchivoAsobancariaImpl extends MainDAO implements ArchivoAsobancariaDAO{

    
    public ArchivoAsobancariaImpl() {
        super("ArchivoAsobancariaDAO.xml");
    }
    
    public ArchivoAsobancariaImpl(String dataBaseName) {
        super("ArchivoAsobancariaDAO.xml",dataBaseName);
    }

    

    @Override
     public ArrayList<RecaudoAsobancariaDiccionario> cargarDiccionario() throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DICCIONARIO_RECAUDO";
        ArrayList<RecaudoAsobancariaDiccionario> listaDiccionario = null;

        try {
            String sql = "";
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            listaDiccionario = new ArrayList();
            RecaudoAsobancariaDiccionario diccionario = null;
          
            while (rs.next()) {

                diccionario = new RecaudoAsobancariaDiccionario();
                diccionario.setTipo_referencia(rs.getString("tipo"));
                diccionario.setCodigo(rs.getString("codigo"));
                diccionario.setDescripcion(rs.getString("descripcion"));
                listaDiccionario.add(diccionario);

            }
           
           
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO DICCIONARIO RECAUDO: cargarDiccionario()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaDiccionario;

    }

  
    @Override
    public int insertarRecaudos(RecaudoAsobancaria recaudoCabecera, ArrayList listDetalle, String usuario, String empresa)  throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_INSERTAR_CABECERA_RECAUDO";
        int resp = 0;
        int idRecaudo=0;
        double valor_recaudo=0;
        try {
          
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, recaudoCabecera.getFacturadora_nit());
            ps.setString(2, recaudoCabecera.getFecha_recaudo());
            ps.setInt(3, recaudoCabecera.getRecaudadora_cod());
            ps.setInt(4, recaudoCabecera.getRecaudadora_cod());
            ps.setString(5, recaudoCabecera.getCuenta_cli());
            ps.setString(6, recaudoCabecera.getFecha_archivo());
            ps.setString(7, recaudoCabecera.getFecha_archivo());
            ps.setString(8, recaudoCabecera.getModificador());
            ps.setString(9, recaudoCabecera.getTipo_cuenta());
            ps.setInt(10, recaudoCabecera.getNum_lotes());
            ps.setInt(11, recaudoCabecera.getTotal_registros());
            valor_recaudo = (recaudoCabecera.getNum_lotes() == 0) ? recaudoCabecera.getValor_total() : recaudoCabecera.getValor_total() / 100;
            ps.setDouble(12, valor_recaudo);
            ps.setString(13, usuario);
            ps.setString(14, empresa);
            resp = ps.executeUpdate();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idRecaudo = rs.getInt(1);
                    ps = con.prepareStatement(insertarRecaudosDetalle(idRecaudo, listDetalle, usuario, empresa));
                    ps.executeUpdate();
                }
            }
            con.commit();

        } catch (SQLException ex) {
            idRecaudo = 0;            
            if (con != null) con.rollback();
            throw new SQLException("ERROR OBTENIENDO EN SQL_INSERTAR_CABECERA_RECAUDO:insertarRecaudos(RecaudoAsobancaria listaCabecera, ArrayList listDetalle) " + ex.getMessage());          
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {   
                    con.setAutoCommit(true);
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
           
            return idRecaudo;
        }

    }
    
    public String insertarRecaudosDetalle(int idRecaudo, ArrayList listDetalle, String usuario, String empresa) throws SQLException {
               
        String cadena = "";
        String query = "SQL_INSERTAR_DETALLE_RECAUDO";
        String sql = "";
        StringStatement st = null;
       
        try {
            double valor_recaudado = 0;
            Iterator<RecaudoAsobancariaDetalle> itrDetalles = listDetalle.iterator();
            while (itrDetalles.hasNext()) {
                RecaudoAsobancariaDetalle detalle = itrDetalles.next();
                sql = this.obtenerSQL(query);
                st = new StringStatement(sql, true);                
                st.setInt(1, idRecaudo);
                st.setString(2, detalle.getCod_servicio_rec());
                st.setInt(3, detalle.getNumero_lote());
                st.setString(4, detalle.getReferencia_factura());
                valor_recaudado = (detalle.getCod_servicio_rec().equals("")) ?  detalle.getValor_recaudado() :  detalle.getValor_recaudado() / 100;
                st.setDouble(5, valor_recaudado);
                st.setString(6, detalle.getProcedencia_pago());
                st.setString(7, detalle.getMedio_pago());
                st.setString(8, detalle.getNum_operacion());
                st.setString(9, detalle.getNum_autorizacion());
                st.setInt(10, detalle.getCod_entidad_debitada());
                st.setInt(11, detalle.getCod_entidad_debitada());
                st.setString(12, detalle.getCod_sucursal());
                st.setInt(13, detalle.getSecuencia());
                st.setString(14, detalle.getCausal_devolucion());
                st.setString(15, usuario);
                st.setString(16, empresa);
                st.setString(17, detalle.getSegunda_referencia_usuario());
                st.setInt(18, detalle.getTipo_recaudo());
                st.setString(19, detalle.getFecha_real_recaudo());
                st.setDouble(20, detalle.getValor_recaudo_canje());
                st.setString(21, detalle.getNumero_cheque());
                st.setString(22, detalle.getCod_compensacion_cheque());
                cadena += st.getSql();
                st = null;
            }
            
         
        } catch (Exception e) {
            System.out.println("Error en SQL_INSERTAR_DETALLE_RECAUDO (insertarRecaudosDetalle)"+e.toString());
            e.printStackTrace();
        }
        return cadena;

    }
    
    @Override
    public String procesarRecaudosDetalle(int idRecaudo, String usuario) throws SQLException{
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_PROCESA_DETALLE_RECAUDO";

        String  respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, idRecaudo);  
            ps.setString(2, usuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                String result=rs.getString("resultado");
                if(result.equals("OK")){
                    respuesta = "02";
                    break;
                }else{
                    respuesta ="19";
                }
            }
                          
        } catch (SQLException ex) {         
            respuesta ="99 procesarRecaudosDetalle(Error: "+ex.getMessage()+")";
            throw new SQLException("ERROR OBTENIENDO SQL_PROCESA_DETALLE_RECAUDO: procesarRecaudosDetalle(int idRecaudo)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }

            return respuesta;
        }
    
    }
    
    
    @Override
    public ArrayList<RecaudoAsobancaria> buscarCabeceraRecaudo(int idRecaudo) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_CABECERA_RECAUDO";   
        String filtro="where id = "+idRecaudo;
        ArrayList<RecaudoAsobancaria> listaCabeceraRecaudo = null;

        try {
            String sql = "";
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));     
            rs = ps.executeQuery();
            listaCabeceraRecaudo = new ArrayList();
            RecaudoAsobancaria cabecera_recaudo = null;
          
            while (rs.next()) {

                cabecera_recaudo = new RecaudoAsobancaria();
                cabecera_recaudo.setId(rs.getInt("id"));
                cabecera_recaudo.setFacturadora_nit(rs.getString("facturadora_nit"));
                cabecera_recaudo.setEntidad_recaudo(rs.getString("entidad_recaudo"));
                cabecera_recaudo.setFecha_recaudo(rs.getString("fecha_recaudo"));
                cabecera_recaudo.setFecha_creacion(rs.getString("creation_date"));
                cabecera_recaudo.setTotal_registros(rs.getInt("numero_filas"));   
                cabecera_recaudo.setTotal_encontrados(rs.getInt("total_encontrados"));    
                cabecera_recaudo.setTotal_procesados(rs.getInt("total_procesados")); 
                cabecera_recaudo.setValor_aplicado(rs.getDouble("valor_aplicado")); 
                cabecera_recaudo.setValor_total(rs.getDouble("valor_total"));     
                cabecera_recaudo.setNum_lotes(rs.getInt("numero_lotes"));      
                listaCabeceraRecaudo.add(cabecera_recaudo);

            }
           
           
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO CABECERA RECAUDO: SQL_BUSCAR_CABECERA_RECAUDO(int idRecaudo)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaCabeceraRecaudo;

    }
    
    
    @Override
    public ArrayList<RecaudoAsobancaria> buscarCabeceraRecaudo(String fechaini, String fechafin, String entidadRecaudadora, String referencia,String empresa) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_CABECERA_RECAUDO";
        String filtro="where dstrct='"+ empresa +"' and creation_date::date between '"+fechaini+"' and '"+ fechafin+"'";
        filtro += (!entidadRecaudadora.equals("")) ? " and recaudadora_cod="+Integer.parseInt(entidadRecaudadora):"";
        filtro += (!referencia.equals("")) ? " and '"+referencia+"' in (select referencia_factura from recaudo.recaudo_detalles where id_rec=e.id)":"";
        ArrayList<RecaudoAsobancaria> listaCabeceraRecaudo = null;

        try {
            String sql = "";
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));  
            rs = ps.executeQuery();
            listaCabeceraRecaudo = new ArrayList();
            RecaudoAsobancaria cabecera_recaudo = null;
          
            while (rs.next()) {

                cabecera_recaudo = new RecaudoAsobancaria();
                cabecera_recaudo.setId(rs.getInt("id"));
                cabecera_recaudo.setFacturadora_nit(rs.getString("facturadora_nit"));
                cabecera_recaudo.setEntidad_recaudo(rs.getString("entidad_recaudo"));
                cabecera_recaudo.setFecha_recaudo(rs.getString("fecha_recaudo"));
                cabecera_recaudo.setFecha_creacion(rs.getString("creation_date"));
                cabecera_recaudo.setTotal_registros(rs.getInt("numero_filas")); 
                cabecera_recaudo.setTotal_encontrados(rs.getInt("total_encontrados"));    
                cabecera_recaudo.setTotal_procesados(rs.getInt("total_procesados"));  
                cabecera_recaudo.setValor_aplicado(rs.getDouble("valor_aplicado"));  
                cabecera_recaudo.setValor_total(rs.getDouble("valor_total"));  
                cabecera_recaudo.setNum_lotes(rs.getInt("numero_lotes"));  
                listaCabeceraRecaudo.add(cabecera_recaudo);

            }
           
           
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO CABECERA RECAUDO: SQL_BUSCAR_CABECERA_RECAUDO()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaCabeceraRecaudo;

    }
    
    @Override
    public ArrayList<RecaudoAsobancariaDetalle> buscarDetalleRecaudo(int idRecaudo, String tipoAsobancaria) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DETALLE_RECAUDO";
        ArrayList<RecaudoAsobancariaDetalle> listaDetalleRecaudo = null;

        try {
            String sql = "";
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));            
            ps.setString(1, tipoAsobancaria);       
            ps.setInt(2, idRecaudo);
            rs = ps.executeQuery();
            listaDetalleRecaudo = new ArrayList();
            RecaudoAsobancariaDetalle detalle_recaudo = null;
          
            while (rs.next()) {

                detalle_recaudo = new RecaudoAsobancariaDetalle();
                detalle_recaudo.setId(rs.getInt("id"));
                detalle_recaudo.setReferencia_factura(rs.getString("referencia_factura"));  
                detalle_recaudo.setNegocio(rs.getString("negocio"));
                detalle_recaudo.setProcedencia_pago(rs.getString("procedencia_pago"));                
                detalle_recaudo.setMedio_pago(rs.getString("medio_pago"));
                detalle_recaudo.setValor_recaudado(rs.getDouble("valor_recaudado"));   
                detalle_recaudo.setEncontrado(rs.getString("encontrado"));   
                detalle_recaudo.setProcesado_cartera(rs.getString("procesado_cartera")); 
                detalle_recaudo.setCausal_devolucion_proc(rs.getString("causal_dev_procesamiento")); 
                listaDetalleRecaudo.add(detalle_recaudo);

            }
           
           
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO DETALLE RECAUDO: SQL_BUSCAR_DETALLE_RECAUDO()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaDetalleRecaudo;

    }
    
    @Override
    public ArrayList<MenuOpcionesModulos> cargarMenuRecaudo(String usuario,String distrito) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OPCIONES_RECAUDO";
        ArrayList<MenuOpcionesModulos> listaOpcionesRecaudo = null;

        try {
            String sql = "";
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1,"%"+usuario+"%");
            ps.setString(2, distrito);
            rs = ps.executeQuery();
            listaOpcionesRecaudo = new ArrayList();
            MenuOpcionesModulos opciones_recaudo = null;
          
            while (rs.next()) {

                opciones_recaudo = new MenuOpcionesModulos();
                opciones_recaudo.setId(rs.getInt("id"));
                opciones_recaudo.setDescripcion(rs.getString("descripcion"));
                opciones_recaudo.setRuta(rs.getString("ruta"));
                opciones_recaudo.setOrden(rs.getInt("orden"));             
                listaOpcionesRecaudo.add(opciones_recaudo);

            }
           
           
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO MENU RECAUDO: cargarMenuRecaudo()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaOpcionesRecaudo;

    }
    
   @Override
    public String cargarCboEntidadesRecaudo(String distrito) throws SQLException{

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ENTIDADES_RECAUDADORAS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", "dstrct='"+distrito+"' and reg_status  = ''"));

            rs = ps.executeQuery();
          
            while (rs.next()) {
                obj.addProperty(rs.getString("codigo_entidad"), rs.getString("descripcion"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }

            return respuestaJson;
        }

    }
    
    @Override
    public String obtenerEstadoRecaudo(String texto) throws SQLException {
       
        String msj = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_ESTADO_RECAUDO";
       
        try {
            String codEstado = texto.substring(0,2);
            String errordesc = texto.substring(2,texto.length());
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));   
            ps.setString(1, codEstado);
            rs = ps.executeQuery();
          
            while (rs.next()) {
                msj = rs.getString("descripcion")+errordesc;
            }

        } catch (SQLException ex) {
           throw new SQLException("ERROR OBTENIENDO ESTADO RECAUDO: obtenerEstadoRecaudo(String texto)  " + ex.getMessage());
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }

            return msj;
        }

    }
    
    @Override
    public boolean ingresadoArchivoRecaudo(int total_registros, double valor_total, int cod_entidad, String fecha_archivo, String distrito) throws SQLException {
        boolean encontrado=false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE_ARCHIVO_RECAUDO";
        String filtro = "";

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setInt(1, total_registros);
            ps.setDouble(2, valor_total);
            ps.setInt(3, cod_entidad);
            ps.setString(4, fecha_archivo);
            ps.setString(5, fecha_archivo);
            ps.setString(6, distrito);
            rs = ps.executeQuery();
          
            while (rs.next()) {
                double cant = rs.getDouble("cantidad");
                if(cant > 0){
                   encontrado = true;
                }
            }

        } catch (SQLException ex) {
           throw new SQLException("ERROR OBTENIENDO ARCHIVO RECAUDO: ingresadoArchivoRecaudo(int total_registros,double valor_total,int cod_entidad,String fecha_archivo)  " + ex.getMessage());
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }

            return encontrado;
        }

    }
    

    @Override
    public boolean existeCodEntidadRecaudo(int cod_entidad) throws SQLException {
        boolean sw=false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ENTIDADES_RECAUDADORAS";
        String filtro = " reg_status  = '' and codigo_entidad="+cod_entidad;

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));           
            rs = ps.executeQuery();
          
            if (rs.next()) {
                sw = true;
            }

        } catch (SQLException ex) {
           throw new SQLException("ERROR OBTENIENDO ENTIDAD RECAUDO: existeCodEntidadRecaudo(int cod_entidad)  " + ex.getMessage());
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }

            return sw;
        }

    }
    
    @Override
    public ArrayList<BeanGeneral> buscarInfoPagoExtracto(int idDetalleRecaudo) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_INFO_PAGO_EXTRACTO";
       
        ArrayList<BeanGeneral> listaPagoExtracto = null;

        try {
            String sql = "";
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, idDetalleRecaudo);
            rs = ps.executeQuery();
            listaPagoExtracto = new ArrayList();
           
          
            while (rs.next()) {

                BeanGeneral bean = new BeanGeneral();
                bean.setValor_01(rs.getString("tipo"));
                bean.setValor_02(rs.getString("num_ingreso"));
                bean.setValor_03(rs.getString("fecha_ingreso"));
                bean.setValor_04(rs.getString("nitcli"));
                bean.setValor_05(rs.getString("nomcli"));
                bean.setValor_06(rs.getString("cuenta"));
                bean.setValor_07(rs.getString("vlr_ingreso"));
                bean.setCreation_user(rs.getString("usuario_creacion"));
                listaPagoExtracto.add(bean);

            }
           
           
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO INFO PAGO EXTRACTO: buscarInfoPagoExtracto(int idDetalleRecaudo)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaPagoExtracto;

    }
    
    @Override
    public ArrayList<BeanGeneral> buscarDetalleIngreso(String numIngreso) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DETALLE_INGRESO";
       
        ArrayList<BeanGeneral> listaDetalleIngreso = null;

        try {
            String sql = "";
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query)); 
            ps.setString(1, numIngreso);
            rs = ps.executeQuery();
            listaDetalleIngreso = new ArrayList();
           
          
            while (rs.next()) {

                BeanGeneral bean = new BeanGeneral();
                bean.setValor_01(rs.getString("tipo_doc"));
                bean.setValor_02(rs.getString("documento"));
                bean.setValor_03(rs.getString("factura"));  
                bean.setValor_04(rs.getString("fecha_vence")); 
                bean.setValor_05(rs.getString("cuenta")); 
                bean.setValor_06(rs.getString("valor_ingreso"));
                listaDetalleIngreso.add(bean);

            }
           
           
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO DETALLE INGRESO: buscarDetalleIngreso(String numIngreso)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaDetalleIngreso;

    }
    
    @Override
     public String obtenerCausalDevolucion(String codCausal) throws SQLException {
       
        String result = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_CAUSAL_DEV_PROC";
       
        try {
         
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));   
            ps.setString(1, codCausal);
            rs = ps.executeQuery();
          
            while (rs.next()) {
                result = rs.getString("descripcion");
            }

        } catch (SQLException ex) {
           throw new SQLException("ERROR OBTENIENDO ESTADO RECAUDO: obtenerCausalDevolucion(String codCausal)  " + ex.getMessage());
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }

            return result;
        }

    }
    
    @Override
    public ArrayList<String> cargarSucursales(int cod_entidad) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_SUCURSALES";
      
        ArrayList<String> listaSucursal = new ArrayList();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, cod_entidad);
            rs = ps.executeQuery();

            while (rs.next()) {
                listaSucursal.add(rs.getString("codigo_sucursal"));
            }
            
             return listaSucursal;

        } catch (SQLException ex) {
            return null;
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
           
        }

    }
    
    @Override
    public ArrayList<BeanGeneral> cargarEntidadesRecaudo(String idempresa) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ENTIDADES_RECAUDADORAS";        
        String filtro = " dstrct='"+idempresa+"' order by reg_status,codigo_entidad";
        ArrayList<BeanGeneral> listaEntidadRecaudo = null;

        try {           
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));           
            rs = ps.executeQuery();
            listaEntidadRecaudo = new ArrayList();
           
          
            while (rs.next()) {

                BeanGeneral bean = new BeanGeneral();
                bean.setValor_01(rs.getString("codigo_entidad"));
                bean.setValor_02(rs.getString("descripcion"));
                bean.setValor_03(rs.getString("nit"));
                bean.setValor_04(rs.getString("direccion"));
                bean.setValor_05(rs.getString("telefono"));
                bean.setValor_06(rs.getString("email"));
                bean.setValor_07(rs.getString("account_number"));
                bean.setValor_08(rs.getString("pais"));
                bean.setValor_09(rs.getString("dpto"));
                bean.setValor_10(rs.getString("ciudad"));
                bean.setValor_11(rs.getString("is_bank"));
                bean.setValor_12(rs.getString("pago_automatico"));
                bean.setReg_status(rs.getString("reg_status"));
                listaEntidadRecaudo.add(bean);

            }
           
           
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO ENTIDADES RECAUDADORAS: cargarEntidadesRecaudo(String idempresa)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaEntidadRecaudo;

    }
    
    @Override
    public boolean existeEntidadRecaudo(String empresa, int codigo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeEntidadRecaudo";
      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, codigo);
            ps.setString(2, empresa);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeEntidadRecaudo " + e.toString());
            } catch (Exception ex) {               
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return resp;
    }

    
    @Override
    public String guardarEntidadRecaudadora(String empresa,int codigo,String descripcion,String nit,String direccion,String telefono,String email,String ciudad,String cuenta,String is_bank,String pago_automatico,String usuario, String reg_status) {
        Connection con = null;
        PreparedStatement ps = null;     
        String query = "SQL_GUARDAR_ENTIDAD_RECAUDADORA";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setInt(2, codigo);
            ps.setString(3, descripcion);
            ps.setString(4, nit);
            ps.setString(5, direccion);
            ps.setString(6, telefono);
            ps.setString(7, email);
            ps.setString(8, ciudad);
            ps.setString(9, cuenta);
            ps.setString(10, is_bank);
            ps.setString(11, pago_automatico);
            ps.setString(12, usuario);
            ps.setString(13, reg_status);
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {           
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarEntidadRecaudadora \n" + e.getMessage());           
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;  
       }
    }

    @Override
    public String actualizarEntidadRecaudadora(String empresa,int codigo,String descripcion,String nit,String direccion,String telefono,String email,String ciudad,String cuenta,String is_bank,String pago_automatico,String usuario, String reg_status) {
        Connection con = null;
        PreparedStatement ps = null;     
        String query = "SQL_ACTUALIZAR_ENTIDAD_RECAUDADORA";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, descripcion);
            ps.setString(2, nit);
            ps.setString(3, direccion);
            ps.setString(4, telefono);
            ps.setString(5, email);
            ps.setString(6, ciudad);
            ps.setString(7, cuenta);
            ps.setString(8, is_bank);
            ps.setString(9, pago_automatico);
            ps.setString(10, usuario);
            ps.setString(11, reg_status);
            ps.setInt(12, codigo);
            ps.setString(13, empresa);
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {           
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarEntidadRecaudadora \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson; 
       }
    }

    
    @Override
    public String cambiarEstadoEntidadRecaudadora(int codEntidad, String estado) {
        Connection con = null;
        PreparedStatement ps = null;       
        
        String   query = "SQL_ACTIVAR_DESACTIVAR_ENTIDAD";
        String  respuestaJson = "{}";
           
        try{
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setString(1, estado);
            ps.setInt(2, codEntidad);
            ps.executeUpdate();
           
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {   
              respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
              throw new SQLException("ERROR cambiarEstadoEntidadRecaudadora \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
       }
    }
    
    @Override
      public String cargarPais() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_PAISES";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));         
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("country_code"), rs.getString("country_name"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }


            return respuestaJson;
        }

    }

    
    @Override
      public String cargarDepartamento(String codpais) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DEPARTAMENTOS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codpais);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("department_code"), rs.getString("department_name"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }


            return respuestaJson;
        }

    }

    /**
     *
     * @param coddpt
     * @return
     */
    @Override
    public String cargarCiudad(String coddpt) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_CIUDADES";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, coddpt);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("codciu"), rs.getString("nomciu"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }


            return respuestaJson;
        }

    }

    @Override
    public String cargarComisionEntidadesRecaudo() {
     
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_COMISION_ENTIDADES_RECAUDADORAS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        JsonArray datos = new JsonArray();
        JsonObject fila = new JsonObject();

        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
          
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("codigo_entidad", rs.getString("codigo_entidad"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("codigo_canal", rs.getString("codigo_canal"));
                fila.addProperty("nombre_canal", rs.getString("nombre_canal"));
                fila.addProperty("valor_comision_recaudo", rs.getString("valor_comision_recaudo"));
                fila.addProperty("porc_iva", rs.getString("porc_iva"));
                fila.addProperty("total_comision", rs.getString("total_comision"));
                datos.add(fila);
            }
             obj.add("rows", datos);
            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
         return respuestaJson;
    }

    @Override
    public String actualizarComisionEntidadRecaudo(int cod_entidad,String codigo_canal, double valor_comision, double porcentaje_iva, double valor_total_comision ,Usuario usuario) {
     Connection con = null;
        PreparedStatement ps = null;       
        
        String   query = "SQL_UPDATE_COMISION_ENTIDAD";
        String  respuestaJson = "{}";
           
        try{
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setDouble(1, valor_comision);
            ps.setDouble(2, porcentaje_iva);
            ps.setDouble(3, valor_total_comision);
            ps.setString(4, usuario.getLogin());
            ps.setInt(5, cod_entidad);
            ps.setString(6, codigo_canal);          
            int i=ps.executeUpdate();
           
            if (i > 0) {
                if (logUpdateCambioComisionEntidad(cod_entidad,codigo_canal, valor_comision,porcentaje_iva,valor_total_comision, usuario)) {
                    respuestaJson = "{\"respuesta\":\"OK\"}";
                }
            } else {
                respuestaJson = "{\"respuesta\":\"NO SE ACTUALIZARON VALORES\"}";
            }
        }catch (Exception e) {   
              respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
               respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
            }            
       }
       return respuestaJson;
    }

    private boolean logUpdateCambioComisionEntidad(int cod_entidad,String codigo_canal, double valor_comision, double porcentaje_iva, double valor_total_comision ,Usuario usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;       
        
        String   query = "SQL_INSERT_LOG_COMISION_ENTIDAD";
           
        try{
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setInt(1, cod_entidad); 
            ps.setString(2, codigo_canal);                       
            ps.setDouble(3, valor_comision);
            ps.setDouble(4, porcentaje_iva);
            ps.setDouble(5, valor_total_comision);
            ps.setString(6, usuario.getLogin());                       
            int i=ps.executeUpdate();           
            return i>0;
            
        }catch (Exception e) {   
              throw new SQLException("ERROR logUpdateCambioComisionEntidad \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                throw new SQLException("ERROR logUpdateCambioComisionEntidad \n" + ex.getMessage());
            }
        }
    }

}