/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.FacturaElectronicaDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HaroldC
 */
public class FacturaElectronicaImpl extends MainDAO implements FacturaElectronicaDAO{

    public FacturaElectronicaImpl(String dataBaseName) {
        super("FacturaElectronicaDAO.xml", dataBaseName);
    }

    
    @Override
    public JsonObject LoginToken(JsonObject jsonData) {
       
        JsonObject jo = null;
        String inputLine;
        HttpURLConnection connection = null;
        JsonObject response = new JsonObject();
        String TokenBearer;
        String CadenaXML = "";
        String query = "SQL_CONSULTAR_CLIENTES";
        Connection con = null;
        PreparedStatement ps = null;

        try {
           
            //TokenBearer = "Bearer " + ObtenerToken("https://plcolabbeta.azure-api.net/Auth/Login", "8012803358214116aa214caa18b2af3e", "802022016", "H4r0ldcu..!", "9646722D-00C6-49E0-9DCF-A904012B4754"); //BETA
            TokenBearer = "Bearer " + ObtenerToken("https://plcolab-api.azure-api.net/Auth/Login", "ee058b9cc0ea4d4c80693de8d05e0f90", "802022016", "Fintra.2019*", "03E8FBA0-7A61-4E8F-98A6-A97101372C80");
            
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            
            JsonArray queryJsonArray = getQueryJsonArray(con, ps);
            int iterador=0;
            
            BufferedReader in = null;
            
            for (Iterator<JsonElement> it = queryJsonArray.iterator(); it.hasNext();) {
                
                connection = getConnection(TokenBearer);
                
                jo = (JsonObject) it.next();
            
                CadenaXML = ConstruirXML(jo.get("cod_fe").getAsString(), "BQ", "1", "110000", con);
                
                DataOutputStream out = new DataOutputStream(connection.getOutputStream());             
                out.writeBytes(CadenaXML);
                out.flush();
                
                int status = connection.getResponseCode();

                if (status == 200) {

                    System.out.println("iterador: " + ++iterador + " - " + jo.get("cod_fe").getAsString() + " - " + status + " - " + connection.getResponseMessage() + "- " + connection.toString());

                    in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder content = new StringBuilder();
                    while ((inputLine = in.readLine()) != null) {
                        content.append(inputLine);
                    }
                    
                    in.close();

                    response = (JsonObject) new JsonParser().parse(content.toString());
                    
                    //METODO QUE ACTUALIZA LAS FACTURAS CON LA RESPUESTA DE LA API DE FACTURE
                    SalvarTransaccionFE(con, String.valueOf(status), jo.get("cod_fe").getAsString(), response.get("isSuccess").getAsString(), response.get("requestId").getAsString(), 
                                        response.get("UUID").getAsString(), response.get("URL").getAsString(), response.get("UrlPdf").getAsString(), CadenaXML);

                } else {
                    
                    System.out.println("iterador: " + ++iterador + " - " + jo.get("cod_fe").getAsString() + " - " + status + " - " + connection.getResponseMessage() + "- " + connection.toString());
                    
                    //METODO QUE ACTUALIZA LAS FACTURAS CON LA RESPUESTA DE LA API DE FACTURE
                    ErrorTransaccionFE(con, String.valueOf(status), jo.get("cod_fe").getAsString(), CadenaXML);
                    
                }

                out.close();
                if (connection != null) connection.disconnect();
                
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
            response = (JsonObject) new JsonParser().parse("{\"error\":{\"data\":{\"comentario\": \"Error en la comunicacion con Datacredito: " + ex.getMessage() + "\"}}}");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            
            desconectarPool(con, null, ps, null, this.getClass());
        }
        return response;
    }     
    
    private HttpURLConnection getConnection(String TokenBearer) throws MalformedURLException, ProtocolException, IOException {

        URL apiUrl = null;
        HttpURLConnection connection = null;
        //apiUrl = new URL("https://plcolabbeta.azure-api.net/Issue/xml");  //BETA
        apiUrl = new URL("https://plcolab-api.azure-api.net/Issue/xml");

        connection = (HttpURLConnection) apiUrl.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/xml");
        //connection.setRequestProperty("X-Who", "8012803358214116aa214caa18b2af3e"); //BETA
        connection.setRequestProperty("X-Who", "ee058b9cc0ea4d4c80693de8d05e0f90");
        connection.setRequestProperty("Authorization", TokenBearer);
        connection.setRequestProperty("X-REF-COCUMENTTYPE", "FACTURA-UBL");
        //connection.setRequestProperty("X-KEYCONTROL", "FDGFDGFDG"); //BETA
        connection.setRequestProperty("X-KEYCONTROL", "417b5eb85faab58a913fbd25cba962abfb991faf149896b3ec543476ab6cff87");
        connection.setRequestProperty("X-REF-DOCUMENTTYPE", "FA");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setUseCaches(false);

        return connection;

    }
    
    
    public String ConstruirXML(String FacturaEnviar,  String ResPrefijo, String ResNumeroInicio, String ResNumeroFin, Connection con ) {
        
        JsonObject jo = null;
        JsonObject joDet = null;
        String response = "";
        String tipo_impuesto_iva;
        String tipo_impuesto_retefuente;
        String tipo_impuesto_reteica;
        
        Double BaseImpuesto_iva;
        Double BaseImpuesto_retefuente;
        Double BaseImpuesto_reteica;

        Double Impuesto_iva;
        Double Impuesto_retefuente;
        Double Impuesto_reteica;
        
        Double PorcImpuesto_iva;
        Double PorcImpuesto_retefuente;
        Double PorcImpuesto_reteica;
        
        Double SumConceptoBase;
        Double SumImpuesto_iva;
        Double SumImpuesto_retefuente;
        Double SumImpuesto_reteica;
        
        Double Operacion;
        
        String query = "SQL_FACTURA_ELECTRONICA";
        String queryDet = "SQL_DETALLE_FACTURA";
        
        //String ResNumeroAprobacion = "44343342342"; //BETA
        String ResNumeroAprobacion = "18762011940664";
        String ResFechaInicio = "2018-12-20";
        String ResFechaFin = "2020-06-01";
        //String ResPrefijo = "FEL"; //BETA
        
        PreparedStatement psd = null;
        
        try {
            
            psd = con.prepareStatement(this.obtenerSQL(query));
            psd.setString(1, FacturaEnviar);

            JsonArray queryJsonArray = getQueryJsonArray(con, psd);
            psd.close();
            psd=null;
         
            response = "<Factura>\n";
                     
            for (Iterator<JsonElement> it = queryJsonArray.iterator(); it.hasNext();) {
                
                jo = (JsonObject) it.next();
                
                response += "<Cabecera Numero=\""+jo.get("cod_fe").getAsString()+"\" Fecha=\"" + jo.get("fecha_emision").getAsString() + "\" Vencimiento=\"" + jo.get("vencimiento_fe").getAsString() + "\" Hora=\"09:22:05\" Observaciones=\"NO APLICA\" TipoFactura=\"FACTURA-UBL\" />\n";
                
                response += "<Notificacion Tipo=\"Mail\" De=\"\">\n"
                          +     "<Para>"+jo.get("mail").getAsString()+"</Para>\n"
                          + "</Notificacion>\n";

                response += "<Emisor TipoPersona=\"1\" TipoRegimen=\"2\" TipoIdentificacion=\"31\" Numero=\"802022016\">\n"
                          +     "<Nombre RazonSocial=\"Fintra S.A\" Registrado=\"Fintra S.A\" />\n"
                          +     "<DireccionFisica Direccion=\"Calle 70 # 50 03 Local 2\" NombreDepartamento=\"Atlantico\" NombreCiudad=\"Barranquilla\" CodigoPais=\"CO\" />\n"
                          + "</Emisor>\n";
                
                response += "<Cliente TipoPersona=\"" + jo.get("tipo_persona").getAsString() + "\" TipoRegimen=\"" + jo.get("tipo_regimen").getAsString() + "\" TipoIdentificacion=\"" + jo.get("tipo_identificacion").getAsString() + "\" Numero=\"" + jo.get("cedula").getAsString() + "\">\n"
                          +     "<Nombre RazonSocial=\"" + jo.get("razon_social").getAsString() + "\" Registrado=\"" + jo.get("razon_social").getAsString() + "\" />\n"
                          +     "<DireccionFisica Direccion=\"" + jo.get("direccion").getAsString() + "\" NombreDepartamento=\"" + jo.get("departamento").getAsString() + "\" NombreCiudad=\"" + jo.get("ciudad").getAsString() + "\" CodigoPais=\"CO\" />\n"
                          +     "<PersonaNatural PrimerNombre=\"" + jo.get("primer_nombre").getAsString() + "\" SegundoNombre=\"" + jo.get("segundo_nombre").getAsString() + "\" Apellido=\"" + jo.get("apellido").getAsString() + "\" />\n"
                          +     "<Contacto Nombre=\"" + jo.get("razon_social").getAsString() + "\" Telefono=\"" + jo.get("telefono").getAsString() + "\" Mail=\"" + jo.get("mail").getAsString() + "\" />\n"
                          + "</Cliente>\n";
                
                psd = con.prepareStatement(this.obtenerSQL(queryDet));
                psd.setString(1, jo.get("id").getAsString());
                JsonArray queryJsonArrayDetalle = getQueryJsonArray(con, psd);
                
                BaseImpuesto_iva = 0.0;
                BaseImpuesto_retefuente = 0.0;
                BaseImpuesto_reteica = 0.0;
                
                Impuesto_iva = 0.0;
                Impuesto_retefuente = 0.0;
                Impuesto_reteica = 0.0;
                
                PorcImpuesto_iva = 0.0;
                PorcImpuesto_retefuente = 0.0;
                PorcImpuesto_reteica = 0.0;
                
                SumConceptoBase = 0.0;
                SumImpuesto_iva = 0.0;
                SumImpuesto_retefuente = 0.0;
                SumImpuesto_reteica = 0.0;
                
                
                //DETALLE DE FACTURA
                response += "<Detalle>\n";
                
                for (Iterator<JsonElement> itd = queryJsonArrayDetalle.iterator(); itd.hasNext();) {
                    
                    joDet = (JsonObject) itd.next();
                    
                    response += "<Linea Cantidad=\"1\" PrecioUnitario=\"" + joDet.get("valor_concepto").getAsString() + "\" SubTotalLinea=\"" + joDet.get("valor_concepto").getAsString() + "\" ValorTotalItem=\"" + joDet.get("total_factura").getAsString() + "\" Codigo=\"" + joDet.get("codigo_concepto").getAsString() + "\" UnidadMedida=\"Unidad\" Descripcion=\"" + joDet.get("descripcion_concepto").getAsString() + "\">\n";
                              
                    tipo_impuesto_iva = joDet.get("cod_tipo_iva").getAsString();
                    if ( !tipo_impuesto_iva.equals("")  ) {
                        response += "<Impuesto><Subtotal Tipo=\"01\" Porcentaje=\"" + joDet.get("porc_iva").getAsString() + "\" Valor=\"" + joDet.get("valor_iva").getAsString() + "\" /></Impuesto>\n";
                        
                        BaseImpuesto_iva = joDet.get("valor_concepto").getAsDouble();
                        PorcImpuesto_iva = joDet.get("porc_iva").getAsDouble();
                        Impuesto_iva = joDet.get("valor_iva").getAsDouble();
                        SumImpuesto_iva = SumImpuesto_iva + Impuesto_iva;
                    }
                              
                    tipo_impuesto_retefuente = joDet.get("cod_tipo_retefuente").getAsString();
                    if ( !tipo_impuesto_retefuente.equals("")  ) {
                        response += "<Impuesto><Subtotal Tipo=\"02\" Porcentaje=\"" + joDet.get("porc_retefuente").getAsString() + "\" Valor=\"" + joDet.get("valor_retefuente").getAsString() + "\" /></Impuesto>\n";
                        
                        BaseImpuesto_retefuente = joDet.get("valor_concepto").getAsDouble();
                        PorcImpuesto_retefuente = joDet.get("porc_iva").getAsDouble();
                        Impuesto_retefuente = joDet.get("porc_iva").getAsDouble();
                        SumImpuesto_retefuente = SumImpuesto_retefuente + Impuesto_retefuente;
                    }

                    tipo_impuesto_reteica = joDet.get("cod_tipo_reteica").getAsString();
                    if ( !tipo_impuesto_reteica.equals("")  ) {
                        response += "<Impuesto><Subtotal Tipo=\"03\" Porcentaje=\"" + joDet.get("porc_reteica").getAsString() + "\" Valor=\"" + joDet.get("valor_reteica").getAsString() + "\" /></Impuesto>\n";
                        
                        BaseImpuesto_reteica = joDet.get("valor_concepto").getAsDouble();
                        PorcImpuesto_reteica = joDet.get("porc_iva").getAsDouble();
                        Impuesto_reteica = joDet.get("porc_iva").getAsDouble();
                        SumImpuesto_reteica = SumImpuesto_reteica + Impuesto_reteica;
                    }
        
                    response += "<Descuento Porcentaje=\"0\" DescuentoValor=\"0\" />\n"
                              + "<Cargo Porcentaje=\"0\" CargoValor=\"0\" />\n"
                              + "</Linea>\n";
                    
                    SumConceptoBase = SumConceptoBase + joDet.get("valor_concepto").getAsDouble();
                }
                
                response += "</Detalle>\n";
                
                //IMPUESTOS
                response += "<Impuestos>\n";
                
                if ( PorcImpuesto_iva != 0 ) {
                    //response += "<Impuesto Valor=\""+Impuesto_iva+"\" Tipo=\"01\"><Subtotal ValorBase=\""+BaseImpuesto_iva+"\" Porcentaje=\""+PorcImpuesto_iva/100+"\" Valor=\""+Impuesto_iva+"\"/></Impuesto>\n";
                    response += "<Impuesto Valor=\""+Impuesto_iva+"\" Tipo=\"01\"><Subtotal ValorBase=\""+BaseImpuesto_iva+"\" Porcentaje=\""+PorcImpuesto_iva+"\" Valor=\""+Impuesto_iva+"\" /></Impuesto>\n";
                }
                
                if ( PorcImpuesto_retefuente != 0 ) {
                    response += "<Impuesto Valor=\""+Impuesto_retefuente+"\" Tipo=\"01\"><Subtotal ValorBase=\""+BaseImpuesto_retefuente+"\" Porcentaje=\""+PorcImpuesto_retefuente+"\" Valor=\""+Impuesto_retefuente+"\" /></Impuesto>\n";
                }

                
                if ( PorcImpuesto_reteica != 0 ) {
                    response += "<Impuesto Valor=\""+Impuesto_reteica+"\" Tipo=\"01\"><Subtotal ValorBase=\""+BaseImpuesto_reteica+"\" Porcentaje=\""+PorcImpuesto_reteica+"\" Valor=\""+Impuesto_reteica+"\" /></Impuesto>\n";
                }
                
                response += "</Impuestos>\n";
                
                //RETENCIONES
                response += "<Retenciones>\n"
                          +     "<Retencion Valor=\"0\" Tipo=\"07\"><Subtotal ValorBase=\"0\" Porcentaje=\"0\" Valor=\"0\" /></Retencion>\n"
                          +     "<Retencion Valor=\"0\" Tipo=\"06\"><Subtotal ValorBase=\"0\" Porcentaje=\"0\" Valor=\"0\" /></Retencion>\n"
                          +     "<Retencion Valor=\"0\" Tipo=\"05\"><Subtotal ValorBase=\"0\" Porcentaje=\"0\" Valor=\"0\" /></Retencion>\n"
                          + "</Retenciones>\n";
                
                //GENERALES
                response += "<Generales>\n"
                          +     "<Descuento Porcentaje=\"0\" DescuentoValor=\"0\" BaseValor=\"0\" />\n"
                          +     "<Cargo Porcentaje=\"0\" CargoValor=\"0\" BaseValor=\"0\" />\n"
                          + "</Generales>\n";
                
                //TOTALES
                Operacion = SumConceptoBase + SumImpuesto_iva;
                response += "<Totales Bruto=\""+SumConceptoBase+"\" Base=\""+SumConceptoBase+"\" Impuestos=\""+SumImpuesto_iva+"\" Descuentos=\"0\" Retenciones=\"0\" Cargos=\"0\" General=\""+Operacion+"\" />\n";
        
                
                //EXTENSIONES
                response += "<Extensiones>\n"
                          +     "<Numeracion NumeroAprobacion=\""+ResNumeroAprobacion+"\" FechaInicio=\""+ResFechaInicio+"\" FechaFin=\""+ResFechaFin+"\" Prefijo=\""+ResPrefijo+"\" NumeroInicio=\""+ResNumeroInicio+"\" NumeroFin=\""+ResNumeroFin+"\" />\n"
                          +     "<InformacionAdicional>\n"
                          +         "<campoAdicional nombre=\"Resolucion\" valor=\"Resolucion DIAN no "+ResNumeroAprobacion+" Fecha "+ResFechaInicio+" DEL No "+ResNumeroInicio+" AL No "+ResNumeroFin+" Habilitacion\" />\n"
                          +         "<campoAdicional nombre=\"Observaciones01\" valor=\"No somos autorretenedores / somos Grandes Contribuyentes Resolucion Dian No 000267 de diciembre 90 de 2014 / CIIU 6493 - Tarifa ICA 5*1000\" />\n"
                          +         "<campoAdicional nombre=\"Observaciones02\" valor=\"Esta factura asimila en sus efectos a una letra de cambio Art 774 del codigo de comercio\" />\n"
                          +         "<campoAdicional nombre=\"Condiciones_Pago\" valor=\"CONDICIONES DE PAGO: Favor consignar a la cuenta de ahorro de bancolombia no 69225845948 a nombre de Fintra SA Nit 802.022.016-1\" />\n"
                          +     "</InformacionAdicional>\n"
                          + "</Extensiones>\n";
                
            }
            response += "</Factura>\n";
            
            
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            desconectarPool(null, null, psd, null, this.getClass());
        }

        return response;

    }    
   
   
    private String ObtenerToken(String _CadURL, String _XWho, String _User, String _Passwd, String _TendantId) {
        
        String inputLine;
        URL apiUrl = null;
        HttpURLConnection connection = null;
        
        JsonObject response = new JsonObject();
        String respuesta = "";
        
        try {

            apiUrl = new URL(_CadURL);
            connection = (HttpURLConnection) apiUrl.openConnection();
            connection.setRequestMethod("POST");                    
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Who", _XWho);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);

            JsonObject paramsObjJson = new JsonObject();
            paramsObjJson.addProperty("u", _User);
            paramsObjJson.addProperty("p", _Passwd);
            paramsObjJson.addProperty("t", _TendantId);

            DataOutputStream out = new DataOutputStream(connection.getOutputStream());
            out.writeBytes(new Gson().toJson(paramsObjJson));
            out.flush();
            out.close();

            int status = connection.getResponseCode();
            
            if (status == 200) {
                
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuffer content = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                response = (JsonObject) new JsonParser().parse(content.toString());
                respuesta = response.get("accessToken").getAsString();

                in.close();
                
                
            } else {
                throw new IOException("Response code " + status);
            }            

        } catch (Exception e) {
            //respuesta = new JsonObject();
            //respuesta.addProperty("error", e.getMessage());
            respuesta = "error: " + e.getMessage();
        } finally {
            return respuesta;
        }
    }

    
    private void SalvarTransaccionFE (Connection con, String estatus, String _FacturaElectronica, String _isSuccess, String _requestId, String _UUID, String _URL, String _UrlPdf, String _XmlResponse) {
        
        String response = "";        
        String query = "SQL_ACTUALIZAR_FE";        
        PreparedStatement ps = null;
        
        try {
            
            ps = con.prepareStatement(this.obtenerSQL(query));
            
            ps.setString(1, _isSuccess);
            ps.setString(2, _requestId);
            ps.setString(3, _UUID);
            ps.setString(4, _URL);
            ps.setString(5, _UrlPdf);
            ps.setString(6, estatus);
            ps.setString(7, _XmlResponse);
            ps.setString(8, _FacturaElectronica);
            
            ps.executeUpdate();
            
        } catch (Exception e) {
            response = "error: " + e.getMessage();
            System.out.println(e.getMessage());
        }
        
    }

    private void ErrorTransaccionFE (Connection con, String estatus, String _FacturaElectronica, String _XmlResponse) {
        
        String response = "";        
        String query = "SQL_ERROR_FE";        
        PreparedStatement ps = null;
        
        try {
            
            ps = con.prepareStatement(this.obtenerSQL(query));
            
            ps.setString(1, estatus);
            ps.setString(2, _XmlResponse);
            ps.setString(3, _FacturaElectronica);
            
            ps.executeUpdate();
            
        } catch (Exception e) {
            response = "error: " + e.getMessage();
            System.out.println(e.getMessage());
        }
        
    }
    


}