/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.encrypted.PGPFileProcessor;
import com.tsp.operation.model.DAOS.InterfazLogisticaDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.BeansAgencia;
import com.tsp.operation.model.beans.BeansAnticipo;
import com.tsp.operation.model.beans.BeansConductor;
import com.tsp.operation.model.beans.BeansPropietario;
import com.tsp.operation.model.beans.EDSPropietarioBeans;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.AnticiposPagosTercerosServices;
import com.tsp.util.Util;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import com.tsp.operation.model.services.DirectorioService;
import static com.tsp.operation.model.threads.HArchivosTransferenciaBancos.Trunc;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

/**
 *
 * @author egonzalez
 */
public class LogisticaAdminImpl extends MainDAO implements InterfazLogisticaDAO {

    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    StringStatement st;

    private final String BANCOLOMBIAPAB = "7B";
    private final String BANCOLOMBIA_CREDIPAGO = "CPAG";
    private final String CERO = "0";
    private final String DERECHA = "R";
    private final String IZQUIERDA = "L";
    private final String SPACE = " ";
    private final String PASSPHRASE = "fintra123456";

    public LogisticaAdminImpl(String dataBaseName) {
        super("LogisticaAdminDAO.xml",dataBaseName);
    }

    @Override
    public String getTransportadorasLista(Usuario user) {

        con = null;
        ps = null;
        rs = null;
        String query = "OBTENER_TRANSPORTADORAS";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                lista = new JsonArray();

                jsonObject = new JsonObject();
                jsonObject.addProperty("id", "");
                jsonObject.addProperty("razon_social", "Seleccione");
                lista.add(jsonObject);

                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("id", rs.getInt("id"));
                    jsonObject.addProperty("razon_social", rs.getString("razon_social"));

                    if (user.getLogin().equals(rs.getString("idusuario"))) {
                        lista = new JsonArray();
                        lista.add(jsonObject);
                        break;
                    } else {
                        lista.add(jsonObject);
                    }

                    // lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            lista = null;

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String getTitulosGrid() {

        con = null;
        ps = null;
        rs = null;
        String query = "OBTENER_COLUMNAS_JQGRID";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("nombre", rs.getString("nombre"));
                    jsonObject.addProperty("tipo", rs.getString("tipo"));
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String getReportProduction(String transportadora, String fecha_inicio, String fecha_fin, String c_conductor, String c_propietario, String planilla, String placa, String factura) {

        con = null;
        ps = null;
        rs = null;
        String query = "GET_REPORT_PRODUCTION";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            //Construimos el filtro.
            String filtro = "";
            if (!fecha_inicio.equals("") && !fecha_fin.equals("") && !transportadora.equals("")) {
                filtro = "WHERE fecha_creacion_fintra::date between '" + fecha_inicio + "'::date AND '" + fecha_fin + "'::date AND id_transportadora=" + transportadora;
            } else if (!transportadora.equals("")) {
                filtro = "WHERE id_transportadora=" + transportadora;
            } else if (!fecha_inicio.equals("") && !fecha_fin.equals("")) {
                filtro = "WHERE fecha_creacion_fintra::date between '" + fecha_inicio + "'::date AND '" + fecha_fin + "'::date";
            }
            if (!planilla.equals("") && !filtro.equals("")) {
                filtro += " AND planilla = '" + planilla + "'";
            } else if (filtro.equals("") && !planilla.equals("")) {
                filtro = "WHERE planilla = '" + planilla + "'";
            }
            if (!c_conductor.equals("") && !filtro.equals("")) {
                filtro += " AND nit_conductor = '" + c_conductor + "'";
            } else if (filtro.equals("") && !c_conductor.equals("")) {
                filtro = "WHERE nit_conductor = '" + c_conductor + "'";
            }
            if (!c_propietario.equals("") && !filtro.equals("")) {
                filtro += " AND nit_propietario = '" + c_propietario + "'";
            } else if (filtro.equals("") && !c_propietario.equals("")) {
                filtro = "WHERE nit_propietario = '" + c_propietario + "'";
            }
            if (!factura.equals("") && !filtro.equals("")) {
                filtro += " AND numero_egreso = '" + factura + "'";
            } else if (filtro.equals("") && !factura.equals("")) {
                filtro = "WHERE numero_egreso = '" + factura + "'";
            }
            if (!placa.equals("") && !filtro.equals("")) {
                filtro += " AND placa = '" + placa + "'";
            } else if (filtro.equals("") && !placa.equals("")) {
                filtro = "WHERE placa = '" + placa + "'";
            }

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query).replace("#filtro", filtro));
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }

    }

    @Override
    public String getReportExtractEds(Usuario user, String fecha_inicio, String fecha_fin, String nombre, String nit) {
        con = null;
        ps = null;
        rs = null;
        String query = "GET_REPORT_EXTRACT_EDS";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            //Construimos el filtro.
            String filtro = "";
            if (!fecha_inicio.equals("") && !fecha_fin.equals("") && !nombre.equals("")) {
                filtro = "WHERE  venta.fecha_venta::date between '" + fecha_inicio + "'::date AND '" + fecha_fin + "'::date AND venta.creation_user='" + nombre + "'";
            } else if (!nombre.equals("")) {
                filtro = "WHERE venta.creation_user='" + nombre + "'";
            } else if (!fecha_inicio.equals("") && !fecha_fin.equals("")) {
                filtro = "WHERE venta.fecha_venta::date  between '" + fecha_inicio + "'::date AND '" + fecha_fin + "'::date";
            }

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query).replace("#filtro", filtro));
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }
            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String loadEds(Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "GET_EDS_NAME";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                lista = new JsonArray();
                jsonObject = new JsonObject();
                jsonObject.addProperty("idusuario", "");
                jsonObject.addProperty("nombre_eds", "");
                lista.add(jsonObject);

                while (rs.next()) {

                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }

                    if (user.getLogin().equals(rs.getString("idusuario"))) {
                        lista = new JsonArray();
                        lista.add(jsonObject);
                        break;
                    } else {
                        lista.add(jsonObject);
                    }
                }
            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String getReportExtactOwner(Usuario user, String propietario,
            String placa, String planilla, String fecha_inicio, String fecha_fin) {
        con = null;
        ps = null;
        rs = null;
        String query = "GET_REPORT_EXTRACT_OWNER";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        JsonObject jsonObjectResponse = null;
        double valor_venta = 0;
        try {
            con = this.conectarJNDI(query);

            //Construimos el filtro.
            String filtro = "";
            if (!propietario.equals("") && !placa.equals("") && !planilla.equals("")) {
                filtro = "and vehiculo.placa='" + placa + "'\n"
                        + "and upper(anticipo.planilla)=upper('" + planilla + "')	\n"
                        + "and venta.fecha_venta::date between '" + fecha_inicio + "'::date and '" + fecha_fin + "'::date\n"
                        + "and vehiculo.id_propietario = '" + propietario + "'::integer";
            } else if (!planilla.equals("") && !propietario.equals("")) {
                filtro = "and upper(anticipo.planilla)=upper('" + planilla + "')	\n"
                        + "and venta.fecha_venta::date between '" + fecha_inicio + "'::date and '" + fecha_fin + "'::date\n"
                        + "and vehiculo.id_propietario = '" + propietario + "'::integer";
            } else if (!placa.equals("") && !propietario.equals("")) {
                filtro = "and vehiculo.placa='" + placa + "'\n"
                        + "and venta.fecha_venta::date between '" + fecha_inicio + "'::date and '" + fecha_fin + "'::date\n"
                        + "and vehiculo.id_propietario = '" + propietario + "'::integer";
            } else if (!propietario.equals("")) {
                filtro = "and venta.fecha_venta::date between '" + fecha_inicio + "'::date and '" + fecha_fin + "'::date\n"
                        + "and vehiculo.id_propietario = '" + propietario + "'::integer";
            } else if (!planilla.equals("")) {
                filtro = "and upper(anticipo.planilla)=upper('" + planilla + "')	\n"
                        + "and venta.fecha_venta::date between '" + fecha_inicio + "'::date and '" + fecha_fin + "'::date";
            } else {
                filtro = "and venta.fecha_venta::date between '" + fecha_inicio + "'::date and '" + fecha_fin + "'::date";
            }

            ps = con.prepareStatement(" CREATE TABLE tem.extracto_propietario_" + user.getLogin() + "\n"
                    + "   (\n"
                    + "      id serial,\n"
                    + "      num_venta varchar(80) ,\n"
                    + "      planilla varchar(80),\n"
                    + "      cedula varchar(80),\n"
                    + "      placa varchar(80),\n"
                    + "      valor_venta numeric(11,2)\n"
                    + "   );");
            ps.executeUpdate();

            if (con != null) {

                ps = con.prepareStatement(this.obtenerSQL(query).replace("#filtro", filtro));
                ps.setString(1, user.getLogin());
                rs = ps.executeQuery();

                lista = new JsonArray();
                ArrayList<String> myPlanillaList = new ArrayList();
                String myPlanilla = "";
                while (rs.next()) {

                    if (!myPlanilla.equals(rs.getString("planilla"))) {
                        myPlanilla = rs.getString("planilla");
                        myPlanillaList.add(myPlanilla);
                    }
                    //esto es para llenar el objeto interno.
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));

                    }
                    lista.add(jsonObject);
                }

                //Borramos la tabla temporal         
                ps = con.prepareStatement("DROP TABLE tem.extracto_propietario_" + user.getLogin());
                ps.executeUpdate();

                jsonObjectResponse = new JsonObject();
                JsonArray listaPlanilla = null;
                int contador = 0;
                for (Object myPlanillaList1 : myPlanillaList) {
                    String manifiesto = (String) myPlanillaList1;
                    listaPlanilla = new JsonArray();
                    JsonObject objects = new JsonObject();
                    for (int i = 0; i < lista.size(); i++) {
                        objects = (JsonObject) lista.get(i);
                        if (objects.get("planilla").getAsString().equals(manifiesto)) {
                            valor_venta = valor_venta + objects.get("valor_venta").getAsDouble();
                            listaPlanilla.add(objects);
                            contador++;
                        }
                    }
                    objects = new JsonObject();
                    objects.addProperty("planilla", manifiesto);
                    objects.addProperty("total_venta", valor_venta);
                    objects.addProperty("rowspan", contador);
                    objects.addProperty("placa", listaPlanilla.get(0).getAsJsonObject().get("placa").getAsString());
                    objects.addProperty("nombre_conductor", listaPlanilla.get(0).getAsJsonObject().get("nombre_conductor").getAsString());
                    objects.addProperty("cedula_conductor", listaPlanilla.get(0).getAsJsonObject().get("cedula_conductor").getAsString());
                    objects.addProperty("valor_anticipo", listaPlanilla.get(0).getAsJsonObject().get("valor_anticipo").getAsString());
                    objects.addProperty("descuento", listaPlanilla.get(0).getAsJsonObject().get("descuento").getAsString());
                    objects.addProperty("valor_planilla", listaPlanilla.get(0).getAsJsonObject().get("valor_planilla").getAsString());
                    objects.add("detalle", listaPlanilla);

                    jsonObjectResponse.add(manifiesto, objects);
                    contador = 0;
                    valor_venta = 0;
                }

            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(jsonObjectResponse);
        }
    }

    @Override
    public String loadOwner(Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "GET_OWNER_NAME";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                lista = new JsonArray();
                jsonObject = new JsonObject();
                jsonObject.addProperty("id", "");
                jsonObject.addProperty("nit_propietario", "");
                jsonObject.addProperty("nombre", "");
                lista.add(jsonObject);

                while (rs.next()) {

                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }

                    if (user.getCedula().equals(rs.getString("nit_propietario"))) {
                        lista = new JsonArray();
                        lista.add(jsonObject);
                        break;
                    } else {
                        lista.add(jsonObject);
                    }
                }
            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String loadPlaca(int id_propietario) {
        con = null;
        ps = null;
        rs = null;
        String query = "GET_PLACA";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setInt(1, id_propietario);
                rs = ps.executeQuery();
                lista = new JsonArray();
                jsonObject = new JsonObject();
                jsonObject.addProperty("placa", "");
                lista.add(jsonObject);
                while (rs.next()) {

                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }

                    lista.add(jsonObject);
                }
            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String aprobateAavances(int id_transportadora) {
        con = null;
        ps = null;
        rs = null;
        String query = "APPROBATE_ADVANCES";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        String filtro = "";
        try {
            if (id_transportadora != 0) {
                filtro = "WHERE id=" + id_transportadora;
            }

            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query).replace("#filtro", filtro));
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {

                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }

                    lista.add(jsonObject);
                }
            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String aprobarTransferencias(JsonArray array, Usuario u) {
        con = null;
        ps = null;
        rs = null;
        boolean retorno = false;
        String respuesta = "{}";
        String query = "UPDATE_ANTICIPOS_TRANSFERENCIAS";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                Array anticipos = con.createArrayOf("text", getStringArray(array, 1));
                ps.setArray(1, anticipos);
                ps.setString(2, u.getLogin());
                rs = ps.executeQuery();
                if (rs.next()) {
                    retorno = rs.getBoolean("retorno");
                }

                if (retorno) {
                    respuesta = "{\"respuesta\":\"OK\"}";
                } else {
                    respuesta = "{\"error\":\"error\"}";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            respuesta = "{\"error\":\"error\"}";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuesta;
        }
    }

    private String[][] getStringArray(JsonArray array, int option) {
        String multiArray[][] = new String[array.size()][];
        for (int i = 0; i < array.size(); i++) {
            JsonObject objects = (JsonObject) array.get(i);
            multiArray[i] = elementosArray(objects, option);
        }

        return multiArray;
    }

    private String[] elementosArray(JsonObject objects, int opcion) {

        String[] elementos = null;
        switch (opcion) {
            case 1:
                elementos = new String[12];
                elementos[0] = objects.get("transportadora").getAsString();
                elementos[1] = objects.get("id_manifiesto").getAsString();
                elementos[2] = objects.get("nombre_agencia").getAsString();
                elementos[3] = objects.get("conductor").getAsString();
                elementos[4] = objects.get("propietario").getAsString();
                elementos[5] = objects.get("placa").getAsString();
                elementos[6] = objects.get("planilla").getAsString();
                elementos[7] = objects.get("fecha_anticipo").getAsString();
                elementos[8] = objects.get("valor_anticipo").getAsString();
                elementos[9] = objects.get("usuario_creacion").getAsString();
                elementos[10] = objects.get("descripcion").getAsString();
                elementos[11] = objects.get("reanticipo").getAsString();

                break;

        }
        return elementos;

    }

    private boolean validarTransferencia(Usuario u) {
        con = null;
        ps = null;
        rs = null;
        String query = "VALIDAR_TRANSFERENCIA";
        boolean estado = false;
        try {

            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, u.getLogin());
                rs = ps.executeQuery();
                if (rs.next()) {
                    estado = rs.getBoolean("retorno");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return estado;
        }

    }

    @Override
    public String buscarAnticiporTransferencia(int id_transportadora, String banco, String cod_banco, String cuenta, String tipo_cuenta, Usuario u) {
        con = null;
        ps = null;
        rs = null;
        String query = "GET_ANTICIPOS_TRANS";
        JsonArray lista = new JsonArray();
        JsonObject jsonObject = null;
        try {
            if (this.validarTransferencia(u)) {

                con = this.conectarJNDI(query);
                if (con != null) {
                    ps = con.prepareStatement(this.obtenerSQL(query));
                    ps.setString(1, banco);
                    ps.setString(2, cod_banco);
                    ps.setString(3, cuenta);
                    ps.setString(4, tipo_cuenta);
                    ps.setInt(5, id_transportadora);
                    ps.setString(6, u.getLogin());
                    rs = ps.executeQuery();

                    while (rs.next()) {

                        jsonObject = new JsonObject();
                        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                        }

                        lista.add(jsonObject);
                    }
                }
            } else {
                jsonObject = new JsonObject();
                jsonObject.addProperty("respuesta", "error");
                lista.add(jsonObject);
            }

        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String desAprobarAnticipos(JsonArray array, Usuario u) {
        con = null;
        ps = null;
        rs = null;
        boolean retorno = false;
        String respuesta = "{}";
        String query = "DESAPROBAR_TRANSFERENCIA";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                Array anticipos = con.createArrayOf("text", getStringArray(array, 1));
                ps.setArray(1, anticipos);
                ps.setString(2, u.getLogin());
                rs = ps.executeQuery();
                if (rs.next()) {
                    retorno = rs.getBoolean("retorno");
                }

                if (retorno) {
                    respuesta = "{\"respuesta\":\"OK\"}";
                } else {
                    respuesta = "{\"error\":\"error\"}";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            respuesta = "{\"error\":\"error\"}";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuesta;
        }
    }

    @Override
    public String anularAnticipos(JsonArray array, Usuario u) {
        con = null;
        ps = null;
        rs = null;
        boolean retorno = false;
        String respuesta = "{}";
        String query = "ANULAR_ANTICIPOS";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                Array anticipos = con.createArrayOf("text", getStringArray(array, 1));
                ps.setArray(1, anticipos);
                ps.setString(2, u.getLogin());
                rs = ps.executeQuery();
                if (rs.next()) {
                    retorno = rs.getBoolean("retorno");
                }

                if (retorno) {
                    respuesta = "{\"respuesta\":\"OK\"}";
                } else {
                    respuesta = "{\"error\":\"error\"}";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            respuesta = "{\"error\":\"error\"}";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuesta;
        }
    }

    @Override
    public synchronized String transferir(JsonArray array, Usuario u, String[] banco) {
        String retorno = "{}";
        //agrupamos la lista de la transferencia.
        JsonArray agruparListaTransferencia = agruparListaTransferencia(array);
        if (agruparListaTransferencia != null) {
            try {
                //se genra el archivo plano
                this.createFile(agruparListaTransferencia, banco[1], banco[0], banco[2], banco[3], u);
                //marcamos los registros como procesados.
                this.marcarAnticiposProcesados(array);
                retorno = "{\"respuesta\":\"OK\"}";
            } catch (Exception ex) {
                retorno = "{\"respuesta\":\"Error generando el archivo..)\"}";
            }
        } else {
            retorno = "{\"respuesta\":\"Lo sentimos no se pudieron agrupar los anticipos.\"}";
        }

        return retorno;
    }

    private JsonArray agruparListaTransferencia(JsonArray array) {
        con = null;
        ps = null;
        rs = null;
        String query = "AGRUPAR_TRANSFERENCA";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        String filtroId = "(";
        try {
            for (int i = 0; i < array.size(); i++) {
                JsonObject objects = (JsonObject) array.get(i);
                if (i == (array.size() - 1)) {
                    filtroId += objects.get("id").getAsInt() + ")";
                } else {
                    filtroId += objects.get("id").getAsInt() + ",";
                }
            }
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, filtroId);
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return lista;
        }
    }

    @Override
    public String reporteProduccionTrans(String transportadora, String fecha_inicio, String fecha_fin) {
        con = null;
        ps = null;
        rs = null;
        String query = "GET_REPORT_PRODUCTION_TRANS";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            //Construimos el filtro.
            /*String filtro = "";
            if (!fecha_inicio.equals("") && !fecha_fin.equals("") && !transportadora.equals("")) {
                filtro = "WHERE fecha_creacion_fintra::date between '" + fecha_inicio + "'::date AND '" + fecha_fin + "'::date AND id_transportadora=" + transportadora;
            } else if (!transportadora.equals("")) {
                filtro = "WHERE id_transportadora=" + transportadora;
            } else if (!fecha_inicio.equals("") && !fecha_fin.equals("")) {
                filtro = "WHERE fecha_creacion_fintra::date between '" + fecha_inicio + "'::date AND '" + fecha_fin + "'::date";
            }*/

            if (con != null) {
                //ps = con.prepareStatement(this.obtenerSQL(query).replace("#filtro", filtro));
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, transportadora);
                ps.setString(2, fecha_inicio);
                ps.setString(3, fecha_fin);
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }

    }

    private void marcarAnticiposProcesados(JsonArray array){
        con = null;
        ps = null;
       String filtroId="(";
        String query = "MARCAR_ANTICIPOS_PROCESADOS";
        for (int i = 0; i < array.size(); i++) {
            JsonObject objects = (JsonObject) array.get(i);
            if (i == (array.size() - 1)) {
                filtroId += objects.get("id").getAsInt() + ")";
            } else {
                filtroId += objects.get("id").getAsInt() + ",";
            }
        }
        try {
            con = this.conectarJNDI(query);

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query).replace("#filtro",filtroId));
                ps.executeUpdate();    
            }
        } catch (SQLException e) {         
            e.printStackTrace();
        } finally {           
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private synchronized void createFile(JsonArray array, String banco_trans, String cod_banco, String cta, String tipo_cta, Usuario usuario) throws Exception {
        try {
            //nombre del archivo 
            String nameFile = getNombreArchivo(banco_trans.replace(" ", "_"));
            DirectorioService ds = new DirectorioService();
            ds.create(usuario.getLogin());
            String ruta = ds.getUrl() + usuario.getLogin() + "/" + nameFile;

            //Configuracion de variables archivo encriptado
            String NAME_FILE = nameFile;
            String E_INPUT = ruta;
            String E_OUTPUT = ruta + ".pgp";
            String E_KEY_FILE = ds.getRuta() + "/pgp/pubring.pkr";

            FileOutputStream fw = new FileOutputStream(ruta);
            BufferedWriter bf = new BufferedWriter(new OutputStreamWriter(fw, "ISO-8859-1"));
            PrintWriter linea = new PrintWriter(bf);

            //tipo de transaccion 
            String tipoTransaccion = "220";
            if (cod_banco.equals(BANCOLOMBIAPAB) && tipo_cta.equals("")) {
                tipoTransaccion = "320";
            }

            //sumamaos el total de la transferencia.
            double totalCredito = 0;
            for (int i = 0; i < array.size(); i++) {
                JsonObject object = array.get(i).getAsJsonObject();
                totalCredito += object.get("vlr_consignacion").getAsDouble();
            }
            //escribimos cabecera del archivo
            writeHeaderFile(linea, cod_banco, tipoTransaccion, cta, tipo_cta, totalCredito, array.size(), usuario);
            //escribimos detalle del archivo.
            for (int i = 0; i < array.size(); i++) {
                JsonObject object = array.get(i).getAsJsonObject();
                bancolombiaPABdet(object, linea, usuario);
            }

            //cerramos el archivo.
            linea.close();
            this.EncriptarArchivo(NAME_FILE, E_INPUT, E_OUTPUT, PASSPHRASE, E_KEY_FILE, ds.getRuta());

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private void writeHeaderFile(PrintWriter linea, String cod_banco, String tipo_trans,
            String cta, String tipo_cuenta, double total_dredito, int total_rows, Usuario usuario) throws Exception {

        switch (cod_banco) {
            case BANCOLOMBIAPAB:
                this.bancolombiaPab(linea, cod_banco, tipo_trans, cta, tipo_cuenta, total_dredito, total_rows, usuario);
                break;
        }

    }

    private void writeDetailFile(PrintWriter linea, String cod_banco, JsonObject object, Usuario usuario) throws Exception {
        switch (cod_banco) {
            case BANCOLOMBIAPAB:
                bancolombiaPABdet(object, linea, usuario);
                break;
        }

    }

    private void bancolombiaPab(PrintWriter linea, String cod_banco, String tipo_transaccion,
            String cta, String tipo_cta, double total_credito, int total_rows, Usuario usuario) throws Exception {

        AnticiposPagosTercerosServices apts = new AnticiposPagosTercerosServices(usuario.getBd());
        //esto hay que normalizarlo en la bd.
        final String BANCOLOMBIA_TIPO_REGISTRO = "1";
        final String BANCOLOMBIAPAB_TIPO_APLICACION = "I"; // I=inmediata
        final String BANCOLOMBIA_PROPOSITO = "PAGFINTRA";
        final double TOTALDEBITO = 0;
        String secuenciaPAB = apts.obtenerSecuenciaBanco(BANCOLOMBIAPAB, usuario.getLogin());

        String dato
                = BANCOLOMBIA_TIPO_REGISTRO + // 1.1  Tipo de Registro
                this.rellenar(apts.getProveedorUser(usuario.getLogin()), CERO, 15, IZQUIERDA) + // 1.2  Nit entidad que envia
                BANCOLOMBIAPAB_TIPO_APLICACION + // 1.3  Tipo de aplicacion
                this.rellenar(SPACE, SPACE, 15, DERECHA) +// 1.4  Filler                             
                tipo_transaccion + // 1.5  Clase de transaccion, segun formato es 220 pago proveedores
                this.rellenar(BANCOLOMBIA_PROPOSITO, SPACE, 10, DERECHA) + // 1.6  Proposito descripci�n
                this.getFecha("yyyyMMdd") + // 1.7  Fecha transacci�n  AAAAMMDD                             
                rellenar(secuenciaPAB, SPACE, 2, DERECHA) + // 1.8  Secuencia del archivo del dia en letra A.B.C....
                this.getFecha("yyyyMMdd") + // 1.9  Fecha aplicaci�n  AAAAMMDD
                rellenar(String.valueOf(total_rows), CERO, 6, IZQUIERDA) + // 1.10  N�mero de registros de detalle
                rellenar(String.valueOf((int) TOTALDEBITO) + "00", CERO, 17, IZQUIERDA) + // 1.11 Sumatoria debito
                rellenar(String.valueOf((int) total_credito) + "00", CERO, 17, IZQUIERDA) + // 1.12 Sumatoria credito
                this.rellenar(cta, CERO, 11, IZQUIERDA) + // 1.13 Cta Cliente
                (tipo_cta.equals("CC") ? "D" : "S") + // 1.14 Tipo Cta  S : aho  /  D : cte
                rellenar(SPACE, SPACE, 149, DERECHA);// 1.15  Filler
        linea.println(dato);
    }

    private void bancolombiaPABdet(JsonObject transf, PrintWriter linea, Usuario usuario) throws Exception {
        try {
            final String BANCOLOMBIA_PROPOSITO = "PAGFINTRA";
            final String BANCOLOMBIA_TIPO_REGISTRO_DET = "6";
            AnticiposPagosTercerosServices apts = new AnticiposPagosTercerosServices(usuario.getBd());
            // 2. DETALLE DE TRANSACCION:
            String tipoCta = transf.get("tipo_cuenta").getAsString();

            // Codigo del  banco del beneficiario
            String codigoBanco = CERO;
            Hashtable infoBanco = apts.getInfoBanco("BANCOLOMBI", transf.get("banco").getAsString());
            if (infoBanco == null) {
                throw new Exception("No hay informaci�n del banco  " + transf.get("banco").getAsString() + " en formato bancolombia");
            } else {
                codigoBanco = (String) infoBanco.get("descripcion");
            }

            // Indicador de pago: sucursal
            String indicador_pago = "S";

            // Tipo de transacion:
            String tipo_transaction = equivalenciaTipoTransaction(tipoCta);

            // Valor que le restamos 1 si ya tiene una trasnferencia en los ultimos cuatro dias
            int valor = (int) transf.get("vlr_consignacion").getAsDouble();

            String dato
                    = BANCOLOMBIA_TIPO_REGISTRO_DET + // 1  Tipo de Registro detalle
                    rellenar(transf.get("nit_cuenta").getAsString(), SPACE, 15, DERECHA) + // 2  Nit beneficiario
                    rellenar(transf.get("nombre_cuenta").getAsString(), SPACE, 30, DERECHA) + // 3  Nombre del beneficiario
                    rellenar(codigoBanco, CERO, 9, IZQUIERDA) + // 4  Codigo banco
                    rellenar(transf.get("cuenta").getAsString(), SPACE, 17, DERECHA) + // 5  N�mero de la Cta
                    indicador_pago + // 6  Lugar de pago
                    tipo_transaction + // 7  Tipo de Transaction
                    rellenar(String.valueOf(valor) + "00", CERO, 17, IZQUIERDA) + // 8  Valor
                    this.getFecha("yyyyMMdd") + // 9  Fecha aplicaci�n  AAAAMMDD
                    rellenar(BANCOLOMBIA_PROPOSITO, SPACE, 9, DERECHA) + // 10  Proposito descripci�n
                    rellenar(transf.get("egreso_item").getAsString(), SPACE, 12, DERECHA) + // 11 referencia                       
                    rellenar(SPACE, SPACE, 1, DERECHA) + // 12 Tipo de documento de identificaci�n
                    rellenar(SPACE, SPACE, 5, DERECHA) + // 13 Oficina de entrega
                    rellenar(SPACE, SPACE, 15, DERECHA) + // 14 N�mero de Fax
                    rellenar(SPACE, SPACE, 80, DERECHA) + // 15 E-mail beneficiario
                    rellenar(SPACE, SPACE, 15, DERECHA) + // 16 N�mero identificaci�n del autorizado
                    rellenar(SPACE, SPACE, 27, DERECHA);   // 17 Filler

            linea.println(dato);

        } catch (Exception e) {
            throw new Exception(" bancolombia " + e.getMessage());
        }
    }

    private String rellenar(String cadena, String caracter, int tope, String posicion) throws Exception {
        try {
            int lon = cadena.length();
            if (tope > lon) {
                for (int i = lon; i < tope; i++) {
                    if (posicion.equals(DERECHA)) {
                        cadena += caracter;
                    } else {
                        cadena = caracter + cadena;
                    }
                }
            } else {
                cadena = Trunc(cadena, tope);
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return cadena;
    }

    private String getFecha(String formato) throws Exception {
        String Fecha = "";
        try {

            SimpleDateFormat FMT = null;
            FMT = new SimpleDateFormat(formato);
            Fecha = FMT.format(new Date());

        } catch (Exception e) {
            throw new Exception("getFecha " + e.getMessage());
        }
        return Fecha.toUpperCase();
    }

    /**
     * M�todo que permite crear nombre del archivo
     *
     * @param desBanco
     * @return
     * @throws java.lang.Exception
     * @autor.......egonzalez
     * @throws......Exception
     * @version.....1.0.
     *
     */
    private String getNombreArchivo(String desBanco) throws Exception {
        String name = "";
        try {
            String hora = Util.getFechaActual_String(6).replaceAll("/|:", "").replaceAll(" ", "_");
            name = desBanco + "_" + hora + ".txt";
        } catch (Exception e) {
            throw new Exception("getNombreArchivo " + e.getMessage());
        }
        return name;
    }

    public String equivalenciaTipoTransaction(String tipo) {
        String equivale = tipo;

        if (tipo.equals("CA")) {
            equivale = "37";
        }
        if ((tipo.equals("CC")) || (tipo.equals(BANCOLOMBIA_CREDIPAGO))) {
            equivale = "27";
        }
        if (tipo.equals("EF")) {
            equivale = "26";
        }

        return equivale;
    }

    public void EncriptarArchivo(String NAME_FILE, String E_INPUT, String E_OUTPUT,
            String PASSPHRASE, String E_KEY_FILE, String ruta) throws Exception {

        PGPFileProcessor p = new PGPFileProcessor();
        p.setInputFileName(E_INPUT);
        p.setOutputFileName(E_OUTPUT);
        p.setPassphrase(PASSPHRASE);
        p.setPublicKeyFileName(E_KEY_FILE);
        if (p.encrypt()) {

            String archivoSalida = ruta + "/copiaFileBanco/" + NAME_FILE;
            File filein = new File(E_INPUT);
            File filecopy = new File(archivoSalida);
            copyFile(filein, filecopy);
            filein.delete();

        } else {

            System.err.println("Error generando archivo plano encriptado.");
        }

    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel origen = null;
        FileChannel destino = null;
        try {
            origen = new FileInputStream(sourceFile).getChannel();
            destino = new FileOutputStream(destFile).getChannel();

            long count = 0;
            long size = origen.size();
            while ((count += destino.transferFrom(origen, count, size - count)) < size);
        } finally {
            if (origen != null) {
                origen.close();
            }
            if (destino != null) {
                destino.close();
            }
        }
    }

    @Override
    public String cuentaCobroTransportadora(int transportadora, String fecha_corrida, Usuario u) {
        con = null;
        ps = null;
        rs = null;
        String query = "GET_LISTA_ANTICIPOS_SIN_CXC";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query, u.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setInt(1, transportadora);
                ps.setString(2, fecha_corrida);
                 ps.setInt(3, transportadora);
                ps.setString(4, fecha_corrida);               
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }
            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String generarCuentaCobroTransportadora(int transportadora, String fecha_corrida, Usuario u) {
        con = null;
        ps = null;
        rs = null;
        String query = "GENERAR_CXC_TRANSPORTADORA";
        JsonObject jsonObject = new JsonObject();
        try {
            con = this.conectarJNDI(query, u.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setInt(1, transportadora);
                ps.setString(2, fecha_corrida);
                ps.setString(3, u.getLogin());
                rs = ps.executeQuery();
                if (rs.next()) {
                    if (rs.getBoolean("retorno")) {
                        jsonObject.addProperty("respuesta", "OK");
                    }else{
                        jsonObject.addProperty("respuesta", "ERROR");
                    }
                }
            }
        } catch (SQLException e) {
            jsonObject.addProperty("respuesta", "fallo");
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(jsonObject);
        }
    }

    @Override
    public String detalleCorridaCXCTransportadora(int transportadora, String fecha_corrida, Usuario u) {
        con = null;
        ps = null;
        rs = null;
        String query = "DETALLE_CORRIDA_CXC";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query, u.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setInt(1, transportadora);
                ps.setString(2, fecha_corrida);
                 ps.setInt(3, transportadora);
                ps.setString(4, fecha_corrida);               
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }
            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String facturasPendientesEDS(int id_eds, String fecha, Usuario u) {
        con = null;
        ps = null;
        rs = null;
        String query = "FACTURAS_PENDIENTES_EDS";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query, u.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setInt(1, id_eds);
                ps.setString(2, fecha);
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }
            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }

    }

    @Override
    public String facturarEds(int id_eds, String fecha, Usuario u) {
        con = null;
        ps = null;
        rs = null;
        String query = "FACTURACION_EDS";
        String retorno="";
        try {
            con = this.conectarJNDI(query, u.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setInt(1, id_eds);
                ps.setString(2,fecha);
                ps.setString(3, u.getLogin());
                rs = ps.executeQuery();
                if (rs.next()) {
                       retorno=rs.getString("retorno");                  
                }
            }
        } catch (SQLException e) {
            retorno="ERROR";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return retorno;
        }
    }

    @Override
    public EDSPropietarioBeans buscarPropietario(int id_eds, Usuario u) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        EDSPropietarioBeans beanEds=null;
        String query = "BUSCAR_PROPIETARIO_EDS";
     
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, id_eds);
            rs = ps.executeQuery();

            if (rs.next()) {
                beanEds = new EDSPropietarioBeans();
                beanEds.setNit(rs.getString("identificacion"));
                beanEds.setNombreprop(rs.getString("razon_social"));
                beanEds.setTelefono(rs.getString("telefono"));
                beanEds.setDireccion(rs.getString("direccion")); 
                beanEds.setRepresentante(rs.getString("representante_legal"));
                beanEds.setNitem(rs.getString("documento_representante_legal"));
                beanEds.setCiudad(rs.getString("ciudad")); 
                beanEds.setPais(rs.getString("pais"));
                beanEds.setEdsnombre(rs.getString("nombre_eds"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return beanEds;
        }
    }
    
    @Override
    public ArrayList<MenuOpcionesModulos> cargarMenuTransportadora(String usuario,String distrito) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OPCIONES_TRANSPORTADORA";
        ArrayList<MenuOpcionesModulos> listaOpcionesTransportadora = null;

        try {
            String sql = "";
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1,"%"+usuario+"%");
            ps.setString(2, distrito);
            rs = ps.executeQuery();
            listaOpcionesTransportadora = new ArrayList();
            MenuOpcionesModulos opciones_transportadora = null;
          
            while (rs.next()) {

                opciones_transportadora = new MenuOpcionesModulos();
                opciones_transportadora.setId(rs.getInt("id"));
                opciones_transportadora.setDescripcion(rs.getString("descripcion"));
                opciones_transportadora.setRuta(rs.getString("ruta"));
                opciones_transportadora.setOrden(rs.getInt("orden"));             
                listaOpcionesTransportadora.add(opciones_transportadora);

            }
           
           
        } catch (SQLException e) {
             e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                     e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                     e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return listaOpcionesTransportadora;

    }
    
    @Override
    public boolean copiarArchivoEnCarpetaDestino(String usuario, String rutaOrigen, String rutaDestino, String filename) {

        boolean swFileCopied = false;
        try {

            File carpetaDestino = new File(rutaDestino);

            deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);

            try {
                //Copiamos el archivo de la carpeta origen a la carpeta destino
                InputStream in = new FileInputStream(rutaOrigen + usuario + "/" + filename);
                OutputStream out = new FileOutputStream(rutaDestino + filename);

                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                swFileCopied = true;

            } catch (Exception k) {
                throw new Exception(" FILE: " + k.getMessage());
            }

        } catch (Exception e) {
            System.out.println("errorrr:" + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }

        return swFileCopied;
    }
    
     public void createDir(String dir) throws Exception{
        try{
        File f = new File(dir);
        if(! f.exists() ) f.mkdir();    
        }catch(Exception e){ throw new Exception ( e.getMessage());}
   }

    static public boolean deleteDirectory(File path) {
        if( path.exists() ) {
          File[] files = path.listFiles();
          for(int i=0; i<files.length; i++) {
             if(files[i].isDirectory()) {
               deleteDirectory(files[i]);
             }
             else {
               files[i].delete();
             }
          }
        }
        return( path.delete() );
  }
    
    @Override
    public String  cargarAnticposTransportadora(String id_transportadora, String fechaini, String fechafin, String planilla, String placa, String producto){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        String filtro=" AND trans.id =" + id_transportadora + " AND mc.creation_date::date BETWEEN '"+fechaini+"' and '"+ fechafin+"'";
        filtro += (planilla.equals("")) ? "": " AND mc.planilla= '" + planilla + "'";
        filtro += (placa.equals("")) ? "": " AND vehiculo.placa= '" + placa + "'";
        filtro += (producto.equals("")) ? "": " AND prot.codigo_proserv= '" + producto + "'";
      
        Gson gson = new Gson();
        String query = "SQL_CARGAR_ANTICIPOS_TRANSPORTADORA";     
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);  
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("codigo_agencia", rs.getString("cod_agencia"));
                fila.addProperty("codigo_empresa", rs.getString("cod_transportadora"));
                fila.addProperty("nombre_empresa", rs.getString("nombre_trans"));
                fila.addProperty("cedula_conductor", rs.getString("identificacion_conductor"));
                fila.addProperty("nombre_conductor", rs.getString("nombre_conductor"));
                fila.addProperty("placa", rs.getString("placa"));
                fila.addProperty("codigo_producto", rs.getString("codigo_producto"));
                fila.addProperty("nombre_producto", rs.getString("nombre_producto"));
                fila.addProperty("planilla_interna", rs.getString("planilla"));
                fila.addProperty("origen", rs.getString("origen"));
                fila.addProperty("destino", rs.getString("destino"));
                fila.addProperty("valor_reanticipo", rs.getDouble("valor_neto_anticipo"));
                fila.addProperty("fecha_anticipo", rs.getString("fecha_anticipo"));
                fila.addProperty("fecha_envio", rs.getString("fecha_sistema"));
                fila.addProperty("secuencia", rs.getString("secuencia"));
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
    @Override
    public String generarJsonListarReanticipo(String id_transportadora, String planilla){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
             
        Gson gson = new Gson();
        String query = "SQL_BUSCAR_INFO_WEBSERV_TRANSPORTADORA";     
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);  
            ps.setString(1, id_transportadora);
            ps.setString(2, planilla);
            rs = ps.executeQuery();          
         
            while (rs.next()){
                obj = new JsonObject(); 
                obj.addProperty("codigo_empresa", rs.getString("cod_transportadora"));
                obj.addProperty("codigo_agencia", rs.getString("cod_agencia"));                
                obj.addProperty("planilla", rs.getString("planilla"));                      
            }
      
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
    @Override
    public String cargarProductosTransportadora(Usuario u) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PRODUCTOS_TRANSP";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));   
            ps.setString(1,u.getLogin());
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("codigo_proserv"), rs.getString("descripcion"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }


            return respuestaJson;
        }

    }   
    
    @Override
    public void setearBeansPropietario(BeansPropietario prop) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_INFO_PROVEEDOR";
        String respuestaJson = "{}";      

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setString(1, prop.getNit());
            rs = ps.executeQuery();

            while (rs.next()) {
                prop.setBanco(rs.getString("banco_transfer"));
                prop.setSucursal(rs.getString("suc_transfer"));
                prop.setCedula_titular_cuenta(rs.getString("cedula_cuenta"));
                prop.setNombre_titular_cuenta(rs.getString("nombre_cuenta"));    
                prop.setTipo_cuenta(rs.getString("tipo_cuenta"));
                prop.setNo_cuenta(rs.getString("no_cuenta"));    
                prop.setDireccion(rs.getString("direccion"));   
                prop.setVeto("N");
                prop.setVeto_causal("");  
            }
           

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
           
        }

    }
    
    @Override
    public void setearBeansConductor(BeansConductor cond) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_INFO_PROVEEDOR";
        String respuestaJson = "{}";
               
        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setString(1, cond.getNit());
            rs = ps.executeQuery();

            while (rs.next()) {
                cond.setFecha_nacimiento(rs.getString("fechanac"));
                cond.setBanco(rs.getString("banco_transfer"));
                cond.setSucursal(rs.getString("suc_transfer"));
                cond.setCedula_titular_cuenta(rs.getString("cedula_cuenta"));
                cond.setNombre_titular_cuenta(rs.getString("nombre_cuenta"));    
                cond.setTipo_cuenta(rs.getString("tipo_cuenta"));
                cond.setNo_cuenta(rs.getString("no_cuenta"));
                cond.setVeto("N");
                cond.setVeto_causal("");  
                cond.setCiudad(rs.getString("ciudad"));
                cond.setBarrio(rs.getString("barrio"));
                cond.setDireccion(rs.getString("direccion"));
                cond.setTelefono(rs.getString("telefono"));
                cond.setCelular(rs.getString("celular"));
                cond.setEmail(rs.getString("e_mail"));                
               
            }

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }

     
        }

    }
    
    @Override
    public void setearBeansAnticipo(BeansAnticipo ant) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DEFAULT_ANTICIPO";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));         
            rs = ps.executeQuery();

            while (rs.next()) {
                ant.setTipo_doc_intermediario(rs.getString("tipo_doc_intermediario"));
                ant.setCedula_intermediario(rs.getString("cedula_intermediario"));
                ant.setPorc_comision_intermediario(rs.getFloat("porc_comision_intermediario"));
                ant.setValor_comision_intermediario(rs.getFloat("valor_comision_intermediario"));
                ant.setFecha_envio_fintra(rs.getString("fecha_envio_fintra"));
                ant.setFecha_creacion_anticipo(rs.getString("fecha_creacion_anticipo"));
            }
          
        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
           
        }

    }
    
    @Override
    public BeansAgencia getInfoAgenciaUsuario(String usuario) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_INFO_AGENCIA_USUARIO";
        BeansAgencia agencia = null;
   
        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                agencia = new BeansAgencia();
                agencia.setId(rs.getInt("id"));
                agencia.setCod_agencia(rs.getString("cod_agencia"));
                agencia.setNombre_agencia(rs.getString("nombre_agencia"));
                agencia.setId_transportadora(rs.getString("id_transportadora"));
                agencia.setCod_transportadora(rs.getString("cod_transportadora"));    
                agencia.setUser_trans(rs.getString("user_trans"));
            }
          
        } catch (SQLException ex) {
             System.out.println(ex.getMessage());
             ex.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
           return agencia;
        }

    }

    @Override
    public String reversarLote(JsonArray array, Usuario u) {
        String retorno = "{}";
        //agrupamos la lista de la transferencia.
        boolean agruparListaTransferencia = actionLote(array);
        if (agruparListaTransferencia) {
            
                retorno = "{\"respuesta\":\"OK\"}";
            
            //    retorno = "{\"respuesta\":\"Error generando el archivo..)\"}";
            
        } else {
            retorno = "{\"respuesta\":\"Lo sentimos no pudimos reversar el Lote.\"}";   
        }

        return retorno;
    }
    
    
    private boolean actionLote(JsonArray array) {
        con = null;
        ps = null;
        rs = null;
        boolean retorno = false;
        String query = "REVERSAR_LOTE";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        String filtroId = "(";
        try {
                for (int i = 0; i < array.size(); i++) {
                    JsonObject objects = (JsonObject) array.get(i);
                    if (i == (array.size() - 1)) {
                        filtroId += "'" + objects.get("nro_lote").getAsString()+ "')";
                    } else {
                        filtroId += "'" + objects.get("nro_lote").getAsString() + "',";
                    }
                }
                con = this.conectarJNDI(query);
                System.out.println(filtroId);

                if (con != null) {
                    ps = con.prepareStatement(this.obtenerSQL(query));
                    ps.setString(1, filtroId);
                    rs = ps.executeQuery();                    
                    if (rs.next()) {
                        retorno = rs.getBoolean("retorno");
                    }

                }
        
        
        
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return retorno;
        }
    }

    @Override
    public String buscarLote(String id_transportadora) {
        con = null;
        ps = null;
        rs = null;
        String query = "BUSCAR_LOTES_TRANSFERIDOS_HOY";
        JsonArray lista = null;
        JsonObject jsonObject = null;
//        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, id_transportadora);
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {

                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }

                    lista.add(jsonObject);
                }
            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }
    
    @Override
    public String getTransportadorasListaXusuario(Usuario user) {

        con = null;
        ps = null;
        rs = null;
        String query = "OBTENER_TRANSPORTADORAS_X_USUARIO";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, user.getLogin());
                rs = ps.executeQuery();
                lista = new JsonArray();

                jsonObject = new JsonObject();
                jsonObject.addProperty("id", "");
                jsonObject.addProperty("razon_social", "Seleccione");
                lista.add(jsonObject);

                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("id", rs.getInt("id"));
                    jsonObject.addProperty("razon_social", rs.getString("razon_social"));
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            lista = null;

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }
    
}
