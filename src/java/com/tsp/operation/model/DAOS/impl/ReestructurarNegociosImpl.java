/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.Barcode;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.NegocioTrazabilidadDAO;
import com.tsp.operation.model.DAOS.ReestructurarNegociosDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.ConfiguracionDescuentosObligaciones;
import com.tsp.operation.model.beans.ConfiguracionTablaInicial;
import com.tsp.operation.model.beans.DocumentosNegAceptado;
import com.tsp.operation.model.beans.ExtractoPagoInicial;
import com.tsp.operation.model.beans.ExtractoPagoInicialDetalle;
import com.tsp.operation.model.beans.LiquidacionFenalcoBeans;
import com.tsp.operation.model.beans.NegocioTrazabilidad;
import com.tsp.operation.model.beans.Negocios;
import com.tsp.operation.model.beans.SaldosReestructuracionBeans;
import com.tsp.operation.model.beans.SolicitudNegocio;
import com.tsp.operation.model.beans.SolicitudPersona;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.resumenSaldosReestructuracion;
import com.tsp.operation.model.beans.tablaPagoInicial;
import com.tsp.operation.model.services.NegociosGenService;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author egonzalez
 */
public class ReestructurarNegociosImpl extends MainDAO implements ReestructurarNegociosDAO {

    public ReestructurarNegociosImpl(String dataBaseName) {
        super("ReestructurarNegociosDAO.xml", dataBaseName);
    }

    @Override
    public ArrayList<BeanGeneral> getNegocios(String cedula, String codigoNegocio) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_NEGOCIOS_MICRO";
        ArrayList<BeanGeneral> listaGeneral = null;


        try {
            String sql = "";
            con = this.conectarJNDI(query);
            //si los dos campos vienen con informacion.
            if (!cedula.equals("") && !codigoNegocio.equals("")) {
                sql = this.obtenerSQL(query).replaceAll("#filtro", "neg.cod_cli= ? and neg.cod_neg= ? ");
                ps = con.prepareStatement(sql);
                ps.setString(1, cedula);
                ps.setString(2, codigoNegocio);
            }

            //si el codigo del negocio esta vacio.
            if (!cedula.equals("") && codigoNegocio.equals("")) {
                sql = this.obtenerSQL(query).replaceAll("#filtro", "neg.cod_cli= ? ");
                ps = con.prepareStatement(sql);
                ps.setString(1, cedula);
            }

            //si la cedula esta vacia.
            if (cedula.equals("") && !codigoNegocio.equals("")) {
                sql = this.obtenerSQL(query).replaceAll("#filtro", "neg.cod_neg= ? ");
                ps = con.prepareStatement(sql);
                ps.setString(1, codigoNegocio);
            }

            rs = ps.executeQuery();
            listaGeneral = new ArrayList();
            BeanGeneral general = null;
            while (rs.next()) {

                general = new BeanGeneral();
                general.setValor_01(rs.getString("cod_cli"));
                general.setValor_02(rs.getString("negasoc"));
                general.setValor_03(rs.getString("cliente"));
                general.setValor_04(rs.getString("fecha_negocio"));
                general.setValor_05(rs.getString("estado_neg"));
                general.setValor_06(rs.getString("convenio"));
                general.setValor_07(rs.getString("renovacion"));
                general.setValor_08(rs.getString("vr_negocio"));
                general.setValor_09(rs.getString("nro_docs"));
                general.setValor_10(rs.getString("saldo_cartera"));

                listaGeneral.add(general);

            }

        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LOS PERFILES DE USUARIO: getNegocios()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaGeneral;


    }

    public ArrayList<SaldosReestructuracionBeans> getFacturasNegocios(String codNegocio) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_FACTURAS_MICRO";
        ArrayList<SaldosReestructuracionBeans> listaSaldo = null;
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codNegocio);
            rs = ps.executeQuery();
            listaSaldo = new ArrayList();
            SaldosReestructuracionBeans reestructuracionBeans = null;
            while (rs.next()) {

                reestructuracionBeans = new SaldosReestructuracionBeans();
                reestructuracionBeans.setDocumento(rs.getString("documento"));
                reestructuracionBeans.setCod_neg(rs.getString("negocio"));
                reestructuracionBeans.setCuota(rs.getInt("cuota"));
                reestructuracionBeans.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                reestructuracionBeans.setDias_mora(rs.getInt("dias_mora"));
                reestructuracionBeans.setEstado(rs.getString("estado"));
                reestructuracionBeans.setSaldo_capital(rs.getDouble("valor_saldo_capital"));
                reestructuracionBeans.setSaldo_interes(rs.getDouble("valor_saldo_mi"));
                reestructuracionBeans.setSaldo_cat(rs.getDouble("valor_saldo_ca"));
                reestructuracionBeans.setSeguro(rs.getDouble("valor_seguro"));
                reestructuracionBeans.setInteres_mora(rs.getDouble("IxM"));
                reestructuracionBeans.setGasto_cobranza(rs.getDouble("GaC"));
                reestructuracionBeans.setTotal_item(rs.getDouble("suma_saldos"));

                listaSaldo.add(reestructuracionBeans);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LOS PERFILES DE USUARIO: getFacturasNegocios()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return listaSaldo;
    }

    public ArrayList<DocumentosNegAceptado> simuladorCreditoMicro(String tipo_cuota, int cuota, double valor_negocio, String primeracuota, String titulo_valor) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LIQUIDACION_MICRO";
        ArrayList<DocumentosNegAceptado> liquidacion = null;

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setDouble(1, valor_negocio);
            ps.setInt(2, cuota);
            ps.setString(3, primeracuota);
            ps.setString(4, tipo_cuota);

            rs = ps.executeQuery();
            liquidacion = new ArrayList();
            DocumentosNegAceptado dna = null;
            while (rs.next()) {

                dna = new DocumentosNegAceptado();
                dna.setFecha(rs.getString("fecha"));
                dna.setDias(rs.getString("dias"));
                dna.setItem(rs.getString("item"));
                dna.setSaldo_inicial(rs.getDouble("saldo_inicial"));
                dna.setValor(rs.getDouble("valor_cuota"));
                dna.setCapital(rs.getDouble("capital"));
                dna.setInteres(rs.getDouble("interes"));
                dna.setCapacitacion(rs.getDouble("capacitacion"));
                dna.setCat(rs.getDouble("cat"));
                dna.setSeguro(rs.getDouble("seguro"));
                dna.setSaldo_final(rs.getDouble("saldo_final"));

                liquidacion.add(dna);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LOS PERFILES DE USUARIO: simuladorCreditoMicro()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return liquidacion;
    }

    @Override
    public String crearNegoMicroNuevo(String negocio_padre, String usuario, int idconvenio, double valor_negocio, int numcuota, 
                                        String fecha_pr_cuota, String tipo_cuotas,double saldo_capital,
                                        double saldo_interes,double saldo_cat , double saldo_seguro,double intxmora,double gac) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CREAR_NEGOCIO_MICRO";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));

            ps.setString(1, negocio_padre);
            ps.setString(2, usuario);
            ps.setInt(3, idconvenio);
            ps.setDouble(4, valor_negocio);
            ps.setInt(5, numcuota);
            ps.setString(6, fecha_pr_cuota);
            ps.setString(7, tipo_cuotas);
            ps.setDouble(8, saldo_capital);
            ps.setDouble(9, saldo_interes);
            ps.setDouble(10, saldo_cat);
            ps.setDouble(11, saldo_seguro);
            ps.setDouble(12, intxmora);
            ps.setDouble(13, gac);
            
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty("respuesta", rs.getString("estado"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }


            return respuestaJson;
        }


    }

    @Override
    public String cargarDepartamento() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DEPARTAMENTOS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("department_code"), rs.getString("department_name"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }


            return respuestaJson;
        }

    }

    /**
     *
     * @param coddpt
     * @return
     */
    @Override
    public String cargarCiudad(String coddpt) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_CIUDADES";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, coddpt);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("codciu"), rs.getString("nomciu"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }


            return respuestaJson;
        }

    }

    @Override
    public String buscarFormulario(String numero_solicitud) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_FORM";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numero_solicitud);
            rs = ps.executeQuery();

            while (rs.next()) {

                obj.addProperty("identificacion", rs.getString("identificacion"));
                obj.addProperty("primer_apellido", rs.getString("primer_apellido"));
                obj.addProperty("segundo_apellido", rs.getString("segundo_apellido"));
                obj.addProperty("primer_nombre", rs.getString("primer_nombre"));
                obj.addProperty("segundo_nombre", rs.getString("segundo_nombre"));
                obj.addProperty("telefono", rs.getString("telefono"));
                obj.addProperty("celular", rs.getString("celular"));
                obj.addProperty("direccion", rs.getString("direccion"));
                obj.addProperty("departamento", rs.getString("departamento"));
                obj.addProperty("ciudad", rs.getString("ciudad"));
                obj.addProperty("barrio", rs.getString("barrio"));
                obj.addProperty("email", rs.getString("email"));
                obj.addProperty("id_cony", rs.getString("id_cony"));
                obj.addProperty("primer_apellido_cony", rs.getString("primer_apellido_cony"));
                obj.addProperty("segundo_apellido_cony", rs.getString("segundo_apellido_cony"));
                obj.addProperty("primer_nombre_cony", rs.getString("primer_nombre_cony"));
                obj.addProperty("segundo_nombre_cony", rs.getString("segundo_nombre_cony"));
                obj.addProperty("telefono_cony", rs.getString("telefono_cony"));
                obj.addProperty("celular_cony", rs.getString("celular_cony"));
                obj.addProperty("direccion_cony", rs.getString("direccion_cony"));
                obj.addProperty("nombre_ng", rs.getString("nombre_ng"));
                obj.addProperty("direccion_ng", rs.getString("direccion_ng"));
                obj.addProperty("departamento_ng", rs.getString("departamento_ng"));
                obj.addProperty("ciudad_ng", rs.getString("ciudad_ng"));
                obj.addProperty("barrio_ng", rs.getString("barrio_ng"));
                obj.addProperty("tel_negocio", rs.getString("tel_negocio"));

            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }


            return respuestaJson;
        }

    }

    @Override
    public String actualizarFormulario(SolicitudPersona persona, SolicitudNegocio negocio, String usuario, String observacion) {

        Connection con = null;
        String query = "SQL_UPDATE_PERSONA";
        String respuestaJson = "{}";
        Statement stmt = null;
        
        JsonObject obj = new JsonObject();

        try {

            //insertamos la traza 
            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
            negtraza.setActividad("SOL");
            negtraza.setNumeroSolicitud(persona.getNumeroSolicitud());
            negtraza.setUsuario(usuario);
            negtraza.setCausal("");
            negtraza.setComentarios(observacion);
            negtraza.setDstrct("FINV");
            negtraza.setConcepto("");
            negtraza.setCodNeg(persona.getTipo());

            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO();

            con = this.conectarJNDI(query);
            con.setAutoCommit(false);
            stmt = con.createStatement();

            stmt.addBatch(this.updatePersona(persona, usuario));
            stmt.addBatch(this.updateNegocio(negocio, usuario));
            stmt.addBatch(daotraza.insertNegocioTrazabilidad(negtraza));
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (SQLException ex) {
            con.rollback();
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
            }
            return respuestaJson;
        }
    }

    private String updatePersona(SolicitudPersona persona, String usuario) throws Exception {
        String cadena = "";
        String query = "SQL_UPDATE_PERSONA";
        String sql = "";
        StringStatement st = null;

        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        st.setString(1, persona.getPrimerApellido());
        st.setString(2, persona.getSegundoApellido());
        st.setString(3, persona.getPrimerNombre());
        st.setString(4, persona.getSegundoNombre());
        st.setString(5, persona.getTelefono());
        st.setString(6, persona.getCelular());
        st.setString(7, persona.getDireccion());
        st.setString(8, persona.getDepartamento());
        st.setString(9, persona.getCiudad());
        st.setString(10, persona.getBarrio());
        st.setString(11, persona.getEmail());
        st.setString(12, persona.getIdentificacionCony());
        st.setString(13, persona.getPrimerApellidoCony());
        st.setString(14, persona.getSegundoApellidoCony());
        st.setString(15, persona.getPrimerNombreCony());
        st.setString(16, persona.getSegundoNombreCony());
        st.setString(17, persona.getTelefonoCony());
        st.setString(18, persona.getCelularCony());
        st.setString(19, persona.getDireccionEmpresaCony());
        st.setString(20, usuario);
        st.setString(21, persona.getNumeroSolicitud());

        cadena = st.getSql();

        st = null;

        return cadena;
    }

    private String updateNegocio(SolicitudNegocio negocio, String usuario) throws Exception {

        String cadena = "";
        String query = "SQL_UPDATE_NEGOCIO";
        String sql = "";
        StringStatement st = null;

        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);
        st.setString(1, negocio.getDireccion());
        st.setString(2, negocio.getDepartamento());
        st.setString(3, negocio.getCiudad());
        st.setString(4, negocio.getBarrio());
        st.setString(5, negocio.getTelefono());
        st.setString(6, usuario);
        st.setString(7, negocio.getNumeroSolicitud());
        cadena = st.getSql();
        st = null;

        return cadena;
    }

    @Override
    public ArrayList<Negocios> getListarNegociosRees(String actividad, String estado) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_NEGOCIOS_RENOVACION";
        ArrayList<Negocios> listaNegocios = null;
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, actividad);
            ps.setString(2, estado);
            rs = ps.executeQuery();
            listaNegocios = new ArrayList();
            Negocios negocios = null;
            int i = 0;
            while (rs.next()) {
                i++;
                negocios = new Negocios();
                negocios.setItem(i);
                negocios.setCod_negocio(rs.getString("cod_neg"));
                negocios.setNombreafil(rs.getString("nombre_cliente"));
                negocios.setCod_cli(rs.getString("cod_cli"));
                negocios.setVr_negocio(rs.getDouble("vr_negocio"));
                negocios.setTotpagado(rs.getDouble("tot_pagado"));
                negocios.setFecha_neg(rs.getString("fecha_negocio"));
                negocios.setNodocs(rs.getInt("nro_docs"));
                negocios.setId_convenio(rs.getInt("id_convenio"));
                negocios.setValor_seguro(rs.getDouble("valor_seguro"));
                negocios.setTasa(rs.getString("tasa_interes"));
                negocios.setPorcentaje_cat(rs.getDouble("porcentaje_cat"));

                listaNegocios.add(negocios);
            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LOS NEGOCIOS: getFacturasNegocios()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return listaNegocios;

    }

    @Override
    public ArrayList<DocumentosNegAceptado> buscarLiquidacionNegocio(String negocio) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_LIQUIDACION";
        ArrayList<DocumentosNegAceptado> liquidacion = null;

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);

            rs = ps.executeQuery();
            liquidacion = new ArrayList();
            DocumentosNegAceptado dna = null;
            while (rs.next()) {

                dna = new DocumentosNegAceptado();
                dna.setFecha(rs.getString("fecha"));
                dna.setDias(rs.getString("dias"));
                dna.setItem(rs.getString("item"));
                dna.setSaldo_inicial(rs.getDouble("saldo_inicial"));
                dna.setValor(rs.getDouble("valor_cuota"));
                dna.setCapital(rs.getDouble("capital"));
                dna.setInteres(rs.getDouble("interes"));
                dna.setCapacitacion(rs.getDouble("capacitacion"));
                dna.setCat(rs.getDouble("cat"));
                dna.setSeguro(rs.getDouble("seguro"));
                dna.setSaldo_final(rs.getDouble("saldo_final"));

                liquidacion.add(dna);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO SQL_BUSCAR_LIQUIDACION: buscarLiquidacionNegocio()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return liquidacion;
    }

    @Override
    public String rechazarNegocio(String negocio, String usuario) {

        Connection con = null;
        String query = "SQL_UPDATE_NEGOCIO_MICRO";
        String respuestaJson = "{}";
        Statement stmt = null;
        JsonObject obj = new JsonObject();

        try {
            
            con = this.conectarJNDI(query);
            con.setAutoCommit(false);
            stmt = con.createStatement();

            stmt.addBatch(this.updateNegocioCredito(negocio, usuario, "R", "DEC","SQL_UPDATE_NEGOCIO_MICRO"));
            stmt.addBatch(this.updateFormulario(negocio, usuario, "R"));
            stmt.addBatch(this.borrarRelacionNegocios(negocio, "SQL_BORRAR_REL_NEG"));
           
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (SQLException ex) {
            con.rollback();
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
            }
            return respuestaJson;
        }

    }
    
    private String borrarRelacionNegocios(String negocio,String nameQuery) throws Exception {

        String cadena = "";
        String query = nameQuery;
        String sql = "";
        StringStatement st = null;

        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);
        st.setString(1, negocio);
        cadena = st.getSql();
        st = null;

        return cadena;
    }

    private String updateNegocioCredito(String negocio, String usuario,String estado, String actividad, String nameQuery) throws Exception {

        String cadena = "";
        String query = nameQuery;
        String sql = "";
        StringStatement st = null;
        if (query.equals("SQL_UPDATE_NEGOCIO_MICRO_A")) {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, estado);
            st.setString(2, actividad);
            st.setString(3, usuario);
            st.setString(4, usuario);
            st.setString(5, negocio);
        } else {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, estado);
            st.setString(2, actividad);
            st.setString(3, usuario);          
            st.setString(4, negocio);
        }
        
        cadena = st.getSql();
        st = null;

        return cadena;
    }

    private String updateFormulario(String negocio, String usuario,String estado) throws Exception {

        String cadena = "";
        String query = "SQL_UPDATE_FORMULARIO";
        String sql = "";
        StringStatement st = null;

        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);
        st.setString(1,estado);
        st.setString(2, usuario);
        st.setString(3, negocio);
        cadena = st.getSql();
        st = null;

        return cadena;
    }

    @Override
    public String buscarTraza(String negocio) {
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_TRAZABILIDAD";
        ArrayList<NegocioTrazabilidad> listaTraza = null;
        String json="";
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);

            rs = ps.executeQuery();
            listaTraza = new ArrayList();
            NegocioTrazabilidad trazabilidad = null;            
            Gson gson = new Gson();
            
            while (rs.next()) {
                
                trazabilidad=new NegocioTrazabilidad();
                trazabilidad.setNumeroSolicitud(rs.getString("numero_solicitud"));
                trazabilidad.setActividad(rs.getString("actividad"));
                trazabilidad.setUsuario(rs.getString("usuario"));
                trazabilidad.setFecha(rs.getString("fecha"));
                trazabilidad.setComentarios(rs.getString("comentarios"));

                listaTraza.add(trazabilidad);
            }
            
             json = gson.toJson(listaTraza);
            
         
        } catch (SQLException ex) {
            json = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }

             return json;
        }
    }

    @Override
    public String aprobarNegocio(String negocio, String formulario,String usuario, String observacion) {
        
        Connection con = null;
        String query = "SQL_UPDATE_NEGOCIO_MICRO";
        String respuestaJson = "{}";
        Statement stmt = null;
        
        JsonObject obj = new JsonObject();

        try {

            //insertamos la traza 
            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
            negtraza.setActividad("DEC");
            negtraza.setNumeroSolicitud(formulario);
            negtraza.setUsuario(usuario);
            negtraza.setCausal("");
            negtraza.setComentarios(observacion);
            negtraza.setDstrct("FINV");
            negtraza.setConcepto("");
            negtraza.setCodNeg(negocio);

            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO();
            
            if(generarIAmicro(negocio,usuario)){
                con = this.conectarJNDI(query);
                con.setAutoCommit(false);
                stmt = con.createStatement();
                
                stmt.addBatch(daotraza.insertNegocioTrazabilidad(negtraza));
                stmt.addBatch(this.updateNegocioCredito(negocio, usuario,"V", "DEC","SQL_UPDATE_NEGOCIO_MICRO_A"));
                stmt.addBatch(this.updateFormulario(negocio, usuario, "V"));
                
                stmt.executeBatch();
                stmt.clearBatch();
                con.commit();
                
                respuestaJson = "{\"respuesta\":\"OK\"}";
            }else {
                
                respuestaJson = "{\"error\":\"No se pudo generar la nota de ajuste\"}";
            
            } 
            
        } catch (SQLException ex) {
            con.rollback();
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
            }
            return respuestaJson;
        }
    
    }
    
    private boolean generarIAmicro(String negocio, String usuario) throws SQLException{
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_NOTA_AJUSTE_MICRO";
        boolean status=false;
        
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            ps.setString(2, usuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                String ok=rs.getString("resultado");
                if(ok.equals("OK")){
                  status=true;
                  break;
                }
            }
                          
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO RESPUESTA DE LA IA: generarIAmicro()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }

            return status;
        }


    
    }

    @Override
    public String actualizarNegocio(String negocio, String usuario,String formulario, String observacion) {
        
        Connection con = null;
        String query = "SQL_UPDATE_NEGOCIO_MICRO";
        String respuestaJson = "{}";
        Statement stmt = null;
        JsonObject obj = new JsonObject();

        try {
            
            //insertamos la traza 
            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
            negtraza.setActividad("DES");
            negtraza.setNumeroSolicitud(formulario);
            negtraza.setUsuario(usuario);
            negtraza.setCausal("");
            negtraza.setComentarios(observacion);
            negtraza.setDstrct("FINV");
            negtraza.setConcepto("");
            negtraza.setCodNeg(negocio);

            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO();
            
            con = this.conectarJNDI(query);
            con.setAutoCommit(false);
            stmt = con.createStatement();
            
            stmt.addBatch(daotraza.insertNegocioTrazabilidad(negtraza));
            stmt.addBatch(this.updateNegocioCredito(negocio, usuario, "T", "DES","SQL_UPDATE_NEGOCIO_MICRO"));
            stmt.addBatch(this.updateFormulario(negocio, usuario, "T"));
           
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (SQLException ex) {
            con.rollback();
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
            }
            return respuestaJson;
        }
      
    
    }

    //Desde aqui empieza los metodos para fenalco.
    @Override
    public ArrayList<BeanGeneral> buscarNegocioFenalco(String cedula,Usuario user) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_NEGOCIOS_FENALCO";
        ArrayList<BeanGeneral> listaGeneral = null;

        try {
            String sql = "";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cedula);
            ps.setString(2, user.getDstrct());
            rs = ps.executeQuery();
            listaGeneral = new ArrayList();
            BeanGeneral general = null;
            while (rs.next()) {

                general = new BeanGeneral();
                general.setValor_01(rs.getString("cod_cli"));
                general.setValor_02(rs.getString("cliente"));
                general.setValor_03(rs.getString("negasoc"));
                general.setValor_04(rs.getString("fecha_negocio"));
                general.setValor_05(rs.getString("estado_neg"));
                general.setValor_06(rs.getString("convenio"));
                general.setValor_07(rs.getString("id_convenio"));
                general.setValor_08(rs.getString("vr_negocio"));
                general.setValor_09(rs.getString("nro_docs"));
                general.setValor_10(rs.getString("saldo_cartera"));
                general.setValor_11(rs.getString("afiliado"));
                general.setValor_12(rs.getString("tipo_negocio"));

                listaGeneral.add(general);

            }

        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LOS PERFILES DE USUARIO: getNegocios()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaGeneral;


    }

    @Override
    public String buscarUnidadNegocio() throws SQLException {
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_UNIDAD_NEGOCIO_BASE";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            
            return respuestaJson;
        }
       
    }

    @Override
    public ArrayList<SaldosReestructuracionBeans> saldosNegocioSegFenalco(ArrayList vectorNegocio,Usuario user) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_SALDOS_FENALCO";
        ArrayList<SaldosReestructuracionBeans> listaSaldo = null;
        try {
            con = this.conectarJNDI(query);
            listaSaldo = new ArrayList();
            SaldosReestructuracionBeans reestructuracionBeans = null;
            for (Object vectorNegocio1 : vectorNegocio) {
               
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, (String) vectorNegocio1);
                ps.setString(2, (String) user.getDstrct());
                rs = ps.executeQuery();                
                while (rs.next()) {
                   
                    reestructuracionBeans = new SaldosReestructuracionBeans();
                    reestructuracionBeans.setDocumento(rs.getString("documento"));
                    reestructuracionBeans.setCod_neg(rs.getString("negocio"));
                    reestructuracionBeans.setCuota(rs.getInt("cuota"));
                    reestructuracionBeans.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                    reestructuracionBeans.setDias_mora(rs.getInt("dias_mora"));
                    reestructuracionBeans.setEstado(rs.getString("estado"));
                    reestructuracionBeans.setTipo_negocio(rs.getString("tipo_negocio"));
                    reestructuracionBeans.setValor_factura(rs.getDouble("valor_factura"));
                    reestructuracionBeans.setValor_abono(rs.getDouble("valor_abono"));
                    reestructuracionBeans.setValor_saldo(rs.getDouble("valor_saldo"));
                    reestructuracionBeans.setSaldo_capital(rs.getDouble("valor_saldo_capital"));
                    reestructuracionBeans.setSaldo_interes(rs.getDouble("valor_saldo_interes"));
                    reestructuracionBeans.setInteres_mora(rs.getDouble("IxM"));
                    reestructuracionBeans.setGasto_cobranza(rs.getDouble("GaC"));
                    reestructuracionBeans.setTotal_item(rs.getDouble("suma_saldos"));

                    listaSaldo.add(reestructuracionBeans);

                }

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LOS PERFILES DE USUARIO: getFacturasNegocios()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaSaldo;
    }

    @Override
    public ArrayList<DocumentosNegAceptado> liquidadorCreditoFenalco(int cuota, double valor_negocio, String primeracuota, String titulo_valor, String idconvenio, String afiliado) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LIQUIDACION_FENALCO";
        ArrayList<DocumentosNegAceptado> liquidacion = null;

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setDouble(1, valor_negocio);
            ps.setInt(2, cuota);
            ps.setString(3, primeracuota);
            ps.setString(4, idconvenio);
            ps.setString(5, afiliado);

            rs = ps.executeQuery();
            liquidacion = new ArrayList();
            DocumentosNegAceptado dna = null;
            while (rs.next()) {

                dna = new DocumentosNegAceptado();
                dna.setFecha(rs.getString("fecha"));
                dna.setDias(rs.getString("dias"));
                dna.setItem(rs.getString("item"));
                dna.setSaldo_inicial(rs.getDouble("saldo_inicial"));
                dna.setValor(rs.getDouble("valor_cuota"));
                dna.setCapital(rs.getDouble("capital"));
                dna.setInteres(rs.getDouble("interes"));                
                dna.setCustodia(rs.getDouble("custodia"));
                dna.setSeguro(rs.getDouble("seguro"));
                dna.setRemesa(rs.getDouble("remesa"));
                dna.setSaldo_final(rs.getDouble("saldo_final"));

                liquidacion.add(dna);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LIQUIDACION FENALCO: simuladorCreditoFenalco()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return liquidacion;
    }

    @Override
    public String guardarExtracto(ArrayList<tablaPagoInicial> listaInicial,String usuario) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GENERAR_EXTRACTO";
        String retorno="";

        try {
            String[][] multiArray = new String[listaInicial.size()][];
            for (int i = 0; i < listaInicial.size(); i++) {
                tablaPagoInicial get = listaInicial.get(i);
                String[] elementos = new String[8];
                elementos[0] = get.getNegocio();
                elementos[1] = get.getTipo_neg();
                elementos[2] = get.getCapital();
                elementos[3] = get.getInteres();
                elementos[4] = get.getIntXmora();
                elementos[5] = get.getGAC();
                elementos[6] = get.getTotal();
                elementos[7] = get.getPctPagar();
                multiArray[i] = elementos;
            }
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            Array miArray;
            miArray = con.createArrayOf("text", multiArray);
            ps.setArray(1, miArray);
            ps.setString(2, usuario); 
            rs = ps.executeQuery();
            
                while (rs.next()) {
                    retorno=rs.getString("salida");
                }

        } catch (SQLException ex) {
            throw new SQLException("ERROR GUARDANDO ESTRACTO: guardarExtracto()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            
           return retorno;
        }     
    }

    @Override
    public String guardarSaldosResumen(resumenSaldosReestructuracion sr, Usuario user) throws SQLException {

        String cadena = "";
        String query = "SQL_GUARDAR_RESUMEN_SALDOS";
        String sql = "";

        try {

            StringStatement st = null;
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);

            st.setString(1, user.getDstrct());
            st.setString(2, sr.getNegocio());
            st.setString(3, sr.getEstado());
            st.setDouble(4, Double.parseDouble(sr.getCapital().replace(",", "")));
            st.setDouble(5, Double.parseDouble(sr.getInteres().replace(",", "")));
            st.setDouble(6, Double.parseDouble(sr.getIntxMora().replace(",", "")));
            st.setDouble(7, Double.parseDouble(sr.getGaC().replace(",", "")));
            st.setDouble(8, Double.parseDouble(sr.getSub_Total().replace(",", "")));
            st.setDouble(9, Double.parseDouble(sr.getDcto_capital().replace(",", "")));
            st.setDouble(10, Double.parseDouble(sr.getDcto_interes().replace(",", "")));
            st.setDouble(11, Double.parseDouble(sr.getDcto_intxmora().replace(",", "")));
            st.setDouble(12, Double.parseDouble(sr.getDcto_gac().replace(",", "")));
            st.setDouble(13, Double.parseDouble(sr.getTotal_Dcto().replace(",", "")));
            st.setDouble(14, Double.parseDouble(sr.getTotal_Items().replace(",", "")));
            st.setInt(15, sr.getId_rop());
            st.setString(16, "");
            st.setString(17, user.getLogin());

            cadena = st.getSql();
            st = null;

        } catch (Exception ex) {
            cadena = ex.getMessage();
        } finally {
            return cadena;
        }
    }

    @Override
    public void borrarExtracto(int idRop) {

        Connection con = null;
        String query = "SQL_GENERAR_EXTRACTO";

        try {
            
            con = this.conectarJNDI(query);
            con.setAutoCommit(false);
            Statement stmt = con.createStatement();

            stmt.addBatch("DELETE FROM resumen_saldo_reestructuracion  WHERE id_rop="+idRop);
            stmt.addBatch("DELETE FROM liquidacion_reestructuracion_fenalco  WHERE id_rop="+idRop);
            stmt.addBatch("DELETE FROM detalle_rop  WHERE id_rop="+idRop);
            stmt.addBatch("DELETE FROM recibo_oficial_pago WHERE id="+idRop);
           
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
  
        } catch (SQLException ex) {
            try {
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                this.desconectar(con);
            } catch (SQLException e) {
                
            }

        }

    }

    @Override
    public String guardarLiquidacion(LiquidacionFenalcoBeans liquidacionBeans) throws SQLException {
        String cadena = "";
        String query = "SQL_GUARDAR_LIQUIDACION";
        String sql = "";

        try {

            StringStatement st = null;
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            
            st.setInt(1, liquidacionBeans.getId_rop());
            st.setString(2,liquidacionBeans.getFecha());
            st.setString(3, liquidacionBeans.getItem());
            st.setDouble(4,liquidacionBeans.getSaldo_inicial());
            st.setDouble(5,liquidacionBeans.getCapital());
            st.setDouble(6,liquidacionBeans.getInteres());
            st.setDouble(7, liquidacionBeans.getCustodia());
            st.setDouble(8, liquidacionBeans.getSeguro());
            st.setDouble(9, liquidacionBeans.getRemesa());
            st.setDouble(10,liquidacionBeans.getValor_cuota());
            st.setDouble(11, liquidacionBeans.getSaldo_final());
            st.setString(12,liquidacionBeans.getCreation_user());
            st.setInt(13,liquidacionBeans.getDias());
           
            cadena = st.getSql();
            st = null;

        } catch (Exception ex) {
            cadena = ex.getMessage();
        } finally {
            return cadena;
        }
    }

    @Override
    public void generarPdf(String userlogin, int id_rop){
        try {
            String directorio = "";
            ResourceBundle rb = null;
            
            //buscamos la info de la cabecera del extracto.
            ExtractoPagoInicial epi=this.infoCabeceraExracto(id_rop);
            
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "Extracto_" + id_rop, "pdf");
            Document documento = null;
            documento = this.crearDocumento();
            //Creamos una instancia del la clase HeaderFooter para agregar el encabezado de la pagina
            PdfWriter writer = PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            HeaderFooter headerFooter = new HeaderFooter(rb,epi.getCod_rop());
            writer.setPageEvent(headerFooter);
            //abrimos el documento.
            documento.open();
            //informacion del cliente
            documento.add(this.informacionCliente(epi));
            //leyendas             
            documento.add(this.leyendas(epi.getVencimiento_rop()));
            Font fuenteK = new Font(Font.HELVETICA, 11, Font.ITALIC);
            fuenteK.setColor(Color.GRAY);
            Paragraph texto=new Paragraph("Ponte al d�a con tu cr�dito y evita costos que incrementen el valor de la cuota! | �Fintra sigue creyendo en Ti! ",fuenteK);
            texto.setSpacingBefore(5f);
            documento.add(texto);
            //estadoCredito
            documento.add(this.estadoCredito());
            //detalle del pago
            documento.add(this.detallePago(id_rop));
            //linea de corte
            texto=new Paragraph("---------------------------------------------------------------------------------------------------------------------------------------------------",fuenteK);
            texto.setSpacingBefore(5f);
            documento.add(texto);              
            documento.add(this.footerExtracto(writer,epi));
            
            documento.close();
        } catch (SQLException ex) {
            Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

       /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons consecutivo de la cotizacion
     * @param extension La extension del archivo
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons + "_" + fmt.format(new Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }
    
    
    private PdfPTable informacionCliente(ExtractoPagoInicial epi) {
        //Informacion del cliente
        Font fuente = new Font(Font.HELVETICA, 10, Font.BOLD);
        fuente.setColor(Color.BLACK);
        PdfPTable tabla = new PdfPTable(4);
        tabla.getDefaultCell().setBorder(0);
        tabla.setWidthPercentage(100f);
        tabla.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell celda = new PdfPCell(new Paragraph("ESTADO DE CUENTA"));
        celda.setColspan(4);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Generado el: " + epi.getFecha_generacion(),fuente));
        celda.setColspan(2);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Agencia: "));
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(new Paragraph(epi.getCiudad(),fuente));
        celda = new PdfPCell(new Paragraph("Sr(a).",fuente));
        celda.setColspan(2);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Linea de Producto: "));
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        tabla.addCell(new Paragraph(epi.getLinea_negocio(),fuente));
        celda = new PdfPCell(new Paragraph(epi.getNombre_cliente(),fuente));
        celda.setColspan(2);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("No.del Credito:"));
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(new Paragraph(epi.getNegocio(),fuente));
        celda = new PdfPCell(new Paragraph(epi.getDireccion(),fuente));
        celda.setColspan(2);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph("Est. Comercio:"));
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(new Paragraph(epi.getEstablecimiento_comercio(),fuente));
        celda = new PdfPCell(new Paragraph(epi.getBarrio(),fuente));
        celda.setColspan(2);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        tabla.addCell("");
        tabla.addCell("");
        celda = new PdfPCell(new Paragraph(epi.getCiudad(),fuente));
        celda.setColspan(2);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        tabla.addCell("");
        tabla.addCell("");
        celda = new PdfPCell(new Paragraph(epi.getDepartamento(),fuente));
        celda.setColspan(2);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        tabla.addCell("");
        tabla.addCell("");
        //Informacion del cliente
        return tabla;
    }

    
    private PdfPTable leyendas(String antes_de) {

        Font fuenteP = new Font(Font.HELVETICA, 14, Font.BOLD);
        fuenteP.setColor(Color.WHITE);
        Font fuenteR = new Font(Font.HELVETICA, 13, Font.BOLD);
        fuenteR.setColor(Color.BLACK);
        Font fuenteI = new Font(Font.HELVETICA, 15, Font.BOLDITALIC);
        fuenteI.setColor(new Color(61, 67, 102));
        Font fuenteJ = new Font(Font.HELVETICA, 14, Font.BOLDITALIC);
        fuenteJ.setColor(Color.WHITE);
        
        Color ColorRelleno=new Color(50,190,250);//Azul

        PdfPTable tabla = new PdfPTable(3);
        tabla.getDefaultCell().setBorder(0);
        tabla.setWidthPercentage(100f);
        tabla.setSpacingBefore(15);
        PdfPCell celda = new PdfPCell(new Paragraph("PAGUESE ANTES DEL", fuenteP));
        celda.setBackgroundColor(ColorRelleno);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        celda.setPaddingBottom(5);
        tabla.addCell(celda);
        tabla.addCell("");
        celda = new PdfPCell(new Paragraph("Creemos en ti", fuenteI));
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        celda.setBorder(Rectangle.NO_BORDER);
        tabla.addCell(celda);
        celda = new PdfPCell(new Paragraph(antes_de, fuenteR));
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        tabla.addCell(celda);
        tabla.addCell("");
        celda = new PdfPCell(new Paragraph("�Gracias por elegirnos!", fuenteJ));
        celda.setBackgroundColor(ColorRelleno);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        celda.setBorderColor(new Color(39, 142, 218));
        celda.setPaddingBottom(5);
        tabla.addCell(celda);
        return tabla;

    }
    
    private PdfPTable estadoCredito() {

        Font fuenteJ = new Font(Font.HELVETICA, 14, Font.BOLDITALIC);
        fuenteJ.setColor(Color.WHITE);
        PdfPTable tabla = new PdfPTable(1);
        tabla.getDefaultCell().setBorder(0);
        tabla.setWidthPercentage(100f);
        tabla.setSpacingBefore(10);
        PdfPCell celda = new PdfPCell(new Paragraph("SU CREDITO SE ENCUENTRA :", fuenteJ));
        celda.setBackgroundColor(new Color(50,190,250));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorderColor(new Color(39, 142, 218));
        celda.setPaddingBottom(5);
        tabla.addCell(celda);

        return tabla;

    }
    
    private PdfPTable detallePago(int id_rop) throws SQLException {
        ArrayList<ExtractoPagoInicialDetalle> detalleExracto = this.detalleExracto(id_rop);

        Font fuenteJ = new Font(Font.HELVETICA, 13, Font.BOLD);
        fuenteJ.setColor(Color.WHITE);
        Font fuenteI = new Font(Font.HELVETICA, 10, Font.BOLD);
        fuenteI.setColor(Color.BLACK);
        Font fuenteK = new Font(Font.HELVETICA, 10, Font.NORMAL);
        fuenteK.setColor(Color.BLACK);
        Font fuenteH = new Font(Font.HELVETICA, 10, Font.BOLD);
        fuenteH.setColor(Color.WHITE);
        Color ColorRelleno=new Color(50,190,250);//Azul
        PdfPTable tabla = new PdfPTable(2);
        tabla.getDefaultCell().setBorder(0);
        tabla.setWidthPercentage(60);
        tabla.setSpacingBefore(10);
        tabla.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell celda = new PdfPCell(new Paragraph("DETALLE DEL PAGO", fuenteJ));
        celda.setBackgroundColor(ColorRelleno);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        celda.setBorderColor(new Color(37, 150, 50));
        celda.setPaddingBottom(5);
        celda.setColspan(2);
        tabla.addCell(celda);

        celda = new PdfPCell(new Paragraph("CONCEPTO DEL PAGO", fuenteI));
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        celda.setPaddingBottom(5);
        celda.setPaddingTop(5);
        tabla.addCell(celda);

        celda = new PdfPCell(new Paragraph("VALOR CONCEPTO", fuenteI));
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        celda.setPaddingBottom(5);
        celda.setPaddingTop(5);
        tabla.addCell(celda);

        //detalle de la tabla
        DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
        simbolo.setDecimalSeparator('.');
        simbolo.setGroupingSeparator(',');
        DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);
        
        
        //modificar
        double suma = 0;
        for (int i = 0; i < detalleExracto.size(); i++) {
            ExtractoPagoInicialDetalle detalle = detalleExracto.get(i);

            celda = new PdfPCell(new Paragraph(detalle.getDescripcion(), fuenteK));
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            celda.setPaddingBottom(5);
            celda.setPaddingTop(5);
            tabla.addCell(celda);

            celda = new PdfPCell(new Paragraph("$ " + formateador.format(detalle.getValor_concepto()), fuenteK));
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celda.setPaddingBottom(5);
            celda.setPaddingTop(5);
            tabla.addCell(celda);
            suma += detalle.getValor_concepto();
        }
  
        celda = new PdfPCell(new Paragraph("TOTAL A PAGAR :", fuenteH));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBackgroundColor(ColorRelleno);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        celda.setBorderColor(new Color(37, 150, 50));
        celda.setPaddingBottom(5);
        tabla.addCell(celda);

        celda = new PdfPCell(new Paragraph("$ " + formateador.format(suma), fuenteI));
        celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
        celda.setPaddingBottom(5);
        tabla.addCell(celda);

        return tabla;

    }

    private PdfPTable footerExtracto(PdfWriter writer,ExtractoPagoInicial epi) throws DocumentException {

        Font fuenteJ = new Font(Font.HELVETICA, 9, Font.BOLD);
        fuenteJ.setColor(Color.BLACK);

        Font fuenteK = new Font(Font.HELVETICA, 9, Font.NORMAL);
        fuenteK.setColor(Color.BLACK);

        PdfPTable tabla = new PdfPTable(3);
        tabla.getDefaultCell().setBorder(0);
        tabla.setWidthPercentage(100f);
        tabla.setSpacingBefore(10);
        float[] medidaCeldas = {1f, 3f, 4f};
        tabla.setWidths(medidaCeldas);
        Color ColorRelleno=new Color(50,190,250);//Azul
        PdfPCell celda = new PdfPCell(new Paragraph("Nombre:", fuenteJ));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(Rectangle.NO_BORDER);
        celda.setBackgroundColor(ColorRelleno);
        celda.setBorderColor(new Color(87, 164, 205));
        celda.setPaddingBottom(5);

        tabla.addCell(celda);

        celda = new PdfPCell(new Paragraph(epi.getNombre_cliente(), fuenteK));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(Rectangle.NO_BORDER);
        celda.setBackgroundColor(ColorRelleno);
        celda.setBorderColor(new Color(87, 164, 205));
        celda.setPaddingBottom(5);

        tabla.addCell(celda);

        //codigo de barras
        celda = new PdfPCell(new Paragraph("", fuenteJ));
        PdfContentByte cb = writer.getDirectContent();
        Barcode128 uccEan128 = new Barcode128();
        uccEan128.setCodeType(Barcode.CODE128_UCC);
        uccEan128.setCode("(415)" + epi.getReferencia_1() + "(8020)" + epi.getReferencia_2()+ "(3900)" + epi.getReferencia_3()+ "(96)" + epi.getReferencia_4());
        uccEan128.setBarHeight(60f);
        uccEan128.setSize(8f);
        celda.addElement(uccEan128.createImageWithBarcode(cb, Color.BLACK, Color.BLACK));
        celda.setBorder(Rectangle.NO_BORDER);
        celda.setRowspan(4);
        tabla.addCell(celda);

        celda = new PdfPCell(new Paragraph("Direccion:", fuenteJ));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(Rectangle.NO_BORDER);
        celda.setBackgroundColor(ColorRelleno);
        celda.setBorderColor(new Color(87, 164, 205));
        celda.setPaddingBottom(5);
        tabla.addCell(celda);

        celda = new PdfPCell(new Paragraph(epi.getDireccion(), fuenteK));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(Rectangle.NO_BORDER);
        celda.setBackgroundColor(ColorRelleno);
        celda.setBorderColor(new Color(87, 164, 205));
        celda.setPaddingBottom(5);
        tabla.addCell(celda);

        celda = new PdfPCell(new Paragraph("Barrio:", fuenteJ));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(Rectangle.NO_BORDER);
        celda.setBackgroundColor(ColorRelleno);
        celda.setBorderColor(new Color(87, 164, 205));
        celda.setPaddingBottom(5);
        tabla.addCell(celda);

        celda = new PdfPCell(new Paragraph(epi.getBarrio(), fuenteK));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(Rectangle.NO_BORDER);
        celda.setBackgroundColor(ColorRelleno);
        celda.setBorderColor(new Color(87, 164, 205));
        celda.setPaddingBottom(5);
        tabla.addCell(celda);

        celda = new PdfPCell(new Paragraph("Valor a pagar:", fuenteJ));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(Rectangle.NO_BORDER);
        celda.setBackgroundColor(ColorRelleno);
        celda.setBorderColor(new Color(87, 164, 205));
        celda.setPaddingBottom(5);
        tabla.addCell(celda);

        DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
        simbolo.setDecimalSeparator('.');
        simbolo.setGroupingSeparator(',');
        DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);
            
        celda = new PdfPCell(new Paragraph(formateador.format(epi.getTotal()), fuenteK));
        celda.setHorizontalAlignment(Element.ALIGN_LEFT);
        celda.setBorder(Rectangle.NO_BORDER);
        celda.setBackgroundColor(ColorRelleno);
        celda.setBorderColor(new Color(87, 164, 205));
        celda.setPaddingBottom(5);
        tabla.addCell(celda);

        return tabla;
    }

     /**
     * Crea un objeto de tipo Document
     * @return Objeto creado
     */
    private Document crearDocumento() {
        Document doc = new Document(PageSize.LETTER, 36, 36,100, 36);
        return doc;
    }
    
    private ExtractoPagoInicial infoCabeceraExracto(int id_rop) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "INFORMACION_EXTRACTO";       
        ExtractoPagoInicial epi = null; 
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, id_rop);
      
            rs = ps.executeQuery();          
            
            while (rs.next()) {

                epi = new ExtractoPagoInicial();
                epi.setCod_rop(rs.getString("cod_rop"));
                epi.setNombre_cliente(rs.getString("nombre_cliente"));
                epi.setDireccion(rs.getString("direccion"));
                epi.setCiudad(rs.getString("ciudad"));
                epi.setDepartamento(rs.getString("departamento"));
                epi.setBarrio(rs.getString("barrio"));
                epi.setFecha_generacion(rs.getString("fecha"));
                epi.setLinea_negocio(rs.getString("linea_producto"));
                epi.setEstablecimiento_comercio(rs.getString("estb_comercio"));
                epi.setNegocio(rs.getString("negocio"));
                epi.setTotal(rs.getDouble("total"));
                epi.setVencimiento_rop(rs.getString("vencimiento_rop"));
                epi.setReferencia_1(rs.getString("referencia_1"));
                epi.setReferencia_2(rs.getString("referencia_2"));
                epi.setReferencia_3(rs.getString("referencia_3"));
                epi.setReferencia_4(rs.getString("referencia_4"));              
              

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO INFORMACION_EXTRACTO: infoCabeceraExracto(int id_rop)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return epi;
    }
    
      private ExtractoPagoInicial infoCabeceraExractoDuplicado(int id_rop) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "INFORMACION_EXTRACTO_DUPLICADO";       
        ExtractoPagoInicial epi = null; 
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, id_rop);
      
            rs = ps.executeQuery();          
            
            while (rs.next()) {

                epi = new ExtractoPagoInicial();
                epi.setCod_rop(rs.getString("cod_rop"));
                epi.setNombre_cliente(rs.getString("nombre_cliente"));
                epi.setDireccion(rs.getString("direccion"));
                epi.setCiudad(rs.getString("ciudad"));
                epi.setDepartamento(rs.getString("departamento"));
                epi.setBarrio(rs.getString("barrio"));
                epi.setFecha_generacion(rs.getString("fecha"));
                epi.setLinea_negocio(rs.getString("linea_producto"));
                epi.setEstablecimiento_comercio(rs.getString("estb_comercio"));
                epi.setNegocio(rs.getString("negocio"));
                epi.setTotal(rs.getDouble("total"));
                epi.setVencimiento_rop(rs.getString("vencimiento_rop"));
                epi.setReferencia_1(rs.getString("referencia_1"));
                epi.setReferencia_2(rs.getString("referencia_2"));
                epi.setReferencia_3(rs.getString("referencia_3"));
                epi.setReferencia_4(rs.getString("referencia_4"));              
              

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO INFORMACION_EXTRACTO: infoCabeceraExracto(int id_rop)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return epi;
    }
    
     private ArrayList<ExtractoPagoInicialDetalle> detalleExracto(int id_rop) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "INFORMACION_EXTRACTO_DET";
        ArrayList<ExtractoPagoInicialDetalle> detalles = null;
        
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, id_rop);
      
            rs = ps.executeQuery();          
            ExtractoPagoInicialDetalle epid = null; 
            detalles=new ArrayList<>();
            while (rs.next()) {
                
                epid=new ExtractoPagoInicialDetalle();
                epid.setDescripcion(rs.getString("descripcion"));
                epid.setValor_concepto(rs.getDouble("valor"));
                detalles.add(epid);              

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO INFORMACION_EXTRACTO: detalleExracto(int id_rop)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return detalles;
    }

    @Override
    public ArrayList<ConfiguracionDescuentosObligaciones> cargarDescuentos(Usuario user) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DESCUENTOS";
        ArrayList<ConfiguracionDescuentosObligaciones> list = null;

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));         
            ps.setString(1,user.getDstrct());
            rs = ps.executeQuery();
            list = new ArrayList();
            ConfiguracionDescuentosObligaciones cdo = null;
            while (rs.next()) {
                cdo = new ConfiguracionDescuentosObligaciones();
                cdo.setId(rs.getInt("id"));
                cdo.setConcepto(rs.getString("concepto"));
                cdo.setDescripcion(rs.getString("descripcion"));
                cdo.setDescuento(rs.getInt("descuento"));
                cdo.setPorcentaje_cta_inicial(rs.getInt("porcentaje_cta_inicial"));
                cdo.setAplica_inicial(rs.getString("aplica_incial"));
                cdo.setPeriodo(rs.getInt("periodo"));
                cdo.setTipo_negocio(rs.getString("tipo_negocio"));
                cdo.setId_unidad_negocio(rs.getInt("id_unidad_negocio"));
                cdo.setCreation_user(rs.getString("creation_date"));
                cdo.setUser_update(rs.getString("user_update"));
                list.add(cdo);
            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LOS DESCUENTOS: cargarDescuentos()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return list;
    }

    @Override
    public ArrayList<ConfiguracionTablaInicial> cargarConfInicial(Usuario user) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CONFIGURACION_INICIAL";
        ArrayList<ConfiguracionTablaInicial> list = null;

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));         
            ps.setString(1, user.getDstrct());
            rs = ps.executeQuery();
            list = new ArrayList();
            ConfiguracionTablaInicial cti = null;
            while (rs.next()) {
                cti = new ConfiguracionTablaInicial();
                cti.setTipo_negocio(rs.getString("tipo_negocio"));
                cti.setIncluyeCapital(rs.getString("pago_capital"));
                cti.setIncluyeInteres(rs.getString("pago_interes"));
                cti.setIncluyeIntxmora(rs.getString("pago_intxmora"));
                cti.setIncluyeGac(rs.getString("pago_gac"));
                cti.setPagoTotal(rs.getString("pago_total"));
                list.add(cti);             
            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LA CONFIGURACION INICIAL: cargarConfInicial()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return list;
    }

    @Override
    public String crearNotaAjusteFenalco(int id_rop, Usuario usuario) throws SQLException {

        String retorno = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CREAR_NOTA_AJUSTE";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, id_rop);
            ps.setString(2, usuario.getLogin());
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("retorno");

                if (!retorno.equals("")) {

                    String[] respuesta = retorno.split(";");

                    if (respuesta[0].equals("OK")) {

                        NegociosGenService genService = new NegociosGenService(usuario.getBd());
                        Negocios negocioF = genService.buscarNegocio(respuesta[2]);

                        BeanGeneral bg = new BeanGeneral();
                        bg.setValor_01(negocioF.getCod_cli()); //codcli - nit del cliente
                        bg.setValor_02(negocioF.getNodocs() + ""); // numero de cuotas
                        bg.setValor_03((negocioF.getTotpagado() / negocioF.getNodocs()) + ""); //Valor de las cuotas
                        bg.setValor_04(usuario.getLogin()); //usuario en sesion
                        bg.setValor_05("PG"); //tipo de negocio: cheque, letra, etc
                        bg.setValor_06(negocioF.getCod_negocio()); //codigo del negocio
                        bg.setValor_07(usuario.getBase()); //base
                        bg.setValor_08("RD-" + negocioF.getCod_cli());
                        bg.setValor_09(negocioF.getNit_tercero()); //nit tercero (nit del afiliado)
                        bg.setValor_10(negocioF.getTotpagado() + ""); //total pagado(valor del campo tot_pagado en la tabla negocios)
                        bg.setValor_11(negocioF.getVr_desem() + ""); //valor del desembolso
                        bg.setValor_12(negocioF.getFecha_neg()); //fecha del negocio
                        bg.setValor_13(negocioF.getNumaval()); //numero de aval
                        bg.setValor_15(negocioF.getvalor_aval()); //valor del aval
                        bg.setValor_16(negocioF.getTneg()); //Codigo del titulo valor
                        bg.setValor_17(negocioF.getId_convenio() + ""); //codigo del convenio
                        bg.setValor_18(usuario.getDstrct()); //distrito del usuario en sesion
                        bg.setValor_27(negocioF.getValor_capacitacion() + "");//valor capacitacion
                        bg.setValor_28(negocioF.getValor_seguro() + "");//valor seguro
                        bg.setValor_29(negocioF.getVr_negocio() + "");// valor negocio

                        TransaccionService tService = new TransaccionService(usuario.getBd());
                        tService.crearStatement();
                        ArrayList detNegocio = genService.buscarDetallesNegocio(bg.getValor_06());
                        genService.setLiquidacion(detNegocio);
                        ArrayList listQuery = genService.generarDocumentosCXCReestructuracionFenalco(bg, usuario, negocioF,genService.getLiquidacion());

                        for (int i = 0; i < listQuery.size(); i++) {
                            String sql = (String) listQuery.get(i);
                            tService.getSt().addBatch(sql);
                        }

                        tService.execute();
                        
                        retorno="OK";
                    }
                }else{
                    retorno="FAIL";
                }
            }

        } catch (SQLException ex) {
            retorno="EXCEPTION :"+ex.getMessage();
            throw new SQLException("ERROR CREANDO NOTA D EAJUSTE FENALCO: crearNotaAjusteFenalco(int id_rop, String usuario)   " + ex.getMessage());
           
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return retorno;
        }

    }

    @Override
    public String guardarTablaPagoInicial(tablaPagoInicial pagoInicial,int idRop, Usuario user) throws SQLException {
        
         String cadena = "";
        String query = "SQL_GUARDAR_TABLA_INICIAL";
        String sql = "";

        try {

            StringStatement st = null;
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            
            st.setInt(1,idRop);
            st.setString(2,pagoInicial.getNegocio());
            st.setString(3,pagoInicial.getTipo_neg());
            st.setDouble(4,Double.parseDouble(pagoInicial.getCapital().replace(",", "")));            
            st.setDouble(5,Double.parseDouble(pagoInicial.getInteres().replace(",", "")));            
            st.setDouble(6,Double.parseDouble(pagoInicial.getIntXmora().replace(",", "")));            
            st.setDouble(7,Double.parseDouble(pagoInicial.getGAC().replace(",", "")));            
            st.setDouble(8,Double.parseDouble(pagoInicial.getTotal().replace(",", "")));            
            st.setDouble(9,Double.parseDouble(pagoInicial.getPctPagar()));            
            st.setDouble(10,Double.parseDouble(pagoInicial.getValorPagar().replace(",", "")));            
            st.setDouble(11,Double.parseDouble(pagoInicial.getSaldoVencido().replace(",", "")));
            st.setDouble(12,Double.parseDouble(pagoInicial.getSaldoCorriente().replace(",", "")));            
            st.setString(13,user.getLogin());


            cadena = st.getSql();
            st = null;

        } catch (Exception ex) {
            cadena = ex.getMessage();  
        } finally {
            return cadena;
        }
    }

    @Override
    public ArrayList<BeanGeneral> buscarNegociosPorAprobar(String aprobar) throws SQLException {
       
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_NEGOCIOS_POR_APROBAR";
        String filtro="";
        ArrayList<BeanGeneral> list = null;

        try {
            
            //and aprobado=?
             switch(aprobar){
                case "T":
                    filtro="and impreso='N'";
                    break;
                case "A":
                    filtro="and aprobado='N' and impreso='N'";
                    break;
                case "I":
                    filtro="and aprobado='S' and impreso='N' ";
                    break;  
                 default: 
                    
            }
            

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro2", filtro));         

            rs = ps.executeQuery();
            list = new ArrayList();
            BeanGeneral bg = null;
            while (rs.next()) {
                bg=new BeanGeneral();
                bg.setValor_01(rs.getString("negocio"));
                bg.setValor_02(rs.getString("cod_rop"));
                bg.setValor_03(rs.getString("cedula"));
                bg.setValor_04(rs.getString("nombre_cliente"));
                bg.setValor_05(rs.getString("aprobado"));
                bg.setValor_06(rs.getString("creation_user"));
                bg.setValor_07(rs.getString("id_rop"));
                list.add(bg);             
            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO EN BUSCAR_NEGOCIOS_POR_APROBAR: buscarNegociosPorAprobar()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return list;
    }

    @Override
    public ArrayList<resumenSaldosReestructuracion> buscarResumenSaldos(int idRop) throws SQLException {
       
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_RESUMEN_SALDOS";
        ArrayList<resumenSaldosReestructuracion> list = null;

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));         
            ps.setInt(1, idRop);
            rs = ps.executeQuery();
            list = new ArrayList();
            resumenSaldosReestructuracion reestructuracion = null;
            while (rs.next()) {
                reestructuracion=new resumenSaldosReestructuracion();
                reestructuracion.setNegocio(rs.getString("negocio"));
                reestructuracion.setEstado(rs.getString("estado_cartera"));
                reestructuracion.setTipo_neg(rs.getString("tipo_negocio"));
                reestructuracion.setCapital(String.valueOf(rs.getDouble("capital")));
                reestructuracion.setInteres(String.valueOf(rs.getDouble("interes")));
                reestructuracion.setIntxMora(String.valueOf(rs.getDouble("intxmora")));
                reestructuracion.setGaC(String.valueOf(rs.getDouble("gasto_cobranza")));
                reestructuracion.setSub_Total(String.valueOf(rs.getDouble("sub_total")));
                reestructuracion.setDcto_capital(String.valueOf(rs.getDouble("dcto_capital")));
                reestructuracion.setDcto_interes(String.valueOf(rs.getDouble("dcto_interes")));
                reestructuracion.setDcto_intxmora(String.valueOf(rs.getDouble("dcto_intxmora")));
                reestructuracion.setDcto_gac(String.valueOf(rs.getDouble("dcto_gasto_cobranza")));
                reestructuracion.setTotal_Dcto(String.valueOf(rs.getDouble("total_descuento")));
                reestructuracion.setTotal_Items(String.valueOf(rs.getDouble("total_items")));

                list.add(reestructuracion);             
            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO EN BUSCAR_RESUMEN_SALDOS: buscarResumenSaldos(int idRop)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return list;        
    }

    @Override
    public ArrayList<tablaPagoInicial> buscarTablaPagoInicial(int idRop) throws SQLException {
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_TABLA_INICIAL";
        ArrayList<tablaPagoInicial> list = null;

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));         
            ps.setInt(1, idRop);
            rs = ps.executeQuery();
            list = new ArrayList();
            tablaPagoInicial  pagoInicial= null;
            while (rs.next()) {
                
                pagoInicial=new tablaPagoInicial();
                pagoInicial.setNegocio(rs.getString("negocio"));
                pagoInicial.setTipo_neg(rs.getString("tipo_negocio"));
                pagoInicial.setCapital(String.valueOf(rs.getDouble("capital")));
                pagoInicial.setInteres(String.valueOf(rs.getDouble("interes")));
                pagoInicial.setIntXmora(String.valueOf(rs.getDouble("intx_mora")));
                pagoInicial.setGAC(String.valueOf(rs.getDouble("gac")));
                pagoInicial.setTotal(String.valueOf(rs.getDouble("total")));
                pagoInicial.setPctPagar(String.valueOf(rs.getDouble("pct_pagar")));
                pagoInicial.setValorPagar(String.valueOf(rs.getDouble("valor_pagar")));
                pagoInicial.setSaldoVencido(String.valueOf(rs.getDouble("saldo_vencido")));
                pagoInicial.setSaldoCorriente(String.valueOf(rs.getDouble("saldo_corriente")));

                list.add(pagoInicial);             
            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO EN BUSCAR_RESUMEN_SALDOS: buscarResumenSaldos(int idRop)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return list; 
        
    }

    @Override
    public String validarPermisos(String usuario) throws SQLException {
    
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_PERMISOS";
        String dato="";

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));         
            ps.setString(1, usuario);
            rs = ps.executeQuery();
         
            tablaPagoInicial  pagoInicial= null;
            while (rs.next()) {
                dato=rs.getString("dato");
            }
            
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO ENBUSCAR_PERMISOS: validarPermisos(String usuario)   " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return dato;        
    
    }

    @Override
    public String aprobarReestructuracion(int idRop,String usuario) throws SQLException {
        
        Connection con = null;
        PreparedStatement ps = null;       
        String query = "SQL_UPDATE_TABLA_INICIAL";
        String  respuestaJson = "{}";

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));        
            ps.setString(1, usuario);
            ps.setInt(2, idRop);
            
            int executeUpdate = ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";;
            
        } catch (SQLException ex) {
            respuestaJson = "{\"respuesta\":\"FAIL\"}";;
            throw new SQLException("ERROR OBTENIENDO EN UPDATE_TABLA_INICIAL: aprobarReestructuracion(int idRop,String usuario) " + ex.getMessage());
        } finally {          
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return respuestaJson;  
        }
    }

    @Override
    public String actualizarImpresion(int idRop, String usuario) throws SQLException {
      
        Connection con = null;
        PreparedStatement ps = null;       
        String query = "SQL_UPDATE_IMPRESION";
        String  respuestaJson = "{}";

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));        
            ps.setString(1, usuario);
            ps.setInt(2, idRop);
            
            int executeUpdate = ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        } catch (SQLException ex) {
            respuestaJson = "{\"respuesta\":\"FAIL\"}";;
            throw new SQLException("ERROR OBTENIENDO EN UPDATE_TABLA_INICIAL:actualizarImpresion(int idRop, String usuario) " + ex.getMessage());
        } finally {          
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return respuestaJson;  
        }
    
    }

    @Override
    public String buscarCabeceralIquidacion(int idRop) throws SQLException {
       
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_INFO_LIQ";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, idRop);
            rs = ps.executeQuery();

            while (rs.next()) {
                
                obj.addProperty("tasa", rs.getDouble("tasa"));
                obj.addProperty("capital", rs.getDouble("capital"));
                obj.addProperty("seguro", rs.getDouble("seguro"));
                obj.addProperty("custodia", rs.getDouble("custodia"));
                obj.addProperty("seguro", rs.getDouble("seguro"));
                obj.addProperty("remesa", rs.getDouble("remesa"));
                obj.addProperty("valor_cuota", rs.getDouble("valor_cuota"));
                obj.addProperty("numcuota", rs.getDouble("numcuota"));
                obj.addProperty("fecha_pr_cuota", rs.getString("fecha_pr_cuota"));              

            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }


            return respuestaJson;
        }
        
    }

    @Override
    public ArrayList<DocumentosNegAceptado> buscarliquidacionFenalco(int idRop) throws SQLException {
        
         Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_LIQUIDACION_FENALCO";
        ArrayList<DocumentosNegAceptado> liquidacion = null;

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));          
            ps.setInt(1, idRop);
            rs = ps.executeQuery();
            liquidacion = new ArrayList();
            DocumentosNegAceptado dna = null;
            while (rs.next()) {

                dna = new DocumentosNegAceptado();
                dna.setFecha(rs.getString("fecha"));
                dna.setDias(rs.getString("dias"));
                dna.setItem(rs.getString("cuota"));
                dna.setSaldo_inicial(rs.getDouble("saldo_inicial"));
                dna.setValor(rs.getDouble("valor_cuota"));
                dna.setCapital(rs.getDouble("capital"));
                dna.setInteres(rs.getDouble("interes"));                
                dna.setCustodia(rs.getDouble("custodia"));
                dna.setSeguro(rs.getDouble("seguro"));
                dna.setRemesa(rs.getDouble("remesa"));
                dna.setSaldo_final(rs.getDouble("saldo_final"));

                liquidacion.add(dna);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO LIQUIDACION FENALCO: SQL_BUSCAR_LIQUIDACION_FENALCO(int idRop)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return liquidacion;
        
    }

    @Override
    public String rechazarReestructuracionFenalco(int id_rop) throws SQLException {
       
        Connection con = null;
        PreparedStatement ps = null;       
        String query = "SQL_BUSCAR_LIQUIDACION_FENALCO";
        String  respuestaJson = "{}";

        try {
            
            con = this.conectarJNDI(query);
            con.setAutoCommit(false);
            Statement stmt = con.createStatement();

            stmt.addBatch("UPDATE tabla_pago_inicial_reestruturacion SET reg_status='A'  WHERE id_rop="+id_rop);
            stmt.addBatch("UPDATE resumen_saldo_reestructuracion SET reg_status='A'  WHERE id_rop="+id_rop);
                       
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();            
            respuestaJson = "{\"respuesta\":\"OK\"}";;
            
        } catch (SQLException ex) {
            respuestaJson = "{\"respuesta\":\"FAIL\"}";;
            throw new SQLException("ERROR RCHAZANDO REESTRUCTURACION:rechazarReestructuracionFenalco(int id_rop) " + ex.getMessage());
        } finally {          
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return respuestaJson;  
        }
        
        
    }
    
    @Override
    public ArrayList<ConfiguracionDescuentosObligaciones> buscarConfigActual() throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_CONFIG_ACTUAL";
        ArrayList<ConfiguracionDescuentosObligaciones> listaDescuento = null;

        try {
            String sql = "";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            listaDescuento = new ArrayList();
            ConfiguracionDescuentosObligaciones desc_oblig = null;
          
            while (rs.next()) {

                desc_oblig = new ConfiguracionDescuentosObligaciones();
                desc_oblig.setId(rs.getInt("id"));
                desc_oblig.setTipo_negocio(rs.getString("tipo_negocio"));
                desc_oblig.setConcepto(rs.getString("idconcepto"));
                desc_oblig.setDescripcion(rs.getString("concepto"));
                desc_oblig.setDescuento(rs.getInt("descuento"));
                desc_oblig.setPorcentaje_cta_inicial(rs.getInt("porc_cta_inicial"));
                desc_oblig.setAplica_inicial(rs.getString("aplica_incial"));               
                listaDescuento.add(desc_oblig);

            }
           
           
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO CONFIGURACION DESCUENTO: SQL_BUSCAR_CONFIG_ACTUAL()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaDescuento;

    }

    @Override
    public String actualizarConfigActual(int id,String tiponeg,int descuento, int porc_cta_ini,String tipo, String usuario) throws SQLException {
      
        Connection con = null;
        PreparedStatement ps = null;       
        String query = "";
        if (tipo.equals("desc")) {
            query = "SQL_UPDATE_CONFIG_ACTUAL_DESC";
        } else {
            query = "SQL_UPDATE_CONFIG_ACTUAL_PORC";
        }
        String  respuestaJson = "{}";

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));  
             if (tipo.equals("desc")) {
                 ps.setInt(1, descuento);               
                 ps.setString(2, usuario);
                 ps.setInt(3, id);
             }else{
                   ps.setInt(1, porc_cta_ini);
                   ps.setString(2, usuario);
                   ps.setString(3, tiponeg);                                  
             }
                  
            
            int executeUpdate = ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";;
            
        } catch (SQLException ex) {
            respuestaJson ="{\"error\":\"" + ex.getMessage() + "\"}";
            throw new SQLException("ERROR OBTENIENDO EN UPDATE_CONFIG_ACTUAL:actualizarConfigActual(int id, String tiponeg, int descuento, int porc_cta_ini, String usuario) " + ex.getMessage());
        } finally {          
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return respuestaJson;  
        }
    
    }
    
    @Override
    public int insertarConfigActual(String usuario) throws SQLException {
      
        Connection con = null;
        PreparedStatement ps = null;       
        String query = "SQL_INSERTAR_CONFIG_ACTUAL";
       int resp=0;
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));        
            ps.setString(1, usuario);         
            resp = ps.executeUpdate();
           
            
        } catch (SQLException ex) {
            resp=0;           
            throw new SQLException("ERROR OBTENIENDO EN INSERTAR_CONFIG_ACTUAL:insertarConfigActual(String usuario) " + ex.getMessage());
        } finally {          
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return resp;  
        }
    
    }
    
     public int insertarConfigPagoIni(String usuario) throws SQLException {
      
        Connection con = null;
        PreparedStatement ps = null;       
        String query = "SQL_INSERTAR_CONFIG_PAGO_INI";
        int resp=0;
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));        
            ps.setString(1, usuario);         
            resp = ps.executeUpdate();                  
            
        } catch (SQLException ex) {
            resp=0;           
            throw new SQLException("ERROR OBTENIENDO EN INSERTAR_CONFIG_ACTUAL:insertarConfigActual(String usuario) " + ex.getMessage());
        } finally {          
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return resp;  
        }
    
    }


    @Override
     public ArrayList<ConfiguracionTablaInicial> buscarConfigPagoIni() throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_CONFIG_PAGO_INI";
        ArrayList<ConfiguracionTablaInicial> listaPagoInicial = null;

        try {
            String sql = "";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            listaPagoInicial = new ArrayList();
            ConfiguracionTablaInicial pago_ini = null;
          
            while (rs.next()) {

                pago_ini = new ConfiguracionTablaInicial();
                pago_ini.setId(rs.getInt("id"));
                pago_ini.setTipo_negocio(rs.getString("tipo_negocio"));
                pago_ini.setIncluyeCapital(rs.getString("pago_capital"));
                pago_ini.setIncluyeInteres(rs.getString("pago_interes"));
                pago_ini.setIncluyeIntxmora(rs.getString("pago_intxmora"));
                pago_ini.setIncluyeGac(rs.getString("pago_gac"));
                pago_ini.setPagoTotal(rs.getString("pago_total"));               
                listaPagoInicial.add(pago_ini);

            }
           
           
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO CONFIGURACION PAGO INICIAL: buscarConfigPagoIni()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaPagoInicial;

    }

    @Override
    public String cambiarCuentasOrden(String negocio,Usuario usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CAMBIAR_CUENTAS";
        String respuestaJson = "{}";

        try {

            con = this.conectarJNDI(query, usuario.getBd());
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();

            if (rs.next()) {
                respuestaJson = rs.getString("retorno");
            }

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return respuestaJson;
        }
    }

    @Override
    public void generarPdfDuplicado(String userlogin, int id_rop) {
        try {
            String directorio = "";
            ResourceBundle rb = null;

            //buscamos la info de la cabecera del extracto.
            ExtractoPagoInicial epi = this.infoCabeceraExractoDuplicado(id_rop);

            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "Extracto_" + id_rop, "pdf");
            Document documento = null;
            documento = this.crearDocumento();
            //Creamos una instancia del la clase HeaderFooter para agregar el encabezado de la pagina
            PdfWriter writer = PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            HeaderFooter headerFooter = new HeaderFooter(rb, epi.getCod_rop());
            writer.setPageEvent(headerFooter);
            //abrimos el documento.
            documento.open();
            //informacion del cliente
            documento.add(this.informacionCliente(epi));
            //leyendas         
            //se valida si es generacion de estado de cuenta EEC
            if (epi.getCod_rop().substring(0, 3).equals("EEC")) {
                documento.add(this.leyendas(epi.getVencimiento_rop()));
            } else {
                documento.add(this.leyendas(epi.getFecha_generacion()));
            }
            Font fuenteK = new Font(Font.HELVETICA, 11, Font.ITALIC);
            fuenteK.setColor(Color.GRAY);
            Paragraph texto = new Paragraph("Ponte al d�a con tu cr�dito y evita costos que incrementen el valor de la cuota! | �Fintra sigue creyendo en Ti! ", fuenteK);
            texto.setSpacingBefore(5f);
            documento.add(texto);
            //estadoCredito
            documento.add(this.estadoCredito());
            //detalle del pago
            documento.add(this.detallePago(id_rop));
            //linea de corte
            texto = new Paragraph("---------------------------------------------------------------------------------------------------------------------------------------------------", fuenteK);
            texto.setSpacingBefore(5f);
            documento.add(texto);
            documento.add(this.footerExtracto(writer, epi));

            documento.close();
        } catch (SQLException ex) {
            Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    @Override
    public String cargarConveniosMicro() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_CONVENIOS_MICRO";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));           
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id_convenio"), rs.getString("nombre"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }


            return respuestaJson;
        }

    }
    
    @Override
    public ArrayList<DocumentosNegAceptado> buscarLiquidacionMicro(Double valor_negocio, int num_cuotas, String fecha_item,String tipo_cuota,String id_convenio, String fecha_liquidacion) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_LIQUIDACION_MICRO";
        ArrayList<DocumentosNegAceptado> liquidacion = null;

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setDouble(1, valor_negocio);
            ps.setInt(2, num_cuotas);
            ps.setString(3, fecha_item);
            ps.setString(4, tipo_cuota);
            ps.setString(5, id_convenio);
            ps.setString(6, fecha_liquidacion);
           
            rs = ps.executeQuery();
            liquidacion = new ArrayList();
            DocumentosNegAceptado dna = null;
            while (rs.next()) {

                dna = new DocumentosNegAceptado();
                dna.setFecha(rs.getString("fecha"));              
                dna.setItem(rs.getString("cuota"));
                dna.setSaldo_inicial(rs.getDouble("saldo_inicial"));
                dna.setValor(rs.getDouble("valor_cuota"));
                dna.setCapital(rs.getDouble("capital"));
                dna.setInteres(rs.getDouble("interes"));
                dna.setCapacitacion(rs.getDouble("capacitacion"));
                dna.setCat(rs.getDouble("cat"));
                dna.setSeguro(rs.getDouble("seguro"));
                dna.setSaldo_final(rs.getDouble("saldo_final"));

                liquidacion.add(dna);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO SQL_BUSCAR_LIQUIDACION_MICRO: buscarLiquidacionMicro()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return liquidacion;
    }

    @Override
    public String getInfoNegociosLiquidarMicro(String codigoNegocio, Usuario usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_INFO_LIQ_NEG_MICRO";
        String respuestaJson = "";
        Gson gson = new Gson();      
       
        try {
            String sql = "";
            con = this.conectarJNDI(query);  
            ps = con.prepareStatement(this.obtenerSQL(query));           
            ps.setString(1, usuario.getLogin());
            ps.setString(2, codigoNegocio);
            rs = ps.executeQuery();          
            JsonObject obj = new JsonObject();           
            while (rs.next()) {
                obj.addProperty("tipo_cuota", rs.getString("tipo_cuota"));
                obj.addProperty("nro_docs", rs.getString("nro_docs"));
                obj.addProperty("vr_negocio", rs.getString("vr_negocio"));
                obj.addProperty("fecha_liquidacion", rs.getString("fecha_liquidacion"));
                obj.addProperty("fecha_primera_cuota", rs.getString("fecha_primera_cuota"));
                obj.addProperty("id_convenio", rs.getString("id_convenio"));        
                obj.addProperty("aut_reliquidar", rs.getString("autorizado_reliquidar"));
            }
            
               respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
            throw new SQLException("ERROR OBTENIENDO INFO NEGOCIO LIQUIDAR: getInfoNegociosLiquidarMicro()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return respuestaJson;


    }
    
    @Override
    public String actualizarLiquidacionMicro(String codigoNegocio, String tipo_cuota, int num_cuotas, Double valor_negocio, String fecha_liquidacion, String fecha_item,String id_convenio) {
        Connection con = null;
        PreparedStatement ps = null;  
        String query = "SQL_UPDATE_LIQUIDACION_MICRO";
        String  respuestaJson = "{}", resp = "";
        ResultSet rs = null;
              
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, codigoNegocio);
            ps.setDouble(2, valor_negocio);
            ps.setInt(3, num_cuotas);    
            ps.setString(4, fecha_item);
            ps.setString(5, tipo_cuota);
            ps.setString(6, id_convenio);
            ps.setString(7, fecha_liquidacion);
                       
            rs = ps.executeQuery();  
            if (rs.next()) {
                resp = rs.getString(1);
            }
            
            respuestaJson = "{\"respuesta\":\""+ resp +"\"}";
            
        }catch (Exception e) {
            try {          
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarLiquidacionMicro \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return respuestaJson; 
       }
    }

    static class HeaderFooter extends PdfPageEventHelper {

        private Image img;
        private String num_extracto;
        public HeaderFooter(ResourceBundle rb, String num_extracto) {
        try {
                String url_logo = "Logo-Fintra.png";
                img = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
                img.setAbsolutePosition(0, 0f);
                img.scalePercent(75);
                this.num_extracto=num_extracto;
            } catch (BadElementException | IOException r) {
                System.err.println("Error al leer la imagen");
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            try {
                document.add(img);
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("REFERENCIA DE PAGO: "+this.num_extracto,new Font(Font.ITALIC, 12, Font.BOLD)), 230, 730f, 0);
            } catch (DocumentException ex) {
                Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}



