/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.AdministracionEtesDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.TransportadorasLogBeans;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mariana
 */
public class AdministracionEtesImpl extends MainDAO implements AdministracionEtesDAO {

    public AdministracionEtesImpl(String dataBaseName) {
        super("AdministracionEtesDAO.xml", dataBaseName);
    }

    @Override
    public String cargarTipoManifiesto() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPOS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("tipo_trama"), rs.getString("tipo_trama"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
            return respuesta;
        }
    }

    @Override
    public String cargarEmpresas(String usuario, String perfil) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EMPRESAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String condicion = (perfil.equals("TRANSPORTAD")) ? " where  trans.idusuario = '" + usuario + "'" : "";
            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            if (!perfil.equals("TRANSPORTAD")) {
                obj.addProperty("", "");
            }
            while (rs.next()) {
                obj.addProperty(rs.getString("idusuario"), rs.getString("razon_social"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return respuesta;
        }
    }

    @Override
    public ArrayList<TransportadorasLogBeans> buscarLogTrama(String fechaInicio, String fechaFinal, String Tipo, String Empresa, String Estado, String planilla) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOSTRAR_LOG";

        ArrayList<TransportadorasLogBeans> lista = new ArrayList<>();

        try {
            String consulta = "";
            con = this.conectarJNDI();

            String fechas = "fecha_inicio_proceso::date between '" + fechaInicio + "'  and '" + fechaFinal + "' ";

            if (!Tipo.equals("")) {
                fechas = fechas + "and  tipo_trama = '" + Tipo + "' ";
            }

            if (!Empresa.equals("")) {
                fechas = fechas + " and id_empresa = (select id from etes.transportadoras where  idusuario = '" + Empresa + "')  ";
            }

            if (!Estado.equals("")) {
                fechas = fechas + "and procesado = (" + Estado + ")::boolean  ";
            }
            
            if (!planilla.equals("")) {
                fechas = fechas + "and json ilike '%\"planilla\":\"" + planilla + "\"%'  ";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", fechas);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            while (rs.next()) {
                TransportadorasLogBeans beanTrans = new TransportadorasLogBeans();
                beanTrans.setIdproceso(rs.getString("id"));
                beanTrans.setEmpresa(rs.getString("razon_social"));
                beanTrans.setEstado(rs.getString("resultado_pro"));
                beanTrans.setTipo(rs.getString("tipo_trama"));
                beanTrans.setExcepcion(rs.getString("exceptions"));
                beanTrans.setFechainicio(rs.getString("fecha_inicio"));
                beanTrans.setFechafin(rs.getString("fecha_fin"));
                beanTrans.setObservacion(rs.getString("observaciones"));
                beanTrans.setTiempo(rs.getString("tiempo"));

                lista.add(beanTrans);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String cargarjsonTrama(String Id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOSTRAR_JSON";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, Id);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("json");
            }

        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
        }
        return respuesta;
    }

    @Override
    public ArrayList<TransportadorasLogBeans> cargarGrillaProductosTrans() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOSTRAR_GRILLA_PRODUCTOS_TRANS";

        ArrayList<TransportadorasLogBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                TransportadorasLogBeans beanEtes = new TransportadorasLogBeans();
                beanEtes.setProId(rs.getString("id"));
                beanEtes.setProCodProducto(rs.getString("codigo_proserv"));
                beanEtes.setProDescripcion(rs.getString("descripcion"));
                beanEtes.setProEstado(rs.getString("Estado"));

                lista.add(beanEtes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String guardarProductosEds(String cia, String nombre_producto) {
        StringStatement st = null;
        String respuesta = "";

        String query = "SQL_GUARDAR_PRODUCTO_TRANS";
        try {

            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, cia);
            st.setString(2, nombre_producto);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cambiarEstadoProductoTrans(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_PRODUCTO_TRANS";

        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);

            st.setString(1, id);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarProductoTrans(String descripcion, String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_PRODUCTO_TRANS";

        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, descripcion);
            st.setString(2, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cargarTransportadoras() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TRANSPORTADORAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("razon_social"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
            return respuesta;
        }
    }

    @Override
    public ArrayList<TransportadorasLogBeans> cargarGrillaTipoDescuento() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_GRILLA_TIPO_DESCUENTO";

        ArrayList<TransportadorasLogBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                TransportadorasLogBeans beanEtes = new TransportadorasLogBeans();
                beanEtes.setTdid(rs.getString("id"));
                beanEtes.setTddescripcion(rs.getString("descripcion"));
                beanEtes.setTdestado(rs.getString("estado"));

                lista.add(beanEtes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String guardarRelProductosTransportadoras(JsonObject info) {
        JsonObject f = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        //String query = "SQL_CARGAR_REL_PRODUCTOS_TRANS";
        TransaccionService tService = null;
        try {
            //query = this.obtenerSQL(query);
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = info.getAsJsonArray("datos");

            for (int i = 0; i < arr.size(); i++) {
                f = arr.get(i).getAsJsonObject();
                if (f.get("cpdid").getAsString().equalsIgnoreCase("")) {
                    //JsonObject f = arr.get(i).getAsJsonObject();
                    st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_REL_PRODUCTOS_TRANS"), true);
                    st.setString(1, info.get("cia").getAsString());
                    st.setString(2, info.get("transportadora").getAsString());
                    st.setString(3, f.get("proId").getAsString());
                    st.setString(4, f.get("tdid").getAsString());
                    st.setString(5, f.get("cpddescripcion").getAsString());
                    st.setString(6, f.get("cpdporcent").getAsString());
                    st.setString(7, f.get("cpdvalor").getAsString());
                    st.setString(8, f.get("cpddescripcioncorta").getAsString());
                } else {
                    st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_REL_PRODUCTO_TRANS"), true);
                    st.setInt(1, info.get("transportadora").getAsInt());
                    st.setInt(2, f.get("proId").getAsInt());
                    st.setInt(3, f.get("tdid").getAsInt());
                    st.setString(4, f.get("cpddescripcion").getAsString());
                    st.setString(5, f.get("cpdporcent").getAsString());
                    st.setString(6, f.get("cpdvalor").getAsString());
                    st.setString(7, f.get("cpddescripcioncorta").getAsString());
                    st.setString(8, f.get("cpdid").getAsString());
                }

                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado:)\"}";
            //respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            //respuesta = ex.getMessage();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
            }
            return respuesta;
        }
    }

    @Override
    public ArrayList<TransportadorasLogBeans> cargarGrillaRelProTrans(String idtrans) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_REL_PRODUCTOS_TRANS";

        ArrayList<TransportadorasLogBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idtrans);
            rs = ps.executeQuery();

            while (rs.next()) {
                TransportadorasLogBeans beanEtes = new TransportadorasLogBeans();
                beanEtes.setCpdid(rs.getString("id"));
                beanEtes.setProId(rs.getString("proid"));
                beanEtes.setProDescripcion(rs.getString("descripcion"));
                beanEtes.setTddescripcion(rs.getString("desc"));
                beanEtes.setTdid(rs.getString("id_tipo_descuentos"));
                beanEtes.setCpddescripcion(rs.getString("descripcion_descuento"));
                beanEtes.setCpddescripcioncorta(rs.getString("descripcion_corta"));
                beanEtes.setCpdporcent(rs.getString("porcentaje_descuento"));
                beanEtes.setCpdvalor(rs.getString("valor_descuento"));
                beanEtes.setCpdestado(rs.getString("estado"));

                lista.add(beanEtes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String cargarProductosTrans() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PRODUCTOS_TRANS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            obj.addProperty("0", "....");
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
            return respuesta;
        }
    }

    @Override
    public String actualizarRelProTrans(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_REL_PRODUCTO_TRANS";

        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cargarTipoDescuento() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_DESCUENTOS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            obj.addProperty("0", "....");
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
            return respuesta;
        }
    }

    @Override
    public String cambiarEstadoTransPro(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_REL_TRANS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarTipoDescuento(String cia, String nomtipdes) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_TIPO_DESCUENTO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, cia);
            st.setString(2, nomtipdes);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarTipoDescuento(String id, String nombretipodes) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_TIPO_DESCUENTO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, nombretipodes);
            st.setString(2, id);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cambiarEstadoTipoDescuento(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_TIPO_DESCUENTO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public ArrayList<TransportadorasLogBeans> cargarGrillaTransportadoras() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_GRILLA_TRANSPORTADORAS";

        ArrayList<TransportadorasLogBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                TransportadorasLogBeans beanEtes = new TransportadorasLogBeans();
                beanEtes.setTransid(rs.getString("id"));
                beanEtes.setTranscod(rs.getString("cod_transportadora"));
                beanEtes.setTranrazonsocial(rs.getString("razon_social"));
                beanEtes.setTransnit(rs.getString("identificacion"));
                beanEtes.setTrandireccion(rs.getString("direccion"));
                beanEtes.setTrancorreo(rs.getString("correo"));
                beanEtes.setPerid(rs.getString("id_periodicidad"));
                beanEtes.setPerdescripcion(rs.getString("periodicidad"));
                beanEtes.setCupo_rotativo(rs.getString("cupo_rotativo"));
                beanEtes.setTranusuario(rs.getString("idusuario"));
                beanEtes.setClave(rs.getString("claveencr"));
                beanEtes.setPais(rs.getString("pais"));
                beanEtes.setDepartamento(rs.getString("dpto"));
                beanEtes.setCiudad(rs.getString("ciudad"));
                beanEtes.setTranreplegal(rs.getString("representante_legal"));
                beanEtes.setTrandoc(rs.getString("doc_representante"));
                beanEtes.setTranestado(rs.getString("estado"));
                beanEtes.setHc(rs.getString("hc"));
                beanEtes.setTipodoc(rs.getString("tipo_doc"));
                beanEtes.setCodcli(rs.getString("codcli"));
                beanEtes.setAutoriza_venta(rs.getString("autoriza_venta"));
                beanEtes.setAutoriza_ventas(rs.getString("autorizar_ventas"));
                beanEtes.setEstado_usuario(rs.getString("estado_usuarios"));

                lista.add(beanEtes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String guardarTransportadoras(String nombretran, String nittran, String direcciontran, String correo, String nombreencargado, String identificacion, String usuario, String cia, String periodicidad, String idusuario,String cuporot) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_TRANSPORTADORAS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, cia);
            st.setString(2, nittran);
            st.setString(3, nombretran);
            st.setString(4, direcciontran);
            st.setString(5, correo);
            st.setString(6, identificacion);
            st.setString(7, nombreencargado);
            st.setString(8, usuario);
            st.setString(9, periodicidad);
            st.setString(10, idusuario);
            st.setString(11, cuporot);
            st.setString(12, nittran);
            

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            try {

            } catch (Exception e) {
            }
            return respuesta;
        }
    }

    @Override
    public String cargarDepartamento(String pais) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DEPARTAMENTO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, pais);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("department_code"), rs.getString("department_name"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarCiudad(String departamento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CIUDAD";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, departamento);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("codciu"), rs.getString("nomciu"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
        }
        return respuesta;
    }

    @Override
    public String cargarPais() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PAIS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("country_code"), rs.getString("country_name"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String cargarPeriodicidad() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PERIODICIDAD";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String actualizarTransportadoras(String nombretran, String direcciontran, String correo, String nombreencargado, String identificacion, String idusuario, String periodicidad, String usuario, String cuporot,String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_TRANSPORTADORAS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);

            st.setString(1, nombretran);
            st.setString(2, direcciontran);
            st.setString(3, correo);
            st.setString(4, identificacion);
            st.setString(5, nombreencargado);
            st.setString(6, periodicidad);
            st.setString(7, usuario);
            st.setString(8, cuporot);
            st.setString(9, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        }
        return respuesta;
    }

    @Override
    public String cambiarEstadoTransportadoras(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_TRANSPORTADORAS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public ArrayList<TransportadorasLogBeans> cargarCxcGeneradasTransportadoras(String transportadoras, String fecha, String fecha_fin) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CXC_GENERADAS_TRANSPORTADORAS";
        String consulta = "";
        String consulta2 = "";
        String parametro;
        ArrayList<TransportadorasLogBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            consulta = " anticipo.cxc_corrida != '' and anticipo.reg_status='' and trans.id = '" + transportadoras + "'";
            consulta2 = "reanticipo.cxc_corrida != '' and  reanticipo.reg_status='' and trans.id = '" + transportadoras + "'";

            if ((!fecha.equals("null")) && (!fecha.equals(""))) {
                consulta = consulta + " and anticipo.fecha_corrida ::date between '" + fecha + "'::date AND '" + fecha_fin + "'::date ";
                consulta2 = consulta2 + " and reanticipo.fecha_corrida ::date  between '" + fecha + "'::date AND '" + fecha_fin + "'::date ";
            }

            parametro = this.obtenerSQL(query).replaceAll("#parametro1", consulta).replaceAll("#parametro2", consulta2);
            ps = con.prepareStatement(parametro);
            rs = ps.executeQuery();

//            ps.setString(1, transportadoras);
//            ps.setString(2, fecha);
//            ps.setString(3, transportadoras);
//            ps.setString(4, fecha);
//            rs = ps.executeQuery();
            while (rs.next()) {
                TransportadorasLogBeans beanEtes = new TransportadorasLogBeans();
                beanEtes.setTransid(rs.getString("id"));
                beanEtes.setNitcli(rs.getString("nit"));
                beanEtes.setNomcli(rs.getString("nom_cliente"));
                beanEtes.setFacvlr(rs.getString("valor_factura"));
                beanEtes.setFacabono(rs.getString("valor_abono"));
                beanEtes.setFacsaldo(rs.getString("valor_saldo"));
                beanEtes.setFacfechvencimiento(rs.getString("fecha_vencimiento"));
                beanEtes.setFacfechultimopago(rs.getString("fecha_ultimo_pago"));
                beanEtes.setUsercreacion(rs.getString("creation_user"));
                beanEtes.setFacdocumento(rs.getString("documento"));
                beanEtes.setFechacorrida(rs.getString("fecha_corrida"));
                beanEtes.setFaccreacion(rs.getString("creation_date"));
                beanEtes.setNumplanilla(rs.getString("numero_planilla"));
                lista.add(beanEtes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String cargarFechasCXCTrans(String transportadora) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "CARGAR_FECHAS_CXC_GENERADAS_TRANS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, transportadora);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("fecha_corrida"), rs.getString("fecha_corrida"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return respuesta;
        }
    }

    @Override
    public ArrayList<TransportadorasLogBeans> cargarCxcDetallesGeneradasTransportadoras(String transportadoras, String fecha, String cxc) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CXC_DETALLES_GENERADAS_TRANSPORTADORAS";

        ArrayList<TransportadorasLogBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, transportadoras);
            ps.setString(2, fecha);
            ps.setString(3, cxc);
            ps.setString(4, transportadoras);
            ps.setString(5, fecha);
            ps.setString(6, cxc);
            rs = ps.executeQuery();
            while (rs.next()) {
                TransportadorasLogBeans beanEtes = new TransportadorasLogBeans();
                beanEtes.setCxc_corrida(rs.getString("cxc_corrida"));
                beanEtes.setPlanilla(rs.getString("planilla"));
                beanEtes.setTipomanifiesto(rs.getString("reanticipo"));
                beanEtes.setFacvlr(rs.getString("valor_neto_anticipo"));
                beanEtes.setFechcreacion(rs.getString("creation_date"));
                beanEtes.setFechacorrida(rs.getString("fecha_corrida"));
                beanEtes.setFechvencimiento(rs.getString("fecha_pago_fintra"));
                lista.add(beanEtes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public ArrayList<TransportadorasLogBeans> cargarGrillaAgenciasTrans() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_GRILLA_AGENCIAS_TRANS";

        ArrayList<TransportadorasLogBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                TransportadorasLogBeans beanEtes = new TransportadorasLogBeans();
                beanEtes.setTransid(rs.getString("id"));
                beanEtes.setTranscod(rs.getString("cod_transportadora"));
                beanEtes.setTransrazonsocial(rs.getString("razon_social"));
                beanEtes.setAgcodagencia(rs.getString("cod_agencia"));
                beanEtes.setAgnombre(rs.getString("nombre_agencia"));
                beanEtes.setCiudad(rs.getString("nomciu"));
                beanEtes.setAgdireccion(rs.getString("direccion"));
                beanEtes.setAgcorreo(rs.getString("correo"));
                beanEtes.setPais(rs.getString("pais"));
                beanEtes.setDepartamento(rs.getString("dpto"));
                beanEtes.setCodciudad(rs.getString("ciudad"));
                beanEtes.setEstado(rs.getString("estado"));

                lista.add(beanEtes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String guardarAgencia(String nombreagencia, String transportadoras, String correo, String direccion, String ciudad, String usuario) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_AGENCIA_TRANS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, nombreagencia);
            st.setString(2, transportadoras);
            st.setString(3, correo);
            st.setString(4, direccion);
            st.setString(5, ciudad);
            st.setString(6, usuario);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String actualizarAgencia(String nombreagencia, String transportadoras, String correo, String direccion, String ciudad, String usuario, String id) {
        StringStatement st = null;
        String respuesta = "";

        String query = "SQL_ACTUALIZAR_AGENCIA_TRANS";

        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, nombreagencia);
            st.setString(2, transportadoras);
            st.setString(3, correo);
            st.setString(4, direccion);
            st.setString(5, ciudad);
            st.setString(6, usuario);
            st.setString(7, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cambiarEstadoAgencia(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_AGENCIA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String verificarUsuarioTrans(String nittrans) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICAR_IDUSUARIO_TRANS";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nittrans);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString(1);
            }
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
        }
        return respuesta;
    }

    @Override
    public String AutorizarVenta(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_AUTORIZAR_VENTA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }
    
    @Override
    public String desactivarUsuario(String estado,String usuario) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_DESACTIVAR_USUARIO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, estado);
            st.setString(2, usuario);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

}
