/* JPACOSTA. Febrero 2014 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.AsignacionCarteraDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author jpacosta
 */
public class AsignacionCarteraImpl extends MainDAO implements AsignacionCarteraDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private String json = "";

    public AsignacionCarteraImpl(String dataBaseName) {
        super("AsignacionCarteraDAO.xml", dataBaseName);
    }

    @Override
    public String getNegocios(String tipo, String unidad_negocio, String rangoMayor, String periodo, String reasigna, String agencia) {
        Gson gson = new Gson();
        JsonArray datos = new JsonArray();
        JsonObject objeto = new JsonObject();
        JsonObject elem = new JsonObject();
        json = "{}";
        try {
            query = "getNegocios";
            con = this.conectarJNDI(query);
            String filtros = "TRUE";
            String uneg = "";
            if (unidad_negocio.equalsIgnoreCase("vacio") || periodo.equalsIgnoreCase("vacio")) {
                return json;
            }
            if (!rangoMayor.equalsIgnoreCase("vacio")) {
                //para no hacer una subconsulta, declare esta variable 'tempo'
                filtros = " vencimiento_mayor= ";
                switch (Integer.parseInt(rangoMayor)) {
                    case 0: filtros += "'01- CORRIENTE'"; break;
                    case 1: filtros += "'02- 1 A 30'"; break;
                    case 2: filtros += "'03- ENTRE 31 Y 60'"; break;
                    case 3: filtros += "'04- ENTRE 61 Y 90'"; break;
                    case 4: filtros += "'05- ENTRE 91 Y 120'"; break;
                    case 5: filtros += "'06- ENTRE 121 Y 180'"; break;
                    case 6: filtros += "'07- ENTRE 180 Y 210'"; break;
                    case 7: filtros += "'08- ENTRE 210 Y 240'"; break;
                    case 8: filtros += "'09- ENTRE 240 Y 270'"; break;
                    case 9: filtros += "'10- ENTRE 270 Y 300'"; break;
                    case 10: filtros += "'11- ENTRE 300 Y 330'"; break;
                    case 11: filtros += "'12- ENTRE 330 Y 365'"; break;
                    case 12: filtros += "'13- MAYOR A 1 ANIO'"; break;
                    default: break;
                }
            }
            String agente = ("vacio".equalsIgnoreCase(reasigna))
                    ? "AND ("+tipo+" IS NULL OR "+tipo+" = '')"
                    : "AND "+tipo+" = '"+reasigna+"'";
            
            String ag = ("vacio".equalsIgnoreCase(agencia)) ? "" : " AND id_convenio IN (SELECT c.id_convenio\n"
                    + " FROM convenios c INNER JOIN rel_unidadnegocio_convenios ruc\n"
                    + " ON c.id_convenio = ruc.id_convenio\n"
                    + " INNER JOIN unidad_negocio un ON ruc.id_unid_negocio = un.id\n"
                    + " WHERE c.agencia = '" + agencia + "')";

            query = this.obtenerSQL(query).replace("--rango--", filtros).replace("--agente--",agente).replace("--agencia--", ag);

            ps = con.prepareStatement(query);
            ps.setString(1, periodo);
            ps.setString(2, unidad_negocio);
            ps.setString(3, unidad_negocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                
                objeto = new JsonObject();
                objeto.addProperty("negocio",rs.getString("negocio"));
                objeto.addProperty("cedula",rs.getString("cedula"));
                objeto.addProperty("nombre",rs.getString("nombre"));
                objeto.addProperty("departamento",rs.getString("departamento"));
                objeto.addProperty("cuotas_vencidas",rs.getString("cuotas_vencidas"));
                objeto.addProperty("saldo_vencido",rs.getString("saldo_vencido"));
                objeto.addProperty("vencimiento_mayor",rs.getString("vencimiento_mayor"));
                datos.add(objeto);
            }
            elem.add("rows", datos);
            
            json = gson.toJson(elem);
        } catch (Exception e) {
            json = "{mensaje:"+e.getMessage()+"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) {}
            return json;
        }
    }

    @Override
    public String getFiltros(boolean unidades, boolean agencias, boolean periodos, boolean vencimientos, boolean asesores, boolean agentes) {
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarBDJNDI("fintra");
            JsonObject elemento;
            if (periodos) {
                query = "getPeriodos";
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                elemento = new JsonObject();
                while (rs.next()) {
                    elemento.addProperty(rs.getString("id"), rs.getString("valor"));
                }
                obj.add("periodos", elemento);
            }
            if (unidades) {
                query = "getUnidadesNegocios";
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                elemento = new JsonObject();
                //elemento.addProperty("vacio", "");
                while (rs.next()) {
                    elemento.addProperty(rs.getString("id"), rs.getString("valor"));
                }
                obj.add("uNegocios", elemento);
            }
            if (vencimientos) {
                elemento = new JsonObject();
                elemento.addProperty("vacio", "");
                elemento.addProperty("0", "CORRIENTE");
                elemento.addProperty("1", "1 - 30");
                elemento.addProperty("2", "31 - 60");
                elemento.addProperty("3", "61 - 90");
                elemento.addProperty("4", "91 - 120");
                elemento.addProperty("5", "121 - 180");
                elemento.addProperty("6", "181 - 210");
                elemento.addProperty("7", "211 - 240");
                elemento.addProperty("8", "241 - 270");
                elemento.addProperty("9", "271 - 300");
                elemento.addProperty("10", "301 - 330");
                elemento.addProperty("11", "331 - 365");
                elemento.addProperty("12", "MAYOR A 1 ANIO");
                
                obj.add("vencMayor", elemento);
            }
            if (asesores) {
                query = "getAsesores";
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                elemento = new JsonObject();
                elemento.addProperty("vacio", "");
                while (rs.next()) {
                    elemento.addProperty(rs.getString("id"), rs.getString("valor"));
                }
                obj.add("asesores", elemento);
            }
            if (agentes) {
                query = "getAgentes";
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                elemento = new JsonObject();
                elemento.addProperty("vacio", "");
                while (rs.next()) {
                    elemento.addProperty(rs.getString("id"), rs.getString("valor"));
                }
                obj.add("agentes", elemento);
            }
            if (agencias) {
                query = "getAgencias";
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                elemento = new JsonObject();
                while (rs.next()) {
                    elemento.addProperty(rs.getString("agencia"), rs.getString("agencia"));
                }
                obj.add("agencias", elemento);
            }
            json = gson.toJson(obj);
        } catch (Exception e) {
            json = "{\"mensaje\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) {}
            return json;
        }

    }

    @Override
    public String asignar(String tipo, String periodo, String asesor, String usuario, String negocios) {
        try {
            query = "asignarAsesores";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query).replace("--negocios--", negocios).replace("agente",tipo));
            ps.setString(1, asesor);
            ps.setString(2, usuario);
            ps.setString(3, periodo);
            rs = ps.executeQuery();
            Gson gson = new Gson();
            JsonObject obj = new JsonObject();
            JsonObject elemento = new JsonObject();
            while (rs.next()) {
                elemento = (JsonObject) obj.get(rs.getString("negasoc"));
                if (elemento == null) {
                    elemento = new JsonObject();
                    elemento.addProperty("cuotas", 1);
                    elemento.addProperty("valor_asignado", rs.getDouble("valor_saldo"));
                    obj.add(rs.getString("negasoc"), elemento);
                } else {
                    elemento.addProperty("cuotas", (elemento.get("cuotas").getAsInt() + 1));
                    elemento.addProperty("valor_asignado", (elemento.get("valor_asignado").getAsDouble() + rs.getDouble("valor_saldo")));
                }
                obj.add(rs.getString("negasoc"), elemento);
            }
            json = gson.toJson(obj);
        } catch (SQLException ex) {
            json = "{\"mensaje\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) {}
            return json;
        }
    }
    @Override
    public String getFichas(Usuario user, String filtro,String agente, String campoOrden, String agencia) {
        try {
            PreparedStatement psI = null;
            ResultSet rsI = null;
            
            String orden = "";
            if(!campoOrden.isEmpty()) {
                orden = "ORDER BY nfi."+ campoOrden;
            }
            
            String ag = ("vacio".equalsIgnoreCase(agencia)) ? "" : " AND c.agencia = '" + agencia + "'";
       
            query = "QUERY_GET_FICHAS";
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replace("#orden#", orden).replace("#agencia#", ag);
            query = !campoOrden.isEmpty() ? query.replace("#filtro2#",filtro).replace("#filtro#", "") : query.replace("#filtro#",filtro).replace("#filtro2#","");
            ps = con.prepareStatement(query);
            ps.setString(1,user.getLogin());
            rs = ps.executeQuery();
            Gson gson = new Gson();
            JsonObject obj = new JsonObject();
            JsonObject ficha = new JsonObject();
            JsonObject elemento = new JsonObject();
            while (rs.next()) {
                ficha = (JsonObject) obj.get(rs.getString("cod_neg"));
                if (ficha == null) {
                    ficha = new JsonObject();
                    ficha.addProperty("cod_neg", rs.getString("cod_neg"));
                    ficha.addProperty("vencimiento_mayor", rs.getString("vencimiento_mayor"));
                    ficha.addProperty("agente", agente);
                    //ficha.add("deudor", null);
                    //ficha.add("codeudor", null);
                    //ficha.add("negocio", new JsonObject());
                    elemento = new JsonObject();
                    elemento.add("familiar",new JsonArray());
                    elemento.add("personal",new JsonArray());
                    ficha.add("referencias", elemento);
                    obj.add(rs.getString("cod_neg"), ficha);
                }
                elemento = (JsonObject)ficha.get(rs.getString("tipo_principal"));
                if (elemento == null) {
                    elemento = new JsonObject();
                    elemento.addProperty("cedula", rs.getString("identificacion"));
                    elemento.addProperty("nombre", rs.getString("nombre"));
                    elemento.addProperty("direccion", rs.getString("direccion"));
                    elemento.addProperty("departamento", rs.getString("departamento"));
                    elemento.addProperty("ciudad", rs.getString("ciudad"));
                    elemento.addProperty("barrio", rs.getString("barrio"));
                    elemento.addProperty("celular", rs.getString("celular"));
                    elemento.addProperty("telefono", rs.getString("telefono"));
                    elemento.addProperty("telefono2", rs.getString("telefono2"));
                    elemento.addProperty("nom_emp", rs.getString("nombre_empresa"));
                    elemento.addProperty("dep_emp", "");
                    elemento.addProperty("ciu_emp", rs.getString("ciudad_empresa"));
                    elemento.addProperty("dir_emp", rs.getString("direccion_empresa"));
                    elemento.addProperty("bar_emp", "");
                    elemento.addProperty("tel_emp", rs.getString("telefono_empresa"));
                    elemento.addProperty("idCony", rs.getString("id_cony"));
                    elemento.addProperty("nomCony", rs.getString("nom_cony"));
                    elemento.addProperty("direccionCony", rs.getString("direccion_cony"));
                    elemento.addProperty("telefonoCony", rs.getString("telefono_cony"));
                    elemento.addProperty("celularCony", rs.getString("celular_cony"));
                    ficha.add(rs.getString("tipo_principal"), elemento);
                }
                elemento = (JsonObject)ficha.get("cartera");
                if(elemento == null) {
                    elemento = new JsonObject();
                    elemento.addProperty("cuotas_vencidas", rs.getString("cuotas_vencidas"));
                    elemento.addProperty("dia_pago", rs.getString("dia_pago"));
                    elemento.addProperty("saldo_vencido", rs.getString("saldo_vencido"));
                    elemento.addProperty("valor_cuota", rs.getString("valor_cuota"));
                    elemento.addProperty("total", rs.getString("total"));
                    psI = con.prepareStatement(this.obtenerSQL("ultimo_pago"));
                    psI.setString(1, rs.getString("cod_neg"));
                    rsI = psI.executeQuery();
                    while(rsI.next()) {
                        elemento.addProperty("fecha_ult_pago", rsI.getString("fecha_ult_pago"));
                        elemento.addProperty("vlr_ult_pago", rsI.getString("vlr_ult_pago"));
                    }
                    if (psI != null) { psI.close(); }
                    if (rsI != null) { rsI.close(); }
                    ficha.add("cartera", elemento);
                }
                elemento = (JsonObject)ficha.get("negocio");
                if (elemento == null) {
                    elemento = new JsonObject();
                    elemento.addProperty("direccion", rs.getString("direccion_negocio"));
                    elemento.addProperty("departamento", rs.getString("departamento_negocio"));
                    elemento.addProperty("ciudad", rs.getString("ciudad_negocio"));
                    elemento.addProperty("barrio", rs.getString("barrio_negocio"));
                    ficha.add("negocio", elemento);
                }
                if(!rs.getString("tipo_referencia").equals("")) {
                elemento = new JsonObject();
                elemento.addProperty("parentesco", rs.getString("parentesco"));
                elemento.addProperty("nombre", rs.getString("nombre_referencia"));
                elemento.addProperty("celular", rs.getString("celular_referencia"));
                elemento.addProperty("telefono", rs.getString("telefono1_referencia"));
                ficha.getAsJsonObject("referencias").getAsJsonArray(rs.getString("tipo_referencia")).add(elemento);
                }
                obj.add(rs.getString("cod_neg"), ficha);
            }
            json = gson.toJson(obj);
        } catch (SQLException ex) {
            json = "{\"mensaje\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) {}
            return json;
        }
    }
    @Override
    public String resumen(String tipo, String periodo, String unegocio) {
        try {
            query = "resumen";
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replace("#AGENTE#",tipo);
            
            ps = con.prepareStatement(query);
            ps.setString(1, periodo);
            ps.setString(2, unegocio);
            ps.setString(3, unegocio);
            rs = ps.executeQuery();
            Gson gson = new Gson();
            JsonObject obj = new JsonObject();
            JsonObject elemento = new JsonObject();
            JsonObject venc = new JsonObject();
            int cantidad;
            while (rs.next()) {
                elemento = (JsonObject) obj.get(rs.getString("descripcion"));
                if (elemento == null) {
                    elemento = new JsonObject();
                    obj.add(rs.getString("descripcion"), elemento);
                }
                venc = (JsonObject) elemento.get(rs.getString("vencimiento"));
                if (venc == null) {
                    venc = new JsonObject();
                    elemento.add(rs.getString("vencimiento"), venc);
                }
                if(venc.get(rs.getString("asig")) == null) {
                    cantidad = rs.getInt("asignados");
                } else {
                    cantidad = (venc.get(rs.getString("asig")).getAsInt() + rs.getInt("asignados"));
                }
                venc.addProperty(rs.getString("asig"), cantidad);
            }
            json = gson.toJson(obj);
        } catch (SQLException ex) {
            json = "{\"mensaje\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) {}
            return json;
        }
    }

    @Override
    public String previsualizarFichas(String periodo, String agente, String vencimiento, String unegocio, String juridica, String ciclo,Usuario user, String agencia) {
        
        String validar="";
         try {
        PreparedStatement psI = null;
        ResultSet rsI = null;
        JsonArray lista = null;
        JsonObject jsonObject = null;
        
            if (unegocio.equalsIgnoreCase("")) {
                return "{}";
            }
            
            if(!vencimiento.equalsIgnoreCase("")) {
                switch (Integer.parseInt(vencimiento)) {            
                        
                    case 0: vencimiento = "01- CORRIENTE"; break;
                    case 1: vencimiento = "02- 1 A 30"; break;
                    case 2: vencimiento = "03- ENTRE 31 Y 60"; break;
                    case 3: vencimiento = "04- ENTRE 61 Y 90"; break;
                    case 4: vencimiento = "05- ENTRE 91 Y 120"; break;
                    case 5: vencimiento = "06- ENTRE 121 Y 180"; break;
                    case 6: vencimiento = "07- ENTRE 180 Y 210"; break;
                    case 7: vencimiento = "08- ENTRE 210 Y 240"; break;
                    case 8: vencimiento = "09- ENTRE 240 Y 270"; break;
                    case 9: vencimiento = "10- ENTRE 270 Y 300"; break;
                   case 10: vencimiento = "11- ENTRE 300 Y 330"; break;
                   case 11: vencimiento = "12- ENTRE 330 Y 365"; break;
                   case 12: vencimiento = "13- MAYOR A 1 ANIO"; break;    
                    default: break;
                }
             
            } else {
                vencimiento = "true";
            }
            
             String ag = ("vacio".equalsIgnoreCase(agencia)) ? "" : "WHERE cod_neg IN (SELECT cod_neg\n"
                     + " FROM negocios n\n"
                     + " INNER JOIN convenios c ON n.id_convenio = c.id_convenio\n"
                     + " WHERE agencia = '" + agencia + "')";
                                  
             query = "getFichas";
             con = this.conectarJNDI(query);
             query = this.obtenerSQL(query).replace("--agencia--", ag);

             ps = con.prepareStatement(query);
             ps.setString(1, user.getLogin());
             ps.setString(2, vencimiento);
             ps.setString(3, periodo);
             ps.setString(4, unegocio);
             ps.setString(5, agente);
             ps.setString(6, ciclo);
             ps.setString(7, juridica);
             rs = ps.executeQuery();
             lista = new JsonArray();
              while (rs.next()) {
                   jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
              }
              ;
            json=new Gson().toJson(lista);
        } catch (SQLException ex) {
            json = "{\"error\":\"Error al ejecutar la consulta\"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) {}
            return json;
        }
            
        //return validar;
    }
}