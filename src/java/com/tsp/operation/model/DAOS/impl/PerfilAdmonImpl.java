/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.PerfilAdmonDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author egonzalez
 */
public class PerfilAdmonImpl extends MainDAO implements PerfilAdmonDAO {

    public PerfilAdmonImpl() {
        super("PerfilAdmonDAO.xml", "fintra");
    }

    @Override
    public JsonObject buscarOpciones(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query;
        JsonObject obj = new JsonObject();
        JsonArray arr;
        JsonObject aro;
        try {
            con = this.conectarJNDIFintra();
            query = "GET_EMPRESAS";
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            arr = new JsonArray();
            while (rs.next()) {
                aro = new JsonObject();
                aro.addProperty("estado", rs.getString("reg_status"));
                aro.addProperty("dstrct", rs.getString("dstrct"));
                aro.addProperty("nombre", rs.getString("description"));
                arr.add(aro);
            }
            obj.add("empresas", arr);
            
            query = "GET_OPCIONES";
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            arr = new JsonArray();
            while(rs.next()) {
                aro = new JsonObject();
                aro.addProperty("tipo_opcion", rs.getBoolean("opcion"));
                aro.addProperty("padre", rs.getString("id_padre"));
                aro.addProperty("codigo", rs.getString("id_opcion"));
                aro.addProperty("nombre", rs.getString("nombre"));
                aro.addProperty("nivel", rs.getString("nivel"));
                arr.add(aro);
            }
            obj.add("opciones", arr);

            if(!id.equalsIgnoreCase("")) {
                query = "GET_PERFIL";
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, id);
                rs = ps.executeQuery();
                aro = new JsonObject();
                if (rs.next()) {
                    aro.addProperty("estado", rs.getString("rec_status"));
                    aro.addProperty("id_perfil", rs.getString("id_perfil"));
                    aro.addProperty("nombre", rs.getString("nombre"));
                }
                obj.add("perfil", aro);
                
                query = "GET_ASIGNACIONES";
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, id);
                rs = ps.executeQuery();
                aro = new JsonObject();
                while(rs.next()) {
                    aro.addProperty(rs.getString("dstrct")+"_"+rs.getString("id_opcion"), true);
                }
                obj.add("asignaciones", aro);
            }            
        } catch (Exception exc) {
            obj = new JsonObject();
            obj.addProperty("mensaje", exc.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (Exception ex) {
            }
            return obj;
        }
    }

    @Override
    public JsonObject modificar(JsonObject info) {
        JsonObject res = new JsonObject();
        String query, codigo;
        JsonArray arr = new JsonArray(), aro = new JsonArray();
        StringStatement ps = null;
        TransaccionService tService = null;
        
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
        /***************** Crear Perfil *********************/
            res = info.getAsJsonObject("perfil");
            if(res.get("estado").getAsString().equalsIgnoreCase("N")) {
                query = this.obtenerSQL("INSERT_PERFIL");
                ps = new StringStatement(query, true);
                ps.setString(1, res.get("codigo").getAsString());
                ps.setString(2, res.get("nombre").getAsString());
                ps.setString(3, info.get("usuario").getAsString());
                tService.getSt().addBatch(ps.getSql());
            } else {
                query = this.obtenerSQL("UPDATE_PERFIL");
                ps = new StringStatement(query, true);
                ps.setString(1, res.get("estado").getAsString());
                ps.setString(2, res.get("nombre").getAsString());
                ps.setString(3, info.get("usuario").getAsString());
                ps.setString(4, res.get("codigo").getAsString());
                tService.getSt().addBatch(ps.getSql());
                
                /************ Anular opciones *****/
                query = this.obtenerSQL("DELETE_OPCIONES");
                ps = new StringStatement(query, true);
                ps.setString(1, res.get("codigo").getAsString());
                tService.getSt().addBatch(ps.getSql());
            } codigo = res.get("codigo").getAsString();
        /*************** Asignar opciones *******************/
            arr = info.getAsJsonArray("rows");
            for (int i = 0; i < arr.size(); i++) {
                res = (JsonObject) arr.get(i);
                aro = res.getAsJsonArray("opciones");
                for (int j = 0; j < aro.size(); j++) {
                    query = this.obtenerSQL("INSERT_OPCIONES");
                    ps = new StringStatement(query, true);
                    ps.setString(1, codigo);
                    ps.setInt(2, aro.get(j).getAsInt());
                    ps.setString(3, res.get("distrito").getAsString());
                    ps.setString(4, info.get("usuario").getAsString());
                    tService.getSt().addBatch(ps.getSql());
                }
            }
            tService.execute();
            
            res = new JsonObject();
            res.addProperty("cmd", "cgaperfil");
            res.addProperty("idp", codigo);
            if(info.getAsJsonObject("perfil").get("estado").getAsString().equalsIgnoreCase("N")) {
                res.addProperty("mensaje", "Perfil Creado");
            } else {
                res.addProperty("mensaje", "Perfil Actualizado");
            }
        } catch ( Exception exc ) {
            res = new JsonObject();
            res.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (tService != null) { tService.closeAll(); }
            } catch (Exception ex) { }
            return res;
        }
    }

}
