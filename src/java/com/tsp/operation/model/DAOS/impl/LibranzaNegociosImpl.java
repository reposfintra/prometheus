/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.LibranzaNegociosDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Harold Cuello G.
 */
public class LibranzaNegociosImpl extends MainDAO implements LibranzaNegociosDAO {

    public LibranzaNegociosImpl(String dataBaseName) {
        super("LibranzaNegociosDAO.xml", dataBaseName);
    }

    @Override
    public JsonObject buscarLiquidacion(String codigo_negocio) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_LIQUIDACION";
        JsonObject res;
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codigo_negocio);

            rs = ps.executeQuery();
            while (rs.next()) {
                res = new JsonObject();
                res.addProperty("fecha", rs.getString("fecha"));
                res.addProperty("dias", rs.getString("dias"));
                res.addProperty("item", rs.getString("item"));
                res.addProperty("valor", rs.getDouble("valor"));
                res.addProperty("capital", rs.getDouble("capital"));
                res.addProperty("interes", rs.getDouble("interes"));
                res.addProperty("saldo_final", rs.getDouble("saldo_final"));
                res.addProperty("saldo_inicial", rs.getDouble("saldo_inicial"));
                res.addProperty("seguro", rs.getDouble("seguro"));
                res.addProperty("cuota_manejo", rs.getDouble("cuota_manejo"));
                res.addProperty("aval", rs.getDouble("aval"));
                arr.add(res);
            }
            res = new JsonObject();
            res.add("rows", arr);
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: generarLiquidacion() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }

    @Override
    public JsonObject generarLiquidacion(JsonObject info) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "TOTALIZAR_LIQUIDACION";
        JsonObject res;
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            //principal
            ps.setDouble(1, info.get("valor_desembolso").getAsDouble());
            ps.setInt(2, info.get("num_cuotas").getAsInt());
            ps.setString(3, info.get("convenio").getAsString());
            ps.setString(4, info.get("tipo_cuota").getAsString());
            ps.setString(5, info.get("fecha_calculo").getAsString());
            ps.setString(6, info.get("fecha_primera_cuota").getAsString());
            ps.setString(7, "SIMULAR");
            ps.setInt(8, info.get("num_solicitud").getAsInt());
            //aval
            /*
            ps.setDouble(7, info.get("valor_desembolso").getAsDouble());
            ps.setInt(8, info.get("num_cuotas").getAsInt());
            ps.setString(9, info.get("convenio").getAsString());
            ps.setString(10, info.get("tipo_cuota").getAsString());
            ps.setString(11, info.get("fecha_calculo").getAsString());
            ps.setString(12, info.get("fecha_primera_cuota").getAsString());
                    */
            rs = ps.executeQuery();
            while (rs.next()) {
                res = new JsonObject();
                res.addProperty("fecha", rs.getString("fecha"));
                res.addProperty("dias", rs.getString("dias"));
                res.addProperty("item", rs.getString("item"));
                res.addProperty("valor", rs.getDouble("valor"));
                res.addProperty("capital", rs.getDouble("capital"));
                res.addProperty("interes", rs.getDouble("interes"));
                res.addProperty("saldo_final", rs.getDouble("saldo_final"));
                res.addProperty("saldo_inicial", rs.getDouble("saldo_inicial"));
                res.addProperty("seguro", rs.getDouble("seguro"));
                res.addProperty("cuota_manejo", rs.getDouble("cuota_manejo"));
                res.addProperty("aval", rs.getDouble("aval"));
                arr.add(res);
            }
            res = new JsonObject();
            res.add("rows", arr);
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: generarLiquidacion() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }

    @Override
    public JsonObject guardarNegocio(JsonObject info) {
        Connection con = null;
        PreparedStatement psNeg = null, ps = null;
        ResultSet rs = null;
        JsonObject res = new JsonObject();
        try {
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            // creacion de negocio
            psNeg = con.prepareStatement(this.obtenerSQL("GENERAR_NEGOCIO"));
            psNeg.setString(1, info.get("usuario").getAsString());
            psNeg.setInt(2, info.get("numero_solicitud").getAsInt());
            rs = psNeg.executeQuery();
            
            if(rs.next()) {
                // Guardar liquidacion
                ps = con.prepareStatement(this.obtenerSQL("GUARDAR_LIQUIDACION"));
                ps.setString(1, rs.getString("cod_neg"));
                ps.setDouble(2, rs.getDouble("vr_desembolso"));
                ps.setInt(3, rs.getInt("nro_docs"));
                ps.setString(4, rs.getString("id_convenio"));
                ps.setString(5, rs.getString("tipo_cuota"));
                ps.setString(6, rs.getString("fecha_negocio"));
                ps.setString(7, info.get("fecha_primera_cuota").getAsString());
                ps.setString(8, "LIQUIDAR");
                ps.setInt(9,  info.get("numero_solicitud").getAsInt());
                ps.executeUpdate();
                
                // Inserta trazabilidad
                ps = con.prepareStatement(this.obtenerSQL("INSERTAR_TRAZABILIDAD"));
                ps.setInt(1, info.get("numero_solicitud").getAsInt());
                ps.setString(2, "SOL");
                ps.setString(3, info.get("usuario").getAsString());
                ps.setString(4, rs.getString("cod_neg"));
                ps.setString(5, "Aceptada la liquidacion");
                ps.setString(6, "");
                ps.setString(7, "");
                ps.executeUpdate();
                
                // Actualizar Solicitud
                ps = con.prepareStatement(this.obtenerSQL("ACTUALIZAR_SOLICITUD"));
                ps.setString(1, info.get("usuario").getAsString());
                ps.setString(2, rs.getString("cod_neg"));
                ps.setString(3, "P");
                ps.setInt(4, info.get("numero_solicitud").getAsInt());
                ps.executeUpdate();
                
                // Inserta trazabilidad
                ps = con.prepareStatement(this.obtenerSQL("INSERTAR_TRAZABILIDAD"));
                ps.setInt(1, info.get("numero_solicitud").getAsInt());
                ps.setString(2, "LIQ");
                ps.setString(3, info.get("usuario").getAsString());
                ps.setString(4, rs.getString("cod_neg"));
                ps.setString(5, "Continua el proceso");
                ps.setString(6, "");
                ps.setString(7, "");
                ps.executeUpdate();

                // Actualizar Negocio
                ps = con.prepareStatement(this.obtenerSQL("ACTUALIZAR_NEGOCIO"));
                ps.setString(1, info.get("usuario").getAsString());
                ps.setDouble(2, info.get("valor_fianza").getAsDouble());
                ps.setString(3, rs.getString("cod_neg"));
                ps.setString(4, rs.getString("cod_neg"));
                ps.executeUpdate();
                
                con.commit();
                res.addProperty("mensaje", "Negocio creado, codigo : "+rs.getString("cod_neg"));
                res.addProperty("codigo_negocio",rs.getString("cod_neg"));
            } else {
                throw new Exception("Informacion de la solicitud no fue encontrada para generar negocio.");
            }      
        } catch(Exception e) {
            con.rollback();
            res = new JsonObject();
            res.addProperty("error", e.getMessage());
        } finally {
            try {     psNeg.close();        } catch (Exception ex) {}
            try {     ps.close();        } catch (Exception ex) {}
            if(con != null) {
                try {   
                    con.setAutoCommit(true);
                    this.desconectar(con);
                } catch (Exception ex) {
                    Logger.getLogger(LibranzaNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return res;
        }
        
    }

    @Override
    public JsonObject buscarFechaPago(String fecha_negocio, String dias_plazo) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_FECHAS_PAGO";
        JsonObject res;
        try {
            res = new JsonObject();
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, fecha_negocio);
            ps.setString(2, dias_plazo+" day");

            rs = ps.executeQuery();
            while (rs.next()) {
                res.addProperty(rs.getString("fecha"), rs.getString("fecha"));
            }
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }
    
    @Override
    public String calcularFechaPago(String fecha_negocio, String Pagaduria) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "CALCULAR_FECHAS_PAGO";
        JsonObject res;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            res = new JsonObject();
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, fecha_negocio);
            ps.setString(2, Pagaduria);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
               objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
             return informacion;
           // return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }    

    @Override
    public JsonObject validarObligaciones(String tipo, String ocupaciones, Double vlr_sol, Double oblig, int Cantoblig, String cuotas, String fianza) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "VALIDAR_OBLIGACIONES";
        JsonObject res;
        try {
            res = new JsonObject();
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipo);
            ps.setString(2, ocupaciones);
            ps.setDouble(3, vlr_sol);
            ps.setDouble(4, oblig);
            ps.setInt(5, Cantoblig);
            ps.setInt(6, Integer.parseInt(cuotas));
            ps.setString(7, fianza);
            rs = ps.executeQuery();
            if (rs.next()) {
                if(!rs.getString("mensaje").equalsIgnoreCase("OK")){
                    res.addProperty("mensaje", "El valor de las obligaciones supera el calculo convenidos, no debe superar: $ "+rs.getString("mensaje"));
                } else {
                    res.addProperty("valido", true);
                }
            }
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }
    
    @Override
    public double obtenerValorFianza(String id_solicitud, String id_convenio, int plazo, double vlr_negocio, int id_producto, int id_cobertura,String nit_empresa) throws Exception {

        double valor = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "OBTENER_VALOR_FIANZA";
        try {
            con = conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(id_solicitud));
            ps.setInt(2, Integer.parseInt(id_convenio));
            ps.setInt(3, plazo);         
            ps.setDouble(4, vlr_negocio); 
            ps.setString(5, nit_empresa); 
            ps.setInt(6, id_producto); 
            ps.setInt(7, id_cobertura); 
            rs = ps.executeQuery();
            if (rs.next()) {

                valor = Double.parseDouble(rs.getString("valor"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en obtenerValorFianza " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return valor;

    } 

}
