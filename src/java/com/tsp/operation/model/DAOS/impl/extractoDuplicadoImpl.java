/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.ReestructurarNegociosDAO;
import com.tsp.operation.model.DAOS.extractoDuplicadoDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.extractoDuplicadoBeans;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mariana
 */
public class extractoDuplicadoImpl extends MainDAO implements extractoDuplicadoDAO {

    public extractoDuplicadoImpl(String dataBaseName) {
        super("extractoDuplicadoDAO.xml", dataBaseName);
    }

    @Override
    public ArrayList<extractoDuplicadoBeans> buscarNegociosCliente(String cedula, String query, Usuario user) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
       // String query = "SQL_BUSCAR_NEGOCIOS_CLIENTE";
        
        ArrayList<extractoDuplicadoBeans> info = new ArrayList<>();
        try {
            con = this.conectarJNDI(this.obtenerSQL(query),user.getLogin());
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cedula);
            rs = ps.executeQuery();
            while (rs.next()) {
                extractoDuplicadoBeans beansext = new extractoDuplicadoBeans();
                beansext.setCedula(rs.getString("cod_cli"));
                beansext.setNombre(rs.getString("cliente"));
                beansext.setNegocio(rs.getString("negasoc"));
                beansext.setValor_saldo(rs.getString("saldo_cartera"));
                beansext.setDireccion(rs.getString("direccion"));
                beansext.setCiudad(rs.getString("ciudad"));
                beansext.setDepartamento(rs.getString("department_name"));
                beansext.setLinea_negocio(rs.getString("linea_negocio"));
                beansext.setUnidad_negocio(rs.getString("unidad_negocio"));

                info.add(beansext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return info;
    }

    @Override
    public ArrayList<extractoDuplicadoBeans> detalleNegocios(String negocio,Usuario user) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DETALLE_NEGOCIO_FENALCO";
        String query2 = "SQL_DETALLE_NEGOCIO_MICRO";

        ArrayList<extractoDuplicadoBeans> info = new ArrayList<>();
        try {
            con = this.conectarJNDI(this.obtenerSQL(query),user.getBd());
            String uniNegocio = buscarConvenioNegocio(negocio,user);
            if (!uniNegocio.equals("1")) {
                ps = con.prepareStatement(this.obtenerSQL(query));
            } else {
                ps = con.prepareStatement(this.obtenerSQL(query2));
            }
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                extractoDuplicadoBeans beansext = new extractoDuplicadoBeans();
                beansext.setDocumento(rs.getString("documento"));
                beansext.setNegocio(rs.getString("negocio"));
                beansext.setCuota(rs.getString("cuota"));
                beansext.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                beansext.setDias_mora(rs.getString("dias_mora"));
                beansext.setEstado_obligacion(rs.getString("estado"));
                beansext.setTipo_negocio(rs.getString("tipo_negocio"));
//                beansext.setValor_factura(rs.getString("valor_factura"));
//                beansext.setValor_abono(rs.getString("valor_abono"));
                beansext.setValor_saldo(rs.getString("valor_saldo"));
                beansext.setValor_saldo_capital(rs.getString("valor_saldo_capital"));
                beansext.setValor_saldo_interes(rs.getString("valor_saldo_interes"));
                beansext.setIxM(rs.getString("ixm"));
                beansext.setGaC(rs.getString("gac"));
                beansext.setSuma_saldos(rs.getString("suma_saldos"));

                info.add(beansext);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return info;
    }

    private String buscarConvenioNegocio(String negocio, Usuario user) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_UNIDAD_NEGOCIOS";
        String respuesta = "";
        try {
            con = this.conectarJNDI(this.obtenerSQL(query),user.getBd());
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#where", negocio));
           // ps.setString(1, negocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("id");
            }

        } catch (Exception ex) {
            respuesta = ex.getMessage();
            ex.getMessage();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }

        }
        return respuesta;
    }

   
    @Override
    public String guardardocumentosExt(JsonArray lista, extractoDuplicadoBeans duplicadoBeans, Usuario user) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String codigo = "";
        TransaccionService tService = null;
        try {
            codigo = serieDuplicado();
            if (!codigo.equals("")) {
                tService = new TransaccionService(this.getDatabaseName());
                tService.crearStatement();
                for (int i = 0; i < lista.size(); i++) {
                    objeto = lista.get(i).getAsJsonObject();

                    st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_FACTURAS"), true);
                    st.setString(1, codigo);
                    st.setString(2, objeto.get("documento").getAsString());
                    st.setString(3, objeto.get("negocio").getAsString());
                    st.setString(4, objeto.get("cuota").getAsString());
                    st.setString(5, objeto.get("fecha_vencimiento").getAsString());
                    st.setString(6, objeto.get("dias_mora").getAsString());
                    st.setString(7, objeto.get("estado_obligacion").getAsString());
//                    st.setDouble(8, objeto.get("valor_factura").getAsDouble());
//                    st.setDouble(9, objeto.get("valor_abono").getAsDouble());
                    st.setDouble(8, objeto.get("valor_saldo").getAsDouble());
                    st.setDouble(9, objeto.get("valor_saldo_capital").getAsDouble());
                    st.setDouble(10, objeto.get("valor_saldo_interes").getAsDouble());
                    st.setDouble(11, objeto.get("IxM").getAsDouble());
                    st.setDouble(12, objeto.get("GaC").getAsDouble());
                    st.setDouble(13, duplicadoBeans.getPorct_capital());//porcentaje descuento capital
                    st.setDouble(14, duplicadoBeans.getPorct_interes());//porcentaje descuento interes
                    st.setDouble(15, duplicadoBeans.getPorct_ixmora());
                    st.setDouble(16, duplicadoBeans.getPorct_gacobranza());
                    st.setDouble(17, objeto.get("suma_saldos").getAsDouble());
                    st.setDouble(18, getValorTotalDescuento(objeto, duplicadoBeans));//total con descentos x cuota
                    st.setString(19, user.getLogin());

                    tService.getSt().addBatch(st.getSql());
                }
                
                //guardamos el consolidado..
                st = new StringStatement(this.obtenerSQL("SQL_CONSOLIDADO_ROP"), true);
                st.setString(1, codigo);
                st.setString(2, duplicadoBeans.getEstado_obligacion());
                st.setDouble(3, duplicadoBeans.getPorct_capital());
                st.setDouble(4, duplicadoBeans.getPorct_interes());
                st.setDouble(5, duplicadoBeans.getPorct_ixmora());
                st.setDouble(6, duplicadoBeans.getPorct_gacobranza());
                st.setDouble(7, Double.parseDouble(duplicadoBeans.getValor_saldo()));
                st.setDouble(8, Double.parseDouble(duplicadoBeans.getValor_saldo_capital()));
                st.setDouble(9, Double.parseDouble(duplicadoBeans.getValor_saldo_interes()));
                st.setDouble(10, Double.parseDouble(duplicadoBeans.getInt_mora()));
                st.setDouble(11, Double.parseDouble(duplicadoBeans.getGasto_cobranza()));
                st.setDouble(12, duplicadoBeans.getTotalExtracto());
                st.setString(13, user.getLogin());
                
                tService.getSt().addBatch(st.getSql());
                tService.execute();
                
                int idrop = crearRop(codigo, user,duplicadoBeans.getUnidad_negocio());
                ReestructurarNegociosDAO rndao = new ReestructurarNegociosImpl(user.getBd());
                rndao.generarPdfDuplicado(user.getLogin(), idrop);
                
                
                respuesta = "{\"respuesta\":\"OK\"}";
            } else {
                respuesta = "{\"respuesta\":\"ERROR\"}";

            }

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
            }
            return respuesta;
        }
    }

    private double getValorTotalDescuento(JsonObject objeto,extractoDuplicadoBeans duplicadoBeans){
        
        double descuentoCapital = Math.round(objeto.get("valor_saldo_capital").getAsDouble() * (duplicadoBeans.getPorct_capital() / 100));
        double descuentoInteres = Math.round(objeto.get("valor_saldo_interes").getAsDouble() * (duplicadoBeans.getPorct_interes() / 100));
        double descuentoIntxmora = Math.round(objeto.get("IxM").getAsDouble() * (duplicadoBeans.getPorct_ixmora() / 100));
        double descuentoGac = Math.round(objeto.get("GaC").getAsDouble() * (duplicadoBeans.getPorct_gacobranza() / 100));

        double totalDescuento = descuentoCapital + descuentoInteres + descuentoIntxmora + descuentoGac;

        double total = objeto.get("suma_saldos").getAsDouble() - totalDescuento;
        
        return total;
    
    }

    @Override
    public String getLineaNegocio(Usuario user) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_LINEA_NEGOCIO";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query,user.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                lista = new JsonArray();

                jsonObject = new JsonObject();
                jsonObject.addProperty("codigo", "");
                jsonObject.addProperty("nombre", "Seleccione");
                lista.add(jsonObject);

                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("codigo", rs.getString("ref_4"));
                    jsonObject.addProperty("nombre", rs.getString("ref_4"));
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            lista = null;

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }
    
    private String serieDuplicado() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_SERIE_FACTURAS";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("retorno");
            }

        } catch (Exception ex) {
            respuesta = ex.getMessage();
            ex.getMessage();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }

        }
        return respuesta;
    }
    
    
    private int crearRop(String numeroDuplicado, Usuario user, String unidadNegocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CREAR_ROP";
        int respuesta = 0;
        try {
            con = this.conectarJNDI(this.obtenerSQL(query), user.getBd());
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, user.getLogin());
            ps.setString(2, numeroDuplicado);
            ps.setString(3, unidadNegocio);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getInt("retorno");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta;
        }

    }


}
