/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.tsp.operation.model.DAOS.ConfigProductosEDSDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.ConfigProductosEDSBeans;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author egonzalez
 */
public class ConfigProductosEDSImpl extends MainDAO implements ConfigProductosEDSDAO {

    public ConfigProductosEDSImpl(String dataBaseName) {
        super("ConfigProductosEDSDAO.xml", dataBaseName);
    }

    @Override
    public JsonObject consultar_productos(JsonObject info) {
        JsonObject respuesta = new JsonObject(), fila;
        JsonArray arr = new JsonArray();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "PRODUCTOSxEDS";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, info.get("id").getAsInt());
            rs = ps.executeQuery();

            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("estado", rs.getBoolean("estado"));
                fila.addProperty("id_producto", rs.getString("id_producto"));
                fila.addProperty("producto", rs.getString("producto"));
                fila.addProperty("precio_producto", rs.getString("precio_producto"));
                fila.addProperty("comision_aplica", rs.getBoolean("comision_aplica"));
                fila.addProperty("comision_descripcion", rs.getString("comision_descripcion"));
                fila.addProperty("comision_porcentaje", rs.getString("comision_porcentaje"));
                fila.addProperty("comision_valor", rs.getString("comision_valor"));
                fila.addProperty("editar_precio", rs.getBoolean("editar_precio"));
                fila.addProperty("precio_ensession", rs.getBoolean("precio_ensession"));
                fila.addProperty("periodo", rs.getString("periodo"));
                fila.addProperty("control", rs.getString("control"));
                fila.addProperty("inmodificable", rs.getString("inmodificable"));
                arr.add(fila);
            }
            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject modificar_productos(JsonObject info) {
        JsonObject respuesta = new JsonObject();
        StringStatement ss = null;
        TransaccionService tsrv = null;
        String query = "";
        try {
            tsrv = new TransaccionService(this.getDatabaseName());
            tsrv.crearStatement();
            JsonArray arr = info.getAsJsonArray("rows");
            for (int i = 0; i < arr.size(); i++) {
                respuesta = arr.get(i).getAsJsonObject();
                if (respuesta.get("id").getAsString().startsWith("neo_")) {
                    ss = new StringStatement(this.obtenerSQL("INSERTAR_PRODUCTO"), true);
                    ss.setInt(1, info.get("eds").getAsInt());
                    ss.setInt(2, respuesta.get("id_producto").getAsInt());
                    ss.setString(3, respuesta.get("comision_descripcion").getAsString());
                    ss.setString(4, (respuesta.get("comision_aplica").getAsString().equalsIgnoreCase("true")) ? "S" : "N");
                    ss.setDouble(5, respuesta.get("comision_porcentaje").getAsDouble());
                    ss.setDouble(6, respuesta.get("comision_valor").getAsDouble());
                    ss.setDouble(7, respuesta.get("precio_producto").getAsDouble());
                    ss.setString(8, (respuesta.get("editar_precio").getAsString().equalsIgnoreCase("true")) ? "S" : "N");
                    ss.setString(9, (respuesta.get("precio_ensession").getAsString().equalsIgnoreCase("true")) ? "S" : "N");
                    ss.setString(10, (respuesta.get("id_producto").getAsString().equalsIgnoreCase("2")) ? "1" : "0");
                    ss.setString(11, info.get("usuario").getAsString());
                } else {
                    ss = new StringStatement(this.obtenerSQL("ACTUALIZAR_PRODUCTO"), true);
                    ss.setString(1, (respuesta.get("estado").getAsString().equalsIgnoreCase("true")) ? "" : "A");
                    ss.setString(2, respuesta.get("comision_descripcion").getAsString());
                    ss.setString(3, (respuesta.get("comision_aplica").getAsString().equalsIgnoreCase("true")) ? "S" : "N");
                    ss.setDouble(4, respuesta.get("comision_porcentaje").getAsDouble());
                    ss.setDouble(5, respuesta.get("comision_valor").getAsDouble());
                    ss.setDouble(6, respuesta.get("precio_producto").getAsDouble());
                    ss.setString(7, (respuesta.get("editar_precio").getAsString().equalsIgnoreCase("true")) ? "S" : "N");
                    ss.setString(8, (respuesta.get("precio_ensession").getAsString().equalsIgnoreCase("true")) ? "S" : "N");
                    ss.setString(9, info.get("usuario").getAsString());
                    ss.setInt(10, respuesta.get("id").getAsInt());
                }
                tsrv.getSt().addBatch(ss.getSql());
            }
            tsrv.execute();
            respuesta = new JsonObject();
            respuesta.addProperty("mensaje", "Edici�n satisfactoria");
        } catch (Exception exc) {
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");
        } finally {
            try {
                if (tsrv != null) {
                    tsrv.closeAll();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargar_combo(JsonObject info) {
        JsonObject respuesta = new JsonObject();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        try {
            query = info.get("query").getAsJsonPrimitive().getAsString();
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            if (info.get("filtros") != null) {
                JsonArray ar = info.get("filtros").getAsJsonArray();
                for (int i = 0; i < ar.size(); i++) {
                    ps.setString(i + 1, ar.get(i).getAsString());
                }
            }
            rs = ps.executeQuery();
            respuesta.addProperty("0", "...");
            while (rs.next()) {
                respuesta.addProperty(rs.getString("codigo"), rs.getString("nombre"));
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public String guardarRangos(JsonObject info) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = info.getAsJsonArray("json");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                if (objeto.get("id").getAsString().equals("")) {
                    st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_RANGOS"), true);
                    st.setString(1, info.get("cia").getAsString());
                    st.setString(2, info.get("usuario").getAsString());
                    st.setString(3, objeto.get("id_configproductos").getAsString());
                    st.setString(4, objeto.get("rango_inicial").getAsString());
                    st.setString(5, objeto.get("rango_final").getAsString());
                    st.setString(6, objeto.get("valor_descuento").getAsString());
                    st.setString(7, objeto.get("porcen_descuento").getAsString());
                } else {
                    st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_RANGOS"), true);
                    st.setInt(1, objeto.get("rango_inicial").getAsInt());
                    st.setInt(2, objeto.get("rango_final").getAsInt());
                    st.setString(3, objeto.get("valor_descuento").getAsString());
                    st.setString(4, objeto.get("porcen_descuento").getAsString());
                    st.setString(5, info.get("usuario").getAsString());
                    st.setString(6, objeto.get("id").getAsString());
                }

                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado:)\"}";
            guardarHistoricoRangos(info);
            //respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
            }
            return respuesta;
        }
    }

    @Override
    public ArrayList<ConfigProductosEDSBeans> listarRangos(String id_config) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_RANGOS";

        ArrayList<ConfigProductosEDSBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_config);
            rs = ps.executeQuery();
            while (rs.next()) {
                ConfigProductosEDSBeans beanRango = new ConfigProductosEDSBeans();
                beanRango.setId(rs.getString("id"));
                beanRango.setId_config_productos(rs.getString("id_config_productos"));
                beanRango.setGalonaje_inicial(rs.getString("galonaje_inicial"));
                beanRango.setGalonaje_final(rs.getString("galonaje_final"));
                beanRango.setVlr_descuento(rs.getString("valor_descuento"));
                beanRango.setPorct_descuento(rs.getString("porcentaje_descuento"));
                lista.add(beanRango);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return lista;
    }

    @Override
    public String guardarHistoricoRangos(JsonObject info) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = info.getAsJsonArray("json");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_HISTORICO"), true);
                st.setString(1, objeto.get("id_configproductos").getAsString());
                st.setString(2, objeto.get("rango_inicial").getAsString());
                st.setString(3, objeto.get("rango_final").getAsString());
                st.setString(4, objeto.get("valor_descuento").getAsString());
                st.setString(5, objeto.get("porcen_descuento").getAsString());
                st.setString(6, info.get("usuario").getAsString());
                st.setString(7, objeto.get("id_configproductos").getAsString());
                st.setString(8, objeto.get("rango_inicial").getAsString());
                st.setString(9, objeto.get("rango_final").getAsString());
                st.setString(10, objeto.get("valor_descuento").getAsString());
                st.setString(11, objeto.get("porcen_descuento").getAsString());

                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado:)\"}";
            //respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
            }
            return respuesta;
        }
    }

    @Override
    public ArrayList<ConfigProductosEDSBeans> listarHistrico(String id_config) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_HISTORICO";

        ArrayList<ConfigProductosEDSBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_config);
            rs = ps.executeQuery();
            while (rs.next()) {
                ConfigProductosEDSBeans beanRango = new ConfigProductosEDSBeans();
                beanRango.setHistorico_id(rs.getString("id"));
                beanRango.setPeriodo(rs.getString("periodo"));
                beanRango.setId_config_productos(rs.getString("id_config_productos"));
                beanRango.setGalonaje_inicial(rs.getString("galonaje_inicial"));
                beanRango.setGalonaje_final(rs.getString("galonaje_final"));
                beanRango.setVlr_descuento(rs.getString("valor_descuento"));
                beanRango.setPorct_descuento(rs.getString("porcentaje_descuento"));
                beanRango.setUsuario(rs.getString("user_update"));
                beanRango.setFecha(rs.getString("last_update"));
                lista.add(beanRango);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return lista;
    }

}
