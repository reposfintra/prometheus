/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.UnidadesNegocioDAO;
import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.ProcesoMeta;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author desarrollo
 */
public class UnidadesNegocioImpl extends MainDAO implements UnidadesNegocioDAO{
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;
    
    public UnidadesNegocioImpl(){
        super("UnidadesNegocio.xml");
    }

    @Override
    public ArrayList<UnidadesNegocio> cargarUnidadesNegocio() {
        con = null; rs = null; ps = null;
        ArrayList<UnidadesNegocio> lista = new ArrayList<UnidadesNegocio>();
        query = "cargarUnidadesNegocios";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                UnidadesNegocio uneg = new UnidadesNegocio();
                uneg.setId(rs.getInt("id"));
                uneg.setCodigo(rs.getString("cod"));
                uneg.setDescripcion(rs.getString("descripcion"));
                uneg.setCiudad(rs.getString("ciudad"));
                uneg.setCodCentralRiesgo(rs.getString("cod_central_riesgo"));
                
                lista.add(uneg);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarUnidadesNegocio " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public void guardarUnidadNegocio(String nomUnidad, String codUnidad, String codCentral) {
        con = null; ps = null;
        query = "crearUnidadNegocio";
        
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nomUnidad);
            ps.setString(2, codUnidad);
            ps.setString(3, codCentral);
            
            ps.executeUpdate();
            
        }catch (Exception e) {
            try {
                throw new SQLException("ERROR guardarUnidadNegocio \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
       }
    }

    @Override
    public ArrayList<Convenio> cargarConvenios() {
        con = null; rs = null; ps = null;
        ArrayList<Convenio> lista = new ArrayList<Convenio>();
        query = "cargarConvenios";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                Convenio conv = new Convenio();
                conv.setId_convenio(rs.getString("id_convenio"));
                conv.setNombre(rs.getString("nombre"));
                                
                lista.add(conv);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarConvenios " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public int obtenerIdUnidadNegocio(String nomUnidad) {
        con = null; rs = null; ps = null;
        int id = 0;
        query = "obtenerIdUnidadNegocio";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nomUnidad);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                id = rs.getInt(1);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en obtenerIdUnidadNegocio " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return id;
    }

    @Override
    public boolean existeUnidadNegocio(String nomUnidad, String codUnidad) {
        con = null; rs = null; ps = null;
        boolean resp = false;
        query = "existeUnidadNegocio";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nomUnidad);
            ps.setString(2, codUnidad);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeUnidadNegocio " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return resp;
    }

    @Override
    public String insertarRelConvenio(int idUnidad, String convenio) {
        con = null; st = null;
        String sql = "";
        query = "insertarRelConvenio";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);
            st.setInt(1, idUnidad);
            st.setInt(2, Integer.parseInt(convenio));
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {
                throw new Exception("Error en insertarRelConvenio " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
         return sql;
    }

    @Override
    public ArrayList<Convenio> cargarConveniosNegocio(String idUnidad) {
        con = null; rs = null; ps = null;
        ArrayList<Convenio> lista = new ArrayList<Convenio>();
        query = "cargarConveniosNegocio";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(idUnidad));
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                Convenio conv = new Convenio();
                conv.setId_convenio(rs.getString("id_convenio"));
                conv.setNombre(rs.getString("nombre"));
                                
                lista.add(conv);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarConveniosNegocio " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public UnidadesNegocio cargarDatosUnidadNegocio(String idUnidad) {
        con = null; rs = null; ps = null;
        UnidadesNegocio datos = new UnidadesNegocio();
        query = "cargarDatosUnidadNegocio";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(idUnidad));
                       
            rs = ps.executeQuery();
            
            if (rs.next()){
                datos.setId(rs.getInt("id"));
                datos.setDescripcion(rs.getString("descripcion"));
                datos.setCodigo(rs.getString("cod"));
                datos.setCodCentralRiesgo(rs.getString("cod_central_riesgo"));
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarDatosUnidadNegocio " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return datos;
    }

    @Override
    public void anularConvenioUnidadNegocio(String idUnidad, String idConvenio) {
        con = null; ps = null;
        query = "anularConvenioUnidadNegocio";
        
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(idUnidad));
            ps.setInt(2, Integer.parseInt(idConvenio));
                       
            ps.executeUpdate();
            
        }catch (Exception e) {
            try {
                throw new SQLException("ERROR anularConvenioUnidadNegocio \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
       }
    }

    @Override
    public void actualizarUnidadNegocio(String idUnidad, String nomUnidad, String codCentralR, String codigoUnd) {
        con = null; ps = null;
        query = "actualizarUnidadNegocio";
        
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nomUnidad);
            ps.setString(2, codigoUnd);
            ps.setString(3, codCentralR);
            ps.setInt(4, Integer.parseInt(idUnidad));
            
            ps.executeUpdate();
            
        }catch (Exception e) {
            try {
                throw new SQLException("ERROR actualizarUnidadNegocio \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
       }
    }

            }
