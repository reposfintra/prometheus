/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lowagie.text.html.simpleparser.StyleSheet;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.ProcesoEjecutivoDAO;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author mcastillo
 */
public class ProcesoEjecutivoImpl extends MainDAO implements ProcesoEjecutivoDAO{
    
    public ProcesoEjecutivoImpl() {
        super("ProcesoEjecutivoDAO.xml");
    }
    
    public ProcesoEjecutivoImpl(String DBName) {
        super("ProcesoEjecutivoDAO.xml", DBName);
    }

    @Override
    public String cargarEstadosCartera() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
       
        Gson gson = new Gson();
        String query = "cargarEstadosCartera";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);           
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));  
                fila.addProperty("reg_status", rs.getString("reg_status"));  
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarEstadosCartera " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
   
    @Override
    public String guardarEstadoCartera( String nombre, String descripcion, String usuario, String dstrct) {
        Connection con = null;
        PreparedStatement ps = null;      
        String query = "guardarEstadoCartera";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);        
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario);
            ps.setString(4, dstrct);
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarEstadoCartera \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;  
       }
    }

    @Override
    public String actualizarEstadoCartera(String nombre, String descripcion, String idEstado, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "actualizarEstadoCartera";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);       
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario);
            ps.setInt(4, Integer.parseInt(idEstado));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarEstadoCartera \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }

    @Override
    public String activaInactivaEstadoCartera(String id, String estado, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "activarInactivarEstadoCartera";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, Integer.parseInt(id));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activaInactivaEstadoCartera \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }
    
     @Override
    public boolean existeEstadoCartera(String nombre) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeEstadoCartera";
        String filtro="";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);         
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeEstadoCartera " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }
    
    
    @Override
    public boolean existeEstadoCartera(String nombre, int idEstado) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeEstadoCartera";
        String filtro=" and id not in("+idEstado+")";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeEstadoCartera " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }

    
    @Override
    public ArrayList<UnidadesNegocio> cargarUndNegocios(String ref) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_UNIDADES_NEGOCIO";
        String filtro = " AND ref_1 = '" + ref +"'";
       
        ArrayList<UnidadesNegocio> lista = new ArrayList<UnidadesNegocio>();      
        try{
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));            
            rs = ps.executeQuery();
            
            while (rs.next()){
                UnidadesNegocio uneg = new UnidadesNegocio();
                uneg.setId(rs.getInt("id"));
                uneg.setCodigo(rs.getString("cod"));
                uneg.setDescripcion(rs.getString("descripcion"));  
                lista.add(uneg);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarUnidadesNegocio " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return lista;
    }
    
    @Override
    public String asignarDesasignarUndProceso(int idUndNegocio, String ref) throws SQLException {
      
        Connection con = null;
        StringStatement st;           
        String query = "SQL_ASIGNAR_DESASIGNAR_UNIDAD_PROC";        
        String sql = "";

        try {

            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);         
            st.setString(1, ref);        
            st.setInt(2, idUndNegocio);   
          
            sql = st.getSql();  
             
        } catch (SQLException ex) {          
            throw new SQLException("ERROR ASIGNANDO Y/O DESASIGNANDO UNIDAD DE NEGOCIO AL PROCESO EJECUTIVO: asignarDesasignarUndProceso(int idUndNegocio, String ref) " + ex.getMessage());
        } finally {  
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return sql;  
        }
    
    }

    @Override
     public ArrayList<BeanGeneral> cargarEstadosCarteraxUnd(int id_unidad_negocio) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ESTADOS_CARTERA_POR_UND";
       
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();      
        try{
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setInt(1, id_unidad_negocio);
            rs = ps.executeQuery();
            
            while (rs.next()){
                BeanGeneral est_cartera =  new BeanGeneral();
                est_cartera.setValor_01(rs.getString("id_intervalo_mora"));
                est_cartera.setValor_02(rs.getString("rango_mora"));
                est_cartera.setValor_03(rs.getString("estado_cartera"));  
                est_cartera.setValor_04(rs.getString("id_estado_cartera"));  
                lista.add(est_cartera);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarEstadosCarteraxUnd " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return lista;
    }
     
    @Override
    public JsonObject listarEstadosCartera() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = null;
        try {
            obj = new JsonObject();
            String query = "SQL_CARGAR_ESTADOS_CARTERA";
            con = this.conectarJNDI();          
            ps = con.prepareStatement(this.obtenerSQL(query));            
            rs = ps.executeQuery();
           
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"),rs.getString("descripcion"));
            }
            obj=fila;
       
        } catch (Exception ex) {  
              throw new SQLException("ERROR OBTENIENDO ESTADOS DE CARTERA: listarEstadosCartera()  " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return obj;
        }
    }
    
    @Override
    public String insertarConfigIniEstadosCartera(int idUndNegocio, int idEstadoCartera, String usuario) throws SQLException {
      
        Connection con = null;
        StringStatement st;         
        String query = "SQL_INSERTAR_CONFIG_ESTADOS_POR_UNIDAD";        
        String sql = "";

        try {

            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);             
                    
            st.setInt(1, idUndNegocio);
            st.setInt(2, idEstadoCartera);
            st.setString(3, usuario);
    
            sql = st.getSql();  
            
        } catch (SQLException ex) {
           
            throw new SQLException("ERROR INSERTANDO CONFIGURACI�N ESTADOS DE CARTERA: SQL_INSERTAR_CONFIG_ESTADOS_POR_UNIDAD(int idUndNegocio, int idEstadoCartera, String usuario) " + ex.getMessage());
        } finally {  
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return sql;  
        }
    
    }
    
    @Override
    public String eliminarRelUndEstadoCartera(int idUndNegocio) throws SQLException {
      
        Connection con = null;
        StringStatement st;         
        String query = "SQL_ELIMINAR_CONFIG_ESTADOS_POR_UNIDAD";        
        String sql = "";

        try {

            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);             
                    
            st.setInt(1, idUndNegocio);
        
    
            sql = st.getSql();  
            
        } catch (SQLException ex) {
           
            throw new SQLException("ERROR ELIMINANDO CONFIGURACI�N ESTADOS DE CARTERA: SQL_ELIMINAR_CONFIG_ESTADOS_POR_UNIDAD(int idUndNegocio) " + ex.getMessage());
        } finally {  
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return sql;  
        }
    
    }
    
    @Override
    public boolean existenDemandasRelUndNegocio(int idUndNegocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String filtro = "";
        String query = "existenDemandasRelUndNegocio";
        try{
           
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, idUndNegocio);
            ps.setInt(2, idUndNegocio);
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existenDemandasRelUndNegocio " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }
    
    @Override
    public String actualizarConfigEstadosCartera(int idUndNegocio, int idIntervaloMora, int idEstadoCartera, String usuario) throws SQLException {
      
        Connection con = null;
        PreparedStatement ps = null;       
        String query = "SQL_UPDATE_CONFIG_ESTADOS_POR_UNIDAD";        
        String  respuestaJson = "{}";

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));              
            ps.setInt(1, idEstadoCartera);
            ps.setString(2, usuario);
            ps.setInt(3, idUndNegocio);   
            ps.setInt(4, idIntervaloMora);        
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        } catch (SQLException ex) {
            respuestaJson ="{\"error\":\"" + ex.getMessage() + "\"}";
            throw new SQLException("ERROR ACTUALIZANDO CONFIGURACI�N ESTADOS DE CARTERA: actualizarConfigEstadosCartera(int idUndNegocio, int idIntervaloMora, int idEstadoCartera, String usuario) " + ex.getMessage());
        } finally {          
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
            return respuestaJson;  
        }
    
    }

    @Override
    public String cargarEtapas(boolean showAll) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
       
        Gson gson = new Gson();
        String query = "cargarEtapas";
        String filtro = (!showAll) ? " WHERE reg_status = ''": "" ;
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);           
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));  
                fila.addProperty("estimado_dias", rs.getString("dur_estimada_dias")); 
                fila.addProperty("reg_status", rs.getString("reg_status"));  
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarEtapas " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }

    @Override
    public String guardarEtapa(String nombre, String descripcion, int estimadoDias, String usuario, String dstrct) {
        Connection con = null;
        PreparedStatement ps = null;      
        String query = "guardarEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);        
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setInt(3, estimadoDias);
            ps.setString(4, usuario);
            ps.setString(5, dstrct);
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;  
       }
    }

    @Override
    public String actualizarEtapa(String nombre, String descripcion, int estimadoDias, String idEtapa, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "actualizarEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);       
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setInt(3, estimadoDias);
            ps.setString(4, usuario);
            ps.setInt(5, Integer.parseInt(idEtapa));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }

    @Override
    public String activaInactivaEtapa(String id, String estado, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "activarInactivarEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, Integer.parseInt(id));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activarInactivarEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }

    @Override
    public boolean existeEtapa(String nombre, int idEtapa) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeEtapa";
        String filtro = (idEtapa > 0) ? " and id not in("+idEtapa+")" : "";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);         
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeEtapa " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }

    @Override
    public String cargarRespuestasEtapa(int idEtapa) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
       
        Gson gson = new Gson();
        String query = "cargarRespuestasEtapa";
        String filtro = "";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);       
            ps.setInt(1, idEtapa);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("id_etapa", rs.getInt("id_etapa"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));  
                fila.addProperty("estimado_dias", rs.getString("dur_estimada_dias"));  
                fila.addProperty("secuencia", rs.getString("secuencia"));
                fila.addProperty("finaliza_proceso", rs.getString("finaliza_proceso"));
                fila.addProperty("editar_respuesta", rs.getString("editar_respuesta"));
                fila.addProperty("reg_status", rs.getString("reg_status"));  
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarRespuestasEtapa " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);

    }

    @Override
    public String guardarRespuestaEtapa(int idEtapa, String nombre, String descripcion, int estimadoDias, int secuencia, String finaliza_proceso, String usuario, String dstrct) {
        Connection con = null;
        PreparedStatement ps = null;      
        String query = "guardarRespuestasEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, dstrct);
            ps.setInt(2, idEtapa);
            ps.setString(3, nombre);
            ps.setString(4, descripcion);
            ps.setInt(5, estimadoDias);
            ps.setInt(6, secuencia);
            ps.setString(7, finaliza_proceso);
            ps.setString(8, usuario);
          
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarRespuestaEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;  
       }
    }

    @Override
    public String actualizarRespuestaEtapa(int id, int idEtapa, String nombre, String descripcion, int estimadoDias,  int secuencia, String finaliza_proceso, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "actualizarRespuestaEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);       
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setInt(3, estimadoDias);
            ps.setInt(4, secuencia);
            ps.setString(5, finaliza_proceso);
            ps.setString(6, usuario);
            ps.setInt(7, id);       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarRespuestaEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }

    @Override
    public String activaInactivaRespuestaEtapa(String id, String estado, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "activarInactivarRespEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, Integer.parseInt(id));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activarInactivarRespEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }

    @Override
    public String cargarCostosEtapa(int idEtapa) {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
       
        Gson gson = new Gson();
        String query = "cargarCostosEtapa";
        String filtro = " where id_etapa = "+idEtapa;
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);          
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("id_etapa", rs.getInt("id_etapa"));
                fila.addProperty("concepto", rs.getString("concepto"));
                fila.addProperty("tipo", rs.getString("tipo"));  
                fila.addProperty("simbolo", rs.getString("simbolo"));  
                fila.addProperty("valor", rs.getString("valor"));  
                fila.addProperty("solo_automotor", rs.getString("solo_automotor"));  
                fila.addProperty("id_estado_ap", rs.getString("id_estado_ap")); 
                fila.addProperty("estado_ap", rs.getString("estado_ap")); 
                fila.addProperty("reg_status", rs.getString("reg_status"));  
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarCostos " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }

    @Override
    public String guardarCostoEtapa(int idEtapa, String concepto, String tipo, double valor, String solo_automotor, String usuario, String dstrct) {
        Connection con = null;
        PreparedStatement ps = null;      
        String query = "guardarCostoEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, dstrct);
            ps.setInt(2, idEtapa);
            ps.setString(3, concepto);
            ps.setString(4, tipo);
            ps.setDouble(5, valor);
            ps.setString(6, solo_automotor);
            ps.setString(7, usuario);          
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarCostoEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;  
       }
    }

    @Override
    public String actualizarCostoEtapa(int id, int idEtapa, String concepto, String tipo, double valor, String solo_automotor, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "actualizarCostoEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);       
            ps.setString(1, concepto);
            ps.setString(2, tipo);
            ps.setDouble(3, valor);
            ps.setString(4, solo_automotor);
            ps.setString(5, usuario);
            ps.setInt(6, id);       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarCostoEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }

    @Override
    public String activaInactivaCostoEtapa(String id, String estado, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "activarInactivarCostoEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, Integer.parseInt(id));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activarInactivarCostoEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }
    
    @Override
    public String cargarReporteJuridica(String etapa,int undNegocio, String negocio, String idCliente) {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();       
        Gson gson = new Gson();
        String query = "SQL_CARGAR_REPORTE_JURIDICA";

        String filtro = etapa.equals("0")?" WHERE etapa_proc_ejec in ( '" + etapa + "','')":" WHERE etapa_proc_ejec = '" + etapa + "'";
       // String filtConvenio = (undNegocio>0)? " AND neg.id_convenio in(select id_convenio FROM rel_unidadnegocio_convenios WHERE id_unid_negocio = " + undNegocio + ")" : "";
        filtro += (undNegocio>0) ? " AND un.id =" + undNegocio +"": "";
        filtro += (!negocio.equals("")) ? " AND neg.cod_neg ='" + negocio +"'": "";
        filtro += (!idCliente.equals("")) ? " AND neg.cod_cli ='" + idCliente +"'": "";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);          
            ps.setString(1, filtro);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("cedula", rs.getString("cedula"));
                fila.addProperty("nombre", rs.getString("nombre"));               
                fila.addProperty("ciudad", rs.getString("ciudad"));
                fila.addProperty("direccion", rs.getString("direccion"));
                fila.addProperty("barrio", rs.getString("barrio"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("celular", rs.getString("celular"));
                fila.addProperty("email", rs.getString("email"));                
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("mora", rs.getString("mora"));
                fila.addProperty("fecha_inicio", rs.getString("fecha_inicio"));
                fila.addProperty("fecha_marcacion", rs.getString("fecha_marcacion"));
                fila.addProperty("dias_transcurridos", rs.getString("dias_transcurridos"));
                fila.addProperty("und_negocio", rs.getString("und_negocio"));
                fila.addProperty("id_convenio", rs.getString("id_convenio"));
                fila.addProperty("convenio", rs.getString("convenio"));
                fila.addProperty("num_pagare", rs.getString("num_pagare"));
                fila.addProperty("id_demanda", rs.getString("id_demanda"));
                fila.addProperty("niter", rs.getString("niter"));
                fila.addProperty("vr_negocio", rs.getString("vr_negocio"));   
                fila.addProperty("vr_desembolso", rs.getString("vr_desembolso"));
                fila.addProperty("valor_saldo", rs.getDouble("valor_saldo"));   
                fila.addProperty("estado_cartera", rs.getString("estado_cartera"));
                fila.addProperty("id_juzgado", rs.getInt("id_juzgado"));
                fila.addProperty("radicado", rs.getString("radicado"));
                fila.addProperty("docs_generados", rs.getString("docs_generados"));
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarReporteCarteraVencida " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
    @Override
    public String actualizarEtapaNegocio(String negocio, String idEtapa, String actualiza_fecha, String usuario) {
        Connection con = null;
        StringStatement st;       
        String sql = "";
        String query = "SQL_ACTUALIZAR_ETAPA_NEGOCIO";
        String filtro = (actualiza_fecha.equals("S")) ? "fecha_inicio_etapa = now(),": "";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            st = new StringStatement(query, true);
            st.setString(1, idEtapa);
            st.setString(2, usuario);
            st.setString(3, negocio);
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {
                throw new Exception("Error en actualizarEtapaNegocio " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return sql;
    }
    
    @Override
     public String actualizarEtapaDemanda(String idDemanda, String idEtapa, String usuario) {
        Connection con = null;
        StringStatement st;       
        String sql = "";
        String query = "SQL_ACTUALIZAR_ETAPA_DEMANDA";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(idEtapa));
            st.setString(2, usuario);
            st.setInt(3, Integer.parseInt(idDemanda));
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {
                throw new Exception("Error en actualizarEtapaDemanda " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return sql;
    }

    
    @Override
    public String cargarCostosNegocioXEtapa(String etapa,String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();       
        Gson gson = new Gson();
        String query = "SQL_CARGAR_COSTOS_NEG_POR_ETAPA";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query); 
            ps.setString(1, etapa);
            ps.setString(2, negocio);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("id_costo", rs.getInt("id_costo"));
                fila.addProperty("concepto", rs.getString("concepto"));
                fila.addProperty("tipo", rs.getString("tipo"));
                fila.addProperty("valor_porc", rs.getDouble("valor_porc"));       
                fila.addProperty("valor", rs.getDouble("valor"));       
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarCostosNegocioXEtapa " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
    @Override
    public String cargarCostosXEtapa(String etapa,String IsAutomotor) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();       
        Gson gson = new Gson();
        String query = "cargarCostosEtapa"; 
        String filtro = (IsAutomotor.equals("N")) ? " WHERE  id_etapa = " + etapa + " AND solo_automotor ='" + IsAutomotor + "' AND c.reg_status='' AND estado_approv = 'A' " : "  WHERE  id_etapa = " + etapa + " AND c.reg_status='' AND estado_approv = 'A' " ;
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);         
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("concepto", rs.getString("concepto"));
                fila.addProperty("tipo", rs.getString("tipo"));
                fila.addProperty("simbolo", rs.getString("simbolo"));
                fila.addProperty("valor", rs.getString("valor"));    
                fila.addProperty("id_etapa", rs.getString("id_etapa"));    
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarCostosXEtapa " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
    @Override
    public boolean existeRelCostoNegEtapa(int idEtapa, String negocio, int idCosto) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeRelCostoNegEtapa";       
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, idEtapa);
            ps.setString(2, negocio);
            ps.setInt(3, idCosto);
              
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeRelCostoNegEtapa " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }

    @Override
     public String insertarRelCostoNegXEtapa(int idEtapa, String negocio, double valor_saldo, int idCosto, String tipo, double valor, String usuario, String dstrct) {
        Connection con = null;
        StringStatement st;       
        String sql = "";    
        String query =  (existeRelCostoNegEtapa(idEtapa, negocio, idCosto)) ? "actualizaRelCostoNegXEtapa" : "insertarRelCostoNegXEtapa";      
        double valorCosto = (tipo.equals("P")) ? (valor_saldo*valor)/100 : valor;
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, dstrct);
            st.setDouble(2, valorCosto);
            st.setString(3, usuario);
            st.setInt(4, idEtapa);
            st.setString(5, negocio);
            st.setInt(6, idCosto);
          
            sql = st.getSql();  
       
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR insertarRelCostoNegXEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return sql;
       }
    }

    @Override
    public String actualizarRelCostoNegXEtapa(int id, double valor, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "actualizarRelCostoNegXEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setDouble(1, valor);      
            ps.setString(2, usuario);
            ps.setInt(3, id);       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarRelCostoNegXEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }

    @Override
    public String eliminarRelCostoNegXEtapa(int id) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "eliminarRelCostoNegXEtapa";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
         
            ps.setInt(1, id);       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR eliminarRelCostoNegXEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
               System.out.println(ex.getMessage());
               ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }
    
    @Override
    public String cargarCboRespuestasEtapa(int idEtapa) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "cargarRespuestasEtapa";
        String filtro = " AND reg_status=''";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));       
            ps.setInt(1, idEtapa);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }

            return respuestaJson;
        }

    }
    
     @Override
     public String insertarRespuestaProcTraz(int idEtapa, String negocio, int idRespuesta, String respuesta, String comentarios, String usuario, String dstrct) {
        Connection con = null;
        StringStatement st;       
        String sql = "";    
        String query =  "insertarRespuestaProcesoTrazab";      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, dstrct);
            st.setInt(2, idEtapa);
            st.setString(3, negocio);
            st.setInt(4, idRespuesta);
            st.setString(5, respuesta);
            st.setString(6, comentarios);
            st.setString(7, usuario);
          
            sql = st.getSql();  
       
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR insertarRespuestaProcTraz \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return sql;
       }
    }

    @Override
    public String guardarConfigDocs(int tipo_doc, String header_info, String initial_info, String footer_info, String signing_info, String footer_page, String aux_1, String aux_2, String aux_3, String aux_4, String aux_5, String usuario, String dstrct) {
        Connection con = null;
        StringStatement st = null; 
        String query = (existeConfigDocs(tipo_doc)) ? "actualizarConfiguracionDocs" : "insertarConfiguracionDocs";
        String sql = ""; 
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true); 
            st.setString(1, dstrct);
            st.setString(2, header_info);
            st.setString(3, initial_info);
            st.setString(4, footer_info);
            st.setString(5, signing_info);
            st.setString(6, footer_page);
            st.setString(7, aux_1);
            st.setString(8, aux_2);
            st.setString(9, aux_3);
            st.setString(10, aux_4);
            st.setString(11, aux_5);
            st.setString(12, usuario); 
            st.setInt(13, tipo_doc);                  
            
            sql = st.getSql();
            
            
        }catch (Exception e) {
            try {              
                throw new SQLException("ERROR guardarConfigDocs \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;  
       }
    }

   @Override
   public boolean existeConfigDocs(int tipo_doc) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "cargarConfiguracionDocs";   
        String filtro = " WHERE tipo_doc = "+ tipo_doc;
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);          
         
              
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeConfigDocs " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }

    @Override
    public String cargarConfigDocs() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null,rs1 = null;
        JsonObject obj = new JsonObject();
        String filtro = "";
        Gson gson = new Gson();
        String query = "cargarConfiguracionDocs";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query); 
            //ps.setInt(1, tipo_doc);
            rs = ps.executeQuery();     
            JsonArray datos = new JsonArray();          
            while (rs.next()){                
                JsonObject fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("tipo_doc", rs.getInt("tipo_doc"));
                fila.addProperty("header_info", rs.getString("header_info"));
                fila.addProperty("initial_info", rs.getString("initial_info"));
                fila.addProperty("footer_info", rs.getString("footer_info"));
                fila.addProperty("signing_info", rs.getString("signing_info"));
                fila.addProperty("footer_page", rs.getString("footer_page"));
                fila.addProperty("aux_1", rs.getString("aux_1"));
                fila.addProperty("aux_2", rs.getString("aux_2"));
                fila.addProperty("aux_3", rs.getString("aux_3"));
                fila.addProperty("aux_4", rs.getString("aux_4"));
                fila.addProperty("aux_5", rs.getString("aux_5"));
                fila.addProperty("reg_status", rs.getString("reg_status"));   
                query = this.obtenerSQL("cargarConfigDocsDet");
                ps = con.prepareStatement(query);      
                //ps.setInt(1, tipo_doc);
                rs1 = ps.executeQuery();  
                JsonArray arr1 = new JsonArray();
                JsonArray arr2 = new JsonArray();
                JsonArray arr3 = new JsonArray();
                while (rs1.next()){      
                    JsonObject filadet = new JsonObject();
                    filadet.addProperty("id_det_doc", rs1.getInt("id"));
                    filadet.addProperty("titulo", rs1.getString("titulo"));
                    filadet.addProperty("nombre", rs1.getString("nombre"));
                    filadet.addProperty("descripcion", rs1.getString("descripcion"));                 
                    if (rs1.getString("tipo").equals("H")) arr1.add(filadet);
                    if (rs1.getString("tipo").equals("P")) arr2.add(filadet);
                    if (rs1.getString("tipo").equals("M")) arr3.add(filadet);
                }
                if (rs.getInt("tipo_doc") == 1){
                   fila.add("hechos",arr1);
                   fila.add("pretensiones",arr2);   
                }else if (rs.getInt("tipo_doc") == 2){
                   fila.add("medidas",arr3);
                }
                datos.add(fila);                       
            }
            obj.add("docs", datos);
         
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarConfigDocs " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);         
    }
    
    @Override
    public String cargarTipoActores() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
       
        Gson gson = new Gson();
        String filtro = "";
        String query = "cargarTipoActores";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);           
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));                
                fila.addProperty("descripcion", rs.getString("descripcion"));                 
                fila.addProperty("reg_status", rs.getString("reg_status"));  
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarTipoActores " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
    @Override
    public String guardarTipoActor(String descripcion, String usuario, String dstrct) {
        Connection con = null;
        PreparedStatement ps = null;      
        String query = "insertarTipoActor";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, dstrct);          
            ps.setString(2, descripcion);          
            ps.setString(3, usuario);          
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarTipoActor \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;  
       }
    }

 
    @Override
    public String actualizarTipoActor(int id, String descripcion, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "actualizarTipoActor";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);       
            ps.setString(1, descripcion);          
            ps.setString(2, usuario);
            ps.setInt(3, id);       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarTipoActor \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }

    @Override
    public String activaInactivaTipoActor(String id, String estado, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "activarInactivarTipoActor";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, Integer.parseInt(id));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activaInactivaTipoActor \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }
    
    @Override
    public boolean existeTipoActor(String descripcion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeTipoActor";
        String filtro="";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setString(1, descripcion);         
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeTipoActor " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }
    
    
    @Override
    public boolean existeTipoActor(String descripcion, int idTipo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeTipoActor";
        String filtro=" and id not in("+idTipo+")";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setString(1, descripcion);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeTipoActor " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }

       
    @Override
    public String cargarActores() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
       
        Gson gson = new Gson();
        String query = "cargarActores";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);           
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("id_tipo_actor", rs.getInt("id_tipo_actor"));
                fila.addProperty("tipo_actor", rs.getString("tipo_actor"));
                fila.addProperty("tipo_documento", rs.getString("tipo_documento"));
                fila.addProperty("documento", rs.getString("documento"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("codciu", rs.getString("codciu"));
                fila.addProperty("coddpto", rs.getString("coddpto"));
                fila.addProperty("codpais", rs.getString("codpais"));
                fila.addProperty("direccion", rs.getString("direccion"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("tel_extension", rs.getString("tel_extension"));
                fila.addProperty("celular", rs.getString("celular"));
                fila.addProperty("email", rs.getString("email"));
                fila.addProperty("tarjeta_profesional", rs.getString("tarjeta_profesional"));
                fila.addProperty("doc_lugar_exped", rs.getString("doc_lugar_exped"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarActores " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
    @Override
    public String cargarCboTipoActores() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "cargarTipoActores";
        String filtro = " WHERE reg_status = ''";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));         
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }


            return respuestaJson;
        }

    }
    
    @Override
     public String cargarTiposIdentificacion() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPOS_IDENTIFICACION";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));         
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("table_code"), rs.getString("referencia"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }


            return respuestaJson;
        }

    }


    @Override
    public String guardarActor(int tipoActor, String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, 
                               String telefono,  String extension, String celular, String email, String tarjeta_prof, String lugarExpCed, String reg_status, String usuario, String dstrct) {
        Connection con = null;
        PreparedStatement ps = null;      
        String query = "insertarActor";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, dstrct);
            ps.setInt(2, tipoActor);
            ps.setString(3, tipoDoc);
            ps.setString(4, documento);
            ps.setString(5, nombre);
            ps.setString(6, ciudad);
            ps.setString(7, dpto);
            ps.setString(8, pais);
            ps.setString(9, direccion);
            ps.setString(10, telefono);
            ps.setString(11, extension);
            ps.setString(12, celular);
            ps.setString(13, email);
            ps.setString(14, tarjeta_prof);
            ps.setString(15, lugarExpCed);
            ps.setString(16, reg_status);
            ps.setString(17, usuario);     
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarActor \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;  
       }
    }

 
    @Override
    public String actualizarActor(int id, int tipoActor, String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, 
                                  String telefono, String extension, String celular, String email, String tarjeta_prof, String lugarExpCed, String reg_status, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "actualizarActor";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query); 
            ps.setInt(1, tipoActor);
            ps.setString(2, tipoDoc);
            ps.setString(3, documento);
            ps.setString(4, nombre);
            ps.setString(5, ciudad);
            ps.setString(6, dpto);
            ps.setString(7, pais);
            ps.setString(8, direccion);
            ps.setString(9, telefono);
            ps.setString(10, extension);
            ps.setString(11, celular);
            ps.setString(12, email);
            ps.setString(13, tarjeta_prof);
            ps.setString(14, lugarExpCed);
            ps.setString(15, reg_status);
            ps.setString(16, usuario);     
            ps.setInt(17, id);       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarActor \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }

    @Override
    public String activaInactivaActor(String id, String estado, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "activarInactivarActor";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, Integer.parseInt(id));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activaInactivaActor \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }
      
    @Override
    public String cargarEquivalencias() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
       
        Gson gson = new Gson();
        String query = "cargarEquivalencias";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);           
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("codparam", rs.getString("codparam"));
                fila.addProperty("descripcion", rs.getString("descripcion")); 
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarEquivalencias " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }

    
    @Override
    public boolean existeDemandaDocs(String negocio, boolean genero_PDF) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeDemandaDocs";
        String filtro = (genero_PDF) ? " AND docs_generados = 'S'" : "";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setString(1, negocio);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                System.out.println(e.getMessage());
                e.printStackTrace();
                throw new Exception("Error en existeDemandaDocs " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }
    

    @Override
    public String crearDemanda(JsonArray jsonArrNeg, Usuario usuario) {
        Connection con = null;
        String resp="{}";
        String query =  "creaDemandaDocs";      

        try{            

            con = this.conectarJNDI(this.obtenerSQL(query),usuario.getBd());
            CallableStatement callableStatement = con.prepareCall("{" + this.obtenerSQL(query) + "}");
            for (int i = 0; i < jsonArrNeg.size(); i++) {
                String negocio = jsonArrNeg.get(i).getAsJsonObject().get("cod_neg").getAsJsonPrimitive().getAsString();
                String nit = jsonArrNeg.get(i).getAsJsonObject().get("nit_demandado").getAsJsonPrimitive().getAsString();
                //double valor_saldo = jsonArrNeg.get(i).getAsJsonObject().get("valor_saldo").getAsJsonPrimitive().getAsDouble();
                String valor_saldo = jsonArrNeg.get(i).getAsJsonObject().get("valor_saldo").getAsJsonPrimitive().getAsString();
                
                if  (!existeDemandaDocs(negocio, false)){
                    callableStatement.setString(1, usuario.getDstrct());
                    callableStatement.setString(2, negocio);
                    callableStatement.setString(3, nit);
                    callableStatement.setString(4, valor_saldo);
                    callableStatement.setString(5, usuario.getLogin()); 
                    callableStatement.addBatch();
                }
               
            }
            int[] updateCounts = callableStatement.executeBatch();
            resp = "{\"respuesta\":\"OK\"}"; 
        }catch (Exception e) {
            resp = "{\"respuesta\":\""+e.getMessage()+"\"}"; 
            try {               
                System.out.println(e.getMessage());
                e.printStackTrace();
                throw new SQLException("ERROR crearDemanda \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }           
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }     
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }      
            return resp;
       }
    }


    @Override
    public String cargarDemandaDocs(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null, rs1 = null;
        JsonObject obj = new JsonObject();
       
        Gson gson = new Gson();
        String query = "cargarDemandaDocs";
        String filtro = " WHERE d.negocio = '"+negocio+"'";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query); 
            rs = ps.executeQuery();    
            JsonArray datos = new JsonArray();          
            while (rs.next()){             
                JsonObject fila = new JsonObject();
                fila.addProperty("id_enc_doc", rs.getInt("id"));
                fila.addProperty("id_demanda", rs.getInt("id_demanda"));
                fila.addProperty("tipo_doc", rs.getInt("tipo_doc"));
                fila.addProperty("header_info", rs.getString("header_info"));
                fila.addProperty("initial_info", rs.getString("initial_info"));
                fila.addProperty("footer_info", rs.getString("footer_info"));
                fila.addProperty("signing_info", rs.getString("signing_info"));
                fila.addProperty("footer_page", rs.getString("footer_page"));
                fila.addProperty("aux_1", rs.getString("aux_1"));
                fila.addProperty("aux_2", rs.getString("aux_2"));
                fila.addProperty("aux_3", rs.getString("aux_3"));
                fila.addProperty("aux_4", rs.getString("aux_4"));
                fila.addProperty("aux_5", rs.getString("aux_5")); 
                query = this.obtenerSQL("cargarDemandaDocsDet");
                ps = con.prepareStatement(query);      
                ps.setInt(1, rs.getInt("id"));
                rs1 = ps.executeQuery();  
                JsonArray arr1 = new JsonArray();
                JsonArray arr2 = new JsonArray();
                JsonArray arr3 = new JsonArray();
                while (rs1.next()){      
                    JsonObject filadet = new JsonObject();
                    filadet.addProperty("id_det_doc", rs1.getInt("id"));
                    filadet.addProperty("titulo", rs1.getString("titulo"));
                    filadet.addProperty("descripcion", rs1.getString("descripcion"));
                    if (rs1.getString("tipo").equals("H")) arr1.add(filadet);
                    if (rs1.getString("tipo").equals("P")) arr2.add(filadet);
                    if (rs1.getString("tipo").equals("M")) arr3.add(filadet);
                }
                
                if (rs.getInt("tipo_doc") == 1) {
                    fila.add("hechos", arr1);
                    fila.add("pretensiones", arr2);
                } else if (rs.getInt("tipo_doc") == 2) {
                    fila.add("medidas", arr3);
                }
               
                datos.add(fila);                
            }          
            obj.add("docs", datos);
        }catch (Exception e) {
            try {
                System.out.println(e.getMessage());
                e.printStackTrace();
                throw new Exception("Error en cargarDemandaDocs " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);         
    }
    
    @Override
    public String guardarDemandaDocs(int id_doc, int id_demanda, int tipo_doc, String header_info, String initial_info, String footer_info, String signing_info, String footer_page, String aux_1, String aux_2, String aux_3, String aux_4, String aux_5, String usuario, String dstrct) {
        Connection con = null;
        StringStatement st;       
        String sql = ""; 
        String query = (id_doc==0) ? "insertarDemandaDocs" : "actualizaDemandaDocs";    
       
        try{
         
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);          
            st.setString(1, dstrct);
            st.setString(2, header_info);
            st.setString(3, initial_info);
            st.setString(4, footer_info);
            st.setString(5, signing_info);
            st.setString(6, footer_page);
            st.setString(7, aux_1);
            st.setString(8, aux_2);
            st.setString(9, aux_3);
            st.setString(10, aux_4);
            st.setString(11, aux_5);
            st.setString(12, usuario); 
            st.setInt(13, id_demanda);       
            st.setInt(14, tipo_doc);                  
            
            sql = st.getSql();
                       
        }catch (Exception e) {
            try {       
                System.out.println(e.getMessage());
                e.printStackTrace();
                throw new SQLException("ERROR guardarDemandaDocs \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }             
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;  
       }
    }
    
    public boolean existeDemandaDocsItem(int id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeDemandaDocsItem";       
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
         
              
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeDemandaDocsItem " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }
    
    @Override
    public String guardarConfigDocsDet(int tipo_doc, String tipo, String titulo, String nombre, String descripcion, String usuario, String dstrct) {
        Connection con = null;
        StringStatement st;       
        String sql = "";       
        String query = "insertarConfigDocsDet";       
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);      
            st.setString(1, dstrct);
            st.setString(2, tipo);
            st.setString(3, titulo);
            st.setString(4, nombre);
            st.setString(5, descripcion);         
            st.setString(6, usuario); 
            st.setInt(7, tipo_doc);              
            
            sql = st.getSql();          
            
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR guardarConfigDocsDet \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;  
       }
    }

    @Override
    public String eliminarConfigDocsDet(int tipo_doc) {
        Connection con = null;
        StringStatement st;       
        String sql = "";    
        String query =  "eliminaConfigDocsDet";  
        String filtro = " WHERE id_tipo_doc = "+ tipo_doc;
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            st = new StringStatement(query, true);                             
          
            sql = st.getSql();  
       
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR eliminarConfigDocsDet \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return sql;
       }
    }
    
    @Override
    public String guardarDemandaDocsDet(String id_det_doc, int id_doc, String tipo, String titulo, String descripcion, String usuario, String dstrct) {
        Connection con = null;
        StringStatement st;       
        String sql = "";       
       // String Qry = (existeDemandaDocsItem(id_det_doc)) ? "actualizaDemandaDocsDet" : "insertarDemandaDocsDet";
        String Qry = "insertarDemandaDocsDet";
        String query ="";      
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(Qry);
            st = new StringStatement(query, true);      
            st.setString(1, dstrct);
            st.setString(2, tipo);
            st.setString(3, titulo);
            st.setString(4, descripcion);         
            st.setString(5, usuario); 
            st.setInt(6, id_doc);
            //st.setInt(6,(Qry.equals("actualizaDemandaDocsDet")) ? id_det_doc : id_doc);                  
            
            sql = st.getSql();          
            
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR guardarDemandaDocsDet \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;  
       }
    }

    @Override
    public String eliminarDemandaDocsDet(int id_doc, String tipo) {
        Connection con = null;
        StringStatement st;       
        String sql = "";    
        String query =  "eliminaDemandaDocsDet";  
        String filtro = " AND tipo ='" + tipo + "'";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            st = new StringStatement(query, true);
            st.setInt(1, id_doc);                    
          
            sql = st.getSql();  
       
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR eliminarDemandaDocsDet \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return sql;
       }
    }
    
    @Override
    public boolean generarDemandaDocs(int id_demanda, String ruta, String usuario, String dstrct){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null, rs1 = null;
        String query = "cargarDemandaDocs";
        String filtro = " WHERE id_demanda = "+id_demanda;
        String htmldoc, header_info = "", initial_info = "", footer_info= "", signing_info, footer_page = "", aux_1 = "", aux_2 = "", aux_3 = "", aux_4 = "", aux_5 = "";
        ResourceBundle rb = null;
        boolean estado = false;
        try{
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            Document document = new Document(PageSize.LEGAL, 60, 60, 72, 52);
           
            HTMLWorker htmlWorker = new HTMLWorker(document);           
        
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(ruta+"/demanda_"+id_demanda+".pdf"));
            
            HeaderFooter headerFooter = new HeaderFooter(rb,this.getDatabaseName());
            pdfWriter.setPageEvent(headerFooter);
            
            document.open();
            document.addAuthor(usuario);
            document.addCreator("FINTRA S.A.");
            document.addCreationDate();
            document.addTitle("Demanda");
            
        
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);  
            rs = ps.executeQuery();              
            while (rs.next()){      
                htmldoc = "";              
                     
                header_info = removeStylesFromHtml(rs.getString("header_info").trim());
                initial_info = removeStylesFromHtml(rs.getString("initial_info").trim());
                htmldoc =  htmldoc +  header_info + "<br>" + initial_info + "<br>";
               //htmldoc = htmldoc + rs.getString("header_info") + rs.getString("initial_info");
                query = this.obtenerSQL("cargarDemandaDocsDet");
                ps = con.prepareStatement(query);      
                ps.setInt(1, rs.getInt("id"));
                rs1 = ps.executeQuery();  
                String titulo = "";
                while (rs1.next()){  
                      while (!titulo.equals(rs1.getString("titulo").trim()) && rs.getInt("tipo_doc") != 2){
                            titulo = rs1.getString("titulo").trim();
                            htmldoc = htmldoc +  "<b><h4 style=\"text-align:center\">" + titulo + "</h4></b><br>";
                      }
                      htmldoc = htmldoc + removeStylesFromHtml(rs1.getString("descripcion").trim() + "<br>");
                }
                aux_1 = ""; aux_2 = ""; aux_3 = ""; aux_4 = ""; aux_5 = "";
                if (rs.getInt("tipo_doc") == 1) {
                    aux_1 = "<b><h4 style=\"text-align:center\">FUNDAMENTOS DE DERECHO</h4></b><br>" + removeStylesFromHtml(rs.getString("aux_1").trim()) + "<br>";
                    aux_2 = "<b><h4 style=\"text-align:center\">COMPETENCIA Y CUANTIA</h4></b><br>" + removeStylesFromHtml(rs.getString("aux_2").trim()) + "<br>";                 ;
                    aux_3 = "<b><h4 style=\"text-align:center\">PRUEBAS</h4></b><br>" + removeStylesFromHtml(rs.getString("aux_3").trim()) + "<br>";
                    aux_4 =  "<b><h4 style=\"text-align:center\">ANEXOS</h4></b><br>" + removeStylesFromHtml(rs.getString("aux_4").trim()) + "<br>";
                    aux_5 =  "<b><h4 style=\"text-align:center\">NOTIFICACIONES</h4></b><br>" + removeStylesFromHtml(rs.getString("aux_5").trim()) + "<br>";
                }
                footer_info = removeStylesFromHtml(rs.getString("footer_info").trim()) + "<br>";
                signing_info = removeStylesFromHtml(rs.getString("signing_info").trim()) + "<br>";
                footer_page = removeStylesFromHtml(rs.getString("footer_page").trim()) + "<br>";
                htmldoc = htmldoc + aux_1 + aux_2 + aux_3 + aux_4 + aux_5 + footer_info + signing_info/* + footer_page*/;
               
                StyleSheet styles = new StyleSheet();
                styles.loadTagStyle("p", "size", "11pt"); 
                styles.loadTagStyle("div", "style", "line-height:14px !important"); 
                styles.loadTagStyle("p", "style", "line-height:14px !important");
               
                   
                htmlWorker.setStyleSheet(styles);
                htmlWorker.parse(new StringReader("<div>"+htmldoc+"</div>"));
   
             
                if (!rs.isLast()) document.newPage();                
            }

            document.close();
            estado = true;
        }catch (Exception ex){
            estado = false;
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }finally{
             return estado;
        }   
    }
    
    public String removeStylesFromHtml(String texto){
          String result = "";
          int pini, pfin;
          result = texto;
          String startTag = "",closeTag = "";
          while(result.contains("<style type=\"text/css\">")||result.contains("<a name=\"__DdeLink__")){
                startTag = (result.contains("<style type=\"text/css\">")) ? "<style type=\"text/css\">": "<a name=\"__DdeLink__";
                closeTag = (result.contains("<style type=\"text/css\">")) ? "</style>": "</a>";
            pini = result.indexOf(startTag);
            pfin = result.indexOf(closeTag);
            result = result.replace(result.substring(pini, pfin + closeTag.length()), "");
        }
          
          return result;    
    }

    @Override
    public String cargarCostosAutorizacion() {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
       
        Gson gson = new Gson();
        String query = "cargarCostosEtapa";
        String filtro = "";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);          
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("id_etapa", rs.getInt("id_etapa"));
                fila.addProperty("etapa", rs.getString("etapa"));
                fila.addProperty("concepto", rs.getString("concepto"));
                fila.addProperty("tipo", rs.getString("tipo"));  
                fila.addProperty("simbolo", rs.getString("simbolo"));  
                fila.addProperty("valor", rs.getString("valor"));  
                fila.addProperty("solo_automotor", rs.getString("solo_automotor"));  
                fila.addProperty("id_estado_ap", rs.getString("id_estado_ap")); 
                fila.addProperty("estado_ap", rs.getString("estado_ap")); 
                fila.addProperty("reg_status", rs.getString("reg_status"));  
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarCostosAutorizacion " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
     @Override
     public String actualizaAprobCostoEtapa(int idCosto, String estado, String usuario, String dstrct) {
        Connection con = null;
        StringStatement st;       
        String sql = "";    
        String query =  "actualizarAprobCostoEtapa";     
      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, estado);
            st.setString(2, usuario);
            st.setString(3, usuario);
            st.setInt(4, idCosto);       
          
            sql = st.getSql();  
       
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR actualizaAprobCostoEtapa \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return sql;
       }
    }
     
    @Override
    public String cargarCboAutorizacionCostos() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "cargarEstadosAutorizacion";
        String filtro = " WHERE reg_status = ''";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));         
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("codigo"), rs.getString("nombre"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }


            return respuestaJson;
        }

    }
    
    @Override
    public String insertarRespuestaTrazabilidad(int idEtapa, String negocio, int idRespuesta, String respuesta, String coments, String usuario, String dstrct) {
        Connection con = null;         
        StringStatement st = null;    
        String sql = "";
        String query = "insertarRespuestaTrazabilidad";
               
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);  
            st.setString(1, dstrct);
            st.setInt(2, idEtapa);
            st.setString(3, negocio);
            st.setInt(4, idRespuesta);
            st.setString(5, respuesta);
            st.setString(6, coments);
            st.setString(7, usuario);
            
            sql = st.getSql();        
            
        }catch (Exception e) {
            try {              
                throw new SQLException("ERROR insertarRespuestaTrazabilidad \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql; 
       }
    }
        
    @Override
    public String actualizaRespuestaDemanda(String idDemanda, int idRespuesta, String usuario) {
        Connection con = null;         
        StringStatement st = null;    
        String sql = "";
        String query = "actualizaRespuestaDemanda";
               
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);          
            st.setInt(1, idRespuesta);
            st.setString(2, usuario);
            st.setInt(3, Integer.parseInt(idDemanda));       
            
            sql = st.getSql();        
            
        }catch (Exception e) {
            try {              
                throw new SQLException("ERROR actualizaRespuestaDemanda \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql; 
       }
    }
   
    @Override
    public String actualizaCampoPdfGenerado(String idDemanda, String pdf_generado, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;  
        String query = "actualizaCampoPdfGenerado";
        String  respuestaJson = "{}";
             
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, pdf_generado);
            ps.setString(2, usuario);
            ps.setInt(3, Integer.parseInt(idDemanda));       
            
            ps.executeUpdate();    
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {          
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizaCampoPdfGenerado \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return respuestaJson; 
       }
    }
    
    @Override
    public String listarEtapaConfigTraz(String idetapa) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();       
        Gson gson = new Gson();
        String query = "SQL_CARGAR_TRAZA_COSTOS_ETAPA";
      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query); 
            ps.setInt(1, Integer.parseInt(idetapa));
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();                
                fila.addProperty("id_etapa", rs.getString("id_etapa"));    
                fila.addProperty("etapa", rs.getString("etapa"));    
                fila.addProperty("usuario", rs.getString("usuario"));
                fila.addProperty("fecha", rs.getString("fecha"));
                fila.addProperty("concepto", rs.getString("concepto"));
                fila.addProperty("valor", rs.getDouble("valor"));           
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en listarProcesoTrazabilidad " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
    
    @Override
    public String listarProcesoTrazabilidad(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();       
        Gson gson = new Gson();
        String query = "SQL_CARGAR_TRAZABILIDAD_PROCESO";
      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query); 
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("tipo", rs.getString("tipo"));
                fila.addProperty("id_etapa", rs.getString("id_etapa"));    
                fila.addProperty("etapa", rs.getString("etapa"));               
                fila.addProperty("usuario", rs.getString("usuario"));
                fila.addProperty("fecha", rs.getString("fecha"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("comentarios", rs.getString("comentarios"));
                fila.addProperty("concepto", rs.getString("concepto"));
                fila.addProperty("valor", rs.getDouble("valor"));           
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en listarProcesoTrazabilidad " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
    
    @Override
    public boolean copiarArchivo(
                                 String negocio
                                ,String rutaOrigen, String rutaDestino
                                ,String filename
                               ) {

        boolean swFileCopied = false;
        try{            
          
            File carpetaDestino = new File(rutaDestino);
          
            //deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);
                          
            try{
                    //Copiamos el archivo de la carpeta origen a la carpeta destino
                    InputStream in = new FileInputStream(rutaOrigen + negocio + "/" +filename);
                    OutputStream out = new FileOutputStream(rutaDestino + filename);

                    // Copy the bits from instream to outstream
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                    swFileCopied = true;
               
                   }catch(Exception k){ throw new Exception(" FILE: " + k.getMessage());}

            }catch(Exception e){
                System.out.println("error:"+e.toString()+"__"+e.getMessage());              
            }
       
        return swFileCopied;
    }
    
     public void createDir(String dir) throws Exception{
        try{
        File f = new File(dir);
        if(! f.exists() ) f.mkdir();    
        }catch(Exception e){ throw new Exception ( e.getMessage());}
   }
     
    @Override
    public String cargarJuzgados() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();

        Gson gson = new Gson();
        String query = "cargarJuzgados";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("nombre", rs.getString("nombre"));             
                fila.addProperty("reg_status", rs.getString("reg_status"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarJuzgados " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String guardarJuzgado(String nombre, String usuario, String dstrct) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "guardarJuzgado";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);       
            ps.setString(2, usuario);
            ps.setString(3, dstrct);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarJuzgado \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarJuzgado(String nombre,String idJuzgado, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "actualizarJuzgado";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);         
            ps.setString(2, usuario);
            ps.setInt(3, Integer.parseInt(idJuzgado));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarJuzgado \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }


    @Override
    public String activaInactivaJuzgado(String id, String estado, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "activarInactivarJuzgado";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activaInactivaJuzgado \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public boolean existeJuzgado(String nombre) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeJuzgado";
        String filtro="";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);         
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeJuzgado " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }
    
    
    @Override
    public boolean existeJuzgado(String nombre, int idJuzgado) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "existeJuzgado";
        String filtro=" and id not in("+idJuzgado+")";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeJuzgado " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }

    @Override
     public String cargarCboJuzgados() {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "cargarJuzgados";
        String filtro = " AND reg_status=''";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));     
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }

            return respuestaJson;
        }

    }
     
             
    @Override
    public String actualizaRadicadoJuzgado(String idDemanda, int idJuzgado, String radicado, String usuario) {
        Connection con = null;         
        StringStatement st = null;    
        String sql = "";
        String query = "actualizaRadicadoJuzgado";
               
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);          
            st.setInt(1, idJuzgado);
            st.setString(2, radicado);
            st.setString(3, usuario);
            st.setInt(4, Integer.parseInt(idDemanda));       
            
            sql = st.getSql();        
            
        }catch (Exception e) {
            try {              
                throw new SQLException("ERROR actualizaRadicadoJuzgado \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql; 
       }
    }
    
    @Override
    public String generarActaPagares(JsonArray jsonArrNeg, String ruta, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;      
        String sql = "";    
        String query =  "obtenerInfoActa", nomarchivo = "";      
        String htmldoc, header_info = "", initial_info = "", footer_info= "", signing_info = "", footer_page = "", aux_1 = "", aux_2 = "", aux_3 = "", aux_4 = "", aux_5 = "";
        ResourceBundle rb = null;
        String  respuestaJson = "{}";
        try{        
           DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
	   //get current date time with Date()
	   Date date = new Date();
           nomarchivo = "acta_entrega_"+dateFormat.format(date)+".pdf";
	   //System.out.println(dateFormat.format(date));
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");           
            Document document = new Document(PageSize.LEGAL, 85, 85, 72, 52);
           
            HTMLWorker htmlWorker = new HTMLWorker(document);           
        
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(ruta+"/"+nomarchivo));  
            
            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
            
            Font f2 = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL);
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
          
            HeaderFooterActa header = new HeaderFooterActa(rb);
            pdfWriter.setPageEvent(header);

            document.open();
            document.addAuthor(usuario);
            document.addCreator("FINTRA S.A.");
            document.addCreationDate();
            document.addTitle("Acta");
            
         
            
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                header_info = rs.getString("header_info");
                initial_info = rs.getString("initial_info");
                signing_info = rs.getString("signing_info");
                aux_1 = rs.getString("fecha_actual");
                aux_2 = rs.getString("abogado");               
            }
            
                      
                Phrase p1 = new Phrase("Barranquilla, "+aux_1, f);
                Phrase p2 = new Phrase("Se�or\n" + aux_2 , f1);
                Phrase p3 = new Phrase(header_info, f1);
                Phrase p4 = new Phrase(initial_info, f);
               

                document.add(p1);
                document.add(new Paragraph("\n\n"));
                document.add(p2);
                document.add(new Paragraph("\n\n"));
                document.add(p3);
                document.add(new Paragraph(" "));
                document.add(p4);
           
            htmldoc = "";                  
            
            PdfPTable table = new PdfPTable(5);
            table.setWidths(new int[]{ 6, 3, 3, 3, 3 });
            table.setWidthPercentage(100);
            PdfPCell cell;
            //ENCABEZADOS DE LA TABLA
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("NOMBRE", f3));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.addCell(cell);
            // row 1, cell 2
            cell = new PdfPCell(new Phrase("IDENTIFICACION", f3));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            //cell.setBorder(Rectangle.NO_BORDER);              
            table.addCell(cell);
            // row 1, cell 3
            cell = new PdfPCell(new Phrase("No NEGOCIO", f3));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.addCell(cell);
            // row 1, cell 4
            cell = new PdfPCell(new Phrase("No PAGAR�", f3));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.addCell(cell);
            // row 1, cell 5
            cell = new PdfPCell(new Phrase("VALOR", f3));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.addCell(cell);
            DecimalFormat formato = new DecimalFormat("$###,###,###.00");         
            for (int i = 0; i < jsonArrNeg.size(); i++) {
                String negocio = jsonArrNeg.get(i).getAsJsonObject().get("cod_neg").getAsJsonPrimitive().getAsString();
                String cedula = jsonArrNeg.get(i).getAsJsonObject().get("nit_demandado").getAsJsonPrimitive().getAsString();
                String nombre = jsonArrNeg.get(i).getAsJsonObject().get("nombre").getAsJsonPrimitive().getAsString();
                String pagare = jsonArrNeg.get(i).getAsJsonObject().get("pagare").getAsJsonPrimitive().getAsString();
                Double valor_saldo = jsonArrNeg.get(i).getAsJsonObject().get("valor_saldo").getAsJsonPrimitive().getAsDouble();
                
                // row 1, cell 1
                cell = new PdfPCell(new Phrase(nombre, f2));               
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                table.addCell(cell);
                // row 1, cell 2
                cell = new PdfPCell(new Phrase(cedula, f2));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);                          
                table.addCell(cell);
                // row 1, cell 3
                cell = new PdfPCell(new Phrase(negocio, f2));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE); 
                table.addCell(cell);
                // row 1, cell 4
                cell = new PdfPCell(new Phrase(pagare, f2));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);            
                table.addCell(cell);
                // row 1, cell 5
                cell = new PdfPCell(new Phrase(formato.format(valor_saldo), f2));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);            
                table.addCell(cell);
                    
           }

        document.add(table);
        
        Paragraph ph = new Paragraph(new Phrase(" "));
        cell = new PdfPCell(ph);
        cell.setBorder(Rectangle.BOTTOM);
        cell.setBorderColor(Color.BLACK);
        cell.setBorderWidth(2f);

        PdfPTable table1 = new PdfPTable(1);                
        table1.addCell(cell);
        table1.setWidthPercentage(37);
        table1.setHorizontalAlignment(Element.ALIGN_LEFT);

        
        Phrase p5 = new Phrase(signing_info, f1);        
        Phrase p6 = new Phrase("SUPERVISOR DE CARTERA.", f1);
        document.add(new Paragraph("\n\n\n")); 
        document.add(p5);           
        document.add(new Paragraph("\n\n")); 
        document.add(table1);      
        document.add(p6); 
        // step 5
        document.close();      
                
        respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\""+"/images/multiservicios/" + usuario + "/"+nomarchivo+"\"}";     

        }catch (Exception e) {
            try {             
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR generarActaPagares \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }           
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }   
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }      
            return respuestaJson;
       }
    }
    
    @Override
    public String listarCondicionesEspeciales(String idItem) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = null;
        Gson gson = new Gson();
        String respuesta="";
        try {
            obj = new JsonObject();
            String query = "listarCondicionesEspeciales";          
            con = this.conectarJNDI();          
            ps = con.prepareStatement(this.obtenerSQL(query));  
            ps.setString(1, idItem);
            rs = ps.executeQuery();      
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("codigo", rs.getString("codigo"));
                fila.addProperty("descripcion", rs.getString("descripcion"));             
                fila.addProperty("aplica_condicion", rs.getString("aplica_condicion"));         
                datos.add(fila);
            }           
            respuesta = gson.toJson(datos);
       
        } catch (Exception ex) {  
              respuesta = "{\"mensaje\":\"" + ex.getMessage() + "\"}";
              throw new SQLException("ERROR OBTENIENDO ESTADOS DE CARTERA: listarEstadosCartera()  " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }
    
    @Override
    public String guardarCondicionEspecial(String id_condicion, String id_item, String usuario, String dstrct) {
        Connection con = null;
        StringStatement st;       
        String sql = "";         
        String Qry = "insertarCondicionEspecial";
        String query ="";      
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(Qry);
            st = new StringStatement(query, true);      
            st.setString(1, dstrct);
            st.setString(2, id_condicion);
            st.setString(3, id_item);          
            st.setString(4, usuario); 
            
            sql = st.getSql();          
            
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR guardarCondicionEspecial \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;  
       }
    }

    @Override
    public String eliminarCondicionesEspeciales(String id_item) {
        Connection con = null;
        StringStatement st;       
        String sql = "";    
        String query =  "eliminaCondicionesEspeciales";         
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id_item);                    
          
            sql = st.getSql();  
       
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR eliminarCondicionesEspeciales \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return sql;
       }
    }
    
  
    @Override
    public String eliminarConfigCondicionEspecial(String id_item) {
        Connection con = null;
        PreparedStatement ps = null;    
        String query = "eliminaCondicionesEspeciales";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
         
            ps.setString(1, id_item);       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR eliminarConfigCondicionEspecial \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
               System.out.println(ex.getMessage());
               ex.printStackTrace();
            }
            return respuestaJson; 
       }
    }    
        
    static class HeaderFooter extends PdfPageEventHelper {

        private Image img1, img2;
        private String DbName;

        public HeaderFooter(ResourceBundle rb, String database) {
            try {
                DbName = database;
                String url_logo1 = "cobranza.jpg", url_logo2 = "cobranza2.png";
                img1 = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo1);
                img1.setAbsolutePosition(10f, 930f);
                img1.scalePercent(70);         
                img2 = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo2);
                img2.setAbsolutePosition(490f, 950f);
                img2.scalePercent(8);           
            } catch (BadElementException | IOException r) {
                System.err.println("Error al leer la imagen");
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            try {
                document.add(img1);
                document.add(img2);
               // ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("REFERENCIA DE PAGO: " + this.num_extracto, new Font(Font.ITALIC, 12, Font.BOLD)), 230, 730f, 0);
            } catch (DocumentException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            Connection con = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            String query = "obtenerInfoFooter", mail = "", direccion = "", telefono = "", extension = "", ciudad = "";
            ProcesoEjecutivoImpl proc = new ProcesoEjecutivoImpl(DbName);
            try {               
                con = proc.conectarJNDI();
                query = proc.obtenerSQL(query);
                ps = con.prepareStatement(query);             
           
                rs = ps.executeQuery();
            
                while (rs.next()){
                    direccion = rs.getString("direccion");
                    telefono = rs.getString("telefono");
                    extension = rs.getString("tel_extension");
                    mail = rs.getString("email");
                    ciudad = rs.getString("ciudad");
                }

                 Font f =  new Font(Font.ITALIC, 10, Font.BOLD);                
                 Font f1 =  new Font(Font.ITALIC, 10, Font.BOLD);                 
                 f1.setColor(Color.BLUE);  
                 f1.setStyle(Font.UNDERLINE);
                 
                Chunk c1 = new Chunk("Email: ", f);
                Chunk c2 = new Chunk(mail, f1);
                Chunk c3 = new Chunk(", "+ ciudad, f);
               /* Chunk c1 = new Chunk("Email: ", f);
                Chunk c2 = new Chunk("carco78@hotmail.com", f1);
                Chunk c3 = new Chunk(", Barranquilla-Colombia", f);*/
  
                Phrase p1 = new Phrase();
                p1.add(c1);
                p1.add(c2);
                p1.add(c3);
              
             
                //ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, p1, 320, 20f, 0);
               // ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("Carrera 53 No 79-01 Oficina 205, Tel�fono 3679905 ext. 1235 " , f), 320, 30f, 0);
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(direccion + ", Tel�fono " + telefono + " ext. " + extension , f), 320, 30f, 0);
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, p1, 320, 20f, 0);
            } catch (Exception ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            try {
                if (con != null) {
                    proc.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        }
    }
    
    
    static class HeaderFooterActa extends PdfPageEventHelper {

        private Image img;      

        public HeaderFooterActa(ResourceBundle rb) {
            try {            
                String url_logo = "logoFintra.jpg";
                img = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
                img.setAbsolutePosition(490f, 930f);
                img.scalePercent(70);   
            } catch (BadElementException | IOException r) {
                System.err.println("Error al leer la imagen");
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            try {
                document.add(img);              
            } catch (DocumentException ex) {
                Logger.getLogger(ProcesoEjecutivoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
