/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.CompraCarteraDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import leerexcel.LeerExcel;

/**
 *
 * @author egonzalez
 */
public class CompraCarteraImpl extends MainDAO implements CompraCarteraDAO{

    public CompraCarteraImpl(String databaseName) {
        super("CompraCarteraDAO.xml", databaseName);
    }
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    StringStatement st;
    
    @Override
    public ArrayList<String> cargarCarteraNueva(String ruta, String fileCredito, 
                            String filePersona, String fileEstudiante, Usuario u)throws Exception{
        
         ArrayList<String> listaQuerys = null;

        String query = "CARGAR_ARCHIVO_SOLICITUD";
        String sql = "";        
        //BLOQUE UNO
        sql = this.obtenerSQL(query);
        StringStatement st = null;
        st = new StringStatement(sql, true);
        st.setString(1, u.getLogin());        
        sql=st.getSql();
        LeerExcel excel=new LeerExcel();
        List lista_datos_celda = excel.Leer_Archivo_Excel(ruta+ "/" +fileCredito);
        String[] vectorQuerys = excel.Imprimir_Consola(lista_datos_celda, sql).split(";");
        listaQuerys = new ArrayList<>();
        listaQuerys.addAll(Arrays.asList(vectorQuerys));
        
        //BLOQUE DOS
        query = "CARGAR_ARCHIVO_PERSONA";
        sql = this.obtenerSQL(query);
        st =null;
        st = new StringStatement(sql, true);
        st.setString(1, u.getLogin());
        sql=st.getSql();        
        lista_datos_celda = excel.Leer_Archivo_Excel(ruta+ "/" +filePersona);
        vectorQuerys = excel.Imprimir_Consola(lista_datos_celda, sql).split(";");
        listaQuerys.addAll(Arrays.asList(vectorQuerys));
        
        
        //BLOQUE TRES
        query = "CARGAR_ARCHIVO_ESTUDIANTE";
        sql = this.obtenerSQL(query);
        st =null;
        st = new StringStatement(sql, true);
        st.setString(1, u.getLogin());
        sql=st.getSql();        
        lista_datos_celda = excel.Leer_Archivo_Excel(ruta+ "/" +fileEstudiante);
        vectorQuerys = excel.Imprimir_Consola(lista_datos_celda, sql).split(";");
        listaQuerys.addAll(Arrays.asList(vectorQuerys));
        
        return listaQuerys;        
        
        
    }

    @Override
    public String getInfoCargaCartera(String query, String parametro, Usuario u) {
        con = null;
        ps = null;
        rs = null;
        
        String respuestaJson="{}";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query,u.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                if(!parametro.equals("")) ps.setInt(1, Integer.parseInt(parametro));                
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }
                
                respuestaJson=new Gson().toJson(lista);
            }
        } catch (SQLException e) {
            lista = null;
            respuestaJson = "{\"error\":\"Lo sentimos algo salio mal al carga la informacion.\","
                    + "\"exception\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }
    }
    
    @Override
     public String crearNuevoNegocioCartera(Usuario u) {
       
        con = null;
        ps = null;
        rs = null;
        String query = "GENERAR_NUEVO_NEGOCIO_CARTERA";
        String respuestaJson="{}";
        try {
            con = this.conectarJNDI(query,u.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, u.getLogin());
                rs = ps.executeQuery();

                if (rs.next()) {
                    respuestaJson = "{\"respuesta\":\"" + rs.getString("retorno") + "\"}";
                } else {
                    respuestaJson = "{\"error\":\"Lo sentimos no se pudo crear el negocio\","
                            + "\"exception\":\" SELECT eg_generar_negocio_compra_cartera(" + u.getLogin() + ") :=BAD as retorno\"}";
                }
            }
        } catch (SQLException e) {
            respuestaJson = "{\"error\":\"Lo sentimos algo salio mal al crear el negocio.\","
                    + "\"exception\":\" SELECT eg_generar_negocio_compra_cartera(" + u.getLogin() + ") := " + e.getMessage() + "\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }

    }

    @Override
    public String buscarNegocioCartera(Usuario u, String estado) {
        con = null;
        ps = null;
        rs = null;
        
        String respuestaJson="{}";
        String query="BUSCAR_NEGOCIOS_CARTERA";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query,u.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, estado);
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }
                
                respuestaJson=new Gson().toJson(lista);
            }
        } catch (SQLException e) {
            lista = null;
            respuestaJson = "{\"error\":\"Lo sentimos algo salio mal al carga la informacion.\","
                    + "\"exception\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }
    }
    
    
    @Override
    public String descartarNegocios(Usuario u) {
        
        String respuestaJson="{}";
        con = null;
        String query = "BUSCAR_NEGOCIOS_CARTERA";

        try {
            
            con = this.conectarJNDI(query,u.getBd());
            con.setAutoCommit(false);
            Statement stmt = con.createStatement();

            stmt.addBatch("UPDATE negocios set estado_neg='R' WHERE cod_neg in (select cod_neg from administrativo.negocios_xliquidacion  where estado='NP');");
            stmt.addBatch("UPDATE solicitud_aval set estado_sol='R' WHERE cod_neg in (select cod_neg from administrativo.negocios_xliquidacion  where estado='NP');");
            stmt.addBatch("DELETE from administrativo.negocios_xliquidacion WHERE estado='NP'");
           
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            
            respuestaJson= "{\"respuesta\":\"OK\"}";
  
        } catch (SQLException ex) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"Lo sentimos algo salio mal al rechazar los negocios.\","
                    + "\"exception\":\"" + ex.getMessage() + "\"}";
                ex.printStackTrace();
            } catch (SQLException ex1) {
                Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                this.desconectar(con);
            } catch (SQLException e) {
                
            }            
            return respuestaJson;
        }

    }

    @Override
    public String eliminarCargaCartera(Usuario u) {
        String respuestaJson="{}";
        con = null;
        String query = "BUSCAR_NEGOCIOS_CARTERA";

        try {
            
            con = this.conectarJNDI(query,u.getBd());
            con.setAutoCommit(false);
            Statement stmt = con.createStatement();

            stmt.addBatch("DELETE from administrativo.solicitud_aval_cc where procesado='N';");
            stmt.addBatch("DELETE from  administrativo.solicitud_persona_cc where procesado='N';");
            stmt.addBatch("DELETE from administrativo.solicitud_estudiante_cc where procesado='N' ;");
           
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            
            respuestaJson= "{\"respuesta\":\"OK\"}";
  
        } catch (SQLException ex) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"Lo sentimos algo salio mal al rechazar los negocios.\","
                    + "\"exception\":\"" + ex.getMessage() + "\"}";
                ex.printStackTrace();
            } catch (SQLException ex1) {
                Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                this.desconectar(con);
            } catch (SQLException e) {
                
            }            
            return respuestaJson;
        }
    }

    @Override
    public String crearDocumentosCartera(Usuario u) {
        con = null;
        ps = null;
        rs = null;
        String query = "GENERAR_DOCUMENTOS_CARTERA";
        String respuestaJson="{}";
        try {
            con = this.conectarJNDI(query,u.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, u.getLogin());
                rs = ps.executeQuery();

                if (rs.next()) {
                    respuestaJson = "{\"respuesta\":\"" + rs.getString("retorno") + "\"}";
                } else {
                    respuestaJson = "{\"error\":\"Lo sentimos no se pudo crear la cartera.\","
                            + "\"exception\":\" SELECT SP_liqNegociosTerceros (" + u.getLogin() + ") as retorno :=BAD\"}";
                }
            }
        } catch (SQLException e) {
            respuestaJson = "{\"error\":\"Lo sentimos algo salio mal al crear el negocio.\","
                    + "\"exception\":\" SELECT SP_liqNegociosTerceros(" + u.getLogin() + ") := " + e.getMessage() + "\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }
    }

}
