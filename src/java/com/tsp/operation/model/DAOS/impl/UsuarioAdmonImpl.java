/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.UsuarioAdmonDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author egonzalez
 */
public class UsuarioAdmonImpl extends MainDAO implements UsuarioAdmonDAO {

    public UsuarioAdmonImpl() {
        super("UsuarioAdmonDAO.xml","fintra");
    }
    
    public JsonObject buscar(String distrito, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query;
        JsonObject obj = new JsonObject();
        JsonArray arr; JsonObject aro;
        try {
            con = this.conectarBDJNDI(this.getDatabaseName());
            query = "GET_PERFILES_DISTRITO";
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            ps.setString(2, distrito);
            rs = ps.executeQuery();
            arr = new JsonArray();
            while(rs.next()) {
                aro = new JsonObject();
                aro.addProperty("cod", rs.getString("id_perfil"));
                aro.addProperty("nombre", rs.getString("nombre"));
                arr.add(aro);
            } obj.add("perfiles",arr);
        } catch(Exception exc) {
            obj = new JsonObject();
            obj.addProperty("mensaje", exc.getMessage());
        } finally {
             try {
                if (rs != null) { try { rs.close(); } catch (Exception e){} }
                if (ps != null) { try { ps.close(); } catch (Exception e){} }
                if (con != null) { try { this.desconectar(con); } catch (Exception e){} } 
            } catch (Exception ex) { }
            return obj;
        }     
    }
    
    public JsonObject crear(JsonObject info) {
        String query;
        JsonObject obj = new JsonObject();
        JsonArray arr = new JsonArray(), arp = new JsonArray();
        StringStatement ps = null;
        TransaccionService tService = null;
        String claveencr="",idusuario="", base="";
        
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            
            obj = info.get("basico").getAsJsonObject();
            claveencr = info.get("claveencr").getAsString();
            idusuario = obj.get("idusuario").getAsString();
            base = obj.get("base").getAsString();
            boolean ap = obj.get("app").getAsBoolean();
            
            /*************** Creacion de usuario ***************************/
            query = this.obtenerSQL("INSERT_USUARIO");
            ps = new StringStatement(query, true);
            ps.setString(1, obj.get("nombre").getAsString());
            ps.setString(2, obj.get("direccion").getAsString());
            ps.setString(3, obj.get("email").getAsString());
            ps.setString(4, obj.get("telefono").getAsString());
            ps.setString(5, obj.get("codpais").getAsString());
            ps.setString(6, obj.get("ciudad").getAsString());
            ps.setString(7, obj.get("nit").getAsString());
            ps.setString(8, obj.get("tipo").getAsString());
            ps.setString(9, obj.get("id_agencia").getAsString());
            ps.setString(10, claveencr);
            ps.setString(11, "FINV");
            ps.setString(12, base);
            ps.setString(13, obj.get("nits_propietario").getAsString());
            ps.setString(14, idusuario);
            ps.setBoolean(15, obj.get("cambio_clave").getAsBoolean());
            ps.setString(16, obj.get("depto").getAsString());
            tService.getSt().addBatch(ps.getSql());
            
           /*
                        -- Agregamos a la transaccion el query de agregar usuario de la app 
                        @Au tor: Boris Terraza G
                        -- Fecha: 2018-08-17
           */
                                
            if (ap) {          
                        query = this.obtenerSQL("SQL_AGREGAR_USUARIO_APP");
                        ps = new StringStatement(query, true);
                        ps.setString(1, obj.get("nombre").getAsString());
                        ps.setString(2, obj.get("nit").getAsString());
                        ps.setString(3, obj.get("email").getAsString());
                        ps.setString(4, idusuario);
                        ps.setString(5, claveencr);
                        ps.setString(6, idusuario);

                        tService.getSt().addBatch(ps.getSql());                    
                    } 
            
            
            /*
                -- Agregamos a la transaccion el query de agregar usuario al modulo de novedades
                @Au tor: Boris Terraza G
                -- Fecha: 2018-10-31
           */                 
                query = this.obtenerSQL("PERFIL_NOVEDADES");
                ps = new StringStatement(query, true);
                ps.setString(1, idusuario);
                tService.getSt().addBatch(ps.getSql());     
            
            /**************** Registro de proyectos *******************/
            arr = info.get("proyectos").getAsJsonArray();
            for (int i = 0; i < arr.size(); i++) {
                obj = (JsonObject) arr.get(i);
                
                query = this.obtenerSQL("ASIGNAR_PROYECTO_USUARIO");
                ps = new StringStatement(query, true);
                ps.setString(1, idusuario);
                ps.setString(2, info.get("usuario").getAsString());
                ps.setString(3, base);
                ps.setString(4, obj.get("distrito").getAsString());

                tService.getSt().addBatch(ps.getSql());
            
                /**************** Registro de perfiles *******************/
                arp = obj.get("perfiles").getAsJsonArray();
                query = this.obtenerSQL("ASIGNAR_PERFIL_USUARIO");
                for(int j = 0; j < arp.size(); j++) {
                    ps = new StringStatement(query,true);
                    ps.setString(1, "");
                    ps.setString(2, info.get("usuario").getAsString());
                    ps.setString(3, idusuario);
                    ps.setString(4, obj.get("distrito").getAsString());
                    ps.setString(5, arp.get(j).getAsJsonObject().get("codigo").getAsString());
                    
                    tService.getSt().addBatch(ps.getSql());
                }
            }
            
            tService.execute();
            obj = new JsonObject();
            obj.addProperty("mensaje", "�Creado con exito!");
        } catch(Exception exc) {
            obj = new JsonObject();
            obj.addProperty("mensaje", exc.getMessage());
        } finally {
             try {
                if (tService != null) { tService.closeAll(); }
            } catch (Exception ex) { }
            return obj;
        }     
    }
    
    public JsonObject Acturalizar(JsonObject info) {
        String query;
        JsonObject obj = new JsonObject();
        JsonArray arr = new JsonArray(), arp = new JsonArray();
        StringStatement ps = null;
        TransaccionService tService = null;
        String claveencr="",idusuario="", base="";
        
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            
            obj = info.get("basico").getAsJsonObject();
            claveencr = info.get("claveencr").getAsString();
            idusuario = obj.get("idusuario").getAsString();
            base = obj.get("base").getAsString();
            
            /*************** Creacion de usuario ***************************/
            query = this.obtenerSQL("UPDATE_USUARIO");
            ps = new StringStatement(query, true);
            ps.setString(1, obj.get("nombre").getAsString());
            ps.setString(2, obj.get("direccion").getAsString());
            ps.setString(3, obj.get("email").getAsString());
            ps.setString(4, obj.get("telefono").getAsString());
            ps.setString(5, obj.get("codpais").getAsString());
            ps.setString(6, obj.get("ciudad").getAsString());
            ps.setString(7, obj.get("nit").getAsString());
            ps.setString(8, obj.get("tipo").getAsString());
            ps.setString(9, obj.get("id_agencia").getAsString());
            ps.setString(10, claveencr);
            ps.setString(11, "FINV");
            ps.setString(12, base);
            ps.setString(13, obj.get("nits_propietario").getAsString());
            ps.setString(14, obj.get("estado").getAsString());
            ps.setBoolean(15, obj.get("cambio_clave").getAsBoolean());
            ps.setString(16, obj.get("depto").getAsString());
            ps.setString(17, idusuario);
            tService.getSt().addBatch(ps.getSql());
            
            /**************** Registro de proyectos *******************/
            arr = info.get("proyectos").getAsJsonArray();
            for (int i = 0; i < arr.size(); i++) {
                obj = (JsonObject) arr.get(i);
                if (obj.get("estado").getAsString().equalsIgnoreCase("N")) {
                    query = "ASIGNAR_PROYECTO_USUARIO";
                    ps = new StringStatement(this.obtenerSQL(query), true);
                    ps.setString(1, idusuario);
                    ps.setString(2, info.get("usuario").getAsString());
                    ps.setString(3, base);
                    ps.setString(4, obj.get("distrito").getAsString());
                } else {
                    query = "ACTUALIZAR_PROYECTO_USUARIO";
                    ps = new StringStatement(this.obtenerSQL(query), true);
                    ps.setString(1, obj.get("estado").getAsString());
                    ps.setString(2, base);
                    ps.setString(3, info.get("usuario").getAsString());
                    ps.setString(4, idusuario);
                    ps.setString(5, obj.get("distrito").getAsString());
                }

                tService.getSt().addBatch(ps.getSql());
            
                /**************** Registro de perfiles *******************/
                arp = obj.get("perfiles").getAsJsonArray();
                String estado = "";
                for(int j = 0; j < arp.size(); j++) {
                    estado = arp.get(j).getAsJsonObject().get("estado").getAsString();
                    if (estado.equalsIgnoreCase("N")) {
                        query = "ASIGNAR_PERFIL_USUARIO";
                        estado = "";
                    } else {
                        query = "ACTUALIZAR_PERFIL_USUARIO";
                    }
                            
                    ps = new StringStatement(this.obtenerSQL(query),true);
                    ps.setString(1, estado);
                    ps.setString(2, info.get("usuario").getAsString());
                    ps.setString(3, idusuario);
                    ps.setString(4, obj.get("distrito").getAsString());
                    ps.setString(5, arp.get(j).getAsJsonObject().get("codigo").getAsString());
                    
                    tService.getSt().addBatch(ps.getSql());
                }
            }
            
            tService.execute();
            obj = new JsonObject();
            obj.addProperty("mensaje", "�Usuario actualizado!");
        } catch(Exception exc) {
            obj = new JsonObject();
            obj.addProperty("mensaje", exc.getMessage());
        } finally {
             try {
                if (tService != null) { tService.closeAll(); }
            } catch (Exception ex) { }
            return obj;
        }     
    }

    @Override
    public JsonObject cargaInicial(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query;
        JsonObject obj = new JsonObject();
        JsonArray arr; JsonObject aro;
        try {
            con = this.conectarBDJNDI(this.getDatabaseName());
            query = "GET_EMPRESAS";
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            arr = new JsonArray();
            while(rs.next()) {
                aro = new JsonObject();
                aro.addProperty("estado", rs.getString("reg_status"));
                aro.addProperty("dstrct", rs.getString("dstrct"));
                aro.addProperty("nombre", rs.getString("description"));
                arr.add(aro);
            } obj.add("empresas",arr);
            
            query = "GET_PAISES";
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            arr = new JsonArray();
            while(rs.next()) {
                aro = new JsonObject();
                aro.addProperty("cod", rs.getString("cod"));
                aro.addProperty("nombre", rs.getString("nombre"));
                arr.add(aro);
            } obj.add("paises",arr);
            
            query = "GET_AGENCIAS";
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            arr = new JsonArray();
            while(rs.next()) {
                aro = new JsonObject();
                aro.addProperty("cod", rs.getString("cod"));
                aro.addProperty("nombre", rs.getString("nombre"));
                arr.add(aro);
            } obj.add("agencias",arr);
            
            if(!usuario.equalsIgnoreCase("")) {
                query = "GET_LOCACION_USUARIO";
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, usuario);
                rs = ps.executeQuery();
                aro = new JsonObject();
                if(rs.next()) {
                    aro.addProperty("cciu", rs.getString("cciu"));
                    aro.addProperty("nciu", rs.getString("nciu"));
                    aro.addProperty("cdpto", rs.getString("cdpto"));
                    aro.addProperty("ndpto", rs.getString("ndpto"));
                } obj.add("locacion",aro);
                
                query = "GET_PERFILES_USUARIO";
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, usuario);
                rs = ps.executeQuery();
                arr = new JsonArray();
                JsonArray arr2 = new JsonArray();
                JsonObject aro2 = new JsonObject();
                String control = "";
                
                while(rs.next()) {
                    if (!control.equalsIgnoreCase(rs.getString("dstrct"))) {
                        control = rs.getString("dstrct");
                        arr2 = new JsonArray();
                        aro2 = new JsonObject();
                        aro2.addProperty("dstrct", rs.getString("dstrct"));
                        aro2.addProperty("empresa", rs.getString("description"));
                        aro2.add("perfiles", arr2);
                        arr.add(aro2);
                    }
                    aro = new JsonObject();
                    aro.addProperty("perfil", rs.getString("perfil"));
                    aro.addProperty("nombre", rs.getString("nombre"));
                    aro.addProperty("estado", rs.getBoolean("status"));
                    arr2.add(aro);
                    
                } obj.add("perfiles",arr);
                
            }
            
            if(usuario.equalsIgnoreCase("")) {
                obj.addProperty("mensaje", "�Informacion obtenida para nuevo!");
            } else {
                obj.addProperty("mensaje", "�Informacion obtenida para consulta!");
            }
        } catch(Exception exc) {
            obj = new JsonObject();
            obj.addProperty("mensaje", exc.getMessage());
        } finally {
             try {
                if (rs != null) { try { rs.close(); } catch (Exception e){} }
                if (ps != null) { try { ps.close(); } catch (Exception e){} }
                if (con != null) { try { this.desconectar(con); } catch (Exception e){} } 
            } catch (Exception ex) { }
            return obj;
        }     
    }

    @Override
    public JsonObject cargarCombo(String query, String filtro) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        JsonArray arr; JsonObject aro;
        try {
            con = this.conectarBDJNDI(this.getDatabaseName());
            
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, filtro);
            rs = ps.executeQuery();
            arr = new JsonArray();
            while(rs.next()) {
                aro = new JsonObject();
                aro.addProperty("cod", rs.getString("cod"));
                aro.addProperty("nombre", rs.getString("nombre"));
                arr.add(aro);
            } obj.add("rows",arr);
            
        } catch(Exception exc) {
            obj = new JsonObject();
            obj.addProperty("mensaje", exc.getMessage());
        } finally {
             try {
                if (rs != null) { try { rs.close(); } catch (Exception e){} }
                if (ps != null) { try { ps.close(); } catch (Exception e){} }
                if (con != null) { try { this.desconectar(con); } catch (Exception e){} } 
            } catch (Exception ex) { }
            return obj;
        }
    }

    @Override
    public JsonObject buscarPerfilesFenalco(String distrito, String usuario) {
         Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query;
        JsonObject obj = new JsonObject();
        JsonArray arr; JsonObject aro;
        try {
            con = this.conectarBDJNDI(this.getDatabaseName());
            query = "GET_PERFILES_FENALCO";
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            ps.setString(2, distrito);
            rs = ps.executeQuery();
            arr = new JsonArray();
            while(rs.next()) {
                aro = new JsonObject();
                aro.addProperty("cod", rs.getString("id_perfil"));
                aro.addProperty("nombre", rs.getString("nombre"));
                arr.add(aro);
            } obj.add("perfiles",arr);
        } catch(Exception exc) {
            obj = new JsonObject();
            obj.addProperty("mensaje", exc.getMessage());
        } finally {
             try {
                if (rs != null) { try { rs.close(); } catch (Exception e){} }
                if (ps != null) { try { ps.close(); } catch (Exception e){} }
                if (con != null) { try { this.desconectar(con); } catch (Exception e){} } 
            } catch (Exception ex) { }
            return obj;
        }     
    }   
  
    @Override
    public String buscarAppm(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query;
        JsonObject obj = new JsonObject();
        JsonArray arr; JsonObject aro;
        String resp = "";
        try {
            con = this.conectarBDJNDI(this.getDatabaseName());
            query = "USUARIO_APP";
//            ps = con.prepareStatement(this.obtenerSQL(query).replace("#", "'%"+usuario+"%'"));
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            arr = new JsonArray();
            while(rs.next()) {
//                aro = new JsonObject();
//                aro.addProperty("cod", rs.getString("nombre"));
//                aro.addProperty("nombre", rs.getString("identificacion"));
//                arr.add(aro);
                resp = rs.getString("idusuario");
            } 
            
            if (resp.equals("")) {
                 resp = "Ok";
            }
           
        } catch(Exception exc) {
            obj = new JsonObject();
            obj.addProperty("mensaje", exc.getMessage());
        } finally {
             try {
                if (rs != null) { try { rs.close(); } catch (Exception e){} }
                if (ps != null) { try { ps.close(); } catch (Exception e){} }
                if (con != null) { try { this.desconectar(con); } catch (Exception e){} } 
            } catch (Exception ex) { }
            return resp;
        }     
    }
}
