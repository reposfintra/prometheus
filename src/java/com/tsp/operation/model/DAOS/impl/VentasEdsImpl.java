/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.VentasEdsDAO;
import com.tsp.operation.model.MenuOpcionesModulos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mcastillo
 */
public class VentasEdsImpl extends MainDAO implements VentasEdsDAO{

    
    public VentasEdsImpl() {
        super("VentasEdsDAO.xml");
    }
    
    public VentasEdsImpl(String dataBaseName) {
        super("VentasEdsDAO.xml",dataBaseName);
    }

    @Override
    public ArrayList<MenuOpcionesModulos> cargarMenuVentasEds(String usuario, String distrito) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OPCIONES_VENTAS_EDS";
        ArrayList<MenuOpcionesModulos> listaOpcionesVentasEds = null;

        try {
            String sql = "";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, "%" + usuario + "%");
            ps.setString(2, distrito);
            rs = ps.executeQuery();
            listaOpcionesVentasEds = new ArrayList();
            MenuOpcionesModulos opciones_venta_eds = null;

            while (rs.next()) {

                opciones_venta_eds = new MenuOpcionesModulos();
                opciones_venta_eds.setId(rs.getInt("id"));
                opciones_venta_eds.setDescripcion(rs.getString("descripcion"));
                opciones_venta_eds.setRuta(rs.getString("ruta"));
                opciones_venta_eds.setOrden(rs.getInt("orden"));
                listaOpcionesVentasEds.add(opciones_venta_eds);

            }

        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO MENU VENTAS EDS: cargarMenuVentasEds()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaOpcionesVentasEds;

    }

    @Override
    public String generarJsonGuardarVentaEds(int idEstacion, String nombre_eds, int idmanifiesto, String planilla, int kilometraje, String listadoProd) {
        Gson gson = new Gson();
        JsonArray arr = new JsonArray();
        JsonObject obj = new JsonObject();
        try {           
   
            JsonObject fila;
            
            obj.addProperty("id_estacion", idEstacion);
            obj.addProperty("nombre_eds", nombre_eds);
            obj.addProperty("id_manifiesto", idmanifiesto);
            obj.addProperty("planilla", planilla);
            obj.addProperty("kilometraje", kilometraje);
              
            JsonParser jsonParser = new JsonParser();
            JsonObject jo = (JsonObject) jsonParser.parse(listadoProd);
            JsonArray jsonArr = jo.getAsJsonArray("productos");
                       
            for (int i = 0; i < jsonArr.size(); i++){
                int idProducto = jsonArr.get(i).getAsJsonObject().get("id_producto").getAsJsonPrimitive().getAsInt();
                String nomproducto = jsonArr.get(i).getAsJsonObject().get("nombre_producto").getAsJsonPrimitive().getAsString();
                double precio = jsonArr.get(i).getAsJsonObject().get("precio_xunidad").getAsJsonPrimitive().getAsDouble();
                double cantidad = jsonArr.get(i).getAsJsonObject().get("cantidad").getAsJsonPrimitive().getAsDouble();
                double total = jsonArr.get(i).getAsJsonObject().get("total").getAsJsonPrimitive().getAsDouble(); 
                fila = new JsonObject();
                fila.addProperty("id_producto", idProducto);
                fila.addProperty("nombre_producto", nomproducto);
                fila.addProperty("precio_xunidad", precio);
                fila.addProperty("cantidad", cantidad);
                fila.addProperty("total", total);
                arr.add(fila);
            }
            obj.add("productos",arr);          
           
       
        } catch (Exception ex) {  
             throw new Exception("ERROR OBTENIENDO JSON GUARDAR VENTA EDS: generarJsonGuardarVentaEds()  " + ex.getMessage());
        }finally{
           return gson.toJson(obj);
        }
    }
    
}
