/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.aspose.cells.Cell;
import com.aspose.cells.CellValueType;
import com.aspose.cells.Cells;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import com.aspose.cells.Worksheets;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.operation.model.DAOS.AplicacionPagosMasivosDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.StringStatement;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author dvalencia
 */
public class AplicacionPagosMasivosImpl extends MainDAO implements AplicacionPagosMasivosDAO {

    private Object usuario;
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;

    public AplicacionPagosMasivosImpl(String dataBaseName) {
        super("AplicacionPagosMasivosDAO.xml", dataBaseName);
    }

    @Override
    public boolean leerArchivoPagoMasivo(InputStream excelFileStream, String usuario, String dstrct) throws IOException, SQLException {
        Connection conn = null;
        PreparedStatement psPagos = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        LinkedList pagos = new LinkedList();
        String[] datos = null;
        String serial="";
            
        Workbook workbook = new Workbook();
        workbook.open(excelFileStream);
        
        Worksheets sheets = workbook.getWorksheets();
        Worksheet base = sheets.getSheet("Base");
        Cells celdas = base.getCells();
        
        try {
            conn = this.conectarJNDI();
            conn.setAutoCommit(false);            
           
            ps = conn.prepareStatement(this.obtenerSQL("BUSCAR_SERIAL_LOTE_PAGOS"));
            rs = ps.executeQuery();
            if (rs.next()) {
                serial = rs.getString("serie");
            }
            
            psPagos = conn.prepareStatement(this.obtenerSQL("INSERTAR_TEM_PAGOS"));            

            for (int i = 2; i <= celdas.getMaxDataRow(); i++) {
                for (int j = 0; j <= celdas.getMaxDataColumn(); j++) {
                    Cell celdaActual = celdas.getCell(i, j);
                    switch (celdaActual.getValueType()) {
                        case CellValueType.STRING:
                        case CellValueType.RICH_TEXT_STRING:
                            psPagos.setString(j + 1, celdaActual.getStringValue().trim());
                            break;
                        case CellValueType.INT:
                            psPagos.setString(j + 1,  Integer.toString((int) celdaActual.getIntValue()));    
                            break;
                        case CellValueType.DOUBLE:
                                psPagos.setString(j + 1, Integer.toString((int) celdaActual.getIntValue()));                            
                            break;
                    }
                }
                psPagos.setString(11 ,usuario);
                psPagos.setString(12, serial);
                psPagos.addBatch();
            }
            
            try{
                psPagos.executeBatch();
                conn.commit();
                
                buscarLotePagos(serial, usuario);
//                
//                ps = conn.prepareStatement(this.obtenerSQL("BUSCAR_SERIAL_LOTE_PAGOS"));
//                rs = ps.executeQuery();
//                if (rs.next()) {
//                    serial = rs.getString("serie");
//                }
                
                
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                ex.getNextException();
            }            
            
                                   
            return true;
       } finally {
            if (psPagos != null) psPagos.close();
            if (ps != null) ps.close();
            if (conn != null) conn.close();
        }
    } 

    @Override
    public String CargarPagosMasivos(String usuario,String lote, String logica_aplicacion) {
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        query = "SQL_CARGAR_PAGOS_MASIVOS";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            ps.setString(2, lote);
            ps.setString(3, logica_aplicacion);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                
                fila = new JsonObject();
             
                fila.addProperty("negasoc", rs.getString("negasoc"));              
                fila.addProperty("nitcli", rs.getString("nitcli"));
                fila.addProperty("unidad_negocio", rs.getString("unidad_negocio"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("tipo_pago", rs.getString("tipo_pago"));
                fila.addProperty("fecha_pago", rs.getString("fecha_pago"));
                fila.addProperty("banco", rs.getString("banco"));
                fila.addProperty("sucursal", rs.getString("sucursal"));
                fila.addProperty("valor_aplicar_neto", rs.getString("valor_aplicar_neto"));
                fila.addProperty("usuario", rs.getString("usuario"));
                fila.addProperty("procesado", rs.getString("procesado"));
                fila.addProperty("creation_date", rs.getString("creation_date"));
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("lote_pago", rs.getString("lote_pago"));
                fila.addProperty("orden", rs.getString("orden"));
                fila.addProperty("extracto", rs.getString("extracto"));
                fila.addProperty("logica_aplicacion", rs.getString("logica_aplicacion"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String AplicarPagosMasivos(String usuario, String lote, String logica_aplicacion) {

        query = "SQL_MOTOR_CLASIFICACION";
        JsonObject response = new JsonObject();
        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            ps.setString(2, lote);
            ps.setString(3, logica_aplicacion);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("retorno");
            }

            switch (logica_aplicacion) {
                case "REFINANCIACION":
                    if (retorno.equals("TRUE")) {
                        query = this.obtenerSQL("BUSCAR_NEGOCIOS_POR_GENERAR_CARTERA");
                        ps = con.prepareStatement(query);
                        rs = ps.executeQuery();
                        while (rs.next()) {
                            String key_ref=rs.getString("key_ref");
                            String cod_neg = rs.getString("cod_neg");
                            
                            //consumimos la api para generar cartera
                            URL url = new URL("http://prometheus.fintra.co:8094/fintra/EndPointCoreServlet?option=7&negocio=" + cod_neg + "&user=" + usuario);
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestMethod("POST");
                            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                            if (conn.getResponseCode() != 200) {
                                throw new RuntimeException("Failed : HTTP Error code : "
                                        + conn.getResponseCode());
                            }
                            StringBuilder builder = new StringBuilder();
                            InputStreamReader in = new InputStreamReader(conn.getInputStream());
                            BufferedReader br = new BufferedReader(in);
                            String output;
                            while ((output = br.readLine()) != null) {
                                builder.append(output);
                            }
                            response = (JsonObject) new JsonParser().parse(builder.toString());

                            String resp = response.get("status").getAsString();

                            if (resp.equals("200")) {

                                query = this.obtenerSQL("UPDATE_CARTERA_GENERADA");
                                ps = con.prepareStatement(query);
                                ps.setString(1, cod_neg);
                                ps.setString(2, key_ref);
                                ps.executeUpdate();

                            }
                            conn.disconnect();
                            //Fin consumo api  para generar cartera
                        }

                        retorno = "OK";
                    }

                    break;
                case "ESTRATEGIA-CARTERA":

                    if (retorno.startsWith("LR")) {                        
                        query = this.obtenerSQL("BUSCAR_NEGOCIOS_POR_GENERAR_CARTERA");
                        ps = con.prepareStatement(query);
                        rs = ps.executeQuery();
                        while (rs.next()) {
                            String key_ref=rs.getString("key_ref");
                            String cod_neg = rs.getString("cod_neg");
                            String codNuevoNegocio = rs.getString("cod_nuevo_negocio");
                            boolean nuevoNeg = rs.getBoolean("negocio_nuevo");
                            int unidadNegocio = rs.getInt("unidad_negocio");
                            String urlapi = "http://prometheus.fintra.co:8094/fintra/EndPointCoreServlet";

                            switch (unidadNegocio) {
                                case 1:
                                    urlapi += nuevoNeg ? "?option=3&negocio=" + codNuevoNegocio + "&user=" + usuario : "?option=7&negocio=" + cod_neg + "&user=" + usuario;
                                    break;
                                case 31:
                                    urlapi += nuevoNeg ? "?option=8&negocio=" + codNuevoNegocio + "&user=" + usuario + "&tipo_liq=NORMAL" + "&diferido=true" : "?option=8&negocio=" + cod_neg + "&user=" + usuario + "&tipo_liq=REFINANCIACION" + "&diferido=true";
                                    break;
                                case 22:
                                    urlapi += nuevoNeg ? "?option=9&negocio=" + codNuevoNegocio + "&user=" + usuario : "?option=9&negocio=" + cod_neg + "&user=" + usuario;
                                    break;
                            }

                            URL url = new URL(urlapi);
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestMethod("POST");
                            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                            if (conn.getResponseCode() != 200) {
                                throw new RuntimeException("Failed : HTTP Error code : "
                                        + conn.getResponseCode());
                            }
                            StringBuilder builder = new StringBuilder();
                            InputStreamReader in = new InputStreamReader(conn.getInputStream());
                            BufferedReader br = new BufferedReader(in);
                            String output;
                            while ((output = br.readLine()) != null) {
                                builder.append(output);
                            }
                            response = (JsonObject) new JsonParser().parse(builder.toString());
                            String resp = response.get("status").getAsString();
                            if (resp.equals("200")) {
                                if (con.isClosed()){
                                    con=this.conectarJNDI();
                                }
                                query = this.obtenerSQL("UPDATE_CARTERA_GENERADA");
                                ps = con.prepareStatement(query);
                                ps.setString(1, cod_neg);
                                ps.setString(2, key_ref);
                                ps.executeUpdate();

                            }
                            conn.disconnect();
                        }

                        //ejecutamos el pago del lote.
                        query = this.obtenerSQL("APLICAR_PAGOS_MASIVOS_V3");
                        ps = con.prepareStatement(query);
                        ps.setString(1, usuario);
                        ps.setString(2, retorno); 
                        ps.setString(3, logica_aplicacion);
                        rs = ps.executeQuery();
                        if (rs.next()) {
                            retorno = rs.getString("retorno");
                        }

                    }
                    break;

            }

        } catch (IOException | RuntimeException | SQLException e) {
            retorno = "ERROR;" + e;
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String CargarComboLotes(String usuario) {
      query = "SQL_CARGAR_COMBOS_LOTES";        
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("lote_pago"), rs.getString("lote_pago"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String EliminarLotePagoMasivo(String usuario, String lote) {
        
         query = "ELIMINAR_LOTE_PAGO_MASIVO";
         String retorno = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            ps.setString(2, lote);
            ps.executeUpdate();
            retorno = "OK";

        } catch (Exception e) {
            e.printStackTrace();
            retorno = e.getMessage();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                retorno = e.getMessage();
            }
        }
        return retorno;
        
    }
    
    /**
     *
     * @param lote
     * @param usuario
     * @return boolean
     */
    @Override
    public boolean buscarLotePagos(String lote, String usuario){
        
        query = "OBTENER_MOTOR_CLASIFICACION";
        boolean decision=false;
        String marca ="";
                
        try{
            
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, lote);
            ps.setString(2, usuario);
            ps.executeQuery();
            decision=true;
                        
        }catch (SQLException e) {            
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {                
            }
        }
        return decision;
        
    }
    
    /**
     * 
     * @param usuario
     * @return String
     */
    @Override
    public String CargarComboLogicaApli(String usuario,String lote) {
      query = "SQL_CARGAR_COMBOS_LOGICA_APLICACION";        
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            ps.setString(2, lote);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("logica_aplicacion"), rs.getString("logica_aplicacion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
    
    @Override
    public String CierrePlanAlDia(String usuario) {
        
         query = "ACTUALIZA_PLAN_AL_DIA";
         String retorno = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            rs=ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("retorno");
            }

        } catch (Exception e) {
            e.printStackTrace();
            retorno = e.getMessage();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                retorno = e.getMessage();
            }
        }
        return retorno;
        
    }

     @Override
    public String ReclasificarLotes(String usuario, String lote) {
        query = "RECLASIFICAR_LOTE_PAGOS";
         String retorno = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, lote);
            ps.setString(2, usuario);
            rs=ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("resp");
            }

        } catch (Exception e) {
            e.printStackTrace();
            retorno = e.getMessage();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                retorno = e.getMessage();
            }
        }
        return retorno;
    }

    
    @Override
    public String resumenArchivoAsobancaria(String usuario,String idArchivo) {
         
         query = "CLASIFICAR_REFERENCIAS_PAGOS";
         String retorno = "";
          String resul="";
        try {
            con = this.conectarJNDI();
            
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            ps.setString(2, idArchivo);
            rs=ps.executeQuery();
            
             if (rs.next()) {                 
              resul=  rs.getString("resultado");
             }
            
             if (resul.equals("OK")){
                 
                query = "RESUMEN_ARCHIVO_ASOBANCARIA";
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, idArchivo);
                rs=ps.executeQuery();
                if (rs.next()) {

                    String id_archivo= rs.getString("id_archivo");
                    String fecha_recaudo= rs.getString("fecha_recaudo");
                    String fecha_carga= rs.getString("fecha_carga");
                    String total_pagos= rs.getString("total_pagos");
                    String valor_recaudo= rs.getString("valor_recaudo");
                    String pagosEncontrados= rs.getString("pagosEncontrados");
                    String valorpagosEncontrados= rs.getString("valorpagosEncontrados");
                    String PagosInconsistentes= rs.getString("PagosInconsistentes");
                    String valorPagosInconsistentes= rs.getString("valorPagosInconsistentes");
                    String banco= rs.getString("banco");
                    String branch_code= rs.getString("branch_code");
                    String bank_account_no= rs.getString("bank_account_no");
                    String creation_user= rs.getString("creation_user");
                    String flag= rs.getString("flag");
                    String total_comision= rs.getString("total_comision");

                 retorno = "{\"respuesta\":\"OK\",\"id_archivo\":\""+id_archivo+"\",\"fecha_recaudo\":\"" + fecha_recaudo + "\",\"fecha_carga\":\""+fecha_carga+"\""
                         + ",\"total_pagos\":\""+total_pagos+"\",\"valor_recaudo\":\""+valor_recaudo+"\",\"pagosEncontrados\":\""+pagosEncontrados+"\",\"valorpagosEncontrados\":\""+valorpagosEncontrados+"\",\"valorPagosInconsistentes\":\""+valorPagosInconsistentes+"\","
                         + "\"PagosInconsistentes\":\""+PagosInconsistentes+"\",\"banco\":\""+banco+"\",\"branch_code\":\""+branch_code+"\",\"bank_account_no\":\""+bank_account_no+"\",\"usuario\":\""+creation_user+"\",\"flag\":\""+flag+"\",\"total_comision\":\""+total_comision+"\"}" ;


                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            retorno = e.getMessage();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                retorno = e.getMessage();
            }
        }
        return retorno;
       
    }

    @Override
    public String cargarArchivoGenerarPago(String usuario) {
        query = "ARCHIVO_POR_GENERAR_LOTES";    
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("id"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    
        }

    @Override
    public String mostrarRefeInconsistencia(String idArchivo) {
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        
         query = "CARGAR_REFE_CON_INCONSISTENCIAS";
        try {
             con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idArchivo);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                
                fila = new JsonObject();
                
                fila.addProperty("id", rs.getString("id"));              
                fila.addProperty("orden", rs.getString("orden"));              
                fila.addProperty("clasificacion", rs.getString("clasificacion"));
                fila.addProperty("referencia", rs.getString("referencia"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("extracto", rs.getString("extracto"));
                fila.addProperty("valor_recaudo", rs.getString("valor_recaudo"));
                fila.addProperty("banco", rs.getString("banco"));
                fila.addProperty("branch_code", rs.getString("branch_code"));
                fila.addProperty("bank_account_no", rs.getString("bank_account_no"));
                fila.addProperty("condicional", rs.getString("condicional"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
           e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj); 
    }

    @Override
    public String generarLotePagos(String usuario,String idArchivo) {
      query = "GENERAR_LOTE_PAGOS";
         String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            ps.setString(2, idArchivo);
            rs=ps.executeQuery();
            if (rs.next()) {
                respuesta = "{\"respuesta\":\"" + rs.getString("retorno") + "\"}";
            }
            
           
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = e.getMessage();
            }
        }
        return respuesta;
    }

    @Override
    public String updateRefrenciaNegocio(String idArchivo, String idNegocio, String idOrden, String idRows, String login) {
       query = "UPDATE_REFERENCIA_NEGOCIOS";
         String respuesta = "{\"respuesta\":\"La referencia no fue actualizada\"}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idNegocio);
            ps.setString(2, login);
            ps.setString(3, idArchivo);
            ps.setString(4, idRows);
            ps.setString(5, idOrden);
            ps.setString(6, idNegocio);
           
            
            if (ps.executeUpdate()>=1) {
                respuesta = "{\"respuesta\":\"OK\"}";
            }
           
        } catch (Exception e) {
           respuesta = "{\"respuesta\":\"Exception [AplicacionPagosMasivos]updateRefrenciaNegocio \"}";
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = e.getMessage();
            }
        }
        return respuesta;
    }

    @Override
    public String mostrarLogReferenciaPagos(String idArchivo) {
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        
         query = "CARGAR_LOG_CLASIFICACION_PAGOS";
        try {
             con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idArchivo);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                
                fila = new JsonObject();
                
                fila.addProperty("id", rs.getString("id"));              
                fila.addProperty("orden", rs.getString("orden"));              
                fila.addProperty("clasificacion", rs.getString("clasificacion"));
                fila.addProperty("referencia", rs.getString("referencia"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("extracto", rs.getString("extracto"));
                fila.addProperty("valor_recaudo", rs.getString("valor_recaudo"));
                fila.addProperty("banco", rs.getString("banco"));
                fila.addProperty("branch_code", rs.getString("branch_code"));
                fila.addProperty("bank_account_no", rs.getString("bank_account_no"));
                fila.addProperty("condicional", rs.getString("condicional"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
           e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj); 
    }
}
