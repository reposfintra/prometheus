/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.ProcesosMetaDAO;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.ProcesoMeta;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author desarrollo
 */
public class ProcesosMetaImpl extends MainDAO implements ProcesosMetaDAO {
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;
    
     public ProcesosMetaImpl(String dataBaseName){
         super("ProcesosMeta.xml", dataBaseName);
    
    }

    @Override
    public ArrayList<ProcesoMeta> cargarProcesosMeta(String empresa, String status) {
        con = null; rs = null; ps = null;
        ArrayList<ProcesoMeta> lista = new ArrayList<ProcesoMeta>();
        query = "cargarProcesosMeta";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setString(2, status);
            rs = ps.executeQuery();
            
            while (rs.next()){
                ProcesoMeta pro = new ProcesoMeta();
                pro.setId(rs.getInt("id"));               
                pro.setNombre(rs.getString("meta_proceso"));
                pro.setDescripcion(rs.getString("descripcion"));
                lista.add(pro);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosMeta " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public ArrayList<CmbGeneralScBeans> cargarComboEmpresa() {
        con = null; rs = null; ps = null;
        ArrayList<CmbGeneralScBeans> lista = new ArrayList<CmbGeneralScBeans>();
        query = "cargarComboEmpresa";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                CmbGeneralScBeans cbo = new CmbGeneralScBeans();
                cbo.setIdCmb(rs.getInt("id"));
                cbo.setDescripcionCmb(rs.getString("descripcion"));
                
                lista.add(cbo);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarComboEmpresa " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public boolean existeMetaProceso(String empresa, String descripcion) {
        con = null; rs = null; ps = null;
        boolean resp = false;
        query = "existeMetaProceso";
        String filtro="";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);;
            
            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setString(2, descripcion);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeMetaProceso " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return resp;
    }

    @Override
    public String guardarMetaProceso(String empresa, String nombre, String descripcion, String usuario) {
        con = null; ps = null;
        query = "guardarMetaProceso";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario);
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;  
       }
    }

    @Override
    public String actualizarMetaProceso(String empresa, String nombre, String descripcion, String idProceso, String usuario) {
        con = null; ps = null;
        query = "actualizarMetaProceso";
        String  respuestaJson = "{}";
        
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario);
            ps.setInt(5, Integer.parseInt(idProceso));       
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson; 
       }
    }

    @Override
    public String cargarComboProcesoMeta(String empresa) {
        con = null; rs = null; ps = null;        
        query = "cargarComboProcesoMeta";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
           
            ps = con.prepareStatement(query);           
            ps.setString(1, empresa);
            rs = ps.executeQuery();
            
             while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(obj);

        }catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return respuestaJson;
    }

    @Override
    public ArrayList<ProcesoMeta> cargarProcesoInterno() {
        con = null; rs = null; ps = null;
        ArrayList<ProcesoMeta> lista = new ArrayList<ProcesoMeta>();
        query = "cargarProcesoInterno";
        String filtro=" pin.reg_status !='A'";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                ProcesoMeta pro = new ProcesoMeta();
                pro.setId(rs.getInt("id"));
                pro.setNombre(rs.getString("nombre"));
                pro.setDescripcion(rs.getString("descripcion"));
                pro.setId_tabla_rel(rs.getInt("id_proceso_meta"));
                pro.setDescripcionTablaRel(rs.getString("meta_proceso"));
                
                lista.add(pro);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesoInterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public boolean existeProcesoInterno(String procesoMeta, String descripcion) {
        con = null; rs = null; ps = null;
        boolean resp = false;
        query = "existeProcesoInterno";
        String filtro="";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(procesoMeta));
            ps.setString(2, descripcion);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeProcesoInterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return resp;
    }

    @Override
    public String guardarProcesoInterno(String procesoMeta, String nombre, String descripcion, String usuario, String empresa) {
        con = null; ps = null;
        query = "guardarProcesoInterno";
        String  respuestaJson = "{}";
        int resp=0, idProInterno=0;
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, Integer.parseInt(procesoMeta));
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario);
            ps.setString(5, empresa);
              
            resp = ps.executeUpdate();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idProInterno = rs.getInt(1);                
                }
            }
            respuestaJson = "{\"respuesta\":\"OK\",\"idProceso\":\"" + idProInterno + "\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\",\"idProceso\":\"0\"}";
                throw new SQLException("ERROR guardarProcesoInterno \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;  
       }
    }

    @Override
    public int obtenerIdProcesoInterno(String nomProceso, String idProMeta) {
        con = null; rs = null; ps = null;
        int id = 0;
        query = "obtenerIdProcesoInterno";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nomProceso);
            ps.setInt(2, Integer.parseInt(idProMeta));
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                id = rs.getInt(1);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en obtenerIdProcesoInterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return id;
    }

    @Override
    public String insertarRelUnidadProInterno(int idProInterno, String IdUnid, String empresa) {
        con = null; st = null;
        String sql = "";
        query = "insertarRelUnidadProInterno";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);
            st.setInt(1, idProInterno);
            st.setInt(2, Integer.parseInt(IdUnid));
            st.setString(3, empresa);
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {
                throw new Exception("Error en insertarRelUnidadProInterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
         return sql;
    }

    @Override
    public ArrayList<UnidadesNegocio> cargarUndNegocioProinterno(String idProinterno) {
       con = null; rs = null; ps = null;
        ArrayList<UnidadesNegocio> lista = new ArrayList<UnidadesNegocio>();
        query = "cargarUndNegocioProinterno";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(idProinterno));
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                UnidadesNegocio uneg = new UnidadesNegocio();
                uneg.setId(rs.getInt("id"));
                uneg.setDescripcion(rs.getString("descripcion"));
                
                lista.add(uneg);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarUndNegocioProinterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public String actualizarProcesoInterno(int idProinterno, String nombre, String descripcion, int idProMeta, String usuario, String empresa) {
        con = null; ps = null;
        query = "actualizarProcesoInterno";
        String  respuestaJson = "{}";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setInt(3, idProMeta);        
            ps.setString(4, usuario);
            ps.setString(5, empresa);
            ps.setInt(6, idProinterno);
           
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarProcesoInterno \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;  
       }
    }

    @Override
    public String eliminarUndProinterno(String idProinterno, String idUnidad) {
        con = null; st = null;
        query = "eliminarUndProinterno";
        String  sql = "";
        
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(idProinterno));
            st.setInt(2, Integer.parseInt(idUnidad));
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR eliminarUndProinterno \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return sql;
       }
    }

    @Override
    public ArrayList<Usuario> listarUsuario() {
        con = null; rs = null; ps = null;
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        query = "listarUsuario";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                Usuario user = new Usuario();
                user.setCodUsuario(rs.getInt("codigo_usuario"));
                user.setNombre(rs.getString("nombre"));
                user.setIdusuario(rs.getString("idusuario"));
                                
                lista.add(user);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuario " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public ArrayList<ProcesoMeta> listarProinternoUsuario(int codUsuario) {
        con = null; rs = null; ps = null;
        ArrayList<ProcesoMeta> lista = new ArrayList<ProcesoMeta>();
        query = "listarProinternoUsuario";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, codUsuario);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                ProcesoMeta pro = new ProcesoMeta();
                pro.setId(rs.getInt("id"));
                pro.setId_tabla_rel(rs.getInt("id_proceso_meta"));
                pro.setTipo(rs.getString("tipo"));
                pro.setDescripcion(rs.getString("descripcion"));
                
                lista.add(pro);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuario " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public ArrayList<ProcesoMeta> listarProinterno() {
        con = null; rs = null; ps = null;
        ArrayList<ProcesoMeta> lista = new ArrayList<ProcesoMeta>();
        query = "listarProinterno";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                ProcesoMeta pro = new ProcesoMeta();
                pro.setId(rs.getInt("id"));
                pro.setId_tabla_rel(rs.getInt("id_proceso_meta"));
                pro.setTipo(rs.getString("tipo"));
                pro.setDescripcion(rs.getString("descripcion"));
                
                lista.add(pro);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuario " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public String insertarRelProInternoUser(String idProInt, int codUsuario, String login, String empresa) {
        con = null; st = null;
        String sql = "";
        query = "insertarRelProInternoUser";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(idProInt));
            st.setInt(2, codUsuario);
            st.setString(3, login);
            st.setString(4, empresa);
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {
                throw new Exception("Error en insertarRelProInternoUser " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
         return sql;
    }
    
    @Override
    public ArrayList<ProcesoMeta> cargarProcesoInterno(int idMetaProceso, String status) {
        con = null; rs = null; ps = null;
        ArrayList<ProcesoMeta> lista = new ArrayList<ProcesoMeta>();
        query = "cargarProcesoInterno";
        String filtro=" pin.reg_status !='"+status+"' and pin.id_proceso_meta="+idMetaProceso;
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                ProcesoMeta pro = new ProcesoMeta();
                pro.setId(rs.getInt("id"));
                pro.setNombre(rs.getString("nombre"));
                pro.setDescripcion(rs.getString("descripcion"));
                pro.setId_tabla_rel(rs.getInt("id_proceso_meta"));
                pro.setDescripcionTablaRel(rs.getString("meta_proceso"));
                
                lista.add(pro);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesoInterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    
    @Override
    public ArrayList<UnidadesNegocio> cargarUnidadesNegocio(int idProceso) {
        con = null; rs = null; ps = null;
        ArrayList<UnidadesNegocio> lista = new ArrayList<UnidadesNegocio>();
        query = "cargarUnidadesNegocios";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, idProceso);
            rs = ps.executeQuery();
            
            while (rs.next()){
                UnidadesNegocio uneg = new UnidadesNegocio();
                uneg.setId(rs.getInt("id"));
                uneg.setCodigo(rs.getString("cod"));
                uneg.setDescripcion(rs.getString("descripcion"));
                uneg.setCiudad(rs.getString("ciudad"));
                uneg.setCodCentralRiesgo(rs.getString("cod_central_riesgo"));
                
                lista.add(uneg);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarUnidadesNegocio " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    
    @Override
    public String anularMetaProceso(int idProceso) {
        con = null; ps = null;
        query = "anularMetaProceso";
        Statement stmt = null;
        String  respuestaJson = "{}";
         
        try{
            con = this.conectarJNDI(query);
            con.setAutoCommit(false);
            stmt = con.createStatement();

            stmt.addBatch(this.generarQueryCambiaEstadoProceso("rel_unidadnegocio_procinterno", "A", " where id_proceso_interno in(select id from proceso_interno where id_proceso_meta="+idProceso+")",  "DEL"));
            stmt.addBatch(this.generarQueryCambiaEstadoProceso("rel_proceso_interno_usuario", "A", " where id_proceso_interno in(select id from proceso_interno where id_proceso_meta="+idProceso+")", "DEL"));
            stmt.addBatch(this.generarQueryCambiaEstadoProceso("proceso_interno", "A", " where id_proceso_meta="+idProceso,"UPD"));
            stmt.addBatch(this.generarQueryCambiaEstadoProceso("proceso_meta", "A", " where id="+idProceso,"UPD"));
            
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();                 
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                con.rollback();
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR anularMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
           return respuestaJson;
       }
    }

    
    @Override
    public String anularProcesoInterno(int idProinterno) {
        con = null; ps = null;
        Statement stmt = null;
        query = "anularProcesoInterno";
        String  respuestaJson = "{}";
           
        try{
            con = this.conectarJNDI(query);           
            con.setAutoCommit(false);
            stmt = con.createStatement();

            stmt.addBatch(this.generarQueryCambiaEstadoProceso("rel_unidadnegocio_procinterno", "A", " where id_proceso_interno="+idProinterno,  "DEL"));
            stmt.addBatch(this.generarQueryCambiaEstadoProceso("rel_proceso_interno_usuario", "A", " where id_proceso_interno="+idProinterno, "DEL"));
            stmt.addBatch(this.generarQueryCambiaEstadoProceso("proceso_interno", "A", " where id="+idProinterno,"UPD"));
          

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                con.rollback();
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR anularProcesoInterno \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
       }
    }
    
    public String generarQueryCambiaEstadoProceso(String tabla, String estado, String where, String action) {
       
        String  query = "";           
        try{           
            if (action.equals("UPD")){
                query = "UPDATE " + tabla + " set reg_status='"+estado+"'" + where;
            }
            if (action.equals("DEL")){
                query = "DELETE FROM " + tabla + where;
            }
            
        }catch (Exception e) {
              Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, e);           
        } finally {
              return query;
       }
    }
    
    @Override
    public ArrayList<Usuario> listarUsuariosProinterno(int idProceso) {
        con = null; rs = null; ps = null;
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        query = "listarUsuariosProinterno";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, idProceso);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                Usuario user = new Usuario();
                user.setCodUsuario(rs.getInt("codigo_usuario"));
                user.setNombre(rs.getString("nombre"));
                user.setIdusuario(rs.getString("idusuario"));
                user.setModerador(rs.getString("moderador"));              
                lista.add(user);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuariosProinterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }
    
    @Override
    public ArrayList<Usuario> listarUsuariosRelProInterno(int idProceso) {
        con = null; rs = null; ps = null;
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        query = "listarUsuariosRelProInterno";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, idProceso);
             
            rs = ps.executeQuery();
            
            while (rs.next()){
                Usuario user = new Usuario();
                user.setCodUsuario(rs.getInt("codigo_usuario"));
                user.setNombre(rs.getString("nombre"));
                user.setIdusuario(rs.getString("idusuario"));
                                
                lista.add(user);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuariosRelProInterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }
    
    @Override
    public String eliminarUsuarioProinterno(int idProinterno, String idUsuario) {
        con = null; st = null;
        query = "eliminarUsuarioProinterno";
        String sql = "";
          
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);           
            st.setInt(1, idProinterno);
            st.setInt(2, Integer.parseInt(idUsuario));
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {              
                throw new SQLException("ERROR eliminarUsuarioProinterno \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return sql;
       }
    }
    
    @Override
    public boolean existenUsuariosRelProceso(int idProceso, String tipo) {
        con = null; rs = null; ps = null;
        boolean resp = false;
        String filtro = "";
        query = "existenUsuariosRelProceso";
        try{
            filtro = (tipo.equals("META")) ? "id_proceso_interno in(select id from proceso_interno where id_proceso_meta=" + idProceso +")" : "id_proceso_interno=" + idProceso;
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
                      
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existenUsuariosRelProceso " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return resp;
    }
    
    @Override
    public boolean existeMetaProceso(String empresa, String descripcion, int idProceso) {
        con = null; rs = null; ps = null;
        boolean resp = false;
        query = "existeMetaProceso";
        String filtro=" and id not in("+idProceso+")";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);;
            
            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setString(2, descripcion);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeMetaProceso " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return resp;
    }

     
    @Override
    public boolean existeProcesoInterno(String procesoMeta, String descripcion, int idProceso) {
        con = null; rs = null; ps = null;
        boolean resp = false;
        query = "existeProcesoInterno";
        String filtro=" and id not in("+idProceso+")";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(procesoMeta));
            ps.setString(2, descripcion);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeProcesoInterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return resp;
    }


    @Override
    public String activarMetaProceso(int idProceso) {
        con = null; ps = null;
        query = "anularMetaProceso";
        Statement stmt = null;
        String  respuestaJson = "{}";
         
        try{
            con = this.conectarJNDI(query);
            con.setAutoCommit(false);
            stmt = con.createStatement();
            
            stmt.addBatch(this.generarQueryCambiaEstadoProceso("proceso_interno", "", " where id_proceso_meta="+idProceso,"UPD"));
            stmt.addBatch(this.generarQueryCambiaEstadoProceso("proceso_meta", "", " where id="+idProceso,"UPD"));
            
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();                 
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                con.rollback();
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
           return respuestaJson;
       }
    }
    
    @Override
    public String activarProcesoInterno(int idProinterno) {
        con = null; ps = null;
        Statement stmt = null;
        query = "activarProcesoInterno";
        String  respuestaJson = "{}";
           
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, idProinterno);
            
            ps.executeUpdate();                
          
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {            
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activarProcesoInterno \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
       }
    }
    
    
        
    @Override
    public String actualizarModerador(int idProceso, int idusuario, String moderador) {
        con = null; ps = null;
        Statement stmt = null;
        query = "actualizarModerador";
        String  respuestaJson = "{}";
           
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, moderador);
            ps.setInt(2, idProceso);
            ps.setInt(3, idusuario);
           
            
            ps.executeUpdate();                
          
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {            
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarModerador \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
       }
    }
    
    @Override
    public ArrayList<Usuario> listarUsuariosRelProInterno() {
        con = null; rs = null; ps = null;
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        query = "listarUsuariosRelProInterno1";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                Usuario user = new Usuario();
                user.setCodUsuario(rs.getInt("id_usuario"));
                user.setIdusuario(rs.getString("login"));
                                
                lista.add(user);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuariosRelProInterno1 " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }
    
    @Override
    public ArrayList<ProcesoMeta> cargarProcesosRelUsuario(String idusuario) {
        con = null; rs = null; ps = null;
        ArrayList<ProcesoMeta> lista = new ArrayList<ProcesoMeta>();
        query = "listarProcesosRelUsuarios";
        String filtro="";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setInt(1,Integer.parseInt(idusuario));
            rs = ps.executeQuery();
            
            while (rs.next()){
                ProcesoMeta pro = new ProcesoMeta();
                pro.setId(rs.getInt("id"));            
                pro.setDescripcion(rs.getString("descripcion"));
                pro.setId_tabla_rel(rs.getInt("id_proceso_meta"));
                pro.setTipo(rs.getString("tipo"));
                lista.add(pro);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosRelUsuario " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }

    @Override
    public ArrayList<ProcesoMeta> cargarTipoRequisicionRelUsuario(String idusuario) {
        con = null; rs = null; ps = null;
        ArrayList<ProcesoMeta> lista = new ArrayList<ProcesoMeta>();
        query = "listarTiposReqRelUsuarios";
        String filtro="";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro",filtro);
            
            ps = con.prepareStatement(query);
            ps.setInt(1,Integer.parseInt(idusuario));
            rs = ps.executeQuery();
            
            while (rs.next()){
                ProcesoMeta pro = new ProcesoMeta();
                pro.setId(rs.getInt("id"));            
                pro.setDescripcion(rs.getString("descripcion"));    
                pro.setTipo(rs.getString("tipo"));
                lista.add(pro);
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarTipoRequisicionRelUsuario " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return lista;
    }
    
    
    @Override
    public String eliminarRelProcesoReqUser(int idUsuario) {
      
        con = null; st = null;   
        String query = "eliminaRelProcesoReqUser";        
        String sql = "";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);            
            st = new StringStatement(query, true); 
            st.setInt(1, idUsuario);
            sql = st.getSql();  
            
        } catch (SQLException ex) {           
            throw new Exception("Error en eliminarRelProcesoReqUser(int idUsuario) " + ex.getMessage());
        } finally {  
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {                   
                }
            }
            return sql;  
        }
    }
  
    
    @Override
    public String insertarRelProcesoReqUser(String idProceso, int codUsuario, String login, Usuario usuario) {
        con = null; st = null;
        String sql = "";
        query = "insertarRelProcesoReqUser";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);
            st.setString(1, usuario.getDstrct());
            st.setInt(2, Integer.parseInt(idProceso));
            st.setInt(3, codUsuario);
            st.setString(4, login);
            st.setString(5, usuario.getLogin());
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {
                throw new Exception("Error en insertarRelProcesoReqUser " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
         return sql;
    }
    
    @Override
    public String eliminarRelTipoReqUser(int idUsuario) {
      
        con = null; st = null;   
        String query = "eliminaRelTipoReqUser";        
        String sql = "";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);            
            st = new StringStatement(query, true); 
            st.setInt(1, idUsuario);
            sql = st.getSql();  
            
        } catch (SQLException ex) {           
            throw new Exception("Error en eliminarRelTipoReqUser(int idUsuario) " + ex.getMessage());
        } finally {  
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {                   
                }
            }
            return sql;  
        }
    }
     
    @Override
    public String insertarRelTipoReqUser(String idTipoReq, int codUsuario, String login, Usuario usuario) {
        con = null; st = null;
        String sql = "";
        query = "insertarRelTipoReqUser";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);
            st.setString(1, usuario.getDstrct());
            st.setInt(2, Integer.parseInt(idTipoReq));
            st.setInt(3, codUsuario);
            st.setString(4, login);
            st.setString(5, usuario.getLogin());
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {
                throw new Exception("Error en insertarRelTipoReqUser " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosMetaImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
         return sql;
    }

  


}
    
   
    

