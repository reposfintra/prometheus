/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itextpdf.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.operation.model.DAOS.NegociosFintraDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.ConceptoFactura;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.VariablesRefinanciacion;
import com.tsp.util.Util;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

/**
 *
 * @author mcamargo
 */
public class NegociosFintraImpl extends MainDAO implements NegociosFintraDAO{

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;
    private boolean hidden=true;

    public NegociosFintraImpl(String dataBaseName) {
        super("NegociosFintraDAO.xml", dataBaseName);
    }

    @Override
    public String cargarNegociosReliquidar() {

        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        query = "SQL_CARGAR_NEGOCIOS_RELIQUIDAR";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                //C.NOMCLI,N.COD_CLI,P.PAYMENT_NAME,SA.AFILIADO,SA.NUMERO_SOLICITUD,N.ID_CONVENIO,S.NOMBRE,N.NRO_DOCS,N.VR_DESEMBOLSO
                fila.addProperty("NOMBRE", rs.getString("NOMBRE"));
                fila.addProperty("COD_CLI", rs.getString("COD_CLI"));
                fila.addProperty("PAYMENT_NAME", rs.getString("PAYMENT_NAME"));
                fila.addProperty("AFILIADO", rs.getString("AFILIADO"));
                fila.addProperty("NUMERO_SOLICITUD", rs.getString("NUMERO_SOLICITUD"));
                fila.addProperty("ID_CONVENIO", rs.getString("ID_CONVENIO"));
                fila.addProperty("NOMBRE", rs.getString("NOMBRE"));
                fila.addProperty("NRO_DOCS", rs.getString("NRO_DOCS"));
                fila.addProperty("VR_DESEMBOLSO", rs.getInt("VR_DESEMBOLSO"));
                fila.addProperty("TIPO_CARRERA", rs.getString("TIPO_CARRERA"));
                fila.addProperty("COD_NEG", rs.getString("COD_NEG"));
                fila.addProperty("ESTADO_NEG", rs.getString("ESTADO_NEG"));
                fila.addProperty("ID_UNID_NEGOCIO", rs.getString("ID_UNID_NEGOCIO"));
                fila.addProperty("FECHA", rs.getString("FECHA"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }
    @Override
    public String cargarLiquidacion(String negocio,String valor,String cuota,String convenio,String fechacuota,String identificacion) {

        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        query = "SQL_CARGAR_RELIQUIDACION";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, negocio);
            ps.setString(2, valor);
            ps.setString(3, cuota);
            ps.setString(4, fechacuota);
            ps.setString(5, convenio);
            ps.setString(6, identificacion);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {


                fila = new JsonObject();
                // FECHA::DATE,ITEM,SALDO_INICIAL,CAPITAL,INTERES,CUSTODIA,REMESA,SEGURO,CUOTA_MANEJO,VALOR,SALDO_FINAL
                fila.addProperty("FECHA", rs.getString("FECHA"));
                fila.addProperty("ITEM", rs.getString("ITEM"));
                fila.addProperty("SALDO_INICIAL", rs.getString("SALDO_INICIAL"));
                fila.addProperty("CAPITAL", rs.getString("CAPITAL"));
                fila.addProperty("INTERES", rs.getString("INTERES"));
                fila.addProperty("CUSTODIA", rs.getString("CUSTODIA"));
                fila.addProperty("REMESA", rs.getString("REMESA"));
                fila.addProperty("SEGURO", rs.getString("SEGURO"));
                fila.addProperty("CUOTA_MANEJO", rs.getInt("CUOTA_MANEJO"));
                fila.addProperty("VALOR", rs.getString("VALOR"));
                fila.addProperty("SALDO_FINAL", rs.getString("SALDO_FINAL"));
                fila.addProperty("COD_NEG", rs.getString("COD_NEG"));
                fila.addProperty("VALOR_AVAL", rs.getString("VALOR_AVAL"));
                fila.addProperty("DIAS", rs.getString("DIAS"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String insert_document_neg_aceptados(String negocio,String valor,String cuota,String convenio,String fechacuota, String usuario,String identificacion) {

        String respuesta = "";


        query = "SQL_INSERT_RELIQUIDACION";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, negocio);
            ps.setString(2, valor);
            ps.setString(3, cuota);
            ps.setString(4, fechacuota);
            ps.setString(5, convenio);
            ps.setString(6, usuario);
            ps.setString(7, identificacion);
            rs = ps.executeQuery();
            if (rs.next()){
             respuesta=rs.getString("RESPUESTA");

            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return respuesta;

    }

    @Override
    public String validar_negocio_activo(String numero_solicitud) {
        con = null;
        ps = null;
        rs = null;

        String respuesta = "";
        String result = "";

        query = "SQL_VALIDAR_NEGOCIO_ACTIVO";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, numero_solicitud);
            rs = ps.executeQuery();
            if (rs.next()){
             result=rs.getString("fecha_pago");
             respuesta = "{\"respuesta\":\""+result+"\"}";
            }else{
               respuesta = "{\"respuesta\":\""+Util.getFechahoy()+"\"}";
            }


        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "{\"respuesta\":\"" + e + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                respuesta = "{\"respuesta\":\"" + ex + "\"}";
            }
        }
        return respuesta;

    }

    @Override
    public String CargarReportesSaldoCartera(String periodo, String unidad_negocio) {

        JsonObject obj = new JsonObject();


        Gson gson = new Gson();
        query = "SQL_REPORTE_ACT_SALDO_CARTERA";
        try{
            con = this.conectarJNDI();

            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, periodo);
            ps.setString(2, unidad_negocio);
            System.out.println(ps.toString());
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("INTERMEDIARIO", rs.getString("INTERMEDIARIO"));
                fila.addProperty("NIT", rs.getString("NIT"));
                fila.addProperty("COD_NEG", rs.getString("COD_NEG"));
                fila.addProperty("SALDO_CAPITAL_CORTE", rs.getString("SALDO_CAPITAL_CORTE"));
                fila.addProperty("TOTAL_OBLICACION", rs.getString("TOTAL_OBLIGACION"));
                fila.addProperty("FECHA_CORTE", rs.getString("FECHA_CORTE"));
                fila.addProperty("CUOTAS_PENDIENTES", rs.getString("CUOTAS_PENDIENTES"));
                fila.addProperty("INICIO_MORA", rs.getString("INICIO_MORA"));
                fila.addProperty("FECHA_DE_CANCELACION", rs.getString("FECHA_DE_CANCELACION"));
                fila.addProperty("ESTADO", rs.getString("ESTADO"));
                fila.addProperty("NUM_PAGARE", rs.getString("NUM_PAGARE"));
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }

    @Override
    public String CargarReporteNegociosNuevos(String fechaini, String fechafin, String unidad_negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String filtro="";
        JsonObject obj = new JsonObject();
        if(unidad_negocio.equals("31")){
        filtro="AND F_DESEM::date BETWEEN '"+fechaini+"'::date AND '"+fechafin+"'::date AND U.ID_UNID_NEGOCIO='"+unidad_negocio+"' AND SPC.TIPO IN ('C','E')";

        }else{
         filtro="AND F_DESEM::date BETWEEN '"+fechaini+"'::date AND '"+fechafin+"'::date AND U.ID_UNID_NEGOCIO='"+unidad_negocio+"'";
        }
        Gson gson = new Gson();
        String query = "SQL_CARGAR_REPORTE_NEGOCIOS_NUEVOS";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            System.out.println(ps.toString());
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("INTERMEDIARIO", rs.getString("INTERMEDIARIO"));
                fila.addProperty("COD_SUCURSAL", rs.getString("COD_SUCURSAL"));
                fila.addProperty("TITULAR", rs.getString("TITULAR"));
                fila.addProperty("TIPO_IDENTIFICACION", rs.getString("TIPO_IDENTIFICACION"));
                fila.addProperty("CEDULA_TITULAR", rs.getString("CEDULA_TITULAR"));
                fila.addProperty("GENERO_TITULAR", rs.getString("GENERO_TITULAR"));
                fila.addProperty("DIRECCION_TITULAR", rs.getString("DIRECCION_TITULAR"));
                fila.addProperty("COD_MUNICIPIO", rs.getString("COD_MUNICIPIO"));
                fila.addProperty("TEL_TITULAR", rs.getString("TEL_TITULAR"));
                fila.addProperty("TELEFONO2_TITULAR", rs.getString("TELEFONO2_TITULAR"));
                fila.addProperty("FAX_TITULAR", rs.getString("FAX_TITULAR"));
                fila.addProperty("COD_CI", rs.getString("COD_CI"));
                fila.addProperty("COD_NEG", rs.getString("COD_NEG"));
                fila.addProperty("NUM_PAGARE", rs.getString("NUM_PAGARE"));
                fila.addProperty("MONEDA", rs.getString("MONEDA"));
                fila.addProperty("VALOR_MONTO", rs.getString("VALOR_MONTO"));
                fila.addProperty("FECHA_DESEMBOLSO", rs.getString("FECHA_DESEMBOLSO"));
                fila.addProperty("NRO_DOCS", rs.getString("NRO_DOCS"));
                fila.addProperty("FECHA_VENCIMIENTO", rs.getString("FECHA_VENCIMIENTO"));
                fila.addProperty("PERIODO_GRACIA", rs.getString("PERIODO_GRACIA"));
                fila.addProperty("TIPO_CARTERA", rs.getString("TIPO_CARTERA"));
                fila.addProperty("TIPO_INVERSION", rs.getString("TIPO_INVERSION"));
                fila.addProperty("TIPO_RECURSO", rs.getString("TIPO_RECURSO"));
                fila.addProperty("VALOR_REDESCUENTO", rs.getString("VALOR_REDESCUENTO"));
                fila.addProperty("PORC_REDESCUENTO", rs.getString("PORC_REDESCUENTO"));
                fila.addProperty("NIT_ENTIDAD_REDESCUENTO", rs.getString("NIT_ENTIDAD_REDESCUENTO"));
                fila.addProperty("CONVENIO", rs.getString("CONVENIO"));
                fila.addProperty("PRODUCTO_GARANTIA", rs.getString("PRODUCTO_GARANTIA"));
                fila.addProperty("PORCENTAJE_AVAL", rs.getString("PORCENTAJE_AVAL"));
                fila.addProperty("RESPONSABLE", rs.getString("RESPONSABLE"));
                fila.addProperty("DESCRIP_INTERMEDIARIO", rs.getString("DESCRIP_INTERMEDIARIO"));
                fila.addProperty("IDENTIFICACION", rs.getString("IDENTIFICACION"));
                fila.addProperty("TIPO_IDENTIFICACION_CODEUDOR", rs.getString("TIPO_IDENTIFICACION_CODEUDOR"));
                fila.addProperty("CODEUDOR", rs.getString("CODEUDOR"));
                fila.addProperty("DIRECCION", rs.getString("DIRECCION"));
                fila.addProperty("COD_MUNICIPIO", rs.getString("COD_MUNICIPIO"));
                fila.addProperty("CELULAR", rs.getString("CELULAR"));
                fila.addProperty("TELEFONO2", rs.getString("TELEFONO2"));
                fila.addProperty("CAMPO_RESERVADO", rs.getString("CAMPO_RESERVADO"));
                fila.addProperty("CAMPO_RESERVADO2", rs.getString("CAMPO_RESERVADO2"));
                fila.addProperty("ACTIVIDAD_ECONOMICA", rs.getString("ACTIVIDAD_ECONOMICA"));
                fila.addProperty("COD_MUNICIPIO", rs.getString("COD_MUNICIPIO"));
                fila.addProperty("PAYMENT_NAME", rs.getString("PAYMENT_NAME"));
                fila.addProperty("TIENDA_SUCURSAL", rs.getString("TIENDA_SUCURSAL"));
                fila.addProperty("ESTRATO", rs.getString("ESTRATO"));
                fila.addProperty("FECHA_NACIMIENTO", rs.getString("FECHA_NACIMIENTO"));
                fila.addProperty("ESTADO_CIVIL", rs.getString("ESTADO_CIVIL"));
                datos.add(fila);
            }

           obj.add("rows", datos);
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }

    @Override
    public String Cargar_Combo_Aseguradoras() {

        query = "SQL_CARGAR_ASEGURADORAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("nit"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;

    }

    @Override
    public String Cargar_Combo_Polizas() {

        query = "SQL_CARGAR_POLIZAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;

    }

    @Override
    public String Cargar_Cxp_Aseguradoras(String nit_aseguradora, String id_poliza, String periodo) {
       JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        query = "SQL_CARGAR_CXP_ASEGURADORAS";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nit_aseguradora);
            ps.setString(2, id_poliza);
            ps.setString(3, periodo);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {


                fila = new JsonObject();

                fila.addProperty("nit", rs.getString("nit"));
                fila.addProperty("aseguradora", rs.getString("aseguradora"));
                fila.addProperty("poliza", rs.getString("poliza"));
                fila.addProperty("documento", rs.getString("documento"));
                fila.addProperty("fecha_vencimiento", rs.getString("fecha_vencimiento"));
                fila.addProperty("documento_relacionado", rs.getString("documento_relacionado"));
                fila.addProperty("vlr_neto", rs.getString("vlr_neto"));
                fila.addProperty("saldo_cartera", rs.getString("saldo_cartera"));
                fila.addProperty("cuota", rs.getInt("cuota"));
                fila.addProperty("capital", rs.getDouble("valor_capital"));
                fila.addProperty("valor_iva", rs.getDouble("valor_iva"));
                fila.addProperty("valor", rs.getDouble("valor"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String Generar_Cxp_consolidada_Aseguradora(String[] ArrayCxp, String login,String aseguradora, String poliza) {

            query = "SQL_GENERAR_CXP_CONSOLIDADA_ASEGURADORA";

        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setArray(1, con.createArrayOf("text", ArrayCxp));
            ps.setString(2, login);
            ps.setString(3, aseguradora);
            ps.setString(4, poliza);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("retorno");
            }

        } catch (Exception e) {

            retorno = "ERROR;"+e;
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String Cargar_combo_cajas() {
       query = "SQL_CARGAR_CAJAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("table_code"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String Cargar_detalle_cxc_caja(String caja, String fecha) {
       JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        query = "SQL_CARGAR_DETALLE_CXC_CAJA";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, caja);
            ps.setString(2, fecha);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();
//                i.tipo_documento,i.num_ingreso,i.nitcli,c.nomcli,i.fecha_consignacion,
//                   i.fecha_ingreso,i.bank_account_no,i.descripcion_ingreso,i.vlr_ingreso,i.creation_user

                fila.addProperty("tipo_documento", rs.getString("tipo_documento"));
                fila.addProperty("num_ingreso", rs.getString("num_ingreso"));
                fila.addProperty("nitcli", rs.getString("nitcli"));
                fila.addProperty("nomcli", rs.getString("nomcli"));
                fila.addProperty("fecha_consignacion", rs.getString("fecha_consignacion"));
                fila.addProperty("fecha_ingreso", rs.getString("fecha_ingreso"));
                fila.addProperty("branch_code", rs.getString("branch_code"));
                fila.addProperty("bank_account_no", rs.getString("bank_account_no"));
                fila.addProperty("descripcion_ingreso", rs.getString("descripcion_ingreso"));
                fila.addProperty("creation_user", rs.getString("creation_user"));
                fila.addProperty("vlr_ingreso", rs.getString("vlr_ingreso"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String Generar_cxc_caja_recaudo(String[] ArrayCxp, String login, String caja,String fecha) {

          query = "SQL_GENERAR_CXC_CAJA_RECAUDO";

        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setArray(1, con.createArrayOf("text", ArrayCxp));
            ps.setString(2, login);
            ps.setString(3, caja);
            ps.setString(4, fecha);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("retorno");
            }

        } catch (Exception e) {

            retorno = "ERROR;"+e;
            e.printStackTrace();
            e.toString();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String cargar_solicitudes_reasignar(String usuario) {
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        query = "SQL_CARGAR_SOLICITUDES_REASIGNAR";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {


                fila = new JsonObject();

                fila.addProperty("numero_solicitud", rs.getString("numero_solicitud"));
                fila.addProperty("identificacion", rs.getString("identificacion"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("asesor", rs.getString("asesor"));
                fila.addProperty("monto_credito", rs.getString("monto_credito"));
                fila.addProperty("convenio", rs.getString("convenio"));
                fila.addProperty("estado_sol", rs.getString("estado_sol"));
                fila.addProperty("creation_date", rs.getString("creation_date"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String cargar_combo_asesores() {
       query = "SQL_CARGAR_COMBO_ASESORES";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("idusuario"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String update_asesores(String[] ArraySolicitudes, String asesor, String login) {
        query = "SQL_UPDATE_ASESORES";

        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setArray(1, con.createArrayOf("text", ArraySolicitudes));
            ps.setString(2, login);
            ps.setString(3, asesor);
            rs=ps.executeQuery();

            if (rs.next()) {
                retorno = rs.getString("retorno");
            }

        } catch (Exception e) {

            retorno = "ERROR;"+e;
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String cargar_negocios_refinanciar(String tipo_busqueda, String documento, String tipo_refi) {
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        String filtro;

        if (tipo_busqueda.equals("ide")) {
            filtro = "f.nit='" + documento + "'";

        } else {
            filtro = "f.negasoc='" + documento + "'";
        }
        query = "SQL_CARGAR_NEGOCIO_REFINANCIAR";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            ps = con.prepareStatement(query);
            ps.setString(1,tipo_refi);
            ps.setString(2,tipo_refi);
            ps.setString(3,tipo_refi);
            ps.setString(4,tipo_refi);
            ps.setString(5,tipo_refi);
            ps.setString(6,tipo_refi);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();

                fila.addProperty("indentificacion", rs.getString("indentificacion"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("valor_negocio", rs.getString("valor_negocio"));
                fila.addProperty("total_cuotas", rs.getString("total_cuotas"));
                fila.addProperty("agencia", rs.getString("agencia"));
                fila.addProperty("valor_cuota", rs.getString("valor_cuota"));
                fila.addProperty("dias_mora", rs.getString("dias_mora"));
                fila.addProperty("periodicidad", rs.getBoolean("periodicidad"));
                fila.addProperty("aplica_inicial", rs.getBoolean("aplica_inicial"));
                fila.addProperty("aplica_proyeccion", rs.getBoolean("aplica_proyeccion"));
                fila.addProperty("fecha_proyeccion", rs.getString("fecha_proyeccion"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
           e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String cargar_detalle_cartera_refinanciar(String negocio,String tipo_refi,String fecha) {
      JsonObject obj = new JsonObject();
        Gson gson = new Gson();

        query = "SQL_CARGAR__DETALLE_CARTERA_REFINANCIAR";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, negocio);
            ps.setString(2, tipo_refi);
            ps.setString(3, fecha);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();

                fila.addProperty("documento", rs.getString("documento"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("cuota", rs.getString("cuota"));
                fila.addProperty("fecha_vencimiento", rs.getString("fecha_vencimiento"));
                fila.addProperty("dias_mora", rs.getString("dias_mora"));
                fila.addProperty("esquema", rs.getString("esquema"));
                fila.addProperty("estado", rs.getString("estado"));
                fila.addProperty("valor_saldo_capital", rs.getString("valor_saldo_capital"));
                fila.addProperty("valor_saldo_mi", rs.getString("valor_saldo_mi"));
                fila.addProperty("valor_saldo_ca", rs.getString("valor_saldo_ca"));
                fila.addProperty("valor_cm", rs.getString("valor_cm"));
                fila.addProperty("valor_seguro", rs.getString("valor_seguro"));
                fila.addProperty("valor_saldo_cuota", rs.getString("valor_saldo_cuota"));
                fila.addProperty("valor_capital_aval", rs.getString("valor_capital_aval"));
                fila.addProperty("valor_interes_aval", rs.getString("valor_interes_aval"));
                fila.addProperty("IxM", rs.getString("IxM"));
                fila.addProperty("GaC", rs.getString("GaC"));
                fila.addProperty("suma_saldos", rs.getString("suma_saldos"));
                datos.add(fila);
            }

            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String ver_cartera_refinanciar(String negocio, String valor_refinanciar, String tipo_refi) {
      JsonObject obj = new JsonObject();
        Gson gson = new Gson();

        query = "SQL_VER_CARTERA_REFINANCIAR";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, negocio);
            ps.setString(2, tipo_refi);
            ps.setString(3, valor_refinanciar);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();

                fila.addProperty("documento", rs.getString("documento"));
                fila.addProperty("cod_neg", rs.getString("cod_neg"));
                fila.addProperty("item", rs.getString("item"));
                fila.addProperty("fecha", rs.getString("fecha"));
                fila.addProperty("capital", rs.getString("capital"));
                fila.addProperty("interes", rs.getString("interes"));
                fila.addProperty("cat", rs.getString("cat"));
                fila.addProperty("cuota_manejo", rs.getString("cuota_manejo"));
                fila.addProperty("valor_cuota", rs.getString("valor_cuota"));
                fila.addProperty("dias_vencidos", rs.getString("dias_vencidos"));
                fila.addProperty("estado", rs.getString("estado"));
                fila.addProperty("color", rs.getString("color"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String proyeccion_refinanciacion_negocios(VariablesRefinanciacion refinanciacion,Usuario usuario) {
         String respuesta = "";
         String url="";

        try {
            //Guardamos la negociacion
            con = this.conectarJNDI();
            query = this.obtenerSQL("SQL_GUARDAR_FINANCIACION");
            ps = con.prepareStatement(query);

            ps.setString(1, refinanciacion.getNegocio());
            ps.setString(2, refinanciacion.getTipoRefi());
            ps.setDouble(3, refinanciacion.getPagoInicial());
            ps.setString(4, usuario.getLogin());
            ps.setInt(5, refinanciacion.getPlazo());
            ps.setString(6, refinanciacion.getFechaPrimeraCuota());
            ps.setString(7, refinanciacion.getCiudad());
            ps.setString(8, refinanciacion.getCompraCartera());
            ps.setInt(9, refinanciacion.getCuota_inicio());
            ps.setBoolean(10, refinanciacion.isIsPago());
            ps.setString(11, refinanciacion.getFechaProyeccion());
            ps.setDouble(12, refinanciacion.getInteres());
            ps.setDouble(13, refinanciacion.getCatVencido());
            ps.setDouble(14, refinanciacion.getCuotaAdmon());
            ps.setDouble(15, refinanciacion.getIntMora());
            ps.setDouble(16, refinanciacion.getGastoCobranza());
            ps.setDouble(17, refinanciacion.getSaldoCat());
            ps.setDouble(18, refinanciacion.getCapitalRefinanciacion());
            ps.setDouble(19, refinanciacion.getTotal_a_pagar());
            ps.setInt(20, refinanciacion.getPeriodo_pago_inicial());
            ps.setString(21, refinanciacion.getObservacion());
            ps.setLong(22, refinanciacion.getCelular());

            rs = ps.executeQuery();

            if (rs.next()) {
                respuesta = rs.getString("rs");
            }

        } catch (SQLException e) {
            return "{\"success\":false,\"data\":\"" + e.getMessage() + "\",\"url\":\""+url+"\",\"status\":\"200\"}" ;
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return "{\"success\":true,\"data\":\"" + respuesta + "\",\"url\":\"" + url + "\",\"status\":\"200\"}";
    }

    public synchronized Usuario validarUserToken(String token) {
        con = null;
        ps = null;
        rs = null;
        String query = "SQL_BUSCAR_USUARIO_SESION";
        Usuario usuario = null;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, token);
                rs = ps.executeQuery();
                if (rs.next()) {
                    usuario = new Usuario();
                    usuario.setNombre(rs.getString("nombre"));
                    usuario.setEmail(rs.getString("email"));
                    usuario.setLogin(rs.getString("idusuario"));
                    usuario.setBd("fintra");
                }
            }
        } catch (SQLException e) {
            usuario = null;
        } finally {
            this.desconectarPool(con, rs, ps, null, NegociosFintraImpl.class);
        }

        return usuario;
    }

    @Override
    public String buscar_fecha_refi(String negocio,String tipo_refi,String fecha) {
      query = "SQL_BUSCAR_FECHA_PAGO_REFI";
        String respuesta = "{}";

        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            ps.setString(2, tipo_refi);
            ps.setString(3, fecha);
            rs = ps.executeQuery();
            if (rs.next()) {

              respuesta="{\"success\":true,\"fecha_pago\":\"" + rs.getString("fecha_pago") +"\","
                      + "\"cuota_inicio\":\"" + rs.getString("cuota_inicio") +"\","
                      + "\"ciudad\":\"" + rs.getString("ciudad") +"\","
                      + "\"compra_cartera\":\"" + rs.getString("compra_cartera") +"\","
                      + "\"tasa\":\"" + rs.getString("tasa") +"\","
                      + "\"saldo_vencido\":\"" + rs.getString("saldo_vencido") +"\"}";
            }

        } catch (Exception ex) {
            respuesta ="{\"success\":false,\"data\":\"" +ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"success\":false,\"data\":\"" +e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String ver_cartera_refinanciar_refi(String valor_refinanciar, String no_cuotas, String fecha, String ciudad, String compra_cartera, String cuota_inicio,String negocio) {
       JsonObject obj = new JsonObject();
        Gson gson = new Gson();

        query = "SQL_VER_CARTERA_REFINANCIAR_REFI";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, valor_refinanciar);
            ps.setString(2, no_cuotas);
            ps.setString(3, fecha);
            ps.setString(4, ciudad);
            ps.setString(5, compra_cartera);
            ps.setString(6, cuota_inicio);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();

                fila.addProperty("item", rs.getString("item"));
                fila.addProperty("fecha", rs.getString("fecha"));
                fila.addProperty("capital", rs.getString("capital"));
                fila.addProperty("interes", rs.getString("interes"));
                fila.addProperty("cat", rs.getString("cat"));
                fila.addProperty("cuota_manejo", rs.getString("cuota_manejo"));
                fila.addProperty("valor_cuota", rs.getString("valor_cuota"));
                fila.addProperty("saldo_inicial", rs.getString("saldo_inicial"));
                fila.addProperty("saldo_final", rs.getString("saldo_final"));
                fila.addProperty("cod_neg", negocio);
                fila.addProperty("valor_aval",  rs.getString("valor_aval"));
                fila.addProperty("capital_aval",  rs.getString("capital_aval"));
                fila.addProperty("interes_aval",  rs.getString("interes_aval"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);

    }

    @Override
    public String buscar_negocios_aprobar_refi(String documento, String tipo_busqueda, String estado, Usuario  user) {
       JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        String filtro="";
        String filtroEstado= " AND cr.estado='"+estado+"'";
        if(!documento.equals("")){
            if (tipo_busqueda.equals("negocio")){
             filtro=" AND cr.cod_neg='"+documento+"'";
            }else if(tipo_busqueda.equals("ide")) {
             filtro=" AND n.cod_cli='"+documento+"'";
            }
            filtroEstado="";
        }

        query = "SQL_BUSCAR_NEGS_APROBAR_REFI";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro).replaceAll("#estado", filtroEstado);
            ps = con.prepareStatement(query);
            ps.setString(1, user.getLogin());
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();
                fila.addProperty("cod_neg", rs.getString("cod_neg"));
                fila.addProperty("estado", rs.getString("estado"));
                fila.addProperty("cod_cli", rs.getString("cod_cli"));
                fila.addProperty("nomcli", rs.getString("nomcli"));
                fila.addProperty("porc_negociacion", rs.getString("porc_negociacion"));
                fila.addProperty("valor_a_pagar", rs.getString("valor_a_pagar"));
                fila.addProperty("fecha_pago_primera_cuota", rs.getString("fecha_pago_primera_cuota"));
                fila.addProperty("departamento", rs.getString("departamento"));
                fila.addProperty("plazo", rs.getString("plazo"));
                fila.addProperty("pago", rs.getString("pago"));
                fila.addProperty("creation_date", rs.getString("creation_date"));
                fila.addProperty("creation_user", rs.getString("creation_user"));
                fila.addProperty("key_ref", rs.getString("key_ref"));
                fila.addProperty("fecha_vencimiento_acuerdo", rs.getString("fecha_vencimiento_acuerdo"));
                fila.addProperty("estrategia", rs.getString("nombre"));
                fila.addProperty("intxmora", rs.getString("intxmora"));
                fila.addProperty("gac", rs.getString("gasto_cobranza"));
                fila.addProperty("interes_corrientes", rs.getString("interes"));
                fila.addProperty("cat_vencido", rs.getString("cat_vencido"));
                fila.addProperty("capital_a_refinanciar", rs.getString("capital_a_refinanciar"));
                fila.addProperty("saldo_cat", rs.getString("saldo_cat"));
                fila.addProperty("cuota_admin", rs.getString("cuota_admin"));
                fila.addProperty("valor_cuota_actual", rs.getString("valor_cuota_actual"));
                fila.addProperty("periodos_pago_inicial", rs.getString("periodos_pago_inicial"));
                fila.addProperty("aprobador", rs.getString("aprobador"));
                fila.addProperty("observacion", rs.getString("observacion"));
                fila.addProperty("observacion_simulacion", rs.getString("observacion_simulacion"));
                fila.addProperty("sms_enviado", rs.getBoolean("sms_enviado"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String buscar_liquidacion_negocios_aprobar_refi(String negocio) {
         JsonObject obj = new JsonObject();
        Gson gson = new Gson();

        query = "SQL_VER_LIQUIDACION_NEGOCIO_POR_REFI";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();

                fila.addProperty("item", rs.getString("item"));
                fila.addProperty("fecha", rs.getString("fecha"));
                fila.addProperty("capital", rs.getString("capital"));
                fila.addProperty("interes", rs.getString("interes"));
                fila.addProperty("cat", rs.getString("cat"));
                fila.addProperty("cuota_manejo", rs.getString("cuota_manejo"));
                fila.addProperty("seguro", rs.getString("seguro"));
                fila.addProperty("capital_aval", rs.getString("capital_aval"));
                fila.addProperty("interes_aval", rs.getString("interes_aval"));
                fila.addProperty("valor_cuota", rs.getString("valor_cuota"));
                fila.addProperty("saldo_inicial", rs.getString("saldo_inicial"));
                fila.addProperty("saldo_final", rs.getString("saldo_final"));
                fila.addProperty("cod_neg", rs.getString("cod_neg"));
                fila.addProperty("valor_aval",  rs.getString("valor_aval"));
                fila.addProperty("capital_aval",  rs.getString("capital_aval"));
                fila.addProperty("interes_aval",  rs.getString("interes_aval"));
                fila.addProperty("color",  rs.getString("color"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String aprobar_refinanciacion(Usuario usuario, String negocio, String tipo_refi, String key_ref, String fecha_vencimiento_acuerdo, String observacion) {
        String respuesta = "{}";
        String retorno = "";

        try {
            con = this.conectarJNDI();
            //Aprobamos la negociacion de cartera
            query = "SQL_APROBAR_REFINANCIACION";
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, usuario.getLogin());
            ps.setString(3, observacion);
            ps.setString(4, key_ref);
            int update = ps.executeUpdate();
            if (update > 0) {
                respuesta = "{\"success\":true,\"data\":\"" + true + "\"}";
            }
            //buscamos la unidad de negocio del ls estrategia
            JsonObject unidadNegocioEstrategia = getUnidadNegocioEstrategia(key_ref);
            if (unidadNegocioEstrategia != null) {
                switch (unidadNegocioEstrategia.get("unidad_negocio").getAsInt()) {
                    case 1:
                        if (unidadNegocioEstrategia.get("es_retanqueo").getAsBoolean()) {
                            respuesta = this.aplicarEstrategiaManual(con, unidadNegocioEstrategia.get("unidad_negocio").getAsInt(), key_ref, observacion, usuario);
                        }
                        break;
                    case 22:
                        respuesta = this.aplicarEstrategiaManual(con, unidadNegocioEstrategia.get("unidad_negocio").getAsInt(), key_ref, observacion, usuario);
                        break;
                }
            } else {
                respuesta = "{\"success\":false,\"data\":\"Lo sentimos no encontro la unidad de negocio de la estrategia\"}";
            }

        } catch (SQLException ex) {
            respuesta = "{\"success\":false,\"data\":\"Ha ocurrido un error interno favor comunicar con la linea de soporte\"}";
        } catch (MalformedURLException ex) {
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            desconectarPool(con, rs, ps, null, NegociosFintraImpl.class);
        }
        return respuesta;
    }

    @Override
    public String cargarComboUnidadNegocio(String usuario) {
        query = "CARGAR_COMBO_UNIDAD_NEGOCIO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (SQLException ex) {
            respuesta = "{\"error\":\"NegociosFintraImpl en el metodo: [cargarComboUnidadNegocio]\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarReporteNegociosCartera(String id_unidad_negocio, String id_entidad_recaudo) {
       JsonObject obj = new JsonObject();
        Gson gson = new Gson();

        query = "CARGAR_REPORTE_NEGOCIOS";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_entidad_recaudo);
            ps.setString(2, id_unidad_negocio);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();
                fila.addProperty("empresa_responsable", rs.getString("empresa_responsable"));
                fila.addProperty("periodo_carga", rs.getString("periodo_carga"));
                fila.addProperty("estado", rs.getString("estado"));
                fila.addProperty("cedula", rs.getString("cedula"));
                fila.addProperty("nombre_cliente", rs.getString("nombre_cliente"));
                fila.addProperty("direccion_domicilio", rs.getString("direccion_domicilio"));
                fila.addProperty("ciudad_domicilio", rs.getString("ciudad_domicilio"));
                fila.addProperty("barrio_domicilio", rs.getString("barrio_domicilio"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("celular", rs.getString("celular"));
                fila.addProperty("direccion_negocio", rs.getString("direccion_negocio"));
                fila.addProperty("barrio_negocio", rs.getString("barrio_negocio"));
                fila.addProperty("telefon_negocio", rs.getString("telefon_negocio"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("unidad_negocio", rs.getString("unidad_negocio"));
                fila.addProperty("agencia", rs.getString("agencia"));
                fila.addProperty("altura_mora_actual", rs.getString("altura_mora_actual"));
                fila.addProperty("dia_pago", rs.getString("dia_pago"));
                fila.addProperty("saldo_actual", rs.getString("saldo_actual"));
                fila.addProperty("valor_saldo_vencido", rs.getString("valor_saldo_vencido"));
                fila.addProperty("interes_mora", rs.getString("interes_mora"));
                fila.addProperty("gac", rs.getString("gac"));
                fila.addProperty("total_a_pagar", rs.getString("total_a_pagar"));
                fila.addProperty("juridica", rs.getString("juridica"));
                fila.addProperty("reestructuracion", rs.getString("reestructuracion"));
                fila.addProperty("fecha_ult_compromiso", rs.getString("fecha_ult_compromiso"));
                fila.addProperty("responsable_cuenta", rs.getString("responsable_cuenta"));

                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String buscarCarteraPorAsignar(String id_unidad_negocio, String id_agencia, String id_altura_mora, String id_responsable_cuenta,String id_ciudad,String id_barrio) {
         JsonObject obj = new JsonObject();
        Gson gson = new Gson();

        query = "CARGAR_NEGOCIOS_POR_ASIGNAR";
        String filtro="";
        try {
            if (!id_responsable_cuenta.equals("")) {
                filtro=filtro+" AND t.responsable_cuenta='"+id_responsable_cuenta+"'";
            }
            if (!id_ciudad.equals("")) {
                filtro=filtro+" AND t.codciu='"+id_ciudad+"'";
            }
            if (!id_barrio.equals("")) {
                filtro=filtro+" AND t.barrio_domicilio='"+id_barrio+"'";
            }
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            ps = con.prepareStatement(query);
            ps.setString(1, id_unidad_negocio);
            ps.setString(2, id_agencia);
            ps.setString(3, id_altura_mora);
            System.out.println(ps.toString());
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();

                fila.addProperty("cedula", rs.getString("cedula"));
                fila.addProperty("nombre_cliente", rs.getString("nombre_cliente"));
                fila.addProperty("direccion_domicilio", rs.getString("direccion_domicilio"));
                fila.addProperty("ciudad_domicilio", rs.getString("ciudad_domicilio"));
                fila.addProperty("barrio_domicilio", rs.getString("barrio_domicilio"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("celular", rs.getString("celular"));
                fila.addProperty("direccion_negocio", rs.getString("direccion_negocio"));
                fila.addProperty("barrio_negocio", rs.getString("barrio_negocio"));
                fila.addProperty("telefon_negocio", rs.getString("telefon_negocio"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("unidad_negocio", rs.getString("unidad_negocio"));
                fila.addProperty("agencia", rs.getString("agencia"));
                fila.addProperty("altura_mora_actual", rs.getString("altura_mora_actual"));
                fila.addProperty("dia_pago", rs.getString("dia_pago"));
                fila.addProperty("saldo_actual", rs.getString("saldo_actual"));
                fila.addProperty("valor_saldo_vencido", rs.getString("valor_saldo_vencido"));
                fila.addProperty("interes_mora", rs.getString("interes_mora"));
                fila.addProperty("gac", rs.getString("gac"));
                fila.addProperty("total_a_pagar", rs.getString("total_a_pagar"));
                fila.addProperty("juridica", rs.getString("juridica"));
                fila.addProperty("reestructuracion", rs.getString("reestructuracion"));
                fila.addProperty("fecha_ult_compromiso", rs.getString("fecha_ult_compromiso"));
                fila.addProperty("responsable_cuenta", rs.getString("responsable_cuenta"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String asignarCartera(String empresa_responsable, String id_unidad_negocio, String id_agencia, String id_altura_mora, String user,String[] ArrayNeg) {

      query="SQL_OBTENER_SERIAL";
       String lote="";
        String retorno = "";
        try {
            con = this.conectarJNDI();

            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, "LTCOBEXT");
            rs = ps.executeQuery();
            if (rs.next()) {
                lote = rs.getString("serie");

                query = "SQL_ASIGNAR_TERCERO";
                query = this.obtenerSQL(query);

                ps = con.prepareStatement(query);
                ps.setString(1, empresa_responsable);
                ps.setString(2, user);
                ps.setString(3, lote);
                ps.setString(4, id_unidad_negocio);
                ps.setString(5, id_agencia);
                ps.setArray(6, con.createArrayOf("text", ArrayNeg));
                ps.setString(7, id_altura_mora);

                int executeUpdate = ps.executeUpdate();
                if (executeUpdate > 0) {
                    retorno = "OK";
                }
            }
        } catch (SQLException e) {
            retorno=e.toString();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String exportarNegociosReporteCartera(String id_unidad_negocio,String id_entidad_recaudo) {
        query = "CARGAR_REPORTE_NEGOCIOS";
        JsonArray lista = null;
        JsonObject obj = new JsonObject();
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_entidad_recaudo);
            ps.setString(2, id_unidad_negocio);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String generarArchivoAsobancaria(String id_archivo,String user) {
        query = "SQL_GENERAR_ARCHIVO_ASOBANCARIA";
        String respuesta = "{}";
        String ruta = "";
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
        File dir = new File(ruta);
        if (!dir.exists()){
            dir.mkdirs();
        }
        SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
        File archivo = new File(ruta+ "/Asobancaria_" + id_archivo + "_" + fmt.format(new Date()) + ".txt");
        BufferedWriter bw = null;
        try {

            if (!archivo.exists()) {
                archivo.createNewFile();

                con = this.conectarJNDI();
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, id_archivo);
                ps.setString(2, id_archivo);
                ps.setString(3, id_archivo);
                ps.setString(4, id_archivo);
                ps.setString(5, id_archivo);
                rs = ps.executeQuery();
                bw = new BufferedWriter(new FileWriter(archivo));
                while (rs.next()) {
                    bw.write(rs.getString("registro"));
                    bw.newLine();
                }
                bw.close();

                query = "UPDATE_ARCHIVO_GEERADO";
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, id_archivo);
                int r = ps.executeUpdate();
                if (r > 0) {
                    respuesta = "{\"success\":true,\"data\":\"OK\",\"status\":\"200\"}";
                }
            }

        } catch (Exception ex) {
            respuesta = "{\"error\":\"NegociosFintraImpl en el metodo: [generarArchivoAsobancaria]\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboEntidadRecaudo() {
           query = "SQL_CARGAR_ENTIDAD_RECAUDO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("codigo_entidad"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarRecaudos(String id_entidad_recaudo, String select_fecha) {
         JsonObject obj = new JsonObject();
        Gson gson = new Gson();

        query = "CARGAR_RECAUDO";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_entidad_recaudo);
            ps.setString(2, select_fecha);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();

                fila.addProperty("entidad", rs.getString("entidad"));
                fila.addProperty("codigo_recaudadora", rs.getString("codigo_recaudadora"));
                fila.addProperty("valor_total", rs.getString("valor_total"));
                fila.addProperty("id_archivo", rs.getString("id_archivo"));
                fila.addProperty("fecha_recaudo", rs.getString("fecha_recaudo"));
                fila.addProperty("cuenta_cliente", rs.getString("cuenta_cliente"));
                fila.addProperty("fecha_archivo", rs.getString("fecha_archivo"));
                fila.addProperty("numero_filas", rs.getString("numero_filas"));
                fila.addProperty("archivo_generado", rs.getString("archivo_generado"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String cargarRecaudoDetalle(String id_archivo) {
      JsonObject obj = new JsonObject();
        Gson gson = new Gson();

        query = "CARGAR_RECAUDO_DETALLE";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_archivo);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();

                fila.addProperty("id_archivo", rs.getString("id_archivo"));
                fila.addProperty("referencia_pricipal", rs.getString("referencia_pricipal"));
                fila.addProperty("valor_recaudado", rs.getString("valor_recaudado"));
                fila.addProperty("procedencia_pago", rs.getString("procedencia_pago"));
                fila.addProperty("medio_pago", rs.getString("medio_pago"));
                fila.addProperty("num_operacion", rs.getString("num_operacion"));
                fila.addProperty("num_autorizacion", rs.getString("num_autorizacion"));
                fila.addProperty("cod_entidad_debitada", rs.getString("cod_entidad_debitada"));
                fila.addProperty("cod_sucursal", rs.getString("cod_sucursal"));
                fila.addProperty("secuencia", rs.getString("secuencia"));
                fila.addProperty("causal_devolucion", rs.getString("causal_devolucion"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String cargarRecaudadorTercero() {
        query = "SQL_CARGAR_RECAUDADORA_TERCERO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("nit"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarCarteraPorAsignar(String id_unidad_negocio, String id_entidad_recaudo) {
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();

        query = "SQL_CARTERA_ASIGNADA_SIN_ASIGNAR";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_entidad_recaudo);
            ps.setString(2, id_unidad_negocio);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {

                fila = new JsonObject();
                fila.addProperty("cantidad", rs.getString("cantidad"));
                fila.addProperty("empresa_responsable", rs.getString("empresa_responsable"));
                fila.addProperty("periodo_carga", rs.getString("periodo_carga"));
                fila.addProperty("estado", rs.getString("estado"));
                fila.addProperty("unidad_negocio", rs.getString("unidad_negocio"));
                fila.addProperty("agencia", rs.getString("agencia"));
                fila.addProperty("altura_mora_actual", rs.getString("altura_mora_actual"));
                fila.addProperty("saldo_actual", rs.getString("saldo_actual"));
                fila.addProperty("valor_saldo_vencido", rs.getString("valor_saldo_vencido"));
                fila.addProperty("interes_mora", rs.getString("interes_mora"));
                fila.addProperty("gac", rs.getString("gac"));
                fila.addProperty("total_a_pagar", rs.getString("total_a_pagar"));
                fila.addProperty("lote", rs.getString("lote"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String  aprobarCarteraPorAsignar(String altura_mora_actual, String lote, String user) {
       query = "SQL_APROBAR_CARTERA_ASIGNADA";

        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, user);
            ps.setString(2, altura_mora_actual);
            ps.setString(3, lote);
            int executeUpdate = ps.executeUpdate();
            if (executeUpdate>0) {
                retorno = "OK";
            }

        } catch (SQLException e) {
            retorno=e.toString();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String cargarComboResponsableCuenta(String sucursal) {
        query = "CARGAR_RESPONSABLE_CUENTA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1,sucursal);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("idusuario"), rs.getString("idusuario"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboCiudad(String sucursal) {
        query = "CARGAR_CIUDAD";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1,sucursal);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("codciu"), rs.getString("nomciu"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboBarrio(String id_ciudad) {
         query = "CARGAR_BARRIO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1,id_ciudad);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("nombre"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarCasasCobranza() {
      JsonObject obj = new JsonObject();
        Gson gson = new Gson();

        query = "SQL_CARGAR_CASA_COBRANZA";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("cambio", rs.getString("cambio"));
                fila.addProperty("nit", rs.getString("nit"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("direccion", rs.getString("direccion"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("nombre_contacto1", rs.getString("nombre_contacto1"));
                fila.addProperty("telefono_contacto1", rs.getString("telefono_contacto1"));
                fila.addProperty("cargo_contacto1", rs.getString("cargo_contacto1"));
                fila.addProperty("email_contacto1", rs.getString("email_contacto1"));
                fila.addProperty("nombre_contacto2", rs.getString("nombre_contacto2"));
                fila.addProperty("telefono_contacto2", rs.getString("telefono_contacto2"));
                fila.addProperty("cargo_contacto2", rs.getString("cargo_contacto2"));
                fila.addProperty("email_contacto2", rs.getString("email_contacto2"));
                fila.addProperty("nombre_contacto3", rs.getString("nombre_contacto3"));
                fila.addProperty("telefono_contacto3", rs.getString("telefono_contacto3"));
                fila.addProperty("cargo_contacto3", rs.getString("cargo_contacto3"));
                fila.addProperty("email_contacto3", rs.getString("email_contacto3"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String cambiarEstadoCasaCobranza(String idCasaCobranza, Usuario usuario) {
        query = "SQL_CAMBIAR_ESTADO_CASA_COBRANZA";

        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, idCasaCobranza);
            int executeUpdate = ps.executeUpdate();
            if (executeUpdate>0) {
                retorno = "OK";
            }

        } catch (SQLException e) {
            retorno=e.toString();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String updateCasaCobranza(String id_casa, String nit, String casa_cobranza, String direccion, String telefono, String nombre_contacto1, String telefono_contacto1, String cargo_contacto1, String email_contacto1, String nombre_contacto2, String telefono_contacto2, String cargo_contacto2, String email_contacto2, String nombre_contacto3, String telefono_contacto3, String cargo_contacto3, String email_contacto3, Usuario usuario) {
      query = "SQL_UPDATE_CASA_COBRANZA";

        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nit);
            ps.setString(2, casa_cobranza);
            ps.setString(3, direccion);
            ps.setString(4, telefono);
            ps.setString(5, nombre_contacto1);
            ps.setString(6, telefono_contacto1);
            ps.setString(7, cargo_contacto1);
            ps.setString(8, email_contacto1);
            ps.setString(9, nombre_contacto2);
            ps.setString(10, telefono_contacto2);
            ps.setString(11, cargo_contacto2);
            ps.setString(12, email_contacto2);
            ps.setString(13, nombre_contacto3);
            ps.setString(14, telefono_contacto3);
            ps.setString(15, cargo_contacto3);
            ps.setString(16, email_contacto3);
            ps.setString(17, usuario.getLogin());
            ps.setString(18, id_casa);
            int executeUpdate = ps.executeUpdate();
            if (executeUpdate>0) {
                retorno = "OK";
            }

        } catch (SQLException e) {
            retorno=e.toString();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String insertCasaCobranza(String id_casa, String nit, String casa_cobranza, String direccion, String telefono, String nombre_contacto1, String telefono_contacto1, String cargo_contacto1, String email_contacto1, String nombre_contacto2, String telefono_contacto2, String cargo_contacto2, String email_contacto2, String nombre_contacto3, String telefono_contacto3, String cargo_contacto3, String email_contacto3, Usuario usuario) {
         query = "SQL_INSERT_CASA_COBRANZA";

        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nit);
            ps.setString(2, casa_cobranza);
            ps.setString(3, direccion);
            ps.setString(4, telefono);
            ps.setString(5, nombre_contacto1);
            ps.setString(6, telefono_contacto1);
            ps.setString(7, cargo_contacto1);
            ps.setString(8, email_contacto1);
            ps.setString(9, nombre_contacto2);
            ps.setString(10, telefono_contacto2);
            ps.setString(11, cargo_contacto2);
            ps.setString(12, email_contacto2);
            ps.setString(13, nombre_contacto3);
            ps.setString(14, telefono_contacto3);
            ps.setString(15, cargo_contacto3);
            ps.setString(16, email_contacto3);
            ps.setString(17, usuario.getLogin());
            int executeUpdate = ps.executeUpdate();
            if (executeUpdate>0) {
                retorno = "OK";
            }

        } catch (SQLException e) {
            retorno=e.toString();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String rechazarCarteraPorAsignar(String altura_mora_actual, String lote, String login) {
       query = "SQL_RECHAZAR_CARTERA_ASIGNADA";

        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, altura_mora_actual);
            ps.setString(2, lote);
            int executeUpdate = ps.executeUpdate();
            if (executeUpdate>0) {
                retorno = "OK";
            }

        } catch (SQLException e) {
            retorno=e.toString();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String generarCartasCobro(String id_unidad_negocio, String id_agencia, String id_altura_mora, Usuario usuario,String id_dia) {
        String ruta = "";
        String texto = "";
        Paragraph prg = null;
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
        String fecha_hoy = fmt.format(new java.util.Date());
        String replaceTexto ="";
        String plantilla="/images/nueva_plantilla_fintra.png";
        try {
            ArrayList<BeanGeneral> infoCartasbeans = this.buscarInfoCartasCliente(id_unidad_negocio, id_agencia, id_altura_mora,id_dia);

            if (infoCartasbeans.size() > 0) {
                ruta = this.directorioArchivo(usuario.getLogin(), "pdf");
                ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                if(id_altura_mora.equals("61-90")||id_altura_mora.equals("91-119")){
                    plantilla="/images/plantilla_cobranza.png";
                }
                Image imagen = Image.getInstance(rsb.getString("ruta") + plantilla);
                Document documento = new Document(PageSize.A4, 25, 25, 35, 30);
                PdfWriter.getInstance(documento, new FileOutputStream(ruta));
                documento.setMargins(30, 25 , 30,25);
                documento.open();
                DecimalFormat formatea = new DecimalFormat("###,###.##");
                texto=getTextoCarta(id_altura_mora);
                int valor_saldo = 0;
                int valor_ixm = 0;
                int valor_gac = 0;
                String saldo="";
                String ixm="";
                String gac="";

                for (int i = 0; i < infoCartasbeans.size(); i++) {
                    BeanGeneral infoCliente = infoCartasbeans.get(i);
                    saldo=infoCliente.getValor_05();
                    ixm=infoCliente.getValor_11();
                    gac=infoCliente.getValor_10();
                    valor_saldo=Integer.parseInt(saldo);
                    valor_ixm=Integer.parseInt(ixm);
                    valor_gac=Integer.parseInt(gac);
                    documento.newPage();
                    imagen.setAlignment(PdfPTable.ALIGN_CENTER);
                    imagen.scaleAbsolute(580, 835);
                    imagen.setAbsolutePosition(10, 10);
                    documento.add(imagen);
                    //Contenido carta de cobro de 1 a 30
                   replaceTexto= texto.replace("#FECHA", fecha_hoy);
                   replaceTexto= replaceTexto.replaceAll("#NOMBRE_CLIENTE", infoCliente.getValor_01());
                   replaceTexto= replaceTexto.replaceAll("#DIRECCION", infoCliente.getValor_02());
                   replaceTexto= replaceTexto.replaceAll("#BARRIO", infoCliente.getValor_03());
                   replaceTexto= replaceTexto.replaceAll("#COD_NEGOCIO", infoCliente.getValor_04());
                   replaceTexto= replaceTexto.replaceAll("#SALDO", formatea.format(valor_saldo));
                   replaceTexto= replaceTexto.replaceAll("#DIAS", infoCliente.getValor_06());
                   replaceTexto= replaceTexto.replaceAll("#FIRMA", infoCliente.getValor_07());
                   replaceTexto= replaceTexto.replaceAll("#OFICINA", infoCliente.getValor_08());
                   replaceTexto= replaceTexto.replaceAll("#CIUDAD", infoCliente.getValor_09());
                   replaceTexto= replaceTexto.replaceAll("#GAC", formatea.format(valor_gac));
                   replaceTexto= replaceTexto.replaceAll("#INTERES_MORA", formatea.format(valor_ixm));
                   replaceTexto= replaceTexto.replaceAll("#RESPONSABLE_CUENTA", infoCliente.getValor_12());

                    prg = new Paragraph(replaceTexto);
                    prg.setAlignment(Element.ALIGN_JUSTIFIED);
                    Font fuente= new Font();
                    fuente.setSize(10);
                    prg.setFont(fuente);

                    documento.add(prg);
                }
                documento.close();
            } else {
                return "false";
            }

        } catch (Exception ex) {
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "OK";
    }

    private String directorioArchivo(String user, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rsb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/Cartas_cobro_" + fmt.format(new java.util.Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    private ArrayList<BeanGeneral>  buscarInfoCartasCliente(String id_unidad_negocio, String id_agencia, String id_altura_mora,String id_dia) {
       query = "SQL_BUSCAR_INFO_CARTAS";
       String ini="";
       String fin="";
        ArrayList<BeanGeneral> ArrayBean = new ArrayList<>();
        String[] parts = id_altura_mora.split("-",2);
        ini = parts[0];
        fin = parts[1];

        if(!id_dia.equals("")){
           int inicial=Integer.parseInt(ini);
           int fina=Integer.parseInt(fin);
            if(Integer.parseInt(id_dia) >= inicial && Integer.parseInt(id_dia)<=fina){
               ini=id_dia;
                fin=id_dia;
             }else{
                return ArrayBean;
            }

       }
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_unidad_negocio);
            ps.setString(2, id_agencia);
            ps.setString(3, ini);
            ps.setString(4, fin);
            rs = ps.executeQuery();
            while (rs.next()) {
                BeanGeneral benGen = new BeanGeneral();
                benGen.setValor_01(rs.getString("nombre"));
                benGen.setValor_02(rs.getString("direccion"));
                benGen.setValor_03(rs.getString("barrio"));
                benGen.setValor_04(rs.getString("cod_neg"));
                benGen.setValor_05(rs.getString("valor_vencido"));
                benGen.setValor_06(rs.getString("dias_mora"));
                benGen.setValor_07(rs.getString("firma"));
                benGen.setValor_08(rs.getString("oficina"));
                benGen.setValor_09(rs.getString("ciudad"));
                benGen.setValor_10(rs.getString("gac"));
                benGen.setValor_11(rs.getString("interes_mora"));
                benGen.setValor_12(rs.getString("responsable_cuenta"));

                ArrayBean.add(benGen);
            }
        } catch (SQLException e) {
                e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return ArrayBean;
    }

    private String getTextoCarta(String id_altura_mora) {
        query = "SQL_GET_TEXTO_CARTAS";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_altura_mora);
            rs = ps.executeQuery();
            if (rs.next()) {
              return rs.getString("descripcion");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return "";
    }

    @Override
    public String cargarComboMora() {
       query = "SQL_CARGAR_COMBO_MORA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }


    @Override
    public String imprimirExtracto(double valorInicial, double ixm, double gac, String key_ref, Usuario usuario) {
        String respuesta = "{}";
        query = "SQL_DATA_EXTRACTO";
        JsonObject json = null;
        String url = "";
        try (Connection conn = this.conectarJNDI()) {
            ps = conn.prepareStatement(this.obtenerSQL(query), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ps.setDouble(1, valorInicial);
            ps.setString(2, key_ref);
            rs = ps.executeQuery();
            rs.last();
            String[][] multiArray = new String[rs.getRow()][];
            rs.beforeFirst();
            int i = 0;
            json = new JsonObject();
            while (rs.next()) {
                String[] elementos = new String[8];
                elementos[0] = rs.getString("cod_neg");
                elementos[1] = rs.getString("documento");
                elementos[2] = rs.getString("capital");
                elementos[3] = rs.getString("interes");
                elementos[4] = rs.getString("intXmora");
                elementos[5] = rs.getString("gac");
                elementos[6] = rs.getString("total");
                elementos[7] = rs.getString("porcentanje");
                multiArray[i] = elementos;
                i++;

                //datos para mensaje de texto
                json.addProperty("celular", rs.getLong("celular"));
                json.addProperty("usuario_aprobacion", rs.getString("aprobador"));
                json.addProperty("usuario_solicitante", rs.getString("usuario_solicitante"));
                json.addProperty("valor_pagar", rs.getDouble("capital"));
                json.addProperty("estrategia", rs.getInt("id_estrategia"));
                json.addProperty("nombre", rs.getString("nombre"));
                json.addProperty("ref2", rs.getString("ref2"));
            }

            boolean resp = false;
            if (multiArray.length > 0) {
                query = this.obtenerSQL("SQL_EXTRACTO_CARTERA");
                ps = conn.prepareStatement(query);
                ps.setArray(1, conn.createArrayOf("text", multiArray));
                ps.setString(2, usuario.getLogin());
                rs = ps.executeQuery();
                if (rs.next()) {
                    respuesta = rs.getString("numero_extracto");
                    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                    url = "http://prometheus.fintra.co:8094/fintra/exportar/migracion/" + usuario.getLogin() + "/Extracto_" + respuesta + "_" + fmt.format(new Date()) + ".pdf";

                    String sms = "#nombre#, tu alivio financiero con Fintra ha sido aprobado, referencia de pago principal #ref1#  y opcional  #ref2# , por valor de $" + Util.FormatoMiles(valorInicial);
                    sms=sms.replaceAll("#nombre#", json.get("nombre").getAsString()).replaceAll("#ref1#", respuesta).replaceAll("#ref2#", json.get("ref2").getAsString());
                    json.addProperty("mensaje", sms);
                    json.addProperty("referencia_pago", respuesta);
                    json.addProperty("key_ref", key_ref);
                    resp = this.sendSmsPago(json);
                    if (!resp) {
                       resp = sendSmsOption2(json);
                    }
                }

                //new ReestructurarNegociosImpl(usuario.getBd()).generarPdf(usuario.getLogin(), Integer.parseInt(respuesta));

                return "{\"success\":" + resp + ",\"data\":\"" + respuesta + "\",\"url\":\"" + url + "\",\"status\":\"200\"}";
            } else {
                respuesta = "{\"success\":false,\"data\":\"No se encontraron datos para la generación del extracto \",\"url\":\"" + url + "\",\"status\":\"200\"}";
            }

        } catch (SQLException ex) {
            respuesta = "{\"success\":false,\"data\":\"" + ex.getMessage() + "\",\"url\":\"" + url + "\",\"status\":\"200\"}";
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            desconectarPool(null, rs, ps, null, this.getClass());
        }
        return respuesta;
    }

    @Override
    public String getCuotaInicialApagar(ConceptoFactura fac, String tipo_ref) {
        String respuesta = "{}";
        query = "SQL_CARGAR_CONFIGURACION";
        JsonObject json = null;
        double valorPagar = 0;
        double valorCapital =0;
        String[] arrayConceptosCapital;
        try (Connection conn = this.conectarJNDI()) {
            ps = conn.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, fac.getDias_mora());
            ps.setString(2, tipo_ref);
            rs = ps.executeQuery();
            if (rs.next()) {
                json = new JsonObject();
                if (rs.getBoolean("aplica_cuota_inicial")) {
                    switch (rs.getString("tipo_calculo_inicial")) {
                        case "PORCENTAJE":
                            if (rs.getDouble("porcentaje") == 0) {
                                json.addProperty("success", false);
                                json.addProperty("error", "No existe un porcentaje de valor inicial configurado para la mora de " + fac.getDias_mora());
                                return new Gson().toJson(json);
                            }
                            valorPagar = Math.round(fac.getSaldoIntXmora() + fac.getSaldoGac() + ((fac.getSaldoActual() * rs.getDouble("porcentaje")) / 100));
                            json.addProperty("success", true);
                            json.addProperty("valor_pagar", valorPagar);
                            json.addProperty("porcentaje", rs.getDouble("porcentaje"));
                            break;
                        case "SUMA CONCEPTOS":
                            hidden = true;
                            String[] arrayConceptos = rs.getString("conceptos").split(",");
                            for (String concepto : arrayConceptos) {
                                valorPagar += getValorConceptos(concepto,fac,"inicial");
                            }

                            arrayConceptosCapital = rs.getString("conceptos_capital").split(",");
                            for (String concepto : arrayConceptosCapital) {
                                valorCapital += getValorConceptos(concepto,fac,"capital");
                            }

                            if(hidden){
                                fac.setSaldoCatVencido(0);
                            }

                            json.addProperty("success", true);
                            json.addProperty("capital", valorCapital);
                            json.addProperty("interes", fac.getSaldoInteres() + fac.getSaldoInteresAval());
                            json.addProperty("cat", fac.getSaldoCat());
                            json.addProperty("cat_vencido", fac.getSaldoCatVencido());
                            json.addProperty("cuota_admon", fac.getSaldoCuotaAdmin());
                            json.addProperty("hidden_cat", hidden);
                            json.addProperty("valor_pagar_total", valorPagar);
                            json.addProperty("valor_pagar", Math.round(valorPagar/rs.getInt("divisor_cuota_inicial")));
                            json.addProperty("periodos_cuota", rs.getInt("divisor_cuota_inicial"));
                            json.addProperty("porcentaje", 0);
                            json.addProperty("dto_gac", rs.getString("dto_gac"));
                            json.addProperty("dto_ixm", rs.getString("dto_ixm"));

                            break;

                        case "VALOR FIJO":

                            arrayConceptosCapital = rs.getString("conceptos_capital").split(",");
                            for(String concepto : arrayConceptosCapital) {
                                valorCapital += getValorConceptos(concepto,fac,"capital");
                            }

                            valorPagar=rs.getDouble("vr_cuota_inicial");

                            json.addProperty("success", true);
                            json.addProperty("capital", valorCapital);
                            json.addProperty("interes", fac.getSaldoInteres() + fac.getSaldoInteresAval());
                            json.addProperty("cat", fac.getSaldoCat());
                            json.addProperty("cat_vencido", fac.getSaldoCatVencido());
                            json.addProperty("cuota_admon", fac.getSaldoCuotaAdmin());
                            json.addProperty("hidden_cat", hidden);
                            json.addProperty("valor_pagar_total", valorPagar);
                            json.addProperty("valor_pagar", Math.round(valorPagar/rs.getInt("divisor_cuota_inicial")));
                            json.addProperty("periodos_cuota", rs.getInt("divisor_cuota_inicial"));
                            json.addProperty("porcentaje", 0);
                            json.addProperty("dto_gac", rs.getString("dto_gac"));
                            json.addProperty("dto_ixm", rs.getString("dto_ixm"));

                            break;

                    }

                }else{

                    arrayConceptosCapital = rs.getString("conceptos_capital").split(",");
                    for (String concepto : arrayConceptosCapital) {
                        valorCapital += getValorConceptos(concepto, fac, "capital");
                    }
                    json.addProperty("success", true);
                    json.addProperty("capital", valorCapital);
                    json.addProperty("interes", fac.getSaldoInteres() + fac.getSaldoInteresAval());
                    json.addProperty("cat", fac.getSaldoCat());
                    json.addProperty("cat_vencido", 0);
                    json.addProperty("cuota_admon", fac.getSaldoCuotaAdmin());
                    json.addProperty("hidden_cat", true);
                    json.addProperty("valor_pagar_total", valorPagar);
                    json.addProperty("valor_pagar",  Math.round(valorPagar/rs.getInt("divisor_cuota_inicial")));
                    json.addProperty("periodos_cuota", rs.getInt("divisor_cuota_inicial"));
                    json.addProperty("porcentaje", 0);
                    json.addProperty("dto_gac", rs.getString("dto_gac"));
                    json.addProperty("dto_ixm", rs.getString("dto_ixm"));

                }

                return new Gson().toJson(json);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            desconectarPool(null, rs, ps, null, this.getClass());
        }
        return respuesta;
    }

    @Override
    public String cargarComboEstrategiaCartera() {
        query = "SQL_GET_ESTRATEGIA_CARTERA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("tipo"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String verCarteraRefinanciar(String valor_capital, String no_cuotas, String fecha,
            String ciudad, String compra_cartera, String cuota_inicio, String tasa, String saldo_cat, String tipo_refi) {

        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI();
            JsonObject unidadNegocioEstrategia = this.getUnidadNegocioEstrategia(tipo_refi,fecha);
            switch (unidadNegocioEstrategia.get("unidad_negocio").getAsInt()) {
                case 1:
                    query = "SQL_VER_CARTERA_REFINANCIAR_REFI";
                    query = this.obtenerSQL(query);
                    ps = con.prepareStatement(query);
                    ps.setString(1, valor_capital);
                    ps.setString(2, no_cuotas);
                    ps.setString(3, fecha);
                    ps.setString(4, ciudad);
                    ps.setString(5, unidadNegocioEstrategia.get("fecha_base_liq").getAsString());
                    ps.setString(6, compra_cartera);
                    ps.setString(7, cuota_inicio);
                    ps.setString(8, tasa);
                    ps.setString(9, saldo_cat);
                    ps.setString(10, tipo_refi);
                    break;
                case 31:
                    query = "SQL_VER_CARTERA_REFINANCIACION_EDU";
                    query = this.obtenerSQL(query);
                    ps = con.prepareStatement(query);
                    ps.setString(1, valor_capital);
                    ps.setString(2, no_cuotas);
                    ps.setString(3, fecha);
                    ps.setString(4, cuota_inicio);
                    ps.setString(5, tasa);
                    ps.setString(6, unidadNegocioEstrategia.get("fecha_base_liq").getAsString());
                    break;
                case 22:
                    query = "SQL_VER_CARTERA_REFINANCIACION_LIB";
                    query = this.obtenerSQL(query);
                    ps = con.prepareStatement(query);
                    ps.setString(1, valor_capital);
                    ps.setString(2, no_cuotas);
                    ps.setString(3, unidadNegocioEstrategia.get("fecha_base_liq").getAsString());
                    ps.setString(4, fecha);
                    ps.setString(5, tasa);
                    ps.setString(6, cuota_inicio);

            }

            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("item", rs.getString("item"));
                fila.addProperty("fecha", rs.getString("fecha"));
                fila.addProperty("capital", rs.getString("capital"));
                fila.addProperty("interes", rs.getString("interes"));
                fila.addProperty("cat", rs.getString("cat"));
                fila.addProperty("cuota_manejo", rs.getString("cuota_manejo"));
                fila.addProperty("seguro", rs.getString("seguro"));
                fila.addProperty("valor_cuota", rs.getString("valor_cuota"));
                fila.addProperty("saldo_inicial", rs.getString("saldo_inicial"));
                fila.addProperty("saldo_final", rs.getString("saldo_final"));
                fila.addProperty("capital_aval", rs.getString("capital_aval"));
                fila.addProperty("interes_aval", rs.getString("interes_aval"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    private double getValorConceptos(String concepto, ConceptoFactura fac, String tipo) {
        double valor = 0;
        switch (concepto) {
            case "K":
                valor = fac.getSaldoCapital();
                break;
            case "INT":
                valor = fac.getSaldoInteres();
                if(tipo.equals("capital")){
                    fac.setSaldoInteres(0);
                }
                break;
            case "CM":
                valor = fac.getSaldoCuotaAdmin();
                if(tipo.equals("capital")){
                    fac.setSaldoCuotaAdmin(0);
                }
                break;
            case "CA":
                if(tipo.equals("inicial")){
                    valor = fac.getSaldoCatVencido();
                    fac.setSaldoCat(fac.getSaldoCat() - fac.getSaldoCatVencido());
                    this.hidden = false;
                }else{
                    fac.setSaldoCat(0);
                }
                break;
            case "SEG":
                valor = 0;
                break;
            case "KV":
                valor = fac.getSaldoCapitalAval();
                break;
            case "INTAV":
                valor = fac.getSaldoInteresAval();
                break;
            case "INTXMORA":
                valor = fac.getSaldoIntXmora();
                break;
            case "GAC":
                valor = fac.getSaldoGac();
                break;
        }
        return valor;
    }

    @Override
    public String anularRefinanciacion(String keyRefinaciacion, Usuario usuario, String observacion) {
        query = "SQL_ANULAR_REFINANCIACION";
        JsonObject response  = new JsonObject();
        try (Connection conn = this.conectarJNDI()) {
            ps = conn.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, observacion);
            ps.setString(3, keyRefinaciacion);
            int executeUpdate = ps.executeUpdate();
            if(executeUpdate >0 ){
               response.addProperty("success", true);
               response.addProperty("mensaje", "Proceso de anulación exitoso");
            }else{
               throw new SQLException();
            }
        } catch (SQLException ex) {
            response.addProperty("success", false);
            response.addProperty("mensaje", "Lo sentimos su anulación no pudo ser procesada.");
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            desconectarPool(con, rs, ps, null, this.getClass());
        }
        return new Gson().toJson(response);
    }

    private JsonObject getUnidadNegocioEstrategia(String tiporef, String fecha_pago) {
        query = "SQL_UNIDAD_ESTRATEGIA";
        JsonObject estrategia=null;
        try (Connection conn = this.conectarJNDI()) {
            ps = conn.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, fecha_pago);
            ps.setString(2, tiporef);
            rs = ps.executeQuery();
            if (rs.next()){
                estrategia=new JsonObject();
                estrategia.addProperty("unidad_negocio", rs.getInt("unidad_negocio"));
                estrategia.addProperty("fecha_base_liq", rs.getString("fecha_base_liq"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            desconectarPool(null, rs, ps, null, this.getClass());
        }
        return estrategia;
    }

    private Boolean sendSmsPago(JsonObject parameter) {
        HttpURLConnection conn = null;
        StringBuilder builder = null;
        InputStreamReader in = null;
        BufferedReader br = null;
        String body;
        String output;
        try {
            URL urlToken = new URL("http://zeus.fintra.co:3000/sms/send");
            conn = (HttpURLConnection) urlToken.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setConnectTimeout(10000); //set timeout to 5 seconds
            conn.setReadTimeout(10000);
            body = new Gson().toJson(parameter);
            conn.setDoOutput(true);
            try (OutputStream os = conn.getOutputStream()) {
                os.write(body.getBytes());
                os.flush();
                os.close();
            }

            if (conn.getResponseCode() == 200) {
                return true;
            }
        } catch (java.net.SocketTimeoutException e) {
            return false;
        } catch (IOException ex) {
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return false;
    }

    private boolean sendSmsOption2(JsonObject inParameter) {

        HttpsURLConnection conn = null;
        StringBuilder builder = null;
        InputStreamReader in = null;
        BufferedReader br = null;
        String output;
        String body;
        String token_type = "";
        String access_token = "";
        JsonObject parameter;
        boolean tk = false;

        try {

            URL urlToken = new URL("https://fintra.sagicc.co/oauth/token");
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new SecureRandom());
            conn = (HttpsURLConnection) urlToken.openConnection();
            conn.setSSLSocketFactory(sslContext.getSocketFactory());
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setConnectTimeout(10000); //set timeout to 5 seconds
            conn.setReadTimeout(10000);
            body = new Gson().toJson(getConfigLoginApiSms("SAGICC"));
            conn.setDoOutput(true);
            try(OutputStream os = conn.getOutputStream()){
                os.write(body.getBytes());
                os.flush();
                os.close();
            }

            if (conn.getResponseCode() != 200) {
                return false;
            }
            in = new InputStreamReader(conn.getInputStream());
            br = new BufferedReader(in);
            builder = new StringBuilder();
            while ((output = br.readLine()) != null) {
                builder.append(output);
            }

            parameter = (JsonObject) new JsonParser().parse(builder.toString());
            conn.disconnect();
            if (parameter != null) {
                token_type = parameter.get("token_type").getAsString();
                access_token = parameter.get("access_token").getAsString();
                tk = true;
            }
        } catch (IOException e) {
            return false;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyManagementException ex) {
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        if (tk) {
            try {
                URL url = new URL("https://fintra.sagicc.co/api/accion/mensaje/nuevo");
                SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
                sslContext.init(null, null, new SecureRandom());
                conn = (HttpsURLConnection) url.openConnection();
                conn.setSSLSocketFactory(sslContext.getSocketFactory());
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("authorization", token_type + " " + access_token);
                conn.setConnectTimeout(10000); //set timeout to 5 seconds
                conn.setReadTimeout(10000);
                body = "{\"canal_id\":31,\"contacto\": \"" + inParameter.get("celular") + "\",\"mensaje\":" + inParameter.get("mensaje") + "}";
                conn.setDoOutput(true);
                try(OutputStream os = conn.getOutputStream()){
                    os.write(body.getBytes());
                    os.flush();
                    os.close();
                }
                if (conn.getResponseCode() != 200) {
                    return false;
                }
                in = new InputStreamReader(conn.getInputStream());
                br = new BufferedReader(in);
                builder = new StringBuilder();
                while ((output = br.readLine()) != null) {
                    builder.append(output);
                }
                parameter = (JsonObject) new JsonParser().parse(builder.toString());
                conn.disconnect();
                if (parameter != null) {
                    String status = parameter.get("status").getAsString();
                    if (status.equals("OK")) {
                        return true;
                    }
                }
            } catch (IOException e) {
                return false;
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
            } catch (KeyManagementException ex) {
                Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }

        }
        return false;
    }

    public JsonObject getConfigLoginApiSms(String proveedor){
        query = "SQL_GET_INFO_LOGIN_API";
        con = null;
        JsonObject json = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1,proveedor );
            rs = ps.executeQuery();
            if (rs.next()) {
                json.addProperty("grant_type", rs.getString("grant_type"));
                json.addProperty("username", rs.getString("user_name"));
                json.addProperty("password", rs.getString("clave"));
                json.addProperty("client_id", rs.getString("client_id"));
                json.addProperty("client_secret", rs.getString("client_secret"));
            }
        } catch (SQLException | NullPointerException e  ) {

        } finally {
            desconectarPool(con, rs, ps, null, this.getClass());
        }
        return json;

    }

    @Override
    public String cargarObservacionSimulacion(String key_ref) {
        query = "SQL_GET_OBSERVACION_SIMULACION";
        JsonObject response  = new JsonObject();
        try (Connection conn = this.conectarJNDI()) {
            ps = conn.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, key_ref);
            rs=ps.executeQuery();
            if(rs.next()){
               response.addProperty("success", true);
               response.addProperty("observacion", rs.getString("observacion_simulacion"));
            }else{
               throw new SQLException();
            }
        } catch (SQLException ex) {
            response.addProperty("success", false);
            response.addProperty("observacion", "Error al cargar la observacion");
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            desconectarPool(con, rs, ps, null, this.getClass());
        }
        return new Gson().toJson(response);
    }

    private boolean getEsquemaViejo(String negocio) {
        query = "SQL_GET_ESQ_VIEJO";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            if (rs.next()) {
              return rs.getBoolean("resp");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return false;
    }
    
    private JsonObject getUnidadNegocioEstrategia(String key_ref) {
        query = "SQL_UNIDAD_ESTRATEGIA_KEY";
        JsonObject estrategia=null;
        try (Connection conn = this.conectarJNDI()) {
            ps = conn.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, key_ref);
            rs = ps.executeQuery();
            if (rs.next()){
                estrategia=new JsonObject();
                estrategia.addProperty("unidad_negocio", rs.getInt("unidad_negocio"));
                estrategia.addProperty("nombre", rs.getString("nombre"));
                estrategia.addProperty("es_retanqueo", rs.getBoolean("es_retanqueo"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(NegociosFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            desconectarPool(null, rs, ps, null, this.getClass());
        }
        return estrategia;
    }
    
    private String aplicarEstrategiaManual(Connection con,int idUnidadNegocio, String key_ref,String observacion,Usuario usuario) 
            throws SQLException, MalformedURLException, IOException {
        
        String respuesta = "{}";
        String retorno = "";
        query = "SQL_APLICAR_ESTRATEGIA_MANUAL";
        query = this.obtenerSQL(query);
        ps = con.prepareStatement(query);
        ps.setString(1, key_ref);
        ps.setString(2, usuario.getLogin());
        ps.setString(3, observacion);
        rs = ps.executeQuery();
        if (rs.next()) {
            retorno = rs.getString("retorno");
            if (retorno.startsWith("LR")) {
                query = this.obtenerSQL("BUSCAR_NEGOCIOS_POR_GENERAR_CARTERA");
                ps = con.prepareStatement(query);
                ps.setString(1, key_ref);
                rs = ps.executeQuery();

                while (rs.next()) {
                    String cod_neg = rs.getString("cod_neg");
                    String codNuevoNegocio = rs.getString("cod_nuevo_negocio");
                    boolean nuevoNeg = rs.getBoolean("negocio_nuevo");
                    String urlapi = "http://prometheus.fintra.co:8094/fintra/EndPointCoreServlet";                    
                    switch (idUnidadNegocio) {
                               case 1:
                                   urlapi += nuevoNeg ? "?option=3&negocio=" + codNuevoNegocio + "&user=" + usuario.getLogin() : "?option=7&negocio=" + cod_neg + "&user=" + usuario.getLogin();
                                   break;
                               case 22:
                                   urlapi += nuevoNeg ? "?option=9&negocio=" + codNuevoNegocio + "&user=" + usuario.getLogin() : "?option=9&negocio=" + cod_neg + "&user=" + usuario.getLogin();                                
                                   break;
                    }                    

                    URL url = new URL(urlapi);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Failed : HTTP Error code : "
                                + conn.getResponseCode());
                    }
                    StringBuilder builder = new StringBuilder();
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String output;
                    while ((output = br.readLine()) != null) {
                        builder.append(output);
                    }
                    JsonObject response = (JsonObject) new JsonParser().parse(builder.toString());
                    String resp = response.get("status").getAsString();
                    if (resp.equals("200")) {
                        if (con.isClosed()) {
                            con = this.conectarJNDI();
                        }
                        query = this.obtenerSQL("UPDATE_CARTERA_GENERADA");
                        ps = con.prepareStatement(query);
                        ps.setString(1, cod_neg);
                        ps.setString(2, key_ref);
                        ps.executeUpdate();
                        respuesta = "{\"success\":true,\"data\":\"" + true + "\"}";
                    }
                    conn.disconnect();
                }
            } else {
                respuesta = "{\"success\":false,\"data\":\"Lo sentimos no se pudo generar el lote de pago\"}";
            }
        }        
        return respuesta;
    }

}
