/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.operation.model.DAOS.MainDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.tsp.operation.model.DAOS.FiltroLibranzaDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author root
 */
public class FiltroLibranzaImpl extends MainDAO implements FiltroLibranzaDAO{

    public FiltroLibranzaImpl(String dataBaseName) {
        super("FiltroLibranzaDAO.xml", dataBaseName);
    }

    @Override
    public JsonObject getOcupLaboral() throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_OCUPACION_LABORAL";
        JsonObject res;
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));

            rs = ps.executeQuery();
            while (rs.next()) {
                res = new JsonObject();
                res.addProperty("id", rs.getString("id"));
                res.addProperty("valor", rs.getString("descripcion"));
                arr.add(res);
            }
            res = new JsonObject();
            res.add("rows", arr);
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: getOcupLaboral() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }

    @Override
    public JsonObject getEstadoCivil() throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_ESTADO_CIVIL";
        JsonObject res;
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));

            rs = ps.executeQuery();
            while (rs.next()) {
                res = new JsonObject();
                res.addProperty("id", rs.getString("cod"));
                res.addProperty("valor", rs.getString("descripcion"));
                arr.add(res);
            }
            res = new JsonObject();
            res.add("rows", arr);
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: getEstadoCivil() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }

    @Override
    public JsonObject getConvenios(String oLab) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_CONVENIOS";
        JsonObject res;
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, oLab);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                res = new JsonObject();
                res.addProperty("id", rs.getString("id"));
                res.addProperty("valor", rs.getString("nombre"));
                arr.add(res);
            }
            res = new JsonObject();
            res.add("rows", arr);
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: getConvenios() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }

    @Override
    public JsonObject buscarPersona(String identificacion) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray arr = new JsonArray();
        JsonObject res = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL("GET_PERSONA"));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            arr = new JsonArray();
            if (rs.next()) {
                res = new JsonObject();
                res.addProperty("id_filtro_busqueda", rs.getString("id_filtro_busqueda"));
                res.addProperty("primer_apellido", rs.getString("primer_apellido"));
                res.addProperty("segundo_apellido", rs.getString("segundo_apellido"));
                res.addProperty("primer_nombre", rs.getString("primer_nombre"));
                res.addProperty("segundo_nombre", rs.getString("segundo_nombre"));
                res.addProperty("telefono", rs.getString("telefono"));
                res.addProperty("celular", rs.getString("celular"));
                res.addProperty("estado_civil", rs.getString("estado_civil"));
                res.addProperty("fecha_nacimiento", rs.getString("fecha_nacimiento"));
                
                res.addProperty("id_ocupacion_laboral", rs.getString("id_ocupacion_laboral"));
                res.addProperty("fecha_ocup_laboral", rs.getString("fecha_ocup_laboral"));
                res.addProperty("id_configuracion_libranza", rs.getString("id_configuracion_libranza"));
                res.addProperty("id_empresa_pagaduria", rs.getString("id_empresa_pagaduria"));
                res.addProperty("factor_seguro", rs.getString("factor_seguro"));
                res.addProperty("salario", rs.getString("salario"));
                res.addProperty("descuento_ley", rs.getString("descuento_ley"));
                res.addProperty("extraprima", rs.getString("extraprima"));                
                res.addProperty("otros_ingresos", rs.getString("otros_ingresos"));
                res.addProperty("valor_solicitado", rs.getString("valor_solicitado"));
                res.addProperty("plazo", rs.getString("plazo"));
                res.addProperty("viable", rs.getString("viable"));
                      
            //    arr.add(res);
                return res;
            } throw new Exception("");
            /*
            ps = con.prepareStatement(this.obtenerSQL("GET_FILTRO_LIBRANZAxCED"));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            while (rs.next()) {
                res = new JsonObject();
                res.addProperty("id", rs.getString("id"));
                res.addProperty("identificacion", rs.getString("identificacion"));
                res.addProperty("primer_apellido", rs.getString("primer_apellido"));
                res.addProperty("segundo_apellido", rs.getString("segundo_apellido"));
                res.addProperty("primer_nombre", rs.getString("primer_nombre"));
                res.addProperty("segundo_nombre", rs.getString("segundo_nombre"));
                res.addProperty("telefono", rs.getString("telefono"));
                res.addProperty("celular", rs.getString("celular"));
                res.addProperty("estado_civil", rs.getString("estado_civil"));
                res.addProperty("fecha_nacimiento", rs.getString("fecha_nacimiento"));
                res.addProperty("id_ocupacion_laboral", rs.getString("id_ocupacion_laboral"));
                res.addProperty("fecha_ocup_laboral", rs.getString("fecha_ocup_laboral"));
                res.addProperty("id_configuracion_libranza", rs.getString("id_configuracion_libranza"));
                res.addProperty("factor_seguro", rs.getString("factor_seguro"));
                res.addProperty("salario", rs.getString("salario"));
                res.addProperty("descuento_ley", rs.getString("descuento_ley"));
                res.addProperty("desc_ingreso", rs.getString("desc_ingreso"));
                res.addProperty("extraprima", rs.getString("extraprima"));
                res.addProperty("otros_ingresos", rs.getString("otros_ingresos"));
                res.addProperty("valor_solicitado", rs.getString("valor_solicitado"));
                res.addProperty("plazo", rs.getString("plazo"));
                arr.add(res);
            }
            res = new JsonObject();
            res.add("rows", arr);
            return res;*/
        } catch (SQLException ex) {
            throw new Exception("ERROR: buscarPersona() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }

    @Override
    public JsonObject infoConvenio(String id_cnl) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "INFO_CONVENIO";
        JsonObject res = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_cnl);

            rs = ps.executeQuery();
            if (rs.next()) {
                res = new JsonObject();
                res.addProperty("tasa", rs.getDouble("tasa"));
                res.addProperty("per_gracia", rs.getInt("periodo_gracia"));
                res.addProperty("fac_seg", rs.getDouble("factor_seguro"));
                res.addProperty("colchon", rs.getDouble("colchon"));
                res.addProperty("porc_desc", rs.getDouble("porc_desc"));
            }
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: infoConvenio() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }

    @Override
    public double getPorcExtraprima(String id_ocup, int edad) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_EXTRAPRIMA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_ocup);
            ps.setInt(2, edad);
 
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getDouble("perc_extraprima");
            }
            throw new SQLException("Sin configuracion para la edad");
        } catch (SQLException ex) {
            throw new Exception("ERROR: getPorcExtraprima() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        } 
    }

    @Override
    public JsonObject getDescuentosLey(String id_ocup, double salario) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_DESCUENTO_LEY";
        JsonObject res = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_ocup);
            ps.setDouble(2, salario);
 
            rs = ps.executeQuery();
            if (rs.next()) {
                res.addProperty("porc_desc", rs.getDouble("total_descuento"));
                res.addProperty("total", salario*rs.getDouble("total_descuento")/100);
                return res;
            }
            throw new SQLException("Sin configuracion para la edad");
        } catch (SQLException ex) {
            throw new Exception("ERROR: getPorcExtraprima() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }

    @Override
    public JsonObject GuardarFiltro(JsonObject form, String[] listadocs, Usuario usuario) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String id_filtro_libranza = "";
        int numero_solicitud = 0;
        JsonObject r = new JsonObject();
        
        try {
            con = this.conectarJNDI(); 
            con.setAutoCommit(false);           
            ps = con.prepareStatement(this.obtenerSQL(form.get("accion").getAsString()));
            ps.setString(1, form.get("identificacion").getAsString());
            ps.setString(2, form.get("pr_apel").getAsString());
            ps.setString(3, form.get("sg_apel").getAsString());
            ps.setString(4, form.get("pr_nom").getAsString());
            ps.setString(5, form.get("sg_nom").getAsString());
            ps.setString(6, form.get("tel").getAsString());
            ps.setString(7, form.get("cel").getAsString());
            ps.setString(8, form.get("est_civil").getAsString());
            ps.setString(9, form.get("fec_nac").getAsString());
            ps.setInt(10, form.get("id_ocupacion").getAsInt());
            ps.setString(11, form.get("fec_ocupa").getAsString());
            ps.setInt(12, form.get("id_config").getAsInt());
            ps.setDouble(13, form.get("fac_seg").getAsDouble());
            ps.setDouble(14, form.get("salario").getAsDouble());
            ps.setDouble(15, form.get("desc_ley").getAsDouble());
            ps.setDouble(16, form.get("extraprima").getAsDouble());
            ps.setDouble(17, form.get("otro_ingr").getAsDouble());
            ps.setDouble(18, form.get("valor_credito").getAsDouble());
            ps.setInt(19, form.get("plazo").getAsInt());
            ps.setString(20, form.get("usuario").getAsString());
            ps.setDouble(21, form.get("vlr_cuota").getAsDouble());
            ps.setInt(22, form.get("id_empresa_pagaduria").getAsInt());
            ps.setString(23, form.get("viable").getAsString());
            if(form.get("accion").getAsString().equals("UPDATE_FILTRO")) {
                ps.setString(24, form.get("id_filtro").getAsString());
            }
    
            rs = ps.executeQuery();
            if (rs.next()) {
                id_filtro_libranza = rs.getString("id");
                numero_solicitud = rs.getInt("numero_solicitud");
                ps = con.prepareStatement(insertarDocsFiltroLibranza(id_filtro_libranza, listadocs, usuario));
                ps.executeUpdate();

                r.addProperty("id_filtro", id_filtro_libranza);
                r.addProperty("num_solicitud", numero_solicitud);

                con.commit();

            } else {
                r.addProperty("mensaje", "La cedula ingresada no posee solicitudes asignadas");
            }
            return r;
        } catch (SQLException ex) {
            throw new Exception("ERROR: getPorcExtraprima() " + ex.getMessage());
        } finally {            
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { con.setAutoCommit(true); this.desconectar(con); } catch (SQLException ex) { } }            
        }
    }

    @Override
    public JsonObject getFiltroLibranza(String idL) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_FILTRO_LIBRANZAxID";
        JsonObject res = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idL);
 
            rs = ps.executeQuery();
            if (rs.next()) {
                res.addProperty("identificacion", rs.getString("identificacion"));
                res.addProperty("primer_apellido", rs.getString("primer_apellido"));
                res.addProperty("segundo_apellido", rs.getString("segundo_apellido"));
                res.addProperty("primer_nombre", rs.getString("primer_nombre"));
                res.addProperty("segundo_nombre", rs.getString("segundo_nombre"));
                res.addProperty("telefono", rs.getString("telefono"));
                res.addProperty("celular", rs.getString("celular"));
                res.addProperty("estado_civil", rs.getString("estado_civil"));
                res.addProperty("fecha_nacimiento", rs.getString("fecha_nacimiento"));
                res.addProperty("id_ocupacion_laboral", rs.getString("id_ocupacion_laboral"));
                res.addProperty("fecha_ocup_laboral", rs.getString("fecha_ocup_laboral"));
                res.addProperty("id_configuracion_libranza", rs.getString("id_configuracion_libranza"));
                res.addProperty("factor_seguro", rs.getString("factor_seguro"));
                res.addProperty("salario", rs.getString("salario"));
                res.addProperty("descuento_ley", rs.getString("descuento_ley"));
                res.addProperty("desc_ingreso", rs.getString("desc_ingreso"));
                res.addProperty("extraprima", rs.getString("extraprima"));
                res.addProperty("otros_ingresos", rs.getString("otros_ingresos"));
                res.addProperty("valor_solicitado", rs.getString("valor_solicitado"));
                res.addProperty("plazo", rs.getString("plazo"));
                res.addProperty("id_pagaduria", rs.getString("id_pagaduria"));
                res.addProperty("doc_pagaduria", rs.getString("doc_pagaduria"));
                return res;
            }
            throw new SQLException("Informacion filtro no encontrada");
        } catch (SQLException ex) {
            throw new Exception("ERROR: getFiltroLibranza() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }

    @Override
    public String formalizarLibranza() {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_FROMALIZAR_LIBRANZA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion="\"respuesta\":\"ERROR\"";
        }
        return informacion;
    }
    
    @Override
    public JsonObject getIdFiltroLibranza(String cedula) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_FILTRO_LIBRANZAxCED";
        String filtro = " AND numero_solicitud = 0 ";
        JsonObject res = new JsonObject();     
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            ps.setString(1, cedula);
          
            rs = ps.executeQuery();
            if (rs.next()) {
                res.addProperty("id_filtro", rs.getString("id"));
            }                  
        } catch (SQLException ex) {           
            throw new Exception("ERROR: getIdFiltroLibranza() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
            return res;
        } 
    }
    
    @Override
    public JsonObject getEmpresasPagaduria(String idConfigLibranza) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_EMPRESAS_PAGADURIA";
        JsonObject res;
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idConfigLibranza);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                res = new JsonObject();
                res.addProperty("id", rs.getString("id"));
                res.addProperty("valor", rs.getString("nombre"));
                arr.add(res);
            }
            res = new JsonObject();
            res.add("rows", arr);
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: getEmpresasPagaduria() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }

    @Override
    public JsonObject getDocumentosRequeridos(String idFiltroLibranza) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_DOCUMENTOS_REQUERIDOS";
        JsonObject res;
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idFiltroLibranza);
            rs = ps.executeQuery();
            while (rs.next()) {
                res = new JsonObject();
                res.addProperty("id", rs.getString("id_documento"));
                res.addProperty("valor", rs.getString("nombre_documento"));
                res.addProperty("estado", rs.getString("estado"));
                arr.add(res);
            }
            res = new JsonObject();
            res.add("rows", arr);
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: getDocumentosRequeridos() " + ex.getMessage());
        } finally {
            if (rs != null) { try { rs.close(); } catch (SQLException ex) { } }
            if (ps != null) { try { ps.close(); } catch (SQLException ex) { } }
            if (con != null) { try { this.desconectar(con); } catch (SQLException ex) { } }
        }
    }
    
   public String insertarDocsFiltroLibranza(String idFiltroLibranza, String[] listadocs, Usuario usuario) throws SQLException {
               
        String cadena = "";
        String query = "SQL_INSERTAR_DOCS_FILTRO_LIBRANZA";
        String sql = "";
        StringStatement st = null;
       
        try {
            //Eliminamos documentos existentes para el filtro dado
             sql = this.obtenerSQL("SQL_DELETE_DOCS_FILTRO_LIBRANZA");
                st = new StringStatement(sql, true);                
                st.setInt(1, Integer.parseInt(idFiltroLibranza));                             
                cadena += st.getSql();
            //Insertamos los nuevos documentos
            for (int i = 0; i < listadocs.length; i++) {               
                sql = this.obtenerSQL(query);
                st = new StringStatement(sql, true);                
                st.setInt(1, Integer.parseInt(idFiltroLibranza));
                st.setInt(2, Integer.parseInt(listadocs[i]));
                st.setString(3, usuario.getLogin());               
                cadena += st.getSql();
                st = null;
            }
            
         
        } catch (Exception e) {
            System.out.println("Error en SQL_INSERTAR_DOCS_FILTRO_LIBRANZA (insertarDocsFiltroLibranza)"+e.toString());
            e.printStackTrace();
        }
        return cadena;

    }
   
   @Override
   public JsonObject consultarHDC(JsonObject jsonData, String token) {
       String inputLine;
       URL apiUrl = null;
       HttpURLConnection connection = null;
       JsonObject response = new JsonObject();
       
       try {
           apiUrl = new URL("http://prometheus.fintra.co:8084/fintracredit/webresources/hdc/credit_history_fintra");
           connection = (HttpURLConnection) apiUrl.openConnection();
           connection.setRequestMethod("PUT");                    
           connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
           connection.setRequestProperty("token", token);
           connection.setDoOutput(true);
           connection.setDoInput(true);
           connection.setUseCaches(false);
           
           JsonObject paramsObj = new JsonObject();
           paramsObj.addProperty("tipo_identificacion", "CED");           
           paramsObj.addProperty("identificacion", jsonData.get("identificacion").getAsString());           
           paramsObj.addProperty("primer_apellido", jsonData.get("pr_apel").getAsString());
           paramsObj.addProperty("nit_empresa", "8020220161");
           paramsObj.addProperty("entidad","LIBRANZA");
           paramsObj.addProperty("ciudad", "BQ");
           paramsObj.addProperty("asesor", jsonData.get("usuario").getAsString());
           paramsObj.addProperty("departamento", "ATL");
           
           String data = "data=" + new Gson().toJson(paramsObj);
           
           DataOutputStream out = new DataOutputStream(connection.getOutputStream());
           out.writeBytes(data);
           out.flush();
           out.close();
           
           int status = connection.getResponseCode();
           if (status == 200) {

               BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
               StringBuffer content = new StringBuffer();

               while ((inputLine = in.readLine()) != null) {
                   content.append(inputLine);
               }
               response = (JsonObject) new JsonParser().parse(content.toString());

               in.close();
           } else {
               throw new IOException("Response code " + status);
           }
       } catch (IOException ex) {
           ex.printStackTrace();
           response = (JsonObject) new JsonParser().parse("{\"error\":{\"data\":{\"comentario\": \"Error en la comunicacion con Datacredito: " + ex.getMessage() + "\"}}}");
       } finally {
           if (connection != null) connection.disconnect();
       }
       return response;
   }        
}