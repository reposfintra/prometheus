/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.aspose.cells.Cell;
import com.aspose.cells.CellValueType;
import com.aspose.cells.Cells;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import com.aspose.cells.Worksheets;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.AdmonRecursosHumanosDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ResourceBundle;

/**
 *
 * @author dvalencia
 */
public class AdmonRecursosHumanosImpl extends MainDAO implements AdmonRecursosHumanosDAO {

   private Object usuario;

    public AdmonRecursosHumanosImpl(String dataBaseName) {
        super("AdmonRecursosHumanosDAO.xml", dataBaseName);
    }

    @Override
    public String cargarTipoProveedor() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPOS_PROVEEDOR";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

   @Override
    public String guardarTipoProveedor(Usuario usuario, String codigo, String descripcion){
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_TIPO_PROVEEDOR";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codigo);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
}
    
   @Override
    public String actualizarTipoProveedor(Usuario usuario, String codigo, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_TIPO_PROVEEDOR";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codigo);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
}
    
    @Override
    public String cambiarEstadoTipoProveedor(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_TIPO_PROVEEDOR";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
}

    @Override
    public String cargarEPS() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EPS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String guardarEPS(Usuario usuario, String descripcion) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_EPS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String actualizarEPS(Usuario usuario, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_EPS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoEPS(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_EPS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }

   @Override
    public String cargarARL() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ARL";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String guardarARL(Usuario usuario, String descripcion) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_ARL";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String actualizarARL(Usuario usuario, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_ARL";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoARL(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_ARL";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }
    
    @Override
    public String cargarAFP() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_AFP";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    @Override
    public String cargarAFP_CES() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_AFP_CES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String guardarAFP(Usuario usuario, String descripcion) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_AFP";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String actualizarAFP(Usuario usuario, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_AFP";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoAFP(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_AFP";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }
    
   @Override
    public String cargarCCF() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CCF";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String guardarCCF(Usuario usuario, String descripcion) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_CCF";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String actualizarCCF(Usuario usuario, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_CCF";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoCCF(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_CCF";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }
    
   @Override
   public String cargarDependencias() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DEPENDENCIAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String guardarDependencias(Usuario usuario, String descripcion) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_DEPENDENCIAS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String actualizarDependencias(Usuario usuario, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_DEPENDENCIAS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoDependencias(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_DEPENDENCIAS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }
    
   @Override
    public String cargarCargos() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CARGOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String guardarCargos(Usuario usuario, String descripcion, String codigo) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String apoteosys = "";
        String query = "SQL_GUARDAR_CARGOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, codigo);
            ps.setString(3, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "{\"respuesta\":\"Guardado\"}";
            
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
            //apoteosys = e.getMessage();
        }
        finally {
           try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String actualizarCargos(Usuario usuario, String descripcion, String codigo, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_CARGOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, codigo);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoCargos(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_CARGOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }
    
    @Override
    public String cargarTiposContrato() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPOS_CONTRATO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String guardarTiposContrato(Usuario usuario, String descripcion) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_TIPOS_CONTRATO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String actualizarTiposContrato(Usuario usuario, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_TIPOS_CONTRATO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoTiposContrato(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_TIPOS_CONTRATO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }

    @Override
    public String guardarTiposRiesgos(Usuario usuario, String intensidad, String porcentaje, String actividades) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_TIPOS_RIESGOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, intensidad);
            ps.setDouble(2, Double.parseDouble(porcentaje));
            ps.setString(3, actividades);
            ps.setString(4, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cargarTiposRiesgos() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPOS_RIESGOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    /**
     *
     * @param usuario
     * @param intensidad
     * @param porcentaje
     * @param actividades
     * @param id
     * @return
     */
    public String actualizarTiposRiesgos(Usuario usuario, String intensidad, String porcentaje, String actividades, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_TIPOS_RIESGOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, intensidad);
            ps.setDouble(2, Double.parseDouble(porcentaje));
            ps.setString(3, actividades);
            ps.setString(4, usuario.getLogin());
            ps.setString(5, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoTiposRiesgos(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR__ESTADO_TIPOS_RIESGOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }

    @Override
    public String cargarEmpleados() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EMPLEADOS";

        JsonArray lista = null;
        JsonObject obj = new JsonObject();
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            obj.add("rows", lista);
            informacion = new Gson().toJson(obj);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

   @Override
    public String cambiarEstadoEmpleados(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_EMPLEADOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }


    public String cargarTipoDoc() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPOS_DOC";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarEstadoCivil() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ESTADO_CIVIL";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarDptoExp() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DPTO_EXP";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarCiudadExp(String dpto_expedicion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CIUDAD_EXP";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, dpto_expedicion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarNivelEstudio() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NIVEL_ESTUDIO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarDptoNac() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DPTO_NAC";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarCiudadNac(String dpto_nacimiento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CIUDAD_NAC";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, dpto_nacimiento);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarNivelesJerarquicos() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NIVELES_JERARQUICOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

   @Override
    public String guardarNivelesJerarquicos(Usuario usuario, String descripcion){
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_NIVELES_JERARQUICOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
}
    
    
    public String actualizarNivelesJerarquicos(Usuario usuario, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_NIVELES_JERARQUICOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoNivelesJerarquicos(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_NIVELES_JERARQUICOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }
    
    public String cargarProfesiones() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PROFESIONES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

   @Override
    public String guardarProfesiones(Usuario usuario, String descripcion){
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_PROFESIONES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
}
    
    
    public String actualizarProfesiones(Usuario usuario, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_PROFESIONES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoProfesiones(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_PROFESIONES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }

    @Override
    public String cargarDptoResidencia() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DPTO_RES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarCiudadResidencia(String dpto) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CIUDAD_RES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, dpto);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String guardarEmpleados(Usuario usuario, String proceso_meta, String proceso_interno, String linea_negocio, String producto, String nivel_jerarquico, String tipo_contrato, String duracion,String nombre_completo, String tipo_doc, String identificacion, 
            String sexo, String fecha_expedicion, String dpto_expedicion, String ciudad_expedicion, String libreta_militar, String salario, String fecha_ingreso, String banco, 
            String tipo_cuenta, String no_cuenta, String cargo, String riesgo, String eps, String afp, String arl, String cesantias, String ccf, String dpto, 
            String ciudad, String direccion, String barrio, String telefono, String celular, String email, String fecha_nacimiento, String dpto_nacimiento, String ciudad_nacimiento, 
            String estado_civil, String nivel_estudio, String profesion, String personas_a_cargo, String num_de_hijos, String total_grupo_familiar, String tipo_vivienda, String fecha_retiro, 
            String causal_retiro, String observaciones) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_EMPLEADOS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(proceso_meta));
            st.setInt(2, Integer.parseInt(proceso_interno));
            st.setInt(3, Integer.parseInt(linea_negocio));
            st.setInt(4, Integer.parseInt(producto));
            st.setInt(5, Integer.parseInt(nivel_jerarquico));
            st.setInt(6, Integer.parseInt(tipo_contrato));
            st.setInt(7, Integer.parseInt(duracion));
            st.setString(8, nombre_completo);
            st.setInt(9, Integer.parseInt(tipo_doc));
            st.setString(10, identificacion);
            st.setString(11, sexo);
            st.setString(12, fecha_expedicion);
            st.setString(13, dpto_expedicion);
            st.setString(14, ciudad_expedicion);
            st.setString(15, libreta_militar);
            st.setInt(16, Integer.parseInt(salario));
            st.setString(17, fecha_ingreso);
            st.setString(18, banco);
            st.setString(19, tipo_cuenta);
            st.setString(20, no_cuenta);
            st.setInt(21, Integer.parseInt(cargo));
            st.setInt(22, Integer.parseInt(riesgo));
            st.setInt(23, Integer.parseInt(eps));
            st.setInt(24, Integer.parseInt(afp));
            st.setInt(25, Integer.parseInt(arl));
            st.setInt(26, Integer.parseInt(cesantias));
            st.setInt(27, Integer.parseInt(ccf));
            st.setString(28, dpto);
            st.setString(29, ciudad);
            st.setString(30, direccion);
            st.setString(31, barrio);
            st.setString(32, telefono);
            st.setString(33, celular);
            st.setString(34, email);
            st.setString(35, fecha_nacimiento);
            st.setString(36, dpto_nacimiento);
            st.setString(37, ciudad_nacimiento);
            st.setInt(38, Integer.parseInt(estado_civil));
            st.setInt(39, Integer.parseInt(nivel_estudio));
            st.setInt(40, Integer.parseInt(profesion));
            st.setInt(41, Integer.parseInt(personas_a_cargo));
            st.setInt(42, Integer.parseInt(num_de_hijos));
            st.setInt(43, Integer.parseInt(total_grupo_familiar));
            st.setString(44, tipo_vivienda);
            st.setString(45, fecha_retiro);
            st.setString(46, causal_retiro);
            st.setString(47, observaciones);
            st.setString(48, usuario.getLogin());
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }
    
    
    
    public String cargarBancos() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_BANCOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String guardarProveedor(Usuario usuario, String nombre_completo, String tipo_doc, String identificacion, String banco, String tipo_cuenta, String no_cuenta) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_PROVEEDOR";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, identificacion);
            st.setString(2, nombre_completo);
            st.setString(3, usuario.getLogin());
            st.setInt(4, Integer.parseInt(tipo_doc));
            st.setString(5, banco);
            st.setString(6, tipo_cuenta);
            st.setString(7, no_cuenta);
            st.setString(8, identificacion);
            respuesta = st.getSql();
                //respuesta = "Guardado";
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }  
    }

    @Override
    public String guardarNit(Usuario usuario, String nombre_completo, String identificacion, String direccion, String ciudad, String dpto, String telefono, String celular, String email, String sexo, String fecha_nacimiento, String tipo_doc, String estado_civil, String ciudad_nacimiento, String libreta_militar, String ciudad_expedicion) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_HOJA_VIDA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, identificacion);
            st.setString(2, nombre_completo);
            st.setString(3, direccion);
            st.setString(4, ciudad);
            st.setString(5, dpto);
            st.setString(6, telefono);
            st.setString(7, celular);
            st.setString(8, email);
            st.setString(9, usuario.getLogin());
            st.setString(10, sexo);
            st.setString(11, fecha_nacimiento);
            st.setInt(12, Integer.parseInt(tipo_doc));
            st.setInt(13, Integer.parseInt(estado_civil));
            st.setString(14, ciudad_nacimiento);
            st.setString(15, libreta_militar);
            st.setString(16, ciudad_expedicion);
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }  
    }

    @Override
    public String actualizarEmpleados(Usuario usuario, String proceso_meta, String proceso_interno, String linea_negocio, String producto, String nivel_jerarquico, String tipo_contrato, String duracion, String nombre_completo, String tipo_doc, String identificacion, String sexo, String fecha_expedicion, String dpto_expedicion, String ciudad_expedicion, String libreta_militar, String salario, String fecha_ingreso, String banco, String tipo_cuenta, String no_cuenta, String cargo, String riesgo, String eps, String afp, String arl, String cesantias, String ccf, String dpto, String ciudad, String direccion, String barrio, String telefono, String celular, String email, String fecha_nacimiento, String dpto_nacimiento, String ciudad_nacimiento, String estado_civil, String nivel_estudio, String profesion, String personas_a_cargo, String num_de_hijos, String total_grupo_familiar, String tipo_vivienda, String fecha_retiro, String causal_retiro, String observaciones, String idEmpleado) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_EMPLEADOS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(proceso_meta));
            st.setInt(2, Integer.parseInt(proceso_interno));
            st.setInt(3, Integer.parseInt(linea_negocio));
            st.setInt(4, Integer.parseInt(producto));
            st.setInt(5, Integer.parseInt(nivel_jerarquico));
            st.setInt(6, Integer.parseInt(tipo_contrato));
            st.setInt(7, Integer.parseInt(duracion));
            st.setString(8, nombre_completo);
            st.setInt(9, Integer.parseInt(tipo_doc));
            st.setString(10, identificacion);
            st.setString(11, sexo);
            st.setString(12, fecha_expedicion);
            st.setString(13, dpto_expedicion);
            st.setString(14, ciudad_expedicion);
            st.setString(15, libreta_militar);
            st.setDouble(16, Double.parseDouble(salario));
            st.setString(17, fecha_ingreso);
            st.setString(18, banco);
            st.setString(19, tipo_cuenta);
            st.setString(20, no_cuenta);
            st.setInt(21, Integer.parseInt(cargo));
            st.setInt(22, Integer.parseInt(riesgo));
            st.setInt(23, Integer.parseInt(eps));
            st.setInt(24, Integer.parseInt(afp));
            st.setInt(25, Integer.parseInt(arl));
            st.setInt(26, Integer.parseInt(cesantias));
            st.setInt(27, Integer.parseInt(ccf));
            st.setString(28, dpto);
            st.setString(29, ciudad);
            st.setString(30, direccion);
            st.setString(31, barrio);
            st.setString(32, telefono);
            st.setString(33, celular);
            st.setString(34, email);
            st.setString(35, fecha_nacimiento);
            st.setString(36, dpto_nacimiento);
            st.setString(37, ciudad_nacimiento);
            st.setInt(38, Integer.parseInt(estado_civil));
            st.setInt(39, Integer.parseInt(nivel_estudio));
            st.setInt(40, Integer.parseInt(profesion));
            st.setInt(41, Integer.parseInt(personas_a_cargo));
            st.setInt(42, Integer.parseInt(num_de_hijos));
            st.setInt(43, Integer.parseInt(total_grupo_familiar));
            st.setString(44, tipo_vivienda);
            st.setString(45, fecha_retiro);
            st.setString(46, causal_retiro);
            st.setString(47, observaciones);
            st.setString(48, usuario.getLogin());
            st.setString(49, idEmpleado);
            //st.setInt(42, Integer.parseInt(idEmpleado));
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }

    @Override
    public String actualizarProveedor(Usuario usuario, String nombre_completo, String tipo_doc, String identificacion, String banco, String tipo_cuenta, String no_cuenta) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_PROVEEDOR";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, identificacion);
            st.setString(2, nombre_completo);
            st.setString(3, usuario.getLogin());
            st.setInt(4, Integer.parseInt(tipo_doc));
            st.setString(5, banco);
            st.setString(6, tipo_cuenta);
            st.setString(7, no_cuenta);
            st.setString(8, identificacion);
            st.setString(9, identificacion);
            respuesta = st.getSql();
                //respuesta = "Guardado";
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }

    @Override
    public String actualizarNit(Usuario usuario, String nombre_completo, String identificacion, String direccion, String ciudad, String dpto, String telefono, String celular, String email, String sexo, String fecha_nacimiento, String tipo_doc, String estado_civil, String ciudad_nacimiento, String libreta_militar, String ciudad_expedicion) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_HOJA_VIDA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, identificacion);
            st.setString(2, nombre_completo);
            st.setString(3, direccion);
            st.setString(4, ciudad);
            st.setString(5, dpto);
            st.setString(6, telefono);
            st.setString(7, celular);
            st.setString(8, email);
            st.setString(9, usuario.getLogin());
            st.setString(10, sexo);
            st.setString(11, fecha_nacimiento);
            st.setInt(12, Integer.parseInt(tipo_doc));
            st.setInt(13, Integer.parseInt(estado_civil));
            st.setString(14, ciudad_nacimiento);
            st.setString(15, libreta_militar);
            st.setString(16, ciudad_expedicion);
            st.setString(17, identificacion);
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }

    @Override
    public Object searchNombresArchivos(String directorioArchivos, String identificacion) {
        ArrayList nombresArchivos=new ArrayList();
        File dir = new File(directorioArchivos + identificacion);
        try{
            if (dir.exists()){
                String[] ficheros = dir.list();
                for (int x=0;x<ficheros.length;x++) {
                      nombresArchivos.add(ficheros[x]);
                }
            }
      
           
        }catch(Exception e){
            System.out.println("error::" + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }
     
        return nombresArchivos;
    }

    @Override
    public boolean almacenarArchivoEnCarpetaUsuario(
                                 String documento
                                ,String rutaOrigen, String rutaDestino
                                ,String nomarchivo
                               ){

        boolean swFileCopied = false;
        try{            
          
            File carpetaDestino =new File(rutaDestino);
          
            //deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);
                          
            try{
                    //Copiamos el archivo de la carpeta origen a la carpeta destino
                    InputStream in = new FileInputStream(rutaOrigen + "/" +nomarchivo);
                    OutputStream out = new FileOutputStream(rutaDestino + nomarchivo);

                    // Copy the bits from instream to outstream
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                    swFileCopied = true;
               
                   }catch(Exception k){ throw new Exception(" FILE: " + k.getMessage());}

            }catch(Exception e){
                System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
                e.printStackTrace();
            }
       
        return swFileCopied;
    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    
    public String cargarMacroprocesos() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_MACROPROCESOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarProcesos(String macroproceso) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PROCESOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, macroproceso);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    
    public String cargarLineasNegocio(String proceso) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_LINEAS_NEGOCIO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, proceso);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    public String cargarProductos(String linea_negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PRODUCTOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, linea_negocio);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    
    public String cargarTipoProveedor2() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPOS_PROVEEDOR2";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String listarEmpleados() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_EMPLEADOS";

        JsonArray lista = null;
        JsonObject obj = new JsonObject();
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            //obj.add("rows", lista);
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarIncapacidades(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_INCAPACIDADES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!status.equals("")) {
                parametro = parametro + " AND aprobado = '" + status + "'";
            }
            
                        
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarTipoNovedad() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPOS_NOVEDAD";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarEnfermedades(String cod_enfermedad) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ENFERMEDADES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cod_enfermedad);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("descripcion");
               /*objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);*/
                
            }

            //informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            //informacion = "\"respuesta\":\"ERROR\"";
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarRazon(String tipo_novedad) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_RAZON";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipo_novedad);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarNombre(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOMBRE";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("nombre_completo");
               /*objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);*/
                
            }

            //informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            //informacion = "\"respuesta\":\"ERROR\"";
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarJefes(Usuario usuario, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_JEFES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!bd.equals("fintra")) {
                parametro = parametro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = r.id_usuario)";
            }
            
            if (bd.equals("fintra")) {
                parametro = parametro + " INNER JOIN usuarios u ON (u.idusuario = r.id_usuario)";
            }
            
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return informacion;
        }
    }

   @Override

    public String guardarNovedadesIncapacidad(JsonObject info, Usuario usuario) {
    JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
        String sqlver1 = "";
        String sqlver2 = "";
        String sqlver3 = "";
	TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
		objeto = info.getAsJsonArray("info").get(0).getAsJsonObject();
		st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_NOVEDADES_INCAPACIDADES"), true);
		st.setInt(1, objeto.get("tipo_novedad").getAsInt());
                st.setString(2, objeto.get("origen").getAsString());
                st.setInt(3, objeto.get("razon").getAsInt());
	    	st.setString(4, objeto.get("identificacion").getAsString());
	    	st.setString(5, objeto.get("fechaSolicitud").getAsString());
	    	st.setString(6, objeto.get("fecha_ini").getAsString() != null ? objeto.get("fecha_ini").getAsString() : "0099-99-99");
                st.setString(7, objeto.get("fecha_fin").getAsString() != null ? objeto.get("fecha_fin").getAsString() : "0099-99-99");
                st.setString(8, objeto.get("duracion_dias").getAsString() != null ? objeto.get("duracion_dias").getAsString() : "0");
                //st.setString(9, objeto.get("hora_ini").getAsString() != null ? objeto.get("hora_ini").getAsString() : "00:00");
	    	//st.setString(10, objeto.get("hora_fin").getAsString() != null ? objeto.get("hora_fin").getAsString() : "00:00");
                //st.setString(9, objeto.get("duracion_horas").getAsString() != null ? objeto.get("duracion_horas").getAsString() : "0");
	    	st.setString(9, objeto.get("cod_enfermedad").getAsString());
	    	st.setString(10, objeto.get("jefe_directo").getAsString());
	    	st.setString(11, objeto.get("descripcion").getAsString());
                //st.setInt(15, objeto.get("dias_disfrute").getAsInt());
	    	//st.setInt(16, objeto.get("dias_a_pagar").getAsInt());
	    	//st.setString(17, objeto.get("recobro").getAsString());
	    	st.setString(12, objeto.get("numsolicitud").getAsString());
                //st.setString(19, objeto.get("proceso_nuevo").getAsString());
	    	//st.setInt(18, objeto.get("proceso_nuevo").getAsInt());
	    	st.setString(13, usuario.getLogin());
                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
		
            if (objeto.get("tipo_novedad").getAsString().equals("6")) {
		st = new StringStatement(this.obtenerSQL("SQL_TRAZABILIDAD_PROCESOS"), true);
                st.setString(1, objeto.get("identificacion").getAsString() != null ? objeto.get("identificacion").getAsString() : "");
                st.setString(2, objeto.get("id_proceso_actual").getAsString() != null ? objeto.get("id_proceso_actual").getAsString() : "");
                st.setString(3, objeto.get("proceso_nuevo").getAsString() != null ? objeto.get("proceso_nuevo").getAsString() : "");
                st.setString(4, usuario.getLogin());
                sqlver1 = st.getSql();
                System.out.println(sqlver1);
                tService.getSt().addBatch(sqlver1);

		st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_PROCESO_JEFATURA"), true);
                st.setString(1, objeto.get("proceso_nuevo").getAsString() != null ? objeto.get("proceso_nuevo").getAsString() : "");
                st.setString(2, usuario.getLogin());
                st.setString(3, objeto.get("identificacion").getAsString() != null ? objeto.get("identificacion").getAsString() : "");
                
                sqlver2 = st.getSql();
                System.out.println(sqlver2);
                tService.getSt().addBatch(sqlver2);

		st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_PROCESO_EMPLEADO"), true);
                st.setString(1, objeto.get("proceso_nuevo").getAsString() != null ? objeto.get("proceso_nuevo").getAsString() : "");
                st.setString(2, usuario.getLogin());
                st.setString(3, objeto.get("identificacion").getAsString() != null ? objeto.get("identificacion").getAsString() : "");
                sqlver3 = st.getSql();
                System.out.println(sqlver3);
                tService.getSt().addBatch(sqlver3);	
          }
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
    }
    }

    @Override
    public String actualizarNovedadesIncapacidad(Usuario usuario, String tipo_novedad,  String origen, String razon, String identificacion, String fecha_solicitud, String fecha_ini, String fecha_fin, String duracion_dias, String cod_enfermedad, String jefe_directo, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_NOVEDADES_INCAPACIDAD";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(tipo_novedad));
            ps.setString(2, origen);
            ps.setInt(3, Integer.parseInt(razon));
            ps.setString(4, identificacion);
            ps.setString(5, fecha_solicitud);
            ps.setString(6, fecha_ini);
            ps.setString(7, fecha_fin);
            ps.setInt(8, Integer.parseInt(duracion_dias));
//            ps.setString(8, hora_ini);
//            ps.setString(9, hora_fin);
//            ps.setInt(10, Integer.parseInt(duracion_horas));
            ps.setString(9, cod_enfermedad);
            ps.setString(10, jefe_directo);
            ps.setString(11, descripcion);
//            ps.setInt(14, Integer.parseInt(dias_disfrute));
//            ps.setInt(15, Integer.parseInt(dias_a_pagar));
//            ps.setString(16, recobro);
//            ps.setInt(17, Integer.parseInt(proceso_nuevo));
            ps.setString(12, usuario.getLogin());
            ps.setString(13, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cambiarEstadoNovedad(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_NOVEDAD";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }

    
    public String cargarNovedadesPorAprobar(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOVEDADES_PORAPROBAR";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!status.equals("")) {
                parametro = parametro + " AND aprobado = '" + status + "'";
            }
            
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String aprobarNovedad(Usuario usuario, String aprobado, String comentario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_APROBAR_NOVEDADES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, aprobado);
            ps.setString(2, comentario);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cargarNovedadesAprobadas(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerada) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOVEDADES_APROBADAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerada.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerada + "'";
            }
            
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String vistoBuenoNovedadIncapacidad(Usuario usuario, String observaciones, String id, Double valor_recobro_eps, Double valor_recobro_arl) {
        
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_VISTOBUENO_INCAPACIDADES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, observaciones);
            ps.setDouble(2, valor_recobro_eps);
            ps.setDouble(3, valor_recobro_arl);
            ps.setString(4, usuario.getLogin());
            ps.setString(5, usuario.getLogin());
            ps.setString(6, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    public Object getNombresArchivosNovedades(String directorioArchivos, String identificacion) {
        ArrayList nombresArchivos=new ArrayList();
        File dir = new File(directorioArchivos + identificacion);
        try{
            if (dir.exists()){
                String[] ficheros = dir.list();
                for (int x=0;x<ficheros.length;x++) {
                      nombresArchivos.add(ficheros[x]);
                }
            }
      
           
        }catch(Exception e){
            System.out.println("error::" + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }
     
        return nombresArchivos;
    }

    @Override
    public boolean almacenarArchivoEnCarpetaUsuarioNovedades(
                                 String documento
                                ,String rutaOrigen, String rutaDestino
                                ,String nomarchivo
                               ){

        boolean swFileCopied = false;
        try{            
          
            File carpetaDestino =new File(rutaDestino);
          
            //deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);
                          
            try{
                    //Copiamos el archivo de la carpeta origen a la carpeta destino
                    InputStream in = new FileInputStream(rutaOrigen + "/" +nomarchivo);
                    OutputStream out = new FileOutputStream(rutaDestino + nomarchivo);

                    // Copy the bits from instream to outstream
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                    swFileCopied = true;
               
                   }catch(Exception k){ throw new Exception(" FILE: " + k.getMessage());}

            }catch(Exception e){
                System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
                e.printStackTrace();
            }
       
        return swFileCopied;
    }

//    @Override
//    public String buscarEmpleado(String identificacion, Usuario usuario) {
//        Connection con = null;
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        String query = "SQL_CARGAR_INFO_EMPLEADO";
//
//        JsonArray lista = null;
//        JsonObject objetoJson = null;
//        String informacion = "";
//        try {
//            con = this.conectarJNDI();
//            ps = con.prepareStatement(this.obtenerSQL(query));
//            ps.setString(1, identificacion);
//            rs = ps.executeQuery();
//            lista = new JsonArray();
//            while (rs.next()) {
//                 // informacion = rs.getString("nombre_completo");
//               objetoJson = new JsonObject();
//                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
//                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
//                }
//                lista.add(objetoJson);
//                
//            }
//
//            informacion = new Gson().toJson(lista);
//        } catch (Exception e) {
//            e.printStackTrace();
//            //informacion = "\"respuesta\":\"ERROR\"";
//            informacion = "ERROR";
//        }
//        finally {
//            try {
//                if (con != null) {
//                    this.desconectar(con);
//                }
//                if (ps != null) {
//                    ps.close();
//                }
//            } catch (SQLException ex) {
//                System.out.println(ex.getMessage());
//                ex.printStackTrace();
//            }
//    return informacion;
//    }
//    
//}

    @Override
    public String novedadTramitada(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_NOVEDAD_TRAMITADA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String calcularDiasFestivos(String fechaIni, String fechaFin) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CALCULAS_DIAS_HABILES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, fechaFin);
            ps.setString(2, fechaIni);
            ps.setString(3, fechaIni);
            ps.setString(4, fechaFin);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("duracion_dias");
           }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String novedadNoTramite(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_NOVEDAD_NO_TRAMITE";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cargarTiposHorasExtras() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPOS_HORAS_EXTRAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarHorasExtras(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_HORAS_EXTRAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    
    public String guardarHorasExtras(JsonObject info, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_HORAS_EXTRAS";
        JsonObject objeto = new JsonObject();
        try {
            objeto = info.getAsJsonArray("info").get(0).getAsJsonObject();
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, objeto.get("tipo_hora").getAsInt());
            ps.setString(2, objeto.get("identificacion").getAsString());
            ps.setString(3, objeto.get("fecha_solicitud").getAsString());
            ps.setString(4, objeto.get("fecha_trabajo").getAsString());
            ps.setString(5, objeto.get("hora_ini").getAsString());
            ps.setString(6, objeto.get("hora_fin").getAsString());
            ps.setInt(7, objeto.get("duracion_horas").getAsInt());
            ps.setString(8, objeto.get("jefe_directo").getAsString());
            ps.setString(9, objeto.get("descripcion").getAsString());
            ps.setString(10, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

   @Override
        public String actualizarHorasExtras(Usuario usuario, String tipo_hora, String identificacion, String fecha_solicitud, String fecha_trabajo, String hora_ini, String hora_fin, String duracion_horas, String jefe_directo, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_HORAS_EXTRAS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(tipo_hora));
            ps.setString(2, identificacion);
            ps.setString(3, fecha_solicitud);
            ps.setString(4, fecha_trabajo);
            ps.setString(5, hora_ini);
            ps.setString(6, hora_fin);
            ps.setInt(7, Integer.parseInt(duracion_horas));
            ps.setString(8, jefe_directo);
            ps.setString(9, descripcion);
            ps.setString(10, usuario.getLogin());
            ps.setString(11, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }
        
       @Override
    public String cambiarEstadoHorasExtras(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_HORAS_EXTRAS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
        }
    return respuesta;
    }
    }     
    
    public String cargarHorasExtrasPorAprobar(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_HORAS_EXTRAS_PORAPROBAR";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    @Override
    public String aprobarHorasExtras(Usuario usuario, String aprobado, String comentario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_APROBAR_HORAS_EXTRAS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, aprobado);
            ps.setString(2, comentario);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cargarHorasExtrasAprobadas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_HORAS_EXTRAS_APROBADAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String vistoBuenoHorasExtras(Usuario usuario, String remunerado, String observaciones, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_VISTOBUENO_HORAS_EXTRAS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, remunerado);
            ps.setString(2, observaciones);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, usuario.getLogin());
            ps.setString(5, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

   @Override
    public String cargarNovedadesAprobadasUsuario(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOVEDADES_APROBADAS_USUARIO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
   @Override
    public String cargarOpcionesMenu(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OPCIONES_MENU";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1,"%"+usuario+"%");
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
   @Override
    public JsonObject getInfoCuentaEnvioCorreo() {
        Connection con = null;
        PreparedStatement ps = null;
        con = null;
        ps = null;
        ResultSet rs = null;
        String query = "GET_INFO_CORREO_REMITENTE";
        JsonObject obj = null;

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("servidor", rs.getString("servidor"));
                fila.addProperty("puerto", rs.getString("puerto"));
                fila.addProperty("usuario", rs.getString("usuario"));
                fila.addProperty("clave", rs.getString("clave"));
                fila.addProperty("bodyMessage", rs.getString("bodyMessage"));
            }
            obj = fila;

        } catch (Exception e) {
            System.out.println("Error en getInfoCuentaEnvioCorreo: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getInfoCuentaEnvioCorreo: " + e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return obj;
        }
    }
    
   @Override
    public String getInfoCuentaEnvioCorreoDestino(String destinatario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EMAIL_DESTINO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, destinatario);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("email");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
   @Override
    public String cargarNombreTipoNovedad(String tipo_novedad) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOMBRE_TIPO_NOVEDAD";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipo_novedad);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("descripcion");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
       
    public String getInfoCuentaEnvioCorreoDestino2() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EMAIL_DESTINO2";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("email");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    @Override
    public String cargarNombreJefeDirecto(String jefe_directo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOMBRE_JEFE";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, jefe_directo);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("nombre");
            }
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String getInfoCuentaEnvioCorreoDestino3(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EMAIL_DESTINO3";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("email");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

 
    public String cargarNombreTipoHora(String tipo_hora) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOMBRE_TIPO_HORA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipo_hora);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("descripcion");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarListaEnfermedades() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_LISTA_ENFERMEDADES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String numeroSolicitudNovedad() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NUMERO_SOLICITUD_NOVEDADES";
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = rs.getString("numsolicitud");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String pocesoActualTrabajador(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PROCESO_ACTUAL_TRABAJADOR";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String pocesoActual(Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PROCESOS_ACTUALES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return informacion;
        }
    }
    
    
    public String cargarTipoIncapacidad() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_INCAPACIDAD";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarEPSoARL(String origen) {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EPS_ARL";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, origen);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }

}
    
   @Override
    public String insertarTrazabilidadEmpleados(Usuario usuario, String proceso_meta, String proceso_interno, String linea_negocio, String producto, String nivel_jerarquico, String tipo_contrato, String duracion, String nombre_completo, String tipo_doc, String identificacion, String sexo, String fecha_expedicion, String dpto_expedicion, String ciudad_expedicion, String libreta_militar, String salario, String fecha_ingreso, String banco, String tipo_cuenta, String no_cuenta, String cargo, String riesgo, String eps, String afp, String arl, String cesantias, String ccf, String dpto, String ciudad, String direccion, String barrio, String telefono, String celular, String email, String fecha_nacimiento, String dpto_nacimiento, String ciudad_nacimiento, String estado_civil, String nivel_estudio, String profesion, String personas_a_cargo, String num_de_hijos, String total_grupo_familiar, String tipo_vivienda, String fecha_retiro, String causal_retiro, String observaciones) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_INSERTAR_TRAZABILIDAD_EMPLEADOS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(proceso_meta));
            st.setInt(2, Integer.parseInt(proceso_interno));
            st.setInt(3, Integer.parseInt(linea_negocio));
            st.setInt(4, Integer.parseInt(producto));
            st.setInt(5, Integer.parseInt(nivel_jerarquico));
            st.setInt(6, Integer.parseInt(tipo_contrato));
            st.setInt(7, Integer.parseInt(duracion));
            st.setString(8, nombre_completo);
            st.setInt(9, Integer.parseInt(tipo_doc));
            st.setString(10, identificacion);
            st.setString(11, sexo);
            st.setString(12, fecha_expedicion);
            st.setString(13, dpto_expedicion);
            st.setString(14, ciudad_expedicion);
            st.setString(15, libreta_militar);
            st.setDouble(16, Double.parseDouble(salario));
            st.setString(17, fecha_ingreso);
            st.setString(18, banco);
            st.setString(19, tipo_cuenta);
            st.setString(20, no_cuenta);
            st.setInt(21, Integer.parseInt(cargo));
            st.setInt(22, Integer.parseInt(riesgo));
            st.setInt(23, Integer.parseInt(eps));
            st.setInt(24, Integer.parseInt(afp));
            st.setInt(25, Integer.parseInt(arl));
            st.setInt(26, Integer.parseInt(cesantias));
            st.setInt(27, Integer.parseInt(ccf));
            st.setString(28, dpto);
            st.setString(29, ciudad);
            st.setString(30, direccion);
            st.setInt(31, Integer.parseInt(barrio));
            st.setString(32, telefono);
            st.setString(33, celular);
            st.setString(34, email);
            st.setString(35, fecha_nacimiento);
            st.setString(36, dpto_nacimiento);
            st.setString(37, ciudad_nacimiento);
            st.setInt(38, Integer.parseInt(estado_civil));
            st.setInt(39, Integer.parseInt(nivel_estudio));
            st.setInt(40, Integer.parseInt(profesion));
            st.setInt(41, Integer.parseInt(personas_a_cargo));
            st.setInt(42, Integer.parseInt(num_de_hijos));
            st.setInt(43, Integer.parseInt(total_grupo_familiar));
            st.setString(44, tipo_vivienda);
            st.setString(45, fecha_retiro);
            st.setString(46, causal_retiro);
            st.setString(47, observaciones);
            st.setString(48, usuario.getLogin());
            //st.setInt(42, Integer.parseInt(idEmpleado));
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }
    
//    public String cargarTrazabilidad(String identificacion2) {
//        Connection con = null;
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        String query = "SQL_CARGAR_TRAZABILIDAD";
//
//        JsonArray lista = null;
//        JsonObject obj = new JsonObject();
//        JsonObject objetoJson = null;
//        String informacion = "{}";
//        try {
//            con = this.conectarJNDI();
//            ps = con.prepareStatement(this.obtenerSQL(query));
//            ps.setString(1, identificacion2);
//            rs = ps.executeQuery();
//            lista = new JsonArray();
//            while (rs.next()) {
//                objetoJson = new JsonObject();
//                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
//                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
//                }
//                lista.add(objetoJson);
//            }
//            obj.add("rows", lista);
//            informacion = new Gson().toJson(obj);
//        } catch (Exception e) {
//            e.printStackTrace();
//            informacion = "\"respuesta\":\"ERROR\"";
//        }
//        finally {
//            try {
//                if (con != null) {
//                    this.desconectar(con);
//                }
//                if (ps != null) {
//                    ps.close();
//                }
//            } catch (SQLException ex) {
//                System.out.println(ex.getMessage());
//                ex.printStackTrace();
//            }
//    return informacion;
//    }
//    }
    
    public String cargarTrazabilidad(String identificacion2) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TRAZABILIDAD";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!identificacion2.equals("")) {
                parametro = parametro + " WHERE identificacion = '" + identificacion2 + "'";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    @Override
    public String cargarOpcionesMenuGestion(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OPCIONES_MENU2";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1,"%"+usuario+"%");
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }


    @Override
    public String cargarIncapacidades2(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, Usuario usuario, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_INCAPACIDADES2";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
                        
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta = consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    public String cargarIncapacidades3(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_INCAPACIDADES3";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerado.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerado + "'";
            }
            
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    @Override
    public String cargarOpcionesMenuVistoBueno(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OPCIONES_MENU3";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1,"%"+usuario+"%");
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarTipoVacaciones() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_VACACIONES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarVacaciones(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_VACACIONES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!status.equals("")) {
                parametro = parametro + " AND aprobado = '" + status + "'";
            }
            
                        
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String calcularDiasSinFestivos(String fechaIni, String duracion_dias) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CALCULAS_DIAS_SIN_FESTIVOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, fechaIni);
            ps.setInt(2, Integer.parseInt(duracion_dias));
            ps.setString(3, fechaIni);
            ps.setString(4, fechaIni);
            ps.setInt(5, Integer.parseInt(duracion_dias));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("fecha_fin");
           }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarSaldo(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_SALDO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("saldo");
               
            }
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarPasivoVacacional(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PASIVO_VACACIONAL";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
//            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
//                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
//            }
//            
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
                        
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    public String cargarFechaIngreso(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_FECHA_INGRESO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("fecha_ingreso");
               
            }
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarSaldoFinal(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_SALDO_INICIAL";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("saldo_final");
               
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarNombreEPS(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOMBRE_EPS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("eps");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarNombreARL(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOMBRE_EPS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("arl");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
 
      public String cargarSalario(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOMBRE_EPS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("salario");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
      
   @Override
    public String UpCP(String document_type){
      Connection con = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      String query = "UP_SERIES";
      JsonArray lista = null;
      JsonObject objetoJson = null;
      String informacion = "";
     try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, document_type);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString(1);;
            }

        }   catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
}  
      
    public String ingresarCXCRecobro(String documento, String nit, String descripcion, Double valor, String numsolicitud){
        String query = "SQL_INSERTAR_CXC_RECOBRO";
        StringStatement st = null;
        String sql = "";
        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo documento
            st.setString(2,documento); //document type para generar el numero de factura
            st.setString(3, nit); //nit
            st.setString(4, nit); //nit para obtener el codcli
            st.setString(5, "TR"); //concepto
            st.setString(6, descripcion); //descripcion 
            st.setString(7, valor + ""); //valor_factura
            st.setInt(8, 1); //valor_tasa
            st.setString(9, "PES"); //moneda
            st.setInt(10, 1); //cantidad_items
            st.setString(11, "CREDITO"); //forma_pago
            st.setString(12, "OP"); //agencia_facturacion
            st.setString(13, "BQ"); //agencia_cobro
            st.setString(14, "COL"); //base
            st.setString(15, "IN");//cmc
            st.setString(16, "OP"); //agencia_impresion
            st.setString(17, valor + ""); //valor_facturame
            st.setString(18, valor + ""); //valor_saldo
            st.setString(19, valor + ""); //valor_saldome
            st.setString(20, numsolicitud); //referencia1
            st.setString(21, "ADMIN"); //creation_user
            sql = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            sql = e.getMessage();
        }
        finally {
            return sql;
    }
    }

    @Override
    public String ingresarDetalleCXCRecobro(String documento, String nit, String descripcion, Double valor) {
        String query = "SQL_INSERTAR_DET_CXC_RECOBRO";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo_documento
            st.setString(2, documento); //document type para generar el numero de factura
            st.setInt(3, 1); //item
            st.setString(4, nit); //nit
            st.setString(5, "TR"); //concepto
            st.setString(6, descripcion); //descripcion
            st.setInt(7, 1); //cantidad
            st.setString(8, valor + ""); //valor_unitario
            st.setString(9, valor + ""); //valor_item
            st.setInt(10, 1); //valor_tasa
            st.setString(11, "PES"); //moneda
            st.setString(12, "ADMIN"); //creation_user
            st.setString(13, valor + ""); //valor_unitariome
            st.setString(14, valor + ""); //valor_itemme
            st.setString(15, "COL"); //base
            st.setString(16, "I010080034255"); //Cuenta (codigo_cuenta_contable)
            sql = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            sql = e.getMessage();
        }
        finally {
            return sql;
    }
  }
    
    
//    public String actualizarPeriodoActual(String identificacion) {
//         Connection con = null;
//       PreparedStatement ps = null;
//       String respuesta ="";
//        String query = "SQL_UPDATE_PERIODO";
//        try {
//            con = this.conectarJNDI();
//            ps = con.prepareStatement(this.obtenerSQL(query));
//            ps.setString(1, identificacion);
//            ps.executeUpdate();
//            respuesta = "Actualizado";
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            respuesta = "ERROR";
//        }finally {
//            try {
//                if (ps != null) {
//                    ps.close();
//                }
//                if (con != null) {
//                    this.desconectar(con);
//                }
//            } catch (SQLException e) {
//                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
//            }
//        }
//        return respuesta;
//    }

    @Override
    public String guardarNovedadesVacaciones(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
        String sqlver1 = "";
        String sqlver2 = "";
        String sqlver3 = "";
	TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
		objeto = info.getAsJsonArray("info").get(0).getAsJsonObject();
		st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_NOVEDADES_VACACIONES"), true);
		st.setInt(1, objeto.get("tipo_novedad").getAsInt());
                st.setString(2, objeto.get("identificacion").getAsString());
	    	st.setString(3, objeto.get("fechaSolicitud").getAsString());
	    	st.setString(4, objeto.get("fecha_ini").getAsString() != null ? objeto.get("fecha_ini").getAsString() : "0099-99-99");
                st.setString(5, objeto.get("fecha_fin").getAsString() != null ? objeto.get("fecha_fin").getAsString() : "0099-99-99");
                st.setString(6, objeto.get("duracion_dias").getAsString() != null ? objeto.get("duracion_dias").getAsString() : "0");
                st.setString(7, objeto.get("dias_compensados").getAsString());
	    	st.setString(8, objeto.get("jefe_directo").getAsString());
	    	st.setString(9, objeto.get("descripcion").getAsString());
                st.setString(10, objeto.get("numsolicitud").getAsString());
                st.setString(11, usuario.getLogin());
                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
		
//            if (objeto.get("tipo_novedad").getAsString().equals("6")) {
//		st = new StringStatement(this.obtenerSQL("SQL_TRAZABILIDAD_PROCESOS"), true);
//                st.setString(1, objeto.get("identificacion").getAsString() != null ? objeto.get("identificacion").getAsString() : "");
//                st.setString(2, objeto.get("id_proceso_actual").getAsString() != null ? objeto.get("id_proceso_actual").getAsString() : "");
//                st.setString(3, objeto.get("proceso_nuevo").getAsString() != null ? objeto.get("proceso_nuevo").getAsString() : "");
//                st.setString(4, usuario.getLogin());
//                sqlver1 = st.getSql();
//                System.out.println(sqlver1);
//                tService.getSt().addBatch(sqlver1);
//
//		st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_PROCESO_JEFATURA"), true);
//                st.setString(1, objeto.get("proceso_nuevo").getAsString() != null ? objeto.get("proceso_nuevo").getAsString() : "");
//                st.setString(2, usuario.getLogin());
//                st.setString(3, objeto.get("identificacion").getAsString() != null ? objeto.get("identificacion").getAsString() : "");
//                
//                sqlver2 = st.getSql();
//                System.out.println(sqlver2);
//                tService.getSt().addBatch(sqlver2);
//
//		st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_PROCESO_EMPLEADO"), true);
//                st.setString(1, objeto.get("proceso_nuevo").getAsString() != null ? objeto.get("proceso_nuevo").getAsString() : "");
//                st.setString(2, usuario.getLogin());
//                st.setString(3, objeto.get("identificacion").getAsString() != null ? objeto.get("identificacion").getAsString() : "");
//                sqlver3 = st.getSql();
//                System.out.println(sqlver3);
//                tService.getSt().addBatch(sqlver3);	
//          }
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
    }
    }

    public String aprobarSolicitudVacaciones(Usuario usuario, String comentario, String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_APROBAR_NOVEDADES";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, comentario);
            st.setString(2, usuario.getLogin());
            st.setInt(3, Integer.parseInt(id));
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }

    @Override
    public String cargarVacacionesPorAprobar(String fechaInicio, String fechaFin, String identificacion,String status, String tipo_novedad, Usuario usuario, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_VACACIONES_POR_APROBAR";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!status.equals("")) {
                parametro = parametro + " AND aprobado = '" + status + "'";
            }
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String rechazarSolicitudVacaciones(Usuario usuario, String comentario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_RECHAZAR_NOVEDADES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, comentario);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cargarVacacionesPorVistoBueno(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_VACACIONES_POR_VISTO_BUENO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerado.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerado + "'";
            }
            
            if (!bd.equals("fintra")) {
                filtro = filtro + " LEFT JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String vistoBuenoNovedadVacaciones(Usuario usuario, String observaciones, String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_VISTOBUENO_NOVEDADES";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, observaciones);
            st.setString(2, usuario.getLogin());
            st.setString(3, usuario.getLogin());
            st.setInt(4, Integer.parseInt(id));
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
}

    @Override
    public String actualizarPasivo(Usuario usuario, String identificacion, String duracion_dias, String dias_compensados) {
         StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_PASIVO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(duracion_dias));
            st.setInt(2, Integer.parseInt(dias_compensados));
            st.setString(3, usuario.getLogin());
            st.setString(4, identificacion);
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }

    @Override
    public String calcularAcumuladoPasivo(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ACUMULADO_PASIVO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String mostrarPeriodo(String identificacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOSTRAR_PERIODO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("periodo");
               
            }
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarPermisos(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PERMISOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!status.equals("")) {
                parametro = parametro + " AND aprobado = '" + status + "'";
            }
            
                        
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
        public String cargarTipoPermisos() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_PERMISOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
        
    @Override
    public String guardarNovedadesPermisos(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
        String sqlver1 = "";
        String sqlver2 = "";
        String sqlver3 = "";
	TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
		objeto = info.getAsJsonArray("info").get(0).getAsJsonObject();
		st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_NOVEDADES_PERMISOS"), true);
		st.setInt(1, objeto.get("tipo_novedad").getAsInt());
                st.setInt(2, objeto.get("razon").getAsInt());
                st.setString(3, objeto.get("identificacion").getAsString());
	    	st.setString(4, objeto.get("fechaSolicitud").getAsString());
	    	st.setString(5, objeto.get("fecha_ini").getAsString() != null ? objeto.get("fecha_ini").getAsString() : "0099-99-99");
                st.setString(6, objeto.get("fecha_fin").getAsString() != null ? objeto.get("fecha_fin").getAsString() : "0099-99-99");
                st.setString(7, objeto.get("duracion_dias").getAsString() != null ? objeto.get("duracion_dias").getAsString() : "0");
                st.setString(8, objeto.get("hora_ini").getAsString() != null ? objeto.get("hora_ini").getAsString() : "00:00");
	    	st.setString(9, objeto.get("hora_fin").getAsString() != null ? objeto.get("hora_fin").getAsString() : "00:00");
                st.setString(10, objeto.get("duracion_horas").getAsString() != null ? objeto.get("duracion_horas").getAsString() : "0");
                st.setString(11, objeto.get("jefe_directo").getAsString());
	    	st.setString(12, objeto.get("descripcion").getAsString());
                st.setString(13, objeto.get("numsolicitud").getAsString());
                st.setString(14, usuario.getLogin());
                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
		

            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
    }
    }    
    

    @Override
    public String cargarPermisosPorAprobar(String fechaInicio, String fechaFin, String identificacion,String status, String tipo_novedad, Usuario usuario, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PERMISOS_POR_APROBAR";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!status.equals("")) {
                parametro = parametro + " AND aprobado = '" + status + "'";
            }
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
   @Override
        public String aprobarSolicitudPermisos(Usuario usuario, String comentario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_APROBAR_NOVEDADES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, comentario);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }
       
    @Override    
    public String rechazarSolicitudPermisos(Usuario usuario, String comentario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_RECHAZAR_NOVEDADES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, comentario);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }
    
    public String cargarPermisosPorVistoBueno(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PERMISOS_POR_VISTO_BUENO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerado.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerado + "'";
            }
            
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
 
    public String vistoBuenoNovedadPermisos(Usuario usuario, String observaciones, String pagar, String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_VISTOBUENO_PERMISOS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, observaciones);
            st.setString(2, usuario.getLogin());
            st.setString(3, usuario.getLogin());
            st.setString(4, pagar);
            st.setInt(5, Integer.parseInt(id));
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
}

    @Override
    public String cargarCausalesRetiro() {
              Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CAUSALES_RETIRO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarBarrios(String ciudad) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_BARRIO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, ciudad);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarTipoLicencias() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_LICENCIAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String cargarClasesPermiso() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CLASES_PERMISO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String guardarNovedadesLicencias(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
        String sqlver1 = "";
        String sqlver2 = "";
        String sqlver3 = "";
	TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
		objeto = info.getAsJsonArray("info").get(0).getAsJsonObject();
		st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_NOVEDADES_LICENCIAS"), true);
		st.setInt(1, objeto.get("tipo_novedad").getAsInt());
                st.setInt(2, objeto.get("razon").getAsInt());
                st.setString(3, objeto.get("identificacion").getAsString());
	    	st.setString(4, objeto.get("fechaSolicitud").getAsString());
	    	st.setString(5, objeto.get("fecha_ini").getAsString() != null ? objeto.get("fecha_ini").getAsString() : "0099-99-99");
                st.setString(6, objeto.get("fecha_fin").getAsString() != null ? objeto.get("fecha_fin").getAsString() : "0099-99-99");
                st.setString(7, objeto.get("duracion_dias").getAsString() != null ? objeto.get("duracion_dias").getAsString() : "0");
                st.setString(8, objeto.get("jefe_directo").getAsString());
	    	st.setString(9, objeto.get("descripcion").getAsString());
                st.setString(10, objeto.get("numsolicitud").getAsString());
                st.setString(11, usuario.getLogin());
                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
		

            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
    }
    }    

    @Override
    public String cargarClasesLicencias(String tipo_novedad) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CLASES_LICENCIAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipo_novedad);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarLicencias(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, Usuario usuario, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_LICENCIAS_LEY_MARIA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!status.equals("")) {
                parametro = parametro + " AND aprobado = '" + status + "'";
            }
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarLicenciasPorAprobar(String fechaInicio, String fechaFin, String identificacion, String status, String tipo_novedad, Usuario usuario, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_LICENCIAS2";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!status.equals("")) {
                parametro = parametro + " AND aprobado = '" + status + "'";
            }
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarLicenciasPorVistoBueno(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_LICENCIAS_POR_VISTO_BUENO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerado.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerado + "'";
            }
            
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String vistoBuenoNovedadLicencia(Usuario usuario, String observaciones, String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_VISTOBUENO_LICENCIA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, observaciones);
            st.setString(2, usuario.getLogin());
            st.setString(3, usuario.getLogin());
            st.setInt(4, Integer.parseInt(id));
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }

    @Override
    public String cargarTipoOtrasLicencias() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OTRO_TIPO_LICENCIAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarOtrasLicencias(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String status, Usuario usuario, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OTRAS_LICENCIAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!status.equals("")) {
                parametro = parametro + " AND aprobado = '" + status + "'";
            }
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarNombreTipoLicencia(String razon) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NOMBRE_TIPO_LICENCIA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, razon);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("descripcion");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String cargarOtrasLicenciasPorAprobar(String fechaInicio, String fechaFin, String identificacion, String status, String tipo_novedad, Usuario usuario, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OTRAS_LICENCIAS_POR_APROBAR";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!status.equals("")) {
                parametro = parametro + " AND aprobado = '" + status + "'";
            }
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String aprobarSolicitudLicencias(Usuario usuario, String comentario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_APROBAR_NOVEDADES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, comentario);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String rechazarSolicitudLicencias(Usuario usuario, String comentario, String id) {
                Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_RECHAZAR_NOVEDADES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, comentario);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }

    @Override
    public String cargarOtrasLicenciasPorVistoBueno(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OTRAS_LICENCIAS_POR_VISTO_BUENO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerado.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerado + "'";
            }
            
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String listarIncapacidades(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_INCAPACIDADES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerado.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerado + "'";
            }
            
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String listarVacaciones(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_VACACIONES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerado.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerado + "'";
            }
            
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String listarPermisos(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_PERMISOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerado.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerado + "'";
            }
            
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public String listarLicencias(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd) {
                Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_LICENCIAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerado.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerado + "'";
            }
            
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public String listarOtrasLicencias(String fechaInicio, String fechaFin, String identificacion, String tipo_novedad, String remunerado, String bd) {
                Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_OTRAS_LICENCIAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND fecha_solicitud::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!tipo_novedad.equals("")) {
                parametro = parametro + " AND  n.id_tipo = '" + tipo_novedad + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND cc_empleado = '" + identificacion + "'";
            }
            
            if (!remunerado.equals("")) {
                parametro = parametro + " AND remunerada = '" + remunerado + "'";
            }
            
            if (!bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuario_view_dblink u ON (u.idusuario = n.user_autoriza)";
            }
            
            if (bd.equals("fintra")) {
                filtro = filtro + " INNER JOIN usuarios u ON (u.idusuario = n.user_autoriza)";
            }
            
                                    
            consulta = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            consulta =consulta.replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }

    @Override
    public boolean guardarCargosTablaTemp(String bd) {
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet rs = null;
        boolean retorno = false;
        String respuesta = "";
        String query = "SQL_GUARDAR_CARGOS_TEMP";
        
        try {
            con = this.conectarJNDI();
            String sql = obtenerSQL(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getBoolean("retorno");
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
           try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return retorno;
        }
    }

  

    public boolean guardarEmpleadosTablaTemp(String bd) {
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet rs = null;
        boolean retorno = false;
        String respuesta = "";
        String query = "SQL_GUARDAR_EMPLEADOS_TEMP";
        
        try {
            con = this.conectarJNDI();
            String sql = obtenerSQL(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getBoolean("retorno");
            }
//            loadTerceroEmp(bd);
            
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
           try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return retorno;
        }
    }
    
    @Override
    public boolean leerArchivoNomina(InputStream excelFileStream, String usuario, String dstrct) throws IOException, SQLException {
        Connection conn = null;
        PreparedStatement psEmpleados = null;
        PreparedStatement psConceptos = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        LinkedList empleados = new LinkedList();
        String[] datos = null;
            
        Workbook workbook = new Workbook();
        workbook.open(excelFileStream);
        
        Worksheets sheets = workbook.getWorksheets();
        Worksheet base = sheets.getSheet("Base");
        Cells celdas = base.getCells();
        
        try {
            conn = this.conectarJNDI();
            conn.setAutoCommit(false);
            
            psEmpleados = conn.prepareStatement(this.obtenerSQL("ELIMINAR_EMPLEADOS"));
            psEmpleados.executeUpdate();
            
            psEmpleados = conn.prepareStatement(this.obtenerSQL("INSERTAR_EMPLEADOS"));            

            for (int i = 1; i <= celdas.getMaxDataRow(); i++) {
                psEmpleados.setString(1, dstrct);
                for (int j = 0; j <= celdas.getMaxDataColumn(); j++) {
                    Cell celdaActual = celdas.getCell(i, j);

                    switch (celdaActual.getValueType()) {
                        case CellValueType.STRING:
                        case CellValueType.RICH_TEXT_STRING:
                            psEmpleados.setString(j + 2, celdaActual.getStringValue().trim());
                            break;
                        case CellValueType.INT:
                        case CellValueType.DOUBLE:
                            if (j != 4 ) {
                                psEmpleados.setString(j + 2, Integer.toString((int) celdaActual.getIntValue()));
                            } else {
                                psEmpleados.setString(j + 2, Integer.toString((int) Math.round((celdaActual.getDoubleValue()) * 100)) + "%");
                            }
                            break;
                    }
                }
                psEmpleados.setString(9 ,usuario);
                psEmpleados.addBatch();
            }
            psEmpleados.executeBatch();
            
            psConceptos = conn.prepareStatement(this.obtenerSQL("ELIMINAR_CONCEPTOS"));
            psConceptos.executeUpdate();
            
            psConceptos = conn.prepareStatement(this.obtenerSQL("INSERTAR_CONCEPTOS"));

            Worksheet conceptos = sheets.getSheet("Conceptos");
            celdas = conceptos.getCells();
            
            for (int i = 1; i <= celdas.getMaxDataRow(); i++) {
                psConceptos.setString(1, dstrct);
                for (int j = 0; j <= celdas.getMaxDataColumn(); j++) {
                    Cell celdaActual = celdas.getCell(i, j);

                    switch (celdaActual.getValueType()) {
                        case CellValueType.STRING:
                        case CellValueType.RICH_TEXT_STRING:
                            psConceptos.setString(j + 2, celdaActual.getStringValue());
                            break;
                        case CellValueType.INT:
                            psConceptos.setString(j + 2, Integer.toString((int) celdaActual.getIntValue()));
                            break;
                    }
                }
                psConceptos.setString(4, usuario);
                psConceptos.addBatch();
            }
            psConceptos.executeBatch();
            conn.commit();
                                   
            return true;
       } finally {
            if (psEmpleados != null) psEmpleados.close();
            if (psConceptos != null) psConceptos.close();
            if (ps != null) ps.close();
            if (conn != null) conn.close();
        }
    } 
    
    public boolean generarArchivoDistribucion(boolean periodos, String periodo, String usuario) throws IOException, SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = this.conectarJNDI();
            if (periodos) {
                ps = conn.prepareStatement(this.obtenerSQL("CRUCE_EMPLEADOS_CONCEPTOS_PERIODOS"));
                periodo = Util.fechaActual(periodo);
            } else {
                ps = conn.prepareStatement(this.obtenerSQL("CRUCE_EMPLEADOS_CONCEPTOS"));
                ps.setString(1, periodo);
            }

            rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();

            // Genera el archivo Excel
            String ruta = ResourceBundle.getBundle("com/tsp/util/connectionpool/db").getString("ruta") + File.separator + "exportar" + File.separator + "migracion" + File.separator + usuario.toUpperCase();
            File folder = new File(ruta);
            if (!folder.exists()) {
                folder.mkdirs();
            }

            Workbook wb = new Workbook();
            Worksheets sheets = wb.getWorksheets();
            Worksheet st = sheets.getActiveSheet();
            Cells celdas = st.getCells();

            celdas.insertRows(0, 1);

            celdas.getCell(0, 0).setValue("cuenta");
            celdas.getCell(0, 1).setValue("nombre");
            celdas.getCell(0, 2).setValue("centro_origen");
            celdas.getCell(0, 3).setValue("tercero_origen");
            celdas.getCell(0, 4).setValue("nombre");
            celdas.getCell(0, 5).setValue("porcentaje");
            celdas.getCell(0, 6).setValue("centro_destino");
            celdas.getCell(0, 7).setValue("tercero_destino");

            int i = 1;
            while (rs.next()) {
                for (int j = 1; j <= rsmd.getColumnCount(); j++) {
                    Cell celdaActual = celdas.getCell(i, j - 1);
                    celdaActual.setValue(rs.getString(j));
                }
                i++;
            }
            wb.save(ruta + File.separator + "distribucion" + periodo + ".xls");
            return true;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    @Override
    public String editarNovedadesPermisos(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
        String sqlver1 = "";
        String sqlver2 = "";
        String sqlver3 = "";
	TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
		objeto = info.getAsJsonArray("info").get(0).getAsJsonObject();
		st = new StringStatement(this.obtenerSQL("SQL_EDITAR_NOVEDADES_PERMISOS"), true);
//		st.setInt(1, objeto.get("tipo_novedad").getAsInt());
//              st.setInt(2, objeto.get("razon").getAsInt());
//              st.setString(3, objeto.get("identificacion").getAsString());
//	    	st.setString(4, objeto.get("fechaSolicitud").getAsString());
	    	st.setString(1, objeto.get("fecha_ini").getAsString() != null ? objeto.get("fecha_ini").getAsString() : "0099-99-99");
                st.setString(2, objeto.get("duracion_dias").getAsString() != null ? objeto.get("duracion_dias").getAsString() : "0");
                st.setString(3, objeto.get("fecha_fin").getAsString() != null ? objeto.get("fecha_fin").getAsString() : "0099-99-99");                
                st.setString(4, objeto.get("hora_ini").getAsString() != null ? objeto.get("hora_ini").getAsString() : "00:00");
	    	st.setString(5, objeto.get("hora_fin").getAsString() != null ? objeto.get("hora_fin").getAsString() : "00:00");
                st.setString(6, objeto.get("duracion_horas").getAsString() != null ? objeto.get("duracion_horas").getAsString() : "0");
//              st.setString(11, objeto.get("jefe_directo").getAsString());
	    	st.setString(7, objeto.get("descripcion").getAsString());
                st.setString(8, objeto.get("numsolicitud").getAsString());
//              st.setString(8, usuario.getLogin());
                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);		

            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
            System.out.println("Modificacion -->" + respuesta);
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
    }
    }    
    
    @Override
    public String anularNovedadesPermisos(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
//        String sqlver1 = "";
//        String sqlver2 = "";
//        String sqlver3 = "";
	TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
		objeto = info.getAsJsonArray("info").get(0).getAsJsonObject();
		st = new StringStatement(this.obtenerSQL("SQL_ANULAR_NOVEDADES_PERMISOS"), true);
//		st.setInt(1, objeto.get("tipo_novedad").getAsInt());
//              st.setInt(2, objeto.get("razon").getAsInt());
//              st.setString(3, objeto.get("identificacion").getAsString());
//	    	st.setString(4, objeto.get("fechaSolicitud").getAsString());
//	    	st.setString(1, objeto.get("fecha_ini").getAsString() != null ? objeto.get("fecha_ini").getAsString() : "0099-99-99");
//                st.setString(2, objeto.get("duracion_dias").getAsString() != null ? objeto.get("duracion_dias").getAsString() : "0");
//                st.setString(3, objeto.get("fecha_fin").getAsString() != null ? objeto.get("fecha_fin").getAsString() : "0099-99-99");                
//                st.setString(4, objeto.get("hora_ini").getAsString() != null ? objeto.get("hora_ini").getAsString() : "00:00");
//	    	st.setString(5, objeto.get("hora_fin").getAsString() != null ? objeto.get("hora_fin").getAsString() : "00:00");
//                st.setString(6, objeto.get("duracion_horas").getAsString() != null ? objeto.get("duracion_horas").getAsString() : "0");
//              st.setString(11, objeto.get("jefe_directo").getAsString());
//	    	st.setString(7, objeto.get("descripcion").getAsString());
                st.setString(1, objeto.get("numsolicitud").getAsString());
//              st.setString(8, usuario.getLogin());
                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
		

            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
            System.out.println("Anulacion -->" + respuesta);
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
    }
    }    

}