/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.operation.model.DAOS.MaestroLibranzaDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author egonzalez
 */
public class MaestroLibranzaImpl extends MainDAO implements MaestroLibranzaDAO {

    public MaestroLibranzaImpl(String dataBaseName) {
        super("MaestroLibranzaDAO.xml", dataBaseName);
    }

    @Override
    public String CargarSalarioMinimo() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_SALARIO_MINIMO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String guardarSalarioMinimo(Usuario usuario, String ano, String salario_minimo_diario, String salario_minimo_mensual, String variacion_anual) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_SALARIO_MINIMO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, ano);
            ps.setDouble(2, Double.parseDouble(salario_minimo_diario));
            ps.setDouble(3, Double.parseDouble(salario_minimo_mensual));
            ps.setDouble(4, Double.parseDouble(variacion_anual));
            ps.setString(5, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String actualizarSalarioMinimo(Usuario usuario, String ano, String salario_minimo_diario, String salario_minimo_mensual, String variacion_anual, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_SALARIO_MINIMO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, ano);
            ps.setDouble(2, Double.parseDouble(salario_minimo_diario));
            ps.setDouble(3, Double.parseDouble(salario_minimo_mensual));
            ps.setDouble(4, Double.parseDouble(variacion_anual));
            ps.setString(5, usuario.getLogin());
            ps.setString(6, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cambiarEstadoSalarioMinimo(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_SALARIO_MINIMO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarPagadurias() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        String filtro = "";
        Gson gson = new Gson();
        String query = "cargarPagadurias";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("razon_social", rs.getString("razon_social"));
                fila.addProperty("documento", rs.getString("documento"));
                fila.addProperty("dv", rs.getString("dv"));
                fila.addProperty("nomciu", rs.getString("nomciu"));
                fila.addProperty("municipio", rs.getString("municipio"));
                fila.addProperty("dpto", rs.getString("dpto"));
                fila.addProperty("pais", rs.getString("pais"));
                fila.addProperty("direccion", rs.getString("direccion"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("correo", rs.getString("correo"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String guardarPagadurias(String razon_social, String documento, String digito_verif, String municipio, String direccion, String telefono, String correo, String usuario, String dstrct) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "guardarPagadurias";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, razon_social);
            ps.setString(2, documento);
            ps.setString(3, digito_verif);
            ps.setString(4, municipio);
            ps.setString(5, direccion);
            ps.setString(6, telefono);
            ps.setString(7, correo);
            ps.setString(8, usuario);
            ps.setString(9, dstrct);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarPagadurias \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarPagadurias(String id, String razon_social, String documento, String digito_verif, String municipio, String direccion, String telefono, String correo, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "actualizarPagaduria";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, razon_social);
            ps.setString(2, documento);
            ps.setString(3, digito_verif);
            ps.setString(4, municipio);
            ps.setString(5, direccion);
            ps.setString(6, telefono);
            ps.setString(7, correo);
            ps.setString(8, usuario);
            ps.setInt(9, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarPagaduria \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaPagadurias(String id, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "activarInactivarPagaduria";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activaInactivaPagadurias \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String CargarOcupacionLaboral() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OCUPACION_LABORAL";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        return informacion;
    }

    @Override
    public boolean existeValorEnPagadurias(String campo, String valor, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "cargarPagadurias";
        String filtro = " and " + campo + "='" + valor + "'";
        filtro += (Integer.parseInt(id) > 0) ? " AND id not in(" + id + ")" : "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return resp;
    }

    @Override
    public String guardarOcupacionLaboral(Usuario usuario, String descripcion) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_OCUPACION_LABORAL";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String actualizarOcupacionLaboral(Usuario usuario, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_OCUPACION_LABORAL";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cambiarEstadoOcupacionLaboral(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_OCUPACION_LABORAL";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String CargarDescuentoLey() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DESCUENTO_LEY";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String cargarComboOcupacionLab() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_OCUPACION_LAB";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String guardarDescuentoLey(Usuario usuario, String ocupacion_laboral, String descripcion, String smlv_inicial, String smlv_final, String totaldesc) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_DESCUENTOS_LEY";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(ocupacion_laboral));
            ps.setString(2, descripcion);
            ps.setDouble(3, Double.parseDouble(smlv_inicial));
            ps.setDouble(4, Double.parseDouble(smlv_final));
            ps.setDouble(5, Double.parseDouble(totaldesc));
            ps.setString(6, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String actualizarDescuentoLey(Usuario usuario, String ocupacion_laboral, String descripcion, String smlv_inicial, String smlv_final, String totaldesc, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_DESCUENTOS_LEY";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(ocupacion_laboral));
            ps.setString(2, descripcion);
            ps.setDouble(3, Double.parseDouble(smlv_inicial));
            ps.setDouble(4, Double.parseDouble(smlv_final));
            ps.setDouble(5, Double.parseDouble(totaldesc));
            ps.setString(6, usuario.getLogin());
            ps.setString(7, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cambiarEstadoDescuentoLey(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_DESCUENTOS_LEY";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String CargarExtraprimaLibranza() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EXTRAPRIMA_LIBRANZA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String guardarExtraprimaLibranza(Usuario usuario, String ocupacion_laboral, String descripcion, String edad_inicial, String edad_final, String perc_extraprima) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_EXTRAPRIMA_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(ocupacion_laboral));
            ps.setString(2, descripcion);
            ps.setDouble(3, Double.parseDouble(edad_inicial));
            ps.setDouble(4, Double.parseDouble(edad_final));
            ps.setDouble(5, Double.parseDouble(perc_extraprima));
            ps.setString(6, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String actualizarExtraprimaLibranza(Usuario usuario, String ocupacion_laboral, String descripcion, String edad_inicial, String edad_final, String perc_extraprima, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_EXTRAPRIMA_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(ocupacion_laboral));
            ps.setString(2, descripcion);
            ps.setDouble(3, Double.parseDouble(edad_inicial));
            ps.setDouble(4, Double.parseDouble(edad_final));
            ps.setDouble(5, Double.parseDouble(perc_extraprima));
            ps.setString(6, usuario.getLogin());
            ps.setString(7, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cambiarEstadoExtraprimaLibranza(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_EXTRAPRIMA_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }finally {
            try {

                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String CargarOpBancariaLibranza() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OP_BANCARIA_LIBRANZA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String guardarOpBancariaLibranza(Usuario usuario, String tipo_documento, String descripcion, String cmc, String cuenta_cxp, String cuenta_detalle ,String tipo_operacion) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_OP_BANCARIA_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(tipo_documento));
            ps.setString(2, descripcion);
            ps.setString(3, cmc);
            ps.setString(4, cuenta_cxp);
            ps.setString(5, cuenta_detalle);
            ps.setInt(6, Integer.parseInt(tipo_operacion));
            ps.setString(7, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String actualizarOpBancariaLibranza(Usuario usuario, String tipo_documento, String descripcion, String cmc, String cuenta_cxp, String cuenta_detalle, String id ,String tipo_operacion) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_OP_BANCARIA_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(tipo_documento));
            ps.setString(2, descripcion);
            ps.setString(3, cmc);
            ps.setString(4, cuenta_cxp);
            ps.setString(5, cuenta_detalle);
            ps.setString(6, usuario.getLogin());
            ps.setInt(7, Integer.parseInt(tipo_operacion));
            ps.setString(8, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cambiarEstadoOpBancariaLibranza(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_OP_BANCARIA_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboTipodoc() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_TIPO_DOC";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("tipodoc"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboHC(String tipodoc) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_HC";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipodoc);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("cmc"), rs.getString("cmc"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String CargarCuentaHC(String cmc, String tipodoc) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CUENTA_HC";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cmc);
            ps.setString(2, tipodoc);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String CargardDeduccionesLibranza() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DEDUCCIONES_LIBRANZA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String guardarDeduccionesLibranza(Usuario usuario, String operacion_bancaria, String descripcion, String desembolso_inicial, String desembolso_final, String valor_cobrar, String perc_cobrar, String n_xmil, String ocupacion_laboral) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_DEDUCCIONES_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(operacion_bancaria));
            ps.setString(2, descripcion);
            ps.setDouble(3, Double.parseDouble(desembolso_inicial));
            ps.setDouble(4, Double.parseDouble(desembolso_final));
            ps.setDouble(5, Double.parseDouble(valor_cobrar));
            ps.setDouble(6, Double.parseDouble(perc_cobrar));
            ps.setDouble(7, Double.parseDouble(n_xmil));
            ps.setDouble(8, Double.parseDouble(ocupacion_laboral));
            ps.setString(9, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
               if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String actualizarDeduccionesLibranza(Usuario usuario, String operacion_bancaria, String descripcion, String desembolso_inicial, String desembolso_final, String valor_cobrar, String perc_cobrar, String n_xmil, String ocupacion_laboral, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_DEDUCCIONES_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(operacion_bancaria));
            ps.setString(2, descripcion);
            ps.setDouble(3, Double.parseDouble(desembolso_inicial));
            ps.setDouble(4, Double.parseDouble(desembolso_final));
            ps.setDouble(5, Double.parseDouble(valor_cobrar));
            ps.setDouble(6, Double.parseDouble(perc_cobrar));
            ps.setDouble(7, Double.parseDouble(n_xmil));
            ps.setDouble(8, Double.parseDouble(ocupacion_laboral));
            ps.setString(9, usuario.getLogin());
            ps.setString(10, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboOpBancariaLibranza(String operacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        String condicion = "reg_status=''";
        String query = "SQL_CARGAR_COMBO_OP_BANCARIAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            if (operacion.equals("Nuevo")) {
                condicion = condicion + " and visible='S'";
            } else if (operacion.equals("Editar")) {
                condicion = condicion + " and true";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cambiarEstadoDeduccionesLibranza(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_DEDUCCIONES_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarConfigLibranza() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        String query = "cargarConfigLibranza";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("nombre_convenio_pagaduria", rs.getString("nombre_convenio_pagaduria"));
                fila.addProperty("id_convenio", rs.getString("id_convenio"));
                fila.addProperty("convenio", rs.getString("convenio"));
                fila.addProperty("id_pagaduria", rs.getString("id_pagaduria"));
                fila.addProperty("pagaduria", rs.getString("pagaduria"));
                fila.addProperty("id_ocupacion_laboral", rs.getString("id_ocupacion_laboral"));
                fila.addProperty("ocupacion", rs.getString("ocupacion"));
                fila.addProperty("tasa_mensual", rs.getFloat("tasa_mensual"));
                fila.addProperty("tasa_anual", rs.getFloat("tasa"));
                fila.addProperty("tasa_renovacion", rs.getFloat("tasa_renovacion"));
                fila.addProperty("monto_minimo", rs.getFloat("monto_minimo"));
                fila.addProperty("monto_maximo", rs.getFloat("monto_maximo"));
                fila.addProperty("plazo_minimo", rs.getFloat("plazo_minimo"));
                fila.addProperty("plazo_maximo", rs.getFloat("plazo_maximo"));
                fila.addProperty("colchon", rs.getFloat("colchon"));
                fila.addProperty("factor_seguro", rs.getFloat("factor_seguro"));
                fila.addProperty("porcentaje_descuento", rs.getFloat("porcentaje_descuento"));
                fila.addProperty("dia_entrega_novedades", rs.getString("dia_entrega_novedades"));
                fila.addProperty("dia_pago", rs.getString("dia_pago"));
                fila.addProperty("periodo_gracia", rs.getString("periodo_gracia"));
                fila.addProperty("requiere_anexo", rs.getString("requiere_anexo"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String cargarFirmasRegistradas(String id_config_libranza) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        String query = "cargarFirmasRegistradas";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_config_libranza);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("id_config_libranza", rs.getInt("id_configuracion_libranza"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("documento", rs.getString("documento"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("correo", rs.getString("correo"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String guardarConfigLibranza(String nom_conv_pagaduria, String id_convenio, String id_pagaduria, String id_ocupacion_laboral, String tasa_mensual, String tasa_anual, String tasa_renovacion, String monto_minimo, String monto_maximo, String plazo_minimo, String plazo_maximo, String colchon, String factor_seguro, String porc_descuento, String dia_ent_novedades, String dia_pago, String periodo_gracia, String requiere_anexo, String usuario, String dstrct) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "guardarConfigLibranza";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nom_conv_pagaduria);
            ps.setInt(2, Integer.parseInt(id_convenio));
            ps.setInt(3, Integer.parseInt(id_pagaduria));
            ps.setInt(4, Integer.parseInt(id_ocupacion_laboral));
            ps.setDouble(5, Double.parseDouble(tasa_mensual));
            ps.setDouble(6, Double.parseDouble(tasa_renovacion));
            ps.setDouble(7, Double.parseDouble(monto_minimo));
            ps.setDouble(8, Double.parseDouble(monto_maximo));
            ps.setInt(9, Integer.parseInt(plazo_minimo));
            ps.setInt(10, Integer.parseInt(plazo_maximo));
            ps.setDouble(11, Double.parseDouble(colchon));
            ps.setDouble(12, Double.parseDouble(factor_seguro));
            ps.setDouble(13, Double.parseDouble(porc_descuento));
            ps.setString(14, dia_ent_novedades);
            ps.setString(15, dia_pago);
            ps.setInt(16, Integer.parseInt(periodo_gracia));
            ps.setString(17, requiere_anexo);
            ps.setString(18, usuario);
            ps.setDouble(19, Double.parseDouble(tasa_anual));
            ps.setString(20, dstrct);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarConfigLibranza \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarConfigLibranza(String id, String nom_conv_pagaduria, String id_convenio, String id_pagaduria, String id_ocupacion_laboral, String tasa_mensual, String tasa_anual, String tasa_renovacion, String monto_minimo, String monto_maximo, String plazo_minimo, String plazo_maximo, String colchon, String factor_seguro, String porc_descuento, String dia_ent_novedades, String dia_pago, String periodo_gracia, String requiere_anexo, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "actualizarConfigLibranza";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nom_conv_pagaduria);
            ps.setInt(2, Integer.parseInt(id_convenio));
            ps.setInt(3, Integer.parseInt(id_pagaduria));
            ps.setInt(4, Integer.parseInt(id_ocupacion_laboral));
            ps.setDouble(5, Double.parseDouble(tasa_mensual));
            ps.setDouble(6, Double.parseDouble(tasa_renovacion));
            ps.setDouble(7, Double.parseDouble(monto_minimo));
            ps.setDouble(8, Double.parseDouble(monto_maximo));
            ps.setInt(9, Integer.parseInt(plazo_minimo));
            ps.setInt(10, Integer.parseInt(plazo_maximo));
            ps.setDouble(11, Double.parseDouble(colchon));
            ps.setDouble(12, Double.parseDouble(factor_seguro));
            ps.setDouble(13, Double.parseDouble(porc_descuento));
            ps.setString(14, dia_ent_novedades);
            ps.setString(15, dia_pago);
            ps.setInt(16, Integer.parseInt(periodo_gracia));
            ps.setString(17, requiere_anexo);
            ps.setString(18, usuario);
            ps.setDouble(19, Double.parseDouble(tasa_anual));
            ps.setInt(20, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarConfigLibranza \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaConfigLibranza(String id, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "activarInactivarConfigLibranza";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activaInactivaConfigLibranza \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String guardarFirmaRegistrada(String id_config_libranza, String nombre, String documento, String telefono, String correo, String usuario, String dstrct) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "guardarFirmaRegistrada";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_config_libranza));
            ps.setString(2, nombre);
            ps.setString(3, documento);
            ps.setString(4, telefono);
            ps.setString(5, correo);
            ps.setString(6, usuario);
            ps.setString(7, dstrct);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarFirmaRegistrada \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarFirmaRegistrada(String id, String id_config_libranza, String nombre, String documento, String telefono, String correo, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "actualizarFirmaRegistrada";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_config_libranza));
            ps.setString(2, nombre);
            ps.setString(3, documento);
            ps.setString(4, telefono);
            ps.setString(5, correo);
            ps.setString(6, usuario);
            ps.setInt(7, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarFirmaRegistrada \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaFirmaRegistrada(String id, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "activarInactivarFirmaRegistrada";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activaInactivaFirmaRegistrada \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarComboPagadurias() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_PAGADURIAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboConvenios() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_CONVENIOS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboConcepto() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_COCEPTO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("table_code"), rs.getString("dato"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String CargarInformacionNegocio(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_INFROMACION_NEGOCIO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
       
    }

    @Override
    public String formalizarLibranza(String negocio, Usuario usuario, String concepto, String coment) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_FORMALIZAR_LIBRANZA";
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            ps.setString(2, usuario.getLogin());          
            ps.setString(3, concepto);
            ps.setString(4, coment);
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = rs.getString("respuesta");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
        
    }

    @Override
    public String cargarComboTipoOpBancariaLibranza() {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_TIPO_OPERACION_BANCARIO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
    
    @Override
    public String calcularTasaAnual(String tasa_mensual) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CALCULAR_TASA_ANUAL";       
        String respuesta = "{}";
        String tasa = "0";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tasa_mensual);

            rs = ps.executeQuery();
            if (rs.next()) {
                tasa = rs.getString("tasa");
            }
            respuesta = "{\"tasa\":\""+tasa+"\"}";

        } catch (SQLException ex) {
            throw new Exception("ERROR: getIdFiltroLibranza() " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    @Override
    public String cargarperfeccionarCompraCartera() {
         Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PERFECCIONAR_COMPRA_CARTERA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        return informacion;
    }

        
       
    @Override
    public String perfeccionamientoCompraCarteraTrazabilidad(String negocio, Usuario usuario, String coment) {
       StringStatement st = null;
        String respuesta = "";
        String query = "SQL_PERFECCIONAMIENTO_CC_TRAZABILIDAD";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, negocio);
            st.setString(2, usuario.getLogin());
            st.setString(3, negocio);
            st.setString(4, coment);

            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        return respuesta;
    }
    
    @Override
     public String perfeccionamientoCompraCarteraNegocios(String negocio, Usuario usuario) {
       StringStatement st = null;
        String respuesta = "";
        String query = "SQL_PERFECCIONAMIENTO_CC_NEGOCIOS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, negocio);

            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        return respuesta;
    }

    @Override
    public String CargardDeduccionesMicrocredito() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DEDUCCIONES_MICROCREDITO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
            return informacion;
        }

    }

    @Override
    public String cargarComboOcupacionLabMicro() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_OCUPACION_LAB_MICRO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cambiarEstadoDeduccionesMicrocredito(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_DEDUCCIONES_MICROCREDITO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
            return respuesta;
        }
    }

    @Override
    public String actualizarDeduccionesMicrocredito(Usuario usuario, String operacion_bancaria, String descripcion, String desembolso_inicial, String desembolso_final, String valor_cobrar, String perc_cobrar, String n_xmil, String ocupacion_laboral, String id) {
         Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_DEDUCCIONES_MICROCREDITO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(operacion_bancaria));
            ps.setString(2, descripcion);
            ps.setDouble(3, Double.parseDouble(desembolso_inicial));
            ps.setDouble(4, Double.parseDouble(desembolso_final));
            ps.setDouble(5, Double.parseDouble(valor_cobrar));
            ps.setDouble(6, Double.parseDouble(perc_cobrar));
            ps.setDouble(7, Double.parseDouble(n_xmil));
            ps.setDouble(8, Double.parseDouble(ocupacion_laboral));
            ps.setString(9, usuario.getLogin());
            ps.setString(10, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
            return respuesta;
        }
    }

    @Override
    public String guardarDeduccionesMicrocredito(Usuario usuario, String operacion_bancaria, String descripcion, String desembolso_inicial, String desembolso_final, String valor_cobrar, String perc_cobrar, String n_xmil, String ocupacion_laboral) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_DEDUCCIONES_MICROCREDITO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(operacion_bancaria));
            ps.setString(2, descripcion);
            ps.setDouble(3, Double.parseDouble(desembolso_inicial));
            ps.setDouble(4, Double.parseDouble(desembolso_final));
            ps.setDouble(5, Double.parseDouble(valor_cobrar));
            ps.setDouble(6, Double.parseDouble(perc_cobrar));
            ps.setDouble(7, Double.parseDouble(n_xmil));
            ps.setDouble(8, Double.parseDouble(ocupacion_laboral));
            ps.setString(9, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
            return respuesta;
        }
    }

    @Override
    public String CargardEntidadesCompraCartera() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ENTIDADES_COMPRA_CARTERA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
            return informacion;
        }

    }

    @Override
    public String cargarProveedoresEntidades(String nombre) {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_PROVEEDORES_ENTIDADES";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nombre);
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("label", rs.getString("info"));
                fila.addProperty("value", rs.getString("nombre"));
                fila.addProperty("mivar1", rs.getString("nit"));
                fila.addProperty("mivar2", rs.getString("digito_verificacion"));
                arr.add(fila);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(arr);
        }
    }

    @Override
    public String CambiarEstadoEntidadesCompraCartera(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_ENTIDAD_CC";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String guardarEntidadesCompraCartera(Usuario usuario, String nombre, String nit, String digver) {
         Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_ENTIDAD_CC";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, nit);
            ps.setString(4, digver);
            ps.setString(5, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
            return respuesta;
        }
    }

    @Override
    public String CargarObligacionesCompra(String numero_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OBLIGACIONES_COMPRA_CARTERA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numero_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        return informacion;
    }

    @Override
    public String guardarObligacionesCompra(Usuario usuario, String operacion, String numero_solicitud, String diferencia) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ACTUALIZAR_OBLIGACIONES_COMPRA";
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(numero_solicitud));
            ps.setString(2, operacion);
            ps.setInt(3, Integer.parseInt(diferencia));
            ps.setString(4, usuario.getLogin());
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = rs.getString("respuesta");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    @Override
    public String verificarNit(String documento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICAR_NIT_PROVEEDOR";
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, documento);
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = "{\"respuesta\":\""+rs.getString("respuesta") + "\"}";
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"respuesta\":\"ERROR\"}";
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    @Override
    public String actualizarObligacionesCompra(Usuario usuario, JsonArray informacion) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            for (int i = 0; i < informacion.size(); i++) {
               objeto = informacion.get(i).getAsJsonObject();
               st = new StringStatement(this.obtenerSQL("SQL_UPDATE_OBLIGACIONES_COMPRA"),true);
               st.setString(1, objeto.get("valor_modificado").getAsString());
               st.setString(2, usuario.getLogin());
               st.setString(3, objeto.get("numero_solicitud").getAsString());
               st.setString(4, objeto.get("secuencia").getAsString());
               
               tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String guardarProveedor(String documento, String razon_social, String municipio, String digito_verif,Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_GUARDAR_PROVEEDOR";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, documento);
            ps.setString(2, razon_social);
            ps.setString(3, municipio);
            ps.setString(4, usuario.getLogin());
            ps.setString(5, digito_verif);
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarPagadurias \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String cargarCHequeCompraCartera(String doc_rel) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CXP_CHEQUE_COMPRA_CARTERA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, doc_rel);
            //ps.setString(2, doc_rel);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        return informacion;
    }

    @Override
    public void actualizarReferencia(String documento) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_REFERENCIA";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, documento);

            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        
    }
    
    
    @Override
    public String auditoriaLibranzas(String fechaini, String fechafin) {
         Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String filtro = "";
        String query = "SQL_AUDITORIA_LIBRANZAS";
        if(fechaini != "" && fechafin != ""){
            filtro="and fecha_negocio::date between '"+fechaini+"' and '"+ fechafin+"'";
        }

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            ps = con.prepareStatement(query);           
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        return informacion;
    }
    
    public ArrayList searchNombresArchivos(String directorioArchivos, String documento) {
        
        ArrayList nombresArchivos=new ArrayList();
        File dir = new File(directorioArchivos + documento);
        try{
            if (dir.exists()){
                String[] ficheros = dir.list();
                for (int x=0;x<ficheros.length;x++) {
                      nombresArchivos.add(ficheros[x]);
                }
            }
      
           
        }catch(Exception e){
            System.out.println("error::" + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }
     
        return nombresArchivos;
    }

    @Override
    public boolean almacenarArchivoEnCarpetaUsuario(
                                 String documento
                                ,String rutaOrigen, String rutaDestino
                                ,String nomarchivo
                               ){

        boolean swFileCopied = false;
        try{            
          
            File carpetaDestino =new File(rutaDestino);
          
            //deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);
                          
            try{
                    //Copiamos el archivo de la carpeta origen a la carpeta destino
                    InputStream in = new FileInputStream(rutaOrigen + "/" +nomarchivo);
                    OutputStream out = new FileOutputStream(rutaDestino + nomarchivo);

                    // Copy the bits from instream to outstream
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                    swFileCopied = true;
               
                   }catch(Exception k){ throw new Exception(" FILE: " + k.getMessage());}

            }catch(Exception e){
                System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
                e.printStackTrace();
            }
       
        return swFileCopied;
    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public String cargarEmpresasPagaduria(String id_pagaduria) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        String query = "cargarEmpresasPagaduria";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_pagaduria);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("id_pagaduria", rs.getInt("id_pagaduria"));
                fila.addProperty("razon_social", rs.getString("razon_social"));
                fila.addProperty("nit_empresa", rs.getString("nit_empresa"));
                fila.addProperty("dv", rs.getString("dv"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("direccion", rs.getString("direccion"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);

    }

    @Override
    public String guardarEmpresaPagaduria(String id_pagaduria, String razon_social, String documento, String dv, String telefono, String direccion, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "guardarEmpresaPagaduria";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_pagaduria));
            ps.setString(2, razon_social);
            ps.setString(3, documento);
            ps.setString(4, dv);
            ps.setString(5, telefono);
            ps.setString(6, direccion);
            ps.setString(7, usuario.getLogin());
            ps.setString(8, usuario.getDstrct());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarEmpresaPagaduria \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }

    }

    @Override
    public String actualizarEmpresaPagaduria(String id, String id_pagaduria, String razon_social, String documento, String dv, String telefono, String direccion, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "actualizarEmpresaPagaduria";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);          
            ps.setString(1, razon_social);
            ps.setString(2, documento);
            ps.setString(3, dv);
            ps.setString(4, telefono);
            ps.setString(5, direccion);
            ps.setString(6, usuario.getLogin());
            ps.setInt(7, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarEmpresaPagaduria \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaEmpresaPagaduria(String id, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "activarInactivarEmpresaPagaduria";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activaInactivaEmpresaPagaduria \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarClientesLibranza(String fecha_ini, String fecha_fin, String id_convenio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        String filtro = (!id_convenio.equals("")) ? " AND conf.id = "+id_convenio: "";
        Gson gson = new Gson();
        String query = "cargarClientesLibranza";     
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            ps.setString(1, fecha_ini);
            ps.setString(2, fecha_fin);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("fecha", rs.getString("fecha"));
                fila.addProperty("identificacion", rs.getString("identificacion"));
                fila.addProperty("nombre_cliente", rs.getString("nombre_cliente"));
                fila.addProperty("apellidos_cliente", rs.getString("apellidos_cliente"));
                fila.addProperty("celular", rs.getString("celular"));
                fila.addProperty("empresa", rs.getString("empresa"));
                fila.addProperty("convenio", rs.getString("convenio"));
                fila.addProperty("monto", rs.getString("monto"));
                fila.addProperty("valor_cuota", rs.getString("valor_cuota"));
                fila.addProperty("plazo", rs.getString("plazo"));
                fila.addProperty("viable", rs.getString("viable"));        
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }
    
    @Override
    public String cargarComboConveniosPagaduria() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_COMBO_CONVENIOS_PAGADURIA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }    

    @Override
        public String cargarComboConveniosReliquidacion() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CONVENIOS_LIBRANZA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id_convenio"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarFormularioReliquidacion(String busqueda ,String dato) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_FORMULARIO_RELIQUIDACION";
        String consulta = "";
        String condicion = " ";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            switch (busqueda) {
                case "NEG":
                    condicion = "and neg.cod_neg ilike '" + dato + "' ";
                    break;
                case "CED":
                    condicion = "and sp.identificacion = '" + dato + "' ";
                    break;
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarConveniosLibranza(String cliente) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CONVENIOS_LIBRANZA_RELACIONADAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cliente);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarempresasLibranza(String id_pagaduria) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EMPRESAS_LIBRANZA_RELACIONADAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_pagaduria);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarTipoCuota() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_CUOTA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("table_code"), rs.getString("dato"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarTipoTituloValor() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_TITULO_VALOR";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("referencia"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String reliquidacionLibranza(Usuario usuario, JsonObject obj) {
        JsonObject objeto = new JsonObject();
        JsonArray informacion = null;
        String existeFianza = "";
        String respuesta = "{}";
        JsonObject fianza = new JsonObject();
        double vlr_fianza = 0;
        try {
            JsonArray arr = obj.getAsJsonArray("json");
            objeto = arr.get(0).getAsJsonObject();
            JsonObject negocio = (JsonObject) new JsonParser().parse(informacionNegReliquidacion(objeto.get("formalario").getAsString()));
            informacion = new JsonArray();
            informacion.add(negocio);
            existeFianza = existeFianza(negocio.get("cod_neg").getAsString());
            if (existeFianza.equals("SI")) {
                vlr_fianza = valorFianza(objeto.get("valor_financiar").getAsDouble(), objeto.get("num_cuotas").getAsInt());
                fianza.addProperty("vlr_fianza", vlr_fianza);
                fianza.addProperty("repuesta", "OK");
                informacion.add(fianza);
            }else{
            
                fianza.addProperty("vlr_fianza", "0");
                fianza.addProperty("repuesta", "OK");
                informacion.add(fianza);
 
            }

            respuesta = new Gson().toJson(informacion);
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    public String existeFianza(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String queryExisteFianza = "SQL_EXISTE_FIANZA";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(queryExisteFianza));
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("respuesta");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
    
    public Double valorFianza(double valor_negocio,int cuotas) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String queryExisteFianza = "SQL_VALOR_FIANZA";
        Double respuesta = null ;
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(queryExisteFianza));
            ps.setDouble(1, valor_negocio);
            ps.setInt(2, cuotas);
            ps.setDouble(3, valor_negocio);
            ps.setInt(4, cuotas);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getDouble("valor");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = null;
            }
        }
        return respuesta;
    }
    
      public String informacionNegReliquidacion(String numero_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_INFORMACION_NEGOCIO_RELIQUIDACION";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numero_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                //lista.add(objetoJson);
            }

            informacion = new Gson().toJson(objetoJson);
            System.out.println(informacion);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String informacionReliquidacionLibranza(String valor_desembolso, String numero_cuotas, String tipo_cuota, String fecha_calculo, String fecha_primera_cuota,int numero_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_INFORMACION_RELIQUIDACION";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, valor_desembolso);
            ps.setString(2, numero_cuotas);
            ps.setString(3, tipo_cuota);
            ps.setString(4, fecha_calculo);
            ps.setString(5, fecha_primera_cuota);
            ps.setString(6, "SIMULAR");
            ps.setInt(7, numero_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String reliquidarLibranza(String valor_desembolso, String numero_cuotas, String tipo_cuota, String fecha_calculo, String fecha_primera_cuota, String formulario, Usuario usuario ,String valorFianza) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_RELIQUIDAR_LIBRANZA";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, valor_desembolso);
            ps.setString(2, numero_cuotas);
            ps.setString(3, tipo_cuota);
            ps.setString(4, fecha_calculo);
            ps.setString(5, fecha_primera_cuota);
            ps.setString(6, formulario);
            ps.setString(7, usuario.getLogin());
            ps.setString(8, valorFianza);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String calculo_fecha_primera_cuota_reliquidacion(String formulario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CALCULO_FECHA_PRIMERA_CUOTA_RELQ";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String pagaduria;
        try {

            pagaduria = buscarpagaduria(formulario);
            if (!pagaduria.equals("")) {
                con = this.conectarJNDI();
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, pagaduria);
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    objetoJson = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(objetoJson);
                }
            } else {
                lista = new JsonArray();
                objetoJson = new JsonObject();
                objetoJson.addProperty("fch_Pago", "NO");
                lista.add(objetoJson);
            }
            

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }
    
    public String buscarpagaduria(String formulario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String queryBuscarpagaduria = "SQL_BUSCAR_PAGADURIA";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(queryBuscarpagaduria));
            ps.setString(1, formulario);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("doc_pagaduria");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
    
    @Override
    public String actualizarTasaNegocio(String cod_neg, String tasa_convenio) {
             Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_TASA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tasa_convenio);
            ps.setString(2, cod_neg);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }
}


