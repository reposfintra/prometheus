/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.ReciboOficialDAO;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.ReporteDatacredito;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.util.LogWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jpacosta
 */
public class ReciboOficialImpl extends MainDAO implements ReciboOficialDAO{

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    
    public ReciboOficialImpl(String dataBaseName) {
        super("ReciboOficial.xml", dataBaseName);
    }
    
    @Override
    public String cargarROP(String documento, String periodo, String unegocio, String negocio, String cedula, String valor) {
        String json = "{}";
        Gson gson = new Gson();
        JsonObject fila;
        JsonArray lista;
        try {
            query = "getROP";
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            query = query.replace("#documento#",
                    (!documento.equalsIgnoreCase("undefined"))
                    ?"rop.cod_rop ilike '%"+documento+"' AND":"");
            
            query = query.replace("#periodo#",
                    (!periodo.equalsIgnoreCase("undefined"))
                    ?"rop.periodo_rop = '"+periodo+"' AND":"");
                            
            query = query.replace("#unidad#",
                    (!unegocio.equalsIgnoreCase("vacio"))
                    ?"rop.id_unidad_negocio = '"+unegocio+"' AND":"");
            
            query = query.replace("#cedula#",
                    (!cedula.equalsIgnoreCase("undefined"))
                    ?"rop.cedula = '"+cedula+"' AND":"");
            
            query = query.replace("#negocio#",
                    (!negocio.equalsIgnoreCase("undefined"))
                    ?"rop.negocio = '"+negocio+"' AND":"");
            
            query = query.replace("#valor#",
                    (!valor.equalsIgnoreCase("undefined"))
                    ? valor+" in (val.subtotal, rop.total) AND":"");
            
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("documento", rs.getString("documento"));
                fila.addProperty("unidad_negocio", rs.getString("unidad_negocio"));
                fila.addProperty("fecha_ultimo_pago", rs.getString("fecha_ultimo_pago"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("cedula", rs.getString("cedula"));
                fila.addProperty("nombre_cliente", rs.getString("nombre_cliente"));
                fila.addProperty("direccion", rs.getString("direccion"));
                fila.addProperty("subtotal", rs.getString("subtotal"));
                fila.addProperty("total_gac", rs.getString("total_gac"));
                fila.addProperty("total_ixm", rs.getString("total_ixm"));
                fila.addProperty("total_descuentos", rs.getString("total_descuentos"));
                fila.addProperty("total", rs.getString("total"));
                lista.add(fila);
            } 
            fila = new JsonObject();
            fila.add("rows", lista);
            json = gson.toJson(fila);
        } catch (Exception e) {
            json = "{mensaje:"+e.getMessage()+"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) {}
            return json;
        }
    }

    @Override
    public String cargarDetalles(String idRop) {
        String json = "{}";
        Gson gson = new Gson();
        JsonObject fila ;
        JsonArray lista ;
        try {
            query = "getDetalles";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idRop);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("fecha_factura_padre", rs.getString("fecha_factura_padre"));
                fila.addProperty("fecha_vencimiento_padre", rs.getString("fecha_vencimiento_padre"));
                fila.addProperty("concepto_recaudo", rs.getString("concepto_recaudo"));
                fila.addProperty("cuota", rs.getString("cuota"));
                fila.addProperty("dias_vencidos", rs.getString("dias_vencidos"));
                fila.addProperty("valor_concepto", rs.getString("valor_concepto"));
                fila.addProperty("interes_por_mora", rs.getString("interes_por_mora"));
                fila.addProperty("gastos_cobranza", rs.getString("gastos_cobranza"));
                fila.addProperty("subtotal", rs.getString("subtotal"));
                fila.addProperty("valor_abono", rs.getString("valor_abono"));
                fila.addProperty("total", rs.getString("total"));
                fila.addProperty("tipo_pago", rs.getString("tipo_pago"));
                lista.add(fila);
            } 
            fila = new JsonObject();
            fila.add("rows", lista);
            json = gson.toJson(fila);
        } catch (Exception e) {
            json = "{mensaje:"+e.getMessage()+"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) {}
            return json;
        }
    }
    
    @Override
    public String cargarUnidades() {
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarBDJNDI("fintra");
            JsonObject elemento;
            query = "getPeriodos";
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            elemento = new JsonObject();
            while (rs.next()) {
                elemento.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            obj.add("periodo", elemento);

            query = "getUnidadesNegocios";
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            elemento = new JsonObject();
            while (rs.next()) {
                elemento.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            obj.add("uNeg", elemento);
            return gson.toJson(obj);
        }catch(Exception e) {
            return "{mensaje:"+e.getMessage()+"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (Exception ex) {}
        }
    }

    @Override
    public String cargarHistoricoPeticiones(String periodo, String unegocio, String negocio, String cedula) {
        String json = "{}";
        Gson gson = new Gson();
        JsonObject fila;
        JsonArray lista;
        String PeriodoAnterior="";
        try {
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
            query = "obtenerHistoricoReportes";
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
           query = query.replace("#periodo#",
                    (!periodo.equalsIgnoreCase("undefined"))
                    ?"periodo_lote = '"+PeriodoAnterior+"' AND":"periodo_lote like '%' and ");
                            
            query = query.replace("#unidad#",
                    (!unegocio.equalsIgnoreCase("undefined"))
                    ?"id_unidad_negocio = '"+unegocio+"' ":"id_unidad_negocio like '%'");
            
            query = query.replace("#cedula#",
                    (!cedula.equalsIgnoreCase("undefined"))
                    ?"AND identificacion = '"+cedula+"' ":"and identificacion like '%'");
            
            query = query.replace("#negocio#",
                    (!negocio.equalsIgnoreCase("undefined"))
                    ?"AND negocio = '"+negocio+"'":" and negocio like '%'");
            
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("un_negocio", rs.getString("un_negocio"));
                fila.addProperty("periodo_lote", rs.getString("periodo_lote"));
                fila.addProperty("tipo_identificacion", rs.getString("tipo_identificacion"));
                fila.addProperty("identificacion", rs.getString("identificacion"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("situacion_titular", rs.getString("situacion_titular"));
                fila.addProperty("fecha_apertura", rs.getString("fecha_apertura"));
                fila.addProperty("fecha_vencimiento", rs.getString("fecha_vencimiento"));
                fila.addProperty("fecha_corte_proceso", rs.getString("fecha_corte_proceso"));
                fila.addProperty("dias_mora", rs.getString("dias_mora"));
                fila.addProperty("novedad", rs.getString("novedad"));
                fila.addProperty("min_dias_mora", rs.getString("min_dias_mora"));
                fila.addProperty("desembolso", rs.getString("desembolso"));
                fila.addProperty("saldo_deuda", rs.getString("saldo_deuda"));
                fila.addProperty("saldo_en_mora", rs.getString("saldo_en_mora"));
                fila.addProperty("cuota_mensual", rs.getString("cuota_mensual"));
                fila.addProperty("numero_cuotas", rs.getString("numero_cuotas"));
                fila.addProperty("cuotas_canceladas", rs.getString("cuotas_canceladas"));
                fila.addProperty("cuotas_en_mora", rs.getString("cuotas_en_mora"));
                fila.addProperty("fecha_limite_pago", rs.getString("fecha_limite_pago"));
                fila.addProperty("ultimo_pago", rs.getString("ultimo_pago"));
                fila.addProperty("ciudad_radicacion", rs.getString("ciudad_radicacion"));
                fila.addProperty("cod_dane_radicacion", rs.getString("cod_dane_radicacion"));
                fila.addProperty("ciudad_residencia", rs.getString("ciudad_residencia"));
                fila.addProperty("cod_dane_residencia", rs.getString("cod_dane_residencia"));
                fila.addProperty("departamento_residencia", rs.getString("departamento_residencia"));
                fila.addProperty("direccion_residencia", rs.getString("direccion_residencia"));
                fila.addProperty("telefono_residencia", rs.getString("telefono_residencia"));
                fila.addProperty("ciudad_laboral", rs.getString("ciudad_laboral"));
                fila.addProperty("cod_dane_laboral", rs.getString("cod_dane_laboral"));
                fila.addProperty("departamento_laboral", rs.getString("departamento_laboral"));
                fila.addProperty("direccion_laboral", rs.getString("direccion_laboral"));
                fila.addProperty("telefono_laboral", rs.getString("telefono_laboral"));
                fila.addProperty("ciudad_correspondencia", rs.getString("ciudad_correspondencia"));
                fila.addProperty("cod_dane_correspondencia", rs.getString("cod_dane_correspondencia"));
                fila.addProperty("direccion_correspondencia", rs.getString("direccion_correspondencia"));
                fila.addProperty("correo_electronico", rs.getString("correo_electronico"));
                fila.addProperty("celular_solicitante", rs.getString("celular_solicitante"));
                fila.addProperty("tipo", rs.getString("tipo"));
                lista.add(fila);
            } 
            fila = new JsonObject();
            fila.add("rows", lista);
            json = gson.toJson(fila);
        } catch (Exception e) {
            json = "{mensaje:"+e.getMessage()+"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) {}
            return json;
        }
    }

    @Override
    public ArrayList<BeanGeneral> listaReportadosDatacredito(String periodo, String unegocio, String negocio, String cedula) {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "obtenerHistoricoReportes";
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            query = query.replace("#periodo#",
                    (!periodo.equalsIgnoreCase("undefined"))
                    ?"periodo_lote = '"+periodo+"' AND":"");
                            
            query = query.replace("#unidad#",
                    (!unegocio.equalsIgnoreCase("vacio"))
                    ?"id_unidad_negocio = '"+unegocio+"' ":"");
            
            query = query.replace("#cedula#",
                    (!cedula.equalsIgnoreCase("undefined"))
                    ?"AND identificacion = '"+cedula+"' ":"");
            
            query = query.replace("#negocio#",
                    (!negocio.equalsIgnoreCase("undefined"))
                    ?"AND negocio = '"+negocio+"'":"");
            
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                BeanGeneral bean = new BeanGeneral();
                bean.setValor_01(rs.getString("un_negocio"));
                bean.setValor_02(rs.getString("periodo_lote"));
                bean.setValor_03(rs.getString("tipo_identificacion"));
                bean.setValor_04(rs.getString("identificacion"));
                bean.setValor_05(rs.getString("nombre"));
                bean.setValor_06(rs.getString("situacion_titular"));
                bean.setValor_07(rs.getString("negocio"));
                bean.setValor_08(rs.getString("fecha_apertura"));
                bean.setValor_09(rs.getString("fecha_vencimiento"));
                bean.setValor_10(rs.getString("fecha_corte_proceso"));
                bean.setValor_11(rs.getString("dias_mora"));
                bean.setValor_12(rs.getString("novedad"));
                bean.setValor_13(rs.getString("min_dias_mora"));
                bean.setValor_14(rs.getString("desembolso"));
                bean.setValor_15(rs.getString("saldo_deuda"));
                bean.setValor_16(rs.getString("saldo_en_mora"));
                bean.setValor_17(rs.getString("cuota_mensual"));
                bean.setValor_18(rs.getString("numero_cuotas"));
                bean.setValor_19(rs.getString("cuotas_canceladas"));
                bean.setValor_20(rs.getString("cuotas_en_mora"));
                bean.setValor_21(rs.getString("fecha_limite_pago"));
                bean.setValor_22(rs.getString("ultimo_pago"));
                bean.setValor_23(rs.getString("ciudad_radicacion"));
                bean.setValor_24(rs.getString("cod_dane_radicacion"));
                bean.setValor_25(rs.getString("ciudad_residencia"));
                bean.setValor_26(rs.getString("cod_dane_residencia"));
                bean.setValor_27(rs.getString("departamento_residencia"));
                bean.setValor_28(rs.getString("direccion_residencia"));
                bean.setValor_29(rs.getString("telefono_residencia"));
                bean.setValor_30(rs.getString("ciudad_laboral"));
                bean.setValor_31(rs.getString("cod_dane_laboral"));
                bean.setValor_32(rs.getString("departamento_laboral"));
                bean.setValor_33(rs.getString("direccion_laboral"));
                bean.setValor_34(rs.getString("telefono_laboral"));
                bean.setValor_35(rs.getString("ciudad_correspondencia"));
                bean.setValor_36(rs.getString("cod_dane_correspondencia"));
                bean.setValor_37(rs.getString("direccion_correspondencia"));
                bean.setValor_38(rs.getString("correo_electronico"));
                bean.setValor_39(rs.getString("celular_solicitante"));
                bean.setValor_40(rs.getString("tipo"));
                
                lista.add(bean);
            }

        } catch (Exception e) {
            try {
                throw new Exception("Error en listaReportadosDatacredito[listaReportadosDatacredito] " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
           
        }
         return lista;
    }

    @Override
    public ArrayList<ReporteDatacredito> cargarPrevisualizarReportados(String periodo, String unegocio, String fecha, int rangoIni, int rangoFin, String nomNeg, String accion) {
        PreparedStatement st = null;
        con = null;
        rs = null;
        int id=0;
        String query = "obtenerPrevisualizarReporte";
        ArrayList<ReporteDatacredito> lista = new ArrayList<ReporteDatacredito>();
        String PeriodoAnterior = "";
        String TramoAnterior ="";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
                TramoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"11";
            }else if (mes.equals("02")){
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
                TramoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
                TramoAnterior = String.valueOf(Integer.parseInt(periodo) - 2);
            }

            ps = con.prepareStatement(query);
            ps.setString(1, nomNeg);
            ps.setInt(2, Integer.parseInt(PeriodoAnterior));
            ps.setString(3, fecha);
            ps.setInt(4, Integer.parseInt(periodo));
            ps.setString(5, unegocio);
            ps.setInt(6, rangoIni);
            ps.setInt(7, rangoFin);
            ps.setString(8, accion);
            ps.setInt(9, Integer.parseInt(unegocio));
            ps.setString(10, TramoAnterior);
            ps.setString(11, PeriodoAnterior);
            ps.setInt(12, Integer.parseInt(unegocio));
                       
            rs = ps.executeQuery();
            
            while (rs.next()) {
                ReporteDatacredito rep = new ReporteDatacredito();
                rep.setId(id+1);
                rep.setUnd_negocio(rs.getString("un_negocio"));
                rep.setPeriodo(rs.getString("periodo"));
                rep.setTipo_identificacion(rs.getString("tipo_identificacion"));
                rep.setIdentificacion(rs.getString("identificacion"));
                rep.setNegocio(rs.getString("negocio"));
                rep.setNombre(rs.getString("nombre"));
                rep.setSituacion_titular(rs.getString("situacion_titular"));
                rep.setFecha_apertura(rs.getString("fecha_apertura"));
                rep.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                rep.setFecha_corte_proceso(rs.getString("fecha_corte_proceso"));
                rep.setDias_mora(rs.getInt("dias_mora"));
                rep.setNovedad(rs.getString("novedad"));
                rep.setMin_dias_mora(rs.getInt("min_dias_mora"));
                rep.setDesembolso(rs.getString("desembolso"));
                rep.setSaldo_deuda(rs.getDouble("saldo_deuda"));
                rep.setSaldo_mora(rs.getString("saldo_en_mora"));
                rep.setCuota_mensual(rs.getString("cuota_mensual"));
                rep.setNumero_cuotas(rs.getString("numero_cuotas"));
                rep.setCuotas_canceladas(rs.getInt("cuotas_canceladas"));
                rep.setCuotas_mora(rs.getInt("cuotas_en_mora"));
                rep.setFecha_limite_pago(rs.getString("fecha_limite_pago"));
                rep.setUltimo_pago(rs.getString("ultimo_pago"));
                rep.setCiudad_radicacion(rs.getString("ciudad_radicacion"));
                rep.setCod_dane_radicacion(rs.getString("cod_dane_radicacion"));
                rep.setCiudad_residencia(rs.getString("ciudad_residencia"));
                rep.setCod_dane_residencia(rs.getString("cod_dane_residencia"));
                rep.setDepartamento_residencia(rs.getString("departamento_residencia"));
                rep.setDireccion_residencia(rs.getString("direccion_residencia"));
                rep.setTelefono_residencia(rs.getString("telefono_residencia"));
                rep.setCiudad_laboral(rs.getString("ciudad_laboral"));
                rep.setCod_dane_laboral(rs.getString("cod_dane_laboral"));
                rep.setDepartamento_laboral(rs.getString("departamento_laboral"));
                rep.setDireccion_laboral(rs.getString("direccion_laboral"));
                rep.setTelefono_laboral(rs.getString("telefono_laboral"));
                rep.setCiudad_correspondencia(rs.getString("ciudad_correspondencia"));
                rep.setCod_dane_correspondencia(rs.getString("cod_dane_correspondencia"));
                rep.setDireccion_correspondencia(rs.getString("direccion_correspondencia"));
                rep.setCorreo_electronico(rs.getString("correo_electronico"));
                rep.setCelular_solicitante(rs.getString("celular_solicitante"));
                rep.setTipo(rs.getString("tipo"));
                rep.setHr_periodo(rs.getString("hr_periodo"));
                rep.setHr_tipo_id(rs.getString("hr_tipo_id"));
                rep.setHr_identificacion(rs.getString("hr_identificacion"));
                rep.setHr_negocio(rs.getString("hr_negocio"));
                rep.setHr_fecha_apertura(rs.getString("hr_fecha_apertura"));
                rep.setHr_fecha_ven(rs.getString("hr_fecha_ven"));
                rep.setHr_novedad(rs.getString("hr_novedad"));
                rep.setHr_nombre(rs.getString("hr_nombre"));
                rep.setHr_saldo_mora(rs.getString("hr_saldo_mora"));
               
                id ++;
                lista.add(rep);
            }

        } catch (SQLException | NumberFormatException e) {
            try {
                throw new Exception("Error en listaReportadosDatacredito[listaReportadosDatacredito] " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
           
        }
         return lista;
    }

    @Override
    public String guardarReportados(BeanGeneral bg, String login) {
        StringStatement st = null;
        String sql = "";
        String query = "guardar_reportados";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, bg.getValor_01()); //periodo
            st.setInt(2, Integer.parseInt(bg.getValor_02())); //unidad negocio
            st.setString(3, bg.getValor_03()); //codigo del negocio
            st.setString(4, bg.getValor_04()); //cedula
            st.setString(5, login);


            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR guardarReportados \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
           } catch (Exception ex) {}
        }
        return sql;
    }

    @Override
    public void limpiarReporte(String periodo, String unegocio) {
        con = null;
        ps = null;
        String query = "limpiar_reporte";
        String sql = "";
        String PeriodoAnterior = "";
        try {
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, PeriodoAnterior);
            ps.setInt(2, Integer.parseInt(unegocio));

            ps.executeUpdate();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR limpiarReporte \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }

    }

    @Override
    public String anularNegocioHistorico(String periodo, String unegocio, String usuario) {
           StringStatement st = null;
           String sql = "";
           String query = "anular_historico_negocio";
           try{
               st = new StringStatement (this.obtenerSQL(query), true);
               st.setString(1, usuario);
               st.setString(2, unegocio);
               st.setString(3, periodo);
               
               sql = st.getSql();
              
           }catch(Exception e){
               try {
                   throw new SQLException("ERROR anularNegocioHistorico \n" + e.getMessage());
               } catch (SQLException ex) {
                   Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
               }
           }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){try {
                throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                } catch (SQLException ex) {
                    Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                }}
            }
           return sql;
    }

    @Override
    public String anularNegocioReportado(String periodo, String unegocio, String usuario) {
           StringStatement st = null;
           String sql = "";
           String query = "anular_reporte_negocio";
           try{
               st = new StringStatement (this.obtenerSQL(query), true);
               st.setString(1, usuario);
               st.setString(2, unegocio);
               st.setString(3, periodo);
               
               sql = st.getSql();
              
           }catch(Exception e){
               try {
                   throw new SQLException("ERROR anularNegocioReportado \n" + e.getMessage());
               } catch (SQLException ex) {
                   Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
               }
           }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){try {
                throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                } catch (SQLException ex) {
                    Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                }}
            }
           return sql;
    }

    
    @Override
    public boolean generadoReporte(String periodo, String unegocio) {
        query = "validar_reporte_datacredito_generado";
        String sql = "";
        String PeriodoAnterior ="";
        boolean resp = false;
        try {
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, PeriodoAnterior);
            ps.setString(2, unegocio);

            rs = ps.executeQuery();
            
            if (rs.next()) {
                resp = true;
                }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR generadoReporte \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }
        return resp;
    }

    @Override
    public String obtenerReporteDatacredito(String periodo, String unegocio, String fecha, int rangoIni, int rangoFin, String accion, String estado) {
        query = "obtener_Reporte_Datacredito";
        String sql = "";
        String plano = "";
        String PeriodoAnterior = "";
        try {
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, fecha);
            ps.setInt(2, Integer.parseInt(periodo));
            ps.setString(3, unegocio);
            ps.setInt(4, rangoIni);
            ps.setInt(5, rangoFin);
            ps.setString(6, accion);
            ps.setString(7, PeriodoAnterior);
            ps.setString(8, estado);

            rs = ps.executeQuery();

            if (rs.next()) {
                plano = rs.getString("archivo");
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR obtenerReporteDatacredito \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }
        return plano;
    }

    @Override
    public String obtenerCodigoUnidNegocioCR(String unegocio) {
        query = "obtener_codigo_central_riesgo";
        String sql = "";
        String codigo = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, unegocio);
            
            rs = ps.executeQuery();

            if (rs.next()) {
                codigo = rs.getString("cod_central_riesgo");
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR obtenerCodigoUnidNegocioCR \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }
        return codigo;
    }

    @Override
    public int obtenerCantidadReportados(String periodo, String unegocio) {
        query = "obtener_cantidad_reportados_datacredito";
        String sql = "";
        String PeriodoAnterior = "";
        int cantidad = 0;
        try {
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(PeriodoAnterior));
            ps.setString(2, unegocio);
            
            rs = ps.executeQuery();

            if (rs.next()) {
                cantidad = rs.getInt("cant");
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR obtenerCantidadReportados \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }
        return cantidad;
    }

    @Override
    public ArrayList<ReporteDatacredito> obtenerNoReportados(String periodo, String unegocio, String fecha, int rangoIni, int rangoFin, String nomNeg, String accion) {
        PreparedStatement st = null;
        con = null;
        rs = null;
        int id=0;
        String query = "obtenerNoReportados";
        ArrayList<ReporteDatacredito> lista = new ArrayList<ReporteDatacredito>();
        String PeriodoAnterior = "";

        try {
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nomNeg);
            ps.setString(2, PeriodoAnterior);
            ps.setString(3, fecha);
            ps.setString(4, periodo);
            ps.setString(5, unegocio);
            ps.setInt(6, rangoIni);
            ps.setInt(7, rangoFin);
            ps.setString(8, accion);
            ps.setString(9, unegocio);
            ps.setString(10, periodo);
            ps.setString(11, PeriodoAnterior);
            ps.setString(12, unegocio);
                       
            rs = ps.executeQuery();
            
            while (rs.next()) {
                ReporteDatacredito rep = new ReporteDatacredito();
                rep.setId(id+1);
                rep.setUnd_negocio(rs.getString("un_negocio"));
                rep.setPeriodo(rs.getString("periodo"));
                rep.setTipo_identificacion(rs.getString("tipo_identificacion"));
                rep.setIdentificacion(rs.getString("identificacion"));
                rep.setNegocio(rs.getString("negocio"));
                rep.setNombre(rs.getString("nombre"));
                rep.setSituacion_titular(rs.getString("situacion_titular"));
                rep.setFecha_apertura(rs.getString("fecha_apertura"));
                rep.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                rep.setFecha_corte_proceso(rs.getString("fecha_corte_proceso"));
                rep.setDias_mora(rs.getInt("dias_mora"));
                rep.setNovedad(rs.getString("novedad"));
                rep.setMin_dias_mora(rs.getInt("min_dias_mora"));
                rep.setDesembolso(rs.getString("desembolso"));
                rep.setSaldo_deuda(rs.getDouble("saldo_deuda"));
                rep.setSaldo_mora(rs.getString("saldo_en_mora"));
                rep.setCuota_mensual(rs.getString("cuota_mensual"));
                rep.setNumero_cuotas(rs.getString("numero_cuotas"));
                rep.setCuotas_canceladas(rs.getInt("cuotas_canceladas"));
                rep.setCuotas_mora(rs.getInt("cuotas_en_mora"));
                rep.setFecha_limite_pago(rs.getString("fecha_limite_pago"));
                rep.setUltimo_pago(rs.getString("ultimo_pago"));
                rep.setCiudad_radicacion(rs.getString("ciudad_radicacion"));
                rep.setCod_dane_radicacion(rs.getString("cod_dane_radicacion"));
                rep.setCiudad_residencia(rs.getString("ciudad_residencia"));
                rep.setCod_dane_residencia(rs.getString("cod_dane_residencia"));
                rep.setDepartamento_residencia(rs.getString("departamento_residencia"));
                rep.setDireccion_residencia(rs.getString("direccion_residencia"));
                rep.setTelefono_residencia(rs.getString("telefono_residencia"));
                rep.setCiudad_laboral(rs.getString("ciudad_laboral"));
                rep.setCod_dane_laboral(rs.getString("cod_dane_laboral"));
                rep.setDepartamento_laboral(rs.getString("departamento_laboral"));
                rep.setDireccion_laboral(rs.getString("direccion_laboral"));
                rep.setTelefono_laboral(rs.getString("telefono_laboral"));
                rep.setCiudad_correspondencia(rs.getString("ciudad_correspondencia"));
                rep.setCod_dane_correspondencia(rs.getString("cod_dane_correspondencia"));
                rep.setDireccion_correspondencia(rs.getString("direccion_correspondencia"));
                rep.setCorreo_electronico(rs.getString("correo_electronico"));
                rep.setCelular_solicitante(rs.getString("celular_solicitante"));
                rep.setTipo(rs.getString("tipo"));
                rep.setHr_periodo(rs.getString("hr_periodo"));
                rep.setHr_tipo_id(rs.getString("hr_tipo_id"));
                rep.setHr_identificacion(rs.getString("hr_identificacion"));
                rep.setHr_negocio(rs.getString("hr_negocio"));
                rep.setHr_fecha_apertura(rs.getString("hr_fecha_apertura"));
                rep.setHr_fecha_ven(rs.getString("hr_fecha_ven"));
                rep.setHr_novedad(rs.getString("hr_novedad"));
                rep.setHr_nombre(rs.getString("hr_nombre"));
                rep.setHr_saldo_mora(rs.getString("hr_saldo_mora"));
               
                id ++;
                lista.add(rep);
            }

        } catch (Exception e) {
            try {
                throw new Exception("Error en listaReportadosDatacredito[listaReportadosDatacredito] " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
           
        }
         return lista;
    }

    @Override
    public void insertarReporteHistorico(String periodo, String unegocio, String fecha, int rangoIni, int rangoFin) {
        query = "insertar_Reporte_Historico_Datacredito";
        String sql = "";
        String PeriodoAnterior = "";
        try {
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(unegocio));
            ps.setInt(2, Integer.parseInt(PeriodoAnterior));
            ps.setString(3, fecha);
            ps.setInt(4, Integer.parseInt(periodo));
            ps.setString(5, unegocio);
            ps.setInt(6, rangoIni);
            ps.setInt(7, rangoFin);
            ps.setInt(8, Integer.parseInt(unegocio));
            ps.setString(9, PeriodoAnterior);
            
            ps.executeUpdate();

           

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertarReporteHistorico \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
       }
       
    }

    @Override
    public String cargarUnidadesDatacredito() {
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarBDJNDI("fintra");
            JsonObject elemento;
            query = "getPeriodos";
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            elemento = new JsonObject();
            while (rs.next()) {
                elemento.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            obj.add("periodoData", elemento);

            query = "getUnidadesNegociosDatacredito";
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            elemento = new JsonObject();
            elemento.addProperty("vacio", "");
            while (rs.next()) {
                elemento.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            obj.add("unidNeg", elemento);
            return gson.toJson(obj);
        }catch(Exception e) {
            return "{mensaje:"+e.getMessage()+"}";
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (Exception ex) {}
        }
    }

    @Override
    public ArrayList<BeanGeneral> buscarDetalle(int idRop, String negocio) throws SQLException {
        
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DETALLE_ROP";
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, idRop);
            ps.setString(2, negocio);
            rs = ps.executeQuery();

            while (rs.next()) {
               
                BeanGeneral bean = new BeanGeneral();
                bean.setValor_01(rs.getString("negasoc"));
                bean.setValor_02(rs.getString("documento"));
                bean.setValor_03(rs.getString("num_doc_fen"));
                bean.setValor_04(rs.getString("fecha_vencimiento"));
                bean.setValor_05(rs.getString("periodo_vcto"));
                bean.setValor_06(rs.getString("num_dias"));
                bean.setValor_07(rs.getString("estado_cartera"));
                bean.setValor_08(rs.getString("descripcion"));
                bean.setValor_09(rs.getString("valor_item"));
                bean.setValor_10(rs.getString("accion"));
                bean.setValor_11(rs.getString("valor_accion"));
                bean.setValor_12(rs.getString("valor_llevar_a"));
               
                
                lista.add(bean);
            }

        } catch (Exception e) {
            try {
                throw new Exception("Error en listaReportadosDatacredito[listaReportadosDatacredito] " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
           
        }
         return lista;
      
    }

    @Override
    public String actualizarIFecha(String fechaif, String fecha_actual, double valor_if, String negocio) throws SQLException {
        query = "SQL_ACTUALIZAR_IF";
        String sql = "";
        String respuestaJson = "{}";
        
        try {
         
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1,fechaif);
            ps.setString(2,negocio);
            ps.setString(3,fecha_actual);
            ps.setDouble(4, valor_if);
            
            ps.executeUpdate();
            
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"respuesta\":\"NOT\"}";
                throw new SQLException("ERROR insertarReporteHistorico \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ReciboOficialImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            
            return respuestaJson;
       }
    }

    @Override
    public String generarPlanoAsobancaria(JsonObject info) {
        PrintWriter pw;
        LogWriter logWriter;
        String linea = "";
        query = "GEN_ARCH_SEPT_01";
        try{            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta") + "/exportar/migracion/" + info.get("usuario").getAsString();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();

            File file = new File(ruta);
            file.mkdirs();
            pw = new PrintWriter(System.err, true);

            ruta += "/planoAsobancaria_"
                 + info.get("periodo").getAsString() + "_"
                 + info.get("ciclo").getAsString()
                 + ".txt";
            
            pw        = new PrintWriter(new FileWriter(ruta, true), true);
            logWriter = new LogWriter("", LogWriter.INFO, pw);
            logWriter.setPrintWriter(pw);
            try {
                String sql = this.obtenerSQL(query);
                con = conectarJNDI(query);
                ps = con.prepareStatement(sql);
                ps.setString(1, info.get("periodo").getAsString());
                ps.setInt(2, info.get("ciclo").getAsInt());
                rs = ps.executeQuery();
                while(rs.next()) {
                    logWriter.log(rs.getString(1));
                }
            } catch (Exception e) {
                logWriter.log("Error > "+e.getMessage());
                throw new Exception(e.getMessage()); 
            }

        } catch (Exception ex){
            ex.printStackTrace();
            return "{\"mensaje\": \"ERROR proceso > "+ex.getMessage()+"\"}";
        } finally {
            try {
                if (con != null) { this.desconectar(con); }
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) { }  
        }
        return "{\"mensaje\": \"Se ha terminado de generar el archivo, revise su log.\"}";
    }

    @Override
    public String genPlanoAsoEfecty(JsonObject info) {
        PrintWriter pw;
        LogWriter logWriter;
        Date objDate = new Date();
        String strDateFormat = "yyyyMMddHHmmss";
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
        String fechasist = objSDF.format(objDate);
        if(info.get("tipo").getAsString().equals("EDU")){
        query = "GEN_ARCH_EFECTY_EDU";
        }else{
        query = "GEN_ARCH_EFECTY";
        }
        try{            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta") + "/exportar/migracion/" + info.get("usuario").getAsString();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();

            File file = new File(ruta);
            file.mkdirs();
            pw = new PrintWriter(System.err, true);

            ruta += "/planoAsobancariaEfecty_"
                 + info.get("periodo").getAsString() + "_"
                 + info.get("ciclo").getAsString()+"_"
                 +info.get("tipo").getAsString()+"_"+fechasist
                 + ".txt";
            
            pw        = new PrintWriter(new FileWriter(ruta, true), true);
            logWriter = new LogWriter("", LogWriter.INFO, pw);
            logWriter.setPrintWriter(pw);
            try {
                String sql = this.obtenerSQL(query);
                con = conectarJNDI(query);
                ps = con.prepareStatement(sql);
                ps.setString(1, info.get("periodo").getAsString());
                ps.setInt(2, info.get("ciclo").getAsInt());
                rs = ps.executeQuery();
                while(rs.next()) {
                    logWriter.log(rs.getString(1));
                }
            } catch (Exception e) {
                logWriter.log("Error > "+e.getMessage());
                throw new Exception(e.getMessage()); 
            }

        } catch (Exception ex){
            ex.printStackTrace();
            return "{\"mensaje\": \"ERROR proceso > "+ex.getMessage()+"\"}";
        } finally {
            try {
                if (con != null) { this.desconectar(con); }
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (SQLException ex) { }  
        }
        return "{\"mensaje\": \"Se ha terminado de generar el archivo, revise su log.\"}";
    }

}
