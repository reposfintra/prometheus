/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.tsp.operation.model.DAOS.CorridasNominaDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.LogWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

/**
 *
 * @author egonzalez
 */
public class CorridasNominaImpl extends MainDAO implements CorridasNominaDAO{

    public CorridasNominaImpl(String dataBaseName) {
        super("CorridasNominaDAO.xml", dataBaseName);
    }

    @Override
    public String getFacturas(String filGerencia, String periodo) {
        String query = "GET_FACTURAS";
        JsonObject obj = new JsonObject(), fila;
        JsonArray datos = new JsonArray();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query).replace("#NOT#", (filGerencia.equals("N")?"NOT":"")));
            ps.setString(1, periodo);
            rs = ps.executeQuery();
            
            while(rs.next()) {
                fila = new JsonObject();
                fila.addProperty("distrito", rs.getString("distrito"));
                fila.addProperty("nit", rs.getString("nit"));
                fila.addProperty("proveedor", rs.getString("proveedor"));
                fila.addProperty("factura", rs.getString("factura"));
                fila.addProperty("tipodoc", rs.getString("tipoDoc"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("agencia", rs.getString("agencia"));
                fila.addProperty("banco", rs.getString("banco"));
                fila.addProperty("sucursal", rs.getString("sucursal"));
                fila.addProperty("moneda_banco", rs.getString("moneda_banco"));
                fila.addProperty("fecha", rs.getString("fecha"));
                fila.addProperty("moneda", rs.getString("moneda"));
                fila.addProperty("saldo", rs.getString("saldo"));
                fila.addProperty("saldo_me", rs.getString("saldo_me"));
                fila.addProperty("usuario_aprobacion", rs.getString("usuario_aprobacion"));
                fila.addProperty("planilla", rs.getString("planilla"));
                fila.addProperty("base", rs.getString("base"));
                fila.addProperty("placa", rs.getString("placa"));
                fila.addProperty("tipo_pago", rs.getString("tipo_pago"));
                fila.addProperty("banco_transfer", rs.getString("banco_transfer"));
                fila.addProperty("suc_transfer", rs.getString("suc_transfer"));
                fila.addProperty("tipo_cuenta", rs.getString("tipo_cuenta"));
                fila.addProperty("no_cuenta", rs.getString("no_cuenta"));
                fila.addProperty("cedula_cuenta", rs.getString("cedula_cuenta"));
                fila.addProperty("nombre_cuenta", rs.getString("nombre_cuenta"));
                datos.add(fila);
            }    
            obj.add("rows",datos);
        } catch(Exception exc) {
        } finally {
             try {
                if (con != null) { this.desconectar(con); }
                if (ps != null) { ps.close();  }
                if (rs != null) { rs.close(); }
            } catch (Exception ex) { }
            return (new Gson()).toJson(obj);
        }        
    }

    @Override
    public String crearCorridas(JsonObject objIn) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query;
        String retorno="";
        JsonObject obj = new JsonObject(), fila;
        JsonArray arr = null;
        JsonObject get;
        try {
            query = "USUARIO_AUTORIZADO";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, objIn.get("distrito").getAsString());
            ps.setString(2, objIn.get("usuario").getAsString());
            ps.setString(3, objIn.get("banco").getAsString());
            ps.setString(4, objIn.get("sucursal").getAsString());
            rs = ps.executeQuery();
            
            if(rs.next()) {
                if (rs.getString("texto").startsWith("NEGADO")) {
                    return "{\"mensaje\": \"El usuario no tiene permitido generar estos documentos\"}";
                } else {
                    fila = new JsonObject();
                    fila.addProperty("nit", rs.getString("nit"));
                    fila.addProperty("banco", rs.getString("banco"));
                    fila.addProperty("sucursal", rs.getString("sucursal"));
                    fila.addProperty("cuenta", rs.getString("cuenta"));
                    obj.add("cabecera", fila);
                }
            } else {
                return "{\"mensaje\": \"No hay informacion del banco\"}";
            }
        } catch(Exception exc) {
            exc.printStackTrace();
            return "{\"mensaje\": \"Error permisos > "+exc.getMessage()+"\"}";
        } finally {
            if (rs != null) { try { rs.close(); } catch (Exception e){} }
            if (ps != null) { try { ps.close(); } catch (Exception e){} }
            if (con != null) { try { this.desconectar(con); } catch (Exception e){} }
        } 
        try {
            query = "CREAR_CORRIDAS";
            arr = objIn.getAsJsonArray("datos");
            String[][] multiArray = new String[arr.size()][];
            String[] elementos;
            for (int i = 0; i < arr.size(); i++) {
                get = (JsonObject) arr.get(i);
                elementos = new String[21];
                elementos[0] = get.get("distrito").getAsString();
                elementos[1] = get.get("tipodoc").getAsString();
                elementos[2] = get.get("factura").getAsString();
                elementos[3] = get.get("nit").getAsString();
                elementos[4] = get.get("proveedor").getAsString();
                elementos[5] = get.get("saldo").getAsString();
                elementos[6] = get.get("descripcion").getAsString();
                elementos[7] = get.get("banco").getAsString();
                elementos[8] = get.get("sucursal").getAsString();
                elementos[9] = get.get("agencia").getAsString();
                elementos[10] = get.get("moneda").getAsString();
                elementos[11] = get.get("base").getAsString();
                elementos[12] = get.get("tipo_pago").getAsString();
                elementos[13] = get.get("no_cuenta").getAsString();
                elementos[14] = get.get("tipo_cuenta").getAsString();
                elementos[15] = get.get("banco_transfer").getAsString();
                elementos[16] = get.get("suc_transfer").getAsString();
                elementos[17] = get.get("cedula_cuenta").getAsString();
                elementos[18] = get.get("nombre_cuenta").getAsString();
                elementos[19] = get.get("planilla").getAsString();
                elementos[20] = get.get("placa").getAsString();
                
                multiArray[i] = elementos;
            }
            String[] confiArray = new String[]{objIn.get("usuario").getAsString(),objIn.get("distrito").getAsString(),objIn.get("banco").getAsString(),objIn.get("sucursal").getAsString()};
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            Array miArray, tuArray;
            miArray = con.createArrayOf("text", multiArray);
            tuArray = con.createArrayOf("text", confiArray);
            ps.setArray(1, miArray);
            ps.setArray(2, tuArray); 
            rs = ps.executeQuery();
            
            arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("corrida", rs.getString("corrida"));
                fila.addProperty("transferencia", rs.getString("transferencia"));
                fila.addProperty("egreso", rs.getString("egreso"));
                fila.addProperty("nit", rs.getString("beneficiario"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("valor", rs.getString("valor"));
                fila.addProperty("banco_cod", rs.getString("banco_cod"));
                fila.addProperty("banco_nom", rs.getString("banco_nom"));
                fila.addProperty("cuenta", rs.getString("cuenta"));
                arr.add(fila);
            }
            obj.add("rows",arr);
            retorno = (new Gson()).toJson(obj);
        } catch (Exception ex) {
            retorno = "{\"mensaje\": \"ERROR proceso > "+ex.getMessage()+"\"}";
            ex.printStackTrace();
        } finally {
            if (rs != null) { try { rs.close(); } catch (Exception e){} }
            if (ps != null) { try { ps.close(); } catch (Exception e){} }
            if (con != null) { try { this.desconectar(con); } catch (Exception e){} }
           return retorno;
        }
    }

    @Override
    public String buscarBanco(String usuario, String banco) {
        String query;
        JsonObject obj = new JsonObject();
        JsonArray datos = new JsonArray();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            query = "OBTENER_BANCOS_TRANSFER";
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replace("--INFO--", 
                        (banco.equalsIgnoreCase("")) 
                        ? "DISTINCT a.branch_code"
                        : "a.bank_account_no");
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            ps.setString(2, banco);
            rs = ps.executeQuery();
            
            while(rs.next()) {
                datos.add(new JsonPrimitive(rs.getString("dato")));
            }    
            obj.add("datos",datos);
        } catch(Exception exc) {
        } finally {
             try {
                if (con != null) { this.desconectar(con); }
                if (ps != null) { ps.close();  }
                if (rs != null) { rs.close(); }
            } catch (Exception ex) { }
            return (new Gson()).toJson(obj);
        }        
    }

    @Override
    public String generarArchivo(JsonObject informacion,Usuario u) {
        PrintWriter pw;
        LogWriter logWriter;
        JsonObject jsaux;
        JsonArray jarr;
        String linea = "";
        try{
            jsaux = informacion.get("cabecera").getAsJsonObject();
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta") + "/exportar/migracion/" + informacion.get("usuario").getAsString();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();

            File file = new File(ruta);
            file.mkdirs();
            pw = new PrintWriter(System.err, true);

            ruta += "/NOMINA_PAB_" + jsaux.get("descr_trans").getAsJsonPrimitive().getAsString() + "_"
                 + jsaux.get("secuencia").getAsJsonPrimitive().getAsString() + "_"
                 + jsaux.get("fecha_trans").getAsJsonPrimitive().getAsString()+"_"+u.getBd()+ ".txt";
            
            pw        = new PrintWriter(new FileWriter(ruta, true), true);
            logWriter = new LogWriter("", LogWriter.INFO, pw);
            logWriter.setPrintWriter(pw);
            
            String tipo_cuenta = (jsaux.get("tipo_cuenta").getAsString().equals("CC") || jsaux.get("tipo_cuenta").getAsString().equals("CPAG")) ? "D" : "S";

            linea = "1"
                  + formatoTexto(jsaux.get("nit").getAsString(),15,true,"0")
                  + formatoTexto("I",1,true," ")
                  + formatoTexto("",15,true," ")
                  + formatoTexto(jsaux.get("clase_trans").getAsString(),3,true,"#")
                  + formatoTexto(jsaux.get("descr_trans").getAsString(),10,false," ")
                  + formatoTexto(jsaux.get("fecha_trans").getAsString(),8,false," ")
                  + formatoTexto(jsaux.get("secuencia").getAsString(),2,false," ")
                  + formatoTexto(jsaux.get("fecha_aplica").getAsString(),8,false," ")
                  + formatoTexto(jsaux.get("total_filas").getAsString(),6,true,"0")
                  + formatoTexto("0",17,true,"0")
                  + formatoTexto(jsaux.get("total_credito").getAsString(),17,true,"0")
                  + formatoTexto(jsaux.get("cuenta").getAsString(),11,true,"0")
                  + formatoTexto(tipo_cuenta,1,true," ");
            
            logWriter.log(linea);
    
            jarr = informacion.getAsJsonArray("datos");
            
            for (int i = 0; i < jarr.size(); i++) {
                jsaux = jarr.get(i).getAsJsonObject();
                
                linea = "6"
                      + formatoTexto(jsaux.get("nit").getAsString(),15,false," ")
                      + formatoTexto(jsaux.get("nombre").getAsString(),30,false," ")
                      + formatoTexto(jsaux.get("banco_cod").getAsString(),9,false,"#")
                      + formatoTexto(jsaux.get("cuenta").getAsString(),17,false," ")
                      + formatoTexto("",1,true," ")
                      + formatoTexto("37",2,true," ")
                      + formatoTexto(jsaux.get("valor").getAsString(),17,true,"0")
                      + formatoTexto("",8,true,"0")
                      + formatoTexto("",27,true," ");
            
                logWriter.log(linea);
            }

        }catch (Exception ex){
            ex.printStackTrace();
            return "{\"mensaje\": \"ERROR proceso > "+ex.getMessage()+"\"}";
        }
        return "{\"mensaje\": \"Se ha terminado de generar el archivo, revise su log.\"}";
    }
    
    private String formatoTexto(String texto, int max_caracter, boolean prefijo, String char_defecto) {
        int vacio = max_caracter - texto.length();
        String relleno = "";
        
        if (vacio <= 0) {
            return texto.substring(0, max_caracter);
        }
        for(int i = 0; i < vacio; i++) {
            relleno += char_defecto;
        }
        if(prefijo) {
            return relleno + texto;
        } else {
            return texto + relleno;
        }
    }

    @Override
    public String generarPrevio(JsonObject ojb) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query;
        String retorno="";
        JsonObject obj = new JsonObject(), fila;
        JsonArray arr;
        try {
            query = "USUARIO_AUTORIZADO";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, ojb.get("distrito").getAsString());
            ps.setString(2, ojb.get("usuario").getAsString());
            ps.setString(3, ojb.get("banco").getAsString());
            ps.setString(4, ojb.get("sucursal").getAsString());
            rs = ps.executeQuery();
            
            if(rs.next()) {
                if (rs.getString("texto").startsWith("NEGADO")) {
                    return "{\"mensaje\": \"El usuario no tiene permitido generar estos documentos\"}";
                } else {
                    fila = new JsonObject();
                    fila.addProperty("nit", rs.getString("nit"));
                    fila.addProperty("banco", rs.getString("banco"));
                    fila.addProperty("sucursal", rs.getString("sucursal"));
                    fila.addProperty("cuenta", rs.getString("cuenta"));
                    obj.add("cabecera", fila);
                }
            } else {
                return "{\"mensaje\": \"No hay informacion del banco\"}";
            }
        } catch(Exception exc) {
            exc.printStackTrace();
            return "{\"mensaje\": \"Error permisos > "+exc.getMessage()+"\"}";
        } finally {
            if (rs != null) { try { rs.close(); } catch (Exception e){} }
            if (ps != null) { try { ps.close(); } catch (Exception e){} }
            if (con != null) { try { this.desconectar(con); } catch (Exception e){} }
        } 
        try {
            query = "BUSCAR_EGRESOS_NOMINA";
            
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query).replace("#NOT#", (ojb.get("filGerencia").getAsString().equals("N")?"NOT":"")));
            ps.setString(1, ojb.get("banco").getAsString());
            ps.setString(2, ojb.get("sucursal").getAsString());
            ps.setString(3, ojb.get("usuario").getAsString());
            rs = ps.executeQuery();
            
            arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("corrida", rs.getString("corrida"));
                fila.addProperty("transferencia", rs.getString("transferencia"));
                fila.addProperty("egreso", rs.getString("egreso"));
                fila.addProperty("nit", rs.getString("beneficiario"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("valor", rs.getString("valor"));
                fila.addProperty("banco_cod", rs.getString("banco_cod"));
                fila.addProperty("banco_nom", rs.getString("banco_nom"));
                fila.addProperty("cuenta", rs.getString("cuenta"));
                arr.add(fila);
            }
            obj.add("rows",arr);
            retorno = (new Gson()).toJson(obj);
        } catch (Exception ex) {
            retorno = "{\"mensaje\": \"ERROR proceso > "+ex.getMessage()+"\"}";
            ex.printStackTrace();
        } finally {
            if (rs != null) { try { rs.close(); } catch (Exception e){} }
            if (ps != null) { try { ps.close(); } catch (Exception e){} }
            if (con != null) { try { this.desconectar(con); } catch (Exception e){} }
           return retorno;
        }
    }
    
}