/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.PrioridadesRequisicionesDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author egonzalez
 */
public class PrioridadesRequisicionesImpl extends MainDAO implements PrioridadesRequisicionesDAO {

    public PrioridadesRequisicionesImpl() {
        super("PrioridadesRequisiciones.xml");
    }
    
    public PrioridadesRequisicionesImpl(String databaseName) {
        super("PrioridadesRequisiciones.xml", databaseName);
    }

    @Override
    public JsonObject buscar_requisiciones(JsonObject info) {
        JsonObject respuesta = new JsonObject(), fila;
        JsonArray arr = new JsonArray();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_REQUISICIONES";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, info.get("anho").getAsString() + "-" + info.get("mes").getAsString());
            ps.setInt(2, info.get("proceso").getAsInt());
            rs = ps.executeQuery();
            
            while (rs.next()) {
                fila = new JsonObject();
                
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("tipo_requisicion", rs.getString("tipo_requisicion"));
                fila.addProperty("proceso_interno", rs.getString("proceso_interno"));
                fila.addProperty("radicado", rs.getString("radicado"));
                fila.addProperty("fch_radicacion", rs.getString("fch_radicacion"));
                fila.addProperty("usuario_generador", rs.getString("usuario_generador"));
                fila.addProperty("asunto", rs.getString("asunto"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("prioridad", rs.getString("prioridad"));
                fila.addProperty("orden_prioridad", rs.getString("orden_priorizacion"));
                fila.addProperty("solucionador_responsable", rs.getString("solucionador_responsable"));
                fila.addProperty("fecha_inicio_estimado", rs.getString("fecha_inicio_estimado"));
                fila.addProperty("fecha_fin_estimado", rs.getString("fecha_fin_estimado"));
                fila.addProperty("horas_trabajo", rs.getString("horas_trabajo"));
                fila.addProperty("fecha_inicio_actividades", rs.getString("fecha_inicio_actividades"));
                fila.addProperty("fch_cierre", rs.getString("fch_cierre"));
                fila.addProperty("estado_requisicion", rs.getString("estado_requisicion"));
                arr.add(fila);
            }
            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject modificar_prioridades(JsonObject info) {
        JsonObject respuesta = new JsonObject();
        StringStatement ss = null;
        TransaccionService tsrv = null;
        String query = "";
        try {
            tsrv = new TransaccionService(this.getDatabaseName());
            tsrv.crearStatement();
            JsonArray arr = info.getAsJsonArray("rows");
            for(int i = 0; i < arr.size(); i++) {
                respuesta = arr.get(i).getAsJsonObject();
                ss = new StringStatement(this.obtenerSQL("MODIFICAR_PRIORIDAD"), true);
                ss.setInt(1, i + 1);
                ss.setString(2, info.get("usuario").getAsString());
                ss.setInt(3, respuesta.get("id").getAsInt());
                tsrv.getSt().addBatch(ss.getSql());
            }
            tsrv.execute();
            respuesta = new JsonObject();
            respuesta.addProperty("mensaje", "Edici�n satisfactoria");
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (tsrv != null) {
                    tsrv.closeAll();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject getFiltro(String consulta) {
        JsonObject respuesta = new JsonObject();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_PROCESOS";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            
            respuesta = new JsonObject();
            while (rs.next()) {
                respuesta.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }
    
    @Override
    public JsonObject getUsuariosProceso(String idProceso) {
        JsonObject respuesta = new JsonObject();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_USUARIOS_PROCESO";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(idProceso));
            rs = ps.executeQuery();
            
            respuesta = new JsonObject();
            while (rs.next()) {
                respuesta.addProperty(rs.getString("id_usuario"), rs.getString("login"));
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }
    
}
