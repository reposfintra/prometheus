/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.AdministracionFintraDAO;
import com.tsp.operation.model.beans.EstadoCuentaIngresos;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dvalencia
 */
public class AdministracionFintraImpl extends MainDAO implements AdministracionFintraDAO {

    public AdministracionFintraImpl(String dataBaseName) {
        super("AdministracionFintraDAO.xml", dataBaseName);
    }

    @Override
    public String cargarNegocios(String cliente, String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_NEGOCIOS_CLIENTE";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtroInterno = "";
            if (!cliente.equals("")) {
                parametro = parametro + "AND neg.cod_cli = '" + cliente + "'";
                filtroInterno = filtroInterno + "AND cod_cli = '" + cliente + "'";
            }
            if (!negocio.equals("")) {
                parametro = parametro + "AND neg.cod_neg = '" + negocio + "'";
                filtroInterno = filtroInterno + "AND fac.negasoc in  (select cod_neg \n" +
                                                    "from negocios \n" +
                                                    "where cod_cli = (\n" +
                                                    "select cod_cli \n" +
                                                    "from negocios \n" +
                                                    "where \n" +
                                                    "cod_neg = '"+negocio+"'\n" +
                                                    ")\n" +
                                                    "and estado_neg in ('T', 'A') \n" +
                                                    "group by \n" +
                                                    "fecha_negocio::date,\n" +
                                                    "cod_neg \n" +
                                                    ") ";
            }
            consulta = this.obtenerSQL(query).replaceAll("#PARAMETRO", parametro);
            consulta = consulta.replaceAll("#FILTROINTERNO", filtroInterno);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String generarPdf(String usuario, String negocio) {
        String directorio = "";
        String msg = "OK";
        try {

            ResourceBundle rb = null;

            //buscamos la info de la cabecera del extracto.
            EstadoCuentaIngresos epi = new EstadoCuentaIngresos();
            epi = infoCabeceraEstado(negocio);
            ArrayList<EstadoCuentaIngresos> apiArray = infoCabeceraEstadoDetalle(negocio);
            //epi=infoCabeceraEstadoDetalle(negocio);

            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            Image imagen = null;
            imagen = Image.getInstance(rb.getString("ruta") + "/images/Logo-Fintra.png");

            directorio = this.directorioArchivo(usuario, "EstadoCuenta" + negocio, "pdf");
            Font fuente = new Font(Font.HELVETICA, 9, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font fuenteG = new Font(Font.HELVETICA, 9, Font.BOLD, new java.awt.Color(0, 0, 0));
            Font fuenteT = new Font(Font.HELVETICA, 9, Font.BOLD, new java.awt.Color(255, 255, 255));

            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.open();
            documento.newPage();
            imagen.setAlignment(PdfPTable.ALIGN_CENTER);
//            imagen.scaleToFit(600, 780);
            //imagen.scaleAbsolute(580, 835);
            imagen.setAbsolutePosition(2, 50f);
            imagen.scalePercent(75);
            documento.add(imagen);

            //logo de fintra aqui
            //creamos una tabla para el logo
            PdfPTable tabla_temp = new PdfPTable(1);
            tabla_temp.setWidthPercentage(20);
//
//            String url_logo ="logo_fintra_new.jpg";
//            com.lowagie.text.Image img = com.lowagie.text.Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
//            img.scaleToFit(150, 150);
//            img.setBorder(1);
//
            PdfPCell celda_logo = new PdfPCell();
            celda_logo = new PdfPCell();
            celda_logo.setBorderWidthTop(0);
            celda_logo.setBorderWidthLeft(0);
            celda_logo.setBorderWidthRight(0);
            celda_logo.setBorderWidthBottom(0);
            celda_logo.setPhrase(new Phrase(" ", fuente));
//            celda_logo.addElement(img);
//            celda_logo.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//            //agremos la celda con la imagen a la tabla del logo
//            tabla_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tabla_temp.addCell(celda_logo);
//
            documento.add(tabla_temp);
//            
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            PdfPTable table = new PdfPTable(2);
            table.setWidths(new int[]{3, 7});
            table.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            table.setWidthPercentage(70);
            PdfPCell cell;
            Color ColorRelleno=new Color(50,190,250);//Azul
            // informacion del negocio
            cell = new PdfPCell(new Phrase("Informacion del Negocio", fuenteT));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            cell.setBackgroundColor(ColorRelleno);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Nombre Cliente", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getNombre_cliente(), fuente));
            cell.setBackgroundColor(new Color(255, 255, 255));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Nit Cliente", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getNit(), fuente));
            cell.setBackgroundColor(new Color(255, 255, 255));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getCod_neg(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Fecha Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getFecha_negocio(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Valor Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getVr_negocio(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            documento.add(table);

            documento.add(new Paragraph(new Phrase(" ", fuente)));

            //crea aqui las cabeceras de la tabla //
            PdfPTable table2 = new PdfPTable(11);
            table2.setWidths(new int[]{3, 5, 8, 7, 7, 7, 7, 7, 7, 7, 7});
            table2.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            table2.setWidthPercentage(100);
            PdfPCell cell2;

            // informacion del negocio
            cell2 = new PdfPCell(new Phrase("Informacion de los Pagos", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setColspan(11);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            documento.add(new Paragraph(new Phrase(" ", fuente)));

            int numfilas = 0;
            cell2 = new PdfPCell(new Phrase("", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("No. Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Fecha Vencimiento", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Fecha Ingreso", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Numero Ingreso", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Factura", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Ingreso", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Abono Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Abono IXM", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Abono GAC", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Saldo Factura", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            table2.addCell(cell2);

            //Detalle
            for (int i = 0; i < apiArray.size(); i++) {

                numfilas = numfilas + 1;

                cell2 = new PdfPCell(new Phrase(String.valueOf(numfilas), fuenteT));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(ColorRelleno);
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getNum_doc_fen(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getFecha_vencimiento(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getFecha_ingreso(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getNum_ingreso(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getValor_factura(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getVlr_ingreso(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getValor_aplicado(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getIxm(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getGac(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getValor_saldo(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);
            }

            documento.add(table2);
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
//            documento.add(new Paragraph(new Phrase(" ", fuente)));
//            documento.add(new Paragraph(new Phrase(" ", fuente)));
//            documento.add(new Paragraph(new Phrase(" ", fuente)));
//            documento.add(new Paragraph(new Phrase(" ", fuente)));

            documento.add(new Paragraph(new Phrase("____________________________________", fuenteG)));
            documento.add(new Paragraph(new Phrase("Direccion Servicio Al Cliente", fuenteG)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.close();

        } catch (SQLException ex) {
            Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msg;

    }

    private Document createDoc() {
        Document doc = new Document(PageSize.A4, 40, 40, 40, 40);
        return doc;
    }

    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons + "_" + fmt.format(new Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    private EstadoCuentaIngresos infoCabeceraEstado(String negocio) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "INFORMACION_ESTADO_CUENTA_INGRESOS";
        EstadoCuentaIngresos epi = null;
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();

            while (rs.next()) {

                epi = new EstadoCuentaIngresos();
                epi.setCod_neg(rs.getString("cod_neg"));
                epi.setNombre_cliente(rs.getString("nombre_cliente"));
                epi.setNit(rs.getString("nit"));
                epi.setFecha_negocio(rs.getString("fecha_negocio"));
                epi.setVr_negocio(rs.getString("vr_negocio"));

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO INFORMACION_EXTRACTO: infoCabeceraExracto(int id_rop)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return epi;
    }

    private ArrayList<EstadoCuentaIngresos> infoCabeceraEstadoDetalle(String negocio) throws SQLException {
        ArrayList<EstadoCuentaIngresos> info = new ArrayList<EstadoCuentaIngresos>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "INFORMACION_ESTADO_CUENTA_INGRESOS_DETALLE";
        EstadoCuentaIngresos epi = null;
        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();

            while (rs.next()) {

                epi = new EstadoCuentaIngresos();
                epi.setNum_doc_fen(rs.getString("num_doc_fen"));
                epi.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                epi.setFecha_ingreso(rs.getString("fecha_ingreso"));
                epi.setNum_ingreso(rs.getString("num_ingreso"));
                epi.setValor_factura(rs.getString("valor_factura"));
                epi.setVlr_ingreso(rs.getString("vlr_ingreso"));
                epi.setValor_aplicado(rs.getString("valor_aplicado"));
                epi.setIxm(rs.getString("ixm"));
                epi.setGac(rs.getString("gac"));
                epi.setValor_saldo(rs.getString("valor_saldo"));
                info.add(epi);

            }
        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO INFORMACION_EXTRACTO: infoCabeceraExracto(int id_rop)  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }

        return info;
    }

    public String cargaPresolicitudesMicrocredito(String fechaInicio, String fechaFin, String identificacion, String numero_solicitud, String estado_solicitud, String etapa, String r_operaciones) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PRESOLICITUDES_MICROCREDITO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND pre.fecha_credito::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND pre.identificacion = '" + identificacion + "'";
            }
            if (!numero_solicitud.equals("")) {
                parametro = parametro + " AND pre.numero_solicitud = '" + numero_solicitud + "'";
            }
            if (!estado_solicitud.equals("")) {
                parametro = parametro + " AND pre.estado_sol = '" + estado_solicitud + "'";
            }

            if (!etapa.equals("")) {
                parametro = parametro + " AND pre.etapa = '" + etapa + "'";
            }
            if (!r_operaciones.equals("")) {
                parametro = parametro + " AND pre.rechazo_operaciones= '" + r_operaciones + "'";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    public String rechazarnegociomicrocredito(Usuario usuario, String numero_solicitud) {

        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_RECHAZAR_NEGOCIO_MICROCREDITO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, numero_solicitud);
            respuesta = st.getSql();

        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        } finally {
            return respuesta;
        }
    }

    public String standbytrazabilidadpresolicitudes(Usuario usuario, String numero_solicitud, String causal, String estado) {

        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_TRAZA_STANDBY_PRESOLICITUD";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, numero_solicitud);
            st.setString(2, estado);
            st.setString(3, usuario.getLogin());
            st.setString(4, causal);
            respuesta = st.getSql();

        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        } finally {
            return respuesta;
        }
    }

    public String generarPdfConsolidado(String usuario, String negocio) {
        String directorio = "";
        String msg = "OK";
        try {

            ResourceBundle rb = null;

            //buscamos la info de la cabecera del extracto.
            EstadoCuentaIngresos epi = new EstadoCuentaIngresos();
            epi = infoCabeceraEstado(negocio);
            ArrayList<EstadoCuentaIngresos> apiArray = infoCabeceraEstadoDetalle(negocio);
            //epi=infoCabeceraEstadoDetalle(negocio);

            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(usuario, "EstadoCuenta" + negocio, "pdf");
            Font fuente = new Font(Font.HELVETICA, 9, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font fuenteG = new Font(Font.HELVETICA, 9, Font.BOLD, new java.awt.Color(0, 0, 0));
            Font fuenteT = new Font(Font.HELVETICA, 9, Font.BOLD, new java.awt.Color(255, 255, 255));

            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.open();
            documento.newPage();

            //logo de fintra aqui
            //creamos una tabla para el logo
            PdfPTable tabla_temp = new PdfPTable(1);
            tabla_temp.setWidthPercentage(20);

            String url_logo = "logo_fintra_new.jpg";
            com.lowagie.text.Image img = com.lowagie.text.Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
            img.scaleToFit(150, 150);
            img.setBorder(1);

            PdfPCell celda_logo = new PdfPCell();
            celda_logo = new PdfPCell();
            celda_logo.setBorderWidthTop(0);
            celda_logo.setBorderWidthLeft(0);
            celda_logo.setBorderWidthRight(0);
            celda_logo.setBorderWidthBottom(0);
            celda_logo.setPhrase(new Phrase(" ", fuente));
            celda_logo.addElement(img);
            celda_logo.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            //agremos la celda con la imagen a la tabla del logo
            tabla_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tabla_temp.addCell(celda_logo);

            documento.add(tabla_temp);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            PdfPTable table = new PdfPTable(2);
            table.setWidths(new int[]{3, 7});
            table.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            table.setWidthPercentage(70);
            PdfPCell cell;

            // informacion del negocio
            cell = new PdfPCell(new Phrase("Informacion del Negocio", fuenteT));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(61, 67, 102));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Nombre Cliente", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getNombre_cliente(), fuente));
            cell.setBackgroundColor(new Color(255, 255, 255));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Nit Cliente", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getNit(), fuente));
            cell.setBackgroundColor(new Color(255, 255, 255));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getCod_neg(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Fecha Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getFecha_negocio(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Valor Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(epi.getVr_negocio(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            documento.add(table);

            documento.add(new Paragraph(new Phrase(" ", fuente)));

            //crea aqui las cabeceras de la tabla //
            PdfPTable table2 = new PdfPTable(9);
            table2.setWidths(new int[]{3, 5, 8, 7, 7, 7, 7, 7, 7});
            table2.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            table2.setWidthPercentage(100);
            PdfPCell cell2;

            // informacion del negocio
            cell2 = new PdfPCell(new Phrase("Informacion de los Pagos", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setColspan(11);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            documento.add(new Paragraph(new Phrase(" ", fuente)));

            int numfilas = 0;
            cell2 = new PdfPCell(new Phrase("", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("No. Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Fecha Vencimiento", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Fecha Ingreso", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Numero Ingreso", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Factura", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Ingreso", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            cell2 = new PdfPCell(new Phrase("Valor Abono Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

//            cell2 = new PdfPCell(new Phrase("Valor Abono IXM", fuenteT));
//            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell2.setBackgroundColor(new Color(61, 67, 102));
//            table2.addCell(cell2);
//
//            cell2 = new PdfPCell(new Phrase("Valor Abono GAC", fuenteT));
//            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
//            cell2.setBackgroundColor(new Color(61, 67, 102));
//            table2.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Saldo Factura", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(61, 67, 102));
            table2.addCell(cell2);

            //Detalle
            for (int i = 0; i < apiArray.size(); i++) {

                numfilas = numfilas + 1;

                cell2 = new PdfPCell(new Phrase(String.valueOf(numfilas), fuenteT));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(61, 67, 102));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getNum_doc_fen(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getFecha_vencimiento(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getFecha_ingreso(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getNum_ingreso(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getValor_factura(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getVlr_ingreso(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getValor_aplicado(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);

//                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getIxm(), fuente));
//                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                cell2.setBackgroundColor(new Color(255, 255, 255));
//                 table2.addCell(cell2);
//                 
//                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getGac(), fuente));
//                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
//                cell2.setBackgroundColor(new Color(255, 255, 255));
//                 table2.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(apiArray.get(i).getValor_saldo(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                table2.addCell(cell2);
            }

            documento.add(table2);
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
//            documento.add(new Paragraph(new Phrase(" ", fuente)));
//            documento.add(new Paragraph(new Phrase(" ", fuente)));
//            documento.add(new Paragraph(new Phrase(" ", fuente)));
//            documento.add(new Paragraph(new Phrase(" ", fuente)));

            documento.add(new Paragraph(new Phrase("____________________________________", fuenteG)));
            documento.add(new Paragraph(new Phrase("Direccion Servicio Al Cliente", fuenteG)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.close();

        } catch (SQLException ex) {
            Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msg;

    }

    @Override
    public String cargarPagadurias() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PAGADURIAS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"Error al cargar las pagadurķas\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarNegocioLibranza(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NEGOCIO";
        JsonObject objetoJson = null;
        String informacion = null;
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
            }
            informacion = new Gson().toJson(objetoJson);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"Error al buscar el negocio\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String actualizarPagaduriaNegocio(String negocio, String pagaduria, String user) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ACTUALIZAR_PAGADURIA_NEGOCIO";
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            ps.setString(2, pagaduria);
            ps.setString(3, user);
            ps.setString(4, "");
            rs = ps.executeQuery();
            while (rs.next()) {
                objetoJson = new JsonObject();
                objetoJson.addProperty("respuesta", rs.getString("actualizar_pagaduria"));
            }
            informacion = new Gson().toJson(objetoJson);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"No se pudo actualizar la pagadurķa\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarTrazabilidadPagaduria(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TRAZABILIDAD";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"No se pudo cargar la trazabilidad\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String buscarScoreSolicitudesCliente(int idCliente, String unidadNegocio) {
        String jsonResponse = "[]", query = null;

        if (unidadNegocio.equals("MICRO")) {
            query = obtenerSQL("BUSCAR_SOLICITUDES_SCORE_CLIENTE_MICRO");
        } else if (unidadNegocio.equals("EDUCA")) {
            query = obtenerSQL("BUSCAR_SOLICITUDES_SCORE_CLIENTE_EDUCA");
        }

        try (Connection conn = conectarJNDIFintra(); PreparedStatement ps = conn.prepareStatement(query);) {
            ps.setInt(1, idCliente);

            ResultSet rs = ps.executeQuery();

            JsonObject solicitudJson;
            JsonArray solicitudesJson = new JsonArray();

            while (rs.next()) {
                solicitudJson = new JsonObject();

                Util.resultSetToJson(rs, solicitudJson);

                solicitudesJson.add(solicitudJson);
            }
            jsonResponse = new Gson().toJson(solicitudesJson);
        } catch (SQLException e) {
            System.err.println("Error cargando el historial de puntajes: " + e.getMessage());
            jsonResponse = "";
        }
        return jsonResponse;
    }

    @Override
    public String buscarScoreSolicitud(int numeroSolicitud, String unidadNegocio) {        
        String jsonResponse = "[]", query = null;

        if (unidadNegocio.equals("MICRO")) {
            query = obtenerSQL("BUSCAR_SOLICITUD_SCORE_MICRO");
        } else if (unidadNegocio.equals("EDUCA")) {
            query = obtenerSQL("BUSCAR_SOLICITUD_SCORE_EDUCA");
        }

        try (Connection conn = conectarJNDIFintra(); PreparedStatement ps = conn.prepareStatement(query);) {
            ps.setInt(1, numeroSolicitud);

            ResultSet rs = ps.executeQuery();

            JsonObject solicitudJson;
            JsonArray solicitudesJson = new JsonArray();
            while (rs.next()) {
                solicitudJson = new JsonObject();

                Util.resultSetToJson(rs, solicitudJson);

                solicitudesJson.add(solicitudJson);
            }
            jsonResponse = new Gson().toJson(solicitudesJson);
        } catch (SQLException e) {
            System.err.println("Error cargando los puntajes de la solicitud: " + e.getMessage());
            jsonResponse = "";
        }
        return jsonResponse;
    }
    
    @Override
    public String FiltrosDuros(String opcionBuscar, int numeroSolicitud) {        
        String jsonResponse = "[]", query = null;
        
        if(opcionBuscar.equals("10")){
            query = obtenerSQL("BUSCAR_FILTRO_DURO");
        }else if(opcionBuscar.equals("11")){
            query = obtenerSQL("BUSCAR_FILTRO_DURO_NUMERO_SOLICITUD");
        }
        
        try (Connection conn = conectarJNDIFintra(); PreparedStatement ps = conn.prepareStatement(query);) {
            ps.setInt(1, numeroSolicitud);

            ResultSet rs = ps.executeQuery();

            JsonObject solicitudJson;
            JsonArray solicitudesJson = new JsonArray();
            while (rs.next()) {
                solicitudJson = new JsonObject();

                Util.resultSetToJson(rs, solicitudJson);

                solicitudesJson.add(solicitudJson);
            }
            jsonResponse = new Gson().toJson(solicitudesJson);
        } catch (SQLException e) {
            System.err.println("Error cargando filtros duros: " + e.getMessage());
            jsonResponse = "";
        }
        return jsonResponse;
    }
}
