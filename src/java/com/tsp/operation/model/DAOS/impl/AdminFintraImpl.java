/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.operation.model.DAOS.AdminFintraDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.ReestructurarNegociosDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.ArchivosAppMicro;
import com.tsp.operation.model.beans.AtributosRowsProd;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.ParmetrosDinamicaTSP;
import com.tsp.operation.model.beans.ReporteProducionBeans;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HDinamicaContable;
import com.tsp.util.Util;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

/**
 *
 * @author egonzalez
 */
public class AdminFintraImpl extends MainDAO implements AdminFintraDAO {

    public AdminFintraImpl(String dataBaseName) {
        super("AdminFintraDAO.xml", dataBaseName);
    }

    @Override
    public String cargarAuditoriaCredi100(String fechaInicio, String fechaFin, String identificacion, String linea_negocio, String numero_solicitud, String negocio, String estado_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_AUDITORIA_CREDI100";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND pre.fecha_credito::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!linea_negocio.equals("")) {
                parametro = parametro + " AND  unineg.id = '" + linea_negocio + "'";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND pre.identificacion = '" + identificacion + "'";
            }
            if (!numero_solicitud.equals("")) {
                parametro = parametro + " AND pre.numero_solicitud = '" + numero_solicitud + "'";
            }
            if (!estado_solicitud.equals("")) {
                parametro = parametro + " AND neg.estado_neg = '" + estado_solicitud + "'";
            }
            if (!negocio.equals("")) {
                parametro = parametro + " AND neg.cod_neg = upper ( '" + negocio + "')";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarLineaNegocio() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_LINEA_NEGOCIO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarSolicitudesDocumentos(String fechaInicio, String fechaFin, String linea_negocio, String empresa, String consultaDep) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try (Connection con = this.conectarJNDI()) {
            if (consultaDep.equals("visualizar")) {
                query = "SQL_CARGAR_SOLICITUD_DOCUMENTOS_VISUALIZAR";
            } else if (consultaDep.equals("buscar")) {
                query = "SQL_CARGAR_SOLICITUD_DOCUMENTOS";
            }

            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                if (consultaDep.equals("visualizar")) {
                    parametro = " AND reldoc.creation_date::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
                } else {
                    parametro = " AND solaval.creation_date::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
                }

            }
            if (!linea_negocio.equals("")) {
                parametro = " AND  unineg.id = '" + linea_negocio + "'";
            }
            if (!empresa.equals("")) {
                parametro = " AND  empresa = '" + empresa + "'";
            }

            String consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String guardarSolicitudesSeleccionadas(JsonObject info, Usuario usuario, String empresa_envio, String exportar) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = info.getAsJsonArray("json");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                if (objeto.get("entregado").getAsString().equals("N")) {
                    st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_SOLOCITUDES_ASIGNADAS"), true);
                    st.setString(1, objeto.get("numero_solicitud").getAsString());
                } else if (objeto.get("entregado").getAsString().equals("S")) {
                    if (info.get("operacion").getAsString().equals("reasignar")) {
                        st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_EMPRESA_MENSAJERIA"), true);
                        st.setString(1, empresa_envio);
                        st.setString(2, objeto.get("numero_solicitud").getAsString());
                    } else {
                        st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_SOLOCITUDES_ASIGNADAS_RECIBIR"), true);
                        st.setString(1, objeto.get("numero_solicitud").getAsString());
                    }

                } else {
                    st = new StringStatement(this.obtenerSQL("SQL_INSERT_SOLOCITUDES_ASIGNADAS"), true);
                    st.setString(1, objeto.get("numero_solicitud").getAsString());
                    st.setString(2, objeto.get("nombre").getAsString() != null ? objeto.get("nombre").getAsString() : "");
                    st.setString(3, !objeto.get("identificacion").getAsString().equals("") ? objeto.get("identificacion").getAsString() : "0");
                    st.setString(4, objeto.get("direccion").getAsString() != null ? objeto.get("direccion").getAsString() : "");
                    st.setString(5, objeto.get("telefono").getAsString() != null ? objeto.get("telefono").getAsString() : "");
                    st.setString(6, !objeto.get("celular").getAsString().equals("") ? objeto.get("celular").getAsString() : "0");
                    st.setString(7, objeto.get("nombre_cod").getAsString() != null ? objeto.get("nombre_cod").getAsString() : "");
                    st.setString(8, !objeto.get("identificacion_cod").getAsString().equals("") ? objeto.get("identificacion_cod").getAsString() : "0");
                    st.setString(9, objeto.get("direccion_cod").getAsString() != null ? objeto.get("direccion_cod").getAsString() : "");
                    st.setString(10, objeto.get("telefono_cod").getAsString() != null ? objeto.get("telefono_cod").getAsString() : "");
                    st.setString(11, !objeto.get("celular_cod").getAsString().equals("") ? objeto.get("celular_cod").getAsString() : "0");
                    st.setString(12, objeto.get("cod_neg").getAsString() != null ? objeto.get("cod_neg").getAsString() : "");
                    st.setString(13, !objeto.get("fecha_solcitud").getAsString().equals("") ? objeto.get("fecha_solcitud").getAsString() : "0099-01-01");
                    st.setString(14, !objeto.get("fecha_entrega_documento").getAsString().equals("") ? objeto.get("fecha_entrega_documento").getAsString() : "0099-01-01");
                    st.setString(15, !objeto.get("fecha_recibido").getAsString().equals("") ? objeto.get("fecha_recibido").getAsString() : "0099-01-01");
                    st.setString(16, exportar);
                    st.setString(17, usuario.getLogin());
                    st.setString(18, empresa_envio);
                }
                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"GUARDADO\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarEmpresaEnvio() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EMPRESA_ENVIO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre_empresa"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarLineaNegocio_() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_LINEA_NEGOCIO_";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarAutorizador() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_AUTORIZADOR";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("codigo"), rs.getString("codigo"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarHC() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_HC";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("table_code"), rs.getString("table_code"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String buscarProveedor(String informacion_, String parametro_busqueda) {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        JsonObject obj = new JsonObject();
        String query = "SQL_BUSCAR_PROVEEDORES";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            String parametro = "";
            if (parametro_busqueda.equals("nit")) {
                parametro = " AND  nit ilike '%" + informacion_ + "%'";
            } else if (parametro_busqueda.equals("nombre")) {
                parametro = " AND  payment_name ilike  '%" + informacion_ + "%'";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("label", rs.getString("info"));
                fila.addProperty("value", rs.getString("nit"));
                fila.addProperty("mivar1", rs.getString("nombre"));
                arr.add(fila);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(arr);
        }
    }

    @Override
    public String buscarImpuesto(String impuesto) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_IMPUESTO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, impuesto);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String guardarCabeceraCXP(JsonObject info, Usuario usuario) {
        String respuesta = "";
        String informacion = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INSERT_CXP_CABECERA";
        try {

            String numeroDocumento = numDocumento();
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numeroDocumento);
            ps.setString(2, info.get("num_factura").getAsString());
            ps.setString(3, info.get("valorLegalizar").getAsString());
            ps.setString(4, info.get("anticipo").getAsString());
            ps.setString(5, usuario.getLogin());
            ps.setString(6, info.get("autorozador_").getAsString());
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = rs.getBoolean("retorno") ? "SI" : "NO";
            }

            if (informacion.equals("SI")) {
                guardarDocumentosCXP(info, numeroDocumento, usuario);
            }
            respuesta = "{\"respuesta\":\"" + numeroDocumento + "\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    public String guardarDocumentosCXP(JsonObject info, String documento, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String cod_impuesto = "";
        String porcent_impuesto = "";
        String sqlver = "";
        double valorImpuesto = 0;
        String proveedor_cab = "";
        int signo;
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray detalle = info.getAsJsonArray("detalle");
            proveedor_cab = info.get("empleado_").getAsString();
            for (int i = 0; i < detalle.size(); i++) {
                objeto = detalle.get(i).getAsJsonObject();
                if (objeto.get("id").getAsString().startsWith("neo_")) {
                    st = new StringStatement(this.obtenerSQL("SQL_INSERT_CXP_DETALLE"), true);
                    st.setString(1, proveedor_cab);
                    st.setString(2, documento);
                    st.setString(3, objeto.get("item").getAsString() != null ? objeto.get("item").getAsString() : "0");
                    st.setString(4, objeto.get("descripcion").getAsString() != null ? objeto.get("motivo").getAsString() + " - " + objeto.get("descripcion").getAsString() : "");
                    st.setString(5, !objeto.get("valor").getAsString().equals("") ? objeto.get("valor").getAsString() : "0");
                    st.setString(6, !objeto.get("valor").getAsString().equals("") ? objeto.get("valor").getAsString() : "0");
                    st.setString(7, objeto.get("codigo_cuenta").getAsString() != null ? objeto.get("codigo_cuenta").getAsString() : "");
                    st.setString(8, usuario.getLogin());
                    st.setString(9, objeto.get("proveedor").getAsString());
                    st.setString(10, objeto.get("numos").getAsString() != null ? objeto.get("numos").getAsString() : "");
                    st.setString(11, objeto.get("multiservicio").getAsString() != null ? objeto.get("multiservicio").getAsString() : "");
                    st.setString(12, objeto.get("referencia_1").getAsString() != null ? objeto.get("referencia_1").getAsString() : "");
                    //st.setString(13, objeto.get("referencia_1").getAsString() != null ? objeto.get("referencia_1").getAsString() : "");

                }
                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
            }
            for (int i = 0; i < detalle.size(); i++) {
                objeto = detalle.get(i).getAsJsonObject();
                if (objeto.get("id").getAsString().startsWith("neo_")) {
                    if (!objeto.get("cod_impuesto_iva").getAsString().equals("") || !objeto.get("cod_impuesto_rica").getAsString().equals("") || !objeto.get("cod_impuesto_rtfuente").getAsString().equals("")) {

                        if (!objeto.get("cod_impuesto_iva").getAsString().equals("")) { //&& objeto.get("ivaPorct").getAsString().equals("0.0000")
                            signo = objeto.get("ivaSigno").getAsInt();
                            cod_impuesto = objeto.get("cod_impuesto_iva").getAsString();
                            porcent_impuesto = objeto.get("porc_iva").getAsString();
                            valorImpuesto = (Util.roundByDecimal((objeto.get("valor").getAsDouble() * objeto.get("porc_iva").getAsDouble()) / 100, 0)) * signo;
                            st = new StringStatement(this.obtenerSQL("SQL_INSERT_IMPUESTOS_CABECERA"), true);
                            st.setString(1, proveedor_cab);
                            st.setString(2, documento);
                            st.setString(3, cod_impuesto);
                            st.setString(4, porcent_impuesto);
                            st.setDouble(5, valorImpuesto);
                            st.setDouble(6, valorImpuesto);
                            st.setString(7, usuario.getLogin());
                            st.setString(8, objeto.get("proveedor").getAsString());
                            st.setString(9, objeto.get("item").getAsString() != null ? objeto.get("item").getAsString() : "0");
                            sqlver = st.getSql();
                            System.out.println(sqlver);
                            tService.getSt().addBatch(sqlver);

                            //DETALLE
                            st = new StringStatement(this.obtenerSQL("SQL_INSERT_IMPUESTOS_DETALLE"), true);
                            st.setString(1, proveedor_cab);
                            st.setString(2, documento);
                            st.setString(3, objeto.get("item").getAsString() != null ? objeto.get("item").getAsString() : "0");
                            st.setString(4, cod_impuesto);
                            st.setString(5, porcent_impuesto);
                            st.setDouble(6, valorImpuesto);
                            st.setDouble(7, valorImpuesto);
                            st.setString(8, usuario.getLogin());
                            st.setString(9, objeto.get("proveedor").getAsString());
                            sqlver = st.getSql();
                            System.out.println(sqlver);
                            tService.getSt().addBatch(sqlver);
                        }
                        if (!objeto.get("cod_impuesto_iva").getAsString().equals("") && !objeto.get("porc_riva").getAsString().equals("0.0000")) {
                            cod_impuesto = objeto.get("cod_impuesto_riva").getAsString();
                            porcent_impuesto = objeto.get("porc_riva").getAsString();
                            signo = objeto.get("ivaSigno").getAsInt();
                            valorImpuesto = (Util.roundByDecimal((objeto.get("valor").getAsDouble() * objeto.get("porc_riva").getAsDouble()) / 100, 0)) * signo;
                            st = new StringStatement(this.obtenerSQL("SQL_INSERT_IMPUESTOS_CABECERA"), true);
                            st.setString(1, proveedor_cab);
                            st.setString(2, documento);
                            st.setString(3, cod_impuesto);
                            st.setString(4, porcent_impuesto);
                            st.setDouble(5, valorImpuesto);
                            st.setDouble(6, valorImpuesto);
                            st.setString(7, usuario.getLogin());
                            st.setString(8, objeto.get("proveedor").getAsString());
                            st.setString(9, objeto.get("item").getAsString() != null ? objeto.get("item").getAsString() : "0");
                            sqlver = st.getSql();
                            System.out.println(sqlver);
                            tService.getSt().addBatch(sqlver);

                            //DETALLE
                            st = new StringStatement(this.obtenerSQL("SQL_INSERT_IMPUESTOS_DETALLE"), true);
                            st.setString(1, proveedor_cab);
                            st.setString(2, documento);
                            st.setString(3, objeto.get("item").getAsString() != null ? objeto.get("item").getAsString() : "0");
                            st.setString(4, cod_impuesto);
                            st.setString(5, porcent_impuesto);
                            st.setDouble(6, valorImpuesto);
                            st.setDouble(7, valorImpuesto);
                            st.setString(8, usuario.getLogin());
                            st.setString(9, objeto.get("proveedor").getAsString());
                            sqlver = st.getSql();
                            System.out.println(sqlver);
                            tService.getSt().addBatch(sqlver);
                        }
                        if (!objeto.get("cod_impuesto_rica").getAsString().equals("")) {
                            cod_impuesto = objeto.get("cod_impuesto_rica").getAsString();
                            porcent_impuesto = objeto.get("porc_rica").getAsString();
                            signo = objeto.get("ricaSigno").getAsInt();
                            valorImpuesto = (Util.roundByDecimal((objeto.get("valor").getAsDouble() * objeto.get("porc_rica").getAsDouble()) / 100, 0)) * signo;
                            st = new StringStatement(this.obtenerSQL("SQL_INSERT_IMPUESTOS_CABECERA"), true);
                            st.setString(1, proveedor_cab);
                            st.setString(2, documento);
                            st.setString(3, cod_impuesto);
                            st.setString(4, porcent_impuesto);
                            st.setDouble(5, valorImpuesto);
                            st.setDouble(6, valorImpuesto);
                            st.setString(7, usuario.getLogin());
                            st.setString(8, objeto.get("proveedor").getAsString());
                            st.setString(9, objeto.get("item").getAsString() != null ? objeto.get("item").getAsString() : "0");
                            sqlver = st.getSql();
                            System.out.println(sqlver);
                            tService.getSt().addBatch(sqlver);

                            //DETALLE
                            st = new StringStatement(this.obtenerSQL("SQL_INSERT_IMPUESTOS_DETALLE"), true);
                            st.setString(1, proveedor_cab);
                            st.setString(2, documento);
                            st.setString(3, objeto.get("item").getAsString() != null ? objeto.get("item").getAsString() : "0");
                            st.setString(4, cod_impuesto);
                            st.setString(5, porcent_impuesto);
                            st.setDouble(6, valorImpuesto);
                            st.setDouble(7, valorImpuesto);
                            st.setString(8, usuario.getLogin());
                            st.setString(9, objeto.get("proveedor").getAsString());
                            sqlver = st.getSql();
                            System.out.println(sqlver);
                            tService.getSt().addBatch(sqlver);
                        }
                        if (!objeto.get("cod_impuesto_rtfuente").getAsString().equals("")) {
                            cod_impuesto = objeto.get("cod_impuesto_rtfuente").getAsString();
                            porcent_impuesto = objeto.get("porc_rtfuente").getAsString();
                            signo = objeto.get("rfteSigno").getAsInt();
                            valorImpuesto = (Util.roundByDecimal((objeto.get("valor").getAsDouble() * objeto.get("porc_rtfuente").getAsDouble()) / 100, 0)) * signo;
                            st = new StringStatement(this.obtenerSQL("SQL_INSERT_IMPUESTOS_CABECERA"), true);
                            st.setString(1, proveedor_cab);
                            st.setString(2, documento);
                            st.setString(3, cod_impuesto);
                            st.setString(4, porcent_impuesto);
                            st.setDouble(5, valorImpuesto);
                            st.setDouble(6, valorImpuesto);
                            st.setString(7, usuario.getLogin());
                            st.setString(8, objeto.get("proveedor").getAsString());
                            st.setString(9, objeto.get("item").getAsString() != null ? objeto.get("item").getAsString() : "0");
                            sqlver = st.getSql();
                            System.out.println(sqlver);
                            tService.getSt().addBatch(sqlver);

                            //DETALLE
                            st = new StringStatement(this.obtenerSQL("SQL_INSERT_IMPUESTOS_DETALLE"), true);
                            st.setString(1, proveedor_cab);
                            st.setString(2, documento);
                            st.setString(3, objeto.get("item").getAsString() != null ? objeto.get("item").getAsString() : "0");
                            st.setString(4, cod_impuesto);
                            st.setString(5, porcent_impuesto);
                            st.setDouble(6, valorImpuesto);
                            st.setDouble(7, valorImpuesto);
                            st.setString(8, usuario.getLogin());
                            st.setString(9, objeto.get("proveedor").getAsString());
                            sqlver = st.getSql();
                            System.out.println(sqlver);
                            tService.getSt().addBatch(sqlver);
                        }
                    }
                }
            }
            st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_ANTICIPO_GASTO"), true);
            st.setString(1, documento);
            st.setString(2, usuario.getLogin());
            st.setString(3, info.get("anticipo").getAsString());
            sqlver = st.getSql();
            System.out.println(sqlver);
            tService.getSt().addBatch(sqlver);

            tService.execute();
            legalizarAnticipoCajaMenor(info.get("valorCXC").getAsString(), info.get("valorLegalizar").getAsString(), info.get("anticipo").getAsString(), usuario);
            respuesta = "{\"respuesta\":\"" + documento + "\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    public String numDocumento() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_NUMERO_DOCUMENTO_CXP";
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = rs.getString("numdoc");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String buscarCuentas(String parametro_busqueda, String informacion_) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        Connection con = null;
        String query = "SQL_BUSCAR_CUENTA";
        try {
            con = this.conectarJNDI();
            String parametro = "";
            if (parametro_busqueda.equals("cuentas")) {
                parametro = " AND  cuenta ilike '%" + informacion_ + "%'";
            } else if (parametro_busqueda.equals("nombre")) {
                parametro = " AND  nombre_largo ilike  '%" + informacion_ + "%'";
            }

            String consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarMotivo() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_MOTIVO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String buscarMultiservicio(String multiservicio, Usuario usuario) {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        JsonObject obj = new JsonObject();
        String query = "";
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            if (usuario.getEmpresa().equals("STRK")) {
                query = "SQL_CARGAR_MULTISERVICIO_SELECTRIK";
            } else {
                query = "SQL_CARGAR_MULTISERVICIO";
            }
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, multiservicio);
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("label", rs.getString("numos"));
                fila.addProperty("value", rs.getString("multiservicio"));
                fila.addProperty("mivar1", rs.getString("multiservicio"));
                arr.add(fila);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(arr);
        }
    }

    @Override
    public String cargarCxpCabecera(String documento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CXP_CABECERA";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, documento);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarCxpDetalle(String documento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CXP_DETALLE";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, documento);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarSolicitudesDocumentos(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NEGOCIO_CUOTA_MANEJO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String anularCuotaManejo(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = info.getAsJsonArray("json");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_ANULAR_CUOTA_MANEJO"), true);
                st.setString(1, objeto.get("documento").getAsString());
                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"GUARDADO\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarNegocioRechazado(String numero_solicitud, String identificacion, String fechaInicio, String fechaFin) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NEGOCIO_RECHAZADO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";

            if (!identificacion.equals("")) {
                parametro = parametro + "and identificacion = '" + identificacion + "'";
            }
            if (!numero_solicitud.equals("")) {
                parametro = parametro + "and numero_solicitud = '" + numero_solicitud + "'";
            }

            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " and fecha_credito::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String reactivarNegocio(String numero_solicitud, String comentario, String fechaInicio, String fechaFin, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_REACTIVAR_NEGOCIO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, comentario);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, numero_solicitud);
            ps.executeUpdate();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String guardarDocumentosAnticipoCajaMenor(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        String respuesta = "";
        String informacion = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ANTICIPOS_CAJA_MENOR";
        try {
            JsonArray anticipos = info.getAsJsonArray("anticipos");
            for (int i = 0; i < anticipos.size(); i++) {
                objeto = anticipos.get(i).getAsJsonObject();
                if (objeto.get("id").getAsString().startsWith("neo_")) {
                    con = this.conectarJNDI();
                    ps = con.prepareStatement(this.obtenerSQL(query));
                    ps.setString(1, objeto.get("tipo_anticipo").getAsString());
                    ps.setString(2, objeto.get("identificacion").getAsString());
                    ps.setString(3, objeto.get("banco").getAsString() != null ? objeto.get("banco").getAsString() : "");
                    ps.setString(4, objeto.get("sucursal").getAsString() != null ? objeto.get("sucursal").getAsString() : "");
                    ps.setString(5, objeto.get("autorizador").getAsString() != null ? objeto.get("autorizador").getAsString() : "");
                    ps.setString(6, objeto.get("concepto").getAsString() != null ? objeto.get("concepto").getAsString() : "");
                    ps.setString(7, !objeto.get("valor").getAsString().equals("") ? objeto.get("valor").getAsString() : "0");
                    ps.setString(8, usuario.getLogin());
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        informacion = rs.getBoolean("retorno") ? "SI" : "NO";
                    }
                }
            }
            respuesta = "{\"respuesta\":\"" + informacion + "\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    @Override
    public String cargarAnticipoCajaMenor(String fechaInicio, String fechaFin, String identificacion, String anticipo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ANTICIPO_CAJA_MENOR";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";

            if (!identificacion.equals("")) {
                parametro = parametro + "and empleado = '" + identificacion + "'";
            }
            if (!anticipo.equals("")) {
                parametro = parametro + "and cod_anticipo = '" + anticipo + "'";
            }

            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " and acm.creation_date::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String legalizarAnticipoCajaMenor(String valorCXC, String valorLegalizar, String anticipo, Usuario usuario) {
        String respuesta = "";
        String informacion = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LEGALIZAR_ANTICIPOS_CAJA_MENOR";
        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, anticipo);
            ps.setString(2, valorLegalizar);
            ps.setString(3, valorCXC);
            ps.setString(4, usuario.getLogin());
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = rs.getBoolean("retorno") ? "SI" : "NO";
            }

            respuesta = "{\"respuesta\":\"" + informacion + "\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    @Override
    public String buscarCXPGastos(String anticipo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CXP_GASTOS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, anticipo);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarDetalleAnticipoCajaMenor(String anticipo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DETALLE_ANTICIPO_CAJA_MENOR";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, anticipo);
            ps.setString(2, anticipo);
            ps.setString(3, anticipo);
            ps.setString(4, anticipo);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarDocumentosAfectoCXC(String factura) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DOCUMENTOS_AFECTO_CXC";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, factura);
            ps.setString(2, factura);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarDocumentosAfectoGCM(String cxp) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DOCUMENTOS_AFECTO_GCM";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cxp);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String anularAnticipo(String id, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ANULAR_ANTICIPO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.setString(3, id);
            ps.setString(4, id);
            ps.setString(5, id);
            ps.setString(6, id);

            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarAnticipoTransferenciaPaso(ParmetrosDinamicaTSP parametros, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        //Valida si se ejecua el hilo runnable
        boolean sw = false;

        try {
            con = this.conectarJNDI();
            String query = "SQL_CARGAR_DINAMICA_CONTABLE_" + parametros.getPaso().toUpperCase();
            ps = con.prepareStatement(this.obtenerSQL(query));

            switch (parametros.getPaso()) {
                case "paso1":
                    ps.setString(1, parametros.getFiltro_general());
                    ps.setString(2, parametros.getPeriodo_inicio());
                    ps.setString(3, parametros.getPeriodo_fin());
                    ps.setString(4, parametros.getTipo_anticipo());
                    break;
                case "paso2":
                    ps.setString(1, parametros.getFiltro_general());
                    ps.setString(2, parametros.getPeriodo_inicio());
                    ps.setString(3, parametros.getPeriodo_fin());
                    ps.setString(4, parametros.getTipo_anticipo());
                    break;
                case "paso3":
                    if (!parametros.getTipo_anticipo().equals("AGA")) {
                        ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filter", "where tipo_referencia_2 = '" + parametros.getTipo_anticipo() + "'"));
                    }
                    ps.setString(1, parametros.getFiltro_general());
                    ps.setString(2, parametros.getPeriodo_inicio());
                    ps.setString(3, parametros.getPeriodo_fin());
                    ps.setString(4, parametros.getTipo_anticipo());

                    break;
                case "paso4":
                    ps.setString(1, parametros.getFiltro_general());
                    ps.setString(2, parametros.getPeriodo_inicio());
                    ps.setString(3, parametros.getPeriodo_fin());
                    ps.setString(4, parametros.getTipo_anticipo());

                    break;
                case "paso5":
                    ps.setString(1, parametros.getFiltro_general());
                    ps.setString(2, parametros.getPeriodo_inicio());
                    ps.setString(3, parametros.getPeriodo_fin());
                    ps.setString(4, parametros.getTipo_anticipo());
                    sw = true;
                    break;
                case "paso6":
                    ps.setString(1, parametros.getFiltro_general());
                    ps.setString(2, parametros.getPeriodo_inicio());
                    ps.setString(3, parametros.getPeriodo_fin());
                    ps.setString(4, parametros.getTipo_anticipo());
                    sw = true;
                    break;
            }

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            //ejecuta procesos runnable
            if (sw) {
                parametros.setInsertLogProcesos(obtenerSQL(ParmetrosDinamicaTSP.queryLogProcesos));
                parametros.setUpdateLogProcesos(obtenerSQL(ParmetrosDinamicaTSP.queryUpLogProcesos));
                parametros.setNombreLogProceso("CONSOLIDAR INFORMACION MOVIMIENTO TSP");
                new Thread(new HDinamicaContable(parametros, con, obtenerSQL(ParmetrosDinamicaTSP.queryHilo), usuario), "H" + parametros.getPaso().toUpperCase()).start();
            }

            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null && !sw) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarReporteProduccion(String paso, String periodo, String filtro, String periodo_inicio, String periodo_fin, String concept_code, String anulado) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_REPORTE_PRODUCCION";
        ArrayList<ReporteProducionBeans> lista = null;
        String informacion = "{}";
        String filtro_general = "";

        try {
            con = this.conectarJNDI();
            switch (filtro) {
                case "aniocorrido":
                    filtro_general = "1";
                    break;
                case "anioanterior":
                    filtro_general = "2";
                    break;
                case "seismeses":
                    filtro_general = "3";
                    break;
                case "otro":
                    filtro_general = "4";
                    break;
                case "docemeses":
                    filtro_general = "5";
                    break;
                case "mespasado":
                    filtro_general = "6";
                    break;
                case "mespresente":
                    filtro_general = "7";
                    break;
            }
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, filtro_general);
            ps.setString(2, periodo_inicio);
            ps.setString(3, periodo_fin);
            ps.setString(4, concept_code);
            ps.setString(5, anulado);

            rs = ps.executeQuery();
            lista = new ArrayList<>();
            while (rs.next()) {
                ReporteProducionBeans beans = new ReporteProducionBeans();
                beans.setPeriodo_anticipo(rs.getString("periodo_anticipo"));
                beans.setPeriodo_contabilizacion(rs.getString("periodo_contabilizacion"));
                beans.setDescripcion(rs.getString("descripcion"));
                beans.setValor(rs.getDouble("valor"));
                beans.setValor_neto(rs.getDouble("valor_neto"));
                beans.setValor_descuento(rs.getDouble("valor_descuento"));
                beans.setValor_combancaria(rs.getDouble("valor_combancaria"));
                beans.setVlr_consignacion(rs.getDouble("vlr_consignacion"));
                AtributosRowsProd attr = new AtributosRowsProd();
                BeanGeneral bg = new BeanGeneral();
                bg.setValor_01(rs.getString("display"));
                bg.setValor_02(String.valueOf(rs.getInt("row_span")));
                attr.setPeriodo_anticipo(bg);
                beans.setArp(attr);
                lista.add(beans);
            }

            informacion = new Gson().toJson(lista);
            System.out.println("listaJson: " + informacion);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarConsolidadoMovimientoTSP() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CONSOLIDADO_MOVIMIENTO_TSP";
        JsonArray lista = null;
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarAnalisisReporteProduccion(ParmetrosDinamicaTSP parametros) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_ANALISIS_REPORTE_PRODUCCION";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, parametros.getFiltro_general());
            ps.setString(2, parametros.getPeriodo_inicio());
            ps.setString(3, parametros.getPeriodo_fin());
            ps.setString(4, parametros.getEstadoReporte());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cagarReporteInversionista(ParmetrosDinamicaTSP parametros) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String parametro = "";
        String consulta = "";
        String query = "SQL_REPORTE_INVERSIONISTA";
        try {
            con = this.conectarJNDI();
            if (!parametros.getTercero().equals("")) {
                parametro = "AND tercero = '" + parametros.getTercero() + "'";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, parametros.getFiltro_general());
            ps.setString(2, parametros.getPeriodo_inicio());
            ps.setString(3, parametros.getPeriodo_fin());
            ps.setString(4, parametros.getPaso());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cagarTercerInversionistas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_TERCERO_INVERSIONISTA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("nit"), rs.getString("tercero"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cagarReporteInversionistaSinFiltros(ParmetrosDinamicaTSP parametros) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_REPORTE_INVERSIONISTA_SIN_FILTROS";
        String parametro = "";
        String consulta = "";
        try {
            con = this.conectarJNDI();
            if (!parametros.getTercero().equals("")) {
                parametro = "AND tercero = '" + parametros.getTercero() + "'";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, parametros.getFiltro_general());
            ps.setString(2, parametros.getPeriodo_inicio());
            ps.setString(3, parametros.getPeriodo_fin());
            ps.setString(4, parametros.getPaso());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cagarHelpDinamicasContables(ParmetrosDinamicaTSP parametros) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String informacion = "";
        String query = "SQL_CARGAR_HELP_DINAMICAS_CONTABLES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, parametros.getPaso());
            ps.setString(2, parametros.getModulo());
            rs = ps.executeQuery();

            while (rs.next()) {
                informacion = rs.getString("descripcion_ayuda");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String permisoHelpDinamicasContables(Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String informacion = "";
        String query = "SQL_PERMISO_HELP_DINAMICAS_CONTABLES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();

            while (rs.next()) {
                informacion = rs.getString(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cagarHelpDinamicasContablesDatos(ParmetrosDinamicaTSP parametros) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_HELP_DINAMICAS_CONTABLES_DATOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, parametros.getPaso());
            ps.setString(2, parametros.getModulo());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String actualizarHelpDinamicasContablesDatos(String id, String textoAyuda, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_ACTUALIZAR_HELP_DINAMICAS_CONTABLES_DATOS";
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, textoAyuda);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();

            informacion = "{\"respuesta\":\"Guardado\"}";
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarRevisionPaso2(String modo_busqueda, String nulo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_REVISION_PASO2";
        String parametro1FormaBusqueda = "";
        String consultaPrincipal = "";
        String Oder = "";
        String filtro = "";
        String consultaFiltro = "";
        String consultaOder = "";

        try {
            con = this.conectarJNDI();
            switch (modo_busqueda) {
                case "innerjoin":
                    parametro1FormaBusqueda = "INNER JOIN";
                    Oder = "t2.documento_";
                    if (nulo.equals("SI")) {
                        filtro = "WHERE t2.documento_ IS NULL";
                    }
                    break;
                case "rightjoin":
                    parametro1FormaBusqueda = "RIGHT JOIN";
                    Oder = "t1.documento";
                    if (nulo.equals("SI")) {
                        filtro = "WHERE t1.documento IS NULL";
                    }
                    break;
                case "leftjoin":
                    parametro1FormaBusqueda = "LEFT JOIN";
                    Oder = "t2.documento_";
                    if (nulo.equals("SI")) {
                        filtro = "WHERE t2.documento_ IS NULL";
                    }
                    break;
            }
            consultaPrincipal = this.obtenerSQL(query).replaceAll("#forma_busqueda", parametro1FormaBusqueda);
            consultaOder = consultaPrincipal.replaceAll("#order", Oder);
            consultaFiltro = consultaOder.replaceAll("#filtro", filtro);
            ps = con.prepareStatement(consultaFiltro);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cagarSeguimientoCaja(String anio, String fecha, String cuenta, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String fecha_actual = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String informacion = "";
        boolean verificacion;
        try {
            verificacion = verificarDatosGuardado(fecha);
            con = this.conectarJNDI();
            if (!verificacion && fecha.equals(fecha_actual)) {
                guardarRegistrosSeguimientoCaja(anio, usuario);
            }
            actualizarInfoSaldosBancosColocacion(anio);
            informacion = cargadoSaldosBancosColocacion(fecha, cuenta);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    public String cargadoSaldosBancosColocacion(String fecha_actual, String cuenta) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_SEGUIENTO_CARTERA_INFROMACION_GUARDADA";
        String parametro = "";
        String consulta = "";
        try {
            con = this.conectarJNDI();
            if (!cuenta.equals("")) {
                parametro = "AND cuenta = '" + cuenta + "' ";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, fecha_actual);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    public String guardarRegistrosSeguimientoCaja(String anio, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String informacion = "{}";
        String query = "SQL_GUARDADO_SEGUIENTO_CARTERA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, anio);
            ps.setString(3, anio);
            ps.executeUpdate();
            informacion = "GUARDADO";
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String procedimientoSeguimientoCaja(JsonObject informacion, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
        TransaccionService tService = null;
        boolean verificacion;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray informacionSeguimiento = informacion.getAsJsonArray("json");
            for (int i = 0; i < informacionSeguimiento.size(); i++) {
                objeto = informacionSeguimiento.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_EDITAR_SEGUIMIENTO_CAJA"), true);
                st.setString(1, !objeto.get("saldo_extracto").getAsString().equals("") ? objeto.get("saldo_extracto").getAsString() : "0");
                st.setString(2, !objeto.get("diferencia").getAsString().equals("") ? objeto.get("diferencia").getAsString() : "0");
                st.setString(3, usuario.getLogin());
                st.setString(4, objeto.get("id_").getAsString() != null ? objeto.get("id_").getAsString() : "");
                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (SQLException ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    public boolean verificarDatosGuardado(String fecha) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICAR_DATOS_SEGUIMIENTO_CAJA";
        boolean informacion = false;
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, fecha);
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = rs.getBoolean("respuesta");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarCuentas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CUENTAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("cuenta"), rs.getString("nombre_cuenta"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String buscarAutoCompletarCuentas(String informacion_) {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_AUTOCOMPLETAR_CUENTAS";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("label", rs.getString("nombre_cuenta"));
                fila.addProperty("value", rs.getString("cuenta"));
                arr.add(fila);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(arr);
        }
    }

    @Override
    public String cagarConfiguracionCuentasDC(String modulo, String cuenta, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_CONFIGURACION_CUENTAS_DC";
        String parametro = "";
        String consulta = "";
        try {
            con = this.conectarJNDI();
            if (!cuenta.equals("")) {
                parametro = " AND cuenta = '" + cuenta + "' ";
            }
            if (!modulo.equals("")) {
                parametro = " AND ccdc.modulo = '" + modulo + "' ";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cambiarEstadoConfiguracionCuentasDC(String id, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ELIMINAR_CUENTAS_DC";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cagarConfiguracionCuentasDC(String cuenta) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_INFORMACION_CUENTA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cuenta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String guardarInformacionCuenta(JsonObject info, Usuario usuario) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INSERT_CUENTA_CONFIGURACION_DC";
        JsonObject objeto = new JsonObject();
        try {
            objeto = info.getAsJsonArray("json").get(0).getAsJsonObject();

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, objeto.get("cuenta_").getAsString());
            ps.setString(2, objeto.get("nombre_cuenta_").getAsString());
            ps.setString(3, usuario.getLogin());
            ps.setString(4, objeto.get("modulo_").getAsString());
            ps.setString(5, objeto.get("paso_").getAsString());
            ps.setString(6, objeto.get("clasificacion_").getAsString());
            ps.setString(7, objeto.get("mostrar").getAsString());
            ps.setString(8, objeto.get("linea_negocio").getAsString());
            ps.executeUpdate();
            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    private void actualizarInfoSaldosBancosColocacion(String anio) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_UPDATE_SALDOS_BANCO_COLOCACION";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, anio);
            ps.setString(2, anio);
            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String cargarInfoMovimientoAuxiliar(String cuentas, String periodo_inicio, String periodo_fin) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject jsonObject = null;
        String informacion = "{}";
        String filtro = " and cuenta in(" + cuentas + ")";
        if (!periodo_inicio.equals("") && !periodo_fin.equals("")) {
            filtro = filtro + " and periodo::numeric  BETWEEN '" + periodo_inicio + "' and '" + periodo_fin + "'";
        }
        String query = "SQL_CARGAR_MOVIMIENTOS_LIBRO_AUXILIAR";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                jsonObject = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(jsonObject);
            }

            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cagarFacturas(String nit, String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String parametro = "";
        String consulta = "";
        String informacion = "{}";
        String query = "SQL_CARGAR_FACTURAS";
        try {
            con = this.conectarJNDI();
            if (!nit.equals("")) {
                parametro = "AND fact.nit = '" + nit + "' ";
            }
            if (!negocio.equals("")) {
                parametro = " AND negasoc = '" + negocio + "' ";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String buscarCliente(String informacion_, String parametro_busqueda) {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        JsonObject obj = new JsonObject();
        String query = "SQL_BUSCAR_CLIENTE";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            String parametro = "";
            if (parametro_busqueda.equals("nit")) {
                parametro = " AND  nit ilike '%" + informacion_ + "%'";
            } else if (parametro_busqueda.equals("nombre")) {
                parametro = " AND  nomcli ilike  '%" + informacion_ + "%'";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("label", rs.getString("info"));
                fila.addProperty("value", rs.getString("nit"));
                fila.addProperty("mivar1", rs.getString("codcli"));
                arr.add(fila);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(arr);
        }
    }

    @Override
    public String cargarConcepto() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CONCEPTO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarTipoIngreso() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_INGRESO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarHcNombres() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_HC_2";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("table_code"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cagarMoneda() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MONEDA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("codmoneda"), rs.getString("nommoneda"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String actualizarInformacionCuenta(JsonObject info, Usuario usuario) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ACTUALIZAR_INFORMACION_CUENTA_DC";
        JsonObject objeto = new JsonObject();
        try {
            objeto = info.getAsJsonArray("json").get(0).getAsJsonObject();

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, objeto.get("cuenta_").getAsString());
            ps.setString(2, objeto.get("nombre_cuenta_").getAsString());
            ps.setString(3, usuario.getLogin());
            ps.setString(4, objeto.get("modulo_").getAsString());
            ps.setString(5, objeto.get("paso_").getAsString());
            ps.setString(6, objeto.get("clasificacion_").getAsString());
            ps.setString(7, objeto.get("linea_negocio").getAsString());
            ps.setString(8, objeto.get("id_").getAsString());
            ps.executeUpdate();
            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject getDocumentosRequeridos(String linea_negocio, String solicitud) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_DOCUMENTOS_REQUERIDOS";
        JsonObject res;
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, solicitud);
            ps.setString(2, linea_negocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                res = new JsonObject();
                res.addProperty("id", rs.getString("id_documento"));
                res.addProperty("valor", rs.getString("nombre_documento"));
                res.addProperty("estado", rs.getString("estado"));
                arr.add(res);
            }
            res = new JsonObject();
            res.add("rows", arr);
            return res;
        } catch (SQLException ex) {
            throw new Exception("ERROR: getDocumentosRequeridos() " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public String buscarNegocio(String negocio) {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_BUSCAR_NEGOCIO";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("label", rs.getString("info"));
                fila.addProperty("value", rs.getString("negocio"));
                arr.add(fila);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(arr);
        }
    }

    @Override
    public String guardarDatosIngresos(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";

        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray ingresoCabecera = info.getAsJsonArray("jsonCabecera");
            JsonArray ingresoDetalle = info.getAsJsonArray("jsonDetalle");
            for (int i = 0; i < ingresoCabecera.size(); i++) {
                objeto = ingresoCabecera.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_INSERT_CONTROL_INGRESOS"), true);
                st.setString(1, objeto.get("tipo_ingreso").getAsString() != null ? objeto.get("tipo_ingreso").getAsString() : "");
                st.setString(2, objeto.get("cliente").getAsString() != null ? objeto.get("cliente").getAsString() : "");
                st.setString(3, objeto.get("banco").getAsString() != null ? objeto.get("banco").getAsString() : "");
                st.setString(4, objeto.get("sucursal").getAsString() != null ? objeto.get("sucursal").getAsString() : "");
                st.setString(5, objeto.get("cuenta").getAsString() != null ? objeto.get("cuenta").getAsString() : "");
                st.setString(6, objeto.get("f_consignacion").getAsString() != null ? objeto.get("f_consignacion").getAsString() : "");
                st.setString(7, !objeto.get("v_consignacion").getAsString().equals("") ? objeto.get("v_consignacion").getAsString() : "0");
                st.setString(8, objeto.get("moneda").getAsString() != null ? objeto.get("moneda").getAsString() : "");
                st.setString(9, objeto.get("concepto").getAsString() != null ? objeto.get("concepto").getAsString() : "");
                st.setString(10, objeto.get("hc").getAsString() != null ? objeto.get("hc").getAsString() : "");
                st.setString(11, objeto.get("descripcion").getAsString() != null ? objeto.get("descripcion").getAsString() : "");
                st.setString(12, usuario.getLogin());

                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
            }
            for (int i = 0; i < ingresoDetalle.size(); i++) {
                objeto = ingresoDetalle.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_INSERT_CONTROL_INGRESOS_DETALLE"), true);
                st.setString(1, objeto.get("item").getAsString() != null ? objeto.get("item").getAsString() : "0");
                st.setString(2, objeto.get("nitcli").getAsString() != null ? objeto.get("nitcli").getAsString() : "");
                st.setString(3, objeto.get("documento").getAsString() != null ? objeto.get("documento").getAsString() : "");
                st.setString(4, objeto.get("fecha_vencimiento").getAsString() != null ? objeto.get("fecha_vencimiento").getAsString() : "");
                st.setString(5, objeto.get("descripcion").getAsString() != null ? objeto.get("descripcion").getAsString() : "");
                st.setString(6, objeto.get("cuenta").getAsString() != null ? objeto.get("cuenta").getAsString() : "");
                st.setString(7, !objeto.get("valor_factura").getAsString().equals("") ? objeto.get("valor_factura").getAsString() : "0");
                st.setString(8, !objeto.get("valor_pagar").getAsString().equals("") ? objeto.get("valor_pagar").getAsString() : "0");
                st.setString(9, usuario.getLogin());

                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarPagosSinReferencia(String fechaInicio, String fechaFin) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PAGOS_SIN_REFERENCIA";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + "WHERE r.fecha_recaudo::date BETWEEN '" + fechaInicio + "'::DATE AND '" + fechaFin + "'::DATE";
            }
            consulta = this.obtenerSQL(query).replaceAll("#PARAMETRO", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarFintraSoluciones(ParmetrosDinamicaTSP parametros) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_REPORTE_FINTRA_SOLUCIONES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, parametros.getFiltro_general());
            ps.setString(2, parametros.getPeriodo_inicio());
            ps.setString(3, parametros.getPeriodo_fin());
            ps.setString(4, parametros.getPaso());
            ps.setString(5, parametros.getPaso());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarModulos() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_MODULOS_CUENTAS_DC";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("codigo"), rs.getString("modulo"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String eliminarMultipleConfiguracionCuentasDC(JsonObject informacion) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray info = informacion.getAsJsonArray("json");
            for (int i = 0; i < info.size(); i++) {
                objeto = info.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_ELIMINAR_CUENTAS_DC"), true);
                st.setString(1, objeto.get("id").getAsString() != null ? objeto.get("id").getAsString() : "");

                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
            }

            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarPazYSalvo(String cliente, String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_PAZ_Y_SALVO";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            String filtroInterno = "";
            if (!cliente.equals("")) {
                parametro = parametro + "AND neg.cod_cli = '" + cliente + "'";
                filtroInterno = filtroInterno + "AND cod_cli = '" + cliente + "'";
            }
            if (!negocio.equals("")) {
                parametro = parametro + "AND neg.cod_neg = '" + negocio + "'";
                filtroInterno = filtroInterno + "AND fac.negasoc in  (select cod_neg \n" +
                                                    "from negocios \n" +
                                                    "where cod_cli = (\n" +
                                                    "select cod_cli \n" +
                                                    "from negocios \n" +
                                                    "where \n" +
                                                    "cod_neg = '"+negocio+"'\n" +
                                                    ")\n" +
                                                    "and estado_neg in ('T', 'A') \n" +
                                                    "group by \n" +
                                                    "fecha_negocio::date,\n" +
                                                    "cod_neg \n" +
                                                    ") ";
            }
            consulta = this.obtenerSQL(query).replaceAll("#PARAMETRO", parametro);
            consulta = consulta.replaceAll("#FILTROINTERNO", filtroInterno);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarLiquidacionDetalle(String negocio, String fecha_max, String general) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "";
        String tipo="";
        try {
            if (general.equals("")) {
                query = "SQL_CARGAR_LIQUIDACION_DETALLE";
            } else {
                query = "SQL_CARGAR_LIQUIDACION_DETALLE_GENERAL";
            }
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            ps.setString(2, negocio);
            ps.setString(3, fecha_max);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String datosPdfPazYSalvo(JsonObject info, Usuario usuario) {
        String respuesta = "";
        JsonObject objeto = new JsonObject();
        try {
            String ancabezado = "";
            String parrafo = "";
            //JsonArray arr = info.getAsJsonArray("json");

            objeto = info.getAsJsonArray("json").get(0).getAsJsonObject();
            parrafo = "De conformidad con el art�culo 880 del c�digo de comercio, Fintra S.A. se reserva el derecho de hacer exigible cualquier "
                    + "obligaci�n no cobrada con anterioridad y que se encuentre debidamente documentada y contabilizada.";
            String nombre = objeto.get("cliente").getAsString() == null ? "" : objeto.get("cliente").getAsString();
            String cedula = objeto.get("cedula").getAsString() == null ? "" : objeto.get("cedula").getAsString();
            String codeudor = objeto.get("codeudor").getAsString() == null ? "" : objeto.get("codeudor").getAsString();
            String cc_codeudor = objeto.get("cc_codeudor").getAsString() == null ? "" : objeto.get("cc_codeudor").getAsString();
            String negocio = objeto.get("negocio").getAsString() == null ? "" : objeto.get("negocio").getAsString();
            String genero = objeto.get("genero").getAsString() == null ? "" : objeto.get("genero").getAsString();
            String tipo_id = objeto.get("tipoid").getAsString() == null ? "" : objeto.get("tipoid").getAsString();
            String genero_codeudor = objeto.get("genero_codeudor").getAsString() == null ? "" : objeto.get("genero_codeudor").getAsString();
            respuesta = exportarPdfPazYSalvo(usuario, nombre, cedula, codeudor, cc_codeudor, negocio, parrafo,genero,tipo_id, genero_codeudor);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return respuesta;
    }

    public String exportarPdfPazYSalvo(Usuario usuario, String nombre, String cedula, String codeudor, String cc_codeudor, String negocio, String parrafo, String genero, String tipo_id, String genero_codeudor) {

        String ruta = "";
        String msg = "OK";
        nombre=nombre.replace("Ñ","�");
        codeudor=codeudor.replace("Ñ","�");
        try {
            ruta = this.directorioArchivo(usuario.getLogin(), "pdf", cedula);
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            Image imagen = null;
            imagen = Image.getInstance(rsb.getString("ruta") + "/images/Logo-Fintra.png");
            Font fuente = new Font(Font.HELVETICA, 10, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font fuenteG = new Font(Font.HELVETICA, 10, Font.BOLD, new java.awt.Color(0, 0, 0));
            DecimalFormat formato2 = new DecimalFormat("###,###,###");
            PdfPCell celda = null;
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(ruta));
            documento.open();
            documento.newPage();
            imagen.setAlignment(PdfPTable.ALIGN_CENTER);
//            imagen.scaleToFit(600, 780);
            //imagen.scaleAbsolute(580, 835);
            //imagen.setAbsolutePosition(20,10);
            imagen.setAbsolutePosition(0,50f);
            imagen.scalePercent(75);
            documento.add(imagen);

            PdfPTable thead = new PdfPTable(1);            
            celda = new PdfPCell();
            celda.setBorder(0);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            documento.add(thead);
            thead = new PdfPTable(1);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setPhrase(new Paragraph(new Phrase("FINTRA S.A", fuenteG)));
            thead.addCell(celda);
            
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setPhrase(new Paragraph(new Phrase("CERTIFICA QUE:", fuenteG)));
            thead.addCell(celda);
            
            documento.add(new Paragraph(new Phrase(" ", fuenteG)));
            documento.add(thead);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            thead = new PdfPTable(1);
            
            String dirigido;
            String ident;
            if (genero.equals("F")) {
                dirigido = "La se�ora";
                ident = "identificada";
            } else {
                dirigido = "El se�or";
                ident = "identificado";
            }
            
            String g_codeudor;
            String ident1;
            if (genero_codeudor.equals("F")) {
                g_codeudor = "La se�ora";
                ident1 = "identificada";
            } else {
                g_codeudor = "El se�or";
                ident1 = "identificado";
            }
            
            
        if(codeudor.equals("")){
            if (tipo_id.equals("CED")){
            documento.add(new Paragraph(new Phrase(dirigido + " " + nombre + " " + ident + "  con c�dula de ciudadan�a No " + formato2.format(Integer.parseInt(cedula)) + "  quien era titular de la obligaci�n"
                    + " n�mero " + negocio + " se encuentra a paz y salvo por este concepto. ", fuente)));
            }   else {
                documento.add(new Paragraph(new Phrase("Que, " + nombre + " " + ident + "  con NIT No. " + formato2.format(Integer.parseInt(cedula)) + " quien era titular de la obligaci�n"
                    + " n�mero " + negocio + " se encuentra a paz y salvo por este concepto.", fuente)));
            } 
            
        } else {
            
            if (tipo_id.equals("CED")){
            documento.add(new Paragraph(new Phrase(dirigido + " " + nombre + " " + ident + "  con c�dula de ciudadan�a No " + formato2.format(Integer.parseInt(cedula)) + "  quien era titular de la obligaci�n"
                    + " n�mero " + negocio + " se encuentra a paz y salvo por este concepto. " + g_codeudor + "  "  + codeudor + " "+ ident1 + "  con c�dula de ciudadan�a No " +formato2.format(Integer.parseInt(cc_codeudor))+ "  era codeudor de esta obligaci�n.", fuente)));
            } else {
                documento.add(new Paragraph(new Phrase("Que, " + nombre + " " + ident + "  con NIT No. " + formato2.format(Integer.parseInt(cedula)) + " quien era titular de la obligaci�n"
                    + " n�mero " + negocio + " se encuentra a paz y salvo por este concepto.", fuente)));
            }
        }    
            
                
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);

            thead = new PdfPTable(1);
            documento.add(new Paragraph(new Phrase(parrafo, fuente)));
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //row2
            thead = new PdfPTable(1);
            Calendar calendario = Calendar.getInstance();
            String mes = "";
            mes = this.mesToString(calendario.get(Calendar.MONTH) + 1);
            documento.add(new Paragraph(new Phrase("La presente se expide a solicitud del interesado a los  " + calendario.get(Calendar.DAY_OF_MONTH) + " d�as del "
                    + "mes de  " + mes + " de " + calendario.get(Calendar.YEAR) + " en la ciudad de Barranquilla.", fuente)));
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("Cordialmente,", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("__________________________________", fuenteG)));
            documento.add(new Paragraph(new Phrase("Direcci�n de Servicio Al Cliente", fuenteG)));
            //documento.add(new Paragraph(new Phrase("Revisado ", fuenteG)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            documento.close();
        } catch (Exception e) {
            msg = "ERROR";
            e.printStackTrace();
        }
        return msg;
    }
    
    @Override
    public String exportarPdfEstadoCuenta(Usuario usuario, JsonObject informacion) {
         String ruta = "";
        String msg = "OK";
        String fechaVencimiento = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        DecimalFormat formatea = new DecimalFormat("###,###.##");
        try {  
            
            ResourceBundle rb = null;
            
            JsonObject objeto = new JsonObject();
            JsonArray informacionNegocio = informacion.getAsJsonArray("informacionNegocio");
            JsonArray informacionDetalle = informacion.getAsJsonArray("informacionDetalle");
            JsonArray informacionGeneral = informacion.getAsJsonArray("informacionGeneral");
            ruta = this.directorioArchivoEstadoCuenta(usuario.getLogin(), "pdf", informacionNegocio.get(0).getAsJsonObject().get("negocioC_").getAsString());
            
            
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            Image imagen = null;
            imagen = Image.getInstance(rb.getString("ruta") + "/images/Logo-Fintra.png");
            
            
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("GET_FECHA_VENCIMIENTO"));
            ps.setString(1, informacionNegocio.get(0).getAsJsonObject().get("negocioC_").getAsString());
            rs = ps.executeQuery();
            while(rs.next()) {
                fechaVencimiento = rs.getString(1);
            }
            
            Font fuente = new Font(Font.HELVETICA, 6, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font fuenteG = new Font(Font.HELVETICA, 9, Font.BOLD, new java.awt.Color(0, 0, 0));
            Font fuenteT = new Font(Font.HELVETICA, 7, Font.BOLD, new java.awt.Color(255, 255, 255));

            Document documento = null;
//            documento = this.createDoc();
            documento = new Document(PageSize.A4, 25, 25, 35, 100);
            PdfWriter.getInstance(documento, new FileOutputStream(ruta));
            documento.open();
            documento.newPage();
            
            Color ColorRelleno=new Color(50,190,250);//Azul
            
            imagen.setAlignment(PdfPTable.ALIGN_CENTER);
//            imagen.scaleToFit(600, 780);
            //imagen.scaleAbsolute(580, 835);
            imagen.setAbsolutePosition(0,50f);
            imagen.scalePercent(75);
            documento.add(imagen);
            
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            PdfPTable table = new PdfPTable(2);
            table.setWidths(new int[]{3, 7});
            table.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            table.setWidthPercentage(70);
            PdfPCell cell;
            // informacion del negocio
            cell = new PdfPCell(new Phrase("Cliente", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("cliente_").getAsString(), fuente));
            cell.setBackgroundColor(new Color(255, 255, 255));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("negocioC_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Informacion del Negocio", fuenteT));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            cell.setBackgroundColor(ColorRelleno);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Valor Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("vlr_negocio_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Tasa Interes", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("tasa_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Numero Cuotas", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("ncuotas_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Valor Aval", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("vlr_aval_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Fecha limite de pago", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(fechaVencimiento, fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            documento.add(table);
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            //detalle

            PdfPTable tableinfoneg = new PdfPTable(15);
            tableinfoneg.setWidths(new int[]{1, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2});
//            tableinfoneg.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            tableinfoneg.setWidthPercentage(100);
            PdfPCell cell2;
            int numfilas = 0;
            cell2 = new PdfPCell(new Phrase("", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Factura", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Fecha", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Dias Mora", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Capital", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Interes", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Mi Pyme", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Intermediaci�n Financiera ", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Seguro", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Saldo Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Interes Mora", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Gastos Cobranza", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Total Saldo", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Estado", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(ColorRelleno);
            tableinfoneg.addCell(cell2);

            for (int i = 0; i < informacionDetalle.size(); i++) {
                numfilas = numfilas + 1;
                objeto = informacionDetalle.get(i).getAsJsonObject();

                cell2 = new PdfPCell(new Phrase(String.valueOf(numfilas), fuenteT));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(ColorRelleno);
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(objeto.get("documento").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(objeto.get("fecha_vencimiento").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(objeto.get("cuota").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);                
                cell2 = new PdfPCell(new Phrase(objeto.get("dias_mora").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase("$"+formatea.format(objeto.get("valor_saldo_capital").getAsDouble()), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase("$"+formatea.format(objeto.get("valor_saldo_mi").getAsDouble()), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase("$"+formatea.format(objeto.get("valor_saldo_ca").getAsDouble()), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase("$"+formatea.format(objeto.get("valor_cm").getAsDouble()), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase("$"+formatea.format(objeto.get("valor_seguro").getAsDouble()), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase("$"+formatea.format(objeto.get("valor_saldo_cuota").getAsDouble()), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase("$"+formatea.format(objeto.get("ixm").getAsDouble()), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase("$"+formatea.format(objeto.get("gac").getAsDouble()), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase("$"+formatea.format(objeto.get("suma_saldos").getAsDouble()), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(objeto.get("estado").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
            }
            documento.add(tableinfoneg);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            // informacion general del negocio
            PdfPTable tablegeneral = new PdfPTable(2);
            tablegeneral.setWidths(new int[]{5, 7});
            tablegeneral.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            tablegeneral.setWidthPercentage(70);
            PdfPCell cell3;
            cell3 = new PdfPCell(new Phrase("Informacion General del Negocio", fuenteT));
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell3.setColspan(4);
            cell3.setBackgroundColor(ColorRelleno);
            tablegeneral.addCell(cell3);

            cell3 = new PdfPCell(new Phrase("Capital", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("capital_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Interes", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("interes_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Mi Pyme", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("miPyme_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Intermediaci�n Financiera", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("cuotaAdmin_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Seguro", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("seguro_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Interes Mora", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("interesMora_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Gasto de Cobranza", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("gac_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Total a Pagar", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("total_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setBorderColor(new Color(87, 164, 205));
            cell.setBackgroundColor(new Color(87, 164, 205));
            tablegeneral.addCell(cell3);
            documento.add(tablegeneral);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("__________________________________", fuenteG)));
            documento.add(new Paragraph(new Phrase("Direcci�n de Servicio Al Cliente", fuenteG)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.close();
        } catch (Exception e) {
            msg = "ERROR";
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                msg = "ERROR";
            }
        }
        return msg;
    }
    
    public String generarExtractoEstadoCuenta(Usuario usuario, JsonObject informacion) {

        String ruta = "";
        String msg = "OK";
        try {
            JsonObject objeto = new JsonObject();
            JsonArray informacionNegocio = informacion.getAsJsonArray("informacionNegocio");
            JsonArray informacionDetalle = informacion.getAsJsonArray("informacionDetalle");
            JsonArray informacionGeneral = informacion.getAsJsonArray("informacionGeneral");
            ruta = this.directorioArchivoEstadoCuenta(usuario.getLogin(), "pdf", informacionNegocio.get(0).getAsJsonObject().get("negocioC_").getAsString());

            Font fuente = new Font(Font.HELVETICA, 7, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font fuenteG = new Font(Font.HELVETICA, 9, Font.BOLD, new java.awt.Color(0, 0, 0));
            Font fuenteT = new Font(Font.HELVETICA, 9, Font.BOLD, new java.awt.Color(255, 255, 255));

            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(ruta));
            documento.open();
            documento.newPage();

            PdfPTable table = new PdfPTable(2);
            table.setWidths(new int[]{3, 7});
            table.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            table.setWidthPercentage(70);
            PdfPCell cell;
            // informacion del negocio
            cell = new PdfPCell(new Phrase("Cliente", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("cliente_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("negocioC_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Informacion del Negocio", fuenteT));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(0, 128, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Valor Negocio", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("vlr_negocio_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Tasa Interes", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("tasa_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Numero Cuotas", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("ncuotas_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Valor Aval", fuenteG));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);
            cell = new PdfPCell(new Phrase(informacionNegocio.get(0).getAsJsonObject().get("vlr_aval_").getAsString(), fuente));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(new Color(255, 255, 255));
            table.addCell(cell);

            documento.add(table);
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            //detalle

            PdfPTable tableinfoneg = new PdfPTable(7);
            tableinfoneg.setWidths(new int[]{1, 3, 3, 3, 5, 5, 3});
//            tableinfoneg.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            tableinfoneg.setWidthPercentage(100);
            PdfPCell cell2;
            int numfilas = 0;
            cell2 = new PdfPCell(new Phrase("", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(0, 128, 255));
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Fecha", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(0, 128, 255));
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(0, 128, 255));
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Dias Mora", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(0, 128, 255));
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Valor Saldo Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(0, 128, 255));
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Valor Saldo Global Cuota", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(0, 128, 255));
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Estado", fuenteT));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new Color(0, 128, 255));
            tableinfoneg.addCell(cell2);

            for (int i = 0; i < informacionDetalle.size(); i++) {
                numfilas = numfilas + 1;
                objeto = informacionDetalle.get(i).getAsJsonObject();

                cell2 = new PdfPCell(new Phrase(String.valueOf(numfilas), fuenteT));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(0, 128, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(objeto.get("fecha").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(objeto.get("item").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(objeto.get("dias_mora").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(objeto.get("valor_saldo_cuota").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(objeto.get("valor_saldo_global_cuota").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(objeto.get("estado").getAsString(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell2.setBackgroundColor(new Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
            }
            documento.add(tableinfoneg);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            // informacion general del negocio
            PdfPTable tablegeneral = new PdfPTable(2);
            tablegeneral.setWidths(new int[]{5, 7});
            tablegeneral.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
            tablegeneral.setWidthPercentage(70);
            PdfPCell cell3;
            cell3 = new PdfPCell(new Phrase("Informacion General del Negocio", fuenteT));
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell3.setColspan(4);
            cell3.setBackgroundColor(new Color(0, 128, 255));
            tablegeneral.addCell(cell3);

            cell3 = new PdfPCell(new Phrase("Deuda Actual mas Aval", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("deuda_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Cuota Corriente", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("cuota_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Saldo Vencido", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("saldo_vencido_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Capital Cuotas Facturas", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("capital_cuotas_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Seguro", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("seguro_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Gasto de Cobranza", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("gac_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Interes", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("intereses_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase("Total a Pagar", fuenteG));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            cell3 = new PdfPCell(new Phrase(informacionGeneral.get(0).getAsJsonObject().get("total_").getAsString(), fuente));
            cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell3.setBackgroundColor(new Color(255, 255, 255));
            tablegeneral.addCell(cell3);
            documento.add(tablegeneral);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("__________________________       __________________________            __________________________", fuenteG)));
            documento.add(new Paragraph(new Phrase("Elaborado Por                                  Revisado Por                                         Recibido Por", fuenteG)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.close();
        } catch (Exception e) {
            msg = "ERROR";
            e.printStackTrace();
        }
        return msg;
    }
    
    private String directorioArchivo(String user, String extension, String nit) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rsb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + "PazySalvo_" + nit + "_" + fmt.format(new java.util.Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    private String directorioArchivoEstadoCuenta(String user, String extension, String negocio) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rsb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + "EstadoCuenta" + negocio + "_" + fmt.format(new java.util.Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    private Document createDoc() {
        Document doc = new Document(PageSize.A4, 25, 25, 35, 30);
        return doc;
    }

    private String mesToString(int mes) {
        String texto = "";
        switch (mes) {
            case 1:
                texto = "Enero";
                break;
            case 2:
                texto = "Febrero";
                break;
            case 3:
                texto = "Marzo";
                break;
            case 4:
                texto = "Abril";
                break;
            case 5:
                texto = "Mayo";
                break;
            case 6:
                texto = "Junio";
                break;
            case 7:
                texto = "Julio";
                break;
            case 8:
                texto = "Agosto";
                break;
            case 9:
                texto = "Septiembre";
                break;
            case 10:
                texto = "Octubre";
                break;
            case 11:
                texto = "Noviembre";
                break;
            case 12:
                texto = "Diciembre";
                break;
            default:
                texto = "Enero";
                break;
        }
        return texto;
    }

    @Override
    public String buscarAsesor(String asesor) {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ASESOR";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, asesor);
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("label", rs.getString("idusuario"));
                fila.addProperty("value", rs.getString("idusuario"));
                arr.add(fila);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(arr);
        }
    }

    @Override
    public String buscarNegocios(String asesor, String fecha_inicio, String fecha_fin,String estadoCartera) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_NEGOCIOS_ADMIN_ASESOR";
        String consulta = "";
        String consultaEjecutar = "";
        String parametro = "";
        String parametro2 = "";
        try {
            con = this.conectarJNDI();
            if (!fecha_inicio.equals("") && !fecha_fin.equals("")) {
                parametro = parametro + " AND neg.creation_date::date between '" + fecha_inicio + "'::date and '" + fecha_fin + "'::date";
            }
            switch (estadoCartera) {
                case "consaldo":
                    parametro = "  AND valor_saldo > 0";
                    break;
                case "sinsaldo":
                    parametro = "  AND valor_saldo  = 0";
                    break;
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            ps.setString(1, asesor);
//            ps.setString(2, fecha_inicio);
//            ps.setString(3, fecha_fin);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarAsesores(String id_combo, String producto) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            switch (id_combo) {
                case "asesor":
                    query = "SQL_CARGAR_ASESORES_CARTERA";
                     ps = con.prepareStatement(this.obtenerSQL(query));
                    break;
                case "asesores":
                    query = "SQL_CARGAR_ASESORES";
                    ps = con.prepareStatement(this.obtenerSQL(query));
                    //ps.setString(1, producto);
                    break;
            }
           
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("idusuario"), rs.getString("asesor"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String actualizarNegocios(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
        String sqlver1 = "";
        String sqlver2 = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray negocios = info.getAsJsonArray("negocios");
            for (int i = 0; i < negocios.size(); i++) {
                objeto = negocios.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_RESPONSABLE_CUENTA_SOL_AVAL"), true);
                st.setString(1, info.get("asesores").getAsString() != null ? info.get("asesores").getAsString() : "");
                st.setString(2, usuario.getLogin());
                st.setString(3, objeto.get("cod_neg").getAsString() != null ? objeto.get("cod_neg").getAsString() : "");
                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
                
                st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_RESPONSABLE_CUENTA_NEGOCIOS"), true);
                st.setString(1, info.get("asesores").getAsString() != null ? info.get("asesores").getAsString() : "");
                st.setString(2, usuario.getLogin());
                st.setString(3, objeto.get("cod_neg").getAsString() != null ? objeto.get("cod_neg").getAsString() : "");
                sqlver1 = st.getSql();
                System.out.println(sqlver1);
                tService.getSt().addBatch(sqlver1);
                
                st = new StringStatement(this.obtenerSQL("SQL_TRAZABILIDAD_ASESORES"), true);
                st.setString(1, objeto.get("responsable_cuenta").getAsString() != null ? objeto.get("responsable_cuenta").getAsString() : "");
                st.setString(2, info.get("asesores").getAsString() != null ? info.get("asesores").getAsString() : "");
                st.setString(3, objeto.get("cod_neg").getAsString() != null ? objeto.get("cod_neg").getAsString() : "");
                st.setString(4, objeto.get("cod_cli").getAsString() != null ? objeto.get("cod_cli").getAsString() : "");
                st.setString(5, usuario.getLogin());
                sqlver2 = st.getSql();
                System.out.println(sqlver2);
                tService.getSt().addBatch(sqlver2);
               
            }
            
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarObservaciones(String numero_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OBSERVACIONES_CONTROL_FIRMAS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numero_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String guardarObservaciones(String numero_solicitud, String observaciones, Usuario usuario) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_GUARDAR_OBSERVACIONES_CONTROL_DOCUMENTOS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, observaciones);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, numero_solicitud);
            ps.executeUpdate();
            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    @Override
    public String cagarAnalisisRecaudo(String banco, String fecha_inicio, String fecha_fin) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_ANALISIS_RECAUDO";
        String parametro = "";
        String consulta = "";
        try {
            con = this.conectarJNDI();
            if (!fecha_inicio.equals("") && !fecha_fin.equals("")) {
                parametro = parametro + "and fecha_consignacion between '" + fecha_inicio + "' and '" + fecha_fin + "'";
            }
            if (!banco.equals("")) {
                parametro = parametro + "and banco = '" + banco + "'";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cagarBarrios(String ciudad) {
         Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_BARRIOS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, ciudad);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarDetalleNegocioLiquidacion(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_DETALLE_LIQUIDACION_NEGOCIO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            ps.setString(2, negocio);
            ps.setString(3, negocio);
            ps.setString(4, negocio);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarFacturasCausacionIntereses(String periodo, String cmc) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_FACTURAS_CAUSACION_INTERESES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, periodo);
            ps.setString(2, cmc);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String causarInteresesMultiservicio(String periodo, String cmc, String cta_ingreso, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
//        JsonArray lista = null;
//        JsonObject objetoJson = null;
        String informacion = "";
        String query = "SQL_CAUSAR_INTERESES_MULTISERVICIO";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, periodo);
            ps.setString(2, cmc);
            ps.setString(3, cta_ingreso);
            ps.setString(4, usuario.getLogin());
            rs = ps.executeQuery();
//            lista = new JsonArray();
            while (rs.next()) {
               // objetoJson = new JsonObject();
                respuesta = rs.getString("respuesta");
//                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
//                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
//            }
//                lista.add(objetoJson);
            }
            informacion = respuesta;
        } catch (SQLException e) {
            e.printStackTrace();
            informacion = "ERROR";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarCmcCausacionInteresesMS() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CMC_CAUSACION_INTERESES";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        JsonArray lista = null;
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                obj = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    obj.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
            }
                lista.add(obj);
            }
            respuesta = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargar_negocio_rechazo(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_CARGAR_NEGOCIO_RECHAZO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String rechazarNegocio(String negocio, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String query = "SQL_RECHAZAR_NEGOCIO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            ps.setString(2, usuario.getLogin());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargar_convenios() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CONVENIOS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String extracto_estado_cuenta(Usuario usuario, JsonObject informacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXTRACTO_ESTADO_CUENTA_ROP";
        String respuesta = "";
        int rop = 0;
        JsonObject objeto = new JsonObject();
        try {
            objeto = informacion.getAsJsonArray("informacionNegocio").get(0).getAsJsonObject();
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, objeto.get("negocioC_").getAsString());
            ps.setString(2, informacion.get("fecha").getAsString());
            ps.setString(3, usuario.getLogin());
            rs = ps.executeQuery();
            while (rs.next()) {
                rop = rs.getInt("rop");
            }
            ReestructurarNegociosDAO rndao = new ReestructurarNegociosImpl(usuario.getBd());
            rndao.generarPdfDuplicado(usuario.getLogin(), rop);
            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarCompobanteCausacion(String comprobanteGenerado) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMPROBANTE_GENERADO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, comprobanteGenerado);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    public String cargaPresolicitudesMicrocredito(String fechaInicio, String fechaFin, String identificacion, String numero_solicitud, String estado_solicitud, String etapa, String r_operaciones, String unidadNegocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_PRESOLICITUDES_MICROCREDITO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " AND pre.fecha_credito::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!identificacion.equals("")) {
                parametro = parametro + " AND pre.identificacion = '" + identificacion + "'";
            }
            if (!numero_solicitud.equals("")) {
                parametro = parametro + " AND pre.numero_solicitud = '" + numero_solicitud + "'";
            }
            if (!estado_solicitud.equals("")) {
                parametro = parametro + " AND pre.estado_sol = '" + estado_solicitud + "'";
            }            
            
            if (!etapa.equals("")) {
                parametro = parametro + " AND pre.etapa = '" + etapa + "'";
            }
            if (!r_operaciones.equals("")) {
                parametro = parametro + " AND pre.rechazo_operaciones= '" + r_operaciones + "'";
            } 
            if (!unidadNegocio.equals("")) {
                parametro += " AND pre.entidad = '" + unidadNegocio + "'";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"respuesta\":\"ERROR\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }
    
    @Override
    public Object searchNombresArchivos(String directorioArchivos, String num_solicitud) {
        ArrayList nombresArchivos=new ArrayList();
        File dir = new File(directorioArchivos + num_solicitud);
        ArrayList<String> filesnotanulated = searchNamesFilesAppNotAnulated(num_solicitud);
        try{
            if (dir.exists()){
                String[] ficheros = dir.list();
                for (String fichero : ficheros) {
                    if (filesnotanulated.contains(fichero)) {
                        nombresArchivos.add(fichero);
                    }
                }
            }                 
        }catch(Exception e){
            System.out.println("error::" + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }
           return nombresArchivos;
    }

    @Override
    public boolean almacenarArchivoEnCarpetaUsuario(
                                 String documento
                                ,String rutaOrigen, String rutaDestino
                                ,String nomarchivo
                               ){

        boolean swFileCopied = false;
        try{            
          
            File carpetaDestino =new File(rutaDestino);
          
            //deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);
                          
            try{
                    //Copiamos el archivo de la carpeta origen a la carpeta destino
                    InputStream in = new FileInputStream(rutaOrigen + "/" +nomarchivo);
                    OutputStream out = new FileOutputStream(rutaDestino + nomarchivo);

                    // Copy the bits from instream to outstream
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                    swFileCopied = true;
               
                   }catch(Exception k){ throw new Exception(" FILE: " + k.getMessage());}

            }catch(Exception e){
                System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
                e.printStackTrace();
            }
       
        return swFileCopied;
    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override       
    public String rechazarnegociomicrocredito(Usuario usuario, String numero_solicitud) {
        //Connection con = null;
        //PreparedStatement ps = null;
         String respuesta = "";
        StringStatement st = null;
        String query = "SQL_RECHAZAR_NEGOCIO_MICROCREDITO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, numero_solicitud);
            respuesta = st.getSql();
//            con = this.conectarJNDI();
//            ps = con.prepareStatement(this.obtenerSQL(query));
//            ps.setString(1, usuario.getLogin());
//            ps.setString(2, numero_solicitud);
//            ps.executeUpdate();
//            respuesta = "{\"respuesta\":\"Negocio puesto en Stand By\"}";
               
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }

    @Override
    public String cargarnegociosdesistir(String cod_neg, String identificacion) {
      Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NEGOCIOS_A_DESISTIR";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";
            if (!identificacion.equals("")) {
                parametro = parametro + " AND identificacion= '" + identificacion + "'";
            }
            if (!cod_neg.equals("")) {
                parametro = parametro + " AND ne.cod_neg= '" + cod_neg + "'";
            }
            
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }
    private ArrayList<String> searchNamesFilesAppNotAnulated(String document) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ARCHIVOS_MULTISERVICIO";
        ArrayList<String> listNamefiles = null;
        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, document);
            rs = ps.executeQuery();
            listNamefiles = new ArrayList<>();
            while (rs.next()) {
                listNamefiles.add(rs.getString("filename"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return listNamefiles;
        }
    } 
    
    @Override
    public void updateFilesAppMIcro(String document,String id_tipo_archivo, Usuario user) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_ANULAR_ARCHIVOS_MULTISERVICIO";
        
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, user.getLogin());
            ps.setString(2, document);
            ps.setString(3, id_tipo_archivo);
            ps.executeUpdate();     
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    } 

    @Override
    public  ArrayList<ArchivosAppMicro>  buscarArchivosCargadosApp(String document,String und_negocio) {
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ARCHIVOS_APP_V2";  
        ArrayList<ArchivosAppMicro>  list=null;
        try{            
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, document);
            ps.setString(2, und_negocio);
            rs = ps.executeQuery();
            list=new ArrayList<>();
            while(rs.next()){
                ArchivosAppMicro app=new ArchivosAppMicro();
                app.setNegocio(rs.getString("negocio"));
                app.setId_archivo(rs.getInt("id_archivo"));
                app.setOrden(rs.getInt("orden"));
                app.setId_unid_negocio(rs.getInt("id_unid_negocio"));
                app.setNombre_categoria(rs.getString("nombre_categoria"));
                app.setNombre_archivo(rs.getString("nombre_archivo"));
                app.setRequest_id(rs.getString("request_id"));
                app.setNombre_archivo_real(rs.getString("nombre_archivo_real"));
                app.setArchivo_cargado(rs.getString("archivo_cargado"));
                app.setUrl("");
                app.setEstado(rs.getString("estado"));
                app.setTarget_width(rs.getInt("target_width"));
                app.setTarget_height(rs.getInt("target_height"));
                app.setQuality(rs.getInt("quality"));
                app.setId_categoria(rs.getInt("id_categoria"));
                app.setNombre_cat(rs.getString("nombre_cat"));
                app.setVisible(rs.getString("visible"));
                app.setIcon(rs.getString("icon"));
                app.setDescripcion(rs.getString("descripcion"));
                list.add(app);
            }
                           
        }catch(Exception e){
            System.out.println("error::" + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }
        
        return list;
           
        
    }
    public String standbypresolicitudes(Usuario usuario, String numero_solicitud) {
//        Connection con = null;
//        PreparedStatement ps = null;
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_STANDBY_PRESOLICITUD_MICROCREDITO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, numero_solicitud);
            respuesta = st.getSql();
//            con = this.conectarJNDI();
//            ps = con.prepareStatement(this.obtenerSQL(query));
//            ps.setString(1, usuario.getLogin());
//            ps.setString(2, numero_solicitud);
//            ps.executeUpdate();
//            respuesta = "{\"respuesta\":\"Negocio puesto en Stand By\"}";
               
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }
    
    public String standbytrazabilidadpresolicitudes(Usuario usuario, String numero_solicitud, String causal, String estado) {

        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_TRAZA_STANDBY_PRESOLICITUD";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, numero_solicitud);
            st.setString(2, estado);
            st.setString(3, usuario.getLogin());
            st.setString(4, causal);
            respuesta = st.getSql();

        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }

    public String devolverSolicitud(Usuario usuario, String numero_solicitud) {
//        Connection con = null;
//        PreparedStatement ps = null;
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_DEVOLVER_PRESOLICITUD_MICROCREDITO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, numero_solicitud);
            respuesta = st.getSql();
//            con = this.conectarJNDI();
//            ps = con.prepareStatement(this.obtenerSQL(query));
//            ps.setString(1, usuario.getLogin());
//            ps.setString(2, numero_solicitud);            
//            ps.executeUpdate();
//            respuesta = "{\"respuesta\":\"Negocio Devuelto\"}";
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }
    
    public String devolverSolicitudtrazabilidad(Usuario usuario, String numero_solicitud) {

        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_TRAZA_DEVOLVER_PRESOLICITUD";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, numero_solicitud);
            st.setString(2, usuario.getLogin());
            respuesta = st.getSql();

        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }
    
    public String consultarCausalStandBy(String numero_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_CAUSAL_STANDBY";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numero_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                  informacion = rs.getString("causal1");
            }
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
    }
    
    public JsonObject getInfoCuentaEnvioCorreo() {
        Connection con = null;
        PreparedStatement ps = null;
        con = null;
        ps = null;
        ResultSet rs = null;
        String query = "GET_INFO_CORREO_STANDBY";
        JsonObject obj = null;

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("servidor", rs.getString("servidor"));
                fila.addProperty("puerto", rs.getString("puerto"));
                fila.addProperty("usuario", rs.getString("usuario"));
                fila.addProperty("clave", rs.getString("clave"));
                fila.addProperty("bodyMessage", rs.getString("bodyMessage"));
            }
            obj = fila;

        } catch (Exception e) {
            System.out.println("Error en getInfoCuentaEnvioCorreo: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getInfoCuentaEnvioCorreo: " + e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return obj;
        }
    }
    
    public String getUnidadConvenio(String numero_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;    
        ResultSet rs = null;
        String query = "GET_UNIDAD_CONVENIO";
        String  idUnidad = "";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, numero_solicitud);                      
            rs = ps.executeQuery();          
            while(rs.next()){
               idUnidad = rs.getString("id_unidad");
            }
                 
        }catch (Exception e) {         
                System.out.println("Error en getUnidadConvenio: " + e.toString());
                e.printStackTrace();
                throw new Exception("Error en getUnidadConvenio: " + e.toString());         
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return idUnidad; 
       }
    }
    
    public String getNombreCliente(String numero_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;    
        ResultSet rs = null;
        String query = "GET_NOMBRE_CLIENTE";
        String  nombre = "";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, numero_solicitud);                      
            rs = ps.executeQuery();          
            while(rs.next()){
               nombre = rs.getString("nombre");
            }
                 
        }catch (Exception e) {         
                System.out.println("Error en getNombreCliente: " + e.toString());
                e.printStackTrace();
                throw new Exception("Error en getNombreCliente: " + e.toString());         
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return nombre; 
       }
    }
    
    public String getCorreosToSendStandBy(String type) {
        Connection con = null;
        PreparedStatement ps = null;    
        ResultSet rs = null;
        String query = "GET_CORREOS_STANDBY";
        String  correos = "";
        
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, type);                      
            rs = ps.executeQuery();          
            while(rs.next()){
               correos = rs.getString("dato");
            }
                 
        }catch (Exception e) {         
                System.out.println("Error en getCorreosToSendStandBy: " + e.toString());
                e.printStackTrace();
                throw new Exception("Error en getCorreosToSendStandBy: " + e.toString());         
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return correos; 
       }
    }

    public String cargarEntidadesPresolicitud() {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray jsonArray = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CARGAR_ENTIDADES_PRESOLICITUD"));
            rs = ps.executeQuery();
            
            jsonArray = new JsonArray();
            while (rs.next()) {
                JsonObject json = new JsonObject();
                json.addProperty("descripcion", rs.getString(1));
                jsonArray.add(json);
            }
            return new Gson().toJson(jsonArray);
        } catch (SQLException e) {
            return "{\"respuesta\":\"ERROR\"}";
        }
    }
    
    public String cargarPresolicitudesMicroRechazadas() {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String jsonResponse = "[]";
        try {
            conn = conectarJNDIFintra();
            ps = conn.prepareStatement(obtenerSQL("CARGAR_PRESOLICITUDES_MICRO_R"));
            rs = ps.executeQuery();
            
            JsonObject solicitudJson;
            JsonArray solicitudesJson = new JsonArray();
            while (rs.next()) {
                solicitudJson = new JsonObject();
                solicitudJson.addProperty("entidad", rs.getString("entidad"));
                solicitudJson.addProperty("numero_solicitud", rs.getString("numero_solicitud"));
                solicitudJson.addProperty("tipo_credito", rs.getString("tipo_credito"));
                solicitudJson.addProperty("tipo_identificacion", rs.getString("tipo_identificacion"));
                solicitudJson.addProperty("identificacion", rs.getString("identificacion"));
                solicitudJson.addProperty("fecha_expedicion", rs.getString("fecha_expedicion"));
                solicitudJson.addProperty("fecha_nacimiento", rs.getString("fecha_nacimiento"));
                solicitudJson.addProperty("primer_apellido", rs.getString("primer_apellido"));
                solicitudJson.addProperty("primer_nombre", rs.getString("primer_nombre"));
                solicitudJson.addProperty("departamento", rs.getString("departamento"));
                solicitudJson.addProperty("ciudad", rs.getString("ciudad"));
                solicitudJson.addProperty("fecha_pago", rs.getString("fecha_pago"));
                solicitudJson.addProperty("email", rs.getString("email"));
                solicitudJson.addProperty("telefono", rs.getString("telefono"));
                solicitudJson.addProperty("cliente", rs.getString("cliente"));
                solicitudJson.addProperty("monto_credito", rs.getDouble("monto_credito"));
                solicitudJson.addProperty("valor_cuota", rs.getDouble("valor_cuota"));
                solicitudJson.addProperty("numero_cuotas", rs.getInt("numero_cuotas"));
                solicitudJson.addProperty("compra_cartera", rs.getString("compra_cartera"));
                solicitudJson.addProperty("fecha_presolicitud", rs.getString("fecha_presolicitud"));
                solicitudesJson.add(solicitudJson);
            }
            jsonResponse = new Gson().toJson(solicitudesJson);
        } catch (SQLException e) {
            System.err.println("Error cargando las presolicitudes micro rechazadas: " + e.getMessage());
            jsonResponse = "";
        }
        return jsonResponse;
    }
}
