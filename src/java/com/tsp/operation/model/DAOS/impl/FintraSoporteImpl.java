/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.Barcode;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.operation.model.DAOS.FintraSoporteDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.FintraSoporteBeans;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.reporteOfertasBeans;
import com.tsp.util.Util;
import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mariana
 */
public class FintraSoporteImpl extends MainDAO implements FintraSoporteDAO {
    private Object usuario;

    public FintraSoporteImpl(String dataBaseName) {
        super("FintraSoporteDAO.xml", dataBaseName);
    }

    @Override
    public ArrayList<FintraSoporteBeans> cargarComprobante(String comprobante) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMPROBANTES";
        ArrayList<FintraSoporteBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, comprobante);

            rs = ps.executeQuery();
            while (rs.next()) {
                FintraSoporteBeans beanFintra = new FintraSoporteBeans();
                beanFintra.setNumdoc(rs.getString("numdoc"));
                beanFintra.setTipodoc(rs.getString("tipodoc"));
                beanFintra.setTercero(rs.getString("tercero"));
                beanFintra.setGrupo_transaccion(rs.getString("grupo_transaccion"));
                beanFintra.setTransaccion(rs.getString("transaccion"));
                beanFintra.setPeriodo(rs.getString("periodo"));
                beanFintra.setCuenta(rs.getString("cuenta"));
                beanFintra.setDetalle(rs.getString("detalle"));
                beanFintra.setNit_tercero(rs.getString("nit_tercero"));
                beanFintra.setNuevo_detalle(rs.getString("nuevo_detalle"));
                beanFintra.setValor_debito(rs.getString("valor_debito"));
                beanFintra.setValor_credito(rs.getString("valor_credito"));
                beanFintra.setCreation_date(rs.getString("creation_date"));
                beanFintra.setCreation_user(rs.getString("creation_user"));

                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String actualizarComprobante(String comprobante) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_COMPROBANTES";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, comprobante);
            ps.executeUpdate();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"" + ex.getMessage() + "\"}";
        }finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return respuesta;
        }
    }

    @Override
    public String eliminarComprobanteCabecera(String comprobante) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ELIMINAR_COMPROBANTE_CABECERA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, comprobante);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    public String eliminarComprobanteDetalle(String comprobante) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ELIMINAR_COMPROBANTE_DETALLE";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, comprobante);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarExcelTemp(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = info.getAsJsonArray("json");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_TEMPORAL"), true);
                st.setString(1, objeto.get("tipo").getAsString());
                st.setString(2, objeto.get("documento").getAsString());
                st.setString(3, objeto.get("tipo_documento").getAsString());
                st.setString(4, objeto.get("nit").getAsString());
                st.setString(5, objeto.get("banco").getAsString());
                st.setString(6, objeto.get("sucursal").getAsString());
                st.setString(7, objeto.get("cambio").getAsString());
                st.setString(8, objeto.get("periodo").getAsString());
                st.setString(9, objeto.get("periodo_nuevo").getAsString());
                st.setString(10, usuario.getLogin());
                st.setString(11, objeto.get("fecha_nueva").getAsString());
                st.setString(12, objeto.get("grupo_transaccion").getAsString());

                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"CARGADO\"}";

            //respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String actualizarDocumentos(Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ACTUALIZAR_DOCUMENTOS";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getBoolean("retorno") ? "SI" : "NO";
            }

        } catch (Exception ex) {
            respuesta = ex.getMessage();
            ex.getMessage();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }
        }
        return respuesta;
    }

    @Override
    public String limpiar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_DOCUMENTOS";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.executeUpdate();
            respuesta = "ELIMINADO";
        } catch (Exception ex) {
            respuesta = ex.getMessage();
            ex.getMessage();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }
        }
        return respuesta;
    }

    @Override
    public ArrayList<FintraSoporteBeans> cargarDocumentosPendientes() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DOCUEMNTOS_PENDIENTES";
        ArrayList<FintraSoporteBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                FintraSoporteBeans beansoport = new FintraSoporteBeans();
                beansoport.setId(rs.getString("id"));
                beansoport.setTipo(rs.getString("tipo"));
                beansoport.setDocumento(rs.getString("documento"));
                beansoport.setTipo_documento(rs.getString("tipo_documento"));
                beansoport.setNit(rs.getString("nit_codcli"));
                beansoport.setBanco(rs.getString("banco"));
                beansoport.setSucursal(rs.getString("sucursal"));
                beansoport.setCambio(rs.getString("cambio"));
                beansoport.setFecha_nueva(rs.getString("fecha_nueva"));
                beansoport.setPeriodo(rs.getString("periodo"));
                beansoport.setPeriodo_nuevo(rs.getString("periodo_nuevo"));
                beansoport.setGrupo_transaccion(rs.getString("grupo_transaccion"));

                lista.add(beansoport);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public ArrayList<FintraSoporteBeans> cargarTiposDocumentos() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_DOCUMENTOS";
        ArrayList<FintraSoporteBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                FintraSoporteBeans beansoport = new FintraSoporteBeans();
                beansoport.setCodigo(rs.getString("codigo"));
                beansoport.setDescripcion(rs.getString("descripcion"));
                beansoport.setTipo(rs.getString("tipo"));

                lista.add(beansoport);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public ArrayList<FintraSoporteBeans> cargarDocumentos(String fechaInicio, String fechaFin, String user, String tipoDocumento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DOCUMENTOS";
        ArrayList<FintraSoporteBeans> info = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String condicion = "";
            if (tipoDocumento.equals("CXP")) {
                condicion = "'CXP' as tipo,documento,tipo_documento, proveedor as nit,banco,sucursal,fecha_contabilizacion::date,transaccion,periodo from fin.cxp_doc";
            } else if (tipoDocumento.equals("INGRESO")) {
                condicion = "'INGRESO' as tipo, num_ingreso as documento,tipo_documento,codcli as nit ,branch_code as banco,bank_account_no as sucursal,fecha_contabilizacion::date,transaccion ,periodo from con.ingreso ";
            } else if (tipoDocumento.equals("CXC")) {
                condicion = " 'CXC' as tipo,documento, tipo_documento,nit,'' as banco,''as sucursal,fecha_contabilizacion::date,transaccion,periodo  from con.factura";
            } else if (tipoDocumento.equals("EGRESO")) {
                condicion = "'EGRESO' as tipo,document_no as documento, '' as tipo_documento ,nit , branch_code as banco,bank_account_no as sucursal, fecha_contabilizacion::date,transaccion,periodo from egreso";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            ps.setString(1, fechaInicio);
            ps.setString(2, fechaFin);
            ps.setString(3, user);
            rs = ps.executeQuery();
            while (rs.next()) {
                FintraSoporteBeans beansoport = new FintraSoporteBeans();
                beansoport.setTipo(rs.getString("tipo"));
                beansoport.setDocumento(rs.getString("documento"));
                beansoport.setTipo_documento(rs.getString("tipo_documento"));
                beansoport.setNit(rs.getString("nit"));
                beansoport.setBanco(rs.getString("banco"));
                beansoport.setSucursal(rs.getString("sucursal"));
                beansoport.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
                beansoport.setTransaccion(rs.getString("transaccion"));
                beansoport.setPeriodo(rs.getString("periodo"));
                info.add(beansoport);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
            return info;
        }
    }

    @Override
    public String eliminarPreAprobadosUniversidad() {
        Connection con = null;
        StringStatement st;
        String sql = "";
        String query = "eliminaPreAprobadosUniv";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR eliminaPreAprobadosUniv \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;
        }
    }

    @Override
    public String insertarPreAprobadosUniversidad(String negocio, String nit, Double valor_ult_credito, Double valor_aprobado, Usuario usuario) {
        Connection con = null;
        StringStatement st;
        String sql = "";
        String Qry = "insertarPreAprobadosUniv";
        String query = "";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(Qry);
            st = new StringStatement(query, true);
            st.setString(1, nit);
            st.setDouble(2, valor_ult_credito);
            st.setDouble(3, valor_aprobado);
            st.setString(4, usuario.getDstrct());
            st.setString(5, negocio);

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertarPreAprobadosUniv \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;
        }
    }

    @Override
    public String cargarEstadoOfertas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ESTADO_OFERTAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("table_code"), rs.getString("referencia"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarOfertas(String estado_oferta, String fechaInicio, String fechaFin, String multiservicio, String cliente, String lineanegocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OFERTAS";
     //   ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String condicion = "";
            if (!estado_oferta.equals("")) {
                condicion = condicion + " and accion.estado = '" + estado_oferta + "' ";
            }
            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                condicion = condicion + "and accion.last_update::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date"; //fecha de actulizacion de estado de la oferta
                //condicion = condicion + " and fecha_oferta::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }
            if (!multiservicio.equals("")) {
                condicion = condicion + " and num_os ='" + multiservicio + "'";
            }
            if (!cliente.equals("")) {
                condicion = condicion + " and cl.nomcli ilike '%" + cliente + "%'";
            }
//            if (!lineanegocio.equals("")) {
//                condicion = condicion + " and tp.cod_proyecto in ( " + lineanegocio + ")";
//            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
//                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
//                beanFintra.setId_solicitud(rs.getString("id_solicitud"));
//                beanFintra.setNombre_solicitud(rs.getString("nombre_solicitud"));
//                beanFintra.setNum_os(rs.getString("num_os"));
//                beanFintra.setId_cliente(rs.getString("id_cliente"));
//                beanFintra.setNomcli(rs.getString("nomcli"));
//                beanFintra.setId_cliente_padre(rs.getString("id_cliente_padre"));
//                beanFintra.setNomcli_padre(rs.getString("nomcli_padre"));
//                beanFintra.setNic(rs.getString("nic"));
//               
//                beanFintra.setTipo_solicitud(rs.getString("tipo_solicitud"));
//                beanFintra.setFecha_oferta(rs.getString("fecha_oferta"));
//                beanFintra.setFecha_inicial(rs.getString("fecha_inicial"));
//                beanFintra.setEsquema(rs.getString("esquema"));
//                beanFintra.setCosto_contratista(rs.getString("costo_contratista"));
//                beanFintra.setMaterial(rs.getString("material"));
//                beanFintra.setMano_obra(rs.getString("mano_obra"));
//                beanFintra.setTransporte(rs.getString("transporte"));
//                beanFintra.setPorc_administracion(rs.getString("porc_administracion"));
//                beanFintra.setAdministracion(rs.getString("administracion"));
//                beanFintra.setPorc_imprevisto(rs.getString("porc_imprevisto"));
//                beanFintra.setImprevisto(rs.getString("imprevisto"));
//                beanFintra.setPorc_utilidad(rs.getString("porc_utilidad"));
//                beanFintra.setBonificacion(rs.getString("bonificacion"));
//                beanFintra.setOpav(rs.getString("opav"));
//                beanFintra.setFintra(rs.getString("fintra"));
//                beanFintra.setInterventoria(rs.getString("interventoria"));
//                beanFintra.setProvintegral(rs.getString("provintegral"));
//                beanFintra.setEca(rs.getString("eca"));
//                beanFintra.setBase_iva_contratista(rs.getString("base_iva_contratista"));
//                beanFintra.setIva_contratista(rs.getString("iva_contratista"));
//                beanFintra.setIva_bonificacion(rs.getString("iva_bonificacion"));
//                beanFintra.setIva_opav(rs.getString("iva_opav"));
//                beanFintra.setIva_fintra(rs.getString("iva_fintra"));
//                beanFintra.setIva_interventoria(rs.getString("iva_interventoria"));
//                beanFintra.setIva_provintegral(rs.getString("iva_provintegral"));
//                beanFintra.setIva_eca(rs.getString("iva_eca"));
//                beanFintra.setFinanciar_sin_iva(rs.getString("financiar_sin_iva"));
//                beanFintra.setIva(rs.getString("iva"));
//                beanFintra.setFinanciar_con_iva(rs.getString("financiar_con_iva"));
//                beanFintra.setPrecio_total(rs.getString("precio_total"));

                //lista.add(beanFintra);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public ArrayList<reporteOfertasBeans> cargarAcciones(String id_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ACCIONES";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setContratista(rs.getString("contratista"));
                beanFintra.setNombre_contratista(rs.getString("nombre_contratista"));
                beanFintra.setNomcli(rs.getString("nomcli"));
                beanFintra.setId_accion(rs.getString("id_accion"));
                beanFintra.setEstado(rs.getString("estado"));
                beanFintra.setDescripcion_estado(rs.getString("descripcion_estado"));
                beanFintra.setCosto_contratista(rs.getString("costo_contratista"));
                beanFintra.setMaterial(rs.getString("material"));
                beanFintra.setMano_obra(rs.getString("mano_obra"));
                beanFintra.setTransporte(rs.getString("transporte"));
                beanFintra.setPorc_administracion(rs.getString("porc_administracion"));
                beanFintra.setAdministracion(rs.getString("administracion"));
                beanFintra.setPorc_imprevisto(rs.getString("porc_imprevisto"));
                beanFintra.setImprevisto(rs.getString("imprevisto"));
                beanFintra.setPorc_utilidad(rs.getString("porc_utilidad"));
                beanFintra.setDescripcion(rs.getString("descripcion"));
                beanFintra.setBonificacion(rs.getString("bonificacion"));
                beanFintra.setOpav(rs.getString("opav"));
                beanFintra.setFintra(rs.getString("fintra"));
                beanFintra.setInterventoria(rs.getString("interventoria"));
                beanFintra.setProvintegral(rs.getString("provintegral"));
                beanFintra.setEca(rs.getString("eca"));
                beanFintra.setBase_iva_contratista(rs.getString("base_iva_contratista"));
                beanFintra.setIva_contratista(rs.getString("iva_contratista"));
                beanFintra.setIva_bonificacion(rs.getString("iva_bonificacion"));
                beanFintra.setIva_opav(rs.getString("iva_opav"));
                beanFintra.setIva_fintra(rs.getString("iva_fintra"));
                beanFintra.setIva_interventoria(rs.getString("iva_interventoria"));
                beanFintra.setIva_provintegral(rs.getString("iva_provintegral"));
                beanFintra.setIva_eca(rs.getString("iva_eca"));
                beanFintra.setFinanciar_sin_iva(rs.getString("financiar_sin_iva"));
                beanFintra.setIva(rs.getString("iva"));
                beanFintra.setFinanciar_con_iva(rs.getString("financiar_con_iva"));
                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public ArrayList<reporteOfertasBeans> cargarDistribuciones() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DISTRIBUCIONES";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setDistribucion(rs.getString("distribucion"));
                beanFintra.setTipo(rs.getString("tipo"));
                beanFintra.setPorc_opav(rs.getString("porc_opav"));
                beanFintra.setPorc_fintra(rs.getString("porc_fintra"));
                beanFintra.setPorc_interventoria(rs.getString("porc_interventoria"));
                beanFintra.setPorc_provintegral(rs.getString("porc_provintegral"));
                beanFintra.setPorc_eca(rs.getString("porc_eca"));
                beanFintra.setPorc_iva(rs.getString("porc_iva"));
                beanFintra.setValor_agregado(rs.getString("valor_agregado"));
                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public String guardarDistribuciones(Usuario usuario, String distribucion, String opav, String fintra, String interv, String provin, String eca, String iva, String vlr_agregado) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_PORC_DISTRIBUCION";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, buscaDistr(distribucion));
            ps.setDouble(2, Double.parseDouble(opav.replace(",", ".")));
            ps.setDouble(3, Double.parseDouble(fintra.replace(",", ".")));
            ps.setDouble(4, Double.parseDouble(interv.replace(",", ".")));
            ps.setDouble(5, Double.parseDouble(provin.replace(",", ".")));
            ps.setDouble(6, Double.parseDouble(eca.replace(",", ".")));
            ps.setDouble(7, Double.parseDouble(iva.replace(",", ".")));
            ps.setString(8, vlr_agregado);
            ps.setString(9, usuario.getLogin());
            ps.setString(10, "NUEVO");
            ps.executeUpdate();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    private String buscaDistr(String distribucion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DST";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, distribucion);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("dato");
            }

        } catch (Exception ex) {
            respuesta = ex.getMessage();
            ex.getMessage();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }
        }
        return respuesta;
    }

    @Override
    public String guardarDistribuciones(Usuario usuario, String oferta, String tipo_numos) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_OFERTA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, oferta);
            st.setString(2, usuario.getLogin());
            st.setString(3, tipo_numos);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarDistribuciones(Usuario usuario, String distribucion) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_DISTRIBUCION";
        try {

            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, distribucion);
            st.setString(2, usuario.getLogin());

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cargarTipoMultiservicio() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_TIPO_MULTESERVICIO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("dato"), rs.getString("dato"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }

        }
        return respuesta;
    }

    @Override
    public ArrayList<reporteOfertasBeans> buscarMultiservicio(String multiservicio, String cliente) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_MULTISERVICIO";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String condicion = "";
            if (!multiservicio.equals("")) {
                condicion = condicion + " and num_os ='" + multiservicio + "'";
            }
            if (!cliente.equals("")) {
                condicion = condicion + "AND ofe.id_cliente = '" + cliente + "'";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setId_cliente(rs.getString("id_cliente"));
                beanFintra.setNomcli(rs.getString("nomcli"));
                beanFintra.setId_solicitud(rs.getString("id_solicitud"));
                beanFintra.setNum_os(rs.getString("num_os"));

                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public String guardarExcluidos(Usuario usuario, JsonArray informacion) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            for (int i = 0; i < informacion.size(); i++) {
                objeto = informacion.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_EXCLUIDOS"), true);
                st.setString(1, objeto.get("id_cliente").getAsString());
                st.setString(2, objeto.get("nomcli").getAsString());
                st.setString(3, objeto.get("id_solicitud").getAsString());
                st.setString(4, objeto.get("num_os").getAsString());
                st.setString(5, usuario.getLogin());
                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"GUARDADO\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public ArrayList<reporteOfertasBeans> cargarExcluidos(String multiservicio, String id_cliente) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EXCLUCIONES";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String condicion = "true";
            if (!multiservicio.equals("")) {
                condicion = condicion + " and num_os ='" + multiservicio + "'";
            }
            if (!id_cliente.equals("")) {
                condicion = condicion + "and codcli = '" + id_cliente + "'";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setEstado(rs.getString("reg_status"));
                beanFintra.setId_cliente(rs.getString("codcli"));
                beanFintra.setNomcli(rs.getString("nombre_cliente"));
                beanFintra.setId_solicitud(rs.getString("id_solicitud"));
                beanFintra.setNum_os(rs.getString("num_os"));

                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public String guardarIncluidos(Usuario usuario, JsonArray informacion) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            for (int i = 0; i < informacion.size(); i++) {
                objeto = informacion.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_INCLUIDOS"), true);
                st.setString(1, usuario.getLogin());
                st.setString(2, objeto.get("id_cliente").getAsString());
                st.setString(3, objeto.get("num_os").getAsString());
                st.setString(4, objeto.get("id_solicitud").getAsString());
                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"GUARDADO\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String anularHistPreAprobUnivActual(Usuario usuario) {
        Connection con = null;
        StringStatement st;
        String sql = "";
        String query = "SQL_UPDATE_HIST_PRE_APROBADOS_UNIV";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR anularHistPreAprobUnivActual \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;
        }
    }

    @Override
    public String insertarHistPreAprobadosUniversidad(String unidad_negocio, Usuario usuario) {
        Connection con = null;
        StringStatement st;
        String sql = "";
        String Qry = "SQL_INSERTAR_HIST_PRE_APROBADOS_UNIV";
        String query = "";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(Qry);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(unidad_negocio));
            st.setString(2, usuario.getLogin());

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertarHistPreAprobadosUniversidad \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;
        }
    }

    @Override
    public String cargarUnidadesNegocio(String filter) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_UNIDAD_NEGOCIO";
        String respuestaJson = "{}";       

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filter));

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return respuestaJson;
        }

    }

    @Override
    public String verificarTipoSolicitud(String distribucion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICAR_TIPO_SOLICITUD";
        String respuesta = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, distribucion);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("resp");
            }
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }

        }
        return respuesta;
    }

    @Override
    public ArrayList<reporteOfertasBeans> cargarTipoProyecto() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_PROYECTO";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setEstado(rs.getString("estado"));
                beanFintra.setCod_proyecto(rs.getString("cod_proyecto"));
                beanFintra.setProyecto(rs.getString("descripcion"));
                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public String guardarTipoProyecto(Usuario usuario, String tipo_proyecto) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_TIPO_PROYECTO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipo_proyecto);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }
            return respuesta;
        }
       
    }

    @Override
    public ArrayList<reporteOfertasBeans> cargarDistribucionesASoc() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DISTRIBUCIONES_AS";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setId(rs.getString("id"));
                beanFintra.setDistribucion(rs.getString("table_code"));

                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public ArrayList<reporteOfertasBeans> cargarTipoProyectASoc(String cod_proyecto) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_POYECTO_AS";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cod_proyecto);
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setId(rs.getString("id"));
                beanFintra.setCod_proyecto(rs.getString("cod_proyecto"));
                beanFintra.setDescripcion(rs.getString("descripcion"));
                beanFintra.setDistribucion(rs.getString("distribucion"));

                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public String guardarRelacionTipoProyectSoli(Usuario usuario, JsonArray informacion, String cod_proyecto) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            for (int i = 0; i < informacion.size(); i++) {
                objeto = informacion.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_PROYECTO_SOLICITUD"), true);
                st.setString(1, cod_proyecto);
                st.setString(2, objeto.get("distribucion").getAsString());
                st.setString(3, usuario.getLogin());

                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"GUARDADO\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String quitarRelacionTipoProyectSoli(JsonArray informacion, String cod_proyecto) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            for (int i = 0; i < informacion.size(); i++) {
                objeto = informacion.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_QUITAR_ASOCIACION"), true);
                st.setString(1, cod_proyecto);
                st.setString(2, objeto.get("distribucion").getAsString());

                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"GUARDADO\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String actualizarTipoProyecto(Usuario usuario, String tipo_proyecto, String cod_proyecto) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_TIPO_PROYECTO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipo_proyecto);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, cod_proyecto);

            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        return respuesta;
    }

    @Override
    public ArrayList<reporteOfertasBeans> cargarDistribucionesLibres() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DISTRIBUCIONES_LIBRES";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setTipo(rs.getString("tipo"));
                beanFintra.setPorc_opav(rs.getString("porc_opav"));
                beanFintra.setPorc_fintra(rs.getString("porc_fintra"));
                beanFintra.setPorc_interventoria(rs.getString("porc_interventoria"));
                beanFintra.setPorc_provintegral(rs.getString("porc_provintegral"));
                beanFintra.setPorc_eca(rs.getString("porc_eca"));
                beanFintra.setPorc_iva(rs.getString("porc_iva"));
                beanFintra.setValor_agregado(rs.getString("valor_agregado"));
                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public ArrayList<reporteOfertasBeans> cargarSolicitudesLibres() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_SOLICITUDES_LIBRES";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setId(rs.getString("id"));
                beanFintra.setDistribucion(rs.getString("table_code"));

                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public String anularTipoProyecto(Usuario usuario, String cod_proyecto) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ANULAR_TIPO_PROYECTO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, cod_proyecto);

            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {

                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta;
        }
        
    }

    @Override
    public ArrayList<reporteOfertasBeans> cargarDistribucionASoc(String cod_distribucion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DISTRIBUCION_RELACIONADAS";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cod_distribucion);
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setTipo(rs.getString("dato"));
                beanFintra.setDistribucion(rs.getString("table_code"));
                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public String guardarRelacionDistrSoli(JsonArray informacion, String cod_distribucion) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            for (int i = 0; i < informacion.size(); i++) {
                objeto = informacion.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_DISTRIBUCION_SOLICITUD"), true);
                st.setString(1, cod_distribucion);
                st.setString(2, objeto.get("distribucion").getAsString());
                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"GUARDADO\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String quitarRelacionDistrSoli(JsonArray informacion, String cod_distribucion) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            for (int i = 0; i < informacion.size(); i++) {
                objeto = informacion.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_QUITAR_DISTRIBUCION_SOLICITUD"), true);
                st.setString(1, cod_distribucion);
                st.setString(2, objeto.get("distribucion").getAsString());
                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"GUARDADO\"}";
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public ArrayList<reporteOfertasBeans> cargarTipoCliente() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_TIPO_CLIENTE";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setEstado(rs.getString("estado"));
                beanFintra.setCod_tipoCliente(rs.getString("cod_tipo_cliente"));
                beanFintra.setTipoCliente(rs.getString("descripcion"));
                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public String guardarTipoCliente(Usuario usuario, String tipocliente) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_TIPO_CLIENTE";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipocliente);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
              return respuesta;
        }
     
    }

    @Override
    public String actualizarTipoCliente(Usuario usuario, String tipoCliente, String cod_tipoCliente) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_TIPO_CLIENTE";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipoCliente);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, cod_tipoCliente);

            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarLineaNegocio() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_LINEA_NEGOCIO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("cod_proyecto"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String eliminarPreAprobados(String id_und_negocio) {
        Connection con = null;
        StringStatement st;
        String sql = "";
        String query = "eliminaPreAprobados";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(id_und_negocio));
            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR eliminarPreAprobados \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;
        }
    }

    @Override
    public String insertarPreAprobados(String id_und_negocio, String nit, String nombre, Double valor_ult_credito, Double valor_aprobado, Double incremento, String und_negocio, String negocio, String afiliado, String fecha_desembolso, String periodo_desembolso, String fecha_vencimiento, String departamento, String ciudad, String direccion, String barrio, String telefono, Double valor_saldo, Usuario usuario) {
        Connection con = null;
        StringStatement st;
        String sql = "";
        String Qry = "insertarPreAprobados";
        String query = "";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(Qry);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(id_und_negocio));
            st.setString(2, nit);
            st.setString(3, nombre);
            st.setDouble(4, valor_ult_credito);
            st.setDouble(5, valor_aprobado);
            st.setDouble(6, incremento);
            st.setString(7, usuario.getDstrct());
            st.setString(8, und_negocio);
            st.setString(9, negocio);
            st.setString(10, afiliado);
            st.setString(11, fecha_desembolso);
            st.setString(12, periodo_desembolso);
            st.setString(13, fecha_vencimiento);
            st.setString(14, departamento);
            st.setString(15, ciudad);
            st.setString(16, direccion);
            st.setString(17, barrio);
            st.setString(18, telefono);
            st.setDouble(19, valor_saldo);
            st.setString(20, usuario.getLogin());

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertarPreAprobados \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;
        }
    }

    @Override
    public String anularHistPreAprobadosActual(String id_und_negocio, Usuario usuario) {
        Connection con = null;
        StringStatement st;
        String sql = "";
        String query = "SQL_UPDATE_HIST_PRE_APROBADOS";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setInt(2, Integer.parseInt(id_und_negocio));

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR anularHistPreAprobadosActual \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;
        }
    }

    @Override
    public String insertarHistPreAprobados(String id_und_negocio, Usuario usuario) {
        Connection con = null;
        StringStatement st;
        String sql = "";
        String Qry = "SQL_INSERTAR_HIST_PRE_APROBADOS";
        String query = "";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(Qry);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setInt(2, Integer.parseInt(id_und_negocio));

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertarHistPreAprobados \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;
        }
    }

    @Override
    public String anularTipoCliente(Usuario usuario, String cod_tipoCliente) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ANULAR_TIPO_CLIENTE";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, cod_tipoCliente);
            ps.setString(3, cod_tipoCliente);
            ps.executeUpdate();

            respuesta = "Actualizado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String buscarMultiservicio(Usuario usuario) {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_MULTISERVICIO_ASOCIAR_CXP";
        String query2= "SQL_CARGAR_MULTISERVICIO_ASOCIAR_CXP_SELECTRIK";
        String queryEjecutar = "";
        JsonArray arr = new JsonArray();
        try {
            String empresa = usuario.getEmpresa();
            if (empresa.equals("INYM")) {
                queryEjecutar = query;
            } else if (empresa.equals("STRK")) {
                queryEjecutar = query2;
            }

            con = this.conectarJNDI();
           // consulta = this.obtenerSQL(queryEjecutar);
            ps = con.prepareStatement(this.obtenerSQL(queryEjecutar));
//            ps.setString(1, multiservicio);
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("value", rs.getString("multiservicio"));
                fila.addProperty("label", rs.getString("numos"));
                arr.add(fila);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(arr);
        }
    }

    @Override
    public ArrayList<reporteOfertasBeans> buscarClienteMultiservicio(String multiservicio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        String query = "SQL_CARGAR_CLIENTE_MULTISERVICIO";
        ArrayList<reporteOfertasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, multiservicio);
            rs = ps.executeQuery();
            while (rs.next()) {
                reporteOfertasBeans beanFintra = new reporteOfertasBeans();
                beanFintra.setNomcli(rs.getString("nomcli"));
                lista.add(beanFintra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }
    }

    @Override
    public String cargarEndosoFacturas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        String query = "SQL_CARGAR_FACTURAS_ENDOSO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String CustodiadaPor() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CEDULA_CUSTODIA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("cedula"), rs.getString("cedula"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String Custodiador(String nit) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CUSTODIADOR";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("nombre"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String guardarFacturaEndoso(String nomproces, String nit, String custodiador, String cuenta, String cmc, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_FACTURA_ENDOSO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nomproces);
            ps.setString(2, nit);
            ps.setString(3, custodiador);
            ps.setString(4, cuenta);
            ps.setString(5, cmc);
            ps.setString(6, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String actualizarFacturaEndoso(String nomproces, String nit, String custodiador, String cuenta, String cmc, Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_FACTURA_ENDOSO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nomproces);
            ps.setString(2, nit);
            ps.setString(3, custodiador);
            ps.setString(4, cuenta);
            ps.setString(5, cmc);
            ps.setString(6, usuario.getLogin());
            ps.setString(7, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarNegociosPoliza(String polizas, String tipo_poliza, String aseguradora, String convenio, String cliente, String estado, String nombre_query) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        String condicion = "";
        String query = nombre_query;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        Gson gson = new Gson();

        try {
            con = this.conectarJNDI();

            if (!cliente.equals("")) {
                condicion = condicion + " and neg.cod_cli = '" + cliente + "'";
            }
            if (!aseguradora.equals("")) {
                JsonObject config = (JsonObject) new JsonParser().parse(cargarConfigPoliza(tipo_poliza, aseguradora));
                String id_config_poliza = config.get("id").getAsString();
                condicion = condicion + " and cp.id = '" + id_config_poliza + "'";
            }
            if (estado.equals("VIGENTE")) {
                condicion = condicion + " and t.fecha_fin::date > now()::date";
            } else if (estado.equals("INACTIVA")) {
                condicion = condicion + " and t.fecha_fin::date < now()::date ";
            } else if (estado.equals("PENDIENTE")) {
                condicion = condicion + " and t.tipo is null ";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    //System.out.println(rs.getMetaData().getColumnName(i));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String cargarDocumentosAjustepeso() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
             
        String query = "SQL_CARGAR_DOCUMENTOS_AJUSTE_PESO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String guardarExcelAjustepeso(JsonObject info, Usuario usuario) {
       JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = info.getAsJsonArray("json");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_HISTORIAL_AJUSTE_PESO"), true);
                st.setString(1, objeto.get("factura").getAsString());
                st.setString(2, objeto.get("codcli").getAsString());
                st.setString(3, objeto.get("descripcion").getAsString());
                st.setString(4, objeto.get("concepto").getAsString());
                st.setString(5, objeto.get("valor").getAsString());
                st.setString(6, objeto.get("cuenta_ajuste").getAsString());
                st.setString(7, objeto.get("banco").getAsString());
                st.setString(8, objeto.get("sucursal").getAsString());
                st.setString(9, objeto.get("fecha_consignacion").getAsString());
                st.setString(10, usuario.getLogin());
                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"CARGADO\"}";

            //respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String limpiarAjustePeso() {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_DOCUMENTOS_AJUSTE_PESO";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.executeUpdate();
            respuesta = "ELIMINADO";
        } catch (Exception ex) {
            respuesta = ex.getMessage();
            ex.getMessage();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }
        }
        return respuesta;
    }

    @Override
    public String AjustarPeso() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ACTUALIZAR_DOCUMENTOS_AJUSTE_PESO";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getBoolean("retorno") ? "SI" : "NO";
            }

        } catch (Exception ex) {
            respuesta = ex.getMessage();
            ex.getMessage();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }
        }
        return respuesta;
    }
    
    @Override
    public String generarPreAprobados(String query, String id_unidad_negocio, String periodo){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();       
        Gson gson = new Gson();
         JsonArray datos =null;    
         String filtro = "";
         if (query.equals("SQL_LISTAR_PREAPROBADOS")) {
            filtro = (periodo.equals("")) ? " AND periodo = replace(substring(now(),1,7),'-','')" : " AND periodo =" + periodo;
        }
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_unidad_negocio));
           
            rs = ps.executeQuery();
            datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();          
                fila.addProperty("id_unidad_negocio", rs.getString("id_unidad_negocio"));
                fila.addProperty("periodo", rs.getString("periodo"));
                fila.addProperty("negasoc", rs.getString("negasoc"));
                fila.addProperty("cedula_deudor", rs.getString("cedula_deudor"));
                fila.addProperty("nombre_deudor", rs.getString("nombre_deudor"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("celular", rs.getString("celular"));
                fila.addProperty("direccion", rs.getString("direccion"));
                fila.addProperty("barrio", rs.getString("barrio"));
                fila.addProperty("ciudad", rs.getString("ciudad"));
                fila.addProperty("email", rs.getString("email"));
                fila.addProperty("cedula_codeudor", rs.getString("cedula_codeudor"));
                fila.addProperty("nombre_codeudor", rs.getString("nombre_codeudor"));
                fila.addProperty("telefono_codeudor", rs.getString("telefono_codeudor"));
                fila.addProperty("celular_codeudor", rs.getString("celular_codeudor"));
                fila.addProperty("cuotas", rs.getString("cuotas"));                
                fila.addProperty("id_convenio", rs.getString("id_convenio"));
                fila.addProperty("afiliado", rs.getString("afiliado"));
                fila.addProperty("tipo", rs.getString("tipo"));
                fila.addProperty("fecha_desembolso", rs.getString("fecha_desembolso"));
                fila.addProperty("fecha_ult_pago", rs.getString("fecha_ult_pago"));                
                fila.addProperty("dias_pagos", rs.getString("dias_pagos"));
                fila.addProperty("vr_negocio", rs.getDouble("vr_negocio"));
                fila.addProperty("valor_factura", rs.getDouble("valor_factura"));
                fila.addProperty("valor_saldo", rs.getDouble("valor_saldo"));
                fila.addProperty("porcentaje", rs.getDouble("porcetaje"));
                fila.addProperty("altura_mora", rs.getString("altura_mora"));
                fila.addProperty("valor_preaprobado", rs.getDouble("valor_preaprobado"));          
                fila.addProperty("responsable_cuenta", rs.getString("responsable_cuenta"));          
                datos.add(fila);
            }          
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(datos);
    }
     
    
    @Override
      public String insertarPreAprobadosFintraCredit(String id_und_negocio,String periodo, String negocio, String cedula_deudor, String nombre_deudor, String telefono, String celular, String direccion, String barrio, String ciudad, String email, String cedula_codeudor, String nombre_codeudor, String telefono_codeudor, String celular_codeudor, String cuotas, String id_convenio, String afiliado, String tipo, String fecha_desembolso, String fecha_ult_pago, String dias_pagos, double vr_negocio, double valor_factura, double valor_saldo, double porcentaje, String altura_mora, double valor_preaprobado) {
        Connection con = null;
        StringStatement st;
        String sql = "";
        String Qry = "insertarPreAprobadosFintraCredit";
        String query = "";
       try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(Qry);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(id_und_negocio));
            st.setString(2, periodo);
            st.setString(3, negocio);
            st.setString(4, cedula_deudor);
            st.setString(5, nombre_deudor);
            st.setString(6, telefono);
            st.setString(7, celular);
            st.setString(8, direccion);
            st.setString(9, barrio);
            st.setString(10, ciudad);
            st.setString(11, email);
            st.setString(12, cedula_codeudor);
            st.setString(13, nombre_codeudor);
            st.setString(14, telefono_codeudor);
            st.setString(15, celular_codeudor);
            st.setString(16, cuotas);
            st.setString(17, id_convenio);
            st.setString(18, afiliado);
            st.setString(19, tipo);
            st.setString(20, fecha_desembolso);
            st.setString(21, fecha_ult_pago);
            st.setString(22, dias_pagos);
            st.setDouble(23, vr_negocio);
            st.setDouble(24, valor_factura);
            st.setDouble(25, valor_saldo);
            st.setDouble(26, porcentaje);
            st.setString(27, altura_mora);
            st.setDouble(28, valor_preaprobado);                                 
                              
            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertarPreAprobadosFintraCredit \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return sql;
        }
    }
      
    public boolean isAllowToGeneratePreAprobados(String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "SQL_VALIDAR_USER_GENERA_PREAPROB";     
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);         
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en isAllowToGeneratePreAprobados " + e.toString());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return resp;
    }
    


    @Override
    public String CargarControlCuentas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CONTROL_CUENTAS_AP";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion="\"respuesta\":\"ERROR\"";
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String cambiarEstadoControlCuentas(String id) {
       Connection con = null;
       PreparedStatement ps = null;
       String respuesta ="";
        String query = "SQL_CAMBIAR_ESTADO_CONTROL_CUENTAS_AP";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarUnidadNegocio() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_UNIDADES_NEGOCIO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }

        }
        return respuesta;
    }

    @Override
    public String guardarControlCuentas(Usuario usuario, String tiponegocio, String ixm, String gac, String cabing, String deting) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_CONTROL_CUENTAS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(tiponegocio));
            ps.setString(2, ixm);
            ps.setString(3, gac);
            ps.setString(4, cabing);
            ps.setString(5, deting);
            ps.setString(6, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String updateControlCuentas(Usuario usuario, String tiponegocio, String ixm, String gac, String cabing, String deting, String id, String cod_controlcuentas) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_CONTROL_CUENTAS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(tiponegocio));
            ps.setString(2, ixm);
            ps.setString(3, gac);
            ps.setString(4, cabing);
            ps.setString(5, deting);
            ps.setString(6, usuario.getLogin());
            ps.setString(7, id);
            ps.setString(8, cod_controlcuentas);

            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboTipoPolizas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_TIPO_POLIZAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre_poliza"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboAseguradoras(String poliza) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_ASEGURADORAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, poliza);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre_seguro"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboTodasAseguradoras() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_TODAS_ASEGURADORAS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre_seguro"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarComboConvenios() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COMBO_CONVENIOS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id_convenio"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String verificarCodigoFasecolda(String codigo_fasecolda) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_VERIFICACION_CODIGO_FASECOLDA";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codigo_fasecolda);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString(1);
            }

        } catch (Exception ex) {
            respuesta = "ERROR" + ex.getMessage();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "ERROR" + e.getMessage();
            }
        }
        return respuesta;
    }

    @Override
    public String cargarConfigPoliza(String poliza, String aseguradora) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        String query = "CARGAR_ID_CONFIG_POLIZA";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);
            ps.setString(1, poliza);
            ps.setString(2, aseguradora);
            rs = ps.executeQuery();
            // lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    System.out.println(rs.getMetaData().getColumnName(i));
                }
                // lista.add(objetoJson);
            }
            informacion = new Gson().toJson(objetoJson);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "ERROR" + e.getMessage();
            }
        }
        return informacion;
    }

    @Override
    public String ActualizarPGV(Usuario usuario, String id_config_poliza, String neg_vehiculo, String nomcliente, String nit, String afiliado, String placa, String servicio, String Vlrpoliza, String fecha_inicio, String fecha_fin, String codigo_fasecolda, String tipo) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "ACTUALIZAR_PGV";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(id_config_poliza));
            ps.setString(2, neg_vehiculo);
            ps.setString(3, nomcliente);
            ps.setString(4, nit);
            ps.setString(5, afiliado);
            ps.setString(6, placa);
            ps.setString(7, servicio);
            ps.setInt(8, Integer.parseInt(Vlrpoliza));
            ps.setDate(9, Date.valueOf(fecha_inicio));
            ps.setDate(10, Date.valueOf(fecha_fin));
            ps.setString(11, codigo_fasecolda);
            ps.setString(12, tipo);
            ps.setString(13, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }
        }
        return respuesta;
    }

    @Override
    public String marcarPGV(Usuario usuario, String neg_vehiculo, String fecha_fin) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_MARCAR_PROCESO_JURIDIC";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            JsonObject config = (JsonObject) new JsonParser().parse(VerificarProcJuridic(neg_vehiculo));
            String marcado = config.get("respuesta").getAsString();
            if (marcado.equals("NO")) {
                respuesta = "NO MARCADO";
            } else {
                ps.setString(1, usuario.getLogin());
                ps.setString(2, neg_vehiculo);
                ps.setString(3, fecha_fin);
                ps.executeUpdate();
                respuesta = "Guardado";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }
        }
        return respuesta;
    }

    public String VerificarProcJuridic(String neg_vehiculo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        String query = "SQL_VERIFICACION_PROC_JURIDIC_NEG";
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);
            ps.setString(1, neg_vehiculo);
            rs = ps.executeQuery();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    System.out.println(rs.getMetaData().getColumnName(i));
                }
            }
            informacion = new Gson().toJson(objetoJson);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }
        }
        return informacion;
    }

    @Override
    public String datosPdf(Usuario usuario, JsonObject info) {
        String respuesta = "";
        JsonObject objeto = new JsonObject();
        try {
            String ancabezado = "";
            String parrafo = "";
            JsonArray arr = info.getAsJsonArray("json");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                if (objeto.get("accion_ejecutar").getAsString().equals("renovacion")) {
                    ancabezado = " Notificaci�n de Renovaci�n de P�liza.";
                    parrafo = "La presente es para informarle que su P�liza Todo Riesgo de Veh�culo sera renovada en "
                            + "el pr�ximo mes. Lo anterior para que realice los respectivos tr�mites ante nuestra entidad.";

                } else if (objeto.get("accion_ejecutar").getAsString().equals("revocacion")) {
                    ancabezado = " Solicitud de Revocaci�n de P�liza.";
                }
                String placa = objeto.get("placa").getAsString() == null ? "" : objeto.get("placa").getAsString();
                respuesta = exportarPdfPolizas(usuario, objeto.get("nombre").getAsString(), objeto.get("nombre_aseguradora").getAsString(), placa, objeto.get("nit").getAsString(), objeto.get("accion_ejecutar").getAsString(), ancabezado, parrafo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return respuesta;
    }

    public String exportarPdfPolizas(Usuario usuario, String nombre, String nombre_seguro, String placa, String nit, String accion, String ancabezado, String parrafo) {

        String ruta = "";
        String msg = "OK";
        try {
            ruta = this.directorioArchivo(usuario.getLogin(), "pdf", accion, nit);

            Font fuente = new Font(Font.HELVETICA, 10, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font fuenteG = new Font(Font.HELVETICA, 10, Font.BOLD, new java.awt.Color(0, 0, 0));
            PdfPCell celda = null;
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(ruta));
            documento.open();
            documento.newPage();

            PdfPTable thead = new PdfPTable(1);
            celda = new PdfPCell();
            celda.setBorder(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //row2
            thead = new PdfPTable(1);
            Calendar calendario = Calendar.getInstance();
            String mes = "";
            mes = this.mesToString(calendario.get(Calendar.MONTH) + 1);
            documento.add(new Paragraph(new Phrase("Barranquilla, " + mes + " " + calendario.get(Calendar.DAY_OF_MONTH) + " de " + calendario.get(Calendar.YEAR), fuente)));
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);

            documento.add(thead);
            thead = new PdfPTable(1);

            if (accion.equals("renovacion")) {
                documento.add(new Paragraph(new Phrase("Se�or(a),", fuenteG)));
                celda.setPhrase(new Paragraph(new Phrase("", fuente)));
                thead.addCell(celda);
                documento.add(new Paragraph(new Phrase(nombre + ",", fuenteG)));
                celda.setPhrase(new Paragraph(new Phrase("", fuente)));
                thead.addCell(celda);
            } else if (accion.equals("revocacion")) {
                documento.add(new Paragraph(new Phrase("Se�ores,", fuenteG)));
                celda.setPhrase(new Paragraph(new Phrase("", fuente)));
                thead.addCell(celda);
                documento.add(new Paragraph(new Phrase(nombre_seguro + ",", fuenteG)));
                celda.setPhrase(new Paragraph(new Phrase("", fuente)));
                thead.addCell(celda);
            }

            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(new Paragraph(new Phrase("Referencia: " + ancabezado, fuenteG)));
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            documento.add(new Paragraph(new Phrase("Estimado cliente,", fuente)));
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);

            if (accion.equals("renovacion")) {
                thead = new PdfPTable(1);
                documento.add(new Paragraph(new Phrase(parrafo, fuente)));
                thead.addCell(celda);
                documento.add(thead);

                String str = "Esperamos su pronta respuesta ya que cumplido el tiempo estipulado ser� renovada automaticamente con la aseguradora"
                        + " <b>" + nombre_seguro + "</b> , seg�n clausula estipulada en el contrato de prenda.";
                HTMLWorker htmlWorker = new HTMLWorker(documento);
                htmlWorker.parse(new StringReader(str));

                celda.setPhrase(new Paragraph(new Phrase("", fuente)));
                thead.addCell(celda);
                documento.add(thead);
            } else if (accion.equals("revocacion")) {
                thead = new PdfPTable(1);
                documento.add(new Paragraph(new Phrase(parrafo, fuente)));
                thead.addCell(celda);
                documento.add(thead);

                String str = "La presente es para solicitar revocaci�n de la p�liza del veh�culo de placas  <b>" + placa + "</b> , a cargo del cliente "
                        + "<b> " + nombre + "</b> cuyo beneficiario es <b>FINTRA S.A.</b> Lo anterior obedece a la cancelaci�n del contrato con el cliente.";
                HTMLWorker htmlWorker = new HTMLWorker(documento);
                htmlWorker.parse(new StringReader(str));

                celda.setPhrase(new Paragraph(new Phrase("", fuente)));
                thead.addCell(celda);
                documento.add(thead);
            }

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("Atentamente,", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("Departamento de Operaciones ", fuenteG)));
            documento.add(new Paragraph(new Phrase("y Control de Garantias", fuenteG)));
            documento.close();
        } catch (Exception e) {
            msg = "ERROR";
            e.printStackTrace();
        }
        return msg;
    }

    private String directorioArchivo(String user, String extension, String accion, String nit) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rsb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + accion + "_" + nit + "_" + fmt.format(new java.util.Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    private Document createDoc() {
        Document doc = new Document(PageSize.A4, 25, 25, 35, 30);
        return doc;
    }

    private String mesToString(int mes) {
        String texto = "";
        switch (mes) {
            case 1:
                texto = "Enero";
                break;
            case 2:
                texto = "Febrero";
                break;
            case 3:
                texto = "Marzo";
                break;
            case 4:
                texto = "Abril";
                break;
            case 5:
                texto = "Mayo";
                break;
            case 6:
                texto = "Junio";
                break;
            case 7:
                texto = "Julio";
                break;
            case 8:
                texto = "Agosto";
                break;
            case 9:
                texto = "Septiembre";
                break;
            case 10:
                texto = "Octubre";
                break;
            case 11:
                texto = "Noviembre";
                break;
            case 12:
                texto = "Diciembre";
                break;
            default:
                texto = "Enero";
                break;
        }
        return texto;
    }

    @Override
    public String cargarObservaciones(String id_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OBSERVACIONES";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String CargarPolizas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_POLIZAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
}    

    @Override
    public String guardarPoliza(Usuario usuario, String nombre_poliza, String descripcion){
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_POLIZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nombre_poliza);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
}
    
    @Override
    public String actualizarPoliza(Usuario usuario, String nombre_poliza, String descripcion, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_UPDATE_POLIZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nombre_poliza);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
}
    
    @Override
    public String CargarCuotaManejo() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CUOTA_MANEJO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cambiarEstadoCuotaManejo(String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_CUOTA_MANEJO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String actualizarCuotaManejo(Usuario usuario, String id, String valor) {
         Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_CUOTA_MANEJO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setDouble(1, Double.parseDouble(valor));
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cambiarEstadoPoliza(Usuario usuario, String id) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_CAMBIAR_ESTADO_POLIZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
}
    
 
    @Override
    public String cargarConvenios(String unidad_negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CONVENIOS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, unidad_negocio);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }
    
    @Override
    public String actualizarConvenio(Usuario usuario, String convenio, String tasa_interes, String tasa_usura, String tasa_compra_cartera, String id_convenio,String tasa_aval,String tasa_max_fintra,String tasa_sic_ea) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_UPDATE_CONVENIO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            
            st.setString(1, convenio);
            st.setDouble(2, Double.parseDouble(tasa_interes));
            st.setDouble(3, Double.parseDouble(tasa_usura));
            st.setDouble(4, Double.parseDouble(tasa_compra_cartera));
            st.setDouble(5, Double.parseDouble(tasa_aval));
            st.setString(6, usuario.getLogin());
            st.setDouble(7, Double.parseDouble(tasa_max_fintra));
            st.setDouble(8, Double.parseDouble(tasa_sic_ea));
            st.setInt(9, Integer.parseInt(id_convenio));
            respuesta = st.getSql();
        } catch (Exception e) {
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
}
    
    /**
     *
     * @param usuario
     * @param unidad_negocio
     * @return
     */
    @Override
     public String actualizarConveniosxUnidadNegocio(Usuario usuario, String unidad_negocio, String tasa_interes,  String tasa_usura, String tasa_compra_cartera,String tasa_avalc) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_UPDATE_CONVENIOS_UNIDAD_NEGOCIO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setDouble(1, Double.parseDouble(tasa_interes));
            st.setDouble(2, Double.parseDouble(tasa_usura));
            st.setDouble(3, Double.parseDouble(tasa_compra_cartera));
            st.setDouble(4, Double.parseDouble(tasa_avalc));
            st.setString(5, usuario.getLogin());
            st.setString(6, unidad_negocio);
            respuesta = st.getSql();
        } catch (Exception e) {
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
}
    

    @Override
    public String reportecostos() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_REPORTE_COSTOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }
 
    /**
     *
     * @param multiservicio
     * @param id_solicitud
     * @return
     */
    @Override
    public String detallecostos(String multiservicio, String id_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DETALLE_COSTOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, multiservicio);
            ps.setString(2, id_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }
    
    
    @Override
    public String listarInsumos() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_INSUMOS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
}

    @Override
    public String ActualizarInsumo(Usuario usuario, String id, String codigo_material, String descripcion) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_INSUMO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codigo_material);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }
    
    @Override
    public String cargarValores() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_VALORES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
}
    
    
    @Override
    public String actualizarValores(Usuario usuario, String id, String valor_xdefecto) {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        String respuesta = "";
        String respuesta1 = "";
        String query = "SQL_ACTUALIZAR_VALORES";
        String query2 = "SQL_ACTUALIZAR_ESPECIFICACION";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps1 = con.prepareStatement(this.obtenerSQL(query2));
            ps.setString(1, valor_xdefecto);
            ps.setString(2, valor_xdefecto);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps1.setString(1, valor_xdefecto);
            ps1.setString(2, usuario.getLogin());
            ps1.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
            if (respuesta == "Guardado") {
                ps1.executeUpdate();
            respuesta1 = "Guardado";
            }
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (ps1 != null) {
                    ps1.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta1;
    }
    }
    
    
    @Override
    public String cargarSubCategorias() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_SUBCATEGORIAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
}
    
    @Override
    public String actualizarSubcategoria(Usuario usuario, String id, String nombre) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_SUBCATEGORIA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nombre);
            ps.setString(2, nombre);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }
    
    @Override
    public String cargarCategorias() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CATEGORIAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
}
    
    @Override
    public String actualizarCategoria(Usuario usuario, String id, String nombre) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_CATEGORIA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nombre);
            ps.setString(2, nombre);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }
    
    @Override
    public String cargarApu() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_APU";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
}
    
    @Override
    public String actualizarApu(Usuario usuario, String id, String nombre) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_APU";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nombre);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }
    
    @Override
    public String cargarEspecificaciones() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ESPECIFICACIONES";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return informacion;
    }
}
    
    @Override
    public String actualizarEspecificacion(Usuario usuario, String id, String nombre) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_ESPECIFICACION";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nombre);
            ps.setString(2, nombre);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuesta = "Guardado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
    return respuesta;
    }
    }
    
    @Override
    public String consultarClasificacionClientes(String id_unidad_negocio, String periodo){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();       
        Gson gson = new Gson();
        JsonArray datos =null; 
        JsonObject jsonObject = new JsonObject();
        String query = "SQL_LISTAR_CLASIFICACION_CLIENTES";
        String filtro =(periodo.equals("")) ? " AND periodo = replace(substring(now(),1,7),'-','')" : " AND periodo =" + periodo;
 
        try{
            con = this.conectarJNDI();   
            ps = con.prepareStatement( this.obtenerSQL(query).replaceAll("#filtro", filtro));
            ps.setInt(1, Integer.parseInt(id_unidad_negocio));
           
            rs = ps.executeQuery();
            datos = new JsonArray();            
            while (rs.next()){
                jsonObject = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                datos.add(jsonObject);              
            }          
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(datos);
    }

    @Override
    public String cargarAnioImpuesto(String anio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ANIO_IMPUESTO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("anio"), rs.getString("anio"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
    
    public String cargarAnticipos(String conductor, String placa, String planilla) {
         Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String filtro = "";
        String query = "SQL_CARGAR_ANTICIPOS_ESTACIONES";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, conductor);
            ps.setString(2, placa);
            ps.setString(3, planilla);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        return informacion;
    }
    
        public String autorizarAnticipo(String id) {
       Connection con = null;
       PreparedStatement ps = null;
       String respuesta ="";
        String query = "SQL_AUTORIZAR_ANTICIPO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            ps.executeUpdate();
            respuesta = "Actualizado";

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String mostrarPresolicitud(String numero_solicitud) {
      Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NEGOCIO_A_PROGRAMAR";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numero_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String marcarRecoleccionFirmas(String numero_solicitud, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_PROGRAMAR_RECOLECCION";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            ps.setString(2, numero_solicitud);
            ps.executeUpdate();
            respuesta = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuesta;
        }
    }
    
    public String buscarNegocioAreactivar(String cod_neg, String fechaInicio, String fechaFin) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NEGOCIO_RECHAZADO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";

            if (!cod_neg.equals("")) {
                parametro = parametro + "and n.cod_neg = '" + cod_neg + "'";
            }

            if (!fechaInicio.equals("") && !fechaFin.equals("")) {
                parametro = parametro + " and fecha_negocio::date between '" + fechaInicio + "'::date and '" + fechaFin + "'::date";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String reactivarNegocio(Usuario usuario, String cod_neg) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_REACTIVAR_NEGOCIO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, cod_neg);
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }

    @Override
    public String reactivarSolicitud(Usuario usuario, String cod_neg) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_REACTIVAR_SOLICITUD";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, cod_neg);
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }

    @Override
    public String insertarTrazabilidad(Usuario usuario, String cod_neg, String comentario) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_INSERTAR_TRAZABILIDAD";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, cod_neg);
            st.setString(2, usuario.getLogin());
            st.setString(3, cod_neg);
            st.setString(4, comentario);
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }
    
    public String historicoConvenios(Usuario usuario, String convenio, String tasa_interes, String tasa_usura, String tasa_compra_cartera, String id_convenio, String tasa_aval,String tasa_max_fintra,String tasa_sic_ea) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_HISTORICO_CONVENIO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(id_convenio));
            st.setString(2, convenio);
            st.setDouble(3, Double.parseDouble(tasa_interes));
            st.setDouble(4, Double.parseDouble(tasa_usura));
            st.setDouble(5, Double.parseDouble(tasa_compra_cartera));
            st.setDouble(6, Double.parseDouble(tasa_aval));
            st.setString(7, usuario.getLogin());
            st.setDouble(8, Double.parseDouble(tasa_max_fintra));
            st.setDouble(9, Double.parseDouble(tasa_sic_ea));
            respuesta = st.getSql();
        } catch (Exception e) {
            //e.printStackTrace();
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
    }
    
    @Override
     public String historicoConveniosMasivo(Usuario usuario, String unidad_negocio, String tasa_interes, String tasa_usura, String tasa_compra_cartera,String tasa_avalc) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_HISTORICO_CONVENIO_MASIVO";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setDouble(1, Double.parseDouble(tasa_interes));
            st.setDouble(2, Double.parseDouble(tasa_usura));
            st.setDouble(3, Double.parseDouble(tasa_compra_cartera));
            st.setDouble(4, Double.parseDouble(tasa_avalc));
            st.setString(5, usuario.getLogin());
            st.setString(6, unidad_negocio);
            respuesta = st.getSql();
        } catch (Exception e) {
            respuesta = e.getMessage();
        }
        finally {
            return respuesta;
    }
}

    @Override
    public String cargarNuevasPolizas() {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        JsonArray lista = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CARGAR_NUEVAS_POLIZAS"));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while(rs.next()) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id", rs.getInt(1));
                obj.addProperty("estado", rs.getString(2));
                obj.addProperty("cambio", rs.getString(3));
                obj.addProperty("dstrct", rs.getString(4));
                obj.addProperty("descripcion", rs.getString(5));                
                lista.add(obj);
            }
            return new Gson().toJson(lista);
        } catch (SQLException ex) {
            JsonObject error = new JsonObject();
            error.addProperty("error", ex.getMessage());
            return new Gson().toJson(error);           
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean insertarNuevasPolizas(Usuario usuario, String descripcion) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("INSERTAR_NUEVAS_POLIZAS"));
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            
            if (ps.executeUpdate() >0) {                
                return true;                
            } else {
                return false;
            }
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {                
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean actualizarNuevasPolizas(Usuario usuario, String descripcion, int id) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("ACTUALIZAR_NUEVA_POLIZAS"));
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, id);

            if (ps.executeUpdate() > 0) {               
                return true;                
            } else {
                return false;
            }       
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean cambiarEstadoNuevasPolizas(String usuario, int id, String estado) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CAMBIAR_ESTADO_NUEVA_POLIZAS"));
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, id);
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public String cargarConfiguracionPoliza(int sucursal, int unidadNegocio) {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        JsonArray lista = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CARGAR_CONFIGURACION_POLIZA"));
            ps.setInt(1, unidadNegocio);
            ps.setInt(2, sucursal);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while(rs.next()) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id", rs.getInt(1));
                obj.addProperty("estado", rs.getString(2));
                obj.addProperty("cambio", rs.getString(3));
                obj.addProperty("id_poliza", rs.getInt(4));
                obj.addProperty("nombre_poliza", rs.getString(5));
                obj.addProperty("id_aseguradora", rs.getInt(6));
                obj.addProperty("nombre_aseguradora", rs.getString(7));
                obj.addProperty("id_tipo_cobro", rs.getInt(8));
                obj.addProperty("tipo_cobro", rs.getString(9));
                obj.addProperty("financiacion", rs.getString(10));
                obj.addProperty("id_valor_poliza", rs.getString(11));
                obj.addProperty("tipo_valor", rs.getString(12));
                obj.addProperty("valor_sobre", rs.getString(13));
                obj.addProperty("valor_absoluto", rs.getInt(14));
                obj.addProperty("valor_porcentaje", rs.getFloat(15));
                obj.addProperty("id_sucursal", rs.getInt(16));
                obj.addProperty("nombre_sucursal", rs.getString(17));
                obj.addProperty("id_afiliado", rs.getString(18));
                obj.addProperty("nombre_afiliado", rs.getString(19));
                obj.addProperty("id_unidad_negocio", rs.getString(20));
                lista.add(obj);
            }
            return new Gson().toJson(lista);
        } catch (SQLException ex) {
            JsonObject error = new JsonObject();
            error.addProperty("error", ex.getMessage());
            return new Gson().toJson(error);           
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean insertarConfiguracionPoliza(Usuario usuario, int poliza, int aseguradora, int tipoCobro, int valorPoliza, int unidadNegocio, int sucursal) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("INSERTAR_CONFIGURACION_POLIZA"));
            ps.setInt(1, aseguradora);
            ps.setInt(2, valorPoliza);
            ps.setInt(3, tipoCobro);
            ps.setInt(4, poliza);
            ps.setInt(5, unidadNegocio);
            ps.setInt(6, sucursal);
            ps.setString(7, usuario.getDstrct());
            ps.setString(8, usuario.getLogin());
            ps.setString(9, usuario.getLogin());
            
            if (ps.executeUpdate() >0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            System.err.println(ex.getErrorCode() + "-" + ex.getSQLState() + "-" + ex.getMessage());
            throw ex;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean actualizarConfiguracionPoliza(Usuario usuario, int poliza, int aseguradora, int tipoCobro, int valorPoliza, int unidadNegocio, int sucursal, int id) throws SQLException  {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("ACTUALIZAR_CONFIGURACION_POLIZA"));
            ps.setInt(1, aseguradora);
            ps.setInt(2, valorPoliza);
            ps.setInt(3, tipoCobro);
            ps.setInt(4, poliza);
            ps.setInt(5, unidadNegocio);
            ps.setInt(6, sucursal);
            ps.setString(7, usuario.getLogin());
            ps.setInt(8, id);

            if (ps.executeUpdate() > 0) {
                return true;
            } else {
                return false;
            }       
        } catch (SQLException ex) {
            System.err.println(ex.getErrorCode() + "-" + ex.getSQLState() + "-" + ex.getMessage());
            throw ex;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean cambiarEstadoConfiguracionPoliza(String usuario, int id, String estado) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CAMBIAR_ESTADO_CONFIGURACION_POLIZA"));
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, id);
            
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }
     
    @Override
    public String cargarAseguradoras() {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        JsonArray lista = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CARGAR_ASEGURADORAS"));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while(rs.next()) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id", rs.getInt(1));
                obj.addProperty("estado", rs.getString(2));
                obj.addProperty("cambio", rs.getString(3));
                obj.addProperty("descripcion", rs.getString(4));
                obj.addProperty("nit", rs.getLong(5));
                obj.addProperty("retorno", rs.getFloat(6));
                obj.addProperty("plazo_pago", rs.getInt(7));
                obj.addProperty("prr", rs.getInt(8));
//                obj.addProperty("fecha_creacion", rs.getString(8));
                lista.add(obj);
            }
            return new Gson().toJson(lista);
        } catch (SQLException ex) {
            JsonObject error = new JsonObject();
            error.addProperty("error", ex.getMessage());
            return new Gson().toJson(error);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean insertarAseguradoras(Usuario usuario, String descripcion, long nit, float retorno, int plazoPago, int prr) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("INSERTAR_ASEGURADORAS"));
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, descripcion);
            ps.setLong(3, nit);
            ps.setFloat(4, retorno);
            ps.setInt(5, plazoPago);
            ps.setInt(6, prr);
            ps.setString(7, usuario.getLogin());
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean actualizarAseguradoras(Usuario usuario, String descripcion, float retorno, int plazoPago, int prr, int id) throws SQLException{
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("ACTUALIZAR_ASEGURADORAS"));
            ps.setString(1, descripcion);
            ps.setFloat(2, retorno);
            ps.setInt(3, plazoPago);
            ps.setInt(4, prr);
            ps.setString(5, usuario.getLogin());
            ps.setInt(6, id);
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean cambiarEstadoAseguradora(String usuario, int id, String estado) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CAMBIAR_ESTADO_ASEGURADORAS"));
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, id);
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public String cargarTipoCobro() {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        JsonArray lista = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CARGAR_TIPO_COBRO"));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while(rs.next()) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id", rs.getInt(1));
                obj.addProperty("estado", rs.getString(2));
                obj.addProperty("cambio", rs.getString(3));
                obj.addProperty("descripcion", rs.getString(4));
                obj.addProperty("tipo", rs.getString(5));
                obj.addProperty("financiacion", rs.getString(6));
//                obj.addProperty("fecha_creacion", rs.getString(6));
                lista.add(obj);
            }
            return new Gson().toJson(lista);
        } catch (SQLException ex) {
            JsonObject error = new JsonObject();
            error.addProperty("error", ex.getMessage());
            return new Gson().toJson(error);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean insertarTipoCobro(Usuario usuario, String descripcion, String tipo, String financiacion) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("INSERTAR_TIPO_COBRO"));
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, descripcion);
            ps.setString(3, tipo);
            ps.setString(4, financiacion);
            ps.setString(5, usuario.getLogin());
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean actualizarTipoCobro(Usuario usuario, String descripcion, String tipo, String financiacion, int id) throws SQLException {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("ACTUALIZAR_TIPO_COBRO"));
            ps.setString(1, descripcion);
            ps.setString(2, tipo);
            ps.setString(3, financiacion);
            ps.setString(4, usuario.getLogin());
            ps.setInt(5, id);
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean cambiarEstadoTipoCobro(String usuario, int id, String estado) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CAMBIAR_ESTADO_TIPO_COBRO"));
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, id);
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public String cargarValorPoliza() {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;        
        JsonArray lista = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CARGAR_VALOR_POLIZA"));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while(rs.next()) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id", rs.getInt(1));
                obj.addProperty("estado", rs.getString(2));
                obj.addProperty("cambio", rs.getString(3));
                obj.addProperty("descripcion", rs.getString(4));
                obj.addProperty("tipo", rs.getString(5));
                obj.addProperty("calcular_sobre", rs.getString(6));
                obj.addProperty("valor_porcentaje", rs.getDouble(7));
                obj.addProperty("valor_absoluto", rs.getDouble(8));
                obj.addProperty("iva", rs.getDouble(9));
                lista.add(obj);
            }
            return new Gson().toJson(lista);
        } catch (SQLException ex) {
            JsonObject error = new JsonObject();
            error.addProperty("error", ex.getMessage());
            return new Gson().toJson(error);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean insertarValorPoliza(Usuario usuario, String descripcion, String tipo, String calcularSobre, float valorPorcentaje, float valorAbsoluto, boolean iva) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("INSERTAR_VALOR_POLIZA"));
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, descripcion);
            ps.setString(3, tipo);
            ps.setString(4, calcularSobre);
            ps.setFloat(5, valorPorcentaje);
            ps.setFloat(6,  valorAbsoluto);
            ps.setFloat(7, (iva ? obtenerIva() : 0));
            ps.setString(8, usuario.getLogin());
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean actualizarValorPoliza(Usuario usuario, String descripcion, String tipo, String calcularSobre, float valorPorcentaje, float valorAbsoluto, boolean iva, int id) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("ACTUALIZAR_VALOR_POLIZA"));
            ps.setString(1, descripcion);
            ps.setString(2, tipo);
            ps.setString(3, calcularSobre);
            ps.setFloat(4, valorPorcentaje);
            ps.setFloat(5,  valorAbsoluto);
            ps.setFloat(6, (iva ? obtenerIva() : 0));
            ps.setString(7, usuario.getLogin());
            ps.setInt(8, id);
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            throw ex;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }

    @Override
    public boolean cambiarEstadoValorPoliza(String usuario, int id, String estado) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CAMBIAR_ESTADO_VALOR_POLIZA"));
            ps.setString(1, estado);
            ps.setString(2, usuario);
            ps.setInt(3, id);
            if (ps.executeUpdate() > 0)
                return true;
            else
                return false;            
        } catch (SQLException ex) {
            return false;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }
    
    @Override
    public String cargarSucursalesUnidad(int unidadNegocio, String ciudad, String departamento) {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;        
        JsonArray lista = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CARGAR_SUCURSALES_UNIDAD"));
            ps.setInt(1, unidadNegocio);
            ps.setString(2, ciudad);
            ps.setString(3, departamento);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while(rs.next()) {
                JsonObject obj = new JsonObject();
                obj.addProperty("id", rs.getInt(1));
                obj.addProperty("descripcion", rs.getString(2));
                lista.add(obj);
            }
            return new Gson().toJson(lista);
        } catch (SQLException ex) {
            JsonObject error = new JsonObject();
            error.addProperty("error", ex.getMessage());
            return new Gson().toJson(error);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }
    
    private float obtenerIva() {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;        
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("OBTENER_IVA"));
            rs = ps.executeQuery();

            while(rs.next()) {
                return rs.getFloat(1);
            }
            return 0;
        } catch (SQLException ex) {
            JsonObject error = new JsonObject();
            error.addProperty("error", ex.getMessage());
            return 0;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }  
    
    public Map cargarDepartamentos() throws SQLException {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        Map<String, String> departamentos = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CARGAR_DEPARTAMENTOS"));
            rs = ps.executeQuery();
            
            departamentos = new LinkedHashMap<String, String>();
            while(rs.next()) {
                departamentos.put(rs.getString(1), rs.getString(2));
            }
            return departamentos;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }
    
    public Map cargarCiudades(String dpto) throws SQLException {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        Map<String, String> ciudades = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CARGAR_CIUDADES"));
            ps.setString(1, dpto);
            rs = ps.executeQuery();
            
            ciudades = new LinkedHashMap<String, String>();
            while(rs.next()) {
                ciudades.put(rs.getString(1), rs.getString(2));
            }
            return ciudades;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }    
    
    @Override
    public int aplicarConfiguracionesPolizas(String[] ids, String usuario) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("COPIAR_CONFIGURACIONES_POLIZAS"));
            ps.setString(1, usuario);
            ps.setArray(2, conn.createArrayOf("integer", ids));
            return ps.executeUpdate();        
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("Error at " + ex.getClass().getSimpleName() + ex.getMessage());
            }
        }
    }
    
    @Override
    public String guardarExcelFacturacion(JsonObject info, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = info.getAsJsonArray("json");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_FACTURACION_EXCEL"), true);
                st.setString(1, objeto.get("id").getAsString());
                st.setString(2, objeto.get("cod_rop").getAsString());
                st.setString(3, objeto.get("cod_rop_barcode").getAsString());
                st.setString(4, objeto.get("generado_el").getAsString());
                st.setString(5, objeto.get("vencimiento_rop").getAsString());
                st.setString(6, objeto.get("negocio").getAsString());
                st.setString(7, objeto.get("cedula").getAsString());
                st.setString(8, objeto.get("nombre_cliente").getAsString());
                st.setString(9, objeto.get("direccion").getAsString());
                st.setString(10, objeto.get("departamento").getAsString());
                st.setString(11, objeto.get("ciudad").getAsString());
                st.setString(12, objeto.get("barrio").getAsString());
                st.setString(13, objeto.get("agencia").getAsString());
                st.setString(14, objeto.get("linea_producto").getAsString());
                st.setString(15, objeto.get("cuotas_vencidas").getAsString());
                st.setString(16, objeto.get("cuotas_pendientes").getAsString());
                st.setString(17, objeto.get("dias_vencidos").getAsString());
                st.setString(18, objeto.get("fch_ultimo_pago").getAsString());
                st.setString(19, objeto.get("subtotal_rop").getAsString());
                st.setString(20, objeto.get("total_sanciones").getAsString());
                st.setString(21, objeto.get("total_descuentos").getAsString());
                st.setString(22, objeto.get("total_rop").getAsString());
                st.setString(23, objeto.get("total_abonos").getAsString());
                st.setString(24, objeto.get("observacion").getAsString());
                st.setString(25, objeto.get("msg_paguese_antes").getAsString());
                st.setString(26, objeto.get("msg_estado_credito").getAsString());
                st.setString(27, objeto.get("capital").getAsString());
                st.setString(28, objeto.get("interes_financiacion").getAsString());
                st.setString(29, objeto.get("seguro").getAsString());
                st.setString(30, objeto.get("interes_xmora").getAsString());
                st.setString(31, objeto.get("gastos_cobranza").getAsString());
                st.setString(32, objeto.get("dscto_capital").getAsString());
                st.setString(33, objeto.get("dscto_interes_financiacion").getAsString());
                st.setString(34, objeto.get("dscto_interes_xmora").getAsString());
                st.setString(35, objeto.get("dscto_gastos_cobranza").getAsString());
                st.setString(36, objeto.get("dscto_seguro").getAsString());
                st.setString(37, objeto.get("ksubtotal_corriente").getAsString());
                st.setString(38, objeto.get("ksubtotal_vencido").getAsString());
                st.setString(39, objeto.get("ksubtotalneto").getAsString());
                st.setString(40, objeto.get("kdescuentos").getAsString());
                st.setString(41, objeto.get("ktotal").getAsString());
                st.setString(42, objeto.get("Items").getAsString());
                st.setString(43, objeto.get("establecimiento_comercio").getAsString());
                st.setString(44, objeto.get("telefono").getAsString());
                st.setString(45, objeto.get("email").getAsString());
                st.setString(46, objeto.get("extracto_email").getAsString());
//                st.setString(47, objeto.get("generado").getAsString());
                st.setString(47, objeto.get("periodo").getAsString());
                st.setString(48, objeto.get("ciclo").getAsString());
                st.setString(49, usuario.getLogin());
                st.setString(50, objeto.get("lote").getAsString());
                st.setString(51,usuario.getDstrct());
                               

                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"OK\"}";

            //respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "ERROR";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarExtractosPorGenerar(String lote_generado) {
       JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "CARGAR_EXTRACTOS_POR_GENERAR";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, lote_generado);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("cod_rop", rs.getString("cod_rop"));              
                fila.addProperty("generado_el", rs.getString("generado_el"));
                fila.addProperty("negocio", rs.getString("negocio"));
                fila.addProperty("cedula", rs.getString("cedula"));
                fila.addProperty("nombre_cliente", rs.getString("nombre_cliente"));
                fila.addProperty("linea_producto", rs.getString("linea_producto"));
                fila.addProperty("periodo", rs.getString("periodo"));
                fila.addProperty("ciclo", rs.getString("ciclo"));
                fila.addProperty("fecha_creacion", rs.getString("fecha_creacion"));
                fila.addProperty("generado", rs.getBoolean("generado"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String cargarComboExtractosPorGenerar() {
        String respuesta ="{}";
       JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "CARGAR_COMBO_EXTRACTOS_POR_GENERAR";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("lote"), rs.getString("lote"));
            }
           respuesta = gson.toJson(obj);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return respuesta;
    }

    @Override
    public String generarExtractosDigitales(String lote_generado , Usuario usuario) {
      String resp = null;
        JsonArray  respJsonArray = getInfoExtractos(lote_generado);
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        DecimalFormat formatea = new DecimalFormat("###,###.##");
        String rutaOrigenPdf=rb.getString("rutaInformes");
        
        com.itextpdf.text.Font fuente= new com.itextpdf.text.Font();
        fuente.setFamily(com.itextpdf.text.Font.FontFamily.TIMES_ROMAN.toString());
        fuente.setStyle(Font.BOLD);
        fuente.setSize(12);
        
        com.itextpdf.text.Font fuente2= new com.itextpdf.text.Font();
        fuente2.setFamily(com.itextpdf.text.Font.FontFamily.TIMES_ROMAN.toString());
        fuente2.setSize(8);
        
        com.itextpdf.text.Font fuente3= new com.itextpdf.text.Font();
        fuente3.setFamily(com.itextpdf.text.Font.FontFamily.TIMES_ROMAN.toString());
        fuente3.setSize(6);
        
        com.itextpdf.text.Font fuenteValor= new com.itextpdf.text.Font();
        fuenteValor.setFamily(com.itextpdf.text.Font.FontFamily.TIMES_ROMAN.toString());
        fuenteValor.setSize(8);

        if(respJsonArray != null){
            for (JsonElement data : respJsonArray) {
             String rutaDestino = rb.getString("rutaImagenes") + "fintracredit1/";
             JsonObject objectcode = data.getAsJsonObject();
             String nombArchivo=objectcode.get("cod_rop").getAsString()+"_"+objectcode.get("negocio").getAsString();
             rutaDestino=rutaDestino+objectcode.get("negocio").getAsString();
                try {
                    String ruta = Util.rutaArchivo(usuario.getLogin(), "pdf",objectcode.get("negocio").getAsString(),nombArchivo,rutaDestino);
                    PdfReader reader = new PdfReader(rutaOrigenPdf+"/FORMATO_EXTRACTO.pdf");
                    PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(ruta));
                    PdfContentByte over = stamper.getOverContent(1);
                    ColumnText ct =new ColumnText(over);
                        
                    String cod_rop=objectcode.get("cod_rop").getAsString();
                    String generado_el=objectcode.get("generado_el").getAsString();
                    String nombre_cliente=objectcode.get("nombre_cliente").getAsString();
                    String direccion=objectcode.get("direccion").getAsString();
                    String barrio=objectcode.get("barrio").getAsString();
                    String ciudad=objectcode.get("ciudad").getAsString()+" - "+objectcode.get("departamento").getAsString();
                    String ciudad1=objectcode.get("ciudad").getAsString();
                    String agencia=objectcode.get("agencia").getAsString();
                    String linea_producto=objectcode.get("linea_producto").getAsString();
                    String negocio=objectcode.get("negocio").getAsString();
                    String establecimiento_comercio=objectcode.get("establecimiento_comercio").getAsString();
                    String msg_paguese_antes=objectcode.get("msg_paguese_antes").getAsString();
                    String msg_estado_credito=objectcode.get("msg_estado_credito").getAsString();
                    double tot_vlr_cuotas_vencidad=objectcode.get("ksubtotal_vencido").getAsDouble();
                    double desc_vlr_cuotas_vencidad=objectcode.get("kdescuentos").getAsDouble();
                    String dias_vencidos=objectcode.get("dias_vencidos").getAsString();
                    String cuotas_vencidas=objectcode.get("cuotas_vencidas").getAsString();
                    String fecha_ultimo_pago=objectcode.get("fch_ultimo_pago").getAsString();
                    String cuotas_pendientes=objectcode.get("cuotas_pendientes").getAsString();
                    double vr_cuota_mes=objectcode.get("ksubtotal_corriente").getAsDouble();
                    double seguro_cat=objectcode.get("seguro").getAsDouble();
                    double interes_xmora=objectcode.get("interes_xmora").getAsDouble();
                    double dscto_interes_xmora=objectcode.get("dscto_interes_xmora").getAsDouble();
                    double gastos_cobranza=objectcode.get("gastos_cobranza").getAsDouble();
                    double dscto_gastos_cobranza=objectcode.get("dscto_gastos_cobranza").getAsDouble();
                    double total_pagar_mes=objectcode.get("ksubtotalneto").getAsDouble();
                    String observacion=objectcode.get("observacion").getAsString();
                    String referencia_1=objectcode.get("referencia_1").getAsString();
                    String referencia_2=objectcode.get("referencia_2").getAsString();
                    String referencia_3=objectcode.get("referencia_3").getAsString();
                    String referencia_4=objectcode.get("referencia_4").getAsString();
                    
                    ct.setSimpleColumn(315,780,500,10);
                    com.itextpdf.text.Phrase phrase =new com.itextpdf.text.Phrase(cod_rop, fuente);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(80,725,500,10);           
                    phrase =new com.itextpdf.text.Phrase(generado_el, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(20,700,500,10);           
                    phrase =new com.itextpdf.text.Phrase(nombre_cliente, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(20,690,500,10);           
                    phrase =new com.itextpdf.text.Phrase(direccion, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(20,680,500,10);           
                    phrase =new com.itextpdf.text.Phrase(barrio, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(20,670,500,10);           
                    phrase =new com.itextpdf.text.Phrase(ciudad, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(245,716,500,10);           
                    phrase =new com.itextpdf.text.Phrase(agencia, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(286,703,500,10);           
                    phrase =new com.itextpdf.text.Phrase(linea_producto, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(270,690,500,10);           
                    phrase =new com.itextpdf.text.Phrase(negocio, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(200,666,500,10);           
                    phrase =new com.itextpdf.text.Phrase(establecimiento_comercio, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(60,609,500,10);           
                    phrase =new com.itextpdf.text.Phrase(msg_paguese_antes, fuente);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(250,578,500,10);           
                    phrase =new com.itextpdf.text.Phrase(msg_estado_credito, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(120,520,170,10);           
                    phrase =new com.itextpdf.text.Phrase("$"+formatea.format(tot_vlr_cuotas_vencidad), fuente2);
                    ct.setText(phrase);
                    ct.setAlignment(Element.ALIGN_RIGHT);
                    ct.go();
                    
                    ct.setSimpleColumn(215,520,170,10);           
                    phrase =new com.itextpdf.text.Phrase("$"+formatea.format(desc_vlr_cuotas_vencidad), fuente2);
                    ct.setText(phrase);
                    ct.setAlignment(Element.ALIGN_RIGHT);
                    ct.go();
                    
                    ct.setSimpleColumn(268,520,160,10);           
                    phrase =new com.itextpdf.text.Phrase(dias_vencidos, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(308,520,160,10);           
                    phrase =new com.itextpdf.text.Phrase(cuotas_vencidas, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(375,520,160,10);           
                    phrase =new com.itextpdf.text.Phrase(fecha_ultimo_pago, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(420,520,160,10);           
                    phrase =new com.itextpdf.text.Phrase(cuotas_pendientes, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(120,509,170,10);           
                    phrase =new com.itextpdf.text.Phrase("$"+formatea.format(vr_cuota_mes), fuente2);
                    ct.setText(phrase);
                    ct.setAlignment(Element.ALIGN_RIGHT);
                    ct.go();
                    
                    ct.setSimpleColumn(120,495,170,10);           
                    phrase =new com.itextpdf.text.Phrase("$"+formatea.format(seguro_cat), fuente2);
                    ct.setText(phrase);
                    ct.setAlignment(Element.ALIGN_RIGHT);
                    ct.go();
                    
                    ct.setSimpleColumn(120,482,170,10);           
                    phrase =new com.itextpdf.text.Phrase("$"+formatea.format(interes_xmora), fuente2);
                    ct.setText(phrase);
                    ct.setAlignment(Element.ALIGN_RIGHT);
                    ct.go();
                    
                    ct.setSimpleColumn(215,482,170,10);           
                    phrase =new com.itextpdf.text.Phrase("$"+formatea.format(dscto_interes_xmora), fuente2);
                    ct.setText(phrase);
                    ct.setAlignment(Element.ALIGN_RIGHT);
                    ct.go();
                    
                    ct.setSimpleColumn(120,469,170,10);           
                    phrase =new com.itextpdf.text.Phrase("$"+formatea.format(gastos_cobranza), fuente2);
                    ct.setText(phrase);
                    ct.setAlignment(Element.ALIGN_RIGHT);
                    ct.go();                    
                    
                    ct.setSimpleColumn(215,469,170,10);           
                    phrase =new com.itextpdf.text.Phrase("$"+formatea.format(dscto_gastos_cobranza), fuente2);
                    ct.setText(phrase);
                    ct.setAlignment(Element.ALIGN_RIGHT);
                    ct.go();
                    
                    ct.setSimpleColumn(120,454,170,10);           
                    phrase =new com.itextpdf.text.Phrase("$"+formatea.format(total_pagar_mes), fuente2);
                    ct.setText(phrase);
                    ct.setAlignment(Element.ALIGN_RIGHT);
                    ct.go(); 
                    
                    ct.setSimpleColumn(246,505,430,50);           
                    phrase =new com.itextpdf.text.Phrase(observacion, fuente3);
                    ct.setAlignment(Element.ALIGN_LEFT);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(60,82,500,10);           
                    phrase =new com.itextpdf.text.Phrase(nombre_cliente, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(70,65,500,10);           
                    phrase =new com.itextpdf.text.Phrase(direccion, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(60,47,500,10);           
                    phrase =new com.itextpdf.text.Phrase(barrio, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(210,47,500,10);           
                    phrase =new com.itextpdf.text.Phrase(ciudad1, fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
                    ct.setSimpleColumn(80,30,500,10);           
                    phrase =new com.itextpdf.text.Phrase("$"+formatea.format(total_pagar_mes), fuente2);
                    ct.setText(phrase);
                    ct.go();
                    
        
                    Barcode128 uccEan128 = new Barcode128();
                    uccEan128.setCodeType(Barcode.CODE128_UCC);
                    uccEan128.setCode("(415)" + referencia_1 + "(8020)" + referencia_2+ "(3900)" + referencia_3+ "(96)" + referencia_4);
                    uccEan128.setBarHeight(60f);
                    uccEan128.setSize(6f);
                    ct.setSimpleColumn(328,65,700,10);   
                    ct.addElement(uccEan128.createImageWithBarcode(over, BaseColor.BLACK, BaseColor.BLACK));
                    ct.go();

                    stamper.close();
                    reader.close(); 
                    String ruta_guardar="https://fintra.co/pago?id="+objectcode.get("id").getAsString();
                    String path="/"+objectcode.get("negocio").getAsString()+"/"+nombArchivo+".pdf";
                    guardarExtractoDigital(objectcode,ruta_guardar,usuario,path);
                     
                    
                    
                } catch (Exception ex) {
                    resp=ex.toString();
                    Logger.getLogger(AdminFintraImpl.class.getName()).log(Level.SEVERE, null, ex);
                    return resp;
                }
            }
            return "Extractos Generados";
        }
        
       
        return resp;

    }
    
     public JsonArray getInfoExtractos(String lote_generado ) {
        JsonArray  respJsonArray = null;
        JsonArray lista = null;
        JsonObject jsonObject = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
       String query = "GET_INFO_EXTRACTOS_POR_GENERAR";
            try {
                con = this.conectarJNDI();
                if (con != null) {
                query = this.obtenerSQL(query);
                ps = con.prepareStatement(query);
                ps.setString(1, lote_generado);
                rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                jsonObject = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                     switch(rs.getMetaData().getColumnTypeName(i)){
                         case "int4":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getInt(rs.getMetaData().getColumnLabel(i)));
                            break;    
                        case "bool":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getBoolean(rs.getMetaData().getColumnLabel(i)));
                            break;    
                        default:
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                   
                }
                lista.add(jsonObject);
            }
                
              respJsonArray = lista;
//                respJsonArray = Util.getQueryJsonArray(con, ps);
                }
          
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
     
        return respJsonArray;
    }

    
    public void guardarExtractoDigital(JsonObject objectcode, String ruta, Usuario usuario,String path) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_GUARDAR_CONTROL_EXTRACTO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, objectcode.get("id").getAsString());
            ps.setString(2, objectcode.get("negocio").getAsString());
            ps.setString(3, objectcode.get("cedula").getAsString());
            ps.setString(4, objectcode.get("celular").getAsString());
            ps.setString(5, objectcode.get("email").getAsString());
            ps.setString(6, ruta);
            ps.setString(7, usuario.getLogin());
            ps.setString(8, objectcode.get("lote").getAsString());
            ps.setString(9, objectcode.get("primer_nombre").getAsString());
            ps.setString(10, objectcode.get("ksubtotalneto").getAsString());
            ps.setString(11, objectcode.get("periodo").getAsString());
            ps.setString(12, path);
            ps.setString(13, objectcode.get("msg_paguese_antes").getAsString());
            int i=ps.executeUpdate();
            if(i>0){
                
         updateGeneradoExtracto(objectcode.get("id").getAsString(),objectcode.get("negocio").getAsString(),usuario.getLogin());
            }
            

        } catch (Exception ex) {
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
           
        }
    }
    
    
    public void updateGeneradoExtracto(String id_rop,String negocio, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_UPDATE_CONTROL_EXTRACTO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario);
            ps.setString(2, id_rop);
            ps.setString(3, negocio);
            ps.executeUpdate();

        } catch (Exception ex) {
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
        }
    }

    @Override
    public String cargarExtractosPorEnviarSms(String lote_generado) {
       JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_EXTRACTOS_POR_ENVIAR_SMS";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, lote_generado);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id_extracto", rs.getString("id_extracto"));              
                fila.addProperty("cod_negocio", rs.getString("cod_negocio"));
                fila.addProperty("cedula", rs.getString("cedula"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("valor_extracto", rs.getString("valor_extracto"));
                fila.addProperty("celular", rs.getString("celular"));
                fila.addProperty("email", rs.getString("email"));
                fila.addProperty("lote", rs.getString("lote"));
                datos.add(fila);
            }
            obj.add("rows", datos);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return gson.toJson(obj);
    }

    @Override
    public String cargarComboExtractosPorEnviarSms() {
         String respuesta ="{}";
       JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "CARGAR_COMBO_SMS_ENVIAR";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("lote"), rs.getString("lote"));
            }
           respuesta = gson.toJson(obj);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return respuesta;
    }

    public String getPatchExtractoDigital(String id_extracto) {
        String path="";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_PAHT_EXTRACTO_DIGITAL";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_extracto);
            rs = ps.executeQuery();
            if (rs.next()) {
              path= rs.getString("path_extracto");
              return path;
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return path;
    }
    
}
