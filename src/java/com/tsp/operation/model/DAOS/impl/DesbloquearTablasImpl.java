/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.tsp.operation.model.DAOS.DesbloquearTablasDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.DesbloquearTablasBeans;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mariana
 */
public class DesbloquearTablasImpl extends MainDAO implements DesbloquearTablasDAO {

    public DesbloquearTablasImpl(String dataBaseName) {
        super("DesbloquearTablasDAO.xml", dataBaseName);
    }

    @Override
    public ArrayList<DesbloquearTablasBeans> cargarTablas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOSTRAR_TABLAS";

        ArrayList<DesbloquearTablasBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                DesbloquearTablasBeans beanTabla = new DesbloquearTablasBeans();
                beanTabla.setNombre_tabla(rs.getString("nombre_tabla"));
                beanTabla.setNombre_campo(rs.getString("nombre_campo"));
                beanTabla.setEs_last_update(rs.getString("es_last_update"));
                beanTabla.setEs_fecha_envio(rs.getString("es_fecha_envio"));
                beanTabla.setEs_pk(rs.getString("es_pk"));
                beanTabla.setEs_fecha_anulacion(rs.getString("es_fecha_anulacion"));
                beanTabla.setCondicion(rs.getString("condicion"));
                lista.add(beanTabla);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public String updateMasivo() {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_UPDATE_MASIVO_TABLAS";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }

    }

}
