/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.tsp.operation.model.DAOS.AuditoriaIngresosDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.AuditoriaIngresos;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lcanchila
 */
public class AuditoriaIngresosImpl extends MainDAO implements AuditoriaIngresosDAO{

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    
    public AuditoriaIngresosImpl(String dataBaseName) {
        super("AuditoriaIngresos.xml", dataBaseName);
    }

    @Override
    public ArrayList<AuditoriaIngresos> cargarAuditoriaIngresos(int periodo , String user) {
        con = null;
        rs = null;
        String query = "cargarAuditoriaIngresos";
        String sql = "";
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();
        
        try{
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, periodo);
            ps.setInt(2, periodo);
            ps.setString(3, user);
                                                
            rs = ps.executeQuery();
            
            while(rs.next()){
                AuditoriaIngresos aud = new AuditoriaIngresos();
                aud.setPeriodo(rs.getString("periodo"));
                aud.setNegocio(rs.getString("negocio"));
                aud.setCedula(rs.getString("cedula"));
                aud.setNombre_cliente(rs.getString("nombre_cliente"));
                aud.setVencimiento_mayor(rs.getString("vencimiento_mayor"));
                aud.setFecha_pago_ingreso(rs.getString("fecha_pago_ingreso"));
                aud.setFecha_vencimiento_mayor(rs.getString("fecha_vencimiento_mayor"));
                aud.setDiferencia_pago(rs.getString("diferencia_pago"));
                aud.setValor_saldo_foto(rs.getDouble("valor_saldo_foto"));
                aud.setInteres_mora(rs.getDouble("interes_mora"));
                aud.setGasto_cobranza(rs.getDouble("gasto_cobranza"));
                aud.setNum_ingreso(rs.getString("num_ingreso"));
                aud.setValor_ingreso(rs.getDouble("valor_ingreso"));
                aud.setValor_cxc_ingreso(rs.getDouble("valor_cxc_ingreso"));
                aud.setG16252145(rs.getDouble("G16252145"));
                aud.setG94350302(rs.getDouble("G94350302"));
                aud.setGI010010014205(rs.getDouble("GI010010014205"));
                aud.setGI010130014205(rs.getDouble("GI010130014205"));
                aud.setI16252147(rs.getDouble("I16252147"));
                aud.setI94350301(rs.getDouble("I94350301"));
                aud.setII010010014170(rs.getDouble("II010010014170"));
                aud.setII010130024170(rs.getDouble("II010130024170"));
                aud.setI010140014170(rs.getDouble("I010140014170"));
                aud.setI010140014205(rs.getDouble("I010140014205"));     
                aud.setI28150530(rs.getDouble("I28150530")); 
                aud.setI28150531(rs.getDouble("I28150531"));
                aud.setValor_ixm_ingreso(rs.getDouble("valor_ixm_ingreso"));
                aud.setValor_gac_ingreso(rs.getDouble("valor_gac_ingreso"));
                aud.setConvenio(rs.getString("convenio"));
                
                lista.add(aud);
                
            }
        }catch(Exception e){
            try {
                throw new SQLException("ERROR cargarAuditoriaIngresos \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(AuditoriaIngresosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }
        return lista;
    }

    @Override
    public ArrayList<AuditoriaIngresos> cargarReporteCosechas(String periodoIni, String periodoFin, String[] listUnd) {
        con = null;
        rs = null;
        ps = null;
        StringStatement st = null;
        String query = "consultarDetalleCosecha2";
        String queryGral = "cargarReporteCosechasGral";
        String sql = "";
        String sql2 = "";
        String sqlGlobal = "";
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();

        try {
            con = conectarJNDI(query);
            queryGral = this.obtenerSQL(queryGral);
            for (int i = 0; i < listUnd.length; i++) {
                sql2 = this.obtenerSQL(query);
                if(i > 0){
                    sql2 = sql2.replaceAll("#reemplazar#", "union");
                }else{
                    sql2 = sql2.replaceAll("#reemplazar#", "");
                }
                st = new StringStatement(sql2, false);
                st.setInt(Integer.parseInt(periodoIni));
                st.setInt(Integer.parseInt(periodoFin));
                st.setString(listUnd[i]);
                sql += st.getSql();
             }
            queryGral = queryGral.replace("--#reemplazar#", sql);
            ps = con.prepareStatement(queryGral);
            rs = ps.executeQuery();

            while (rs.next()) {
                AuditoriaIngresos aud = new AuditoriaIngresos();
                aud.setVencimiento_mayor(rs.getString("vencimiento_mayor"));
                aud.setValor_asignado(rs.getDouble("colocacion"));
                aud.setCantidad(rs.getInt("cantidades"));
                aud.setValor_pagos(rs.getDouble("pagos"));
                aud.setValor_saldo_foto(rs.getDouble("saldo"));
                aud.setPorc_item(rs.getDouble("perc_item"));

                 lista.add(aud);
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR cargarReporteCosechas \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(AuditoriaIngresosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }
    
    public ArrayList<CmbGeneralScBeans> cargarPeriodo() throws Exception {
        ArrayList<CmbGeneralScBeans> listcmb = new ArrayList<CmbGeneralScBeans>();
        int periodoActual = Integer.parseInt(Util.getPeriodo(1));
        int perFin = Integer.parseInt(String.valueOf(periodoActual).substring(0, 4));
        int periodoIni = 2012;
        String periodo = "";
        for (int i = periodoIni; i <= perFin; i++) {
            periodo = String.valueOf(i).substring(0, 4);
            int ini = 1;
            int fin = 12;
            if (i == perFin) {
                fin = Integer.parseInt(String.valueOf(periodoActual).substring(4, 6));
            }
            while (ini <= fin) {
                CmbGeneralScBeans cmb = new CmbGeneralScBeans();
                if (ini <= 9) {
                    cmb.setIdCmb(Integer.parseInt(periodo + "0" + ini));
                    cmb.setDescripcionCmb(periodo + "0" + ini);
                } else {
                    cmb.setIdCmb(Integer.parseInt(periodo + ini));
                    cmb.setDescripcionCmb(periodo + ini);
                }
                ini++;
                listcmb.add(cmb);
            }
        }
        return listcmb;
    }

    @Override
    public ArrayList<AuditoriaIngresos> consultarDetalleCosecha(String periodoIni, String periodoFin, String und_negocio, String venc_mayor) {
        con = null;
        rs = null;
        String query = "consultarDetalleCosecha";
        String sql = "";
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();
        
        try{
            sql = this.obtenerSQL(query);
            if(!venc_mayor.equals("TOTAL")){
                sql = sql + " where vencimiento_mayor = ? order by unidad_negocio,vencimiento_mayor,fecha_aprobacion";
            }
            else {
                sql = sql + " order by unidad_negocio,vencimiento_mayor,fecha_aprobacion";
            }
            
            
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(periodoIni));
            ps.setInt(2, Integer.parseInt(periodoFin));
            ps.setString(3, und_negocio);
            if(!venc_mayor.equals("TOTAL")){
                ps.setString(4, venc_mayor);
            }                      
            rs = ps.executeQuery();
            
            while(rs.next()){
                AuditoriaIngresos aud = new AuditoriaIngresos();
                aud.setCedula(rs.getString("cedula"));
                aud.setNombre_cliente(rs.getString("nombre"));
                aud.setUnidad_negocio(rs.getString("unidad_negocio"));
                aud.setNegocio(rs.getString("negocio"));
                aud.setAfiliado(rs.getString("afiliado"));
                aud.setFecha_aprobacion(rs.getString("fecha_aprobacion"));
                aud.setFecha_desembolso(rs.getString("fecha_desembolso"));
                aud.setPeriodo(rs.getString("periodo_desembolso"));
                aud.setTotal_desembolsado(rs.getString("total_desembolsado"));
                aud.setPlazo(rs.getString("plazo"));
                aud.setCuota(rs.getString("cuota"));
                aud.setCuotas_vencidas(rs.getString("cuotas_vencidas"));
                aud.setAnalista(rs.getString("analista"));
                aud.setAsesor_comercial(rs.getString("asesor_comercial"));
                aud.setCobrador_telefonico(rs.getString("cobrador_telefonico"));
                aud.setCobrador_campo(rs.getString("cobrador_campo"));
                aud.setFecha_ultimo_pago(rs.getString("fecha_ultimo_pago"));
                aud.setVencimiento_mayor(rs.getString("vencimiento_mayor"));
                aud.setVencimiento_mayor_maximo(rs.getString("vencimiento_mayor_maximo"));
                aud.setTramo_anterior(rs.getString("tramo_anterior"));
                aud.setFecha_vencimiento_mayor(rs.getString("fecha_vencimiento"));
                aud.setDireccion(rs.getString("direccion"));
                aud.setTelefono(rs.getString("telefono"));
                aud.setCelular(rs.getString("celular"));
                aud.setEmail(rs.getString("email"));
                aud.setEstrato(rs.getString("estrato"));
                aud.setOcupacion(rs.getString("ocupacion"));
                aud.setDepartamento(rs.getString("departamento"));
                aud.setMunicipio(rs.getString("municipio"));
                aud.setBarrio(rs.getString("barrio"));
                aud.setNombre_empresa(rs.getString("nombre_empresa"));
                aud.setCargo(rs.getString("cargo"));
                aud.setColocacion(rs.getDouble("colocacion"));
                aud.setValor_pagos(rs.getDouble("pagos"));
                aud.setSaldo(rs.getDouble("saldo"));
                aud.setSaldo_porvencer(rs.getDouble("saldo_porvencer"));
                
                lista.add(aud);
                
            }
        }catch(Exception e){
            try {
                throw new SQLException("ERROR cargarReporteCosechas \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(AuditoriaIngresosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }
        return lista;
    }

    @Override
    public ArrayList<AuditoriaIngresos> cargarCuentasCaidas(String periodo, String und_negocio) {
        con = null;
        rs = null;
        ps = null;
        StringStatement st = null;
        String query = "cargarCuentasCaidas";
        String sql = "";
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();

        try {
            sql = this.obtenerSQL(query);
            
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setString(2, und_negocio);
            
            rs = ps.executeQuery();

            while (rs.next()) {
                AuditoriaIngresos aud = new AuditoriaIngresos();
                aud.setVencimiento_mayor(rs.getString("venc_mayor_ant"));
                aud.setCuentas_debido(rs.getInt("cuentas_debido"));
                aud.setCuentas_caidas(rs.getInt("cuentas_caidas"));
                aud.setPorc_item(rs.getDouble("porcentaje"));
                
                lista.add(aud);
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR cargarCuentasCaidas \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(AuditoriaIngresosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public ArrayList<AuditoriaIngresos> detalleCuentasCaidas(String periodo, String und_negocio, String venc_mayor) {
        con = null;
        rs = null;
        ps = null;
        StringStatement st = null;
        String query = "detalleCuentasCaidas";
        String sql = "";
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();

        try {
            sql = this.obtenerSQL(query);
            
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setInt(2, Integer.parseInt(und_negocio));
            ps.setString(3, venc_mayor);
            
            rs = ps.executeQuery();

            while (rs.next()) {
                AuditoriaIngresos aud = new AuditoriaIngresos();
                aud.setCedula(rs.getString("cedula"));
                aud.setNombre_cliente(rs.getString("nombre"));
                aud.setNegocio(rs.getString("negocio"));
                aud.setUnidad_negocio(rs.getString("und_negocio"));
                aud.setConvenio(rs.getString("id_convenio"));
                aud.setCartera_con_sancion(rs.getDouble("cartera_sancion"));
                aud.setValor_debido(rs.getDouble("valor_debido"));
                aud.setValor_cuota(rs.getDouble("valor_cuota"));
                aud.setValor_recaudo(rs.getDouble("valor_recaudo"));
                aud.setValor_recaudo_debido(rs.getDouble("recaudo_aplicado"));
                aud.setPorc_item(rs.getDouble("porcentaje"));
                                
                lista.add(aud);
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR detalleCuentasCaidas \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(AuditoriaIngresosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }
    
}