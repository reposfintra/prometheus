/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.MenuBootstrapDAO;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class MenuBootstrapImpl extends MainDAO implements MenuBootstrapDAO {

    public MenuBootstrapImpl(String dataBaseName) {
        super("MenuBootstrapDAO.xml", dataBaseName);
    }

    @Override
    public ArrayList<MenuOpcionesModulos> cargarMenuOpciones(Usuario usuario, String modulo) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_OPCIONES_MENU";
        ArrayList<MenuOpcionesModulos> listaOpcionesMenu = null;

        try {
            String sql = "";
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, "%" + usuario.getLogin() + "%");
            ps.setString(2, usuario.getDstrct());
            ps.setString(3, modulo);
            rs = ps.executeQuery();
            listaOpcionesMenu = new ArrayList();
            MenuOpcionesModulos opciones_menu = null;

            while (rs.next()) {

                opciones_menu = new MenuOpcionesModulos();
                opciones_menu.setId(rs.getInt("id"));
                opciones_menu.setDescripcion(rs.getString("descripcion"));
                opciones_menu.setRuta(rs.getString("ruta"));
                opciones_menu.setOrden(rs.getInt("orden"));
                opciones_menu.setPadre(rs.getInt("padre"));
                opciones_menu.setNivel(rs.getInt("nivel"));
                opciones_menu.setModulo(rs.getString("modulo"));
                listaOpcionesMenu.add(opciones_menu);

            }

        } catch (SQLException ex) {
            throw new SQLException("ERROR OBTENIENDO MENU OPCIONES: cargarMenuOpciones()  " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return listaOpcionesMenu;

    }

}
