/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.Auto_ScoringDAO;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.Unidad_Negocio;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Proveedor;
import com.tsp.operation.model.beans.ValorPredeterminadoUn;
import com.tsp.opav.model.beans.UnidadMedida;
import com.tsp.operation.model.beans.variable_mercado;
import com.tsp.operation.model.TransaccionService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fnunez
 */
public class Auto_ScoringImpl extends MainDAO implements Auto_ScoringDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;

    public Auto_ScoringImpl(String dataBaseName) {
        super("Auto_ScoringDAO.xml", dataBaseName);

    }

    public ArrayList<String> cargarNom_subCat(String insumo) {

        con = null;
        rs = null;
        ps = null;
        ArrayList<String> lista = new ArrayList<String>();
        query = "cargarnomsubcat";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            //ps.setInt(1, Integer.parseInt(insumo));
            rs = ps.executeQuery();

            while (rs.next()) {

                lista.add(rs.getString("nombre"));

            }
        } catch (Exception e) {
            try {
                throw new Exception("Error al Cargar las Sub Categorias " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    public ArrayList<UnidadMedida> cargarunidadmedida(String empresa, String status) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<UnidadMedida> lista = new ArrayList<UnidadMedida>();
        query = "cargarunidadmedida";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                UnidadMedida und = new UnidadMedida();
                und.setIdunidadmedida(rs.getInt("idunidadmedicion"));
                und.setNombre(rs.getString("nombre"));
                lista.add(und);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosMeta " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    public ArrayList<ValorPredeterminadoUn> listar_valorespredeterminados(String empresa, String status) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<ValorPredeterminadoUn> lista = new ArrayList<ValorPredeterminadoUn>();
        query = "listarvalorespredeterminados";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            //ps.setString(2, status);
            rs = ps.executeQuery();

            while (rs.next()) {
                ValorPredeterminadoUn itemsel = new ValorPredeterminadoUn();
                itemsel.setId(rs.getInt("id"));
                itemsel.setValor_xdefecto(rs.getString("descripcion"));
                itemsel.setInicio(rs.getString("inicio"));
                itemsel.setFin(rs.getString("fin"));
                lista.add(itemsel);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosMeta " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }
    
    public ArrayList<ValorPredeterminadoUn> listar_valorespredeterminados2(String empresa, String status) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<ValorPredeterminadoUn> lista = new ArrayList<ValorPredeterminadoUn>();
        query = "listarvalorespredeterminados2";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            //ps.setString(2, status);
            rs = ps.executeQuery();

            while (rs.next()) {
                ValorPredeterminadoUn itemsel = new ValorPredeterminadoUn();
                itemsel.setId(rs.getInt("id"));
                itemsel.setValor_xdefecto(rs.getString("descripcion"));
                itemsel.setInicio(rs.getString("unico"));
                lista.add(itemsel);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosMeta " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    public ArrayList<ValorPredeterminadoUn> listar_valorespredeterminados_filtro(String empresa, String status, String id, String ids) {
        con = null;
        rs = null;
        ps = null;

        ArrayList<ValorPredeterminadoUn> lista = new ArrayList<ValorPredeterminadoUn>();
        query = "listarvalorespredeterminados_filtro";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ids = "'" + ids + "'";
            //ps.setString(1, ids.replace(",", "','"));
            query = query.replace("?1", ids.replace(",", "','"));
            String filtro = "";
            String inner = "";

            if (!id.equals("0") && !id.equals("")) {

                inner = "inner join opav.sl_conf_predeterminados b on(b.id_valores_predeterminados=a.id)";

                filtro = "and b.id_especificacion=" + id;

            }

            if (!ids.equals("''")) {
                filtro = filtro + " and a.id not in(" + ids.replace(",", "','") + ")";

            }

            query = query.replace("inner", inner);
            query = query.replace("filtro", filtro);

            ps = con.prepareStatement(query);
            //ps.setString(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                ValorPredeterminadoUn itemsel = new ValorPredeterminadoUn();
                itemsel.setId(rs.getInt("id"));
                itemsel.setValor_xdefecto(rs.getString("Valor_xdefecto"));
                itemsel.setDescripcion(rs.getString("descripcion"));
                lista.add(itemsel);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosMeta " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    public ArrayList<Proveedor> cargarproveedores(String empresa, String status) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Proveedor> lista = new ArrayList<Proveedor>();
        query = "cargarProveedores";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            //ps.setString(2, status);
            rs = ps.executeQuery();

            while (rs.next()) {
                Proveedor prov = new Proveedor();
                prov.setC_payment_name(rs.getString("nombre"));
                prov.setC_nit(rs.getString("nit"));
                lista.add(prov);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosMeta " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public String cargarCategorias() {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        JsonObject obj = new JsonObject();
        String query = "buscarCategorias";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
//            ps.setString(1, multiservicio);
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("value", rs.getString("id"));
                fila.addProperty("label", rs.getString("nombreComp"));
                arr.add(fila);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(arr);
        }
    }

    @Override
    public ArrayList<Unidad_Negocio> cargarProcesosMeta(String empresa, String status, String insumo) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Unidad_Negocio> lista = new ArrayList<Unidad_Negocio>();
        query = "cargarProcesosMeta";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            //ps.setInt(1, Integer.parseInt(insumo));
            rs = ps.executeQuery();

            while (rs.next()) {
                Unidad_Negocio pro = new Unidad_Negocio();
                pro.setId(rs.getInt("id"));
                pro.setNombre(rs.getString("descripcion"));
                pro.setDescripcion(rs.getString("descripcion"));
                lista.add(pro);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosMeta " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public ArrayList<CmbGeneralScBeans> cargarComboEmpresa() {
        con = null;
        rs = null;
        ps = null;
        ArrayList<CmbGeneralScBeans> lista = new ArrayList<CmbGeneralScBeans>();
        query = "cargarComboEmpresa";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                CmbGeneralScBeans cbo = new CmbGeneralScBeans();
                cbo.setIdCmb(rs.getInt("id"));
                cbo.setDescripcionCmb(rs.getString("descripcion"));

                lista.add(cbo);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarComboEmpresa " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public boolean existeMetaProceso(String empresa, String descripcion) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "existeMetaProceso";
        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);;

            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setString(2, descripcion);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeMetaProceso " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public boolean existeEspecificacion(String empresa, String descripcion) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "existeEspecificacion";
        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);;

            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setString(2, descripcion);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeMetaProceso " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String guardarMetaProceso(String insumo, String empresa, String nombre, String descripcion, String usuario) {
        con = null;
        ps = null;
        query = "guardarMetaProceso";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(insumo));
            ps.setString(2, empresa);
            ps.setString(3, nombre);
            ps.setString(4, descripcion);
            ps.setString(5, usuario);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String guardarEspecificacion(String empresa, String nombre, String descripcion, String usuario, String idtipo) {
        con = null;
        ps = null;
        query = "guardarEspecificacion";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            int resp = 0, idProInterno = 0;
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, empresa);
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario);
            ps.setString(5, idtipo);
            resp = ps.executeUpdate();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idProInterno = rs.getInt(1);
                }
            }
            respuestaJson = "{\"respuesta\":\"OK\",\"idProceso\":\"" + idProInterno + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarEspecificacion \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarMetaProceso(String empresa, String nombre, String descripcion, String idProceso, String usuario) {
        con = null;
        ps = null;
        query = "actualizarMetaProceso";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario);
            ps.setInt(5, Integer.parseInt(idProceso));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarComboProcesoMeta(String empresa) {
        con = null;
        rs = null;
        ps = null;
        query = "cargarComboProcesoMeta";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            //ps.setString(1, empresa);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public ArrayList<Unidad_Negocio> cargarProcesoInterno() {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Unidad_Negocio> lista = new ArrayList<Unidad_Negocio>();
        query = "cargarProcesoInterno";
        String filtro = " pin.reg_status !='A'";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                Unidad_Negocio pro = new Unidad_Negocio();
                pro.setId(rs.getInt("id"));
                pro.setNombre(rs.getString("nombre"));
                pro.setDescripcion(rs.getString("descripcion"));
                pro.setId_tabla_rel(rs.getInt("id_proceso_meta"));
                pro.setDescripcionTablaRel(rs.getString("meta_proceso"));

                lista.add(pro);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesoInterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public boolean existeProcesoInterno(String procesoMeta, String descripcion) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "existeProcesoInterno";
        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(procesoMeta));
            ps.setString(2, descripcion);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeProcesoInterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public boolean existeSubcategoria(String descripcion, String dstrct) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "obtenerIdSubcategoria";
        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            ps.setString(1, descripcion);
            ps.setString(2, dstrct);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeProcesoInterno " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public boolean existeRelCatSubcategoria(int idsub, String idcat) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "obtenerIdRelSubCat";
        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            ps.setString(1, idcat);
            ps.setInt(2, idsub);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeRelCatSubcategoria " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    public String concatespe(JsonObject info, String id, String nomsub) {
        String cadena = nomsub;
        JsonObject respuesta = new JsonObject();
        JsonArray arr = info.getAsJsonArray("rows");
        JsonArray columnas = info.getAsJsonArray("arrColmodel");
        JsonArray titulos = info.getAsJsonArray("arrColnames");
        JsonObject recorre = new JsonObject();

        for (int i = 0; i < arr.size(); i++) {
            respuesta = arr.get(i).getAsJsonObject();

            if (respuesta.get("id").getAsString().startsWith(id)) {

                String nombre = "";
                String nombre2 = "";
                String valor = "";
                String coma = "";

                int var = 0;
                for (int d = 0; d < titulos.size(); d++) {

                    String recorre1 = titulos.get(d).toString();

                    if (recorre1.contains("tipo-")) {
                        var = 1;
                    } else if (var == 1) {
                        nombre2 = "esp-" + recorre1.replace(" ", "");
                        nombre2 = nombre2.replace("\"", "");
                        valor = respuesta.get(nombre2).getAsString();
                        cadena = cadena + " " + valor;
                        coma = ",";
                        var = 0;
                    }

                }

            }

        }

        return cadena;
    }

    public boolean verificaExisteMat(int subcategoria, String concat) {
        int resp = 0;
        boolean ret = false;
        Connection con1 = null;
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        String query1 = "";

        try {
            con1 = this.conectarJNDI("ConsultaMat");
            query1 = this.obtenerSQL("ConsultaMat");
            ps1 = con1.prepareStatement(query1);

            ps1.setInt(1, subcategoria);
            ps1.setString(2, concat);

            rs1 = ps1.executeQuery();

            while (rs1.next()) {
                resp = rs1.getInt("no");
            }

            if (resp > 0) {

                ret = true;

            }

        } catch (SQLException ex) {
            Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ret;
    }

    @Override
    public JsonObject modificar_esp(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        JsonObject recorre = new JsonObject();
        StringStatement ss = null;
        con = null;
        ps = null;
        String mensaje = "";
        //query = "GuardarEncabezadoInsumo";
        TransaccionService tsrv = null;
        int resp = 0, idinsumo = 0;
        int idCab = 0;

        try {

            JsonArray arr = info.getAsJsonArray("rows");
            JsonArray columnas = info.getAsJsonArray("arrColmodel");
            Connection con = null;
            PreparedStatement ps = null;
            con = this.conectarJNDI();

            for (int i = 0; i < arr.size(); i++) {
                respuesta = arr.get(i).getAsJsonObject();

                String concat = concatespe(info, respuesta.get("id").getAsString(), info.get("nomsub").getAsString());

                if (respuesta.get("id").getAsString().startsWith("neo_")) {

                    if (verificaExisteMat(info.get("sub").getAsInt(), concat)) {
                        mensaje = "Algunos Registros se Encuentran Duplicados...";
                        break;
                    }

                    String query = "GuardarEncabezadoInsumo";
                    con.setAutoCommit(false);

                    ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);

                    ps.setInt(1, info.get("sub").getAsInt());
                    ps.setString(2, concat);
                    ps.setString(3, info.get("sub").getAsString());
                    ps.setString(4, info.get("dstrct").getAsString());
                    ps.setString(5, info.get("usuario").getAsString());

                    resp = ps.executeUpdate();
                    if (resp > 0) {
                        ResultSet rs = ps.getGeneratedKeys();
                        if (rs.next()) {
                            idCab = rs.getInt(1);
                            ps = con.prepareStatement(insertarInsumoDetalle(idCab, respuesta, columnas, info.get("usuario").getAsString(), info.get("dstrct").getAsString(), info));
                            ps.executeUpdate();
                        }
                    }
                    mensaje = "Edicion satisfactoria";
                    con.commit();

                } else {

                    String query = "ActualizarDescEncabezadoInsumo";
                    con.setAutoCommit(false);

                    ps = con.prepareStatement(this.obtenerSQL(query));

                    ps.setString(1, concat);
                    ps.setInt(2, respuesta.get("id").getAsInt());

                    ps.executeUpdate();

                    ps = con.prepareStatement(updateInsumoDetalle(respuesta.get("id").getAsInt(), respuesta, columnas, info.get("usuario").getAsString(), info.get("dstrct").getAsString(), info));
                    ps.executeUpdate();

                    mensaje = "Edicion satisfactoria";
                    con.commit();

                }

            }

            respuesta = new JsonObject();

            respuesta.addProperty(
                    "mensaje", mensaje);

        } catch (Exception exc) {
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject modificar_espVal(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        StringStatement st = null;
        con = null;
        ps = null;
        String mensaje = "";
        TransaccionService tsrv = null;
        int resp = 0;
        String sql = "";

        try {

            JsonArray arr = info.getAsJsonArray("rows");
            Connection con = null;
            PreparedStatement ps = null;
            con = this.conectarJNDI();

            String query = "guardarEspecificacion";
            con.setAutoCommit(false);

            ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, info.get("dstrct").getAsString());
            ps.setString(2, info.get("nomesp").getAsString());
            ps.setString(3, info.get("nomesp").getAsString());
            ps.setString(4, info.get("usuario").getAsString());
            ps.setString(5, info.get("t_entrada").getAsString());

            resp = ps.executeUpdate();

            if (resp > 0 && arr.size() > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    int idCab = rs.getInt(1);

                    for (int i = 0; i < arr.size(); i++) {
                        respuesta = arr.get(i).getAsJsonObject();

                        sql = this.obtenerSQL("guardarValPred");
                        st = new StringStatement(sql, true);

                        st.setInt(1, idCab);
                        st.setInt(2, respuesta.get("id").getAsInt());
                        st.setString(3, info.get("dstrct").getAsString());
                        st.setString(4, info.get("usuario").getAsString());

                        ps = con.prepareStatement(st.getSql());
                        ps.executeUpdate();
                    }
                }

            }
            mensaje = "Edicion satisfactoria";
            con.commit();
            respuesta = new JsonObject();

            respuesta.addProperty(
                    "mensaje", mensaje);

        } catch (Exception exc) {
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject modificar_Val(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        StringStatement ss = null;
        con = null;
        ps = null;
        String mensaje = "";
        //query = "GuardarEncabezadoInsumo";

        try {

            JsonArray arr = info.getAsJsonArray("rows");
            Connection con = null;
            PreparedStatement ps = null;
            con = this.conectarJNDI();

            String query = "";
            con.setAutoCommit(false);

            for (int i = 0; i < arr.size(); i++) {
                respuesta = arr.get(i).getAsJsonObject();

                //if (respuesta.get("id").getAsString().startsWith("neo_")) {

                    if (info.get("op").getAsString().equals("valorespredeterminados")) {
                        query=(respuesta.get("id").getAsString().startsWith("neo_")) ? "guardarValorPredeterminado1": "actualizarValorPredeterminado2" ;
                        ps = con.prepareStatement(this.obtenerSQL(query));

                        ps.setString(1, info.get("dstrct").getAsString());
                        ps.setString(2, respuesta.get("valor_xdefecto").getAsString());
                        ps.setInt(3, respuesta.get("inicio").getAsInt());
                        ps.setInt(4, respuesta.get("fin").getAsInt());
                        ps.setString(5, info.get("usuario").getAsString());
                        if (query.equals("actualizarValorPredeterminado1")) ps.setString(6, respuesta.get("id").getAsString());
                        
                        ps.executeUpdate();
                        mensaje = "Edicion satisfactoria";

                    }else{
                         query=(respuesta.get("id").getAsString().startsWith("neo_")) ? "guardarValorPredeterminado2": "actualizarValorPredeterminado2" ;
                         ps = con.prepareStatement(this.obtenerSQL(query));

                        ps.setString(1, info.get("dstrct").getAsString());
                        ps.setString(2, respuesta.get("valor_xdefecto").getAsString());
                        ps.setString(3, respuesta.get("inicio").getAsString());
                        ps.setString(4, info.get("usuario").getAsString());
                        if (query.equals("actualizarValorPredeterminado2")) ps.setString(5, respuesta.get("id").getAsString());

                        ps.executeUpdate();
                        mensaje = "Edicion satisfactoria";
                    }

               // }

            }
            con.commit();

            respuesta = new JsonObject();

            respuesta.addProperty("mensaje", mensaje);

        } catch (Exception exc) {
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, ha ocurrido un error al guardar los valores");
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    public String verificaExisteValPre(String idValPre) {
        int resp = 0;
        String ret = "";
        Connection con1 = null;
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        String query1 = "";
        String mensaje = "";
        String coma = "";

        try {
            con1 = this.conectarJNDI("VerifExisteValPre");
            query1 = this.obtenerSQL("VerifExisteValPre");
            ps1 = con1.prepareStatement(query1);

            ps1.setString(1, idValPre);

            rs1 = ps1.executeQuery();

            while (rs1.next()) {
                ret = ret + coma + rs1.getString("nombre");
                coma = ", ";
                resp = resp + 1;
            }

            if (resp > 0) {
                mensaje = "El Valor Predeterminado " + idValPre + " se encuentra creado";
            }

            if (!ret.equals("")) {

                mensaje = mensaje + " En las Especificaciones: " + ret;

            }

        } catch (SQLException ex) {
            Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps1 != null) {
                try {
                    ps1.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (con1 != null) {

                try {
                    this.desconectar(con1);
                } catch (SQLException ex) {
                    Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            return mensaje;
        }
    }

    public String updateInsumoDetalle(int idCab, JsonObject respuesta, JsonArray columnas, String usuario, String empresa, JsonObject info) throws SQLException {

        String cadena = "";
        String query = "ActualizarDetalleInsumo";
        String sql = "";
        StringStatement st = null;
        JsonObject recorre = new JsonObject();
        String nombre;
        String nombre2;
        String valor;
        boolean booleano = true;
        int conteo = 0;
        int conteo1 = 0;
        int var = 0;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setInt(3, idCab);

            for (int c = 0; c < columnas.size(); c++) {
                recorre = columnas.get(c).getAsJsonObject();

                if (recorre.get("name").toString().contains("tipo-0")) {

                    booleano = true;
                    var = 1;

                } else if (recorre.get("name").toString().contains("tipo-1") || recorre.get("name").toString().contains("tipo-2") || recorre.get("name").toString().contains("tipo-3")
                        || recorre.get("name").toString().contains("tipo-4")) {

                    booleano = false;
                    var = 1;

                }

                if (var == 1) {

                    if (booleano) {

                        if (recorre.get("name").getAsString().startsWith("esp-")) {

                            nombre = recorre.get("name").getAsString().substring(4);
                            nombre2 = "esp-" + nombre;
                            valor = respuesta.get(nombre2).getAsString();
                            st.setString(2, valor);

                        } else if (recorre.get("name").getAsString().startsWith("espidval-")) {

                            nombre = recorre.get("name").getAsString().substring(9);
                            nombre2 = "espidval-" + nombre;
                            valor = respuesta.get(nombre2).getAsString();
                            st.setInt(1, Integer.parseInt(valor));

                        } else if (recorre.get("name").getAsString().startsWith("espid-")) {

                            nombre = recorre.get("name").getAsString().substring(6);
                            valor = nombre;
                            st.setInt(4, Integer.parseInt(valor));
                            conteo = conteo + 1;

                        }

                        if (conteo != conteo1) {

                            cadena += st.getSql();
                            st = null;
                            conteo1 = conteo1 + 1;
                            st = new StringStatement(sql, true);
                            st.setInt(3, idCab);

                        }

                    } else {

                        if (recorre.get("name").getAsString().startsWith("esp-")) {

                            nombre = recorre.get("name").getAsString().substring(4);
                            nombre2 = "esp-" + nombre;
                            valor = respuesta.get(nombre2).getAsString();
                            st.setString(2, valor);

                        } else if (recorre.get("name").getAsString().startsWith("espid-")) {

                            nombre = recorre.get("name").getAsString().substring(6);
                            valor = nombre;
                            st.setInt(4, Integer.parseInt(valor));

                            st.setInt(1, 0);

                            cadena += st.getSql();
                            st = null;
                            st = new StringStatement(sql, true);
                            st.setInt(3, idCab);

                        }

                    }

                }

            }

        } catch (Exception e) {
            System.out.println("Error en SQL_INSERTAR_DETALLE_RECAUDO (insertarRecaudosDetalle)" + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }

    ////////////////////////
    public String insertarInsumoDetalle(int idCab, JsonObject respuesta, JsonArray columnas, String usuario, String empresa, JsonObject info) throws SQLException {

        String cadena = "";
        String query = "GuardarDetalleInsumo";
        String sql = "";
        StringStatement st = null;
        JsonObject recorre = new JsonObject();
        String nombre;
        String nombre2;
        String valor = "";
        String valor1 = "";
        boolean booleano = true;
        int conteo = 0;
        int conteo1 = 0;
        int var = 0;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setString(1, info.get("dstrct").getAsString());
            st.setInt(3, idCab);
            st.setString(6, info.get("usuario").getAsString());

            for (int c = 0; c < columnas.size(); c++) {
                recorre = columnas.get(c).getAsJsonObject();

                if (recorre.get("name").toString().contains("tipo-0")) {

                    booleano = true;
                    var = 1;

                } else if (recorre.get("name").toString().contains("tipo-1") || recorre.get("name").toString().contains("tipo-2") || recorre.get("name").toString().contains("tipo-3")
                        || recorre.get("name").toString().contains("tipo-4")) {

                    booleano = false;
                    var = 1;

                }

                if (var == 1) {

                    if (booleano) {

                        if (recorre.get("name").getAsString().startsWith("esp-")) {

                            nombre = recorre.get("name").getAsString().substring(4);
                            nombre2 = "esp-" + nombre;
                            valor = respuesta.get(nombre2).getAsString();
                            if (!valor.equals("...")) {
                                st.setString(5, valor);
                            }

                        } else if (recorre.get("name").getAsString().startsWith("espidval-")) {

                            nombre = recorre.get("name").getAsString().substring(9);
                            nombre2 = "espidval-" + nombre;
                            valor = respuesta.get(nombre2).getAsString();
                            if (!valor.equals("")) {
                                st.setInt(2, Integer.parseInt(valor));
                            }

                        } else if (recorre.get("name").getAsString().startsWith("espid-")) {

                            nombre = recorre.get("name").getAsString().substring(6);
                            valor1 = nombre;
                            if (!valor.equals("")) {
                                st.setInt(4, Integer.parseInt(valor1));
                                conteo = conteo + 1;
                            }

                        } else { //if (recorre.get("name").getAsString().equals("precio_compra")) {

                            //valor = respuesta.get("precio_compra").getAsString();
                        }

                        if (conteo != conteo1) {

                            cadena += st.getSql();
                            st = null;
                            conteo1 = conteo1 + 1;
                            st = new StringStatement(sql, true);
                            st.setString(1, info.get("dstrct").getAsString());
                            st.setInt(3, idCab);
                            st.setString(6, info.get("usuario").getAsString());
                        }

                    } else {

                        if (recorre.get("name").getAsString().startsWith("esp-")) {

                            nombre = recorre.get("name").getAsString().substring(4);
                            nombre2 = "esp-" + nombre;
                            valor = respuesta.get(nombre2).getAsString();
                            st.setString(5, valor);
                        } else if (recorre.get("name").getAsString().startsWith("espid-")) {

                            nombre = recorre.get("name").getAsString().substring(6);
                            valor = nombre;
                            st.setInt(4, Integer.parseInt(valor));

                            st.setInt(2, 0);
                            cadena += st.getSql();
                            st = null;
                            st = new StringStatement(sql, true);
                            st.setString(1, info.get("dstrct").getAsString());
                            st.setInt(3, idCab);
                            st.setString(6, info.get("usuario").getAsString());

                        }
                    }

                }

            }
            /*
             double valor_recaudado = 0;
             Iterator<RecaudoAsobancariaDetalle> itrDetalles = listDetalle.iterator();
             while (itrDetalles.hasNext()) {
             RecaudoAsobancariaDetalle detalle = itrDetalles.next();
             sql = this.obtenerSQL(query);
             st = new StringStatement(sql, true);
             st.setInt(1, idRecaudo);
             st.setString(2, detalle.getCod_servicio_rec());
             st.setInt(3, detalle.getNumero_lote());
             st.setString(4, detalle.getReferencia_factura());
             valor_recaudado = (detalle.getCod_servicio_rec().equals("")) ? detalle.getValor_recaudado() : detalle.getValor_recaudado() / 100;
             st.setDouble(5, valor_recaudado);
             st.setString(6, detalle.getProcedencia_pago());
             st.setString(7, detalle.getMedio_pago());
             st.setString(8, detalle.getNum_operacion());
             st.setString(9, detalle.getNum_autorizacion());
             st.setInt(10, detalle.getCod_entidad_debitada());
             st.setInt(11, detalle.getCod_entidad_debitada());
             st.setString(12, detalle.getCod_sucursal());
             st.setInt(13, detalle.getSecuencia());
             st.setString(14, detalle.getCausal_devolucion());
             st.setString(15, usuario);
             st.setString(16, empresa);
             cadena += st.getSql();
             st = null;
             }*/

        } catch (Exception e) {
            System.out.println("Error en SQL_INSERTAR_DETALLE_RECAUDO (insertarRecaudosDetalle)" + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }
    ////////////////////////

    @Override
    public String guardarProcesoInterno(String procesoMeta, String nombre, String descripcion, String usuario, String empresa
    ) {
        con = null;
        ps = null;
        query = "guardarProcesoInterno";
        String respuestaJson = "{}";
        int resp = 0, idProInterno = 0;
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario);
            ps.setString(4, empresa);

            resp = ps.executeUpdate();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idProInterno = rs.getInt(1);
                }
            }
            respuestaJson = "{\"respuesta\":\"OK\",\"idProceso\":\"" + idProInterno + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\",\"idProceso\":\"0\"}";
                throw new SQLException("ERROR guardarProcesoInterno \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public int guardarSubcategoria(String nombre, String usuario, String empresa
    ) {
        con = null;
        ps = null;
        query = "guardarSubcategoria";
        String respuestaJson = "{}";
        int resp = 0, idProInterno = 0;
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, nombre);
            ps.setString(2, usuario);
            ps.setString(3, empresa);

            resp = ps.executeUpdate();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idProInterno = rs.getInt(1);
                }
            }

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\",\"idProceso\":\"0\"}";
                throw new SQLException("ERROR guardarProcesoInterno \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return idProInterno;
        }
    }

    @Override
    public int guardarRelCatSubcategoria(String idcategoria, int idsubcategoria, String usuario, String empresa
    ) {
        con = null;
        ps = null;
        query = "insertRelCatSubcatgoria";
        String respuestaJson = "{}";
        int resp = 0, idProInterno = 0;
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, Integer.parseInt(idcategoria));
            ps.setInt(2, idsubcategoria);
            ps.setString(3, empresa);
            ps.setString(4, usuario);

            resp = ps.executeUpdate();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idProInterno = rs.getInt(1);
                }
            }

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\",\"idProceso\":\"0\"}";
                throw new SQLException("ERROR guardarRelCatSubcategoria \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return idProInterno;
        }
    }

    @Override
    public int actualizarRelCatSubcategoria(String idcategoria, int idsubcategoria, String empresa
    ) {
        con = null;
        ps = null;
        query = "actualizarRelCatSubcategoria";
        String respuestaJson = "{}";
        int resp = 0, idProInterno = 0;
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, Integer.parseInt(idcategoria));
            ps.setInt(2, idsubcategoria);
            ps.setString(3, empresa);

            ps.executeUpdate();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idProInterno = rs.getInt(1);
                }
            }

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\",\"idProceso\":\"0\"}";
                throw new SQLException("ERROR actualizarRelCatSubcategoria \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return idProInterno;
        }
    }

    @Override
    public int obtenerIdSubcategoria(String nombre, String dstrct
    ) {
        con = null;
        rs = null;
        ps = null;
        int id = 0;
        query = "obtenerIdSubcategoria";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, dstrct);

            rs = ps.executeQuery();

            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en obtenerIdSubcategoria " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return id;
    }

    @Override
    public int obtenerIdProcesoInterno(String nomProceso, String idProMeta
    ) {
        con = null;
        rs = null;
        ps = null;
        int id = 0;
        query = "obtenerIdProcesoInterno";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, nomProceso);
            ps.setInt(2, Integer.parseInt(idProMeta));

            rs = ps.executeQuery();

            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en obtenerIdProcesoInterno " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return id;
    }

    @Override
    public String insertarRelUnidadProInterno(int idProInterno, String IdUnid, String Obligatorio, String empresa, int idProMetaEdit
    ) {
        con = null;
        st = null;
        String sql = "";
        query = "insertarRelUnidadProInterno";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            st = new StringStatement(query, true);
            st.setInt(1, idProInterno);
            st.setInt(2, Integer.parseInt(IdUnid));
            st.setInt(3, Integer.parseInt(Obligatorio));
            st.setString(4, empresa);
            st.setInt(5, idProMetaEdit);

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new Exception("Error en insertarRelUnidadProInterno " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
        return sql;
    }

    @Override
    public String insertarRelValPred(int idProInterno, String IdUnid, String empresa, String usuario
    ) {
        con = null;
        st = null;
        String sql = "";
        query = "guardarValPred";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            st = new StringStatement(query, true);
            st.setInt(1, idProInterno);
            st.setInt(2, Integer.parseInt(IdUnid));
            st.setString(3, empresa);
            st.setString(4, usuario);

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new Exception("Error en guardarValPred " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
        return sql;
    }

    @Override
    public ArrayList<variable_mercado> cargarUndNegocioProinterno(String idProinterno, String idProMetaEdit
    ) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<variable_mercado> lista = new ArrayList<variable_mercado>();
        query = "cargarUndNegocioProinterno";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(idProinterno));
            ps.setInt(2, Integer.parseInt(idProMetaEdit));

            rs = ps.executeQuery();

            while (rs.next()) {
                variable_mercado uneg = new variable_mercado();
                uneg.setId(rs.getInt("idespecificacion"));
                uneg.setNombre(rs.getString("nombre"));
                uneg.setDescripcion(rs.getString("descripcion"));
                uneg.setObligatorio(rs.getInt("obligatorio"));
                uneg.setTipo_dato(rs.getInt("t_entrada"));

                lista.add(uneg);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarUndNegocioProinterno " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public String actualizarProcesoInterno(int idProinterno, String nombre, int idProMeta, String usuario, String empresa, int idSubcategoria
    ) {
        con = null;
        ps = null;
        query = "actualizarProcesoInterno";
        String respuestaJson = "{}";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, usuario);
            ps.setInt(3, idSubcategoria);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarProcesoInterno \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String eliminarUndProinterno(String idProinterno, String idUnidad
    ) {
        con = null;
        st = null;
        query = "eliminarUndProinterno";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(idProinterno));
            st.setInt(2, Integer.parseInt(idUnidad));

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR eliminarUndProinterno \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return sql;
        }
    }

    @Override
    public ArrayList<Usuario> listarUsuario() {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        query = "listarUsuario";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                Usuario user = new Usuario();
                user.setCodUsuario(rs.getInt("codigo_usuario"));
                user.setNombre(rs.getString("nombre"));
                user.setIdusuario(rs.getString("idusuario"));

                lista.add(user);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuario " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public ArrayList<Unidad_Negocio> listarProinternoUsuario(int codUsuario
    ) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Unidad_Negocio> lista = new ArrayList<Unidad_Negocio>();
        query = "listarProinternoUsuario";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, codUsuario);

            rs = ps.executeQuery();

            while (rs.next()) {
                Unidad_Negocio pro = new Unidad_Negocio();
                pro.setId(rs.getInt("id"));
                pro.setId_tabla_rel(rs.getInt("id_proceso_meta"));
                pro.setTipo(rs.getString("tipo"));
                pro.setDescripcion(rs.getString("descripcion"));

                lista.add(pro);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuario " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public ArrayList<Unidad_Negocio> listarProinterno() {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Unidad_Negocio> lista = new ArrayList<Unidad_Negocio>();
        query = "listarProinterno";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                Unidad_Negocio pro = new Unidad_Negocio();
                pro.setId(rs.getInt("id"));
                pro.setId_tabla_rel(rs.getInt("id_proceso_meta"));
                pro.setTipo(rs.getString("tipo"));
                pro.setDescripcion(rs.getString("descripcion"));

                lista.add(pro);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuario " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public String insertarRelProInternoUser(String idProInt, int codUsuario, String login, String empresa
    ) {
        con = null;
        st = null;
        String sql = "";
        query = "insertarRelProInternoUser";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(idProInt));
            st.setInt(2, codUsuario);
            st.setString(3, login);
            st.setString(4, empresa);

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new Exception("Error en insertarRelProInternoUser " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
        return sql;
    }

    @Override
    public ArrayList<Unidad_Negocio> cargarProcesoInterno(int idMetaProceso, String status
    ) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Unidad_Negocio> lista = new ArrayList<Unidad_Negocio>();
        query = "cargarProcesoInterno";
        String filtro = " b.reg_status !='" + status + "' and b.id_unidad_negocio=" + idMetaProceso;
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                Unidad_Negocio pro = new Unidad_Negocio();
                pro.setId(rs.getInt("id"));
                pro.setNombre(rs.getString("nombresub"));
                pro.setDescripcion(rs.getString("descsub"));
                pro.setId_tabla_rel(rs.getInt("id_categoria"));
                pro.setDescripcionTablaRel(rs.getString("nombre"));
                pro.setIdsubcategoria(rs.getInt("id_subcategoria"));

                lista.add(pro);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesoInterno " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public ArrayList<variable_mercado> cargarUnidadesNegocio(int idProceso, int idProMetaEdit
    ) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<variable_mercado> lista = new ArrayList<variable_mercado>();
        query = "cargarEspecificaciones";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            //ps.setInt(1, idProceso);
            //ps.setInt(2, idProMetaEdit);
            rs = ps.executeQuery();

            while (rs.next()) {
                variable_mercado uneg = new variable_mercado();
                uneg.setId(rs.getInt("idespecificacion"));
                uneg.setDescripcion(rs.getString("descripcion"));
                uneg.setNombre(rs.getString("nombre"));
                uneg.setTipo_dato(rs.getInt("t_entrada"));
                uneg.setObligatorio(rs.getInt("obligatorio"));

                lista.add(uneg);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarUnidadesNegocio " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public String anularMetaProceso(int idProceso, String usuario
    ) {
        con = null;
        ps = null;
        Statement stmt = null;
        String respuestaJson = "{}";

        try {

            if (!verificaExisteCat(idProceso)) {

                con = this.conectarJNDI();
                con.setAutoCommit(false);
                stmt = con.createStatement();

                stmt.addBatch("update  opav.sl_categoria set reg_status='A' where id=" + idProceso);

                stmt.executeBatch();
                stmt.clearBatch();
                con.commit();
                respuestaJson = "{\"respuesta\":\"OK\"}";

            } else {
                respuestaJson = "{\"respuesta\":\"OK1\"}";
            }

        } catch (Exception e) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR anularMetaProceso \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String anularProcesoInterno(int idProinterno,
            int idProcesoMeta, String usuario
    ) {
        con = null;
        ps = null;
        Statement stmt = null;
        String respuestaJson = "{}";

        try {

            //if (!verificaExisteSub(idProinterno)) {
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();

            stmt.addBatch(this.generarQueryCambiaEstadoProceso("administrativo.conf_tipo_variable", "A", " where  id_tipo_variable_mercado in (select id_tipo_variable_mercado from administrativo.rel_und_tipo where id_unidad_negocio= " + idProcesoMeta + " and id =" + idProinterno + ")", "UPD", usuario));
            stmt.addBatch(this.generarQueryCambiaEstadoProceso("administrativo.rel_und_tipo", "A", " where id_unidad_negocio=" + idProcesoMeta + "and id=" + idProinterno, "UPD", usuario));
            //stmt.addBatch(this.generarQueryCambiaEstadoProceso("opav.sl_categoria", "A", " where idcategoria="+idProceso,"UPD",usuario));

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            /*} else {
             respuestaJson = "{\"respuesta\":\"OK1\"}";
             }*/

        } catch (Exception e) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR anularProcesoInterno \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    public String generarQueryCambiaEstadoProceso(String tabla, String estado, String where, String action, String usuario) {

        // Deprecated
        String query = "";
        try {
            if (action.equals("UPD")) {
                query = "UPDATE " + tabla + " set reg_status='" + estado + "'," + "last_update = now(), user_update= '" + usuario + "' " + where;
            }
            if (action.equals("DEL")) {
                query = "DELETE FROM " + tabla + where;

            }

        } catch (Exception e) {
            Logger.getLogger(Auto_ScoringImpl.class
                    .getName()).log(Level.SEVERE, null, e);
        } finally {
            return query;
        }
    }

    @Override
    public ArrayList<Usuario> listarUsuariosProinterno(int idProceso) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        query = "listarUsuariosProinterno";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, idProceso);

            rs = ps.executeQuery();

            while (rs.next()) {
                Usuario user = new Usuario();
                user.setCodUsuario(rs.getInt("codigo_usuario"));
                user.setNombre(rs.getString("nombre"));
                user.setIdusuario(rs.getString("idusuario"));
                user.setModerador(rs.getString("moderador"));
                lista.add(user);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuariosProinterno " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public ArrayList<Usuario> listarUsuariosRelProInterno(int idProceso) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        query = "listarUsuariosRelProInterno";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, idProceso);

            rs = ps.executeQuery();

            while (rs.next()) {
                Usuario user = new Usuario();
                user.setCodUsuario(rs.getInt("codigo_usuario"));
                user.setNombre(rs.getString("nombre"));
                user.setIdusuario(rs.getString("idusuario"));

                lista.add(user);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuariosRelProInterno " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public String eliminarUsuarioProinterno(int idProinterno, String idUsuario) {
        con = null;
        st = null;
        query = "eliminarUsuarioProinterno";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, idProinterno);
            st.setInt(2, Integer.parseInt(idUsuario));

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR eliminarUsuarioProinterno \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return sql;
        }
    }

    @Override
    public boolean existenUsuariosRelProceso(int idProceso, String tipo) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;

        query = "existenUsuariosRelProceso";
        try {

            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, idProceso);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existenUsuariosRelProceso " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public boolean existeMetaProceso(String empresa, String descripcion, int idProceso) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "existeMetaProceso";
        String filtro = " and idcategoria not in(" + idProceso + ")";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);;

            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setString(2, descripcion);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeMetaProceso " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public boolean existeProcesoInterno(String procesoMeta, String descripcion, int idProceso) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "existeProcesoInterno";
        String filtro = " and id_cat_sub_rel not in(" + idProceso + ")";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(procesoMeta));
            ps.setString(2, descripcion);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeProcesoInterno " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String activarMetaProceso(int idProceso) {
        con = null;
        ps = null;
        query = "anularMetaProceso";
        Statement stmt = null;
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            con.setAutoCommit(false);
            stmt = con.createStatement();

            stmt.addBatch(this.generarQueryCambiaEstadoProceso("proceso_interno", "", " where id_proceso_meta=" + idProceso, "UPD", ""));
            stmt.addBatch(this.generarQueryCambiaEstadoProceso("proceso_meta", "", " where id=" + idProceso, "UPD", ""));

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activarMetaProceso \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String activarProcesoInterno(int idProinterno) {
        con = null;
        ps = null;
        Statement stmt = null;
        query = "activarProcesoInterno";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, idProinterno);

            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR activarProcesoInterno \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarModerador(int idProceso, int idusuario, String moderador) {
        con = null;
        ps = null;
        Statement stmt = null;
        query = "actualizarModerador";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, moderador);
            ps.setInt(2, idProceso);
            ps.setInt(3, idusuario);

            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarModerador \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public ArrayList<Usuario> listarUsuariosRelProInterno() {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        query = "listarUsuariosRelProInterno";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                Usuario user = new Usuario();
                user.setCodUsuario(rs.getInt("id_usuario"));
                user.setIdusuario(rs.getString("login"));

                lista.add(user);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en listarUsuariosRelProInterno " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(UnidadesNegocioImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public ArrayList<Unidad_Negocio> cargarProcesosRelUsuario(String idusuario) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Unidad_Negocio> lista = new ArrayList<Unidad_Negocio>();
        query = "listarProcesosRelUsuarios";
        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(idusuario));
            rs = ps.executeQuery();

            while (rs.next()) {
                Unidad_Negocio pro = new Unidad_Negocio();
                pro.setId(rs.getInt("id"));
                pro.setDescripcion(rs.getString("descripcion"));
                pro.setId_tabla_rel(rs.getInt("id_proceso_meta"));
                pro.setTipo(rs.getString("tipo"));
                lista.add(pro);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosRelUsuario " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public ArrayList<Unidad_Negocio> cargarTipoRequisicionRelUsuario(String idusuario) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<Unidad_Negocio> lista = new ArrayList<Unidad_Negocio>();
        query = "listarTiposReqRelUsuarios";
        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(idusuario));
            rs = ps.executeQuery();

            while (rs.next()) {
                Unidad_Negocio pro = new Unidad_Negocio();
                pro.setId(rs.getInt("id"));
                pro.setDescripcion(rs.getString("descripcion"));
                pro.setTipo(rs.getString("tipo"));
                lista.add(pro);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarTipoRequisicionRelUsuario " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public String eliminarRelProcesoReqUser(int idUsuario) {

        con = null;
        st = null;
        String query = "eliminaRelProcesoReqUser";
        String sql = "";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, idUsuario);
            sql = st.getSql();

        } catch (SQLException ex) {
            throw new Exception("Error en eliminarRelProcesoReqUser(int idUsuario) " + ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                }
            }
            return sql;
        }
    }

    @Override
    public String insertarRelProcesoReqUser(String idProceso, int codUsuario, String login, Usuario usuario) {
        con = null;
        st = null;
        String sql = "";
        query = "insertarRelProcesoReqUser";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            st = new StringStatement(query, true);
            st.setString(1, usuario.getDstrct());
            st.setInt(2, Integer.parseInt(idProceso));
            st.setInt(3, codUsuario);
            st.setString(4, login);
            st.setString(5, usuario.getLogin());

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new Exception("Error en insertarRelProcesoReqUser " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
        return sql;
    }

    @Override
    public String eliminarRelTipoReqUser(int idUsuario) {

        con = null;
        st = null;
        String query = "eliminaRelTipoReqUser";
        String sql = "";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, idUsuario);
            sql = st.getSql();

        } catch (SQLException ex) {
            throw new Exception("Error en eliminarRelTipoReqUser(int idUsuario) " + ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                }
            }
            return sql;
        }
    }

    @Override
    public String insertarRelTipoReqUser(String idTipoReq, int codUsuario, String login, Usuario usuario) {
        con = null;
        st = null;
        String sql = "";
        query = "insertarRelTipoReqUser";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            st = new StringStatement(query, true);
            st.setString(1, usuario.getDstrct());
            st.setInt(2, Integer.parseInt(idTipoReq));
            st.setInt(3, codUsuario);
            st.setString(4, login);
            st.setString(5, usuario.getLogin());

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new Exception("Error en insertarRelTipoReqUser " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
        return sql;
    }

    @Override
    public String obtenerEspecificacionesColumnas(String subcategoria) {

        con = null;
        ps = null;
        rs = null;
        String query = "ConsultaEspecificaciones";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query);

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, subcategoria);
                rs = ps.executeQuery();
                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("idesp", rs.getString("idesp"));
                    jsonObject.addProperty("nombre", rs.getString("nombre"));
                    jsonObject.addProperty("tipo", rs.getString("tipo"));
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            lista = null;
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    private void getObjetoJson(JsonObject object, ResultSet rs) throws SQLException {

        object.addProperty("id", rs.getString("id"));
        if (rs.getString("tipo_dato").equals("0")) {
            object.addProperty("esp-" + rs.getString("nombre_esp").replace(" ", ""), rs.getString("valor_especificacion"));
            object.addProperty("espidval-" + rs.getString("nombre_esp").replace(" ", ""), rs.getString("id_valor_predeterminado"));

        } else {
            object.addProperty("esp-" + rs.getString("nombre_esp").replace(" ", ""), rs.getString("valor_especificacion"));
        }
        object.addProperty("descripcion", rs.getString("descripcion"));
        object.addProperty("codigogenerado", rs.getString("codigogenerado"));

    }

    @Override
    public JsonObject cargar_combo(JsonObject info) {
        JsonObject respuesta = new JsonObject();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        try {
            query = info.get("query").getAsJsonPrimitive().getAsString();
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            if (info.get("filtros") != null) {
                JsonArray ar = info.get("filtros").getAsJsonArray();
                for (int i = 0; i < ar.size(); i++) {
                    ps.setString(i + 1, ar.get(i).getAsString());
                }
            }
            rs = ps.executeQuery();
            respuesta.addProperty("0", "...");
            while (rs.next()) {
                respuesta.addProperty(rs.getString("codigo"), rs.getString("nombre"));
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject consultar(String idsubcategoria) {
        JsonObject respuesta = new JsonObject(), fila;
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        String queryCol = "ConsultaEspecificaciones";
        String query = "consultarinfo";
        String queryContar = "consultarCuantasColumnas";
        String queryConte = "consultarCuantosRegistros";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(idsubcategoria));
            rs = ps.executeQuery();
            int idAux = 0;
            int contador = 1;
            while (rs.next()) {

                if (idAux != rs.getInt("id")) {//entra una vez                    

                    object = new JsonObject();
                    idAux = rs.getInt("id");
                    getObjetoJson(object, rs);

                } else if (idAux == rs.getInt("id")) {
                    contador++;
                    idAux = rs.getInt("id");
                    getObjetoJson(object, rs);
                }
                if (contador == rs.getInt("control")) {
                    arr.add(object);
                    contador = 1;
                }
            }

            System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public String guardarValorPredeterminado(String empresa, String nombre, String usuario) {
        con = null;
        ps = null;
        query = "guardarValorPredeterminado";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, empresa);
            ps.setString(2, nombre);
            ps.setString(3, usuario);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarMetaProceso \n" + e.getMessage());

            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    public ArrayList<ValorPredeterminadoUn> listar_valorespredeterminados_filtro_edit(String empresa, String status, String id) {
        con = null;
        rs = null;
        ps = null;

        ArrayList<ValorPredeterminadoUn> lista = new ArrayList<ValorPredeterminadoUn>();
        query = "listarvalorespredeterminados_filtro_edit";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                ValorPredeterminadoUn itemsel = new ValorPredeterminadoUn();
                itemsel.setId(rs.getInt("id"));
                itemsel.setValor_xdefecto(rs.getString("Valor_xdefecto"));
                itemsel.setDescripcion(rs.getString("descripcion"));
                itemsel.setIdrel(rs.getInt("idrel"));
                lista.add(itemsel);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosMeta " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    public ArrayList<ValorPredeterminadoUn> listar_valorespredeterminados_edit(String empresa, String status, String id) {
        con = null;
        rs = null;
        ps = null;

        ArrayList<ValorPredeterminadoUn> lista = new ArrayList<ValorPredeterminadoUn>();
        query = "listarvalorespredeterminados_edit";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                ValorPredeterminadoUn itemsel = new ValorPredeterminadoUn();
                itemsel.setId(rs.getInt("id"));
                itemsel.setValor_xdefecto(rs.getString("Valor_xdefecto"));
                itemsel.setDescripcion(rs.getString("descripcion"));
                lista.add(itemsel);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en cargarProcesosMeta " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    public JsonObject modificar_espVal_edit(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        StringStatement st = null;
        con = null;
        ps = null;
        String mensaje = "";
        TransaccionService tsrv = null;
        int resp = 0;
        String sql = "";

        try {

            Connection con = null;
            PreparedStatement ps = null;
            con = this.conectarJNDI();

            String query = "guardarEspecificacionEdit";
            con.setAutoCommit(false);

            ps = con.prepareStatement(this.obtenerSQL(query));

            ps.setString(1, info.get("nomesp").getAsString());
            ps.setString(2, info.get("nomesp").getAsString());
            ps.setString(3, info.get("usuario").getAsString());
            ps.setString(4, info.get("t_entrada").getAsString());
            ps.setString(5, info.get("id").getAsString());

            ps.executeUpdate();

            ps = con.prepareStatement(updateEspEdit(info));
            ps.executeUpdate();

            mensaje = "Edicion satisfactoria";
            con.commit();

            respuesta = new JsonObject();

            respuesta.addProperty(
                    "mensaje", mensaje);

        } catch (Exception exc) {
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede actualizar");
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    public String updateEspEdit(JsonObject info) throws SQLException {

        String cadena = "";
        String query = "deleteValPredEdit";
        String sql = "";
        StringStatement st = null;
        JsonObject recorre = new JsonObject();
        String nombre;
        String nombre2;
        String valor;
        boolean booleano = true;
        int conteo = 0;
        int conteo1 = 0;
        int var = 0;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);
        JsonArray arr = info.getAsJsonArray("rows");

        try {

            st.setInt(1, info.get("id").getAsInt());
            cadena += st.getSql();

            st = null;
            sql = this.obtenerSQL("guardarValPred");
            st = new StringStatement(sql, true);

            for (int c = 0; c < arr.size(); c++) {
                recorre = arr.get(c).getAsJsonObject();

                st.setString(1, info.get("id").getAsString());
                st.setInt(2, recorre.get("id").getAsInt());
                st.setString(3, info.get("dstrct").getAsString());
                st.setString(4, info.get("usuario").getAsString());

                cadena += st.getSql();

                st = null;
                sql = this.obtenerSQL("guardarValPred");
                st = new StringStatement(sql, true);

            }

        } catch (Exception e) {
            System.out.println("Error en updateEspEdit ()" + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }

    public boolean verificaExisteCat(int categoria) {
        int resp = 0;
        boolean ret = false;
        Connection con1 = null;
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        String query1 = "";

        try {
            con1 = this.conectarJNDI("ConsultaTieneDatoCat");
            query1 = this.obtenerSQL("ConsultaTieneDatoCat");
            ps1 = con1.prepareStatement(query1);

            ps1.setInt(1, categoria);

            rs1 = ps1.executeQuery();

            while (rs1.next()) {
                resp = rs1.getInt("num");
            }

            if (resp > 0) {

                ret = true;

            }

        } catch (SQLException ex) {
            Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ret;
    }

    public boolean verificaExisteSub(int categoria) {
        int resp = 0;
        boolean ret = false;
        Connection con1 = null;
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        String query1 = "";

        try {
            con1 = this.conectarJNDI("ConsultaTieneDatoSub");
            query1 = this.obtenerSQL("ConsultaTieneDatoSub");
            ps1 = con1.prepareStatement(query1);

            ps1.setInt(1, categoria);

            rs1 = ps1.executeQuery();

            while (rs1.next()) {
                resp = rs1.getInt("num");
            }

            if (resp > 0) {

                ret = true;

            }

        } catch (SQLException ex) {
            Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ret;
    }

    @Override
    public String cargarComboInsumos(String empresa) {
        con = null;
        rs = null;
        ps = null;
        query = "ConsultaTiposInsumos";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            //ps.setString(1, empresa);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre_insumo"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public ArrayList<String> cargarNom_Especificacion(String idcategoria) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<String> lista = new ArrayList<String>();
        query = "cargarnomespecificaciones";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(idcategoria));
            rs = ps.executeQuery();

            while (rs.next()) {

                lista.add(rs.getString("nombre"));

            }
        } catch (Exception e) {
            try {
                throw new Exception("Error al Cargar las Sub Categorias " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public boolean anularInsumo(int id) {
        con = null;
        ps = null;
        boolean resp = false;
        query = "AnularInsumo";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, id);

            ps.executeUpdate();
            resp = true;

        } catch (Exception e) {
            try {
                throw new Exception("Error en existeMetaProceso " + e.toString());

            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }
    
    @Override
    public String cargarInfoScoring(String id_und_negocio) {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String consulta = "";
        JsonObject obj = new JsonObject();
        String query = "cargarScoring";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_und_negocio);
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();     
                fila.addProperty("puntaje_maximo", rs.getString("puntaje_maximo"));
                fila.addProperty("valor_min", rs.getString("valor_min"));
                fila.addProperty("valor_max", rs.getString("valor_max"));
                fila.addProperty("id_conf_pred", rs.getString("id_conf_pred"));
                fila.addProperty("tipo", rs.getString("tipo"));
                fila.addProperty("variable_mercado", rs.getString("variable_mercado"));                
                fila.addProperty("descripcion", rs.getString("descripcion"));               
                fila.addProperty("puntaje", rs.getString("puntaje"));
                fila.addProperty("minimo", rs.getString("minimo"));
                fila.addProperty("maximo", rs.getString("maximo"));
                fila.addProperty("puntaje_maximo", rs.getString("puntaje_maximo"));
                arr.add(fila);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(arr);
        }
    }
    
    @Override
     public String actualizarScoreUndNegocio(int id_uidad_negocio,String puntaje_maximo, String fieldUpdate, String valor) {
        Connection con = null;
        StringStatement st;       
        String sql = "";    
        String query =  "actualizarScoringUndNegocio";      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#param", fieldUpdate);
            st = new StringStatement(query, true);
            st.setString(1, puntaje_maximo);
            st.setInt(2, Integer.parseInt(valor));
            st.setInt(3, id_uidad_negocio);         
          
            sql = st.getSql();  
       
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR actualizarScoreUndNegocio \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return sql;
       }
    }

     
     @Override
     public String actualizarScoring(int id_conf_pred,String puntaje, String status, String fieldUpdate) {
        Connection con = null;
        StringStatement st;       
        String sql = "";    
        String query =  "actualizarScoring";      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#param", fieldUpdate);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(puntaje));
            st.setString(2, status);           
            st.setInt(3, id_conf_pred);      
            sql = st.getSql();  
       
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR actualizarScoring \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return sql;
       }
    }
     
    @Override
    public String cargarGridSimuladorScoring(String id_und_negocio) {
        Gson gson = new Gson();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;       
        JsonObject obj = new JsonObject();
        String query = "cargarSimuladorScoring";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();        
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_und_negocio);
            rs = ps.executeQuery();
            JsonObject fila;
            while (rs.next()) {
                fila = new JsonObject();     
                fila.addProperty("id", rs.getString("id"));                
                fila.addProperty("variable_mercado", rs.getString("variable_mercado"));                
                fila.addProperty("tipo_variable", rs.getString("tipo_variable"));   
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }
    
    @Override
    public String cargarValoresPredeterminados(String id_und_negocio) {
        con = null;
        rs = null;
        ps = null;
        query = "cargarValoresPredeterminados";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_und_negocio);
            rs = ps.executeQuery();
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                    JsonObject fila = new JsonObject();
                    fila.addProperty("id_variable_mercado", rs.getString("id_variable_mercado"));
                    fila.addProperty("id", rs.getString("id"));
                    fila.addProperty("descripcion", rs.getString("descripcion"));
                    arr.add(fila);
            }
            respuestaJson = gson.toJson(arr);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
     @Override
     public void guardarValoresVM(String id_unidad_negocio, String fieldNames, String values, String usuario) {
        con = null;
        ps = null;     
        String sql = "";    
        String query = (id_unidad_negocio.equals("2") || id_unidad_negocio.equals("8")) ? "guardarValoresParamsEducativo": (id_unidad_negocio.equals("3") || id_unidad_negocio.equals("9")) ? "guardarValoresParamsTaxi" : (id_unidad_negocio.equals("4") || id_unidad_negocio.equals("10")) ? "guardarValoresParamsConsumo" : "guardarValoresParamsMicro";      
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#param1", fieldNames).replaceAll("#param2", values);
            ps = con.prepareStatement(query);   
            ps.setString(1, usuario); 
            ps.executeUpdate();      
       
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR actualizarValoresParamsVM \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }   
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }          
       }
    }
     
    @Override
    public String get_Next_Consecutivo_Simulador() {
        con = null;
        ps = null;
        rs = null;
        boolean resp = false;
        String query = "SQL_TRAER_NEXT_CONSECUTIVO_SIMULADOR";   
        String respuestaJson = "{}";
        String num_solicitud = "";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query); 
            rs = ps.executeQuery();
            
            if (rs.next()){
                num_solicitud = "SIM_"  + rs.getString("num_solicitud");
            }
            respuestaJson = "{\"respuesta\":\""+num_solicitud+"\"}";
        }catch (Exception e) {
            try {
                throw new Exception("Error en get_Next_Consecutivo_Simulador " + e.toString());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return respuestaJson;
    }
    
     @Override
     public void actualizaConsecutivoSimulador() {
        con = null;
        ps = null;    
        String sql = "";    
        String query =  "SQL_ACTUALIZA_SECUENCIA_SIMULADOR";      
        try{
            con = this.conectarJNDI();          
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query); 
            ps.executeUpdate();      
                 
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR actualizaConsecutivoSimulador \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }   
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }      
       }
    }
     
   @Override
    public JsonObject cargarInfoSimulacion(String id_und_negocio, String num_solicitud) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        String consulta = "";
        JsonObject obj = new JsonObject();
        String query = (id_und_negocio.equals("2") || id_und_negocio.equals("8")) ? "SQL_CARGAR_INFO_SIMULACION_EDUCATIVO": (id_und_negocio.equals("3") || id_und_negocio.equals("9")) ? "SQL_CARGAR_INFO_SIMULACION_TAXI" : (id_und_negocio.equals("4") || id_und_negocio.equals("10")) ? "SQL_CARGAR_INFO_SIMULACION_CONSUMO" : "SQL_CARGAR_INFO_SIMULACION_MICRO";      

        try {
            con = this.conectarJNDI();
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();     
                fila.addProperty("id_solicitud", rs.getString("id_solicitud"));
                fila.addProperty("puntaje_minimo", rs.getString("puntaje_minimo"));
                fila.addProperty("puntaje_maximo", rs.getString("puntaje_maximo"));
            }
            obj = fila;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return obj;
        }
    }
    
    @Override
     public String simularScoring(String num_solicitud, String id_und_negocio,int tipo) {
        con = null;
        ps = null;
        rs = null;    
        String puntaje = "";    
        String query =  "SQL_CALCULAR_SCORING";      
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_solicitud);
            ps.setInt(2, Integer.parseInt(id_und_negocio));
            ps.setInt(3, tipo);

            rs = ps.executeQuery();  
            while (rs.next()) {
                    puntaje = rs.getString("calcula_scoring");
            }
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR simularScoring \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
           return puntaje;
       }
    }
     
      @Override
     public void simulaScoring(String num_solicitud, String id_und_negocio,int tipo) {
        con = null;
        ps = null;              
        String query =  "SQL_CALCULAR_SCORING";      
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_solicitud);
            ps.setInt(2, Integer.parseInt(id_und_negocio));
            ps.setInt(3, tipo);

            ps.executeUpdate();            
       
        }catch (Exception e) {
            try {               
                throw new SQLException("ERROR simulaScoring \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }       
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }          
       }
    }
     
    @Override
    public ArrayList<ValorPredeterminadoUn> listar_valoresVariableMercado(String id_variable_mercado) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<ValorPredeterminadoUn> lista = new ArrayList<ValorPredeterminadoUn>();
        query = "listarvalorespredeterminadosEspecif";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_variable_mercado));
            rs = ps.executeQuery();

            while (rs.next()) {
                ValorPredeterminadoUn itemsel = new ValorPredeterminadoUn();
                itemsel.setId(rs.getInt("id"));
                itemsel.setValor_xdefecto(rs.getString("descripcion"));
                itemsel.setInicio(rs.getString("inicio"));
                itemsel.setFin(rs.getString("fin"));
                lista.add(itemsel);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en listar_valoresVariableMercado " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }
    
    @Override
    public ArrayList<ValorPredeterminadoUn> listar_valoresVariableMercado2(String id_variable_mercado) {
        con = null;
        rs = null;
        ps = null;
        ArrayList<ValorPredeterminadoUn> lista = new ArrayList<ValorPredeterminadoUn>();
        query = "listarvalorespredeterminadosEspecif2";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_variable_mercado);
            rs = ps.executeQuery();

            while (rs.next()) {
                ValorPredeterminadoUn itemsel = new ValorPredeterminadoUn();
                itemsel.setId(rs.getInt("id"));
                itemsel.setValor_xdefecto(rs.getString("descripcion"));
                itemsel.setInicio(rs.getString("unico"));
                lista.add(itemsel);
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en listar_valoresVariableMercado2 " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(Auto_ScoringImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }
    
    public JsonObject eliminar_Val_Pred(String id_variable_mercado, String idVP) {

        JsonObject respuesta = new JsonObject();
        StringStatement st = null;
        con = null;
        ps = null;
        String mensaje = "";
        TransaccionService tsrv = null;
        int resp = 0;
        String sql = "";

        try {

            Connection con = null;
            PreparedStatement ps = null;
            con = this.conectarJNDI();

            String query = "eliminarValorPredeterminado";
            con.setAutoCommit(false);
            
            ps = con.prepareStatement(eliminarRelValPred(id_variable_mercado, idVP));
            ps.executeUpdate();
            
            ps = con.prepareStatement(this.obtenerSQL(query));

            ps.setInt(1, Integer.parseInt(idVP));
            ps.executeUpdate();

         

            mensaje = "Eliminacion satisfactoria";
            con.commit();

            respuesta = new JsonObject();

            respuesta.addProperty(
                    "mensaje", mensaje);

        } catch (Exception exc) {
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no se pudo eliminar");
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    public String eliminarRelValPred(String id_variable_mercado, String idVP) {

        String cadena = "";
        String query = "eliminarVPEspecif";
        String sql = "";
        StringStatement st = null;
        JsonObject recorre = new JsonObject();
        String nombre;
        String nombre2;
        String valor;
        boolean booleano = true;
        int conteo = 0;
        int conteo1 = 0;
        int var = 0;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);
      
        try {

            st.setInt(1, Integer.parseInt(id_variable_mercado));
            st.setInt(2, Integer.parseInt(idVP));
            cadena += st.getSql();
            st=null;

        } catch (Exception e) {
            System.out.println("Error en eliminarRelValPred ()" + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }


   




}
