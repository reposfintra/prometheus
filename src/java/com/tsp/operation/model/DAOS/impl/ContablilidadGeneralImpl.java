package com.tsp.operation.model.DAOS.impl;

import com.aspose.cells.Cell;
import com.aspose.cells.CellValueType;
import com.aspose.cells.Cells;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import com.aspose.cells.Worksheets;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.ContabilidadGeneralDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.ParametrosBeans;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.LogWriter;
import com.tsp.util.Util;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author egonzalez
 */
public class ContablilidadGeneralImpl extends MainDAO implements ContabilidadGeneralDAO {

    public ContablilidadGeneralImpl(String databaseName) {
        super("ContabilidadGeneralDAO.xml", databaseName);

    }

    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    StringStatement st;

    @Override
    public String getSelectDataBaseJson(String query, ParametrosBeans parametrosBeans, Usuario user) {
        con = null;
        ps = null;
        rs = null;

        String respuestaJson = "{}";
        String filtro = "";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query, user.getBd());

            if (con != null) {

                switch (query) {

                    case "BUSCAR_FACTURAS_INDEMNIZACION":

                        if (parametrosBeans.getStringParameter3().equals("")) {
                            filtro = "replace(substring(now(),1,7),'-','')";
                        } else {
                            filtro = parametrosBeans.getStringParameter3();
                        }

                        ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
                        ps.setString(1, parametrosBeans.getStringParameter5());
                        ps.setString(2, parametrosBeans.getStringParameter5());
                        ps.setInt(3, Integer.parseInt(parametrosBeans.getStringParameter1()));
                        ps.setString(4, parametrosBeans.getStringParameter2());
                        ps.setString(5, parametrosBeans.getStringParameter4());
                        break;

                    case "FACTURAS_POR_DESISTIR":

                        query = "FACTURAS_POR_DESISTIR";
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        ps.setString(1, parametrosBeans.getStringParameter1());
                        ps.setString(2, parametrosBeans.getStringParameter2());
                        break;

                    case "VER_FACTURAS_INDENIZADAS":

                        query = "VER_FACTURAS_INDENIZADAS";
                        ps = con.prepareStatement(this.obtenerSQL(query));
                        break;

                    case "BUSCAR_FACTURAS_ENDOSAR":

                        query = "QRY_BUSCAR_FACTURAS_ENDOSAR";

                        if (parametrosBeans.getStringParameter2().equals("todos")) {
                            filtro = "AND un.ref_4 = '" + parametrosBeans.getStringParameter1() + "'";
                        } else {
                            filtro = "AND un.id = " + parametrosBeans.getStringParameter2();
                        }

                        ps = con.prepareStatement(this.obtenerSQL(query));
                        //ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
                        ps.setString(1, parametrosBeans.getStringParameter1());
                        ps.setString(2, parametrosBeans.getStringParameter2());
                        ps.setString(3, parametrosBeans.getStringParameter3());
                        ps.setString(4, parametrosBeans.getStringParameter4());
                        ps.setString(5, parametrosBeans.getStringParameter5());
                        ps.setString(6, parametrosBeans.getStringParameter6());

                        System.out.println(ps);
                        break;
                }

                rs = ps.executeQuery();

                lista = new JsonArray();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                    lista.add(jsonObject);
                }

                respuestaJson = new Gson().toJson(lista);
            }
        } catch (SQLException e) {
            lista = null;
            respuestaJson = "{\"error\":\"Lo sentimosXX algo salio mal al carga la informacion.\","
                    + "\"exception\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return respuestaJson;
        }
    }

    @Override
    public String getInsertDataBaseJson(String query, JsonObject objects, Usuario user) {
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            StringStatement st = null;
            st = new StringStatement(sql, true);
            st.setString(1, objects.get("periodo_foto").getAsString());
            st.setString(2, objects.get("codcli").getAsString());
            st.setString(3, objects.get("nit_cliente").getAsString());
            st.setString(4, objects.get("nombre_cliente").getAsString());
            st.setString(5, objects.get("documento").getAsString());
            st.setString(6, objects.get("cuota").getAsString());
            st.setDouble(7, objects.get("valor_factura").getAsDouble());
            st.setDouble(8, objects.get("valor_indemnizado").getAsDouble());
            st.setString(9, objects.get("fecha_vencimiento").getAsString());
            st.setString(10, objects.get("negocio").getAsString());
            st.setString(11, objects.get("numero_aval").getAsString());
            st.setInt(12, objects.get("dias_vencidos").getAsInt());
            st.setInt(13, objects.get("convenio").getAsInt());
            st.setString(14, objects.get("cuenta").getAsString());
            st.setString(15, objects.get("ref_4").getAsString());
            st.setString(16, objects.get("cartera_en").getAsString());
            st.setString(17, user.getLogin());
            sql = st.getSql();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(ContablilidadGeneralImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }

    @Override
    public String carteraEn(Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "BUSCAR_RELACION_CARTERA";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query, user.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                lista = new JsonArray();

                jsonObject = new JsonObject();
                jsonObject.addProperty("codigo", "");
                jsonObject.addProperty("nombre", "Seleccione");
                lista.add(jsonObject);

                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("codigo", rs.getString("codigo"));
                    jsonObject.addProperty("nombre", rs.getString("nombre"));
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            lista = null;

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String CustodiaCartera(Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "BUSCAR_CUSTODIADOR_CARTERA";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query, user.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                lista = new JsonArray();

                jsonObject = new JsonObject();
                jsonObject.addProperty("codigo", "");
                jsonObject.addProperty("nombre", "Seleccione");
                lista.add(jsonObject);

                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("codigo", rs.getString("cedula"));
                    jsonObject.addProperty("nombre", rs.getString("nombre"));
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            lista = null;

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String EndosarAfiducia(String FiduciaActual, Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "BUSCAR_ENDOSAR_A";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query, user.getBd());

            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, FiduciaActual);
                System.out.println(ps);

                rs = ps.executeQuery();
                lista = new JsonArray();

                jsonObject = new JsonObject();
                jsonObject.addProperty("codigo", "");
                jsonObject.addProperty("nombre", "Seleccione");
                lista.add(jsonObject);

                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("codigo", rs.getString("cedula"));
                    jsonObject.addProperty("nombre", rs.getString("nombre"));
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            lista = null;

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String UnidadNegocio(String lineaNegocio, Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "BUSCAR_UNIDADES_NEGOCIO";
        JsonArray lista = null;
        JsonObject jsonObject = null;
        try {
            con = this.conectarJNDI(query, user.getBd());

            if (con != null) {

                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, lineaNegocio);
                System.out.println(ps);

                rs = ps.executeQuery();
                lista = new JsonArray();

                jsonObject = new JsonObject();
                jsonObject.addProperty("codigo", "todos");
                jsonObject.addProperty("nombre", "Todos");
                lista.add(jsonObject);

                while (rs.next()) {
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("codigo", rs.getString("id"));
                    jsonObject.addProperty("nombre", rs.getString("descripcion"));
                    lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            lista = null;

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String getInsertDataGenericJson(String query, String LoteEndoso, String lineaNegocio, String UnidadNegocio, String cartera_en, String endosar_a, String checkEstadoSaldo, String EstadoEndoso, JsonObject objects, Usuario user) {
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            StringStatement st = null;

            /*
            objects.get("xxxx").getAsString()
            objects.get("xxxx").getAsInt()
            objects.get("xxxx").getAsDouble()
             */
            st = new StringStatement(sql, true);

            st.setString(1, LoteEndoso);
            st.setString(2, endosar_a);
            st.setString(3, cartera_en);

            //st.setString(4, fechaCorte);
            //st.setString(4, lineaNegocio);
            st.setInt(4, objects.get("id_uneg_negocio").getAsInt());

            st.setInt(5, objects.get("id_uneg_negocio").getAsInt());
            st.setString(6, objects.get("uneg_negocio").getAsString());
            st.setString(7, objects.get("nit_cliente").getAsString());
            st.setString(8, objects.get("nombre_cliente").getAsString());
            st.setString(9, objects.get("codcli").getAsString());
            st.setString(10, objects.get("negocio").getAsString());
            st.setString(11, objects.get("tipo_negocio").getAsString());
            st.setString(12, objects.get("documento").getAsString());
            st.setString(13, objects.get("cuota").getAsString());
            st.setString(14, objects.get("fecha_vencimiento").getAsString());
            st.setString(15, objects.get("dias_vencidos").getAsString());
            st.setString(16, objects.get("valor_factura").getAsString());
            st.setString(17, objects.get("valor_abono").getAsString());
            st.setString(18, objects.get("valor_saldo").getAsString());

            st.setString(19, EstadoEndoso);
            st.setString(20, user.getLogin());

            sql = st.getSql();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(ContablilidadGeneralImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }

    @Override
    public String getUpdateDataGenericJson(String query, JsonObject objects, Usuario user) {
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            StringStatement st = null;

            st = new StringStatement(sql, true);

            st.setString(1, user.getLogin());
            st.setInt(2, objects.get("id_uneg_negocio").getAsInt());
            st.setString(3, objects.get("uneg_negocio").getAsString());
            st.setString(4, objects.get("negocio").getAsString());
            st.setString(5, objects.get("documento").getAsString());

            sql = st.getSql();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(ContablilidadGeneralImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }

    @Override
    public String ValidarOneValue(Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "LOTE_ENDOSO";
//        JsonArray lista = null;
//        JsonObject jsonObject = null;
        String serieLote = "";
        try {
            con = this.conectarJNDI(query, user.getBd());

            if (con != null) {

                ps = con.prepareStatement(this.obtenerSQL(query));
                System.out.println(ps);

                rs = ps.executeQuery();
//                lista = new JsonArray();
                if (rs.next()) {
                    serieLote = rs.getString("lote_endoso");
//                jsonObject = new JsonObject();
//                jsonObject.addProperty("lote_number", rs.getString("lote_endoso"));
//                lista.add(jsonObject);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            // lista = null;

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return serieLote;
        }
    }

    @Override
    public String crearComprobanteDiarioEndoso(Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "SQL_CREAR_CDIAR_ENDOSO";
        String retorno = "";
        try {
            con = this.conectarJNDI(query, user.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, user.getLogin());
                rs = ps.executeQuery();
                while (rs.next()) {
                    retorno = rs.getString("retorno");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            this.deleteDocumentsCDIAR(user);
            retorno = "Lo sentimos error al crear el comprobante de Endosamiento.";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return retorno;
        }
    }

    @Override
    public String crearComprobanteDiario(Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "SQL_CREAR_CDIAR_INDEMNIZACION";
        String retorno = "";
        try {
            con = this.conectarJNDI(query, user.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, user.getLogin());
                rs = ps.executeQuery();
                while (rs.next()) {
                    retorno = rs.getString("retorno");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            this.deleteDocumentsCDIAR(user);
            retorno = "Lo sentimos error al crear el comprobante de indemnizacion.";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return retorno;
        }
    }

    /**
     * Metodo que borra las registro insertados el la tabla control indemniacion.
     *
     * @param user
     * @return
     */
    private void deleteDocumentsCDIAR(Usuario user) {
        Connection con = null;
        String query = "SQL_CREAR_CDIAR_INDEMNIZACION";

        try {

            con = this.conectarJNDI(query, user.getBd());
            con.setAutoCommit(false);
            Statement stmt = con.createStatement();

            stmt.addBatch("DELETE  FROM administrativo.control_indemnizacion_fenalco  "
                    + "WHERE creation_user='" + user.getLogin() + "' AND estado_proceso='' "
                    + "AND creation_date::DATE=now()::DATE ");

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();

        } catch (SQLException ex) {
            try {
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(ReestructurarNegociosImpl.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            try {
                this.desconectar(con);
            } catch (SQLException e) {

            }

        }

    }

    @Override
    public String getInsertFacturaXdesistir(String query, JsonObject objects, Usuario user) {
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            StringStatement st = null;
            st = new StringStatement(sql, true);
            st.setString(1, objects.get("periodo_foto").getAsString());
            st.setString(2, objects.get("negocio").getAsString());
            st.setString(3, objects.get("documento").getAsString());
            st.setDouble(4, objects.get("valor_desistir").getAsDouble());
            st.setDouble(5, objects.get("cuenta").getAsDouble());
            st.setString(6, objects.get("ref_4").getAsString());
            st.setString(7, objects.get("cartera_en").getAsString());
            st.setString(8, user.getLogin());
            sql = st.getSql();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(ContablilidadGeneralImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }

    @Override
    public String crearComprobanteDiarioDesistimiento(Usuario user) {
        con = null;
        ps = null;
        rs = null;
        String query = "SQL_CREAR_CDIAR_DESISTIMIENTO";
        String retorno = "";
        try {
            con = this.conectarJNDI(query, user.getBd());
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, user.getLogin());
                rs = ps.executeQuery();
                while (rs.next()) {
                    retorno = rs.getString("retorno");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            this.deleteDocumentsCDIAR(user);
            retorno = "Lo sentimos error al crear el comprobante de desistimiento.";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return retorno;
        }
    }

    @Override
    public String cargarTipoDiferidos() {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray diferidos = new JsonArray();
        JsonObject obj = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("CARGAR_TIPO_DIFERIDOS"));
            rs = ps.executeQuery();

            while (rs.next()) {
                obj = new JsonObject();
                obj.addProperty("value", rs.getString(1));
                obj.addProperty("descripcion", rs.getString(1));
                diferidos.add(obj);
            }
        } catch (SQLException ex) {
            obj = new JsonObject();
            obj.addProperty("error", "No se pudo cargar los tipos diferidos");
            System.err.println("ERROR EN " + ex.getClass().getSimpleName() + ": " + ex.getErrorCode() + "-" + ex.getSQLState() + "-" + ex.getMessage());
            return obj.toString();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.err.println("ERROR EN " + ex.getClass().getSimpleName() + ": " + ex.getErrorCode() + "-" + ex.getSQLState() + "-" + ex.getMessage());
            }
        }
        return diferidos.toString();
    }

    @Override
    public String buscarDiferidos(String codigo, String tipo) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray unegocio = new JsonArray();
        JsonObject obj = null;
        String query = null;
        try {
            query = this.obtenerSQL("BUSCAR_DIFERIDOS");
            if (!tipo.equals("")) {
                query = query.replace("#tipo#", "AND inf.tipodoc = ?");
            } else {
                query = query.replace("#tipo#", "");
            }

            conn = this.conectarJNDI();
            ps = conn.prepareStatement(query);
            ps.setString(1, codigo);
            if (!tipo.equals("")) {
                ps.setString(2, tipo);
            }
            rs = ps.executeQuery();

            while (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                obj = new JsonObject();
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    obj.addProperty(rsmd.getColumnLabel(i), rs.getString(i));
                }
                unegocio.add(obj);
            }
        } catch (SQLException ex) {
            obj = new JsonObject();
            obj.addProperty("error", "No se pudo cargar la información");

            System.err.println("ERROR EN " + ex.getClass().getSimpleName() + ": " + ex.getErrorCode() + "-" + ex.getSQLState() + "-" + ex.getMessage());

            return obj.getAsString();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                obj = new JsonObject();
                obj.addProperty("descripcion", "No se pudo cargar la información");
                return obj.getAsString();
            }
        }
        return unegocio.toString();
    }

    @Override
    public String adelantarDiferidos(String codigo, String[] diferidos, Usuario usuario) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PrintWriter pw = null;
        String response = "";
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = path = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("ADELANTAR_DIFERIDOS"), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ps.setString(1, codigo);
            ps.setArray(2, conn.createArrayOf("text", diferidos));
            rs = ps.executeQuery();

            if (rs.next()) {
                File file = new File(path);
                file.mkdirs();

                String logFile = path + "/adelantarDiferidos" + new SimpleDateFormat("YYYYMMDD-HHmm").format(new Date()) + ".txt";

                pw = new PrintWriter(new FileWriter(logFile, true), true);
                LogWriter logW = new LogWriter("Adelantar diferido", LogWriter.INFO, pw);
                logW.tituloInicial("Los siguientes diferidos no se pudieron anular:");

                String[] arr = rs.getString(1).split(",");
                for (String s : arr) {
                    logW.log(s, logW.INFO);
                }
                logW.tituloFinal();
                response = "{\"mensaje\": \"Archivo de log creado, revise su carpeta por favor.\"}";
            }
        } catch (SQLException ex) {
            response = "{\"error\": \"No se pudo adelantar los diferidos.\"}";
            System.err.println("ERROR EN " + ex.getClass().getSimpleName() + ": " + ex.getErrorCode() + "-" + ex.getSQLState() + "-" + ex.getMessage());
        } catch (IOException ex) {
            response = "{\"error\": \"No se pudo crear el archivo de log\"}";
            System.err.println("ERROR EN " + ex.getClass().getSimpleName() + ": " + ex.getMessage());
        } finally {
            try {
                if (pw != null) {
                    pw.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                response = "\"error\": \"No se pudo adelantar los diferidos.\"";
            }
        }
        return response;
    }

    @Override
    public String anularDiferidos(String codigo, String[] diferidos, Usuario usuario) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PrintWriter pw = null;
        String response = "";
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = path = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("ANULAR_DIFERIDOS"), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ps.setString(1, codigo);
            ps.setArray(2, conn.createArrayOf("text", diferidos));
            rs = ps.executeQuery();

            if (rs.next()) {
                File file = new File(path);
                file.mkdirs();

                String logFile = path + "/anularDiferidos" + new SimpleDateFormat("YYYYMMDD-HHmm").format(new Date()) + ".txt";

                pw = new PrintWriter(new FileWriter(logFile, true), true);
                LogWriter logW = new LogWriter("Anular diferido", LogWriter.INFO, pw);
                logW.tituloInicial("Los siguientes diferidos no se pudieron anular:");

                String[] arr = rs.getString(1).split(",");
                for (String s : arr) {
                    logW.log(s, logW.INFO);
                }
                logW.tituloFinal();
                response = "{\"mensaje\": \"Archivo de log creado, revise su carpeta por favor.\"}";
            }
        } catch (SQLException ex) {
            response = "{\"error\": \"No se pudo anular los diferidos.\"}";
            System.err.println("ERROR EN " + ex.getClass().getSimpleName() + ": " + ex.getErrorCode() + "-" + ex.getSQLState() + "-" + ex.getMessage());
        } catch (IOException ex) {
            response = "{\"error\": \"No se pudo crear el archivo de log\"}";
            System.err.println("ERROR EN " + ex.getClass().getSimpleName() + ": " + ex.getMessage());
        } finally {
            try {
                if (pw != null) {
                    pw.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                response = "\"error\": \"No se pudo anular los diferidos.\"";
            }
        }
        return response;
    }

    @Override
    public String buscarIngreso(String numeroIngreso) {
        JsonObject json = null, detalleJson = new JsonObject();
        JsonArray jsonArray = new JsonArray(), detalleArray = new JsonArray();

        try (Connection conn = conectarJNDIFintra(); PreparedStatement ingresoPs = conn.prepareStatement(obtenerSQL("BUSCAR_INGRESO"));
                PreparedStatement ingresoDetallePs = conn.prepareStatement(obtenerSQL("BUSCAR_INGRESO_DETALLE"))) {
            ingresoPs.setString(1, numeroIngreso);

            ResultSet rs = ingresoPs.executeQuery();

            if (rs.next()) {
                json = new JsonObject();
                json.addProperty("nitcli", rs.getString("nitcli"));
                json.addProperty("num_ingreso", rs.getString("num_ingreso"));
                json.addProperty("estado", rs.getString("estado"));
                json.addProperty("tipo_documento", rs.getString("tipo_documento"));
                json.addProperty("bank_account_no", rs.getString("bank_account_no"));
                json.addProperty("branch_code", rs.getString("branch_code"));
                json.addProperty("descripcion_ingreso", rs.getString("descripcion_ingreso"));
                json.addProperty("vlr_ingreso", rs.getDouble("vlr_ingreso"));
                json.addProperty("periodo", rs.getInt("periodo"));
                json.addProperty("fecha_ingreso", rs.getString("fecha_ingreso"));
                json.addProperty("fecha_consignacion", rs.getString("fecha_consignacion"));
                json.addProperty("transaccion", rs.getInt("transaccion"));
                json.addProperty("cuenta", rs.getString("cuenta"));

                ingresoDetallePs.setString(1, numeroIngreso);
                ResultSet rs2 = ingresoDetallePs.executeQuery();

                while (rs2.next()) {
                    detalleJson.addProperty("referencia_1", rs2.getString("referencia_1"));
                    detalleJson.addProperty("num_ingreso", rs2.getString("num_ingreso"));
                    detalleJson.addProperty("item", rs2.getString("item"));
                    detalleJson.addProperty("valor_ingreso", rs2.getString("valor_ingreso"));
                    detalleJson.addProperty("documento", rs2.getString("documento"));
                    detalleJson.addProperty("descripcion", rs2.getString("descripcion"));
                    detalleJson.addProperty("tipo_documento", rs2.getString("tipo_documento"));
                    detalleJson.addProperty("saldo_factura", rs2.getString("saldo_factura"));
                    detalleJson.addProperty("cuenta", rs2.getString("cuenta"));
                    detalleJson.addProperty("transaccion", rs2.getString("transaccion"));
                    detalleArray.add(detalleJson);

                    detalleJson = new JsonObject();
                }
                json.add("detalle", detalleArray);
                jsonArray.add(json);
            }
            return jsonArray.toString();
        } catch (SQLException e) {
            json = new JsonObject();
            json.addProperty("error", "No se pudo cargar la información. Error: " + e.getSQLState() + "|" + e.getErrorCode());
            jsonArray.add(json);

        }
        return jsonArray.toString();
    }
    
    @Override
    public String anularIngreso(String numeroIngreso, String usuario) {
        JsonObject json = new JsonObject();
        
        try (Connection conn = conectarJNDIFintra(); PreparedStatement ps = conn.prepareStatement(obtenerSQL("ANULAR_INGRESO"))) {
            ps.setString(1, numeroIngreso);
            ps.setString(2, usuario);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                json.addProperty("success", true);
            }
            rs.close();
            
            return json.toString();
        } catch (SQLException e) {
            json.addProperty("success", false);
            json.addProperty("error", "No se pudo cargar la información. Error: " + e.getSQLState() + "|" + e.getErrorCode());
        }
        return json.toString();
    }
    
    public boolean leerArchivoEndosoEdu(InputStream excelFileStream) throws IOException, SQLException {
        Connection conn = null;
        PreparedStatement psNegocios = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String[] datos = null;
            
        Workbook workbook = new Workbook();
        workbook.open(excelFileStream);
        
        Worksheets sheets = workbook.getWorksheets();
        Worksheet base = sheets.getSheet("Base");
        Cells celdas = base.getCells();
        
        try {
            conn = this.conectarJNDI();
            conn.setAutoCommit(false);
            
            psNegocios = conn.prepareStatement(this.obtenerSQL("ELIMINAR_NEGOCIOS"));
            psNegocios.executeUpdate();
            
            psNegocios = conn.prepareStatement(this.obtenerSQL("INSERTAR_NEGOCIOS"));            

            for (int i = 1; i <= celdas.getMaxDataRow(); i++) {
                for (int j = 0; j <= celdas.getMaxDataColumn(); j++) {
                    Cell celdaActual = celdas.getCell(i, j);

                    switch (celdaActual.getValueType()) {
                        case CellValueType.STRING:
                        case CellValueType.RICH_TEXT_STRING:
                            psNegocios.setString(1, celdaActual.getStringValue().trim());
                            break;
                        case CellValueType.INT:
                        case CellValueType.DOUBLE:
                    }
                }
                psNegocios.addBatch();
            }
            psNegocios.executeBatch();
            
            conn.commit();
                                   
            return true;
       } finally {
            if (psNegocios != null) psNegocios.close();
            if (ps != null) ps.close();
            if (conn != null) conn.close();
        }
    } 
}
