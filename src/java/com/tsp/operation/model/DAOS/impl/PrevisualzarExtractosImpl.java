/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.PrevisualzarExtractosDAO;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.PrevisualizarExtractos;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lcanchila
 */
public class PrevisualzarExtractosImpl extends MainDAO implements PrevisualzarExtractosDAO{
    
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";

    public PrevisualzarExtractosImpl(String dataBaseName) {
        super("PrevisualzarExtractos.xml", dataBaseName);
    }

    @Override
    public ArrayList<PrevisualizarExtractos> previsualizarExtractoMicro(String periodo, String accion, String usuario, String unegocio, String[] vencimientos, String ciclo, String fechaHoy) {
        PreparedStatement st = null;
        con = null;
        rs = null;
        int id=0;
        String query = "previsualizarExtractoMicro";
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        String venc = "";
        
        for (int i = 0; i < vencimientos.length; i++) {
            if (!venc.equals("")) {
                venc += ",";
            }
            venc += "'" + vencimientos[i] + "'";
        }

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            query = query.replaceAll("#venc#", venc);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(unegocio));
            ps.setInt(2, Integer.parseInt(unegocio));
            ps.setString(3, periodo);
            ps.setInt(4, Integer.parseInt(ciclo));
            ps.setString(5, fechaHoy);
            ps.setString(6, accion);
            ps.setString(7, usuario);
            
                                  
            rs = ps.executeQuery();
            
            while (rs.next()) {
                PrevisualizarExtractos ext = new PrevisualizarExtractos();
                ext.setIdUndNegocio(rs.getInt("und_neg"));
                ext.setVencimiento_rop(rs.getString("vencimiento_rop"));
                ext.setVenc_mayor(rs.getString("venc_mayor"));
                ext.setNegasoc(rs.getString("negasoc"));
                ext.setNit(rs.getString("nit"));
                ext.setNom_cli(rs.getString("nom_cli"));
                ext.setDireccion(rs.getString("direccion"));
                ext.setBarrio(rs.getString("barrio"));
                ext.setCiudad(rs.getString("ciudad"));
                ext.setDepartamento(rs.getString("departamento"));
                ext.setAgencia(rs.getString("agencia"));
                ext.setLinea_producto(rs.getString("linea_producto"));
                ext.setTotal_cuotas_vencidas(rs.getInt("total_cuotas_vencidas"));
                ext.setCuotas_pendientes(rs.getString("cuotas_pendientes"));
                ext.setMin_dias_ven(rs.getInt("min_dias_ven"));
                ext.setFecha_ultimo_pago(rs.getString("fecha_ultimo_pago"));
                ext.setSubtotal_det(rs.getDouble("subtotal_det"));
                ext.setTotal_sanciones(rs.getDouble("total_sanciones"));
                ext.setTotal_dscto_det(rs.getDouble("total_dscto_det"));
                ext.setTotal_det(rs.getDouble("total_det"));
                ext.setTotal_abonos(rs.getDouble("total_abonos"));
                ext.setObservaciones(rs.getString("observaciones"));
                ext.setMsg_paguese_antes(rs.getString("msg_paguese_antes"));
                ext.setMsg_estado(rs.getString("msg_estado"));
                ext.setCapital(rs.getDouble("capital"));
                ext.setInt_cte(rs.getDouble("int_cte"));
                ext.setCat(rs.getDouble("cat"));
                ext.setInt_mora(rs.getDouble("int_mora"));
                ext.setGxc(rs.getDouble("gxc"));
                ext.setDscto_capital(rs.getDouble("dscto_cap"));
                ext.setDscto_cat(rs.getDouble("dscto_cat"));
                ext.setDscto_int_cte(rs.getDouble("dscto_int_cte"));
                ext.setDscto_int_mora(rs.getDouble("dscto_int_mora"));
                ext.setDscto_gxc(rs.getDouble("dscto_gxc"));
                ext.setSubtotal_corriente(rs.getDouble("subtotal_corriente"));
                ext.setSubtotal_vencido(rs.getDouble("subtotal_vencido"));
                ext.setSubtotal(rs.getDouble("subtotal"));
                ext.setTotal_descuento(rs.getDouble("total_descuento"));
                ext.setTotal(rs.getDouble("total"));
                
                lista.add(ext);
            }

        } catch (Exception e) {
            try {
                throw new Exception("Error en previsualizarExtracto[previsualizarExtracto] " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
           
        }
         return lista;
    }

    @Override
    public ArrayList<PrevisualizarExtractos> previsualizarExtractoFenalco(String periodo, String unegocio, String accion, String usuario, String[] vencimientos, String ciclo, String fechaHoy) {
        con = null;
        rs = null;
        String query = "previsualizarExtractoFenalco";
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        String venc = "";
        
        for (int i = 0; i < vencimientos.length; i++) {
            if (!venc.equals("")) {
                venc += ",";
            }
            venc += "'" + vencimientos[i] + "'";
        }

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            query = query.replaceAll("#venc#", venc);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(unegocio));
            ps.setInt(2, Integer.parseInt(unegocio));
            ps.setString(3, periodo);
            ps.setInt(4, Integer.parseInt(ciclo));
            ps.setString(5, fechaHoy);
            ps.setString(6, accion);
            ps.setString(7, usuario);
                                             
            rs = ps.executeQuery();
            
            while (rs.next()) {
                PrevisualizarExtractos ext = new PrevisualizarExtractos();
                ext.setIdUndNegocio(rs.getInt("und_neg"));
                ext.setVencimiento_rop(rs.getString("vencimiento_rop"));
                ext.setVenc_mayor(rs.getString("venc_mayor"));
                ext.setIdConvenio(rs.getInt("id_convenio"));
                ext.setNegasoc(rs.getString("negasoc"));
                ext.setNit(rs.getString("nit"));
                ext.setNom_cli(rs.getString("nom_cli"));
                ext.setDireccion(rs.getString("direccion"));
                ext.setBarrio(rs.getString("barrio"));
                ext.setCiudad(rs.getString("ciudad"));
                ext.setDepartamento(rs.getString("departamento"));
                ext.setAgencia(rs.getString("agencia"));
                ext.setLinea_producto(rs.getString("linea_producto"));
                ext.setTotal_cuotas_vencidas(rs.getInt("total_cuotas_vencidas"));
                ext.setCuotas_pendientes(rs.getString("cuotas_pendientes"));
                ext.setMin_dias_ven(rs.getInt("min_dias_ven"));
                ext.setFecha_ultimo_pago(rs.getString("fecha_ultimo_pago"));
                ext.setSubtotal_det(rs.getDouble("subtotal_det"));
                ext.setTotal_sanciones(rs.getDouble("total_sanciones"));
                ext.setTotal_dscto_det(rs.getDouble("total_descuento"));
                ext.setTotal_det(rs.getDouble("total_det"));
                ext.setTotal_abonos(rs.getDouble("total_abonos"));
                ext.setObservaciones(rs.getString("observaciones"));
                ext.setMsg_paguese_antes(rs.getString("msg_paguese_antes"));
                ext.setMsg_estado(rs.getString("msg_estado"));
                ext.setCapital(rs.getDouble("capital"));
                ext.setInt_cte(rs.getDouble("int_cte"));
                ext.setSeguros(rs.getDouble("seguro"));
                ext.setInt_mora(rs.getDouble("int_mora"));
                ext.setGxc(rs.getDouble("gxc"));
                ext.setDscto_capital(rs.getDouble("dscto_cap"));
                ext.setDscto_seguro(rs.getDouble("dscto_seguro"));
                ext.setDscto_int_cte(rs.getDouble("dscto_int_cte"));
                ext.setDscto_int_mora(rs.getDouble("dscto_int_mora"));
                ext.setDscto_gxc(rs.getDouble("dscto_gxc"));
                ext.setSubtotal_corriente(rs.getDouble("subtotal_corriente"));
                ext.setSubtotal_vencido(rs.getDouble("subtotal_vencido"));
                ext.setSubtotal(rs.getDouble("subtotalneto"));
                ext.setTotal_descuento(rs.getDouble("descuentos"));
                ext.setTotal(rs.getDouble("total"));
                ext.setEst_comercio(rs.getString("est_comercio"));
                
                lista.add(ext);
            }

        } catch (Exception e) {
            try {
                throw new Exception("Error en previsualizarExtractoFenalco[previsualizarExtractoFenalco] " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
           
        }
         return lista;
    }

    @Override
    public String insertarNegociosExtracto(String negocio, String nit, String periodo, String unegocio, String login, int num_ciclo) {
        StringStatement st = null;
        String sql = "";
        String query = "insertarNegociosExtracto";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setInt(1, Integer.parseInt(unegocio));
            st.setString(2,negocio);
            st.setString(3, nit);
            st.setString(4, periodo);
            st.setInt(5, num_ciclo);
            st.setString(6, login);

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertarNegociosExtracto \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
           } catch (Exception ex) {}
        }
        return sql;
    }

    @Override
    public void limpiarRegistros(String periodo, String unegocio, int num_ciclo) {
        con = null;
        ps = null;
        String query = "limpiarRegistros";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setInt(2, num_ciclo);
            ps.setInt(3, Integer.parseInt(unegocio));

            ps.executeUpdate();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR limpiarRegistros \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }
    }

    @Override
    public boolean isExtractoGenerado(String periodo, String unegocio) {
        con = null;
        ps = null;
        rs = null;
        String query = "isExtractoGenerado";
        String sql = "";
        boolean resp = false;
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setInt(2, Integer.parseInt(unegocio));

            rs = ps.executeQuery();
            
            if(rs.next()){
                resp = true;
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR isExtractoGenerado \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }
        return resp;

    }

    @Override
    public boolean hayRegistrosAGenerar(String periodo, String unegocio, int num_ciclo) {
        con = null;
        ps = null;
        rs = null;
        String query = "hayRegistrosAGenerar";
        String sql = "";
        boolean resp = false;
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setInt(2, num_ciclo);
            ps.setInt(3, Integer.parseInt(unegocio));

            rs = ps.executeQuery();
            
            if(rs.next()){
                resp = true;
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR hayRegistrosAGenerar \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }
        return resp;
    }

    @Override
    public ArrayList<PrevisualizarExtractos> exportarExtractoMicro(String periodo, String unegocio, String vencMayor, String[] vencimientos, String ciclo) {
        con = null;
        rs = null;
        String query = "exportarExtractoMicro";
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        String venc = "";
        
        for (int i = 0; i < vencimientos.length; i++) {
            if (!venc.equals("")) {
                venc += ",";
            }
            venc += "'" + vencimientos[i] + "'";
        }

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            query = query.replaceAll("#venc#", venc);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(unegocio));
            ps.setInt(2, Integer.parseInt(periodo));
            ps.setInt(3, Integer.parseInt(ciclo));
                       
            rs = ps.executeQuery();
            
            while (rs.next()) {
                PrevisualizarExtractos ext = new PrevisualizarExtractos();
                ext.setId(rs.getInt("id"));
                ext.setCod_rop(rs.getString("cod_rop"));
                ext.setCod_rop_barcode(rs.getString("cod_rop_barcode"));
                ext.setGenerado_el(rs.getString("generado_el"));
                ext.setVencimiento_rop(rs.getString("vencimiento_rop"));
                ext.setVenc_mayor(rs.getString("venc_mayor"));
                ext.setNegasoc(rs.getString("negocio"));
                ext.setNit(rs.getString("cedula"));
                ext.setNom_cli(rs.getString("nombre_cliente"));
                ext.setDireccion(rs.getString("direccion"));
                ext.setBarrio(rs.getString("barrio"));
                ext.setCiudad(rs.getString("ciudad"));
                ext.setDepartamento(rs.getString("departamento"));
                ext.setAgencia(rs.getString("agencia"));
                ext.setLinea_producto(rs.getString("linea_producto"));
                ext.setTotal_cuotas_vencidas(rs.getInt("cuotas_vencidas"));
                ext.setCuotas_pendientes(rs.getString("cuotas_pendientes"));
                ext.setMin_dias_ven(rs.getInt("dias_vencidos"));
                ext.setFecha_ultimo_pago(rs.getString("fch_ultimo_pago"));
                ext.setSubtotal_det(rs.getDouble("subtotal_rop"));
                ext.setTotal_sanciones(rs.getDouble("total_sanciones"));
                ext.setTotal_dscto_det(rs.getDouble("total_descuentos"));
                ext.setTotal_det(rs.getDouble("total_rop"));
                ext.setTotal_abonos(rs.getDouble("total_abonos"));
                ext.setObservaciones(rs.getString("observacion"));
                ext.setMsg_paguese_antes(rs.getString("msg_paguese_antes"));
                ext.setMsg_estado(rs.getString("msg_estado_credito"));
                ext.setCapital(rs.getDouble("capital"));
                ext.setCat(rs.getDouble("cat"));
                ext.setInt_cte(rs.getDouble("interes_cte"));
                ext.setInt_mora(rs.getDouble("interes_xmora"));
                ext.setGxc(rs.getDouble("gastos_cobranza"));
                ext.setDscto_capital(rs.getDouble("dscto_capital"));
                ext.setDscto_cat(rs.getDouble("dscto_cat"));
                ext.setDscto_int_cte(rs.getDouble("dscto_interes_cte"));
                ext.setDscto_int_mora(rs.getDouble("dscto_interes_xmora"));
                ext.setDscto_gxc(rs.getDouble("dscto_gastos_cobranza"));
                ext.setSubtotal_corriente(rs.getDouble("subtotal_corriente"));
                ext.setSubtotal_vencido(rs.getDouble("subtotal_vencido"));
                ext.setSubtotal(rs.getDouble("ksubtotalneto"));
                ext.setTotal_descuento(rs.getDouble("kdescuentos"));
                ext.setTotal(rs.getDouble("ktotal"));
                ext.setTelefono(rs.getString("telefono"));
                ext.setEmail(rs.getString("email"));
                ext.setExtracto_email(rs.getString("extracto_email"));
                ext.setPeriodo_generacion_fact(rs.getString("periodo_generacion_fact"));
                ext.setPeriodo_facturacion(rs.getString("periodo_facturacion"));
                ext.setCiclo_facturacion(rs.getInt("ciclo_facturacion"));
                ext.setPeriodo_desembolso(rs.getString("periodo_desembolso"));
                
                lista.add(ext);
            }

        } catch (Exception e) {
            try {
                throw new Exception("Error en exportarExtractoMicro[exportarExtractoMicro] " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
           
        }
         return lista;
    }

    @Override
    public ArrayList<PrevisualizarExtractos> exportarExtractoFenalco(String periodo, String unegocio, String vencMayor, String[] vencimientos, String ciclo) {
        con = null;
        rs = null;
        String query = "exportarExtractoFenalco";
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        String venc = "";
        
        for (int i = 0; i < vencimientos.length; i++) {
            if (!venc.equals("")) {
                venc += ",";
            }
            venc += "'" + vencimientos[i] + "'";
        }

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            query = query.replaceAll("#venc#", venc);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(unegocio));
            ps.setInt(2, Integer.parseInt(periodo));
            ps.setInt(3, Integer.parseInt(ciclo));
                       
            rs = ps.executeQuery();
            
            while (rs.next()) {
                PrevisualizarExtractos ext = new PrevisualizarExtractos();
                ext.setId(rs.getInt("id"));
                ext.setCod_rop(rs.getString("cod_rop"));
                ext.setCod_rop_barcode(rs.getString("cod_rop_barcode"));
                ext.setGenerado_el(rs.getString("generado_el"));
                ext.setVencimiento_rop(rs.getString("vencimiento_rop"));
                ext.setVenc_mayor(rs.getString("venc_mayor"));
                ext.setNegasoc(rs.getString("negocio"));
                ext.setNit(rs.getString("cedula"));
                ext.setNom_cli(rs.getString("nombre_cliente"));
                ext.setDireccion(rs.getString("direccion"));
                ext.setBarrio(rs.getString("barrio"));
                ext.setCiudad(rs.getString("ciudad"));
                ext.setDepartamento(rs.getString("departamento"));
                ext.setAgencia(rs.getString("agencia"));
                ext.setLinea_producto(rs.getString("linea_producto"));
                ext.setTotal_cuotas_vencidas(rs.getInt("cuotas_vencidas"));
                ext.setCuotas_pendientes(rs.getString("cuotas_pendientes"));
                ext.setMin_dias_ven(rs.getInt("dias_vencidos"));
                ext.setFecha_ultimo_pago(rs.getString("fch_ultimo_pago"));
                ext.setSubtotal_det(rs.getDouble("subtotal_rop"));
                ext.setTotal_sanciones(rs.getDouble("total_sanciones"));
                ext.setTotal_dscto_det(rs.getDouble("total_descuentos"));
                ext.setTotal_det(rs.getDouble("total_rop"));
                ext.setTotal_abonos(rs.getDouble("total_abonos"));
                ext.setObservaciones(rs.getString("observacion"));
                ext.setMsg_paguese_antes(rs.getString("msg_paguese_antes"));
                ext.setMsg_estado(rs.getString("msg_estado_credito"));
                ext.setCapital(rs.getDouble("capital"));
                ext.setSeguros(rs.getDouble("seguro"));
                ext.setInt_cte(rs.getDouble("interes_cte"));
                ext.setInt_mora(rs.getDouble("interes_xmora"));
                ext.setGxc(rs.getDouble("gastos_cobranza"));
                ext.setDscto_capital(rs.getDouble("dscto_capital"));
                ext.setDscto_seguro(rs.getDouble("dscto_seguro"));
                ext.setDscto_int_cte(rs.getDouble("dscto_interes_cte"));
                ext.setDscto_int_mora(rs.getDouble("dscto_interes_xmora"));
                ext.setDscto_gxc(rs.getDouble("dscto_gastos_cobranza"));
                ext.setSubtotal_corriente(rs.getDouble("subtotal_corriente"));
                ext.setSubtotal_vencido(rs.getDouble("subtotal_vencido"));
                ext.setSubtotal(rs.getDouble("ksubtotalneto"));
                ext.setTotal_descuento(rs.getDouble("kdescuentos"));
                ext.setTotal(rs.getDouble("ktotal"));
                ext.setEst_comercio(rs.getString("est_comercio"));
                ext.setTelefono(rs.getString("telefono"));
                ext.setEmail(rs.getString("email"));
                ext.setExtracto_email(rs.getString("extracto_email"));
                ext.setPeriodo_generacion_fact(rs.getString("periodo_generacion_fact"));
                ext.setPeriodo_facturacion(rs.getString("periodo_facturacion"));
                ext.setCiclo_facturacion(rs.getInt("ciclo_facturacion"));
                ext.setPeriodo_desembolso(rs.getString("periodo_desembolso"));
                
                lista.add(ext);
            }

        } catch (Exception e) {
            try {
                throw new Exception("Error en exportarExtractoFenalco[exportarExtractoFenalco] " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
           
        }
         return lista;
    }

    @Override
    public int obtenerNumCicloPago(String fechaHoy) {
        con = null;
        ps = null;
        String query = "obtenerNumCicloPago";
        String sql="";
        int numero = 0;
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, fechaHoy);
            
            rs = ps.executeQuery();
            
            if(rs.next()){
                numero = rs.getInt("num_ciclo");
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR obtenerNumCicloPago \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
           } catch (Exception ex) {}
        }
        return numero;
    }

    @Override
    public ArrayList<CmbGeneralScBeans> cargarPeriodos() {
        con = null;
        ps = null;
        String query = "cargarPeriodos";
        String sql="";
        ArrayList combo = null;
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            
            rs = ps.executeQuery();
            combo = new ArrayList();
            while (rs.next()) {
                CmbGeneralScBeans cmb = new CmbGeneralScBeans();
                cmb.setIdCmb(rs.getInt("id"));
                cmb.setDescripcionCmb(rs.getString("descripcion"));
                combo.add(cmb);
            }
            
        } catch (Exception e) {
            try {
                throw new SQLException("ERROR cargarPeriodos \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
           } catch (Exception ex) {}
        }
        return combo;
    }

    @Override
    public String obtenerFechaMaximaCiclo(String fechaHoy) {
        con = null;
        ps = null;
        String query = "obtenerFechaMaximaCiclo";
        String sql="";
        String fecha = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, fechaHoy);
            
            rs = ps.executeQuery();
            
            if(rs.next()){
                fecha = rs.getString("fecha_fin");
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR obtenerFechaMaximaCiclo \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
           } catch (Exception ex) {}
        }
        return fecha;
    }

    @Override
    public String tomarFotoCiclo(String periodo, String ciclo) {
        con = null;
        ps = null;
        rs = null;
        String query = "tomarFotoCiclo";
        String sql = "";
        String resp = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setInt(2, Integer.parseInt(ciclo));

            rs = ps.executeQuery();
            if (rs.next()) {
                resp = rs.getString(1);
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR tomarFotoCiclo \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public boolean isMIGenerado() {
        con = null;
        ps = null;
        rs = null;
        String query = "isMIGenerado";
        String sql = "";
        boolean resp = false;
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            
            rs = ps.executeQuery();
            if (rs.next()) {
                resp = true;
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR isMIGenerado \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public boolean isFotoGenerada(String periodo, String ciclo) {
       con = null;
        ps = null;
        rs = null;
        String query = "isFotoGenerada";
        String sql = "";
        boolean resp = false;
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setString(2, periodo);
            ps.setInt(3, Integer.parseInt(ciclo));
            
            rs = ps.executeQuery();
            if (rs.next()) {
                resp = true;
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR isFotoGenerada \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }
    
    @Override
    public boolean isSancionesGeneradas(String periodo, String tipo) {
        con = null;
        ps = null;
        rs = null;
        String query = "isSancionesGeneradas";
        String sql = "";
        boolean resp = false;
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setInt(2, Integer.parseInt(tipo));
            
            rs = ps.executeQuery();
            if (rs.next()) {
                resp = true;
            }

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR isSancionesGeneradas \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String copiarSancionesMes(String periodo, String login) {
        con = null;
        ps = null;
        String query = "copiarSancionesMes";
        String sql = "";
        String PeriodoAnterior = "";
        String resp = "OK";
        try {
             String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setString(2, login);
            ps.setString(3, login);
            ps.setString(4, PeriodoAnterior);
            
            ps.executeUpdate();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR copiarSancionesMes \n" + e.getMessage());
            } catch (SQLException ex) {
                resp= "Error";
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
       }
        return resp;
    }
    

    @Override
    public ArrayList<PrevisualizarExtractos> obtenerDetalleSanciones(String periodo, String tipo, String[] listUnd) {
        con = null;
        rs = null;
        String PeriodoAnterior="";
        String query = "obtenerDetalleSanciones";
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        String und = "";
        
        for (int i = 0; i < listUnd.length; i++) {
            if (!und.equals("")) {
                und += ",";
            }
            und += "'" + listUnd[i] + "'";
        }

        try {
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            query = query.replaceAll("#und#", und);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(PeriodoAnterior));
            ps.setInt(2, Integer.parseInt(tipo));
                       
            rs = ps.executeQuery();
            
            while (rs.next()) {
                PrevisualizarExtractos ext = new PrevisualizarExtractos();
                ext.setId_sancion(rs.getInt("id"));
                ext.setUnd_negocio(rs.getString("und_neg"));
                ext.setId_concepto_recaudo(rs.getInt("id_conceptos_recaudo"));
                ext.setConcepto_recaudo(rs.getString("concepto"));
                ext.setCategoria(rs.getString("categoria"));
                ext.setDescripcion_sancion(rs.getString("sancion"));
                ext.setRango_ini(rs.getInt("dias_rango_ini"));
                ext.setRango_fin(rs.getInt("dias_rango_fin"));
                ext.setPorcentaje(rs.getString("porcentaje"));
                
                lista.add(ext);
            }

        } catch (Exception e) {
            try {
                throw new Exception("Error en obtenerDetalleSanciones " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
           
        }
         return lista;
    }

    @Override
    public ArrayList<String> insertSancionesMes(String login, String periodo, ArrayList<PrevisualizarExtractos> lista, String tipo) {
        StringStatement st = null;
        String sql = "";
        ArrayList<String> listSql = new ArrayList<String>();
        String query = "insertSancionesMes";
        try {
            for (int i = 0; i < lista.size(); i++) {
                
                st = new StringStatement(this.obtenerSQL(query));
                st.setString(lista.get(i).getUnd_negocio());
                st.setInt(Integer.parseInt(tipo));
                st.setInt(lista.get(i).getId_concepto_recaudo());
                st.setString(periodo);
                st.setString(lista.get(i).getCategoria());
                st.setString(lista.get(i).getDescripcion_sancion());
                st.setInt(lista.get(i).getRango_ini());
                st.setInt(lista.get(i).getRango_fin());
                st.setString(lista.get(i).getPorcentaje());
                st.setString(login);
                st.setString(login);

                listSql.add(st.getSql());
            }
        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertSancionesMes \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
           } catch (Exception ex) {}
        }
        return listSql;
    }

    @Override
    public ArrayList<PrevisualizarExtractos> obtenerDetalleCondonaciones(String periodo, String tipo, String[] listUnd) {
        con = null;
        rs = null;
        String PeriodoAnterior="";
        String query = "obtenerDetalleCondonaciones";
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        String und = "";
        
        for (int i = 0; i < listUnd.length; i++) {
            if (!und.equals("")) {
                und += ",";
            }
            und += "'" + listUnd[i] + "'";
        }

        try {
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            query = query.replaceAll("#und#", und);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(tipo));
            ps.setInt(2, Integer.parseInt(tipo));
                       
            rs = ps.executeQuery();
            
            while (rs.next()) {
                PrevisualizarExtractos ext = new PrevisualizarExtractos();
                ext.setId_sancion(rs.getInt("id"));
                ext.setUnd_negocio(rs.getString("und_neg"));
                ext.setId_concepto_recaudo(rs.getInt("id_conceptos_recaudo"));
                ext.setConcepto_recaudo(rs.getString("concepto"));
                ext.setCategoria(rs.getString("categoria"));
                ext.setDescripcion_sancion(rs.getString("sancion"));
                ext.setId_aplicado(rs.getInt("aplicado_a"));
                ext.setAplicado_a(rs.getString("concep"));
                ext.setRango_ini(rs.getInt("dias_rango_ini"));
                ext.setRango_fin(rs.getInt("dias_rango_fin"));
                ext.setPorcentaje(rs.getString("porcentaje"));
                
                lista.add(ext);
            }

        } catch (Exception e) {
            try {
                throw new Exception("Error en obtenerDetalleCondonaciones " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
           
        }
         return lista;
    }

    @Override
    public ArrayList insertCondonacionesMes(String login, String periodo, ArrayList<PrevisualizarExtractos> lista, String tipo) {
        StringStatement st = null;
        String sql = "";
        ArrayList<String> listSql = new ArrayList<String>();
        String query = "insertCondonacionesMes";
        try {
            for (int i = 0; i < lista.size(); i++) {
                
                st = new StringStatement(this.obtenerSQL(query));
                st.setString(lista.get(i).getUnd_negocio());
                st.setInt(Integer.parseInt(tipo));
                st.setInt(lista.get(i).getId_concepto_recaudo());
                st.setString(periodo);
                st.setString(lista.get(i).getCategoria());
                st.setString(lista.get(i).getDescripcion_sancion());
                st.setInt(lista.get(i).getId_aplicado());
                st.setInt(lista.get(i).getRango_ini());
                st.setInt(lista.get(i).getRango_fin());
                st.setString(lista.get(i).getPorcentaje());
                st.setString(login);
                st.setString(login);

                listSql.add(st.getSql());
            }
        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertCondonacionesMes \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(PrevisualzarExtractosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
           } catch (Exception ex) {}
        }
        return listSql;
    }

    @Override
    public String tomarFotoMes() {        
        con = null;
        ps = null;
        rs = null;
        String query = "TOMAR_FOTO_MES";
        String sql = "";
        String resp = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);        
            rs = ps.executeQuery();
            if (rs.next()) {
                resp = rs.getString("retorno");
            }

        } catch (Exception e) {
            resp="Lo sentimos algo salio mal al tomar la foto";
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.err.println("SQLException :"+ex.getMessage());
            }
        }
        return resp;
    }

    
}
