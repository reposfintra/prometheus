/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS.impl;

import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.DAOS.fintralogisticaAdministrativoDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.fintralogisticaAdministrativoBeans;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mariana
 */
public class fintralogisticaAdministrativoImpl extends MainDAO implements fintralogisticaAdministrativoDAO {

    public fintralogisticaAdministrativoImpl(String dataBaseName) {
        super("fintralogisticaAdministrativoDAO.xml", dataBaseName);
    }

    @Override
    public ArrayList<fintralogisticaAdministrativoBeans> cargarReporte(String ventas, String planilla, String corrida, String fecha_corrida, String empresa, String ingreso) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_REPORTE";

        ArrayList<fintralogisticaAdministrativoBeans> lista = new ArrayList<>();
        try {
            String consulta;
            con = this.conectarJNDI();
            String condicion = "";

            if (!fecha_corrida.equals("")) {
                condicion = " fecha_corrida::date = '" + fecha_corrida + "'::date ";
            } else {
                condicion = " fecha_corrida::date <= now()::date ";
            }

            if (!ventas.equals("")) {
                condicion = condicion + " and tiene_venta = " + ventas + " ";
            }

            if (!planilla.equals("")) {
                condicion = condicion + " and planilla = '" + planilla + "' ";
            }

            if (!corrida.equals("")) {
                condicion = condicion + " and cxc_corrida= '" + corrida + "' ";
            }

            if (!empresa.equals("")) {
                condicion = condicion + " and tabla.id_transportadora = '" + empresa + "' ";
            }
            if (ingreso.equals("si")) {
                condicion = condicion + " and tabla.num_ingreso_ultimo_ingreso!='' ";
            } else if (ingreso.equals("no")) {
                condicion = condicion + " and tabla.num_ingreso_ultimo_ingreso ='' ";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();

            while (rs.next()) {
                fintralogisticaAdministrativoBeans beansAdmin = new fintralogisticaAdministrativoBeans();
                beansAdmin.setEmpresa(rs.getString("razon_social"));
                beansAdmin.setPlanilla(rs.getString("planilla"));
                beansAdmin.setFecha_anticipo(rs.getString("fecha_anticipo"));
                beansAdmin.setVenta(rs.getString("tiene_venta").equals("f") ? "No" : "Si");
                beansAdmin.setFecha_venta(rs.getString("fecha_venta"));
                beansAdmin.setReanticipo(rs.getString("reanticipo").equals("N") ? "No" : "Si");                
                beansAdmin.setFecha_corrida(rs.getString("fecha_corrida"));
                beansAdmin.setCxc_corrida(rs.getString("cxc_corrida"));
                beansAdmin.setDocumento(rs.getString("documento"));
                beansAdmin.setReferencia1(rs.getString("referencia_1"));
                beansAdmin.setTipdoc_ultimoingreso(rs.getString("tipo_documento_ultimo_ingreso"));
                beansAdmin.setNumingreso_ultimoingreso(rs.getString("num_ingreso_ultimo_ingreso"));

                lista.add(beansAdmin);
            }
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
            return lista;
        }
    }

    @Override
    public ArrayList<fintralogisticaAdministrativoBeans> generarIAPendientes(String empresa, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_IA_PENDIENTES";
        ArrayList<fintralogisticaAdministrativoBeans> lista = new ArrayList<>();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(empresa));
            rs = ps.executeQuery();

            while (rs.next()) {
                fintralogisticaAdministrativoBeans beansAdmin = new fintralogisticaAdministrativoBeans();
                beansAdmin.setEmpresa(rs.getString("razon_social"));
                beansAdmin.setFecha_corrida(rs.getString("fecha_corrida"));
                beansAdmin.setCxc_corrida(rs.getString("cxc_corrida"));
                beansAdmin.setRespuesta(generarIA(rs.getString("fecha_corrida"), usuario.getLogin()));
                lista.add(beansAdmin);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
            }
            return lista;
        }
    }

    private String generarIA(String fecha_corrida, String usuario) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GENERAR_IA";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, fecha_corrida);
            ps.setString(2, usuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getBoolean("retorno")?"SI":"NO";
            }

        } catch (Exception ex) {
            respuesta = ex.getMessage();
            ex.getMessage();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                e.getMessage();
            }

        }
        return respuesta;
    }
}
