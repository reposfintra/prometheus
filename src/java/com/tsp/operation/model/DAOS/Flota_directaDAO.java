/************************************************************************
 * Nombre clase: Flota_directaDAO.java                                  *
 * Descripci�n: Clase que maneja las consultas de las flotas directas.  *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: 2 de diciembre de 2005, 08:29 AM                              *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class Flota_directaDAO {
    private Flota_directa flota_directa;
    private Vector vecflota_directa;
    private static final String SQL_INSERT = "insert into flota_directa (nitpro, placa, rec_status, last_update, user_update, creation_date, creation_user, base,distrito) values "+
    "(?,?,'','now()',?, 'now()',?,?,?)";
    
    private static final String SQL_SERCH = "select * from flota_directa where nitpro=? and placa=?  and rec_status != 'A'";
    
    private static final String SQL_LIST = "select * from flota_directa where rec_status != 'A' order by nit";
    
    private static final String SQL_UPDATE = "update flota_directa set nitpro=?, placa=?, last_update='now()', user_update=?, rec_status='' where nitpro = ? and placa=?";
    
    private static final String SQL_ANULAR = "update flota_directa set rec_status='A',last_update='now()', user_update=? where nitpro = ? and placa=?";
    
    private static final String SQL_SEARCH_DETALLES = "Select * from flota_directa where nitpro like ? and placa like ? and rec_status != 'A' order by placa";
    /** Creates a new instance of Flota_directaDAO */
    public Flota_directaDAO () {
    }
    /**
     * Metodo: getFlota_directa, permite retornar un objeto de registros de flota directa.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Flota_directa getFlota_directa () {
        return flota_directa;
    }
    
    /**
     * Metodo: setFlota_directa, permite obtener un objeto de registros de flota directa.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setFlota_directa (Flota_directa flota_directa) {
        this.flota_directa = flota_directa;
    }
    
    /**
     * Metodo: getFlota_directas, permite retornar un vector de registros de flota directa.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getFlota_directas () {
        return vecflota_directa;
    }
    
    /**
     * Metodo: setFlota_directas, permite obtener un vector de registros de flota directa.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setFlota_directas (Vector vecflota_directa) {
        this.vecflota_directa = vecflota_directa;
    }
    
    /**
     * Metodo: insertFlota_directa, permite ingresar un registro a la tabla flota_directa.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertFlota_directa () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_INSERT);
                st.setString (1,flota_directa.getNit ());
                st.setString (2,flota_directa.getPlaca ());
                st.setString (3,flota_directa.getUsuario_creacion ());
                st.setString (4,flota_directa.getUsuario_modificacion ());
                st.setString (5,flota_directa.getBase ());
                st.setString (6,flota_directa.getDistrito ());
                ////System.out.println(st);
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA INSERCION DE LA FLOTA DIRECTA " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: searchFlota_directa, permite buscar una flota directa dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */
    public void searchFlota_directa (String nit, String placa)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (SQL_SERCH);
                st.setString (1,nit);
                st.setString (2,placa);
                rs= st.executeQuery ();
                vecflota_directa = new Vector ();
                while(rs.next ()){
                    flota_directa = new Flota_directa ();
                    flota_directa.setNit (rs.getString ("nitpro"));
                    flota_directa.setPlaca (rs.getString ("placa"));
                    vecflota_directa.add (flota_directa);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DE LA FLOTA DIRECTA " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: existFlota_directa, permite buscar una flota directa dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */
    public boolean existFlota_directa (String nit, String placa) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_SERCH);
                st.setString (1,nit);
                st.setString (2,placa);
                rs = st.executeQuery ();
                if (rs.next ()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA BUSQUEDA DE LA FLOTA DIRECTA " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Metodo: listFlota_directa, permite listar todas las flotas directas
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listFlota_directa ()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (SQL_LIST);
                rs= st.executeQuery ();
                vecflota_directa = new Vector ();
                while(rs.next ()){
                    flota_directa = new Flota_directa ();
                    flota_directa.setNit (rs.getString ("nitpro"));
                    flota_directa.setPlaca (rs.getString ("placa"));
                    vecflota_directa.add (flota_directa);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DE LAS FLOTAS DIRECTAS " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: updateFlota_directa, permite actualizar una flota directa dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario, la placa y usuario.
     * @version : 1.0
     */
    public void updateFlota_directa (String nnit, String nplaca, String usuario,String nit, String placa) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_UPDATE);
                st.setString (1,nnit);
                st.setString (2,nplaca);
                st.setString (3,usuario);
                st.setString (4,nit);
                st.setString (5,placa);
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA MODIFICACI�N DE LA FLOTA DIRECTA " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: anularFlota_directa, permite anular una flota directa.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void anularFlota_directa () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_ANULAR);
                st.setString (1,flota_directa.getUsuario_modificacion ());
                st.setString (2,flota_directa.getNit ());
                st.setString (3,flota_directa.getPlaca ());
                st.executeUpdate ();
            }
        }
        catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA ANULACION DE LA FLOTA DIRECTA " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: searchDetalleFlota_directa, permite listar flotas directas por detalles dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */    
    public void searchDetalleFlota_directa (String nit, String placa) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vecflota_directa=null;
        
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            boolean sw = false;
            if(con!=null){
                st = con.prepareStatement (SQL_SEARCH_DETALLES);
                st.setString (1, nit+"%");
                st.setString (2, placa+"%");
                rs = st.executeQuery ();
                vecflota_directa = new Vector ();
                while(rs.next ()){
                    flota_directa = new Flota_directa ();
                    flota_directa.setNit (rs.getString ("nitpro"));
                    flota_directa.setPlaca (rs.getString ("placa"));
                    vecflota_directa.add (flota_directa);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA BUSQUEDA DE LAS FLOTAS DIRECTAS " + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
    }
}
