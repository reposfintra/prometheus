/***********************************************************************************
 * Nombre clase :  IdentidadDAO.java
 * Descripcion :   Clase que maneja los DAO ( Data Access Object )
 *                 los cuales contienen los metodos que interactuan
 *                 con la BD.
 * Autor :         Ing. Diogenes Antonio Bastidas Morales
 * Fecha :         16 de julio de 2005, 06:23 PM
 * Version :  1.0
 * Copyright : Fintravalores S.A.
 ***********************************************************************************/


package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.text.*;


public class IdentidadDAO extends MainDAO {
    
    /** Creates a new instance of IdentidadDAO */
    public IdentidadDAO() {
        super("IdentidadDAO.xml");//JJCastro fase2
    }
    public IdentidadDAO(String dataBaseName) {
        super("IdentidadDAO.xml", dataBaseName);//JJCastro fase2
    }
    private Identidad identidad;
    private Vector VecIdentidad;
    
    private List LstIdentidad;
    //HENRY 151005
    private Tenedor tenedor;
    private Proveedor prov;
    
    private static final  String listar = "select *             "+
    "from                 "+
    "       nit           "+
    " where               "+
    "       estado <> 'I'   " +
    "    order by nombre1,apellido1  ";
    
    private static final  String existe = " select *         "+
    " from             "+
    "    nit     "+
    " where            "+
    "       cedula = ? ";
    
    private static final String modificar = "update               "+
    "      nit            "+
    "    set              "+
    "       id_mims = ?,  "+
    "       nombre1 = ?,   " +
    "       nombre2 = ?,   " +
    "       apellido1 = ?, " +
    "       apellido2 = ?, "+
    "       direccion = ?,"+
    "       codciu = ?,   "+
    "       coddpto = ?,  "+
    "       codpais = ?,  "+
    "       telefono = ?, "+
    "       celular = ?,  "+
    "       e_mail = ?,   "+
    "       fechaultact = 'now()', " +
    "       usuario = ?,   "+
    "       sexo = ?,     "+
    "       fechanac = ?, "+
    "       base = ?,     "+
    "       telefono1 = ?,"+
    "       cargo = ?,    "+
    "       ref1 = ?,     "+
    "       tipo_iden = ?,"+
    "       observacion = ?," +
    "       nombre = ?,     " +
    "       est_civil = ?,  " +
    "       lugarnac = ?,   " +
    "       barrio = ?,     " +
    "       libmilitar = ?,  "+
    "       senalparti = ?," +
    "       expced = ?," +
    "       nemotecnico = ?," +
    "       clasificacion = ?," +
    "       veto = ?    "+
    "    where            "+
    "          cedula = ? ";
    
    
    private static final  String existeanulado = " select *         "+
    " from             "+
    "    nit           "+
    " where            "+
    "       cedula = ? " +
    "  and estado = 'I'  ";
    
     //idevia 19.04.07
    private static final String SQL_IDENTIDAD_NOAPROBADAS = "SELECT a.cedula, a.nombre, get_nombreusuario(a.usuariocrea) AS usuariocrea " +
    "FROM    " +
    "  nit a,   " +
    "  usuarios b  " +
    "WHERE  " +
    "       a.usuario_aprobacion = '' " +
    "   AND upper(b.idusuario) = upper(a.usuariocrea)  " +
    "   AND b.id_agencia IN " +
    "   (SELECT id_agencia FROM usuario_aprobacion WHERE usuario_aprobacion = ? AND tabla = 'NIT' ) " +
    "   ORDER BY a.cedula";

    
    
    private static final  String anular = " update               "+
    "       nit            "+
    "  set                 "+
    "      estado = 'I',   "+
    "      usuario = ?,    "+
    "      fechaultact ='now()'"+
    " where                "+
    "       cedula = ?     ";
    
    
    private static final String activar = "update               "+
    "      nit            "+
    "    set              "+
    "       estado = 'I',  "+
    "       id_mims = ?,  "+
    "       nombre1 = ?,   " +
    "       nombre2 = ?,   " +
    "       apellido1 = ?, " +
    "       apellido2 = ?, "+
    "       direccion = ?,"+
    "       codciu = ?,   "+
    "       coddpto = ?,  "+
    "       codpais = ?,  "+
    "       telefono = ?, "+
    "       celular = ?,  "+
    "       e_mail = ?,   "+
    "       fechaultact = 'now()', " +
    "       usuario = ?,   "+
    "       sexo = ?,     "+
    "       fechanac = ?, "+
    "       base = ?,     "+
    "       telefono1 = ?,"+
    "       cargo = ?,    "+
    "       ref1 = ?,     "+
    "       tipo_iden = ?,"+
    "       observacion = ?, "+
    "       nombre = ?,   " +
    "       cia = ?,      " +
    "       est_civil = ?,  " +
    "       lugarnac = ?,   " +
    "       barrio = ?,     " +
    "       libmilitar = ?,  " +
    "       senalparti  =?,   " +
    "       expced = ?," +
    "       nemotecnico = ?," +
    "       clasificacion = ?," +
    "       veto = ?       "+
    "    where            "+
    "          cedula = ? ";
    
    
    private static final String listarXbusq = " SELECT a.*," +
    "       B.nomciu as ciudad," +
    "       C.country_name as pais" +
    "     FROM" +
    "     (SELECT cedula, nombre, direccion, codpais, codciu, telefono, telefono1, cia, aprobado,estado  " +
    "           FROM                          " +
    "            nit  i                       " +
    "     WHERE                           " +
    "         i.cedula like ?               " +
    "     AND i.nombre like ?" +
    "     AND i.cia like ?" +
    "     ORDER BY i.nombre)a " +
    "LEFT JOIN ciudad B  ON (B.codciu = a.codciu) " +
    "LEFT JOIN pais C  ON (C.country_code = A.codpais)";
    
    private static final String insertar ="insert into nit ( estado, cedula, id_mims, nombre1,nombre2,apellido1,apellido2, direccion, codciu, coddpto, codpais, telefono, celular, e_mail, fechaultact, usuario, fechacrea,  usuariocrea, sexo, fechanac, base, telefono1, cargo, cia, ref1, tipo_iden, observacion, nombre,est_civil,lugarnac,barrio,libmilitar,senalparti,expced,nemotecnico,clasificacion,veto ) "+
    " values ( 'I', ?, ?, ? , ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'now()', ?, 'now()', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,? )";
    
    private static final  String per_natural = " select *         "+
    " from             "+
    "    nit     "+
    " where            "+
    "       cedula = ? " +
    "  and  tipo_iden = 'CED' ";
    
    private static final String ver= "select cedula from nit where nemotecnico = ? and  nemotecnico <> ''";
    
    private static final String SQL_METADATA = "select * from nit limit(0)";
    
    
    private static final String SQL_APROBACION ="UPDATE nit " +
    "SET estado = 'A', " +
    "    aprobado = ? , " +
    "    usuario_aprobacion = ?, " +
    "    fecha_aprobacion = 'now()' " +
    "WHERE " +
    "   cedula = ? ";
    
    private static final  String SQL_NITXTID =  "   SELECT  cedula "+
    "                                               FROM    nit      "+
    "                                               WHERE   cedula = ?  "+
    "                                                       AND tipo_iden = ? " +
    "                                                       AND estado <> 'I' ";
    
    
    //sescalante 01.02.06
    private static final String SQL_NITXNIT = " SELECT  cedula, " +
    "                                                   nombre " +
    "                                           FROM    nit " +
    "                                           WHERE   tipo_iden = 'NIT' " +
    "                                                   AND estado = 'A' " +
    "                                                   AND nombre LIKE ?";
    //sescalante 01.02.06
    private static final String SQL_NITXCED = "	SELECT	cedula, "+
    "							nombre  "+
    "					       	FROM 	nit "+
    "						WHERE   tipo_iden = 'CED' " +
    "                                                   AND estado = 'A' " +
    "                                                   AND nombre LIKE ?";
    

    private static final String SQL_TENEDOR =
    "SELECT distinct" +
    "      cedula,  " +
    "      direccion, " +
    "      coalesce (get_nombreciudad(expced ), 'NO REGISTRA' ) as expced,  " +
    "      coalesce (get_nombreciudad(codciu),  'NO REGISTRA' ) as ciudad, " +
    "      coalesce (get_nombrepais(codpais), 'NO REGISTRA' ) as pais, " +
    "      telefono, " +
    "      celular,   " +
    "      CASE WHEN nombre1 = '' then   " +
    "            nombre" +
    "      ELSE " +
    "           nombre1" +
    "      END as nombre1,  " +
    "      nombre2,  " +
    "      coalesce ( apellido1, '') as apellido1,  " +
    "      coalesce ( apellido2, '') as apellido2, " +
    "      nombre      " +
    "    from  " +
    "        nit " +
    "    where  " +
    "       cedula=?  ";

     private static final String SQL_PROPIETARIO ="select distinct " +
    "      n.cedula,  " +
    "      n.direccion, " +
    "      coalesce (get_nombreciudad(n.expced ), 'NO REGISTRA' ) as expced,  " +
    "      coalesce (get_nombreciudad(n.codciu),  'NO REGISTRA' ) as ciudad,  " +
    "      coalesce (get_nombrepais(codpais), 'NO REGISTRA' ) as pais, " +
    "      n.telefono, " +
    "      n.celular,  " +
    "      CASE WHEN n.nombre1 = '' then" +
    "            n.nombre " +
    "      ELSE " +
    "          n.nombre1       " +
    "      END as nombre1  ,  " +
    "      n.nombre2,  " +
    "      n.apellido1,  " +
    "      n.apellido2, " +
    "      coalesce (get_nombreciudad(p.agency_id), 'NO REGISTRA' ) as sedepago," +
    "      n.nombre," +
    "      coalesce (n.veto,'') as veto  " +
    "  FROM    " +
    "      nit n,   " +
    "      proveedor p" +
    "  WHERE  " +
    "      cedula=? " +
    "  AND p.nit = n.cedula  ";



public void buscarPropietarioxCedula(String cedula)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        prov = null;
        String query = "SQL_PROPIETARIO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,cedula);
                rs = st.executeQuery();
                if(rs.next()){
                    prov = new Proveedor();
                    prov.setC_nit(rs.getString("cedula"));
                    prov.setNom1(rs.getString("nombre1"));
                    prov.setNom2(rs.getString("nombre2"));
                    prov.setApe1(rs.getString("apellido1"));
                    prov.setApe2(rs.getString("apellido2"));
                    prov.setC_agency_id(rs.getString("sedepago"));
                    prov.setCiudad(rs.getString("ciudad"));
                    prov.setExpced(rs.getString("expced"));
                    prov.setDireccion(rs.getString("direccion"));
                    prov.setTelefono(rs.getString("telefono"));
                    prov.setCelular(rs.getString("celular"));
                    prov.setNombre(rs.getString("nombre"));
                    prov.setPais(rs.getString("pais"));
                    prov.setVeto(rs.getString("veto"));
                }
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DEL PROPIETARIO X CEDULA NIT" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo buscarIdentidadxTid, busca identidades dado unos parametros
     * Autor Sandra Escalante
     * @param String ced (cedula), String tid (tipo de identidad)
     */
    
    public boolean buscarIdentidadxTid( String ced, String tid ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean res = false;
        String query = "SQL_NITXTID";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ced );
                st.setString(2, tid );
                rs = st.executeQuery();
                
                if (rs.next()){
                    res = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("Error : IdentidadDAO.buscarIdentidadxTid /n" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return res;
    }
    
    /**
     * Metodo busquedaxNit, busca identidades NIT
     * Autor Sandra Escalante
     * @param String frase
     */
    public void busquedaxNit(String frase) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        VecIdentidad = null;
        String query = "SQL_NITXNIT";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, frase);
                rs = st.executeQuery();
                
                VecIdentidad =  new Vector();
                
                while (rs.next()){
                    identidad = new Identidad();
                    identidad.setCedula(rs.getString("cedula"));
                    identidad.setNombre(rs.getString("nombre"));
                    VecIdentidad.add(identidad);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo busquedaxCedula, busca identidades CED
     * Autor Sandra Escalante
     * @param String frase
     */
    public void busquedaxCedula(String frase) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        VecIdentidad = null;
        String query = "SQL_NITXCED";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, frase);
                rs = st.executeQuery();
                
                VecIdentidad =  new Vector();
                
                while (rs.next()){
                    identidad = new Identidad();
                    identidad.setCedula(rs.getString("cedula"));
                    identidad.setNombre(rs.getString("nombre"));
                    VecIdentidad.add(identidad);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo setIdentidad, setea el objeto identidad
     * @param: objeto Identidad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void  setIdentidad(Identidad identidad){
        
        this.identidad=identidad;
        
    }
    /**
     * Metodo obtVecIdentidad, retorna el vector de Identidades
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector obtVecIdentidad(){
        return VecIdentidad;
    }
    
    /**
     * Metodo obtIdentidad, retorna el objeto de Identidades
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Identidad obtIdentidad(){
        return identidad;
    }
    
    /**
     * Metodo listarIdentidad, lista las Identidades
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void listarIdentidad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        VecIdentidad = null;
        String query = "SQL_LISTAR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                VecIdentidad = new Vector();
                rs = st.executeQuery();
                
                while (rs.next()){
                    Identidad identidad = new Identidad();
                    identidad.setCedula(rs.getString("cedula"));
                    identidad.setNombre((rs.getString("nombre")!=null)?rs.getString("nombre"):"");
                    identidad.setDireccion(rs.getString("direccion"));
                    identidad.setCodciu( (rs.getString("codciu")!=null)? rs.getString("codciu"): "NR" );
                    identidad.setCoddpto((rs.getString("coddpto")!=null)? rs.getString("coddpto") : "NR");
                    identidad.setCodpais((rs.getString("codpais")!=null)? rs.getString("codpais"): "NR");
                    try{
                        identidad.setTelefono(rs.getString("telefono"));
                    }catch(Exception e){}
                    identidad.setTelefono1(rs.getString("telefono1"));
                    identidad.setCargo(rs.getString("cargo"));
                    VecIdentidad.add(identidad);
                    identidad = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    /**
     * Metodo buscarIdentidad, busca la identidad
     * @param: cedula, cia
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarIdentidad(String ced,String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        identidad = null;
        String query = "SQL_EXISTE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ced );
                rs = st.executeQuery();
                
                if (rs.next()){
                    identidad = Identidad.load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo insertarIdentidad, ingresa un registro en la tabla identidad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertarIdentidad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERTAR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, identidad.getCedula());
                st.setString(2, identidad.getId_mims());
                st.setString(3, identidad.getNom1());
                st.setString(4, identidad.getNom2());
                st.setString(5, identidad.getApe1());
                st.setString(6, identidad.getApe2());
                st.setString(7, identidad.getDireccion() );
                st.setString(8, identidad.getCodciu() );
                st.setString(9, identidad.getCoddpto() );
                st.setString(10, identidad.getCodpais() );
                st.setString(11, identidad.getTelefono() );
                st.setString(12, identidad.getCelular() );
                st.setString(13, identidad.getE_mail() );
                st.setString(14, identidad.getUsuario()  );
                st.setString(15, identidad.getUsuariocrea() );
                st.setString(16, identidad.getSexo() );
                st.setString(17, identidad.getFechanac() );
                st.setString(18, identidad.getBase() );
                st.setString(19, identidad.getTelefono1() );
                st.setString(20, identidad.getCargo() );
                st.setString(21, identidad.getCia() );
                st.setString(22, identidad.getRef1() );
                st.setString(23, identidad.getTipo_iden() );
                st.setString(24, identidad.getObservacion() );
                st.setString(25, identidad.getNombre());
                st.setString(26, identidad.getEst_civil());
                st.setString(27, identidad.getLugarnac());
                st.setString(28, identidad.getBarrio());
                st.setString(29, identidad.getLibmilitar());
                st.setString(30, identidad.getSenalParticular());
                st.setString(31, identidad.getExpced());
                st.setString(32, identidad.getNomnemo());
                st.setString(33, identidad.getClasificacion());
                st.setString(34, identidad.getVeto());
                ////System.out.println("Insertar "+st);
                st.executeUpdate();
                AutorizacionFleteDAO aflete = new AutorizacionFleteDAO();
                aflete.insertControlActualizacion(st.toString(), identidad.getUsuariocrea());
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR LA IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo  existeIdentidad, retorna true o false si existe la identidad.
     * @param: codigo identidad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeIdentidad(String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_EXISTE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod );
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICAR SI EXISTE IDENTIFICACION" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    /**
     * Metodo  existeIdentidadAnulado, retorna true o false si la identidad esta anulada.
     * @param:compa�ia, codigo identidad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeIdentidadAnulado(String cod, String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_EXISTE_ANULADO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod );
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICAR SI EXISTE IDENTIFICACION ANULADO " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    /**
     * Metodo modificarIdentidad, modifica un registro en la tabla identidad
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarIdentidad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, identidad.getId_mims());
                st.setString(2, identidad.getNom1());
                st.setString(3, identidad.getNom2());
                st.setString(4, identidad.getApe1());
                st.setString(5, identidad.getApe2());
                st.setString(6, identidad.getDireccion() );
                st.setString(7, identidad.getCodciu() );
                st.setString(8, identidad.getCoddpto() );
                st.setString(9, identidad.getCodpais() );
                st.setString(10, identidad.getTelefono() );
                st.setString(11, identidad.getCelular() );
                st.setString(12, identidad.getE_mail() );
                st.setString(13, identidad.getUsuario()  );
                st.setString(14, identidad.getSexo() );
                st.setString(15, identidad.getFechanac() );
                st.setString(16, identidad.getBase() );
                st.setString(17, identidad.getTelefono1() );
                st.setString(18, identidad.getCargo() );
                st.setString(19, identidad.getRef1() );
                st.setString(20, identidad.getTipo_iden() );
                st.setString(21, identidad.getObservacion() );
                st.setString(22, identidad.getNombre());
                st.setString(23, identidad.getEst_civil());
                st.setString(24, identidad.getLugarnac());
                st.setString(25, identidad.getBarrio());
                st.setString(26, identidad.getLibmilitar());
                st.setString(27, identidad.getSenalParticular());
                st.setString(28, identidad.getExpced());
                st.setString(29, identidad.getNomnemo());
                st.setString(30, identidad.getClasificacion());
                st.setString(31, identidad.getVeto());
                st.setString(32, identidad.getCedula());                
                ////System.out.println("Modificar " +st);                
                st.executeUpdate();                
                //Modificando proveedor
                modificarNomProveedor();
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL MODIFICAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo  anularIdentidad, anula la identidad.
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void anularIdentidad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, identidad.getUsuario() );
                st.setString(2, identidad.getCedula() );
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo  activarIdentidad, modifica la identidad.
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void activarIdentidad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ACTIVAR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, identidad.getId_mims());
                st.setString(2, identidad.getNom1());
                st.setString(3, identidad.getNom2());
                st.setString(4, identidad.getApe1());
                st.setString(5, identidad.getApe2());
                st.setString(6, identidad.getDireccion() );
                st.setString(7, identidad.getCodciu() );
                st.setString(8, identidad.getCoddpto() );
                st.setString(9, identidad.getCodpais() );
                st.setString(10, identidad.getTelefono() );
                st.setString(11, identidad.getCelular() );
                st.setString(12, identidad.getE_mail() );
                st.setString(13, identidad.getUsuario()  );
                st.setString(14, identidad.getSexo() );
                st.setString(15, identidad.getFechanac() );
                st.setString(16, identidad.getBase() );
                st.setString(17, identidad.getTelefono1() );
                st.setString(18, identidad.getCargo() );
                st.setString(19, identidad.getRef1() );
                st.setString(20, identidad.getTipo_iden() );
                st.setString(21, identidad.getObservacion() );
                st.setString(22, identidad.getNombre());
                st.setString(23, identidad.getCia() );
                st.setString(24, identidad.getEst_civil());
                st.setString(25, identidad.getLugarnac());
                st.setString(26, identidad.getBarrio());
                st.setString(27, identidad.getLibmilitar());
                st.setString(28, identidad.getSenalParticular());
                st.setString(29, identidad.getExpced());
                st.setString(30, identidad.getNomnemo());
                st.setString(31, identidad.getClasificacion());
                st.setString(32, identidad.getVeto());
                st.setString(33, identidad.getCedula());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTIVAR IDENTIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo listarIdentidadXBusq, lista las identidades dependiendo al documento,
     * nombre y compa�ia
     * @param:documento, nombre cia
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void listarIdentidadXBusq(String ced, String nomc,  String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        VecIdentidad = null;
        String query = "SQL_listarXbusq";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ced);
                st.setString(2, nomc);
                st.setString(3, cia);
                
                VecIdentidad = new Vector();
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    Identidad iden = new Identidad();
                    iden.setCedula(rs.getString("cedula"));
                    iden.setNombre(rs.getString("nombre"));
                    iden.setDireccion(rs.getString("direccion"));
                    iden.setCodpais((rs.getString("pais")!=null)?rs.getString("pais"):"");
                    iden.setCodciu((rs.getString("ciudad")!=null)?rs.getString("ciudad"):"");
                    try{
                        iden.setTelefono(rs.getString("telefono"));
                    }catch(Exception e){}
                    iden.setTelefono1(rs.getString("telefono1"));
                    iden.setCia(rs.getString("cia"));
                    iden.setAprobado(rs.getString("aprobado"));
                    iden.setEstado(rs.getString("estado"));
                    VecIdentidad.add(iden);
                iden = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo buscarPersonaNatural, busca las identidad dependiendo del la cedula y
     * es cedula su tipo documento.
     * @param: documento
     * @autor : Ing. Sandra Escalante
     * @version : 1.0
     */
    public void buscarPersonaNatural(String ced) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        identidad = null;
        String query = "SQL_PER_NATURAL";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ced );
                rs = st.executeQuery();
                
                if (rs.next()){
                    identidad = Identidad.load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo reset, restea el objeto identidad
     * @param:
     * @autor : Ing. Sandra Escalante
     * @version : 1.0
     */
    public void reset(){
        identidad = null;
    }
    /**
     * Metodo existeNomnemo, retorna true o false si existe el nombre nemotecnico
     * @param: nombre nemotecnico
     * @autor : Ing. Diogenes Bastidas
     * @version : 1.0
     */
    public boolean existeNomnemo(String nom) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_VER";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nom );
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICAR SI EXISTE NOMNEMO" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    ////HENRY 151005
    /**
     * Metodo buscarTenedorxCedula, retorna el objeto Identidad depandiendo de la cedula
     * @param: cedula, atitulo
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public void buscarTenedorxCedula(String cedula, String atitulo)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tenedor = new Tenedor();
        String query = "SQL_TENEDOR";//JJCastro fase2

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,cedula);
                rs = st.executeQuery();
                if(rs.next()){
                    tenedor.setCedula(rs.getString("cedula"));
                    tenedor.setNombre(rs.getString("nombre"));
                    tenedor.setExpedicionced(rs.getString("expced"));
                    tenedor.setDireccion(rs.getString("direccion"));
                    tenedor.setCiudad(rs.getString("ciudad"));
                    tenedor.setTelefono(rs.getString("telefono"));
                    tenedor.setCelular(rs.getString("celular"));
                    tenedor.setNom1(rs.getString("nombre1"));
                    tenedor.setNom2(rs.getString("nombre2"));
                    tenedor.setApe1(rs.getString("apellido1"));
                    tenedor.setApe2(rs.getString("apellido2"));
                    tenedor.setPais(rs.getString("pais"));
                    tenedor.setAtitulo(atitulo);
                }
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DEL TENEDOR X CEDULA NIT" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    /**
     * Getter for property tenedor.
     * @return Value of property tenedor.
     */
    public com.tsp.operation.model.beans.Tenedor getTenedor() {
        return tenedor;
    }
    
    /**
     * Setter for property tenedor.
     * @param tenedor New value of property tenedor.
     */
    public void setTenedor(com.tsp.operation.model.beans.Tenedor tenedor) {
        this.tenedor = tenedor;
    }
    ///////////////////////////////////////////////////////////////DAO////////////////////////////////////////////////////////
    /**
     * Metodo consultaTxt , Metodo que Realiza una consulta de un registro para a�adirlo a una linea
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : String ruta
     * @param : String base
     * @param : String dato
     * @version : 1.0
     */
    public void consultaTxt(String ruta, String base, String dato) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_EXISTE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dato);
                rs = st.executeQuery();
                String linea = "";
                String tipo = "";
                int numcolum = rs.getMetaData().getColumnCount();
                while (rs.next()){
                    for (int i=1; i<=numcolum; i++){
                        tipo = rs.getMetaData().getColumnTypeName(i);
                        if ((tipo.equalsIgnoreCase("varchar"))||(tipo.equalsIgnoreCase("char"))||(tipo.equalsIgnoreCase("text"))){
                            linea += rs.getString(i)+",";
                        }else if (tipo.equalsIgnoreCase("timestamp")){
                            linea += rs.getTimestamp(i)+",";
                        } else if (tipo.equalsIgnoreCase("date")){
                            linea += rs.getDate(i)+",";
                        }else if (tipo.equalsIgnoreCase("numeric")){
                            linea += rs.getDouble(i)+",";
                        }
                    }
                }
                this.archivotxt(linea, ruta, base);
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL REALIZAR LA CONSULTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo archivoTxt , M�todo que a�ade una linea con los datos de un registro a un archivo de auditor�a
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : String ruta
     * @param : String base
     * @param : String linea
     * @version : 1.0
     */
    private void archivotxt(String linea, String ruta, String base) throws SQLException{
        LinkedList lineasrs = new LinkedList();
        Calendar FechaHoy = Calendar.getInstance();
        java.util.Date d = FechaHoy.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyMMdd");
        String FechaFormated1 = s1.format(d);
        String lineas="";
        String archivo = ruta + "/nit" + base +"_"+ FechaFormated1 + ".txt";
        ////System.out.println("ARCHIVO  "+ archivo);
        try{
            
            FileWriter x = new FileWriter(archivo, true);
            x.write("\n"+linea);
            x.close();
        }
        catch(IOException e){
            ////System.out.println("No se pudo abrir el archivo");
        }
    }
    /**
     * Obtiene el ResultSetMetaData de la tabla nit
     * @autor Ing. Tito Andr�s Maturana
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public ResultSetMetaData obtenerMetadata() throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_METADATA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                psttm = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = psttm.executeQuery();                
                return rs.getMetaData();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL METADATA" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (psttm  != null){ try{ psttm.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return null;
    }
   
    /**
     * aprobarRegistrio, aprueba los registros de la tabla nit
     * @autor Ing. Diogenes Bastidas Morales
     * @param: usuario, estado, cedula
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void aprobarRegistrio(String usuario, String estado, String cedula ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_APROBACION";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, estado);
                st.setString(2, usuario);
                st.setString(3, cedula);
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL APROBAR LA IDENTIDAD " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
   
    
    /**
     * Getter for property prov.
     * @return Value of property prov.
     */
    public com.tsp.operation.model.beans.Proveedor getPropietario() {
        return prov;
    }
    /**
     * existePropietario, busca el propietario en la tabla nit
     * y propietario dependiendo de la cedula
     * @autor Ing. Diogenes Bastidas Morales
     * @param:  cedula
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public boolean existePropietario(String cedula)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_PROPIETARIO";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,cedula);
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;
                }
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA  DEL PROPIETARIO X CEDULA NIT" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
   


    /**
     * listarIdentidadNOAprobada, lista los nit no aprobados de acuerdo al usuario
     * asignado aprobar las identidaes.
     * @autor Ing. Diogenes Bastidas Morales
     * mod: Ing. Iv�n Devia
     * @version 1.0
     * @throws SQLException Si se presenta un error de la base de datos.
     */
    public void listarIdentidadNOAprobada(String login) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        VecIdentidad = null;
        String query = "SQL_IDENTIDAD_NOAPROBADAS";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, login);
                VecIdentidad = new Vector();
                rs = st.executeQuery();
                
                while (rs.next()){
                    Identidad iden = new Identidad();
                    iden.setCedula(rs.getString("cedula"));
                    iden.setNombre(rs.getString("nombre"));
                    iden.setUsuariocrea(rs.getString("usuariocrea"));
                    VecIdentidad.add(iden);
                    iden = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR IDENTIDAD NO APROBADAS" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /*QUERY PARA MODIFICAR EL NOMBRE DEL PROVEEDOR*/
     private static final  String modificar_proveedor = "update proveedor set payment_name = ? where nit = ?";


     /**
     * Metodo modificarNomProveedor, modifica un el nom,bre de la tabla proveedor
     * @param:
     * @autor : Ing. fily steven fernandez
     * @version : 1.0
     */
      public void modificarNomProveedor() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR_PROVEEDOR";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, identidad.getNombre());
                st.setString(2, identidad.getCedula());
                st.executeUpdate();
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL MODIFICAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
      
      
    
    
}
