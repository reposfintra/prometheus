package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Inversionista;
import com.tsp.operation.model.beans.MovimientosCaptaciones;
import com.tsp.operation.model.beans.NitSot;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * DAO para el nuevo programa de captaciones
 * 24/02/2012
 * @author darrieta
 */
public class CaptacionInversionistaDAO extends MainDAO {

    public CaptacionInversionistaDAO() {
        super("CaptacionInversionistaDAO.xml");
    }
    public CaptacionInversionistaDAO(String dataBaseName) {
        super("CaptacionInversionistaDAO.xml",dataBaseName);
    }

    public NitSot buscarNit(String id) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        NitSot nit = null;
        String query = "SQL_SEARCH_NIT";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, id);
            rs = st.executeQuery();
            if (rs != null) {
                if (rs.next()) {
                    nit = new NitSot();
                    nit.setEstado(rs.getString(1));
                    nit.setCedula(rs.getString(2));
                    nit.setId_mims(rs.getString(3));
                    nit.setNombre(rs.getString(4));
                    nit.setDireccion(rs.getString(5));
                    nit.setCodciu(rs.getString(6));
                    nit.setCoddpto(rs.getString(7));
                    nit.setCodpais(rs.getString(8));
                    nit.setTelefono(rs.getString(9));
                    nit.setCellular(rs.getString(10));
                    nit.setE_mail(rs.getString(11));
                    nit.setFechaultact(rs.getString(12));
                    nit.setUsuario(rs.getString(13));
                    nit.setFechacrea(rs.getString(14));
                    nit.setUsuariocrea(rs.getString(15));
                    nit.setSexo(rs.getString(16));
                    nit.setFechanac(rs.getString(17));
                    nit.setNombre1(rs.getString("nombre1"));
                    nit.setNombre2(rs.getString("nombre2"));
                    nit.setApellido1(rs.getString("apellido1"));
                    nit.setApellido2(rs.getString("apellido2"));
                    nit.setDireccion_oficina(rs.getString("direccion_oficina"));
                    nit.setE_mail2(rs.getString("e_mail2"));
                }
            }
        } catch (Exception e) {
            throw new Exception("Error en buscarNit[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs != null) {
                try {
                    rs = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
        return nit;
    }
    
    /**
     * Inserta un registro en la tabla inversionista
     * @param inversionista
     * @return sql generado
     * @throws Exception nit, subcuenta, dstrct, nombre_subcuenta, creation_date, creation_user,nit_parentesco,tipo_parentesco,tipo_inversionista)
            VALUES (?, ?, ?, ?, now(), ?,?,?,?);
     */
    public String insertarInversionista(Inversionista inversionista) throws Exception{
        StringStatement st = null;
        String query = "INSERTAR_INVERSIONISTA";
        String sql;
        
        try{            
            st = new StringStatement(this.obtenerSQL(query), true);
            int param = 1;
            st.setString(param++, inversionista.getNit());
            st.setInt(param++, inversionista.getSubcuenta());
            st.setString(param++, inversionista.getDstrct());
            st.setString(param++, inversionista.getNombre_subcuenta());
            st.setString(param++, inversionista.getCreation_user());
            st.setString(param++, inversionista.getNit_parentesco());
            st.setString(param++, inversionista.getTipo_parentesco());
            st.setString(param++, inversionista.getTipo_inversionista());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en insertarInversionista[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    /**
     * Obtiene las subcuentas de un inversionista
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<Inversionista> consultarSubcuentas(String nit, String dstrct) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_SUBCUENTAS";
        ArrayList<Inversionista> subcuentas = new ArrayList<Inversionista>();
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, nit);
            st.setString(2, dstrct);
            rs = st.executeQuery();
            while (rs.next()) {
                Inversionista inversionista = new Inversionista();
                inversionista.setNit(nit);
                inversionista.setSubcuenta(rs.getInt("subcuenta"));
                inversionista.setNombre_subcuenta(rs.getString("nombre_subcuenta"));
                subcuentas.add(inversionista);
            }
            
        } catch (Exception e) {
            throw new Exception("Error en consultarSubcuentas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return subcuentas;
    }
    
    /**
     * Obtiene el maximo numero de subcuentas de un inversionista
     * @param nit identificacion del inversionista
     * @param dstrct distrito del usuario en sesion
     * @return maximo numero de subcuentas del inversionista
     * @throws Exception  
     */
    public int consultarMaxSubcuenta(String nit, String dstrct) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_MAX_SUBCUENTA";
        int max = 0;
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, nit);
            st.setString(2, dstrct);
            rs = st.executeQuery();
            if (rs.next()) {
                max = rs.getInt("max");
            }
            
        } catch (Exception e) {
            throw new Exception("Error en consultarMaxSubcuenta[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return max;
    }
    
    /**
     * Edita un registro en la tabla inversionista
     * @param inversionista
     * @return sql generado
     * @throws Exception 
     */
    public String editarInversionista(Inversionista inversionista) throws Exception{
        StringStatement st = null;
        String query = "EDITAR_SUBCUENTA";
        String sql;
        
        try{            
            st = new StringStatement(this.obtenerSQL(query), true);
            int param = 1;
            st.setString(param++, inversionista.getNombre_subcuenta());
            st.setString(param++, inversionista.getUser_update());
            st.setString(param++, inversionista.getNit());
            st.setInt(param++, inversionista.getSubcuenta());
            st.setString(param++, inversionista.getNit_parentesco());
            st.setString(param++, inversionista.getTipo_parentesco());
            st.setString(param++, inversionista.getTipo_inversionista());
            st.setString(param++, inversionista.getNit());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en editarInversionista[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    /**
     * Obtiene los inversionistas activos
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<Inversionista> consultarInversionistas(String dstrct) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_INVERSIONISTAS";
        ArrayList<Inversionista> subcuentas = new ArrayList<Inversionista>();
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            rs = st.executeQuery();
            while (rs.next()) {
                Inversionista inversionista = new Inversionista();
                inversionista.setDstrct(rs.getString("dstrct"));
                inversionista.setNit(rs.getString("nit"));
                inversionista.setNombre_subcuenta(rs.getString("nombre"));
                inversionista.setNit_parentesco(rs.getString("nit_parentesco"));
                subcuentas.add(inversionista);
            }
            
        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return subcuentas;
    }

     /**
     * Obtiene los inversionistas y su clasificacion
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<Inversionista> reportesInversionistas(String dstrct) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "REPORTE_INVERSIONISTAS";
        ArrayList<Inversionista> subcuentas = new ArrayList<Inversionista>();

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            rs = st.executeQuery();
            while (rs.next()) {
                Inversionista inversionista = new Inversionista();
                inversionista.setDstrct(rs.getString("dstrct"));
                inversionista.setNit(rs.getString("nit"));
                inversionista.setNombre_subcuenta(rs.getString("nombre_inversionista"));
                inversionista.setTipo_inversionista(rs.getString("tipo_inversionista"));
                inversionista.setTipo_parentesco(rs.getString("parentesco"));
                inversionista.setComputa(rs.getString("computa"));                


                subcuentas.add(inversionista);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return subcuentas;
    }


         /**
     * Obtiene los inversionistas y su clasificacion
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public BeanGeneral clasificacionInversionistas(String dstrct) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CLASIFICACION_INVERSIONISTAS";
        BeanGeneral info = new BeanGeneral();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            rs = st.executeQuery();
            while (rs.next()) {
               info.setValor_01(rs.getString("concepto1"));
               info.setValor_02(rs.getString("concepto2"));
               info.setValor_03(rs.getString("socios"));
               info.setValor_04(rs.getString("particulares"));
               info.setValor_05(rs.getString("inversionistas"));
               info.setValor_06(rs.getString("familiar_socio"));
               
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return info;
    }
    
    
         /**
     * Obtiene los inversionistas y su clasificacion
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public BeanGeneral clasificacionInversionistasMes(String dstrct, String mes) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CLASIFICACION_INVERSIONISTAS_MES";
        BeanGeneral info = new BeanGeneral();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
             st.setString(2, mes);
            rs = st.executeQuery();
            while (rs.next()) {
               info.setValor_01(rs.getString("concepto1"));
               info.setValor_02(rs.getString("concepto2"));
               info.setValor_03(rs.getString("socios"));
               info.setValor_04(rs.getString("particulares"));
               info.setValor_05(rs.getString("inversionistas"));
               info.setValor_06(rs.getString("familiar_socio"));
               
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return info;
    }




    
    public String guardarDatosLiquidacion(Inversionista inversionista) throws Exception{
        StringStatement st = null;
        String query = "GUARDAR_DATOS_LIQUIDACION";
        String sql;
        
        try{            
            st = new StringStatement(this.obtenerSQL(query), true);
            int param = 1;
            
            st.setDouble(param++, inversionista.getTasa());
            st.setInt(param++, inversionista.getAno_base());
              st.setInt(param++, inversionista.getAno_base_tasa_diaria());
            st.setString(param++, inversionista.getRendimiento());
            st.setString(param++, inversionista.getTipo_interes());
            st.setString(param++, inversionista.getRetefuente());
            st.setString(param++, inversionista.getReteica());
            st.setString(param++, inversionista.getUser_update());
            
            st.setDouble(param++, inversionista.getTasa_diaria());
            st.setDouble(param++, inversionista.getTasa_ea());
            st.setDouble(param++, inversionista.getTasa_nominal());
             st.setInt(param++, inversionista.getDias_calendario());
             st.setString(param++, inversionista.getPago_automatico());
             st.setString(param++, inversionista.getPeriodicidad_pago());
              st.setString(param++, inversionista.getGenera_documentos());
            st.setString(param++, inversionista.getNit());
            st.setInt(param++, inversionista.getSubcuenta());
           
           
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en guardarDatosLiquidacion[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    public String updateNit(NitSot nit) throws Exception{
        StringStatement st = null;
        String query = "ACTUALIZAR_NIT";
        String sql;
        
        try{            
            st = new StringStatement(this.obtenerSQL(query), true);
            int param = 1;
            st.setString(param++, nit.getDireccion_oficina());
            st.setString(param++, nit.getE_mail2());
            st.setString(param++, nit.getCedula());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en updateNit[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }







      /**
     * Obtiene los inversionistas activos
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public Inversionista consultarSubcuentaInversionista(String dstrct,String nit,int subcuenta) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_SUBCUENTA_INVERSIONISTA";
        Inversionista inversionista = new Inversionista();

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            st.setString(2, nit);
            st.setInt(3, subcuenta);
            rs = st.executeQuery();
            while (rs.next()) {

                inversionista.setNit(rs.getString("nit"));
                inversionista.setNombre_subcuenta(rs.getString("nombre_subcuenta"));
                inversionista.setNombre_nit(rs.getString("nombre_nit"));
                inversionista.setTasa(rs.getDouble("Tasa"));
                inversionista.setAno_base(rs.getInt("Ano_base"));
                inversionista.setAno_base_tasa_diaria(rs.getInt("Ano_base_tasa_diaria"));
                inversionista.setTasa_nominal(rs.getDouble("tasa_nominal"));
                inversionista.setTasa_ea(rs.getDouble("tasa_ea"));
                inversionista.setTasa_diaria(rs.getDouble("tasa_diaria"));
                inversionista.setRendimiento(rs.getString("rendimiento"));
                inversionista.setTipo_interes(rs.getString("tipo_interes"));
                inversionista.setRetefuente(rs.getString("retefuente"));
                inversionista.setReteica(rs.getString("reteica"));
                inversionista.setSubcuenta(rs.getInt("subcuenta"));
                inversionista.setCodcliente(rs.getString("codcli"));
                inversionista.setDias_calendario(rs.getInt("dias_calendario"));
                inversionista.setDstrct(rs.getString("dstrct"));
                inversionista.setNit_parentesco(rs.getString("nit_parentesco"));
                inversionista.setTipo_parentesco(rs.getString("tipo_parentesco"));
                inversionista.setTipo_inversionista(rs.getString("tipo_inversionista"));
               inversionista.setPago_automatico(rs.getString("pago_automatico"));
                inversionista.setPeriodicidad_pago(rs.getString("periodicidad_pago"));
                 inversionista.setGenera_documentos(rs.getString("genera_documentos"));

            }
        } catch (Exception e) {
            throw new Exception("Error en consultarSubcuentaInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return inversionista;
    }



     /**
     * Obtiene los inversionistas activos
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarUltimosMovimientos(String dstrct,String nit ,String subcuenta,String mes) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_ULTIMOS_MOVIMIENTOS";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        

        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);

            if(mes.equals("S"))
            {
              query= query.replace("#mes","and  replace (substring(fecha,0,8),'-','')::integer >= replace (substring(now(),0,8),'-','')::integer order by fecha asc,no_transaccion ");
            }
            else
            {
              query=   query.replace("#mes", "order by fecha desc,no_transaccion desc  limit 1");
            }

            st = con.prepareStatement(query);
            st.setString(1, dstrct);
            st.setString(2, nit);
            st.setString(3, subcuenta);
            rs = st.executeQuery();
            MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
            while (rs.next()) {
               movimiento = movimiento.load(rs);
                movimiento.setNombre_subcuenta(rs.getString("nombre_subcuenta") !=null ? rs.getString("nombre_subcuenta"):"");
               movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }


      /**
      * Obtiene los inversionistas activos
      * @param dstrct distrito del usuario en sesion
      * @return ArrayList<Inversionista> con los resultados obtenidos
      * @throws Exception
      */
        public ArrayList<MovimientosCaptaciones> consultarMovimientosMes(String dstrct,String nit ,String subcuenta,String periodo) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_MOVIMIENTOS_MES";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();


        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1, dstrct);
            st.setString(2, nit);
            st.setString(3, subcuenta);
            st.setString(4, periodo);
            rs = st.executeQuery();
            MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
            while (rs.next()) {
               movimiento = movimiento.load(rs);
               movimiento.setTipo_transferencia(rs.getString("medio_transfericia"));
               movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }



 /*
     * Autor jpinedo
     * Insertar un registro para el log de mov enviados por tranferencias
     */
    public String guardarMovimiento(MovimientosCaptaciones mov) throws Exception {

        StringStatement st = null;
        String sql="";
        ResultSet rs = null;
        String query = "GUARDAR_MOVIMIENTO";
                       
           
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, mov.getDstrct());
            st.setString(2, mov.getNit());
            st.setInt(3, mov.getSubcuenta());
            st.setDouble(4, mov.getTasa_ea());
            st.setString(5, mov.getFecha());
            st.setDouble(6, mov.getSaldo_inicial());
            st.setDouble(7, mov.getValor_intereses());
            st.setDouble(8, mov.getValor_retefuente());
            st.setDouble(9, mov.getValor_reteica());
            st.setDouble(10, mov.getSubtotal());
            st.setDouble(11, mov.getConsignacion());
            st.setDouble(12, mov.getRetiro());
            st.setString(13, mov.getTipo_movimiento());
            st.setDouble(14, mov.getSaldo_final());
            st.setString(15, mov.getBanco());
            st.setString(16, mov.getSucursal());
            st.setString(17, mov.getCreation_user());
            st.setString(18, mov.getCreation_user());
            st.setString(19, mov.getEstado());
            st.setDouble(20, mov.getBase_intereses());
            st.setDouble(21, mov.getIntereses_acomulados());
            st.setString(22, mov.getCuenta());
            st.setString(23, mov.getTitular_cuenta());
            st.setString(24, mov.getNit_cuenta());
            st.setString(25, mov.getTipo_cuenta());
            st.setString(26, mov.getNombre_beneficiario());
            st.setString(27, mov.getNit_beneficiario());
            st.setString(28, mov.getCheque_cruzado());
            st.setString(29, mov.getCheque_primer_beneficiario());
            st.setString(30, mov.getNombre_beneficiario_captacion());
            st.setString(31, mov.getTipo_transferencia());
            st.setString(32, mov.getConcepto_transaccion());
             sql = st.getSql();

        

        } catch (Exception e) {
            throw new Exception(" Error INSERTANDO mov" + e.getMessage());
        }
        return sql;

    }


     /**
     * Obtiene los inversionistas activos
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getMovimientosRecalcular(MovimientosCaptaciones mov) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_ULTIMOS_MOVIMIENTOS_RECALCULO";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1, mov.getDstrct());
            st.setString(2, mov.getNit());
            st.setInt(3, mov.getSubcuenta());
            
            rs = st.executeQuery();
            while (rs.next()) {
                MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
              movimiento=  movimiento.load(rs);
                movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }



     /**
     * Obtiene los inversionistas activos
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getMovimientosCausacion(String dstrct) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_MOVIMIENTOS_CAUSACION";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1,dstrct);
            rs = st.executeQuery();
            while (rs.next()) {
                MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
                movimiento.setDstrct(rs.getString("dstrct"));
                movimiento.setNit(rs.getString("nit"));
                movimiento.setSubcuenta(rs.getInt("subcuenta"));
                movimiento.setNombre_subcuenta(rs.getString("nombre_subcuenta"));
                movimiento.setFecha(rs.getString("fecha"));
                movimiento.setNo_transaccion(rs.getString("no_transaccion"));
                movimiento.setSaldo_inicial(rs.getDouble("saldo_final"));
                movimiento.setIntereses_acomulados(rs.getDouble("intereses_acomulado"));
                movimiento.setTipo_movimiento("C");//cierre mensual
                movimiento.setRetiro(rs.getDouble("retiro"));
                movimiento.setTasa_ea(rs.getDouble("tasa_ea"));
                movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }
    
     /**
     * Obtiene los movimientos de informe cierre de los inversionistas
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getInformeCierreInversionistas(String dstrct ,String periodo) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_INFORME_CIERRE_MENSUAL";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1,periodo);
            st.setString(2,dstrct);
            st.setString(3,periodo);
            rs = st.executeQuery();
            while (rs.next()) {
                MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
                movimiento.setNombre_inversionista(rs.getString("nombre_inversionista"));               
                movimiento.setSaldo_final(rs.getDouble("saldo_final"));
                movimiento.setPorcentaje_total(rs.getDouble("porcentaje_total"));
                movimiento.setPromedio_ponderado(rs.getDouble("promedio_ponderado"));
                movimiento.setTasa_ea(rs.getDouble("tasa"));
                movimiento.getInversionista().setTipo_inversionista(rs.getString("tipo_inversionista"));
                
                movimientos.add(movimiento);
                
               
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }


      /**
     * Obtiene los movimientos de informe cierre de los socios
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getInformeCierreSocio(String dstrct ,String periodo) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        int cont = 1;
        String query = "CONSULTAR_INFORME_CIERRE_SOCIOS";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1,periodo);
            st.setString(2,dstrct);
            st.setString(3,periodo);
            rs = st.executeQuery();
            while (rs.next()) {
                MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
                movimiento.setId(cont);
                movimiento.setDstrct(rs.getString("dstrct"));
                movimiento.setNit(rs.getString("nit"));
                movimiento.setNombre_inversionista(rs.getString("nombre_inversionista"));               
                movimiento.setSaldo_final(rs.getDouble("saldo_final"));
                movimiento.setTotal(rs.getDouble("total"));
                movimiento.setPorcentaje_total(rs.getDouble("porcentaje_total"));
                movimiento.setPromedio_ponderado(rs.getDouble("promedio_ponderado"));
                movimiento.setTasa_ea(rs.getDouble("tasa"));
                movimiento.getInversionista().setTipo_inversionista(rs.getString("tipo_inversionista"));
                movimiento.getInversionista().setComputa(rs.getString("computa"));
                movimientos.add(movimiento);
                
                cont++;
                
            }
            

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }
    
     /**
     * Obtiene los movimientos de informe cierre de los otros inversionistas
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getInformeCierreOtros(String dstrct ,String periodo) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        int cont = 1;
        String query = "CONSULTAR_INFORME_CIERRE_PARTICULARES";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1,periodo);
            st.setString(2,dstrct);
            st.setString(3,periodo);
            rs = st.executeQuery();
            while (rs.next()) {
                MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
                movimiento.setId(cont);
                movimiento.setDstrct(rs.getString("dstrct"));
                movimiento.setNit(rs.getString("nit"));
                movimiento.setNombre_inversionista(rs.getString("nombre_inversionista"));               
                movimiento.setSaldo_final(rs.getDouble("saldo_final"));
                movimiento.setTotal(rs.getDouble("total"));
                movimiento.setPorcentaje_total(rs.getDouble("porcentaje_total"));
                movimiento.setPromedio_ponderado(rs.getDouble("promedio_ponderado"));
                movimiento.setTasa_ea(rs.getDouble("tasa"));
                movimiento.getInversionista().setTipo_inversionista(rs.getString("tipo_inversionista"));
                movimiento.getInversionista().setComputa(rs.getString("computa"));
                
                movimientos.add(movimiento);
                
                cont++;
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }





     /**
     * Obtiene los inversionistas activos
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> getMovimientosRecalcularRetiros(MovimientosCaptaciones mov) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_ULTIMOS_MOVIMIENTOS_RECALCULO_RETIRO";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            

            st = con.prepareStatement(query);
            st.setString(1, mov.getDstrct());
            st.setString(2, mov.getNit());
            st.setInt(3, mov.getSubcuenta());
            st.setString(4, mov.getNo_transaccion());
            rs = st.executeQuery();
            while (rs.next()) {
                MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
                movimiento=movimiento.load(rs);
                movimiento.setNombre_inversionista(rs.getString("nombre_inversionista"));
                movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }





    public String updateMovimientos(MovimientosCaptaciones movimiento) throws Exception{
        StringStatement st = null;
        String query = "ACTUALIZAR_MOVIMIENTOS";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);                

            st.setDouble(1, movimiento.getSaldo_inicial());
            st.setDouble(2, movimiento.getValor_intereses());
            st.setDouble(3, movimiento.getValor_retefuente());
            st.setDouble(4, movimiento.getValor_reteica());
            st.setDouble(5, movimiento.getSubtotal());
            st.setDouble(6, movimiento.getConsignacion());
            st.setDouble(7, movimiento.getRetiro());
            st.setDouble(8, movimiento.getSaldo_final());
            st.setString(9, movimiento.getUser_update());
            st.setDouble(10, movimiento.getBase_intereses());            
            st.setDouble(11, movimiento.getIntereses_acomulados());
            st.setDouble(12, movimiento.getTasa_ea());
            st.setString(13, movimiento.getFecha());
            st.setString(14, movimiento.getDstrct());
            st.setString(15, movimiento.getNit());
            st.setInt(16, movimiento.getSubcuenta());
            st.setString(17, movimiento.getNo_transaccion());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en updateNit[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

         /**
     * Obtiene los inversionistas activos
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarRetirosXconfirmar(String dstrct) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_RETIROS_X_CONFIRMAR";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
            while (rs.next()) {
                movimiento = movimiento.load(rs);
                movimiento.setNombre_inversionista(rs.getString("nombre_inversionista"));
                movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }


      public String confirmarRetiro(MovimientosCaptaciones movimiento) throws Exception{
        StringStatement st = null;
        String query = "ACTUALIZAR_CONFIRMAR_APROBAR_RETIRO";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, movimiento.getEstado());
            st.setString(2, movimiento.getUser_update());
            st.setString(3,  movimiento.getUser_update());
            st.setString(4, movimiento.getDstrct());
            st.setString(5, movimiento.getNit());
            st.setInt(6, movimiento.getSubcuenta());
            st.setString(7, movimiento.getNo_transaccion());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en confirmarRetiro[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

       public String actualizarDatosPagosAutomaticos(Inversionista inversionista) throws Exception{
        StringStatement st = null;
        String query = "ACTUALIZAR_DATOS_PAGOS_AUTOMATICOS";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, inversionista.getPago_automatico());
            st.setString(2, inversionista.getPeriodicidad_pago());
            st.setString(3, inversionista.getDstrct());
            st.setString(4, inversionista.getNit());
            st.setInt(5, inversionista.getSubcuenta());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en confirmarRetiro[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }




  /*  public String eliminarCuentasTransferencias(MovimientosCaptaciones movimiento) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_DATOS_TRASNFERENCIAS";
        String sql;
        try{

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, movimiento.getDstrct());
            st.setString(2, movimiento.getTipo_transferencia());
            st.setString(3,  movimiento.getNit());
            st.setString(4, movimiento.getCuenta());
            st.setString(5, movimiento.getNit_beneficiario());
            st.setString(6, movimiento.getNo_transaccion());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en confirmarRetiro[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }*/
    
    public void eliminarCuentasTransferencias(MovimientosCaptaciones movimiento) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "ELIMINAR_DATOS_TRASNFERENCIAS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, movimiento.getDstrct());
            st.setString(2, movimiento.getTipo_transferencia());
            st.setString(3, movimiento.getNit());
            st.setString(4, movimiento.getCuenta());
            st.setString(5, movimiento.getNit_beneficiario());
            st.setString(6, movimiento.getNo_transaccion());

            st.executeUpdate();

        } catch (Exception e) {
            throw new Exception("Error en eliminarCuentasTransferencias[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
    }

      public String actualizaFechaRetiro(MovimientosCaptaciones movimiento) throws Exception{
        StringStatement st = null;
        String query = "ACTUALIZAR_FECHA_RETIRO";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, movimiento.getFecha());
            st.setString(2, movimiento.getDstrct());
            st.setString(3, movimiento.getNit());
            st.setInt(4, movimiento.getSubcuenta());
            st.setString(5, movimiento.getNo_transaccion());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en confirmarRetiro[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }


               /**
     * Obtiene los inversionistas activos
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarRetirosXaprobar(String dstrct) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_RETIROS_X_APROBAR";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
            while (rs.next()) {
                movimiento = movimiento.load(rs);
                movimiento.setNombre_inversionista(rs.getString("nombre_inversionista"));
                movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarRetirosXaprobar[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }



        public String aprobarCxp_retiro(String usuario,String dstrct,String tipo_doc ,String documento,String nit) throws Exception{
        StringStatement st = null;
        String query = "APROBAR_CXP_RETIRO";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, usuario);
            st.setString(2, dstrct);
            st.setString(3, nit);
            st.setString(4, tipo_doc);
            st.setString(5, documento);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en confirmarRetiro[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

   public String actualizarCxp_retiro(MovimientosCaptaciones movimiento) throws Exception{
        StringStatement st = null;
        String query = "ACTUALIZAR_CXP_RETIRO";
        String sql;
        try{

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, movimiento.getUser_update());
            st.setDouble(2, movimiento.getRetiro());
            st.setDouble(3, movimiento.getRetiro());                        
            st.setString(4, movimiento.getDstrct());
            st.setString(5, movimiento.getNit());
            st.setString(6, "FAP");
            st.setString(7, movimiento.getNo_transaccion());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en confirmarRetiro[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }


               /**
     * Obtiene las cuentas utilizadas en los retiros      *
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarCuentasTransferencias(String dstrct,String nit,String tipo ) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_CUENTAS_TRANSFERENCIAS";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();


        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1, dstrct);
            st.setString(2, nit);
            st.setString(3, tipo);
            rs = st.executeQuery();          
            while (rs.next()) {
               MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
               movimiento.setBanco(rs.getString("banco"));
               movimiento.setNit_cuenta(rs.getString("nit_cuenta"));
               movimiento.setCuenta(rs.getString("cuenta"));
               movimiento.setTipo_cuenta(rs.getString("tipo_cuenta"));
               movimiento.setTitular_cuenta(rs.getString("titular_cuenta"));               
               movimiento.setNit_beneficiario(rs.getString("nit_beneficiario"));
               movimiento.setNombre_beneficiario(rs.getString("nombre_beneficiario"));               
               movimiento.setCheque_cruzado(rs.getString("cheque_cruzado"));
               movimiento.setCheque_primer_beneficiario(rs.getString("cheque_primer_beneficiario"));
               movimiento.setDstrct(rs.getString("dstrct"));
               movimiento.setTipo_transferencia(rs.getString("tipo_transferencia"));
               movimiento.setNo_transaccion(rs.getString("id"));
               movimiento.setNit(rs.getString("nit"));

               movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }

     /**
     * Obtiene las cuentas utilizadas en los retiros      *
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarSubcuentasMovimientosInversionista(String dstrct,String nit ) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_SUBCUENTAS_MOVIMIENTOS_INVERSIONISTA";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();


        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1, dstrct);
            st.setString(2, nit);
            rs = st.executeQuery();

            while (rs.next()) {
               MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
               movimiento.setNit(rs.getString("nit"));
               movimiento.setSubcuenta(rs.getInt("subcuenta"));
               movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }



               /**
     * Obtiene las cuentas utilizadas en los retiros      *
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public String consultarFechaUltimoCierre(String dstrct,String nit,String subcuenta ) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_FECHA_ULTIMO_CIERRE";
        String fecha = "";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1, dstrct);
            st.setString(2, nit);
            st.setString(3, subcuenta);
            rs = st.executeQuery();
            while (rs.next()) {          
               fecha = rs.getString("fecha");
              
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return fecha;
    }



         /**
     * Obtiene los inversionistas activos
     * @param dstrct distrito del usuario en sesion
     * @return ArrayList<Inversionista> con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<MovimientosCaptaciones> consultarRetirosXtransferir(String dstrct) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_RETIROS_X_TRANSFERIR";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
            while (rs.next()) {
                movimiento = movimiento.load(rs);
                movimiento.setNombre_inversionista(rs.getString("nombre_inversionista"));
                movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }




        public String crearEgresoRetiro(MovimientosCaptaciones movimiento) throws Exception{
        StringStatement st = null;
        String query = "INSERTAR_EGRESO";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, movimiento.getDstrct());
            st.setString(2, movimiento.getBanco());
            st.setString(3, movimiento.getSucursal());
            st.setString(4, movimiento.getNit());
            st.setString(5, movimiento.getNombre_inversionista());
            st.setString(6, "OP");
            st.setString(7, "CI");
            st.setDouble(8, movimiento.getRetiro());
            st.setDouble(9, movimiento.getRetiro());
            st.setString(10, movimiento.getUser_update());
            st.setString(11, movimiento.getUser_update());
            st.setString(12, "COL");
            st.setString(13, "004");
            st.setString(14, movimiento.getNit());
            st.setString(15, movimiento.getNit());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en creando egreso[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }



                    public String crearEgresoDetalleRetiro(MovimientosCaptaciones movimiento) throws Exception{
        StringStatement st = null;
        String query = "INSERTAR_EGRESO_DETALLE";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, movimiento.getDstrct());
            st.setString(2, movimiento.getBanco());
            st.setString(3, movimiento.getSucursal());
            st.setString(4, "1");
            st.setString(5, "CI");
            st.setDouble(6, movimiento.getRetiro());
            st.setDouble(7, movimiento.getRetiro());
            st.setString(8, movimiento.getUser_update());
            st.setString(9, movimiento.getUser_update());
            st.setString(10, "Pago retiro inversionista "+movimiento.getNombre_inversionista());
            st.setString(11, "COL");
            st.setString(12, "FAP");
            st.setString(13, movimiento.getNo_transaccion());
            st.setString(14, "");
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en creando egreso DETALLE[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }



                        //--ADD 16-Junio-2009
    /**
     * M�todo que agrupa los retiros  a transferir.
     * @autor   JPINEDO
     * @throws  Exception
     * @version 23-MAYO-2012
     **/
    public ArrayList<MovimientosCaptaciones> agruparRetiros( String retiros[] ) throws Exception{

            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            Connection         con     = null;
            String             query   = "CONSULTAR_RETIRO_ARCHIVO";
            ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
            try{
                String ids = "";
                for(int i=0;i<retiros.length;i++){
                    if(!ids.equals(""))  ids +=",";
                    ids += "'" + retiros[i] + "'";
                }
                String cxps=ids.replaceAll("'","''");
                con = this.conectarJNDI( query );
                String sql   =   this.obtenerSQL( query ).replaceAll("#retiros", ids ).replaceAll("#cxps",cxps);
                st= con.prepareStatement(sql);
                rs=st.executeQuery();
                while(rs.next())
               {
               MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
               movimiento.setBanco(rs.getString("banco"));
               movimiento.setNit_cuenta(rs.getString("nit_cuenta"));
               movimiento.setCuenta(rs.getString("cuenta"));
               movimiento.setTipo_cuenta(rs.getString("tipo_cuenta"));
               movimiento.setTitular_cuenta(rs.getString("titular_cuenta"));
               movimiento.setRetiro(rs.getDouble("total"));
               movimiento.setLote_transferencia(rs.getString("lote_transferencia"));
               movimientos.add(movimiento);
                }


            }catch(Exception e){
                  throw new Exception( "Agrupar Negocios " + e.getMessage());
            }
            finally{
                if(st!=null) st.close();
                if(rs!=null) rs.close();
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
            return movimientos;

     }





    /**
      * Obtiene los inversionistas activos
      * @param dstrct distrito del usuario en sesion
      * @return ArrayList<Inversionista> con los resultados obtenidos
      * @throws Exception
      */
        public ArrayList<MovimientosCaptaciones> consultarMovimientosPosCausacion(String dstrct,String nit ,String subcuenta) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_MOVIMIENTOS_POSTERIORES_CAUSACION";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1, dstrct);
            st.setString(2, nit);
            st.setString(3, subcuenta);
            rs = st.executeQuery();           
            while (rs.next()) {
               MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
               movimiento=movimiento.load(rs);
               movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }



           /**
      * Obtiene los movimeintos iniciales
      * @param dstrct distrito del usuario en sesion
      * @return ArrayList<Inversionista> con los resultados obtenidos
      * @throws Exception
      */
        public ArrayList<MovimientosCaptaciones> consultarMovimientosIniciales(String dstrct,String nit ,String subcuenta) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_MOVIMIENTOS_INICIALES";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1, dstrct);
            st.setString(2, nit);
            st.setString(3, subcuenta);
            rs = st.executeQuery();
            while (rs.next()) {
               MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
               movimiento=movimiento.load(rs);
               movimientos.add(movimiento);
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarMovimientosIniciales[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }

         public String guardarDatosTransferencia(MovimientosCaptaciones movimiento) throws Exception{
        StringStatement st = null;
        String query = "INSERTAR_DATOS_TRASNFERENCIAS";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, movimiento.getDstrct());
            st.setString(2, movimiento.getTipo_transferencia());
            st.setString(3, movimiento.getNit());
            st.setString(4, movimiento.getCuenta());
            st.setString(5, movimiento.getTitular_cuenta());
            st.setString(6, movimiento.getNit_cuenta());
            st.setString(7, movimiento.getTipo_cuenta());
            st.setString(8, movimiento.getNit_beneficiario());
            st.setString(9, movimiento.getNombre_beneficiario());
            st.setString(10, movimiento.getCheque_cruzado());
            st.setString(11, movimiento.getCheque_primer_beneficiario());
            st.setString(12, movimiento.getBanco());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en creando egreso[CaptacionInversionistaDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }




        public boolean getPagoIntereses(MovimientosCaptaciones movimeinto) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "GET_PAGO_INTERESES";
         boolean sw = false;
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1,movimeinto.getFecha());
            st.setString(2,movimeinto.getDstrct());
            st.setString(3,movimeinto.getNit());
            st.setInt(4,movimeinto.getSubcuenta());
            rs = st.executeQuery();
           
            while (rs.next()) {
             sw = rs.getBoolean("sw");
            }

        } catch (Exception e) {
            throw new Exception("Error en GET_PAGO_INTERESES[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return sw;
    }




        public double getValorRetiroRI(MovimientosCaptaciones movimeinto) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_VALOR_RETIRO_RI";
         double valor = 0;
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1,movimeinto.getDstrct());
            st.setString(2,movimeinto.getNit());
            st.setInt(3,movimeinto.getSubcuenta());
            rs = st.executeQuery();

            if (rs.next()) {
             valor = rs.getDouble("total_retiro_ri");
            }

        } catch (Exception e) {
            throw new Exception("Error en CONSULTAR_VALOR_RETIRO_RI[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return valor;
    }






        public double getSaldoInicialMes(Inversionista subcuenta) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_SALDO_INICIAL_MES";
         double valor = 0;
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1,subcuenta.getDstrct());
            st.setString(2,subcuenta.getNit());
            st.setInt(3,subcuenta.getSubcuenta());
            rs = st.executeQuery();

            while (rs.next()) {
             valor = rs.getDouble("saldo_final");
            }

        } catch (Exception e) {
            throw new Exception("Error en CONSULTAR_SALDO_INICIAL_MES[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return valor;
    }


      

        public boolean ExisteDatosTrans(MovimientosCaptaciones movimiento) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "EXISTE_DATOS_TRANSFERENCIAS";
         boolean  sw = true;
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1,movimiento.getDstrct());            
            st.setString(2,movimiento.getNit());
             st.setString(3,movimiento.getCuenta());
             st.setString(4,movimiento.getTipo_transferencia());
            st.setString(5,movimiento.getNit_beneficiario());
            rs = st.executeQuery();
            if (rs.next()) {
             sw = false;
            }

        } catch (Exception e) {
            throw new Exception("Error en CONSULTAR_SALDO_INICIAL_MES[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return sw;
    }

    public String obtenerUltimaFechaCausacion(String dstrct, String nit, int subcuenta) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "OBTENER_ULTIMA_FECHA_CAUSACION";
        String fecha = "";
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1, dstrct);
            st.setString(2, nit);
            st.setInt(3, subcuenta);
            rs = st.executeQuery();
            while (rs.next()) {          
               fecha = rs.getString("fecha");
              
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return fecha;
    }

    public ArrayList<MovimientosCaptaciones> getInformeDetalleInversionista(String dstrct, String periodo, String nit) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_INFORME_DETALLE_INVERSIONISTA";
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        try {
            con = this.conectarJNDI(query);
            query=this.obtenerSQL(query);
            st = con.prepareStatement(query);
            st.setString(1,periodo);
            st.setString(2,dstrct);
            st.setString(3,periodo);
            st.setString(4,nit);
            rs = st.executeQuery();
            while (rs.next()) {
                MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
                movimiento.getInversionista().setSubcuenta(rs.getInt("subcuenta"));               
                movimiento.getInversionista().setNombre_subcuenta(rs.getString("nombre_subcuenta")); 
                movimiento.setSaldo_final(rs.getDouble("saldo_final"));
                movimiento.setTotal(rs.getDouble("total"));
                movimiento.setPorcentaje_total(rs.getDouble("porcentaje_total"));
                movimiento.setPromedio_ponderado(rs.getDouble("promedio_ponderado"));
                movimiento.setTasa_ea(rs.getDouble("tasa"));
                movimientos.add(movimiento);
                
               
            }

        } catch (Exception e) {
            throw new Exception("Error en consultarInversionistas[CaptacionInversionistaDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return movimientos;
    }

    
}
