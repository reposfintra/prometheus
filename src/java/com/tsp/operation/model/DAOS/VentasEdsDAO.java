/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.MenuOpcionesModulos;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mcastillo
 */
public interface VentasEdsDAO {
    
     /***
     * @param usuario
     * @param distrito
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<MenuOpcionesModulos> cargarMenuVentasEds(String usuario, String distrito) throws SQLException;
    
    public String  generarJsonGuardarVentaEds(int idEstacion, String nombre_eds, int idmanifiesto,String planilla, int kilometraje, String listadoProd);
    
}
