/***********************************************************************
* Nombre ......................Concepto_equipoDAO.java                 *
* Descripci�n..................Clase DAO para los conceptos del equipo *
* Autor........................Ing. Jose de la rosa                    *
* Fecha Creaci�n...............14 de diciembre de 2005, 09:05 PM       *
* Modificado por...............LREALES                                 *
* Fecha Modificaci�n...........5 de junio de 2006, 09:24 AM            *
* Versi�n......................1.0                                     *
* Coyright.....................Transportes Sanchez Polo S.A.           *
***********************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;


public class Concepto_equipoDAO extends MainDAO {
    
    private Concepto_equipo concepto_equipo;
    private Vector vecconcepto_equipo;
    
    private Vector otro_vector;
    
    /** Creates a new instance of Concepto_equipoDAO */
    public Concepto_equipoDAO () {
        
        super ( "Concepto_equipoDAO.xml" );
        
    }
    
    /**
     * Metodo: getConcepto_equipo, permite retornar un objeto de registros de Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Concepto_equipo getConcepto_equipo () {
        return concepto_equipo;
    }
    
    /**
     * Metodo: setConcepto_equipo, permite obtener un objeto de registros de Acuerdo especial.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setConcepto_equipo (Concepto_equipo concepto_equipo) {
        this.concepto_equipo = concepto_equipo;
    }
    
    /**
     * Metodo: getConcepto_equipos, permite retornar un vector de registros del concepto equipo.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getConcepto_equipos () {
        return vecconcepto_equipo;
    }
    
    /**
     * Metodo: setConcepto_equipos, permite obtener un vector de registros del concepto equipo.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setConcepto_equipos (Vector vecconcepto_equipo) {
        this.vecconcepto_equipo = vecconcepto_equipo;
    }
    
    /**
     * Metodo:          insertConcepto_equipo
     * Descripcion :    M�todo que permite ingresar un registro a la tabla concepto_equipo.
     * @autor :         Ing. Jose de la rosa
     * @modificado por: LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */ 
    public void insertConcepto_equipo () throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_INSERT";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, concepto_equipo.getCodigo());
                st.setString(2, concepto_equipo.getDescripcion());
                st.setString(3, concepto_equipo.getUsuario_modificacion());
                st.setString(4, concepto_equipo.getUsuario_creacion());
                st.execute();

            }
        } catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'insertConcepto_equipo' - [Concepto_equipoDAO].. " + e.getMessage() + " " + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Metodo:          searchConcepto_equipo
     * Descripcion :    M�todo que permite buscar un concepto equipo dado unos parametros.
     * @autor :         Ing. Jose de la rosa
     * @modificado por: LREALES
     * @param:          codigo del concepto.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void searchConcepto_equipo ( String codigo )throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SEARCH";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            
            vecconcepto_equipo = new Vector ();
            
            while( rs.next () ){
                concepto_equipo = new Concepto_equipo ();
                concepto_equipo.setCodigo ( rs.getString ("table_code") );
                concepto_equipo.setDescripcion ( rs.getString ("descripcion") );
                vecconcepto_equipo.add ( concepto_equipo );
            }            
            
        }}catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'searchConcepto_equipo' - [Concepto_equipoDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Metodo:          existConcepto_equipo
     * Descripcion :    M�todo que permite buscar un concepto equipo dado unos parametros.
     * @autor :         Ing. Jose de la rosa
     * @modificado por: LREALES
     * @param:          codigo del concepto.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean existConcepto_equipo ( String codigo ) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_SEARCH";//JJCastro fase2
        try {

            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, codigo);
                rs = st.executeQuery();

                if (rs.next()) {
                    sw = true;
                }

            }
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existConcepto_equipo' - [Concepto_equipoDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        return sw;
        
    }        
    
    /**
     * Metodo:          listConcepto_equipo
     * Descripcion :    M�todo que permite listar todas los conceptos equipos.
     * @autor :         Ing. Jose de la rosa
     * @modificado por: LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void listConcepto_equipo ()throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LIST";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs= st.executeQuery ();
            vecconcepto_equipo = new Vector ();
            
            while( rs.next () ){
                
                concepto_equipo = new Concepto_equipo ();
                
                concepto_equipo.setCodigo ( rs.getString ("table_code") );
                concepto_equipo.setDescripcion ( rs.getString ("descripcion") );
                
                vecconcepto_equipo.add ( concepto_equipo );
                
            }            
            
        }} catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'listConcepto_equipo' - [Concepto_equipoDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Metodo:          updateConcepto_equipo
     * Descripcion :    M�todo que permite actualizar un concepto de equipo.
     * @autor :         Ing. Jose de la rosa
     * @modificado por: LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void updateConcepto_equipo () throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, concepto_equipo.getDescripcion());
                st.setString(2, concepto_equipo.getUsuario_modificacion());
                st.setString(3, concepto_equipo.getCodigo());
                st.executeUpdate();

            }
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'updateConcepto_equipo' - [Concepto_equipoDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Metodo:          anularConcepto_equipo
     * Descripcion :    M�todo que permite anular un concepto equipo.
     * @autor :         Ing. Jose de la rosa
     * @modificado por: LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void anularConcepto_equipo () throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_ANULAR";//JJCastro fase2

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, concepto_equipo.getUsuario_modificacion());
                st.setString(2, concepto_equipo.getCodigo());

                st.executeUpdate();

            }
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'anularConcepto_equipo' - [Concepto_equipoDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
      
    /**
     * Metodo:          searchDetalleConcepto_equipo
     * Descripcion :    M�todo que permite listar concepto equipo por detalles dados unos parametros.
     * @autor :         Ing. Jose de la rosa
     * @modificado por: LREALES
     * @param:          codigo del concepto y la descripcion de este.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void searchDetalleConcepto_equipo ( String codigo, String descripcion ) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;        
        vecconcepto_equipo = null;
        boolean sw = false;
        String query = "SQL_SEARCH_DETALLES";//JJCastro fase2
        try {                       
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, codigo + "%" );
            st.setString ( 2, descripcion + "%" );
            
            rs = st.executeQuery ();
            
            vecconcepto_equipo = new Vector ();
            
            while( rs.next () ){
                concepto_equipo = new Concepto_equipo ();
                concepto_equipo.setCodigo ( rs.getString ("table_code") );
                concepto_equipo.setDescripcion ( rs.getString ("descripcion") );
                vecconcepto_equipo.add ( concepto_equipo );
                
            }
            
        }} catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'searchDetalleConcepto_equipo' - [Concepto_equipoDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }  
    
    /**************************************************************************/
    
    /**
     * Getter for property otro_vector.
     * @return Value of property otro_vector.
     */
    public java.util.Vector getOtro_vector() {
        
        return otro_vector;
        
    }
    
    /**
     * Setter for property otro_vector.
     * @param otro_vector New value of property otro_vector.
     */
    public void setOtro_vector( java.util.Vector otro_vector ) {
        
        this.otro_vector = otro_vector;
        
    }
    
    
    /**
     * Metodo:          existeCodigoEnTBLCON
     * Descripcion :    M�todo que permite buscar un concepto equipo dado unos parametros.
     * @autor :         LREALES
     * @param:          distrito y codigo del concepto.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean existeCodigoEnTBLCON ( String distrito, String codigo ) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_EXISTE_TBLCON";//JJCastro fase2

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, codigo);
                rs = st.executeQuery();

                if (rs.next()) {
                    sw = true;
                }

            }
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existeCodigoEnTBLCON' - [Concepto_equipoDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
        return sw;
        
    }  


    /**
     * Metodo:          searchDetallesTBLCON
     * Descripcion :    M�todo que permite buscar un concepto equipo dado unos parametros.
     * @autor :         LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void searchDetallesTBLCON ()throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE_TBLCON";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, "%" );
            st.setString ( 2, "%" );
            rs = st.executeQuery ();
            
            otro_vector = new Vector ();
            
            while( rs.next () ){
                String cod = rs.getString( "concept_code" )!=null?rs.getString( "concept_code" ).toUpperCase():"";
                String des = rs.getString( "concept_desc" )!=null?rs.getString( "concept_desc" ).toUpperCase():"";
                Hashtable ht = new Hashtable();
                
                ht.put( "codigo", cod );
                ht.put( "descripcion", des );
                
                this.otro_vector.add( ht );
                
            }            
            
        }} catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'searchDetallesTBLCON' - [Concepto_equipoDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
}
