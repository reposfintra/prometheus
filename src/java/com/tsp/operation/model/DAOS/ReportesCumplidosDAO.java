/*
 * ReportesPtoDAO.java
 *
 * Created on 31 de julio de 2005, 10:49
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import java.util.*;
import java.text.*;
/**
 *
 * @author  LEONARDO PARODY
 */
public class ReportesCumplidosDAO {
   
    public ReportesCumplidosDAO(){
        
    }
    private String Agencia;
    private String FechaI;
    private String FechaF;
    
    private ReportePlaRemPorFecha reportePlaRemPorFecha;
    private List Reporte;
    
    private final String SQL_REPORTEPLAREMPORFECHA =
    "select                     "+ 
        "   a.*,                    "+ 
        "   b.creation_user as creation_userpla, b.id_agencia as agcumpla, b.creation_date as creation_datepla, b.cantidad as cantidadpla,"+
        "   c.creation_user as creation_userrem, c.id_agencia as agcumrem, c.creation_date as creation_daterem, c.cantidad as cantidadrem"+
        " from                      "+
        " (                         "+ 
        "   select                  "+ 
        "                 "+
        "   b.numpla,               "+  
        "   b.fecdsp,               "+
        "   b.agcpla, d.nomciu as agcplanilla,     "+    
        "   b.oripla, e.nomciu as oriplanilla,     "+
        "   b.despla , f.nomciu as desplanilla,    "+
        "   b.pesoreal as cantplanilla,             "+
        "                  "+
        "   c.numrem,               "+   
        "   c.fecrem,               "+
        "   c.pesoreal as cantremesa,             "+
	"   c.qty_value_received,   "+
        "   c.agcrem, h.nomciu as agcremesa,    "+    
        "   c.orirem, i.nomciu as oriremesa,    "+
        "   c.desrem, j.nomciu as desremesa,    "+
        "   c.cliente,              "+
        "   g.nomcli,               "+
        "   c.docuinterno          "+
        "   from                    "+
        "   plarem   a,             "+
        "   planilla b,             "+
        "   remesa   c,             "+
	"   ciudad   d,             "+
	"   ciudad   e,             "+
        "   ciudad   f,             "+
        "   cliente  g,             "+
        "   ciudad   h,             "+
        "   ciudad   i,             "+
        "   ciudad   j              "+
        "   where                   "+  
        "   b.fecdsp between ? and ?";
        
    
    
    public void ReportePlaRemPorFecha (String fechaI, String FechaF, String Agencia)throws SQLException{
        ////System.out.println("ReportePlaRemPorFecha");
        Connection con= null;
        PreparedStatement st = null;
        double numdoc = 0;
        String creation_date = "";
        ResultSet rs = null;       
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        ResultSet rs4 = null;
        ResultSet rs5 = null;
        ResultSet rs6 = null;
        PoolManager poolManager = null;
        
        String s = " and a.numpla = b.numpla and c.numrem = a.numrem "+
        "   and d.codciu = b.agcpla and e.codciu = b.oripla "+ 
        "   and f.codciu = b.despla and h.codciu = c.agcrem and i.codciu = c.desrem and j.codciu = c.orirem"+
        "   and g.codcli = c.cliente"+
        "   and b.reg_status = 'C'  "+
        "   order by b.numpla       "+
        "   ) a                     "+
        "   left join cumplido b on (b.tipo_doc = '001' and b.cod_doc = a.numpla)"+
        "   left join cumplido c on (c.tipo_doc = '002' and c.cod_doc = a.numrem)";
        ////System.out.println("-----------Agencia:----"+Agencia+"----");
        if (!Agencia.equalsIgnoreCase("")){
              ////System.out.println("Agencia de reporte:__"+Agencia+"__");
              s = " and b.agcpla = '"+Agencia+"' " + s;
        }
        ////System.out.println(SQL_REPORTEPLAREMPORFECHA + s);
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_REPORTEPLAREMPORFECHA + s);
                st.setString(1,fechaI);
                st.setString(2,FechaF);
                ////System.out.println(st);
                rs = st.executeQuery();
                Reporte =  new LinkedList();
                ////System.out.println("1 "+st);
                while (rs.next()){
                    ReportePlaRemPorFecha Rep = new ReportePlaRemPorFecha();
                    ReportePlaRemPorFecha x = new ReportePlaRemPorFecha();
                    x = Rep.load(rs);
                    int i=0;
                    ////System.out.println("Ya cargue un objeto al reporte  "+i);i++;
                    ////System.out.println("Numpla = "+x.getNumpla()+" Rempla ="+x.getNumRem());
                    ////////////////////////////////////////////////////
                    //  Retorna el numero de documentos de una remesa //
                    ////////////////////////////////////////////////////
                    if (con != null){
                        numdoc = 0;
                        st = con.prepareStatement("select count(numrem) as numdoc from remesa_docto where numrem = '"+x.getNumRem()+"' GROUP BY numrem");
                        rs1 = st.executeQuery();
                        while (rs1.next()){
                                numdoc = rs1.getDouble("numdoc");
                                x.setDocIntRem(numdoc);
                                ////System.out.println("3-1");
                        }
                    }
                    /////////////////////////////////////////////////////////
                    //  Retorna el numero de discrepancias de una planilla //
                    /////////////////////////////////////////////////////////
                    if (con != null){
                        st = con.prepareStatement("select count(numpla) as numdis from discrepancia where numpla = '"+x.getNumpla()+"' GROUP BY numpla");
                        rs2 = st.executeQuery();
                        while (rs2.next()){
                            x.setNumDisPla(rs2.getString("numdis"));
                        }
                    }
                    ////////////////////////////////////////////////
                    //  Retorna el numero de documentos cumplidos //
                    ////////////////////////////////////////////////
                    if (con != null){
                        double doccump = 0;
                        st = con.prepareStatement("select count(numrem) as numdoc from cumplido_doc where numrem = '"+x.getNumRem()+"' GROUP BY numrem");
                        rs3 = st.executeQuery();
                        while (rs3.next()){
                            doccump = rs3.getDouble("numdoc");
                            x.setDocIntPendRem( numdoc - doccump);
                            ////System.out.println("Documentos por cumplir "+(numdoc - doccump)+" numdoc:"+numdoc+"  doccump:"+doccump);
                        }
                        if (doccump==0){
                            x.setDocIntPendRem(numdoc);
                        }
                    }
                    ////////////////////////////////////////////////////
                    //  Retorna la fecha de entrega de la planilla    //
                    ////////////////////////////////////////////////////
                    if (con != null){
                        st = con.prepareStatement("select creation_date  from rep_mov_trafico where numpla = '"+x.getNumpla()+"' and tipo_reporte = 'Entregado'");
                        rs4 = st.executeQuery();
                        while (rs4.next()){
                            creation_date = rs4.getString("creation_date");
                            x.setFecEntPla(creation_date);
                            ////System.out.println("3-4");
                        }
                    }
                    ///////////////////////////////////////////////////////////////////
                    //  Inserta el nombre de las agencias de cumplido de planilla    //
                    ///////////////////////////////////////////////////////////////////
                    
                    if (x.getAgenCumplPla() != null){
                        if (con != null){
                            st = con.prepareStatement("select nomciu  from ciudad where codciu = '"+x.getAgenCumplPla()+"'");
                            rs5 = st.executeQuery();
                            while (rs5.next()){
                                x.setAgenCumplPla(rs5.getString("nomciu"));
                            }
                        }
                    }
                    ///////////////////////////////////////////////////////////////////
                    //  Inserta el nombre de las agencias de cumplido de remesas     //
                    ///////////////////////////////////////////////////////////////////
                    
                    if (x.getAgenCumplRem() != null){
                        if (con != null){
                            st = con.prepareStatement("select nomciu  from ciudad where codciu = '"+x.getAgenCumplRem()+"'");
                            rs6 = st.executeQuery();
                            while (rs6.next()){
                                x.setAgenCumplRem(rs6.getString("nomciu"));
                            }
                        }
                    }
                    
                                 
                    if (x.getFechaCumPla() != null) {
                        String creation_datepla = x.getFechaCumPla();
                        ////System.out.println("fecha cumplido "+creation_datepla);
                        Calendar FechaCump = Calendar.getInstance();
                        int year=Integer.parseInt(creation_datepla.substring(0,4));
                        ////System.out.println("year "+year);
                        int month=Integer.parseInt(creation_datepla.substring(5,7))-1;
                        ////System.out.println("month "+month);
                        int date= Integer.parseInt(creation_datepla.substring(8,10));
                        ////System.out.println("date "+date);
                        FechaCump.set(year,month,date,0,0,0); 
                        ////System.out.println("5");
                        Calendar FechaEnt = Calendar.getInstance();
                   
                        if (x.getFecEntPla() != null){
                        
                            int year1=Integer.parseInt(creation_date.substring(0,4));
                            int month1=Integer.parseInt(creation_date.substring(5,7))-1;
                            int date1= Integer.parseInt(creation_date.substring(8,10));
                            FechaEnt.set(year1,month1,date1,0,0,0); 
                    
                    
                            ////System.out.println("6");
                    
                            double dif = FechaEnt.getTimeInMillis() - FechaCump.getTimeInMillis();
                            double seg = dif/1000;
                            double min = seg/60;
                            double Horas = min/60;
                            double dias = Horas/24;
                    
                            x.setDifFecEntCumPla(dias);
                        }else{ x.setDifFecEntCumPla(0);
                        }
                        
                    }
                    ////System.out.println("4");
                    Reporte.add(x);
                    
                    
                } 
                ////System.out.println("Agreg� la lista completa de reportes");
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                      throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
     }
    
     public List ObtenerReporte(){
         ////System.out.println("ObtenerReporte");
         return Reporte;
     }
}
