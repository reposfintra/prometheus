/*
 * MigracionFacturasClientesDao.java
 *
 * Created on 25 de agosto de 2006, 10:11 AM
 */

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import javax.servlet.*;
import com.tsp.util.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.OpDAO;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  ALVARO
 */
public class MigracionFacturasClientesDao extends MainDAO{
    
    /** Creates a new instance of MigracionFacturasClientesDao */
    public MigracionFacturasClientesDao() {
        super("MigracionFacturasClientesDao.xml");
    }
    
    Vector vFacturaCabecera, vFacturaDetalle;
    
    String fecha1="", fecha2="";
    public boolean buscarFacturasClientesCabeceraOracle(String f1,String f2) throws SQLException{
        fecha1=f1;
        fecha2=f2;
        
        f1=f1.replaceAll("-","");
        f2=f2.replaceAll("-","");
        vFacturaCabecera = new Vector();
        boolean resu=true;
        
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            st = this.crearPreparedStatement("SQL_ORACLE_FACTURAS_CLIENTES_CABECERA");
            st.setString(1, f1);
            st.setString(2, f2);
            ////System.out.println("QUERY: "+st.toString());
            rs = st.executeQuery();
            while (rs.next()) {
                
                factura f = new factura();
                f.setFactura(rs.getString(1));
                f.setDescripcion(rs.getString(2));
                f.setCodcli(rs.getString(3));
                f.setFecha_vencimiento(rs.getString(4));
                f.setCantidad_items(rs.getInt(5));
                f.setFecha_factura(rs.getString(6));
                f.setValor_factura(rs.getDouble(7));
                f.setValor_facturame(rs.getDouble(8));
                f.setNit(rs.getString(9));
                f.setValor_abono(rs.getDouble(10));
                f.setValor_abonome(rs.getDouble(11));
                f.setValor_saldo(rs.getDouble(12));
                f.setValor_saldome(rs.getDouble(13));
                vFacturaCabecera.add(f);
                
            }
            ////System.out.println("vFacturaCabecera:"+vFacturaCabecera.size());
        }catch(SQLException e){
            resu=false;
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR FACTURAS CABECERAS:" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar( "SQL_ORACLE_FACTURAS_CLIENTES_CABECERA" );
        }
        return resu;
    }
    
    public boolean buscarFacturasClientesDetallesOracle() throws SQLException{
        vFacturaDetalle = new Vector();
        boolean resu=true;
        
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String Factura="";
        String FacturasOracle="";
        
        String rem="",aux1="",aux2="";
        
        for(int i=0;i<vFacturaCabecera.size();i++){
            
            try{
                //System.out.println("antesvFacturaDetalle:"+vFacturaDetalle.size());
                String query = this.obtenerSQL("SQL_ORACLE_FACTURAS_CLIENTES_DETALLES");
                int numeroderem=0;
                FacturasOracle="";
                for(int j=i;j<vFacturaCabecera.size();j++){
                    //System.out.println("numeroderem:"+numeroderem);
                    if(numeroderem<230){
                        //////System.out.println("if:"+j);
                        factura pla = (factura)vFacturaCabecera.get(j);
                        if(!Factura.equals(pla.getFactura())){
                            FacturasOracle+="'"+pla.getFactura()+"'";
                            //PlanillasPostgres+="'"+pla.getNumrem()+"'";
                        }
                        else{
                        }
                        Factura=pla.getFactura();
                    }
                    else{
                        i=j-1;
                        ////System.out.println("else:"+j);
                        break;
                    }
                    numeroderem++;
                    i=j;
                }
                FacturasOracle = FacturasOracle.replaceAll("''","','");
                query = query.replaceAll("#FACTURAS#", FacturasOracle);
                ////System.out.println("QUERY:"+query);
                Connection con = this.conectar("SQL_ORACLE_FACTURAS_CLIENTES_DETALLES");
                st = con.prepareStatement(query);
                rs = st.executeQuery();
                while (rs.next()) {
                    factura_detalle p = new factura_detalle();
                    p.setFactura(rs.getString(1));
                    p.setItem(rs.getInt(2));
                    String desc=rs.getString(3);
                    desc = desc.replaceAll( "'"," " );
                    desc = desc.replaceAll( "\\\\", " " );
                    desc = desc.replaceAll( "&quot;", " " );
                    p.setDescripcion(desc);
                    p.setConcepto(rs.getString(4));
                    p.setNumero_remesa(rs.getString(5));
                    p.setCantidad(new Double(rs.getDouble(6)+""));
                    p.setValor_unitario(rs.getDouble(7));
                    p.setValor_item(rs.getDouble(8));
                    p.setValor_itemme(rs.getDouble(9));
                    try{
                        p.setValor_unitariome(p.getValor_itemme()/p.getCantidad().doubleValue());
                    }
                    catch(Exception t){
                        p.setValor_unitariome(0);
                    }
                    p.setValor_tasa(new Double(rs.getString(10)+""));
                    p.setCodigo_cuenta_contable("");
                    p.setAuxiliar("");
                    
                    if(!rem.equals(p.getNumero_remesa())){
                       
                        
                        //Para encontrar codigo cuenta contable
                        PreparedStatement st2 = null;
                        ResultSet rs2 = null;
                        try{
                            
                            st2 = this.crearPreparedStatement("SQL_ORACLE_CODIGOCUENTACONTABLE");
                            st2.setString(1, p.getNumero_remesa());
                            
                            //////System.out.println(p.getFactura()+" query: "+st2);
                            rs2 = st2.executeQuery();
                            while (rs2.next()) {
                                p.setCodigo_cuenta_contable(rs2.getString(1));
                                p.setAuxiliar(rs2.getString(2)+" "+rs2.getString(3));
                            }
                             rem=p.getNumero_remesa();
                             aux1=p.getAuxiliar();
                            
                        }catch(SQLException e){
                            e.printStackTrace();
                            throw new SQLException("ERROR AL BUSCAR CORRIDAS " + e.getMessage()+"" + e.getErrorCode());
                        }
                        finally{
                            if(st2 != null){
                                st2.close();
                            }
                            desconectar("SQL_ORACLE_CODIGOCUENTACONTABLE");
                        }
                        //
                    }
                    else{
                        p.setNumero_remesa(rem);
                        p.setAuxiliar(aux1);
                    }
                    vFacturaDetalle.add(p);
                }
                ////System.out.println("despues vFacturaDetalle:"+vFacturaDetalle.size());
                
            }catch(SQLException e){
                resu=false;
                e.printStackTrace();
                throw new SQLException("ERROR AL BUSCAR FACTURAS DETALLES ORACLE: " + e.getMessage()+"" + e.getErrorCode());
            }
            finally{
                if(st != null){
                    st.close();
                }
                desconectar("SQL_ORACLE_FACTURAS_CLIENTES_DETALLES");
            }
            
        }
        return resu;
    }
    
    /**
     * Getter for property fecha1.
     * @return Value of property fecha1.
     */
    public java.lang.String getFecha1() {
        return fecha1;
    }
    
    /**
     * Setter for property fecha1.
     * @param fecha1 New value of property fecha1.
     */
    public void setFecha1(java.lang.String fecha1) {
        this.fecha1 = fecha1;
    }
    
    /**
     * Getter for property fecha2.
     * @return Value of property fecha2.
     */
    public java.lang.String getFecha2() {
        return fecha2;
    }
    
    /**
     * Setter for property fecha2.
     * @param fecha2 New value of property fecha2.
     */
    public void setFecha2(java.lang.String fecha2) {
        this.fecha2 = fecha2;
    }
    
    public boolean ingresarFacturaDetalle(String login,String distrito,String base) throws SQLException {
        System.gc();
        PreparedStatement ps = null;
        int j=0;
        int i=0;
        boolean h=true;
        try{
            ps = this.crearPreparedStatement("SQL_INGRESAR_FACTURA_DETALLE");
            
            while(i<vFacturaDetalle.size()){
                j=0;
                ps.clearBatch();
                for( i=i; i<vFacturaDetalle.size();i++){
                    
                    ps.clearParameters();
                    factura_detalle f = (factura_detalle)vFacturaDetalle.get(i);
                    
                    ps.setString(1,distrito);
                    ps.setString(2,"FAC");
                    ps.setString(3,f.getFactura());
                    ps.setString(4,""+f.getItem());
                    ps.setString(5,"");
                    ps.setString(6, f.getConcepto());
                    ps.setString(7,f.getNumero_remesa());
                    ps.setString(8,f.getDescripcion());
                    ps.setString(9,f.getCodigo_cuenta_contable());
                    ps.setString(10,""+f.getCantidad());
                    ps.setString(11,""+f.getValor_unitario());
                    ps.setString(12,""+f.getValor_unitariome());
                    ps.setString(13,""+f.getValor_item());
                    ps.setString(14, ""+f.getValor_itemme());
                    ps.setString(15, ""+f.getValor_tasa());
                    ps.setString(16,"");
                    ps.setString(17,base);
                    ps.setString(18,""+ login);
                    ps.setString(19,""+ login);
                    ps.setString(20,""+ f.getAuxiliar());
                    ps.addBatch();
                    
                    j++;
                    if(j>25000){
                        i++;
                        break;    
                    }  
                }
                ////System.out.println("Factura insert:"+ps.toString());
                ps.executeBatch();
                if(i==vFacturaDetalle.size()){
                    break;
                }
            }
            
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            if(ps != null){
                ps.close();
            }
            desconectar("SQL_INGRESAR_FACTURA_DETALLE");
        }
        return h;
    }
    
    public boolean ingresarFacturaCabecera(String login,String distrito,String base) throws SQLException {
        System.gc();
        PreparedStatement ps = null;
         int j=0;
        int i=0;
        boolean h=true;
        try{
            ps = this.crearPreparedStatement("SQL_INGRESAR_FACTURA_CABECERA");
            while(i<vFacturaCabecera.size()){
                j=0;
                ps.clearBatch();
                for(i=i; i<vFacturaCabecera.size();i++){
                    ps.clearParameters();
                    
                    factura factu = (factura)vFacturaCabecera.get(i);
                    
                    ps.setString(1,""+distrito);
                    ps.setString(2,factu.getFactura());
                    ps.setString(3,""+factu.getNit());
                    ps.setString(4,""+factu.getCodcli());
                    ps.setString(5,"");
                    ps.setString(6, ""+factu.getFecha_factura());
                    ps.setString(7, ""+factu.getFecha_vencimiento());
                    ps.setString(8, ""+factu.getDescripcion().replaceAll("'\'",""));
                    ps.setString(9, "");
                    ps.setString(10,""+factu.getValor_factura());
                    ps.setString(11,""+factu.getValor_abono());
                    ps.setString(12,""+factu.getValor_saldo());
                    ps.setString(13,""+factu.getValor_facturame());
                    ps.setString(14,"0");
                    ps.setString(15, "");
                    ps.setString(16,""+factu.getCantidad_items());
                    ps.setString(17,"");
                    ps.setString(18, "");
                    ps.setString(19, "");
                    ps.setString(20, ""+base);
                    ps.setString(21, ""+login);
                    ps.setString(22, ""+login);
                    ps.setString(23, ""+factu.getValor_abonome());
                    ps.setString(24, ""+factu.getValor_saldome());
                    ps.addBatch();
                     j++;
                    if(j>25000){
                        i++;
                        break;    
                    }  
                }
                ////System.out.println("Factura_det insert:"+ps.toString());
                ps.executeBatch();
                if(i==vFacturaCabecera.size()){
                    break;
                }
            }
            
        }catch(SQLException e){
            
            e.printStackTrace();
            return false;
        }finally{
            if(ps != null){
                ps.close();
            }
            desconectar("SQL_INGRESAR_FACTURA_CABECERA");
        }
        return h;
    }
    
    /**
     * Getter for property vFacturaCabecera.
     * @return Value of property vFacturaCabecera.
     */
    public java.util.Vector getVFacturaCabecera() {
        return vFacturaCabecera;
    }
    
    /**
     * Setter for property vFacturaCabecera.
     * @param vFacturaCabecera New value of property vFacturaCabecera.
     */
    public void setVFacturaCabecera(java.util.Vector vFacturaCabecera) {
        this.vFacturaCabecera = vFacturaCabecera;
    }
    
    /**
     * Getter for property vFacturaDetalle.
     * @return Value of property vFacturaDetalle.
     */
    public java.util.Vector getVFacturaDetalle() {
        return vFacturaDetalle;
    }
    
    /**
     * Setter for property vFacturaDetalle.
     * @param vFacturaDetalle New value of property vFacturaDetalle.
     */
    public void setVFacturaDetalle(java.util.Vector vFacturaDetalle) {
        this.vFacturaDetalle = vFacturaDetalle;
    }
    
}
