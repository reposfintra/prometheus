/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tsp.perfiles.model.DAOS.*;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.perfilRiesgo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author JM-DESARROLLO
 */
public class PerfilesDeRiesgoDao extends MainDAO  {
    
    
        /** Creates a new instance of PerfilDAO */
    public PerfilesDeRiesgoDao() {
        super("perfilesDeRiesgoDAO.xml", "fintra");
    }
     
    
    private perfilRiesgo perfilRiesgo;
    private Unidad_Negocio Unidad_Negocio;

  public ArrayList<perfilRiesgo> ListarPerfiles()throws SQLException {
      
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_PERFILES_RIESGOS";
         ArrayList<perfilRiesgo> lista = new ArrayList<>();
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                //st.setString( 1, usuario );
                rs = st.executeQuery();
                //vperfilesR = new Vector();                
                while(rs.next()){
                    perfilRiesgo = new perfilRiesgo();
                    perfilRiesgo.setId(rs.getInt("id"));
                    perfilRiesgo.setMonto_maximo(rs.getDouble("monto_maximo"));
                    perfilRiesgo.setMonto_minimo(rs.getDouble("monto_minimo")); 
                    perfilRiesgo.setCant_max_cred(rs.getInt("cant_max_cred"));
                    perfilRiesgo.setDescripcion(rs.getString("descripcion"));
                    perfilRiesgo.setReg_status(rs.getString("reg_status"));
                    perfilRiesgo.setEliminar("Eliminar");
                    perfilRiesgo.setModificar("Modificar ");
                    lista.add(perfilRiesgo);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
    
  
    public String EliminarPerfiles(String idPerfil,String nuevoEstado)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_PERFILES_RIESGOS";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, nuevoEstado );
                st.setString( 2, idPerfil );
             int est= st.executeUpdate();    
            if(est>0){
                 respuesta="ok";
            }
            else{
                respuesta ="Error al eliminar el perfil";
            }
           
            }
        }catch(SQLException e){
             respuesta ="Error al eliminar el perfil";
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return respuesta;
    }

   
    
        public String EliminarUnidadesAsignadas(String idPerfil)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_UNIDADES_ASIGNADAS";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, idPerfil );
             int est= st.executeUpdate();  
                 respuesta= "ok";
            }
        }catch(SQLException e){
             respuesta= "Error al eliminar las unidades de negocio asociadas al perfil";
            // throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return respuesta;
    }
        
    public String CrearPerfiles(String monto_maximo, String  monto_minimo, 
                                String  cant_max_cred,  String  descripcion,
                                String usuarioSesion,String  dstrct)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CREAR_PERFILES_RIESGOS";
        String respuesta = "";
        try {
            //Crear validaciones para insertar los perfiles
            con = this.conectarJNDIFintra();
            if (con != null) {     
              if(monto_maximo.equals("") || monto_minimo.equals("") || cant_max_cred.equals("") ||descripcion.equals("") ||usuarioSesion.equals("")  || dstrct.equals(""))
              {
                respuesta= "Todos los campos son obligatorios";
              }else if(Double.parseDouble(monto_minimo) >= Double.parseDouble(monto_maximo)){
                 respuesta="El monto m�nimo no puede ser mayor ni igual al monto maximo";
              } else {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setDouble(1, Double.parseDouble(monto_maximo) );
                st.setDouble( 2,Double.parseDouble( monto_minimo) );
                st.setDouble( 3, Double.parseDouble(cant_max_cred));
                st.setString( 4, descripcion );
                st.setString( 5, usuarioSesion );
                st.setString( 6, dstrct );
                int est= st.executeUpdate();
             
                if(est>0){
                  //LLamar a la funcion que guarda las unidades de negocios
                  respuesta= "ok";
                  
                }else {
                 respuesta= "Error al crear el perfil";
                }
             }  
            }else {
                 respuesta= "Error al crear el perfil";
            }
        }catch(SQLException e){
            String mensaje=e.getMessage();
            
            int nombreDuplicado= mensaje.indexOf("duplicate key violates unique constraint \"uq_descripcion\"");
            int confDuplicada= mensaje.indexOf("duplicate key violates unique constraint \"uq_configuracion\"");
            if(nombreDuplicado!=-1){
               respuesta="Ya se encentra registrado un perfil con el mismo nombre";
            }else if(confDuplicada!=-1){
                respuesta="Ya se encentra registrado un perfil con configuraciones iguales";
            }else{
               respuesta="Error la crear el perfil";
            }
            //throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
               
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        
        return respuesta;
    }
    

        public String buscarIdPerfil(String nombrePerfil)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ID_PERFIL";
        String idPerfil="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                //vperfilesR = new Vector();     
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, nombrePerfil );
                rs = st.executeQuery();
               // vperfilesR = new Vector();                
                while(rs.next()){
                  idPerfil = rs.getString("id");
                }
               /* if(idPerfil==""){
                //vperfilesR.add( "debe ingresar el nombre del perfil");
                }*/

            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       return idPerfil;
    }

      public String asignarUnidadNeg(String  idPerfil,
                                    String unidadNegocio,
                                    String creation_user,
                                    String dstrct)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ASIGNAR_UNIDADES_AL_PERFIL";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
              //  vperfilesR = new Vector();     
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setInt(1, Integer.parseInt(idPerfil) );
                st.setInt(2,Integer.parseInt( unidadNegocio) );
                st.setString( 3, creation_user );
                st.setString( 4, dstrct );
               int est= st.executeUpdate();
             
            if(est>0){
                  respuesta = "ok";
            }
            else{
                respuesta = "error";
            }

            }
        }catch(SQLException e){
             respuesta = "error";
          //  throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
       }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return respuesta;
    }
                
     public ArrayList<perfilRiesgo> ListarPerfil(String idPerfil)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_PERFIL_RIESGO";
         ArrayList<perfilRiesgo> lista = new ArrayList<>(); 
       
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, idPerfil );
                rs = st.executeQuery();            
                while(rs.next()){
                    perfilRiesgo = new perfilRiesgo();
                    perfilRiesgo.setId(rs.getInt("id"));
                    perfilRiesgo.setMonto_maximo(rs.getDouble("monto_maximo"));
                    perfilRiesgo.setMonto_minimo(rs.getDouble("monto_minimo")); 
                    perfilRiesgo.setCant_max_cred(rs.getInt("cant_max_cred"));
                    perfilRiesgo.setDescripcion(rs.getString("descripcion"));
                    perfilRiesgo.setEliminar("Eliminar");
                    perfilRiesgo.setModificar("Modificar ");
                    lista.add( perfilRiesgo);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
     
       public String ModificarPerfiles(String idPerfil, String monto_maximo, String  monto_minimo, 
                              String  cant_max_cred,  String  descripcion,String usuarioSesion, String  dstrct)throws SQLException 
                             {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR_PERFIL_RIESGO";
        String respuesta="";
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
               // vperfilesR = new Vector();        
              if(monto_maximo.equals("") || monto_minimo.equals("") || cant_max_cred.equals("") ||descripcion.equals("") ||usuarioSesion.equals("")  || dstrct.equals(""))
              {
                  respuesta="Todos los campos son obligatorios";
              }else if(Double.parseDouble(monto_minimo) >= Double.parseDouble(monto_maximo)){
                 respuesta="El monto m�nimo no puede ser mayor ni igual al monto maximo";
                }else{
                 
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setDouble(1, Double.parseDouble(monto_maximo) );
                st.setDouble( 2,Double.parseDouble( monto_minimo) );
                st.setDouble( 3, Double.parseDouble(cant_max_cred));
                st.setString( 4, descripcion );
                st.setString( 5,usuarioSesion  );
                st.setString( 6, dstrct );
                st.setString( 7, idPerfil );
                
                int est= st.executeUpdate();
             
                if(est>0){
                //Guardar el texto en un vector
                respuesta="ok";
                }else{
                respuesta="Error al modificar el perfil";
                }
           
             }  
              
            }else{
                respuesta="Error al modificar el perfil";
            }
        }catch(SQLException e){
            String mensaje=e.getMessage();
            
             int nombreDuplicado= mensaje.indexOf("duplicate key violates unique constraint \"uq_descripcion\"");
            int confDuplicada= mensaje.indexOf("duplicate key violates unique constraint \"uq_configuracion\"");
            if(nombreDuplicado!=-1){
               respuesta="Ya se encentra registrado un perfil con el mismo nombre";
            }else if(confDuplicada!=-1){
                respuesta="Ya se encentra registrado un perfil con configuraciones iguales";
            }else{
               respuesta="Error al modificar el perfil";
            }
            
            //throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return respuesta;
    }
       
            public ArrayList<Unidad_Negocio> ListarUniNego()throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_UNIDADES_DE_NEGOCIO";
         ArrayList<Unidad_Negocio> lista = new ArrayList<>(); 
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
               // st.setString( 1, id );
                rs = st.executeQuery();
              //  vUnidadNegocio = new Vector();                
                while(rs.next()){
                    Unidad_Negocio = new Unidad_Negocio();
                    Unidad_Negocio.setId(rs.getInt("id"));
                    Unidad_Negocio.setNombre(rs.getString("cod"));
                    Unidad_Negocio.setDescripcion(rs.getString("descripcion"));
                    lista.add( Unidad_Negocio);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
    
      public ArrayList<Unidad_Negocio> ListarUniNegoPerf(String idPerfil)throws SQLException {
       Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_UNIDADES_DE_NEGOCIO_DE_UN_PERFIL";
         ArrayList<Unidad_Negocio> lista = new ArrayList<>(); 
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString( 1, idPerfil );
                rs = st.executeQuery();
               // vUnidadNegocio = new Vector();                
                while(rs.next()){
                    Unidad_Negocio = new Unidad_Negocio();
                    Unidad_Negocio.setId(rs.getInt("id_unidad"));
                    //Unidad_Negocio.setNombre(rs.getString("cod_unidad"));
                    //Unidad_Negocio.setDescripcion(rs.getString("desc_unidad"));
                    lista.add( Unidad_Negocio);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }  
        return lista;
    }
}
