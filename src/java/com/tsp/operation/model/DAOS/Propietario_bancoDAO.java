package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import java.util.*;

public class Propietario_bancoDAO extends MainDAO {
    private Vector vector;
    
    /** Creates a new instance of ClienteDAO */
    public Propietario_bancoDAO() {
        super("PropietarioBancoDAO.xml");
    }
    public Propietario_bancoDAO(String dataBaseName) {
        super("PropietarioBancoDAO.xml", dataBaseName);
    }

/**
 *
 * @param BancoPo
 * @throws SQLException
 */
     public void agregarBancoPropietario(Banco_Propietario BancoPo)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
         String query = "SQL_INGRESAR_PROPIETARIO";//JJCastro fase2
           
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, BancoPo.getDistrito());
            st.setString(2, BancoPo.getPropietario());
            st.setString(3, BancoPo.getHc());
            st.setString(4, BancoPo.getBanco());           
            st.setString(5, BancoPo.getSucursal());
            st.setString(6, BancoPo.getCreation_user());
            st.setString(7, BancoPo.getBase());
            System.out.println("query adicionar "+st.toString());
            st.executeUpdate();
           
        }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA SQL_INGRESAR_PROPIETARIO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
    //BUSQUEDA SI EXISTE EL BANCO PROPIETARIO EN LA TABLA BANCO_PROPIETARIO
/**
 * 
 * @param Propietario
 * @return
 * @throws SQLException
 */
    public boolean existeBancoP(String Propietario)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_PROPIETARIO_CONSULTA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement("SQL_PROPIETARIO_CONSULTA");//JJCastro fase2
                st.setString(1, Propietario);
                rs = st.executeQuery();
                return rs.next();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE existeBancoP " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

/**
 *
 * @param Propietario
 * @param HC
 * @param Banco
 * @throws Exception
 */
    public void BusquedaBancoPro(String Propietario,String HC,String Banco ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        Banco_Propietario BancoPo = null;
        vector = new Vector();
        String query = "SQL_BUSCAR_BANCOPROPIETARIO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            int cont = 1;
            st.setString(1, "%"+Propietario+"%");
            st.setString(2, "%"+HC+"%");
            st.setString(3, "%"+Banco+"%");
            
            rs = st.executeQuery();
            System.out.println("query buscar banco propietario "+st.toString());
            while(rs.next()){
                BancoPo = new Banco_Propietario();
                
                
                BancoPo.setDistrito((rs.getString("dstrct")!=null)?rs.getString("dstrct"):"");
                BancoPo.setPropietario((rs.getString("propietario")!=null)?rs.getString("propietario"):"");
                BancoPo.setHc((rs.getString("hc")!=null)?rs.getString("hc"):"");
                BancoPo.setBanco((rs.getString("banco")!=null)?rs.getString("banco"):"");
                BancoPo.setSucursal((rs.getString("sucursal")!=null)?rs.getString("sucursal"):"");
                BancoPo.setBase((rs.getString("base")!=null)?rs.getString("base"):"");
                BancoPo.setCreation_user((rs.getString("creation_user")!=null)?rs.getString("creation_user"):"");
                BancoPo.setDescripcionHc((rs.getString("descripcionhc")!=null)?rs.getString("descripcionhc"):"");
              
                 this.vector.addElement( BancoPo );

		
            }
            
        }}catch(Exception e) {
            throw new Exception("Error al Busqueda del  SQL_BUSCAR_BANCOPROPIETARIO [ClienteDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    
    //FUNCION PARA BUSCAR DATOS PARA MODIFICAR EL PROPIEATARIO DEL BANCO
/**
 * 
 * @param Propietario
 * @param HC
 * @param Distrito
 * @throws Exception
 */
     public void BusquedaDatosBancoPro(String Propietario,String HC,String Distrito ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        Banco_Propietario BancoPo = null;
        vector = new Vector();
        String query = "SQL_BUSCAR_DATOS_BANCOPROPIETARIO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            int cont = 1;
            st.setString(1, Distrito);
            st.setString(2, Propietario);
            st.setString(3, HC);
            
            rs = st.executeQuery();
            System.out.println("query SQL_BUSCAR_DATOS_BANCOPROPIETARIO propietario "+st.toString());
            if(rs.next()){
                BancoPo = new Banco_Propietario();
                
                
                BancoPo.setDistrito((rs.getString("dstrct")!=null)?rs.getString("dstrct"):"");
                BancoPo.setPropietario((rs.getString("propietario")!=null)?rs.getString("propietario"):"");
                BancoPo.setHc((rs.getString("hc")!=null)?rs.getString("hc"):"");
                BancoPo.setBanco((rs.getString("banco")!=null)?rs.getString("banco"):"");
                BancoPo.setSucursal((rs.getString("sucursal")!=null)?rs.getString("sucursal"):"");
                BancoPo.setBase((rs.getString("base")!=null)?rs.getString("base"):"");
                BancoPo.setCreation_user((rs.getString("creation_user")!=null)?rs.getString("creation_user"):"");
               
                 this.vector.addElement( BancoPo );

		
            }
            
        }}catch(Exception e) {
            throw new Exception("Error al Busqueda del  SQL_BUSCAR_DATOS_BANCOPROPIETARIO [ClienteDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
     //funcion para modificar los datos de propietario banco
/** *
 *
 * @param BancoPo
 * @throws SQLException
 */
    public void ModificarBancoPropietario(Banco_Propietario BancoPo)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR_BANCOPROPIETARIO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, BancoPo.getBanco());           
            st.setString(2, BancoPo.getSucursal());
            st.setString(3, BancoPo.getDistrito());
            st.setString(4, BancoPo.getPropietario());
            st.setString(5, BancoPo.getHc());
             System.out.println("query modificar "+st.toString());
            st.executeUpdate();
           
        }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA SQL_MODIFICAR_BANCOPROPIETARIO " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    //funcion para anular un banco propietario
/**
 *
 * @param distrito
 * @param Propietario
 * @param hc
 * @throws Exception
 */
    public void AnularBancoPro(String distrito,String Propietario,String hc) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR_BANCO_PROPIETARIO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                st.setString(2, Propietario);
                st.setString(3, hc);

                System.out.println("query para anular -->>>  " + st.toString());
                st.executeUpdate();
            }
        }catch(Exception e) {
            throw new Exception("Error al SQL_ANULAR_BANCO_PROPIETARIO Cliente [ClienteDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
}


