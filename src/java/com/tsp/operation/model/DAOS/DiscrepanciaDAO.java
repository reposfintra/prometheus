/************************************************************************
 * Nombre clase: DiscrepanciaDAO.java                                   *
 * Descripci�n: Clase que maneja las consultas de las discrepancia y    *
 *              los productos de las discrepancias.                     *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: Created on 23 de septiembre de 2005, 08:43 AM                 *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;

public class DiscrepanciaDAO extends MainDAO{
    private Discrepancia discrepancia;
    private Discrepancia discrepancia2;
    
    private Vector VecDiscrepancias;
    //Diogenes 04.01.2006
    private Vector VecClientes;
    //Karen 03-02-2006
    private Producto producto;
    
    //jose de la rosa 2006-09-15
    private Discrepancia cabDiscrepancia;
    private Vector itemsDiscrepancia = new Vector();
    
    
    private Vector VecUbicaciones;
    
    private final String SQL_ANULAR_DISCREPANCIA = "update discrepancia set rec_status='A',last_update='now()', user_update=? where num_discre = ? and numpla = ? and numrem = ? and tipo_doc = ? and documento = ? and tipo_doc_rel = ? and documento_rel = ?";
    
    private final String SQL_ANULAR_DISCREPANCIA_GENERAL = "update discrepancia_producto set rec_status='A',last_update='now()', user_update=? where num_discre=? and numpla=?";
    
    private final String SQL_ANULAR_DISCREPANCIA_PRODUCTO_GENERAL = "update discrepancia set rec_status='A',last_update='now()', user_update=? where num_discre=? and numpla=?";
    
    private final String SQL_ANULAR_DISCREPANCIA_PRODUCTO = "update discrepancia_producto set rec_status='A',last_update='now()', user_update=? where num_discre=? and numpla=? and numrem=? and tipo_doc=? and documento=? and tipo_doc_rel=? and documento_rel=? and creation_date=?";
    
    private final String SQL_ANULAR_PRODUCTO = "update discrepancia_producto set rec_status='A',last_update='now()', user_update=? where num_discre=? and numpla=? and numrem=? and tipo_doc=? and documento=? and tipo_doc_rel=? and documento_rel=? and cod_producto = ? and cod_cdiscrepancia = ?";
    
    private final String SQL_INSERT = "insert into discrepancia ( numpla, numrem, tipo_doc, documento,tipo_doc_rel, documento_rel,fec_devolucion, nro_rech_cli, reportado, contacto, ubicacion, ret_pago, observacion, rec_status, last_update, user_update, creation_date, creation_user,base,distrito ) "+
    "values (?,?,?,?,?,?,?,?,?,?,?,?,?, '' , 'now()', ?, 'now()', ?,?,?)";
    
    private final String SQL_INSERT_DISCREPANCIA_PRODUCTO = "insert into discrepancia_producto ( num_discre, numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, cod_producto, descripcion, cod_unidad, cod_cdiscrepancia, cantidad, causa_dev, responsable, valor, rec_status, last_update, user_update, creation_date, creation_user, base, distrito, grupo_mail ) "+
    "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, '' , 'now()', ?, 'now()', ?,?,?,?)";
    
    private final String SQL_EXISTE_DISCREPANCIA = "select * from discrepancia  where rec_status!='A' and "+
    "numpla = ? and numrem = ? and tipo_doc = ? and documento = ? and tipo_doc_rel = ? and documento_rel = ? AND distrito = ?";
    
    private final String SQL_EXISTE_DISCREPANCIA_PRODUCTO = "select * from discrepancia_producto  where rec_status!='A' and "+
    "num_discre = ? and numpla = ? and numrem = ? and tipo_doc = ? and documento = ? and cod_producto = ? and cod_cdiscrepancia = ? AND distrito = ?";
    
    private final String SQL_LISTAR_DISCREPANCIAS = "Select * from discrepancia where rec_status != 'A'";
    
    private final String SQL_LISTAR_DISCREPANCIAS_PRODUCTO = "Select * from discrepancia_producto "+
    "where rec_status != 'A' and "+
    "num_discre = ? and "+
    "numpla = ? and "+
    "numrem = ? and "+
    "tipo_doc = ? and "+
    "documento = ? and " +
    "tipo_doc_rel = ? and "+
    "documento_rel = ? AND " +
    "distrito = ? ORDER BY COD_PRODUCTO";
    
    private final String SQL_SEARCH_DISCREPANCIA = "select * from discrepancia where rec_status != 'A' and numpla = ? and numrem = ? and tipo_doc = ? and documento = ? and tipo_doc_rel = ? and documento_rel = ? AND distrito = ?";
    
    private final String SQL_SEARCH_DISCREPANCIA_PRODUCTO = "select * from discrepancia_producto where rec_status != 'A' and num_discre = ? and numpla = ? and numrem = ? and tipo_doc = ? and documento = ? and tipo_doc_rel = ? and documento_rel = ? and cod_producto = ? and cod_cdiscrepancia = ?" +
    " and rec_status != 'A' and creation_date = ? AND distrito = ?";
    
    private final String UPDATE_DISCREPANCIA = "update discrepancia set fec_devolucion=?, nro_rech_cli=?, reportado=?, contacto=?, ubicacion=?, ret_pago=?, observacion = ?, last_update='now()', user_update=?, rec_status='' "+
    "where num_discre = ? and numpla = ? and numrem = ? and tipo_doc = ? and documento = ? and tipo_doc_rel = ? and documento_rel = ?";
    
    private final String UPDATE_DISCREPANCIA_PRODUCTO = "update discrepancia_producto set descripcion=?, cod_unidad=?, cantidad=?, causa_dev=?, responsable=?, valor = ?, last_update='now()', user_update=?, rec_status='' "+
    "where num_discre=? and numpla=? and numrem=? and tipo_doc=? and documento=? and tipo_doc_rel=? and documento_rel=? and cod_producto=? and cod_cdiscrepancia=? and creation_date = ?";
    
    private final String UPDATE_DISCREPANCIA_PRODUCTO_GENERAL = "update discrepancia_producto set cod_unidad=?, cantidad=?, valor = ?, last_update='now()', user_update=?, rec_status='' where num_discre=? and numpla=? and creation_date = ? ";
    
    private final String UPDATE_DISCREPANCIA_GENERAL = "update discrepancia set ret_pago=?, observacion = ?, last_update='now()', user_update=? where num_discre = ? and numpla = ? ";
    
    private final String SQL_UPDATE_SANCION = "update discrepancia set cod_discrepancia=?, valor=?, observacion=?, referencia=?, responsable=?, contacto=?,"
    + "last_update='now()', user_update=?, rec_status=' ' where cod_planilla=? and creation_date=?";
    
    private final String SQL_LISTAR_DISCREPANCIAS_SIN_CIERRE = "select distinct d.numpla, d.numrem,d.fecha_cierre, d.usuario_cierre, td1.document_name, d.tipo_doc, d.tipo_doc_rel, d.documento_rel, c.nomcli, p.plaveh, n.nombre, d.creation_date as fecha, d.documento, d.num_discre "+
    "from discrepancia as d "+
    "LEFT JOIN remesa r on ( d.numrem  = r.numrem    ) "+
    "LEFT JOIN cliente c on ( r.cliente =  c.codcli  ) "+
    "LEFT JOIN planilla p on ( d.numpla = p.numpla   ) "+
    "LEFT JOIN placa pl on ( p.plaveh = pl.placa     ) "+
    "LEFT JOIN tbldoc td1 on ( d.tipo_doc  = td1.document_type ) "+
    "LEFT JOIN nit n on ( pl.conductor = n.cedula ) "+
    "where d.fecha_cierre = '0099-01-01 00:00:00' and "+
    "d.rec_status != 'A' ORDER BY d.numpla, d.numrem";
    
    private final String SQL_LISTAR_DISCREPANCIAS_CON_CIERRE = "select distinct d.numpla, d.numrem,d.fecha_cierre, d.usuario_cierre, td1.document_name, d.tipo_doc, d.tipo_doc_rel, d.documento_rel, c.nomcli, p.plaveh, n.nombre, d.creation_date as fecha, d.documento, d.num_discre "+
    "from discrepancia as d "+
    "LEFT JOIN remesa r on ( d.numrem  = r.numrem    ) "+
    "LEFT JOIN cliente c on ( r.cliente =  c.codcli  ) "+
    "LEFT JOIN planilla p on ( d.numpla = p.numpla   ) "+
    "LEFT JOIN placa pl on ( p.plaveh = pl.placa     ) "+
    "LEFT JOIN tbldoc td1 on ( d.tipo_doc  = td1.document_type ) "+
    "LEFT JOIN nit n on ( pl.conductor = n.cedula ) "+
    "where d.fecha_cierre BETWEEN ? AND ? AND "+
    "d.rec_status != 'A' ORDER BY d.numpla, d.numrem";
    
    //jose 2007-02-15
    private final String SQL_LISTAR_DISCREPANCIAS_PLANILLA  =   "SELECT DISTINCT "+
                                                                "	d.numpla, "+
                                                                "	d.fecha_cierre, "+
                                                                "	d.usuario_cierre, "+
                                                                "	d.numrem, "+
                                                                "	td1.document_name, "+
                                                                "	d.tipo_doc, "+
                                                                "	d.documento_rel, "+
                                                                "	d.tipo_doc_rel, "+
                                                                "	d.fecha_cierre, "+
                                                                "	d.usuario_cierre, "+
                                                                "	get_nombrecliente( r.cliente ) AS nomcli, "+
                                                                "	p.plaveh, "+
                                                                "	get_nombrenit( p.cedcon ) AS nombre, "+
                                                                "	d.creation_date as fecha, "+
                                                                "	d.documento, "+
                                                                "	d.num_discre "+
                                                                "FROM "+
                                                                "	discrepancia as d, "+
                                                                "	tbldoc as td1, "+
                                                                "	remesa as r, "+
                                                                "	planilla as p "+
                                                                "WHERE "+
                                                                "	p.numpla = ? AND "+
                                                                "	distrito = ? AND "+
                                                                "	d.rec_status != 'A' AND "+
                                                                "	td1.document_type = d.tipo_doc AND "+
                                                                "	r.numrem = d.numrem AND "+
                                                                "	r.estado != 'A' AND "+
                                                                "	d.numpla = p.numpla AND "+
                                                                "	p.reg_status != 'A' "+
                                                                "ORDER BY "+
                                                                "	d.numrem";
    //Jose 04/11/05
    
    private final String SQL_INSERT_CIERRE_PRODUCTO  = "update discrepancia set "+
    "usuario_cierre = ?, "+
    "fecha_cierre=?, "+
    "valor=?, "+
    "referencia=?, "+
    "nota_debito=?, "+
    "usuario_nota_debito=?, "+
    "fecha_nota_debito=?, "+
    "nota_credito=?, "+
    "usuario_nota_credito=?, "+
    "fecha_nota_credito=?, "+
    "observacion_cierre=? "+
    "where num_discre=? "+
    "and numpla=? "+
    "and numrem=? "+
    "and tipo_doc=? "+
    "and documento=? "+
    "and tipo_doc_rel=? "+
    "and documento_rel=?";
    
    private final String SQL_PRODUCTOS_NO_CERRADOS  = "SELECT * FROM DISCREPANCIA_PRODUCTO WHERE NUM_DISCRE = ? AND rec_status != 'A' AND FECHA_CIERRE != '0099-01-01 00:00:00'";
    
    private final String SQL_PRODUCTOS_CERRADOS  = "SELECT * FROM DISCREPANCIA_PRODUCTO WHERE NUM_DISCRE = ? AND rec_status != 'A' AND FECHA_CIERRE = '0099-01-01 00:00:00'";
    
    private final String SQL_CIERRE_DISCREPANCIA  = "update discrepancia set usuario_cierre=?, fecha_cierre='now()' "+
    "where num_discre = ? and numpla = ? and numrem = ? and tipo_doc = ? and documento = ? and tipo_doc_rel = ? and documento_rel = ?";
    
    private final String SQL_SEARCH_DETALLE = "Select * from discrepancia where tipo_registro like ? and cod_discrepancia like ? and cod_planilla like ? and referencia like ? and responsable like ? and contacto like ? and rec_status != 'A'";
    
    private final String SQL_LISTAR_DISCREPANCIAS_GENERAL = "select distinct d.numpla, d.valor AS valor_aprobado, d.fecha_cierre, d.usuario_cierre, d.ret_pago, p.cod_cdiscrepancia, p.cantidad, p.valor, p.cod_unidad, d.observacion, d.num_discre, d.referencia, d.nota_debito "+
    "from discrepancia as d, discrepancia_producto as p "+
    "where d.num_discre = p.num_discre and "+
    "d.numpla = p.numpla and "+
    "d.numpla = ? and "+
    "d.numrem = p.numrem and "+
    "d.numrem = '' and "+
    "d.tipo_doc = p.tipo_doc and "+
    "d.tipo_doc = '' and "+
    "d.documento = p.documento and "+
    "d.documento = '' and "+
    "d.tipo_doc_rel = p.tipo_doc_rel and "+
    "d.tipo_doc_rel = '' and "+
    "d.documento_rel = p.documento_rel and "+
    "d.documento_rel = '' and d.distrito = ? AND "+
    "d.rec_status != 'A' ORDER BY d.numpla, d.num_discre ";
    
    private final String SQL_SEARCH_DISCREPANCIAS_GENERAL = "select DISTINCT d.numpla, d.ret_pago, p.cod_cdiscrepancia, p.cantidad, p.valor, p.cod_unidad, d.observacion, d.observacion_cierre, d.num_discre, p.creation_date, d.valor AS valor_aprobado, d.referencia, d.nota_debito, d.nota_credito, d.fecha_cierre, d.usuario_cierre "+
    "from discrepancia as d, discrepancia_producto as p "+
    "where d.num_discre = p.num_discre and "+
    "d.numpla = p.numpla and "+
    "d.num_discre = ? and "+
    "d.numpla = ? and "+
    "p.numpla = ? and "+
    "d.numrem = p.numrem and "+
    "d.numrem = '' and "+
    "d.tipo_doc = p.tipo_doc and "+
    "d.tipo_doc = '' and "+
    "d.documento = p.documento and "+
    "d.documento = '' and "+
    "d.tipo_doc_rel = p.tipo_doc_rel and "+
    "d.tipo_doc_rel = '' and "+
    "d.documento_rel = p.documento_rel and "+
    "d.documento_rel = '' and d.distrito = ? AND "+
    "d.rec_status != 'A'";
    
    private final String SQL_EXISTE_PLANILLA = "select numpla from discrepancia  where rec_status!='A' and numpla=? ";
    
    private final String SQL_CIERRE_DISCREPANCIA_GENERAL = "update discrepancia set usuario_cierre=?, fecha_cierre='now()' where num_discre = ? and numpla = ? ";
    
    private final String SQL_INSERT_CIERRE_PRODUCTO_GENERAL = "update discrepancia_producto set valor_aprobado=?, referencia=?, nota_contable=?, usuario_cierre = ?, fecha_cierre='now()' where num_discre=? and numpla=? and creation_date = ? ";
    //rodrigo
    private final String SQL_LISTAR_DISCREPANCIAS_TODAS =   "SELECT DISTINCT "+
                                                            "	d.fecha_cierre, "+
                                                            "	d.usuario_cierre, "+
                                                            "	d.numpla, "+
                                                            "	d.numrem, "+
                                                            "	td1.document_name, "+
                                                            "	d.tipo_doc, "+
                                                            "	d.documento_rel, "+
                                                            "	d.tipo_doc_rel, "+
                                                            "	get_nombrecliente( r.cliente ) AS nomcli,  "+
                                                            "	p.plaveh, "+
                                                            "	get_nombrenit( p.cedcon ) AS nombre, "+
                                                            "	d.creation_date as fecha, "+
                                                            "	d.documento, "+
                                                            "	d.num_discre "+
                                                            "FROM "+
                                                            "	discrepancia as d "+
                                                            "	LEFT JOIN remesa r on ( d.numrem  = r.numrem  AND r.estado != 'A'  ) "+
                                                            "	LEFT JOIN planilla p on ( d.numpla = p.numpla  AND p.reg_status != 'A' ) "+
                                                            "	LEFT JOIN tbldoc td1 on ( d.tipo_doc  = td1.document_type ) "+
                                                            "WHERE "+
                                                            "	d.numpla = ? AND "+
                                                            "        d.rec_status != 'A' "+
                                                            "ORDER BY "+
                                                            "	d.numrem ";
    
    //Tito Andr�s
    
    private final String SQL_LISTAR_SANCIONES = "Select * from discrepancia where cod_planilla=?" +
    " and rec_status != 'A' and tipo_registro='S' order by creation_date";
    
    private final String SQL_AUTORIZAR_SANCION = "update discrepancia set autorizado=?, fecha_autoriza='now()', cantidad_autorizada=0," +
    " valor_autorizado=?, last_update='now()', user_update=? where cod_planilla=? and creation_date=?";
    
    private final String SQL_INSERT_INVENTARIO = "INSERT INTO inventario_discrepancia (num_discre, numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, cod_producto, cod_cdiscrepancia, cantidad_original, "+
    "cantidad_inventario, ubicacion, valor, codcli, last_update, user_update, creation_date, creation_user, base, dstrct ) VALUES "+
    "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,'now()',?,'now()',?,?,?)";
    
    //Diogenes 20.0.2006
    private final String SQL_LISTAR_CLIENTE = "SELECT DISTINCT a.codcli, b.nomcli " +
    "  FROM inventario_discrepancia a, " +
    "       cliente b                  " +
    "  WHERE                         " +
    "        a.codcli = b.codcli     " +
    " AND  a.cantidad_inventario > 0 " +
    " AND  a.dstrct = ? ";
    
    private final String SQL_LISTAR_UBICACION =
    "SELECT " +
    "     b.cod_ubicacion, b.descripcion" +
    " FROM " +
    "     (SELECT " +
    "            DISTINCT ubicacion  " +
    "  FROM  inventario_discrepancia  " +
    "   WHERE codcli =  ?" +
    "     AND dstrct = ? )a" +
    "  LEFT JOIN ubicacion b  ON (b.cod_ubicacion = a.ubicacion) ";
    
    private final String SQL_LISTAR_PRODUCTO =
    "SELECT a.*," +
    "       b.descripcion   " +
    " FROM         " +
    " (SELECT num_discre, " +
    "       numpla, " +
    "       numrem, " +
    "       tipo_doc, " +
    "       documento, " +
    "       tipo_doc_rel, " +
    "       documento_rel," +
    "       cod_producto," +
    "       cod_cdiscrepancia," +
    "       creation_date," +
    "       ubicacion," +
    "       cantidad_inventario," +
    "       codcli," +
    "       valor" +
    " FROM  inventario_discrepancia" +
    "  WHERE codcli = ? " +
    "        AND ubicacion = ?" +
    "        AND cantidad_inventario > 0" +
    "        AND dstrct = ? )a" +
    "    LEFT JOIN producto B  ON (B.codigo = A.cod_producto)" +
    "    ORDER BY b.descripcion ";
    
    private static final String SQL_INSERTAR_MOVDISCREPANCIA = "INSERT INTO movimiento_discrepancia (nueva_numpla,num_discre, numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, cod_producto, cod_cdiscrepancia, cantidad, ubicacion, valor, codcli, last_update, user_update, creation_date, creation_user, dstrct, base) "+
    " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,'now()',?,'now()',?,?,?)";
    
    private static final String SQL_ACTUALIZAR_INVENTARIO = "UPDATE inventario_discrepancia  " +
    " SET cantidad_inventario = (cantidad_inventario - ? )  " +
    " WHERE    " +
    "        dstrct = ?" +
    "    AND num_discre = ?  " +
    "    AND numpla = ?    " +
    "    AND numrem  = ?   " +
    "    AND tipo_doc = ?  " +
    "    AND documento = ? " +
    "    AND tipo_doc_rel = ? " +
    "    AND documento_rel = ? " +
    "    AND cod_producto = ? " +
    "    AND cod_cdiscrepancia = ? " +
    "    AND creacion_discrepancia = ?";
    
    
    private static final String SQL_PRODUCTOS_MOVDISCREPANCIA = " SELECT a.*, " +
    "   b.descripcion    " +
    "   FROM          " +
    "   (SELECT num_discre,  " +
    "    numpla, " +
    "    numrem,  " +
    "    tipo_doc,  " +
    "    documento,  " +
    "    tipo_doc_rel,  " +
    "    documento_rel, " +
    "    cod_producto, " +
    "    cod_cdiscrepancia, " +
    "    creation_date, " +
    "    ubicacion, " +
    "    cantidad, " +
    "    codcli, " +
    "    valor " +
    "    FROM  movimiento_discrepancia " +
    "    WHERE codcli like ?  " +
    "    AND ubicacion like ? " +
    "    AND creation_date BETWEEN ? AND ? " +
    "    AND dstrct = ? )a " +
    "    LEFT JOIN producto B  ON (B.codigo = A.cod_producto)" +
    "    ORDER BY b.descripcion";
    
    //Karen 03-02-2006
    private final String UPDATE_PRODUCTOS= "update inventario_discrepancia set numpla_despacho=?, numrem_despacho=?, user_update=? "+
    "where dstrct =? and num_discre = ? and cod_producto||creacion_discrepancia= ? and ubicacion=?";
    
    
    private final String DESMARCAR_PRODUCTOS= "update inventario_discrepancia set numpla_despacho='', numrem_despacho='' "+
    " where numpla_despacho=?";
    private final String BUSCAR_PROD_PLANILLA= "select cod_producto||creacion_discrepancia||'/'|| num_discre||'/'||ubicacion as codigo from inventario_discrepancia where numpla_despacho=?";
    
    
    private final String SQL_ANULAR_TODOS_DISCREPANCIA_PRODUCTO = "update discrepancia_producto set rec_status='A',last_update='now()', user_update=? where num_discre=? and numpla=? and numrem=? and tipo_doc=? and documento=? and tipo_doc_rel=? and documento_rel=?";
    
    
    /** Creates a new instance of DiscrepanciaDAO */
    public DiscrepanciaDAO() {
        super("DiscrepanciaDAO.xml");
    }
    
    /**
     * Metodo: getDiscrepancia, permite retornar un objeto de registros de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Discrepancia getDiscrepancia() {
        return discrepancia;
    }
    
    /**
     * Metodo: setDiscrepancia, permite obtener un objeto de registros de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setDiscrepancia(Discrepancia discrepancia) {
        this.discrepancia = discrepancia;
    }
    
    /**
     * Metodo: getDiscrepancias, permite retornar un vector de registros de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getDiscrepancias() {
        return VecDiscrepancias;
    }
    /**
     * Metodo: setDiscrepancias, permite obtener un vector de registros de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setDiscrepancias(Vector VecDiscrepancias) {
        this.VecDiscrepancias = VecDiscrepancias;
    }
    
    /**
     * Metodo: insertarCierreDiscrepancia, permite realizar un cierre de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertarCierreDiscrepancia() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_CIERRE_DISCREPANCIA);
                st.setString(1, discrepancia.getUsuario_modificacion());
                st.setInt(2, discrepancia.getNro_discrepancia());
                st.setString(3, discrepancia.getNro_planilla());
                st.setString(4, discrepancia.getNro_remesa());
                st.setString(5, discrepancia.getTipo_doc());
                st.setString(6, discrepancia.getDocumento());
                st.setString(7, discrepancia.getTipo_doc_rel());
                st.setString(8, discrepancia.getDocumento_rel());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL REALIZAR EL CIERRE DE DISCREPANCIA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Metodo: insertarCierreProducto, permite realizar un cierre de una discrepancia por documento.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertarCierreDiscrepanciaDocumento() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_INSERT_CIERRE_PRODUCTO);
                st.setString(1,  discrepancia.getUsuario_cierre());
                st.setString(2,  discrepancia.getFecha_cierre());
                st.setDouble(3,  discrepancia.getCantidad_autorizada());
                st.setString(4,  discrepancia.getReferencia());
                st.setString(5,  discrepancia.getNota_debito());
                st.setString(6,  discrepancia.getUsuario_nota_debito());
                st.setString(7,  discrepancia.getFecha_nota_debito());
                st.setString(8,  discrepancia.getNota_credito());
                st.setString(9,  discrepancia.getUsuario_nota_credito());
                st.setString(10, discrepancia.getFecha_nota_credito());
                st.setString(11, discrepancia.getObservacion_cierre());
                st.setInt    (12, discrepancia.getNro_discrepancia());
                st.setString(13, discrepancia.getNro_planilla());
                st.setString(14, discrepancia.getNro_remesa());
                st.setString(15, discrepancia.getTipo_doc());
                st.setString(16, discrepancia.getDocumento());
                st.setString(17, discrepancia.getTipo_doc_rel());
                st.setString(18, discrepancia.getDocumento_rel());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR UN CIERRE DE DISCREPSNCIA POR DOCUMENTOS " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    
    /**
     * Metodo: existeDiscrepancia, permite buscar una discrepancia dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de la planilla, numero de la remesa, tipo de documento y documento
     * @version : 1.0
     */
    public boolean existeDiscrepancia(String numpla, String numrem, String tipo_doc, String documento, String tipo_doc_rel, String documento_rel, String distrito ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(this.SQL_EXISTE_DISCREPANCIA);
                st.setString(1, numpla);
                st.setString(2, numrem);
                st.setString(3, tipo_doc);
                st.setString(4, documento);
                st.setString(5, tipo_doc_rel);
                st.setString(6, documento_rel);
                st.setString(7, distrito);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA DISCREPANCIA EXISTE " + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return sw;
    }
    
    /**
     * Metodo: productosNoCerrados, informa si los productos de discrepancia no estan cerrados.
     * @autor : Ing. Jose de la rosa
     * @param : numero de discrepancia
     * @version : 1.0
     */
    public boolean productosNoCerrados(int numdiscre) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(this.SQL_PRODUCTOS_NO_CERRADOS);
                st.setInt(1, numdiscre);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LOS PRODUCTOS CERRADOS DE UNA DISCREPANCIA " + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return sw;
    }
    
    /**
     * Metodo: productosCerrados, informa si los productos de discrepancia estan cerrados.
     * @autor : Ing. Jose de la rosa
     * @param : numero de discrepancia
     * @version : 1.0
     */
    public boolean productosCerrados(int numdiscre) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(this.SQL_PRODUCTOS_CERRADOS);
                st.setInt(1, numdiscre);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LOS PRODUCTOS CERRADOS DE UNA DISCREPANCIA " + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return sw;
    }
    
    /**
     * Metodo: existeDiscrepanciaProducto, permite buscar un producto de  discrepancia dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de discrepancia, numero de la planilla, numero de la remesa, tipo de documento, documento,
     *          codigo del producto y codigo de la discrepancia.
     * @version : 1.0
     */
    public boolean existeDiscrepanciaProducto(int num_discre, String numpla, String numrem, String tipo_doc, String documento, String cod_producto, String cod_cdiscrepancia, String distrito ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(this.SQL_EXISTE_DISCREPANCIA_PRODUCTO);
                st.setInt(1, num_discre);
                st.setString(2, numpla);
                st.setString(3, numrem);
                st.setString(4, tipo_doc);
                st.setString(5, documento);
                st.setString(6, cod_producto);
                st.setString(7, cod_cdiscrepancia);
                st.setString(8, distrito);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA DISCREPANCIA DEL PRODUCTO " + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return sw;
    }
    
    /**
     * Metodo: listDiscrepancia, permite listar todas las discrepancias
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listDiscrepancia()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_DISCREPANCIAS);
                rs= st.executeQuery();
                this.VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setNro_planilla(rs.getString("numpla"));
                    discrepancia.setNro_remesa(rs.getString("numrem"));
                    discrepancia.setTipo_doc(rs.getString("tipo_doc"));
                    discrepancia.setDocumento(rs.getString("documento"));
                    discrepancia.setFecha_devolucion(rs.getString("fec_devolucion"));
                    discrepancia.setNumero_rechazo(rs.getString("nro_rech_cli"));
                    discrepancia.setReportado(rs.getString("reportado"));
                    discrepancia.setContacto(rs.getString("contacto"));
                    discrepancia.setUbicacion(rs.getString("ubicacion"));
                    discrepancia.setRetencion(rs.getString("ret_pago"));
                    discrepancia.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                    discrepancia.setDocumento_rel(rs.getString("documento_rel"));
                    VecDiscrepancias.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: listDiscrepancia, permite listar todas las discrepancias dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de discrepancia, numero de la planilla, numero de la remesa, tipo de documento y documento.
     * @version : 1.0
     */
    public void listDiscrepanciaProducto(int num_discre, String numpla, String numrem, String tipo_doc, String documento, String tipo_doc_rel, String documento_rel, String dstrct )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_DISCREPANCIAS_PRODUCTO);
                st.setInt(1, num_discre);
                st.setString(2, numpla);
                st.setString(3, numrem);
                st.setString(4, tipo_doc);
                st.setString(5, documento);
                st.setString(6, tipo_doc_rel);
                st.setString(7, documento_rel);
                st.setString(8, dstrct);
                rs= st.executeQuery();
                this.VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia2 = new Discrepancia();
                    discrepancia2.setNro_planilla(rs.getString("numpla"));
                    discrepancia2.setNro_remesa(rs.getString("numrem"));
                    discrepancia2.setTipo_doc(rs.getString("tipo_doc"));
                    discrepancia2.setDocumento(rs.getString("documento"));
                    discrepancia2.setCod_producto(rs.getString("cod_producto"));
                    discrepancia2.setDescripcion(rs.getString("descripcion"));
                    discrepancia2.setUnidad(rs.getString("cod_unidad"));
                    discrepancia2.setCod_discrepancia(rs.getString("cod_cdiscrepancia"));
                    discrepancia2.setCausa_dev(rs.getString("causa_dev")!=null?rs.getString("causa_dev"):"");
                    discrepancia2.setResponsable(rs.getString("responsable")!=null?rs.getString("responsable"):"");
                    discrepancia2.setCantidad(rs.getDouble("cantidad"));
                    discrepancia2.setValor(rs.getDouble("valor"));
                    discrepancia2.setFecha_creacion(rs.getString("creation_date"));
                    discrepancia2.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                    discrepancia2.setDocumento_rel(rs.getString("documento_rel"));
                    discrepancia2.setGrupo (rs.getString("grupo_mail")!=null?rs.getString("grupo_mail"):"");
                    discrepancia2.setNro_discrepancia ( num_discre );
                    VecDiscrepancias.add(discrepancia2);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: searchDiscrepancia, permite buscar una discrepancia dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de la planilla, numero de la remesa, tipo de documento y documento.
     * @version : 1.0
     */
    public void searchDiscrepancia(String numpla, String numrem, String tipo_doc, String documento, String tipo_doc_rel, String documento_rel, String distrito )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        discrepancia = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SQL_SEARCH_DISCREPANCIA);
                st.setString(1, numpla);
                st.setString(2, numrem);
                st.setString(3, tipo_doc);
                st.setString(4, documento);
                st.setString(5, tipo_doc_rel);
                st.setString(6, documento_rel);
                st.setString(7, distrito);
                rs= st.executeQuery();
                if(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setNro_discrepancia(rs.getInt("num_discre"));
                    discrepancia.setNro_planilla(rs.getString("numpla"));
                    discrepancia.setNro_remesa(rs.getString("numrem"));
                    discrepancia.setTipo_doc(rs.getString("tipo_doc"));
                    discrepancia.setDocumento(rs.getString("documento"));
                    discrepancia.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                    discrepancia.setDocumento_rel(rs.getString("documento_rel"));
                    discrepancia.setFecha_devolucion(rs.getString("fec_devolucion"));
                    discrepancia.setNumero_rechazo(rs.getString("nro_rech_cli"));
                    discrepancia.setReportado(rs.getString("reportado"));
                    discrepancia.setContacto(rs.getString("contacto"));
                    discrepancia.setUbicacion(rs.getString("ubicacion"));
                    discrepancia.setRetencion(rs.getString("ret_pago"));
                    discrepancia.setObservacion(rs.getString("observacion"));
                    
                    discrepancia.setFecha_cierre(rs.getString("fecha_cierre"));
                    discrepancia.setUsuario_cierre(rs.getString("usuario_cierre"));
                    
                    discrepancia.setObservacion_cierre(rs.getString("observacion_cierre"));
                    discrepancia.setReferencia(rs.getString("referencia"));
                    discrepancia.setValor_autorizado(rs.getDouble("valor"));
                    
                    discrepancia.setNota_credito(rs.getString("nota_credito"));
                    discrepancia.setUsuario_nota_credito(rs.getString("usuario_nota_credito"));
                    discrepancia.setFecha_nota_credito(rs.getString("fecha_nota_credito"));
                    
                    discrepancia.setNota_debito(rs.getString("nota_debito"));
                    discrepancia.setUsuario_nota_debito(rs.getString("usuario_nota_debito"));
                    discrepancia.setFecha_nota_debito(rs.getString("fecha_nota_debito"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA SERCH " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: listDiscrepanciaPlanilla, permite buscar una discrepancia dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de la planilla.
     * @version : 1.0
     */
    public void listDiscrepanciaPlanilla(String numpla, String distrito)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_DISCREPANCIAS_PLANILLA);
                st.setString(1,numpla);
                st.setString(2,distrito);
                rs= st.executeQuery();
                this.VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setNro_planilla(rs.getString("numpla"));
                    discrepancia.setNro_remesa(rs.getString("numrem"));
                    discrepancia.setTipo_doc(rs.getString("tipo_doc"));
                    discrepancia.setDocumento(rs.getString("documento"));
                    discrepancia.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                    discrepancia.setDocumento_rel(rs.getString("documento_rel"));
                    discrepancia.setNom_documento(rs.getString("document_name"));
                    discrepancia.setNro_discrepancia(rs.getInt("num_discre"));
                    discrepancia.setNom_conductor(rs.getString("nombre"));
                    discrepancia.setCliente(rs.getString("nomcli"));
                    discrepancia.setPlaca(rs.getString("plaveh"));
                    discrepancia.setFecha_creacion(rs.getString("fecha"));
                    discrepancia.setFecha_cierre(rs.getString("fecha_cierre"));
                    discrepancia.setUsuario_cierre(rs.getString("usuario_cierre"));
                    VecDiscrepancias.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: searchDiscrepanciaProducto, permite buscar un producto de discrepancia dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de discrepancia, numero de la planilla, numero de la remesa, tipo de documento, documento
     *          codigo del producto, codigo de la discrepancia y la fecha de creacion del producto de discrepancia.
     * @version : 1.0
     */
    public void searchDiscrepanciaProducto(int num_discre, String numpla, String numrem, String tipo_doc, String documento, String tipo_doc_rel, String documento_rel, String cod_producto, String cod_cdiscrepancia, String fecha, String distrito)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        discrepancia = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SQL_SEARCH_DISCREPANCIA_PRODUCTO);
                st.setInt(1, num_discre);
                st.setString(2, numpla);
                st.setString(3, numrem);
                st.setString(4, tipo_doc);
                st.setString(5, documento);
                st.setString(6, tipo_doc_rel);
                st.setString(7, documento_rel);
                st.setString(8, cod_producto);
                st.setString(9, cod_cdiscrepancia);
                st.setString(10, fecha);
                st.setString(11, distrito);
                ////System.out.println(" SQL "+st);
                rs= st.executeQuery();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setNro_discrepancia(rs.getInt("num_discre"));
                    discrepancia.setNro_planilla(rs.getString("numpla"));
                    discrepancia.setNro_remesa(rs.getString("numrem"));
                    discrepancia.setTipo_doc(rs.getString("tipo_doc"));
                    discrepancia.setDocumento(rs.getString("documento"));
                    discrepancia.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                    discrepancia.setDocumento_rel(rs.getString("documento_rel"));
                    discrepancia.setCod_producto(rs.getString("cod_producto"));
                    discrepancia.setDescripcion(rs.getString("descripcion"));
                    discrepancia.setUnidad(rs.getString("cod_unidad"));
                    discrepancia.setCod_discrepancia(rs.getString("cod_cdiscrepancia"));
                    discrepancia.setCausa_dev(rs.getString("causa_dev"));
                    discrepancia.setResponsable(rs.getString("responsable"));
                    discrepancia.setCantidad(rs.getDouble("cantidad"));
                    discrepancia.setValor(rs.getDouble("valor"));
                    discrepancia.setFecha_creacion(rs.getString("creation_date"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: updateDiscrepancia, permite actualizar una discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String updateDiscrepancia() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.UPDATE_DISCREPANCIA);
                st.setString(1, discrepancia.getFecha_devolucion());
                st.setString(2, discrepancia.getNumero_rechazo());
                st.setString(3, discrepancia.getReportado());
                st.setString(4, discrepancia.getContacto());
                st.setString(5, discrepancia.getUbicacion());
                st.setString(6, discrepancia.getRetencion());
                st.setString(7, discrepancia.getObservacion());
                st.setString(8, discrepancia.getUsuario_modificacion());
                st.setInt(9, discrepancia.getNro_discrepancia());
                st.setString(10, discrepancia.getNro_planilla());
                st.setString(11, discrepancia.getNro_remesa());
                st.setString(12, discrepancia.getTipo_doc());
                st.setString(13, discrepancia.getDocumento());
                st.setString(14, discrepancia.getTipo_doc_rel());
                st.setString(15, discrepancia.getDocumento_rel());
                //st.executeUpdate();
                sql = st.toString ();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    
    /**
     * Metodo: updateDiscrepanciaProducto, permite actualizar un producto de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String updateDiscrepanciaProducto() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.UPDATE_DISCREPANCIA_PRODUCTO);
                st.setString(1, discrepancia.getDescripcion());
                st.setString(2, discrepancia.getUnidad());
                st.setDouble(3, discrepancia.getCantidad());
                st.setString(4, discrepancia.getCausa_dev());
                st.setString(5, discrepancia.getResponsable());
                st.setDouble(6, discrepancia.getValor());
                st.setString(7, discrepancia.getUsuario_modificacion());
                st.setInt(8, discrepancia.getNro_discrepancia());
                st.setString(9, discrepancia.getNro_planilla());
                st.setString(10, discrepancia.getNro_remesa());
                st.setString(11, discrepancia.getTipo_doc());
                st.setString(12, discrepancia.getDocumento());
                st.setString(13, discrepancia.getTipo_doc_rel());
                st.setString(14, discrepancia.getDocumento_rel());
                st.setString(15, discrepancia.getCod_producto());
                st.setString(16, discrepancia.getCod_discrepancia());
                st.setString(17, discrepancia.getFecha_creacion());
                //st.executeUpdate();
                sql = st.toString ();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA DISCREPANCIA DEL PRODUCTO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    
    /**
     * Metodo: updateSancion, permite anular una sancion.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void updateSancion() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_UPDATE_SANCION);
                st.setString(1, discrepancia.getCod_discrepancia());
                st.setDouble(2,discrepancia.getValor());
                st.setString(3,discrepancia.getObservacion());
                st.setString(4, discrepancia.getReferencia());
                st.setString(5, discrepancia.getResponsable());
                st.setString(6, discrepancia.getContacto());
                st.setString(7,discrepancia.getUsuario_modificacion());
                st.setString(8,discrepancia.getCod_planilla());
                st.setString(9,discrepancia.getFecha_creacion());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA SANCION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: searchDetalleDiscrepancia, permite listar discrepancias por detalles dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : (String reg, String discre, String numpla,String ref,String resp,String cont)
     * @version : 1.0
     */
    public void searchDetalleDiscrepancia(String reg, String discre, String numpla,String ref,String resp,String cont) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        VecDiscrepancias=null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_SEARCH_DETALLE);
                st.setString(1, reg+"%");
                st.setString(2, discre+"%");
                st.setString(3, numpla+"%");
                st.setString(4, ref+"%");
                st.setString(5, resp+"%");
                st.setString(6, cont+"%");
                rs = st.executeQuery();
                VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setCod_discrepancia(rs.getString("cod_discrepancia"));
                    discrepancia.setCantidad(rs.getDouble("cantidad"));
                    discrepancia.setValor(rs.getDouble("valor"));
                    discrepancia.setRemb_conductor(rs.getString("remb_conductor"));
                    discrepancia.setAutorizado(rs.getString("autorizado"));
                    discrepancia.setFecha_autoriza(rs.getString("fecha_autoriza"));
                    discrepancia.setObservacion(rs.getString("observacion"));
                    discrepancia.setCod_planilla(rs.getString("cod_planilla"));
                    discrepancia.setTipo_registro(rs.getString("tipo_registro"));
                    discrepancia.setFecha_creacion("creation_date");
                    discrepancia.setValor_autorizado(rs.getDouble("valor_autorizado"));
                    discrepancia.setFecha_creacion(rs.getString("creation_date"));
                    discrepancia.setUnidad(rs.getString("unidad"));
                    discrepancia.setReferencia(rs.getString("referencia"));
                    discrepancia.setResponsable(rs.getString("responsable"));
                    discrepancia.setContacto(rs.getString("contacto"));
                    VecDiscrepancias.add(discrepancia);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    //Tito Andr�s
    /**
     *   @autor Tito Andr�s
     *   @param numpla N�mero de la planilla
     */
    public void listarSancionesXplanilla(String numpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SQL_LISTAR_SANCIONES);
                st.setString(1,numpla);
                
                rs = st.executeQuery();
                this.VecDiscrepancias = new Vector();
                
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setCod_discrepancia(rs.getString("cod_discrepancia"));
                    discrepancia.setCantidad(rs.getDouble("cantidad"));
                    discrepancia.setValor(rs.getDouble("valor"));
                    discrepancia.setValor_autorizado(rs.getDouble("valor_autorizado"));
                    discrepancia.setRemb_conductor(rs.getString("remb_conductor"));
                    discrepancia.setAutorizado(rs.getString("autorizado"));
                    discrepancia.setFecha_autoriza(rs.getString("fecha_autoriza"));
                    discrepancia.setObservacion(rs.getString("observacion"));
                    discrepancia.setCod_planilla(rs.getString("cod_planilla"));
                    discrepancia.setTipo_registro(rs.getString("tipo_registro"));
                    discrepancia.setFecha_creacion(rs.getString("creation_date"));
                    VecDiscrepancias.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS DISCREPANCIAS POR NUMERO DE PLANILLA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: anularDiscrepancia, permite anular una discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String anularDiscrepancia() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ANULAR_DISCREPANCIA);
                st.setString(1,discrepancia.getUsuario_modificacion());
                st.setInt(2, discrepancia.getNro_discrepancia());
                st.setString(3, discrepancia.getNro_planilla());
                st.setString(4, discrepancia.getNro_remesa());
                st.setString(5, discrepancia.getTipo_doc());
                st.setString(6, discrepancia.getDocumento());
                st.setString(7, discrepancia.getTipo_doc_rel());
                st.setString(8, discrepancia.getDocumento_rel());
                //st.executeUpdate();
                sql = st.toString ()+";";
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    
    /**
     * Metodo: anularDiscrepanciaProducto, permite anular un producto de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String anularDiscrepanciaProducto() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ANULAR_DISCREPANCIA_PRODUCTO);
                st.setString(1, discrepancia.getUsuario_modificacion());
                st.setInt(2, discrepancia.getNro_discrepancia());
                st.setString(3, discrepancia.getNro_planilla());
                st.setString(4, discrepancia.getNro_remesa());
                st.setString(5, discrepancia.getTipo_doc());
                st.setString(6, discrepancia.getDocumento());
                st.setString(7, discrepancia.getTipo_doc_rel());
                st.setString(8, discrepancia.getDocumento_rel());
                st.setString(9, discrepancia.getFecha_creacion());
                sql = st.toString ();
                //st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LA DISCREPANCIA DEL PRODUCTO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    
    /**
     * Metodo: anularDiscrepanciaProducto, permite anular un producto de discrepancia.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String anularDiscrepanciaTodosProducto() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ANULAR_TODOS_DISCREPANCIA_PRODUCTO);
                st.setString(1, discrepancia.getUsuario_modificacion());
                st.setInt(2, discrepancia.getNro_discrepancia());
                st.setString(3, discrepancia.getNro_planilla());
                st.setString(4, discrepancia.getNro_remesa());
                st.setString(5, discrepancia.getTipo_doc());
                st.setString(6, discrepancia.getDocumento());
                st.setString(7, discrepancia.getTipo_doc_rel());
                st.setString(8, discrepancia.getDocumento_rel());
                sql = st.toString ();
                //st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LA DISCREPANCIA DEL PRODUCTO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    
    /**
     * Metodo: anularProducto, permite anular un producto.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void anularProducto() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ANULAR_PRODUCTO);
                st.setString(1, discrepancia.getUsuario_modificacion());
                st.setInt(2, discrepancia.getNro_discrepancia());
                st.setString(3, discrepancia.getNro_planilla());
                st.setString(4, discrepancia.getNro_remesa());
                st.setString(5, discrepancia.getTipo_doc());
                st.setString(6, discrepancia.getDocumento());
                st.setString(7, discrepancia.getCod_producto());
                st.setString(8, discrepancia.getCod_discrepancia());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LA DISCREPANCIA DEL PRODUCTO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     *   @autor Tito Andr�s Maturana
     *
     */
    public void autorizarSancion() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_AUTORIZAR_SANCION);
                st.setString(1, discrepancia.getAutorizado());
                st.setDouble(2,discrepancia.getValor_autorizado());
                st.setString(3, discrepancia.getAutorizado());
                st.setString(4,discrepancia.getCod_planilla());
                st.setString(5,discrepancia.getFecha_creacion());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     *   @autor Tito Andr�s Maturana
     *
     */
    public void listarDiscrepanciaXplanilla(String numpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SQL_LISTAR_SANCIONES);
                st.setString(1,numpla);
                rs = st.executeQuery();
                this.VecDiscrepancias = new Vector();
                
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setCod_discrepancia(rs.getString("cod_discrepancia"));
                    discrepancia.setCantidad(rs.getDouble("cantidad"));
                    discrepancia.setValor(rs.getDouble("valor"));
                    discrepancia.setRemb_conductor(rs.getString("remb_conductor"));
                    discrepancia.setAutorizado(rs.getString("autorizado"));
                    discrepancia.setFecha_autoriza(rs.getString("fecha_autoriza"));
                    discrepancia.setObservacion(rs.getString("observacion"));
                    discrepancia.setCod_planilla(rs.getString("cod_planilla"));
                    discrepancia.setTipo_registro(rs.getString("tipo_registro"));
                    discrepancia.setFecha_creacion(rs.getString("creation_date"));
                    discrepancia.setUnidad(rs.getString("unidad"));
                    discrepancia.setReferencia(rs.getString("referencia"));
                    discrepancia.setResponsable(rs.getString("responsable"));
                    discrepancia.setContacto(rs.getString("contacto"));
                    VecDiscrepancias.add(discrepancia);
                    
                    //Aqui van PlaAux
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS DISCREPANCIAS POR NUMERO DE PLANILLA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: listDiscrepanciaSinCierre, permite listar una discrepancia sin fecha de cierre
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listDiscrepanciaSinCierre()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_DISCREPANCIAS_SIN_CIERRE);
                rs= st.executeQuery();
                this.VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setNro_planilla( (rs.getString("numpla")!=null)?rs.getString("numpla"):"");
                    discrepancia.setNro_remesa( (rs.getString("numrem")!=null)?rs.getString("numrem"):"");
                    discrepancia.setTipo_doc( (rs.getString("tipo_doc")!=null)?rs.getString("tipo_doc"):"");
                    discrepancia.setDocumento( (rs.getString("documento")!=null)?rs.getString("documento"):"");
                    discrepancia.setNom_documento( (rs.getString("document_name")!=null)?rs.getString("document_name"):"");
                    discrepancia.setNro_discrepancia( (rs.getInt("num_discre")!=0)?rs.getInt("num_discre"):0);
                    discrepancia.setNom_conductor( (rs.getString("nombre")!=null)?rs.getString("nombre"):"");
                    discrepancia.setCliente( (rs.getString("nomcli")!=null)?rs.getString("nomcli"):"");
                    discrepancia.setPlaca( (rs.getString("plaveh")!=null)?rs.getString("plaveh"):"");
                    discrepancia.setTipo_doc_rel( (rs.getString("tipo_doc_rel")!=null)?rs.getString("tipo_doc_rel"):"");
                    discrepancia.setDocumento_rel( (rs.getString("documento_rel")!=null)?rs.getString("documento_rel"):"");
                    discrepancia.setFecha_creacion( (rs.getString("fecha")!=null)?rs.getString("fecha"):"");
                    discrepancia.setFecha_cierre( (rs.getString("fecha_cierre")!=null)?rs.getString("fecha_cierre"):"");
                    discrepancia.setUsuario_cierre( (rs.getString("usuario_cierre")!=null)?rs.getString("usuario_cierre"):"");
                    VecDiscrepancias.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA SIN CIERRE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: listDiscrepanciaConCierre, permite listar una discrepancia con fecha de cierre dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : fecha de inicio y fecha de fin
     * @version : 1.0
     */
    public void listDiscrepanciaConCierre(String fini,String ffin)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_DISCREPANCIAS_CON_CIERRE);
                st.setString(1,fini+" 00:00");
                st.setString(2,ffin+" 23:59");
                rs= st.executeQuery();
                this.VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setNro_planilla( (rs.getString("numpla")!=null)?rs.getString("numpla"):"");
                    discrepancia.setNro_remesa( (rs.getString("numrem")!=null)?rs.getString("numrem"):"");
                    discrepancia.setTipo_doc( (rs.getString("tipo_doc")!=null)?rs.getString("tipo_doc"):"");
                    discrepancia.setDocumento( (rs.getString("documento")!=null)?rs.getString("documento"):"");
                    discrepancia.setNom_documento( (rs.getString("document_name")!=null)?rs.getString("document_name"):"");
                    discrepancia.setNro_discrepancia( (rs.getInt("num_discre")!=0)?rs.getInt("num_discre"):0);
                    discrepancia.setNom_conductor( (rs.getString("nombre")!=null)?rs.getString("nombre"):"");
                    discrepancia.setCliente( (rs.getString("nomcli")!=null)?rs.getString("nomcli"):"");
                    discrepancia.setPlaca( (rs.getString("plaveh")!=null)?rs.getString("plaveh"):"");
                    discrepancia.setTipo_doc_rel( (rs.getString("tipo_doc_rel")!=null)?rs.getString("tipo_doc_rel"):"");
                    discrepancia.setDocumento_rel( (rs.getString("documento_rel")!=null)?rs.getString("documento_rel"):"");
                    discrepancia.setFecha_creacion( (rs.getString("fecha")!=null)?rs.getString("fecha"):"");
                    discrepancia.setFecha_cierre( (rs.getString("fecha_cierre")!=null)?rs.getString("fecha_cierre"):"");
                    discrepancia.setUsuario_cierre( (rs.getString("usuario_cierre")!=null)?rs.getString("usuario_cierre"):"");
                    VecDiscrepancias.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA CON CIERRE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     *   @autor Rodrigo Salazar
     *
     */
    public boolean existeDiscrepanciaPlanilla(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(SQL_EXISTE_PLANILLA);
                st.setString(1, numpla);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA DISCREPANCIA " + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if(st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return sw;
    }
    
    /**
     * Metodo: listDiscrepanciaTodas, permite listar todas discrepancia dado un parametro
     * @autor : Ing. Jose de la rosa
     * @param : numero planilla
     * @version : 1.0
     */
    public void listDiscrepanciaTodas(String numpla)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_DISCREPANCIAS_TODAS);
                st.setString(1,numpla);
                rs= st.executeQuery();
                this.VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setNro_planilla(rs.getString("numpla")!=null?rs.getString("numpla"):"");
                    discrepancia.setNro_remesa(rs.getString("numrem")!=null?rs.getString("numrem"):"");
                    discrepancia.setTipo_doc(rs.getString("tipo_doc")!=null?rs.getString("tipo_doc"):"");
                    discrepancia.setDocumento(rs.getString("documento")!=null?rs.getString("documento"):"");
                    discrepancia.setTipo_doc_rel(rs.getString("tipo_doc_rel")!=null?rs.getString("tipo_doc_rel"):"");
                    discrepancia.setDocumento_rel(rs.getString("documento_rel")!=null?rs.getString("documento_rel"):"");
                    discrepancia.setNom_documento(rs.getString("document_name")!=null?rs.getString("document_name"):"");
                    discrepancia.setNro_discrepancia(rs.getInt("num_discre"));
                    discrepancia.setNom_conductor(rs.getString("nombre")!=null?rs.getString("nombre"):"");
                    discrepancia.setCliente(rs.getString("nomcli")!=null?rs.getString("nomcli"):"");
                    discrepancia.setPlaca(rs.getString("plaveh")!=null?rs.getString("plaveh"):"");
                    discrepancia.setFecha_creacion(rs.getString("fecha")!=null?rs.getString("fecha"):"");
                    discrepancia.setFecha_cierre( (rs.getString("fecha_cierre")!=null)?rs.getString("fecha_cierre"):"");//jose 2007-02-15
                    discrepancia.setUsuario_cierre( (rs.getString("usuario_cierre")!=null)?rs.getString("usuario_cierre"):"");//jose 2007-02-15
                    VecDiscrepancias.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: listDiscrepanciaGeneral, permite listar todas discrepancia generales dado un parametro
     * @autor : Ing. Jose de la rosa
     * @param : numero planilla
     * @version : 1.0
     */
    public void listDiscrepanciaGeneral(String numpla, String distrito)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_DISCREPANCIAS_GENERAL);
                st.setString(1,numpla);
                st.setString(2,distrito);
                rs= st.executeQuery();
                this.VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setNro_planilla(rs.getString("numpla"));
                    discrepancia.setNro_discrepancia(rs.getInt("num_discre"));
                    discrepancia.setRetencion(rs.getString("ret_pago"));
                    discrepancia.setCod_discrepancia(rs.getString("cod_cdiscrepancia"));
                    discrepancia.setCantidad(rs.getDouble("cantidad"));
                    discrepancia.setValor(rs.getDouble("valor"));
                    discrepancia.setUnidad(rs.getString("cod_unidad"));
                    discrepancia.setObservacion(rs.getString("observacion"));
                    discrepancia.setFecha_cierre(rs.getString("fecha_cierre"));
                    discrepancia.setUsuario_cierre(rs.getString("usuario_cierre"));
                    discrepancia.setValor_autorizado(rs.getDouble("valor_aprobado"));
                    discrepancia.setReferencia(rs.getString("referencia"));
                    discrepancia.setNota_contable(rs.getString("nota_debito"));
                    VecDiscrepancias.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA GENERAL " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: searchDiscrepanciaGeneral, permite buscar una discrepancia dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de la planilla, numero de la discrepancia
     * @version : 1.0
     */
    public void searchDiscrepanciaGeneral(String numpla, int num_discre, String distrito)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_SEARCH_DISCREPANCIAS_GENERAL);
                st.setInt(1,num_discre);
                st.setString(2,numpla);
                st.setString(3,numpla);
                st.setString(4,distrito);
                rs= st.executeQuery();
                this.VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia2 = new Discrepancia();
                    discrepancia2.setNro_planilla(rs.getString("numpla"));
                    discrepancia2.setNro_discrepancia(rs.getInt("num_discre"));
                    discrepancia2.setRetencion(rs.getString("ret_pago"));
                    discrepancia2.setCod_discrepancia(rs.getString("cod_cdiscrepancia"));
                    discrepancia2.setCantidad(rs.getDouble("cantidad"));
                    discrepancia2.setValor(rs.getDouble("valor"));
                    discrepancia2.setUnidad(rs.getString("cod_unidad"));
                    discrepancia2.setObservacion(rs.getString("observacion"));
                    discrepancia2.setObservacion_cierre(rs.getString("observacion_cierre"));
                    discrepancia2.setFecha_creacion(rs.getString("creation_date"));
                    discrepancia2.setFecha_cierre(rs.getString("fecha_cierre"));
                    discrepancia2.setUsuario_cierre(rs.getString("usuario_cierre"));
                    discrepancia2.setValor_autorizado(rs.getDouble("valor_aprobado"));
                    discrepancia2.setReferencia(rs.getString("referencia"));
                    discrepancia2.setNota_debito(rs.getString("nota_debito"));
                    discrepancia2.setNota_credito(rs.getString("nota_credito"));
                    VecDiscrepancias.add(discrepancia2);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA DISCREPANCIA GENERAL " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: updateDiscrepanciaProductoGeneral, permite actualizar un producto de discrepancia general.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void updateDiscrepanciaProductoGeneral() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.UPDATE_DISCREPANCIA_PRODUCTO_GENERAL);
                st.setString(1, discrepancia.getUnidad());
                st.setDouble(2, discrepancia.getCantidad());
                st.setDouble(3, discrepancia.getValor());
                st.setString(4, discrepancia.getUsuario_modificacion());
                st.setInt(5, discrepancia.getNro_discrepancia());
                st.setString(6, discrepancia.getNro_planilla());
                st.setString(7, discrepancia.getFecha_creacion());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA DISCREPANCIA DEL PRODUCTO GENERAL " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: updateDiscrepanciaGeneral, permite actualizar una discrepancia general.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void updateDiscrepanciaGeneral() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.UPDATE_DISCREPANCIA_GENERAL);
                st.setString(1, discrepancia.getRetencion());
                st.setString(2, discrepancia.getObservacion());
                st.setString(3, discrepancia.getUsuario_modificacion());
                st.setInt(4, discrepancia.getNro_discrepancia());
                st.setString(5, discrepancia.getNro_planilla());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA DISCREPANCIA GENERAL " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: anularDiscrepanciaGeneral, permite anular una discrepancia general.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void anularDiscrepanciaGeneral() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_ANULAR_DISCREPANCIA_GENERAL);
                st.setString(1, discrepancia.getUsuario_modificacion());
                st.setInt(2, discrepancia.getNro_discrepancia());
                st.setString(3, discrepancia.getNro_planilla());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LA DISCREPANCIA GENERAL " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: anularDiscrepanciaProductoGeneral, permite anular un producto de discrepancia general.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void anularDiscrepanciaProductoGeneral() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_ANULAR_DISCREPANCIA_PRODUCTO_GENERAL);
                st.setString(1, discrepancia.getUsuario_modificacion());
                st.setInt(2, discrepancia.getNro_discrepancia());
                st.setString(3, discrepancia.getNro_planilla());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LA DISCREPANCIA PRODUCTO GENERAL " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo: listClienteDiscrepancia, permite listar los clientes que sus productos
     *         tenga la cantidad > 0
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public void listClienteDiscrepancia(String dstrct )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_CLIENTE);
                st.setString(1, dstrct);
                rs= st.executeQuery();
                this.VecClientes = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setCod_Cliente(rs.getString("codcli"));
                    discrepancia.setCliente(rs.getString("nomcli"));
                    VecClientes.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    /**
     * Metodo: listPoductosCliente, permite listar los clientes que sus productos
     *         tenga la cantidad > 0
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : codigo cliente y ubicacion
     * @version : 1.0
     */
    public void listProductosCliente(String codcliente, String ubicacion, String dstrct  )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_PRODUCTO);
                st.setString(1, codcliente);
                st.setString(2, ubicacion);
                st.setString(3, dstrct );
                rs= st.executeQuery();
                this.VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setNro_discrepancia(rs.getInt("num_discre"));
                    discrepancia.setNro_planilla(rs.getString("numpla"));
                    discrepancia.setNro_remesa(rs.getString("numrem"));
                    discrepancia.setTipo_doc(rs.getString("tipo_doc"));
                    discrepancia.setDocumento(rs.getString("documento"));
                    discrepancia.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                    discrepancia.setDocumento_rel(rs.getString("documento_rel"));
                    discrepancia.setCod_producto(rs.getString("cod_producto"));
                    discrepancia.setCod_discrepancia(rs.getString("cod_cdiscrepancia"));
                    discrepancia.setFecha_creacion(rs.getString("creation_date"));
                    discrepancia.setCantidad(rs.getDouble("cantidad_inventario"));
                    discrepancia.setDescripcion(rs.getString("descripcion"));
                    discrepancia.setUbicacion(rs.getString("ubicacion"));
                    discrepancia.setValor(rs.getDouble("valor"));
                    discrepancia.setCod_Cliente(rs.getString("codcli"));
                    
                    
                    VecDiscrepancias.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: listUbicacion, permite listar la lista de ubicacion de los productos
     *         de un cliente
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public void listUbicacion(String codcliente, String dstrct)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR_UBICACION);
                st.setString(1, codcliente);
                st.setString(2, dstrct);
                rs= st.executeQuery();
                this.VecUbicaciones = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setUbicacion(rs.getString("descripcion"));
                    discrepancia.setCodubicacion(rs.getString("cod_ubicacion"));
                    VecUbicaciones.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo: getUbicaciones, retorna la listas de ubicaciones.
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public Vector getUbicaciones() {
        return VecUbicaciones;
    }
    
    /**
     * Metodo: getUbicaciones, retorna la listas de ubicaciones.
     * @autor : Ing. Diogenes Bastidas Morales
     * @param :
     * @version : 1.0
     */
    public Vector getClientes() {
        return VecClientes;
    }
    /**
     * Metodo insertarMovDiscrepancia, inserta en la bd la los movimiento discrepancia
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void insertarMovDiscrepancia() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_INSERTAR_MOVDISCREPANCIA);
                st.setString(1, discrepancia.getNuevo_numpla());
                st.setInt(2, discrepancia.getNro_discrepancia());
                st.setString(3, discrepancia.getNro_planilla());
                st.setString(4, discrepancia.getNro_remesa());
                st.setString(5, discrepancia.getTipo_doc());
                st.setString(6, discrepancia.getDocumento());
                st.setString(7, discrepancia.getTipo_doc_rel());
                st.setString(8, discrepancia.getDocumento_rel());
                st.setString(9, discrepancia.getCod_producto());
                st.setString(10, discrepancia.getCod_discrepancia());
                st.setDouble(11, discrepancia.getCantidad_nueva());
                st.setString(12, discrepancia.getUbicacion());
                st.setDouble(13, discrepancia.getValor());
                st.setString(14, discrepancia.getCod_Cliente());
                st.setString(15, discrepancia.getUsuario_creacion());
                st.setString(16, discrepancia.getUsuario_modificacion());
                st.setString(17, discrepancia.getDistrito());
                st.setString(18, discrepancia.getBase());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EL MOVIENTO DISCREPANCIA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Metodo , actualizarInventario, actualiza en la el registro de la cantidad despachada
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void actualizarInventario() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ACTUALIZAR_INVENTARIO);
                st.setDouble(1, discrepancia.getCantidad_nueva());
                st.setString(2, discrepancia.getDistrito());
                st.setInt(3, discrepancia.getNro_discrepancia());
                st.setString(4, discrepancia.getNro_planilla());
                st.setString(5, discrepancia.getNro_remesa());
                st.setString(6, discrepancia.getTipo_doc());
                st.setString(7, discrepancia.getDocumento());
                st.setString(8, discrepancia.getTipo_doc_rel());
                st.setString(9, discrepancia.getDocumento_rel());
                st.setString(10, discrepancia.getCod_producto());
                st.setString(11, discrepancia.getCod_discrepancia());
                st.setString(12, discrepancia.getFecha_creacion());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EL MOVIENTO DISCREPANCIA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Metodo: listProductosMovTrafico, permite listar productos despachados de acuerdo al cliente,
     *         ubicacion un rango de fecha almacenados en movimiento_trafico
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : codigo cliente y ubicacion
     * @version : 1.0
     */
    public void listProductosMovTrafico(String codcliente, String ubicacion, String fecini, String fecfin, String dstrct )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_PRODUCTOS_MOVDISCREPANCIA);
                st.setString(1, codcliente+"%");
                st.setString(2, ubicacion+"%");
                st.setString(3, fecini+" 00:00");
                st.setString(4, fecfin+" 23:59");
                st.setString(5, dstrct);
                rs= st.executeQuery();
                this.VecDiscrepancias = new Vector();
                while(rs.next()){
                    discrepancia = new Discrepancia();
                    discrepancia.setNro_discrepancia(rs.getInt("num_discre"));
                    discrepancia.setNro_planilla(rs.getString("numpla"));
                    discrepancia.setNro_remesa(rs.getString("numrem"));
                    discrepancia.setTipo_doc(rs.getString("tipo_doc"));
                    discrepancia.setDocumento(rs.getString("documento"));
                    discrepancia.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                    discrepancia.setDocumento_rel(rs.getString("documento_rel"));
                    discrepancia.setCod_producto(rs.getString("cod_producto"));
                    discrepancia.setCod_discrepancia(rs.getString("cod_cdiscrepancia"));
                    discrepancia.setFecha_creacion(rs.getString("creation_date"));
                    discrepancia.setCantidad(rs.getDouble("cantidad"));
                    discrepancia.setDescripcion(rs.getString("descripcion"));
                    discrepancia.setUbicacion(rs.getString("ubicacion"));
                    discrepancia.setValor(rs.getDouble("valor"));
                    discrepancia.setCod_Cliente(rs.getString("codcli"));
                    
                    
                    VecDiscrepancias.add(discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    /**
     * Metodo: utilizarProducto, marca los campos numpla_despacho y numrem_despacho
     * con el numero de planilla y remesa con que fueron despachados.
     * @autor : Ing. Karen Reales
     * @param :
     * @version : 1.0
     */
    public String utilizarProducto() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                //              private final String UPDATE_PRODUCTOS= "update inventario_discrepancia set numpla_despacho=?, numrem_despacho=?, user_update=? "+
                //              "where dstrct =? and num_discre = ? and cod_producto = ? and ubicacion=?";
                st = con.prepareStatement(this.UPDATE_PRODUCTOS);
                st.setString(1, producto.getNumpla());
                st.setString(2, producto.getNumrem());
                st.setString(3, producto.getUsuario_modificacion());
                st.setString(4, producto.getDistrito());
                st.setString(5, producto.getDiscrepancia());
                st.setString(6, producto.getCodigo());
                st.setString(7, producto.getUbicacion());
                sql = st.toString();
                //st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA UTILIZACION DEL PRODUCTO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    
    /**
     * Getter for property producto.
     * @return Value of property producto.
     */
    public com.tsp.operation.model.beans.Producto getProducto() {
        return producto;
    }
    
    /**
     * Setter for property producto.
     * @param producto New value of property producto.
     */
    public void setProducto(com.tsp.operation.model.beans.Producto producto) {
        this.producto = producto;
    }
    
    /**
     * Metodo: desmarcarProductos, desmarca los campos numpla_despacho y numrem_despacho
     * de los productos relacionados a un numero de planilla.
     * @autor : Ing. Karen Reales
     * @param :
     * @version : 1.0
     */
    public String desmarcarProductos(String numpla) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql="";
        try{
            st = this.crearPreparedStatement("DESMARCAR_PRODUCTOS");
            st.setString(1, numpla);
            sql = st.toString();
            //st.executeUpdate ();
            
        }catch(SQLException e){
            throw new SQLException("ERROR DESMARCANDO LOS PRODUCTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("DESMARCAR_PRODUCTOS");
        }
        return sql;
    }
    /**
     * Metodo: obtenerProductos, retorna un string con los codigos
     * de los productos relacionados a un numero de planilla.
     * @autor : Ing. Karen Reales
     * @param :
     * @version : 1.0
     */
    public String obtenerProductos(String numpla) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String codigo="";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                
                
                st = con.prepareStatement(this.BUSCAR_PROD_PLANILLA);
                st.setString(1, numpla);
                rs = st.executeQuery();
                while (rs.next()){
                    codigo =rs.getString("codigo")+","+ codigo;
                }
                //st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LOS PRODUCTOS RELACIONADOS A LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return codigo;
    }
    /*2006-09-05 */
    
    public String insertarDiscrepancia() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_INSERT);
                st.setString(1, discrepancia.getNro_planilla());
                st.setString(2, discrepancia.getNro_remesa());
                st.setString(3, discrepancia.getTipo_doc());
                st.setString(4, discrepancia.getDocumento());
                st.setString(5, discrepancia.getTipo_doc_rel());
                st.setString(6, discrepancia.getDocumento_rel());
                st.setString(7, discrepancia.getFecha_devolucion());
                st.setString(8, discrepancia.getNumero_rechazo());
                st.setString(9, discrepancia.getReportado());
                st.setString(10, discrepancia.getContacto());
                st.setString(11, discrepancia.getUbicacion());
                st.setString(12, discrepancia.getRetencion());
                st.setString(13, discrepancia.getObservacion());
                st.setString(14, discrepancia.getUsuario_modificacion());
                st.setString(15, discrepancia.getUsuario_creacion());
                st.setString(16, discrepancia.getBase());
                st.setString(17, discrepancia.getDistrito());
                sql = st.toString();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EL DISCREPANCIA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sql;
    }
    
    public String insertarDiscrepanciaProducto() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_INSERT_DISCREPANCIA_PRODUCTO);
                st.setInt(1, discrepancia.getNro_discrepancia());
                st.setString(2, discrepancia.getNro_planilla());
                st.setString(3, discrepancia.getNro_remesa());
                st.setString(4, discrepancia.getTipo_doc());
                st.setString(5, discrepancia.getDocumento());
                st.setString(6, discrepancia.getTipo_doc_rel());
                st.setString(7, discrepancia.getDocumento_rel());
                st.setString(8, discrepancia.getCod_producto());
                st.setString(9, discrepancia.getDescripcion());
                st.setString(10, discrepancia.getUnidad());
                st.setString(11, discrepancia.getCod_discrepancia());
                st.setDouble(12, discrepancia.getCantidad());
                st.setString(13, discrepancia.getCausa_dev());
                st.setString(14, discrepancia.getResponsable());
                st.setDouble(15, discrepancia.getValor());
                st.setString(16, discrepancia.getUsuario_modificacion());
                st.setString(17, discrepancia.getUsuario_creacion());
                st.setString(18, discrepancia.getBase());
                st.setString(19, discrepancia.getDistrito());
                st.setString(20, discrepancia.getGrupo());
                sql = st.toString();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EL DISCREPANCIA DEL PRODUCTO " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sql;
    }
    
    
    
    public String insertarDiscrepanciaInventario() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_INSERT_INVENTARIO);
                st.setInt(1, discrepancia.getNro_discrepancia());
                st.setString(2, discrepancia.getNro_planilla());
                st.setString(3, discrepancia.getNro_remesa());
                st.setString(4, discrepancia.getTipo_doc());
                st.setString(5, discrepancia.getDocumento());
                st.setString(6, discrepancia.getTipo_doc_rel());
                st.setString(7, discrepancia.getDocumento_rel());
                st.setString(8, discrepancia.getCod_producto());
                st.setString(9, discrepancia.getCod_discrepancia());
                st.setDouble(10, discrepancia.getCantidad());
                st.setDouble(11, discrepancia.getCantidad());
                st.setString(12, discrepancia.getUbicacion());
                st.setDouble(13, discrepancia.getValor());
                st.setString(14, discrepancia.getContacto());
                st.setString(15, discrepancia.getUsuario_modificacion());
                st.setString(16, discrepancia.getUsuario_creacion());
                st.setString(17, discrepancia.getBase());
                st.setString(18, discrepancia.getDistrito());
                sql = st.toString();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EL INVENTARIO DISCREPANCIA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return sql;
    }
     /**
     * Metodo: obtenerProductos, retorna un vector con los datos de la discrepancia en un rango de fechas.
     * @autor : Ing. Jose De la rosa
     * @param : fecha inicial. fecha final y estado
     * @version : 1.0
     */
    public void planillasMigradasMIMS ( String fecini, String fecfin, String anulada )throws SQLException{
        Connection     conPostgres = null, conOracle = null;
        PreparedStatement  st      = null, st1 = null;
        ResultSet          rs      = null, rs1 = null;
        VecDiscrepancias           = null;
        discrepancia               = null;
        VecDiscrepancias   = new Vector ();
        String             query   = "SQL_PLANILLA_MIGRADAS_WEB";
        String             query2   = "SQL_PLANILLA_SUBIDA_MIMS";
        try{
            conPostgres = this.conectar (query);
            st1           =   this.crearPreparedStatement ( query2 );
            String filtro =   ( anulada.equals ( "N" ) )?" AND d.rec_status != 'A' " : " AND d.rec_status = 'A' ";
            String sql    =   this.obtenerSQL ( query ).replaceAll ("#estado#", filtro );
            st            =   conPostgres.prepareStatement ( sql );
            st.setString (1, fecini+" 00:00");
            st.setString (2, fecfin+" 23:59");
            rs=st.executeQuery ();
            boolean sw = false;
            while(rs.next ()){
                discrepancia = new Discrepancia ();
                st1.setString (1, rs.getString ("numpla") );
                st1.setString (2, rs.getString ("cod_cdiscrepancia")!=null?rs.getString ("cod_cdiscrepancia"):"" );
                st1.setString (3, rs.getString ("cod_unidad")!=null?rs.getString ("cod_unidad"):"" );
                rs1 = st1.executeQuery ();
                sw = false;
                if(rs1.next ()){
                    sw = true;
                }
                discrepancia.setNro_planilla (rs.getString ("numpla"));
                discrepancia.setFecha_creacion (rs.getString ("creation_date"));
                discrepancia.setCod_discrepancia (rs.getString ("desc_cdiscrepancia")!=null?rs.getString ("desc_cdiscrepancia"):"");
                discrepancia.setDescripcion (rs.getString ("observacion")!=null?rs.getString ("observacion"):"");
                discrepancia.setFecha_migracion ( sw == true ? "SI" : "NO" );
                VecDiscrepancias.add (discrepancia);
                st1.clearParameters ();
            }
            
        }catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
        finally{
            if(st!=null) st.close ();
            if(rs!=null) rs.close ();
            if(st1!=null) st.close ();
            if(rs1!=null) rs.close ();
            this.desconectar (query);
            this.desconectar (query2);
        }
    }
    
    /**
     * Getter for property cabDiscrepancia.
     * @return Value of property cabDiscrepancia.
     */
    public com.tsp.operation.model.beans.Discrepancia getCabDiscrepancia () {
        return cabDiscrepancia;
    }
    
    /**
     * Setter for property cabDiscrepancia.
     * @param cabDiscrepancia New value of property cabDiscrepancia.
     */
    public void setCabDiscrepancia (Discrepancia cabDiscrepancia) {
        this.cabDiscrepancia = cabDiscrepancia;
    }
    
    /**
     * Getter for property itemsDiscrepancia.
     * @return Value of property itemsDiscrepancia.
     */
    public Vector getItemsDiscrepancia () {
        return itemsDiscrepancia;
    }
    
    /**
     * Setter for property itemsDiscrepancia.
     * @param itemsDiscrepancia New value of property itemsDiscrepancia.
     */
    public void setItemsDiscrepancia (Vector itemsDiscrepancia) {
        this.itemsDiscrepancia = itemsDiscrepancia;
    }
    
        /**
     * Setter for property itemsDiscrepancia.
     * @param itemsDiscrepancia New value of property itemsDiscrepancia.
     */
    public void addItemsDiscrepancia ( Discrepancia discre ) {
        this.itemsDiscrepancia.add ( discre );
    }
    
     /**
     * Setter for property itemsDiscrepancia.
     * @param itemsDiscrepancia New value of property itemsDiscrepancia.
     */
    public void deleteItemsDiscrepancia ( int x ) {
        this.itemsDiscrepancia.remove ( x );
    }
    
    /**
     * Setter for property itemsDiscrepancia.
     * @param itemsDiscrepancia New value of property itemsDiscrepancia.
     */
    public void addItemsObjetoDiscrepancia ( int x, Discrepancia discre ) {
        this.itemsDiscrepancia.set ( x, discre );
    }    
    
        /**
     * Setter for property itemsDiscrepancia.
     * @param itemsDiscrepancia New value of property itemsDiscrepancia.
     */
    public void resetItemsObjetoDiscrepancia (  ) {
        this.itemsDiscrepancia = null;
        itemsDiscrepancia = new Vector();
    }
    

    public int numeroDiscrepancia () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        int sql = 0;
        try{
             poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT ( max(num_discre) + 1 ) AS num FROM discrepancia");
                rs= st.executeQuery();
                if(rs.next()){
                    sql = rs.getInt("num");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NUMERO DE DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    
     /**
     * Metodo para grabar una discrepancia desde el programa de importacion
     * @autor mfontalvo
     * @param d, Discrepancia a grabar
     * @throws Exception.
     */
    public void grabarDiscrepanciaImportacion (Discrepancia d) throws Exception {
        if (d!=null){
            
            String queryID      = "SQL_ID";
            String queryInsert1 = "SQL_INSERT_1";
            String queryInsert2 = "SQL_INSERT_2";
            
            Connection        con         = null;
            ResultSet         rs          = null;
            PreparedStatement ps          = null;
            Statement         stb         = null;
            
            try{
             
                con = this.conectar( queryID );
                if (con==null)
                    throw new Exception ("Sin conexion");
                
                con.setAutoCommit(false);
                

                // consecutivo de la discrepancia
                d.setNro_discrepancia( 0 );
                ps = con.prepareStatement( this.obtenerSQL(queryID) );
                rs = ps.executeQuery();
                if (rs.next()){
                    d.setNro_discrepancia( rs.getInt("num") );
                }
                rs.close();
                ps.close();
                
                // inicializacion del batch
                stb = con.createStatement();
                
                
                
                // cabecera de la discrepancia
                ps = con.prepareStatement( this.obtenerSQL(queryInsert1) );
                ps.setInt   (1, d.getNro_discrepancia() );
                ps.setString(2, d.getDistrito()         );
                ps.setString(3, d.getNro_planilla()     );
                ps.setString(4, d.getObservacion()      );
                ps.setString(5, d.getRetencion()        );
                ps.setString(6, d.getUsuario_creacion() );
                ps.setString(7, d.getUsuario_creacion() );
                ps.setString(8, d.getBase()             );                
                stb.addBatch(ps.toString());
                ps.close();
                
                // detalle de la dicrepancia
                ps = con.prepareStatement(this.obtenerSQL(queryInsert2) );
                ps.setInt   (1, d.getNro_discrepancia() );
                ps.setString(2, d.getDistrito()         );
                ps.setString(3, d.getNro_planilla()     );
                ps.setString(4, d.getDescripcion()      );
                ps.setString(5, d.getCod_discrepancia() );
                ps.setString(6, d.getUsuario_creacion() );
                ps.setString(7, d.getUsuario_creacion() );
                ps.setString(8, d.getBase()             );                
                stb.addBatch(ps.toString());
                ps.close();
                
                
                stb.executeBatch();
                con.commit();
                
            } catch (Exception ex){
                
                if (con!=null)
                    con.rollback();
                ex.printStackTrace();
                throw new Exception (ex.getMessage());
                
            } finally {
                if (rs  != null) rs.close();
                if (ps  != null) ps.close();
                if (stb != null) stb.close();
                if (con!=null){
                    con.setAutoCommit(true);
                    this.desconectar(queryID);
                }
                
            }
        }
    }
    
     /**
     * Metodo para obtener las dicrepancias a migrar
     * @autor mfontalvo
     * @param usuario que migra sus propias dicrepancias.
     * @return vector de nuevas discrepancias
     * @throws Exception.
     **/    
    public Vector obtenerDiscrepanciasPendientes(String usuario) throws Exception{
        ResultSet         rs          = null;
        PreparedStatement ps          = null;
        String            query       = "SQL_DISCREPANCIAS_IMPORTACION";
        Vector            datos       = new Vector();
        try{
            
            ps = this.crearPreparedStatement(query);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            while (rs.next()){
                Discrepancia d = new Discrepancia();
                d.setDistrito        ( rs.getString("dstrct")       );
                d.setNro_planilla    ( rs.getString("numpla")       );
                d.setObservacion     ( rs.getString("observacion")  );
                d.setObservacion     ( rs.getString("observacion")  );
                d.setRetencion       ( rs.getString("retiene_pago") );
                d.setCod_discrepancia( rs.getString("coddis") );
                d.setUsuario_creacion( rs.getString("creation_user") );
                datos.add(d);
            }
            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (rs  != null) rs.close();
            if (ps  != null) ps.close();
            this.desconectar(query);
        }
        return datos;
    }
    
}
