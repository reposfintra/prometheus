/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;

/**
 *
 * @author egonzalez
 */
public interface UsuarioAdmonDAO {
    
    public abstract JsonObject cargarCombo(String query, String filtro);
    
    public abstract JsonObject buscar(String distrito, String usuario);
    
    public abstract JsonObject crear(JsonObject info);
    
    public abstract JsonObject Acturalizar(JsonObject info);
    
    public abstract JsonObject cargaInicial(String usuario);

    public abstract JsonObject buscarPerfilesFenalco(String distrito, String usuario);
    
    public abstract String buscarAppm(String usuario);
    }
