/*
 * Nombre        ReporteTiemposDeViajeConductoresDAO.java
 * Descripci�n   Clase encargada de accesar a los datos necesarios para mostrar el reporte de tiempos
 *               de viaje para conductores.
 * Autor         Alejandro Payares
 * Fecha         23 de enero de 2006, 10:43 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.exceptions.InformationException;
import java.util.*;
import com.tsp.util.Util;

/**
 * Clase encargada de accesar a los datos necesarios para mostrar el reporte de tiempos
 * de viaje para conductores.
 * @author Alejandro Payares
 */
public class ReporteTiemposDeViajeConductoresDAO extends MainDAO{
    
    /**
     * Lista que contiene los datos del reporte.
     */
    private LinkedList datos;
    /**
     * Variable que contiene el numero de d�as m�ximo que mostrar� el reporte
     */
    private int numeroDeDiasMayor = 0;
    
    /**
     * Crea una nueva instancia de ReporteTiemposDeViajeConductoresDAO
     * @autor  Alejandro Payares
     */
    public ReporteTiemposDeViajeConductoresDAO() {
        super("ReporteTiemposDeViajeConductoresDAO.xml");
    }
    
    /**
     * Se encarga de realizar la consulta para obtener los datos del reporte.
     * @param fechaInicio La fecha de inicio de consulta del reporte
     * @param fechaFin La fecha de fin de consulta del reporte
     * @param cliente El cliente de consulta del reporte
     * @param placa La placa de consulta del reporte
     * @param origen El origen de consulta del reporte
     * @param destino El destino de consulta del reporte
     * @throws SQLException Si algun error ocurre al conectarse a la base de datos
     */
    public void buscarDatosReporte(String fechaInicio, String fechaFin, String cliente, String placa, String origen, String destino)throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            numeroDeDiasMayor = 0;
            String sql = this.obtenerSQL("SQL_PRINCIPAL");
            StringBuffer condiciones = new StringBuffer();
            condiciones.append(" fechasalidatraf between '"+fechaInicio+" 00:00' and '"+fechaFin+" 23:59'");
            if ( placa != null ) {
                placa = placa.replaceAll(",", "','");
            }
            if ( cliente != null ) {
                cliente = cliente.replaceAll(",", "','");
                condiciones.append(" AND r.cliente IN ('"+cliente+"')");
            }
            if ( origen != null ) {
                condiciones.append(" AND p.oripla = '"+origen+"'");
            }
            if ( destino != null ) {
                condiciones.append(" AND p.despla = '"+destino+"'");
            }
            if ( placa != null ) {
                condiciones.append(" AND p.plaveh IN ('"+placa+"')");
            }
            sql = sql.replaceFirst("condiciones", condiciones.toString());
            datos = new LinkedList();
            con = this.conectar("SQL_PRINCIPAL");
            ps = con.prepareStatement(sql);
            ////System.out.println("Query principal: "+ps);
            long comienzo = System.currentTimeMillis();
            rs = ps.executeQuery();
            //Con este while recorremos todas planillas encontradas para los parametros dados
            while( rs.next() ){
                Hashtable fila = new Hashtable();
                String planilla = rs.getString("numpla");
                fila.put("planilla", planilla);
                fila.put("origen", rs.getString("origen"));
                fila.put("destino", rs.getString("destino"));
                fila.put("cliente", rs.getString("cliente"));
                fila.put("nombrecliente", rs.getString("nombrecliente"));
                fila.put("conductor", rs.getString("conductor"));
                fila.put("placa", rs.getString("placa"));
                fila.put("fechaSalida", rs.getString("fechaSalida"));
                PreparedStatement psAux = null;
                try {
                    // buscamos los tiempos de la planilla
                    psAux = this.crearPreparedStatement("SQL_BUSCAR_TIEMPOS");
                    psAux.setString(1, planilla);
                    psAux.setString(2, planilla);
                    ResultSet rsAux = psAux.executeQuery();
                    String fechaAnterior = null;
                    int dias = 0;
                    while( rsAux.next() ){
                        String inicio = rsAux.getString("inicio");
                        String fin = rsAux.getString("fin");
                        String fecha = rsAux.getString("fecha");
                        String tiempo = rsAux.getString("tiempo");
                        String demora = rsAux.getString("demora");
                        String descripciones = rsAux.getString("descripciones");
                        String total = rsAux.getString("total");
                        Hashtable datosDia = new Hashtable();
                        int diasEnBlanco = 0;
                        // para llenar de vac�os los dias que no hubo reportes de trafico
                        if ( fechaAnterior != null && (diasEnBlanco = Util.obtenerDiferenciaEnDias(Util.obtenerCalendar(fecha),Util.obtenerCalendar(fechaAnterior))) > 1 ) {
                            for( int i=1; i< diasEnBlanco && diasEnBlanco < 8; i++ ){
                                Hashtable datosDiaAux = new Hashtable();
                                datosDiaAux.put("inicio", "-");
                                datosDiaAux.put("fin", "-");
                                datosDiaAux.put("tiempo", "-");
                                datosDiaAux.put("demora", "-");
                                datosDiaAux.put("descripciones", "-");
                                datosDiaAux.put("total", "-");
                                fila.put("dia"+(++dias), datosDiaAux);
                            }
                        }
                        if ( diasEnBlanco >= 8 ) {
                            break;
                        }
                        datosDia.put("inicio", inicio);
                        datosDia.put("fin", fin);
                        datosDia.put("tiempo", tiempo);
                        datosDia.put("demora", demora == null ? "-" : demora );
                        datosDia.put("descripciones", descripciones == null? "-" : descripciones);
                        datosDia.put("total", demora == null ? tiempo : total );
                        fila.put("dia"+(++dias), datosDia);
                        fechaAnterior = fecha;
                        
                    }
                    datos.add(fila);
                    // para saber en el jsp cual ser� el numero de columnas que tendr� la tabla
                    if ( dias > numeroDeDiasMayor ) {
                        numeroDeDiasMayor = dias;
                    }
                }
                catch( Exception ex ){
                    ex.printStackTrace();
                }
                finally {
                    if(psAux != null){psAux.close();}
                    this.desconectar("SQL_BUSCAR_TIEMPOS");
                }
            }
            ////System.out.println("Listo!!! tiempo empleado: "+(System.currentTimeMillis()-comienzo)+" milisegundos");
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
        finally {
            if ( ps != null ) {ps.close();}
            this.desconectar("SQL_PRINCIPAL");
        }
        
    }
    
    /**
     * Devuelve el numero m�ximo de d�as que mostrara el reporte, este valor es util para saber
     * cuantas columnas tendr� la tabla que muestra los datos ya sea en la p�gina Web o en el
     * archivo excel.
     * @return EL numero de dias m�ximo encontrado en alguna placa del reporte
     */
    public int obtenerNumeroMaximoDeDias(){
        return numeroDeDiasMayor;
    }
    
    /**
     * Permite obtener la lista que contiene todos los datos del reporte.
     * @return Una lista de objetos <CODE>Hashtable</CODE> conteniendo la informaci�n de los viajes
     * y sus tiempos.
     */
    public LinkedList obtenerDatosReporte(){
        return datos;
    }
    
    public void borrarDatos(){
        datos.clear();
        datos = null;
    }
}
