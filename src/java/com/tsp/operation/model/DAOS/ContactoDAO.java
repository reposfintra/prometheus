/*
 * Nombre        ContactoDAO.java
 * Autor         Ing. Rodrigo Salazar.
 * Fecha         24 de febrero de 2005, 10:42 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

/**
 *
 * @author  DIBASMO
 */
public class ContactoDAO extends MainDAO {
          
    public ContactoDAO() {
        super("ContactoDAO.xml");
    }
    
    private Contacto contacto;    
    private Vector VecContactos; 
    
    private static final  String Select_Consulta = "select * from Contacto where rec_status = '' order by cod_cia";
    
    //sescalante 01.02.06
    private static final String SQL_INSERT = "  INSERT INTO tablagen (  table_type, " +
    "                                                                   table_code, " +
    "                                                                   referencia, " +
    "                                                                   descripcion, " +
    "                                                                   user_update, " +
    "                                                                   creation_user ) " +
    "                                           VALUES ('CONTACTO', ?, ?, ? , ?, ?)";
    
    private static final String SQL_VERTODOS = "SELECT  t.table_code AS cod_contacto, " +
    "                                                   nitcont.nombre AS nomcont, " +
    "                                                   t.referencia AS cod_cia, " +
    "                                                   nitcia.nombre AS nomcia," +
    "                                                   coalesce ( tc.descripcion, 'NR') AS descripcion " +
    "                                           FROM    tablagen t 	" +
    "                                                               JOIN nit nitcia ON ( t.referencia = nitcia.cedula )" +
    "                                                               JOIN nit nitcont ON ( t.table_code = nitcont.cedula )" +
    "                                                               LEFT JOIN tablagen tc ON ( t.descripcion = tc.table_code AND tc.table_type = 'TCONTACTO')				" +
    "                                           WHERE   t.table_type = 'CONTACTO'                                                      " +
    "                                                   AND t.reg_status = '' " +
    "                                           ORDER BY t.referencia";
    
    private static final String SQL_BUSQUEDA = "    SELECT  t.table_code AS cod_contacto, " +
    "                                                       nitcont.nombre AS nomcont, " +
    "                                                       t.referencia AS codcia, " +
    "                                                       nitcia.nombre AS nomcia," +
    "						            COALESCE ( tc.table_code, '' ), 	 " +
    "                                                       COALESCE ( tc.descripcion, 'NR') AS descripcion " +
    "                                               FROM    tablagen t 	JOIN nit nitcia ON ( t.referencia = nitcia.cedula )" +
    "									JOIN nit nitcont ON ( t.table_code = nitcont.cedula )" +
    "									JOIN (" +
    "                                                                               SELECT  table_code, descripcion" +
    "                                                                               FROM    tablagen" +
    "                                                                               WHERE   descripcion LIKE ?" +
    "                                                                                       AND table_type = 'TCONTACTO'" +
    "                                                                   ) tc ON ( t.descripcion = tc.table_code)				" +
    "                                               WHERE   t.table_type = 'CONTACTO'                                                       " +
    "                                                       AND t.reg_status != 'A'" +
    "                                                       AND t.table_code like ? " +
    "                                                       AND t.referencia like ? "; 
    
    private static final String SQL_CONTACTO = "    SELECT  t.table_code AS cod_contacto, " +
    "                                                       nitcont.nombre AS nomcont, " +
    "                                                       t.referencia AS cod_cia, " +
    "                                                       nitcia.nombre AS nomcia," +
    "   						    coalesce ( tc.table_code, '' ) AS tipo, 	" +
    "                                                       coalesce ( tc.descripcion, 'NR') AS descripcion " +
    "                                               FROM    tablagen t 	" +
    "                                                                   JOIN nit nitcia ON ( t.referencia = nitcia.cedula )" +
    "									JOIN nit nitcont ON ( t.table_code = nitcont.cedula )" +
    "									LEFT JOIN tablagen tc ON ( t.descripcion = tc.table_code AND tc.table_type = 'TCONTACTO')				" +
    "                                               WHERE   t.table_code = ? " +
    "                                                       AND t.referencia = ?" +
    "                                                       AND t.table_type = 'CONTACTO'";
    
    private static final String SQL_UPDATE= " UPDATE    tablagen " +
    "                                         SET       descripcion = ?, " +
    "                                                   last_update='now()', " +
    "                                                   user_update=? " +
    "                                         WHERE     table_code = ? " +
    "                                                   AND referencia =?" +
    "                                                   AND table_type = 'CONTACTO'";
    
    private static final String SQL_DELETE = "  DELETE FROM tablagen " +
    "                                           WHERE  table_code = ? " +
    "                                                  AND referencia = ?" +
    "                                                  AND table_type = 'CONTACTO'";
    
    private static final String SQL_CXCIA = "    SELECT  t.table_code AS cod_contacto, " +
    "                                                       nitcont.nombre AS nomcont, " +
    "                                                       t.referencia AS cod_cia, " +
    "                                                       nitcia.nombre AS nomcia," +
    "   						    coalesce ( tc.table_code, '' ) AS tipo, 	" +
    "                                                       coalesce ( tc.descripcion, 'NR') AS descripcion " +
    "                                               FROM    tablagen t 	" +
    "                                                                   JOIN nit nitcia ON ( t.referencia = nitcia.cedula )" +
    "									JOIN nit nitcont ON ( t.table_code = nitcont.cedula )" +
    "									LEFT JOIN tablagen tc ON ( t.descripcion = tc.table_code AND tc.table_type = 'TCONTACTO')				" +
    "                                               WHERE   t.referencia = ? " +
    "                                                       AND  t.reg_status != 'A'" +
    "                                                       AND t.table_type = 'CONTACTO'";
    
    /**
     * Metodo <tt>setContacto()</tt>, instacia el objeto contacto 
     * @autor : Ing. Rodrigo Salazar
     * @param : Contacto     
     * @version : 1.0
     */
    public void setContacto(Contacto contacto){    
        this.contacto = contacto;
    }
    
    /**
     * Metodo <tt>insertarContacto()</tt>, registra un contacto
     * @autor : Ing. Rodrigo Salazar     
     * @version : 1.0
     */   
    public void insertarContacto() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_INSERT";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, contacto.getCod_contacto());
                st.setString(2, contacto.getCod_cia());
                st.setString(3, contacto.getTipo());
                st.setString(4, contacto.getUser_update());
                st.setString(5, contacto.getCreation_user());                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR EL CONTACTO" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 
    
    /**
     * Metodo <tt>obtContactos()</tt>, obtiene el vector de contactos
     * @autor : Ing. Rodrigo Salazar     
     * @version : 1.0
     */
    public Vector obtContactos (){
        return VecContactos;
    }
    
    
    /**
     * Metodo <tt>contactos()</tt>, busca los contactos del sistema
     * @autor : Ing. Rodrigo Salazar     
     * @version : 1.0
     */
    public void contactos ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        VecContactos = null;
        String descripcion = "";
        String query = "SQL_VERTODOS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                
                VecContactos =  new Vector();
                
                while (rs.next()){
                    contacto = new Contacto();
                    contacto.setCod_contacto(rs.getString("cod_contacto"));
                    contacto.setNomcontacto(rs.getString("nomcont"));
                    contacto.setCod_cia(rs.getString("cod_cia"));
                    contacto.setNomcopania(rs.getString("nomcia"));
                    descripcion = ( rs.getString("descripcion").equalsIgnoreCase("NR") )?"NO REGISTRA":rs.getString("descripcion");
                    contacto.setNomtipo(descripcion);
                    VecContactos.add(contacto);
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CONTACTOS " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    
    /**
     * Metodo <tt>buscarContactoNombre</tt>, obtiene los contactos dado unos parametros
     * @autor : Ing. Rodrigo Salazar     
     * @param codigo del contacto, codigo de la compania, tipo de contacto
     * @version : 1.0
     */
    public void buscarContactoNombre (String cto, String ccia, String tipo) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        VecContactos = null;
        String query = "SQL_BUSQUEDA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tipo);
                st.setString(2, cto);
                st.setString(3, ccia);
                
                rs = st.executeQuery();
                
                VecContactos =  new Vector();
                
                while (rs.next()){
                    contacto = new Contacto();
                    contacto.setCod_contacto(rs.getString("cod_contacto"));
                    contacto.setNomcontacto(rs.getString("nomcont"));
                    contacto.setCod_cia(rs.getString("codcia"));
                    contacto.setNomcopania(rs.getString("nomcia"));
                    contacto.setNomtipo(rs.getString("descripcion"));
                    VecContactos.add(contacto);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL CONTACTO " + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    
    /**
     * Metodo <tt>obtenerContacto</tt>, obtiene el objeto Contacto, ya instanciado
     * @autor : Ing. Rodrigo Salazar     
     * @return Contacto
     * @version : 1.0
     */
    public Contacto obtenerContacto()throws SQLException{
        return contacto;
    }          
     
    /**
     * Metodo <tt>bucarContactos()</tt>, busca los contacto segun los datos ingresados
     * @autor : Ing. Rodrigo Salazar 
     * @param codigo del contacto y codigo de la compania    
     * @version : 1.0
     */
    public void buscarContacto (String cod, String codcia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        String query = "SQL_CONTACTO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod);
                st.setString(2, codcia);
                rs = st.executeQuery();
                
                if (rs.next()){
                    contacto = new Contacto();
                    contacto.setCod_contacto(rs.getString("cod_contacto"));
                    contacto.setNomcontacto(rs.getString("nomcont"));
                    contacto.setCod_cia(rs.getString("cod_cia"));
                    contacto.setNomcopania(rs.getString("nomcia"));
                    contacto.setTipo(rs.getString("tipo"));
                    contacto.setNomtipo(rs.getString("descripcion"));                                    
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL CONTACTO " + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * Metodo <tt>modificarContactos</tt>, actualiza contactos
     * @autor : Ing. Rodrigo Salazar     
     * @version : 1.0
     */
    public void modificarContacto () throws SQLException{        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_UPDATE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, contacto.getTipo());
                st.setString(2, contacto.getUser_update());                                
                st.setString(3, contacto.getCod_contacto());
                st.setString(4, contacto.getCod_cia());
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR EL CONTACTO " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    
    /**
     * Metodo <tt>anularContacto</tt>, .elimina unc contacto del sistema
     * @autor : Ing. Rodrigo Salazar     
     * @version : 1.0
     */
    public void anularContacto () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_DELETE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, contacto.getCod_contacto());                
                st.setString(2, contacto.getCod_cia());
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR ELIMINAR UN CONTACTO " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
   /**
     * Metodo <tt>buscarContactoCia</tt>, busca los contactos dado una compania
     * @autor : Ing. Rodrigo Salazar     
     * @param codigo de la compania
     * @version : 1.0
     */
    public void buscarContactoCia(String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        VecContactos = null;
        String query = "SQL_CXCIA";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cia);                
                rs = st.executeQuery();
                
                VecContactos =  new Vector();
                
                while (rs.next()){   
                    contacto = new Contacto();
                    contacto.setCod_contacto(rs.getString("cod_contacto"));
                    contacto.setNomcontacto(rs.getString("nomcont"));
                    contacto.setCod_cia(rs.getString("cod_cia"));
                    contacto.setNomcopania(rs.getString("nomcia"));
                    contacto.setTipo(rs.getString("tipo"));
                    contacto.setNomtipo(rs.getString("descripcion"));  
                    VecContactos.add(contacto);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR CONTACTO POR COMPA�IA " + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /**
     * Getter for property VecContactos.
     * @return Value of property VecContactos.
     */
    public java.util.Vector getVecContactos() {
        return VecContactos;
    }
    
    /**
     * Setter for property VecContactos.
     * @param VecContactos New value of property VecContactos.
     */
    public void setVecContactos(java.util.Vector VecContactos) {
        this.VecContactos = VecContactos;
    }
}
