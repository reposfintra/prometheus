/*
 * InteresesFenalcoDAO.java
 *
 * Created on 6 de julio de 2008, 11:08
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.lang.*;
import com.tsp.operation.model.TransaccionService;

/**
 *
 * @author  navi
 */
public class InteresesFenalcoDAO  extends MainDAO  {

    private static InteresesFenalcoDAO interesesFenalcoDAO;

    /** Creates a new instance of InteresesFenalcoDAO */
    public InteresesFenalcoDAO() {
        super("InteresesFenalcoDAO.xml");
    }
    public InteresesFenalcoDAO(String dataBaseName) {
        super("InteresesFenalcoDAO.xml",dataBaseName);
    }


          /**
     *
     * @return
     * @throws Exception
     */


    //Belleza 4

    public String InsertIngFenalco() throws Exception{
        Connection con = null;
        String sql="";
        PreparedStatement  ps = null;
        ResultSet rs = null;

        String  Query = "SQL_CONSULTA_NEGOCIOS_PENDIENTES";

        PreparedStatement ingfen = null;
        PreparedStatement up = null;

        try{
            ps = null;//this.crearPreparedStatement(Query);
            up=null;//crearPreparedStatement("SQL_UP");
            rs = ps.executeQuery();
            String meses="";
            double num_meses=0;
            String neg="";
            String valmes="";
            double num_valmes=0;
            String nit="";
            String fecha_neg="";
            //String fecha_insertable="";
            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
            int contador_ing_fenalco=0;
            while (rs.next())
            {
                meses=rs.getString("meses");
                neg=rs.getString("cod_neg");
                num_meses=Double.parseDouble(meses);
                valmes=rs.getString("valmes");
                num_valmes=Double.parseDouble(valmes);
                nit=rs.getString("cod_cli");
                fecha_neg=rs.getString("fecha_negocio");
                sql="";
                for (int j=0;j<num_meses;j++){
                    //System.out.println("neg"+neg);
                    ingfen = null;//crearPreparedStatement("SQL_INSERT_ING");
                    ingfen.setString(1,"FINV");
                    ingfen.setInt(2,contador_ing_fenalco);/////
                    contador_ing_fenalco=contador_ing_fenalco+1;
                    ingfen.setString(3,neg);
                    ingfen.setDouble(4,num_valmes);//valor
                    ingfen.setString(5,nit);//nit cliente
                    ingfen.setString(6,"NAVI");
                    ingfen.setString(7,"COL");
                    //fecha_insertable="(CAST('"+fecha_neg+"' AS DATE) + CAST('"+j+" month' AS INTERVAL))";
                    ingfen.setString(8,fecha_neg);
                    ingfen.setString(9,""+j+" month");
                    //{ingfen.setString(8,(bg.getV2())[1]);}else{ingfen.setString(8,(bg.getV1())[j]);}//fechas

                    sql+=ingfen.toString();

                }
                try{
                    up.setString(1, "IF");
                    sql+=up.toString();

                    scv.crearStatement();
                    scv.getSt().addBatch(sql);
                    //scv.execute();
                }catch(SQLException e){
                    System.out.println("InsertIngFenalco"+e.toString()+"__"+e.getMessage());

                    throw new SQLException("ERROR DURANTE INSERCION DE INTERES"+e.getMessage());
                }

            }

        }
        catch (Exception ex)
        {
            System.out.println("error en InsertIngFenalco"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            if (ingfen!=null) ingfen.close();
            this.desconectar(con);
            //System.out.println("finally");
        }
        return "ok";
    }







    public static void main(String[] args){
        if( interesesFenalcoDAO == null ){
            interesesFenalcoDAO = new InteresesFenalcoDAO();
        }
        try{
            //interesesFenalcoDAO.InsertIngFenalco();
            interesesFenalcoDAO.InsertIngFenalcoRemix();
            //InsertIngFenalcoRemix
        }catch(Exception e){
            System.out.println("oops en InsertIngFenalco"+e.toString()+"__"+e.getMessage());
        }

    }

   /**
    * Modificad: 30/03/2010
    * Ing. Jose Castro
    * @return
    * @throws Exception
    */

    public String InsertIngFenalcoRemix() throws Exception{
        Connection con = null;
        String sql="";
        PreparedStatement  ps = null;
        ResultSet rs = null;
        String  Query = "SQL_CONSULTA_NEGS_PENDIENTES_REMIX";
        StringStatement ingfen = null;
        StringStatement up = null;
        ResultSet rssci = null;
        try{

            con = this.conectarJNDI(Query);//JJCastro fase2
                            if(con!=null){
                            ps = con.prepareStatement(this.obtenerSQL(Query));//JJCastro fase2
                            up = new StringStatement (this.obtenerSQL("SQL_UP"), true);//JJCastro fase2
                            up.setString(1, "IF");
                            rs = ps.executeQuery();
                            String meses="";
                            double num_meses=0;
                            String neg="";
                            String valmes="";
                            double num_valmes=0;
                            String nit="";
                            String fecha_neg="";
                            //String fecha_insertable="";
                            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
                            PreparedStatement sci=null;
                            String sql_consulta_intereses="SQL_CONSULTA_INTERESES_NEGS";
                            int contador_ing_fenalco=0;



                    while (rs.next()) {

                                            neg = rs.getString("cod_neg");
                                            nit = rs.getString("cod_cli");

                                            sci = con.prepareStatement(this.obtenerSQL("SQL_CONSULTA_INTERESES_NEGS"));//JJCastro fase2
                                            sci.setString(1, neg);
                                            rssci = sci.executeQuery();
                                            String interesi = "", fechai = "";

                                                        while (rssci.next()) {
                                                            interesi = rssci.getString("interes");
                                                            fechai = rssci.getString("fecha");

                                                            ingfen = new StringStatement (this.obtenerSQL("SQL_INSERT_ING_REMIX"), true);//JJCastro fase2
                                                            ingfen.setString(1, "FINV");
                                                            ingfen.setInt(2, contador_ing_fenalco);/////
                                                            contador_ing_fenalco = contador_ing_fenalco + 1;
                                                            ingfen.setString(3, neg);
                                                            ingfen.setDouble(4, Double.parseDouble(interesi));//valor
                                                            ingfen.setString(5, nit);//nit cliente
                                                            ingfen.setString(6, "NAVI");
                                                            ingfen.setString(7, "COL");
                                                            ingfen.setString(8, fechai);

                                                            sql += ingfen.getSql();
                                                        }

                    }
                            try{
                                sql+=up.getSql();
                                //System.out.println("sql final"+sql);
                                scv.crearStatement();
                                scv.getSt().addBatch(sql);
                                scv.execute();
                                //System.out.println("ya");
                            }catch(SQLException e){
                                System.out.println("InsertIngFenalcoRemix"+e.toString()+"__"+e.getMessage());
                                throw new SQLException("ERROR DURANTE INSERCION DE INTERESESX"+e.getMessage());
                            }
                        }

        }
        catch (Exception ex)
        {
            System.out.println("error en InsertIngFenalco"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (rssci  != null){ try{ rssci.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET rssci" + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO ps" + e.getMessage()); }}
            if (up  != null){ try{ up = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (ingfen  != null){ try{ingfen = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return "ok";
    }




/**
 *
 * @param cod_neg
 * @param cod_cli
 * @param usuario
 * @return
 * @throws SQLException
 */
    public String InsertIngFenalcoNegRemix(String cod_neg,String cod_cli,String usuario) throws SQLException{
        Connection con = null;
        String sql="";
        StringStatement ingfen = null;
        StringStatement up = null;
        ResultSet rssci = null;
        String query = "SQL_UP";

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           up  = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
           up.setString(1, (cod_neg.substring(0,2).equals("NF"))?"IM":"IF");
            String meses="";
            double num_meses=0;
            String valmes="";
            double num_valmes=0;
            String fecha_neg="";
            PreparedStatement sci=null;
            String sql_consulta_intereses="SQL_CONSULTA_INTERESES_NEGS";
            int contador_ing_fenalco=0;


                sci = con.prepareStatement(this.obtenerSQL("SQL_CONSULTA_INTERESES_NEGS"));//JJCastro fase2
                sci.setString(1, cod_neg);
                rssci= sci.executeQuery();
                String interesi="",fechai="";
                                while (rssci.next()){
                                    interesi=rssci.getString("interes");
                                    fechai=rssci.getString("fecha");

                                    ingfen = new StringStatement (this.obtenerSQL("SQL_INSERT_ING_REMIX"), true);//JJCastro fase2
                                    ingfen.setString(1,"FINV");
                                    ingfen.setString(2,(cod_neg.substring(0,2).equals("NF"))?"IM":"IF");/////
                                    ingfen.setInt(3,contador_ing_fenalco);/////
                                    contador_ing_fenalco=contador_ing_fenalco+1;
                                    ingfen.setString(4,cod_neg);
                                    ingfen.setDouble(5,Double.parseDouble(interesi));//valor
                                    ingfen.setString(6,cod_cli);//nit cliente
                                    ingfen.setString(7,usuario);
                                    ingfen.setString(8,"COL");
                                    ingfen.setString(9,fechai);
                                    ingfen.setString(10,(!cod_neg.substring(0,2).equals("NF"))?"01":"04");

                                    sql+=ingfen.getSql();
                                }
                                sql+=up.getSql();
                        }

        }
        catch (Exception ex)
        {
            System.out.println("error en InsertIngFenalco"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
            throw new SQLException(ex.getMessage());
        }finally{
            if (rssci  != null){ try{ rssci.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET rssci" + e.getMessage()); }}
            if (up  != null){ try{ up = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (ingfen  != null){ try{ingfen = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }

     public ArrayList<String> insertarIngFenalco(String cod_neg, String cod_cli, String usuario, String documentType, String cmc) throws Exception {

        Connection con = null;
        PreparedStatement ps = null;
        StringStatement ingfen = null;
        StringStatement up = null;
        ResultSet rs = null;
        int contador_ing_fenalco = 0;
        ArrayList<String> listSql = new ArrayList<String>();
        String queryConsulta = "SQL_CONSULTA_INTERESES_NEGS";
        String queryInsert = "SQL_INSERT_ING_REMIX";
        String querySerie = "SQL_UP";
        try {
            con = conectarJNDI(queryConsulta);
            ps = con.prepareStatement(this.obtenerSQL(queryConsulta));
            ps.setString(1, cod_neg);
            rs = ps.executeQuery();
            while (rs.next()) {
                ingfen = new StringStatement(this.obtenerSQL(queryInsert), true);
                ingfen.setString(1, "FINV");
                ingfen.setString(2, documentType);// document_type para buscar el consecutivo
                ingfen.setInt(3, contador_ing_fenalco);//
                contador_ing_fenalco++;
                ingfen.setString(4, cod_neg); //codigo del negocio
                ingfen.setDouble(5, Double.parseDouble(rs.getString("interes")));//valor del interes
                ingfen.setString(6, cod_cli);//nit cliente
                ingfen.setString(7, usuario);//usuario en sesion
                ingfen.setString(8, "COL"); //base
                ingfen.setString(9, rs.getString("fecha")); //fecha
                ingfen.setString(10, cmc); //cmc
                ingfen.setInt(11, contador_ing_fenalco); //cmc

                listSql.add(ingfen.getSql());
            }
            //Actualiza la serie
            up = new StringStatement(this.obtenerSQL(querySerie), true);
            up.setString(1, documentType);
            listSql.add(up.getSql());

        } catch (Exception e) {
            throw new Exception("Error en insertarIngFenalco[InteresesFenalcoDAO] "+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (ingfen  != null){
                ingfen = null;
            }
            if (up  != null){
                up = null;
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return listSql;
    }
    
    /**
     *
     * @param queryConsulta
     * @param queryInsert
     * @param cod_neg
     * @param cod_cli
     * @param usuario
     * @param documentType
     * @param tipo_liquidacion
     * @param cuota
     * @param cmc
     * @return
     * @throws Exception
     */
    public ArrayList<String> insertarIngFenalcoCargosFijos(String queryConsulta,String queryInsert,String cod_neg, String cod_cli, String usuario, String documentType, String cmc, String tipo_liquidacion, String cuota) throws Exception {

        Connection con = null;
        PreparedStatement ps = null;
        StringStatement ingfen = null;
        StringStatement up = null;
        ResultSet rs = null;
        int contador_ing_fenalco = 0;
        ArrayList<String> listSql = new ArrayList<>();
        String querySerie = "SQL_UP";
        try {
            con = conectarJNDI(queryConsulta);
            String filtro=" AND tipo_liquidacion='NORMAL'";
            if (tipo_liquidacion.equals("REFINANCIACION")){
                filtro=" AND tipo_liquidacion='"+tipo_liquidacion+"'";
            }
            ps = con.prepareStatement(this.obtenerSQL(queryConsulta).replace("#filtro", filtro));
            ps.setString(1, cod_neg);
            rs = ps.executeQuery();
            while (rs.next()) {
                ingfen = new StringStatement(this.obtenerSQL(queryInsert), true);
                ingfen.setString(1, "FINV");
                ingfen.setString(2, documentType);// document_type para buscar el consecutivo
                ingfen.setInt(3, contador_ing_fenalco);//
                contador_ing_fenalco++;
                ingfen.setString(4, cod_neg); //codigo del negocio
                ingfen.setDouble(5, Double.parseDouble(rs.getString("valor_diferido")));//valor del interes
                ingfen.setString(6, cod_cli);//nit cliente
                ingfen.setString(7, usuario);//usuario en sesion
                ingfen.setString(8, "COL"); //base
                ingfen.setString(9, rs.getString("fecha")); //fecha
                ingfen.setString(10, cmc); //cmc
                ingfen.setString(11, documentType); //tipodoc
                ingfen.setInt(12, Integer.parseInt(rs.getString("item"))); //cuota

                listSql.add(ingfen.getSql());
            }
            //Actualiza la serie si la lista tiene datos
            if(!listSql.isEmpty()){
                up = new StringStatement(this.obtenerSQL(querySerie), true);
                up.setString(1, documentType);
                listSql.add(up.getSql());
            }

        } catch (Exception e) {
            throw new Exception("Error en insertarIngFenalco[InteresesFenalcoDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (ingfen != null) {
                ingfen = null;
            }
            if (up != null) {
                up = null;
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return listSql;
    }
    
    public ArrayList<String> insertarInteresIFApi(String cod_neg, String cod_cli, String usuario, String documentType, String cmc,String tipo_liq) throws Exception {

        Connection con = null;
        PreparedStatement ps = null;
        StringStatement ingfen = null;
        StringStatement up = null;
        ResultSet rs = null;
        int contador_ing_fenalco = 0;
        ArrayList<String> listSql = new ArrayList<String>();
        String queryConsulta = "SQL_CONSULTA_INTERESES_NEGS_API";
        String queryInsert = "SQL_INSERT_ING_REMIX";
        String querySerie = "SQL_UP";
        try {
            con = conectarJNDI(queryConsulta);
            ps = con.prepareStatement(this.obtenerSQL(queryConsulta));
            ps.setString(1, cod_neg);
            ps.setString(2, tipo_liq);
            rs = ps.executeQuery();
            while (rs.next()) {
                ingfen = new StringStatement(this.obtenerSQL(queryInsert), true);
                ingfen.setString(1, "FINV");
                ingfen.setString(2, documentType);// document_type para buscar el consecutivo
                ingfen.setInt(3, contador_ing_fenalco);//
                contador_ing_fenalco++;
                ingfen.setString(4, cod_neg); //codigo del negocio
                ingfen.setDouble(5, Double.parseDouble(rs.getString("interes")));//valor del interes
                ingfen.setString(6, cod_cli);//nit cliente
                ingfen.setString(7, usuario);//usuario en sesion
                ingfen.setString(8, "COL"); //base
                ingfen.setString(9, rs.getString("fecha")); //fecha
                ingfen.setString(10, cmc); //cmc
                ingfen.setInt(11, Integer.parseInt(rs.getString("item"))); //cuota

                listSql.add(ingfen.getSql());
            }
            //Actualiza la serie
            up = new StringStatement(this.obtenerSQL(querySerie), true);
            up.setString(1, documentType);
            listSql.add(up.getSql());

        } catch (Exception e) {
            throw new Exception("Error en insertarIngFenalco[InteresesFenalcoDAO] "+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (ingfen  != null){
                ingfen = null;
            }
            if (up  != null){
                up = null;
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return listSql;
    }


}


