/*
 * ProcesoCorficolombianaDAO.java
 *
 * Created on 28 de julio de 2008, 11:05 AM
 */
package com.tsp.operation.model.DAOS;

import com.tsp.finanzas.contab.model.DAO.CmcDAO;
import com.tsp.finanzas.contab.model.DAO.ComprobantesDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionDiferidosDAO;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.SeriesService;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author  Administrador
 */
public class ProcesoCorficolombianaDAO extends MainDAO {

    /** Creates a new instance of ProcesoCorficolombianaDAO */
    public ProcesoCorficolombianaDAO() {
        super("ProcesoCorficolombianaDAO.xml");
    }
    public ProcesoCorficolombianaDAO(String dataBaseName) {
        super("ProcesoCorficolombianaDAO.xml", dataBaseName);
    }

    public String endozarFacturas(String factura) throws java.lang.Exception {
        StringStatement st = null;
        String query = "MARCAR_FACTURA";
        String sql = "";
        try {            
                st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                st.setString(1, factura);
                sql = st.getSql();

            
        } catch (java.lang.Exception e) {
            throw new java.lang.Exception("[ProcesoCorficolombianaDAO.endozarFacturas]: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
           
        }
        return sql;
    }

     public boolean existeComprobante(String negocio) throws java.lang.Exception {
        Connection con = null;
        boolean sw=false;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE_COMPROBANTE";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, negocio);
            rs = st.executeQuery();
            if (rs.next()) {
                sw=true;
            }

        } catch (java.lang.Exception e) {
            throw new java.lang.Exception("[ProcesoCorficolombianaDAO.existeComprobante]: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sw;
    }

    public Comprobantes buscarNegocio(String negocio, String dstrct) throws java.lang.Exception {
        Connection con = null;
        Comprobantes comprobante = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_COMPROBANTE_INTERESES";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, negocio);
            st.setString(2, dstrct);
            rs = st.executeQuery();
            if (rs.next()) {
                comprobante = loadComprobantes2(rs);
            }

        } catch (java.lang.Exception e) {
            throw new java.lang.Exception("[ProcesoCorficolombianaDAO.buscarnegocio]: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return comprobante;
    }

    /**
     * M�todo que carga los datos de los negocios
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public Comprobantes loadComprobantes2(ResultSet rs) throws Exception {
        Comprobantes comp = new Comprobantes();
        try {
            comp.setDstrct(rs.getString("dist"));
            comp.setNumdoc(rs.getString("cod_neg"));
            comp.setTercero(rs.getString("cod_cli"));
            comp.setRef_1(reset(rs.getString("nit_tercero")));//nit del proveedor
            comp.setAuxiliar("RD-" + rs.getString("cod_cli"));
            comp.setFecha_creacion(reset(rs.getString("fecha_negocio")));
            comp.setDetalle("Traslado negocio No " + rs.getString("cod_neg"));
            comp.setRef_2(rs.getString("NOMB"));
            comp.setRef_5(rs.getInt("nro_docs"));
            comp.setCustodia(rs.getDouble("vr_custodia"));
            comp.setRem(rs.getDouble("valor_remesa"));
            comp.setModrem(rs.getString("mod_remesa"));
            comp.setCmc(rs.getString("cmc"));
            comp.setTpr(rs.getDouble("porterem"));
            comp.setTdesc(rs.getDouble("tdescuento"));
            comp.setRef_3(reset(rs.getString("fecha_ap")));//Mod TMOLINA 03-Enero-2008
            comp.setVlr_aval(rs.getDouble("valor_aval"));
            comp.setId_convenio(rs.getInt("id_convenio"));
            comp.setId_remesa(rs.getInt("id_remesa"));
            comp.setTipoNegocio(rs.getString("tneg"));
        } catch (Exception e) {
            throw new Exception("loadComprobantes2" + e.getMessage());
        }
        return comp;
    }

    /**
     * M�todos que setea valores nulos
     * @autor.......ivargas
     * @version.....1.0.
     **/
    private String reset(String val) {
        if (val == null) {
            val = "";
        }
        return val;
    }

    public String activarProcesoIntereses(String prefix_dif_fid, String dif_fid, String dif, String negocio, String usuario) throws java.lang.Exception {
        Connection con = null;
        StringStatement st = null;
        String query = "SQL_ACTIVAR_PROCESO_INTERESES_FID";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL("SQL_ACTIVAR_PROCESO_INTERESES_FID"), true);//JJCastro fase2
                st.setString(1, prefix_dif_fid);
                st.setString(2, dif_fid);
                st.setString(3, usuario);
                st.setString(4, negocio);
                st.setString(5, dif);
                sql = st.getSql();

            }
        } catch (java.lang.Exception e) {
            throw new java.lang.Exception("[ProcesoCorficolombianaDAO.activarProcesoIntereses]: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

    public String anularProcesoIntereses(String dif, String negocio, String usuario) throws java.lang.Exception {
        Connection con = null;
        StringStatement st = null;
        String query = "SQL_ANULAR_PROCESO_INTERESES";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL("SQL_ANULAR_PROCESO_INTERESES"), true);//JJCastro fase2
                st.setString(1, usuario);
                st.setString(2, negocio);
                st.setString(3, dif);
                sql = st.getSql();

            }
        } catch (java.lang.Exception e) {
            throw new java.lang.Exception("[ProcesoCorficolombianaDAO.anularProcesoIntereses]: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

   public ArrayList<String> comprobantesReversion(String negocio, Convenio conv, Usuario usuario, String nit_fiducia) throws java.lang.Exception {
        String prefijo_fiducia="",hc_fiducia="", cuenta_fiducia="";
         for (int k = 0; k < conv.getConvenioFiducias().size(); k++) {
                         if (conv.getConvenioFiducias().get(k).getNit_fiducia().equals(nit_fiducia)) {
                             prefijo_fiducia=conv.getConvenioFiducias().get(k).getPrefijo_dif_fiducia();
                             hc_fiducia=conv.getConvenioFiducias().get(k).getHc_dif_fiducia();
                             cuenta_fiducia=conv.getConvenioFiducias().get(k).getCuenta_dif_fiducia();
                         }
                    }
        SeriesService serie = new SeriesService();
        ContabilizacionDiferidosDAO dao = new ContabilizacionDiferidosDAO();
        Connection con = null;
        Comprobantes comprobante = null;
        List<Comprobantes> comproList = new LinkedList();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_COMPROBANTE";
        String query2 = "SQL_BUSCAR_COMPRODET";
        ArrayList<String> sql = new ArrayList<>();


        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, negocio);
            st.setString(2, conv.getPrefijo_diferidos());
            rs = st.executeQuery();
            while (rs.next()) {
                comprobante = loadComprobantes(rs);
                comproList.add(comprobante);
            }
            if (comproList.size() > 0) {
                com.tsp.finanzas.contab.model.beans.Cmc cmcob = new com.tsp.finanzas.contab.model.beans.Cmc();
                cmcob.setDstrct(usuario.getDstrct());
               
                cmcob.setTipodoc(prefijo_fiducia);
                cmcob.setCmc(hc_fiducia);
                CmcDAO cmcdao = new CmcDAO();
                cmcdao.setCmc(cmcob);
                cmcdao.obtenerCmcDoc();
                String cuentacmc = cmcdao.getCmc().getCuenta();

                // documentos de reverision
                for (int i = 0; i < comproList.size(); i++) {
                    double valor = comproList.get(i).getTotal_debito();
                    sql.add(this.InsertComprobante(comproList.get(i), usuario.getLogin()));
                    st = con.prepareStatement(this.obtenerSQL(query2));
                    st.setString(1, comproList.get(i).getNumdoc());
                    rs = st.executeQuery();
                    while (rs.next()) {
                        comproList.get(i).setCuenta(rs.getString("cuenta"));
                        comproList.get(i).setDetalle(rs.getString("detalle"));
                        comproList.get(i).setAuxiliar(rs.getString("auxiliar"));
                        comproList.get(i).setAbc(rs.getString("abc"));
                        comproList.get(i).setDocumento_interno(rs.getString("documento_interno"));
                        comproList.get(i).setTdoc_rel(rs.getString("tipodoc_rel"));
                        comproList.get(i).setNumdoc_rel(rs.getString("documento_rel"));
                        if (rs.getDouble("valor_debito") == 0) {
                            comproList.get(i).setTotal_debito(valor);
                            comproList.get(i).setTotal_credito(0.0);
                        } else {
                            comproList.get(i).setTotal_debito(0.0);
                            comproList.get(i).setTotal_credito(valor);
                        }
                        sql.add(this.InsertComprobanteDetalle(comproList.get(i), usuario.getLogin()));
                    }
                    sql.add(this.updateIngRev(comproList.get(i).getGrupo_transaccion(),  comproList.get(i).getNumdoc()));
                    comproList.get(i).setTotal_debito(valor);
                    comproList.get(i).setTotal_credito(valor);
                }
                // nuevo documento con el prefijo de diferidos fiducia
                for (int i = 0; i < comproList.size(); i++) {
                    String numdoc = comproList.get(i).getNumdoc();
                   comproList.get(i).setTipodoc(prefijo_fiducia);
                    comproList.get(i).setNumdoc(serie.obtenerPrefix(prefijo_fiducia) + numdoc.substring(2));
                    comproList.get(i).setDetalle("intereses de " + comproList.get(i).getPeriodo());
                    sql.add(this.InsertComprobante(comproList.get(i), usuario.getLogin()));
                    st = con.prepareStatement(this.obtenerSQL(query2));
                    st.setString(1, numdoc);
                    rs = st.executeQuery();
                    while (rs.next()) {

                        comproList.get(i).setAuxiliar(rs.getString("auxiliar"));
                        comproList.get(i).setAbc(rs.getString("abc"));
                        comproList.get(i).setDocumento_interno(rs.getString("documento_interno"));
                        comproList.get(i).setTdoc_rel(rs.getString("tipodoc_rel"));
                        comproList.get(i).setNumdoc_rel(rs.getString("documento_rel"));
                        comproList.get(i).setTotal_debito(rs.getDouble("valor_debito"));
                        comproList.get(i).setTotal_credito(rs.getDouble("valor_credito"));
                        if (comproList.get(i).getTotal_debito() != 0) {
                            comproList.get(i).setCuenta(cuentacmc);
                            comproList.get(i).setDetalle(dao.getAccountDES(cuentacmc));
                        } else {
                            comproList.get(i).setCuenta(cuenta_fiducia);
                            comproList.get(i).setDetalle(dao.getAccountDES(cuenta_fiducia));
                        }

                        sql.add(this.InsertComprobanteDetalle(comproList.get(i), usuario.getLogin()));
                    }
                    sql.add(this.updatecont(usuario.getLogin(), comproList.get(i).getGrupo_transaccion(), comproList.get(i).getPeriodo(), comproList.get(i).getNumdoc()));
                }
            }


        } catch (java.lang.Exception e) {
            throw new java.lang.Exception("[ProcesoCorficolombianaDAO.buscarnegocio]: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }
  

    /**
     * M�todo que carga los datos de los negocios
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public Comprobantes loadComprobantes(ResultSet rs) throws Exception {
        ComprobantesDAO comproDAO;
        comproDAO = new ComprobantesDAO();
        Comprobantes comp = new Comprobantes();
        try {
            int sec = comproDAO.getGrupoTransaccion();
            comp.setGrupo_transaccion(sec);
            String fecha_actual = Util.getFechaActual_String(4);
            String periodo = fecha_actual.substring(0, 7).replaceAll("-", "");
            comp.setPeriodo(periodo);
            comp.setFechadoc(fecha_actual);
            comp.setDstrct(rs.getString("dstrct"));
            comp.setTipodoc(rs.getString("tipodoc"));
            comp.setNumdoc(rs.getString("numdoc"));
            comp.setSucursal(rs.getString("sucursal"));
            comp.setDetalle("reversion de " + rs.getString("detalle"));
            comp.setTercero(rs.getString("tercero"));
            comp.setTotal_debito(rs.getDouble("total_debito"));
            comp.setTotal_credito(rs.getDouble("total_credito"));
            comp.setTotal_items(rs.getInt("total_items"));
            comp.setMoneda(rs.getString("moneda"));
            comp.setAprobador(rs.getString("aprobador"));
            comp.setBase(rs.getString("base"));
            comp.setTipo_operacion(rs.getString("tipo_operacion"));

        } catch (Exception e) {
            throw new Exception("loadComprobantes " + e.getMessage());
        }
        return comp;
    }

    /**
     * Sql de insercion del comprobante
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public String InsertComprobante(Comprobantes comprobante, String user) throws Exception {
        StringStatement st = null;
        String query = "SQL_INSERT_COMPROBANTE";
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, comprobante.getDstrct());
            st.setString(2, comprobante.getTipodoc());
            st.setString(3, comprobante.getNumdoc());
            st.setString(4, comprobante.getSucursal());  //  comprobante.getSucursal()
            st.setString(5, comprobante.getPeriodo());
            st.setString(6, comprobante.getFechadoc());
            st.setString(7, comprobante.getDetalle());
            st.setString(8, comprobante.getTercero());
            st.setDouble(9, comprobante.getTotal_debito());
            st.setDouble(10, comprobante.getTotal_credito());
            st.setInt(11, comprobante.getTotal_items());
            st.setString(12, comprobante.getMoneda());
            st.setString(13, comprobante.getAprobador());
            st.setString(14, user);
            st.setString(15, comprobante.getBase());
            st.setString(16, comprobante.getTipo_operacion());
            st.setInt(17, comprobante.getGrupo_transaccion());

            sql = st.getSql();

        } catch (Exception e) {
            throw new Exception("InsertComprobante " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }

        }
        return sql;
    }

    /**
     * Sql de insercion del comprobante detalle
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public String InsertComprobanteDetalle(Comprobantes comprodet, String user) throws Exception {
        StringStatement st = null;
        String query = "SQL_INSERT_COMPRODET";
        String sql = "";
        String consulta = this.obtenerSQL(query);
        try {


            st = new StringStatement(consulta, true);//JJCastro fase2
            st.setString(1, comprodet.getDstrct());
            st.setString(2, comprodet.getTipodoc());
            st.setString(3, comprodet.getNumdoc());
            st.setInt(4, comprodet.getGrupo_transaccion());
            st.setString(5, comprodet.getPeriodo());
            st.setString(6, comprodet.getCuenta());
            st.setString(7, comprodet.getAuxiliar());
            st.setString(8, comprodet.getAbc());
            st.setString(9, comprodet.getDetalle());
            st.setDouble(10, comprodet.getTotal_debito());
            st.setDouble(11, comprodet.getTotal_credito());
            st.setString(12, comprodet.getTercero());
            st.setString(13, user);
            st.setString(14, comprodet.getBase());
            st.setString(15, comprodet.getDocumento_interno());
            st.setString(16, comprodet.getTdoc_rel());
            st.setString(17, comprodet.getNumdoc_rel());

            sql += st.getSql();//JJCastro fase2
            st = null;



        } catch (Exception e) {
            throw new Exception("InsertComprobanteDetalle " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }

        }
        return sql;
    }

    public String updatecont(String ob, int cod, String est, String codn) throws Exception {
        StringStatement st = null;
        String query = "SQL_UPDATE_NEG";
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, ob);
            st.setInt(2, cod);//20100524
            st.setString(3, est);
            st.setString(4, codn);
            sql = st.getSql();

        } catch (SQLException e) {
            //System.out.println("Entra en el error 177");
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE NEGOCIOS " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return sql;
    }

 public String updateIngRev( int transa, String cod) throws Exception {
        StringStatement st = null;
        String query = "SQL_UPDATE_ING_REV";
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setInt(1, transa);
            st.setString(2, cod);
            sql = st.getSql();

        } catch (SQLException e) {
            //System.out.println("Entra en el error 177");
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE NEGOCIOS " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return sql;
    }

}
