/* * CxpsApplusDAO.java * * Created on 23 de julio de 2009, 15:43 */

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
//import com.tsp.util.connectionpool.PoolManager;
import java.io.*;

/** * * @author  Fintra */
public class CxpsApplusDAO   extends MainDAO  {
    ArrayList cxps,cxps_boni,cxps_pr,cxps_pf,cxps_ar;    
    public CxpsApplusDAO() {
        super("CxpsApplusDAO.xml");
    }
    public CxpsApplusDAO(String dataBaseName) {
        super("CxpsApplusDAO.xml", dataBaseName);
    }


/**
 * 
 * @param cxc_app
 * @return
 * @throws Exception
 */
    public String buscarCxpDeCxcApp(String cxc_app)throws Exception{   
        cxps=new ArrayList();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con =null;
        String query = "SQL_SEARCH_CXPS_APP";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cxc_app);
            rs = st.executeQuery();
            while (rs.next()) {                
                CxpApplus cxpApplus=CxpApplus.load(rs);
                cxps.add(cxpApplus);        
            }
            
            }}catch(SQLException e){
            System.out.println("error en cxpsapplusdao..:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR TODAS LAS CXPS APP " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return "";
    }




    
    public ArrayList getCxpDeCxcApp(){
        return cxps;
    }


/**
 * 
 * @param idordenes
 * @param cxps
 * @param abonoPrefactura1
 * @param descripcionPrefactura
 * @param loginx
 * @param fecfac
 * @return
 * @throws Exception
 */
 public Vector bajarCxps(String[] idordenes,ArrayList cxps,String abonoPrefactura1,String descripcionPrefactura,String loginx,String fecfac) throws Exception{
        //se supone que el total a abonar es positivo
        String cxc_apx="";//090730
        boolean mata_saldo_viejo=false;//090730
        String conjunto_aps="";//090730

        Vector respuesta =new Vector();
        String tipodoc="NC";//090729
        double abonoPrefactura=Double.parseDouble(abonoPrefactura1);
        double abonoPrefacturaUsable=abonoPrefactura;

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String insert_nc="INSERT_NC_DOC2";
        String insert_det_nc="INSERT_DET_NC_DOC";
        String update_cxp="UPDATE_CXP_APP";

        String insert_cxp="INSERT_BIG_CXP2"; //090730
        String insert_det_cxp="INSERT_DET_BIG_CXP"; //090730

        String insercion_nc,insercion_det_nc;
        String modificacion_cxp;

        String insercion_cxp,insercion_det_cxp;//090730

        //CxpApplus cxp1=(CxpApplus)cxps.get(0);
        ArrayList cxps_filtradas=new ArrayList();
        boolean sw_cxp_aparecio   =false;
        //inicio de filtrar cxps
        for (int j=0;j<cxps.size();j++){
            sw_cxp_aparecio=false;
            CxpApplus cxpx=(CxpApplus)cxps.get(j);
            cxc_apx=cxpx.getCxcAp();//090730

            int k=0;
            while (k<idordenes.length && sw_cxp_aparecio==false){
                if (idordenes[k].equals(cxpx.getIdOrden()) && Double.parseDouble(cxpx.getSaldo())!=0){
                    cxps_filtradas.add(cxpx);
                    conjunto_aps=conjunto_aps+cxpx.getFacturaApp()+"_";//090730
                    sw_cxp_aparecio=true;//090730
                    if (Double.parseDouble(cxpx.getAbono())!=0){
                        mata_saldo_viejo=true;//090730
                    }
                }
                k=k+1;
            }

        }
        //fin de filtrar cxps

        //mq hayan filas seleccionadas sin procesar y quede plata por abonar
        int i=0;
        double saldo_cxpx=0;
        double abonox=0;
        while (i<cxps_filtradas.size()){

            CxpApplus cxpx=(CxpApplus)cxps_filtradas.get(i);
            String doc_nc=""+cxpx.getFacturaApp()+"-"+cxpx.getCxcAp();
            if (Double.parseDouble(cxpx.getAbono())!=0){
                doc_nc=doc_nc+"-2";
            }
            saldo_cxpx=Double.parseDouble(cxpx.getSaldo());
            //si el saldo de la cxp es positivo:
            //if (saldo_cxpx>0){//se quit� porque siempre que que el saldo es negativo es menor que lo pendiente del total por abonar (positivo siempre) y en ese caso el abono debe ser el saldo de la cxp
                if (saldo_cxpx>abonoPrefacturaUsable){//si el saldo de la cxp es mayor que lo pendiente del total por abonar
                    abonox=abonoPrefacturaUsable; //el abono es lo pendiente del total por abonar
                }else{
                    abonox=saldo_cxpx; //el abono es el saldo de la cxp
                }
            //}

            abonoPrefacturaUsable=abonoPrefacturaUsable-abonox;


            try {
                insercion_nc=obtenerSQL( insert_nc );
                st = new StringStatement (insercion_nc, true);//JJCastro fase2
                st.setString(1,"8305137738");
                if (abonox>0){
                    st.setString(2,"NC");
                }else{
                    st.setString(2,"ND");
                }

                st.setString(3,""+doc_nc);
                st.setString(4,"APPLUS__"+descripcionPrefactura+"__"+cxpx.getNumOs());
                st.setString(5,cxpx.getBanco());
                st.setString(6,cxpx.getSucursal());

                if (abonox>0){
                    st.setString(7,""+abonox);
                    st.setString(8,""+abonox);
                    st.setString(9,""+abonox);
                    st.setString(10,""+abonox);
                }else{
                    st.setString(7,""+(abonox*-1));
                    st.setString(8,""+(abonox*-1));
                    st.setString(9,""+(abonox*-1));
                    st.setString(10,""+(abonox*-1));
                }
                st.setString(11,loginx);

                st.setString(12,cxpx.getFacturaApp());

                st.setString(13,fecfac);

                comando_sql = st.getSql();//JJCastro fase2
                //.out.println("comando_sql1"+comando_sql);

                respuesta.add(comando_sql);

                insercion_det_nc=obtenerSQL( insert_det_nc );
                st = new StringStatement (insercion_det_nc, true);//JJCastro fase2
                st.setString(1,"8305137738");
                if (abonox>0){
                    st.setString(2,"NC");
                }else{
                    st.setString(2,"ND");
                }
                st.setString(3,""+doc_nc);
                st.setString(4,"001");

                st.setString(5,""+descripcionPrefactura+"__"+cxpx.getNumOs());
                if (abonox>0){
                    st.setString(6,""+abonox);
                    st.setString(7,""+abonox);
                }else{
                    st.setString(6,""+(abonox*-1));
                    st.setString(7,""+(abonox*-1));
                }
                st.setString(8,""+"11050511");

                st.setString(9,"");

                st.setString(10,loginx);

                st.setString(11,"");

                comando_sql = st.getSql();//JJCastro fase2
                //.out.println("comando_sql2"+comando_sql);
                respuesta.add(comando_sql);

                insercion_det_cxp=obtenerSQL( insert_det_cxp );//090730
                st = new StringStatement (insercion_det_cxp, true);//JJCastro fase2

                st.setString(1,"8305137738");//090730
                st.setString(2,"FAP");//090730

                String doc_cxp=""+cxc_apx+"-";//090730
                if (mata_saldo_viejo){//090730
                    doc_cxp=doc_cxp+"2";//090730
                }else{//090730
                    doc_cxp=doc_cxp+"1";//090730
                }
                st.setString(3,""+doc_cxp);//090730
                if (i==0){
                    st.setString(4,"001");
                }else{
                    st.setString(4,""+(i+1));
                }
                st.setString(5,""+cxpx.getFacturaApp()+"__"+descripcionPrefactura+"__"+cxpx.getNumOs());

                st.setString(6,""+abonox);
                st.setString(7,""+abonox);
                st.setString(8,""+"11050511");
                st.setString(9,"");
                st.setString(10,loginx);
                st.setString(11,"");

                comando_sql = st.getSql();//JJCastro fase2
                //.out.println("comando_sql2"+comando_sql);
                respuesta.add(comando_sql);//090730

                modificacion_cxp=obtenerSQL( update_cxp );
                st = new StringStatement (modificacion_cxp, true);//JJCastro fase2
                st.setString(1,""+abonox);
                st.setString(2,""+abonox);
                st.setString(3,""+abonox);
                st.setString(4,""+abonox);
                st.setString(5,cxpx.getCxcAp());
                st.setString(6,"FINV");
                st.setString(7,"8305137738");
                st.setString(8,"FAP");
                st.setString(9,cxpx.getFacturaApp());

                comando_sql = st.getSql();//JJCastro fase2
                //.out.println("comando_sql3"+comando_sql);
                respuesta.add(comando_sql);

            }catch(Exception e){
                System.out.println("errorcit en CxpsApplusDAO:"+e.toString());
                throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA bajar cxpapplus. \n " + e.toString()+"__"+e.getMessage());
            }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }

            i=i+1;
        }//fin de mq hayan filas seleccionadas sin procesar y quede plata por abonar
        try{
            insercion_cxp=obtenerSQL( insert_cxp );//090730
            st = new StringStatement (insercion_cxp, true);//JJCastro fase2
            st.setString(1,"8305137738");//090730
            st.setString(2,"FAP");//090730

            String doc_cxp=""+cxc_apx+"-";//090730
            if (mata_saldo_viejo){//090730
                doc_cxp=doc_cxp+"2";//090730
            }else{//090730
                doc_cxp=doc_cxp+"1";//090730
            }
            st.setString(3,""+doc_cxp);//090730
            st.setString(4,"APPLUS. "+descripcionPrefactura+". Cxps: "+conjunto_aps);//090730
            st.setString(5,"BANCOLOMBIA");//090730
            st.setString(6,"CPAG");//090730
            st.setString(7,""+abonoPrefactura1);     //090730
            st.setString(8,""+abonoPrefactura1);  //090730
            st.setString(9,""+abonoPrefactura1);  //090730
            st.setString(10,""+abonoPrefactura1);  //090730
            st.setString(11,loginx);  //090730
            st.setString(12,""); //090730
            st.setString(13,fecfac);

            comando_sql = st.getSql();    //090730 //JJCastro fase2
            //.out.println("comando_sql4"+comando_sql);//090730
            respuesta.add(0,comando_sql); //090730
        }catch(Exception ee){//090730
            System.out.println("error metiendo cxp:"+ee.toString()+"__"+ee.getMessage());//090730
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }     //090730
        return respuesta;        //090730

    }



 /**
  *
  * @param cxc_boni
  * @return
  * @throws Exception
  */
    public String buscarCxpDeCxcBoni(String cxc_boni)throws Exception{   
        cxps_boni=new ArrayList();        
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con =null;
        String query = "SQL_SEARCH_CXPS_BONI";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cxc_boni);           
            rs = st.executeQuery();
            while (rs.next()) {                
                CxpBoni cxpBoni=CxpBoni.load(rs);
                cxps_boni.add(cxpBoni);        
            }            
            }}catch(SQLException e){
            System.out.println("error en cxpsapplusdao:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR TODAS LAS CXPS BONI " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return "";
    }
    
    public ArrayList getCxpDeCxcBoni(){
        return cxps_boni;
    }    



 /**
  * 
  * @param idacciones
  * @param cxps
  * @param abonoPrefactura1
  * @param descripcionPrefactura
  * @param loginx
  * @param fecfac
  * @return
  * @throws Exception
  */
    public Vector bajarCxpsBoni(String[] idacciones,ArrayList cxps,String abonoPrefactura1,String descripcionPrefactura,String loginx,String fecfac) throws Exception{
        //se supone que el total a abonar es positivo
        String tercero="9002335631";//090805
        String nombre_tercero="CONSORCIO";
        String cxc_abx="";//090730
        boolean mata_saldo_viejo=false;//090730
        String conjunto_abs="";//090730
        
        Vector respuesta =new Vector();            
        String tipodoc="NC";//090729
        double abonoPrefactura=Double.parseDouble(abonoPrefactura1);
        double abonoPrefacturaUsable=abonoPrefactura;
                        
        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String insert_nc="INSERT_NC_DOC2";
        String insert_det_nc="INSERT_DET_NC_DOC";
        String update_cxp="UPDATE_CXP_AB"; 
     
        String insert_cxp="INSERT_BIG_CXP2"; //090730
        String insert_det_cxp="INSERT_DET_BIG_CXP"; //090730        
        String insercion_nc,insercion_det_nc;
        String modificacion_cxp;        
        String insercion_cxp,insercion_det_cxp;//090730
        
        //CxpApplus cxp1=(CxpApplus)cxps.get(0);
        ArrayList cxps_filtradas=new ArrayList();
        boolean sw_cxp_aparecio   =false;
        //inicio de filtrar cxps
        for (int j=0;j<cxps.size();j++){
            sw_cxp_aparecio=false;
            CxpBoni cxpx=(CxpBoni)cxps.get(j);
            cxc_abx=cxpx.getCxcAb();//090730
            
            int k=0;
            while (k<idacciones.length && sw_cxp_aparecio==false){
                if (idacciones[k].equals(cxpx.getIdAccion()) && Double.parseDouble(cxpx.getSaldo())!=0){
                    cxps_filtradas.add(cxpx);
                    conjunto_abs=conjunto_abs+cxpx.getFacturaBoni()+"_";//090730
                    sw_cxp_aparecio=true;//090730
                    if (Double.parseDouble(cxpx.getAbono())!=0){
                        mata_saldo_viejo=true;//090730
                    }
                }
                k=k+1;
            }            
            
        }
        //fin de filtrar cxps
        
        //mq hayan filas seleccionadas sin procesar y quede plata por abonar 
        int i=0;
        double saldo_cxpx=0;
        double abonox=0;
        while (i<cxps_filtradas.size()){
                                        
            CxpBoni cxpx=(CxpBoni)cxps_filtradas.get(i);
            String doc_nc=""+cxpx.getFacturaBoni()+"-"+cxpx.getCxcAb();
            if (Double.parseDouble(cxpx.getAbono())!=0){
                doc_nc=doc_nc+"-2";                
            }
            saldo_cxpx=Double.parseDouble(cxpx.getSaldo());
            //si el saldo de la cxp es positivo:
            //if (saldo_cxpx>0){//se quit� porque siempre que que el saldo es negativo es menor que lo pendiente del total por abonar (positivo siempre) y en ese caso el abono debe ser el saldo de la cxp
                if (saldo_cxpx>abonoPrefacturaUsable){//si el saldo de la cxp es mayor que lo pendiente del total por abonar
                    abonox=abonoPrefacturaUsable; //el abono es lo pendiente del total por abonar
                }else{
                    abonox=saldo_cxpx; //el abono es el saldo de la cxp
                }
            //}
            
            abonoPrefacturaUsable=abonoPrefacturaUsable-abonox;
                        
            try {
                insercion_nc=obtenerSQL( insert_nc );
                st = new StringStatement (insercion_nc, true);//JJCastro fase2
                st.setString(1,tercero);//090805 ex8305137738
                if (abonox>0){
                    st.setString(2,"NC");
                }else{
                    st.setString(2,"ND");
                }
                
                st.setString(3,""+doc_nc);
                st.setString(4,""+nombre_tercero+"__"+descripcionPrefactura+"__"+cxpx.getPrefactura()+"__"+cxpx.getContratista());//090805 exAPPLUS
                st.setString(5,cxpx.getBanco());
                st.setString(6,cxpx.getSucursal());
                
                if (abonox>0){
                    st.setString(7,""+abonox);                
                    st.setString(8,""+abonox);
                    st.setString(9,""+abonox);
                    st.setString(10,""+abonox);
                }else{
                    st.setString(7,""+(abonox*-1));                
                    st.setString(8,""+(abonox*-1));
                    st.setString(9,""+(abonox*-1));
                    st.setString(10,""+(abonox*-1));
                }
                st.setString(11,loginx);                
                st.setString(12,cxpx.getFacturaBoni());
                st.setString(13,fecfac);
                
                comando_sql = st.getSql();//JJCastro fase2
                //.out.println("comando_sql1"+comando_sql);
                                                                
                respuesta.add(comando_sql);
                              
                insercion_det_nc=obtenerSQL( insert_det_nc );
                st = new StringStatement (insercion_det_nc, true);//JJCastro fase2
                st.setString(1,tercero);//090805
                if (abonox>0){
                    st.setString(2,"NC");
                }else{
                    st.setString(2,"ND");
                }
                st.setString(3,""+doc_nc);
                st.setString(4,"001");
                                
                st.setString(5,""+descripcionPrefactura+"__"+cxpx.getPrefactura()+"__"+cxpx.getContratista()); 
                if (abonox>0){
                    st.setString(6,""+abonox);
                    st.setString(7,""+abonox);
                }else{
                    st.setString(6,""+(abonox*-1));
                    st.setString(7,""+(abonox*-1));
                }
                st.setString(8,""+"11050512");                
                st.setString(9,"");                
                st.setString(10,loginx);                
                st.setString(11,"");
                
                comando_sql = st.getSql();//JJCastro fase2
                //.out.println("comando_sql22"+comando_sql);                
                respuesta.add(comando_sql);

                insercion_det_cxp=obtenerSQL( insert_det_cxp );//090730  
                st = new StringStatement (insercion_det_cxp, true);//JJCastro fase2
                st.setString(1,tercero);//090805
                st.setString(2,"FAP");//090730            
                              
                String doc_cxp=""+cxc_abx+"-";//090730
                if (mata_saldo_viejo){//090730
                    doc_cxp=doc_cxp+"2";//090730
                }else{//090730
                    doc_cxp=doc_cxp+"1";//090730
                }
                st.setString(3,""+doc_cxp);//090730
                if (i==0){                
                    st.setString(4,"001");
                }else{
                    st.setString(4,""+(i+1));
                }                                
                st.setString(5,""+cxpx.getFacturaBoni()+"__"+descripcionPrefactura+"__"+cxpx.getPrefactura()+"__"+cxpx.getCantidadAcciones()+"__"+cxpx.getContratista()); 
                
                st.setString(6,""+abonox);
                st.setString(7,""+abonox);
                
                st.setString(8,""+"11050512");
                
                st.setString(9,"");
                
                st.setString(10,loginx);
                
                st.setString(11,"");
                
                comando_sql = st.getSql();
                //.out.println("comando_sql2"+comando_sql);                
                respuesta.add(comando_sql);//090730
                                            

                modificacion_cxp=obtenerSQL( update_cxp );
                st = new StringStatement (modificacion_cxp, true);//JJCastro fase2
                st.setString(1,""+abonox);
                st.setString(2,""+abonox);
                st.setString(3,""+abonox);
                st.setString(4,""+abonox);
                st.setString(5,cxpx.getCxcAb());
                st.setString(6,"FINV");
                st.setString(7,tercero);//090805
                st.setString(8,"FAP");
                st.setString(9,cxpx.getFacturaBoni());
                
                comando_sql = st.getSql();//JJCastro fase2
                //.out.println("comando_sql3"+comando_sql);
                respuesta.add(comando_sql);
                
            }catch(Exception e){
                System.out.println("errorcit en CxpsBoniDAO:"+e.toString());        
                throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA bajar cxpboni. \n " + e.toString()+"__"+e.getMessage());
            }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
            
            i=i+1;  
        }//fin de mq hayan filas seleccionadas sin procesar y quede plata por abonar 
        try{
            insercion_cxp=obtenerSQL( insert_cxp );//090730            
            st                              = new StringStatement (insercion_cxp, true);//JJCastro fase2
            st.setString(1,tercero);//090805            
            st.setString(2,"FAP");//090730            
            String doc_cxp=""+cxc_abx+"-";//090730
            if (mata_saldo_viejo){//090730
                doc_cxp=doc_cxp+"2";//090730
            }else{//090730
                doc_cxp=doc_cxp+"1";//090730
            }
            st.setString(3,""+doc_cxp);//090730
            st.setString(4,""+nombre_tercero+". "+descripcionPrefactura+". Cxps: "+conjunto_abs);//090805
            st.setString(5,"BANCOLOMBIA");//090730
            st.setString(6,"CPAG");//090730
            st.setString(7,""+abonoPrefactura1);     //090730           
            st.setString(8,""+abonoPrefactura1);  //090730           
            st.setString(9,""+abonoPrefactura1);  //090730           
            st.setString(10,""+abonoPrefactura1);  //090730        
            st.setString(11,loginx);  //090730           
            st.setString(12,""); //090730           
            st.setString(13,fecfac);
            
            comando_sql = st.getSql();    //090730 //JJCastro fase2
            //.out.println("comando_sql4"+comando_sql);//090730           
            respuesta.add(0,comando_sql); //090730           
        }catch(Exception ee){//090730           
            System.out.println("error metiendo cxp por ab:"+ee.toString()+"__"+ee.getMessage());//090730           
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }      //090730
        return respuesta;        //090730           
               
    }




    /**
     * 
     * @param cxc_pr
     * @return
     * @throws Exception
     */
    public String buscarCxpDeCxcPr(String cxc_pr)throws Exception{   
        cxps_pr=new ArrayList();
        
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con =null;
        String query = "SQL_SEARCH_CXPS_PR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cxc_pr);
            rs = st.executeQuery();
            while (rs.next()) {                
                //.out.println("va u");
                CxpPr cxpPr=CxpPr.load(rs);                
                cxps_pr.add(cxpPr);        
            }
            
            }}catch(SQLException e){
            System.out.println("error en cxpsapplusdao pr..:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR TODAS LAS CXPS PR " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return "";
    }
    
    public ArrayList getCxpDeCxcPr(){
        return cxps_pr;
    }



/**
 *
 * @param idordenes
 * @param cxps
 * @param abonoPrefactura1
 * @param descripcionPrefactura
 * @param loginx
 * @param fecfac
 * @return
 * @throws Exception
 */
    public Vector bajarCxpsPr(String[] idordenes,ArrayList cxps,String abonoPrefactura1,String descripcionPrefactura,String loginx,String fecfac) throws Exception{
        //se supone que el total a abonar es positivo
        String cxc_prx="";//090730
        boolean mata_saldo_viejo=false;//090730
        String conjunto_prs="";//090730
        
        Vector respuesta =new Vector();            
        String tipodoc="NC";//090729
        double abonoPrefactura=Double.parseDouble(abonoPrefactura1);
        double abonoPrefacturaUsable=abonoPrefactura;
                        
        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String insert_nc="INSERT_NC_DOC2";
        String insert_det_nc="INSERT_DET_NC_DOC";
        String update_cxp="UPDATE_CXP_PR"; 
     
        String insert_cxp="INSERT_BIG_CXP2"; //090730
        String insert_det_cxp="INSERT_DET_BIG_CXP"; //090730
        
        String insercion_nc,insercion_det_nc;
        String modificacion_cxp;
        
        String insercion_cxp,insercion_det_cxp;//090730

        ArrayList cxps_filtradas=new ArrayList();
        boolean sw_cxp_aparecio   =false;
        for (int j=0;j<cxps.size();j++){
            sw_cxp_aparecio=false;
            CxpPr cxpx=(CxpPr)cxps.get(j);
            cxc_prx=cxpx.getCxcPr();//090730
            
            int k=0;
            while (k<idordenes.length && sw_cxp_aparecio==false){
                if (idordenes[k].equals(cxpx.getIdOrden()) && Double.parseDouble(cxpx.getSaldo())!=0){
                    cxps_filtradas.add(cxpx);
                    conjunto_prs=conjunto_prs+cxpx.getFacturaPr()+"_";//090730
                    sw_cxp_aparecio=true;//090730
                    if (Double.parseDouble(cxpx.getAbono())!=0){
                        mata_saldo_viejo=true;//090730
                    }
                }
                k=k+1;
            }            
            
        }
        //fin de filtrar cxps
        
        //mq hayan filas seleccionadas sin procesar y quede plata por abonar 
        int i=0;
        double saldo_cxpx=0;
        double abonox=0;
        //.out.println("antes de while:"+cxps_filtradas.size());
        while (i<cxps_filtradas.size()){
                                        
            CxpPr cxpx=(CxpPr)cxps_filtradas.get(i);
            String doc_nc=""+cxpx.getFacturaPr()+"-"+cxpx.getCxcPr();
            if (Double.parseDouble(cxpx.getAbono())!=0){
                doc_nc=doc_nc+"-2";                
            }
            saldo_cxpx=Double.parseDouble(cxpx.getSaldo());
            //si el saldo de la cxp es positivo:
            //if (saldo_cxpx>0){//se quit� porque siempre que que el saldo es negativo es menor que lo pendiente del total por abonar (positivo siempre) y en ese caso el abono debe ser el saldo de la cxp
                if (saldo_cxpx>abonoPrefacturaUsable){//si el saldo de la cxp es mayor que lo pendiente del total por abonar
                    abonox=abonoPrefacturaUsable; //el abono es lo pendiente del total por abonar
                }else{
                    abonox=saldo_cxpx; //el abono es el saldo de la cxp
                }
            //}
            
            abonoPrefacturaUsable=abonoPrefacturaUsable-abonox;
            
            
            try {
                insercion_nc=obtenerSQL( insert_nc );
                st = new StringStatement (insercion_nc, true);//JJCastro fase2
                st.setString(1,"9000742958");
                if (abonox>0){
                    st.setString(2,"NC");
                }else{
                    st.setString(2,"ND");
                }
                
                st.setString(3,""+doc_nc);
                st.setString(4,"PROVINTEGRAL__"+descripcionPrefactura+"__"+cxpx.getNumOs());
                st.setString(5,cxpx.getBanco());
                st.setString(6,cxpx.getSucursal());
                
                if (abonox>0){
                    st.setString(7,""+abonox);                
                    st.setString(8,""+abonox);
                    st.setString(9,""+abonox);
                    st.setString(10,""+abonox);
                }else{
                    st.setString(7,""+(abonox*-1));                
                    st.setString(8,""+(abonox*-1));
                    st.setString(9,""+(abonox*-1));
                    st.setString(10,""+(abonox*-1));
                }
                st.setString(11,loginx);
                
                st.setString(12,cxpx.getFacturaPr());
                st.setString(13,fecfac);
                
                comando_sql = st.getSql();//JJCastro fase2
                respuesta.add(comando_sql);
               

                insercion_det_nc=obtenerSQL( insert_det_nc );
                st = new StringStatement (insercion_det_nc, true);//JJCastro fase2
                st.setString(1,"9000742958");
                if (abonox>0){
                    st.setString(2,"NC");
                }else{
                    st.setString(2,"ND");
                }
                st.setString(3,""+doc_nc);
                st.setString(4,"001");
                                
                st.setString(5,""+descripcionPrefactura+"__"+cxpx.getNumOs()); 
                if (abonox>0){
                    st.setString(6,""+abonox);
                    st.setString(7,""+abonox);
                }else{
                    st.setString(6,""+(abonox*-1));
                    st.setString(7,""+(abonox*-1));
                }
                st.setString(8,""+"11050513");
                
                st.setString(9,"");
                
                st.setString(10,loginx);
                
                st.setString(11,"");
                
                comando_sql = st.getSql();//JJCastro fase2
                respuesta.add(comando_sql);


                insercion_det_cxp=obtenerSQL( insert_det_cxp );//090730            
                st = new StringStatement (insercion_det_cxp, true);//JJCastro fase2
                st.setString(1,"9000742958");//090730            
                st.setString(2,"FAP");//090730            
                              
                String doc_cxp=""+cxc_prx+"-";//090730
                if (mata_saldo_viejo){//090730
                    doc_cxp=doc_cxp+"2";//090730
                }else{//090730
                    doc_cxp=doc_cxp+"1";//090730
                }
                st.setString(3,""+doc_cxp);//090730
                if (i==0){                
                    st.setString(4,"001");
                }else{
                    st.setString(4,""+(i+1));
                }                                
                st.setString(5,""+cxpx.getFacturaPr()+"__"+descripcionPrefactura+"__"+cxpx.getNumOs()); 
                
                st.setString(6,""+abonox);
                st.setString(7,""+abonox);
                st.setString(8,""+"11050513");
                st.setString(9,"");
                st.setString(10,loginx);
                st.setString(11,"");
                
                comando_sql = st.getSql();//JJCastro fase2
                respuesta.add(comando_sql);//090730
            

                modificacion_cxp=obtenerSQL( update_cxp );
                st = new StringStatement (modificacion_cxp, true);//JJCastro fase2
                st.setString(1,""+abonox);
                st.setString(2,""+abonox);
                st.setString(3,""+abonox);
                st.setString(4,""+abonox);
                st.setString(5,cxpx.getCxcPr());
                st.setString(6,"FINV");
                st.setString(7,"9000742958");
                st.setString(8,"FAP");
                st.setString(9,cxpx.getFacturaPr());
                
                comando_sql = st.getSql();//JJCastro fase2
                respuesta.add(comando_sql);
                
            }catch(Exception e){
                System.out.println("errorcit en CxpsApplusDAO pr:"+e.toString());        
                throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA bajar cxppr. \n " + e.toString()+"__"+e.getMessage());
            }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
            
            i=i+1;  
        }//fin de mq hayan filas seleccionadas sin procesar y quede plata por abonar 
        try{
            insercion_cxp=obtenerSQL( insert_cxp );//090730            
            st = new StringStatement (insercion_cxp, true);//JJCastro fase2
            st.setString(1,"9000742958");//090730            
            st.setString(2,"FAP");//090730            

            String doc_cxp=""+cxc_prx+"-";//090730
            if (mata_saldo_viejo){//090730
                doc_cxp=doc_cxp+"2";//090730
            }else{//090730
                doc_cxp=doc_cxp+"1";//090730
            }
            st.setString(3,""+doc_cxp);//090730
            st.setString(4,"PROVINTEGRAL. "+descripcionPrefactura+". Cxps: "+conjunto_prs);//090730
            st.setString(5,"BANCOLOMBIA");//090730
            st.setString(6,"CPAG");//090730
            st.setString(7,""+abonoPrefactura1);     //090730           
            st.setString(8,""+abonoPrefactura1);  //090730           
            st.setString(9,""+abonoPrefactura1);  //090730           
            st.setString(10,""+abonoPrefactura1);  //090730           
            st.setString(11,loginx);  //090730           
            st.setString(12,""); //090730           
            st.setString(13,fecfac);//

            comando_sql = st.getSql();    //090730           //JJCastro fase2
            respuesta.add(0,comando_sql); //090730           
        }catch(Exception ee){//090730           
            System.out.println("error metiendo cxp:"+ee.toString()+"__"+ee.getMessage());//090730           
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }      //090730
        return respuesta;        //090730           
       
    }




/**
 * 
 * @param cxc_pf
 * @return
 * @throws Exception
 */
    public String buscarCxpDeCxcPf(String cxc_pf)throws Exception{   
        cxps_pf=new ArrayList();        
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con =null;
        String query = "SQL_SEARCH_CXPS_PF";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cxc_pf);           
            rs = st.executeQuery();
            while (rs.next()) {                
                CxpPf cxpPf=CxpPf.load(rs);
                cxps_pf.add(cxpPf);        
            }            
            }}catch(SQLException e){
            System.out.println("error en cxpsapplusdao:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR TODAS LAS CXPS PF " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return "";
    }



    
    
    public ArrayList getCxpDeCxcPf(){
        return cxps_pf;
    }    
    
    
    public Vector bajarCxpsPf(String[] idacciones,ArrayList cxps,String abonoPrefactura1,String descripcionPrefactura,String loginx,String fecfac) throws Exception{
        //se supone que el total a abonar es positivo
        String tercero="9000742958";//090805
        String nombre_tercero="PROVINTEGRAL";
        String cxc_pfx="";//090730
        boolean mata_saldo_viejo=false;//090730
        String conjunto_pfs="";//090730
        
        Vector respuesta =new Vector();            
        String tipodoc="NC";//090729
        double abonoPrefactura=Double.parseDouble(abonoPrefactura1);
        double abonoPrefacturaUsable=abonoPrefactura;
                        
        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String insert_nc="INSERT_NC_DOC2";
        String insert_det_nc="INSERT_DET_NC_DOC";
        String update_cxp="UPDATE_CXP_PF";      
        String insert_cxp="INSERT_BIG_CXP2"; //090730
        String insert_det_cxp="INSERT_DET_BIG_CXP"; //090730
        
        String insercion_nc,insercion_det_nc;
        String modificacion_cxp;
        
        String insercion_cxp,insercion_det_cxp;//090730
        
        //CxpApplus cxp1=(CxpApplus)cxps.get(0);
        ArrayList cxps_filtradas=new ArrayList();
        boolean sw_cxp_aparecio   =false;
        //inicio de filtrar cxps
        for (int j=0;j<cxps.size();j++){
            sw_cxp_aparecio=false;
            CxpPf cxpx=(CxpPf)cxps.get(j);
            cxc_pfx=cxpx.getCxcPf();//090730
            
            int k=0;
            while (k<idacciones.length && sw_cxp_aparecio==false){
                if (idacciones[k].equals(cxpx.getIdAccion()) && Double.parseDouble(cxpx.getSaldo())!=0){
                    cxps_filtradas.add(cxpx);
                    conjunto_pfs=conjunto_pfs+cxpx.getFacturaPf()+"_";//090730
                    sw_cxp_aparecio=true;//090730
                    if (Double.parseDouble(cxpx.getAbono())!=0){
                        mata_saldo_viejo=true;//090730
                    }
                }
                k=k+1;
            }            
            
        }
        //fin de filtrar cxps
        
        //mq hayan filas seleccionadas sin procesar y quede plata por abonar 
        int i=0;
        double saldo_cxpx=0;
        double abonox=0;
        while (i<cxps_filtradas.size()){
                                        
            CxpPf cxpx=(CxpPf)cxps_filtradas.get(i);
            String doc_nc=""+cxpx.getFacturaPf()+"-"+cxpx.getCxcPf();
            if (Double.parseDouble(cxpx.getAbono())!=0){
                doc_nc=doc_nc+"-2";                
            }
            saldo_cxpx=Double.parseDouble(cxpx.getSaldo());
            //si el saldo de la cxp es positivo:
            //if (saldo_cxpx>0){//se quit� porque siempre que que el saldo es negativo es menor que lo pendiente del total por abonar (positivo siempre) y en ese caso el abono debe ser el saldo de la cxp
                if (saldo_cxpx>abonoPrefacturaUsable){//si el saldo de la cxp es mayor que lo pendiente del total por abonar
                    abonox=abonoPrefacturaUsable; //el abono es lo pendiente del total por abonar
                }else{
                    abonox=saldo_cxpx; //el abono es el saldo de la cxp
                }
            //}
            
            abonoPrefacturaUsable=abonoPrefacturaUsable-abonox;
                        
            try {
                insercion_nc=obtenerSQL( insert_nc );
                st = new StringStatement (insercion_nc, true);//JJCastro fase2
                st.setString(1,tercero);//090805 ex8305137738
                if (abonox>0){
                    st.setString(2,"NC");
                }else{
                    st.setString(2,"ND");
                }
                
                st.setString(3,""+doc_nc);
                st.setString(4,""+nombre_tercero+"__"+descripcionPrefactura+"__"+cxpx.getPrefactura()+"__"+cxpx.getContratista());//090805 exAPPLUS
                st.setString(5,cxpx.getBanco());
                st.setString(6,cxpx.getSucursal());
                
                if (abonox>0){
                    st.setString(7,""+abonox);                
                    st.setString(8,""+abonox);
                    st.setString(9,""+abonox);
                    st.setString(10,""+abonox);
                }else{
                    st.setString(7,""+(abonox*-1));                
                    st.setString(8,""+(abonox*-1));
                    st.setString(9,""+(abonox*-1));
                    st.setString(10,""+(abonox*-1));
                }
                st.setString(11,loginx);
                
                st.setString(12,cxpx.getFacturaPf());
                st.setString(13,fecfac);
                comando_sql = st.getSql();//JJCastro fase2
                //.out.println("comando_sql1"+comando_sql);
                                                                
                respuesta.add(comando_sql);
                              
                insercion_det_nc=obtenerSQL( insert_det_nc );
                st = new StringStatement (insercion_det_nc, true);//JJCastro fase2
                st.setString(1,tercero);//090805
                if (abonox>0){
                    st.setString(2,"NC");
                }else{
                    st.setString(2,"ND");
                }
                st.setString(3,""+doc_nc);
                st.setString(4,"001");
                                
                st.setString(5,""+descripcionPrefactura+"__"+cxpx.getPrefactura()+"__"+cxpx.getContratista()); 
                if (abonox>0){
                    st.setString(6,""+abonox);
                    st.setString(7,""+abonox);
                }else{
                    st.setString(6,""+(abonox*-1));
                    st.setString(7,""+(abonox*-1));
                }
                st.setString(8,""+"11050514");                
                st.setString(9,"");                
                st.setString(10,loginx);                
                st.setString(11,"");
                
                comando_sql = st.getSql();//JJCastro fase2
                respuesta.add(comando_sql);

                insercion_det_cxp=obtenerSQL( insert_det_cxp );//090730            
                st = new StringStatement (insercion_det_cxp, true);//JJCastro fase2
                st.setString(1,tercero);//090805
                st.setString(2,"FAP");//090730            
                              
                String doc_cxp=""+cxc_pfx+"-";//090730
                if (mata_saldo_viejo){//090730
                    doc_cxp=doc_cxp+"2";//090730
                }else{//090730
                    doc_cxp=doc_cxp+"1";//090730
                }
                st.setString(3,""+doc_cxp);//090730
                if (i==0){                
                    st.setString(4,"001");
                }else{
                    st.setString(4,""+(i+1));
                }                                
                st.setString(5,""+cxpx.getFacturaPf()+"__"+descripcionPrefactura+"__"+cxpx.getPrefactura()+"__"+cxpx.getCantidadAcciones()+"__"+cxpx.getContratista()); 
                st.setString(6,""+abonox);
                st.setString(7,""+abonox);
                st.setString(8,""+"11050514");
                st.setString(9,"");
                st.setString(10,loginx);
                st.setString(11,"");
                
                comando_sql = st.getSql();//JJCastro fase2
                respuesta.add(comando_sql);//090730
                                            
                modificacion_cxp=obtenerSQL( update_cxp );
                st = new StringStatement (modificacion_cxp, true);//JJCastro fase2
                st.setString(1,""+abonox);
                st.setString(2,""+abonox);
                st.setString(3,""+abonox);
                st.setString(4,""+abonox);
                st.setString(5,cxpx.getCxcPf());
                st.setString(6,"FINV");
                st.setString(7,tercero);//090805
                st.setString(8,"FAP");
                st.setString(9,cxpx.getFacturaPf());
                
                comando_sql = st.getSql();//JJCastro fase2
                respuesta.add(comando_sql);
                
            }catch(Exception e){
                System.out.println("errorcit en CxpsPfDAO:"+e.toString());        
                throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA bajar cxppf. \n " + e.toString()+"__"+e.getMessage());
            }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
            
            i=i+1;  
        }//fin de mq hayan filas seleccionadas sin procesar y quede plata por abonar 
        try{
            insercion_cxp=obtenerSQL( insert_cxp );//090730            
            st = new StringStatement (insercion_cxp, true);//JJCastro fase2
            st.setString(1,tercero);//090805            
            st.setString(2,"FAP");//090730            

            String doc_cxp=""+cxc_pfx+"-";//090730
            if (mata_saldo_viejo){//090730
                doc_cxp=doc_cxp+"2";//090730
            }else{//090730
                doc_cxp=doc_cxp+"1";//090730
            }
            st.setString(3,""+doc_cxp);//090730
            st.setString(4,""+nombre_tercero+". "+descripcionPrefactura+". Cxps: "+conjunto_pfs);//090805
            st.setString(5,"BANCOLOMBIA");//090730
            st.setString(6,"CPAG");//090730
            st.setString(7,""+abonoPrefactura1);     //090730           
            st.setString(8,""+abonoPrefactura1);  //090730           
            st.setString(9,""+abonoPrefactura1);  //090730           
            st.setString(10,""+abonoPrefactura1);  //090730           
            st.setString(11,loginx);  //090730           
            st.setString(12,""); //090730           
            st.setString(13,fecfac);
            comando_sql = st.getSql();    //090730      //JJCastro fase2
            //.out.println("comando_sql4"+comando_sql);//090730           
            respuesta.add(0,comando_sql); //090730           
        }catch(Exception ee){//090730           
            System.out.println("error metiendo cxp por pf:"+ee.toString()+"__"+ee.getMessage());//090730           
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }      //090730
        return respuesta;        //090730           
               
    }
    

 /**
  * 
  * @param cxc_ar
  * @return
  * @throws Exception
  */
    public String buscarCxpDeCxcAr(String cxc_ar)throws Exception{
        cxps_ar=new ArrayList();        
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con =null;
        String query = "SQL_SEARCH_CXPS_AR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cxc_ar);           
            rs = st.executeQuery();
            while (rs.next()) {                
                CxpAr cxpAr=CxpAr.load(rs);
                cxps_ar.add(cxpAr);        
            }            
            }}catch(SQLException e){
            System.out.println("error en cxpsapplusdaoar:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR TODAS LAS CXPS AR " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return "";
    }
    
    public ArrayList getCxpDeCxcAr(){
        return cxps_ar;
    }    


    
/**
 *
 * @param idacciones
 * @param cxps
 * @param abonoPrefactura1
 * @param descripcionPrefactura
 * @param loginx
 * @param fecfac
 * @return
 * @throws Exception
 */
    public Vector bajarCxpsAr(String[] idacciones,ArrayList cxps,String abonoPrefactura1,String descripcionPrefactura,String loginx,String fecfac) throws Exception{
        //se supone que el total a abonar es positivo
        String tercero="9002335631";//090805
        String nombre_tercero="CONSORCIO";
        String cxc_arx="";//090730
        boolean mata_saldo_viejo=false;//090730
        String conjunto_ars="";//090730
        
        Vector respuesta =new Vector();            
        String tipodoc="NC";//090729
        double abonoPrefactura=Double.parseDouble(abonoPrefactura1);
        double abonoPrefacturaUsable=abonoPrefactura;
                        
        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql = "";
        String insert_nc="INSERT_NC_DOC2";
        String insert_det_nc="INSERT_DET_NC_DOC";
        String update_cxp="UPDATE_CXP_AR"; 
     
        String insert_cxp="INSERT_BIG_CXP2"; //090730
        String insert_det_cxp="INSERT_DET_BIG_CXP"; //090730
        
        String insercion_nc,insercion_det_nc;
        String modificacion_cxp;
        
        String insercion_cxp,insercion_det_cxp;//090730
        
        //CxpApplus cxp1=(CxpApplus)cxps.get(0);
        ArrayList cxps_filtradas=new ArrayList();
        boolean sw_cxp_aparecio   =false;
        //inicio de filtrar cxps
        for (int j=0;j<cxps.size();j++){
            sw_cxp_aparecio=false;
            CxpAr cxpx=(CxpAr)cxps.get(j);
            cxc_arx=cxpx.getCxcAr();//090730
            
            int k=0;
            while (k<idacciones.length && sw_cxp_aparecio==false){
                if (idacciones[k].equals(cxpx.getIdAccion()) && Double.parseDouble(cxpx.getSaldo())!=0){
                    cxps_filtradas.add(cxpx);
                    conjunto_ars=conjunto_ars+cxpx.getFacturaAr()+"_";//090730
                    sw_cxp_aparecio=true;//090730
                    if (Double.parseDouble(cxpx.getAbono())!=0){
                        mata_saldo_viejo=true;//090730
                    }
                }
                k=k+1;
            }            
            
        }
        //fin de filtrar cxps
        
        //mq hayan filas seleccionadas sin procesar y quede plata por abonar 
        int i=0;
        double saldo_cxpx=0;
        double abonox=0;
        //.out.println("cxps_filtradas.size()"+cxps_filtradas.size());
        while (i<cxps_filtradas.size()){
                                        
            CxpAr cxpx=(CxpAr)cxps_filtradas.get(i);
            String doc_nc=""+cxpx.getFacturaAr()+"-"+cxpx.getCxcAr();
            //.out.println("doc_nc"+doc_nc);
            if (Double.parseDouble(cxpx.getAbono())!=0){
                doc_nc=doc_nc+"-2";                
            }
            saldo_cxpx=Double.parseDouble(cxpx.getSaldo());
            //si el saldo de la cxp es positivo:
            //if (saldo_cxpx>0){//se quit� porque siempre que que el saldo es negativo es menor que lo pendiente del total por abonar (positivo siempre) y en ese caso el abono debe ser el saldo de la cxp
                if (saldo_cxpx>abonoPrefacturaUsable){//si el saldo de la cxp es mayor que lo pendiente del total por abonar
                    abonox=abonoPrefacturaUsable; //el abono es lo pendiente del total por abonar
                }else{
                    abonox=saldo_cxpx; //el abono es el saldo de la cxp
                }
            //}
            
            abonoPrefacturaUsable=abonoPrefacturaUsable-abonox;
                        
            try {
                insercion_nc=obtenerSQL( insert_nc );
                st = new StringStatement (insercion_nc, true);//JJCastro fase2
                st.setString(1,tercero);//090805 ex8305137738
                if (abonox>0){
                    st.setString(2,"NC");
                }else{
                    st.setString(2,"ND");
                }
                
                st.setString(3,""+doc_nc);
                st.setString(4,""+nombre_tercero+"__"+descripcionPrefactura+"__"+cxpx.getPrefactura()+"__"+cxpx.getContratista());//090805 exAPPLUS
                st.setString(5,cxpx.getBanco());
                st.setString(6,cxpx.getSucursal());
                
                if (abonox>0){
                    st.setString(7,""+abonox);                
                    st.setString(8,""+abonox);
                    st.setString(9,""+abonox);
                    st.setString(10,""+abonox);
                }else{
                    st.setString(7,""+(abonox*-1));                
                    st.setString(8,""+(abonox*-1));
                    st.setString(9,""+(abonox*-1));
                    st.setString(10,""+(abonox*-1));
                }
                st.setString(11,loginx);
                
                st.setString(12,cxpx.getFacturaAr());
                st.setString(13,fecfac);
                comando_sql = st.getSql();//JJCastro fase2
                                                                
                respuesta.add(comando_sql);
                              

                insercion_det_nc=obtenerSQL( insert_det_nc );
                st = new StringStatement (insercion_det_nc, true);//JJCastro fase2
                st.setString(1,tercero);//090805
                if (abonox>0){
                    st.setString(2,"NC");
                }else{
                    st.setString(2,"ND");
                }
                st.setString(3,""+doc_nc);
                st.setString(4,"001");
                st.setString(5,""+descripcionPrefactura+"__"+cxpx.getPrefactura()+"__"+cxpx.getContratista()); 
                if (abonox>0){
                    st.setString(6,""+abonox);
                    st.setString(7,""+abonox);
                }else{
                    st.setString(6,""+(abonox*-1));
                    st.setString(7,""+(abonox*-1));
                }
                st.setString(8,""+"11050515");
                st.setString(9,"");
                st.setString(10,loginx);
                st.setString(11,"");
                
                comando_sql = st.getSql();//JJCastro fase2
                //.out.println("comando_sql22"+comando_sql);                
                respuesta.add(comando_sql);


                insercion_det_cxp=obtenerSQL( insert_det_cxp );//090730            
                st = new StringStatement (insercion_det_cxp, true);//JJCastro fase2
                st.setString(1,tercero);//090805
                
                st.setString(2,"FAP");//090730            
                              
                String doc_cxp=""+cxc_arx+"-";//090730
                if (mata_saldo_viejo){//090730
                    doc_cxp=doc_cxp+"2";//090730
                }else{//090730
                    doc_cxp=doc_cxp+"1";//090730
                }
                st.setString(3,""+doc_cxp);//090730
                if (i==0){                
                    st.setString(4,"001");
                }else{
                    st.setString(4,""+(i+1));
                }                                
                st.setString(5,""+cxpx.getFacturaAr()+"__"+descripcionPrefactura+"__"+cxpx.getPrefactura()+"__"+cxpx.getCantidadAcciones()+"__"+cxpx.getContratista()); 
                st.setString(6,""+abonox);
                st.setString(7,""+abonox);
                st.setString(8,""+"11050515");
                st.setString(9,"");
                st.setString(10,loginx);
                st.setString(11,"");
                
                comando_sql = st.getSql();//JJCastro fase2
                //.out.println("comando_sql2"+comando_sql);                
                respuesta.add(comando_sql);//090730
                                            
                modificacion_cxp=obtenerSQL( update_cxp );
                st = new StringStatement (modificacion_cxp, true);//JJCastro fase2
                
                st.setString(1,""+abonox);
                st.setString(2,""+abonox);
                st.setString(3,""+abonox);
                st.setString(4,""+abonox);
                st.setString(5,cxpx.getCxcAr());
                st.setString(6,"FINV");
                st.setString(7,tercero);//090805
                st.setString(8,"FAP");
                st.setString(9,cxpx.getFacturaAr());
                
                comando_sql = st.getSql();//JJCastro fase2
                respuesta.add(comando_sql);
                
            }catch(Exception e){
                System.out.println("errorcit en CxpsArDAO:"+e.toString());        
                throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA bajar cxpar. \n " + e.toString()+"__"+e.getMessage());
            }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
            
            
            i=i+1;  
        }//fin de mq hayan filas seleccionadas sin procesar y quede plata por abonar 
        try{
            insercion_cxp=obtenerSQL( insert_cxp );//090730            
            st = new StringStatement (insercion_cxp, true);//JJCastro fase2
            st.setString(1,tercero);//090805            
            st.setString(2,"FAP");//090730            

            String doc_cxp=""+cxc_arx+"-";//090730
            if (mata_saldo_viejo){//090730
                doc_cxp=doc_cxp+"2";//090730
            }else{//090730
                doc_cxp=doc_cxp+"1";//090730
            }
            st.setString(3,""+doc_cxp);//090730
            st.setString(4,""+nombre_tercero+". "+descripcionPrefactura+". Cxps: "+conjunto_ars);//090805
            st.setString(5,"BANCOLOMBIA");//090730
            st.setString(6,"CPAG");//090730
            st.setString(7,""+abonoPrefactura1);     //090730           
            st.setString(8,""+abonoPrefactura1);  //090730           
            st.setString(9,""+abonoPrefactura1);  //090730           
            st.setString(10,""+abonoPrefactura1);  //090730           
            st.setString(11,loginx);  //090730           
            st.setString(12,""); //090730           
            st.setString(13,fecfac);
            comando_sql = st.getSql();    //090730           //JJCastro fase2
            //.out.println("comando_sql4"+comando_sql);//090730           
            respuesta.add(0,comando_sql); //090730           
        }catch(Exception ee){//090730           
            System.out.println("error metiendo cxp por ar:"+ee.toString()+"__"+ee.getMessage());//090730           
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }      //090730
        return respuesta;        //090730           
               
    }
    
}
