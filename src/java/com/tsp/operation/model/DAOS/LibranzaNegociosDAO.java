/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;

/**
 *
 * @author Harold Cuello G.
 */
public interface LibranzaNegociosDAO {
    
    public abstract JsonObject buscarLiquidacion(String codigo_negocio) throws Exception;
            
    public abstract JsonObject generarLiquidacion(JsonObject info) throws Exception;
    
    public abstract JsonObject guardarNegocio(JsonObject info);

    public JsonObject buscarFechaPago(String fecha_negocio, String dias_plazo) throws Exception ;
    
    public String calcularFechaPago(String fecha_negocio, String Pagaduria) throws Exception ;    
    
    public JsonObject validarObligaciones(String tipo, String ocupaciones, Double vlr_sol, Double oblig, int Cantoblig, String cuotas, String fianza) throws Exception ;
    
    public double obtenerValorFianza(String id_solicitud, String id_convenio, int plazo, double vlr_negocio, int id_producto, int id_cobertura,String nit_empresa) throws Exception ;
    
}
