/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author rhonalf
 */
public class GestionSolicitudAvalDAO extends MainDAO {

    private String loginuser;
    private String dstrct;

    public GestionSolicitudAvalDAO() {
        super("GestionSolicitudAvalDAO.xml");
        loginuser = "ADMIN";
        dstrct = "FINV";
    }
    public GestionSolicitudAvalDAO(String dataBaseName) {
        super("GestionSolicitudAvalDAO.xml", dataBaseName);
        loginuser = "ADMIN";
        dstrct = "FINV";
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getLoginuser() {
        return loginuser;
    }

    public void setLoginuser(String loginuser) {
        this.loginuser = loginuser;
    }

    /**
     * Genera una lista de ciudades
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listadoCiudades(String dept) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CIU";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, dept);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("codciu") + ";_;" + rs.getString("nomciu"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar ciudades: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    public String nombreCiudad(String ciu) throws Exception {
        String cadena = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "NMB_CIU";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, ciu);
            rs = ps.executeQuery();
            if (rs.next()) {
                cadena = rs.getString("nomciu");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al nombrar ciudades: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    /**
     * Genera una lista de ocupaciones
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listadoOcupaciones(String act) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_OCU";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, act);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("table_code") + ";_;" + rs.getString("dato"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar ciudades: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    /**
     * Genera una lista de departamentos
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listadoDeps() throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_DEP";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("department_code") + ";_;" + rs.getString("department_name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar departamentos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    public String nombreDept(String dept) throws Exception {
        String cadena = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "NMB_DEP";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, dept);
            rs = ps.executeQuery();
            if (rs.next()) {
                cadena = rs.getString("department_name");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al nombrar departamento: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    /**
     * Busca dato en tablagen
     * @param dato table_type a buscar
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList busquedaGeneral(String dato) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_TBG";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, dato);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("table_code") + ";_;" + rs.getString("dato"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en busqueda general: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    public SolicitudAval buscarSolicitud(int num_solicitud) throws Exception {
        SolicitudAval bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SOL";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs = ps.executeQuery();
            if (rs.next()) {
                bean = new SolicitudAval();
                bean = bean.load(rs);
                bean.setTipoConv(rs.getString("tipo"));
                bean.setCat(rs.getBoolean("cat"));
                bean.setRenovacion(rs.getString("renovacion"));
                bean.setFecha_primera_cuota("fecha_primera_cuota");
                bean.setCodNegocioRenovado(rs.getString("cod_negocio_renovado"));
                bean.setPreAprobadoMicro(rs.getString("pre_aprobado_micro"));
                bean.setVr_negocio(rs.getString("vr_negocio"));
                bean.setCompraCartera(rs.getBoolean("compra_cartera"));
                bean.setPolitica(rs.getString("politica"));
                bean.setValor_renovacion(rs.getDouble("valor_renovacion"));
                bean.setNit_fondo(rs.getString("nit_fondo"));
                bean.setProducto_fondo(rs.getInt("producto_fondo"));
                bean.setCobertura_fondo(rs.getInt("cobertura_fondo"));
                bean.setDestinoCredito(rs.getString("destino_credito"));
                
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de solicitud_aval: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de solicitud_aval: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return bean;
    }

    public SolicitudAval buscarSolicitud(String negocio) throws Exception {
        SolicitudAval bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SOL_NEG";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            if (rs.next()) {
                bean = new SolicitudAval();
                bean = bean.load(rs);
                bean.setTipoConv(rs.getString("tipo"));
                bean.setCat(rs.getBoolean("cat"));
                bean.setRenovacion(rs.getString("renovacion"));
                bean.setFecha_primera_cuota("fecha_primera_cuota");
                bean.setCodNegocioRenovado(rs.getString("cod_negocio_renovado"));
                bean.setPreAprobadoMicro(rs.getString("pre_aprobado_micro"));
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de solicitud_aval: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de solicitud_aval: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return bean;
    }

    public boolean existeSolicitud(int num_solicitud) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SOL";
        String sql = "";

        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs = ps.executeQuery();
            if (rs.next()) {

                return true;
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de existe solicitud_aval: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de existe solicitud_aval: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return false;
    }

    public ArrayList<SolicitudCuentas> buscarCuentas(int num_solicitud) throws Exception {
        ArrayList<SolicitudCuentas> lista = new ArrayList<SolicitudCuentas>();
        SolicitudCuentas cuenta = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SCU";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                cuenta = new SolicitudCuentas();
                cuenta = cuenta.load(rs);
                lista.add(cuenta);
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de cuentas: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de cuentas: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList<SolicitudReferencias> buscarReferencias(int num_solicitud, String tipo, String tipo_ref) throws Exception {
        ArrayList<SolicitudReferencias> lista = new ArrayList<SolicitudReferencias>();
        SolicitudReferencias referencia = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SRF";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            ps.setString(2, tipo);
            ps.setString(3, tipo_ref);
            rs = ps.executeQuery();
            while (rs.next()) {
                referencia = new SolicitudReferencias();
                referencia = referencia.load(rs);
                lista.add(referencia);
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de referencias: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de referencias: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList<SolicitudBienes> buscarBienes(int num_solicitud, String tipo_p) throws Exception {
        ArrayList<SolicitudBienes> lista = new ArrayList<SolicitudBienes>();
        SolicitudBienes bien = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SBI";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            ps.setString(2, tipo_p);
            rs = ps.executeQuery();
            while (rs.next()) {
                bien = new SolicitudBienes();
                bien = bien.load(rs);
                lista.add(bien);
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de bienes: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de bienes: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList<SolicitudVehiculo> buscarVehiculos(int num_solicitud) throws Exception {
        ArrayList<SolicitudVehiculo> lista = new ArrayList<SolicitudVehiculo>();
        SolicitudVehiculo vehiculo = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SVE";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                vehiculo = new SolicitudVehiculo();
                vehiculo = vehiculo.load(rs);
                lista.add(vehiculo);
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de vehiculos: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de vehiculos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }
    
    public ArrayList<SolicitudOblComprar> buscarObligaciones(int num_solicitud) throws Exception {
        ArrayList<SolicitudOblComprar> lista = new ArrayList<SolicitudOblComprar>();
        SolicitudOblComprar obligation = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_OBL";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                obligation = new SolicitudOblComprar();
                obligation = obligation.load(rs);
                lista.add(obligation);
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de las Obligaciones: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de las Obligaciones: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }    

    public SolicitudEstudiante datosEstudiante(int num_solicitud) throws Exception {
        SolicitudEstudiante estudiante = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SES";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs = ps.executeQuery();
            if (rs.next()) {
                estudiante = new SolicitudEstudiante();
                estudiante = estudiante.load(rs);
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de datos est: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de datos est: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return estudiante;
    }

    public SolicitudLaboral datosLaboral(int num_solicitud, String tipo) throws Exception {
        SolicitudLaboral laboral = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SLA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            ps.setString(2, tipo);
            rs = ps.executeQuery();
            if (rs.next()) {
                laboral = new SolicitudLaboral();
                laboral = laboral.load(rs);
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de datos persona: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de datos persona: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return laboral;
    }

    public SolicitudPersona buscarPersona(int num_solicitud, String tipo) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SPE";
        String sql = "";
        SolicitudPersona persona = null;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            ps.setString(2, tipo);
            rs = ps.executeQuery();
            if (rs.next()) {
                persona = new SolicitudPersona();
                persona = persona.load(rs);
            }
        } catch (Exception e) {
            System.out.println("Error en buscar persona: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar persona: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return persona;
    }

    public ArrayList<SolicitudHijos> buscarHijos(int num_sol, String tipo) throws Exception {
        ArrayList<SolicitudHijos> lista = new ArrayList<SolicitudHijos>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SHI";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_sol);
            ps.setString(2, tipo);
            rs = ps.executeQuery();
            while (rs.next()) {
                SolicitudHijos hijo = new SolicitudHijos();
                hijo = hijo.load(rs);
                lista.add(hijo);
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAfilsUsuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfilsUsuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public SolicitudPersona buscarPersonaId(String id) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SPE_ID";
        String sql = "";
        SolicitudPersona persona = null;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                persona = new SolicitudPersona();
                persona = persona.load(rs);
            }
        } catch (Exception e) {
            System.out.println("Error en buscar persona: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar persona: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return persona;
    }

    public String codigoCliente(String nit) throws Exception {
        String codigo = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CCL";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nit);
            rs = ps.executeQuery();
            if (rs.next()) {
                codigo = rs.getString("codcli") + ";_;"
                        + rs.getString("nombre1") + ";_;"
                        + rs.getString("nombre2") + ";_;"
                        + rs.getString("apellido1") + ";_;"
                        + rs.getString("apellido2") + ";_;";
            }
        } catch (Exception e) {
            System.out.println("Error en buscar codigo cliente: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar codigo cliente: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return codigo;
    }

    public String infoCliente(String nit) throws Exception {
        String info = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CCL_ID";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nit);
            rs = ps.executeQuery();
            if (rs.next()) {
                info = rs.getString("apellido1") + ";_;"
                        + rs.getString("apellido2") + ";_;"
                        + rs.getString("nombre1") + ";_;"
                        + rs.getString("nombre2") + ";_;"
                        + rs.getString("nombre") + ";_;"
                        + rs.getString("sexo") + ";_;"
                        + rs.getString("tipo_id") + ";_;"
                        + rs.getString("nit") + ";_;"
                        + rs.getString("direccion") + ";_;"
                        + rs.getString("telefono") + ";_;"
                        + "0";

            }
        } catch (Exception e) {
            System.out.println("Error en buscar codigo cliente: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar codigo cliente: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return info;
    }

    /**
     * Busca los datos de una identificacion en la tabla girador del esquema fenalco
     * @param id identificacion de la persona a buscar
     * @return String con los resultados obtenidos
     * @autor ivargas
     * @throws Exception
     */
    public ArrayList<String> infoGirador(String id) throws Exception {
        ArrayList<String> info = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_GIRADOR_ID";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                info.add(rs.getString("girdoccod"));
                info.add(rs.getString("girdidnom"));
                info.add(rs.getString("gircodsex"));
                info.add(rs.getString("girdidnum"));
                info.add(rs.getString("girdir"));
                info.add(rs.getString("girtel1"));
                info.add(rs.getString("girtel2"));
                info.add(rs.getString("girtelcel"));

            }
        } catch (Exception e) {
            System.out.println("Error en infoGirador: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en infoGirador: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return info;
    }

    /**
     * Busca las solicitudes de consumo que fueron devueltas desde formalizacion
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> solicitudesDevueltasRad() throws Exception {
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SRC_SOL_DEVOLU_FORMA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("form"));
                bean.setValor_02(rs.getString("negocio"));
                bean.setValor_03(rs.getString("cliente"));
                bean.setValor_04(rs.getString("aseafil"));
                bean.setValor_05(rs.getString("fecha"));
                bean.setValor_06(rs.getString("comentarios"));

                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en solicitudesDevueltasRad en GestionSolicitudAvalDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList<String> borrarSolicitud(int num_solicitud) throws Exception {
        ArrayList<String> listaSql = new ArrayList<String>();
        String query = "SQL_DELX";
        String sql = "";
        String lista [] = null;
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#num_sol", "" + num_solicitud);
            lista = sql.split(";");
            listaSql.addAll(Arrays.asList(lista));
        } catch (Exception e) {
            System.out.println("Error en borrar datos solicitud: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en borrar datos solicitud: " + e.toString());
        }
        return  listaSql;
    }

    public ArrayList<String> buscarAfilsUsuario(String id_usuario) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_PRA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id_usuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("nit_proveedor") + ";_;" + rs.getString("payment_name"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAfilsUsuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfilsUsuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public String buscarCodAfils(String afil) throws Exception {
        String cod = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_COD_AFIL";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, afil);
            rs = ps.executeQuery();
            if (rs.next()) {
                cod = rs.getString("cod_fenalco");
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAfilsUsuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfilsUsuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cod;
    }

    public boolean insertarDatos(SolicitudAval datoSolic, ArrayList<SolicitudReferencias> lista_refs,
            ArrayList<SolicitudCuentas> lista_cuentas, ArrayList<SolicitudLaboral> lista_lab,
            ArrayList<SolicitudBienes> lista_bienes, ArrayList<SolicitudVehiculo> lista_veh, ArrayList<SolicitudPersona> lista_pers, ArrayList<SolicitudHijos> lista_hij, SolicitudEstudiante estudiante,
            NegocioTrazabilidad negtraza, SolicitudNegocio solcneg,ArrayList<ObligacionesCompra> ocompraList) throws Exception {
        boolean sw = true;
        Connection con = null;
        int num_solicitud = Integer.parseInt(datoSolic.getNumeroSolicitud());
        Statement stmt = null;
        String query = "INS_SAV";
        con = conectarJNDI(query);
        con.setAutoCommit(false);
        stmt = con.createStatement();
        try {
            if (this.existeSolicitud(num_solicitud)) {
                ArrayList<String> listSql = this.borrarSolicitud(num_solicitud);
                for (String sql : listSql) {
                    stmt.addBatch(sql);
                }
                stmt.addBatch(this.updateSolicitud(datoSolic));
            } else {
                stmt.addBatch(this.insertSolicitud(datoSolic));
            }
            stmt.addBatch(this.updateAsesorPresolicitud(datoSolic));
            for (int i = 0; i < lista_veh.size(); i++) {
                stmt.addBatch(this.insertVehiculos(lista_veh.get(i)));
            }
            for (int i = 0; i < lista_bienes.size(); i++) {
                stmt.addBatch(this.insertBienes(lista_bienes.get(i)));
            }
            for (int i = 0; i < lista_refs.size(); i++) {
                stmt.addBatch(this.insertRefs(lista_refs.get(i)));
            }
            for (int i = 0; i < lista_pers.size(); i++) {
                stmt.addBatch(this.insertPersona(lista_pers.get(i)));
            }
            for (int i = 0; i < lista_lab.size(); i++) {
                stmt.addBatch(this.insertLaboral(lista_lab.get(i)));
            }
            for (int i = 0; i < lista_hij.size(); i++) {
                stmt.addBatch(this.insertHijo(lista_hij.get(i)));
            }
            if (estudiante != null) {
                stmt.addBatch(this.insertEstudiante(estudiante));
            }
            if (solcneg != null) {
                stmt.addBatch(this.insertNegocio(solcneg));
            }

            for (int i = 0; i < lista_cuentas.size(); i++) {
                stmt.addBatch(this.insertCuentas(lista_cuentas.get(i)));
            }
            
            for (int i = 0; i < ocompraList.size(); i++) {
                stmt.addBatch(this.insertObligacionesCompras(ocompraList.get(i)));                
            }

            if (!datoSolic.getEstadoSol().equals("B")) {
                NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
                stmt.addBatch(daotraza.insertNegocioTrazabilidad(negtraza));
                if (negtraza.getCodNeg()!= null){
                    stmt.addBatch(daotraza.updateActividad(negtraza.getCodNeg(), negtraza.getActividad()));
                }
            }

             if ((!datoSolic.getEstadoSol().equals("B"))&&(!datoSolic.getMod_formulario().equals(""))) {
               datoSolic.setUserUpdate(loginuser);
                stmt.addBatch(this.updateModSolicitud(datoSolic));
            }

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
        } catch (Exception e) {
            System.out.println("Error en insertarDatosnat: " + e.toString());
            sw = false;
            con.rollback();
            if (e instanceof SQLException) {
                throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + ((SQLException) e).getNextException());
            } else {
                e.printStackTrace();
            }
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }

    public String insertSolicitud(SolicitudAval solicitud) {
        String cadena = "";
        String query = "INS_SAV";
        String sql = "";
        StringStatement st = null;
        try {
            
            boolean compraCartera=solicitud.getDestinoCredito().equals("CC") || solicitud.getDestinoCredito().equals("MIX");
            
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, solicitud.getNumeroSolicitud());
            st.setString(2, solicitud.getFechaConsulta());
            st.setString(3, solicitud.getValorSolicitado());
            st.setString(4, solicitud.getAgente());
            st.setString(5, solicitud.getAfiliado());
            st.setString(6, solicitud.getCodigo());
            st.setString(7, solicitud.getEstadoSol());
            st.setString(8, solicitud.getTipoPersona());
            st.setString(9, loginuser);
            st.setString(10, dstrct);
            st.setString(11, solicitud.getIdConvenio());
            st.setString(12, solicitud.getProducto());
            st.setString(13, solicitud.getServicio());
            st.setString(14, solicitud.getCiudadMatricula());
            st.setString(15, solicitud.getValorProducto());
            st.setString(16, solicitud.getAsesor());
            st.setString(17, solicitud.getPlazo());
            st.setString(18, solicitud.getPlazoPrCuota());
            st.setString(19, solicitud.getTipoNegocio());
            st.setString(20, solicitud.getNumTipoNegocio());
            st.setString(21, solicitud.getRenovacion());
            st.setString(22, solicitud.getFecha_primera_cuota());
            st.setString(23, solicitud.getCodNegocioRenovado());
            st.setString(24, solicitud.getPreAprobadoMicro());
            st.setString(25, solicitud.getFianza());
            st.setString(26, solicitud.getAsesor());
            st.setString(27, solicitud.getIdConvenio());
            st.setBoolean(28, solicitud.isCapitalDeTrabajo());
            st.setBoolean(29, solicitud.isActivoFijo());
            //st.setBoolean(30, solicitud.isCompraCartera());
            st.setBoolean(30, compraCartera);
            st.setString(31, solicitud.getCuotaMaxima());
            st.setDouble(32, solicitud.getValor_renovacion());
            st.setString(33, solicitud.getPolitica());
            st.setString(34, solicitud.getNit_fondo());
            st.setInt(35, solicitud.getProducto_fondo());
            st.setInt(36, solicitud.getCobertura_fondo());
            st.setString(37, solicitud.getDestinoCredito());
            
            cadena = st.getSql();
            if (solicitud.getSector() == null || solicitud.getSector().equals("")) {
                cadena = cadena.replace("#SECTOR#", "null");
            } else {
                cadena = cadena.replace("#SECTOR#", "'" + solicitud.getSector() + "'");
            }

            if (solicitud.getSubsector() == null || solicitud.getSubsector().equals("")) {
                cadena = cadena.replace("#SUBSECTOR#", "null");
            } else {
                cadena = cadena.replace("#SUBSECTOR#", "'" + solicitud.getSubsector() + "'");
            }

            if (solicitud.getBanco() == null || solicitud.getBanco().equals("")) {
                cadena = cadena.replace("#BANCO#", "null");
            } else {
                cadena = cadena.replace("#BANCO#", "'" + solicitud.getBanco() + "'");
            }
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql solicitud: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String updateSolicitud(SolicitudAval solicitud) {
        String cadena = "";
        String query = "";
        if (solicitud.getEstadoSol().equals("B")) {
            query = "UPD_SAV";
        } else {
            query = "UPD_SAV2";
        }
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, solicitud.getFechaConsulta());
            st.setString(2, solicitud.getValorSolicitado());
            st.setString(3, solicitud.getCodigo());
            st.setString(4, solicitud.getEstadoSol());
            st.setString(5, solicitud.getTipoPersona());
            st.setString(6, loginuser);
            st.setString(7, solicitud.getProducto());
            st.setString(8, solicitud.getServicio());
            st.setString(9, solicitud.getCiudadMatricula());
            st.setString(10, solicitud.getValorProducto());
            st.setString(11, solicitud.getAsesor());
            if (solicitud.getEstadoSol().equals("B")) {
                st.setString(12, solicitud.getAfiliado());
                st.setString(13, solicitud.getIdConvenio());
                st.setString(14, solicitud.getPlazo());
                st.setString(15, solicitud.getPlazoPrCuota());
                st.setString(16, solicitud.getTipoNegocio());
                st.setString(17, solicitud.getNumTipoNegocio());
                st.setString(18, solicitud.getRenovacion());
                st.setString(19, solicitud.getFecha_primera_cuota());
                st.setString(20, solicitud.getCodNegocioRenovado());
                st.setString(21, solicitud.getPreAprobadoMicro());
                st.setString(22, solicitud.getFianza());
                st.setBoolean(23, solicitud.isCapitalDeTrabajo());
                st.setBoolean(24, solicitud.isActivoFijo());
                st.setBoolean(25, solicitud.isCompraCartera());
                st.setString(26, solicitud.getCuotaMaxima());
                st.setDouble(27, solicitud.getValor_renovacion());
                st.setString(28, solicitud.getPolitica());
                st.setString(29, solicitud.getNit_fondo());
                st.setInt(30, solicitud.getProducto_fondo());
                st.setInt(31, solicitud.getCobertura_fondo());
                st.setString(32, solicitud.getNumeroSolicitud());
                
            } else {
                st.setString(12, solicitud.getNumTipoNegocio());
                st.setString(13, solicitud.getPlazo());
                st.setString(14, solicitud.getTipoNegocio());
                st.setString(15, solicitud.getRenovacion());
                st.setString(16, solicitud.getFecha_primera_cuota());
                st.setString(17, solicitud.getCodNegocioRenovado());
                st.setString(18, solicitud.getPreAprobadoMicro());
                st.setString(19, solicitud.getFianza());
                st.setBoolean(20, solicitud.isCapitalDeTrabajo());
                st.setBoolean(21, solicitud.isActivoFijo());
                st.setBoolean(22, solicitud.isCompraCartera());
                st.setString(23, solicitud.getCuotaMaxima());
                st.setDouble(24, solicitud.getValor_renovacion());
                st.setString(25, solicitud.getPolitica());
                st.setString(26, solicitud.getNit_fondo());
                st.setInt(27, solicitud.getProducto_fondo());
                st.setInt(28, solicitud.getCobertura_fondo());
                st.setString(29, solicitud.getNumeroSolicitud());
                
            }
            cadena = st.getSql();
            if (solicitud.getSector() == null || solicitud.getSector().equals("")) {
                cadena = cadena.replace("#SECTOR#", "null");
            } else {
                cadena = cadena.replace("#SECTOR#", "'" + solicitud.getSector() + "'");
            }
            if (solicitud.getSubsector() == null || solicitud.getSubsector().equals("")) {
                cadena = cadena.replace("#SUBSECTOR#", "null");
            } else {
                cadena = cadena.replace("#SUBSECTOR#", "'" + solicitud.getSubsector() + "'");
            }
            if (solicitud.getBanco() == null || solicitud.getBanco().equals("")) {
                cadena = cadena.replace("#BANCO#", "null");
            } else {
                cadena = cadena.replace("#BANCO#", "'" + solicitud.getBanco() + "'");
            }
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql solicitud: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String insertLaboral(SolicitudLaboral laboral) {
        String cadena = "";
        String query = "INS_SLA";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, laboral.getNumeroSolicitud());
            st.setString(2, laboral.getTipo());
            st.setString(3, laboral.getOcupacion());
            st.setString(4, laboral.getActividadEconomica());
            st.setString(5, laboral.getNombreEmpresa());
            st.setString(6, laboral.getNit());
            st.setString(7, laboral.getDireccion());
            st.setString(8, laboral.getCiudad());
            st.setString(9, laboral.getDepartamento());
            st.setString(10, laboral.getTelefono());
            st.setString(11, laboral.getExtension());
            st.setString(12, laboral.getCargo());
            st.setString(13, laboral.getFechaIngreso());
            st.setString(14, laboral.getTipoContrato());
            st.setString(15, laboral.getSalario());
            st.setString(16, laboral.getOtrosIngresos());
            st.setString(17, laboral.getConceptoOtrosIng());
            st.setString(18, laboral.getGastosManutencion());
            st.setString(19, laboral.getGastosCreditos());
            st.setString(20, laboral.getGastosArriendo());
            st.setString(21, laboral.getCelular());
            st.setString(22, laboral.getEmail());
            st.setString(23, laboral.getEps());
            st.setString(24, laboral.getTipoAfiliacion());
            st.setString(25, loginuser);
            st.setString(26, dstrct);
            st.setString(27, laboral.getDireccionCobro());
            st.setString(28, laboral.getId_persona());

            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql laboral: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String insertHijo(SolicitudHijos hijo) {
        String cadena = "";
        String query = "INS_SHI";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, hijo.getNumeroSolicitud());
            st.setString(2, hijo.getTipo());
            st.setString(3, hijo.getSecuencia());
            st.setString(4, hijo.getNombre());
            st.setString(5, hijo.getDireccion());
            st.setString(6, hijo.getTelefono());
            st.setString(7, hijo.getEdad());
            st.setString(8, hijo.getEmail());
            st.setString(9, loginuser);
            st.setString(10, dstrct);
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql laboral: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String insertCuentas(SolicitudCuentas cuenta) {
        String cadena = "";
        String query = "INS_SCU";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, cuenta.getNumeroSolicitud());
            st.setString(2, cuenta.getConsecutivo());
            st.setString(3, cuenta.getTipo());
            st.setString(4, cuenta.getBanco());
            st.setString(5, cuenta.getCuenta());
            st.setString(6, cuenta.getFechaApertura());
            st.setString(7, cuenta.getNumeroTarjeta());
            st.setString(8, loginuser);
            st.setString(9, dstrct);
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql cuentas: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String insertRefs(SolicitudReferencias referencia) {
        String cadena = "";
        String query = "INS_SRE";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, referencia.getNumeroSolicitud());
            st.setString(2, referencia.getTipo());
            st.setString(3, referencia.getTipoReferencia());
            st.setString(4, referencia.getSecuencia());
            st.setString(5, referencia.getNombre());
            st.setString(6, referencia.getPrimerApellido());
            st.setString(7, referencia.getSegundoApellido());
            st.setString(8, referencia.getPrimerNombre());
            st.setString(9, referencia.getSegundoNombre());
            st.setString(10, referencia.getTelefono());
            st.setString(11, referencia.getTelefono2());
            st.setString(12, referencia.getExtension());
            st.setString(13, referencia.getCelular());
            st.setString(14, referencia.getCiudad());
            st.setString(15, referencia.getDepartamento());
            st.setString(16, referencia.getTiempoConocido());
            st.setString(17, referencia.getParentesco());
            st.setString(18, referencia.getEmail());
            st.setString(19, referencia.getDireccion());
            st.setString(20, loginuser);
            st.setString(21, dstrct);
            st.setString(22, referencia.getReferenciaComercial());
            cadena = st.getSql();
            System.out.println(cadena);
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql referencias: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String insertBienes(SolicitudBienes bien) {
        String cadena = "";
        String query = "INS_SBI";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, bien.getNumeroSolicitud());
            st.setString(2, bien.getTipo());
            st.setString(3, bien.getSecuencia());
            st.setString(4, bien.getTipoBien());
            st.setString(5, bien.getHipoteca());
            st.setString(6, bien.getaFavorDe());
            st.setString(7, bien.getValorComercial());
            st.setString(8, bien.getDireccion());
            st.setString(9, loginuser);
            st.setString(10, dstrct);
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String insertVehiculos(SolicitudVehiculo vehiculo) {
        String cadena = "";
        String query = "INS_SVE";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, vehiculo.getNumeroSolicitud());
            st.setString(2, vehiculo.getTipo());
            st.setString(3, vehiculo.getSecuencia());
            st.setString(4, vehiculo.getMarca());
            st.setString(5, vehiculo.getTipoVehiculo());
            st.setString(6, vehiculo.getPlaca());
            st.setString(7, vehiculo.getModelo());
            st.setString(8, vehiculo.getValorComercial());
            st.setString(9, vehiculo.getCuotaMensual());
            st.setString(10, vehiculo.getPignoradoAFavorDe());
            st.setString(11, loginuser);
            st.setString(12, dstrct);
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql vehiculos: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String insertObligaciones(SolicitudOblComprar obligacion) {
        String cadena = "";
        String query = "INS_OBLIGACIONES";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            
            st.setString(1, obligacion.getNumero_solicitud());
            st.setString(2, obligacion.getSecuencia());
            st.setString(3, obligacion.getEntidad());
            st.setString(4, obligacion.getNit_proveedor());
            st.setString(5, obligacion.getTipo_cuenta());
            st.setString(6, obligacion.getNumero_cuenta());
            st.setString(7, obligacion.getValor_comprar());
            st.setString(8, loginuser);
            st.setString(9, dstrct);
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql vehiculos: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }    
    
    public String insertPersona(SolicitudPersona persona) {
        String cadena = "";
        String query = "INS_SPE";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);

            st.setString(1, persona.getNumeroSolicitud());
            st.setString(2, persona.getTipoPersona());
            st.setString(3, persona.getTipo());
            st.setString(4, persona.getCodcli());
            st.setString(5, persona.getIdentificacion());
            st.setString(6, persona.getTipoId());
            st.setString(7, persona.getFechaExpedicionId());
            st.setString(8, persona.getCiudadExpedicionId());
            st.setString(9, persona.getDptoExpedicionId());
            st.setString(10, persona.getFechaNacimiento());
            st.setString(11, persona.getCiudadNacimiento());
            st.setString(12, persona.getDptoNacimiento());
            st.setString(13, persona.getNivelEstudio());
            st.setString(14, persona.getProfesion());
            st.setString(15, persona.getPersonasACargo());
            st.setString(16, persona.getNumHijos());
            st.setString(17, persona.getTotalGrupoFamiliar());
            st.setString(18, persona.getEstrato());
            st.setString(19, persona.getTiempoResidencia());
            st.setString(20, persona.getPrimerApellido());
            st.setString(21, persona.getSegundoApellido());
            st.setString(22, persona.getPrimerNombre());
            st.setString(23, persona.getSegundoNombre());
            st.setString(24, persona.getCiiu());
            st.setString(25, persona.getFax());
            st.setString(26, persona.getTipoEmpresa());
            st.setString(27, persona.getFechaConstitucion());
            st.setString(28, persona.getRepresentanteLegal());
            st.setString(29, persona.getGeneroRepresentante());
            st.setString(30, persona.getTipoIdRepresentante());
            st.setString(31, persona.getIdRepresentante());
            st.setString(32, persona.getFirmadorCheques());
            st.setString(33, persona.getGeneroFirmador());
            st.setString(34, persona.getTipoIdFirmador());
            st.setString(35, persona.getIdFirmador());
            st.setString(36, persona.getTelefono2());
            st.setString(37, persona.getPrimerApellidoCony());
            st.setString(38, persona.getSegundoApellidoCony());
            st.setString(39, persona.getPrimerNombreCony());
            st.setString(40, persona.getSegundoNombreCony());
            st.setString(41, persona.getTipoIdentificacionCony());
            st.setString(42, persona.getIdentificacionCony());
            st.setString(43, persona.getEmpresaCony());
            st.setString(44, persona.getDireccionEmpresaCony());
            st.setString(45, persona.getTelefonoCony());
            st.setString(46, persona.getCargoCony());
            st.setString(47, persona.getSalarioCony());
            st.setString(48, persona.getCelularCony());
            st.setString(49, persona.getEmailCony());
            st.setString(50, persona.getCiudad());
            st.setString(51, persona.getDepartamento());
            st.setString(52, persona.getGenero());
            st.setString(53, persona.getEmail());
            st.setString(54, persona.getEstadoCivil());
            st.setString(55, persona.getDireccion());
            st.setString(56, persona.getBarrio());
            st.setString(57, persona.getTipoVivienda());
            st.setString(58, persona.getTelefono());
            st.setString(59, persona.getCelular());
            st.setString(60, persona.getNombre());
            st.setString(61, loginuser);
            st.setString(62, dstrct);
            st.setBoolean(63, persona.isEnviarExtractoCorrespondecia());
            st.setBoolean(64, persona.isEnviarExtractoEmail());
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql persona: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String insertEstudiante(SolicitudEstudiante estudiante) {
        String cadena = "";
        String query = "INS_SES";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, estudiante.getNumeroSolicitud());
            st.setString(2, estudiante.getParentescoGirador());
            st.setString(3, estudiante.getUniversidad());
            st.setString(4, estudiante.getPrograma());
            st.setString(5, estudiante.getFechaIngresoPrograma());
            st.setString(6, estudiante.getCodigo());
            st.setString(7, estudiante.getSemestre());
            st.setString(8, estudiante.getValorSemestre());
            st.setString(9, estudiante.getTipoCarrera());
            st.setString(10, estudiante.getTrabaja());
            st.setString(11, estudiante.getNombreEmpresa());
            st.setString(12, estudiante.getDireccionEmpresa());
            st.setString(13, estudiante.getTelefonoEmpresa());
            st.setString(14, estudiante.getSalario());
            st.setString(15, loginuser);
            st.setString(16, dstrct);
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql estudiante: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public void insertDocumentos(int num_solicitud, ArrayList<SolicitudDocumentos> lista) throws Exception {
        String query = "INS_SDO";
        Connection con = null;
        Statement stmt = null;
        con = conectarJNDI(query);
        con.setAutoCommit(false);
        stmt = con.createStatement();
        try {
            stmt.addBatch(this.borrarDatosDocs(num_solicitud));
            for (int i = 0; i < lista.size(); i++) {
                SolicitudDocumentos b = lista.get(i);
                stmt.addBatch(this.insertDocs(b));
            }

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
        } catch (Exception e) {
            System.out.println("Error en insertDocumentos: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en insertDocumentos: " + e.toString());
        } finally {
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
    }

    public void updateDocumentos(int num_solicitud, ArrayList<SolicitudDocumentos> lista) throws Exception {
        String query = "UPD_SDO";
        Connection con = null;
        Statement stmt = null;
        con = conectarJNDI(query);
        con.setAutoCommit(false);
        stmt = con.createStatement();
        try {
            for (int i = 0; i < lista.size(); i++) {
                SolicitudDocumentos b = lista.get(i);
                stmt.addBatch(this.updateDocs(num_solicitud, b.getNumTitulo(), b.getFecha()));
            }

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
        } catch (Exception e) {
            System.out.println("Error en updateDocumentos: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en updateDocumentos: " + e.toString());
        } finally {
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
    }

    public String insertDocs(SolicitudDocumentos sd) {
        String cadena = "";
        String query = "INS_SDO";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setInt(1, Integer.parseInt(sd.getNumeroSolicitud()));
            st.setString(2, sd.getNumTitulo());
            st.setDouble(3, Double.parseDouble(sd.getValor()));
            st.setString(4, sd.getFecha());
            st.setString(5, sd.getEstIndemnizacion());
            st.setString(6, loginuser);
            st.setString(7, dstrct);

            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql docs: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String updateDocs(int num_solicitud, String num_titulo, String fecha) {
        String cadena = "";
        String query = "UPD_SDO";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, num_titulo);
            st.setString(2, loginuser);
            st.setInt(3, num_solicitud);
            st.setString(4, fecha);
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql docs: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public ArrayList<SolicitudDocumentos> buscarDocsSolicitud(int num_solicitud) throws Exception {
        ArrayList<SolicitudDocumentos> lista = new ArrayList<SolicitudDocumentos>();
        String query = "SRC_SDO";
        String sql = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                SolicitudDocumentos documento = null;
                documento = new SolicitudDocumentos();
                documento = documento.load(rs);
                lista.add(documento);
            }
        } catch (Exception e) {
            System.out.println("Error en insertDocumentos: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en insertDocumentos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public String borrarDatosDocs(int num_solicitud) {
        String cadena = "";
        String query = "DEL_SDO";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setInt(1, num_solicitud);
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql del_docs: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public String updateSolicitudDocs(int num_solicitud, String ciudad_cheque, String tipo_negocio, String num_tipo_neg, String banco, String sucursal, String num_chequera) {
        String cadena = "";
        String query = "UPD_SDA";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, ciudad_cheque);
            st.setString(2, tipo_negocio);
            st.setString(3, num_tipo_neg);
            st.setString(4, sucursal);
            st.setString(5, num_chequera);
            st.setString(6, loginuser);
            st.setInt(7, num_solicitud);
            cadena = st.getSql();
            if (banco.equals("0") || banco.equals("")) {
                cadena = cadena.replaceAll("_BANCO_", "null");
            } else {
                cadena = cadena.replaceAll("_BANCO_", "'" + banco + "'");
            }
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql upd_docs: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public void actualizarSolicDoc(int num_solicitud, String ciudad_cheque, String tipo_negocio, String num_tipo_negocio, String banco, String sucursal, String num_chequera) throws Exception {
        String query = "UPD_SDA";
        String sql = "";
        Connection con = null;
        PreparedStatement ps = null;
        try {
            sql = this.updateSolicitudDocs(num_solicitud, ciudad_cheque, tipo_negocio, num_tipo_negocio, banco, sucursal, num_chequera);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            int k = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en actualizarSolicDoc: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en actualizarSolicDoc: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
    }

    public String nombreAfiliado(String nit) throws Exception {
        String nombre = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSC_AFIL";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nit);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombre = rs.getString("payment_name");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return nombre;
    }

    public String nombreTablagen(String table_type, String table_code) throws Exception {
        String nombre = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "NOM_TBG";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, table_type);
            ps.setString(2, table_code);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombre = rs.getString("dato");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return nombre;
    }

    public String refTablagen(String table_type, String table_code) throws Exception {
        String nombre = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "REF_TBG";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, table_type);
            ps.setString(2, table_code);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombre = rs.getString("referencia");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return nombre;
    }

    public String NumeroSolc() throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "UP_SERIES";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                if (rs.next()) {
                    ret = rs.getString(1);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return ret;
    }

    /**
     * Busca la informacion del formulario para un afiliado
     * @param nitprov nit del proveedor
     * @param idUsuario id del usuario
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> obtenerFormProv(String nitprov) throws Exception {
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "FORM_PROVEEDOR";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, nitprov);
            //        st.setString(2, idUsuario);
            rs = st.executeQuery();
            while (rs.next()) {
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("identificacion"));
                bean.setValor_02(rs.getString("nombre"));
                bean.setValor_03(rs.getString("numero_solicitud"));
                bean.setValor_04(rs.getString("valor_solicitado"));
                bean.setValor_05(rs.getString("id_convenio"));
                bean.setValor_06(rs.getString("nom_convenio"));
                bean.setValor_07(rs.getString("factura_tercero"));
                bean.setValor_08(rs.getString("nit_tercero"));
                bean.setValor_09(rs.getString("plazo"));
                bean.setValor_10(rs.getString("plazo_pr_cuota"));
                bean.setValor_11(rs.getString("tipo_negocio"));
                bean.setValor_12(rs.getString("cod_sector"));
                bean.setValor_13(rs.getString("cod_subsector"));
                bean.setValor_14(rs.getString("sector"));
                bean.setValor_15(rs.getString("subsector"));
                bean.setValor_16(rs.getString("genera_remesa"));
                bean.setValor_17(rs.getString("id_prov_convenio"));
                bean.setValor_18(rs.getString("fianza"));
                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en obtenerFormProv en GestionSolicitudAvalDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public SolicitudAval buscarSolicitud(int num_solicitud, String usuario) throws Exception {
        SolicitudAval bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SOL_USUA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            ps.setString(2, usuario);
            rs = ps.executeQuery();
            if (rs.next()) {
                bean = new SolicitudAval();
                bean = bean.load(rs);
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de solicitud_aval por usuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de solicitud_aval por usuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return bean;
    }

    public ArrayList<String> buscarAfils() throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_AFILIADOS";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("nit") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAfilsUsuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfilsUsuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList<String> buscarConveniosAfil(String id_usuario, String afil) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CONVENIOS_AFILIADO_USUA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id_usuario);
            ps.setString(2, afil);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("id_convenio") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAfilsUsuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfilsUsuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList<String> buscarConveniosAfil(String afil) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CONVENIOS_AFILIADO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, afil);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("id_convenio") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAfilsUsuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfilsUsuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public boolean isAvalTercero(int id_convenio) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_AVAL_TERCERO";
        String sql = "";
        boolean aval_tercero = false;

        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, id_convenio);
            rs = ps.executeQuery();
            if (rs.next()) {

                aval_tercero = rs.getBoolean("aval_tercero");
            }
        } catch (Exception e) {
            System.out.println("Error en isAvalTercero(int id_convenio) GestionSolicitudAval: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en isAvalTercero(int id_convenio) GestionSolicitudAval: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return aval_tercero;
    }

    /**
     * Busca dato en tablagen
     * @param table_type table_type a buscar
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList ListaTablaGen(String table_type) throws Exception {
        ArrayList lista = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_TABLAGEN";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, table_type);
            rs = ps.executeQuery();
            while (rs.next()) {
                BeanGeneral bean = new BeanGeneral();
                bean.setValor_01(rs.getString("table_code"));
                bean.setValor_02(rs.getString("descripcion"));
                bean.setValor_03(rs.getString("referencia"));
                bean.setValor_04(rs.getString("dato"));

                lista.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en listaTablaGen GestionSolicitudAvsl: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public String buscarNumSolicitud(String cod_neg) throws Exception {
        String cod = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_NUM_SOLC";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, cod_neg);
            rs = ps.executeQuery();
            if (rs.next()) {
                cod = rs.getString("numero_solicitud");
            }
        } catch (Exception e) {
            System.out.println("Error en buscarNumSolicitud: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarNumSolicitud: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cod;
    }

    public String nombreAsesor(String login) throws Exception {
        String nombre = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_NOMBRE_ASESOR";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, login);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombre = rs.getString("nombre");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return nombre;
    }

    public String insertNegocio(SolicitudNegocio negocio) {
        String cadena = "";
        String query = "INS_SOLICITUD_NEGOCIO";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, negocio.getNumeroSolicitud());
            st.setString(2, negocio.getNombre());
            st.setString(3, negocio.getDireccion());
            st.setString(4, negocio.getDepartamento());
            st.setString(5, negocio.getCiudad());
            st.setString(6, negocio.getBarrio());
            st.setString(7, negocio.getTelefono());
            st.setString(8, negocio.getNumExpNeg());
            st.setString(9, negocio.getTiempoLocal());
            st.setString(10, negocio.getTiempoMicroempresario());
            st.setString(11, negocio.getNumTrabajadores());
            st.setString(12, loginuser);
            st.setString(13, dstrct);
            st.setString(14, negocio.getTieneSocios());
            st.setString(15, negocio.getPorcentajeParticipacion());
            st.setString(16, negocio.getNombreSocio());
            st.setString(17, negocio.getCedulaSocio());
            st.setString(18, negocio.getDireccionSocio());
            st.setString(19, negocio.getTelefonoSocio());
            st.setString(20, negocio.getTipoActividad());
            st.setString(21, negocio.getLocal());
            st.setString(22, negocio.getTieneCamaraComercio());
            st.setString(23, negocio.getTipoNegocio());
            st.setString(24, negocio.getActivos());
            st.setString(25, negocio.getPasivos());
            
            cadena = st.getSql();
            if (negocio.getSector().equals("")) {
                cadena = cadena.replaceAll("_SECTOR_", "null");
            } else {
                cadena = cadena.replaceAll("_SECTOR_", "'" + negocio.getSector() + "'");
            }
            if (negocio.getSubsector().equals("")) {
                cadena = cadena.replaceAll("_SUBSECTOR_", "null");
            } else {
                cadena = cadena.replaceAll("_SUBSECTOR_", "'" + negocio.getSubsector() + "'");
            }

            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql negocio: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    public SolicitudNegocio buscarNegocio(int num_solicitud) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SOLICITUD_NEGOCIO";
        String sql = "";
        SolicitudNegocio negocio = null;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs = ps.executeQuery();
            if (rs.next()) {
                negocio = new SolicitudNegocio();
                negocio = negocio.load(rs);
            }
        } catch (Exception e) {
            System.out.println("Error en buscar negocio: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar negocio: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return negocio;
    }

    /**
     * Busca las solicitudes de consumo que fueron devueltas
     * @param usuario id del usuario
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> solicitudesDevueltasConsumo(String usuario) throws Exception {
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SRC_SOL_DEVOLU_CONSUMO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, usuario);
            rs = st.executeQuery();
            while (rs.next()) {
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("form"));
                bean.setValor_02(rs.getString("negocio"));
                bean.setValor_03(rs.getString("cliente"));
                bean.setValor_04(rs.getString("afiliado"));
                bean.setValor_05(rs.getString("fecha"));
                bean.setValor_06(rs.getString("comentarios"));

                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en obtenerFormProv en GestionSolicitudAvalDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    /**
     * Busca las solicitudes de microcredito que fueron devueltas
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> solicitudesDevueltasMicrocredito() throws Exception {
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SRC_SOL_DEVOLU_MICRO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("form"));
                bean.setValor_02(rs.getString("negocio"));
                bean.setValor_03(rs.getString("cliente"));
                bean.setValor_04(rs.getString("asesor"));
                bean.setValor_05(rs.getString("fecha"));
                bean.setValor_06(rs.getString("comentarios"));

                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en solicitudesDevueltasMicrocredito en GestionSolicitudAvalDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public SolicitudLaboral buscarLaboralId(String id) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_LABORAL_ID";
        String sql = "";
        SolicitudLaboral laboral = null;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                laboral = new SolicitudLaboral();
                laboral = laboral.load(rs);
            }
        } catch (Exception e) {
            System.out.println("Error en buscarLaboralId: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarLaboralId: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return laboral;
    }

    /**
     * consulta si un cheque fue girado
     * @param cuenta
     * @param numero_titulo
     * @return boolean
     * @throws Exception Cuando hay error
     */
    public boolean chequeGirado(String cuenta, String num_titulo) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CHEQUE_GIRADO";
        String sql = "";

        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, cuenta);
            ps.setString(2, num_titulo);
            rs = ps.executeQuery();
            if (rs.next()) {

                return true;
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de chequeGirado: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de chequeGirado: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return false;
    }

    /**
     * trae sql que actualiza la comision de aval de un documento de solicitud.
     * @param objeto de SolicitudDocumento
     * @return Sql
     * @throws Exception Cuando hay error
     */
    public String updateComisionAvalDoc(SolicitudDocumentos soldoc) {
        String cadena = "";
        String query = "UPD_COMISION_AVAL_DOC";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setDouble(1, soldoc.getComisionAval());
            st.setString(2, soldoc.getUserUpdate());
            st.setString(3, soldoc.getNumeroSolicitud());
            st.setString(4, soldoc.getNumTitulo());
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql docs: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    /**
     * trae sql que actualiza las devoluciones de un documento de solicitud.
     * @param objeto de SolicitudDocumento
     * @return Sql
     * @throws Exception Cuando hay error
     */
    public String updateDevueltoDocs(SolicitudDocumentos soldoc) {
        String cadena = "";
        String query = "UPD_DEVUELTO_DOC";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setDouble(1, soldoc.getDevuelto());
            st.setString(2, soldoc.getUserUpdate());
            st.setString(3, soldoc.getNumeroSolicitud());
            st.setString(4, soldoc.getNumTitulo());
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql docs: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    /**
     * trae sql que actualiza el estado de indemnizacion de un documento de solicitud.
     * @param objeto de SolicitudDocumento
     * @return Sql
     * @throws Exception Cuando hay error
     */
    public String updateEstIndDocs(SolicitudDocumentos soldoc) {
        String cadena = "";
        String query = "UPD_EST_INDEM_DOC";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, soldoc.getEstIndemnizacion());
            st.setString(2, soldoc.getUserUpdate());
            st.setString(3, soldoc.getNumeroSolicitud());
            st.setString(4, soldoc.getNumTitulo());
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql docs: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }
    
    /**

     * clona una solicitud
     * @param idsolicitud
     * @return Sql
     * @throws Exception Cuando hay error
     */
        public String ClonarSolicitud(String solicitud ,String cod_neg,double valor_negocio,String nueva_solicitud) throws Exception{
        String cadena = "";
        String query = "CLONAR_DATOS_FORMULARIO";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);

            cadena = st.getSql();

            cadena = cadena.replace("#1", solicitud);
            cadena = cadena.replace("#2", cod_neg);
            cadena = cadena.replace("#3", String.valueOf(valor_negocio));
            cadena = cadena.replace("#4", nueva_solicitud);
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error al generar sql docs: "+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }



        /**
     * clona una solicitud
     * @param idsolicitud
     * @return Sql
     * @throws Exception Cuando hay error
     */
        public String ClonarDetalleSolicitud(String solicitud ,String nueva_solicitud) throws Exception{
        String cadena = "";
        String query = "CLONAR_DATOS_DETALLES_FORMULARIO";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            cadena = st.getSql();

            cadena = cadena.replace("#1", solicitud);
            cadena = cadena.replace("#2", nueva_solicitud);
        }
        catch (Exception e) {
            System.out.println("Error al generar sql docs: "+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

                 /**

     * marcar una solicitud con el negocio y convenio
     * @param idsolicitud
     * @return Sql
     * @throws Exception Cuando hay error
     */
        public String MarcarSolicitud(String solicitud ,String cod_neg,String idconvenio) throws Exception{
        String cadena = "";
        String query = "MARCAR_FORMULARIO";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);

            cadena = st.getSql();

            cadena = cadena.replace("#1", cod_neg);
            cadena = cadena.replace("#2", idconvenio);
            cadena = cadena.replace("#3", solicitud);
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error al generar sql docs: "+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }



        public boolean ValidarSolicitudCliente(String id) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "VALIDA_SOLICITUD_CLIENTE";
        String sql = "";
        boolean sw=true;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw=false;
            }
        } catch (Exception e) {
            System.out.println("Error en buscar persona: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar persona: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }



         public String updateModSolicitud(SolicitudAval solicitud) {
        String query = "ACTUALIZAR_MODIFICACIONES_FORM";

        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, solicitud.getUserUpdate());
            st.setString(2, solicitud.getMod_formulario());
            st.setString(3, solicitud.getNumeroSolicitud());
            sql = st.getSql();


            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql solicitud: " + e.toString());
            e.printStackTrace();
        }
        return sql;
    }
         
   /**
     * Busca las solicitudes de multiservicios que fueron devueltas
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> solicitudesDevueltasMultiservicio() throws Exception {
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SRC_SOL_DEVOLU_MULTI";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("form"));
                bean.setValor_02(rs.getString("negocio"));
                bean.setValor_03(rs.getString("cliente"));
                bean.setValor_04(rs.getString("asesor"));
                bean.setValor_05(rs.getString("fecha"));
                bean.setValor_06(rs.getString("comentarios"));

                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en solicitudesDevueltasMicrocredito en GestionSolicitudAvalDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }
    
 /**
     * Valida si una solicitud fue  devuelta
     * @param numero_solicitud numero de solicitud
     * @param act_actual actividad actual del negocio
     * @param act_anterior actividad anterior del negocio
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public boolean ValidarSolicitudDevuelta(String numero_solicitud,String act_actual,String act_anterior) throws Exception {
        boolean sw =false;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "VALIDA_SOL_DEVUELTA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);           
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, numero_solicitud);
            st.setString(2, act_actual);
            st.setString(3, act_anterior);
            rs = st.executeQuery();
            if (rs.next()) {
                sw=true;
            }
        } catch (Exception e) {
            throw new Exception("Error en ValidarSolicitudDevuelta en GestionSolicitudAvalDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error ValidarSolicitudDevuelta el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }



  /**
     * lista las solicitudes devueltas
     * @param act_actual actividad actual del negocio
     * @param act_anterior actividad anterior del negocio
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> SolicitudesDevueltas(String act_actual,String act_anterior) throws Exception {
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "LISTAR_SOL_DEVUELTAS";
        String sql = "";
        try {
             lista = new ArrayList<BeanGeneral>();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, act_actual);
            st.setString(2, act_anterior);
            rs = st.executeQuery();
           while (rs.next()) {
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("form"));
                bean.setValor_02(rs.getString("negocio"));
                bean.setValor_03(rs.getString("cliente"));
                bean.setValor_04(rs.getString("aseafil"));
                bean.setValor_05(rs.getString("fecha"));
                bean.setValor_06(rs.getString("comentarios"));
                bean.setValor_07(rs.getString("tipo"));

                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en ValidarSolicitudDevuelta en GestionSolicitudAvalDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error ValidarSolicitudDevuelta el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }
    
    /**
     * lista las solicitudes devueltas
     * @param act_actual actividad actual del negocio
     * @param act_anterior actividad anterior del negocio
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> getListarPreaprobados() throws Exception {
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "LISTAR_SOL_PREAPROBADOS";
        String sql = "";
        try {
             lista = new ArrayList<BeanGeneral>();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
           while (rs.next()) {
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("form"));
                bean.setValor_02(rs.getString("negocio"));
                bean.setValor_03(rs.getString("cliente"));
                bean.setValor_04(rs.getString("aseafil"));
                bean.setValor_05(rs.getString("fecha"));
                bean.setValor_06(rs.getString("comentarios"));
                bean.setValor_07(rs.getString("tipo"));

                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en ValidarSolicitudDevuelta en GestionSolicitudAvalDAO.java: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("Error ValidarSolicitudDevuelta el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }
    
    
    
   
    public boolean buscarReferencias(int num_solicitud, String tipo, String tipo_ref,String secuencia) throws Exception {
        boolean sw=false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SRF_SECUENCIA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            ps.setString(2, tipo);
            ps.setString(3, tipo_ref);
            ps.setString(4, secuencia);
            rs = ps.executeQuery();
            if (rs.next()) {
               sw=true;
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de referencias: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de referencias: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }
    
    public String getUltimaSolicitud(String cedula) throws Exception {
        String numero_solicitud="";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_ULTIMA_SOLICITUD";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, cedula);
            rs = ps.executeQuery();
            if (rs.next()) {
               numero_solicitud=rs.getString("numero_solicitud");
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de SRC_ULTIMA_SOLICITUD: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de SRC_ULTIMA_SOLICITUD: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return numero_solicitud;
    }


        public boolean ValidarClientePreaprobado(String id) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "VALIDA_SOLICITUD_CLIENTE_PREAPROBADO";
        String sql = "";
        boolean sw=false;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw=true;
            }
        } catch (Exception e) {
            System.out.println("Error en buscar persona: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar persona: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }
        
    public boolean ValidarClienteSaltaData(String id) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "VALIDA_CLIENTE_SALTA_DATA";
        String sql = "";
        boolean sw=false;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw=true;
            }
        } catch (Exception e) {
            System.out.println("Error en buscar persona: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar persona: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }    

    public ArrayList<BeanGeneral> verificarRenovacion(String identificacion) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        BeanGeneral bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICA_RENOVACION";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, identificacion);
           // ps.setString(2, numero_sol);
            rs = ps.executeQuery();
            if(rs.next()) {
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("vr_negocio"));
                bean.setValor_02(rs.getString("nro_docs"));
                bean.setValor_03(rs.getString("negasoc"));
                bean.setValor_04(rs.getString("capital"));
                bean.setValor_05(rs.getString("aldia"));
                bean.setValor_06(rs.getString("fecha_pc"));
                bean.setValor_07(rs.getString("docPagos"));
                lista.add(bean);
}
        } catch (Exception e) {
            System.out.println("Error en buscarAfilsUsuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfilsUsuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    
     public boolean isRenovacion(String numero_sol) throws Exception {
        boolean sw = false;
        BeanGeneral bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_IS_RENOVACION";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, numero_sol);
            rs = ps.executeQuery();
            if (rs.next()) {

                if (rs.getString("renovacion").equals("S")) {

                    sw = true;
                }
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAfilsUsuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfilsUsuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }
     
     
     public String ultimaFecha(String cod_neg) throws Exception {
     
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_FECHA_ULTIMO_PAGO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, cod_neg);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("fecha");                
            }
        } catch (Exception e) {
            System.out.println("Error en buscarAfilsUsuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfilsUsuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return "";
    }
     
      public String cargarVias(String codciu) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_VIAS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();

        try {

            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codciu);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre"));
            }

            respuestaJson = gson.toJson(obj);

        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
            }


            return respuestaJson;
        }

    }
      
    public String cargarNomenclaturas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();       
        Gson gson = new Gson();        
        String query = "SQL_BUSCAR_NOMENCLATURAS";
        String filtro = "";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            ps = con.prepareStatement(query);          
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("is_default", rs.getString("is_default"));            
                fila.addProperty("reg_status", rs.getString("reg_status"));
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarNomenclaturas " + e.toString());
            } catch (Exception ex) {               
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
      
    public String cargarNomenclaturas(String codciu,Boolean showRel) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();       
        Gson gson = new Gson();        
        String query = (showRel) ? "SQL_BUSCAR_NOMENCLATURAS_REL" : "SQL_BUSCAR_NOMENCLATURAS_POR_ASOC_CIUDAD";
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);  
            ps.setString(1, codciu);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                if (showRel) {
                    fila.addProperty("ciudad", rs.getString("ciudad"));
                } else {
                    fila.addProperty("is_default", rs.getString("is_default"));
                }
                fila.addProperty("reg_status", rs.getString("reg_status"));
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarNomenclaturas " + e.toString());
            } catch (Exception ex) {               
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
    
     public String insertarNomenclatura(String nombre, String descripcion, String is_default, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;      
        String sql = "";
        String query = "insertarNomenclatura";
        String  respuestaJson = "{}";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, is_default);
            ps.setString(5, usuario.getLogin());
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new Exception("Error en insertarNomenclatura " + e.toString());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }
     
    public String actualizarNomenclatura(String idNomenclatura, String nombre, String descripcion, String is_default, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;      
        String sql = "";
        String query = "actualizarNomenclatura";
        String  respuestaJson = "{}";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, is_default);
            ps.setString(4, usuario.getLogin());
            ps.setInt(5, Integer.parseInt(idNomenclatura));
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new Exception("Error en actualizarNomenclatura " + e.toString());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
        
    }
    
    public boolean existeNomenclaturaRelCiudad(int idNomenclatura, String codciu) {
        Connection con = null;
        PreparedStatement ps = null;  
        ResultSet rs = null;
        boolean resp = false;       
        String query = "existeNomenclaturaRelCiudad";
        String filtro = (!codciu.equals("")) ? " AND ciudad = '"+codciu+"'": "";
        try{         
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, idNomenclatura);
            rs = ps.executeQuery();
            
            if (rs.next()){
                resp = true;
            }
        }catch (Exception e) {
            try {
                throw new Exception("Error en existeNomenclaturaRelCiudad " + e.toString());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
         return resp;
    }
    
    
       public String cambiaEstadoNomenclatura(String idNomenclatura, String estado, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;      
        String sql = "";
        String query = "cambiarEstadoNomenclatura";
         String  respuestaJson = "{}";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, estado);          
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(idNomenclatura));
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new Exception("Error en cambiarEstadoNomenclatura " + e.toString());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
        
    }
     
    public String insertarRelNomenclaturaDireccion(String idNomenclatura, String ciudad, Usuario usuario) {
        Connection con = null;
        StringStatement st = null;
        ResultSet rs = null;
        String sql = "";
        String query = "insertarRelNomenclaturaDir";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            st = new StringStatement(query, true);
            st.setString(1, usuario.getDstrct());
            st.setInt(2, Integer.parseInt(idNomenclatura));            
            st.setString(3, ciudad);
            st.setString(4, usuario.getLogin());
            
            sql = st.getSql();
            
        }catch (Exception e) {
            try {
                throw new Exception("Error en insertarRelNomenclaturaDireccion " + e.toString());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
            }
        }
         return sql;
    }
   
    public String eliminarRelNomenclaturaDireccion(String id) {
        Connection con = null;
        StringStatement st = null;
        String query = "eliminarRelNomenclaturaDir";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setInt(1, Integer.parseInt(id));           

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR eliminarRelNomenclaturaDireccion \n" + e.getMessage());
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }               
            } catch (SQLException ex) {
            }
            return sql;
        }
    }
    
    /**
     * Genera una lista de las nomenclaturas por defecto
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listadoNomenclaturasDefault() throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_NOMENCLATURAS";
        String filtro = " WHERE reg_status = '' AND is_default = 'S'";
        String sql = "";
        try {
            sql = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);          
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar nomenclaturas: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }
    
    
    public String insertRefsLibranza(SolicitudReferencias referencia) {
        String cadena = "";
        String query = "INS_SRE_LB";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, referencia.getNumeroSolicitud());
            st.setString(2, referencia.getTipo());
            st.setString(3, referencia.getTipoReferencia());
            st.setString(4, referencia.getSecuencia());
            st.setString(5, referencia.getNombre());
            st.setString(6, referencia.getPrimerApellido());
            st.setString(7, referencia.getSegundoApellido());
            st.setString(8, referencia.getPrimerNombre());
            st.setString(9, referencia.getSegundoNombre());
            st.setString(10, referencia.getTelefono());
            st.setString(11, referencia.getTelefono2());
            st.setString(12, referencia.getExtension());
            st.setString(13, referencia.getCelular());
            st.setString(14, referencia.getCiudad());
            st.setString(15, referencia.getDepartamento());
            st.setString(16, referencia.getTiempoConocido());
            st.setString(17, referencia.getParentesco());
            st.setString(18, referencia.getEmail());
            st.setString(19, referencia.getDireccion());
            st.setString(20, loginuser);
            st.setString(21, dstrct);
            st.setString(22, referencia.getReferenciado());
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql referencias: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }
    
    
    /**
     * Genera una lista de Pagadurias
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */    
    
    public boolean insertarDatosLibranza(SolicitudAval datoSolic, ArrayList<SolicitudReferencias> lista_refs,
            ArrayList<SolicitudCuentas> lista_cuentas, ArrayList<SolicitudLaboral> lista_lab,
            ArrayList<SolicitudBienes> lista_bienes, ArrayList<SolicitudVehiculo> lista_veh, ArrayList<SolicitudOblComprar> lista_obligaciones, ArrayList<SolicitudPersona> lista_pers, ArrayList<SolicitudHijos> lista_hij, SolicitudEstudiante estudiante,
            NegocioTrazabilidad negtraza, SolicitudNegocio solcneg,
            ArrayList<SolicitudFinanzas> lista_finanzas, ArrayList<SolicitudTransacciones> lista_Transacciones) throws Exception {
        boolean sw = true;
        Connection con = null;
        int num_solicitud = Integer.parseInt(datoSolic.getNumeroSolicitud());
        Statement stmt = null;
        String query = "INS_SAV";
        con = conectarJNDI(query);
        con.setAutoCommit(false);
        stmt = con.createStatement();
        try {
            
            if (this.existeSolicitud(num_solicitud)) {
                
                ArrayList<String> listSql;
                if(lista_finanzas != null || lista_Transacciones != null) {
                    listSql = this.borrarSolicitudLibranza(num_solicitud);
                } else {
                    listSql = this.borrarSolicitud(num_solicitud);
                }
                
                for (String sql : listSql) {
                    stmt.addBatch(sql);
                }
                
                stmt.addBatch(this.updateSolicitud(datoSolic));
                
            } else {
                stmt.addBatch(this.insertSolicitud(datoSolic));
            }
            for (int i = 0; i < lista_veh.size(); i++) {
                stmt.addBatch(this.insertVehiculos(lista_veh.get(i)));
            }
            for (int i = 0; i < lista_obligaciones.size(); i++) {
                stmt.addBatch(this.insertObligaciones(lista_obligaciones.get(i)));
            }            
            for (int i = 0; i < lista_bienes.size(); i++) {
                stmt.addBatch(this.insertBienes(lista_bienes.get(i)));
            }
            for (int i = 0; i < lista_refs.size(); i++) {
                stmt.addBatch(this.insertRefsLibranza(lista_refs.get(i)));
            }
            for (int i = 0; i < lista_pers.size(); i++) {
                stmt.addBatch(this.insertPersona(lista_pers.get(i)));
            }
            for (int i = 0; i < lista_lab.size(); i++) {
                stmt.addBatch(this.insertLaboral(lista_lab.get(i)));
            }
            for (int i = 0; i < lista_hij.size(); i++) {
                stmt.addBatch(this.insertHijo(lista_hij.get(i)));
            }
            if(lista_finanzas != null) {
                for (int i = 0; i < lista_finanzas.size(); i++) {
                    stmt.addBatch(this.insertFinanzas(lista_finanzas.get(i)));
                }
            }
            if(lista_Transacciones != null) {
                for (int i = 0; i < lista_Transacciones.size(); i++) {
                    stmt.addBatch(this.insertTransacciones(lista_Transacciones.get(i)));
                }
            }
            if (estudiante != null) {
                stmt.addBatch(this.insertEstudiante(estudiante));
            }
            if (solcneg != null) {
                stmt.addBatch(this.insertNegocio(solcneg));
            }

            for (int i = 0; i < lista_cuentas.size(); i++) {
                stmt.addBatch(this.insertCuentas(lista_cuentas.get(i)));
            }

            if (!datoSolic.getEstadoSol().equals("B")) {
                NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
                stmt.addBatch(daotraza.insertNegocioTrazabilidad(negtraza));
                if (negtraza.getCodNeg()!= null){
                    stmt.addBatch(daotraza.updateActividad(negtraza.getCodNeg(), negtraza.getActividad()));
                }
            }

             if ((!datoSolic.getEstadoSol().equals("B"))&&(!datoSolic.getMod_formulario().equals(""))) {
               datoSolic.setUserUpdate(loginuser);
                stmt.addBatch(this.updateModSolicitud(datoSolic));
            }

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
        } catch (Exception e) {
            System.out.println("Error en insertarDatosnat: " + e.toString());
            sw = false;
            con.rollback();
            if (e instanceof SQLException) {
                throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + ((SQLException) e).getNextException());
            } else {
                e.printStackTrace();
            }
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }
    
    /**
     * Genera una lista de Pagadurias
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */      

    public ArrayList<String> buscarPagaduriasLibranzas(String id_usuario) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_PAGADURIA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id_usuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("nit_proveedor") + ";_;" + rs.getString("payment_name"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscarPagaduriasLibranzas: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarPagaduriasLibranzas: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public String datosEntidadesProveedor(String nit_entidad, Usuario u) throws Exception {
       
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ENTIDADES_PROVEEDOR";       
        JsonObject jsonObject = null;
        String sql = "";
        try {
        sql = this.obtenerSQL(query);
        con = conectarJNDI(query, u.getBd());
        
            if (con != null) {
                ps = con.prepareStatement(sql);
                ps.setString(1, nit_entidad);      
                rs=ps.executeQuery();
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                  
                }
            }
        } catch (Exception e) {
             jsonObject = new JsonObject();
             jsonObject.addProperty("error", "Lo sentimos algo salio mal al cargar los datos de la entidad");
            e.printStackTrace();
            throw new Exception("Error al listar proveedores: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
            return new Gson().toJson(jsonObject);
        }
     
    }
    
    public JsonArray EntidadesProveedor(Usuario u) throws Exception {
       
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ENTIDADES";       
        JsonObject jsonObject = null;
        JsonArray lista = null;
        String sql = "";
        try {
        sql = this.obtenerSQL(query);
        con = conectarJNDI(query, u.getBd());
        
            if (con != null) {
                ps = con.prepareStatement(sql);
                rs=ps.executeQuery();
                lista = new JsonArray();      
                while (rs.next()) {
                    jsonObject = new JsonObject();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                        jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                lista.add(jsonObject);
                }
            }
        } catch (Exception e) {
             e.printStackTrace();
            throw new Exception("Error al listar proveedores de entidades: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
           return lista;
        }
     
    }
    
    public ArrayList<ObligacionesCompra> buscarObligacionesForm(int num_solicitud) throws Exception {
        ArrayList<ObligacionesCompra> lista = new ArrayList<>();
        ObligacionesCompra obligaciones = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SOBLIGACIONES";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, num_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                obligaciones = new ObligacionesCompra();
                obligaciones.setNit_proveedor(rs.getString("nit_entidad"));
                obligaciones.setEntidad(rs.getString("entidad"));
                obligaciones.setNumero_cuenta(rs.getString("numero_cuenta"));
                obligaciones.setTipo_cuenta(rs.getString("tipo_cuenta"));
                obligaciones.setValor_comprar(rs.getDouble("valor_comprar"));
                obligaciones.setSecuencia(rs.getInt("secuencia"));
                lista.add(obligaciones);
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de cuentas: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de cuentas: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }
    
     public String insertObligacionesCompras(ObligacionesCompra obligaciones) {
        String cadena = "";
        String query = "INS_OBLIGACIONES_COMPRAS";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, obligaciones.getNumero_solicitud());
            st.setInt(2, obligaciones.getSecuencia());
            st.setString(3, obligaciones.getNit_proveedor());
            st.setString(4, obligaciones.getEntidad());
            st.setString(5, obligaciones.getTipo_cuenta());
            st.setString(6, obligaciones.getNumero_cuenta());
            st.setDouble(7, obligaciones.getValor_comprar());
            st.setString(8, loginuser);
          
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql obligaciones: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }
    

    /**
     * Genera una lista de Entidades a Comprar Cartera
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listadoEntidadesComprar(String act) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ENTIDADES_COMPRA_CARTERA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            //ps.setString(1, act);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("nit") + ";_;" + rs.getString("payment_name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar entidades: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }   

    /**
     * Genera una lista de ocupaciones
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listadoDetalleEntidad(String entidad) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ENTIDAD_PROVEEDORA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, entidad);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("nit") + ";_;" + rs.getString("tipo_cuenta") + ";_;" + rs.getString("no_cuenta"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar Detalle Entidad: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    
    public ArrayList<BeanGeneral> negociosPorLegalizar (String usuario) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        BeanGeneral bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_NEGOCIOS_LEGALIZAR";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            while(rs.next()) {
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("estado_sol"));
                bean.setValor_02(rs.getString("fecha_consulta"));
                bean.setValor_03(rs.getString("fecha_creacion"));
                bean.setValor_04(rs.getString("valor_solicitado"));
                bean.setValor_05(rs.getString("cod_neg"));
                bean.setValor_06(rs.getString("estado_neg"));
                bean.setValor_07(rs.getString("numero_solicitud"));
                bean.setValor_08(rs.getString("solicitante"));
                bean.setValor_09(rs.getString("plazo"));
                lista.add(bean);
            }
        } catch (Exception e) {
            System.out.println("Error en negociosPorLegalizar: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error negociosPorLegalizar: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    } 
     
    public String insertFinanzas(SolicitudFinanzas finanzas) {
        String cadena = "";
        String query = "INS_SFI";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, dstrct);
            st.setString(2, finanzas.getNumeroSolicitud());
            st.setString(3, finanzas.getTipo());
            st.setString(4, finanzas.getId_persona());
            
            st.setString(5, finanzas.getSalario());
            st.setString(6, finanzas.getHonorarios());
            st.setString(7, finanzas.getOtros_ingresos());
            st.setString(8, finanzas.getTotal_ingresos());
            
            st.setString(9, finanzas.getDescuento_nomina());
            st.setString(10, finanzas.getGastos_arriendo());
            st.setString(11, finanzas.getGastos_creditos());
            st.setString(12, finanzas.getOtros_gastos());
            st.setString(13, finanzas.getTotal_egresos());
            
            st.setString(14, finanzas.getActivos());
            st.setString(15, finanzas.getPasivos());
            st.setString(16, finanzas.getTotal_patrimonio());
            
            st.setString(17, loginuser);
            
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql finanzas: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }
    
    public String insertTransacciones(SolicitudTransacciones transf) {
        String cadena = "";
        String query = "INS_STR";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, dstrct);
            st.setString(2, transf.getNumeroSolicitud());
            st.setString(3, transf.getTipo());
            st.setString(4, transf.getId_persona());
            
            st.setString(5, transf.getTransacciones_al_extanjero());
            st.setString(6, transf.getImportaciones());
            st.setString(7, transf.getExportaciones());
            st.setString(8, transf.getInversiones());
            st.setString(9, transf.getGiros());
            st.setString(10, transf.getPrestamos());
            st.setString(11, transf.getOtras_transacciones());
            
            st.setString(12, transf.getPago_cuenta_exterior());
            st.setString(13, transf.getBanco());
            st.setString(14, transf.getCuenta());
            st.setString(15, transf.getPais());
            st.setString(16, transf.getCiudad());
            st.setString(17, transf.getMoneda());
            st.setString(18, transf.getTipo_pro());
            st.setString(19, transf.getMonto());
            
            st.setString(20, loginuser);
                 
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql transsacciones: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }
    
    public ArrayList<SolicitudPersona> buscarPersonas(int numero_solicitud) throws SQLException {
        ArrayList<SolicitudPersona> lista = new ArrayList<SolicitudPersona>();
        SolicitudPersona bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SPES";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, numero_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new SolicitudPersona();
                bean = bean.load(rs);
                lista.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return lista;
    }

    public ArrayList<SolicitudLaboral> buscarLabores(int numero_solicitud) throws SQLException {
        ArrayList<SolicitudLaboral> lista = new ArrayList<SolicitudLaboral>();
        SolicitudLaboral bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SLAS";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, numero_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new SolicitudLaboral();
                bean = bean.load(rs);
                lista.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return lista;
    }
    
    public ArrayList<SolicitudReferencias> buscarReferencias(int numero_solicitud) throws SQLException {
        ArrayList<SolicitudReferencias> lista = new ArrayList<SolicitudReferencias>();
        SolicitudReferencias bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SRFS";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, numero_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new SolicitudReferencias();
                bean = bean.load(rs);
                lista.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return lista;
    }
    
    public ArrayList<SolicitudFinanzas> buscarFinanzas(int numero_solicitud) throws SQLException {
        ArrayList<SolicitudFinanzas> lista = new ArrayList<SolicitudFinanzas>();
        SolicitudFinanzas bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_SFN";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, numero_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new SolicitudFinanzas();
                bean = bean.load(rs);
                lista.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return lista;
    }
    
    public ArrayList<SolicitudTransacciones> buscarTransacciones(int numero_solicitud) throws SQLException {
        ArrayList<SolicitudTransacciones> lista = new ArrayList<SolicitudTransacciones>();
        SolicitudTransacciones bean = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_STR";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, numero_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                bean = new SolicitudTransacciones();
                bean = bean.load(rs);
                lista.add(bean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return lista;
    }

    public void rechazarSolicitud(String numero_solicitud) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        String query = "RECHAZAR_SOLICITUD";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, loginuser);
            ps.setString(2, numero_solicitud);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en buscarAfilsUsuario: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarAfilsUsuario: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
    }
    
    public ArrayList<String> borrarSolicitudLibranza(int num_solicitud) throws Exception {
        ArrayList<String> listaSql = new ArrayList<String>();
        String query = "SQL_DEL_LIBS";
        String sql = "";
        String lista [] = null;
        try {
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#num_sol", "" + num_solicitud);
            lista = sql.split(";");
            listaSql.addAll(Arrays.asList(lista));
        } catch (Exception e) {
            System.out.println("Error en borrar datos solicitud: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en borrar datos solicitud: " + e.toString());
        }
        return  listaSql;
    }
    
     public void actualizarSolicitudFiltroLibranza(int numero_solicitud, int id_filtro) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        String query = "ACTUALIZAR_SOLICITUD_FILTRO_LB";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, numero_solicitud);
            ps.setString(2, loginuser);
            ps.setInt(3, id_filtro);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en actualizarSolicitudFiltroLibranza: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error actualizarSolicitudFiltroLibranza: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
    }
     
    /**
    * Genera una lista de barrios
    * @return lista con los datos encontrados
    * @throws Exception cuando hay error
    */
    public ArrayList listadoBarrios(String ciudad) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_BARRIO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, ciudad);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("nombre") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar barrios: " + e.toString());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }
    
    public String cargarGridBarrios(String codciu) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject obj = new JsonObject();       
        Gson gson = new Gson();        
        String query = "SRC_BARRIO";       
        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);    
            ps.setString(1, codciu);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = new JsonObject();
            while (rs.next()){
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));                 
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                datos.add(fila);
            }
           obj.add("rows", datos);
        }catch (Exception e) {
            try {
                throw new Exception("Error en cargarGridBarrios " + e.toString());
            } catch (Exception ex) {               
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
         return  gson.toJson(obj);
    }
   
    public String insertarBarrio(String codciu, String nombre, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;      
        String sql = "";
        String query = "insertarBarrio";
        String  respuestaJson = "{}";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, codciu);       
            ps.setString(3, nombre);         
            ps.setString(4, usuario.getLogin());
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new Exception("Error en insertarBarrio " + e.toString());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }
     
    public String actualizarBarrio(String idBarrio, String nombre, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;      
        String sql = "";
        String query = "actualizarBarrio";
        String  respuestaJson = "{}";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);         
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(idBarrio));
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new Exception("Error en actualizarBarrio " + e.toString());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
        
    }
    
    
    public String cambiaEstadoBarrio(String idBarrio, Usuario usuario) {
        Connection con = null;
        PreparedStatement ps = null;      
        String sql = "";
        String query = "cambiarEstadoBarrio";
         String  respuestaJson = "{}";
        try{
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);           
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(idBarrio));
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        }catch (Exception e) {
            try {
                respuestaJson ="{\"error\":\"" + e.getMessage() + "\"}";
                throw new Exception("Error en cambiarEstadoNomenclatura " + e.toString());
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
        
    }
    
    /**
     * Metodo que busca la fecha de un credito pre-aprobado
     *
     * @param identificacion
     * @param idUnidadNegocio
     * @return
     */
    public BeanGeneral buscarFechaPreaprobado(String identificacion, int idUnidadNegocio) {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "FECHA_PREAPROBADO";
        BeanGeneral bg = new BeanGeneral();
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, idUnidadNegocio);
            ps.setString(2, identificacion);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                bg.setValor_01(rs.getString("cod_cli"));
                bg.setValor_02(rs.getString("cod_neg"));
                bg.setValor_03(rs.getString("id_convenio"));
                bg.setValor_04(rs.getString("f_desem"));
                bg.setValor_05(rs.getString("vr_negocio"));
                bg.setValor_06(rs.getString("nit_tercero"));
                bg.setValor_07(rs.getString("estado_neg"));
                bg.setValor_08(rs.getString("fecha_pr_cuota"));
            } else {
                throw new Exception();
            }

        } catch (Exception e) {
            bg.setValor_08((com.tsp.util.Util.AnoActual() + "-" + com.tsp.util.Util.getFechaActual_String(3) + "-" + com.tsp.util.Util.getFechaActual_String(5)));
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
        }
        return bg;
    }

    /**
     * Buscar una persona por su documento
     * @param id documento
     * @return Objeto SolicitudPersona
     * @throws Exception SQLException
     */
    public SolicitudPersona buscarPersona(int id) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_PERSONA";
        String sql = "";
        SolicitudPersona persona = null;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                persona = new SolicitudPersona();
                persona.setNombre(rs.getString("nombre"));
                persona.setCiudadExpedicionId(rs.getString("ciudad_expedicion_id"));
                persona.setCiudad(rs.getString("ciudad"));
                persona.setDepartamento(rs.getString("departamento"));
                persona.setDireccion(rs.getString("direccion"));
                persona.setBarrio(rs.getString("barrio"));
                persona.setCelular(rs.getString("celular"));
                persona.setTelefono(rs.getString("telefono"));
                persona.setEmail(rs.getString("email"));
            }
        } catch (Exception e) {
            System.out.println("Error en buscar persona: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar persona: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return persona;
    }
    
    public int obtenerIdFiltro(int numeroSolicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL("BUSCAR_ID_FILTRO"));
            ps.setInt(1, numeroSolicitud);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            System.err.println("Error en buscar id filtro: " + e.toString());
            return 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    System.err.println("Error cerrando el statement: " + e.toString());
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    System.err.println("Error cerrando el statement: " + e.toString());
                }
            }
        }
    }
    
    public String buscarCompraCartera(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonObject json;
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL("BUSCAR_COMPRA_CARTERA"));
            ps.setString(1, negocio);
            rs = ps.executeQuery();
            
            JsonArray array = new JsonArray();
            while (rs.next()) {
                json = new JsonObject();
                if (!rs.getString(7).equals("V")) {
                    json.addProperty("respuesta", "No se puede modificar la solicitud porque no est� avalada.");
                    return gson.toJson(json);
                }
                json.addProperty("entidad", rs.getString(2));
                json.addProperty("secuencia", rs.getString(3));
                json.addProperty("nit", rs.getString(4));
                json.addProperty("cuenta", rs.getString(5));
                json.addProperty("valor", rs.getString(6));
                array.add(json);
            }
            return gson.toJson(array);
        } catch (SQLException e) {
            System.err.println("Error en buscar la compra de cartera de la solicitud: " + e.toString());
            json = new JsonObject();
            json.addProperty("error", "Hubo un error al consultar en la base de datos");
            return gson.toJson(json);
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    System.err.println("Error cerrando el statement: " + e.toString());
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    System.err.println("Error cerrando el statement: " + e.toString());
                }
            }
        }
    }
    
    public void modificarCompraCartera(String numeroSolicitud, int[] secuencia, String[] nit, String[] cuenta, double[] valor) throws SQLException {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = this.conectarJNDI();            
            ps = conn.prepareStatement(this.obtenerSQL("UPD_COMPRA_CARTERA"));

            for (int i = 0; i < secuencia.length; i++) {
                ps.setDouble(1, valor[i]);
                ps.setString(2, numeroSolicitud);
                ps.setInt(3, secuencia[i]);
                ps.setString(4, nit[i]);
                ps.setString(5, cuenta[i]);
                ps.addBatch();
            }
            int[] updateCount = ps.executeBatch();
            if (updateCount.length != secuencia.length) {
                throw new SQLException("Algunos registros no se modificaron o no fueron encontrados.", "INV0");
            }
        } finally {
            if (ps != null) ps.close();
            if (conn != null) conn.close();
        }
    }

    private String updateAsesorPresolicitud(SolicitudAval solicitud) {
       String cadena = "";
       String query = "";        
       query = "UPD_ASESOR_PRESOLICITUD";
        
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);
            st.setString(1, solicitud.getAsesor());
            st.setString(2, solicitud.isCompraCartera()? "S":"N");
            st.setString(3, solicitud.getNumeroSolicitud());
           
            cadena = st.getSql();
            st = null;
        } catch (Exception e) {
            System.out.println("Error al actualizar asesor sql solicitud: " + e.toString());
            e.printStackTrace();
        }
        return cadena;
    }
    
    /**
     * Retorna la configuracion de la fianza segun el comvenio y el plazo
     * @param idConvenio
     * @param plazo
     * @param id_producto
     * @param id_cobertura
     * @param nit_empresa
     * @return
     */
    public AvalFianzaBeans getConfiguracionFianza(int idConvenio ,int plazo, int id_producto, int id_cobertura,String nit_empresa ){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        AvalFianzaBeans av=null;
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL("BUSCAR_CONFIG_FIANZA_AVAL"));
            ps.setInt(1, idConvenio);
            ps.setInt(2, id_producto);//Producto
            ps.setInt(3, id_cobertura);//Cobertura
            ps.setString(4, nit_empresa);//Nit Fianza
            ps.setInt(5, plazo);
            rs = ps.executeQuery();
            av=new AvalFianzaBeans();
            while (rs.next()) {    
               av.setPorc_fin_fianza(rs.getDouble("porc_aval_fin"));
               av.setPorc_dto_fianza(rs.getDouble("porc_aval_clie"));
            }
        } catch (SQLException e) {
            av=null;
            e.printStackTrace();           
        } finally {
            desconectarPool(con, rs, ps, null, this.getClass());
        }        
        return av;
    }

    public JsonObject validarDireccionCliente(String direccion, String nit) {
       Connection con = null;
       PreparedStatement ps = null;
       ResultSet rs = null;
       
        String query = "VALIDAR_DIRECCION";
        JsonObject jo = new JsonObject();
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
                ps = con.prepareStatement(query);
                ps.setString(1, nit);
                ps.setString(2, direccion);
                rs = ps.executeQuery();
                if (rs.next()) {
                    jo.addProperty("success", true);
                    jo.addProperty("numero_solicitud", rs.getString("numero_solicitud"));
                    jo.addProperty("nombre", rs.getString("nombre"));
                    jo.addProperty("identificacion", rs.getString("identificacion"));
                    jo.addProperty("cod_neg", rs.getString("cod_neg"));
                }
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            desconectarPool(con, rs, ps, null, this.getClass());
        }

        return jo;
    }
    
     public ArrayList<Destino_Credito> cargarDestinos(int unidadNegocio) throws SQLException {
        ArrayList<Destino_Credito> lista = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_DESTINOS";       
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, unidadNegocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                Destino_Credito destcre = new Destino_Credito();
                destcre.setDestino_credito(rs.getString("destino"));
                destcre.setCod_destino_credito(rs.getString("codigo"));
                lista.add(destcre);
            }
        } catch (SQLException e) {    
            throw new SQLException("Error cargarDestinos: " + e.toString());
        } finally {
            desconectarPool(con, rs, ps, null, this.getClass());
        }
        return lista;
    }
    
}
