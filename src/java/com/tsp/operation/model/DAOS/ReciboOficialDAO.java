/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.ReporteDatacredito;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author jpacosta
 */
public interface ReciboOficialDAO {
    public abstract String cargarROP(String documento, String periodo, String unegocio, String negocio, String cedula, String valor) ;
    public abstract String cargarDetalles(String idRop);
    public abstract String cargarUnidades();

    public abstract String cargarHistoricoPeticiones(String periodo, String unegocio, String negocio, String cedula);

    public abstract ArrayList<BeanGeneral> listaReportadosDatacredito(String periodo, String unegocio, String negocio, String cedula);

    public ArrayList<ReporteDatacredito> cargarPrevisualizarReportados(String periodo, String unegocio, String fecha, int rangoIni, int rangoFin, String nomNeg, String accion);

    public String guardarReportados(BeanGeneral bg, String login);

    public void limpiarReporte(String periodo, String unegocio);

    public String anularNegocioHistorico(String periodo, String unegocio, String usuario);

    public String anularNegocioReportado(String periodo, String unegocio, String usuario);

    public boolean generadoReporte(String periodo, String unegocio);

    public String obtenerReporteDatacredito(String periodo, String unegocio, String fecha, int rangoIni, int rangoFin, String accion, String estado);

    public String obtenerCodigoUnidNegocioCR(String unegocio);

    public int obtenerCantidadReportados(String periodo, String unegocio);

    public ArrayList<ReporteDatacredito> obtenerNoReportados(String periodo, String unegocio, String fecha, int rangoIni, int rangoFin, String nomNeg, String visualizar);

    public void insertarReporteHistorico(String periodo, String unegocio, String fecha, int rangoIni, int rangoFin);

   // public ArrayList<CmbGeneralScBeans> cargarUnidadesDatacredito();

    public String cargarUnidadesDatacredito();
    
    public ArrayList<BeanGeneral> buscarDetalle(int idRop, String negocio)throws SQLException;
    
    /**
     *
     * @param fechaif
     * @param fecha_actual
     * @param valor_if
     * @param negocio
     * @return
     * @throws SQLException
     */
    public String actualizarIFecha(String fechaif,String fecha_actual,double valor_if, String negocio)throws SQLException;

    public String generarPlanoAsobancaria(JsonObject info);

    public String genPlanoAsoEfecty(JsonObject info);

}
