/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Usuario;

/**
 *
 * @author root
 */
public interface FiltroLibranzaDAO {
      
    public abstract JsonObject getOcupLaboral() throws Exception;
    
    public abstract JsonObject getEstadoCivil() throws Exception;
    
    public abstract JsonObject getConvenios(String oLab) throws Exception;
    
    public abstract JsonObject buscarPersona(String identificacion) throws Exception;
  
    public abstract JsonObject infoConvenio(String id_cnl) throws Exception;

    public abstract double getPorcExtraprima(String id_ocup, int edad) throws Exception;
    
    public abstract JsonObject getDescuentosLey(String id_ocup, double salario) throws Exception;
    
    public abstract JsonObject GuardarFiltro(JsonObject form, String[] listadocs, Usuario usuario) throws Exception;
    
    public abstract JsonObject getFiltroLibranza(String idL) throws Exception;
    
    public String formalizarLibranza();
    
    public JsonObject getIdFiltroLibranza(String cedula) throws Exception;
    
    public JsonObject getEmpresasPagaduria(String idConfigLibranza) throws Exception;
    
    public JsonObject getDocumentosRequeridos(String idFiltroLibranza)throws Exception;
    
    public JsonObject consultarHDC(JsonObject jsonData, String token);
}
