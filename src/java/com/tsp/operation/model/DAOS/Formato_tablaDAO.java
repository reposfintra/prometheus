/****************************************************************************
 * Nombre clase: Formato_tablaDAO.java                                      *
 * Descripci�n: Clase que maneja las consultas de los formatos de tablas.   *
 * Autor: Ing. Jose de la rosa                                              *
 * Fecha: 25 de noviembre de 2006, 10:16 AM                                 *
 * Versi�n: Java 1.0                                                        *
 * Copyright: Fintravalores S.A. S.A.                                  *
 ***************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class Formato_tablaDAO extends MainDAO{

    private Formato_tabla ftabla;
    private Vector Vecftabla;
    private Vector VecftablaMod;
    private Vector Vecformato;
    
    private Vector VecCampos;
    private Vector tablas;
    private Vector fordefinidos;
    private Vector vecbusqueda;
    
    
    /** Creates a new instance of Formato_tablaDAO */
    public Formato_tablaDAO () {
        super ( "Formato_tablaDAO.xml" );
    }
    public Formato_tablaDAO (String dataBaseName) {
        super ( "Formato_tablaDAO.xml", dataBaseName );
    }
    
    /**
     * Getter for property ftabla.
     * @return Value of property ftabla.
     */
    public Formato_tabla getFtabla () {
        return ftabla;
    }
    
    /**
     * Setter for property ftabla.
     * @param ftabla New value of property ftabla.
     */
    public void setFtabla (Formato_tabla ftabla) {
        this.ftabla = ftabla;
    }
    
    /**
     * Getter for property Vecftabla.
     * @return Value of property Vecftabla.
     */
    public Vector getVecftabla () {
        return Vecftabla;
    }
    
    /**
     * Setter for property Vecftabla.
     * @param Vecftabla New value of property Vecftabla.
     */
    public void setVecftabla (Vector Vecftabla) {
        this.Vecftabla = Vecftabla;
    }
    
    /**
     * Getter for property Vecformato.
     * @return Value of property Vecformato.
     */
    public Vector getVecformato () {
        return Vecformato;
    }
    
    /**
     * Setter for property Vecformato.
     * @param Vecformato New value of property Vecformato.
     */
    public void setVecformato (java.util.Vector Vecformato) {
        this.Vecformato = Vecformato;
    }
    
     /**
     * Getter for property VecftablaMod.
     * @return Value of property VecftablaMod.
     */
    public java.util.Vector getVecftablaMod () {
        return VecftablaMod;
    }
    
    /**
     * Setter for property VecftablaMod.
     * @param VecftablaMod New value of property VecftablaMod.
     */
    public void setVecftablaMod (java.util.Vector VecftablaMod) {
        this.VecftablaMod = VecftablaMod;
    }
    
    public void resetVecftablaMod () {
        this.VecftablaMod = null;
        this.VecftablaMod = new Vector();
    }
    
    /**
     * Metodo: insertFormato_tabla, permite ingresar un registro a la tabla formato_tabla.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String insertFormato_tabla () throws SQLException{
        PreparedStatement st = null;
        String query = "SQL_INSERT", sql = "";
        try{
            st = crearPreparedStatement (query);
            st.setString (1,ftabla.getFormato ());
            st.setString (2,ftabla.getTabla ());
            st.setString (3,ftabla.getCampo_tabla ());
            st.setString (4,ftabla.getCampo_jsp ());
            st.setString (5,ftabla.getTitulo ());
            st.setString (6,ftabla.getTipo_campo ());
            st.setString (7,ftabla.getValidar ());
            st.setString (8,ftabla.getTabla_val ());
            st.setString (9,ftabla.getCampo_val ());
            st.setString (10,ftabla.getPrimaria ());
            st.setInt    (11,ftabla.getOrden ());
            st.setDouble (12,ftabla.getLongitud ());
            st.setString (13,ftabla.getFecha_creacion ());
            st.setString (14,ftabla.getUsuario_creacion ());
            st.setString (15,ftabla.getBase ());
            st.setString (16,ftabla.getDistrito ());
            st.setString (17,ftabla.getUltima_modificacion ());
            st.setString (18,ftabla.getUsuario_modificacion ());
            st.setString (19,ftabla.getValor_campo ());
            st.setString (20,ftabla.getTipo_campo_equivalente ());
            sql = st.toString ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL INGRESAR EL FORMATO DE TABLA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
        return sql;
    }
    
    /**
     * Metodo: existeTabla, permite buscar si existe el nombre de la tabla en la BD
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla.
     * @version : 1.0
     */
    public void existeTabla (String nombre_tabla) throws Exception{
        Vecftabla                     = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        Conexion    = poolManager.getConnection("fintra");
        boolean           result      = false, sw = false;
        Vecftabla                     = null;
        List              llave       = new LinkedList();
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
            llave = obtenerLLavesPrimarias( nombre_tabla );
            String schema, table;
            String []tab = nombre_tabla.split("\\.");
            
            if (tab.length==2){
                schema = tab[0];
                table  = tab[1];
            }
            else{
                schema = "public";
                table  = nombre_tabla;
            }
            String tipos[] = new String [] { "TABLE" };
            DatabaseMetaData dbmd   = Conexion.getMetaData();
            ResultSet        tablas = dbmd.getTables( null, schema, table , tipos );
            if ( tablas.next() ) {
                String    tabla    = tablas.getString( tablas.findColumn( "TABLE_NAME" ) );
                ResultSet columnas = dbmd.getColumns(null, schema, tabla, null);
                Vecftabla = new Vector();
                while(columnas.next()){
                    
                    String defa = columnas.getString("COLUMN_DEF")==null?"":columnas.getString("COLUMN_DEF");
                    String []def = defa.split("\\::");
                    if (def.length==2){
                        defa = def[0].trim ().replace ("'","");
                    }
                    ftabla = new Formato_tabla ();
                    ftabla.setCampo_tabla ( columnas.getString("COLUMN_NAME") );
                    ftabla.setValor_campo ( columnas.getString("TYPE_NAME").equals ("int4") && !defa.equals ("0")? "" : defa );
                    int taman = columnas.getInt ("COLUMN_SIZE") < 0 ? 0 : columnas.getInt ("COLUMN_SIZE");
                    double val = (double) ( Double.parseDouble ( "0." + columnas.getInt ("DECIMAL_DIGITS") ) )+ taman;
                    ftabla.setLongitud ( val );
                    ftabla.setTipo_campo ( columnas.getString("TYPE_NAME") );
                    sw = false;
                    for(int j=0; j<llave.size ();j++){
                        if( llave.get (j).equals ( columnas.getString("COLUMN_NAME") ) )
                            sw = true;
                    }
                    if( sw )
                        ftabla.setPrimaria ( "S" );
                    else
                        ftabla.setPrimaria ( "N" );
                    ftabla.setCampo_jsp ("");
                    ftabla.setCampo_val ("");
                    ftabla.setTabla_val ("");
                    ftabla.setOrden (0);
                    ftabla.setTitulo ("");
                    ftabla.setValidar ("N");
                    Vecftabla.add (ftabla);
                }
            }
        }catch(Exception e) {
            throw new Exception("Error en rutina Incluir [ImportacionDAO]... \n"+e.getMessage());
        }
        finally {
            poolManager.freeConnection("fintra"  ,Conexion);
        }
    }
    
    /**
     * Metodo: estaTabla, permite buscar si existe el nombre de la tabla en la BD
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla.
     * @version : 1.0
     */
    public boolean estaTabla (String nombre_tabla) throws SQLException{
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        Conexion    = poolManager.getConnection("fintra");
        boolean           result      = false, sw = false;
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
            String schema, table;
            String []tab = nombre_tabla.split("\\.");
            
            if (tab.length==2){
                schema = tab[0];
                table  = tab[1];
            }
            else{
                schema = "public";
                table  = nombre_tabla;
            }
            String tipos[] = new String [] { "TABLE" };
            DatabaseMetaData dbmd   = Conexion.getMetaData();
            ResultSet        tablas = dbmd.getTables( null, schema, table , tipos );
            sw = tablas.next();
        }catch(SQLException e) {
            throw new SQLException("Error en rutina estaTabla (String nombre_tabla)... \n"+e.getMessage());
        }
        finally {
            poolManager.freeConnection("fintra"  ,Conexion);
        }
        return sw;
    }
    
    /**
     * Metodo: estaCampo, permite buscar si existe el nombre del campo en la tabla
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla, nombre campo.
     * @version : 1.0
     */
    public boolean estaCampo (String nombre_tabla, String campo_val) throws SQLException{
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        Conexion    = poolManager.getConnection("fintra");
        boolean           result      = false, sw = false;
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
            String schema, table;
            String []tab = nombre_tabla.split("\\.");
            
            if (tab.length==2){
                schema = tab[0];
                table  = tab[1];
            }
            else{
                schema = "public";
                table  = nombre_tabla;
            }
            String tipos[] = new String [] { "TABLE" };
            DatabaseMetaData dbmd   = Conexion.getMetaData();
            ResultSet        tablas = dbmd.getTables( null, schema, table , tipos );
            if ( tablas.next() ) {
                String    tabla    = tablas.getString( tablas.findColumn( "TABLE_NAME" ) );
                ResultSet columnas = dbmd.getColumns(null, schema, tabla, null);
                while(columnas.next()){
                    if( campo_val.equals ( columnas.getString("COLUMN_NAME") ) )
                        sw = true;
                }
            }
        }catch(SQLException e) {
            throw new SQLException("Error en rutina Incluir [ImportacionDAO]... \n"+e.getMessage());
        }
        finally {
            poolManager.freeConnection("fintra"  ,Conexion);
        }
        return sw;
    }
     
    /**
     * Procedimiento que incluye una tabla de la base de datos como una tabla de
     * importacion
     * @autor : Ing. Mario Fontalvo
     * @param ntabla nombre de la tabla
     * @throws Exception .
     * @return boolean que indica si se pudo o no incluir la tabla como una tabla de importacion
     * @see EXECUTE_UPDATE (int, String [])
     */
    public List obtenerLLavesPrimarias( String ntabla ) throws Exception{
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        Conexion    = poolManager.getConnection("fintra");
        List              result      = new LinkedList();
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
            
            String schema, table;
            String []tab = ntabla.split("\\.");
            
            if (tab.length==2){
                schema = tab[0];
                table  = tab[1];
            }
            else{
                schema = null;
                table  = ntabla;
            }
            
            
            // Listamos s�lo tablas
            String tipos[] = new String [] { "TABLE" };
            DatabaseMetaData dbmd   = Conexion.getMetaData();
            ResultSet        tablas = dbmd.getPrimaryKeys( null, schema, table );
            
            while ( tablas.next() ) {
                result.add( tablas.getString( "COLUMN_NAME" ) );
            }
            
        }
        catch(Exception e) {
            throw new Exception("Error en rutina Incluir [ImportacionDAO]... \n"+e.getMessage());
        }
        finally {
            poolManager.freeConnection("fintra"  ,Conexion);
        }
        return result;
    }
    
    /**
     * Metodo: existeTablaModificar, permite buscar si existe el nombre de la tabla en la BD
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla.
     * @version : 1.0
     */
    public void existeTablaModificar (String distrito, String formato ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        VecftablaMod            = null;
        String query = "SQL_EXISTE_TABLA_MODIFICAR";
        try {
            st = crearPreparedStatement (query);
            st.setString (1, distrito);
            st.setString (2, formato);
            VecftablaMod = new Vector ();
            rs = st.executeQuery ();
            while(rs.next ()){
                ftabla = new Formato_tabla ();
                ftabla.setFormato       ( formato );
                ftabla.setTabla         ( rs.getString ("tabla")!=null?rs.getString ("tabla"):"");
                ftabla.setCampo_tabla   ( rs.getString ("campo_tabla")!=null?rs.getString ("campo_tabla"):"");
                ftabla.setCampo_jsp     ( rs.getString ("campo_jsp")!=null?rs.getString ("campo_jsp"):"");
                ftabla.setTitulo        ( rs.getString ("titulo")!=null?rs.getString ("titulo"):"");
                ftabla.setTipo_campo    ( rs.getString ("tipo_campo")!=null?rs.getString ("tipo_campo"):"");
                ftabla.setValidar       ( rs.getString ("validar")!=null?rs.getString ("validar"):"S");
                ftabla.setTabla_val     ( rs.getString ("tabla_val")!=null?rs.getString ("tabla_val"):"");
                ftabla.setCampo_val     ( rs.getString ("campo_val")!=null?rs.getString ("campo_val"):"");
                ftabla.setOrden         ( rs.getInt    ("orden"));
                ftabla.setLongitud      ( rs.getDouble ("longitud"));
                ftabla.setPrimaria      ( rs.getString ("primaria")!=null?rs.getString ("primaria"):"N");
                ftabla.setFecha_creacion( rs.getString ("creation_date")!=null?rs.getString ("creation_date"):"0099-01-01 00:00:00");
                ftabla.setValor_campo   ( rs.getString ("valor_default")!=null?rs.getString ("valor_default"):"");
                VecftablaMod.add (ftabla);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR EL NOMBRE DE TABLA EN LA BD AL MODIFICAR, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
    }
    
    /**
     * Metodo: updateTabla, permite actualizar el campo referencia en tablagen.
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla, codigo de la tabla, referencia.
     * @version : 1.0
     */
    public String updateTabla (String table_type, String table_code, String referencia) throws SQLException{
        PreparedStatement st = null;
        String query = "SQL_UPDATE", sql = "";
        try{
            st = crearPreparedStatement (query);
            st.setString (1,referencia);
            st.setString (2,table_type);
            st.setString (3,table_code);
            sql = st.toString ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR EL CODIGO DE LA TABLA DEL FORMATO EN TABLAGEN, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
        return sql;
    }
    
    /**
     * Metodo: formatoTabla, permite todos los formatos en tablagen
     * @autor : Ing. Jose de la rosa
     * @param : nombre tabla
     * @version : 1.0
     */
    public void formatoTabla ( String table_type ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        Vecformato              = null;
        String query = "SQL_EXISTE_FORMATO";
        try {
            st = crearPreparedStatement (query);
            st.setString (1,table_type);
            rs= st.executeQuery ();
            Vecformato = new Vector ();
            while(rs.next ()){
                ftabla = new Formato_tabla ();
                ftabla.setFormato ( rs.getString ("table_code"));
                ftabla.setNombre_formato ( rs.getString ("descripcion"));
                Vecformato.add (ftabla);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR LOS FORMATOS DE TABLAS EN TABLAGEN, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
    }
    
    /**
     * Metodo: formatoTabla, permite todos los formatos en tablagen
     * @autor : Ing. Jose de la rosa
     * @param : nombre tabla
     * @version : 1.0
     */
    public void formatoTablaModificar ( String table_type ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        Vecformato              = null;
        String query = "SQL_EXISTE_FORMATO_MODIFICAR";
        try {
            st = crearPreparedStatement (query);
            st.setString (1,table_type);
            rs= st.executeQuery ();
            Vecformato = new Vector ();
            while(rs.next ()){
                ftabla = new Formato_tabla ();
                ftabla.setFormato ( rs.getString ("table_code"));
                ftabla.setNombre_formato ( rs.getString ("descripcion"));
                Vecformato.add (ftabla);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR LOS FORMATOS DE TABLAS EN TABLAGEN, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
    }
    
    /**
     * Metodo: existeFormatoTabla, verifica si existe el formato en la tabla
     * @autor : Ing. Jose de la rosa
     * @param : distrito, formato.
     * @version : 1.0
     */
    public boolean existeFormatoTabla ( String distrito, String formato, String tabla, String campo ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        boolean sw = false;
        String query = "SQL_EXISTE_FORMATO_TABLA";
        try {
            st = crearPreparedStatement (query);
            st.setString (1,distrito);
	    st.setString (2,formato);
            st.setString (3,tabla);
            st.setString (4,campo);
            rs= st.executeQuery ();
            sw = rs.next ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR VERIFICA SI EXISTE EL DATO EN LA TABLA FORMATO_TABLA, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
        return sw;
    }
    
    /**
     * Metodo: deleteTabla, permite eliminar todos los datos de la tabla.
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla, codigo de la tabla, referencia.
     * @version : 1.0
     */
    public String deleteTabla ( String distrito, String formato, String tabla ) throws SQLException{
        PreparedStatement st = null;
        String query = "SQL_DELETE", sql = "";
        try{
            st = crearPreparedStatement (query);
            st.setString (1,distrito);
            st.setString (2,formato);
            st.setString (3,tabla);
            sql = st.toString ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR EL CODIGO DE LA TABLA DEL FORMATO EN TABLAGEN, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
        return sql;
    }    
    
    /**
     * Metodo: anularTabla, permite anular todos los datos de la tabla.
     * @autor : Ing. Jose de la rosa
     * @param : nombre de la tabla, codigo de la tabla, referencia.
     * @version : 1.0
     */
    public String anularTabla ( String distrito, String formato, String tabla ) throws SQLException{
        PreparedStatement st = null;
        String query = "SQL_ANULAR", sql = "";
        try{
            st = crearPreparedStatement (query);
            st.setString (1,distrito);
            st.setString (2,formato);
            st.setString (3,tabla);
            sql = st.toString ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ANULAR TODOS LOS DATOS DE LAS TABLAS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
        return sql;
    }  
    
    /**
     * Metodo: converTipoCampo, permite buscar el valor de conversion del tipo de formato
     * @autor : Ing. Jose de la rosa
     * @param : nombre tabla, tipo de campo
     * @version : 1.0
     */
    public String converTipoCampo ( String table_type, String tipo_campo ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String sql              = "TEXT";
        String query = "SQL_CONVERT_TIPO_CAMPO";
        try {
            st = crearPreparedStatement (query);
            st.setString (1,table_type);
            st.setString (2,tipo_campo);
            rs= st.executeQuery ();
            if( rs.next () )
                sql = rs.getString ("referencia");
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR EL CODIGO DE CONVERSION, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
        return sql;
    }
    
    /**
     * Metodo: cargar campos para el formulario de acuerdo  a una tabla y formato
     * @autor : Ing. Diogenes Bastidas  
     * @param : nombre tabla
     * @version : 1.0
     */
    public void camposFormulario ( String formato ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        Vecftabla               = null;
        String query = "SQL_CAMPOS_FORMULARIO";
        try {
            st = crearPreparedStatement (query);
            st.setString (1,formato);
            rs= st.executeQuery ();
            VecCampos = new Vector ();
            while(rs.next ()){
                Formato_tabla campo = new Formato_tabla ();
                campo.setDistrito( rs.getString("dstrct"));
                campo.setFormato( rs.getString("formato"));
                campo.setTabla(rs.getString("tabla"));
                campo.setCampo_tabla(rs.getString("campo_tabla"));
                campo.setCampo_jsp(rs.getString("campo_jsp"));
                campo.setTitulo(rs.getString("titulo"));
                campo.setTipo_campo(rs.getString("tipo_campo_equivalente"));
                campo.setValidar(rs.getString("validar"));
                campo.setTabla_val(rs.getString("tabla_val"));
                campo.setCampo_val(rs.getString("campo_val"));
                campo.setPrimaria(rs.getString("primaria"));
                campo.setOrden(rs.getInt("orden"));
                campo.setLongitud(rs.getDouble("longitud"));
                campo.setValor_campo(rs.getString("valor_default"));
                VecCampos.add(campo);
                
               
                
                
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR LOS FORMATOS DE TABLAS EN TABLAGEN, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
    }
    
    
    /**
     * Metodo: cargar campos para el formulario de busqueda
     * @autor : Ing. Diogenes Bastidas  
     * @param : nombre tabla
     * @version : 1.0
     */
    public void camposFormularioBusqueda ( String formato ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        Vecftabla               = null;
        String query = "SQL_CAMPOS_BUSQUEDA";
        try {
            st = crearPreparedStatement (query);
            st.setString (1,formato);
            rs= st.executeQuery ();
            vecbusqueda = new Vector ();
            while(rs.next ()){
                Formato_tabla campo = new Formato_tabla ();
                campo.setDistrito( rs.getString("dstrct"));
                campo.setFormato( rs.getString("formato"));
                campo.setTabla(rs.getString("tabla"));
                campo.setCampo_tabla(rs.getString("campo_tabla"));
                campo.setCampo_jsp(rs.getString("campo_jsp"));
                campo.setTitulo(rs.getString("titulo"));
                campo.setTipo_campo(rs.getString("tipo_campo_equivalente"));
                campo.setValidar(rs.getString("validar"));
                campo.setTabla_val(rs.getString("tabla_val"));
                campo.setCampo_val(rs.getString("campo_val"));
                campo.setPrimaria(rs.getString("primaria"));
                campo.setOrden(rs.getInt("orden"));
                campo.setLongitud(rs.getDouble("longitud"));
                campo.setValor_campo("");
                vecbusqueda.add(campo);
                
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR LOS FORMATOS DE TABLAS EN TABLAGEN, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
    }
    
    /**
     * Getter for property VecCampos.
     * @return Value of property VecCampos.
     */
    public java.util.Vector getVecCampos() {
        return VecCampos;
    }
    
    /**
     * Setter for property VecCampos.
     * @param VecCampos New value of property VecCampos.
     */
    public void setVecCampos(java.util.Vector VecCampos) {
        this.VecCampos = VecCampos;
    }
    
       
    
    
    /**
     * Getter for property vecbusqueda.
     * @return Value of property vecbusqueda.
     */
    public java.util.Vector getVecbusqueda() {
        return vecbusqueda;
    }
    
    /**
     * Setter for property vecbusqueda.
     * @param vecbusqueda New value of property vecbusqueda.
     */
    public void setVecbusqueda(java.util.Vector vecbusqueda) {
        this.vecbusqueda = vecbusqueda;
    }
    
    /**
     * Metodo: formatoTabla, permite todos los formatos que tengan estructura
     * @autor : Ing. Diogenes Bastidas
     * @param : nombre tabla
     * @version : 1.0
     */
    public void formatoDefinidos ( String table_type ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        Vecformato              = null;
        String query = "SQL_FORMATOS";
        try {
            st = crearPreparedStatement (query);
            st.setString (1,table_type);
            rs= st.executeQuery ();
            fordefinidos = new Vector ();
            while(rs.next ()){
                ftabla = new Formato_tabla ();
                ftabla.setFormato ( rs.getString ("table_code"));
                ftabla.setNombre_formato ( rs.getString ("descripcion"));
                fordefinidos.add (ftabla);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR LOS FORMATOS DE TABLAS EN TABLAGEN, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar (query);
        }
    }
    
    /**
     * Getter for property fordefinidos.
     * @return Value of property fordefinidos.
     */
    public java.util.Vector getFordefinidos() {
        return fordefinidos;
    }
    
    /**
     * Setter for property fordefinidos.
     * @param fordefinidos New value of property fordefinidos.
     */
    public void setFordefinidos(java.util.Vector fordefinidos) {
        this.fordefinidos = fordefinidos;
    }
    
    //***Operez
    
     /**
     * Metodo: obtenerFormatosTablas, obtiene los distintos formatos y tablas existentes
     * @autor : Ing. Osvaldo P�rez Ferrer
     * @param : distrito
     * @version : 1.0
     */
    public Vector obtenerFormatosTablas( String distrito ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        Vecftabla               = null;
        Vecformato = new Vector();
        String query = "SQL_OBTENER_FORMATOS_TABLAS";
        try {
            st = crearPreparedStatement(query);
            st.setString(1,distrito);
            
            rs= st.executeQuery();
            
            while( rs.next() ){
                
                Hashtable h = new Hashtable();
                
                h.put("formato",        nvl( rs.getString("formato") ));
                h.put("tabla",          nvl( rs.getString("tabla") ));
                h.put("nombre_formato", nvl( rs.getString("nombre_formato") ));
                
                Vecformato.add( h );
            }
            
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.obtenerFormatosTablas, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return Vecformato;
    }
    
    /**
     * Metodo: obtenerCamposTabla, obtiene los campos de una tabla
     * @autor : Ing. Osvaldo P�rez Ferrer
     * @param : String distrito
     * @param : String formato, formato al que pertenece la tabla
     * @version : 1.0
     */
    public Vector obtenerCamposTabla( String distrito, String formato ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        Vecftabla               = null;
        
        Vector v;
        String query = "SQL_OBTENER_TABLA";
        
        try {
            st = crearPreparedStatement(query);
            
            st.setString( 1, formato);
            st.setString( 2, distrito);
            
            rs= st.executeQuery();
            
            v = new Vector();
            
            while( rs.next() ){
                
                Hashtable h = new Hashtable();
                
                h.put( "campo_tabla", nvl( rs.getString("campo_tabla") ) );
                h.put( "campo_jsp"  , nvl( rs.getString("campo_jsp") ) );
                h.put( "title"  ,     nvl( rs.getString("titulo") ) );
                h.put( "pk"  ,        nvl( rs.getString("primaria") ) );
                h.put( "tipo"  ,      nvl( rs.getString("tipo_campo_equivalente") ) );
                
                v.add( h );
                
            }
            
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.obtenerCamposTabla, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return v;
    }
    
    /***********************************************************
     * Metodo: obtenerDatosTabla, obtiene los datos de una tabla
     * @autor : Ing. Osvaldo P�rez Ferrer
     * @param : String distrito
     * @param : String conditions, condiciones a aplicar
     * @param : String campos, campos para seleccionar
     * @version : 1.0
     ***********************************************************/
    public Vector obtenerDatosTabla( String dstrct, String conditions, String campos ) throws Exception{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        Vecftabla               = null;
        
        Vector v = new Vector();
        String query = "SQL_GENERAL_FACTURA_MIGRACION";
        String sql   = "";
        
        try{
            
            st = crearPreparedStatement(query);
            
            sql = st.toString();
            sql = sql.replaceAll("#CAMPOS#", campos );
            sql = sql.replaceAll("#CONDITIONS#", " WHERE "+conditions );
            sql = sql+" AND reg_status != 'A' AND aprobado = 'N' AND user_aprobacion = ''";
            
            ////System.out.println("query --> "+sql);
            rs = st.executeQuery(sql);
            
            v = new Vector();
            String[] campo = campos.split(",");
            
            int c = 1;
            while( rs.next() ){
                Hashtable h = new Hashtable();
                h.put( "id", "F"+c );//Identificador del objeto
                h.put( "oid", rs.getString("id") );
                for( int i=0; i<campo.length;i++ ){
                    
                    h.put( campo[i].trim(),  rs.getString( campo[i].trim() ) );
                    
                }
                v.add( h );
                c++;
            }
            
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.obtenerDatosTabla, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return v;
    }
    
    
    
    
    
    /***********************************************************
     * Metodo: buscarDatosAdicionales, obtiene los datos adicionales
     *          a factura_migracion, para grabar en movpla
     * @autor : Ing. Osvaldo P�rez Ferrer
     * @param : String numpla, numero de planilla
     * @param : Movpla m, objeto movpla a completar
     * @version : 1.0
     ***********************************************************/
    public Movpla buscarDatosAdicionales( String numpla, Movpla m ) throws SQLException{
        
        PreparedStatement st    = null;
        ResultSet rs            = null;
        
        String query = "SQL_DATOS_ADICIONALES_MOVPLA";
        
        try{
            
            st = crearPreparedStatement( query );
            
            st.setString( 1, numpla );
            
            rs = st.executeQuery();
            ////System.out.println(" buscarDatosAdicionales says: "+st.toString());
            if( rs.next() ){
                
                m.setPla_owner(       nvl( rs.getString( "pla_owner" ) ) );
                m.setBase(            nvl( rs.getString( "base" ) ) );
                m.setBranch_code(     nvl( rs.getString( "branch_code" ) ) );
                m.setBank_account_no( nvl( rs.getString( "bank_account_no" ) ) );
                m.setBeneficiario(    nvl( rs.getString( "beneficiario")));
                m.setCausa("OK");//Este atributo es para verificar que se encontraron datos
            }
            
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.buscarDatosAdicionales, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return m;
    }
    
    
    
    /***********************************************************
     * Metodo: buscarDatosAdicionales, obtiene los datos adicionales
     *          a factura_migracion, para grabar en cxp o rxp
     * @autor : Ing. Osvaldo P�rez Ferrer
     * @param : String placa
     * @param : CXP_Doc c, objeto CXP_Doc a completar
     * @version : 1.0
     ***********************************************************/
    public CXP_Doc buscarDatosAdicionales( String placa, CXP_Doc c, String emp_servicio ) throws SQLException{
        
        PreparedStatement st    = null;
        ResultSet rs            = null;
        
        String query = "SQL_DATOS_ADICIONALES_CXP";
        String sql   = "";
        
        try{
            
            st = crearPreparedStatement( query );
            
            st.setString( 1, placa );
            
            sql = st.toString();
            
            //Se buscan los datos del propietario a partir de la placa
            //o con el nit 'emp_servicio' dado
            if( emp_servicio != null  ){
                sql = sql.replaceAll( "#COMMENT#", "--");
                sql = sql.replaceAll( "#FROM#", " FROM proveedor pr");
                sql = sql.replaceAll( "#WHERE#", " WHERE nit ='"+emp_servicio+"'" );
            }else{
                sql = sql.replaceAll( "#COMMENT#", "");
                sql = sql.replaceAll( "#FROM#", "");
                sql = sql.replaceAll( "#WHERE#", "");
            }
            ////System.out.println(" buscarDatosAdicionales says: "+sql);
            rs = st.executeQuery( sql );
            
            if( rs.next() ){
                
                c = new CXP_Doc();
                
                c.setProveedor(         nvl( rs.getString( "nit" ) ) );
                c.setBanco(             nvl( rs.getString( "banco" ) ) );
                c.setSucursal(          nvl( rs.getString( "sucursal" ) ) );
                c.setHandle_code(       nvl( rs.getString( "hc" ) ) );
                c.setPlazo(                  rs.getInt( "plazo"));
                c.setAgencia(           nvl( rs.getString( "agency_id" ) ) );
                c.setMoneda_banco(      nvl( rs.getString( "currency" ) ) ) ;
                c.setFecha_vencimiento( nvl( rs.getString( "vencimiento" ) ) );
                
            }
            
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.buscarDatosAdicionales(CXPRXP), "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return c;
    }
    
    /****************************************************************
     * Metodo formatoGrabaEn, obtiene la tabla en la que el formato
     *      dado, debe migrar registros(facturas)
     * @param: String formato, formato de tabla del que se desea migrar
     * @throws: SQLException, en caso de error de BD
     ****************************************************************/
    public String formatoGrabaEn( String formato ) throws SQLException{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String query = "SQL_GRABA_FACTURA_EN";
        String tabla = "";
        int c = 0;
        try{
            
            st = crearPreparedStatement( query );
            st.setString( 1,formato );
            
            rs = st.executeQuery();
            while( rs.next() ){
                c++;
                tabla = nvl( rs.getString("tabla") );
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.formatoGrabaEn, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return c==1? tabla : "error";
    }
    
    public String[] obtenerSerie()throws SQLException{
        
        String[] serieyprefijo = null;
        
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String query = "SQL_GET_SERIE";
        
        try{
            
            st = crearPreparedStatement( query );
            
            rs = st.executeQuery();
            if( rs.next() ){
                
                serieyprefijo = new String[2];
                
                serieyprefijo[0] = rs.getString("prefix");
                serieyprefijo[1] = UtilFinanzas.rellenar( (rs.getInt("last_number")+""), "0", 6);
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.formatoGrabaEn, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        
        return serieyprefijo;
        
    }
    
    public void actualizarSerie( String usuario ) throws SQLException{
        
        PreparedStatement st    = null;
        String query = "SQL_UPDATE_SERIE";
        
        try{
            
            st = crearPreparedStatement( query );
            st.setString( 1, usuario );
            st.executeUpdate();
            
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.actualizarSerie, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        
    }
    
    public String nvl( String value ){
        return value == null? "" : value.trim();
    }
    /*******************************************************************
     * Metodo que graba una discrepancia
     * @param: Movpla m, objeto con algunos datos de la discrepancia
     * @param: Hashtable h, objeto con algunos datos de la discrepancia
     * @author: Osvaldo P�rez Ferrer
     *******************************************************************/
    public void insertarDiscrepancia( Movpla m, Hashtable h ) throws SQLException{
        
        PreparedStatement st    = null;
        String query = "SQL_INSERT_DISCREPANCIA";
        
        try{
            
            st = crearPreparedStatement( query );
            
            st.setString( 1, m.getPlanilla() );
            st.setString( 2, nvl( (String)h.get("numrem") ) );
            st.setString( 3, nvl( (String)h.get("tipo_doc") ) );
            st.setString( 4, nvl( (String)h.get("documento") ) );
            st.setString( 5, nvl( (String)h.get("generador") )  );
            st.setString( 6, nvl( (String)h.get("retenido_pago") ).equals("")? "N" : nvl( (String)h.get("retenido_pago") ) );
            st.setString( 7, m.getCreation_user() );
            st.setString( 8, m.getDstrct() );
            st.setString( 9, nvl((String)h.get("observacion") ) );
            
            st.executeUpdate();
            
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.insertarDiscrepancia, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        
    }
    
    /*******************************************************************
     * Metodo que graba una sancion
     * @param: Hashtable h, objeto con datos de la sancion
     * @author: Osvaldo P�rez Ferrer
     *******************************************************************/
        
    /*********************************************************************
     * Metodo getCodigoCuenta, retorna el codigo de cuenta de un proveedor
     *        dependiendo del concept_class de tabla tblcon
     * @param String Distrito
     * @param String cedula del proveedor
     * @param String EL codigo del concepto
     * @param String El numero de planilla
     *********************************************************************/
    public String getCodigoCuenta( String dstrct, String proveedor, String concepto, String numpla ) throws Exception{
        
        PreparedStatement st = null;
        ResultSet         rs = null;
        
        String query = "SQL_GET_CODIGO_CUENTA";
        String codigo = null;
        
        try{
            if( numpla != null && concepto != null ){
                st = crearPreparedStatement( query );
                
                st.setString( 1, dstrct );
                st.setString( 2, proveedor );
                st.setString( 3, concepto );
                st.setString( 4, numpla );
                
                rs = st.executeQuery();
                
                if( rs.next() ){
                    codigo = nvl( rs.getString("codigo") );
                }
            }else{
                OpDAO op = new OpDAO();
                codigo   = op.getCuenta(dstrct, proveedor, "010", "");
            }
        }catch(Exception ex){
            throw new Exception( "ERROR en Formato_tablaDAO.getCodigoABC, "+ex.getMessage() );
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        
        return codigo;
    }
    
     /*******************************************************************
     * Metodo que graba una sancion
     * @param: Hashtable h, objeto con datos de la sancion
     * @author: Osvaldo P�rez Ferrer
     *******************************************************************/
    public void insertarSancion( Hashtable h, Movpla m ) throws SQLException{
        
        PreparedStatement st    = null;
        String query = "SQL_INSERT_SANCION";
        
        try{
            
            st = crearPreparedStatement( query );
            
            
            st.setString( 1, nvl( (String)h.get("numpla") ) );
            st.setDouble( 2, m.getValor() );
            st.setString( 3, m.getCreation_user() );
            st.setString( 4, "SFACT");
            st.setString( 5, m.getDstrct() );
            st.setString( 6, m.getBase() );
            st.setString( 7, "SANCION DESCUENTO PLANILLA");
            
            st.executeUpdate();
            
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.insertarSancion, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        
    }

    
    public String insertMovPla( Movpla m, String id ) throws SQLException{
        
        PreparedStatement st    = null;
        ResultSet rs            = null;
        
        String query = "SQL_INSERT_MOVPLA";
        String mens  = "";
        try{
            
            st = crearPreparedStatement(query);
            
            st.setString( 1,  m.getDstrct() );
            st.setString( 2,  m.getAgency_id() );
            st.setString( 3,  m.getDocument_type() );
            st.setString( 4,  m.getConcept_code() );
            st.setString( 5,  m.getPla_owner() );
            st.setString( 6,  m.getPlanilla() );
            st.setString( 7,  m.getSupplier() );
            st.setString( 8,  m.getApplicated_ind() );
            st.setString( 9,  m.getApplication_ind() );
            st.setString( 10, m.getInd_vlr() );
            st.setDouble( 11, m.getValor() );
            st.setDouble( 12, m.getVlr_for() );
            st.setString( 13, m.getCurrency() );
            st.setString( 14, m.getProveedor_anticipo() );
            st.setString( 15, m.getBranch_code() );
            st.setString( 16, m.getBank_account_no() );
            st.setString( 17, m.getBeneficiario() );
            st.setString( 18, m.getBase() );
            st.setString( 19, m.getCreation_user() );
            st.setDouble( 20, m.getValor() );//Valor descuento
            //Para actualizar factura_migracion
            st.setString( 21, m.getCreation_user() );
            st.setString( 22, m.getCreation_user() );
            st.setString( 23, id );                        
            
            ////System.out.println("insertMovPla says: "+st.toString() );
            
            st.executeUpdate();
            
            mens = "\nInsertado el registro concept_code "+m.getConcept_code()+
            ", planilla "+m.getPlanilla()+
            ", pla_owner "+m.getPla_owner()+
            ", creation_date "+Util.getFechaActual_String(4)+"....";
            
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.insertMovPla, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return mens;
    }
    public String insertCxpRxp( CXP_Doc c, String tabla, String id ) throws SQLException{
        
        PreparedStatement st    = null;
        ResultSet rs            = null;
        
        String query = "SQL_INSERT_CXP";
        String mens  = "";
        try{
            
            st = crearPreparedStatement(query);
            
            //cabecera
            
            st.setString( 1,  c.getDstrct() );
            st.setString( 2,  c.getProveedor() );
            st.setString( 3,  c.getTipo_documento() );
            st.setString( 4,  c.getDocumento() );
            st.setString( 5,  c.getDescripcion() );
            st.setString( 6,  c.getAgencia() );
            st.setString( 7,  c.getHandle_code() );
            st.setString( 8,  c.getBanco() );
            st.setString( 9,  c.getSucursal() );
            st.setString( 10, c.getMoneda() );
            st.setDouble( 11, c.getVlr_neto() );
            st.setDouble( 12, c.getVlr_saldo() );
            st.setDouble( 13, c.getVlr_neto_me() );
            st.setDouble( 14, c.getVlr_saldo_me() );
            st.setDouble( 15, c.getTasa() );
            st.setString( 16, c.getMoneda_banco() );
            st.setString( 17, c.getFecha_vencimiento() );
            st.setString( 18, c.getCreation_user() );
            st.setString( 19, c.getCreation_user() );
            
            //items
            
            st.setString( 20, c.getDstrct());
            st.setString( 21, c.getProveedor() );
            st.setString( 22, c.getTipo_documento() );
            st.setString( 23, c.getDocumento() );
            st.setString( 24, "001");
            st.setString( 25, c.getDescripcion() );
            st.setDouble( 26, c.getVlr_neto() );
            st.setDouble( 27, c.getVlr_neto_me() );
            st.setString( 28, c.getCorrida() );//Codigo de cuenta
            st.setString( 29, c.getAprobador() );
            st.setString( 30, c.getPeriodo() );//Planilla
            st.setString( 31, c.getCreation_user() );
            st.setString( 32, c.getProveedor() ); //Auxiliar,  OJO CONFIRMAR si lo q va aqui es el proveedor
            st.setString( 33, c.getCreation_user());
            //Para actualizar factura_migracion
            st.setString( 34, c.getCreation_user() );
            st.setString( 35, c.getProveedor() );
            st.setString( 36, c.getDocumento() );
            st.setString( 37, c.getCreation_user() );
            st.setString( 38, id );
            
            String sql = st.toString();
            sql = sql.replace("#TABLE#", tabla );
            
            if( tabla.equals("cxp") ){
                sql = sql.replace("#COLUMNS#", "" );
                sql = sql.replace("#VALUES#" , "" );
            }else{
                sql = sql.replace("#COLUMNS#", ",num_cuotas,fecha_inicio_transfer,frecuencia" );
                sql = sql.replace("#VALUES#" , ","+c.getNum_cuotas()+",now(),'"+c.getFrecuencia()+"'" );
            }
            
            //System.out.println(" EL SQL "+sql);
            
            actualizarSerie( c.getCreation_user() );
            
            st.executeUpdate( sql );
            
            mens = "\nInsertado el registro distrito "+c.getDstrct()+
            ", proveedor "+c.getProveedor()+
            ", documento "+c.getDocumento()+
            ", tipo_documento "+c.getTipo_documento();
            
        }catch(SQLException ex){
            throw new SQLException("ERROR en Formato_tablaDAO.insertCxpRxp, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{ st.close();}
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar(query);
        }
        return mens;
    }

}
