/***********************************************
 * Nombre       LiberarFacturasXCorridasDAO.java
 * Description  Permite ejecutar sql para llevar a cabo la liberaci�n de facturas en la corrida.
 * Autor        ING FERNEL VILLACOB DIAZ
 * Modifico     ING ANDRES MATURANA DE LA CRUZ
 * Fecha        12/04/206
 * Copyright    Transportes Sanchez Polo S.A.
 **********************************************/



package com.tsp.operation.model.DAOS;




import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;


import org.apache.log4j.*;


public class LiberarFacturasXCorridasDAO extends MainDAO {
    
    
    
    Logger logger = Logger.getLogger(this.getClass());
    private TreeMap treemap;
    
    
    public LiberarFacturasXCorridasDAO() {
        super("LiberarFacturasXCorridasDAO.xml");
    }
    public LiberarFacturasXCorridasDAO(String dataBaseName) {
        super("LiberarFacturasXCorridasDAO.xml", dataBaseName);
    }
    
    
    
    
    public String reset(String val){
        return  (val==null)?"":val;
    }
    
    
    
    
    public static void main(String[] args)throws Exception{
        LiberarFacturasXCorridasDAO  x = new LiberarFacturasXCorridasDAO();
        x.getCorridasParaLiberar("FINV");
    }
    
    
    
    
    /**
     * M�todo que obtiene las corridas para liberar a nivel de distrito, es para el proceso automatico
     * @autor       fvillacob
     * @param       String distrito
     * @throws      Exception
     * @version     1.1
     **/
    public String[] getCorridasParaLiberar(String distrito) throws Exception{
        Connection con = null;
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_CORRIDAS_ALL";
        List              list      = new LinkedList();
        String[]          runes     = null;
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );
            st.setString(2, distrito );
            
            rs = st.executeQuery();
            while( rs.next() )
                  list.add(  reset( rs.getString("corrida") )   );    
            
            
            
            if( list.size()>0 ){
                runes = new String[ list.size() ];
                for(int i=0;i<list.size();i++){
                    String cor = (String)list.get(i);
                    runes[i]   = cor;
                }
            }
            
            
            }}
        catch(SQLException e){
            throw new SQLException(" DAO: getCorridasParaLiberar.-->"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return runes;
    }
    
    
    
    
    
    /**
     * M�todo que obtiene las corridas disponibles del usuario
     * @autor       fvillacob
     * @param       String distrito, String login
     * @throws      Exception
     * @version     1.1
     **/
    public void getCorridasParaLiberar(String distrito, String login) throws Exception{
        Connection con = null;
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_CORRIDAS_LIB";
        this.treemap = new TreeMap();
        
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, login     );//AMATURANA 27.11.2006
            st.setString(3, distrito  );//AMATURANA 14.11.2006
            st.setString(4, login     );//AMATURANA 27.11.2006
            System.out.println( st.toString() );
            rs = st.executeQuery();
            while(rs.next()){
                String corr = rs.getString("corrida");
                this.treemap.put(corr, corr);
            }
            
        }}
        catch(SQLException e){
            throw new SQLException(" DAO: getCorridasParaLiberar.-->"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    
    
    
    /**
     * M�todo que obtiene las facturas para ser liberadas
     * @autor       fvillacob
     * @modified    Ing. Andr�s Maturana
     * @param       String distrito
     * @return      List
     * @throws      Exception
     * @version     1.1
     **/
/*    public List getFacturasParaLiberar(String distrito, String corrida, String user) throws Exception{
        
        Connection con = null;        
        PreparedStatement st = null;
        ResultSet rs         = null; 
        
        String            query      = "SQL_FACTURAS_NO_APROBADAS";
        String            query2     = "SQL_ES_CORRIDA_CHEQUE_CERO";
        List              lista      = new LinkedList();
        
        try {
            
            con = conectar( query2 );
            
            String cero = "";                      
            st          = con.prepareStatement( this.obtenerSQL(query2) );
            
            st.setString( 1, distrito );
            st.setString( 2, corrida );            
            rs = st.executeQuery();            
            if( rs.next() )
                cero = rs.getString("cheque_cero")!=null? rs.getString("cheque_cero") : "" ;
            
            
            st.clearParameters();            
            
            String sql = "";            
            st= con.prepareStatement( this.obtenerSQL( query ) );
            st.setString(1, distrito );
            st.setString(2, corrida  );
            st.setString(3, user     );
            st.setString(4, distrito );
            st.setString(5, corrida  );
            st.setString(6, user     );
            
            sql = st.toString();            
            
            String proceso_auto =  ( user.equals("ADMIN")    )? "--" : "" ;            
            sql                 =   cero.equals("S")? sql.replaceAll("#MENOR#", " < ") : sql.replaceAll("#MENOR#", " <= ");
            sql                 =   sql.replaceAll("#OMITIR_LINEA#", proceso_auto );             
            
            rs = st.executeQuery(sql);
            while(rs.next()){
                Hashtable  factura = new Hashtable();
                factura.put("distrito",       rs.getString("dstrct")         );
                factura.put("corrida",        rs.getString("corrida")        );
                factura.put("proveedor",      rs.getString("beneficiario")   );
                factura.put("tipo_documento", rs.getString("tipo_documento") );
                factura.put("documento",      rs.getString("documento")      );
                lista.add( factura );
            }
            
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getFacturasParaLiberar.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar( query2 );
        }
        return lista;
    }*/
    


    /**
     * M�todo que obtiene las facturas para ser liberadas
     * @autor       fvillacob
     * @modified    Ing. Andr�s Maturana
     * @param       String distrito
     * @return      List
     * @throws      Exception
     * @version     1.1
     **/
    public List getFacturasParaLiberar(String distrito, String corrida, String user) throws Exception{

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;

        String            query      = "SQL_FACTURAS_NO_APROBADAS";
        String            query2     = "SQL_ES_CORRIDA_CHEQUE_CERO";
        List              lista      = new LinkedList();

        try {

            con = this.conectarJNDI( query2 );//JJCastro fase2
            if(con!=null){
            String cero = "";
            st          = con.prepareStatement( this.obtenerSQL(query2) );
            st.setString( 1, distrito );
            st.setString( 2, corrida );
            rs = st.executeQuery();
            if( rs.next() )
            cero = rs.getString("cheque_cero")!=null? rs.getString("cheque_cero") : "" ;
            st.clearParameters();

            String sql = "";
            String proceso_auto =  ( user.equals("ADMIN")    )? "--" : "" ;
            sql = this.obtenerSQL(query);
            sql = cero.equals("S")? sql.replaceAll("#MENOR#", " < ") : sql.replaceAll("#MENOR#", " <= ");
            sql                 =   sql.replaceAll("#OMITIR_LINEA#", proceso_auto );

            st= con.prepareStatement( sql );
            st.setString(1, distrito );
            st.setString(2, corrida  );
            st.setString(3, user     );
            st.setString(4, distrito );
            st.setString(5, corrida  );
            st.setString(6, user     );
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable  factura = new Hashtable();
                factura.put("distrito",       rs.getString("dstrct")         );
                factura.put("corrida",        rs.getString("corrida")        );
                factura.put("proveedor",      rs.getString("beneficiario")   );
                factura.put("tipo_documento", rs.getString("tipo_documento") );
                factura.put("documento",      rs.getString("documento")      );
                lista.add( factura );
            }
        }}
        catch(SQLException e){
            throw new SQLException(" DAO: getFacturasParaLiberar.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }


    
    
    
    
    
    /**
     * M�todo que obtiene las facturas para ser liberadas
     * @autor   fvillacob
     * @param   String distrito
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public String obtenerSQLLiberacion( String distrito, String corrida, String proveedor, String tipo_documento, String documento) throws Exception{
        StringStatement stDelete       = null;
        StringStatement stUpdate       = null;
        PreparedStatement ps       = null;
        String            queryDelete    = "SQL_DELETE_FACTURAS_CORRIDAS";
        String            queryUpdate    = "SQL_UPDATE_FACTURAS";
        String            sql            = "";
        System.out.println("LLego aqui!!!");

        Connection con=null;
        try {
            stDelete = new StringStatement (this.obtenerSQL(queryDelete), true);//JJCastro fase2
            stUpdate = new StringStatement (this.obtenerSQL(queryUpdate), true);//JJCastro fase2
            // Eliminamos:
            stDelete.setString(1, distrito       );
            stDelete.setString(2, corrida        );
            stDelete.setString(3, tipo_documento );
            stDelete.setString(4, documento      );
            stDelete.setString(5, proveedor      );

            sql += stDelete.getSql();//JJCastro fase2

            // Actualizamos:
            stUpdate.setString(1, distrito       );
            stUpdate.setString(2, proveedor      );
            stUpdate.setString(3, tipo_documento );
            stUpdate.setString(4, documento      );

            sql += stUpdate.getSql() ;//JJCastro fase2

            con=conectarJNDI("SQL_UPDATE_FACTURAS");
            ps=con.prepareStatement(sql);
            ps.execute();
        }catch(SQLException e){
            throw new SQLException(" DAO: obtenerSQLLiberacion.-->"+ e.getMessage());
        }finally{
            if (stDelete  != null){ try{ stDelete = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (stUpdate  != null){ try{ stUpdate = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if(ps!=null){ps.close();}
            if(con!=null){desconectar(con);}
        }

        return sql;
    }

    
    
    
    
    
    /**
     * Getter for property treemap.
     * @return Value of property treemap.
     */
    public java.util.TreeMap getTreemap() {
        return treemap;
    }
    
    /**
     * Setter for property treemap.
     * @param treemap New value of property treemap.
     */
    public void setTreemap(java.util.TreeMap treemap) {
        this.treemap = treemap;
    }
    
    
    
    
    
    
    
    
}
