/*
 * ZonaDAO.java
 *
 * Created on 13 de julio de 2005, 09:00 AM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;


/**
 *
 * @author  Henry
 */
public class ZonaUsuarioDAO {
          
    public ZonaUsuarioDAO() {
    }
    
    private ZonaUsuario zonausuario;
    private List zonas;
    private Vector VecZonaUsuario; 
    private static final String SQL_LISTAR_ZONAS_USUARIOS    = "SELECT * FROM ZONAUSUARIO WHERE REC_STATUS<>'A' ORDER BY ZONA";
    private static final String SQL_INSERTAR_ZONA_USUARIO    = "INSERT INTO ZONAUSUARIO (zona, nomusuario, base, creation_user) VALUES(?,?,?,?)";
    private static final String SQL_EXISTE_ZONA_USUARIO      = "SELECT * FROM ZONAUSUARIO WHERE ZONA=? AND REC_STATUS<>'A'"+
                                                               "AND upper(NOMUSUARIO)=?";
    private static final String SQL_ACTUALIZAR_ZONA_USUARIO  = "UPDATE ZONAUSUARIO SET nomusuario=?, user_update = ?, last_update = 'now()' "+
                                                               "WHERE ZONA=? AND upper(NOMUSUARIO)=?";
    private static final String SQL_ELIMINAR_ZONA_USUARIO    = "UPDATE ZONAUSUARIO SET rec_status = 'A', user_update = ?, last_update = 'now()' where "+
                                                               "ZONA=? AND upper(NOMUSUARIO)=?";
    private static final String SQL_EXISTE_ZONA             = "SELECT * FROM ZONA WHERE CODZONA=?";
    private static final String SQL_EXISTE_USUARIO          = "SELECT * FROM USUARIOS WHERE IDUSUARIO=?";
    private static final String SQL_ZONAU_ANULADA           = "SELECT * FROM ZONAUSUARIO WHERE ZONA=? AND upper(NOMUSUARIO)=? AND REC_STATUS='A'";
    private static final String SQL_DESANULAR               = "UPDATE ZONAUSUARIO SET REC_STATUS='' WHERE ZONA=? AND upper(NOMUSUARIO)=?";
    

    public void setZonaUsuario(ZonaUsuario zon){        
        this.zonausuario = zon;        
    }
    
    // insertar zona en BD
    public void insertarZonaUsuario() throws SQLException {
        Connection con= null;
        PreparedStatement st = null, st2 = null;
        ResultSet rs = null, rs2 = null;       
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            ////System.out.println("------------------------ codigo de zona: "+zonausuario.getCodZona());
            if (con != null){
                st2 = con.prepareStatement(SQL_ZONAU_ANULADA);
                st2.setString(1, zonausuario.getCodZona());
                st2.setString(2, zonausuario.getNomUsuario());
                rs2 = st2.executeQuery();
                if (rs2.next()){
                    st = con.prepareStatement(SQL_DESANULAR);
                    st.setString(1, zonausuario.getCodZona());
                    st.setString(2, zonausuario.getNomUsuario());
                    st.execute();
                } else {
                    st = con.prepareStatement(SQL_INSERTAR_ZONA_USUARIO);
                    st.setString(1, zonausuario.getCodZona());
                    st.setString(2, zonausuario.getNomUsuario()); 
                    st.setString(3, zonausuario.getBase());
                    st.setString(4, zonausuario.getCreation_user());
                    st.executeUpdate();
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR ZONAUSUARIO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
    }    
   
    public boolean existeZonaUsuario (String codZona, String nomUsr) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(SQL_EXISTE_ZONA_USUARIO);
                st.setString(1, codZona);
                st.setString(2, nomUsr);
                ////System.out.println(st);
                rs = st.executeQuery();
                return rs.next();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return false;
    }    
     public boolean existeZona(String codZona) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(this.SQL_EXISTE_ZONA);
                st.setString(1, codZona);                
                rs = st.executeQuery();
                return rs.next();
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return false;
    }    
    public boolean existeUsuario(String usr) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(this.SQL_EXISTE_USUARIO);
                st.setString(1, usr);                
                rs = st.executeQuery();
                return rs.next();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return false;
    }
    //obtiene la lista de ZONAS
    public List obtenerZonas (){
        return zonas;
    }
    public Vector obtZonas (){
        return VecZonaUsuario;
    }   
    public void listarZonas ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        zonas = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_LISTAR_ZONAS_USUARIOS);
                rs = st.executeQuery();                
                zonas =  new LinkedList();                
                while (rs.next()){
                    zonas.add(ZonaUsuario.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                      throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
        
    }
    public ZonaUsuario obtenerZonaUsuario()throws SQLException{
        return zonausuario;
    }          
    public void zonasUsuarios ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        VecZonaUsuario = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_LISTAR_ZONAS_USUARIOS);
                rs = st.executeQuery();
                
                VecZonaUsuario =  new Vector();
                
                while (rs.next()){
                    VecZonaUsuario.add(ZonaUsuario.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                      throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
        
    }
     // retorna el objeto ZONA de pendiendo el codigo
    public void buscarZonaUsuario (String cod, String nom) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        boolean sw = false;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(this.SQL_EXISTE_ZONA_USUARIO);
                st.setString(1, cod);
                st.setString(2, nom);
                rs = st.executeQuery();                
                if (rs.next()){
                   zonausuario = ZonaUsuario.load(rs);                                     
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        
    }
    
    public void buscarZonaUsuarioNombre (String nom) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        boolean sw = false;
        VecZonaUsuario = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
            st = con.prepareStatement("select * from zonausuario where rec_status<>'A' and nomusuario like ? order by zona");
                st.setString(1, nom);
                rs = st.executeQuery();                
                VecZonaUsuario =  new Vector();                
                while (rs.next()){
                    VecZonaUsuario.add(ZonaUsuario.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        
    }    
    public void modificarZonaUsuario (String viejo) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ACTUALIZAR_ZONA_USUARIO);
                st.setString(1, zonausuario.getNomUsuario());
                st.setString(2, zonausuario.getUser_update());
                st.setString(3, zonausuario.getCodZona());
                st.setString(4, viejo);                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR ZONA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
        
    }
    public void eliminarZonaUsuario () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ELIMINAR_ZONA_USUARIO);
                st.setString(1, zonausuario.getUser_update());
                st.setString(2, zonausuario.getCodZona());                
                st.setString(3, zonausuario.getNomUsuario());
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR ZONA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
        
    }
}
