/*********************************************************************************
 * Nombre clase :           ReporteRemesasPorFacturarDAO.java                         *
 * Descripcion :            DAO del ReporteRemesasPorFacturarDAO.java                 *
 *                          Clase que maneja los DAO (Data Access Object) los cuales  *
 *                          contienen los metodos que interactuan con la B.D.         *
 * Autor :                  LREALES
 * Modificado y Corregido:  Osvaldo P�rez Ferrer
 * Fecha :                  17 de julio de 2006, 11:00 AM                             *
 * Version :                1.0                                                       *
 * Copyright :              Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import com.tsp.util.connectionpool.PoolManager;

public class ReporteRemesasPorFacturarDAO extends MainDAO {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private ReporteRemesasPorFacturar reporte;
    
    private Vector remesa;
    
    /** Creates a new instance of ReporteRemesasPorFacturarDAO */
    public ReporteRemesasPorFacturarDAO() {
        
        super( "ReporteRemesasPorFacturarDAO.xml" );
        
    }
    
    /**
     * Setter for property remesa.
     * @param plaman New value of property remesa.
     */
    public void setRemesa(java.util.Vector remesa) {
        
        this.remesa = remesa;
        
    }
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.util.Vector getRemesa() {
        
        return remesa;
        
    }
    
    
    /**
     * Funcion publica que obtiene la informaci�n que se va a generar
     * en el reporte de remesas por facturar.
     */
    public void RemesasPorFacturar( String distrito, String fechaini, String fechafin, String agencia_duenia, String agencia_facturadora, String anual, String anio ) throws Exception {
        
        Connection con = null;
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            String remplazar = "";
            remplazar = agencia_duenia.equals("")?remplazar:remplazar + " AND cl.agduenia = '" + agencia_duenia + "'";
            remplazar = agencia_facturadora.equals("")?remplazar:remplazar + " AND cl.agfacturacion = '" + agencia_facturadora + "'";
            
            String agregar = "";
            String sql = this.obtenerSQL("SQL_LISTA");
            
            agregar = sql.replaceAll("#FILTROS#", remplazar);
            
            con = this.conectar("SQL_LISTA");
            
            if( anual.equals("NO") ){
                agregar = agregar.replaceAll("#PERIODO#", " AND r.fecrem BETWEEN ? AND ?");
                
                st = con.prepareStatement( agregar );
                
                st.setString( 1, distrito );
                st.setString( 2, fechaini );
                st.setString( 3, fechafin );
            } else {
                agregar = agregar.replaceAll("#PERIODO#", " AND TO_CHAR(r.fecrem, 'YYYY') = ?");
                
                st = con.prepareStatement( agregar );
                
                st.setString( 1, distrito );
                st.setString( 2, anio );
            }
            
            //logger.info( "Consulta Reporte: " + st.toString() );
            //System.out.println("Consulta Reporte: \n" + st );
            rs = st.executeQuery();
            
            String fecha_actual     = Util.fechaActualTIMESTAMP();
            String anio_reporte     = fecha_actual.substring( 0, 4 );
            String mes_reporte      = fecha_actual.substring( 5, 7 );
            String dia_reporte      = fecha_actual.substring( 8, 10 );
            
            this.remesa = new Vector();
            
            while( rs.next() ){
                
                
                String creation_date    = reset( rs.getString( "creation_date" ) );
                creation_date           = creation_date.length()== 10? creation_date +" 00:00:00" : creation_date;
                int day                 = Util.diasTranscurridos( creation_date );
                String dias             = String.valueOf( day );
                
                
                
                reporte = new ReporteRemesasPorFacturar();
                
                reporte.setOt(              reset( rs.getString("numrem") ) );
                //reporte.setValor_ot(        resetN( rs.getString( "vlrrem" ) ) );
                reporte.setValor_ot(        resetN( rs.getString( "vlrrem2" ) ) );//AMATURANA 10.05.2007
                reporte.setAge_ori(         reset( rs.getString( "agasoc_ori" ) ) );
                reporte.setAge_dest(        reset( rs.getString( "agasoc_des" ) ) );
                reporte.setFec_desp(        reset( rs.getString( "fecdsp" ) ) );
                
                reporte.setFec_trafico(     reset( rs.getString("fechareporte") ) );
                reporte.setEntregada(       reporte.getFec_trafico().equals("")? "NO":"SI" );
                reporte.setAge_cump(        reset( rs.getString( "id_agencia" ) ) );
                reporte.setFec_cump(        reset( rs.getString( "feccum" ) ) );
                reporte.setCumplida(        reporte.getFec_cump().equals("")?"NO":"SI" );
                reporte.setOc(              reset( rs.getString( "numpla" ) ) );
                reporte.setCliente(         reset( rs.getString( "nomcli" ) ) );
                reporte.setCod_cliente(     reset( rs.getString( "cliente" ) ) );
                reporte.setRuta(            reset( rs.getString( "ruta_pla" ) ) );
                reporte.setAge_duenia(      reset( rs.getString( "agduenia" ) ) );
                
                String doc_int =            reset( rs.getString( "docuinterno" ) ).equals("")? reset( rs.getString( "documento" ) ) : reset( rs.getString( "docuinterno" ) );
                
                reporte.setDoc_int(         doc_int );
                reporte.setStd_job(         reset( rs.getString( "std_job_no" ) ) );
                reporte.setDescripcion_ot(  reset( rs.getString( "descripcion" ) ) );
                reporte.setAux_integral(    reset( rs.getString( "aux_facturacion" ) ) );
                reporte.setDias( dias );
                reporte.setAnio_cump(       reset( rs.getString( "anio_cump" ) ) );
                reporte.setMes_cump(        reset( rs.getString( "mes_cump" ) ) );
                reporte.setDia_cump(        reset( rs.getString( "dia_cump" ) ) );
                reporte.setAnio_reporte(    anio_reporte );
                reporte.setMes_reporte(     mes_reporte );
                reporte.setDia_reporte(     dia_reporte );
                reporte.setValor_oc(        resetN( rs.getString( "vlrpla" ) ) );
                reporte.setCant_oc(         resetN( rs.getString( "cantidad" ) ) );
                reporte.setPlaca(           reset( rs.getString( "plaveh" ) ) );
                reporte.setWo_type(         reset( rs.getString( "wo_type" ) ) );
                reporte.setAnio_desp(       reset( rs.getString( "anio_desp" ) ) );
                reporte.setMes_desp(        reset( rs.getString( "mes_desp" ) ) );
                reporte.setCumplidor(       reset( rs.getString( "creation_user" ) ) );
                reporte.setTipo_entrega(    reset( rs.getString("tipo_reporte") ) );
                reporte.setOt_padre(        reset( rs.getString("ot_padre") ) );
                reporte.setAsociadas_ot_padre( reset( rs.getString("asociadas") ) );
                reporte.setFec_envio_fis(   reset( rs.getString("fecha_envio") ) );
                reporte.setFec_envio_log(   reset( rs.getString("fecha_envio_logico") ) );
                reporte.setFec_recibo_fis(  reset( rs.getString("fecha_recibido") ) );
                reporte.setFec_recibo_log(  reset( rs.getString("fecha_recibido_logico") ) );
                reporte.setEstado_ot(       reset( rs.getString("sta_rem") ) );
                reporte.setFiduciaria( reset( rs.getString("fiduciaria") ) );//AMATURANA 31.03.2007
                                                
                String fec_cump = reporte.getFec_cump();
                
                fec_cump = fec_cump.length()== 10? fec_cump +" 00:00:00" : fec_cump;
                
                String vencimiento = fec_cump.equals("")? "-" : String.valueOf( Util.diasTranscurridos( fec_cump, fecha_actual ));
                                               
                reporte.setVencimiento( vencimiento );
                reporte.setAge_facturacion( reset( rs.getString( "agfacturacion" ) ) );
                reporte.setEsVacio(         reset( rs.getString( "vacio" ) ) );
                
                this.remesa.add( reporte );
                
            }
            
        } catch ( java.lang.OutOfMemoryError me ){
            throw new Exception( "ERROR DURANTE 'RemesasPorFacturar()' - [ReporteRemesasPorFacturarDAO].. " + me.getMessage() );
        }
        catch( Exception e ){
            throw new Exception( "ERROR DURANTE 'RemesasPorFacturar()' - [ReporteRemesasPorFacturarDAO].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( Exception e ){
                    
                    //e.printStackTrace();
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_LISTA" );
            
        }
        
    }
    
    
    public String reset( String value ){
        return value!=null? value.toUpperCase() : "";
    }    
    
    public String resetN( String value ){
        return value!=null? value.toUpperCase() : "0";
    }
    public static void main(String[]akjs) throws Exception{
        ReporteRemesasPorFacturarDAO r = new ReporteRemesasPorFacturarDAO();
        r.RemesasPorFacturar("FINV", "2007-01-01", "2007-01-07", "", "", "NO", "");
    }
    
}

