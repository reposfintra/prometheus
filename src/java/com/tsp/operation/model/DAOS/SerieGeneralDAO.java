/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.operation.model.beans.*;



/**
 *
 * @author Alvaro
 */
public class SerieGeneralDAO extends MainDAO{



    /** Creates a new instance of ApplusDAO */
    public SerieGeneralDAO() {
        super("SerieGeneral.xml");
    }
    public SerieGeneralDAO(String dataBaseName) {
        super("SerieGeneral.xml", dataBaseName);
    }



    public SerieGeneral getSerie (String dstrct, String  agency_id, String document_type)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs               = null;
        String            query    = "SQL_GET_ULTIMO_NUMERO";
        SerieGeneral      serie    = null;

        try {
            con   = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, dstrct );
            st.setString( 2, agency_id );
            st.setString( 3, document_type );

            rs = st.executeQuery();

            if(rs.next()){
                serie = new SerieGeneral();
                serie = (SerieGeneral.load(rs));
            }

        }}catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA BUSQUEDA DE UNA SERIE. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
             if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return serie;

    }

    public void setSerie (String dstrct, String  agency_id, String document_type)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String            query    = "SQL_SET_ULTIMO_NUMERO";

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, dstrct );
            st.setString( 2, agency_id );
            st.setString( 3, document_type );
            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA SERIE.  \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }

public SerieGeneral getSerie (String dstrct, String  agency_id, String document_type,String dataBaseName)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs               = null;


        String            query    = "SQL_GET_ULTIMO_NUMERO";
        SerieGeneral      serie    = null;


        try {


            con   = this.conectarJNDI(query, dataBaseName );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, dstrct );
            st.setString( 2, agency_id );
            st.setString( 3, document_type );


            rs = st.executeQuery();

            if(rs.next()){
                serie = new SerieGeneral();
                serie = (SerieGeneral.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA BUSQUEDA DE UNA SERIE. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return serie;

    }







    public void setSerie (String dstrct, String  agency_id, String document_type, String dataBaseName)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String            query    = "SQL_SET_ULTIMO_NUMERO";

        try {
            con   = this.conectarJNDI( query, dataBaseName );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, dstrct );
            st.setString( 2, agency_id );
            st.setString( 3, document_type );

            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA SERIE.  \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

    }



}
