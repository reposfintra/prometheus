/*
 * TBLActividadesDAO.java
 *
 * Created on 13 de octubre de 2005, 08:52 AM
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import java.util.*;
import java.text.*;
/**
 *
 * @author  LEONARDO PARODY
 */
public class TBLActividadesDAO {
   
    public TBLActividadesDAO(){
        
    }
    private String Codigo;
    private String Descripcion;
    private String Sigla;
    
    private final String SQL_TBLActividad = "select table_code as activity_type, descripcion as activity_name from tablagen where table_type = 'TBLACT' and table_code = ?";
    private final String SQL_TBLActividades = "select table_code as activity_type, descripcion as activity_name from tablagen where reg_status <> 'A' and table_type = 'TBLACT'";
    
    ///////////////////////////////////////////////////
    ////      Consulta una actividad segun codigo  ////
    ///////////////////////////////////////////////////
    
    public TBLActividades ConsultarTBLActividad (String codigo, String distrito) throws SQLException{
        ////System.out.println("---------------------DAO---------------"+codigo+"-------"+distrito);
        TBLActividades actividad = new TBLActividades();
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_TBLActividad);
                st.setString(1,codigo);
            //    st.setString(2,distrito);
                rs = st.executeQuery();
                while (rs.next()){
                    actividad = new TBLActividades();          
                    actividad.setCodigo( rs.getString("activity_type") );                            
                    actividad.setDescripcion( rs.getString("activity_name") );
                    //actividad = actividad.load(rs);
                }
            }
            ////System.out.println("actividad ---------- desc-----"+actividad.getDescripcion());
            return actividad;
            }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                      throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    ///////////////////////////////////////////////////
    ////       Lista todas las actividadades       ////
    ///////////////////////////////////////////////////
    
    public List ConsultarTBLActividades (String distrito) throws SQLException{
        List actividades = null;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_TBLActividades);
               // st.setString(1,distrito);
                rs = st.executeQuery();
                actividades =  new LinkedList();
                ////System.out.println(" -------SQL-----"+st);
                while (rs.next()){
                    TBLActividades actividad = new TBLActividades();
                    actividad = new TBLActividades();          
                    actividad.setCodigo( rs.getString("activity_type") );                            
                    actividad.setDescripcion( rs.getString("activity_name") );
                    actividades.add(actividad);
                    ////System.out.println(" ------------------------- DAO -------------- "+ actividad.getCodigo() + "----" + actividad.getDescripcion());
                }
            }
            return actividades;
            }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                      throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
     //////////////////////////////////////////////////
    ////      Inserta o Modifica una actividadades ////
    ///////////////////////////////////////////////////
    
    public void InsertarActividad(TBLActividades actividad) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        ResultSet rs = null;  
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st=con.prepareStatement("select activity_name from tblact where activity_type = ? and dstrct_code=?");
                st.setString(1, actividad.getCodigo());
                st.setString(2, actividad.getDstrct_code());
                rs = st.executeQuery();
                if (rs.next()){
                    if (con != null){
                        st= con.prepareStatement("update tblact set reg_status = '', activity_name = ?,sigla = ?, last_update = now(), user_update = ? where  activity_type = '"+actividad.getCodigo()+"'");
                        st.setString(1, actividad.getDescripcion());
                        st.setString(2, actividad.getSigla());
                        st.setString(3, actividad.getUser_update());
                        st.executeUpdate();
                    }
                }else {
                    if (con != null){
                        st = con.prepareStatement("insert into tblact values ('', ?, ?, ?, now(), ?, now(), ?,?)");
                        st.setString(1, actividad.getDstrct_code());
                        st.setString(2, actividad.getCodigo());
                        st.setString(3, actividad.getDescripcion());
                        st.setString(4, actividad.getUser_update());
                        st.setString(5,  actividad.getCreation_user());
                        st.setString(6, actividad.getSigla());
                        ////System.out.println("Insert query == "+st);
                        st.executeUpdate();
                        ////System.out.println("UPDATE query == "+st);
                    }
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR LA ACTIVIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
        
     public void AnularActividad(TBLActividades actividad) throws SQLException{
            Connection con=null;
            PreparedStatement st = null;
            PoolManager poolManager = null;
            ResultSet rs = null;  
            try{
                poolManager = PoolManager.getInstance();
                con = poolManager.getConnection("fintra");
                if (con != null){
                        if (this.ExisteTBLActividad(actividad.getCodigo(), actividad.getDstrct_code())==true){
                            st=con.prepareStatement("update tblact set reg_status = 'A', user_update = ?, last_update = now()  where  activity_type = '"+actividad.getCodigo()+"' and dstrct_code = ?");
                            st.setString(1, actividad.getUser_update());
                            st.setString(2, actividad.getDstrct_code());
                            st.executeUpdate();
                        }
                }
            }catch(SQLException e){
                 throw new SQLException("ERROR AL ANULAR LA ACTIVIDAD" + e.getMessage()+"" + e.getErrorCode());
            }finally{
                if(st != null){
                    try{
                        st.close();
                       }
                       catch(SQLException e){
                           throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                       }
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con );
                }
            }
        }
     
        public boolean ExisteTBLActividad(String codigo, String distrito) throws SQLException{
            boolean sw = true;
            Connection con=null;
            PreparedStatement st = null;
            PoolManager poolManager = null;
            ResultSet rs = null;  
            try{
                poolManager = PoolManager.getInstance();
                con = poolManager.getConnection("fintra");
                if (con != null){
                    st=con.prepareStatement("select * from tblact where  activity_type = '"+codigo+"' and dstrct_code = ?");
                    st.setString(1, distrito);
                    rs = st.executeQuery();
                    if (rs.next()){
                      sw = true;  
                    }else{
                        sw = false;
                    }
               }
                return sw;
            }catch(SQLException e){
                 throw new SQLException("ERROR AL ANULAR LA ACTIVIDAD" + e.getMessage()+"" + e.getErrorCode());
            }finally{
                if(st != null){
                    try{
                        st.close();
                       }
                       catch(SQLException e){
                           throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                       }
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con );
                }
            }
        }
}
