
/**********************************************************************************
 * Nombre clase : ............... ReporteOCVacioAnticipoDAO                        *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD para generar los datos del reporte     *
 *                                de OC con Anticipo sin tr�fico.                  *
 * Autor :....................... Ing. Enrique De Lavalle Rizo                     *
 * Fecha :........................ 23 de noviembre de 2006, 01:36 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;

public class ReporteOCVacioAnticipoDAO extends MainDAO{
    
    /** Creates a new instance of ReporteOCAnticipoDAO */
    public ReporteOCVacioAnticipoDAO() {
        super("ReporteOCAnticipoDAO.xml");
    }
    public ReporteOCVacioAnticipoDAO(String dataBaseName) {
        super("ReporteOCAnticipoDAO.xml", dataBaseName);
    }
    
    
    
    
    /* LISTADO
     * @autor : Ing. Enrique De Lavalle Rizo 
     * @param : String numpla
     */
    public String getDiscrepancias( String numpla ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_DISCREPANCIA_OC";
        String discrepancia  = "";
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, numpla);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    discrepancia = rs.getString("discrepancia");
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return discrepancia;
    }
    
    /* LISTADO
     * @autor : Ing. Enrique De Lavalle Rizo 
     * @param : String numrem
     * @param : String ruta_pla
     * @param : String fechai
     * @param : String fechaf
     * @param : String distrito
     */
    public String getOcasociadas( String numrem , String ruta_pla, String numpla ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_OC_VACIA_ASOCIADAS";
        String ocasociadas   = "",  par = "";        
        Vector partes = new Vector();
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, numrem );
            st.setString(2, ruta_pla);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    ocasociadas = rs.getString("ocasociadas");
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        //se sacan los num de planillas q estasn repetidas dentro de las asociadas, solo se debe mostrar una vez
        StringTokenizer tokens = new StringTokenizer(ocasociadas,",");
        while (tokens.hasMoreTokens()) { 
            par = tokens.nextToken();
            if (!par.equals(numpla)){      
            
                if (partes.isEmpty()){
                    partes.add(par);
                }
                else{
                    if(!partes.contains(par)){
                        partes.add(par);
                    }
                }
            }
        }
        if (partes.isEmpty()){
            return ("");
        }else{
            return partes.toString();
        }
        
    }
    
         
    /* LISTADO
     * @autor : Ing. Enrique De Lavalle
     * @param : String fechainicial
     * @param : String fechafinal
     * @param : String distrito
     */
    public List List_oc_vacias( String fechai, String fechaf, String distrito ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_OC_VACIOS";
        List lista           = new LinkedList();
        List rList           = new LinkedList();
        boolean sw;
        int c =0;
        ReporteOCVacioAnticipo  reporte = new ReporteOCVacioAnticipo();
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, fechai );
            st.setString(2, fechaf );
                        
            rs=st.executeQuery();
            
            if(rs!=null){
                
                while(rs.next()){
                    sw = true;  
                    reporte = ReporteOCVacioAnticipo.load2(rs);                    
                                       
                    /*Discrepancia*/
                    if( !this.getDiscrepancias(reporte.getNumpla()).equals(""))
                        sw = false;                    
                          
                    /*Placa*/
                    if( ( reporte.getPlatlr().equals("TPO518") || reporte.getPlatlr().equals("KDC513") || reporte.getPlatlr().equals("SRJ621") || reporte.getPlatlr().equals("QGU515") ) && /*Viajes*/( reporte.getAgc_destino().equals(reporte.getAgc_origen())  ) )
                        sw = false;
                    
                    /*Validaciones Placas*/                    
                    if( sw ){
                        int genOc =0, dias =0;                  
                        
                        if (Integer.parseInt(reporte.getDif())>0)
                            genOc = Integer.parseInt(reporte.getDif()) - this.getFestivos(reporte.getCreation_date(),reporte.getPrinter_date(), reporte.getPais());
                        
                        if (Integer.parseInt(reporte.getDias())>0)
                           dias = Integer.parseInt(reporte.getDias()) - this.getFestivos(reporte.getCreation_date(),Util.getFechaActual_String(8), reporte.getPais());
                                                    
                        reporte.setDif(String.valueOf(genOc));
                        reporte.setDias(String.valueOf(dias));
                        reporte.setOcasociadas(this.getOcasociadas(reporte.getNumrem(),reporte.getRuta_pla(),reporte.getNumpla()));
                                                
                        reporte.setContaroc("1");
                        
                        if (rList.isEmpty()){
                            lista.add(reporte); 
                            rList.add(reporte);
                        }
                        else{
                            if (!existeNumPla(reporte.getNumpla(),rList)){
                                lista.add(reporte); 
                                rList.add(reporte);
                            }
                                
                        }
                                         
                    }                  
                }
            }
            
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Error en rutina LIST [ReporteOCVacioAnticipoDAO_List_oc_vacias].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        
        for (int j=0; j<lista.size();j++){
           // //System.out.println("esta: "+rList.contains(lista.get(j)));
            if (!rList.contains(lista.get(j))){
                rList.add(lista.get(j));
            }
        }
        return rList;
    }
    
    
    /* existeNumPla
     * @autor : Ing. Enrique De Lavalle Rizo
     * @param : List, List 
     */
    public boolean existeNumPla (String numPla, List rList){
        ReporteOCVacioAnticipo  reporte;
        for (int i=0; i<rList.size();i++){
            reporte = (ReporteOCVacioAnticipo) rList.get(i);
            if (numPla.equals(reporte.getNumpla())){
                return true;
            }
        }                    
        return false;
    }
    
    
     /* getFestivos
     * @autor : Ing. Enrique De Lavalle Rizo
     * @param : String 
     */
    public int getFestivos( String fechai, String fechaf, String pais ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_FESTIVOS";
        int festivos = 0;
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, fechai );
            st.setString(2, fechaf );
            st.setString(3, pais );
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    festivos = rs.getInt("cant_festivos");
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return festivos;
    }
   

}

/***************************************************************************
 Entregado a elavalle 12 Febrero 2007
 ***************************************************************************/
