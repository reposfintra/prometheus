/********************************************************************
 *  Nombre Clase.................   ReportePlacaFotosDAO.java
 *  Descripci�n..................   DAO de la tabla (Placa, TblImagen y Nit)
 *  Autor........................   Ing. Leonardo Parodi
 *  Fecha........................   06.01.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import javax.swing.*;
import java.text.*;

/**
 *
 * @author  EQUIPO12
 */
public class ReportePlacaFotosDAO extends MainDAO{
    private String agencia="";
    
    //calculo de tiempos
    private String estado = "";
    private String ultrep = "";
    private double dempla = 0;
    private String fecultrep = "0099-01-01 00:00:00";
    
    /** Creates a new instance of ReportePlacaFotosDAO */
    public ReportePlacaFotosDAO()  {
        super("ReportePlacaFotosDAO.xml");
    }
    
    
    
    /**
     * Metodo ReportePlacaFoto , Metodo que obtiene si una placa tiene foto o no
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @parameter : String fechaI
     * @parameter : String fechaF
     * @parameter : String agencia
     * @retorna una lista de registros de placas y sus fotos
     * @version : 1.0
     */
    public List ReportePlacaFoto(String fechaI, String fechaF, String agencia, String dstrct) throws Exception {
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        Connection        con       = null;
        String Query = "SQL_REPORTEPLACAFOTO";
        
        List reportesplacasfotos = new LinkedList();
        ReportePlacaFotos reporteplacafoto = new ReportePlacaFotos();
        try{
            st = this.crearPreparedStatement(Query);
            st.setString(1,  dstrct);
            st.setString(2, fechaI+" 00:00");
            st.setString(3, fechaF+" 23:59");
            st.setString(4, agencia);
            rs = st.executeQuery();
            while (rs.next()) {
                reporteplacafoto = reporteplacafoto.load(rs);
                reportesplacasfotos.add(reporteplacafoto);
            }
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina Buscar Documentos Placa [ReportePlacaFotosDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(Query);
        }
        return reportesplacasfotos;
    }
    
    /**
     * Metodo ReporteConductorFoto , Metodo que obtiene si un conductor tiene foto o no
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @parameter : String fechaI
     * @parameter : String fechaF
     * @parameter : String agencia
     * @retorna una lista de registros de conductores y sus fotos
     * @version : 1.0
     */
    public List ReporteConductorFoto(String fechaI, String fechaF, String agencia, String dstrct) throws Exception {
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        Connection        con       = null;
        String Query = "SQL_REPORTECONDUCTORFOTO";
        
        List reporteconductorfotos = new LinkedList();
        ReporteConductorFotos reporteconductorfoto = new ReporteConductorFotos();
        
        try{
            st = this.crearPreparedStatement(Query);
            st.setString(1, agencia);
            st.setString(2,  dstrct);            
            st.setString(3, fechaI+" 00:00");
            st.setString(4, fechaF+" 23:59");
            
            
            rs = st.executeQuery();
            while (rs.next()) {
                reporteconductorfoto = reporteconductorfoto.load(rs);
                reporteconductorfotos.add(reporteconductorfoto);
            }
            
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina Buscar ReporteConductorFoto [ReportePlacaFotosDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(Query);
        }
        return reporteconductorfotos;
    }
    
    //**************************************< Diogenes Bastidas>**********************************
    /**
     * busca los documentos de un conductor
     * @autor Diogenes Bastidas
     * @throws SQLException
     * @version 1.0
     */
    public ReporteDocumentosHV obtenerDocumentosConductor(ReporteDocumentosHV rep,String dstrct, String documento) throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        Connection        con       = null;
        String Query = "SQL_BUSCAR_DOCUMENTOS";
        String SQL;
        
        try {
            con =  this.conectar(Query);
            SQL = this.obtenerSQL(Query);
            st = con.prepareStatement(SQL);
            st.setString(1, dstrct);
            st.setString(2, "003" );
            st.setString(3, documento);
            rs = st.executeQuery();
            while(rs.next()){
                if(rs.getString("documento").equals("028")){
                    rep.setArp(rs.getString("imagen"));
                    rep.setFecha_arp(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("011")){
                    rep.setCedula(rs.getString("imagen"));
                    rep.setFecha_cedula(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("025")){
                    rep.setCert_jud(rs.getString("imagen"));
                    rep.setFecha_cert_jud(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("027")){
                    rep.setEps( rs.getString("imagen"));
                    rep.setFecha_eps( rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("039")){
                    rep.setFirma( rs.getString("imagen"));
                    rep.setFecha_firma( rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("032")){
                    rep.setFoto_con( rs.getString("imagen"));
                    rep.setFecha_foto_con( rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("024")){
                    rep.setLib_mil( rs.getString("imagen"));
                    rep.setFecha_lib_mil( rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("029")){
                    rep.setLib_tripulante( rs.getString("imagen"));
                    rep.setFecha_lib_tripulante(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("030")){
                    rep.setPasaporte( rs.getString("imagen"));
                    rep.setFecha_pasaporte( rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("023")){
                    rep.setPase(rs.getString("imagen"));
                    rep.setFecha_pase( rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("026")){
                    rep.setVisa( rs.getString("imagen"));
                    rep.setFecha_visa( rs.getString("fecha"));
                }
            }
            
            rep.setPrimera_h("NO");
            rep.setFecha_primera_h("");
            rep.setSegunda_h("NO");
            rep.setFecha_segunda_h("");
            //busco si tiene la huellas
            SQL = this.obtenerSQL("SQL_BUSCAR_HUELLAS");
            st = con.prepareStatement(SQL);
            st.setString(1, dstrct);
            st.setString(2, documento);
            rs = st.executeQuery();
            while(rs.next()){
                if(rs.getString("documento").equals("015")){
                    rep.setPrimera_h("SI");
                    rep.setFecha_primera_h(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("016")){
                    rep.setSegunda_h("SI");
                    rep.setFecha_segunda_h(rs.getString("fecha"));
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina Buscar Documentos conductor [ReportePlacaFotosDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(Query);
        }
        return rep;
    }
    
    /**
     * busca los documentos de una placa
     * @autor Diogenes Bastidas
     * @throws SQLException
     * @version 1.0
     */
    public ReporteDocumentosHV obtenerDocumentosPlaca(ReporteDocumentosHV rep,String dstrct, String documento) throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        Connection        con       = null;
        String Query = "SQL_BUSCAR_DOCUMENTOS";
        
        try {
            
            st = this.crearPreparedStatement(Query);
            st.setString(1, dstrct);
            st.setString(2, "005" );
            st.setString(3, documento);
            rs = st.executeQuery();
            while(rs.next()){
                if(rs.getString("documento").equals("017")){
                    rep.setEmision_gas(rs.getString("imagen"));
                    rep.setFecha_emision_gas(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("022")){
                    rep.setCont_arriendo(rs.getString("imagen"));
                    rep.setFecha_cont_arriendo(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("021")){
                    rep.setCont_compra(rs.getString("imagen"));
                    rep.setFecha_cont_compra(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("031")){
                    rep.setFoto_placa(rs.getString("imagen"));
                    rep.setFecha_foto_placa(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("016")){
                    rep.setPoliza_andina(rs.getString("imagen"));
                    rep.setFecha_poliza_andina(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("020")){
                    rep.setReg_nal_carga(rs.getString("imagen"));
                    rep.setFecha_reg_nal_carga(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("034")){
                    rep.setReg_nal_semire(rs.getString("imagen"));
                    rep.setFecha_reg_nal_semire(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("019")){
                    rep.setSoat(rs.getString("imagen"));
                    rep.setFecha_soat(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("015")){
                    rep.setTar_habi(rs.getString("imagen"));
                    rep.setFecha_tar_habi(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("018")){
                    rep.setTar_empresarial(rs.getString("imagen"));
                    rep.setFecha_tar_empresarial(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("014")){
                    rep.setTar_propiedad_cab(rs.getString("imagen"));
                    rep.setFecha_tar_propiedad_cab(rs.getString("fecha"));
                }
                else if(rs.getString("documento").equals("033")){
                    rep.setTar_propiedad_semi(rs.getString("imagen"));
                    rep.setFecha_tar_propiedad_semi(rs.getString("fecha"));
                }
                
            }
            
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina Buscar Documentos Placa [ReportePlacaFotosDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(Query);
        }
        return rep;
    }
    
    /**
     * busca los documentos de una placa
     * @autor Diogenes Bastidas
     * @throws SQLException
     * @version 1.0
     */
    public ReporteDocumentosHV obtenerPlanViaje(ReporteDocumentosHV rep,String planilla, String dstrct) throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        Connection        con       = null;
        String Query = "SQL_BUSCAR_PLAN_VIAJE";
        
        try {
            
            st = this.crearPreparedStatement(Query);
            st.setString(1, dstrct);
            st.setString(2, planilla);
            rs = st.executeQuery();
            if(rs.next()){
                rep.setPlanviaje("SI");
                rep.setPuesto1(rs.getString("pl1"));
                rep.setTel1(rs.getString("pt1"));
                rep.setPuesto2(rs.getString("pl2"));
                rep.setTel2(rs.getString("pt2"));
                rep.setPuesto3(rs.getString("pl3"));
                rep.setTel3(rs.getString("pt3"));
                rep.setCelular(rs.getString("celular"));
                rep.setTiempo(rs.getDouble("tiempo"));
                rep.setNro_celular(rs.getString("nro_celular"));
                rep.setFecha_salida(rs.getString("fecha_salida"));
                rep.setPernotacion(rs.getString("pernoctacion"));
            }
            else{
                rep.setPlanviaje("NO");
                rep.setPuesto1("");
                rep.setTel1("");
                rep.setPuesto2("");
                rep.setTel2("");
                rep.setPuesto3("");
                rep.setTel3("");
                rep.setCelular("NO");
                rep.setTiempo(0);
                rep.setNro_celular("");
                rep.setFecha_salida("0099-01-01 00:00");
                rep.setPernotacion("NO");
            }
            
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina Buscar obtenerPlanViaje [ReportePlacaFotosDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(Query);
        }
        return rep;
    }
    
    public Vector obtenerDespachos(String dstrct,String agencia, String fecha1, String fecha2) throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        ResultSet         rs1       = null;
        Connection        con       = null;
        String Query = "SQL_BUSCAR_DESPACHO";
        Vector vec = new Vector();
        String fec="";
        
        
        try {
            st = this.crearPreparedStatement(Query);
            st.setString(1,fecha1+" 00:00");
            st.setString(2,fecha2+" 23:59");
            st.setString(3,agencia);
            //System.out.println(st.toString());
            rs = st.executeQuery();
            while(rs.next()){
                ReporteDocumentosHV rep = new ReporteDocumentosHV();
                rep.setAgencia(rs.getString("nomagencia"));
                rep.setNumpla(rs.getString("numpla"));
                rep.setFechadsp(rs.getString("fecdsp"));
                rep.setDespachador(rs.getString("despachador"));
                rep.setPlaca(rs.getString("plaveh"));
                rep.setNrocedula(rs.getString("cedcon"));
                rep.setConductor(rs.getString("nomcon"));
                rep.setCliente(rs.getString("nomcliente"));
                rep.setRuta(rs.getString("ruta"));
                rep.setTipo_viaje(rs.getString("tipo"));
                rep.setHreporte(rs.getString("hreporte"));
                rep.setFecha_salida_dsp(rs.getString("fec_salida"));
                rep.setPernotacion(rs.getString("pernoctacion"));
                rep.setDebe_pernoctar("");
                rep.setPlanviaje(rs.getString("plan_viaje"));
                rep.setFecha_pos_llegada( (!rs.getString("fec_salida").equals(""))?fechaEstimadaLlegada(dstrct, rs.getString("numpla"),rs.getString("fec_salida")):"" );                 
                rep = obtenerDocumentosConductor(rep, dstrct,rs.getString("cedcon") ); 
                rep = obtenerDocumentosPlaca(rep, dstrct,rs.getString("plaveh") );
                if(!rs.getString("plan_viaje").equals("NO APLICA"))
                    rep = obtenerPlanViaje(rep,rs.getString("numpla"),dstrct);
                
                vec.add(rep);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina obtener Despachos [ReportePlacaFotosDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(Query);
        }
        return vec;
    }
    
    /***************************************Calcular tiempo de llegada***********************************************/
    /**
     * busca el tiempo entre un puesto de control y otro
     * @autor Ing. Diogenes Bastidas
     * @param distrito, origen,destino
     * @throws SQLException
     * @version 1.0.
     **/
    public double buscarTiempo(String dstrct, String origen, String destino) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SQL_TIEMPO";
        double tiempo = 0;
        try{
            ps = crearPreparedStatement(sql);
            ps.setString(1, dstrct);
            ps.setString(2, origen);
            ps.setString(3, destino);
            
            rs = ps.executeQuery();
            if (rs.next()){
                tiempo = rs.getDouble("tiempo");
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR TIEMPO DE LOS PC"+ex.getMessage()+", "+ex.getErrorCode());
        } finally {
            if(ps != null) {
                ps.close();
            }
            desconectar(sql);
        }
        return tiempo;
    }
    
    /**
     * M�todo que retorna la fecha que esta en reporte trafico
     * @autor       Diogenes Bastidas
     * @throws      Exception
     * @version     1.0.
     * @return      Fecha
     **/
    public String buscarReporteTrafico(String planilla) throws SQLException{
        Connection con =null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SQL_REPORTE_TRAFICO";
        String val = "";
        try{
            ps = crearPreparedStatement(sql);
            ps.setString(1, planilla);
            rs = ps.executeQuery();
            if (rs.next()){
                val = rs.getString("fechareporte");
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR ULTIMO REPORTE"+ex.getMessage()+", "+ex.getErrorCode());
        } finally {
            if(ps != null) {
                ps.close();
            }
            desconectar(sql);
        }
        return val;
    }
    
    
    /**
     * busca la ruta de una planilla
     * @autor Ing. Diogenes Bastidas
     * @param planilla
     * @throws SQLException
     * @version 1.0.
     **/
    public String buscarRutaPlanilla(String planilla) throws SQLException{
        Connection con =null;
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SQL_BUSCAR_VIA";
        String via = "";
        try{
            ps = crearPreparedStatement(sql);
            ps.setString(1, planilla);
            rs = ps.executeQuery();
            if (rs.next()){
                estado = rs.getString("reg_status");
                ultrep = rs.getString("pto_control_ultreporte");
                dempla = rs.getDouble("demora");
                via = rs.getString("via");
                fecultrep = rs.getString("fecha_ult_reporte");
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR LA RUTA"+ex.getMessage()+", "+ex.getErrorCode());
        } finally {
            if(ps != null) {
                ps.close();
            }
            desconectar(sql);
        }
        return via;
    }
    
    
     /**
     * M�todo que retorna la fecha de estimada de llegada de una planilla
     * @autor       Diogenes Bastidas
     * @throws      Exception
     * @version     1.0.
     * @return      Fecha
     **/
    public String fechaEstimadaLlegada(String dstrct, String planilla, String fec_salida_dsp)throws Exception{
        int i=0, inicia = 0;
        double horas = 0;
        String fecha_ult_rep = "";
        Vector pc_control = new Vector();
        //busca la via de la planilla
        String via = buscarRutaPlanilla(planilla);
        if(via.trim().equals("")){
           return fecha_ult_rep = buscarReporteTrafico(planilla);
        }
        //obtengo la fecha ultimo reporte
        fecha_ult_rep = (fecultrep.equals("0099-01-01 00:00:00")||fecultrep.equals(""))?fec_salida_dsp+":00":fecultrep;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date fecha = fmt.parse(fecha_ult_rep);
        //obtengo el reg_status
        horas = dempla;
        if(!estado.equals("D")){
            for(i=0; i<via.length();i+=2){
                pc_control.add(via.substring(i,i+2));
                if(ultrep.equals(via.substring(i,i+2))){
                    inicia=i;
                }
            }
            //sumo los tiempos
            for(i=inicia; i<pc_control.size();i++){
                int sig = i+1;
                if(sig<pc_control.size()){
                    horas += buscarTiempo(dstrct, (String) pc_control.get(i),(String) pc_control.get( sig ));
                    ////System.out.println((String) pc_control.get(i)+" - "+ (String) pc_control.get( sig )+" "+buscarTiempo(dstrct, (String) pc_control.get(i),(String) pc_control.get( sig )));
                }
            }
        }
        else{
            fecha = fmt.parse(Utility.getDate(8));
            horas = 0;
        }  // //System.out.println("Planilla "+ planilla+" Fecha salida DSP "+fec_salida_dsp+" Ult rep trafico "+fecha_ult_rep+" Horas "+horas);
            fecha.setTime(  fecha.getTime() + (long) ( horas * 60 * 60 * 1000 )  );
        return fmt.format(fecha);
    }
    
    /*public static void main(String[]arg){
        ReportePlacaFotosDAO prog = new ReportePlacaFotosDAO();
        try{
            //System.out.println("Inicia");
            //System.out.println(prog.fechaEstimadaLlegada("FINV","792349","2007-01-02 18:38:00") );
            //System.out.println("fin");
        }catch(Exception e){
            e.printStackTrace();
        }   
    }*/
}
