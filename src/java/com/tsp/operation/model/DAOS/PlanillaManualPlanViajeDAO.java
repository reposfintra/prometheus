/*********************************************************************************
 * Nombre clase :      PlanillaManualPlanViajeDAO.java                           *
 * Descripcion :       DAO del PlanillaManualPlanViajeDAO.java                   *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  * 
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             LREALES                                                   *
 * Fecha :             10 de agosto de 2006, 10:12 PM                            *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;

public class PlanillaManualPlanViajeDAO extends MainDAO {
    
    private Hashtable ht = null;
    
    private Vector planilla;    
    
    /** Creates a new instance of PlanillaManualPlanViajeDAO */
    public PlanillaManualPlanViajeDAO () {
        
        super( "PlanillaManualPlanViajeDAO.xml" );
        
    }  
        
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.util.Vector getPlanilla() {
        
        return planilla;
        
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla( java.util.Vector planilla ) {
        
        this.planilla = planilla;
        
    }
    
    /*******************************************/
    
    /** Funcion publica que extrae info (plan de viaje) */
    public void consulta ( String numpla ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_CONSULTA" );
            
            st.setString( 1, numpla );
            
            rs = st.executeQuery();
                        
            this.planilla = new Vector();
            
            if ( rs.next() ){
                
                Hashtable ht = new Hashtable();
                
                ht.put( "placa", rs.getString("placa")!=null?rs.getString("placa").toUpperCase():"" );
                ht.put( "fecha_despacho", rs.getString("fecha_despacho")!=null?rs.getString("fecha_despacho").toUpperCase():"" );
                String fecha_salida = rs.getString("fecha_salida")!=null?rs.getString("fecha_salida").toUpperCase():"";
                fecha_salida = fecha_salida.substring( 0, 16 );
                ht.put( "fecha_salida", fecha_salida.equals( "0099-01-01 00:00" )?"":fecha_salida );
                ht.put( "cedcon", rs.getString("cedcon")!=null?rs.getString("cedcon").toUpperCase():"" );
                ht.put( "nomcon", rs.getString("nomcon")!=null?rs.getString("nomcon").toUpperCase():"" );
                ht.put( "codruta", rs.getString("codruta")!=null?rs.getString("codruta").toUpperCase():"" );
                ht.put( "ruta", rs.getString("ruta")!=null?rs.getString("ruta").toUpperCase():"" );
                ht.put( "codcli", rs.getString("codcli")!=null?rs.getString("codcli").toUpperCase():"" );
                ht.put( "nomcli", rs.getString("nomcli")!=null?rs.getString("nomcli").toUpperCase():"" );
                ht.put( "carga", rs.getString("carga")!=null?rs.getString("carga").toUpperCase():"" );
                ht.put( "otro", rs.getString("otro")!=null?rs.getString("otro").toUpperCase():"" );
                ht.put( "comentario1", rs.getString("comentario1")!=null?rs.getString("comentario1").toUpperCase():"" );
                ht.put( "comentario2", rs.getString("comentario2")!=null?rs.getString("comentario2").toUpperCase():"" );
                ht.put( "comentario3", rs.getString("comentario3")!=null?rs.getString("comentario3").toUpperCase():"" );
                                
                this.planilla.add( ht );
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE [ consulta ] - PlanillaManualPlanViajeDAO.. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO: " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_CONSULTA" );
            
        }
        
    }
    
}