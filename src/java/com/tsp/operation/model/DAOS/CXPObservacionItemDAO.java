/***********************************************************************************
 * Nombre clase : ............... CXPObservacionItemDao.java                               *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Henry A.Osorio González                     *
 * Fecha :....................... 8 de octubre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import com.tsp.util.connectionpool.PoolManager;

public class CXPObservacionItemDAO extends MainDAO {
    
        public CXPObservacionItemDAO() {
            super("CXPObservacionItemDAO.xml");
        }    
        public CXPObservacionItemDAO(String dataBaseName) {
            super("CXPObservacionItemDAO.xml", dataBaseName);
        }    
        private CXPObservacionItem observacionItem = new CXPObservacionItem();
    
        private static final String SQL_INSERT_OBS =  "Insert into fin.cxp_obs_aprob_item " +
                                                      "     (dstrct, proveedor," +
                                                      "     tipo_documento,documento," +
                                                      "     item,observacion_autorizador," +
                                                      "     fecha_ob_autorizador,usuario_ob_autorizador," +
                                                      "     creation_user,textoactivo) " +
                                                      "values(?,?,?,?,?,?,?,?,?,?)";
    
        private static final String UPT_OBS_AUTOR =  "Update fin.cxp_obs_aprob_item " +
                                                      "       set observacion_autorizador=?, " +
                                                      "       textoactivo='R' " +
                                                      "where " +
                                                      "       dstrct=? and " +
                                                      "       proveedor=? and " +
                                                      "       tipo_documento=? and" +
                                                      "       documento=? and item=?";
    
        private static final String CERRAR_OBS  =     "update " +
                                                      "     fin.cxp_obs_aprob_item " +
                                                      "     set cierre_observacion=?, " +
                                                      "     textoactivo='N' " +
                                                      "where " +
                                                      "     dstrct=? and " +
                                                      "     proveedor=? and " +
                                                      "     tipo_documento=? and " +
                                                      "     documento=? and " +
                                                      "     item=?";
    
        private static final  String SQL_GET_OBS =    "Select " +
                                                      "     dstrct," +
                                                      "     proveedor," +
                                                      "     tipo_documento," +
                                                      "     documento," +
                                                      "     item," +
                                                      "     observacion_autorizador," +
                                                      "     observacion_pagador," +
                                                      "     ob_registra_factura, " +
                                                      "     textoactivo " +
                                                      "from " +
                                                      "     fin.cxp_obs_aprob_item a " +
                                                      "where" +
                                                      "     dstrct=? and" +
                                                      "     proveedor=? and" +
                                                      "     tipo_documento=? and" +
                                                      "     documento=? and" +
                                                      "     item=?";
    
        private  static final String OBT_OBS_AUT  =   "Select " +
                                                      "      observacion_autorizador " +
                                                      "from " +
                                                      "      fin.cxp_obs_aprob_item " +
                                                      "where " +
                                                      "     dstrct=? and" +
                                                      "     proveedor=? and" +
                                                      "     tipo_documento=? and" +
                                                      "     documento=? and" +
                                                      "     item=?";
         /**
         * Metodo insertObservacionItem, recibe el objeto item el cual contiene información
         * para agregar una observacion a ese item por parte de un autorizador
         * @autor : Ing. Henry A. Osorio González
         * @param : CXPObservacionItem item de la factura
         * @version : 1.0
         */     
        public void insertObservacionItem(CXPObservacionItem items,String Usuario) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                PoolManager poolManager = null;
                observacionItem = new CXPObservacionItem();
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection(this.getDatabaseName());
                        if (con != null){
                                st = con.prepareStatement(SQL_INSERT_OBS);
                                st.setString(1, items.getDstrct());
                                st.setString(2, items.getProveedor());
                                st.setString(3, items.getTipo_documento());
                                st.setString(4, items.getDocumento());
                                st.setString(5, items.getItem());
                                st.setString(6, Usuario+" "+ Util.getFechaActual_String(4)+":\n"+items.getObservacion_autorizador());
                                st.setString(7, items.getFecha_ob_autorizador());
                                st.setString(8, items.getUsuario_ob_autorizador());
                                st.setString(9, items.getCreation_user());
                                st.setString(10, "R");
                                ////System.out.println("Query: "+st.toString());
                                st.execute();
                                
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL INSERTAR OBSERVACIONES" + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection(this.getDatabaseName(), con );
                        }
                }
        }
         /**
         * Metodo updateObservacionItem, recibe el objeto Observacion Item y el usuario autorizador
         * @autor : Ing. Henry A. Osorio González
         * @param : CXPObservacionItem item de la factura, String usuario autorizador
         * @version : 1.0
         */ 
        public void updateObservacionItem(CXPObservacionItem items, String usuario) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                PoolManager poolManager = null;
                observacionItem = new CXPObservacionItem();
                ResultSet rs = null;
                String obsActual = "";
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection(this.getDatabaseName());
                        if (con != null){
                                st = con.prepareStatement(OBT_OBS_AUT);
                                st.setString(1, items.getDstrct());
                                st.setString(2, items.getProveedor());
                                st.setString(3, items.getTipo_documento());
                                st.setString(4, items.getDocumento());
                                st.setString(5, items.getItem());
                                rs = st.executeQuery();
                                if(rs.next()) {
                                        obsActual = rs.getString("observacion_autorizador");
                                }
                                st = null;
                                st = con.prepareStatement(UPT_OBS_AUTOR);
                                st.setString(1, obsActual+"\n\n"+usuario+" "+Util.getFechaActual_String(4)+":\n"+items.getObservacion_autorizador());
                                st.setString(2, items.getDstrct());
                                st.setString(3, items.getProveedor());
                                st.setString(4, items.getTipo_documento());
                                st.setString(5, items.getDocumento());
                                st.setString(6, items.getItem());
                                ////System.out.println("Query: "+st.toString());
                                st.execute();
                                
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL ACTUALIZAR OBSERVACIONES" + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection(this.getDatabaseName(), con );
                        }
                }
        }
        
        
        
         /**
         * Metodo updateObservacionItemPagador, que actualiza las observaciones del pagador
         * @autor : Ing. Ivan Dario Gomez
         * @param : CXPObservacionItem item de la factura, String usuario autorizador
         * @version : 1.0
         */ 
        public void updateObservacionItemPagador(CXPObservacionItem items, String usuario) throws SQLException {
                PreparedStatement st = null;
                observacionItem = new CXPObservacionItem();
                ResultSet rs = null;
                String obsActual = "";
                try{
                    st = crearPreparedStatement("SQL_OBT_OBS_PAGADOR");
                    st.setString(1, items.getDstrct());
                    st.setString(2, items.getProveedor());
                    st.setString(3, items.getTipo_documento());
                    st.setString(4, items.getDocumento());
                    st.setString(5, items.getItem());
                    rs = st.executeQuery();
                    if(rs.next()) {
                            obsActual = rs.getString("observacion_autorizador");
                    }
                    st = null;
                    st = crearPreparedStatement("SQL_UPT_OBS_PAGADOR");
                    st.setString(1, obsActual+"\n\n"+usuario+" "+Util.getFechaActual_String(4)+":\n"+items.getObservacion_pagador());
                    st.setString(2, items.getDstrct());
                    st.setString(3, items.getProveedor());
                    st.setString(4, items.getTipo_documento());
                    st.setString(5, items.getDocumento());
                    st.setString(6, items.getItem());
                    ////System.out.println("Query: "+st.toString());
                    st.execute();
                                
                        
                }catch(SQLException e){
                        throw new SQLException("ERROR AL ACTUALIZAR OBSERVACIONES DEL PAGADOR" + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                                }
                        }
                        desconectar("SQL_OBT_OBS_PAGADOR");
                        desconectar("SQL_UPT_OBS_PAGADOR");
                }
        }
        
        
         
         /**
         * Metodo buscarTextoActivoObservacion, que busca el valor del texto activo
         * @autor : Ing. Ivan Dario Gomez
         * @param : CXPObservacionItem item de la factura
         * @version : 1.0
         */ 
        public String buscarTextoActivoObservacion(CXPItemDoc items) throws SQLException {
                PreparedStatement st = null;
                ResultSet rs = null;
                String TextoActivo = "";
                try{
                    st = crearPreparedStatement("SQL_OBT_TEXTOACTIVO");
                    st.setString(1, items.getDstrct());
                    st.setString(2, items.getProveedor());
                    st.setString(3, items.getTipo_documento());
                    st.setString(4, items.getDocumento());
                    st.setString(5, items.getItem());
                    rs = st.executeQuery();
                    if(rs.next()) {
                        TextoActivo = rs.getString("textoactivo");
                    }
                   
                    return TextoActivo;            
                        
                }catch(SQLException e){
                        throw new SQLException("ERROR EN buscarTextoActivoObservacion" + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                                }
                        }
                        desconectar("SQL_OBT_TEXTOACTIVO");
                }
        }
        
        
         /**
         * Metodo buscarObservacionesItem, permite buscar si existe una observacion de un item        
         * @autor : Ing. Henry A. Osorio González
         * @param : CXPObservacionItem item de la factura
         * @version : 1.0
         */   
        public void buscarObservacionesItem(CXPItemDoc items) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                observacionItem = new CXPObservacionItem();
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection(this.getDatabaseName());
                        if (con != null){
                                st = con.prepareStatement(SQL_GET_OBS);
                                st.setString(1, items.getDstrct());
                                st.setString(2, items.getProveedor());
                                st.setString(3, items.getTipo_documento());
                                st.setString(4, items.getDocumento());
                                st.setString(5, items.getItem());
                                ////System.out.println("Query: "+st.toString());
                                rs = st.executeQuery();
                                if(rs.next()){
                                        observacionItem.setDstrct(rs.getString("dstrct"));
                                        observacionItem.setProveedor(rs.getString("proveedor"));
                                        observacionItem.setTipo_documento(rs.getString("tipo_documento"));
                                        observacionItem.setDocumento(rs.getString("documento"));
                                        observacionItem.setItem(rs.getString("item"));
                                        observacionItem.setObservacion_pagador(rs.getString("observacion_pagador"));
                                        observacionItem.setOb_registra_factura(rs.getString("ob_registra_factura"));
                                        observacionItem.setObservacion_autorizador(rs.getString("observacion_autorizador"));
                                        observacionItem.setTextoactivo(rs.getString("textoactivo"));
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL buscar observaciones " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection(this.getDatabaseName(), con );
                        }
                }
        }
        /**
         * Metodo cerrarObservacion, permite cerrar la observacion abierta de un item especifico
         * @autor : Ing. Henry A. Osorio González
         * @param : CXPObservacionItem item de la factura
         * @version : 1.0
         */          
        public void cerrarObservacion(CXPObservacionItem items) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                PoolManager poolManager = null;
                observacionItem = new CXPObservacionItem();
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection(this.getDatabaseName());
                        if (con != null){
                                st = con.prepareStatement(CERRAR_OBS);
                                st.setString(1, items.getCierre_observacion());
                                st.setString(2, items.getDstrct());
                                st.setString(3, items.getProveedor());
                                st.setString(4, items.getTipo_documento());
                                st.setString(5, items.getDocumento());
                                st.setString(6, items.getItem());
                                ////System.out.println("Query: "+st.toString());
                                st.execute();
                                
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL CERRAR LA OBSERVACION " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection(this.getDatabaseName(), con );
                        }
                }
        }        
         /**
         * Metodo getObservacionItem, retorna la observacion abierta de un item especifico
         * @autor : Ing. Henry A. Osorio González       
         * @version : 1.0
         */          
        public CXPObservacionItem getObservacionItem() {
                return observacionItem;
        }
}