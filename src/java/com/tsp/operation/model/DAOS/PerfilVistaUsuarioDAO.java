/********************************************************************
 *      Nombre Clase.................   Perfil_vistaService.java
 *      Descripci�n..................   DAO de la tabla perfil_vista_usuario
 *      Autor........................   Rodrigo Salazar
 *      Fecha........................   5.07.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.DAOS;

import java.io.*;
import javax.swing.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class PerfilVistaUsuarioDAO {
    private PerfilVistaUsuario pvu;
    private Vector PerfilVistaUsuarios;
    private List tcontactos;
    
    /** Creates a new instance of PerfilVistaUsuarioDAO */
    public PerfilVistaUsuarioDAO() {
    }
    
    /**
     * Obtiene la propiedad pvu
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public PerfilVistaUsuario getPerfilVistaUsuario() {
        return pvu;
    }
    
    /**
     * Establece la propiedad pvu
     * @autor Rodrigo Salazar
     * @param pvu Nuevo valor de la propiedad pvu
     * @version 1.0
     */
    public void setPerfilVistaUsuario(PerfilVistaUsuario pvu) {
        this.pvu = pvu;
    }
    
    /**
     * Obtiene la propiedad PerfilVistaUsuarios
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Vector getPerfilVistaUsuarios() {
        return PerfilVistaUsuarios;
    }
    
    /**
     * Establece la propiedad PerfilVistaUsuarios
     * @autor Rodrigo Salazar
     * @param PerfilVistaUsuarios Nuevo valor de la propiedad pvu
     * @version 1.0
     */
    public void setPerfilVistaUsuarios(Vector PerfilVistaUsuarios) {
        this.PerfilVistaUsuarios = PerfilVistaUsuarios;
    }
    
    /**
     * Inserta un nuevo registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.2
     */
    public void insertPerfilVistaUsuario()throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector vmenu = null;        
       
        try {            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            ////System.out.println(" entra a funcion");
            //tito andres maturana
            /*st = con.prepareStatement("select max(cod_pvu) as maximo from perfil_vista_usuario");
            ResultSet rs1 = st.executeQuery();
            rs1.next();
            int max =   rs1.getInt("maximo") +1;      
            String idopcion = ""+ max ;
            ////System.out.println(" idopcion " + idopcion + "perfil " +pvu.getPerfil());*/
            if(con!=null){      
                st = con.prepareStatement("insert into perfil_vista_usuario(usuario, perfil, rec_status,last_update, user_update, " +
                        "cia, creation_date, creation_user) values(?,?,'','now()','',?,'now()',?)");
                //st.setString(1, idopcion);                
  
                st.setString(1, pvu.getUsuario());
                st.setString(2, pvu.getPerfil());
                st.setString(3, pvu.getCia());
                st.setString(4, pvu.getCreation_user());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA OPCION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Obtiene un registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @param usuario Login del usuario
     * @param perfil Nombre del perfil
     * @throws SQLException
     * @version 1.0
     */
    public void searchPerfilVistaUsuario(String usuario,String perfil)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from perfil_vista_usuario where  codigo=? and rec_status != 'A'");
                st.setString(1,usuario+"%");
                st.setString(1,perfil+"%");
                rs= st.executeQuery();
                PerfilVistaUsuarios = new Vector();
                while(rs.next()){
                    pvu = new PerfilVistaUsuario();
                    pvu.setUsuario(rs.getString("usuario"));
                    pvu.setPerfil(rs.getString("perfil"));
                    PerfilVistaUsuarios.add(pvu);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL TIPO DE UBICACI�N " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Verifica la existencia de un registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public boolean existPerfilVistaUsuario(String usuario, String perfil) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select * from perfil_vista_usuario where usuario=? and perfil=?");
                st.setString(1,usuario);
                st.setString(2,perfil);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DEL TIPO DE UBICACI�N " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Lista todos los registros no anulados de la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void listPerfilVistaUsuario()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select a.cod_pvu,a.usuario,c.nombre from perfil_vista_usuario a, perfil_trf c where a.perfil = c.id_perfil and a.rec_status != 'A'");
                rs= st.executeQuery();
                PerfilVistaUsuarios = new Vector();
                while(rs.next()){
                    pvu = new PerfilVistaUsuario();
                    pvu.setCodigo(rs.getString("cod_pvu"));
                    pvu.setUsuario(rs.getString("usuario"));
                    pvu.setPerfil(rs.getString("nombre"));
                    PerfilVistaUsuarios.add(pvu);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA UNIDAD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Actualiza un registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void updatePerfilVistaUsuario() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update perfil_vista_usuario set perfil=?, last_update='now()', user_update=? where cod_pvu = ?");
                st.setString(1,pvu.getPerfil());
                st.setString(2,pvu.getUser_update());
                st.setString(3,pvu.getCodigo());
                ////System.out.println(st);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DEL TIPO DE UBICACI�N" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Anula un registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @param usu Login del usuario
     * @param codigo C�digo del perfil-vista-usuario
     * @throws SQLException
     * @version 1.0
     */
    public void anularPerfilVistaUsuario(String usu, String codigo) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ////System.out.println(codigo);
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update perfil_vista_usuario set rec_status='A',last_update='now()', user_update=? where cod_pvu=?");
                st.setString(1,usu);
                st.setString(2,codigo);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACI�N DEL TIPO DEL UBICI�N" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Obtiene un registro en la tabla perfil_vista_usuario
     * @autor Rodrigo Salazar
     * @param cod C�digo del perfil-vista-usuario
     * @throws SQLException
     * @version 1.0
     */
    public Vector searchDetallePerfilVistaUsuarios(String cod) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        PerfilVistaUsuarios=null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                
                st = con.prepareStatement("Select * from perfil_vista_usuario where cod_pvu = ? and rec_status != 'A'");
                st.setString(1, cod);
                ////System.out.println(st.toString());
                
                rs = st.executeQuery();
                PerfilVistaUsuarios = new Vector();
                while(rs.next()){
                    pvu = new PerfilVistaUsuario();
                    pvu.setCodigo(rs.getString("cod_pvu"));
                    pvu.setUsuario(rs.getString("usuario"));
                    pvu.setPerfil(rs.getString("perfil"));
                    pvu.setRec_status(rs.getString("rec_status"));
                    pvu.setUser_update(rs.getString("user_update"));
                    pvu.setCreation_user(rs.getString("creation_user"));
                    PerfilVistaUsuarios.add(pvu);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE CONTACTO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return PerfilVistaUsuarios;
    }
    
    
    
    
    
    
}


