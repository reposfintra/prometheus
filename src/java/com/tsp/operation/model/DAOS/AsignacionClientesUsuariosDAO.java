/*
 * AsignacionClientesUsuariosDAO.java
 *
 * Created on 16 de septiembre de 2005, 09:23 AM
 */

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.General;
import com.tsp.util.Util;
import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  mfontalvo
 */
public class AsignacionClientesUsuariosDAO extends MainDAO{
    
    public static final int CREATE_PERFIL_CLIENTES = 0;
    public static final int INSERT_PERFIL_CLIENTES = 1;
    public static final int DELETE_PERFIL_CLIENTES = 2;
    
    public static final int CREATE_PERFIL_USUARIOS = 3;
    public static final int INSERT_PERFIL_USUARIOS = 4;
    public static final int DELETE_PERFIL_USUARIOS = 5;
    
    public static final int CREATE_RELACION_PERFIL = 6;
    public static final int INSERT_RELACION_PERFIL = 7;
    public static final int DELETE_RELACION_PERFIL = 8;
    
    public static final int SELECT_USUARIOS        = 9;
    public static final int SELECT_CLIENTES        = 10;
    public static final int SELECT_TIPOS           = 11;
    public static final int SELECT_PERFILES        = 12;
    public static final int SELECT_PERFIL_CLIENTE  = 13;
    public static final int SELECT_PERFIL_USUARIO  = 14;
    public static final int SELECT_RELACION_PERFIL = 15;
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private final String SQL_CREATE_PERFIL_CLIENTES = 
       " create table perfil_cliente(                                                    " +
       "  dstrct_code           varchar (4)   not null default '',                       " +
       "  perfil                varchar (40)  not null default '',                       " +
       "  cliente               varchar (40)  not null default '',                       " +
       "  usuario_creacion      varchar (40)  not null default '',                       " +
       "  fecha_creacion        timestamp     not null default now(),                    " +
       "  usuario_actualizacion varchar (40)  not null default '',                       " +
       "  fecha_actualizacion   timestamp     not null default now(),                    " +
       "  constraint pk_perfil_cliente primary key (dstrct_code, perfil, cliente));   " ;

    private final String SQL_INSERT_PERFIL_CLIENTES = " insert into perfil_cliente (dstrct_code, perfil, cliente, usuario_creacion, fecha_creacion, usuario_actualizacion, fecha_actualizacion ) values (?,?,?,?,?,?,?)";    
    
    private final String SQL_DELETE_PERFIL_CLIENTES = " delete from perfil_cliente where perfil = ? ";
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private final String SQL_CREATE_PERFIL_USUARIOS = 
       " create table perfil_usuario (                                                   " +
       "  dstrct_code           varchar (4)   not null default '',                       " +
       "  perfil                varchar (40)  not null default '',                       " +
       "  usuario               varchar (40)  not null default '',                       " +
       "  usuario_creacion      varchar (40)  not null default '',                       " +
       "  fecha_creacion        timestamp     not null default now(),                    " +
       "  usuario_actualizacion varchar (40)  not null default '',                       " +
       "  fecha_actualizacion   timestamp     not null default now(),                    " +
       "  constraint pk_perfil_usuario primary key (dstrct_code, perfil, usuario));      " ;
    
    private final String SQL_INSERT_PERFIL_USUARIOS = " insert into perfil_usuario (dstrct_code, perfil, usuario, usuario_creacion, fecha_creacion, usuario_actualizacion, fecha_actualizacion ) values (?,?,?,?,?,?,?)";    
    
    private final String SQL_DELETE_PERFIL_USUARIOS = " delete from perfil_usuario where perfil = ? ";
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private final String SQL_CREATE_RELACION_PERFIL = 
       " create table relacion_perfil(                                                    " +
       "  dstrct_code           varchar (4)   not null default '',                       " +
       "  relacion              varchar (40)  not null default '',                       " +
       "  perfil                varchar (40)  not null default '',                       " +
       "  usuario_creacion      varchar (40)  not null default '',                       " +
       "  fecha_creacion        timestamp     not null default now(),                    " +
       "  usuario_actualizacion varchar (40)  not null default '',                       " +
       "  fecha_actualizacion   timestamp     not null default now(),                    " +
       "  constraint pk_relacion_perfil primary key (dstrct_code, relacion, perfil));   " ;

    private final String SQL_INSERT_RELACION_PERFIL = " insert into relacion_perfil (dstrct_code, relacion, perfil, usuario_creacion, fecha_creacion, usuario_actualizacion, fecha_actualizacion ) values (?,?,?,?,?,?,?)";    
    
    private final String SQL_DELETE_RELACION_PERFIL = " delete from relacion_perfil where perfil = ? ";

    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    private static final String SQL_TIPOS     = " SELECT tipo      , descripcion  FROM tipo_relacion ORDER BY 2 ";
    private static final String SQL_PERFILES  = " SELECT id_perfil    , nombre  FROM perfil_trf     ORDER BY 2 ";
    private static final String SQL_USUARIOS  = " SELECT idusuario , nombre       FROM usuarios      WHERE estado    ='A'  ORDER BY 2 ";
    private static final String SQL_CLIENTES  = " SELECT codcli    , nomcli       FROM cliente       ORDER BY 2 "; 
    
    
    
    private static final String SQL_PERFIL_USUARIO   =  " SELECT perfil, usuario  FROM perfil_usuario  WHERE perfil = ? " ;
    private static final String SQL_PERFIL_CLIENTE   =  " SELECT perfil, cliente  FROM perfil_cliente  WHERE perfil = ? " ;
    private static final String SQL_RELACION_PERFIL  =  " SELECT perfil, relacion FROM relacion_perfil WHERE perfil = ? " ;
    
    
    /** Creates a new instance of AsignacionClientesUsuariosDAO */
    public AsignacionClientesUsuariosDAO() {
        super("AsignacionClientesUsuariosDAO.xml");
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // TIPO 0:CREATE; 1: INSERT; 2:UPDATE; 3:DELETE;
    public void EXECUTE_UPDATE(int Tipo, String args[], Connection conn) throws Exception {

        PreparedStatement st          = null;
        if (conn==null){
         conn = this.conectarBDJNDI("fintra");//JJCastro fase2
        }
        if (conn == null)
            throw new SQLException("Sin conexion");
       try {
       String SQL = (Tipo == CREATE_PERFIL_CLIENTES        )? this.obtenerSQL("SQL_CREATE_PERFIL_CLIENTES"):
                         (Tipo == INSERT_PERFIL_CLIENTES        )? this.obtenerSQL("SQL_INSERT_PERFIL_CLIENTES"):
                         (Tipo == DELETE_PERFIL_CLIENTES        )?this.obtenerSQL(" SQL_DELETE_PERFIL_CLIENTES"):
                         (Tipo == CREATE_PERFIL_USUARIOS        )?this.obtenerSQL(" SQL_CREATE_PERFIL_USUARIOS"):
                         (Tipo == INSERT_PERFIL_USUARIOS        )? this.obtenerSQL("SQL_INSERT_PERFIL_USUARIOS"):
                         (Tipo == DELETE_PERFIL_USUARIOS        )?this.obtenerSQL(" SQL_DELETE_PERFIL_USUARIOS"):
                         (Tipo == CREATE_RELACION_PERFIL        )? this.obtenerSQL("SQL_CREATE_RELACION_PERFIL"):
                         (Tipo == INSERT_RELACION_PERFIL        )? this.obtenerSQL("SQL_INSERT_RELACION_PERFIL"):
                         (Tipo == DELETE_RELACION_PERFIL        )? this.obtenerSQL("SQL_DELETE_RELACION_PERFIL"): "";
                         
            if (!SQL.equals("")){
                st = conn.prepareStatement(SQL);
                if (args!=null){
                    for (int i=0; i<args.length;i++)
                        st.setString((i+1),  (!args[i].equals("#FECHA#")? args[i]: Util.getFechaActual_String(6) ) );
                }
                st.executeUpdate();
            }
            else throw new SQLException("Tipo de Operacion no definida . ["+Tipo+"]");
        }
        catch(Exception e) {
            if (!(Tipo == CREATE_PERFIL_CLIENTES || Tipo == CREATE_PERFIL_USUARIOS || Tipo == CREATE_RELACION_PERFIL )) throw new SQLException("Error en rutina EXECUTE_UPDATE [AsignacionClientesUsuariosDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    





    public List EXECUTE_QUERY(int Tipo, String []args) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        Connection        conPostgres = null;
        List              Resultado   = new LinkedList();

        con = this.conectarBDJNDI("fintra");//JJCastro fase2
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            String SQL = (Tipo == SELECT_USUARIOS        ? this.obtenerSQL("SQL_USUARIOS"):
                          Tipo == SELECT_CLIENTES        ? this.obtenerSQL("SQL_CLIENTES")       :
                          Tipo == SELECT_PERFILES        ? this.obtenerSQL("SQL_PERFILES")       :
                          Tipo == SELECT_TIPOS           ?  this.obtenerSQL("SQL_TIPOS")             :
                          Tipo == SELECT_PERFIL_USUARIO  ? this.obtenerSQL("SQL_PERFIL_USUARIO")  :
                          Tipo == SELECT_PERFIL_CLIENTE  ? this.obtenerSQL("SQL_PERFIL_CLIENTE")  :
                          Tipo == SELECT_RELACION_PERFIL ? this.obtenerSQL("SQL_RELACION_PERFIL"): "");
            if (!SQL.equals("")){
                st = conPostgres.prepareStatement(SQL);
                if (args!=null){
                    for (int i=0;i<args.length;i++)
                        st.setString((i+1),  (args[i].equals("ALL")?"%":args[i]));
                }
                ////System.out.println("SQL: "+st.toString());
                rs = st.executeQuery();
                while (rs.next()){
                    Object datos = null;
                    datos = General.load(rs);
                    Resultado.add(datos);
                    if (Tipo == SELECT_RELACION_PERFIL) break;
                }
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina EXECUTE_QUERY [AsignacionClientesUsuariosDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Resultado;
    }
    
    
    public void upUsuariosClientes (String [] Usuarios, String []Clientes, String Tipo,  String Perfil  , String Usuario) throws Exception{
        Connection con = null;//JJCastro fase2
        
        con = this.conectarBDJNDI("fintra");//JJCastro fase2
        if (con == null)
            throw new SQLException("Sin conexion");
        try {
            String [] paramDelete = { Perfil } ;
            
            //////////////////////////////////////////////////////////////////////////////////////////////
            EXECUTE_UPDATE(this.DELETE_RELACION_PERFIL, paramDelete, con);
            if (Tipo!=null){
                String [] paramInsertRP = { "FINV", Tipo, Perfil , Usuario, "#FECHA#", Usuario,  "#FECHA#" };
                EXECUTE_UPDATE(INSERT_RELACION_PERFIL, paramInsertRP, con);
            }
            
            
            //////////////////////////////////////////////////////////////////////////////////////////////
            EXECUTE_UPDATE(this.DELETE_PERFIL_USUARIOS, paramDelete, con);
            if (Usuarios!=null){
                for (int i = 0; i<Usuarios.length; i++){
                    String [] paramInsert = { "FINV", Perfil, Usuarios [i], Usuario, "#FECHA#", Usuario,  "#FECHA#" };
                    EXECUTE_UPDATE(INSERT_PERFIL_USUARIOS, paramInsert, con);
                }
            }
            
            //////////////////////////////////////////////////////////////////////////////////////////////
            EXECUTE_UPDATE(this.DELETE_PERFIL_CLIENTES, paramDelete, con);
            if (Clientes!=null){
                for (int i = 0; i<Clientes.length; i++){
                    String [] paramInsert = { "FINV", Perfil, Clientes [i], Usuario, "#FECHA#", Usuario,  "#FECHA#" };
                    EXECUTE_UPDATE(INSERT_PERFIL_CLIENTES, paramInsert, con);
                }
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina upUsuariosClientes [AsignacionClientesUsuariosDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }    
    
    
}
