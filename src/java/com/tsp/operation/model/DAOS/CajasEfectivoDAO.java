package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.CajaIngreso;
import com.tsp.operation.model.beans.CajaPago;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.factura;
import com.tsp.operation.model.beans.factura_detalle;
import com.tsp.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 * dao para el programa de cajas de efectivo
 * 6/02/2012
 * @author darrieta
 */
public class CajasEfectivoDAO extends MainDAO {

    public CajasEfectivoDAO() {
        super("CajasEfectivoDAO.xml");
    }
    public CajasEfectivoDAO(String dataBaseName) {
        super("CajasEfectivoDAO.xml", dataBaseName);
    }
    
    /**
     * Inserta un registro en la tabla caja_ingreso
     * @param ingreso bean con los datos a ingresar
     * @return sql generado
     * @throws Exception 
     */
    public String insertarIngresoCaja(CajaIngreso ingreso) throws Exception{
        StringStatement st = null;
        String query = "INSERTAR_INGRESO_CAJA";
        String sql;        
        try{                        
            st = new StringStatement(this.obtenerSQL(query), true);
            int param = 1;
            st.setString(param++, ingreso.getDstrct());
            st.setString(param++, ingreso.getAgencia());
            st.setString(param++, ingreso.getFecha());
            st.setString(param++, ingreso.getNumIngreso());
            st.setDouble(param++, ingreso.getValor());
            st.setString(param++, ingreso.getCreation_user());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en insertarIngresoCaja [CajasEfectivoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    /**
     * Obtiene la ultima fecha en que se encuentre registrado saldo 
     * que sea menor o igual a la fecha seleccionada
     * @throws Exception 
     */
    public String consultarMaxFecha(String fechaInicial, String agencia) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "ULTIMA_FECHA_HISTORICO";
        String maxFecha = null;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, fechaInicial);
            st.setString(2, agencia);
            rs = st.executeQuery();
            if (rs.next()) {
                maxFecha = rs.getString("max");
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarMaxFecha [CajasEfectivoDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return maxFecha;
    }
    
    /**
     * Obtiene la ultima fecha en que se encuentre registrado saldo 
     * que sea menor o igual a la fecha seleccionada
     * @throws Exception 
     */
    public Double consultarSaldoInicial(String dstrct, String agencia, String fecha) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_SALDO";
        Double saldo = null;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            st.setString(2, agencia);
            st.setString(3, fecha);
            rs = st.executeQuery();
            if (rs.next()) {
                saldo = new Double(rs.getDouble("saldo_inicial"));
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarSaldoInicial [CajasEfectivoDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return saldo;
    }
    
    /**
     * Obtiene el total de anticipos de efectivo de una agencia en un rango de fechas
     * @throws Exception 
     */
    public Double consultarTotalAnticipos(String agencia, String fechaInicial, String fechaFinal) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "TOTAL_ANTICIPOS_AGENCIA";
        Double total = null;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, agencia);
            st.setString(2, fechaInicial);
            st.setString(3, fechaFinal);
            rs = st.executeQuery();
            if (rs.next()) {
                total = new Double(rs.getDouble("total"));
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarSaldoInicial [CajasEfectivoDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return total;
    }
    

    /**
     * Obtiene el total de pagos de una agencia en un rango de fechas
     * @param dstrct
     * @param agencia
     * @param fechaInicial
     * @param fechaFinal
     * @return sumatoria de pagos en el rango de fechas
     * @throws Exception 
     */
    public Double consultarTotalPagos(String dstrct, String agencia, String fechaInicial, String fechaFinal) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "TOTAL_PAGOS_AGENCIA";
        Double total = null;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            st.setString(2, agencia);
            st.setString(3, fechaInicial);
            st.setString(4, fechaFinal);
            rs = st.executeQuery();
            if (rs.next()) {
                total = new Double(rs.getDouble("total"));
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarTotalPagos [CajasEfectivoDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return total;
    }
    
    
    /**
     * Inserta un registro en la tabla caja_saldo
     * @param ingreso bean con los datos a ingresar
     * @return sql generado
     * @throws Exception 
     */
    public void insertarSaldoCaja(String dstrct, String agencia, String fecha, double saldo, String usuario) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "INSERTAR_CAJA_SALDO";       
        try{                        
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            int param = 1;
            st.setString(param++, dstrct);
            st.setString(param++, agencia);
            st.setString(param++, fecha);
            st.setDouble(param++, saldo);
            st.setString(param++, usuario);
            st.executeUpdate();
        }catch(Exception ex){
            throw new Exception("ERROR en insertarSaldoCaja [CajasEfectivoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
    }
    
    /**
     * Obtiene el saldo en canje de una agencia
     * @param dstrct
     * @param agencia
     * @return
     * @throws Exception 
     */
    public double consultarSaldoTotalCanje(String dstrct, String agencia) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SALDO_CANJE";
        double saldo = 0;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            st.setString(2, agencia);
            rs = st.executeQuery();
            if (rs.next()) {
                saldo = rs.getDouble("saldo");
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarSaldoTotalCanje [CajasEfectivoDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return saldo;
    }
    
    /**
     * Obtiene los saldos en canje de una agencia
     * @param dstrct
     * @param agencia
     * @return TreeMap<String, Double>
     * @throws Exception 
     */
    public TreeMap<String, Double> consultarSaldosCanje(String dstrct, String agencia) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "CONSULTAR_SALDOS_EN_CANJE";
        TreeMap<String, Double> saldos = new TreeMap<String, Double>();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            st.setString(2, agencia);
            rs = st.executeQuery();
            while(rs.next()) {
                Double valor = new Double(rs.getDouble("valor"));
                saldos.put(rs.getString("num_ingreso"), valor);
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarSaldosCanje [CajasEfectivoDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return saldos;
    }
    
    /**
     * Confirma la recepcion del dinero en caja
     * @param ingreso
     * @return sql generado
     * @throws Exception 
     */
    public String confirmarIngresoCaja(CajaIngreso ingreso) throws Exception{
        StringStatement st = null;
        String query = "CONFIRMAR_INGRESO";
        String sql;        
        try{                        
            st = new StringStatement(this.obtenerSQL(query), true);
            int param = 1;
            st.setString(param++, ingreso.getUsuarioRecibido());
            st.setString(param++, ingreso.getDstrct());
            st.setString(param++, ingreso.getAgencia());
            st.setString(param++, ingreso.getNumIngreso());
            sql = st.getSql();
        }catch(Exception ex){
            throw new Exception("ERROR en confirmarIngresoCaja [CajasEfectivoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    /**
     * Suma el saldo del ingreso confirmado al saldo inicial del dia
     * @param ingreso
     * @return sql generado
     * @throws Exception 
     */
    public String actualizarSaldoCaja(String dstrct, String agencia, String numIngreso, String usuario) throws Exception{
        StringStatement st = null;
        String query = "ACTUALIZAR_SALDO";
        String sql;        
        try{                        
            st = new StringStatement(this.obtenerSQL(query), true);
            int param = 1;
            st.setString(param++, usuario);
            st.setString(param++, numIngreso);
            st.setString(param++, agencia);
            st.setString(param++, agencia);
            st.setString(param++, dstrct);
            st.setString(param++, dstrct);
            sql = st.getSql();
        }catch(Exception ex){
            throw new Exception("ERROR en actualizarSaldoCaja [CajasEfectivoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    /**
     * Obtiene los pagos realizados por una agencia en un rango de fechas
     * @param dstrct distrito del usuario en sesion
     * @param agencia agencia a buscar
     * @param fechaInicial fecha inicial
     * @param fechaFinal fecha final
     * @return ArrayList<CajaPago> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<CajaPago> consultarPagosAgencia(String dstrct, String agencia, String fechaInicial, String fechaFinal) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "PAGOS_AGENCIA";
        ArrayList<CajaPago> pagos = new ArrayList<CajaPago>();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            st.setString(2, agencia);
            st.setString(3, fechaInicial);
            st.setString(4, fechaFinal);
            rs = st.executeQuery();
            
            while (rs.next()) {
                CajaPago cajaPago = CajaPago.load(rs);
                cajaPago.setCedulaBeneficiario(rs.getString("cedula_beneficiario"));
                pagos.add(cajaPago);
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarPagosAgencia [CajasEfectivoDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return pagos;
    }
    
    /**
     * Obtiene los anticipos realizados por una agencia en un rango de fechas
     * @param dstrct distrito del usuario en sesion
     * @param agencia agencia a buscar
     * @param fechaInicial fecha inicial
     * @param fechaFinal fecha final
     * @return ArrayList<CajaPago> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<CajaPago> consultarPagosAnticipos(String dstrct, String agencia, String fechaInicial, String fechaFinal) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "ANTICIPOS_AGENCIA";
        ArrayList<CajaPago> pagos = new ArrayList<CajaPago>();
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, dstrct);
            st.setString(2, agencia);
            st.setString(3, fechaInicial);
            st.setString(4, fechaFinal);
            rs = st.executeQuery();
            while (rs.next()) {
                CajaPago cajaPago = new CajaPago();                
                cajaPago.setFecha(rs.getString("fecha_transferencia"));
                cajaPago.setNumDocumento(rs.getString("planilla"));
                cajaPago.setCedulaBeneficiario(rs.getString("cedcon"));
                cajaPago.setNombreBeneficiario(rs.getString("nombre_conductor"));
                cajaPago.setValor(rs.getDouble("vlr"));
                cajaPago.setComision(rs.getDouble("porcentaje"));
                cajaPago.setValorComision(rs.getDouble("vlr_descuento"));
                cajaPago.setValorEntregar(rs.getDouble("vlr_consignacion"));                
                pagos.add(cajaPago);
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarPagosAnticipos [CajasEfectivoDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return pagos;
    }
    
    /**
     * Inserta un registro en la tabla caja_pago
     * @param pago
     * @return sql generado
     * @throws Exception 
     */
    public String insertarCajaPago(CajaPago pago) throws Exception{
        StringStatement st = null;
        String query = "INSERTAR_PAGO";
        String sql;        
        try{                        
            st = new StringStatement(this.obtenerSQL(query), true);
            int param = 1;            
            st.setString(param++, pago.getDstrct());
            st.setString(param++, pago.getAgencia());
            st.setString(param++, pago.getFecha());
            st.setString(param++, pago.getTransportadora());
            st.setString(param++, pago.getTipoDocumento());
            st.setString(param++, pago.getNumDocumento());
            st.setString(param++, pago.getBeneficiario());
            st.setString(param++, pago.getBanco());
            st.setDouble(param++, pago.getValor());
            st.setDouble(param++, pago.getComision());
            st.setDouble(param++, pago.getValorComision());
            st.setString(param++, pago.getCreationUser());
            st.setString(param++, pago.getNumCxc());
            st.setDouble(param++, pago.getComisionBancaria());
            sql = st.getSql();
        }catch(Exception ex){
            throw new Exception("ERROR en insertarCajaPago [CajasEfectivoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    /**
     * Obtiene el numero para la cxc a generar
     * @throws Exception 
     */
    public String consultarSerieCXC(String document_type) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "OBTENER_SERIE_CXC";
        String serie = null;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, document_type);
            rs = st.executeQuery();
            if (rs.next()) {
                serie = rs.getString("serie");
            }
        } catch (Exception e) {
            throw new Exception("Error en consultarSerieCXC [CajasEfectivoDAO] " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return serie;
    }
    

    public String ingresarFactura(factura factu,String distrito,String usuario,String base) throws Exception {
        StringStatement st = null;
        String sql ="";
        String nombre_sql="SQL_INGRESAR_FACTURA";
        try{
            st = new StringStatement (this.obtenerSQL(nombre_sql), true);//JJCastro fase2
            
            int param = 1;
            st.setString(param++,""+distrito);
            st.setString(param++,factu.getDocumento());
            st.setString(param++,""+factu.getNit());
            st.setString(param++,""+factu.getCodcli());
            st.setString(param++,"TR");
            st.setString(param++, ""+factu.getFecha_factura());
            st.setString(param++, ""+Util.fechaFinalYYYYMMDD(factu.getFecha_factura(),Integer.parseInt(factu.getPlazo())));
            st.setString(param++, ""+factu.getDescripcion());
            st.setString(param++, ""+factu.getObservacion());
            st.setString(param++,""+factu.getValor_factura());
            st.setString(param++,"0");
            st.setString(param++,""+factu.getValor_factura());
            st.setString(param++,""+factu.getValor_facturame());
            st.setString(param++,""+factu.getValor_facturame());
            st.setString(param++,""+factu.getValor_tasa());
            st.setString(param++, ""+factu.getMoneda());
            st.setString(param++,""+factu.getCantidad_items());
            st.setString(param++, ""+factu.getForma_pago());

            st.setString(param++, ""+factu.getAgencia_facturacion());
            st.setString(param++, ""+factu.getAgencia_cobro());
            st.setString(param++, ""+base);
            st.setString(param++, ""+usuario);
            st.setString(param++, ""+usuario);
            st.setString(param++, ""+factu.getRif());
            st.setString(param++, ""+factu.getCmc());
            st.setString(param++, ""+factu.getZona());
            st.setString(param++, ""+factu.getAgencia_facturacion());
            sql +=  st.getSql()+";";
            System.out.println(sql);
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("ERROR AL GRABAR LA FACTURA" + e.getMessage() );
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    public String ingresarDetalleFactura(factura_detalle factud,String distrito,String usuario,String base) throws Exception {
        StringStatement ps = null;
        String sql ="";
        String nombre_sql="SQL_INGRESAR_FACTURA_DETALLE";
        try{
            ps = new StringStatement (this.obtenerSQL(nombre_sql), true);//JJCastro fase2
            
            int param = 1;
            if(factud.getFecrem()==null){
                factud.setFecrem("");
            }

            ps.setString(1,""+distrito);
            ps.setString(2,""+factud.getDocumento());
            ps.setString(3,""+factud.getItem());
            ps.setString(4,""+factud.getNit());

            if(factud.getConcepto()==null){
                factud.setConcepto("TR");
            }

            ps.setString(5, factud.getConcepto());
            ps.setString(6,""+factud.getNumero_remesa());
            ps.setString(7,""+ factud.getDescripcion());

            String auxili ="";
            if(factud.getAux().equals("S")){
                auxili=factud.getTiposubledger()+"-"+factud.getAuxliliar();
            }

            ps.setString(8,""+ factud.getCodigo_cuenta_contable());
            ps.setString(9,auxili);
            ps.setString(10,""+factud.getCantidad());
            ps.setString(11,""+factud.getValor_unitario());
            ps.setString(12, ""+factud.getValor_unitariome());
            ps.setString(13,""+factud.getValor_item());
            ps.setString(14, ""+factud.getValor_itemme());
            ps.setString(15, ""+factud.getValor_tasa());
            ps.setString(16,""+factud.getMoneda());
            ps.setString(17,""+base);
            ps.setString(18,""+usuario);
            ps.setString(19,""+usuario);

            sql +=  ps.getSql()+";";

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception("ERROR AL GRABAR LA FACTURA DETALLE" + e.getMessage() );
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
}
