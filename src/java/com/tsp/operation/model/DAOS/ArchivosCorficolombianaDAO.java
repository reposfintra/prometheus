/*
 * ArchivosCorficolombianaDAO.java
 *
 * Created on 27 de octubre de 2008, 12:14
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Fintra
 */
public class ArchivosCorficolombianaDAO extends MainDAO{
    public ArchivosCorficolombianaDAO() {
        super("ArchivosCorficolombianaDAO.xml");
    }
    
    public ArrayList getRegistros(String fecini,String fecfin,String op) throws Exception{
        Connection con = null;
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        ArrayList            respuesta   = new ArrayList();
        String            query   = "SQL_ARCHIVO_PRINCIPALCC";
        String registro="";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,fecini);
            st.setString(2,fecfin);
            st.setString(3,op);
            //System.out.println("st.toString"+st.toString());
            rs = st.executeQuery();
            
            while ( rs.next() ){
                registro=rs.getString("reg");
                respuesta.add(registro);  
            }            
            
            }}catch(Exception e){
            System.out.println("error en getRegistros :"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("Error en getRegistros --->" + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;        
    }
}
