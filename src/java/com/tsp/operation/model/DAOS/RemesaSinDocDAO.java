/***********************************************************************************
 * Nombre clase : ............... RemesaSinDocDAO.java                             *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD para generar los datos del reporte     *
 *                                de viaje.                                        *
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Fecha :........................ 18 de noviembre de 2005, 10:10 AM              *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;

public class RemesaSinDocDAO {
    
    private static String SQLCONSULTA   =   "   SELECT R.NUMREM,R.FECREM, R.CLIENTE, C.NOMCIU AS ORIGEN, CI.NOMCIU AS DESTINO,                                                          "+
                                            "   R.PESOREAL AS PESO, R.UNIT_OF_WORK AS UNIDAD, CD.NOMCIU AS AGENCIA, to_char(R.CREATION_DATE,'YYYY-MM-DD HH24:MI:SS') AS CREATION_DATE, " +
                                            "   R.USUARIO                                                                                                                               "+
                                            "   FROM REMESA R                                                                                                                           "+
                                            "   LEFT OUTER JOIN CIUDAD C ON ( C.CODCIU = R.ORIREM )                                                                                     "+
                                            "   LEFT OUTER JOIN CIUDAD CI ON ( CI.CODCIU = R.DESREM )                                                                                   "+
                                            "   LEFT OUTER JOIN CIUDAD CD ON ( CD.CODCIU = R.AGCREM )                                                                                   "+
                                            "   WHERE R.FECREM BETWEEN ? AND ?                                                                                                          "+
                                            "   AND R.AGCREM LIKE ? AND R.USUARIO LIKE ?                                                                                                "+
                                            "   AND R.NUMREM NOT IN (SELECT NUMREM FROM REMESA_DOCTO)                                                                                   "+
                                            "   ORDER BY R.AGCREM, R.FECREM, R.NUMREM                                                                                                   ";
    
    /** Creates a new instance of RemesaSinDocDAO */
    public RemesaSinDocDAO() {
    }
    
    /**
     * Metodo listRemesasSinDoc, lista todas las remesas que no tienen documentos asociados o documentos asociados al 
     * destinatario entre un rango de fechas , y con agencia y usuario de creacion como parametros 
     * adicionales
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String agencia , String usuario, String fecha inicial , String fecha final
     * @version : 1.0
     */
    public List listRemesasSinDoc( String agencia, String usuario, String fechaInicial, String fechaFinal ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLCONSULTA);
            st.setString(1, fechaInicial);
            st.setString(2, fechaFinal);
            st.setString(3, agencia);
            st.setString(4, usuario);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(RemesaSinDoc.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LIST [ReporteViajeDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return lista;
    }
    
}
