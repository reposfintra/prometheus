/***********************************************************************************
 * Nombre clase : ............... UsuarioAprobacionDAO.java
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 12 de enero de 2006, 08:15 AM                  *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;


public class UsuarioAprobacionDAO extends MainDAO{
    
    /** Creates a new instance of UsuarioAprobacionDAO */
    public UsuarioAprobacionDAO() {
        super("UsuarioAprobacionDAO.xml");
    }
    public UsuarioAprobacionDAO(String dataBaseName) {
        super("UsuarioAprobacionDAO.xml", dataBaseName);
    }
    
    private UsuarioAprobacion usuapro;
    private Vector vec; 
    

    private static String SQL_LISTADO_APROBACION="SELECT DISTINCT tabla FROM usuario_aprobacion WHERE usuario_aprobacion = ? ORDER BY tabla";
    
    private static String SQL_INSERTAR = " INSERT INTO usuario_aprobacion (dstrct, id_agencia, tabla, usuario_aprobacion, last_update, user_update, creation_date, creation_user,base)" +
                                         " VALUES (?,?,?,?,?,?,?,?,?)";
    
    private static String SQL_ELIMINAR = "DELETE  FROM usuario_aprobacion " +
                                       " WHERE    id_agencia = ? " +
                                       "  AND tabla = ? " +
                                       "  AND usuario_aprobacion = ?";
    
    private static String SQL_BUSQUEDA = " SELECT a.usuario_aprobacion,  " +
                                            "     a.tabla,  " +
                                            "     a.id_agencia," +
                                            "     b.nomciu  " +
                                            "    FROM usuario_aprobacion a," +
                                            "      ciudad b  " +
                                            "     WHERE  a.id_agencia like ? " +
                                            "       AND  a.tabla like ?  " +
                                            "       AND  a.usuario_aprobacion like ? " +
                                            "       AND  a.id_agencia =  b.codciu ";
    
    
    private static String SQL_BUSCAR = "SELECT usuario_aprobacion, " +
                                        "       tabla, " +
                                        "       id_agencia, " +
                                        "FROM usuario_aprobacion " +
                                        "WHERE  id_agencia = ? " +
                                        "  AND  tabla = ? " +
                                        "  AND  usuario_aprobacion = ? ";
    
    private static String SQL_ACTUALIZAR = "UPDATE usuario_aprobacion " +
                                           "  SET  id_agencia = ?," +
                                           "       tabla = ?,  " +
                                           "       usuario_aprobacion = ?, " +
                                           "       last_update = ?, " +
                                           "       user_update = ?" +
                                           "  WHERE  id_agencia = ?  " +
                                           "  AND  tabla = ?  " +
                                           "  AND  usuario_aprobacion = ?";
    
    /**
     * Metodo obtVecTablaAprovacion, retorna el vector de las tabla 
     *        que puede aprovar el usuario 
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector obtVecTablaAprovacion (){
        return this.vec;
    }
     /**
     * Metodo setUsuarioAprobacion, setea el objeto al    
     *        que puede aprovar el usuario 
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void setUsuarioAprobacion(UsuarioAprobacion usuapro){
        this.usuapro = usuapro;
    }
    
    /**
     * Metodo listarTablaUsuAprobacion, lista tabla usuario de aprovacion                  
     * @param: login
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void listarTablaUsuAprobacion( String login ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        vec = new Vector();
        String query = "SQL_LISTADO_APROBACION";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, login);
                rs = st.executeQuery();
                
                while (rs.next()){
                    usuapro = new UsuarioAprobacion();
                    usuapro.setTabla(rs.getString("tabla"));
                    vec.add(usuapro); 
                }

            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR TABLA USUARIO APROBACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo getUsuarioAprobacion,obtiene el objeto del usuario aprobacion                  
     * @param: 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public UsuarioAprobacion getUsuarioAprobacion(){
        return this.usuapro;
    }
    
    /**
     * Metodo insertarUsuario, lista tabla usuario de aprovacion                  
     * @param: login
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void insertarUsuario( ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_INSERTAR";
        
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, usuapro.getDstrct());
                st.setString(2, usuapro.getId_agencia());
                st.setString(3, usuapro.getTabla());
                st.setString(4, usuapro.getUsuario_aprobacion());  
                st.setString(5, usuapro.getLast_update());
                st.setString(6, usuapro.getUser_update());
                st.setString(7, usuapro.getCreation_date());
                st.setString(8, usuapro.getCreation_user());
                st.setString(9, usuapro.getBase());
                st.executeUpdate();               

            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR TABLA USUARIO APROBACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo listarUsuariosAprobacion, lista tabla usuario de aprovacion                  
     * @param: agencia,tabla,usuario
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void listarUsuariosAprobacion( String agencia, String tabla, String usuario ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        vec = new Vector();
        String query = "SQL_BUSQUEDA";
        
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, agencia+"%");
                st.setString(2, tabla+"%");
                st.setString(3, usuario+"%");
                rs = st.executeQuery();
                
                while (rs.next()){
                    usuapro = new UsuarioAprobacion();
                    usuapro.setTabla(rs.getString("tabla"));
                    usuapro.setId_agencia(rs.getString("id_agencia"));
                    usuapro.setUsuario_aprobacion(rs.getString("usuario_aprobacion"));
                    usuapro.setNomagencia(rs.getString("nomciu"));
                    vec.add(usuapro); 
                }

            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR TABLA USUARIO APROBACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo anularUsuarioAprobacion, anula un registro
     * de la tabla usuario_aprobacion                  
     * @param: agencia,tabla,usuario
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void eliminarUsuarioAprobacion() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, usuapro.getId_agencia());
                st.setString(2, usuapro.getTabla());
                st.setString(3, usuapro.getUsuario_aprobacion()); 
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR IDENTIDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo buscarUsuarioAprobacion, lista los usuario de aprobacion
     * UsuarioAprobacion
     * @param: agencia,tabla,usuario
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void buscarUsuarioAprobacion( String agencia, String tabla, String usuario ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        usuapro = null;
        String query = "SQL_BUSQUEDA";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, agencia);
                st.setString(2, tabla);
                st.setString(3, usuario);
                rs = st.executeQuery();
                
                if (rs.next()){
                    this.usuapro = new UsuarioAprobacion();
                    usuapro.setTabla(rs.getString("tabla"));
                    usuapro.setId_agencia(rs.getString("id_agencia"));
                    usuapro.setUsuario_aprobacion(rs.getString("usuario_aprobacion"));                    
                }

            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR TABLA USUARIO APROBACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo buscarUsuariosAprobacion, busca el objeto tipo                   
     * UsuarioAprobacion
     * @param: agencia,tabla,usuario
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public boolean existeUsuariosAprobacion( String agencia, String tabla, String usuario ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        String query = "SQL_BUSQUEDA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, agencia);
                st.setString(2, tabla);
                st.setString(3, usuario);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true ;                  
                }

            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR TABLA USUARIO APROBACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
     /**
     * Metodo modificarUsuario, modifica los usuario de aprovacion                  
     * @param: login
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    public void modificarUsuario( ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_ACTUALIZAR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, usuapro.getId_agencia());
                st.setString(2, usuapro.getTabla());
                st.setString(3, usuapro.getUsuario_aprobacion());  
                st.setString(4, usuapro.getLast_update());
                st.setString(5, usuapro.getUser_update());
                st.setString(6, usuapro.getId_agenciaAct());
                st.setString(7, usuapro.getTablaAct());
                st.setString(8, usuapro.getUsuario_aprobacionAct());                
                st.executeUpdate();               

            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR TABLA USUARIO APROBACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
}
