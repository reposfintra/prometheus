/***********************************************************************************
 * Nombre clase :                 ReferenciaDAO.java                  
 * Descripcion :                  Clase que maneja los DAO ( Data Access Object )  
 *                                los cuales contienen los metodos que interactuan 
 *                                con la BD.                                       
 * Autor :                        Ing. Henry Osorio          
 * Fecha :                        24 de septiembre de 2005, 13:07
 * Version :  1.0          
 * Copyright : Fintravalores S.A.                         
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.*;
import java.sql.*;
import java.util.*;

public class ReferenciaDAO {
    
    private Vector vecrefs;
    private Referencia ref;
    
    private static String SQL_INS_ref =
    "INSERT into referencia " +
    "Values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    private static String SQL_UPDATE_ref =
    "UPDATE referencia " +
    "    set nomempresa=?, telefono=?, ciudadtel=?, direccion=?, ciudaddir=?, contacto=?, " +
    "    cargo_contacto=?, relacion=?, referencia=?, user_update=?, last_update=?, otra_relacion = ? " +
    "WHERE " +
    "    dstrct_code=? AND tiporef=? AND clase=? AND fecref=? ";
    
    private static String BUSC_refS   = "SELECT * from referencia WHERE clase=? order by tiporef";
    
    private static final String EXISTE_REF = "SELECT " +
    "      count(*) " +
    "FROM " +
    "      referencia " +
    "WHERE " +
    "      dstrct_code=? AND " +
    "      tiporef=? AND " +
    "      clase=?";
    //Diogenes 24-02-2006
    private static final String VERIFICAR_REFERENCIA = "UPDATE referencia SET" +
    " usuarioconfir = ?," +
    " fecconfir = ?," +
    " ref_confirmacion = ?," +
    " reg_status = ?" +
    " WHERE          " +
    "      dstrct_code = ? AND " +
    "      tiporef = ? AND " +
    "      clase = ? AND" +
    "      fecref = ?";
    
  
    private static final String BUSCAR_REFERENCIAPLACA = " SELECT A.*, " +
    "      B.nomciu as CiudadTelefono,   " +
    "      C.nomciu as CiudadDireccion,     " +
    "      CASE WHEN relacion = 'OTRO' then  " +
    "          otra_relacion      " +
    "      ELSE " +
    "          D.descripcion" +
    "      END as desrelacion," +
    "      COALESCE( E.nombre, '' ) as nombre " +
    "  FROM" +
    "      (SELECT * " +
    "        FROM referencia " +
    "        WHERE " +
    "            dstrct_code = ? AND" +
    "            clase= ?  AND " +
    "            tiporef = 'EA' )A" +
    "        LEFT JOIN ciudad B  ON (B.codciu = A.ciudadtel)" +
    "        LEFT JOIN ciudad C  ON (C.codciu = A.ciudaddir)" +
    "        LEFT JOIN tablagen D  ON (D.table_type = 'RELACION' AND table_code = A.relacion)" +
    "        LEFT JOIN usuarios E  ON (upper(E.idusuario) = upper(A.usuarioconfir)) " +
    "ORDER BY  fecref ";
    
    private static final String BUSCAR_REFERENCIANIT = " SELECT A.*, " +
    "      B.nomciu as CiudadTelefono,   " +
    "      C.nomciu as CiudadDireccion,     " +
    "      CASE WHEN relacion = 'OTRO' then  " +
    "          otra_relacion      " +
    "      ELSE " +
    "          D.descripcion" +
    "      END as desrelacion," +
    "      COALESCE( E.nombre, '' ) as nombre " +
    "  FROM" +
    "      (SELECT * " +
    "        FROM referencia " +
    "        WHERE " +
    "            dstrct_code = ? AND" +
    "            clase= ? AND     " +
    "        tiporef IN ('CO','EC','PR')" +
    "        order by tiporef)A" +
    "        LEFT JOIN ciudad B  ON (B.codciu = A.ciudadtel)" +
    "        LEFT JOIN ciudad C  ON (C.codciu = A.ciudaddir)" +
    "        LEFT JOIN tablagen D  ON (D.table_type = 'RELACION' AND table_code = A.relacion)" +
    "        LEFT JOIN usuarios E  ON (upper(E.idusuario) = upper(A.usuarioconfir)) " +
    "ORDER BY  tiporef, fecref ";
    
    
    /** Creates a new instance of refsDAO */
    public ReferenciaDAO() {
    }
    
    /**
     * Metodo ocurrenciasTipoReferencia, cuenta el numero de referencias de acurdo al tipo,
     *        retorna una arreglo de enteros con la cantidad de referencia de cada tipo
     * @param: vector
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public int[] ocurrenciasTipoReferencia(Vector vec){
        int[] num = new int[4];
        num[0] = 0;num[1] = 0;num[2] = 0;num[3] = 0;
        for (int i=0; i<vec.size(); i++){
            Referencia ref = (Referencia) vec.elementAt(i);
            if(ref.getTiporef().equals("EA"))
                num[0]++;
            else if (ref.getTiporef().equals("CO"))
                num[1]++;
            else if (ref.getTiporef().equals("PR"))
                num[2]++;
            else if (ref.getTiporef().equals("EC"))
                num[3]++;
        }
        return num;
    }
     /**
     * Metodo insertarReferenciaEmpresaAfiliada, insertar los registros a la tabla referencia 
     *         para la empresa afiliadora,
     * @param: objeto Referencia
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public void insertarReferenciaEmpresaAfiliada(Referencia ref) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(this.SQL_INS_ref);
            psttm.setString(1, ref.getDstrct_code());
            psttm.setString(2, ref.getTiporef());
            psttm.setString(3, ref.getClase());
            psttm.setString(4, ref.getFecref());
            psttm.setString(5, ref.getNomempresa());
            psttm.setString(6, ref.getTelefono());
            psttm.setString(7, ref.getCiudadtel());
            psttm.setString(8, ref.getDireccion());
            psttm.setString(9, ref.getCiudaddir());
            psttm.setString(10, ref.getContacto());
            psttm.setString(11, ref.getCargo_contacto());
            psttm.setString(12, "");
            psttm.setString(13, ref.getReferencia());
            psttm.setString(14, "");
            psttm.setString(15, "0099-01-01 00:00:00");
            psttm.setString(16, "");
            psttm.setString(17, ref.getBase());
            psttm.setString(18, ref.getCreation_user());
            psttm.setString(19, ref.getCreation_date());
            psttm.setString(20, "");
            psttm.setString(21, "now()");
            psttm.setString(22, "");
            psttm.setString(23, "");
            psttm.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DE LA ref: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
     /**
     * Metodo insertarReferenciaConductorPropietario, insertar los registros a la tabla referencia 
     *         de los conductores y/o propieatios.
     * @param: objeto Referencia
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public void insertarReferenciaConductorPropietario(Referencia ref) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(this.SQL_INS_ref);
            psttm.setString(1, ref.getDstrct_code());
            psttm.setString(2, ref.getTiporef());
            psttm.setString(3, ref.getClase());
            psttm.setString(4, ref.getFecref());
            psttm.setString(5, ref.getNomempresa());
            psttm.setString(6, ref.getTelefono());
            psttm.setString(7, ref.getCiudadtel());
            psttm.setString(8, ref.getDireccion());
            psttm.setString(9, ref.getCiudaddir());
            psttm.setString(10, "");//Contacto
            psttm.setString(11, "");//Cargo contacto
            psttm.setString(12, ref.getRelacion());//relacion
            psttm.setString(13, ref.getReferencia());//ref
            psttm.setString(14, "");
            psttm.setString(15, "0099-01-01 00:00:00");
            psttm.setString(16, "");
            psttm.setString(17, ref.getBase());
            psttm.setString(18, ref.getCreation_user());
            psttm.setString(19, ref.getCreation_date());
            psttm.setString(20, "");
            psttm.setString(21, "now()");
            psttm.setString(22, "");
            psttm.setString(23, ref.getOtra_relacion());
            psttm.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DE LA ref: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     * Metodo insertarReferenciaEmpresaCarga, insertar los registros a la tabla referencia 
     *         de los conductores y/o propieatios.
     * @param: objeto Referencia
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public void insertarReferenciaEmpresaCarga(Referencia ref) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(this.SQL_INS_ref);
            psttm.setString(1, ref.getDstrct_code());
            psttm.setString(2, ref.getTiporef());
            psttm.setString(3, ref.getClase());
            psttm.setString(4, ref.getFecref());
            psttm.setString(5, ref.getNomempresa());
            psttm.setString(6, ref.getTelefono());
            psttm.setString(7, ref.getCiudadtel());
            psttm.setString(8, ref.getDireccion());
            psttm.setString(9, ref.getCiudaddir());
            psttm.setString(10, ref.getContacto());//Contacto
            psttm.setString(11, ref.getCargo_contacto());//Cargo contacto
            psttm.setString(12, "");//relacion
            psttm.setString(13, ref.getReferencia());//ref
            psttm.setString(14, "");
            psttm.setString(15, "0099-01-01 00:00:00");
            psttm.setString(16, "");
            psttm.setString(17, ref.getBase());
            psttm.setString(18, ref.getCreation_user());
            psttm.setString(19, ref.getCreation_date());
            psttm.setString(20, "");
            psttm.setString(21, "now()");
            psttm.setString(22, "");
            psttm.setString(23, "");
            ////System.out.println("--------------> "+psttm);
            psttm.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DE LA ref: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     * Metodo updateReferencia, actualiza los registros a la tabla referencia 
     * @param: objeto Referencia
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public void updateReferencia(Referencia ref) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            
            psttm = con.prepareStatement(this.SQL_UPDATE_ref);
            psttm.setString(1, ref.getNomempresa());
            psttm.setString(2, ref.getTelefono());
            psttm.setString(3, ref.getCiudadtel());
            psttm.setString(4, ref.getDireccion());
            psttm.setString(5, ref.getCiudaddir());
            psttm.setString(6, ref.getContacto());
            psttm.setString(7, ref.getCargo_contacto());
            psttm.setString(8, ref.getRelacion());//relacion
            psttm.setString(9, ref.getReferencia());//ref
            psttm.setString(10, ref.getUser_update());//usr actu
            psttm.setString(11, ref.getLast_update()); //fec actu
            psttm.setString(12, ref.getOtra_relacion());//otra relacion
            psttm.setString(13, ref.getDstrct_code());
            psttm.setString(14, ref.getTiporef());
            psttm.setString(15, ref.getClase());
            psttm.setString(16, ref.getFecref());
            ////System.out.println("SQL: "+psttm.toString());
            psttm.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACIÓN DE LA ref: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     * Metodo existeEmpresaAfiliada, retorna la cantidad de registros que tiene 
     *    la tabla referencia deacuerdo al documento 
     * @param: objeto Referencia
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public int existeEmpresaAfiliada(Referencia refer) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(EXISTE_REF);
            psttm.setString(1, refer.getDstrct_code());
            psttm.setString(2, refer.getTiporef());
            psttm.setString(3, refer.getClase());
            rs = psttm.executeQuery();
            if (rs.next()){
                return rs.getInt(1);
            }
            return 0;
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACIÓN DE LA ref: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    /**
     * Metodo buscarReferencias, busca el objeto referencia por medio
     *                           el documento
     * @param: objeto Referencia
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public void buscarReferencias(String doc) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        ref = null;
        vecrefs = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(this.BUSC_refS);
            psttm.setString(1, doc);
            rs = psttm.executeQuery();
            while(rs.next()){
                ref = new Referencia();
                ref.setDstrct_code(rs.getString("dstrct_code"));
                ref.setClase(rs.getString("clase"));
                ref.setTiporef(rs.getString("tiporef"));
                ref.setFecref(rs.getString("fecref"));
                ref.setNomempresa(rs.getString("nomempresa"));
                ref.setTelefono(rs.getString("telefono"));
                ref.setCiudadtel(rs.getString("ciudadtel"));
                ref.setContacto(rs.getString("contacto"));
                ref.setCargo_contacto(rs.getString("cargo_contacto"));
                ref.setReferencia(rs.getString("referencia"));
                ref.setRelacion(rs.getString("relacion"));
                ref.setDireccion(rs.getString("direccion"));
                ref.setCiudaddir(rs.getString("ciudaddir"));
                ref.setOtra_relacion(rs.getString("otra_relacion"));
                vecrefs.addElement(ref);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACIÓN DE LA ref: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    /**
     * Metodo getVectorReferencias, retorna el vector de tipo referencia
     * @param: objeto Referencia
     * @autor : Ing. Henry Osorio
     * @version : 1.0
     */
    public Vector getVectorReferencias(){
        return vecrefs;
    }
    /**
     * Metodo verificarReferencia, Verifica las referencias si es correcta o incorrecta,
     * actualizando el usuario que realiza la verificación
     * @param: objeto Referencia
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void verificarReferencia(Referencia ref) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            
            psttm = con.prepareStatement(this.VERIFICAR_REFERENCIA);
            psttm.setString(1, ref.getUsrconfirma());
            psttm.setString(2, ref.getFecconfir());
            psttm.setString(3, ref.getRef_confirmacion());
            psttm.setString(4, ref.getReg_status());
            psttm.setString(5, ref.getDstrct_code());
            psttm.setString(6, ref.getTiporef());
            psttm.setString(7, ref.getClase());
            psttm.setString(8, ref.getFecref());
            ////System.out.println(psttm);
            psttm.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA VERIFICACION DE LA REF: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    /**
     * Metodo buscarReferencias, buscar la referencias del documento dependiendo el
     * documento y el distrito
     * @param: documento, distrito, tipo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarReferencias(String doc, String dstrct, String tipo) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        ref = null;
        vecrefs = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            if ( tipo.equals("C") ){
                psttm = con.prepareStatement(this.BUSCAR_REFERENCIANIT);
            }
            else{
                psttm = con.prepareStatement(this.BUSCAR_REFERENCIAPLACA);
            }
            psttm.setString(1, dstrct);
            psttm.setString(2, doc);
            ////System.out.println(psttm);
            rs = psttm.executeQuery();
            while(rs.next()){
                ref = new Referencia();
                ref.setDstrct_code(rs.getString("dstrct_code"));
                ref.setClase(rs.getString("clase"));
                ref.setTiporef(rs.getString("tiporef"));
                ref.setFecref(rs.getString("fecref"));
                ref.setNomempresa(rs.getString("nomempresa"));
                ref.setTelefono(rs.getString("telefono"));
                ref.setCiudadtel(rs.getString("ciudadtelefono"));
                ref.setContacto(rs.getString("contacto"));
                ref.setCargo_contacto(rs.getString("cargo_contacto"));
                ref.setReferencia(rs.getString("referencia"));
                ref.setRelacion(rs.getString("desrelacion"));
                ref.setDireccion(rs.getString("direccion"));
                ref.setCiudaddir(rs.getString("ciudaddireccion"));
                ref.setReg_status(rs.getString("reg_status"));
                //campos de verificacion
                ref.setUsrconfirma(rs.getString("nombre"));
                ref.setFecconfir(rs.getString("fecconfir").substring(0,16));
                ref.setRef_confirmacion(rs.getString("ref_confirmacion"));
                vecrefs.addElement(ref);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA ref: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    
    
}
