/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.Siniestros;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author jpinedo
 */



public class SiniestrosDAO extends MainDAO{


    public SiniestrosDAO() {
        super("SiniestrosDAO.xml");
    }
    public SiniestrosDAO(String dataBaseName) {
        super("SiniestrosDAO.xml", dataBaseName);
    }



/*--------------------------------------------------------
    * Autor jpinedo
    * Consulta  VALIDAR SINIESTROS ACTIVOS (F, N, A, C )
    */
       public boolean    Validar_Siniestros_Activos_1(String id) throws Exception
       {
        boolean  estado=false;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_VALIDAR_SINIESTROS_ACTIVOS_1";
        try
        {
            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, id);
            rs=st.executeQuery();
            if(rs.next())
            {
              estado=true;
            }

        }
        catch(Exception e)
        {
            throw new Exception( " Error Validaondo Siniestros " + e.getMessage());
        }
        finally
        {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return estado;
    }










     /*
      * Autor jpinedo
      * Consulta  VALIDAR SINIESTROS ACTIVOS (F, N, A, C )
      */
       public boolean    Validar_Siniestros_Activos_2(String id) throws Exception
       {
        boolean  estado=false;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_VALIDAR_SINIESTROS_ACTIVOS_2";
        try
        {
            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, id);
            rs=st.executeQuery();
            if(rs.next())
            {
              estado=true;
            }

        }
        catch(Exception e)
        {
            throw new Exception( " Error Validaondo Siniestros " + e.getMessage());
        }
        finally
        {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return estado;
    }



       /*
      * Autor jpinedo
      * Consulta  VALIDAR SINIESTROS ACTIVOS (F, N, A, C )
      */
       public boolean    Validar_Siniestros_Activos_3(String id) throws Exception
       {
        boolean  estado=false;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_VALIDAR_SINIESTROS_ACTIVOS_3";
        try
        {
            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, id);
            rs=st.executeQuery();
            if(rs.next())
            {
              estado=true;
            }

        }
        catch(Exception e)
        {
            throw new Exception( " Error Validaondo Siniestros " + e.getMessage());
        }
        finally
        {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return estado;
    }





        /**
     * M�todo que lista siniestros enviados
     * @autor.......jpinedo
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ArrayList< Siniestros> ListarSiniestro( String id) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_LISTAR_SINIESTROS_ACTIVOS";
        ArrayList lista = new ArrayList();

        try{

            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, id);
            rs=st.executeQuery();
            while(rs.next()){
                Siniestros siniestros = new  Siniestros();
                siniestros.setTipo(rs.getString("Tipo_Consulta_Siniestro"));
                siniestros.setNombre_siniestro(rs.getString("Nombre_del_Siniestro"));
                siniestros.setDireccion(rs.getString("direccion"));
                siniestros.setTelefono1(rs.getString("Telefono_1_siniestro"));
                siniestros.setTelefono2(rs.getString("Telefono_2_siniestro"));
                siniestros.setCelular(rs.getString("Telefono_Celular_siniestro"));
                siniestros.setCodigo_banco(rs.getString("Codigo_de_Banco"));
                siniestros.setBanco(rs.getString("nombre_banco"));
                siniestros.setNumero_cuenta_siniestro(rs.getString("Numero_Cuenta_siniestro"));
                siniestros.setNumero_cheuqe_siniestro(rs.getString("Numero_Cheque_siniestro"));
                siniestros.setValor_cheque_siniestro(rs.getDouble("Valor_Cheque_siniestro"));
                siniestros.setFecha_consignacion_siniestro(rs.getString("Fecha_Consignacion_siniestro"));
                 siniestros.setDias_recuperacion(rs.getString("Dias_recuperacion"));
                lista.add( siniestros );
            }

        }catch(Exception e){
            throw new Exception( "listarsiniestros " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }



/**
     * M�todo que consulta si una cedula esta reportada y trae el motivo del reporte
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String  cedulaReportada( String cedula) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_CEDULA_REPORTADA";
        String reporte="";

        try{

            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, cedula);
            rs=st.executeQuery();
            while(rs.next()){
               reporte=rs.getString("descripcion");
            }

        }catch(Exception e){
            throw new Exception( "cedulaReportada " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return reporte;

    }

    /**
     * M�todo que consulta si una cuenta esta reportada y trae el motivo del reporte
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String  cuentaReportada( String cuenta, String banco) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_CUENTA_REPORTADA";
        String reporte="";

        try{

            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setInt(1, Integer.parseInt(banco));
            st.setLong(2,  Long.parseLong(cuenta));
            st.setInt(3,  Integer.parseInt(banco));
            st.setLong(4,  Long.parseLong(cuenta));
            rs=st.executeQuery();
            while(rs.next()){
               reporte=rs.getString("descripcion");
            }

        }catch(Exception e){
            throw new Exception( "cuentaReportada " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return reporte;

    }




}