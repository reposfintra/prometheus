/***********************************************************************************
 * Nombre clase : ............... CXPDocDao.java                                   *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 8 de octubre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import javax.swing.*;
import java.text.*;
import java.util.Date;
//logger
import org.apache.log4j.Logger;


public class RXPDocDAO extends MainDAO {
    
    Logger log = Logger.getLogger(this.getClass());
    
    private String batch = "";
    
    public RXPDocDAO() {
        super("RXPDocDAO.xml");
    }
    
    private Vector vecCxp_doc;
    
    private CXP_Doc factura = new CXP_Doc();
    
    private List facturas;
    
    private Vector vecRxpItemsDoc;
    
    private TreeMap treemap;
    
    
    
    /**
     * Metodo vectorFacturas. Retorna el vector con las facturas que va autorizar el usuario en session.
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @version : 1.0
     */
    public Vector vectorFacturas(){
        return vecCxp_doc;
    }
    
    
    public List getFacturas(){
        return this.facturas;
    }    

   
    /**
     * Metodo que retorna un boolean para verificar la existencia de un Documento por pagar en el sistema
     * retorna true si existe y false si no existe.
     * @autor.......David Lamadrid
     * @param.......String dis(Distrito),String proveedor,String tipoDoc,String docuemnto.
     * @see.........CXP_Doc.class
     * @throws.......
     * @version.....1.0.
     * @return......boolean.
     */
    public boolean existeDoc(String dis,String proveedor,String tipoDoc,String documento ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        
        try {
            st = crearPreparedStatement("SQL_EXISTEDOC");
            st.setString(1, dis);
            st.setString(2, proveedor);
            st.setString(3,tipoDoc);
            st.setString(4, documento);
            rs = st.executeQuery();
            
            while (rs.next()) {
                sw=true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage()+"" + e.getErrorCode());
        }
        finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_EXISTEDOC");
        }
        return sw;
    }
    
    /**
     * Metodo que retorna un Objeto tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......ninguno
     * @see.........CXP_Doc.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return......CXPDoc.
     */
    public com.tsp.operation.model.beans.CXP_Doc getFactura(){
        return factura;
    }
    
    /**
     * Metodo que instancia un Objeto tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......CXPDoc factura(objeto de CXPDoc)
     * @see.........CXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return.......
     */
    public void setFactura(com.tsp.operation.model.beans.CXP_Doc factura){
        this.factura = factura;
    }
    
    /**
     * Metodo que retorna un Vector de  Objetos tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......ninguno.
     * @see.........CXPDocDAO.class
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return.......Vector vCXPDoc.
     */
    public java.util.Vector getVecCxp_doc(){
        return vecCxp_doc;
    }
    
    /**
     * Metodo que setea un Vector de  Objetos tipo CXDOC en CXPDocDAO
     * @autor.......David Lamadrid
     * @param.......Vector vecCxp_doc(Vector de objetos de CXPDoc)
     * @see..........
     * @throws......Clase no encontrada.
     * @version.....1.0.
     * @return......Vector de Objetos CXPDoc
     */
    public void setVecCxp_doc(java.util.Vector vecCxp_doc){
        this.vecCxp_doc = vecCxp_doc;
    }
    
    
    /**
     * Metodo que retorna un String con fecha generada de una fecha inicial y un numero de dias
     * @autor.......David Lamadrid
     * @param.......ninguno
     * @see.........Util.class
     * @throws.......
     * @version.....1.0.
     * @return......String fechaFinal.
     */
    public  String fechaFinal(String fechai,int n){
        String timeStamp = fechai;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar c = Util.crearCalendar(timeStamp);
        c.add(c.DATE, n);
        java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(c.getTime().getTime());
        String fechaf=""+sqlTimestamp;
        return fechaf;
    }
    
    /**
     * Metodo: getCXP_doc, permite retornar un objeto de registros de CXP_doc.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public CXP_Doc getCXP_doc() {
        return factura;
    }
    
    /**
     * Metodo: getCXP_doc, permite obtener un objeto de registros de CXP_doc.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setCXP_doc(CXP_Doc factura) {
        this.factura = factura;
    }
    
    /**
     * Setter for property facturas.
     * @param facturas New value of property facturas.
     */
    public void setFacturas(java.util.Vector facturas) {
        this.facturas = facturas;
    }
    
    /**
     * Getter for property vFacturas.
     * @return Value of property vFacturas.
     */
    public java.util.Vector getVFacturas() {
        return vFacturas;
    }
    
    /**
     * Setter for property vFacturas.
     * @param vFacturas New value of property vFacturas.
     */
    public void setVFacturas(java.util.Vector vFacturas) {
        this.vFacturas = vFacturas;
    }
    
    private Vector vFacturas;
    
    /**
     * Metodo: obtenerNumeroFXP , obtiene un Vector con los Numero de facturas no canceladas a un proveedor
     * @autor : Ing. David Lamadrid
     * @param : String proveedor.
     * @version : 1.0
     */
    public void obtenerNumeroFXP(String proveedor)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            st = crearPreparedStatement("SQL_NUMERO_F_POR_PROVEEDOR");
            st.setString(1,proveedor);
            rs= st.executeQuery();
            this.vFacturas= new Vector();
            while(rs.next()){
                vFacturas.add(rs.getString("documento"));
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE lOS CAMPOS DE LAS PLANILLAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            desconectar("SQL_NUMERO_F_POR_PROVEEDOR");
        }
    }
   
    
    
    /**
     * Getter for property vecCxpItemsDoc.
     * @return Value of property vecCxpItemsDoc.
     */
    public java.util.Vector getVecRxpItemsDoc() {
        return vecRxpItemsDoc;
    }
    
    /**
     * Setter for property vecCxpItemsDoc.
     * @param vecCxpItemsDoc New value of property vecCxpItemsDoc.
     */
    public void setVecRxpItemsDoc(java.util.Vector vecRxpItemsDoc) {
        this.vecRxpItemsDoc = vecRxpItemsDoc;
    }
    
    /********************************************************************
     *                  ANULACION DE FACTURAS RECURRENTES               *
     ********************************************************************/
    
    /**
     * Metodo:          ExisteCXP_Doc
     * Descriocion :    Funcion publica que nos dice si existe un documento en cuentas por pagar
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         boo ( retorna un boolean )
     * @version :       1.0
     */
    public boolean ExisteCXP_Doc( String distrito, String proveedor, String tipo_documento, String documento ) throws SQLException{
        
        CXP_Doc cXP_Doc = new CXP_Doc();
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        boolean boo = false;
        
        try{
            
            st = crearPreparedStatement( "SQL_ConsultarCXP_Doc" );
            
            st.setString( 1, distrito );
            st.setString( 2, proveedor );
            st.setString( 3, tipo_documento );
            st.setString( 4, documento );
            
            rs = st.executeQuery();
            
            if ( rs.next() )
                boo = true;
            
        } catch( SQLException e ){
            
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LA EXISTENCIA DEL DOCUMENTO DE CUENTAS POR PAGAR " + e.getMessage() + " " + e.getErrorCode() );
            
        } finally{
            
            if( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_ConsultarCXP_Doc" );
            
        }
        
        return boo;
        
    }
    
    
    /**
     * Metodo:          AnularCXP_Doc
     * Descriocion :    Funcion publica que nos coloca un documento de cuentas por pagar en estado 'ANULADO'
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         cXP_Doc ( documento de cuentas por pagar )
     * @return:         nada.
     * @version :       1.0
     */
    public void AnularCXP_Doc( CXP_Doc cXP_Doc ) throws SQLException{
        
        PreparedStatement st = null;
        
        try{
            
            st = this.crearPreparedStatement( "SQL_AnularCXP_Doc" );
            
            st.setString( 1, cXP_Doc.getUser_update() );
            st.setString( 2, cXP_Doc.getUser_update() );
            st.setString( 3, cXP_Doc.getDstrct() );
            st.setString( 4, cXP_Doc.getProveedor() );
            st.setString( 5, cXP_Doc.getTipo_documento() );
            st.setString( 6, cXP_Doc.getDocumento() );
            
            st.executeUpdate();
            
        } catch( SQLException e ){
            
            throw new SQLException( "ERROR DURANTE LA ANULACION DEL DOCUMENTO " + e.getMessage() + "" + e.getErrorCode() );
            
        } finally{
            
            if( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_AnularCXP_Doc" );
            
        }
        
    }
    
    /**
     * Metodo:          SQL_Nombre_Proveedor
     * Descriocion :    Funcion publica que obtiene el nombre de un proveedor
     * @autor :         LREALES
     * @param :         distrito y proveedor
     * @return:         nombre ( retorna un String )
     * @version :       1.0
     */
    public String SQL_Nombre_Proveedor( String distrito, String proveedor ) throws SQLException{
        
        CXP_Doc cXP_Doc = new CXP_Doc();
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String nombre = "";
        
        try{
            
            st = crearPreparedStatement( "SQL_Nombre_Proveedor" );
            
            st.setString( 1, distrito );
            st.setString( 2, proveedor );
            
            rs = st.executeQuery();
            
            if ( rs.next() )
                nombre = rs.getString("payment_name");
            
        } catch( SQLException e ){
            
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DEL NOMBRE DEL PROVEEDOR " + e.getMessage() + " " + e.getErrorCode() );
            
        } finally{
            
            if( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_Nombre_Proveedor" );
            
        }
        
        return nombre;
        
    }
  
  
  
       
    
    /**
     * Metodo: calcularNetoItem, calcula el valor neto del item
     *         aplicando los respectivos impuestos
     * @autor : Osvaldo P�rez Ferrer
     * @param : item, item al que se calculara su valor neto
     * @return : vector con el neto moneda local y neto moneda extranjera
     * @version : 1.0
     */
    public double[] calcularNetoItem(CXPItemDoc item) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        double[] neto = new double[2];
        try{
            st = crearPreparedStatement("SQL_IMPUESTOS_X_ITEM");
            st.setString( 1, item.getDstrct());
            st.setString( 2, item.getProveedor());
            st.setString( 3, item.getTipo_documento());
            st.setString( 4, item.getDocumento());
            st.setString( 5, item.getItem());
            sql = st.toString();
            sql = sql.replaceAll("#TABLE#", "cxp_imp_item");
            
            rs = st.executeQuery(sql);
            
            neto[0] = item.getVlr();
            neto[1] = item.getVlr_me();
            while(rs.next()){
                neto[0] += rs.getDouble("vlr_total_impuesto");
                neto[1] += rs.getDouble("vlr_total_impuesto_me");
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR en calcularNetoItem " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            this.desconectar("SQL_IMPUESTOS_X_ITEM");
        }
        return neto;
    }
    
  
    public boolean existe( CXP_Doc r, String tabla, String consecutivo ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        try{
            st = this.crearPreparedStatement("SQL_EXISTE");
            
            String cons = consecutivo.equals("")? "" : "_"+consecutivo;
            
            st.setString(1, r.getDstrct());
            st.setString(2, r.getProveedor());
            st.setString(3, r.getTipo_documento());
            st.setString(4, r.getDocumento()+cons );
            
            String sql = st.toString().replaceAll( "#TABLE#", tabla );
            
            rs = st.executeQuery(sql);
            
            if( rs.next() ){
                existe = true;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR en RXPDocDAO.existe " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            this.desconectar("SQL_EXISTE");
        }
        return existe;
    }    
   
    
     /**
     * Metodo buscarTotalIMP , Metodo para buscar el total del impuesto de una factura
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public double buscarTotalIMP(String Dstrct, String Proveedor, String Tipo_documento,String Documento,String agencia,String FechaFac,String TipoIMP) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        double imp = 0;
        try{
            st = this.crearPreparedStatement("SQL_BUSCAR_TOTAL_IMP");
            st.setString(1, Dstrct);
            st.setString(2, Proveedor);
            st.setString(3, Tipo_documento);
            st.setString(4, Documento);
            st.setString(5, agencia);
            st.setString(6, FechaFac);
            st.setString(7, TipoIMP);
            rs = st.executeQuery();
            while (rs.next()) {
                 imp = rs.getDouble("imp");
            }
            
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_TOTAL_IMP" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_BUSCAR_TOTAL_IMP");
        }
        return imp;
    }
    
    
    /**
     * Muestra el detalle de la factura
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void detalleItemFactura(CXP_Doc factura, Vector vTipImpuestos)throws SQLException{
                
        PreparedStatement st = null;
        ResultSet rs = null;
        PreparedStatement st3 = null;
        ResultSet rs3 = null;
        Connection con = null;
        String planilla ="";
        boolean sw  = true;
        
        vecCxp_doc = new Vector();
        
        try{
            st = crearPreparedStatement("SQL_DETALLE_ITEM_FACTURA");            
            st3 = crearPreparedStatement("SQL_IMP_X_ITEM");
            
            st.setString(1, factura.getDstrct());
            st.setString(2, factura.getProveedor());
            st.setString(3, factura.getTipo_documento());
            st.setString(4, factura.getDocumento());
           // logger.info("SQL: " + st.toString());
            rs = st.executeQuery();
            
            String  AGE  = factura.getAgencia();
            
            while (rs.next()) {
                
                CXPItemDoc item = new CXPItemDoc();
                item.setConcepto(rs.getString("concepto"));
                item.setDescripcion(rs.getString("descripcion"));
                item.setCodigo_cuenta(rs.getString("codigo_cuenta"));                
                item.setCodigo_abc(rs.getString("codigo_abc"));
                item.setPlanilla(rs.getString("planilla"));
                item.setVlr_me(rs.getDouble("vlr_me"));
                item.setVlr_total(rs.getDouble("vlr_me"));
                item.setTipcliarea(rs.getString("tipcliarea"));
                item.setCodcliarea(rs.getString("codcliarea"));
                item.setItem(rs.getString("item"));
                item.setCodigo_cuenta(rs.getString("codigo_cuenta"));
                item.setAuxiliar(rs.getString("auxiliar"));
                
                              
                //item.setDescliarea("");
                /* IMPUESTOS */
                double VlrTotal = 0;
                String iva = "";
                Vector vImpuestosPorItem= new Vector();
                for(int x=0;x<vTipImpuestos.size();x++){
                    String imp = (String) vTipImpuestos.elementAt(x);
                    CXPImpItem impuestoItem = new CXPImpItem();
                    impuestoItem.setTipo_impuesto(imp);
                    st3.clearParameters();
                    
                    String AGE_IMP = (imp.equals("RFTE") ||  imp.equals("IVA") ||  imp.equals("RIVA"))?"":AGE; 
                    
                    st3.setString(1,factura.getDstrct());
                    st3.setString(2,factura.getProveedor());
                   // st3.setString(3,factura.getTipo_documento());
                    st3.setString(3,factura.getDocumento());
                    st3.setString(4,item.getItem());
                    st3.setString(5, AGE_IMP  );
                    st3.setString(6, imp);
                    //System.out.println("SQL IMPUESTOS " + st3.toString()); 
                    rs3 = st3.executeQuery();
                    if(rs3.next()){
                        impuestoItem.setCod_impuesto(rs3.getString("cod_impuesto"));
                        impuestoItem.setVlr_total_impuesto(rs3.getDouble("vlr_total_impuesto_me"));
                        vImpuestosPorItem.add(impuestoItem);
                        
                        if(imp.equals("IVA") && !factura.isOP() ){
                            VlrTotal += rs3.getDouble("vlr_total_impuesto_me");
                            iva      =  rs3.getString("cod_impuesto");
                        }
                        
                    }else{
                        impuestoItem.setCod_impuesto("");
                        impuestoItem.setVlr_total_impuesto(0);
                        vImpuestosPorItem.add(impuestoItem);
                    }
                }                
                
                item.setVlr(item.getVlr_me() + VlrTotal);
                item.setIva(iva);
                item.setVItems( vImpuestosPorItem);
                item.setVCopia(vImpuestosPorItem);
                /***************/
                vecCxp_doc.add(item);
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA FACTURA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null)st.close();
            if (rs != null)rs.close();
            desconectar("SQL_DETALLE_ITEM_FACTURA");
            desconectar("SQL_IMP_X_ITEM");
        }
    }
    
      /**
     * Obtiene el consecutivo
     * @autor   Ing. Andres Maturana
     * @throws  SQLException
     * @version 1.0
     */
    public String obtenerConsecutivo() throws SQLException{
        PreparedStatement st = null;
        PreparedStatement st0 = null;
        ResultSet rs = null;
        Connection con = null;
        
        String  serie = "";
        
        try{
            con = this.conectar("SQL_CONSULTA_SERIE");
            st = con.prepareStatement(this.obtenerSQL("SQL_CONSULTA_SERIE"));
            rs = st.executeQuery();
            
            while (rs.next()) {
                 String prefix = rs.getString("prefix");
                 String consec = "" + rs.getInt("last_number");
                 int len = consec.length();
                 
                 for( int i = 0; i<(5 - len); i++){
                     consec = "0" + consec;
                 }
                 
                 serie = prefix + consec;
                 
                 st0 = con.prepareStatement(this.obtenerSQL("SQL_UPDATE_SERIE"));
                 st0.executeUpdate();
            }
            
            
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerConsecutivo() " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_CONSULTA_SERIE");
        }
        
        return serie;
    }
    
    //MODIFICACION OPEREZ 21 ENERO DE 2006
       /**
     * Metodo: transferImpuestosDoc, metodo que obtiene los impuestos aplicados
     *         a una factura, para agregarlos em cxp_imp_doc
     * @autor : Osvaldo P�rez Ferrer
     * @param : factura, factura a la cual se le agregaran sus impuestos
     * @version : 1.0
     */
    public void transferImpuestosDoc(CXP_Doc factura, String documento) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            st = this.crearPreparedStatement("SQL_IMP_X_FACTURA_RXP");
            st.setString( 1, factura.getDstrct() );
            st.setString( 2, factura.getProveedor() );
            st.setString( 3, factura.getTipo_documento() );
            st.setString( 4, factura.getDocumento() );
            rs = st.executeQuery();
            
            ////System.out.println(st.toString());
            
            int cuotas = factura.getNum_cuotas();
            int transfer = factura.getNum_cuotas_transfer();
            
            double vlr = 0;
            double vlr_me = 0;
            
            double imp_doc = 0;
            double imp_doc_me = 0;
            
            CXPImpDoc imp;
            
            while(rs.next()){
                imp = new CXPImpDoc();
                imp.setDstrct( factura.getDstrct());
                imp.setProveedor( factura.getProveedor());
                imp.setTipo_documento( factura.getTipo_documento());
                imp.setDocumento( documento );
                imp.setCod_impuesto( rs.getString( "cod_impuesto" ));
                imp.setPorcent_impuesto(rs.getDouble( "porcent_impuesto" ) );
                
                vlr = rs.getDouble("vlr_total_impuesto");
                vlr_me = rs.getDouble("vlr_total_impuesto_me");
                
                imp_doc = (transfer+1 < cuotas)? Math.round( (vlr)/cuotas ) :
                    vlr - ( ( Math.round( vlr/cuotas ) ) * (cuotas - 1));
                    
                    imp_doc_me = (transfer+1 < cuotas)? Math.round( (vlr_me)/cuotas ) :
                        vlr_me - ( ( Math.round( vlr_me/cuotas ) ) * (cuotas - 1));
                        
                        imp.setVlr_total_impuesto( imp_doc);
                        imp.setVlr_total_impuesto_me( imp_doc_me);                                                
                        imp.setCreation_user( factura.getCreation_user() );
                        imp.setUser_update( "" );
                        imp.setBase( factura.getBase() );
                        
                        this.insertarImpuestoDoc(imp);
            }
        }catch(SQLException e){
            throw new SQLException("ERROR en transferImpuestosDoc " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            this.desconectar("SQL_IMP_X_FACTURA_RXP");
        }
    }
    
    /**
     * Metodo: insertarImpuestoDoc, inserta el impuesto de documento dado en cxp_imp_doc
     * @autor : Osvaldo P�rez Ferrer
     * @param : impDoc, impuesto de documento a ingresar
     * @version : 1.0
     */
    public void insertarImpuestoDoc(CXPImpDoc impDoc) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            st = this.crearPreparedStatement("SQL_IMPDOC_INTO_CXP");
            
            st.setString(1, impDoc.getDstrct());
            st.setString(2, impDoc.getProveedor());
            st.setString(3, impDoc.getTipo_documento());
            st.setString(4, impDoc.getDocumento());
            st.setString(5, impDoc.getCod_impuesto());
            st.setDouble(6, impDoc.getPorcent_impuesto());
            st.setDouble(7, impDoc.getVlr_total_impuesto());
            st.setDouble(8, impDoc.getVlr_total_impuesto_me());
            st.setString(9, impDoc.getCreation_user());
            st.setString(10, impDoc.getUser_update());
            st.setString(11, impDoc.getBase());
            
            this.batch += st.toString();
            
            //st.executeUpdate();
        }catch(SQLException e){
            throw new SQLException("ERROR en insertarImpuestoDoc " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            this.desconectar("SQL_IMPDOC_INTO_CXP");
        }
    }
    
    
    /**
     * Metodo: actualizarRXP_Doc, actualiza los datos de control de transferencia
     *         de la factura dada.
     * @param : r, factura a actualizar
     * @param : ml, valor transferido de moneda local para acumular
     * @param : me, valor transferido de moneda extranjera para acumular
     * @autor : Osvaldo P�rez Ferrer
     * @version : 1.0
     */
    public void actualizarRXP_Doc(CXP_Doc r, double ml, double me) throws SQLException{
        PreparedStatement st = null;
        
        try{
            st = this.crearPreparedStatement("SQL_ACTUALIZAR_RECURRENTE");
            st.setDouble( 1, ml );
            st.setDouble( 2, me );
            st.setString( 3, r.getDstrct());
            st.setString( 4, r.getProveedor());
            st.setString( 5, r.getTipo_documento());
            st.setString( 6, r.getDocumento());
            
            //st.executeUpdate();
            this.batch += st.toString();
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE ACTUALIZAR RECURRENTE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            this.desconectar("SQL_ACTUALIZAR_RECURRENTE");
        }
    }
    
    
    /**
     * Metodo: trasnferirItems, transfiere los items de una factura de rxp a cxp
     *         dividido entre el numero de cuotas.
     * @autor : Osvaldo P�rez Ferrer
     * @param : factura, factura de la cual se van a transferir los items
     * @param : documento, n�mero de documento que tendr�n los items
     * @version : 1.0
     */
    public double[] transferirItems(CXP_Doc factura, String documento) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        double[] neto = new double[2];
        neto[0] = 0;
        neto[1] = 0;
        try {
            st = crearPreparedStatement("SQL_ITEM_FACTURA");
            st.setString(1,factura.getDstrct());
            st.setString(2,factura.getProveedor());
            st.setString(3,factura.getDocumento());
            rs = st.executeQuery();
            double vlr = 0;
            double vlr_me = 0;
            
            int cuotas = factura.getNum_cuotas();
            int transfer = factura.getNum_cuotas_transfer();
            
            while(rs.next()){
                CXPItemDoc item = new CXPItemDoc();
                item.setDstrct( rs.getString("dstrct"));
                item.setProveedor( rs.getString("proveedor"));
                item.setTipo_documento( rs.getString("tipo_documento"));
                item.setDocumento(documento);
                item.setItem( rs.getString("item"));
                item.setDescripcion( rs.getString("descripcion"));
                
                double neto_item = (transfer+1 < cuotas)?  Math.round( (rs.getDouble("vlr"))/cuotas ):
                    rs.getDouble("vlr")-( ( Math.round(rs.getDouble("vlr")/cuotas) ) * (cuotas - 1));
                    
                    item.setVlr( neto_item);
                    
                    double neto_me_item = (transfer+1 < cuotas)?  Math.round( (rs.getDouble("vlr_me"))/cuotas ):
                        rs.getDouble("vlr_me")- ( Math.round( (rs.getDouble("vlr_me")/cuotas) ) * (cuotas - 1));
                        item.setVlr_me( neto_me_item );
                        
                        
                        item.setCodigo_cuenta( rs.getString("codigo_cuenta"));
                        item.setCodigo_abc( rs.getString("codigo_abc"));
                        item.setPlanilla( rs.getString("planilla"));
                        item.setUser_update("");
                        item.setCreation_user( factura.getCreation_user());
                        item.setBase( factura.getBase());
                        item.setCodcliarea( rs.getString("codcliarea"));
                        item.setTipcliarea( rs.getString("tipcliarea"));
                        item.setConcepto( rs.getString( "concepto" ));
                        item.setAuxiliar( rs.getString( "auxiliar" ));
                        
                        this.insertarItem(item, "cxp");
                        neto = this.transferirImpuestos(item, factura, documento);
                        //neto = this.calcularNetoItem(item);
                        vlr += neto[0] + neto_item;
                        vlr_me += neto[1] + neto_me_item;
                        
            }
            neto[0] = vlr;
            neto[1] = vlr_me;
        }catch(SQLException e){
            throw new SQLException("ERROR en trasnferirItems " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            
            this.desconectar("SQL_ITEM_FACTURA");
        }
        return neto;
    }
    
    /**
     * Metodo: trasnferirImpuestos, transfiere los impuestos pertenecientes al
     *         item dado, de rxp a cxp, divididos entre el n�mero de cuotas
     * @autor : Osvaldo P�rez Ferrer
     * @param : item, item del cual se transferir�n los impuestos
     * @param : factura, factura de la cual se van a transferir los items
     * @param : documento, n�mero de documento que tendr�n los items
     * @version : 1.0
     */
    public double[] transferirImpuestos(CXPItemDoc item, CXP_Doc factura, String documento) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        double neto[] =  {0,0};
        
        try{
            st = crearPreparedStatement("SQL_IMPUESTOS_X_ITEM");
            st.setString( 1, item.getDstrct());
            st.setString( 2, item.getProveedor());
            st.setString( 3, item.getTipo_documento());
            st.setString( 4, factura.getDocumento());
            st.setString( 5, item.getItem());
            sql = st.toString();
            sql = sql.replaceAll("#TABLE#", "rxp_imp_item");
            
            int cuotas = factura.getNum_cuotas();
            int transfer = factura.getNum_cuotas_transfer();
            
            double vlr = 0;
            double vlr_me = 0;
            
            double imp = 0;
            double imp_me = 0;
            
            rs = st.executeQuery(sql);
            while(rs.next()){
                CXPImpItem i = new CXPImpItem();
                i.setDstrct( rs.getString("dstrct"));
                i.setProveedor( rs.getString("proveedor"));
                i.setTipo_documento( rs.getString("tipo_documento"));
                i.setDocumento(documento);
                i.setItem( rs.getString("item"));
                i.setCod_impuesto( rs.getString("cod_impuesto"));
                i.setPorcent_impuesto(rs.getDouble("porcent_impuesto"));
                
                vlr = rs.getDouble("vlr_total_impuesto");
                vlr_me = rs.getDouble("vlr_total_impuesto_me");
                
                imp = (transfer+1 < cuotas)? Math.round( (vlr)/cuotas ) :
                    vlr - ( ( Math.round( vlr/cuotas ) ) * (cuotas - 1));
                    imp_me = (transfer+1 < cuotas)? Math.round( (vlr_me)/cuotas ) :
                        vlr_me - ( ( Math.round( vlr_me/cuotas ) ) * (cuotas - 1));
                        
                        i.setVlr_total_impuesto( imp);
                        i.setVlr_total_impuesto_me( imp_me);
                        i.setUser_update("");
                        i.setCreation_user( factura.getCreation_user());
                        i.setBase(factura.getBase());
                        neto[0] += imp;
                        neto[1] += imp_me;
                        this.insertarImpuesto(i, "cxp");
                        
            }
        }catch(SQLException e){
            throw new SQLException("ERROR en transferirImpuestos " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            
            this.desconectar("SQL_IMPUESTOS_X_ITEM");
        }
        return neto;
    }
    
    /**
     * Metodo: insertarItem, inserta el item dado en cxp o rxp
     * @autor : Osvaldo P�rez Ferrer
     * @param : item, item a insertar
     * @param : tabla, "cxp" o "rxp"
     * @version : 1.0
     */
    public void insertarItem(CXPItemDoc item, String tabla) throws SQLException{
        PreparedStatement st = null;
        String tbl;
        String sql = "";
        if(tabla.equals("cxp")){
            tbl = "cxp_items_doc";
        }
        else{
            tbl = "rxp_items_doc";
        }
        try {
            st = crearPreparedStatement("SQL_INSERTAR_ITEM_DOCUMENTO");
            
            st.setString(1, item.getDstrct());
            st.setString(2, item.getProveedor());
            st.setString(3, item.getTipo_documento());
            st.setString(4, item.getDocumento());
            st.setString(5, item.getItem());
            st.setString(6, item.getDescripcion());
            st.setDouble(7, item.getVlr());
            st.setDouble(8, item.getVlr_me());
            st.setString(9, item.getCodigo_cuenta());
            st.setString(10, item.getCodigo_abc());
            st.setString(11, item.getPlanilla());
            st.setString(12, "");
            st.setString(13, item.getCreation_user());
            st.setString(14, item.getBase());
            st.setString(15, item.getCodcliarea());
            st.setString(16, item.getTipcliarea());
            st.setString(17, item.getConcepto());
            st.setString(18, item.getAuxiliar());
            
            sql = st.toString();
            sql = sql.replaceAll("#TABLE#" , tbl );
            
            //st.executeUpdate(sql);
            
            this.batch += sql;
            
            ////System.out.println("insertarItem --> \n "+sql);
        }catch(SQLException e){
            throw new SQLException("ERROR en insertarItem " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            
            this.desconectar("SQL_INSERTAR_ITEM_DOCUMENTO");
        }
    }
    
    /**
     * Metodo: insertarImpuesto, inserta el impuesto dado en cxp o rxp
     * @autor : Osvaldo P�rez Ferrer
     * @param : i, impuesto a insertar
     * @param : tabla, "cxp" o "rxp"
     * @version : 1.0
     */
    public void insertarImpuesto(CXPImpItem i, String tabla) throws SQLException{
        PreparedStatement st = null;
        String tbl;
        String sql = "";
        if(tabla.equals("cxp")){
            tbl = "cxp_imp_item";
        }
        else{
            tbl = "rxp_imp_item";
        }
        try {
            st = crearPreparedStatement("SQL_INSERTAR_IMP_X_ITEM");
            
            st.setString( 1, i.getDstrct());
            st.setString( 2, i.getProveedor());
            st.setString( 3, i.getTipo_documento());
            st.setString( 4, i.getDocumento());
            st.setString( 5, i.getItem());
            st.setString( 6, i.getCod_impuesto());
            st.setDouble( 7, i.getPorcent_impuesto());
            st.setDouble( 8, i.getVlr_total_impuesto());
            st.setDouble( 9, i.getVlr_total_impuesto_me());
            st.setString( 10,  "" );
            st.setString( 11, i.getCreation_user());
            st.setString( 12, i.getBase());
            
            sql = st.toString();
            sql = sql.replaceAll("#TABLE#", tbl);
            
            this.batch += sql;
            
            //st.executeUpdate(sql);
        }catch(SQLException e){
            throw new SQLException("ERROR en insertarImpuesto " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            
            this.desconectar("SQL_INSERTAR_IMP_X_ITEM");
        }
    }
    
    
    
     /*****************************************
     /***************************************************
     * metodo aplazarRecurrente, actualiza la fecha de la
     *        ultima transferencia para aplazar la generacion 
     *        de la proxima factura(cuota)
     * @author: Ing: Osvaldo P�rez Ferrer
     * @throws: en casi de un error de BD
     ***************************************************/
    public void aplazarRecurrente ( CXP_Doc r ) throws Exception{
        
        PreparedStatement st = null;                
        
        try{
            st = this.crearPreparedStatement("SQL_APLAZAR_RECURRENTE");
                                   
            st.setString(1, r.getFecha_ultima_transfer());
            st.setString(2, r.getUser_update());
            st.setString(3, r.getDstrct() );
            st.setString(4, r.getProveedor() );
            st.setString(5, r.getDocumento() );
            st.setString(6, r.getTipo_documento() );
          
            st.executeUpdate();
            
        }catch(SQLException e){
            throw new SQLException("ERROR en RXPDocDAO.aplazarRecurrente " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            this.desconectar("SQL_APLAZAR_RECURRENTE");
        }
        
    }
    
    /**
     * Metodo: transferirDoc, transfiere las facturas recurrentes a facturas (de rxp a cxp)
     *         seg�n la frecuencia con que se deba transferir  la factura
     * @param : r, factura a transferir
     * @param : usuario, usuario que inicia la transferencia
     * @param : base, base del usuario que inicia la transferencia
     * @autor : Osvaldo P�rez Ferrer
     * @version : 1.0
     */
    public String[] transferirDoc(CXP_Doc r, String usuario, String base) throws SQLException{
        PreparedStatement st = null;
        PreparedStatement st2 = null;
        double[] neto = new double[2];
        Statement sttmt = null;
        String[] log = new String[2];
        String success = "";
        int transfer = 0;

        this.batch = "";

        try{
            int cuotas = r.getNum_cuotas();
            transfer   = r.getNum_cuotas_transfer();
            if(! this.existe(r, "cxp_doc", ""+(transfer+1) ) ){
                if(! this.existe(r, "cxp_items_doc", ""+(transfer+1) ) ){
                    if(! this.existe(r, "cxp_imp_item", ""+(transfer+1) ) ){
                        if(! this.existe(r, "cxp_imp_doc", ""+(transfer+1) ) ){

                            Connection con = conectar("SQL_TRANSFERIR_DOCUMENTO");
                            con.setAutoCommit(false);
                            sttmt = con.createStatement();
                            //st = this.crearPreparedStatement("SQL_TRANSFERIR_DOCUMENTO");
                            st = con.prepareStatement( this.obtenerSQL("SQL_TRANSFERIR_DOCUMENTO"));

                            String documento= r.getDocumento()+"-"+(transfer+1);

                            st.setString( 1, r.getDstrct());
                            st.setString( 2, r.getProveedor());
                            st.setString( 3, r.getTipo_documento());
                            st.setString( 4, documento);
                            st.setString( 5, r.getDescripcion());
                            st.setString( 6, r.getAgencia());
                            st.setString( 7, r.getHandle_code());
                            st.setString( 8, r.getId_mims());
                            st.setString( 9, r.getTipo_documento_rel());
                            st.setString( 10, r.getDocumento_relacionado());

                            if( r.getVlr_neto_me() < 0 ){//Si es negativa se debe aprobar automaticamente
                                st.setString( 11, "now()");
                                st.setString( 12, r.getAprobador());
                                st.setString( 13, r.getAprobador());
                            }else{
                                st.setString( 11, r.getFecha_aprobacion());
                                st.setString( 12, r.getAprobador());
                                st.setString( 13, r.getUsuario_aprobacion());
                            }
                            st.setString( 14, r.getBanco());
                            st.setString( 15, r.getSucursal());
                            st.setString( 16, r.getMoneda());
                            st.setDouble( 18, r.getVlr_total_abonos());
                            st.setDouble( 21, r.getVlr_total_abonos_me());
                            st.setDouble( 23, r.getTasa());
                            st.setString( 24,  r.getUsuario_contabilizo());
                            st.setString( 25, r.getFecha_contabilizacion());
                            st.setString( 26, r.getUsuario_anulo());
                            st.setString( 27,  r.getFecha_anulacion());
                            st.setString( 28, r.getFecha_contabilizacion_anulacion());
                            st.setString( 29, r.getObservacion());
                            st.setDouble( 30, r.getNum_obs_autorizador());
                            st.setDouble( 31,r.getNum_obs_pagador());
                            st.setDouble( 32, r.getNum_obs_registra());
                            st.setString( 33, "");
                            st.setString( 34, usuario);
                            st.setString( 35, base);
                            st.setString( 36,  r.getCorrida());
                            st.setString( 37, r.getCheque());
                            st.setString( 38, r.getPeriodo());
                            st.setString( 39, r.getFecha_contabilizacion_ajc());
                            st.setString( 40, r.getFecha_contabilizacion_ajv());
                            st.setString( 41, r.getPeriodo_ajc());
                            st.setString( 42, r.getPeriodo_ajv());
                            st.setString( 43, r.getUsuario_contabilizo_ajc());
                            st.setString( 44, r.getUsuario_contabilizo_ajv());
                            st.setInt( 45, r.getTransaccion_ajc());
                            st.setInt( 46, r.getTransaccion_ajv());
                            st.setString( 47, r.getFecha_procesado());
                            st.setString( 48, r.getClase_documento());
                            st.setInt( 49, r.getTransaccion());
                            st.setString( 50, r.getMoneda_banco());
                            st.setString( 51, Util.getFechaActual_String(4) );//hoy
                            st.setString( 52, r.getFecha_vencimiento());
                            st.setString( 53, r.getUltima_fecha_pago());

                            r.setBase(base);
                            r.setCreation_user(usuario);

                            neto = this.transferirItems(r, documento );

                            st.setDouble( 17, neto[0]);
                            st.setDouble( 19, neto[0]);
                            st.setDouble( 20, neto[1]);
                            st.setDouble( 22, neto[1]);

                            //st.executeUpdate();
                            ////System.out.println("transferirDoc --> \n"+st.toString());
                            ////System.out.println("EL BATCH --> \n"+this.batch);

                            String cabecera = st.toString();
                            this.transferImpuestosDoc(r, documento);
                            this.actualizarRXP_Doc(r, neto[0], neto[1]);

                            sttmt.addBatch( cabecera+this.batch );
                            sttmt.executeBatch();
                            ////System.out.println("BATCH \n"+cabecera+this.batch);
                            con.commit();

                            log[0] = "Factura transferida: dstrct = "+r.getDstrct()+
                            " proveedor = "+r.getProveedor()+" tipo_documento = "+r.getTipo_documento()+
                            " documento = "+r.getDocumento()+", cuota Nro. "+(transfer+1);;
                            success = "ok";

                        }else{
                            log[0] = "No se pudo transferir factura dstrct = "+r.getDstrct()+
                            " proveedor = "+r.getProveedor()+" tipo_documento = "+r.getTipo_documento()+
                            " documento = "+r.getDocumento()+", YA EXISTE UN REGISTRO EN cxp_imp_doc"+
                            " para la cuota Nro. "+(transfer+1);
                        }
                    }else{
                        log[0] = "No se pudo transferir factura dstrct = "+r.getDstrct()+
                        " proveedor = "+r.getProveedor()+" tipo_documento = "+r.getTipo_documento()+
                        " documento = "+r.getDocumento()+", YA EXISTE UN REGISTRO EN cxp_imp_item"+
                        " para la cuota Nro. "+(transfer+1);
                    }
                }else{
                    log[0] = "No se pudo transferir factura dstrct = "+r.getDstrct()+
                    " proveedor = "+r.getProveedor()+" tipo_documento = "+r.getTipo_documento()+
                    " documento = "+r.getDocumento()+", YA EXISTE UN REGISTRO EN cxp_items_doc"+
                    " para la cuota Nro. "+(transfer+1);
                }
            }else{
                log[0] = "No se pudo transferir factura dstrct = "+r.getDstrct()+
                " proveedor = "+r.getProveedor()+" tipo_documento = "+r.getTipo_documento()+
                " documento = "+r.getDocumento()+", YA EXISTE UN REGISTRO EN cxp_doc"+
                " para la cuota Nro. "+(transfer+1);
            }

        }catch(SQLException e){
            try{
                log[0] = "No se pudo transferir factura dstrct = "+r.getDstrct()+
                " proveedor = "+r.getProveedor()+" tipo_documento = "+r.getTipo_documento()+
                " documento = "+r.getDocumento()+" para la cuota Nro. "+(transfer+1)+
                " Error en los datos";

            }catch (Exception ex){}
            throw new SQLException("ERROR en transferirDoc " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }

            this.desconectar("SQL_TRANSFERIR_DOCUMENTO");
        }
        log[1]=""+success;
        return log;
    }
    
     /**
     * Metodo: BuscarFactura, busca los datos de la factura
     * @autor : Ing. Ivan Gomez
     * @param :String distrito, String codigo del proveedor y numero de la factura
     * @version : 1.0
     */
    public void BuscarFactura(String dstrct, String proveedor, String documento)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        factura =null;
        try {
            
            st = crearPreparedStatement("SQL_BUSCAR_FACTURA");
            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,documento);
            log.info("BUSCAR FACTURA -->"+st.toString());
            rs= st.executeQuery();
            if(rs.next()){
                factura = new CXP_Doc();
                factura.setTipo_documento(rs.getString("tipo_documento"));
                factura.setDocumento(rs.getString("documento"));
                factura.setProveedor(rs.getString("proveedor"));
                factura.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
                factura.setDocumento_relacionado(rs.getString("documento_relacionado"));
                factura.setFecha_documento(rs.getString("fecha_documento"));
                factura.setBanco(rs.getString("banco"));
                factura.setVlr_neto(rs.getDouble("vlr_neto_me"));
                factura.setVlr_total(rs.getDouble("vlr_saldo_me"));
                factura.setMoneda(rs.getString("moneda"));
                factura.setSucursal(rs.getString("sucursal"));
                factura.setDescripcion(rs.getString("descripcion"));
                factura.setObservacion(rs.getString("observacion"));
                factura.setUsuario_aprobacion(rs.getString("aprobador"));
                factura.setPlazo(rs.getInt("plazo"));
                factura.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
                factura.setFecha_aprobacion(rs.getString("fecha_aprobacion"));
                factura.setAgencia(rs.getString("agencia"));
                factura.setDstrct(dstrct);
                factura.setMoneda_banco(rs.getString("moneda_banco"));
                
                /*************************************************************/
                factura.setNum_cuotas(rs.getInt("num_cuotas"));
                factura.setFecha_inicio_transfer(rs.getString("fecha_inicio_transfer"));
                factura.setFecha_inicio_transferO(rs.getString("fecha_inicio_transfer"));
                factura.setFrecuencia(rs.getString("frecuencia"));
                /************************************************************/
                factura.setAgenciaBanco(rs.getString("agencia"));
                factura.setCorrida(rs.getString("corrida"));
                factura.setHandle_code(rs.getString("handle_code"));
                
                factura.setVlr_transfer_me( rs.getDouble("vlr_transfer_me") );//Osvaldo
                factura.setReferencia( rs.getString("referencia") );//Osvaldo
                factura.setReg_status( rs.getString("reg_status") );//Osvaldo
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA FACTURA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null)st.close();
            if (rs != null)rs.close();
            desconectar("SQL_BUSCAR_FACTURA");
        }
    }
     /**
     * Muestra el detalle de la factura
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void detalleFactura(String dstrct, String proveedor, String documento, String tipo_doc)throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        factura =null;
        try {
            
            org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
            
            st = crearPreparedStatement("SQL_DETALLE_FACTURA");
            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,tipo_doc);
            st.setString(4,documento);
            rs= st.executeQuery();
            if(rs.next()){
                factura = new CXP_Doc();
                factura.setTipo_documento(rs.getString("tipo_documento"));
                factura.setDocument_name(rs.getString("tipo_documento_des"));
                factura.setDocumento(rs.getString("documento"));
                factura.setProveedor(rs.getString("proveedor"));
                factura.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
                factura.setDocumento_relacionado(rs.getString("documento_relacionado"));
                factura.setDocumento_rel_name(rs.getString("tipo_documento_rel_des"));
                factura.setFecha_documento(rs.getString("fecha_documento"));
                factura.setBanco(rs.getString("banco"));
                factura.setVlr_neto(rs.getDouble("vlr_neto_me"));
                factura.setVlr_total(rs.getDouble("vlr_saldo_me"));
                factura.setMoneda(rs.getString("moneda"));
                factura.setSucursal(rs.getString("sucursal"));
                factura.setDescripcion(rs.getString("descripcion"));
                factura.setObservacion(rs.getString("observacion"));
                factura.setUsuario_aprobacion(rs.getString("aprobador"));
                factura.setPlazo(rs.getInt("plazo"));
                factura.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
                factura.setFecha_aprobacion(rs.getString("fecha_aprobacion"));
                factura.setAgencia(rs.getString("agencia"));
                factura.setDstrct(dstrct);
                factura.setMoneda_banco(rs.getString("moneda_banco"));
                factura.setValor_saldo_anterior(rs.getDouble("vlr_saldo"));
                factura.setValor_saldo_me_anterior(rs.getDouble("vlr_saldo_me"));
                factura.setCorrida(rs.getString("corrida"));
                factura.setNomProveedor(rs.getString("nomprov"));
                factura.setClase_documento(rs.getString("clase_documento"));
                factura.setDstrct(rs.getString("dstrct"));
                //logger.info("detalleFactura-->DSTRCT: " + rs.getString("dstrct"));
                factura.setHandle_code(rs.getString("handle_code"));
                factura.setBeneficiario(rs.getString("beneficiario"));
                
                factura.setNum_cuotas(rs.getInt("num_cuotas"));
                factura.setFecha_inicio_transfer(rs.getString("fecha_inicio_transfer"));
                factura.setFrecuencia(rs.getString("frecuencia"));                               
                factura.setFecha_ultima_transfer( rs.getString( "fecha_ultima_transfer" ));
                factura.setReferencia( rs.getString( "referencia" ));
                factura.setNum_cuotas_transfer( rs.getInt("num_cuotas_transfer") );
                factura.setVlr_transfer_me( rs.getDouble("vlr_transfer_me") );
                factura.setReg_status( rs.getString("reg_status") );//Osvaldo
                factura.setVlr_neto_me( rs.getDouble("vlr_neto_neto_me") );//Osvaldo, aqui va el neto de la factura porque tito uso el setVlr_neto para un valor que no es el neto
                
                factura.setTotalIva(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), factura.getAgencia(), factura.getFecha_documento(),  "IVA"));
                factura.setTotalRiva(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), factura.getAgencia(), factura.getFecha_documento(), "RIVA"));
                factura.setTotalRica(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), factura.getAgencia(), factura.getFecha_documento(), "RICA"));
                factura.setTotalRfte(this.buscarTotalIMP(factura.getDstrct(), factura.getProveedor(), factura.getTipo_documento(), factura.getDocumento(), factura.getAgencia(), factura.getFecha_documento(), "RFTE"));
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA FACTURA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null)st.close();
            if (rs != null)rs.close();
            desconectar("SQL_DETALLE_FACTURA");
        }
    }
    
      /**
     * Metodo que Actualiza los datos de la factura .
     * @autor.......Ivan Gomez
     * @param.......CXDoc doc(Objeto tipo CXPDoc), Vector vItems(Vector de objetos CxPItem),Vector vImpuestos(Vector de Objetos CXPImpDoc).
     * @throws......Existe Documento en el Sistema.
     * @version.....1.0.
     * @return.......
     */
    public void UpdateFactura( CXP_Doc doc,Vector vItems,Vector vImpuestosDoc,String agencia ) throws Exception {
        Connection con = null;
        Statement st = null;
        PreparedStatement ps = null;
        ResultSet rs         = null;
        String sql = "";
        String SQL_INSERTAROCUMENTO      = "";
        String SQL_INSERTARITEMDOCUMENTO = "";
        String SQL_INSERTARIMPPORITEM    = "";
        String SQL_INSERTARIMPDOC        = "";
        
        try {
            con = conectar("SQL_INSERTAROCUMENTO");
            con.setAutoCommit(false);
            st = con.createStatement();
            SQL_INSERTAROCUMENTO      = this.obtenerSQL("SQL_INSERTAROCUMENTO");
            SQL_INSERTARITEMDOCUMENTO = this.obtenerSQL("SQL_INSERTARITEMDOCUMENTO");
            SQL_INSERTARIMPPORITEM    = this.obtenerSQL("SQL_INSERTARIMPPORITEM");
            SQL_INSERTARIMPDOC        = this.obtenerSQL("SQL_INSERTARIMPDOC");
            sql = this.obtenerSQL("SQL_DELETE_FACTURA").replace("#DISTRITO#",doc.getDstrct()).replace("#PROVEEDOR#",doc.getProveedor()).replace("#DOC#",doc.getDocumento());
            ps = con.prepareStatement(sql);
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(SQL_INSERTAROCUMENTO);
            ps.setString(1, doc.getDstrct());
            ps.setString(2, doc.getProveedor());
            ps.setString(3, doc.getTipo_documento());
            ps.setString(4, doc.getDocumento());
            ps.setString(5, doc.getDescripcion());
            ps.setString(6, agencia);
            ps.setString(7, doc.getHandle_code());
            ps.setString(8, doc.getId_mims());
            ps.setString(9, doc.getFecha_documento());
            ps.setString(10, doc.getTipo_documento_rel());
            ps.setString(11, doc.getDocumento_relacionado());
            ps.setString(12, doc.getFecha_aprobacion());
            ps.setString(13, doc.getAprobador());
            ps.setString(14, doc.getUsuario_aprobacion());
            ps.setString(15, doc.getFecha_vencimiento());
            ps.setString(16, doc.getUltima_fecha_pago());
            ps.setString(17, doc.getBanco());
            ps.setString(18, doc.getSucursal());
            ps.setString(19, doc.getMoneda());
            ps.setDouble(20, doc.getVlr_neto());
            ps.setDouble(21, doc.getVlr_total_abonos());
            ps.setDouble(22, doc.getVlr_saldo());
            ps.setDouble(23, doc.getVlr_neto_me());
            ps.setDouble(24, doc.getVlr_total_abonos_me());
            ps.setDouble(25, doc.getVlr_saldo_me());
            ps.setDouble(26, doc.getTasa());
            ps.setString(27, doc.getUsuario_contabilizo());
            ps.setString(28, doc.getFecha_contabilizacion());
            ps.setString(29, doc.getUsuario_anulo());
            ps.setString(30, doc.getFecha_anulacion());
            ps.setString(31, doc.getFecha_contabilizacion_anulacion());
            ps.setString(32, doc.getObservacion());
            ps.setDouble(33, doc.getNum_obs_autorizador());
            ps.setDouble(34, doc.getNum_obs_pagador());
            ps.setDouble(35, doc.getNum_obs_registra());
            ps.setString(36, doc.getCreation_user());
            ps.setString(37, doc.getUser_update());
            ps.setString(38, doc.getBase());
            ps.setString(39, doc.getMoneda_banco());
            /************************************************/
            
            ps.setInt(40, doc.getNum_cuotas());
            ps.setString(41, doc.getFecha_inicio_transfer());
            ps.setString(42, doc.getFrecuencia());
            ps.setString(43, doc.getReferencia());//Osvaldo            
            
            /***********************************************/
            log.info("CONSULTA 1 " + ps);
            st.addBatch(ps.toString());
            
            
            for (int i=0; i < vItems.size();i++) {
                CXPItemDoc item =(CXPItemDoc)vItems.elementAt(i);
                if(item.getDescripcion()!=null){
                    ps = con.prepareStatement(SQL_INSERTARITEMDOCUMENTO);
                    ps.setString(1, item.getDstrct());
                    ps.setString(2, item.getProveedor());
                    ps.setString(3, item.getTipo_documento());
                    ps.setString(4, item.getDocumento());
                    ps.setString(5, item.getItem());
                    ps.setString(6, item.getDescripcion());
                    ps.setDouble(7, item.getVlr());
                    ps.setDouble(8, item.getVlr_me());
                    ps.setString(9, item.getCodigo_cuenta());
                    ps.setString(10, item.getCodigo_abc());
                    ps.setString(11, item.getPlanilla());
                    ps.setString(12, item.getUser_update());
                    ps.setString(13, item.getCreation_user());
                    ps.setString(14, item.getBase());
                    ps.setString(15, item.getCodcliarea());
                    ps.setString(16, item.getTipcliarea());
                    ps.setString(17, item.getConcepto());
                    if(item.getTipoSubledger().equals(""))
                        ps.setString(18, item.getAuxiliar());
                    else
                        ps.setString(18, item.getTipoSubledger()+"-"+item.getAuxiliar());
                    
                    st.addBatch(ps.toString());
                    
                    
                    Vector vImpDoc = item.getVItems();
                    for( int ii =0;ii< vImpDoc.size();ii++) {
                        CXPImpItem impItem =(CXPImpItem)vImpDoc.elementAt(ii);
                        ps = con.prepareStatement(SQL_INSERTARIMPPORITEM);
                        ps.setString(1, impItem.getDstrct());
                        ps.setString(2, impItem.getProveedor());
                        ps.setString(3, impItem.getTipo_documento());
                        ps.setString(4, impItem.getDocumento());
                        ps.setString(5, impItem.getItem());
                        ps.setString(6, impItem.getCod_impuesto());
                        ps.setDouble(7, impItem.getPorcent_impuesto());
                        ps.setDouble(8, impItem.getVlr_total_impuesto());
                        ps.setDouble(9, impItem.getVlr_total_impuesto_me());
                        ps.setString(10, impItem.getCreation_user());
                        ps.setString(11, impItem.getUser_update());
                        ps.setString(12, impItem.getBase());
                        log.info("CONSULTA 3 " + ps);
                        st.addBatch(ps.toString());
                    }
                }
            }
            
            for(int i =0;i<vImpuestosDoc.size();i++) {
                CXPImpDoc impDoc =(CXPImpDoc) vImpuestosDoc.elementAt(i);
                ps = con.prepareStatement(SQL_INSERTARIMPDOC);
                ps.setString(1, impDoc.getDstrct());
                ps.setString(2, impDoc.getProveedor());
                ps.setString(3, impDoc.getTipo_documento());
                ps.setString(4, impDoc.getDocumento());
                ps.setString(5, impDoc.getCod_impuesto());
                ps.setDouble(6, impDoc.getPorcent_impuesto());
                ps.setDouble(7, impDoc.getVlr_total_impuesto());
                ps.setDouble(8, impDoc.getVlr_total_impuesto_me());
                ps.setString(9, impDoc.getCreation_user());
                ps.setString(10, impDoc.getUser_update());
                ps.setString(11, impDoc.getBase());
                log.info("CONSULTA 4 " + ps);
                st.addBatch(ps.toString());
            }
            
            st.executeBatch();
            
            con.commit();
            con.setAutoCommit(true);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            if(con!=null) {
                con.rollback();
            }
            throw new SQLException("Error en rutina UpdateFactura  [insertarCXPDoc].... \n"+ e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar("SQL_INSERTAROCUMENTO");
        }
        
    }
    
     /**
     * Metodo:          ConsultarCXP_Doc
     * Descriocion :    Funcion publica que nos arroja la informacion de un documento en cuentas por pagar
     * @autor :         Leonardo Parody
     * @modificado por: LREALES - mayo del 2006
     * @param :         distrito, proveedor, tipo de documento y documento
     * @return:         cXP_Doc ( retorna un Objeto )
     * @version :       1.0
     */
    public CXP_Doc ConsultarCXP_Doc( String distrito, String proveedor, String tipo_documento, String documento ) throws SQLException{
        
        CXP_Doc cXP_Doc = new CXP_Doc();
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            
            st = crearPreparedStatement( "SQL_ConsultarCXP_Doc" );
            
            st.setString( 1, distrito );
            st.setString( 2, proveedor );
            st.setString( 3, tipo_documento );
            st.setString( 4, documento );
            
            rs = st.executeQuery();
            
            vFacturas = new Vector();
            
            while ( rs.next() ){
                
                cXP_Doc = cXP_Doc.loadRXP_Doc( rs );
                cXP_Doc.setReferencia( rs.getString("referencia") );
                
            }
            
        } catch( SQLException e ){
            
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DEL DOCUMENTO DE CUENTAS POR PAGAR " + e.getMessage() + " " + e.getErrorCode() );
            
        } finally{
            
            if( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_ConsultarCXP_Doc" );
            
        }
        
        return cXP_Doc;
        
    }
   /**
     * Metodo obtenerAgencias , Metodo para buscar las agencias
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void consultarDocumento(
    String dstrct,
    String fra,
    String tipo_doc,
    String agc,
    String banco,
    String sucursal,
    String nit,
    String fechai,
    String fechaf,
    String pagada,
    String anuladas,
    String tipo) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        vecCxp_doc = null;
        vecCxp_doc = new Vector();
        System.gc();
        try{
            
            con = this.conectar("SQL_CONSULTA_RXP");
            String sql = this.obtenerSQL("SQL_CONSULTA_RXP");
            
            sql = ( fra.length()!=0 ) ? sql.replaceAll("#1", " AND documento = '" + fra + "' ") : sql.replaceAll("#1", "");
            sql = ( nit.length()!=0 ) ? sql.replaceAll("#2", " AND proveedor = '" + nit + "' ") : sql.replaceAll("#2", "");
            sql = ( agc.length()!=0 ) ? sql.replaceAll("#3", " AND agencia = '" + agc + "' ") : sql.replaceAll("#3", "");
            sql = ( banco.length()!=0 ) ? sql.replaceAll("#4", " AND banco = '" + banco + "' ") : sql.replaceAll("#4", "");
            sql = ( sucursal.length()!=0 ) ? sql.replaceAll("#5", " AND sucursal = '" + sucursal + "' ") : sql.replaceAll("#5", "");
            
            sql = ( fechai.length()!=0 && fechaf.length()!=0 ) ? sql.replaceAll("#6", " AND fecha_documento BETWEEN '" + fechai + "' AND '" + fechaf + "' ") : sql.replaceAll("#6", "");
            
            sql = ( pagada.length()!=0 ) ?
            ( pagada.equals("S") ) ? sql.replaceAll("#8", " AND num_cuotas = num_cuotas_transfer ") : sql.replaceAll("#8", " AND num_cuotas != num_cuotas_transfer ")
            : sql.replaceAll("#8", "");
            
            sql = anuladas.equals("all")? sql.replaceAll("#ANULADAS#","") : sql;
            sql = anuladas.equals("null")? sql.replaceAll("#ANULADAS#","AND a.reg_status = 'A'") : sql;
            sql = anuladas.equals("nonull")? sql.replaceAll("#ANULADAS#","AND a.reg_status != 'A'") : sql;
            
            if( !tipo.equals("") ){
                sql = sql.replaceAll("#TIPO#", " AND tipo = '"+tipo+"'");
            }else{
                sql = sql.replaceAll("#TIPO#", "");
            }
            
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);
            
            org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
            
            logger.info("SQL CONSULTA FACTURA: " + st);
            ////System.out.println(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                factura = new CXP_Doc();
                factura.setDstrct(rs.getString("dstrct"));
                
                factura.setProveedor(rs.getString("proveedor"));
                factura.setNomProveedor(rs.getString("nomprov"));
                
                
                factura.setDocumento(rs.getString("documento"));
                factura.setTipo_documento(rs.getString("tipo_documento"));
                factura.setDocumento_relacionado(rs.getString("documento_relacionado"));
                factura.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
                factura.setFecha_documento(rs.getString("fecha_documento"));
                factura.setDescripcion(rs.getString("descripcion"));
                factura.setObservacion(rs.getString("observacion"));
                factura.setVlr_saldo(rs.getDouble("vlr_saldo"));
                factura.setVlr_saldo_me(rs.getDouble("vlr_saldo_me"));
                factura.setCorrida(rs.getString("corrida"));
                factura.setCheque(rs.getString("cheque"));
                
                factura.setMoneda(rs.getString("moneda"));
                factura.setVlr_neto(rs.getDouble("vlr_neto"));
                factura.setVlr_neto_me(rs.getDouble("vlr_neto_me"));
                
                
                factura.setAgencia(rs.getString("nomagc"));
                factura.setBanco(rs.getString("banco"));
                factura.setSucursal(rs.getString("sucursal"));
                
                factura.setCreation_user(rs.getString("creation_user"));
                factura.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                factura.setUltima_fecha_pago(rs.getString("fecha_ultima_transfer"));
                
                
                factura.setNum_cuotas(rs.getInt("num_cuotas"));
                factura.setNum_cuotas_transfer(rs.getInt("num_cuotas_transfer"));
                factura.setVlr_transfer_me(rs.getDouble("vlr_transfer_me"));
                factura.setReg_status( rs.getString("reg_status") );//Osvaldo
                factura.setTipo( Util.coalesce( rs.getString("tipo_factura"), "" ) );//Osvaldo
                vecCxp_doc.addElement(factura);
            }
            System.gc();
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_BUSCAR_DOCUMENTOS SQL_CONSULTA_RXP" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_CONSULTA_RXP");
        }
    }
    
    
     /**
     * Metodo: DeleteFactura, Actualiza la factura pra su modificacion
     * @autor : Ing. Ivan Gomez
     * @param : String distrito, String codigo del proveedor y numero de la factura
     * @version : 1.0
     */
    public void UpdateFacturaaa(String dstrct, String proveedor, String documento){
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String sql ="";
        
        sql = this.obtenerSQL("SQL_DELETE_FACTURA").replace("#DISTRITO#",dstrct).replace("#PROVEEDOR#",proveedor).replace("#DOC#",documento);
        //con = this.conectar("SQL_DELETE_FACTURA");
        //st = con.prepareStatement(sql);
        //st.executeUpdate();
    }
    
     /**
     * Metodo que Inserta un Documento por pagar en el Sistema .
     * @autor.......David Lamadrid
     * @param.......CXDoc doc(Objeto tipo CXPDoc), Vector vItems(Vector de objetos CxPItem),Vector vImpuestos(Vector de Objetos CXPImpDoc).
     * @throws......Existe Documento en el Sistema.
     * @version.....1.0.
     * @return.......
     */
    public void insertarCXPDoc( CXP_Doc doc,Vector vItems,Vector vImpuestosDoc,String agencia ) throws Exception {
        Connection con = null;
        Statement st = null;
        PreparedStatement ps = null;
        ResultSet rs         = null;
        String SQL_INSERTAROCUMENTO      = "";
        String SQL_INSERTARITEMDOCUMENTO = "";
        String SQL_INSERTARIMPPORITEM    = "";
        String SQL_INSERTARIMPDOC        = "";
        
        try {
            con = conectar("SQL_INSERTAROCUMENTO");
            con.setAutoCommit(false);
            st = con.createStatement();
            SQL_INSERTAROCUMENTO      = this.obtenerSQL("SQL_INSERTAROCUMENTO");
            SQL_INSERTARITEMDOCUMENTO = this.obtenerSQL("SQL_INSERTARITEMDOCUMENTO");
            SQL_INSERTARIMPPORITEM    = this.obtenerSQL("SQL_INSERTARIMPPORITEM");
            SQL_INSERTARIMPDOC        = this.obtenerSQL("SQL_INSERTARIMPDOC");
            
            ps = con.prepareStatement(SQL_INSERTAROCUMENTO);
            ps.setString(1, doc.getDstrct());
            ps.setString(2, doc.getProveedor());
            ps.setString(3, doc.getTipo_documento());
            ps.setString(4, doc.getDocumento());
            ps.setString(5, doc.getDescripcion());
            ps.setString(6, agencia);
            ps.setString(7, doc.getHandle_code());
            ps.setString(8, doc.getId_mims());
            ps.setString(9, doc.getFecha_documento());
            ps.setString(10, doc.getTipo_documento_rel());
            ps.setString(11, doc.getDocumento_relacionado());
            ps.setString(12, doc.getFecha_aprobacion());
            ps.setString(13, doc.getAprobador());
            ps.setString(14, doc.getUsuario_aprobacion());
            ps.setString(15, doc.getFecha_vencimiento());
            ps.setString(16, doc.getUltima_fecha_pago());
            ps.setString(17, doc.getBanco());
            ps.setString(18, doc.getSucursal());
            ps.setString(19, doc.getMoneda());
            ps.setDouble(20, doc.getVlr_neto());
            ps.setDouble(21, doc.getVlr_total_abonos());
            ps.setDouble(22, doc.getVlr_saldo());
            ps.setDouble(23, doc.getVlr_neto_me());
            ps.setDouble(24, doc.getVlr_total_abonos_me());
            ps.setDouble(25, doc.getVlr_saldo_me());
            ps.setDouble(26, doc.getTasa());
            ps.setString(27, doc.getUsuario_contabilizo());
            ps.setString(28, doc.getFecha_contabilizacion());
            ps.setString(29, doc.getUsuario_anulo());
            ps.setString(30, doc.getFecha_anulacion());
            ps.setString(31, doc.getFecha_contabilizacion_anulacion());
            ps.setString(32, doc.getObservacion());
            ps.setDouble(33, doc.getNum_obs_autorizador());
            ps.setDouble(34, doc.getNum_obs_pagador());
            ps.setDouble(35, doc.getNum_obs_registra());
            ps.setString(36, doc.getCreation_user());
            ps.setString(37, doc.getUser_update());
            ps.setString(38, doc.getBase());
            ps.setString(39, doc.getMoneda_banco());
            
            
            /************************************************/
            
            ps.setInt(40, doc.getNum_cuotas());
            ps.setString(41, doc.getFecha_inicio_transfer());
            ps.setString(42, doc.getFrecuencia());
            ps.setString(43, doc.getReferencia());//Osvaldo            
            
            /***********************************************/
            
            //System.out.println("SQL INSERT-->" + ps.toString());
            st.addBatch(ps.toString());
            
            log.info("ITEMS RXP: " + vItems.size());
            for (int i=0; i < vItems.size();i++) {
                CXPItemDoc item =(CXPItemDoc)vItems.elementAt(i);
                if(item.getDescripcion()!=null){
                    ps = con.prepareStatement(SQL_INSERTARITEMDOCUMENTO);
                    ps.setString(1, item.getDstrct());
                    ps.setString(2, item.getProveedor());
                    ps.setString(3, item.getTipo_documento());
                    ps.setString(4, item.getDocumento());
                    ps.setString(5, item.getItem());
                    ps.setString(6, item.getDescripcion());
                    ps.setDouble(7, item.getVlr());
                    ps.setDouble(8, item.getVlr_me());
                    ps.setString(9, item.getCodigo_cuenta());
                    ps.setString(10, item.getCodigo_abc());
                    ps.setString(11, item.getPlanilla());
                    ps.setString(12, item.getUser_update());
                    ps.setString(13, item.getCreation_user());
                    ps.setString(14, item.getBase());
                    ps.setString(15, item.getCodcliarea());
                    ps.setString(16, item.getTipcliarea());
                    ps.setString(17, item.getConcepto());
                    if(item.getTipoSubledger().equals(""))
                        ps.setString(18, item.getAuxiliar());
                    else
                        ps.setString(18, item.getTipoSubledger()+"-"+item.getAuxiliar());
                    
                    st.addBatch(ps.toString());
                    
                    
                    Vector vImpDoc = item.getVItems();
                    for( int ii =0;ii< vImpDoc.size();ii++) {
                        CXPImpItem impItem =(CXPImpItem)vImpDoc.elementAt(ii);
                        ps = con.prepareStatement(SQL_INSERTARIMPPORITEM);
                        ps.setString(1, impItem.getDstrct());
                        ps.setString(2, impItem.getProveedor());
                        ps.setString(3, impItem.getTipo_documento());
                        ps.setString(4, impItem.getDocumento());
                        ps.setString(5, impItem.getItem());
                        ps.setString(6, impItem.getCod_impuesto());
                        ps.setDouble(7, impItem.getPorcent_impuesto());
                        ps.setDouble(8, impItem.getVlr_total_impuesto());
                        ps.setDouble(9, impItem.getVlr_total_impuesto_me());
                        ps.setString(10, impItem.getCreation_user());
                        ps.setString(11, impItem.getUser_update());
                        ps.setString(12, impItem.getBase());
                        log.info("CONSULTA 3 " + ps);
                        st.addBatch(ps.toString());
                    }
                }
            }
            
            for(int i =0;i<vImpuestosDoc.size();i++) {
                CXPImpDoc impDoc =(CXPImpDoc)vImpuestosDoc.elementAt(i);
                ps = con.prepareStatement(SQL_INSERTARIMPDOC);
                ps.setString(1, impDoc.getDstrct());
                ps.setString(2, impDoc.getProveedor());
                ps.setString(3, impDoc.getTipo_documento());
                ps.setString(4, impDoc.getDocumento());
                ps.setString(5, impDoc.getCod_impuesto());
                ps.setDouble(6, impDoc.getPorcent_impuesto());
                ps.setDouble(7, impDoc.getVlr_total_impuesto());
                ps.setDouble(8, impDoc.getVlr_total_impuesto_me());
                ps.setString(9, impDoc.getCreation_user());
                ps.setString(10, impDoc.getUser_update());
                ps.setString(11, impDoc.getBase());
                log.info("CONSULTA 4 " + ps);
                st.addBatch(ps.toString());
            }
            
            st.executeBatch();
            
            con.commit();
            con.setAutoCommit(true);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            if(con!=null) {
                con.rollback();
            }
            throw new SQLException("Error en rutina INSERT  [insertarCXPDoc].... \n"+ e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar("SQL_INSERTAROCUMENTO");
        }
        
    }

     /**
     * Metodo: BuscarItemFactura, busca los datos de la factura
     * @autor : Ing. Ivan Gomez
     * @param : String distrito, String codigo del proveedor y numero de la factura
     * @version : 1.0
     */
    public Vector BuscarItemFactura(CXP_Doc factura,Vector vTipImpuestos)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        PreparedStatement st2 = null;
        ResultSet rs2 = null;
        PreparedStatement st3 = null;
        ResultSet rs3 = null;
        Vector vItems= new Vector();
        String agencia = factura.getAgencia().equals("OP")?"BQ":factura.getAgencia();
        try {
            st3 = crearPreparedStatement("SQL_IMP_X_ITEM");
            st = crearPreparedStatement("SQL_ITEM_FACTURA");
            st.setString(1,factura.getDstrct());
            st.setString(2,factura.getProveedor());
            st.setString(3,factura.getDocumento());
            rs = st.executeQuery();
            double totalFac =0;
            while(rs.next()){
                String iva ="";
                String[]  codigos ={"-","--","---","---","----"};
                CXPItemDoc item = new CXPItemDoc();
                item.setConcepto      (rs.getString("concepto"));
                item.setDescripcion   (rs.getString("descripcion"));
                item.setCodigo_cuenta(rs.getString("codigo_cuenta"));
                if(item.getCodigo_cuenta().length()==13){
                    codigos[0] = item.getCodigo_cuenta().substring(0,1);
                    codigos[1] = item.getCodigo_cuenta().substring(1,3);
                    codigos[2] = item.getCodigo_cuenta().substring(3,6);
                    codigos[3] = item.getCodigo_cuenta().substring(6,9);
                    codigos[4] = item.getCodigo_cuenta().substring(9,13);
                }
                if((item.getCodigo_cuenta().length()==12)&& (item.getCodigo_cuenta().substring(0,8).equals("28150502") )){
                    item.setRee("C");
                    item.setRef3(item.getCodigo_cuenta().substring(8,item.getCodigo_cuenta().length()));
                    item.setRef4("");
                }else if ((item.getCodigo_cuenta().length()==14)&& (item.getCodigo_cuenta().substring(0,10).equals("1370100121") )){
                    item.setRee("P");
                    item.setRef3("");
                    item.setRef4(item.getCodigo_cuenta().substring(10,item.getCodigo_cuenta().length()));
                }else{
                    item.setRee("");
                    item.setRef3("");
                    item.setRef4("");
                }
                
                item.setCodigos(codigos);
                item.setCodigo_abc    (rs.getString("codigo_abc"));
                item.setPlanilla      (rs.getString("planilla"));
                item.setVlr_me           (rs.getDouble("vlr_me"));
                
                double VlrTotal     =  rs.getDouble("vlr_me");
                item.setTipcliarea    (rs.getString("tipcliarea"));
                item.setCodcliarea    (rs.getString("codcliarea"));
                item.setItem          (rs.getString("item"));
                //Ivan Gomez 22 julio 2006
                item.setAuxiliar      (rs.getString("auxiliar"));
                int a = item.getAuxiliar().indexOf("-");
                String vec[]  =  item.getAuxiliar().split("-");
                if( vec.length>1){
                    item.setTipoSubledger(vec[0]);
                    item.setAuxiliar     (vec[1]);
                }
                else
                    item.setTipoSubledger("");
                
                /*if(a != -1 ){
                    String [] aux =  item.getAuxiliar().split("-");
                    item.setTipoSubledger(aux[0]);
                    item.setAuxiliar(aux[1]);
                }else{
                    item.setTipoSubledger("");
                }*/
                
                //////////////////////////////////////////////////
                //st2 = crearPreparedStatement("SQL_TIPO_IMPUESTOS");
                //rs2 = st2.executeQuery();
                Vector vImpuestosPorItem= new Vector();
                for(int x=0;x<vTipImpuestos.size();x++){
                    String ag = agencia;
                    String imp = (String) vTipImpuestos.elementAt(x);
                    ag = imp.equals("RICA")?ag:"";
                    CXPImpItem impuestoItem = new CXPImpItem();
                    st3.clearParameters();
                    st3.setString(1,factura.getDstrct());
                    st3.setString(2,factura.getProveedor());
                    st3.setString(3,factura.getDocumento());
                    st3.setString(4,item.getItem());
                    st3.setString(5, ag );
                    st3.setString(6, imp);
                    rs3 = st3.executeQuery();
                    if(rs3.next()){
                        impuestoItem.setCod_impuesto(rs3.getString("cod_impuesto"));
                        vImpuestosPorItem.add(impuestoItem);
                        
                        if(imp.equals("IVA")){
                            VlrTotal += rs3.getDouble("vlr_total_impuesto_me");
                            iva      =  rs3.getString("porcent_impuesto");
                        }
                    }else{
                        impuestoItem.setCod_impuesto("");
                        vImpuestosPorItem.add(impuestoItem);
                    }
                }
                //System.out.println("IVAAA - " + iva);
                totalFac += VlrTotal;
                item.setVlr_total(VlrTotal);
                item.setIva(iva);
                item.setVItems( vImpuestosPorItem);
                item.setVCopia(vImpuestosPorItem);
                vItems.add(item);
                //item.setVlr_total     (valor_t); tengo que extraer los valores de los impuestos y aplicarlos
            }
            factura.setVlr_total(totalFac);
            factura.setVlr_neto(totalFac);
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS ITEM " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st  != null)st.close();
            if (st2 != null)st.close();
            if (st3 != null)st.close();
            if (rs  != null)rs.close();
            if (rs2 != null)rs2.close();
            if (rs3 != null)rs3.close();
            desconectar("SQL_ITEM_FACTURA");
            desconectar("SQL_IMP_X_ITEM");
        }
        return vItems;
    }
    
    /**
     * Metodo buscarFacturasPlanillas , Metodo para buscar las facturas de una planilla
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @throws  : SQLException
     * @version : 1.0
     */
    public void buscarFacturasPlanillas(String dstrct,
    String fra,
    String tipo_doc,
    String agc,
    String banco,
    String sucursal,
    String nit,
    String fechai,
    String fechaf,
    String pagada,
    String planilla) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con =null;
        vecCxp_doc = null;
        vecCxp_doc = new Vector();
        System.gc();
        String sql = this.obtenerSQL("SQL_FACTURA_PLANILLA");
        try{
            
            
            sql = ( nit.length()!=0 ) ? sql.replaceAll("#1", " AND fra.proveedor = '" + nit + "' ") : sql.replaceAll("#1", "");
            sql = ( agc.length()!=0 ) ? sql.replaceAll("#2", " AND fra.agencia = '" + agc + "' ") : sql.replaceAll("#2", "");
            sql = ( banco.length()!=0 ) ? sql.replaceAll("#3", " AND fra.banco = '" + banco + "' ") : sql.replaceAll("#3", "");
            sql = ( sucursal.length()!=0 ) ? sql.replaceAll("#4", " AND fra.sucursal = '" + sucursal + "' ") : sql.replaceAll("#4", "");
            
            sql = ( fechai.length()!=0 && fechaf.length()!=0 ) ? sql.replaceAll("#5", " AND fra.fecha_documento BETWEEN '" + fechai + "' AND '" + fechaf + "' ") : sql.replaceAll("#5", "");
            //System.out.println("PAGADA--> " + pagada);
            sql = ( pagada.length()!=0 ) ?
            ( pagada.equals("S") ) ? sql.replaceAll("#6", " AND fra.num_cuotas = fra.num_cuotas_transfer ") : sql.replaceAll("#6", " AND fra.num_cuotas != fra.num_cuotas_transfer ")
            : sql.replaceAll("#6", "");
            
            con = this.conectar("SQL_FACTURA_PLANILLA");
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);
            st.setString(2, planilla);
            //System.out.println("SQL --->" + st.toString());
            rs = st.executeQuery();
            while (rs.next()) {
                factura = new CXP_Doc();
                factura.setDstrct(rs.getString("dstrct"));
                
                factura.setProveedor(rs.getString("proveedor"));
                factura.setNomProveedor(rs.getString("nomprov"));
                
                
                factura.setDocumento(rs.getString("documento"));
                factura.setTipo_documento(rs.getString("tipo_documento"));
                factura.setDocumento_relacionado(rs.getString("documento_relacionado"));
                factura.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
                factura.setFecha_documento(rs.getString("fecha_documento"));
                factura.setDescripcion(rs.getString("descripcion"));
                factura.setObservacion(rs.getString("observacion"));
                factura.setVlr_saldo(rs.getDouble("vlr_saldo"));
                factura.setVlr_saldo_me(rs.getDouble("vlr_saldo_me"));
                factura.setCorrida(rs.getString("corrida"));
                factura.setCheque(rs.getString("cheque"));
                
                factura.setMoneda(rs.getString("moneda"));
                factura.setVlr_neto(rs.getDouble("vlr_neto"));
                factura.setVlr_neto_me(rs.getDouble("vlr_neto_me"));
                
                
                factura.setAgencia(rs.getString("nomagc"));
                factura.setBanco(rs.getString("banco"));
                factura.setSucursal(rs.getString("sucursal"));
                
                factura.setCreation_user(rs.getString("creation_user"));
                factura.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
                factura.setUltima_fecha_pago(rs.getString("fecha_ultima_transfer"));
                
                
                factura.setNum_cuotas(rs.getInt("num_cuotas"));
                factura.setNum_cuotas_transfer(rs.getInt("num_cuotas_transfer"));
                factura.setVlr_transfer_me(rs.getDouble("vlr_transfer_me"));
                
                
                
                vecCxp_doc.addElement(factura);
            }
            
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA SQL_FACTURA_PLANILLA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_FACTURA_PLANILLA");
        }
        
    }
       

    /**
     * Consultar RxP filtrando por concepto
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param nit Nit del proveedor
     * @param fechai Fecha de inicio del per�odo
     * @param fechaf Fecha de fin del per�odo
     * @param conc C�digo del concepto
     * @param pendiente Si el saldo en me es igual a 0
     * @throws SQLException
     * @version 1.0
     */
    public void consultarRxPConcepto (String dstrct, String nit, String fechai, String fechaf, String conc, String pendiente ) throws Exception{
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        PreparedStatement st = null;                
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_POR_CONCEPTO";
        String sql = null;
        
        try{
            sql = this.obtenerSQL(query);
            if( nit.length()!=0 ){
                sql = sql.replaceAll("#NIT#", "AND rxp.proveedor = '" + nit + "' ");
            } else {
                sql = sql.replaceAll("#NIT#", "");
            }
            if( conc.length()!=0 ){
                sql = sql.replaceAll("#CONC#", "AND it.concepto =  '" + conc + "' ");
            } else {
                sql = sql.replaceAll("#CONC#", "");
            }
            if( pendiente.length()!=0 ){
                if( pendiente.equals("S") ){
                    sql = sql.replaceAll("#PEND#", "AND rxp.vlr_saldo_me != 0 ");
                } else {
                    sql = sql.replaceAll("#PEND#", "AND rxp.vlr_saldo_me = 0 ");
                }
            } else {
                sql = sql.replaceAll("#PEND#", "");
            }
                
            con = this.conectar(query);
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);
            st.setString(2, fechai + " 00:00:00");
            st.setString(3, fechaf + " 23:59:59");
            logger.info("?Reporte rxp: " + st);
            
            rs = st.executeQuery();
            
            this.vecCxp_doc = new Vector();
            while( rs.next() ){
                RepGral obj = new RepGral();
                obj.loadReporteRxP(rs);
                this.vecCxp_doc.add(obj);
                //logger.info("... RXP: " + rs.getString("documento") + ", Item: " + rs.getString("item"));
            }
            
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR en RXPDocDAO.cargarConceptos " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            this.desconectar(query);
        }
        
    }
    
    /**
     * Getter for property treemap.
     * @return Value of property treemap.
     */
    public java.util.TreeMap getTreemap() {
        return treemap;
    }    
    
    /**
     * Setter for property treemap.
     * @param treemap New value of property treemap.
     */
    public void setTreemap(java.util.TreeMap treemap) {
        this.treemap = treemap;
    }
    
      /**
     * Metodo: transferir, transfiere las facturas recurrentes a facturas (de rxp a cxp)
     *         seg�n la frecuencia con que se deba transferir  la factura
     * @autor : Osvaldo P�rez Ferrer
     * @param : usuario, usuario que ejecura la transferencia
     * @param : base, base del usuario
     * @version : 1.0
     */
    public String[] transferir(String usuario, String base, String nit) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean recurrentes = false;
        String[] log = new String[2];
        String[] logFinal = new String[3];
        int success = 0;
        int error = 0;
        try{
            st = this.crearPreparedStatement("CUENTAS_X_TRANSFERIR");
            
            String sql = st.toString();
            
            sql = nit.equals("")? sql.replaceAll( "#PROVEEDOR#", "" ) : sql.replaceAll( "#PROVEEDOR#", " AND proveedor = '" + nit + "'" );
            
            rs = st.executeQuery( sql );
            
            CXP_Doc r = new CXP_Doc();
            logFinal[0] = "";
            while(rs.next()){
                recurrentes = true;
                r = factura.loadRXP_Doc(rs);
                r.setFecha_vencimiento( rs.getString("vencimiento") );
                log = this.transferirDoc(r, usuario, base);
                logFinal[0] += log[0]+"\n";
                success += ( log[1].equals("ok") )? 1 : 0;
                error += ( !log[1].equals("ok") )? 1 : 0;
            }
            
            logFinal[1] = ""+success;
            logFinal[2] = ""+error;
            
        }catch(SQLException e){
            throw new SQLException("ERROR en transferir " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            this.desconectar("CUENTAS_X_TRANSFERIR");
        }
        return logFinal;
    }
    
    
    /**
     * Metodo updateBanco, permite actualizar el banco de una factura
     * recibe por parametro
     * @autor : Ing. Ivan Dario Gomez
     * @param : String distrito, String proveedor, String tipo_doc, String documento, String banco,String sucursal,String moneda
     * @version : 1.0
     */
    public void updateBanco(String distrito, String proveedor, String tipo_doc, String documento, String banco,String sucursal) throws SQLException {
        PreparedStatement st = null;
        String moneda ="";
        String agencia = "";
        try{
                st = crearPreparedStatement("SQL_UPDATE_BANCO");
                String [] mon_age = getMonedaBanco(banco,sucursal).split(",");
                moneda = mon_age[0];
                agencia = mon_age[1];
                st.setString(1,banco);
                st.setString(2,sucursal);
                st.setString(3,moneda);
                st.setString(4,agencia);
                st.setString(5,distrito);
                st.setString(6,proveedor);
                st.setString(7,tipo_doc);
                st.setString(8,documento);
                st.executeUpdate();
        }catch(SQLException e){
            throw new SQLException("ERROR AL SQL_UPDATE_BANCO " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null)st.close();
            this.desconectar("SQL_UPDATE_BANCO");   
        }
    }
    
    
    /**
     * Metodo getMonedaBanco, retorna la moneda del banco - sucursal
     * recibe por parametro
     * @autor : Ing. Ivan Dario Gomez
     * @param : String banco,String sucursal
     * @version : 1.0
     */
    public String getMonedaBanco(String banco,String sucursal) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String moneda ="";
        try{
                st = crearPreparedStatement("SQL_GET_MONEDA");
                
                st.setString(1,banco);
                st.setString(2,sucursal);
                
                rs = st.executeQuery();
                if(rs.next()){
                    moneda = rs.getString("currency")!=null?rs.getString("currency"):"";
                    moneda += ","+rs.getString("agency_id");
                }
        }catch(SQLException e){
            throw new SQLException("ERROR AL SQL_GET_MONEDA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null)st.close();
            this.desconectar("SQL_GET_MONEDA");   
        }
        return moneda;
    }
    
     /**
     * Obtener todos los conceptos
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void cargarConceptos (String dstrct ) throws Exception{
        
        PreparedStatement st = null;                
        ResultSet rs = null;
        this.treemap = new TreeMap();
        
        try{
            st = this.crearPreparedStatement("SQL_CONCEPTOS");
            st.setString(1, dstrct);
            rs = st.executeQuery();
            
            while( rs.next() )
                //this.treemap.put(rs.getString("descripcion"), rs.getString("codigo"));
                this.treemap.put(rs.getString("descripcion"), rs.getString("descripcion"));//AMATURANA 27.04.2007
            
        }catch(SQLException e){
            throw new SQLException("ERROR en RXPDocDAO.cargarConceptos " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            this.desconectar("SQL_CONCEPTOS");
        }
        
    }
    
}//fin de la clase
