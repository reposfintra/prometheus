/***************************************
* Nombre Clase ............. TransferenciaDAO.java
* Descripci�n  .. . . . . .  Permite ejecutar SQL para generar las tranferencia
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  31/03/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/


package com.tsp.operation.model.DAOS;


import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;


public class TransferenciaDAO extends MainDAO{
    
    
    
    
     
    public TransferenciaDAO() {
        super("TransferenciaDAO.xml");
    }
    public TransferenciaDAO(String dataBaseName) {
        super("TransferenciaDAO.xml", dataBaseName);
    }
    
    
    public String reset(String val){
        if(val==null)
            val="";
        return val;
    }
    
    
    
    
    
    // ***************************************************************************************************
    // ***************************************************************************************************
    
    
    
    
    
    
     /**
     * Metodo que devuelve la secuencia de cuenta para el propietario
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public int  getUltimaSecuencia(String nit)throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_SECUENCIA_CUENTAS";
        int               sec         = 0;
        try{ 
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, nit  );
            rs = st.executeQuery();
            if( rs.next() )
                sec = rs.getInt(1);
            
            }}catch(Exception e) {
              throw new SQLException(" getUltimaSecuencia  -> " +e.getMessage());
         }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sec;
        
    }
    
    
    
    
    
    
    
    
    
     /**
     * Metodo que devuelve la lista de cuentas del propietario
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public List  getListCuentas(String nit, String tipo)throws Exception{
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              lista       = new LinkedList();
        String            query       = "SQL_LISTA_CUENTAS";
        Connection        con         = null;
        try{ 
        
            
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            String valor = (tipo.equals("ALL"))?" like '%' " : " ='"+ tipo +"'";
            String sql   =  this.obtenerSQL(query).replaceAll("#VALOR#", valor );
            st = con.prepareStatement( sql );
            st.setString(1, nit  );
            rs = st.executeQuery(); 
            while(rs.next()){
                
                Hashtable  cta  =  new  Hashtable();
                   
                   cta.put("dstrct",        reset(  rs.getString("dstrct") )        );
                   cta.put("idmims",        reset(  rs.getString("idmims") )        );
                   cta.put("nit",           reset(  rs.getString("nit") )           );
                   cta.put("banco",         reset(  rs.getString("banco") )         );
                   cta.put("sucursal",      reset(  rs.getString("sucursal") )      );
                   cta.put("cuenta",        reset(  rs.getString("cuenta") )        );
                   cta.put("tipo_cuenta",   reset(  rs.getString("tipo_cuenta") )   );
                   cta.put("nombre_cuenta", reset(  rs.getString("nombre_cuenta") ) );
                   cta.put("cedula_cuenta", reset(  rs.getString("cedula_cuenta") ) );
                   
                   cta.put("secuencia",     reset(  rs.getString("secuencia") )     );
                   cta.put("primaria",      reset(  rs.getString("primaria") )      );
                   
               
                lista.add( cta );
                cta = null;//Liberar Espacio JJCastro
            }
            
             
         }}catch(Exception e) {
             throw new SQLException(" getListCuentas  -> " +e.getMessage());
         }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
        
    }
    
    
    
    
    
    /**
     * Metodo que devuelve informaci�n del proveedor
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public List  getInfoProveedor(String tipo, String id)throws Exception{
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              lista       = new LinkedList();
        String            query       = "SQL_INFO_PROVEEDOR";
        Connection        con         = null;
        try{ 
        
            con          =  this.conectarJNDI( query );//JJCastro fase2
            if(con!=null){
            String campo =  ( tipo.equals("NIT") ) ? "nit" : "id_mims"; 
            String sql   =  this.obtenerSQL( query ).replaceAll("#CAMPO#", campo );
            
            st = con.prepareStatement( sql );
            st.setString(1, id  );
          
            rs = st.executeQuery(); 
            while(rs.next()){
                
                Hashtable  info  =  new  Hashtable();
                    info.put("dstrct",           reset(rs.getString("dstrct")         ) );
                    info.put("nit",              reset(rs.getString("nit")            ) );
                    info.put("id_mims",          reset(rs.getString("id_mims")        ) );
                    info.put("payment_name",     reset(rs.getString("payment_name")   ) );
                    info.put("branch_code",      reset(rs.getString("branch_code")    ) );
                    info.put("bank_account_no",  reset(rs.getString("bank_account_no")) );
                    info.put("agency_id",        reset(rs.getString("agency_id")      ) );
               
                   lista.add( info );
                   info = null;//Liberar Espacio JJCastro
            }
             
             
         }}catch(Exception e) {
             throw new SQLException(" DAO getInfoProveedor  -> " + st.toString() + e.getMessage());
         }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
        
    }
    
    
    
    
    
    
    /**
     * Metodo que devuelve informaci�n de tablagen dependiendo el tipo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public List  getTablaGen(String tipo)throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              lista       = new LinkedList();
        String            query       = "SQL_TABLAGEN";
         try{ 
             con = this.conectarJNDI(query);
             if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo  );
            rs = st.executeQuery(); 
            while(rs.next()){
                
                Hashtable  tabla  =  new  Hashtable();
                    tabla.put("codigo",           rs.getString("table_code")  );
                    tabla.put("descripcion",      rs.getString("descripcion") );
                    tabla.put("referencia",       rs.getString("referencia")  );
                    
                lista.add( tabla );
            tabla = null;//Liberar Espacio JJCastro
            }
             
             
         }}catch(Exception e) {
             throw new SQLException(" getTablaGen  -> " +e.getMessage());
         }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
        
    }
    
    
    
    
    /**
     * Metodo que devuelve informaci�n de tablagen dependiendo el tipo
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public Hashtable  getInfoTablaGen(String tipo, String codigo)throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        Hashtable         tabla       = null;
        String            query       = "SQL_TABLA_GENERAL";
         try{ 
        
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo  );
            st.setString(2, codigo  );
            rs = st.executeQuery(); 
            if(rs.next()){
                tabla  =  new  Hashtable();
                tabla.put("codigo",           rs.getString("table_code") );
                tabla.put("descripcion",      rs.getString("descripcion") );
                tabla.put("referencia",       rs.getString("referencia") );
                
            }
             
             
         }}catch(Exception e) {
             throw new SQLException(" getInfoTablaGen  -> " +e.getMessage());
         }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tabla;
        
    }
    
    
    
    
    
    /**
     * Metodo que busca las cuotas pendientes del propietario
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List searchCuotasPendientes(String tercero, String nit) throws Exception{  
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_CUOTAS_PENDIENTES";
        List              lista       = new LinkedList();
        try{          
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tercero );
            st.setString(2, nit     );
            rs = st.executeQuery(); 
            while (rs.next()){
                Amortizacion amort = new Amortizacion();
                   amort.setDstrct      ( rs.getString("dstrct")        );
                   amort.setPrestamo    ( rs.getString("prestamo")      );
                   amort.setItem        ( rs.getString("item")          );
                   amort.setFechaPago   ( rs.getString("fecha_pago")    );
                   amort.setMonto       ( rs.getDouble("valor_monto")   );
                   amort.setCapital     ( rs.getDouble("valor_capital") );
                   amort.setInteres     ( rs.getDouble("valor_interes") );
                   amort.setTotalAPagar ( rs.getDouble("valor_a_pagar") );
                   amort.setSaldo       ( rs.getDouble("valor_saldo")   );
                lista.add(amort);
            amort = null;//Liberar Espacio JJCastro
            }           
            
            }}catch(Exception e) {
             throw new SQLException(" searchCuotasPendientes  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
     
     
     
     
    
    
    
    /**
     * Metodo que busca las cuentas grabadas
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void deleteDescuentos(List lista) throws Exception{  
        Connection con = null;
         PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_DELETE_DESCUENTOS";
        try{          
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for(int i=0;i<lista.size();i++){
                Hashtable  ht = (Hashtable)lista.get(i);
                st.setString(1, (String)ht.get("distrito") );
                st.setString(2, (String)ht.get("idmims")   );
                st.setString(3, (String)ht.get("nit")      );
                st.execute();
                st.clearParameters();
            }
            
        }}catch(Exception e) {
             throw new SQLException(" deleteDescuentos  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
     
     
     
    
    
    /**
     * Metodo que busca los descuentos de propietsrios
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List getDescuentos() throws Exception{  
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_DESCUENTOSALL";
        List              lista       = new LinkedList();
        try{          
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery(); 
            int cont = 1;
            while (rs.next()){
               Hashtable  ht = new Hashtable();
                 ht.put("id",                     String.valueOf(cont)           );
                 ht.put("distrito",        reset( rs.getString("dstrct")       ) );
                 ht.put("idmims",          reset( rs.getString("idmims")       ) );
                 ht.put("nit",             reset( rs.getString("nit")          ) );
                 ht.put("nombre",          reset( rs.getString("nombre")       ) );
                 ht.put("vlr",             reset( rs.getString("vlr_desc")     ) );
               lista.add(ht);
               cont++;
            }
            
            }}catch(Exception e) {
             throw new SQLException(" getDescuentos  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
     
     
     
    
    /**
     * Metodo que actualiza el descuento al propietarios
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public String updateDescuento(Hashtable  cuenta) throws Exception{  
         Connection con = null;
         PreparedStatement st          = null;
        String            query       = "SQL_UPDATE_DESCUENTO";
        String            msj         = "Registro actualizado...";
        try{          
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              st.setString(1,  (String) cuenta.get("vlr")        );
              st.setString(2,  (String) cuenta.get("idmims")     );
              st.setString(3,  (String) cuenta.get("nit")        );
              st.execute();
            
            }}catch(Exception e) {
             msj = e.getMessage();
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msj;
    }
     
     
     
     
    
    
    /**
     * Metodo que guarda el descuento al propietarios
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public String saveDescuento(Hashtable  cuenta) throws Exception{  
         Connection con = null;
         PreparedStatement st          = null;
        String            query       = "SQL_SAVE_DESCUENTO";
        String            msj         = "Registro guardado...";
        try{                      
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              st.setString(1,  (String) cuenta.get("distrito")   );
              st.setString(2,  (String) cuenta.get("idmims")     );
              st.setString(3,  (String) cuenta.get("nit")        );
              st.setString(4,  (String) cuenta.get("vlr")        );
              st.setString(5,  (String) cuenta.get("user")       );
              st.execute();
            
        }}catch(Exception e) {
             msj = e.getMessage();
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msj;
    }
     
     
     
     
     
    
    
    
    /**
     * Metodo que busca las cuentas grabadas
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void deleteCuentas(List lista) throws Exception{  
         Connection con = null;
         PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_DELETE_CUENTA";
        try{          
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for(int i=0;i<lista.size();i++){
                Hashtable  ht = (Hashtable)lista.get(i);
                st.setString(1, (String)ht.get("nit")         );
                st.setString(2, (String)ht.get("secuencia")   );
                st.execute();
                st.clearParameters();
            }
            }}catch(Exception e) {
             throw new SQLException(" deleteCuentas  -> " +e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
     
     
     
    
    
    
    /**
     * Metodo que busca las cuentas grabadas
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List getTipoCuentas() throws Exception{  
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_TIPO_CUENTA";
        List              lista       = new LinkedList();
        try{          
            con = this.conectarJNDI(query);
           if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery(); 
            while (rs.next()){
               Hashtable  ht = new Hashtable();                
                 ht.put("codigo",           reset( rs.getString("table_code")) );
                 ht.put("descripcion",      reset( rs.getString("descripcion")) );
               lista.add(ht);
            ht = null;//Liberar Espacio JJCastro
            }
            
           }}catch(Exception e) {
             throw new SQLException(" getCuentas  -> " +e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
     
     
     /**
     * Metodo que actualiza la cuenta para la transferencia de una liquidacion
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public String asignarCTA(Transferencia  cta) throws Exception{  
         Connection con = null;
         PreparedStatement st          = null;
        String            query       = "SQL_ASIGNAR_CTA";
        String            msj         = "Registro actualizado...";
        try{     
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              st.setString(1,   cta.getBanco()        );
              st.setString(2,   cta.getSucursal()     );
              st.setString(3,   cta.getCuenta()       );              
              st.setString(4,   cta.getTipoCuenta()   );
              st.setString(5,   cta.getNombreCuenta() );      
              st.setString(6,   cta.getCedulaCuenta() );
              st.setInt   (7,   cta.getSecuencia()    );
              st.setString(8,   cta.getLiquidacion()  );
             st.execute();
              
        }}catch(Exception e) {
             msj = e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msj;
    }
     
     
     
     
     
     
     
     /**
     * Metodo que actualiza cuentas default
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public String updateDefaultCuenta(Hashtable  cuenta) throws Exception{  
         Connection con = null;
         PreparedStatement st = null;
         String query = "SQL_UPDATE_DEFAULT_CTA";
         String msj = "Registro actualizado...";
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, (String) cuenta.get("distrito"));
                 st.setString(2, (String) cuenta.get("nit"));
                 st.execute();

             }
         }catch(Exception e) {
             msj = e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msj;
    }
     
     
     
     
     
    
    
    
     /**
     * Metodo que ELIMIna el prestamo de la liquidacion.
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void deletePrestamo(String liq) throws Exception{  
        PreparedStatement st = null;
         Connection con = null;
         String query = "SQL_DELETE_PRESTAMO";
         try {

             con = this.conectarJNDI(query);//JJCastro fase2
             if (con != null) {
                 String sql = this.obtenerSQL(query).replaceAll("#LIQ#", liq);
                 st = con.prepareStatement(sql);
                 st.execute();

             }
         } catch (Exception e) {
             throw new SQLException(" deleteTransferencia  -> " +e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
    
     
     
     
     
     /**
     * Metodo que  busca los bancos que tengan formatos para transferencias
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public List getBancosFormatos() throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_BUSCAR_BANCOS_FORMATO";
        List              lista       = new LinkedList();        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery(); 
            while (rs.next()){
                Hashtable bk = new Hashtable();
                  bk.put("codigo",      rs.getString("TABLE_CODE")  );
                  bk.put("descripcion", rs.getString("DESCRIPCION") );                
                lista.add(bk);                
            }           
            
            }}catch(Exception e) {
             throw new SQLException(" getBancosFormatos  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
     
     
     
     
     
    /**
     * Metodo que aprueba  la transferencia
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void aprobarTransferencia(Transferencia  tr) throws Exception{
         Connection con = null;
         PreparedStatement st          = null;
        String            query       = "SQL_APROBAR_LIQ";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setDouble( 1, tr.getVlrDescuento()           );
            st.setDouble( 2, tr.getVlrNeto()                );
            st.setDouble( 3, tr.getVlrComisionBancaria()    );
            st.setDouble( 4, tr.getVlrConsignar()           );
            st.setString( 5, tr.getBancoPagoTransferencia() );            
            st.setString( 6, tr.getLiquidacion()            );
            st.execute();
            
            }}catch(Exception e) {
             throw new SQLException(" aprobarTransferencia  -> " +e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
    
    
    
    /**
     * Metodo que BUSCA la transferencia
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public Transferencia getDatosTransferencia(String distrito, String liq) throws Exception{        
        Connection con = null;
        PreparedStatement st          = null; 
        ResultSet         rs          = null;
        Transferencia     transf      = new Transferencia();
        String            query       = "SQL_TRANSFERENCIA";
        try{                       
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );
            st.setString(2, liq      );
            rs = st.executeQuery(); 
            if(rs.next()){
             
                 transf.setDistrito      ( reset( rs.getString("dstrct")       ) );
                 transf.setIdMims        ( reset( rs.getString("idmims")       ) );
                 transf.setNit           ( reset( rs.getString("nit")          ) );
                 transf.setNombre        ( reset( rs.getString("payment_name") ) );
                 transf.setLiquidacion   ( reset( rs.getString("liquidacion")  ) );
                 transf.setBanco         ( reset( rs.getString("banco")        ) );
                 transf.setSucursal      ( reset( rs.getString("sucursal")     ) );
                 transf.setCuenta        ( reset( rs.getString("cuenta")       ) );
                 transf.setTipoCuenta    ( reset( rs.getString("tipo_cuenta")  ) );
                 transf.setNombreCuenta  ( reset( rs.getString("nombre_cuenta")) );
                 transf.setCedulaCuenta  ( reset( rs.getString("cedula_cuenta")) );
                 transf.setVlrLiquidacion     ( rs.getDouble("vlr_liquidacion")  );
                 transf.setVlrDescuento       ( rs.getDouble("vlr_descuento")    );
                 transf.setVlrNeto            ( rs.getDouble("vlr_neto")         );
                 transf.setVlrComisionBancaria( rs.getDouble("vlr_combancaria" ) );
                 transf.setVlrConsignar       ( rs.getDouble("vlr_consignacion") );
                 transf.setTipoPago      ( reset( rs.getString("tipo_pago")    ) );
                
            }  
            
            }}catch(Exception e) {
             throw new SQLException(" getDatosTransferencia  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return transf;
    }
     
    
      
    /**
     * Metodo que elimina la transferencia
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void deleteTransferencia(String liq) throws Exception{        
         Connection con = null;
         PreparedStatement st = null;
         String query = "SQL_DELETE_LIQ";
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, liq);
                 st.execute();
             }
         }catch(Exception e) {
             throw new SQLException(" deleteTransferencia  -> " +e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
     
    
    /**
     * Metodo que graba la transferencia
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public void insertTransferencia(Transferencia transf, String user) throws Exception{
         Connection con = null;
         PreparedStatement st = null;
         String query = "SQL_INSERT_TRANSFERENCIA";
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, transf.getDistrito());
                 st.setString(2, transf.getIdMims());
                 st.setString(3, transf.getNit());
                 st.setString(4, transf.getNombre());
                 st.setString(5, transf.getLiquidacion());
                 st.setString(6, transf.getBanco());
                 st.setString(7, transf.getSucursal());
                 st.setString(8, transf.getCuenta());
                 st.setString(9, transf.getTipoCuenta());
                 st.setString(10, transf.getNombreCuenta());
                 st.setString(11, transf.getCedulaCuenta());
                 st.setDouble(12, transf.getVlrLiquidacion());
                 st.setString(13, transf.getTipoPago());
                 st.setString(14, user);
                 st.setInt(15, transf.getSecuencia());

                 st.setDouble(16, transf.getVlrDescuento());
                 st.setDouble(17, transf.getVlrNeto());
                 st.setDouble(18, transf.getVlrComisionBancaria());
                 st.setDouble(19, transf.getVlrConsignar());
                 st.execute();

             }
         } catch(Exception e) {
             throw new SQLException(" insertTransferencia  -> " +e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
     
    
     /**
     * Metodo que busca datos de la cuenta de transferencia del propietario
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public Transferencia getDatosCuenta(Transferencia transf) throws Exception{        
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_DATOS_CUENTA";
        try{
                       
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, transf.getNit());
                st.setInt(2, transf.getSecuencia());
                rs = st.executeQuery();
                if (rs.next()) {

                    transf.setDistrito(reset(rs.getString("dstrct")));
                    transf.setBanco(reset(rs.getString("banco")));
                    transf.setSucursal(reset(rs.getString("sucursal")));
                    transf.setCuenta(reset(rs.getString("cuenta")));
                    transf.setTipoCuenta(reset(rs.getString("tipo_cuenta")));
                    transf.setNombreCuenta(reset(rs.getString("nombre_cuenta")));
                    transf.setCedulaCuenta(reset(rs.getString("cedula_cuenta")));
                    transf.setDescTipoCuenta(reset(rs.getString("descripcion")));

                }
           
            }}catch(Exception e) {
             throw new SQLException(" getDatosCuenta  -> " +e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return transf;
    }
    
    /**
     * Metodo que busca las cuentas grabadas
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @modificado ... Julio Barros
     * @version ...... 1.0
     */ 
     public List getCuentas() throws Exception{  
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_CUENTASALL";
        List              lista       = new LinkedList();
        try{          
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery(); 
            int cont = 1;
            while (rs.next()){
               Hashtable  ht = new Hashtable();
                 ht.put("id",                     String.valueOf(cont)           );
                 ht.put("distrito",        reset( rs.getString("dstrct")       ) );
                 ht.put("idmims",          reset( rs.getString("idmims")       ) );
                 ht.put("nit",             reset( rs.getString("nit")          ) );
                 ht.put("nombre",          reset( rs.getString("nombre")       ) );
                 ht.put("banco",           reset( rs.getString("banco")        ) );
                 ht.put("sucursal",        reset( rs.getString("sucursal")     ) );
                 ht.put("cuenta",          reset( rs.getString("cuenta")       ) );
                 ht.put("tipo",            reset( rs.getString("tipo_cuenta")  ) );
                 ht.put("nombre_cuenta",   reset( rs.getString("nombre_cuenta")) );
                 ht.put("ced_cuenta",      reset( rs.getString("cedula_cuenta")) );
                 ht.put("secuencia",       reset( rs.getString("secuencia"))     );
                 ht.put("primaria",        reset( rs.getString("primaria"))      );
                 ht.put("vlr_desc",        reset( rs.getString("vlr_desc"))      );
                 ht.put("e_mail",          reset( rs.getString("e_mail"))        );
               lista.add(ht);
               cont++;
            ht = null;//Liberar Espacio JJCastro
            }
            
            }}catch(Exception e) {
             throw new SQLException(" getCuentas  -> " +e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }




/**
     * Metodo que busca las cuentas grabadas
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @modificado ... Julio Barros
     * @version ...... 1.0
     */ 
     public List getCuentas(String nit) throws Exception{  
         Connection con = null;
         PreparedStatement st          = null;
        ResultSet         rs          = null;
        String            query       = "SQL_CUENTASNIT";
        List              lista       = new LinkedList();
        try{          
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, nit );
            rs = st.executeQuery(); 
            int cont = 1;
            while (rs.next()){
               Hashtable  ht = new Hashtable();
                 ht.put("id",                     String.valueOf(cont)           );
                 ht.put("distrito",        reset( rs.getString("dstrct")       ) );
                 ht.put("idmims",          reset( rs.getString("idmims")       ) );
                 ht.put("nit",             reset( rs.getString("nit")          ) );
                 ht.put("nombre",          reset( rs.getString("nombre")       ) );
                 ht.put("banco",           reset( rs.getString("banco")        ) );
                 ht.put("sucursal",        reset( rs.getString("sucursal")     ) );
                 ht.put("cuenta",          reset( rs.getString("cuenta")       ) );
                 ht.put("tipo",            reset( rs.getString("tipo_cuenta")  ) );
                 ht.put("nombre_cuenta",   reset( rs.getString("nombre_cuenta")) );
                 ht.put("ced_cuenta",      reset( rs.getString("cedula_cuenta")) );
                 ht.put("secuencia",       reset( rs.getString("secuencia"))     );
                 ht.put("primaria",        reset( rs.getString("primaria"))      );
                 ht.put("vlr_desc",        reset( rs.getString("vlr_desc"))      );
                 ht.put("e_mail",          reset( rs.getString("e_mail"))        );
                 
               lista.add(ht);
               cont++;
            ht = null;//Liberar Espacio JJCastro
            }
            
            }}catch(Exception e) {
             throw new SQLException(" getCuentas  -> " +e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }




/**
     * Metodo que guarda una cuenta de banco para cancelar a propietarios
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @modificado ... Julio Barros
     * @version ...... 1.0
     */ 
     public String saveCuenta(Hashtable  cuenta, int secuencia ) throws Exception{  
         Connection con = null;
         PreparedStatement st          = null;
        String            query       = "SQL_SAVE_CUENTA_BANCO";
        String            msj         = "Registro guardado...";
        try{     
            
            String nombreCta = (String) cuenta.get("nombre_cuenta");
            String desc      = (String) cuenta.get("descuento");
            
            String defaultCTA =  (String) cuenta.get("principal");
            if( defaultCTA.equals("S")  )
                updateDefaultCuenta( cuenta );
            
            
            nombreCta =  nombreCta.toUpperCase().replaceAll("�","N");
            
            
            con = this.conectarJNDI(query);
            if (con != null) {
              st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              st.setString(1,  (String) cuenta.get("distrito")   );
              st.setString(2,  (String) cuenta.get("idmims")     );
              st.setString(3,  (String) cuenta.get("nit")        );
              st.setString(4,  (String) cuenta.get("banco")      );
              st.setString(5,  (String) cuenta.get("sucursal")   );
              st.setString(6,  (String) cuenta.get("cuenta")     );
              st.setString(7,  (String) cuenta.get("tipo")       );
              st.setString(8,  nombreCta                         );
              st.setString(9,  (String) cuenta.get("ced_cuenta") );
              st.setString(10, (String) cuenta.get("user")       );
              st.setInt   (11, secuencia                         );
              st.setString(12, defaultCTA                        );
              
            st.execute();
            
            
         // Descuento:
            cuenta.put("vlr",  desc);
            String k =  saveDescuento ( cuenta ); 
            if( ! k.equals("") )
                updateDescuento( cuenta ); 
            
            
            }}catch(Exception e) {
             msj = e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        msj = updateEmail((String) cuenta.get("e_mail") ,(String) cuenta.get("nit") );
        return msj;
    }
     
     
     /**
     * Metodo que actualiza una cuenta de banco para cancelar a propietarios
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @modificado ... Julio Barros
     * @version ...... 1.0
     */ 
     public String updateCuenta(Hashtable  cuenta) throws Exception{  
         Connection con = null;
         PreparedStatement st          = null;
        String            query       = "SQL_UPDATE_CUENTA_BANCO";
        String            msj         = "Registro actualizado...";
        try{     
            
            String nombreCta = (String) cuenta.get("nombre_cuenta");
            String desc      = (String) cuenta.get("descuento");
            
            String defaultCTA =  (String) cuenta.get("principal");
            if( defaultCTA.equals("S")  )
                updateDefaultCuenta( cuenta );
            
            nombreCta =  nombreCta.toUpperCase().replaceAll("�","N");
            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, (String) cuenta.get("banco"));
                st.setString(2, (String) cuenta.get("sucursal"));
                st.setString(3, (String) cuenta.get("cuenta"));
                st.setString(4, (String) cuenta.get("tipo"));
                st.setString(5, nombreCta);
                st.setString(6, (String) cuenta.get("ced_cuenta"));
                st.setString(7, defaultCTA);
                st.setString(8, (String) cuenta.get("idmims"));

                st.setString(9, (String) cuenta.get("distrito"));
                st.setString(10, (String) cuenta.get("nit"));
                st.setString(11, (String) cuenta.get("secuencia"));
                st.execute();
            
         // Descuento:
            cuenta.put("vlr",  desc);
            String k =  saveDescuento ( cuenta ); 
            if( ! k.equals("") )
                updateDescuento( cuenta ); 
            
        }}catch(Exception e) {
             msj = e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        msj = updateEmail((String) cuenta.get("e_mail") ,(String) cuenta.get("nit") );
        return msj;
    }



/**
     * Metodo que actualiza una cuenta lengthinserta el e-mail
     * @autor: ....... Julio Barros
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public String updateEmail(String e_mail,String nit) throws Exception{  
         Connection con = null;
         PreparedStatement st          = null;
        String            query       = "SQL_UPDATE_E_MAIL_PROPIETARIO";
        String            msj         = "Registro actualizado...";
        try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, e_mail);
                 st.setString(2, nit);
                 st.execute();
             }
         }catch(Exception e) {
             msj = e.getMessage();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msj;
    }
    
}
