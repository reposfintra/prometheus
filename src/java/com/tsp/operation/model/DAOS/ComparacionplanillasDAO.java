package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import javax.servlet.*;
import com.tsp.util.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.OpDAO;
import com.tsp.operation.model.DAOS.MainDAO;

public class ComparacionplanillasDAO extends MainDAO {
    String PlanillasPostgres;
    /** Creates a new instance of ComparacionplanillasDAO */
    public ComparacionplanillasDAO() {
         super("PorcentajeDAO.xml"); 
    }
    public ComparacionplanillasDAO(String dataBaseName) {
         super("PorcentajeDAO.xml", dataBaseName); 
    }
    Vector vPostgres;
    Vector vOracle;
    public boolean buscarPlanillasPostgres(String f1,String f2) throws SQLException{
        boolean resu=true;
        vPostgres = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
                st = this.crearPreparedStatement("SQL_POSTGRES_PLANILLAS");
                st.setString(1, f1);
                st.setString(2, f2);
              
                ////System.out.println("QUERY: "+st);
                rs = st.executeQuery();
                while (rs.next()) {
                    
                    Planilla p = new Planilla();
                                        
                    p.setNumpla(rs.getString("numpla"));
                    p.setCia(rs.getString("cia"));
                    p.setFechapla(rs.getDate("fecpla"));
                    p.setAgcpla(rs.getString("agcpla"));
                    p.setOripla(rs.getString("oripla"));
                    p.setDespla(rs.getString("despla"));
                    p.setPlaveh(rs.getString("plaveh"));
                    p.setCedcon(rs.getString("cedcon"));
                    p.setFecdsp(rs.getString("fecdsp"));
                    p.setFechaposllegada(rs.getDate("fechaposllegada"));
                    p.setVlrpla(rs.getFloat("vlrpla"));
                    p.setNomcon(rs.getString("nomcon"));
                    p.setOrinom(rs.getString("orinom"));
                    p.setDestinopla(rs.getString("desnom"));
                    //p.setTipoviaje(rs.getString("numpla"));
                    p.setCelularcon(rs.getString("celularcon"));
                    p.setUltimoreporte(rs.getString("ultimoreporte"));
                    //p.setTiempoentransito(rs.getString("tiempoentransito"));
                    p.setObservacion(rs.getString("observacion"));
                    //
                    p.setStatus_220(rs.getString("status_220"));
                    p.setDespachador(rs.getString("despachador"));
                    //p.setFecha_salida(rs.getString("numpla"));
                   
                    vPostgres.add(p);                    
                }
                ////System.out.println("numero de planillas encontradas:"+vPostgres.size());
               
        }catch(SQLException e){
            resu=false;
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR REMESAS POSTGRES " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar( "SQL_POSTGRES_PLANILLAS" );
        }
        return resu;
    }
    public boolean buscarPlanillasOracle() throws SQLException{
        boolean resu=true;
        vOracle = new Vector();
       
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String planilla="";
        PlanillasPostgres="";
        for(int i=0;i<vPostgres.size();i++){
                    
                    try{
                        
                        String query = this.obtenerSQL("SQL_ORACLE_PLANILLAS");
                        int numeroderem=0;
                        PlanillasPostgres="";
                        for(int j=i;j<vPostgres.size();j++){
                            ////System.out.println("numeroderem:"+numeroderem);
                            if(numeroderem<230){
                                ////System.out.println("if:"+j);
                                Planilla pla = (Planilla)vPostgres.get(j);
                                if(!planilla.equals(pla.getNumpla())){
                                    PlanillasPostgres+="'"+pla.getNumpla()+"'";
                                }
                                planilla=pla.getNumpla();
                            }
                            else{
                                i=j-1;
                                ////System.out.println("else:"+j);
                                break;
                            }
                            numeroderem++;
                            i=j;
                        }
                        PlanillasPostgres = PlanillasPostgres.replaceAll("''","','");
                        query = query.replaceAll("#NUMPLA#", PlanillasPostgres);
                        ////System.out.println("QUERY:"+query);
                        Connection con = this.conectar("SQL_ORACLE_PLANILLAS");
                        st = con.prepareStatement(query);
                        rs = st.executeQuery();
                        while (rs.next()) {
                            Planilla p = new Planilla();
                            
                            p.setNumpla(rs.getString(1));
                            p.setCia(rs.getString(2));
                            p.setFechapla(rs.getDate(3));
                            p.setAgcpla(rs.getString(4));
                            p.setOripla(rs.getString(5));
                            p.setDespla(rs.getString(6));
                            p.setPlaveh(rs.getString(7));
                            p.setCedcon(rs.getString(8));
                            p.setFecdsp(rs.getString(9));
                            p.setFechaposllegada(rs.getDate(10));
                            p.setVlrpla(rs.getFloat(11));
                            p.setNomcon(rs.getString(12));
                            p.setOrinom(rs.getString(13));
                            p.setDestinopla(rs.getString(14));
                            //p.setTipoviaje(rs.getString("numpla"));
                            p.setCelularcon(rs.getString(15));
                            p.setUltimoreporte(rs.getString(16));
                            //p.setTiempoentransito(rs.getString("tiempoentransito"));
                            p.setObservacion(rs.getString(17));
                            //
                            p.setStatus_220(rs.getString(18));
                            p.setDespachador(rs.getString(19));
                            //p.setFecha_salida(rs.getString("numpla"));
                            
                            vOracle.add(p);    
                        }
                        ////System.out.println("numero de planillas encontradas:"+vOracle.size());

                    }catch(SQLException e){
                        resu=false;
                        e.printStackTrace();
                        throw new SQLException("ERROR AL BUSCAR REMESAS ORACLE: " + e.getMessage()+"" + e.getErrorCode());
                    }  finally {
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar( "SQL_ORACLE_PLANILLAS" );
        }
                    
                }     
         return resu;
    }
    
    /**
     * Getter for property vPostgres.
     * @return Value of property vPostgres.
     */
    public java.util.Vector getVPostgres() {
        return vPostgres;
    }
    
    /**
     * Setter for property vPostgres.
     * @param vPostgres New value of property vPostgres.
     */
    public void setVPostgres(java.util.Vector vPostgres) {
        this.vPostgres = vPostgres;
    }
    
    /**
     * Getter for property vOracle.
     * @return Value of property vOracle.
     */
    public java.util.Vector getVOracle() {
        return vOracle;
    }
    
    /**
     * Setter for property vOracle.
     * @param vOracle New value of property vOracle.
     */
    public void setVOracle(java.util.Vector vOracle) {
        this.vOracle = vOracle;
    }
}
