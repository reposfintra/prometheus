/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.ArchivosAppMicro;
import com.tsp.operation.model.beans.ParmetrosDinamicaTSP;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

/**
 *
 * @author egonzalez
 */
public interface AdminFintraDAO {

    public String cargarAuditoriaCredi100(String fechaInicio, String fechaFin, String identificacion, String linea_negocio, String numero_solicitud, String negocio, String estado_solicitud);

    public String cargarSolicitudesDocumentos(String fechaInicio, String fechaFin, String linea_negocio, String empresa, String consulta);

    public String cargarLineaNegocio();

    public String cargarEmpresaEnvio();

    public String guardarSolicitudesSeleccionadas(JsonObject info, Usuario usuario, String empresa_envio, String exportar);

    public String cargarLineaNegocio_();

    public String cargarAutorizador();

    public String cargarHC();

    public String buscarProveedor(String informacion_, String parametro_busqueda);

    public String buscarImpuesto(String impuesto);

    public String guardarCabeceraCXP(JsonObject info, Usuario usuario);

    public String buscarCuentas(String parametro_busqueda, String informacion);

    public String cargarMotivo();

    public String buscarMultiservicio(String multiservicio, Usuario usuario);

    public String cargarCxpCabecera(String documento);

    public String cargarCxpDetalle(String documento);

    public String cargarSolicitudesDocumentos(String negocio);

    public String anularCuotaManejo(JsonObject info, Usuario usuario);

    public String cargarNegocioRechazado(String numero_solicitud, String identificacion, String fechaInicio, String fechaFin);

    public String reactivarNegocio(String numero_solicitud, String comentario, String fechaInicio, String fechaFin, Usuario usuario);

    public String guardarDocumentosAnticipoCajaMenor(JsonObject info, Usuario usuario);

    public String cargarAnticipoCajaMenor(String fechaInicio, String fechaFin, String identificacion, String anticipo);

    public String legalizarAnticipoCajaMenor(String valorCXP, String valorLegalizar, String anticipo, Usuario usuario);

    public String buscarCXPGastos(String anticipo);

    public String cargarDetalleAnticipoCajaMenor(String anticipo);

    public String cargarDocumentosAfectoCXC(String factura);

    public String cargarDocumentosAfectoGCM(String factura);

    public String anularAnticipo(String id, Usuario usuario);

    /**
     * Metodo que consulta las dinamicas contables de fintra logistica
     * dependiendo de los pasos definidos
     *
     * @param parametros
     * @param usuario
     * @return
     */
    public String cargarAnticipoTransferenciaPaso(ParmetrosDinamicaTSP parametros, Usuario usuario);

    /**
     * Metodo que permite cargar el reporte de produccion dependiendo de los
     * pasos
     *
     * @param paso
     * @param periodo
     * @param filtro
     * @param periodo_inicio
     * @param periodo_fin
     * @param concept_code
     * @param anulado
     * @return
     */
    public String cargarReporteProduccion(String paso, String periodo, String filtro, String periodo_inicio, String periodo_fin, String concept_code, String anulado);

    public String cargarConsolidadoMovimientoTSP();

    /**
     *
     * @param parametros
     * @return
     */
    public String cargarAnalisisReporteProduccion(ParmetrosDinamicaTSP parametros);

    /**
     *
     * @param parametros
     * @return
     */
    public String cagarReporteInversionista(ParmetrosDinamicaTSP parametros);

    /**
     *
     * @param parametros
     * @return
     */
    public String cagarReporteInversionistaSinFiltros(ParmetrosDinamicaTSP parametros);

    public String cagarTercerInversionistas();

    /**
     *
     * @param parametros
     * @return
     */
    public String cagarHelpDinamicasContables(ParmetrosDinamicaTSP parametros);

    /**
     *
     * @param usuario
     * @return
     */
    public String permisoHelpDinamicasContables(Usuario usuario);

    /**
     *
     * @param parametros
     * @return
     */
    public String cagarHelpDinamicasContablesDatos(ParmetrosDinamicaTSP parametros);

    /**
     *
     * @param id
     * @param textoAyuda
     * @param usuario
     * @return
     */
    public String actualizarHelpDinamicasContablesDatos(String id, String textoAyuda, Usuario usuario);

    /*
     Este metodo es para buscar cuando el valor de paso2 no cuadren 
     */
    /**
     *
     * @param modo_busqueda
     * @param nulo
     * @return
     */
    public String cargarRevisionPaso2(String modo_busqueda, String nulo);

    /**
     *
     * @param anio
     * @param fecha
     * @param cuenta
     * @param usuarios
     * @return
     */
    public String cagarSeguimientoCaja(String anio, String fecha, String cuenta, Usuario usuarios);

    /**
     *
     * @param informacion
     * @param usuario
     * @return
     */
    public String procedimientoSeguimientoCaja(JsonObject informacion, Usuario usuario);

    public String cargarCuentas();

    /**
     *
     * @param informacion_
     * @return
     */
    public String buscarAutoCompletarCuentas(String informacion_);

    /**
     *
     * @param modulo
     * @param cuenta
     * @param usuario
     * @return
     */
    public String cagarConfiguracionCuentasDC(String modulo, String cuenta, Usuario usuario);

    /**
     *
     * @param id
     * @param usuario
     * @return
     */
    public String cambiarEstadoConfiguracionCuentasDC(String id, Usuario usuario);

    /**
     *
     * @param cuenta
     * @return
     */
    public String cagarConfiguracionCuentasDC(String cuenta);

    /**
     *
     * @param info
     * @param usuario
     * @return
     */
    public String guardarInformacionCuenta(JsonObject info, Usuario usuario);

    /**
     *
     * @param info
     * @param usuario
     * @return
     */
    public String actualizarInformacionCuenta(JsonObject info, Usuario usuario);

    /**
     *
     * @param nit
     * @param negocio
     * @return
     */
    public String cagarFacturas(String nit, String negocio);

    /**
     *
     * @param cuentas
     * @param periodo_inicio
     * @param periodo_fin
     * @return
     */
    public String cargarInfoMovimientoAuxiliar(String cuentas, String periodo_inicio, String periodo_fin);

    /**
     *
     * @param informacion_
     * @param parametro_busqueda
     * @return
     */
    public String buscarCliente(String informacion_, String parametro_busqueda);

    public String cargarConcepto();

    public String cargarTipoIngreso();

    public String cargarHcNombres();

    public String cagarMoneda();

    public JsonObject getDocumentosRequeridos(String linea_negocio, String solicitud) throws Exception;

    public String buscarNegocio(String negocio);

    /**
     *
     * @param info
     * @param usuario
     * @return
     */
    public String guardarDatosIngresos(JsonObject info, Usuario usuario);

    /**
     *
     * @param fechaInicio
     * @param fechaFin
     * @return
     */
    public String cargarPagosSinReferencia(String fechaInicio, String fechaFin);

    /**
     *
     * @param parametros
     * @return
     */
    public String cargarFintraSoluciones(ParmetrosDinamicaTSP parametros);

    public String cargarModulos();

    /**
     *
     * @param informacion
     * @return
     */
    public String eliminarMultipleConfiguracionCuentasDC(JsonObject informacion);

    /**
     *
     * @param cliente
     * @param negocio
     * @return
     */
    public String cargarPazYSalvo(String cliente, String negocio);

    /**
     *
     * @param negocio
     * @param fecha_max
     * @param general
     * @return
     */
    public String cargarLiquidacionDetalle(String negocio, String fecha_max, String general);

    /**
     *
     * @param info
     * @param usuario
     * @return
     */
    public String datosPdfPazYSalvo(JsonObject info, Usuario usuario);


    /**
     *
     * @param asesor
     * @return
     */
    public String buscarAsesor(String asesor);

    /**
     *
     * @param asesor
     * @param fecha_inicio
     * @param fecha_fin
     * @param estadoCartera
     * @return
     */
    public String buscarNegocios(String asesor, String fecha_inicio, String fecha_fin, String estadoCartera);

    public String cargarAsesores(String id_combo,String producto);

    public String actualizarNegocios(JsonObject info, Usuario usuario);
    
    /**
     *
     * @param numero_solicitud
     * @return
     */
    public String cargarObservaciones(String numero_solicitud);

    /**
     *
     * @param numero_solicitud
     * @param observaciones
     * @param usuario
     * @return
     */
    public String guardarObservaciones(String numero_solicitud,String observaciones, Usuario usuario);
   
    public String cagarAnalisisRecaudo(String banco, String fecha_inicio, String fecha_fin);

    public String cagarBarrios(String ciudad);

    public String cargarDetalleNegocioLiquidacion(String negocio);

    public String cargarFacturasCausacionIntereses(String periodo, String cmc);

    public String causarInteresesMultiservicio(String periodo, String cmc, String cta_ingreso, Usuario usuario);

    public String cargarCmcCausacionInteresesMS();

    public String cargar_negocio_rechazo(String negocio);

    public String rechazarNegocio(String negocio, Usuario usuario);

    public String cargar_convenios();

    public String extracto_estado_cuenta(Usuario usuario, JsonObject informacion);

    public String exportarPdfEstadoCuenta(Usuario usuario, JsonObject informacion);

    public String cargarCompobanteCausacion(String comprobanteGenerado);

    public String cargaPresolicitudesMicrocredito(String fechaInicio, String fechaFin, String identificacion, String numero_solicitud, String estado_solicitud, String etapa, String r_operaciones, String unidadNegocio);

    public Object searchNombresArchivos(String rutaOrigen, String num_solicitud);

    public boolean almacenarArchivoEnCarpetaUsuario(String num_solicitud, String rutaOrigen, String rutaDestino, String nomarchivo);

    public String rechazarnegociomicrocredito(Usuario usuario, String numero_solicitud);

    public String cargarnegociosdesistir(String cod_neg, String identificacion);
    
    public void updateFilesAppMIcro(String document,String id_tipo_archivo, Usuario user) ;
    
    public  ArrayList<ArchivosAppMicro> buscarArchivosCargadosApp(String document,String und_negocio);  

    public String standbypresolicitudes(Usuario usuario, String numero_solicitud);

    public String devolverSolicitud(Usuario usuario, String numero_solicitud);

    public String standbytrazabilidadpresolicitudes(Usuario usuario, String numero_solicitud, String causal, String estado);

    public String devolverSolicitudtrazabilidad(Usuario usuario, String numero_solicitud);

    public String consultarCausalStandBy(String numero_solicitud);

    public JsonObject getInfoCuentaEnvioCorreo();

    public String getUnidadConvenio(String numero_solicitud);

    public String getNombreCliente(String numero_solicitud);

    public String getCorreosToSendStandBy(String receiversType);

   public String cargarEntidadesPresolicitud();
   
   public String cargarPresolicitudesMicroRechazadas();
}
