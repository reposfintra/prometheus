/*
 * Codigo_discrepanciaDAO.java
 *
 * Created on 27 de junio de 2005, 11:59 PM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Jose
 */
public class Codigo_discrepanciaDAO {
    private Codigo_discrepancia codigo_discrepancia;
    private Vector codigo_discrepancias;    
    /** Creates a new instance of Codigo_discrepanciaDAO */
    public Codigo_discrepanciaDAO() {
    }
    public Codigo_discrepancia getCodigo_discrepancia() {
        return codigo_discrepancia;
    }
    
    public void setCodigo_discrepancia(Codigo_discrepancia codigo_discrepancia) {
        this.codigo_discrepancia = codigo_discrepancia;
    }
    
    public Vector getCodigo_discrepancias() {
        return codigo_discrepancias;
    }
    
    public void setCodigo_discrepancias(Vector codigo_discrepancias) {
        this.codigo_discrepancias = codigo_discrepancias;
    }
    
    public void insertCodigo_discrepancia() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("Insert into codigo_discrepancia (cod_cdiscrepancia,desc_cdiscrepancia,creation_user,creation_date,user_update,last_update,cia,rec_status,base) values(?,?,?,'now()',?,'now()',?,' ',?)");
                st.setString(1,codigo_discrepancia.getCodigo());
                st.setString(2,codigo_discrepancia.getDescripcion());
                st.setString(3,codigo_discrepancia.getCreation_user());
                st.setString(4,codigo_discrepancia.getUser_update());
                st.setString(5,codigo_discrepancia.getCia());
                st.setString(6,codigo_discrepancia.getBase());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL CODIGO DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void searchCodigo_discrepancia(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from codigo_discrepancia where cod_cdiscrepancia=? and rec_status != 'A'");
                st.setString(1,cod);
                rs= st.executeQuery();
                codigo_discrepancias = new Vector();
                while(rs.next()){
                    codigo_discrepancia = new Codigo_discrepancia();
                    codigo_discrepancia.setCodigo(rs.getString("cod_cdiscrepancia"));
                    codigo_discrepancia.setDescripcion(rs.getString("desc_cdiscrepancia"));
                    codigo_discrepancias.add(codigo_discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL CODIGO DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public boolean existCodigo_discrepancia(String cod) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select * from codigo_discrepancia where cod_cdiscrepancia=? and rec_status != 'A'");
                st.setString(1,cod);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DEL CODIGO DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public void listCodigo_discrepancia()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from codigo_discrepancia where rec_status != 'A' order by cod_cdiscrepancia");
                rs= st.executeQuery();
                codigo_discrepancias = new Vector();
                while(rs.next()){
                    codigo_discrepancia = new Codigo_discrepancia();
                    codigo_discrepancia.setCodigo(rs.getString("cod_cdiscrepancia"));
                    codigo_discrepancia.setDescripcion(rs.getString("desc_cdiscrepancia"));
                    codigo_discrepancia.setBase(rs.getString("base"));
                    codigo_discrepancias.add(codigo_discrepancia);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL CODIGO DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void updateCodigo_discrepancia(String cod, String desc, String usu) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update codigo_discrepancia set desc_cdiscrepancia=?, last_update='now()', user_update=?, rec_status=' ' where cod_cdiscrepancia = ?");
                st.setString(1,desc);
                st.setString(2,usu);
                st.setString(3,cod);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DEL CODIGO DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
   
    public void anularCodigo_discrepancia() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update codigo_discrepancia set rec_status='A',last_update='now()', user_update=? where cod_cdiscrepancia = ?");
                st.setString(1,codigo_discrepancia.getUser_update());
                st.setString(2,codigo_discrepancia.getCodigo());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL CODIGO DE LA DISCREPANCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public void searchDetalleCodigo_discrepancias(String cod, String desc, String base) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        codigo_discrepancias=null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                
                st = con.prepareStatement("Select * from codigo_discrepancia where cod_cdiscrepancia like ? and desc_cdiscrepancia like ? and base like ? and rec_status != 'A'");
                st.setString(1, cod+"%");
                st.setString(2, desc+"%");
                st.setString(3, base+"%");
                
                ////System.out.println(st.toString());
                
                rs = st.executeQuery();
                codigo_discrepancias = new Vector();
                while(rs.next()){
                    codigo_discrepancia = new Codigo_discrepancia();
                    codigo_discrepancia.setCodigo(rs.getString("cod_cdiscrepancia"));
                    codigo_discrepancia.setDescripcion(rs.getString("desc_cdiscrepancia"));
                    codigo_discrepancia.setRec_status(rs.getString("rec_status"));
                    codigo_discrepancia.setUser_update(rs.getString("user_update"));
                    codigo_discrepancia.setCreation_user(rs.getString("creation_user"));
                    codigo_discrepancia.setBase(rs.getString("base"));
                    codigo_discrepancias.add(codigo_discrepancia);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CODIGOS DE LA DISCREPANCIAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public Vector listarCodigo_discrepancias ()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        codigo_discrepancias = null;
        PoolManager poolManager = null;
        
        try {            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select * from codigo_discrepancia where rec_status!='A' order by cod_cdiscrepancia");
                                
                rs = st.executeQuery();
                codigo_discrepancias = new Vector();
                
                while(rs.next()){
                    codigo_discrepancia = new Codigo_discrepancia();
                    codigo_discrepancia.setCodigo(rs.getString("cod_cdiscrepancia"));
                    codigo_discrepancia.setDescripcion(rs.getString("desc_cdiscrepancia"));
                    codigo_discrepancias.add(codigo_discrepancia);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS CODIGOS DE LA DISCREPANCIAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return codigo_discrepancias;
    }
    
}
