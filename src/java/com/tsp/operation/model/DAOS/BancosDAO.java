package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.Bancos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * DAO para la tabla de bancos
 * @author darrieta - geotech
 */
public class BancosDAO extends MainDAO {
    
    public BancosDAO(){
        super("BancosDAO.xml");
    }
    public BancosDAO(String dataBaseName){
        super("BancosDAO.xml", dataBaseName);
    }

    /**
     * Obtiene los registros no anulados de la tabla bancos
     * @return ArrayList<Bancos> con los resultados obtenidos
     * @throws Exception 
     */
   /* public ArrayList<Bancos> obtenerBancosNit() throws Exception {
        ArrayList<Bancos> lista = new ArrayList<Bancos>();
        Bancos bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_BANCOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while (rs.next()) {
                bean = new Bancos();
                bean.setCodigo(rs.getString("codigo"));
                bean.setNombre(rs.getString("nombre"));
                bean.setNit(rs.getString("nit"));
                bean.setBaseAno(rs.getInt("base_ano"));
                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en obtenerBancos en BancosDAO.java: " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }*/

     
     /**
     * Obtiene los registros no anulados de la tabla bancos
     * @return ArrayList<Bancos> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<Bancos> obtenerBancosNit() throws Exception {
        ArrayList<Bancos> lista = new ArrayList<Bancos>();
        Bancos bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_BANCOS_NIT";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while (rs.next()) {
                bean = new Bancos();
                bean.setCodigo(rs.getString("codigo"));
                bean.setNombre(rs.getString("nombre"));
                bean.setNit(rs.getString("nit"));
                bean.setBaseAno(rs.getInt("base_ano"));
                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en obtenerBancosNit en BancosDAO.java: " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }
    
    /**
     * Obtiene los registros de la tabla bancos de acuerdo a la nacionalidad
     * @param nacionales true si se quiere buscar bancos nacionales o false si se quiere buscar extranjeros
     * @return ArrayList<Bancos> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<Bancos> obtenerBancosNacionalidad(boolean nacionales) throws Exception {
        ArrayList<Bancos> lista = new ArrayList<Bancos>();
        Bancos bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_BANCOS_PAIS";
        try {
            con = this.conectarJNDI(query);
            String pais = "n.codpais='CO'";
            if(!nacionales){
                pais = "n.codpais!='CO'";
            }
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#PAIS#", pais));
            rs = st.executeQuery();
            while (rs.next()) {
                bean = new Bancos();
                bean.setCodigo(rs.getString("codigo"));
                bean.setNombre(rs.getString("nombre"));
                bean.setNit(rs.getString("nit"));
                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en obtenerBancosNacionalidad en BancosDAO.java: " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }
    
    /**
     * Obtiene un banco de acuerdo a su nit
     * @param nit
     * @return bean con los datos del banco
     * @throws Exception 
     */
    public Bancos obtenerBancoXNit(String nit) throws Exception {
        Bancos bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_BANCO_X_NIT";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, nit);
            rs = st.executeQuery();
            if (rs.next()) {
                bean = new Bancos();
                bean.setCodigo(rs.getString("codigo"));
                bean.setNombre(rs.getString("nombre"));
                bean.setNit(rs.getString("nit"));
                bean.setBaseAno(rs.getInt("base_ano"));
            }
        } catch (Exception e) {
            throw new Exception("Error en obtenerBancosXNit en BancosDAO.java: " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return bean;
    }
    
      /**
     * Obtiene los registros no anulados de la tabla bancos
     * @return ArrayList<Bancos> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<Bancos> obtenerBancos() throws Exception {
        ArrayList<Bancos> lista = new ArrayList<Bancos>();
        Bancos bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_BANCOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            while (rs.next()) {
                bean = new Bancos();
                bean.setCodigo(rs.getString("codigo"));
                bean.setNombre(rs.getString("nombre"));
                bean.setNit(rs.getString("nit"));
                bean.setBaseAno(rs.getInt("base_ano"));
                lista.add(bean);
            }
        } catch (Exception e) {
            throw new Exception("Error en obtenerBancos en BancosDAO.java: " + e.toString());
        } finally {
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return lista;
    }

}
