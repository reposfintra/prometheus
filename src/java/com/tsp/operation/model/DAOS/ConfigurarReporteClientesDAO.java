/*
 * Nombre        ConfigurarReporteClientesDAO.java
 * Descripci�n   Clase para el acceso a los datos de las configuraciones de reportes
 * Autor         Alejandro Payares
 * Fecha         10 de agosto de 2005, 07:19 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.xml.*;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import com.tsp.operation.model.RemiDestDAO;


/**
 * Clase para el acceso a los datos de las configuraciones de reportes
 * @autor Alejandro Payares
 */
public class ConfigurarReporteClientesDAO extends MainDAO {
    
    /**
     * Vector para almacenar los datos de las diferentes configuraciones de reportes existentes
     */    
    private Vector datosReporte;
    
    /**
     * TreeMap usado para cargar los usuarios del sistema
     */    
    private TreeMap usuarios;
    
    /**
     * TreeMap para cargar los diferentes reportes del sistema
     */    
    private TreeMap reportes;
    
    /**
     * Objeto para agregar mensajes al log
     */    
    private Logger logger = Logger.getLogger(ConfigurarReporteClientesDAO.class);
    
    /**
     * Creates a new instance of CreacionEquipoDAO
     * @autor Alejandro Payares
     */
    public ConfigurarReporteClientesDAO() {
        super("ConfigurarReporteClientesDAO.xml");
    }
    
    

    
    
    /**
     * Busca una configuraci�n de reporte especifica dentro de las encontradas por el metodo
     * generarDatosReporte.
     * @param datos Las configuraciones de reportes encontrados por el metodo generarDatosReporte
     * @param codigoCli El codigo de cliente o usuario a buscar
     * @return La configuraci�n de reporte encontrada, null si no encuentra ninguna
     * @autor Alejandro Payares
     */    
    private ConfigReporte buscarConfigReporte(Vector datos,String codigoCli){
        for( int i=0; i<datos.size(); i++ ){
            ConfigReporte cf = (ConfigReporte) datos.elementAt(i);
            if ( cf.equals(codigoCli) ){
                return cf;
            }
        }
        return null;
    }
    
    /**
     * Elimina una configuraci�n de reporte del sistema
     * @param datosConfigReportes Un array que debe contener 2 valores:
     * - en la posici�n 0 el codigo del cliente o usuario.
     * - en la posici�n 1 el codigo del reporte a eliminar.
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */    
    public void eliminarConfigReporte(String [] datosConfigReportes) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try{
            ps = this.crearPreparedStatement("SQL_ELIMINAR_CONF_REPORTES");
            for( int i=0; i<datosConfigReportes.length; i++ ){
                String [] datos = datosConfigReportes[i].split("/");
                ps.setString(1, datos[0]);//codclidest
                ps.setString(2, datos[1]);//codreporte
                ps.executeUpdate();
                ps.clearParameters();
            }
        }
        catch(SQLException e){
            throw new SQLException("CONFIGURAR REPORTES DE CLIENTES: ERROR ELIMINANDO CONFIGURACIONES DE REPORTE -> " + e.getMessage());
        }
        finally{
            if (ps != null) try { ps.close(); }catch(SQLException e){}
            desconectar("SQL_ELIMINAR_CONF_REPORTES");
        }
    }
    
    /**
     * Devuelve los usuarios encontrados por el metodo buscarUsuarios
     * @return El treeMap de usuarios
     * @autor Alejandro Payares
     */    
    public TreeMap getUsuarios(){
        if ( usuarios == null ) {
            return new TreeMap();
        }
        return usuarios;
    }
    
    
    /**
     * Busca y guarda en un treeMap los usuarios que puede ver el usuario en sesi�n.
     * @param tipo El tipo de usuario
     * @param loggedUser El usuario en sesi�n
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */    
    public void buscarUsuarios(String tipo, Usuario loggedUser)throws SQLException {
        if ( tipo.equals("Cliente") ) {
            ClienteDAO cdao = new ClienteDAO();
            cdao.listar();
            usuarios = cdao.getCbxCliente();
        }
        else if ( tipo.equals("Usuario") ) {
            UsuarioDAO udao = new UsuarioDAO();
            udao.usuariosCreadosSearch(loggedUser,true);
            usuarios = udao.getTusuarios();
        }
        else if ( tipo.equals("Destinatario") ) {
            RemiDestDAO rdao = new RemiDestDAO();
            rdao.destinatarioSearch(loggedUser.getClienteDestinat(),true);
            usuarios = rdao.getTgetDestinatarios();
        }
    }
    
    /**
     * Devuelve los reportes encontrados por el metodo buscarReportes
     * @return El TreeMap con los reportes encontrados
     * @autor Alejandro Payares
     */    
    public TreeMap obtenerReportes(){
        return reportes;
    }
    
    /**
     * Busca y guarda en un TreeMap los diferentes reportes existentes en el sistema.
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */    
    public void buscarReportes() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        reportes = null;
        try{
            ps = this.crearPreparedStatement("SQL_OBTENER_REPORTES");
            rs = ps.executeQuery();
            if ( rs.next() ) {
                reportes = new TreeMap();
                do {
                    reportes.put(rs.getString("nombrerpt"),rs.getString("codigorpt"));
                }while( rs.next() );
            }
        }
        catch( Exception ex ){
            logger.error("ERROR AL OBTENER REPORTES: "+ex.getMessage(),ex);
            throw new SQLException("ERROR AL OBTENER REPORTES: "+ex.getMessage());
        }
        finally {
            if (rs != null) rs.close();
            if ( ps != null ) ps.close();
            this.desconectar("SQL_OBTENER_REPORTES");
        }
    }
    
    /**
     * Getter for property datosReporte.
     * @return Value of property datosReporte.
     * @autor Alejandro Payares
     */
    public java.util.Vector getDatosReporte() {
        return datosReporte;
    }
    
    /**
     * Setter for property datosReporte.
     * @param datosReporte New value of property datosReporte.
     * @autor Alejandro Payares
     */
    public void setDatosReporte(java.util.Vector datosReporte) {
        this.datosReporte = datosReporte;
    }
    
    /*Correcion conexiones  DAOS*/
        /**
     * Busca los datos de las diferentes configuraciones de reporte
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */   
    public void generarDatosReporte() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
         try{
            ps = this.crearPreparedStatement("SQL_REPORTES_DE_CLIENTES");
            rs = ps.executeQuery();
            Vector datos = new Vector();
            
            while( rs.next() ){
                ConfigReporte encontrado = buscarConfigReporte(datos,rs.getString("codcli"));
                if ( encontrado == null ){
                    encontrado = new ConfigReporte();
                    encontrado.setCodigoCliente(rs.getString("codcli"));
                    encontrado.setNombreCliente(rs.getString("nomcli"));
                    datos.addElement(encontrado);
                }
                encontrado.agregarReporte(rs.getString("codreporte"), rs.getString("nombrerpt"), rs.getString("campos"), rs.getString("nomcamposrpt"));
            }
            rs.close();
            ps.close();
            ps = this.crearPreparedStatement("SQL_REPORTES_DE_USUARIOS");
            rs = ps.executeQuery();
            while( rs.next() ){
                ConfigReporte encontrado = buscarConfigReporte(datos,rs.getString("idusuario"));
                if ( encontrado == null ){
                    encontrado = new ConfigReporte();
                    encontrado.setCodigoCliente(rs.getString("idusuario"));
                    encontrado.setNombreCliente(rs.getString("nombre"));
                    datos.addElement(encontrado);
                }
                encontrado.agregarReporte(rs.getString("codreporte"), rs.getString("nombrerpt"), rs.getString("campos"), rs.getString("nomcamposrpt"));
            }
            this.setDatosReporte(datos);
        }
        catch(SQLException e){
            throw new SQLException("CONFIGURAR REPORTES DE CLIENTES: ERROR GENERANDO LOS DATOS DEL REPORTE -> " + e.getMessage());
        }
        finally{
            if (ps != null) try { ps.close(); }catch(SQLException e){}
            desconectar("SQL_REPORTES_DE_CLIENTES");
            desconectar("SQL_REPORTES_DE_USUARIOS");
        }
    }
    

    
     public void mostrarDatos() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Sql sql = null;
        
        try{
            Connection con = conectar("SQL_REPORTES_DE_CLIENTES");
            rs = con.getMetaData().getSuperTypes(null,null,null);
            while ( rs.next() ){
                ////System.out.println("TIPO = "+rs.getString("TYPE_NAME"));
            }
            if ( 3 > 2 ) {
                return;
            }
            /*ps = con.prepareStatement("select nombre,idusuario,claveencr from usuarios");
            rs = ps.executeQuery();
            while(rs.next()){
                ////System.out.println("nombre: "+rs.getString("nombre")+", id: "+rs.getString("idusuario")+", clave: "+com.tsp.util.Util.unencript("d5C9A2bf",rs.getString("claveencr")));
            }*/
            
            ps = con.prepareStatement("select * from remesa_docto where tipo_doc_rel <> '' and documento_rel <> '' order by tipo_doc, documento");
            rs = ps.executeQuery();
            StringBuffer sb = new StringBuffer();
            String padreAnterior = null;
            while(rs.next()){
                String aux = rs.getString("tipo_doc")+":"+rs.getString("documento");
                if ( padreAnterior == null || !padreAnterior.equals(aux)){
                    if ( padreAnterior != null ){
                        sb.append("]");
                        sb.append("/");
                    }
                    sb.append(aux);
                }
                else {
                    sb.append(";");
                }
                
                // hijos
                if ( padreAnterior == null || !padreAnterior.equals(aux) ){
                    sb.append("[");
                }
                sb.append(rs.getString("tipo_doc_rel"));
                sb.append(":");
                sb.append(rs.getString("documento_rel"));
                padreAnterior = aux;
            }
            sb.append("]");
            
            ////System.out.println("cadena formato: "+sb);
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("CONFIGURAR REPORTES DE CLIENTES: ERROR ELIMINANDO CONFIGURACIONES DE REPORTE -> " + e.getMessage());
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        finally{
            if (ps != null) try { ps.close(); }catch(SQLException e){}
            desconectar("SQL_REPORTES_DE_CLIENTES");
        }
        
    }
    
    /**
     * Salva una configuraci�n de reporte para un cliente / destinatario dado.
     * @param rptSetup Configuraci�n de reporte que se va a salvar.
     *        rptSetup[0] = C�d. del cliente / destinatario.<BR>
     *        rptSetup[1] = C�d. del reporte.<BR>
     *        rptSetup[2] = Lista de campos configurados.<BR>
     *        rptSetup[3] = Tipo de usuario (cliente o destinatario)
     * @throws SQLException Si un error de base de datos ocurre.
     */
    public void salvarConfigReporte(String [] rptSetup) throws SQLException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            // Verificar que existe una configuraci�n para el reporte
            // asociada al cliente / destinatario pasado como par�metro.
            ps = this.crearPreparedStatement("SQL_EXISTE_CONF");
            ps.setString(1, rptSetup[0] );
            ps.setString(2, rptSetup[1] );
            rs = ps.executeQuery();
            String query = null;
            Level oldLevel = logger.getLevel();
            logger.setLevel(Level.DEBUG);
            logger.debug(
            "Nueva configuracion de campos ('" +
            rptSetup[0] + "'; '" +
            rptSetup[1] + "'; '" +
            rptSetup[2] + "'; '" +
            rptSetup[3] + "')"
            );
            if( rs.next() ) {
                ps.close();
                logger.debug("Actualizando configuracion de campos de un reporte para un usuario.");
                ps = this.crearPreparedStatement("SQL_ACTUALIZAR_CONF");
                ps.setString(1, rptSetup[2] );
                ps.setString(2, rptSetup[0] );
                ps.setString(3, rptSetup[1] );
            }
            else {// Si no, insertar la nueva configuraci�n de reporte.
                ps.close();
                logger.debug("Insertando nueva configuracion de campos de un reporte para un usuario.");
                ps = this.crearPreparedStatement("SQL_NUEVA_CONF");
                ps.setString(1,rptSetup[0]);
                ps.setString(2,rptSetup[1]);
                ps.setString(3,rptSetup[2]);
                ps.setString(4,rptSetup[3]);
            }
            logger.setLevel(oldLevel);
            ////System.out.println("query: "+ps);
            ps.executeUpdate();
        } finally {
            if (rs != null) rs.close();
            if ( ps != null ) ps.close();
            this.desconectar("SQL_EXISTE_CONF");
            this.desconectar("SQL_ACTUALIZAR_CONF");
            this.desconectar("SQL_NUEVA_CONF");
        }
    }
    
    
}
