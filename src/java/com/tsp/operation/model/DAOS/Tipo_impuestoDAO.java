/***************************************************************************
 *      Nombre Clase.................   Tipo_impuestoDAO.java
 *      Descripci�n..................   DAO del archivo tipo_de_impuesto
 *      Autor........................   Ing. Juan Manuel Escand�n
 *      Fecha........................   27 de septiembre de 2005, 10:04 AM
 *      Versi�n......................   1.1
 *      Versi�n XML DAO..............   Ing. Andr�s Maturana De La Cruz
 *      Copyright....................   Transportes Sanchez Polo S.A.
 ***************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
import org.apache.log4j.*;
/**
 *
 * @author  JuanM
 */
public class Tipo_impuestoDAO extends MainDAO {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    public Vector impuestos = new Vector();
    
    /** Creates a new instance of Tipo_impuestoDAO */
    public Tipo_impuestoDAO() {
        super("Tipo_impuestoDAO.xml");
    }
    
    public Tipo_impuestoDAO(String dataBaseName) {
        super("Tipo_impuestoDAO.xml", dataBaseName);
    }
    
    /*INSERT*/  
        
    
    /**
     * Ingresa un registro en el archivotipo_de_impuesto de tipo ICA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param concepto Concepto
     * @param descripcion Descripci�n
     * @param fecha_vigencia Fecha de vigencia
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cod_cuenta_contable C�digo de la cuenta contable
     * @param agencia C�digo de la agencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void INSERTRICA( String dstrct, String codigo_impuesto, String tipo_impuesto, String concepto, String descripcion, String fecha_vigencia, double porcentaje1, String cod_cuenta_contable, String agencia, String login, String signo )  throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_INSERTRICA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, tipo_impuesto);
            st.setString(4, concepto);
            st.setString(5, descripcion);
            st.setString(6,fecha_vigencia);
            st.setDouble(7, porcentaje1);
            st.setString(8, cod_cuenta_contable);
            st.setString(9, agencia);
            st.setString(10, login);
            st.setString(11, signo);
            
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_RICA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                  if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /**
     * Ingresa un registro en el archivotipo_de_impuesto de tipo RFTE.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param concepto Concepto
     * @param fecha_vigencia Fecha de vigencia
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cod_cuenta_contable C�digo de la cuenta contable
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void INSERTRFTE( String dstrct, String codigo_impuesto, String tipo_impuesto, String concepto, String descripcion, String fecha_vigencia, double porcentaje1, String cod_cuenta_contable, String login, String signo)  throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_INSERTRFTE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, tipo_impuesto);
            st.setString(4, concepto);
            st.setString(5, descripcion);
            st.setString(6,fecha_vigencia);
            st.setDouble(7, porcentaje1);
            st.setString(8, cod_cuenta_contable);
            st.setString(9, login);
            st.setString(10, signo);
            
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_RFTE [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /*FIN INSERT*/
    
    /*UPDATEESTADOS*/
        
    /**
     * Actualiza el estado de un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param reg_status Estado del registro
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEESTADOIVA(String reg_status, String dstrct, String codigo_impuesto,  String fecha_vigencia, String login ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATERIVA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, reg_status);
            st.setString(2, login);
            st.setString(3, dstrct);
            st.setString(4, codigo_impuesto);
            st.setString(5, fecha_vigencia);
            
            st.executeUpdate();
            }} catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEESTADOIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /**
     * Actualiza el estado de un registro en el archivotipo_de_impuesto de tipo RICA.
     * @autor Ing. Juan Manuel Escand�n
     * @param reg_status Estado del registro
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param concepto Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEESTADORICA(String reg_status, String dstrct, String codigo_impuesto,  String fecha_vigencia, String agencia, String concepto, String login ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATERRICA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, reg_status);
            st.setString(2, login);
            st.setString(3, dstrct);
            st.setString(4, codigo_impuesto);
            st.setString(5, fecha_vigencia);
            st.setString(6, agencia);
            st.setString(7, concepto);
            
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEESTADORICA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /**
     * Actualiza el estado de un registro en el archivotipo_de_impuesto de tipo RFTE.
     * @autor Ing. Juan Manuel Escand�n
     * @param reg_status Estado del registro
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param concepto Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEESTADORFTE(String reg_status, String dstrct, String codigo_impuesto,  String fecha_vigencia, String concepto, String login ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATERRFTE";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, reg_status);
            st.setString(2, login);
            st.setString(3, dstrct);
            st.setString(4, codigo_impuesto);
            st.setString(5, fecha_vigencia);
            st.setString(6, concepto);
            
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEESTADORFTE [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /*FIN UPDATEESTADOS*/
    
    /*UPDATE*/
    
    /**
     * Actualiza un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo Nuevo c�digo del impuesto
     * @param fechav Nueva fecha de vigencia
     * @param concepto Nuevo concepto
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cuenta C�digo de la cuenta contable
     * @param codigoi C�digo del impuesto
     * @param fv Fecha de vigencia
     * @param concept Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEIVA(String tipo, String codigo, String concepto, String fechav, String porcentaje1, String porcentaje2, String cuenta, String codigoi, String fv, String concep, String login  ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATEIVA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo);
            st.setString(2, codigo);
            st.setString(3, concepto);
            st.setString(4, fechav);
            st.setString(5, porcentaje1);
            st.setString(6, porcentaje2);
            st.setString(7, cuenta);
            st.setString(8, login);
            st.setString(9, codigoi);
            st.setString(10, fv);
            st.setString(11, concep);
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /**
     * Actualiza un registro en el archivotipo_de_impuesto de tipo RICA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo Nuevo c�digo del impuesto
     * @param fechav Nueva fecha de vigencia
     * @param concepto Nuevo concepto
     * @param descripcion Descripci�n
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cuenta C�digo de la cuenta contable
     * @param codigoi C�digo del impuesto
     * @param fv Fecha de vigencia
     * @param concept Concepto
     * @param agencia C�digo de la agencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATERICA(String tipo, String codigo, String concepto, String descripcion, String fechav, String porcentaje1, String cuenta, String ag, String codigoi, String fv, String concep, String agencia, String login, String signo  ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATERICA";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo);
            st.setString(2, codigo);
            st.setString(3, concepto);
            st.setString(4, descripcion);
            st.setString(5, fechav);
            st.setString(6, porcentaje1);
            st.setString(7, cuenta);
            st.setString(8, ag);
            st.setString(9, login);
            st.setString(10, signo);
            st.setString(11, codigoi);
            st.setString(12, fv);
            st.setString(13, concep);
            st.setString(14, agencia);
            logger.info("UPDATE RICA: " + st);
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    
    /**
     * Actualiza un registro en el archivotipo_de_impuesto de tipo RFTE.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo Nuevo c�digo del impuesto
     * @param fechav Nueva fecha de vigencia
     * @param concepto Nuevo concepto
     * @param descripcion Descripci�n
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cuenta C�digo de la cuenta contable
     * @param codigoi C�digo del impuesto
     * @param fv Fecha de vigencia
     * @param concept Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATERFTE(String tipo, String codigo, String concepto, String descripcion, String fechav, String porcentaje1, String cuenta, String codigoi, String fv, String concep, String login, String signo  ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATERFTE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo);
            st.setString(2, codigo);
            st.setString(3, concepto);
            st.setString(4, descripcion);
            st.setString(5, fechav);
            st.setString(6, porcentaje1);
            st.setString(7, cuenta);
            st.setString(8, login);
            st.setString(9, signo);
            st.setString(10, codigoi);
            st.setString(11, fv);
            st.setString(12, concep);
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /*DELETE*/
        
    /**
     * Elimina un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void DELETEIVA(String dstrct, String codigo_impuesto, String fecha_vigencia ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_DELETEIVA";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, fecha_vigencia);
            
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina DELETEIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /**
     * Elimina un registro en el archivotipo_de_impuesto de tipo RICA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param agencia C�digo de la agencia
     * @param concepto Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void DELETERICA(String dstrct, String codigo_impuesto,  String fecha_vigencia, String agencia, String concepto ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_DELETERICA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, fecha_vigencia);
            st.setString(4, agencia);
            st.setString(5, concepto);
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina DELETERICA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /**
     * Elimina un registro en el archivotipo_de_impuesto de tipo RFTE.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void DELETERFTE(String dstrct, String codigo_impuesto,  String fecha_vigencia,  String concepto ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_DELETERFTE";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, fecha_vigencia);
            st.setString(4, concepto);
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina DELETERFTE [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /*FIN DELETE*/
    //Modificacion Juan 17-ene-2006
    /**
     * Elimina un registro en el archivo tipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tp Tipo de impuestp
     * @param ag C�digo de la agencia
     * @param concepto Concepto
     * @return Lista de los registro obtenidos del archivo tipo_de_impuesto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public List LIST(String tp, String ag, String concepto) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        String query = "SQL_LISTALL";//JJCastro fase2
        String query2 = "SQL_LIST";//JJCastro fase2
        String sql = "";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
//                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

            ag       = (ag.equals(""))?"%":ag;
            concepto = (concepto.equals(""))?"%":concepto;
            
            sql = (tp.equals("ALL") || tp.equals(""))?
                query : query2;

            st = con.prepareStatement(this.obtenerSQL(sql)); //JJCastro fase2

            if(!tp.equals("ALL") && !tp.equals("")){
                st.setString( 1, tp);
                st.setString( 2, ag);
                st.setString( 3, concepto );
            }
            ////System.out.println("LISTADO " + st);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(Tipo_impuesto.loadRFTE(rs));
                }
            }
        }} catch(Exception e){
            throw new SQLException("Error en rutina LIST [Tipo_impuestoDAO].... \n"+ e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return lista;
    }
    
    /**
     * Obtiene los registros del archivo tipo_de_impuesto por el tipo.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo  C�digo del impuesto
     * @param fecha Fecha de vigencia
     * @param concepto Concepto
     * @param agencia C�digo de la agencia
     * @return Lista de los registro obtenidos del archivo tipo_de_impuesto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public Tipo_impuesto SEARCH_TI(String dstrct, String codigo, String fecha, String concepto, String agencia ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        Tipo_impuesto datos = null;
        String query = "SQL_SEARCHTI";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, codigo);
            st.setString(3, fecha);
            st.setString(4, concepto);
            st.setString(5, agencia);
            rs = st.executeQuery();
            while(rs.next()){
                datos = Tipo_impuesto.loadRFTE(rs);
                break;
            }
        }} catch(Exception e) {
            throw new SQLException("Error en rutina SEARCH_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return datos;
    }
    
    /**
     * Obtiene los registros del archivo tipo_de_impuesto mediante filtros especificos.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo  C�digo del impuesto
     * @return Lista de los registro obtenidos del archivo tipo_de_impuesto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public Tipo_impuesto SEARCH(String dstrct, String tipo, String codigo ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        Tipo_impuesto datos = null;
        String query = "SQL_SEARCH";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, tipo);
            st.setString(3, codigo);
            rs = st.executeQuery();
            while(rs.next()){
                datos = Tipo_impuesto.loadRFTE(rs);
                break;
            }
        }}catch(Exception e) {
            throw new SQLException("Error en rutina SEARCH_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return datos;
    }
    
    //David 23.12.05
    /**
     * Este m�todo genera un vector con los tipos de impuestos  
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor Ing. David Lamadrid
     * @version 1.0
     */
    public Vector vTiposImpuestos() throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        Tipo_impuesto datos = null;
        Vector v=new Vector();
        String query = "SQL_TIPOS_DE_IMPUESTOS";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()) {
                    v.add("" + rs.getString("tipo_impuesto"));
                }
            }
        } catch(Exception e) {
            throw new SQLException("Error en rutina SEARCH_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return v;
    }
    
     /**
     * Este m�todo genera un boolean true con si existe un impuesto dado el codig ,y la fecha de actualizacion y false si no existe  
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @param codigoImpuesto C�digo del impuesto
     * @param fecha Fecha de vigencia
     * @autor Ing. David Lamadrid
     * @version 1.0
     */
    public boolean  existeImpuestoPorCodigo(String codigoImpuesto,String tipoImpuesto, String fecha, String agencia) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        Tipo_impuesto datos = new  Tipo_impuesto();
        boolean sw = false;
        String query = "SQL_EXISTE_IMPUESTO_POR_CODIGO";//JJCastro fase2
        try {
            agencia = agencia.equals("OP")?"BQ":agencia;
            agencia = tipoImpuesto.equals("RICA")?agencia:"";
           
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codigoImpuesto);
            st.setString(2, tipoImpuesto);
            st.setString(3, fecha);
            
            rs = st.executeQuery();            
            while(rs.next()){
               sw=true;
            }
        }
        }catch(Exception e) {
            throw new SQLException("Error en rutina existeImpuestoPorCodigo [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return sw;
    } 

    
    
    
    /* RIVA */
    
    /**
     * Ingresa un registro en el archivotipo_de_impuesto de tipo RIVA.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param concepto Concepto
     * @param fecha_vigencia Fecha de vigencia
     * @param porcentaje1 Porcentaje 1
     * @param cod_cuenta_contable C�digo de la cuenta contable
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void INSERTRIVA(String dstrct, String codigo_impuesto , String tipo_impuesto, String concepto, String fecha_vigencia, double porcentaje1, String cod_cuenta_contable, String login, String signo) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_INSERTRIVA";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, tipo_impuesto);
            st.setString(4, concepto);
            st.setString(5,fecha_vigencia);
            st.setDouble(6, porcentaje1);
            st.setString(7, cod_cuenta_contable);
            st.setString(8, login);
            st.setString(9, signo);
            st.executeUpdate();
            
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_IVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
    }
    
     /**
     * Actualiza el estado de un registro en el archivotipo_de_impuesto de tipo RIVA.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param reg_status Estado del registro
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEESTADORIVA(String reg_status, String dstrct, String codigo_impuesto,  String fecha_vigencia, String login ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATE_RIVA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, reg_status);
            st.setString(2, login);
            st.setString(3, dstrct);
            st.setString(4, codigo_impuesto);
            st.setString(5, fecha_vigencia);
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEESTADORIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
    }
    
    /**
     * Actualiza un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo Nuevo c�digo del impuesto
     * @param fechav Nueva fecha de vigencia
     * @param concepto Nuevo concepto
     * @param porcentaje1 Porcentaje 1
     * @param cuenta C�digo de la cuenta contable
     * @param codigoi C�digo del impuesto
     * @param fv Fecha de vigencia
     * @param concept Concepto
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATERIVA(String tipo, String codigo, String concepto, String fechav, String porcentaje1, String cuenta, String codigoi, String fv, String concep, String login, String signo  ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATE_RIVA2";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo);
            st.setString(2, codigo);
            st.setString(3, concepto);
            st.setString(4, fechav);
            st.setString(5, porcentaje1);
            st.setString(6, cuenta);
            st.setString(7, login);
            st.setString(8, signo);
            st.setString(9, codigoi);
            st.setString(10, fv);
            st.setString(11, concep);
            st.executeUpdate();
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
    }
    
    /**
     * Elimina un registro en el archivotipo_de_impuesto de tipo RIVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void DELETERIVA(String dstrct, String codigo_impuesto, String fecha_vigencia ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_DELETERIVA";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, codigo_impuesto);
                st.setString(3, fecha_vigencia);
                st.executeUpdate();
            }
        } catch(SQLException e) {
            throw new SQLException("Error en rutina DELETEIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
    }
    
    /* EXISTEN */
    
    /**
     * Verfica si existe un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @return true si existe, false si no
     * @version 1.0
     */
    public boolean EXISTEIVA(String dstrct, String codigo_impuesto, String fecha_vigencia ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTEIVA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, fecha_vigencia);
            rs = st.executeQuery();
            
            if( rs.next() ) existe = true;
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina EXISTEIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }        
        return existe;
    }
    
    /**
     * Verifica si existe un registro en el archivotipo_de_impuesto de tipo RFTE.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param tipo RFTE/RFTECL
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @return true si existe, false si no
     * @version 1.0
     */
    public boolean EXISTERFTE(String dstrct, String codigo_impuesto,  String fecha_vigencia,  String concepto, String tipo ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTERFTE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, fecha_vigencia);
            st.setString(4, concepto);
            st.setString(5, tipo);
            rs = st.executeQuery();
            
            if( rs.next() ) existe = true;
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina EXISTERFTE [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        
        return existe;
    }
    
    /**
     * Verifica si existe un registro en el archivotipo_de_impuesto de tipo RICA.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @param agencia C�digo de la agencia
     * @param concepto Concepto
     * @param tipo RICA/RICACL
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @return true si existe, false si no
     * @version 1.0
     */
    public boolean EXISTERICA(String dstrct, String codigo_impuesto,  String fecha_vigencia, String agencia, String concepto, String tipo ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTERICA";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, fecha_vigencia);
            st.setString(4, agencia);
            st.setString(5, concepto);
            st.setString(6, tipo);
            rs = st.executeQuery();
            
            if( rs.next() ) existe = true;
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina EXISTERICA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
        return existe;
    }
    
    
    /**
     * Verifica si existe un registro en el archivo tipo_de_impuesto de tipo RIVA.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @return true si existe, false si no
     * @version 1.0
     */
    public boolean EXISTERIVA(String dstrct, String codigo_impuesto, String fecha_vigencia ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTERIVA";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, fecha_vigencia);
            rs = st.executeQuery();
            
            if( rs.next() ) existe = true;
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina EXISTERIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;
    }
        /**
     * Este m�todo genera un objeto tipo Tipo_Impuesto dado los parametros dados
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @param tipoImpuesto Tipo de impuesto
     * @param codigoImpuesto C�digo del impuesto
     * @param fecha Fecha de vigencia
     * @autor Ing. David Lamadrid
     * @version 1.0
     */
    public Tipo_impuesto buscarImpuestoPorCodigos(String tipoImpuesto, String codigoImpuesto, String distrito, String agencia) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        Tipo_impuesto datos = new  Tipo_impuesto();
        String query = "SQL_IMPUESTO_POR_CODIGO";//JJCastro fase2
        
        try {
            agencia = agencia.equals("OP")?"BQ":agencia;
            agencia = tipoImpuesto.equals("RICA")?agencia:"";
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, tipoImpuesto);
            st.setString(3, codigoImpuesto);
            rs = st.executeQuery();            
            if(rs.next()){
                datos.setTipo_impuesto(tipoImpuesto);
                datos.setCodigo_impuesto(codigoImpuesto);
                datos.setPorcentaje1(rs.getDouble("porcentaje1"));
                datos.setPorcentaje2(rs.getDouble("porcentaje2"));
                datos.setInd_signo(rs.getInt("ind_signo"));
                datos.setCod_cuenta_contable(rs.getString("cod_cuenta_contable"));
                datos.setDescripcion(rs.getString("descripcion"));
            }else
                datos =null;
        }
        }catch(Exception e) {
            throw new SQLException("Error en rutina SEARCH_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return datos;
    } 
    
    
   
    /*
     *Metodo que Retorna un Vector con los tipos de Impuestos en una fecha Determinada
     *David lamadrid
     *@params String tipoImpuesto,String fecha
     *@Exceptions SQLException
    */
    public Vector buscarImpuestoPorTipo(String tipoImpuesto,String fecha,String distrito,String agencia) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        Vector vImpuestos = new Vector();
        String query = "SQL_B_IMPUESTO_POR_C";//JJCastro fase2
        
        try {
            agencia = agencia.equals("OP")?"BQ":agencia;
            agencia = tipoImpuesto.equals("RICA")?agencia:"";
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, tipoImpuesto);
            st.setString(3, fecha);            
            rs = st.executeQuery();            
            while(rs.next()){
                 Tipo_impuesto datos = new  Tipo_impuesto();              
                datos.setAgencia(rs.getString("nombre"));
                datos.setTipo_impuesto(rs.getString("tipo_impuesto"));
                datos.setCodigo_impuesto(rs.getString("codigo_impuesto"));
                datos.setPorcentaje1(rs.getDouble("porcentaje1"));
                datos.setPorcentaje2(rs.getDouble("porcentaje2"));
                datos.setCod_cuenta_contable(rs.getString("concepto")); 
                datos.setDescripcion(rs.getString("descripcion"));
                
                vImpuestos.add(datos);
            datos = null;//Liberar Espacio JJCastro
            }
        }
        }catch(Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina SEARCH_TP [Tipo_proveedorDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vImpuestos;
    } 
    
    
    
      /*
     *Metodo que Retorna un Vector con los tipos de Impuestos en una fecha Determinada
     *Jose de la rosa
     *@params String tipoImpuesto,String fecha
     *@Exceptions SQLException
    */
    public Vector buscarImpuestoCliente(String tipoImpuesto,String fecha,String distrito,String agencia) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        Vector vImpuestos = new Vector();
        String query = "SQL_IMPUESTO_CLIENTE";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, agencia.equals ("OP")?"%":agencia);
            st.setString(3, tipoImpuesto);
            st.setString(4, fecha);
            rs = st.executeQuery();            
            while(rs.next()){
                 Tipo_impuesto datos = new  Tipo_impuesto();
                datos.setTipo_impuesto(rs.getString("tipo_impuesto"));
                datos.setCodigo_impuesto(rs.getString("codigo_impuesto"));
                datos.setPorcentaje1(rs.getDouble("porcentaje1"));
                datos.setPorcentaje2(rs.getDouble("porcentaje2"));
                datos.setCod_cuenta_contable(rs.getString("concepto")); 
                datos.setDescripcion(rs.getString("descripcion"));
                
                vImpuestos.add(datos);
                datos = null;//Liberar Espacio JJCastro
            }
        }
        }catch(Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina buscarImpuestoCliente(String tipoImpuesto,String fecha,String distrito,String agencia)... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vImpuestos;
    }
    
    
    /*
     *Metodo que busca los codigos del rica de una agencia del a�o en curso
     *Diogenes Bastidas
     *@params distrito, agencia, fecha
     *@Exceptions SQLException
    */
    public void buscarRicaAgencia(String distrito, String agencia, String fecha) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query = "SQL_BUSCAR_RICA";
        try {
            this.impuestos =  new Vector();
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, agencia);
            st.setString(3, fecha);
            rs = st.executeQuery();            
            while(rs.next()){
                Tipo_impuesto datos = new  Tipo_impuesto();
                datos.setCodigo_impuesto(rs.getString("codigo_impuesto"));
                datos.setPorcentaje1(rs.getDouble("porcentaje1"));
                datos.setPorcentaje2(rs.getDouble("porcentaje2"));
                impuestos.add(datos);
            } 
        }
        }catch(Exception e) {
            throw new SQLException("Error en rutina los Codigos RICA... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 
    
    
    /**
     * Getter for property impuestos.
     * @return Value of property impuestos.
     */
    public java.util.Vector getImpuestos() {
        return impuestos;
    }    

    /**
     * Setter for property impuestos.
     * @param impuestos New value of property impuestos.
     */
    public void setImpuestos(java.util.Vector impuestos) {
        this.impuestos = impuestos;
    }
    /*
       *Metodo que Retorna un Vector con los tipos de Impuestos en una fecha Determinada
       *Jose de la rosa
       *@params String tipoImpuesto,String fecha
       *@Exceptions SQLException
       */
    public Tipo_impuesto buscarImpuestoxCodigo(String dstrct, String codigo) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        Vector vImpuestos = new Vector();
        String query = "SQL_IMPUESTO_XCODIGO";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, codigo);
            rs = st.executeQuery();
            if(rs.next()){
                Tipo_impuesto datos = new  Tipo_impuesto();
                datos.setTipo_impuesto(rs.getString("tipo_impuesto"));
                datos.setCodigo_impuesto(rs.getString("codigo_impuesto"));
                datos.setPorcentaje1(rs.getDouble("porcentaje1"));
                datos.setPorcentaje2(rs.getDouble("porcentaje2"));
                datos.setCod_cuenta_contable(rs.getString("cod_cuenta_contable"));
                datos.setDescripcion(rs.getString("descripcion"));
                
                return datos;
            }
        }
        }catch(Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina buscarImpuestoxCodigo(String dstrct, String codigo)... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return null;
    }
    
    /**
     * Verfica si existe un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @return true si existe, false si no
     * @version 1.0
     */
    public boolean EXISTEIVA(String dstrct, String codigo_impuesto, String fecha_vigencia, String concepto ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTEIVA";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, fecha_vigencia);
            st.setString(4, concepto);//AMATURANA 03.03.2007
            org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
            logger.info("?EXISTE: " + st);
            
            rs = st.executeQuery();
            
            if( rs.next() ) existe = true;
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina EXISTEIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;
    }

    /**
     * Ingresa un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param concepto Concepto
     * @param fecha_vigencia Fecha de vigencia
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cod_cuenta_contable C�digo de la cuenta contable
     * @param signo 1/-1
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void INSERTIVA(String dstrct, String codigo_impuesto , String tipo_impuesto, String concepto, String fecha_vigencia, double porcentaje1, double porcentaje2, String cod_cuenta_contable, String login, String descripcion, String signo) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_INSERTIVA";//JJCastro fase2
        
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, codigo_impuesto);
            st.setString(3, tipo_impuesto);
            st.setString(4, concepto);
            st.setString(5,fecha_vigencia);
            st.setDouble(6, porcentaje1);
            st.setDouble(7, porcentaje2);
            st.setString(8, cod_cuenta_contable);
            st.setString(9, login);
            st.setString(10, descripcion);
            st.setString(11, signo);
            st.executeUpdate();
            
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_IVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Actualiza un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param dstrct C�digo del distrito
     * @param tipo Tipo de impuesto
     * @param codigo Nuevo c�digo del impuesto
     * @param fechav Nueva fecha de vigencia
     * @param concepto Nuevo concepto
     * @param porcentaje1 Porcentaje 1
     * @param porcentaje2 Porcentaje 2
     * @param cuenta C�digo de la cuenta contable
     * @param codigoi C�digo del impuesto
     * @param fv Fecha de vigencia
     * @param concept Concepto
     * @param signo 1/-1
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEIVA(String tipo, String codigo, String concepto, String fechav, String porcentaje1, String porcentaje2, String cuenta, String descripcion, String codigoi, String fv, String concep, String login, String signo  ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATEIVA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tipo);
                st.setString(2, codigo);
                st.setString(3, concepto);
                st.setString(4, fechav);
                st.setString(5, porcentaje1);
                st.setString(6, porcentaje2);
                st.setString(7, cuenta);
                st.setString(8, descripcion);
                st.setString(9, login);
                st.setString(10, signo);
                st.setString(11, codigoi);
                st.setString(12, fv);
                st.setString(13, concep);
                org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
                logger.info("?UPDATE IVA: " + st);
                st.executeUpdate();
            }
        } catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     /**
     * Actualiza el estado de un registro en el archivotipo_de_impuesto de tipo IVA.
     * @autor Ing. Juan Manuel Escand�n
     * @param reg_status Estado del registro
     * @param dstrct C�digo del distrito
     * @param codigo_impuesto C�digo del impuesto
     * @param fecha_vigencia Fecha de vigencia
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void UPDATEESTADOIVA(String reg_status, String dstrct, String codigo_impuesto,  String fecha_vigencia, String login, String concepto ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATERIVA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, reg_status);
                st.setString(2, login);
                st.setString(3, dstrct);
                st.setString(4, codigo_impuesto);
                st.setString(5, fecha_vigencia);
                st.setString(6, concepto);

                st.executeUpdate();
            }
        } catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEESTADOIVA [Tipo_impuestoDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }





    /**
     *Metodo que Retorna un impuesto de un codigo y  una fecha Determinada
     *@autor Iris Vargas
     *@params dstrct
     *@params codigo
     *@params fecha
     *@Exceptions SQLException
     */
    public Tipo_impuesto buscarImpuestoxCodigoxFecha(String dstrct, String codigo, String fecha) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector vImpuestos = new Vector();
        String query = "SQL_IMPUESTO_XCODIGO_XFECHA";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, dstrct);
                st.setString(2, codigo);
                st.setString(3, fecha);
                rs = st.executeQuery();
                if (rs.next()) {
                    Tipo_impuesto datos = new Tipo_impuesto();
                    datos.setTipo_impuesto(rs.getString("tipo_impuesto"));
                    datos.setCodigo_impuesto(rs.getString("codigo_impuesto"));
                    datos.setPorcentaje1(rs.getDouble("porcentaje1"));
                    datos.setPorcentaje2(rs.getDouble("porcentaje2"));
                    datos.setCod_cuenta_contable(rs.getString("concepto"));
                    datos.setDescripcion(rs.getString("descripcion"));
                    datos.setCod_cuenta_contable(rs.getString("cod_cuenta_contable"));

                    return datos;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina buscarImpuestoxCodigoxFecha(String dstrct, String codigo, String fecha)... \n" + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return null;
    }



}
