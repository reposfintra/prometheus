/**********************************************************
 * Nombre:        ImportacionCXCDAO.java                  *
 * Descripci�n:   Accesso a la base de datos              *
 * Autor:         Ing. Ivan Dario Gomez*
 * Fecha:         24 de octubre de 2005, 02:16 PM         *
 * Versi�n:       Java  1.0                               *
 * Copyright:     Fintravalores S.A. S.A.            *
 **********************************************************/

package com.tsp.operation.model.DAOS;


import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.Util;

import java.sql.*;
import java.util.*;

public class ImportacionCXCDAO extends MainDAO {
    
    
    /** Creates a new instance of ImportacionCXP */
    public ImportacionCXCDAO() {
        super ("ImportacionCXCDAO.xml");
    }
    public ImportacionCXCDAO(String dataBaseName) {
        super ("ImportacionCXCDAO.xml", dataBaseName);
    }
 
    
    
    

    
    
    
    
    
    
    /** verifica la existencia de la agencia */
    
    public boolean searchAgencia(String id_agencia) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean           existe      = false;
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_AGENCIA");
            st.setString( 1 , id_agencia    );
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchAgencia [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_AGENCIA");
        }
        return existe;
    }
    
    
    
    
    /** busca el valor de las monedas de las compa�ias */
    
    public TreeMap searchMonedasCias() throws SQLException {
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        TreeMap           monedas= new TreeMap();
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_MON_CIA");
            rs = st.executeQuery();
            while (rs.next()){
                CIA cia = new CIA();
                cia.setDistrito(rs.getString("dstrct"));
                cia.setMoneda  (rs.getString("moneda"));
                cia.setBase    (rs.getString("base"));
                monedas.put    (rs.getString("dstrct"),  cia );
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchMonedasCias [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_MON_CIA");
        }
        return monedas;
    }
    
    
    
    /** busca el valor de las monedas de las compa�ias */
    
    public List searchFacturaPendiente(String usuario) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              factura     = new LinkedList();
        try {
            st = this.crearPreparedStatement("SQL_SEARCH_IMPORT");
            st.setString(1, usuario);
            rs = st.executeQuery();
            while (rs.next()){
                factura_importacion fac = new factura_importacion();
                 fac.setDstrct             (rs.getString("dstrct"));
                 fac.setCodcli             (rs.getString("codcli"));               
                 fac.setNit                (rs.getString("nit"));               
                 
                 fac.setFecha_factura      (rs.getString("fecha_factura"));               
                 fac.setForma_pago         (rs.getString("forma_pago"));               
                 fac.setPlazo              (rs.getString("plazo"));               
                 fac.setDescripcion        (rs.getString("descripcion"));               
                 fac.setObservacion        (rs.getString("observacion"));               
                 fac.setMoneda             (rs.getString("moneda"));               
                 fac.setValor_tasa         (new Double(rs.getString("valor_tasa")));               
                 fac.setValor_factura      (rs.getDouble("valor_factura"));  
                 
                 fac.setItem               (rs.getInt("item"));
                 fac.setNumero_remesa      (rs.getString("numero_remesa"));
                 
                 fac.setFecrem             (rs.getString("fecha_remesa"));
                 fac.setDescripcion_item   (rs.getString("descripcion_item"));
                 fac.setCantidad           (new Double (rs.getString("cantidad")));
                 fac.setValor_unitario     (rs.getDouble("valor_unitariome"));
                 fac.setValor_item         (rs.getDouble("vlr_me"));
                 fac.setCodigo_cuenta_contable(rs.getString("codigo_cuenta"));
                 fac.setTiposubledger         (rs.getString("tipo_auxiliar"));
                 fac.setAuxliliar             (rs.getString("auxiliar"));
                 fac.setAgencia_facturacion   (rs.getString("formato"));
                 
                 fac.setCreation_date(rs.getString("creation_date"));
                 fac.setUser_update(rs.getString("user_update"));
                 fac.setCreation_user(rs.getString("creation_user"));
                 fac.setLast_update(rs.getString("last_update"));
                 fac.setFecha_migracion(rs.getString("fecha_migracion"));
                 
                 factura.add(fac);
               
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchFacturasPendientes [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_SEARCH_IMPORT");
        }
        return factura;
    }
    
    
    
    /** verifica la existencia de la agencia */
    
    public Cliente searchDatosCliente(String codcli) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        Cliente           cli      = null;
        try {
            st = this.crearPreparedStatement("SQL_BUSCAR_DATOS_CLIENTE");
            st.setString( 1 , codcli    );
            rs = st.executeQuery();
            if (rs.next()){
                cli = new Cliente();
                cli.setNit   (rs.getString("nit"));
                cli.setCodcli(rs.getString("codcli"));
                cli.setNomcli(rs.getString("nomcli"));
                cli.setRif   (rs.getString("rif"));
                cli.setAgenciaFacturacion(rs.getString("agfacturacion"));
                cli.setCmc(rs.getString("cmc"));
                
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchAgencia [ImportacionCXPDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_BUSCAR_DATOS_CLIENTE");
        }
        return cli;
    }
    
    
    /** verifica la existencia de la agencia */
    public boolean exiteRemesa(String numrem,String codPagador) throws SQLException {
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean           existe      = false;
        try {
            st = this.crearPreparedStatement("SQL_EXISTE_REMESA");
            st.setString( 1 , numrem    );
            st.setString( 2 , codPagador    );
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina SQL_EXISTE_REMESA [ImportacionCXCDAO]... \n"+e.getMessage());
        }
        finally {
            if (rs!=null)  rs.close();
            if (st!=null)  st.close();
            this.desconectar("SQL_EXISTE_REMESA");
        }
        return existe;
    }
    
}
