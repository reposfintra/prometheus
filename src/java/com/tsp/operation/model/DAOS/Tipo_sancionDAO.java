/*
 * Tipo_sancionDAO.java
 *
 * Created on 20 de octubre de 2005, 01:35 PM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;


/**
 *
 * @author  Jose
 */
public class Tipo_sancionDAO extends MainDAO {
    private Tipo_sancion tipo_sancion;
    private Vector tipo_sanciones;     
    private final String SQL_LIST = "select table_code as codigo, descripcion from tablagen where reg_status <> 'A' and table_type = 'TSANCION' order by codigo";
    /** Creates a new instance of Tipo_sancionDAO */
    public Tipo_sancionDAO() {
        super("Tipo_sancionDAO.xml");
    }
    public Tipo_sancion getTipo_sancion() {
        return tipo_sancion;
    }
    
    public void setTipo_sancion(Tipo_sancion tipo_sancion) {
        this.tipo_sancion = tipo_sancion;
    }
    
    public Vector getTipo_sanciones() {
        return tipo_sanciones;
    }
    
    public void setTipo_sanciones(Vector tipo) {
        this.tipo_sanciones = tipo;
    }
    
    public void listarTipo_sanciones ()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tipo_sanciones = null;
        String query = "SQL_LIST";
        
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                tipo_sanciones = new Vector();
                
                while(rs.next()){
                    tipo_sancion = new Tipo_sancion();
                    tipo_sancion.setCodigo(rs.getString("codigo"));
                    tipo_sancion.setDescripcion(rs.getString("descripcion"));
                    tipo_sanciones.add(tipo_sancion);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE SANCIONES " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


 /**
  * 
  * @throws SQLException
  */
    public void listTipo_sanciones()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LIST";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs= st.executeQuery();
                tipo_sanciones = new Vector();
                while(rs.next()){
                    tipo_sancion = new Tipo_sancion();
                    tipo_sancion.setCodigo(rs.getString("codigo"));
                    tipo_sancion.setDescripcion(rs.getString("descripcion"));
                    tipo_sanciones.add(tipo_sancion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LOS PRODUCTOS " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }    
}
