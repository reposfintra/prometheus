/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.AfiliadoConvenio;
import com.tsp.operation.model.beans.AfiliadoConvenioRangos;
import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.ConvenioComision;
import com.tsp.operation.model.beans.ConvenioCxc;
import com.tsp.operation.model.beans.ConveniosRemesas;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.tsp.operation.model.beans.Propietario; //darrieta
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.ConvenioCxcFiducias;
import com.tsp.operation.model.beans.ConvenioFiducias;
import java.sql.SQLException;
import com.tsp.operation.model.TransaccionDAO;
import com.tsp.operation.model.beans.CargosFijosConvenios;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.Deducciones;
import com.tsp.operation.model.beans.Unidad_Negocio;

/**
 *
 * @author maltamiranda
 */
public class GestionConveniosDAO extends MainDAO {

    public GestionConveniosDAO() {
        super("GestionConveniosDAO.xml");
    }

    public GestionConveniosDAO(String dataBaseName) {
        super("GestionConveniosDAO.xml", dataBaseName);
    }

 public ArrayList getConvenios(String tipo) throws Exception {
        ArrayList convenios = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_GET_CONVENIOS");
            String sql = this.obtenerSQL("SQL_GET_CONVENIOS");
            ps = con.prepareStatement(sql);
            ps.setString(1, tipo);
            rs = ps.executeQuery();
            convenios = rstotbl(rs);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return convenios;
    }

    public ArrayList rstotbl(ResultSet rs) throws Exception {
        ArrayList tabla = new ArrayList();
        while (rs.next()) {
            ArrayList tabla2 = new ArrayList();
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                tabla2.add(rs.getString(i));
            }
            tabla.add(tabla2);
        }
        return tabla;
    }

    public ArrayList<ConvenioComision> buscar_comisiones(String bd, String id_convenio) throws Exception {
        ArrayList<ConvenioComision> convenio_comision = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_BUSCAR_COMISIONES");
            String sql = this.obtenerSQL("SQL_BUSCAR_COMISIONES");
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                ConvenioComision cnv = new ConvenioComision();
                cnv.setReg_status(rs.getString("reg_status"));
                cnv.setDstrct(rs.getString("dstrct"));
                cnv.setId_convenio(rs.getString("id_convenio"));
                cnv.setId_comision(rs.getString("id_comision"));
                cnv.setNombre(rs.getString("nombre"));
                cnv.setCuenta_comision(rs.getString("cuenta_comision"));
                cnv.setCreation_user(rs.getString("creation_user"));
                cnv.setUser_update(rs.getString("user_update"));
                cnv.setCreation_date(rs.getString("creation_date"));
                cnv.setLast_update(rs.getString("last_update"));
                cnv.setPorcentaje_comision(rs.getDouble("porcentaje_comision"));
                cnv.setComision_tercero(rs.getBoolean("comision_tercero"));
                cnv.setIndicador_contra(rs.getBoolean("indicador_contra"));
                cnv.setCuenta_contra(rs.getString("cuenta_contra"));
                convenio_comision.add(cnv);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return convenio_comision;
    }

    public ArrayList<ConveniosRemesas> buscar_remesas(String bd, String id_convenio) throws Exception {
        ArrayList<ConveniosRemesas> convenio_remesas = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_BUSCAR_REMESAS");
            String sql = this.obtenerSQL("SQL_BUSCAR_REMESAS");
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                ConveniosRemesas cnv = new ConveniosRemesas();
                cnv.setReg_status(rs.getString("reg_status"));
                cnv.setDstrct(rs.getString("dstrct"));
                cnv.setId_convenio(id_convenio);
                cnv.setId_remesa(rs.getString("id_remesa"));
                cnv.setCiudad_sede(rs.getString("ciudad_sede"));
                cnv.setBanco_titulo(rs.getString("banco_titulo"));
                cnv.setCiudad_titulo(rs.getString("ciudad_titulo"));
                cnv.setGenera_remesa(rs.getBoolean("genera_remesa"));
                cnv.setCuenta_remesa(rs.getString("cuenta_remesa"));
                cnv.setCreation_user(rs.getString("creation_user"));
                cnv.setUser_update("user_update");
                cnv.setPorcentaje_remesa(rs.getDouble("porcentaje_remesa"));
                cnv.setCreation_date(rs.getString("creation_date"));
                cnv.setLast_update(rs.getString("last_update"));
                convenio_remesas.add(cnv);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return convenio_remesas;
    }

    public ArrayList<ConvenioCxc> buscar_cxc(String bd, String id_convenio) throws Exception {
        ArrayList<ConvenioCxc> convenio_cxc = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_BUSCAR_CXC");
            String sql = this.obtenerSQL("SQL_BUSCAR_CXC");
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                ConvenioCxc cnv = new ConvenioCxc();
                cnv.setReg_status(rs.getString("reg_status"));
                cnv.setDstrct(rs.getString("dstrct"));
                cnv.setId_convenio(id_convenio);
                cnv.setTitulo_valor(rs.getString("titulo_valor"));
                cnv.setPrefijo_factura(rs.getString("prefijo_factura"));
                cnv.setCuenta_cxc(rs.getString("cuenta_cxc"));
                cnv.setHc_cxc(rs.getString("hc_cxc"));
                cnv.setCreation_user(rs.getString("creation_user"));
                cnv.setUser_update("user_update");
                cnv.setCreation_date(rs.getString("creation_date"));
                cnv.setLast_update(rs.getString("last_update"));
                cnv.setGenRemesa(rs.getBoolean("genera_remesa"));
                cnv.setCuenta_prov_cxc(rs.getString("cuenta_prov_cxc"));
                cnv.setCuenta_prov_cxp(rs.getString("cuenta_prov_cxp"));
                cnv.setConvenioCxcFiducias(this.buscar_cxc_fiducia(bd, id_convenio, cnv.getTitulo_valor()));
                convenio_cxc.add(cnv);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return convenio_cxc;
    }

    public Convenio buscar_convenio(String bd, String id_convenio) throws Exception {
        Convenio convenio = new Convenio(buscar_cxc(bd, id_convenio), buscar_remesas(bd, id_convenio), buscar_comisiones(bd, id_convenio),buscar_fiducias(bd, id_convenio));
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_BUSCAR_CONVENIO");
            String sql = this.obtenerSQL("SQL_BUSCAR_CONVENIO");
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            if (rs.next()) {  
                convenio.setConvenio(convenio);
                convenio=convenio.load(rs);
                convenio.setCargos_fijos_convenios(this.buscar_cargos_fijos(bd, id_convenio));
                convenio.setUnidad_negocio(this.buscarUnidadNegocio(bd,id_convenio));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return convenio;
    }

    public void insertar_convenio(String bd, Convenio cnv, String redescuento,String tipo) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_OBTENER_CODIGO_CONVENIO");
            con.setAutoCommit(false);
            ps = con.prepareStatement(this.obtenerSQL("SQL_OBTENER_CODIGO_CONVENIO"));
            rs = ps.executeQuery();
            if (rs.next()) {
                cnv.setId_convenio(rs.getString("codigo"));
                ps2 = con.prepareStatement(this.obtenerSQL("SQL_INSERTAR_CONV_REDESCUENTO"));
                ps2.setString(1, rs.getString("codigo") );
                ps2.setString(2, redescuento);
                ps2.executeUpdate();
            }
            String sql = this.obtenerSQL("SQL_INSERTAR_CONVENIOS");
            ps = con.prepareStatement(sql);
            ps.setString(1, cnv.getNombre());
            ps.setString(2, cnv.getDescripcion());
            ps.setString(3, cnv.getNit_convenio());
            ps.setBoolean(4, cnv.isFactura_tercero());
            ps.setString(5, cnv.getNit_tercero());
            ps.setDouble(6, cnv.getTasa_interes());
            ps.setString(7, cnv.getCuenta_interes());
            ps.setDouble(8, cnv.getValor_custodia());
            ps.setString(9, cnv.getCuenta_custodia());
            ps.setString(10, cnv.getPrefijo_negocio());
            ps.setString(11, cnv.getPrefijo_cxp());
            ps.setString(12, cnv.getCuenta_cxp());
            ps.setString(13, cnv.getHc_cxp());
            ps.setBoolean(14, cnv.isDescuenta_gmf());
            ps.setString(15, cnv.getCuota_gmf());
            ps.setString(16, cnv.getPrefijo_nc_gmf());
            ps.setString(17, cnv.getCuenta_gmf());
            ps.setBoolean(18, cnv.isDescuenta_aval());
            ps.setString(19, cnv.getPrefijo_nc_aval());
            ps.setString(20, cnv.getCuenta_aval());
            ps.setString(21, cnv.getPrefijo_diferidos());
            ps.setString(22, cnv.getCuenta_diferidos());
            ps.setString(23, cnv.getHc_diferidos());
            ps.setString(24, cnv.getCreation_user());
            ps.setString(25, cnv.getId_convenio());
            ps.setDouble(26, cnv.getPorc_gmf());//2010-09-27
            ps.setString(27, cnv.getImpuesto());
            ps.setString(28, cnv.getCuenta_ajuste());
            ps.setDouble(29, cnv.getPorc_gmf2());
            ps.setString(30, cnv.getCuenta_gmf2());       
            ps.setString(31, cnv.getPrefijo_endoso());
            ps.setString(32, cnv.getHc_endoso());
            ps.setBoolean(33, cnv.isMediador_aval());
            ps.setString(34, cnv.getNit_mediador());
            ps.setBoolean(35, cnv.isAval_tercero());
            ps.setBoolean(36, cnv.isCentral());
            ps.setBoolean(37, cnv.isCapacitacion());
            ps.setBoolean(38, cnv.isSeguro());
            ps.setString(39, cnv.getNit_central());
            ps.setString(40, cnv.getNit_capacitador());
            ps.setString(41, cnv.getNit_asegurador());
            ps.setString(42, cnv.getPrefijo_cxc_interes());
            ps.setString(43, cnv.getPrefijo_cxp_central());
            ps.setString(44, cnv.getCuenta_central());
            ps.setString(45, cnv.getCuenta_com_central());
            ps.setString(46, cnv.getPrefijo_cxc_cat());
            ps.setString(47, cnv.getCuenta_cat());
            ps.setString(48, cnv.getCuenta_capacitacion());
            ps.setString(49, cnv.getCuenta_seguro());
            ps.setString(50, cnv.getCuenta_com_seguro());
            ps.setDouble(51, cnv.getValor_central());
            ps.setDouble(52, cnv.getValor_com_central());
            ps.setDouble(53, cnv.getPorcentaje_cat());
            ps.setDouble(54, cnv.getValor_capacitacion());
            ps.setDouble(55, cnv.getValor_seguro());
            ps.setDouble(56, cnv.getPorcentaje_com_seguro());
            ps.setDouble(57, cnv.getMonto_minimo());
            ps.setDouble(58, cnv.getMonto_maximo());
            ps.setInt(59, Integer.parseInt(cnv.getPlazo_maximo()));
            ps.setString(60, cnv.getTipo());
            ps.setBoolean(61, cnv.isRedescuento());
            ps.setBoolean(62, cnv.isAval_anombre());
            ps.setBoolean(63, cnv.isCxp_avalista());
            ps.setString(64, cnv.getNit_anombre());
            ps.setString(65, cnv.getCctrl_db_cxc_aval());
            ps.setString(66, cnv.getCctrl_cr_cxc_aval());
            ps.setString(67, cnv.getCctrl_iva_cxc_aval());
            ps.setString(68, cnv.getPrefijo_cxp_avalista());
            ps.setString(69, cnv.getHc_cxp_avalista());
            ps.setString(70, cnv.getCuenta_cxp_avalista());
            ps.setString(71, cnv.getCuenta_cxc_aval());
            ps.setString(72, cnv.getHc_cxc_aval());
            ps.setString(73, cnv.getPrefijo_cxc_aval());
            ps.setBoolean(74, cnv.isCat());
            ps.setString(75, cnv.getAgencia());
            ps.setString(76, cnv.getCuenta_cuota_administracion());
            ps.setString(77, cnv.getPrefijo_cuota_admin());
            ps.setString(78, cnv.getHc_cuota_admin());
            ps.setString(79, cnv.getCta_cuota_admin_diferido());
            //ps.setString(75, cnv.getRuta());
            
            

            ps.execute();
            actualizar_tablas(con, ps, cnv, tipo);
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public void actualizar_tablas(Connection con, PreparedStatement ps, Convenio cnv, String tipo) throws Exception {
        try {
            for (int i = 0; i < cnv.getConvenioComision().size(); i++) {
                if (((ConvenioComision) cnv.getConvenioComision().get(i)).getId_comision().equals("")) {
                    ((ConvenioComision) cnv.getConvenioComision().get(i)).setId_convenio(cnv.getId_convenio());
                    this.insertar_comision(con, ps, ((ConvenioComision) cnv.getConvenioComision().get(i)));
                } else {
                    this.actualizar_comision(con, ps, ((ConvenioComision) cnv.getConvenioComision().get(i)));
                }
            }
            for (int i = 0; i < cnv.getConveniosRemesas().size(); i++) {
                if (((ConveniosRemesas) cnv.getConveniosRemesas().get(i)).getId_remesa().equals("")) {
                    ((ConveniosRemesas) cnv.getConveniosRemesas().get(i)).setId_convenio(cnv.getId_convenio());
                    this.insertar_remesa(con, ps, ((ConveniosRemesas) cnv.getConveniosRemesas().get(i)));
                } else {
                    this.actualizar_remesa(con, ps, ((ConveniosRemesas) cnv.getConveniosRemesas().get(i)));
                }
            }
            TransaccionDAO scv = new TransaccionDAO(this.getDatabaseName());
            scv.crearStatement();
            if(tipo.equals("MODIFICAR_CONVENIO")){
                 scv.getSt().addBatch(this.eliminar_cxc_fiducias(cnv.getId_convenio()));
            }
            scv.getSt().addBatch(eliminar_fiducias(cnv.getId_convenio()));
            scv.getSt().addBatch(this.eliminar_cxc(cnv.getId_convenio()));
            for (int i = 0; i < cnv.getConvenioFiducias().size(); i++) {
                ((ConvenioFiducias) cnv.getConvenioFiducias().get(i)).setId_convenio(cnv.getId_convenio());
                scv.getSt().addBatch(insertar_fiducia((ConvenioFiducias) cnv.getConvenioFiducias().get(i)));
            }
            for (int i = 0; i < cnv.getConvenioCxc().size(); i++) {
                ((ConvenioCxc) cnv.getConvenioCxc().get(i)).setId_convenio(cnv.getId_convenio());
                scv.getSt().addBatch(insertar_cxc((ConvenioCxc) cnv.getConvenioCxc().get(i)));
                if (tipo.equals("MODIFICAR_CONVENIO")) {
                    for (int j = 0; j < cnv.getConvenioCxc().get(i).getConvenioCxcFiducias().size(); j++) {
                        ((ConvenioCxcFiducias) cnv.getConvenioCxc().get(i).getConvenioCxcFiducias().get(j)).setId_convenio(cnv.getId_convenio());
                        ((ConvenioCxcFiducias) cnv.getConvenioCxc().get(i).getConvenioCxcFiducias().get(j)).setTitulo_valor(((ConvenioCxc) cnv.getConvenioCxc().get(i)).getTitulo_valor());
                        scv.getSt().addBatch(insertar_cxc_fiducia((ConvenioCxcFiducias) cnv.getConvenioCxc().get(i).getConvenioCxcFiducias().get(j)));
                    }
                }
            }
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    private String insertar_cxc(ConvenioCxc cnv) throws Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("SQL_INSERTAR_CXC"), true);
            st.setString(1, cnv.getReg_status());
            st.setString(2, cnv.getDstrct());
            st.setString(3, cnv.getId_convenio());
            st.setString(4, cnv.getTitulo_valor());
            st.setString(5, cnv.getPrefijo_factura());
            st.setString(6, cnv.getCuenta_cxc());
            st.setString(7, cnv.getHc_cxc());
            st.setString(8, cnv.getCreation_user());
            st.setBoolean(9, cnv.isGenRemesa());
            st.setString(10, cnv.getCuenta_prov_cxc());

            sql = st.getSql();
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE SQL_INSERTAR_CXC" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE SQL_INSERTAR_CXC" + e.getMessage());
                }
            }
        }
        return sql;

    }

    public void actualizar_remesa(Connection con, PreparedStatement ps, ConveniosRemesas cnv) throws Exception {
        try {
            String sql = this.obtenerSQL("SQL_ACTUALIZAR_REMESAS");
            ps = con.prepareStatement(sql);
            ps.setString(1, cnv.getReg_status());
            ps.setString(2, cnv.getDstrct());
            ps.setString(3, cnv.getCiudad_sede());
            ps.setString(4, cnv.getBanco_titulo());
            ps.setString(5, cnv.getCiudad_titulo());
            ps.setBoolean(6, cnv.isGenera_remesa());
            ps.setDouble(7, cnv.getPorcentaje_remesa());
            ps.setString(8, cnv.getCuenta_remesa());
            ps.setString(9, cnv.getUser_update());
            ps.setString(10, cnv.getId_convenio());
            ps.setString(11, cnv.getId_remesa());

            ps.execute();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    private void insertar_remesa(Connection con, PreparedStatement ps, ConveniosRemesas cnv) throws Exception {
        try {
            String sql = this.obtenerSQL("SQL_INSERTAR_REMESAS");
            ps = con.prepareStatement(sql);
            ps.setString(1, cnv.getReg_status());
            ps.setString(2, cnv.getDstrct());
            ps.setString(3, cnv.getId_convenio());
            ps.setString(4, cnv.getCiudad_sede());
            ps.setString(5, cnv.getBanco_titulo());
            ps.setString(6, cnv.getCiudad_titulo());
            ps.setBoolean(7, cnv.isGenera_remesa());
            ps.setDouble(8, cnv.getPorcentaje_remesa());
            ps.setString(9, cnv.getCuenta_remesa());
            ps.setString(10, cnv.getCreation_user());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception(e.getMessage());
        }

    }

    public void actualizar_comision(Connection con, PreparedStatement ps, ConvenioComision cnv) throws Exception {
        try {
            String sql = this.obtenerSQL("SQL_ACTUALIZAR_COMISION");
            ps = con.prepareStatement(sql);
            ps.setString(1, cnv.getReg_status());
            ps.setString(2, cnv.getDstrct());
            ps.setString(3, cnv.getNombre());
            ps.setString(4, cnv.getCuenta_comision());
            ps.setDouble(5, cnv.getPorcentaje_comision());
            ps.setString(6, cnv.getUser_update());
            ps.setBoolean(7, cnv.isComision_tercero());
            ps.setBoolean(8, cnv.isIndicador_contra());
            ps.setString(9, cnv.getCuenta_contra());
            ps.setString(10, cnv.getId_convenio());
            ps.setString(11, cnv.getId_comision());
            ps.execute();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    private void insertar_comision(Connection con, PreparedStatement ps, ConvenioComision cnv) throws Exception {
        try {
            String sql = this.obtenerSQL("SQL_INSERTAR_COMISION");
            ps = con.prepareStatement(sql);
            ps.setString(1, cnv.getReg_status());
            ps.setString(2, cnv.getDstrct());
            ps.setString(3, cnv.getId_convenio());
            ps.setString(4, cnv.getNombre());
            ps.setString(5, cnv.getCuenta_comision());
            ps.setDouble(6, cnv.getPorcentaje_comision());
            ps.setString(7, cnv.getCreation_user());
            ps.setBoolean(8, cnv.isComision_tercero());
            ps.setBoolean(9, cnv.isIndicador_contra());
            ps.setString(10, cnv.getCuenta_contra());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            throw new Exception(e.getMessage());
        }

    }

    public void actualizar_convenio(String bd, Convenio cnv, String tipo) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = conectarJNDI("SQL_ACTUALIZAR_CONVENIOS");
            con.setAutoCommit(false);
            String sql = this.obtenerSQL("SQL_ACTUALIZAR_CONVENIOS");
            ps = con.prepareStatement(sql);
            ps.setString(1, cnv.getReg_status());
            ps.setString(2, cnv.getNombre());
            ps.setString(3, cnv.getDescripcion());
            ps.setString(4, cnv.getNit_convenio());
            ps.setBoolean(5, cnv.isFactura_tercero());
            ps.setString(6, cnv.getNit_tercero());
            ps.setDouble(7, cnv.getTasa_interes());
            ps.setString(8, cnv.getCuenta_interes());
            ps.setDouble(9, cnv.getValor_custodia());
            ps.setString(10, cnv.getCuenta_custodia());
            ps.setString(11, cnv.getPrefijo_negocio());
            ps.setString(12, cnv.getPrefijo_cxp());
            ps.setString(13, cnv.getCuenta_cxp());
            ps.setString(14, cnv.getHc_cxp());
            ps.setBoolean(15, cnv.isDescuenta_gmf());
            ps.setString(16, cnv.getCuota_gmf());
            ps.setString(17, cnv.getPrefijo_nc_gmf());
            ps.setString(18, cnv.getCuenta_gmf());
            ps.setBoolean(19, cnv.isDescuenta_aval());
            ps.setString(20, cnv.getPrefijo_nc_aval());
            ps.setString(21, cnv.getCuenta_aval());
            ps.setString(22, cnv.getPrefijo_diferidos());
            ps.setString(23, cnv.getCuenta_diferidos());
            ps.setString(24, cnv.getHc_diferidos());
            ps.setString(25, cnv.getUser_update());
            ps.setDouble(26, cnv.getPorc_gmf());//2010-09-27
            ps.setString(27, cnv.getImpuesto());
            ps.setString(28, cnv.getCuenta_ajuste());
            ps.setDouble(29, cnv.getPorc_gmf2());
            ps.setString(30, cnv.getCuenta_gmf2());
            ps.setString(31, cnv.getPrefijo_endoso());
            ps.setString(32, cnv.getHc_endoso());
            ps.setBoolean(33, cnv.isMediador_aval());
            ps.setString(34, cnv.getNit_mediador());
            ps.setBoolean(35, cnv.isAval_tercero());
            ps.setBoolean(36, cnv.isCentral());
            ps.setBoolean(37, cnv.isCapacitacion());
            ps.setBoolean(38, cnv.isSeguro());
            ps.setString(39, cnv.getNit_central());
            ps.setString(40, cnv.getNit_capacitador());
            ps.setString(41, cnv.getNit_asegurador());
            ps.setString(42, cnv.getPrefijo_cxc_interes());
            ps.setString(43, cnv.getPrefijo_cxp_central());
            ps.setString(44, cnv.getCuenta_central());
            ps.setString(45, cnv.getCuenta_com_central());
            ps.setString(46, cnv.getPrefijo_cxc_cat());
            ps.setString(47, cnv.getCuenta_cat());
            ps.setString(48, cnv.getCuenta_capacitacion());
            ps.setString(49, cnv.getCuenta_seguro());
            ps.setString(50, cnv.getCuenta_com_seguro());
            ps.setDouble(51, cnv.getValor_central());
            ps.setDouble(52, cnv.getValor_com_central());
            ps.setDouble(53, cnv.getPorcentaje_cat());
            ps.setDouble(54, cnv.getValor_capacitacion());
            ps.setDouble(55, cnv.getValor_seguro());
            ps.setDouble(56, cnv.getPorcentaje_com_seguro());
            ps.setDouble(57, cnv.getMonto_minimo());
            ps.setDouble(58, cnv.getMonto_maximo());
            ps.setInt(59, Integer.parseInt(cnv.getPlazo_maximo()));
            ps.setBoolean(60, cnv.isRedescuento());
            ps.setString(61, cnv.getPrefijo_cxc_aval());
            ps.setBoolean(62, cnv.isAval_anombre());
            ps.setBoolean(63, cnv.isCxp_avalista());
            ps.setString(64, cnv.getNit_anombre());
            ps.setString(65, cnv.getCctrl_db_cxc_aval());
            ps.setString(66, cnv.getCctrl_cr_cxc_aval());
            ps.setString(67, cnv.getCctrl_iva_cxc_aval());
            ps.setString(68, cnv.getPrefijo_cxp_avalista());
            ps.setString(69, cnv.getHc_cxp_avalista());
            ps.setString(70, cnv.getCuenta_cxp_avalista());
            ps.setString(71, cnv.getHc_cxc_aval());
            ps.setString(72, cnv.getCuenta_cxc_aval());
            ps.setBoolean(73, cnv.isCat());
            ps.setString(74, cnv.getAgencia());
            ps.setString(75, cnv.getCuenta_cuota_administracion());
            ps.setString(76, cnv.getPrefijo_cuota_admin());
            ps.setString(77, cnv.getHc_cuota_admin());
            ps.setString(78, cnv.getCta_cuota_admin_diferido());
            ps.setString(79, cnv.getId_convenio());
            
            ps.execute();
            this.actualizar_tablas(con, ps, cnv, tipo );
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public ArrayList getAgencias() throws Exception {
        ArrayList agencia = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_GET_AGENCIAS");
            String sql = this.obtenerSQL("SQL_GET_AGENCIAS");
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            agencia = new ArrayList();
            while (rs.next()) {
                String[] datoagencia = new String[2];
                datoagencia[0] = rs.getString("department_code");
                datoagencia[1] = rs.getString("department_name");
                agencia.add(datoagencia);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return agencia;
    }
    public ArrayList getPrefijos(String bd) throws Exception {
        ArrayList prefijos = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_GET_PREFIJOS");
            String sql = this.obtenerSQL("SQL_GET_PREFIJOS");
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            prefijos = new ArrayList();
            while (rs.next()) {
                String[] prefijo = new String[3];
                prefijo[0] = rs.getString("tipo_documento");
                prefijo[1] = rs.getString("prefix");
                prefijo[2] = rs.getString("document_type");
                prefijos.add(prefijo);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return prefijos;
    }

    public ArrayList getTipo_documento(String bd) throws Exception {
        ArrayList prefijos = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_GET_TIPODOC");
            String sql = this.obtenerSQL("SQL_GET_TIPODOC");
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            prefijos = new ArrayList();
            while (rs.next()) {
                String[] prefijo = new String[2];
                prefijo[0] = rs.getString("cod");
                prefijo[1] = rs.getString("tipo_documento");
                prefijos.add(prefijo);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return prefijos;
    }

    public ArrayList<Convenio> listarConvenios(String bd) throws Exception {
        ArrayList<Convenio> lista = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "LIST_CONV";
        String sql = "";
        Convenio conv = null;
        try {
            lista = new ArrayList<Convenio>();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                conv = new Convenio();
                conv.setId_convenio(rs.getString("id_convenio"));
                conv.setCuenta_aval(rs.getString("cuenta_aval"));
                conv.setCuenta_custodia(rs.getString("cuenta_custodia"));
                conv.setCuenta_cxp(rs.getString("cuenta_cxp"));
                conv.setCuenta_diferidos(rs.getString("cuenta_diferidos"));
                conv.setCuenta_gmf(rs.getString("cuenta_gmf"));
                conv.setCuenta_gmf2(rs.getString("cuenta_gmf2"));
                conv.setCuenta_interes(rs.getString("cuenta_interes"));
                conv.setCuota_gmf(rs.getString("cuota_gmf"));
                conv.setDescripcion(rs.getString("descripcion"));
                conv.setDescuenta_aval(rs.getBoolean("descuenta_aval"));
                conv.setDescuenta_gmf(rs.getBoolean("descuenta_gmf"));
                conv.setFactura_tercero(rs.getBoolean("factura_tercero"));
                conv.setHc_cxp(rs.getString("hc_cxp"));
                conv.setHc_diferidos(rs.getString("hc_diferidos"));
                conv.setNit_convenio(rs.getString("nit_convenio"));
                conv.setNit_tercero(rs.getString("nit_tercero"));
                conv.setNombre(rs.getString("nombre"));
                conv.setPrefijo_cxp(rs.getString("prefijo_cxp"));
                conv.setPrefijo_diferidos(rs.getString("prefijo_diferidos"));
                conv.setPrefijo_nc_aval(rs.getString("prefijo_nc_aval"));
                conv.setPrefijo_nc_gmf(rs.getString("prefijo_nc_gmf"));
                conv.setPrefijo_negocio(rs.getString("prefijo_negocio"));
                conv.setTasa_interes(rs.getDouble("tasa_interes"));
                conv.setValor_custodia(rs.getDouble("valor_custodia"));
                conv.setImpuesto(rs.getString("impuesto"));                
                conv.setCuenta_ajuste(rs.getString("cuenta_ajuste"));
                conv.setAval_anombre(rs.getBoolean("aval_anombre"));
                conv.setCxp_avalista(rs.getBoolean("cxp_avalista"));
                conv.setNit_anombre(rs.getString("nit_anombre"));
                conv.setPrefijo_cxc_aval(rs.getString("prefijo_cxc_aval"));
                conv.setHc_cxc_aval(rs.getString("hc_cxc_aval"));
                conv.setCuenta_cxc_aval(rs.getString("cuenta_cxc_aval"));
                conv.setCctrl_db_cxc_aval(rs.getString("cctrl_db_cxc_aval"));
                conv.setCctrl_cr_cxc_aval(rs.getString("cctrl_cr_cxc_aval"));
                conv.setCctrl_iva_cxc_aval(rs.getString("cctrl_iva_cxc_aval"));
                conv.setPrefijo_cxp_avalista(rs.getString("prefijo_cxp_avalista"));
                conv.setHc_cxp_avalista(rs.getString("hc_cxp_avalista"));
                conv.setCuenta_cxp_avalista(rs.getString("cuenta_cxp_avalista"));
                conv.setCat(rs.getBoolean("cat"));
                conv.setConvenioComision(this.buscar_comisiones(bd, conv.getId_convenio()));
                conv.setConvenioCxc(this.buscar_cxc(bd, conv.getId_convenio()));
                conv.setConveniosRemesas(this.buscar_remesas(bd, conv.getId_convenio()));
                lista.add(conv);
                conv = null;
            }
        } catch (Exception e) {
            throw new Exception("Error al listar convenios: " + e.toString());
        }
        return lista;
    }

    public ArrayList<String> buscarDatosTercero(String columna, String cadena) throws Exception {
        ArrayList<String> lista = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSC_TER";
        String sql = "";
        try {
            lista = new ArrayList<String>();
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", columna);
            sql = sql.replaceAll("#cadena", cadena);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("cedula") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList<String> buscarDatosCiudad(String columna, String cadena) throws Exception {
        ArrayList<String> lista = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSC_CIU";
        String sql = "";
        try {
            lista = new ArrayList<String>();
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", columna);
            sql = sql.replaceAll("#cadena", cadena);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("codciu") + ";_;" + rs.getString("nomciu"));
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public String datosCodigoUsuario(String codusuario) throws Exception {
        String datos = ";_;";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "DAT_COD";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDIFintra();
            ps = con.prepareStatement(sql);
            ps.setString(1, codusuario);
            rs = ps.executeQuery();
            if (rs.next()) {
                datos = rs.getString("nit") + ";_;" + rs.getString("nombre");
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return datos;
    }

    public boolean existeCuenta (String cuenta) throws Exception {
        boolean sw =false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "EXISTE_CUENTA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, cuenta);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw = true;
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }

    public ArrayList<String> buscarDatosAfil(String columna, String cadena) throws Exception {
        ArrayList<String> lista = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSC_AFIL";
        String sql = "";
        try {
            lista = new ArrayList<String>();
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", columna);
            sql = sql.replaceAll("#cadena", cadena);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("nit") + ";_;" + rs.getString("payment_name") + ";_;" + rs.getString("sede"));
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList<String> datosConveniosSectores(String nitAfil) throws Exception {
        ArrayList<String> lista = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "DAT_CONV_SC";
        String sql = "";
        try {
            lista = new ArrayList<String>();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nitAfil);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("id_prov_convenio") + ";_;" + rs.getString("id_convenio") + ";_;" + rs.getString("cod_sector") + ";_;" + rs.getString("cod_subsector"));
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public String nombreConvenio(String id_convenio) throws Exception {
        String nombre = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "DT_CONV";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombre = rs.getString("nombre");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return nombre;
    }

    public String nombreSector(String cod_sector) throws Exception {
        String nombre = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "DT_SECT";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, cod_sector);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombre = rs.getString("nombre");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return nombre;
    }

    public String nombreSubSector(String cod_sector, String cod_subsector) throws Exception {
        String nombre = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "DT_SSECT";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, cod_sector);
            ps.setString(2, cod_subsector);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombre = rs.getString("nombre");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return nombre;
    }

    public void asignarUsuario(String codusuario, ArrayList<String> convs, String user) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "DEL_FILS";
        String sql = "";
        String cad1 = "";
        String cad2 = "";
        StringStatement st1 = null;
        StringStatement st2 = null;
        try {
            if (convs.size() > 0) {
                String[] split = null;
                String activo = "";
                for (int i = 0; i < convs.size(); i++) {
                    query = "DEL_FILS";
                    split = (convs.get(i)).split(";_;");
                    sql = this.obtenerSQL(query);
                    st1 = new StringStatement(sql, true);
                    st1.setString(1, codusuario);
                    st1.setString(2, split[0]);
                    cad1 += "\n" + st1.getSql();
                    st1 = null;
                    query = "INS_FILS";
                    sql = this.obtenerSQL(query);
                    activo = split[1];
                    activo = activo.equals("N") ? "A" : "";
                    sql = sql.replaceAll("#reg", "'" + activo + "'");
                    st2 = new StringStatement(sql, true);
                    st2.setString(1, codusuario);
                    st2.setString(2, split[0]);
                    st2.setString(3, user);
                    cad2 += "\n" + st2.getSql();
                    st2 = null;
                }
                sql = cad1 + cad2;
                con = this.conectarJNDI(query);
                ps = con.prepareStatement(sql);
                int updr = ps.executeUpdate();
                if (updr < 1) {
                    System.out.println("No se insertaron o borraron filas");
                }
            }
        } catch (Exception e) {
            throw new Exception("Error al ingresar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
    }

    public ArrayList<String> asignacionesUsuarioAfil(String codusuario, String nitafil) throws Exception {
        ArrayList<String> lista = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_DTUPC";
        String sql = "";
        try {
            lista = new ArrayList<String>();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nitafil);
            ps.setString(2, codusuario);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("id_prov_convenio") + ";_;" + rs.getString("id_convenio") + ";_;"
                        + rs.getString("cod_sector") + ";_;" + rs.getString("cod_subsector") + ";_;" + rs.getString("activo"));
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ArrayList<String> listarBancos() throws Exception {
        ArrayList<String> lista = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_BANCOS";
        String sql = "";
        try {
            lista = new ArrayList<String>();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("codigo") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

//darrieta
    public ArrayList<String> buscarBancos(String campo, String valor) throws Exception {
        ArrayList<String> lista = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_BANCOS";
        String sql = "";
        try {
            lista = new ArrayList<String>();
            sql = this.obtenerSQL(query).replaceAll("#param#", campo).replaceAll("#cadena#", valor);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("codigo") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }

    public ConveniosRemesas buscarRemesa(ConveniosRemesas datosRemesa) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_REMESA";
        ConveniosRemesas remesa = null;
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, datosRemesa.getDstrct());
            ps.setString(2, datosRemesa.getId_convenio());
            ps.setString(3, datosRemesa.getCiudad_sede());
            ps.setString(4, datosRemesa.getBanco_titulo());
            ps.setString(5, datosRemesa.getCiudad_titulo());
            rs = ps.executeQuery();
            if (rs.next()) {
                remesa = ConveniosRemesas.load(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarRemesa[GestionConveniosDAO] " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return remesa;
    }

    public Propietario buscarProvConvenio(int idProvConvenio) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_PROV_CONVENIO";
        Propietario prop = null;

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, idProvConvenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                prop = new Propietario();
                prop.setTasa_fenalco(Double.parseDouble(rs.getString("tasa_interes")));
                prop.setCustodiacheque(rs.getDouble("valor_custodia"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarProvConvenio[GestionConveniosDAO] " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return prop;
    }

    public ArrayList<AfiliadoConvenio> buscarConvenios(String nit) throws Exception {
        Connection con = null;
        ArrayList<AfiliadoConvenio> list = new ArrayList();
        AfiliadoConvenio aficonv;
        AfiliadoConvenioRangos aficonvrang;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        String query = "BUSCAR_CONVENIO_NIT";
        String query2 = "BUSCAR_CONVENIO_NIT_RANGOS";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit);
            rs = ps.executeQuery();
            while (rs.next()) {
                aficonv = new AfiliadoConvenio();
                aficonv.load(rs);
                ps2 = null;
                ps2 = con.prepareStatement(this.obtenerSQL(query2));
                ps2.setString(1, aficonv.getIdProvConvenio());
                rs2 = ps2.executeQuery();
                while (rs2.next()) {
                    aficonvrang = new AfiliadoConvenioRangos();
                    aficonvrang.load(rs2);
                    aficonv.getRangos().add(aficonvrang);
                }
                list.add(aficonv);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarConvenios[GestionConveniosDAO] " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return list;
    }

    public ArrayList getPrefijosCXC(String bd) throws Exception {
        ArrayList prefijos = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_GET_PREFIJOS_CXC");
            String sql = this.obtenerSQL("SQL_GET_PREFIJOS_CXC");
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            prefijos = new ArrayList();
            while (rs.next()) {
                String[] prefijo = new String[2];
                prefijo[0] = rs.getString("prefijo_factura");
                prefijo[1] = rs.getString("prefix");
                prefijos.add(prefijo);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return prefijos;
    }

    public String buscarNombreTercero(String nit) throws Exception {
        String nombre = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSC_TER_NAME";
        String sql = "";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nit);
            rs = ps.executeQuery();
            while (rs.next()) {
                nombre= rs.getString("nombre");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return nombre;
    }
    
    
    
     public boolean isAvalTercero (String id_convenio) throws Exception {
        boolean sw =false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "AVAL_TERCERO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw = rs.getBoolean("aval_tercero");
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }
     
     
     /**
     * Busca dato en tablagen
     * @param dato table_type a buscar
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList busquedaGeneral(String dato) throws Exception{
        ArrayList cadena = new ArrayList();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_TBG";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setString(1, dato);
            rs=ps.executeQuery();
            while(rs.next()){
                cadena.add(rs.getString("table_code")+";_;"+rs.getString("dato"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en busqueda general: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return cadena;
    }

    public ArrayList<String> buscarConveniosTipo(String tipo, boolean cat) throws Exception{
        ArrayList<String> lista = new ArrayList<String>();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_CONVENIOS_TIPO";
        String sql = "";
        try{
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setString(1, tipo);
            ps.setBoolean(2, cat);
            rs = ps.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("id_convenio") + ";_;" + rs.getString("nombre"));
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarConveniosTipo: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error buscarConveniosTipo: "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }


      /**
     * Busca la informacion de los formulario de un tipo de convenio
     * @param tipoconv tipo de convenio
     * @param cat indicador de ley mipyme
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList<BeanGeneral> obtenerForm(String tipoconv, boolean cat) throws Exception{
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "FORM_TIPOCONV";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            lista = new ArrayList<BeanGeneral>();
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, tipoconv);
            st.setBoolean(2, cat);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("identificacion"));
                bean.setValor_02(rs.getString("nombre"));
                bean.setValor_03(rs.getString("numero_solicitud"));
                bean.setValor_04(rs.getString("valor_solicitado"));
                bean.setValor_05(rs.getString("id_convenio"));
                bean.setValor_06(rs.getString("nom_convenio"));
                bean.setValor_07(rs.getString("tasa_interes"));
                bean.setValor_08(rs.getString("monto_minimo"));
                bean.setValor_09(rs.getString("monto_maximo"));
                bean.setValor_10(rs.getString("plazo_maximo"));
                bean.setValor_11(rs.getString("renovacion"));
                bean.setValor_12(rs.getString("fecha_primera_cuota"));
                bean.setValor_13(rs.getString("fianza"));
                bean.setValor_14(rs.getString("pre_aprobado_micro"));
                lista.add(bean);
            }
        }
        catch (Exception e) {
            throw new Exception("Error en obtenerForm en GestionConveniosDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Busca la informacion de los montos de un convenio
     * @param id_convenio
     * @return BeanGeneral con los resultados obtenidos
     * @throws Exception
     */
    public BeanGeneral obtenerMontos(String id_convenio) throws Exception{
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SRC_MONTOS_CONVENIO";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, id_convenio);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("monto_minimo"));
                bean.setValor_02(rs.getString("monto_maximo"));

            }
        }
        catch (Exception e) {
            throw new Exception("Error en obtenerMontos en GestionConveniosDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return bean;
    }


    
 /**
     * Busca el la tasa de un proveedor
     * @param convenio
     * @param proveedor
     * @return nombre alterno sector
     * @throws Exception
     */
      public double getTasaProveedor(String convenio, String proveedor) throws Exception {
        double tasa=-1;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_TASA_PROV";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, convenio);
            ps.setString(2, proveedor);
            rs = ps.executeQuery();
            if (rs.next()) {
                tasa= rs.getDouble("tasa_interes");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return tasa;
    }

        public ArrayList<String> buscarProveedores(String columna, String cadena) throws Exception {
        ArrayList<String> lista = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSC_PROVEEDORES";
        String sql = "";
        try {
            lista = new ArrayList<String>();
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", columna);
            sql = sql.replaceAll("#cadena", cadena);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("nit") + ";_;" + rs.getString("payment_name") + ";_;" + rs.getString("sede"));
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return lista;
    }
        
        
        /**
     * Busca el redescuento de un convenio
     * @param id_convenio
     * @return boolean redescuento
     * @throws Exception
     */
    public boolean tieneRedescuento (String id_convenio) throws Exception {
        boolean sw =false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "REDESCUENTO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw = rs.getBoolean("redescuento");
            }
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return sw;
    }
    
    
    /**
     * Busca el nombre alterno del sector
     * @param cod_sector
     * @return nombre alterno sector
     * @throws Exception
     */
      public String nombreAlternoSector(String cod_sector) throws Exception {
        String nombre = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "NOMBRE_ALT_SECT";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, cod_sector);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombre = rs.getString("nombre_alterno");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return nombre;
    }

    /**
     * Busca el nombre alterno del subsector
     * @param cod_sector
     * @param cod_subsector
     * @return nombre alterno subsector
     * @throws Exception
     */
    public String nombreAlternoSubSector(String cod_sector, String cod_subsector) throws Exception {
        String nombre = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "NOMBRE_ALT_SSECT";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, cod_sector);
            ps.setString(2, cod_subsector);
            rs = ps.executeQuery();
            if (rs.next()) {
                nombre = rs.getString("nombre_alterno");
            }
        } catch (Exception e) {
            throw new Exception("Error al buscar datos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return nombre;
    }

       public String getTipoConvenio(String id_convenio) throws Exception {
        String tipo = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "TIPO_CONVENIO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            if (rs.next()) {
                tipo = rs.getString("tipo");
}
        } catch (Exception e) {
            throw new Exception("error: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return tipo;
    }

        public Convenio buscar_convenio(String cod_neg) throws Exception {
        Convenio convenio = new Convenio();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_BUSCAR_CONVENIO_NEG");
            String sql = this.obtenerSQL("SQL_BUSCAR_CONVENIO_NEG");
            ps = con.prepareStatement(sql);
            ps.setString(1, cod_neg);
            rs = ps.executeQuery();
            if (rs.next()) {
                convenio.setConvenio(convenio);
                convenio=convenio.load(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return convenio;
    }


        public Propietario buscarProvConvenio(String nit_proveedor,String id_convenio,String cod_sector,String cod_subsector) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_PROV_CONVENIO_NEGOCIO";
        Propietario prop = null;

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit_proveedor);
            ps.setString(2, id_convenio);
            ps.setString(3, cod_sector);
            ps.setString(4, cod_subsector);
            rs = ps.executeQuery();
            while (rs.next()) {
                prop = new Propietario();
                prop.setTasa_fenalco(Double.parseDouble(rs.getString("tasa_interes")));
                prop.setCustodiacheque(rs.getDouble("valor_custodia"));
                prop.setInicio(rs.getString("id_prov_convenio"));
                 prop.setId(rs.getInt("id_prov_convenio"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarProvConvenio[GestionConveniosDAO] " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return prop;
    }





        public ArrayList buscar_convenios_reactivacion(String tipo) throws Exception {
        ArrayList convenios = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_GET_CONVENIOS_REACTIVACION_NEGOCIOS");
            String sql = this.obtenerSQL("SQL_GET_CONVENIOS_REACTIVACION_NEGOCIOS");
            ps = con.prepareStatement(sql);
            ps.setString(1, tipo);
            rs = ps.executeQuery();
            convenios = rstotbl(rs);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return convenios;
    }


        public String valida_remesa(String id_convenio,String titulo) throws Exception {
       String resp="f";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_VALIDA_REMESA");
            String sql = this.obtenerSQL("SQL_VALIDA_REMESA");
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            ps.setString(2, titulo);
            rs = ps.executeQuery();
            if (rs.next()) {
                resp=rs.getString("genera_remesa");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return resp;
    }

    public ArrayList<ConvenioCxcFiducias> buscar_cxc_fiducia(String bd, String id_convenio, String titulo_valor) throws Exception {
        ArrayList<ConvenioCxcFiducias> convenio_cxc_fiducias = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_BUSCAR_CXC_FIDUCIA");
            String sql = this.obtenerSQL("SQL_BUSCAR_CXC_FIDUCIA");
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            ps.setString(2, titulo_valor);
            rs = ps.executeQuery();
            while (rs.next()) {
                ConvenioCxcFiducias cnv = new ConvenioCxcFiducias();
                cnv.setReg_status(rs.getString("reg_status"));
                cnv.setDstrct(rs.getString("dstrct"));
                cnv.setId_convenio(id_convenio);
                cnv.setTitulo_valor(rs.getString("titulo_valor"));
                cnv.setNit_fiducia(rs.getString("nit_fiducia"));
                cnv.setCreation_user(rs.getString("creation_user"));
                cnv.setUser_update("user_update");
                cnv.setCreation_date(rs.getString("creation_date"));
                cnv.setLast_update(rs.getString("last_update"));
                cnv.setPrefijo_cxc_fiducia(rs.getString("prefijo_cxc_fiducia"));
                cnv.setCuenta_cxc_fiducia(rs.getString("cuenta_cxc_fiducia"));
                cnv.setHc_cxc_fiducia(rs.getString("hc_cxc_fiducia"));
                cnv.setPrefijo_cxc_endoso(rs.getString("prefijo_factura_endoso"));
                cnv.setHc_cxc_endoso(rs.getString("hc_cxc_endoso"));
                cnv.setCuenta_cxc_endoso(rs.getString("cuenta_cxc_endoso"));

                convenio_cxc_fiducias.add(cnv);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return convenio_cxc_fiducias;
    }

    private String insertar_cxc_fiducia(ConvenioCxcFiducias cnv) throws Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("SQL_INSERTAR_CXC_FIDUCIA"), true);
            st.setString(1, cnv.getReg_status());
            st.setString(2, cnv.getDstrct());
            st.setString(3, cnv.getId_convenio());
            st.setString(4, cnv.getTitulo_valor());
            st.setString(5, cnv.getNit_fiducia());
            st.setString(6, cnv.getPrefijo_cxc_fiducia());
            st.setString(7, cnv.getCuenta_cxc_fiducia());
            st.setString(8, cnv.getHc_cxc_fiducia());
            st.setString(9, cnv.getCreation_user());
            st.setString(10, cnv.getPrefijo_cxc_endoso());
            st.setString(11, cnv.getHc_cxc_endoso());
            st.setString(12, cnv.getCuenta_cxc_endoso());

            sql = st.getSql();
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE AVALAR SQL_ELIMINAR_FIDUCIAS" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE SQL_ELIMINAR_FIDUCIAS" + e.getMessage());
                }
            }
        }

        return sql;

    }

    private String insertar_fiducia(ConvenioFiducias cnv) throws Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("SQL_INSERTAR_FIDUCIA"), true);

            st.setString(1, cnv.getReg_status());
            st.setString(2, cnv.getDstrct());
            st.setString(3, cnv.getId_convenio());
            st.setString(4, cnv.getNit_fiducia());
            st.setString(5, cnv.getPrefijo_dif_fiducia());
            st.setString(6, cnv.getCuenta_dif_fiducia());
            st.setString(7, cnv.getHc_dif_fiducia());
            st.setString(8, cnv.getPrefijo_end_fiducia());
            st.setString(9, cnv.getHc_end_fiducia());
            st.setString(10, cnv.getCreation_user());

            sql = st.getSql();
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE SQL_INSERTAR_FIDUCIA" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE SQL_INSERTAR_FIDUCIA" + e.getMessage());
                }
            }
        }
        return sql;
    }

    public String eliminar_fiducias(String id_convenio) throws SQLException, Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("SQL_ELIMINAR_FIDUCIAS"), true);
            st.setString(1, id_convenio);
            sql = st.getSql();

        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE SQL_ELIMINAR_FIDUCIAS" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE SQL_ELIMINAR_FIDUCIAS" + e.getMessage());
                }
            }
        }
        return sql;
    }

    public String eliminar_cxc_fiducias(String id_convenio) throws SQLException, Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("SQL_ELIMINAR_CXC_FIDUCIAS"), true);
            st.setString(1, id_convenio);
            sql = st.getSql();

        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE  SQL_ELIMINAR_CXC_FIDUCIAS" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE SQL_ELIMINAR_CXC_FIDUCIAS" + e.getMessage());
                }
            }
        }
        return sql;
    }

    public String eliminar_cxc(String id_convenio) throws SQLException, Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("SQL_ELIMINAR_CXC"), true);
            st.setString(1, id_convenio);
            sql = st.getSql();

        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE  SQL_ELIMINAR_CXC" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE SQL_ELIMINAR_CXC" + e.getMessage());
                }
            }
        }
        return sql;
    }

    public ArrayList<ConvenioFiducias> buscar_fiducias(String bd, String id_convenio) throws Exception {
        ArrayList<ConvenioFiducias> convenio_fid = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_BUSCAR_FIDUCIAS");
            String sql = this.obtenerSQL("SQL_BUSCAR_FIDUCIAS");
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                ConvenioFiducias cnv = new ConvenioFiducias();
                cnv.setReg_status(rs.getString("reg_status"));
                cnv.setDstrct(rs.getString("dstrct"));
                cnv.setId_convenio(id_convenio);
                cnv.setCreation_user(rs.getString("creation_user"));
                cnv.setUser_update("user_update");
                cnv.setCreation_date(rs.getString("creation_date"));
                cnv.setLast_update(rs.getString("last_update"));
                cnv.setNit_fiducia(rs.getString("nit_fiducia"));
                cnv.setNombre_fiducia(rs.getString("nombre_fiducia"));
                cnv.setPrefijo_dif_fiducia(rs.getString("prefijo_dif_fiducia"));
                cnv.setCuenta_dif_fiducia(rs.getString("cuenta_dif_fiducia"));
                cnv.setHc_dif_fiducia(rs.getString("hc_dif_fiducia"));
                cnv.setPrefijo_end_fiducia(rs.getString("prefijo_end_fiducia"));
                cnv.setHc_end_fiducia(rs.getString("hc_end_fiducia"));


                convenio_fid.add(cnv);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return convenio_fid;
    }

     public ArrayList getFiducias() throws Exception {
        ArrayList fiducias = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_GET_FIDUCIAS");
            String sql = this.obtenerSQL("SQL_GET_FIDUCIAS");
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            fiducias = new ArrayList();
            while (rs.next()) {
                String[] fiducia = new String[2];
                fiducia [0] = rs.getString("nit_fiducia");
                fiducia [1] = rs.getString("nombre_fiducia");
                fiducias.add(fiducia);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return fiducias;
    }
     
    public ArrayList getEntidadRedescuento() throws Exception {
        ArrayList name = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_ENTIDAD_REDESCUENTO");
            String sql = this.obtenerSQL("SQL_ENTIDAD_REDESCUENTO");
            ps = con.prepareStatement(sql);
              name = new ArrayList();
            rs = ps.executeQuery();
           
            while (rs.next()) {
                name.add(rs.getInt("id")+"-"+rs.getString("nombre"));                                
             }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return name;
    }
      
      
       public String getEntidadRedescuentoNom(String id) throws Exception {
        String name = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_ENTIDAD");
            String sql = this.obtenerSQL("SQL_ENTIDAD");
            ps = con.prepareStatement(sql);
                ps.setString(1, id);
         
            rs = ps.executeQuery();
           
            while (rs.next()) {
                name = rs.getString("nombre");                                
             }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return name;
    }
      public String getRedescuentoConvenio(String convenio) throws Exception {
        String name = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_OBTENER_ENTIDAD_REDESCUENTO");
            String sql = this.obtenerSQL("SQL_OBTENER_ENTIDAD_REDESCUENTO");
            ps = con.prepareStatement(sql);
                ps.setString(1, convenio);
         
            rs = ps.executeQuery();
           
            while (rs.next()) {
                name = rs.getString("id");                                
             }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return name;
    } 

    public ArrayList<CmbGeneralScBeans> GetComboGenerico(String Query, String IdCmb, String DescripcionCmb, String wuereCmb) throws Exception {
        
        ArrayList combo = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            con = conectarJNDI(Query);
            String sql = this.obtenerSQL(Query);
            
            ps = con.prepareStatement(sql);
            if ( wuereCmb != "") {
                ps.setString(1, wuereCmb);
            }
            
            rs = ps.executeQuery();
            combo = new ArrayList();
            while (rs.next()) {
                CmbGeneralScBeans cmb = new CmbGeneralScBeans();
                cmb.setIdCmb(rs.getInt(IdCmb));
                cmb.setDescripcionCmb(rs.getString(DescripcionCmb));
                combo.add(cmb);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return combo;
    }
    
    private ArrayList<CargosFijosConvenios> buscar_cargos_fijos(String bd, String id_convenio) throws Exception {
        ArrayList<CargosFijosConvenios> cargosFijosConvenios = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_CARGOS_FIJOS_CONVENIOS");
            String sql = this.obtenerSQL("SQL_CARGOS_FIJOS_CONVENIOS");
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            while (rs.next()) {
                CargosFijosConvenios cjc = new CargosFijosConvenios();
                cjc.setId_convenio(rs.getString("id_convenio"));
                cjc.setDescripcion(rs.getString("descripcion"));
                cjc.setPrefijo(rs.getString("prefijo"));
                cjc.setCuenta_diferido(rs.getString("cuenta_diferido"));
                cjc.setHc_diferido(rs.getString("hc_diferido"));
                cjc.setCuenta(rs.getString("cuenta"));
                cjc.setTipo_calculo(rs.getString("tipo_calculo"));
                cjc.setValor(rs.getDouble("valor"));
                cjc.setActivo(rs.getBoolean("activo"));                        
                cjc.setAplica_iva(rs.getString("aplica_iva"));
                cjc.setEsdiferido(rs.getString("esdiferido"));

                cargosFijosConvenios.add(cjc);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return cargosFijosConvenios;
    }

    public Deducciones buscar_deducciones_convenio(String bd, String id_convenio) throws SQLException {
       Deducciones Deducciones= new Deducciones();
               Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = conectarJNDI("SQL_BUSCAR_DEDUCCIONES");
            String sql = this.obtenerSQL("SQL_BUSCAR_DEDUCCIONES");
            ps = con.prepareStatement(sql);
            ps.setString(1, id_convenio);
            rs = ps.executeQuery();
            if (rs.next()) { 
        Deducciones.setid(rs.getString("id"));
        Deducciones.setreg_status(rs.getString("reg_status"));
        Deducciones.setdstrct(rs.getString("dstrct"));
        Deducciones.setdescripcion(rs.getString("descripcion"));
        Deducciones.setmodalidad(rs.getString("modalidad"));
        Deducciones.setunid_negocio(rs.getString("unid_negocio"));
        Deducciones.setid_convenio(rs.getString("id_convenio"));
        Deducciones.setcreation_date(rs.getString("creation_date"));
        Deducciones.setcreation_user(rs.getString("creation_user"));
        Deducciones.setlast_update(rs.getString("last_update"));
        Deducciones.setuser_update(rs.getString("user_update"));
        Deducciones.setdesembolso_inicial(rs.getDouble("desembolso_inicial"));
        Deducciones.setdesembolso_final(rs.getDouble("desembolso_final"));
        Deducciones.setvalor_cobrar(rs.getDouble("valor_cobrar"));
        Deducciones.setperc_cobrar(rs.getDouble("perc_cobrar"));
                
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return Deducciones;
    }

    private Unidad_Negocio buscarUnidadNegocio(String bd, String id_convenio) throws SQLException {
        Connection con = conectarJNDIFintra();
        String sql = this.obtenerSQL("SQL_BUSCAR_UNIDAD_NEGOCIO");
        Unidad_Negocio un = null;
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, id_convenio);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            un = new Unidad_Negocio();
            un.setId(rs.getInt("id"));
            un.setDescripcion(rs.getString("descripcion"));
            un.setPrefijo_negocio(rs.getString("prefijo_negocio"));
        }
         con.close();
        return un;
        
    }
}
