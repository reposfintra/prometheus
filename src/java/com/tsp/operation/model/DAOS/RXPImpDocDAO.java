/***********************************************************************************
 * Nombre clase : ...............  RXPImpDocDAO.java                                   *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. David Lamadrid                               *
 * Fecha :....................... 15 de noviembre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  dlamadrid
 */
public class RXPImpDocDAO
{
        
        private CXPImpDoc impuestoDoc;
        private Vector vImpuestosDoc;
        /** Creates a new instance of CXPImpDocDAO */
        public RXPImpDocDAO()
        {
        }
        
        /**
         * Getter for property impuestoDoc.
         * @return Value of property impuestoDoc.
         */
        public com.tsp.operation.model.beans.CXPImpDoc getImpuestoDoc ()
        {
                return impuestoDoc;
        }
        
        /**
         * Setter for property impuestoDoc.
         * @param impuestoDoc New value of property impuestoDoc.
         */
        public void setImpuestoDoc (com.tsp.operation.model.beans.CXPImpDoc impuestoDoc)
        {
                this.impuestoDoc = impuestoDoc;
        }
        
        /**
         * Getter for property vImpuestosDoc.
         * @return Value of property vImpuestosDoc.
         */
        public java.util.Vector getVImpuestosDoc ()
        {
                return vImpuestosDoc;
        }
        
        /**
         * Setter for property vImpuestosDoc.
         * @param vImpuestosDoc New value of property vImpuestosDoc.
         */
        public void setVImpuestosDoc (java.util.Vector vImpuestosDoc)
        {
                this.vImpuestosDoc = vImpuestosDoc;
        }
        
        
         
        /**
         * Metodo que genera un Vector de Objetos tipo CXPImpDoc y lo retorna.
         * @autor.......David Lamadrid
         * @param.......Vector de Objetos tipo CXPImpDoc
         * @see........ CXPImpDoc.class
         * @throws.......
         * @version.....1.0.
         * @return......Vector vImpuestos.
         */
        public Vector generarVImpuestosDoc (Vector vImpuestos)
        {
                Vector resultado = new Vector ();
                for ( int i=0;i<vImpuestos.size ();i++)
                {
                        CXPImpDoc impDoc =(CXPImpDoc)vImpuestos.elementAt (i);
                        String cod_impuesto = impDoc.getCod_impuesto ();
                        double valor= impDoc.getVlr_total_impuesto ();
                        double valor_me = impDoc.getVlr_total_impuesto_me ();
                        int ban=0;
                        for(int x=0;x<resultado.size ();x++)
                        {
                                CXPImpDoc impDoc1 =(CXPImpDoc)resultado.elementAt (x);
                                String cod_impuesto1 = impDoc1.getCod_impuesto ();
                                double valor1= impDoc1.getVlr_total_impuesto ();
                                double valor_me1 = impDoc1.getVlr_total_impuesto_me ();
                                if (cod_impuesto.equals (cod_impuesto1))
                                {
                                        valor1=valor1+valor;
                                        valor_me1=valor_me1+valor_me;
                                        impDoc1.setVlr_total_impuesto (valor1);
                                        impDoc1.setVlr_total_impuesto_me (valor_me1);
                                        resultado.setElementAt (impDoc1, x);
                                        ban=1;
                                }
                        }
                        if(ban==0)
                        {
                                resultado.add (impDoc);
                        }
                }
                return resultado;
        }
        
}
