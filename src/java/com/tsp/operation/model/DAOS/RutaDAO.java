/*
 * RutaDAO.java
 *
 * Created on 24 de junio de 2005, 09:59 AM
 */

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.*;

/**
 *
 * @author  Jcuesta
 */
public class RutaDAO  extends MainDAO{
    
    Ruta Ruta;
    Vector Rutas;  
    private Vector vRutas;//David 23.11.05
    
    private static final String SQL_DURACION_TRAMO       = " SELECT TIEMPO FROM TRAMO WHERE DSTRCT = ? AND ORIGIN = ? AND DESTINATION = ? ";
    private static final String SQL_LISTADO_VIAS         = " SELECT ORIGEN, DESTINO, SECUENCIA, VIA  FROM VIA WHERE ORIGEN LIKE ?  AND DESTINO LIKE ? AND ESTADO='A' AND REC_STATUS = '' ORDER BY ORIGEN, DESTINO, SECUENCIA";
    private static final String SQL_UPDATE_DURACION_VIAS = " UPDATE VIA SET TIEMPO = ?   WHERE ORIGEN = ? AND DESTINO = ? AND SECUENCIA = ? AND ESTADO='A' AND REC_STATUS = ''";
    private static final String SQL_UPDATE_DURACION_MAX  = " UPDATE VIA SET DURACION = ? WHERE ORIGEN = ? AND DESTINO = ? AND ESTADO='A' AND REC_STATUS = ''";
    /** Creates a new instance of RutaDAO */
    public RutaDAO() {
        super("RutaDAO.xml");
    }
    
    
    public void agregarRuta()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                ///calular tiempo de la via
                double d = getDuracion(con, Ruta.getVia());
                ////System.out.println(" DURACION VIA: " + d);
                              
                st = con.prepareStatement("insert into via (rec_status, cia, origen, destino, secuencia, via,user_update,creation_user,pais_o,pais_d,descripcion, tiempo, duracion, base) values ('',?,?,?,?,?,?,?,?,?,?,?,?,?)");
                st.setString(1, Ruta.getCia());
                st.setString(2, Ruta.getOrigen());
                st.setString(3, Ruta.getDestino());
                st.setString(4, Ruta.getSecuencia());
                st.setString(5, Ruta.getVia());
                st.setString(6, Ruta.getUsuario());
                st.setString(7, Ruta.getUsuario());
                st.setString(8, Ruta.getPais_o());
                st.setString(9, Ruta.getPais_d());
                st.setString(10, Ruta.getDescripcion());
                st.setDouble(11, d);
                st.setDouble(12, Ruta.getDuracion());
                st.setString(13, Ruta.getBase());
                st.executeUpdate();
                
                ///actuliza duracion con origen y destino igual
                 ActualizacionTiemposEnVia(Ruta.getOrigen(),Ruta.getDestino());
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL Ruta " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void anularRuta()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update via set rec_status='A', estado = 'I', lastupdate = 'now()', user_update = ? where cia = ? and origen = ? and destino = ? and secuencia=?");
                st.setString(1, Ruta.getUsuario());
                st.setString(2, Ruta.getCia());
                st.setString(3, Ruta.getOrigen());
                st.setString(4, Ruta.getDestino());
                st.setString(5, Ruta.getSecuencia());
                st.executeUpdate();
                
                ///actuliza duracion con origen y destino igual
                ActualizacionTiemposEnVia(Ruta.getOrigen(),Ruta.getDestino());
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL Ruta " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void modificarRuta()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                
                ///calular tiempo de la via
                double d = getDuracion(con, Ruta.getVia());
                ////System.out.println(" DURACION VIA: " + d);
                
                st = con.prepareStatement("update via set via=?, lastupdate = 'now()', user_update = ?, tiempo=?, descripcion=? where cia = ? and origen = ? and destino = ? and secuencia=?");
                st.setString(1, Ruta.getVia());
                st.setString(2, Ruta.getUsuario());
                st.setDouble(3, d);
                st.setString(4, Ruta.getDescripcion());
                st.setString(5, Ruta.getCia());                
                st.setString(6, Ruta.getOrigen());
                st.setString(7, Ruta.getDestino());
                st.setString(8, Ruta.getSecuencia());
                
                st.executeUpdate();
                
                ///actuliza duracion con origen y destino igual
                 ActualizacionTiemposEnVia(Ruta.getOrigen(),Ruta.getDestino());
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACION DEL Ruta " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void listarRutas()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select  t.*,  " +
                "	ciuo.nomciu as nomorigen, " +
                "       ciud.nomciu as nomdest, " +
                "       po.country_name as po, " +
                "       pd.country_name as pd " +
                "     from    via t" +
                "			left outer join  ciudad ciud on (ciud.codciu = t.destino)                 " +
                "			left outer join  ciudad ciuo on (ciuo.codciu = t.origen)                 " +
                "			left outer join  pais pd on (pd.country_code = ciud.pais )                " +
                "			left outer join  pais po on (po.country_code = ciuo.pais )" +
                "    where   t.rec_status = ''");
                rs= st.executeQuery();
                
                ////se obtienen las ciudades del sistema -> treemap
                PreparedStatement st1 = con.prepareStatement("select * from ciudad");
                ResultSet rs1 = st1.executeQuery();
                TreeMap ciudades = new TreeMap();
                while (rs1.next()){
                    ciudades.put(rs1.getString("codciu"), rs1.getString("nomciu"));
                }         
                                
                Rutas = new Vector();
                while(rs.next()){
                    Ruta = new Ruta();
                    Ruta.setCia(rs.getString("cia"));
                    Ruta.setNcia(rs.getString("descripcion"));
                    Ruta.setNorigen(rs.getString("nomorigen"));
                    Ruta.setNdestino(rs.getString("nomdest"));
                    Ruta.setOrigen(rs.getString("origen"));
                    Ruta.setDestino(rs.getString("destino"));
                    Ruta.setSecuencia(rs.getString("secuencia"));
                    Ruta.setVia(rs.getString("Via"));
                    Ruta.setNvia(armarRutaxNombre(rs.getString("Via"), ciudades, ""));
                    Ruta.setPais_d(rs.getString("pd"));
                    Ruta.setPais_o(rs.getString("po"));
                    
                    Rutas.add(Ruta);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL Ruta " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void consultarRutas()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select distinct 	t.*," +                
                "	co.nomciu as nomorigen," +
                "	cd.nomciu as nomdest," +
                "       po.country_name as po," +
                "       pd.country_name as pd" +
                " from   	via t," +                
                "        ciudad co," +
                "	ciudad cd," +
                "       pais po," +
                "       pais pd" +
                " where  	" +                
                "	co.nomciu like ? and" +
                "	cd.nomciu like ? and" +
                "	co.codciu = t.origen and" +
                "	cd.codciu = t.destino and" +
                "       t.rec_status = ''" +
                "      and po.country_code = co.pais" +
                "      and pd.country_code = cd.pais");
                st.setString(1, Ruta.getOrigen()+"%");
                st.setString(2, Ruta.getDestino()+"%");
                ////System.out.println("ST RUTA:"+st);
                rs= st.executeQuery();
                
                ////se obtienen las ciudades del sistema -> treemap
                PreparedStatement st1 = con.prepareStatement("select * from ciudad");
                ResultSet rs1 = st1.executeQuery();
                TreeMap ciudades = new TreeMap();
                while (rs1.next()){
                    ciudades.put(rs1.getString("codciu"), rs1.getString("nomciu"));
                }         
                
                Rutas = new Vector();
                while(rs.next()){
                    Ruta = new Ruta();
                    Ruta.setCia(rs.getString("cia"));
                    Ruta.setNcia(rs.getString("descripcion"));
                    Ruta.setNorigen(rs.getString("nomorigen"));
                    Ruta.setNdestino(rs.getString("nomdest"));
                    Ruta.setOrigen(rs.getString("origen"));
                    Ruta.setDestino(rs.getString("destino"));
                    Ruta.setSecuencia(rs.getString("secuencia"));
                    Ruta.setVia(rs.getString("Via"));
                    Ruta.setNvia(armarRutaxNombre(rs.getString("Via"), ciudades, ""));
                    Ruta.setPais_d(rs.getString("pd"));
                    Ruta.setPais_o(rs.getString("po"));
                    Rutas.add(Ruta);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LA Ruta " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void buscarRuta(String cia, String origen, String destino, String secuencia)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select 	t.*," +                
                "	co.nomciu as norigen," +
                "	cd.nomciu as ndestino," +
                "       po.country_name as po," +
                "       pd.country_name as pd" +
                " from 	via t," +                
                "	ciudad co," +
                "	ciudad cd," +
                "       pais po," +
                "       pais pd" +
                " where t.rec_status = '' " +
                "	and t.cia = ? " +
                "	and t.origen = ? " +
                "	and t.destino = ?" +                
                "	and co.codciu = t.origen" +
                "	and cd.codciu = t.destino" +
                "       and t.secuencia = ?" +
                "       and po.country_code = co.pais" +
                "       and pd.country_code = cd.pais");
                st.setString(1,cia);
                st.setString(2,origen);
                st.setString(3,destino);
                st.setString(4,secuencia);////System.out.println("-----" + st);
                rs= st.executeQuery();
                
                ////se obtienen las ciudades del sistema -> treemap
                PreparedStatement st1 = con.prepareStatement("select * from ciudad");
                ResultSet rs1 = st1.executeQuery();
                TreeMap ciudades = new TreeMap();
                while (rs1.next()){
                    ciudades.put(rs1.getString("codciu"), rs1.getString("nomciu"));
                }   
                
                if(rs.next()){
                    Ruta = new Ruta();
                    Ruta.setCia(rs.getString("cia"));
                    Ruta.setNcia(rs.getString("descripcion"));
                    Ruta.setOrigen(rs.getString("origen"));
                    Ruta.setNorigen(rs.getString("norigen"));
                    Ruta.setNdestino(rs.getString("ndestino"));
                    Ruta.setDestino(rs.getString("destino"));
                    Ruta.setSecuencia(rs.getString("secuencia"));
                    Ruta.setVia(rs.getString("Via"));
                     //double d = getDuracion(con, rs.getString("Via"));////System.out.println(" DURACIOOOOOON VIAAA: " + d);
                    Ruta.setNvia(armarRutaxNombre(rs.getString("Via"), ciudades, "texto"));
                    Ruta.setTvia(armarRutaxCodigo(rs.getString("Via")));
                    Ruta.setPais_d(rs.getString("pd"));
                    Ruta.setPais_o(rs.getString("po"));
                    Ruta.setTiempo(rs.getDouble("tiempo"));
                    Ruta.setDuracion(rs.getDouble("duracion"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL Ruta " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    
    public String ultimaRuta(String cia, String origen, String destino)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sw = "1";//no existe
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  max(secuencia::numeric)+100 as ult  from via where cia = ? and origen = ? and destino = ? ");
                st.setString(1,cia);
                st.setString(2,origen);
                st.setString(3,destino);
                rs = st.executeQuery();
                if(rs.next()){
                    sw = rs.getString("ult");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL Ruta " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    public boolean existeRuta(String cia, String origen, String destino)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;//no existe
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from via where cia = ? and origen = ? and destino = ? ");
                st.setString(1,cia);
                st.setString(2,origen);
                st.setString(3,destino);
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;//si existe
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL Ruta " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
        
        
    public String armarRutaxNombre( String via, TreeMap ciudades, String tipo ) throws SQLException {
        String ruta  ="";        
        int j = 0;            
        ruta  ="";
        String par = "";  
        String sep = (tipo=="texto")?"\n":"<br>";

        while (j < via.length()){                                
            par = via.substring(j, j+2);                 
                    
            ruta = ruta + ciudades.get(par) + sep;
            ////System.out.println("RUTA " + ruta);                                        
            j = j+2;                               
        }
        return ruta;
    }
    
    public String armarRutaxCodigo( String via ) {
        int j = 0;
        String tvia = "";
        while (j < via.length()){
            String par = via.substring(j, j+2);
            tvia = tvia + par;
            if ( j != via.length()-2){
                tvia = tvia + "\n";
            }
            j = j+2;
            ////System.out.println(" ---- " + tvia);
        }
        return tvia;
    }
    /**
     * Getter for property Ruta.
     * @return Value of property Ruta.
     */
    public com.tsp.operation.model.beans.Ruta getRuta() {
        return Ruta;
    }    
    
    /**
     * Setter for property Ruta.
     * @param Ruta New value of property Ruta.
     */
    public void setRuta(com.tsp.operation.model.beans.Ruta Ruta) {
        this.Ruta = Ruta;
    }
    
    /**
     * Getter for property Rutas.
     * @return Value of property Rutas.
     */
    public java.util.Vector getRutas() {
        return Rutas;
    }
    
    /**
     * Setter for property Rutas.
     * @param Rutas New value of property Rutas.
     */
    public void setRutas(java.util.Vector Rutas) {
        this.Rutas = Rutas;
    }
    
    ///140905
    
    private static double getDuracion(Connection Conexion, String Origen, String Destino) throws SQLException{
        PoolManager       poolManager = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        boolean liberar   = false;
        
        
        // variable de retorno
        double     duracion  = 0;
        
        if (Conexion == null){
            poolManager = PoolManager.getInstance();
            Conexion    = poolManager.getConnection("fintra");
            liberar     = true;
        }
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
            st = Conexion.prepareStatement(SQL_DURACION_TRAMO);
            st.setString(1,  "FINV");
            st.setString(2,  Origen);
            st.setString(3,  Destino);
            rs = st.executeQuery();
            while (rs.next()){
                duracion = rs.getDouble(1);
                break;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina getDuracion [xxxxxDAO]... "+e.getMessage());
        }
        finally {
            if (rs!=null) rs.close();
            if (st!=null) st.close();
            if (liberar)  poolManager.freeConnection("fintra",Conexion);
        }
        return duracion;
    }
    
    
    /**
     * Sobre Carga Funcion que recorre todos los tramos de una via y al final obtiene
     * la duracion del recorrido general
     * Parametros : Conexion (Opcional)
     *              Via
     **/
    
    
    private static double getDuracion(Connection Conexion, String via)throws SQLException{
        PoolManager       poolManager = null;
        boolean liberar   = false;
        
        // variable de retorno
        double     duracion  = 0;
        
        if (Conexion == null){
            poolManager = PoolManager.getInstance();
            Conexion    = poolManager.getConnection("fintra");
            liberar     = true;
        }
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        
        try{
            for (int i=0; i < via.length() -2 ; i += 2 ){
                String tOrigen  = via.substring( i  , i+2 );
                String tDestino = via.substring( i+2, i+4 );
                duracion += getDuracion(Conexion, tOrigen, tDestino);
            }
        }catch (Exception ex){
            throw new SQLException("Error en la rutina Duracion [xxxxxDAO] : " + ex.getMessage() );
        }
        finally {
            if (liberar)  poolManager.freeConnection("fintra",Conexion);
        }////System.out.println(" DURACION " + duracion);
        return duracion;
    } 
    
    /**
     * Procedimiento que obtiene en primera instancia un listado general de todas
     * las vias disponibles en la base de datos relacionadas a un origen y un destino
     * ,posteriormente a cada una le calcula la duracion acumulada de sus tramos
     * para luego actualizarlas en la misma tabla
     **/
    
    public static void ActualizacionTiemposEnVia(String Origen, String Destino) throws SQLException{
        
        PoolManager  poolManager = PoolManager.getInstance();
        Connection   Conexion    = poolManager.getConnection("fintra");
        
        PreparedStatement st = null , stUpdate = null;
        ResultSet         rs           = null;
        
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
            
            /**
             * Extraemos un listado general de las vias
             * que vamos a actualizar
             **/
            
            
            st = Conexion.prepareStatement(SQL_LISTADO_VIAS);
            st.setString(1, Origen );
            st.setString(2, Destino);
            rs = st.executeQuery();
            
            
            /**
             * Varables para calcular la duracion maxima entre un
             * origen y un destino una sola vez
             **/
            
            String Tmp = "";
            double DuracionMax = 0;
            
            /*
             * Inicio del proceso
             */
            while (rs.next()){
                
                /**
                 * Obtenemos los datos generales de la via
                 **/
                
                String _Origen    = rs.getString(1);
                String _Destino   = rs.getString(2);
                String _Secuencia = rs.getString(3);
                String _Via       = rs.getString(4);
                
                /**
                 * Calculamos la duracion de la via
                 **/
                
                double Duracion  = getDuracion(Conexion, _Via);
                
                
                /**
                 * Actualizamos en el mismo momento la duracion
                 * acumulada cada tramo de la via en la tabla de vias
                 **/
                
                stUpdate = Conexion.prepareStatement(SQL_UPDATE_DURACION_VIAS);
                stUpdate.setDouble(1, Duracion   );
                stUpdate.setString(2, _Origen                    );
                stUpdate.setString(3, _Destino                   );
                stUpdate.setString(4, _Secuencia                 );
                stUpdate.executeUpdate();
                
                /*
                 * Actualizamos la duracion maxima
                 **/
                
                if ( Tmp.equals("") || (Tmp.equals(_Origen  + _Destino) &&  Duracion > DuracionMax) )
                    
                    DuracionMax  = Duracion;
                    
                /**
                 * Actualizamos el tiempo maximo de las secuencias
                 **/
                
                else if (!Tmp.equals("") && !Tmp.equals(_Origen  + _Destino)){
                    
                    stUpdate = Conexion.prepareStatement(SQL_UPDATE_DURACION_MAX);
                    stUpdate.setDouble(1, DuracionMax);
                    stUpdate.setString(2, Tmp.substring(0,2)         );
                    stUpdate.setString(3, Tmp.substring(2,4)         );
                    stUpdate.executeUpdate();
                    
                    DuracionMax = Duracion;
                    
                    
                }
                
                Tmp = _Origen  + _Destino;
            }
            
            /**
             * Actualizamos el tiempo maximo de las secuencias
             **/           
            
            if (!Tmp.equals("")){
                
                stUpdate = Conexion.prepareStatement(SQL_UPDATE_DURACION_MAX);
                stUpdate.setDouble(1, DuracionMax);
                stUpdate.setString(2, Tmp.substring(0,2)         );
                stUpdate.setString(3, Tmp.substring(2,4)         );
                stUpdate.executeUpdate();
            }
            
            
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina ActualizacionTiemposEnVia ... "+e.getMessage());
        }
        finally {
            if (rs!=null)       rs.close();
            if (st!=null)       st.close();
            if (stUpdate!=null) stUpdate.close();
            if (Conexion!=null) poolManager.freeConnection("fintra",Conexion);
        }
    }
    
    private static final String sqlClientesPorNombre = "select distinct v.origen,v.destino,v.secuencia,v.descripcion ,c.nomciu, get_nombreciudad(v.destino) as ciudadDestino from via as v,ciudad as c where  c.codciu=v.origen and c.nomciu like UPPER(?) ORDER BY v.descripcion";
    
    /**
     * Metodo vClientes , Metodo que retorna un Vector de Vectores con las rutas dado un String con la ciudad de origen de la ruta
     * @autor : Ing. David Lamadrid
     * @param : String nombre
     * @version : 1.0
     */
    public void vClientes(String nombre) throws SQLException
        {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                PoolManager poolManager = null;
                this.vRutas = new Vector ();
                try
                {
                        poolManager = PoolManager.getInstance ();
                        con = poolManager.getConnection ("fintra");
                        if (con != null)
                        {
                                st = con.prepareStatement (this.sqlClientesPorNombre);
                                st.setString (1, nombre+"%");
                             
                                ////System.out.println ("Query: "+st.toString ());
                                rs = st.executeQuery ();
                                while(rs.next ())
                                {
                                    
                                     Vector ruta = new Vector();
                                     ruta.add (rs.getString ("origen"));
                                     ruta.add (rs.getString ("destino"));
                                     ruta.add (rs.getString ("secuencia"));
                                     ruta.add (rs.getString ("nomciu"));
                                     ruta.add (rs.getString ("ciudadDestino"));
                                     ruta.add (rs.getString ("descripcion"));
                                     vRutas.add (ruta);
                                }
                        }
                }catch(SQLException e)
                {
                        throw new SQLException ("ERROR AL INSERTAR EL ESTADO" + e.getMessage ()+"" + e.getErrorCode ());
                }finally
                {
                        if(st != null)
                        {
                                try
                                {
                                        st.close ();
                                }
                                catch(SQLException e)
                                {
                                        throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                                }
                        }
                        if ( con != null)
                        {
                                poolManager.freeConnection ("fintra", con );
                        }
                }
        }
    
    /**
     * Getter for property vRutas.
     * @return Value of property vRutas.
     */
    public java.util.Vector getVRutas ()
    {
            return vRutas;
    }
    
    /**
     * Setter for property vRutas.
     * @param vRutas New value of property vRutas.
     */
    public void setVRutas (java.util.Vector vRutas)
    {
            this.vRutas = vRutas;
    }
     /**
     * metodo que obtine las vias vias con origen y destino definido
     * y verifica que no se repita la ruta para una via
     * @autor Ing. SEscalante
     * @throws SQLException
     * @param origen y destino de la via (String), ruta (String)
     * @return boolean
     * @version 1.0
     */
    public boolean verificarRuta(String origen, String destino, String nueva_ruta) throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        boolean resp = false;

        try {
            st = this.crearPreparedStatement("SQL_OBTENER_VIAS_OD");
            st.setString(1, origen);
            st.setString(2, destino);
            rs = st.executeQuery();

            while(rs.next()){
                if (nueva_ruta.equalsIgnoreCase(rs.getString("via"))){
                    resp = true;
                    break;
                }
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina verificarRuta [RutaDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_OBTENER_VIAS_OD");
        }
        return resp;
    }
    /**
     * M�todo que obtiene las ruta de una via
     * @autor Ing. David Pi�a Lopez
     * @throws SQLException
     * @param origen Origen de la via
     * @version 1.0
     */
    public void getRutaOrigen( String origen ) throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        
        try {
            st = this.crearPreparedStatement( "SQL_OBTENER_VIA_ORIGEN" );
            st.setString( 1, origen );            
            rs = st.executeQuery();
            Rutas = new Vector();
            while(rs.next()){
                Ruta r = new Ruta();
                r.setOrigen( rs.getString( "origen" ) );
                r.setDestino( rs.getString( "destino" ) );
                r.setDescripcion( rs.getString( "descripcion" ) );
                r.setSecuencia( rs.getString( "secuencia" ) );
                r.setVia( rs.getString( "via" ) );
                r.setTvia( rs.getString( "oid" ) );
                Rutas.add( r );
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina getRutaOrigen [RutaDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( "SQL_OBTENER_VIA_ORIGEN" );
        }        
    }
    /**
     * M�todo que obtiene las ruta de una via
     * @autor Ing. David Pi�a Lopez
     * @throws SQLException
     * @param origen Origen de la via
     * @version 1.0
     */
    public void armarVias( String via ) throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        
        try {
            vRutas = new Vector();
            st = this.crearPreparedStatement( "SQL_OBTENER_DESCRIPCION" );
            
            for( int i = 0; i < via.length(); i += 2 ){
                Ruta r = new Ruta();
                String origen  = via.substring( i, i+2 );
                st.setString( 1, origen );
                rs = st.executeQuery();
                if( rs.next() ){
                    r.setOrigen( origen );
                    r.setDescripcion( rs.getString( "nombreciudad" ) );
                   // ////System.out.println( "Origen: " + origen + " - " + rs.getString( "nombreciudad" ) );
                    vRutas.add( r );
                }
                
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina armarVias [RutaDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( "SQL_OBTENER_DESCRIPCION" );
        }        
    }
    public static void main( String args[] ) throws Exception {
        RutaDAO rd = new RutaDAO();
        rd.armarVias( "3FHQJFJBMMW8EU4KQRPRSWSNSBBQDXKNDX4OMK2IR3PA" );
        Vector vec = rd.getVRutas();
        for( int i = 0; i < vec.size(); i++ ){
            Ruta rut = (Ruta)vec.get( i );
            ////System.out.println( "Origen: " + rut.getOrigen() + " - " + rut.getDescripcion() );
        }
    }
    
    /**
     * metodo que obtine las vias vias con origen y destino definido
     * y verifica que no se repita la ruta para una via
     * @autor Ing. Jose de la rosa
     * @throws SQLException
     * @param origen y destino de la via (String), ruta (String)
     * @return boolean
     * @version 1.0
     */
    public boolean verificarRutaSecuencia(String origen, String destino, String nueva_ruta) throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        boolean resp = false;

        try {
            st = this.crearPreparedStatement("SQL_OBTENER_VIAS_SEC");
            st.setString(1, origen);
            st.setString(2, destino);
            st.setString(3, nueva_ruta);
            rs = st.executeQuery();
            resp = rs.next();
        }
        catch(Exception e) {
            throw new Exception("Error en rutina verificarRuta [RutaDAO.xml]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_OBTENER_VIAS_SEC");
        }
        return resp;
    }
}