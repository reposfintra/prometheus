/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.AuditoriaIngresos;
import java.util.ArrayList;

/**
 *
 * @author lcanchila
 */
public interface AuditoriaIngresosDAO {

    public ArrayList<AuditoriaIngresos> cargarAuditoriaIngresos(int periodo, String user);

    public ArrayList<AuditoriaIngresos> cargarReporteCosechas(String periodoIni, String periodoFin, String[] listUnd);

    public ArrayList<AuditoriaIngresos> consultarDetalleCosecha(String periodoIni, String periodoFin, String und_negocio, String venc_mayor);

    public ArrayList<AuditoriaIngresos> cargarCuentasCaidas(String periodo, String und_negocio);

    public ArrayList<AuditoriaIngresos> detalleCuentasCaidas(String periodo, String und_negocio, String venc_mayor);

}
