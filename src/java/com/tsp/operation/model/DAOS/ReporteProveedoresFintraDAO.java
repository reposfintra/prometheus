/*********************************************************************************
 * Nombre clase :      ReporteProveedoresFintraDAO.java                          *
 * Descripcion :       DAO del ReporteProveedoresFintraDAO.java                  *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  *
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             LREALES                                                   *
 * Fecha :             12 de septiembre de 2006, 10:00 AM                        *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;

public class ReporteProveedoresFintraDAO extends MainDAO {
    
    private Movpla bean;
    
    private Vector vector;
    private Vector vector_nits;
    
    /** Creates a new instance of ReporteProveedoresFintraDAO */
    public ReporteProveedoresFintraDAO () {
        
        super ( "ReporteProveedoresFintraDAO.xml" );
        
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector () {
        
        return vector;
        
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector ( java.util.Vector vector ) {
        
        this.vector = vector;
        
    }
    
    /**
     * Getter for property vector_nits.
     * @return Value of property vector_nits.
     */
    public java.util.Vector getVector_nits () {
        
        return vector_nits;
        
    }
    
    /**
     * Setter for property vector_nits.
     * @param vector_nits New value of property vector_nits.
     */
    public void setVector_nits ( java.util.Vector vector_nits ) {
        
        this.vector_nits = vector_nits;
        
    }
    
    /** 
     * Funcion publica que obtiene la información que se va a generar
     * en el reporte de todos los proveedores. 
     */
    public void reporteEspecifico ( String nit_prov, String fecini, String fecfin ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MOVPLA_ESPECIFICO";
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, nit_prov );
            st.setString( 2, fecini );
            st.setString( 3, fecfin );
            
            rs = st.executeQuery();
            
            this.vector = new Vector();
            
            while( rs.next() ){
                
                String agencia = rs.getString( "agencia" )!=null?rs.getString( "agencia" ).toUpperCase():"";
                String cheque = rs.getString( "cheque" )!=null?rs.getString( "cheque" ).toUpperCase():"";
                String propietario = rs.getString( "propietario" )!=null?rs.getString( "propietario" ).toUpperCase():"";                
                String planilla = rs.getString( "planilla" )!=null?rs.getString( "planilla" ).toUpperCase():"";
                String placa = rs.getString( "placa" )!=null?rs.getString( "placa" ).toUpperCase():"";
                String fecha = rs.getString( "fecha" );
                float descuento = rs.getFloat( "descuento" );
                float vlr = rs.getFloat( "vlr" );
                float vlr_for = rs.getFloat( "vlr_for" );                
                String moneda = rs.getString( "moneda" )!=null?rs.getString( "moneda" ).toUpperCase():"";
                String banco = rs.getString( "banco" )!=null?rs.getString( "banco" ).toUpperCase():"";
                String sucursal = rs.getString( "sucursal" )!=null?rs.getString( "sucursal" ).toUpperCase():"";                
                String nit_proveedor = rs.getString( "nit_proveedor" )!=null?rs.getString( "nit_proveedor" ).toUpperCase():"";
                String proveedor = rs.getString( "proveedor" )!=null?rs.getString( "proveedor" ).toUpperCase():"";
                String creation_date = rs.getString( "creation_date" )!=null?rs.getString( "creation_date" ).toUpperCase():"";
                String usuario = rs.getString( "usuario" )!=null?rs.getString( "usuario" ).toUpperCase():"";
                String origen = rs.getString( "origen" )!=null?rs.getString( "origen" ).toUpperCase():"";
                String destino = rs.getString( "destino" )!=null?rs.getString( "destino" ).toUpperCase():"";
                
                float tasa = 0;
                if ( vlr == 0 ) {
                    tasa = 0;
                } else {
                    tasa = ( vlr_for / vlr );
                }
                
                bean = new Movpla ();
                
                bean.setAgency_id ( agencia );
                bean.setDocument ( cheque );
                bean.setPla_owner( propietario );
                bean.setPlanilla( planilla );
                bean.setSupplier( placa );
                bean.setDate_doc( fecha );
                bean.setVlr_disc( descuento );// float
                bean.setVlr( vlr );// float
                bean.setVlr_for( vlr_for );// float
                bean.setCurrency( moneda );
                bean.setBranch_code( banco );
                bean.setBank_account_no( sucursal );
                
                bean.setApplicated_ind( fecini );// fecini
                bean.setApplication_ind( fecfin );// fecfin
                
                bean.setProveedor_anticipo( nit_proveedor );
                bean.setProveedor( proveedor );
                bean.setCreation_date( creation_date );
                bean.setCreation_user( usuario );
                bean.setObservacion( origen );// origen
                bean.setDocument_type( destino );// destino
                bean.setTipo_rec( "" + UtilFinanzas.customFormat ("#,###.##########",  tasa ,10 ) );// tasa
                
                this.vector.add( bean );
                
            }
            
        }} catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'reporteEspecifico()' - [ReporteProveedoresFintraDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /** 
     * Funcion publica que obtiene la información que se va a generar
     * en el reporte de todos los proveedores. 
     */
    public void reporteTodos ( String fecini, String fecfin ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MOVPLA_TODOS";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, fecini );
            st.setString( 2, fecfin );
            rs = st.executeQuery();
            this.vector = new Vector();
            
            while( rs.next() ){
                
                String agencia = rs.getString( "agencia" )!=null?rs.getString( "agencia" ).toUpperCase():"";
                String cheque = rs.getString( "cheque" )!=null?rs.getString( "cheque" ).toUpperCase():"";
                String propietario = rs.getString( "propietario" )!=null?rs.getString( "propietario" ).toUpperCase():"";                
                String planilla = rs.getString( "planilla" )!=null?rs.getString( "planilla" ).toUpperCase():"";
                String placa = rs.getString( "placa" )!=null?rs.getString( "placa" ).toUpperCase():"";
                String fecha = rs.getString( "fecha" );
                float descuento = rs.getFloat( "descuento" );
                float vlr = rs.getFloat( "vlr" );
                float vlr_for = rs.getFloat( "vlr_for" );                
                String moneda = rs.getString( "moneda" )!=null?rs.getString( "moneda" ).toUpperCase():"";
                String banco = rs.getString( "banco" )!=null?rs.getString( "banco" ).toUpperCase():"";
                String sucursal = rs.getString( "sucursal" )!=null?rs.getString( "sucursal" ).toUpperCase():"";                
                String nit_proveedor = rs.getString( "nit_proveedor" )!=null?rs.getString( "nit_proveedor" ).toUpperCase():"";
                String proveedor = rs.getString( "proveedor" )!=null?rs.getString( "proveedor" ).toUpperCase():"";
                String creation_date = rs.getString( "creation_date" )!=null?rs.getString( "creation_date" ).toUpperCase():"";
                String usuario = rs.getString( "usuario" )!=null?rs.getString( "usuario" ).toUpperCase():"";
                String origen = rs.getString( "origen" )!=null?rs.getString( "origen" ).toUpperCase():"";
                String destino = rs.getString( "destino" )!=null?rs.getString( "destino" ).toUpperCase():"";
                
                float tasa = 0;
                if ( vlr == 0 ) {
                    tasa = 0;
                } else {
                    tasa = ( vlr_for / vlr );
                }
                
                bean = new Movpla ();
                
                bean.setAgency_id ( agencia );
                bean.setDocument ( cheque );
                bean.setPla_owner( propietario );
                bean.setPlanilla( planilla );
                bean.setSupplier( placa );
                bean.setDate_doc( fecha );
                bean.setVlr_disc( descuento );// float
                bean.setVlr( vlr );// float
                bean.setVlr_for( vlr_for );// float
                bean.setCurrency( moneda );
                bean.setBranch_code( banco );
                bean.setBank_account_no( sucursal );
                
                bean.setApplicated_ind( fecini );// fecini
                bean.setApplication_ind( fecfin );// fecfin
                
                bean.setProveedor_anticipo( nit_proveedor );
                bean.setProveedor( proveedor );
                bean.setCreation_date( creation_date );
                bean.setCreation_user( usuario );
                bean.setObservacion( origen );// origen
                bean.setDocument_type( destino );// destino
                bean.setTipo_rec( "" + tasa );// tasa
                
                this.vector.add( bean );
                
            }
            
        }}catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'reporteTodos()' - [ReporteProveedoresFintraDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /** 
     * Funcion publica que obtiene la lista de los nits y nombres de proveedores
     */
    public void proveedores ( String nom_prov ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_PROVEEDOR";
        try {
            con = this.conectarJNDI(query);
           if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, nom_prov + "%" );
            rs = st.executeQuery();
            this.vector_nits = new Vector();
            
            while( rs.next() ){
                
                Vector info = new Vector();
                
                String nit = rs.getString( "nit" )!=null?rs.getString( "nit" ).toUpperCase():"";
                String nombre = rs.getString( "payment_name" )!=null?rs.getString( "payment_name" ).toUpperCase():"";
                
                info.add( nit );
                info.add( nombre );
                                                
                this.vector_nits.add( info );
                
            }
            
        }}catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'proveedores()' - [ReporteProveedoresFintraDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
}
