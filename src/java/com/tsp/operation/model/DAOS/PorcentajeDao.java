/******************************************************************
* Nombre ......................PorcentajeDAO.java
* Descripci�n..................clase que maneja las consultas
* Autor........................Andres Martinez
* Fecha........................21 julio 2006
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/


package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import javax.servlet.*;
import com.tsp.util.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.OpDAO;
import com.tsp.operation.model.DAOS.MainDAO;

public class PorcentajeDao extends MainDAO {
    String PlanillasPostgres;
    /** Creates a new instance of PorcentajeDao */
    public PorcentajeDao() {
         super("PorcentajeDAO.xml");
        
    }
    Vector vPostgres;
    Vector vOracle;
    String fecha1="", fecha2="";

 /**
  *
  * @param f1
  * @param f2
  * @return
  * @throws SQLException
  */
    public boolean buscarPorcentajesPostgres(String f1,String f2) throws SQLException{
        Connection con = null;
        fecha1=f1;
        fecha2=f2;
         vPostgres = new Vector();
        boolean resu=true;        
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_POSTGRES_PORCENTAJES";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, f1+" 00:00");
                st.setString(2, f2+" 23:59");

                rs = st.executeQuery();
                while (rs.next()) {
                    
                    Plarem p = new Plarem();
                    p.setNumpla(rs.getString("numpla"));
                    p.setNumrem(rs.getString("numrem"));
                    p.setPorcent(rs.getString("porcent"));
                    if(!p.getNumpla().equals("")){ 
                        vPostgres.add(p);
                    }
                }
              }}catch(SQLException e){
            resu=false;
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR REMESAS POSTGRES " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resu;
    }


/**
 * 
 * @return
 * @throws SQLException
 * belleza7 
 */
    public boolean buscarPorcentajesOracle() throws SQLException{
        Connection con = null;
        vOracle = new Vector();
        boolean resu=true;
        PreparedStatement st = null;
        ResultSet rs = null;        
        String planilla="";
        PlanillasPostgres="";
        String query = "";
        
        try{
            con = this.conectarJNDI("SQL_ORACLE_PORCENTAJES");//JJCastro fase2
            if (con != null) {
                for (int i = 0; i < vPostgres.size(); i++) {

                    query = this.obtenerSQL("SQL_ORACLE_PORCENTAJES");
                    int numeroderem = 0;
                    PlanillasPostgres = "";
                    for (int j = i; j < vPostgres.size(); j++) {
                        if (numeroderem < 230) {
                            Plarem pla = (Plarem) vPostgres.get(j);
                            if (!planilla.equals(pla.getNumpla())) {
                                PlanillasPostgres += "'" + pla.getNumpla() + "'";
                            } else {
                            }
                            planilla = pla.getNumpla();
                        } else {
                            i = j - 1;
                            break;
                        }
                        numeroderem++;
                        i = j;
                    }
                    PlanillasPostgres = PlanillasPostgres.replaceAll("''", "','");
                    query = query.replaceAll("#NUMPLA#", PlanillasPostgres);

                    st = con.prepareStatement(query);
                    rs = st.executeQuery();
                    while (rs.next()) {
                        Plarem p = new Plarem();
                        p.setNumpla(rs.getString(1));
                        p.setNumrem(rs.getString(2));
                        p.setPorcent(rs.getString(3));
                        vOracle.add(p);
                    }

                }
            }
        }catch(SQLException e){
                        resu=false;
                        e.printStackTrace();
                        throw new SQLException("ERROR AL BUSCAR REMESAS ORACLE: " + e.getMessage()+"" + e.getErrorCode());
                    }finally{//JJCastro fase2
                        if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
                        if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                        if (con != null){ try{ this.desconectar(con); } catch(Exception e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                    }
         return resu;
    }
    
    /**
     * Getter for property vPostgres.
     * @return Value of property vPostgres.
     */
    public java.util.Vector getVPostgres() {
        return vPostgres;
    }
    
    /**
     * Setter for property vPostgres.
     * @param vPostgres New value of property vPostgres.
     */
    public void setVPostgres(java.util.Vector vPostgres) {
        this.vPostgres = vPostgres;
    }
    
    /**
     * Getter for property vOracle.
     * @return Value of property vOracle.
     */
    public java.util.Vector getVOracle() {
        return vOracle;
    }
    
    /**
     * Setter for property vOracle.
     * @param vOracle New value of property vOracle.
     */
    public void setVOracle(java.util.Vector vOracle) {
        this.vOracle = vOracle;
    }
    
    /**
     * Getter for property fecha1.
     * @return Value of property fecha1.
     */
    public java.lang.String getFecha1() {
        return fecha1;
    }
    
    /**
     * Setter for property fecha1.
     * @param fecha1 New value of property fecha1.
     */
    public void setFecha1(java.lang.String fecha1) {
        this.fecha1 = fecha1;
    }
    
    /**
     * Getter for property fecha2.
     * @return Value of property fecha2.
     */
    public java.lang.String getFecha2() {
        return fecha2;
    }
    
    /**
     * Setter for property fecha2.
     * @param fecha2 New value of property fecha2.
     */
    public void setFecha2(java.lang.String fecha2) {
        this.fecha2 = fecha2;
    }
    
}
