/***********************************************************************************
 * Nombre clase :                 RemesaImpresionDAO.java                          *
 * Descripcion :                  Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD para generar la impresion de la remsa  *
 * Autor :                        Ing. Juan Manuel Escandon Perez                  *
 * Fecha :                        6 de enero de 2006, 02:57 PM                     *
 * Version :                      1.0                                              *
 * Copyright :                    Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;

public class RemesaImpresionDAO {
    
   
  
    public static String DOCUMENTOS_GENERALES=          "   SELECT tipo_doc,                                "+
    "   documento, documento_rel                        "+
    "   FROM   remesa_docto                             "+
    "   WHERE numrem = ?   AND reg_status <> 'A'        ";
    
    
    public static String PLANILLAS_PENDIENTES   =       "   SELECT p.reg_status, p.numpla, r.numrem FROM planilla p                 "+
    "   LEFT OUTER JOIN plarem pr ON ( pr.numpla = p.numpla )                   "+
    "   LEFT OUTER JOIN remesa r  ON ( r.numrem  = pr.numrem )                  "+
    "   WHERE r.numrem = ?                                                      ";
    
    private static String SQLBUSCAR =   "   SELECT                                                                                  "+
    "   get_nombreciudad(r.orirem) as origen ,                                                  "+
    "   r.fecrem as fecha,                                                                      "+
    "   r.numrem,                                                                               "+
    "   p.numpla,                                                                               "+
    "   p.plaveh as placa,                                                                      "+
    "   p.cedcon,                                                                               "+
    "   n.nombre,                                                                               "+
    "   rm.nombre as remitente,                                                                 "+
    "   d.nombre as nomdest,                                                                    "+
    "   d.direccion,                                                                            "+
    "   get_nombreciudad(d.ciudad) as ciudad,                                                   "+
    "   u.nombre as despachador,                                                                "+
    "   get_nombreciudad(r.agcrem) as agencia,                                                  "+
    "   a.dstrct,                                                                               "+
    "   c.fiduciaria,                                                                           "+
    "   ci.texto_remesa,                                                                        "+
    "   p.precinto,                                                                             "+
    "   p.contenedores,                                                                         "+
    "   p.creation_date                                                                         "+
    "   FROM remesa r                                                                           "+
    "   LEFT JOIN plarem pr on( pr.numrem = r.numrem )                                          "+
    "   LEFT JOIN planilla p on( p.numpla = pr.numpla )                                         "+
    "   LEFT JOIN nit n on( n.cedula = p.cedcon  )                                              "+
    "   LEFT JOIN remesadest rm1 on( rm1.numrem = r.numrem and rm1.tipo = 'RE'  )               "+
    "   LEFT JOIN remesadest d1 on( d1.numrem = r.numrem and d1.tipo = 'DE'  )                  "+
    "   LEFT JOIN remidest rm on( rm.codigo = rm1.codigo   )                                    "+
    "   LEFT JOIN remidest d on( d.codigo = d1.codigo   )                                       "+
    "   LEFT JOIN cliente c on ( c.codcli = r.cliente )                                         "+
    "   LEFT JOIN agencia a on ( a.id_agencia = c.agduenia )                                    "+
    "   LEFT JOIN cia ci on    ( ci.dstrct = a.dstrct )                                         "+
    "   LEFT JOIN usuarios u on( u.idusuario = p.despachador )                                  "+
    "   WHERE r.numrem = ?                                                                      "+
    "   ORDER BY   p.creation_date   DESC                                                       ";



public static String SQL_planillas_asociadas = "   SELECT                                                                                  "+
    "   get_nombreciudad(r.orirem) as origen ,                                                  "+
    "   r.fecrem as fecha,                                                                      "+
    "   r.numrem,                                                                               "+
    "   p.numpla,                                                                               "+
    "   p.plaveh as placa,                                                                      "+
    "   p.cedcon,                                                                               "+
    "   n.nombre,                                                                               "+
    "   rm.nombre as remitente,                                                                 "+
    "   d.nombre as nomdest,                                                                    "+
    "   d.direccion,                                                                            "+
    "   get_nombreciudad(d.ciudad) as ciudad,                                                   "+
    "   u.nombre as despachador,                                                                "+
    "   get_nombreciudad(r.agcrem) as agencia,                                                  "+
    "   a.dstrct,                                                                               "+
    "   c.fiduciaria,                                                                           "+
    "   ci.texto_remesa,                                                                        "+
    "   p.precinto,                                                                             "+
    "   p.contenedores,                                                                         "+
    "   p.creation_date                                                                         "+
    "   FROM remesa r                                                                           "+
    "   LEFT JOIN planilla p on( p.numpla = ? )                                                 "+
    "   LEFT JOIN nit n on( n.cedula = p.cedcon  )                                              "+
    "   LEFT JOIN remesadest rm1 on( rm1.numrem = r.numrem and rm1.tipo = 'RE'  )               "+
    "   LEFT JOIN remesadest d1 on( d1.numrem = r.numrem and d1.tipo = 'DE'  )                  "+
    "   LEFT JOIN remidest rm on( rm.codigo = rm1.codigo   )                                    "+
    "   LEFT JOIN remidest d on( d.codigo = d1.codigo   )                                       "+
    "   LEFT JOIN cliente c on ( c.codcli = r.cliente )                                         "+
    "   LEFT JOIN agencia a on ( a.id_agencia = c.agduenia )                                    "+
    "   LEFT JOIN cia ci on    ( ci.dstrct = a.dstrct )                                         "+
    "   LEFT JOIN usuarios u on( u.idusuario = p.despachador )                                  "+
    "   WHERE r.numrem = ?                                                                      "+
    "   ORDER BY   p.creation_date   DESC                                                       ";
    



/**
     * Metodo ListaPlanillaAsociadas, permite buscar los datos correspondiente a una remesa para su impresion.
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @version : 1.0
     */
    public List ListaPlanillaAsociadas( String numrem ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List Lista           = new LinkedList();
        
        try{
            st = con.prepareStatement(this.SQLBUSCAR);
            st.setString(1, numrem);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    DatosRemesaImpPDF datos  = new DatosRemesaImpPDF();
                    datos = DatosRemesaImpPDF.loadItem(rs);
                    String documentos = this.BuscarDocInternos(numrem);
                    datos.setDocinternos(documentos);
                    Lista.add(datos);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina ListaPlanillaAsociadas [RemesaImpresionDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return Lista;
    }


  /**
     * Metodo DatosRemesaAsociadas, permite buscar los datos correspondiente a una remesa para su impresion.
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @version : 1.0
     */
    public DatosRemesaImpPDF DatosRemesaAsociadas( String numpla, String numrem ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        DatosRemesaImpPDF datos           = new DatosRemesaImpPDF();
        try{
            st = con.prepareStatement(this.SQL_planillas_asociadas);
            st.setString(1, numpla);
            st.setString(2, numrem);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    datos = DatosRemesaImpPDF.loadItem(rs);
                    break;
                }
                String documentos = this.BuscarDocInternos(numrem);
                datos.setDocinternos(documentos);
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina DatosRemesaAsociadas [RemesaImpresionDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return datos;
    }
    
    
    /** Creates a new instance of RemesaImpresionDAO */
    public RemesaImpresionDAO() {
    }
    
    
    /**
     * Metodo DatosRemesa, permite buscar los datos correspondiente a una remesa para su impresion.
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @version : 1.0
     */
    public DatosRemesaImpPDF DatosRemesa( String numrem ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        DatosRemesaImpPDF datos           = new DatosRemesaImpPDF();
        try{
            st = con.prepareStatement(this.SQLBUSCAR);
            st.setString(1, numrem);
            ////System.out.println("Remesa Impresion DAO " + st);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    datos = DatosRemesaImpPDF.loadItem(rs);
                    break;
                }
                String documentos = this.BuscarDocInternos(numrem);
                datos.setDocinternos(documentos);
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina DATOSREMESA [RemesaImpresionDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return datos;
    }
    
   
    
    /**
     * Metodo SearchPlanilla_Pendiente, indica si una remesa tiene asociadas planillas pendientes ( reg_status = 'P' )
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @version : 1.0
     */
    public boolean SearchPlanilla_Pendiente( String numrem )throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        boolean           flag     = false;
        if (con == null){
            throw new SQLException("Sin conexion");
        }
        try {
            st = con.prepareStatement(this.PLANILLAS_PENDIENTES);
            st.setString(1, numrem);
            rs = st.executeQuery();
            while(rs.next()){
                if( rs.getString("reg_status").equals("P")){
                    flag = true;
                    break;
                }
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina SearchPlanilla_Pendiente [RemesaImpresionDAO]... \n"+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return flag;
    }
    
    /**
     * Metodo BuscarDocInternos, permite buscar los documentos internos asocidados de una remesa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @version : 1.0
     */
    public String BuscarDocInternos( String numrem )throws Exception{
         PoolManager poolManager = PoolManager.getInstance();
         Connection con = poolManager.getConnection("fintra");
         PreparedStatement st       = null;
         ResultSet         rs       = null;
         String documentos = "";
         if (con == null){
             throw new SQLException("Sin conexion");
         }
         try {
             st = con.prepareStatement(DOCUMENTOS_GENERALES);
             st.setString(1, numrem);
             rs = st.executeQuery();
             while(rs.next()){
                 if( !rs.getString("documento_rel").equals(""))
                     documentos  += rs.getString("documento")  + "/" + rs.getString("documento_rel") + ", ";
                 else
                     documentos  += rs.getString("documento") + ", ";
             }
             if( documentos.length() > 0 ) {
                documentos = documentos.trim();
                documentos = documentos.substring(0, documentos.length() - 1 );
             }
         }
         catch(Exception e) {
             e.printStackTrace();
             throw new Exception("Error en rutina BuscarDocInternos [RemesaImpresionDAO]... \n"+e.getMessage());
         }
         finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             poolManager.freeConnection("fintra",con);
         }
         return documentos;
     }
}
