/***********************************************************************************
 * Nombre clase : ............... BalancePruebaDAO.java                            *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la tabla Flota.                              *
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Modificado:................... Ing. Iv�n Devia                                  *
 * Fecha :........................ 5 de diciembre de 2005, 05:33 PM                *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;

public class BalancePruebaDAO extends MainDAO{

    /** Creates a new instance of FlotaDAO */
    Vector elementos_y_unidades;

    public BalancePruebaDAO() {
        super("BalancePruebaDAO.xml");
    }
    public BalancePruebaDAO(String dataBaseName) {
        super("BalancePruebaDAO.xml", dataBaseName);
    }


    /*************************************************************************
     * metodo cuentasBalance, obtiene los datos de las cuentas tipo balance
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param: dstrct, distrito
     * @param: anio, a�o del reporte
     * @param: mes, mes del reporte
     * @return: vector con objetos de datos del reporte
     * @throws: en caso de un error de BD
     *************************************************************************/
    public Vector cuentasBalance( String dstrct, String anio, int mes) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        Vector v             = new Vector();
        String sql           = "";
        String mess          = mes < 10? "0"+String.valueOf(mes) : String.valueOf(mes) ;
        String query = "CUENTAS_BALANCE";
        try{

            //Se  inicializan los distintos elemento y unidades
            this.buscarElementos_y_unidades();

            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#ANTERIOR#", this.querySaldoAnterior(mes) ).replaceAll("#MES#", mess ));//JJCastro fase2

            st.setString (1,dstrct);//20100608
            st.setString (2,anio);//20100608

            rs = st.executeQuery();

            BPCuentas2 c = new BPCuentas2();

            String elemento = "";
            String unidad   = "";
            String grupo    = "";
            String grupo_rs = "";

            //Totales por unidad de negocio
            double t_unidad_anterior = 0 ;
            double t_unidad_debito   = 0 ;
            double t_unidad_credito  = 0 ;
            double t_unidad_actual   = 0 ;

            //Totales por elemento del gasto
            double t_elemento_anterior = 0 ;
            double t_elemento_debito   = 0 ;
            double t_elemento_credito  = 0 ;
            double t_elemento_actual   = 0 ;

            //totales por tipo de cuenta
            double t_tipo_anterior = 0 ;
            double t_tipo_debito   = 0 ;
            double t_tipo_credito  = 0 ;
            double t_tipo_actual   = 0 ;

            //totales por grupo de cuenta( los dos primeros caracteres )
            double t_grupo_anterior = 0 ;
            double t_grupo_debito   = 0 ;
            double t_grupo_credito  = 0 ;
            double t_grupo_actual   = 0 ;

            while( rs.next() ){
                c = new BPCuentas2();
                if( r( rs.getString("elemento") ).length()>1 ){
                    grupo_rs = r( rs.getString("elemento") ).substring(0,2);
                }

                //Si es la misma unidad de negocio se acumulan los valores
                if( (unidad.equals( r( rs.getString("unidad" ) ) ) || unidad.equals("")) && ( elemento.equals( r( rs.getString("elemento" ) ) ) || elemento.equals("") ) ){
                    //Se subtotaliza por unidad
                    t_unidad_anterior += rs.getDouble("anterior" );
                    t_unidad_debito   += rs.getDouble("debito" );
                    t_unidad_credito  += rs.getDouble("credito" );
                    t_unidad_actual   += rs.getDouble("actual" );

                    unidad = r( rs.getString("unidad" ) );


                }else{
                    //Se agrega el subtotal por unidad
                    c.setUnidad(                "SUBTOTAL_UNIDAD" );

                    c.setVlr_saldo_anterior(    t_unidad_anterior );
                    c.setVlr_debito(            t_unidad_debito );
                    c.setVlr_credito(           t_unidad_credito );
                    c.setVlr_saldo_actual(      t_unidad_actual );

                    //Se empieza acumular el nuevo subtotal por unidad
                    t_unidad_anterior = rs.getDouble("anterior" );
                    t_unidad_debito   = rs.getDouble("debito" );
                    t_unidad_credito  = rs.getDouble("credito" );
                    t_unidad_actual   = rs.getDouble("actual" );

                    unidad = r( rs.getString("unidad" ) );
                    v.add(c);
                    c = new BPCuentas2();
                }

                //Si es el mismo elememto se acumulan los valores
                if( elemento.equals( r( rs.getString("elemento" ) ) ) || elemento.equals("") ){
                    //Se subtotaliza por elemento
                    t_elemento_anterior += rs.getDouble("anterior" );
                    t_elemento_debito   += rs.getDouble("debito" );
                    t_elemento_credito  += rs.getDouble("credito" );
                    t_elemento_actual   += rs.getDouble("actual" );

                    elemento = r( rs.getString("elemento" ) );
                }else{
                    //Se agrega el subtotal por elemento
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_ELEMENTO" );

                    c.setVlr_saldo_anterior(    t_elemento_anterior );
                    c.setVlr_debito(            t_elemento_debito );
                    c.setVlr_credito(           t_elemento_credito );
                    c.setVlr_saldo_actual(      t_elemento_actual );

                    //Se acumulan los totales por tipo
                    t_tipo_anterior += t_elemento_anterior;
                    t_tipo_debito   += t_elemento_debito;
                    t_tipo_credito  += t_elemento_credito;
                    t_tipo_actual   += t_elemento_actual;

                    //Se empieza acumular el nuevo subtotal por elemento
                    t_elemento_anterior = rs.getDouble("anterior" );
                    t_elemento_debito   = rs.getDouble("debito" );
                    t_elemento_credito  = rs.getDouble("credito" );
                    t_elemento_actual   = rs.getDouble("actual" );


                    elemento = r( rs.getString("elemento" ) );
                    v.add(c);
                    c = new BPCuentas2();
                }

                //Si es el mismo grupo se acumulan los valores
                if( grupo.equals( grupo_rs ) || grupo.equals("") ){
                    //Se subtotaliza por grupo
                    t_grupo_anterior += rs.getDouble("anterior" );
                    t_grupo_debito   += rs.getDouble("debito" );
                    t_grupo_credito  += rs.getDouble("credito" );
                    t_grupo_actual   += rs.getDouble("actual" );

                    grupo = grupo_rs.toString();
                }else{
                    //Se agrega el subtotal por grupo
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_GRUPO" );

                    c.setVlr_saldo_anterior(    t_grupo_anterior );
                    c.setVlr_debito(            t_grupo_debito );
                    c.setVlr_credito(           t_grupo_credito );
                    c.setVlr_saldo_actual(      t_grupo_actual );

                    //Se empieza acumular el nuevo subtotal por grupo
                    t_grupo_anterior = rs.getDouble("anterior" );
                    t_grupo_debito   = rs.getDouble("debito" );
                    t_grupo_credito  = rs.getDouble("credito" );
                    t_grupo_actual   = rs.getDouble("actual" );

                    grupo = grupo_rs.toString();
                    v.add(c);
                    c = new BPCuentas2();
                }

                //Se agrega al reporte la cuenta con sus valores
                c.setElemento(              r( rs.getString("elemento" ) ) );
                c.setUnidad(                r( rs.getString("unidad" ) ) );
                c.setGrupo(                 grupo_rs);
                //c.setDesc_elemento(         r( rs.getString("desc_elemento" ) ) );
                //c.setDesc_unidad(           r( rs.getString("desc_unidad" ) ) );
                c.setCuenta(                r( rs.getString("cuenta" ) ) );
                c.setDesc_cuenta(           r( rs.getString("nombre_largo" ) ) );
                c.setVlr_saldo_anterior(    rs.getDouble("anterior" ) );
                c.setVlr_debito(            rs.getDouble("debito" ) );
                c.setVlr_credito(           rs.getDouble("credito" ) );
                c.setVlr_saldo_actual(      rs.getDouble("actual" ) );

                v.add(c);

                //Si es el ultimo registro se agregan los subtotales correspondientes
                if( rs.isLast() ){

                    //Subtotal por unidad
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_UNIDAD" );

                    c.setVlr_saldo_anterior(    t_unidad_anterior );
                    c.setVlr_debito(            t_unidad_debito );
                    c.setVlr_credito(           t_unidad_credito );
                    c.setVlr_saldo_actual(      t_unidad_actual );

                    v.add(c);

                    //Subtotal por elemento
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_ELEMENTO" );

                    c.setVlr_saldo_anterior(    t_elemento_anterior );
                    c.setVlr_debito(            t_elemento_debito );
                    c.setVlr_credito(           t_elemento_credito );
                    c.setVlr_saldo_actual(      t_elemento_actual );

                    v.add(c);

                    //Subtotal por grupo
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_GRUPO" );

                    c.setVlr_saldo_anterior(    t_grupo_anterior );
                    c.setVlr_debito(            t_grupo_debito );
                    c.setVlr_credito(           t_grupo_credito );
                    c.setVlr_saldo_actual(      t_grupo_actual );

                    v.add(c);

                    //Se acumula el ultimo elemento al tipo
                    t_tipo_anterior += t_elemento_anterior;
                    t_tipo_debito   += t_elemento_debito;
                    t_tipo_credito  += t_elemento_credito;
                    t_tipo_actual   += t_elemento_actual;

                    //Subtotal por tipo
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_TIPO" );

                    c.setVlr_saldo_anterior(    t_tipo_anterior );
                    c.setVlr_debito(            t_tipo_debito );
                    c.setVlr_credito(           t_tipo_credito );
                    c.setVlr_saldo_actual(      t_tipo_actual );

                    v.add(c);
                }

            }



        }}catch (Exception ex){
            ex.printStackTrace();
            throw new SQLException("Error en BalancePruebaDAO.cuentasBalance "+ex.getMessage());
        }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return v;

    }


    /*************************************************************************
     * metodo cuentasBalance, obtiene los datos de las cuentas tipo ingreso
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param: dstrct, distrito
     * @param: anio, a�o del reporte
     * @param: mes, mes del reporte
     * @return: vector con objetos de datos del reporte
     * @throws: en caso de un error de BD
     *************************************************************************/
    /*public Vector cuentasIngreso( String dstrct, String anio, int mes, String tipo) throws Exception{

        PreparedStatement st = null;
        ResultSet rs         = null;

        Vector v             = new Vector();
        String sql           = "";
        String mess          = mes < 10? "0"+String.valueOf(mes) : String.valueOf(mes) ;

        try{

            st = this.crearPreparedStatement("CUENTAS_INGRESO");

            st.setString( 1, dstrct );
            st.setString( 2, anio );

            sql = st.toString();
            sql = sql.replaceAll("#ANTERIOR#", this.querySaldoAnterior(mes) );
            sql = sql.replaceAll("#MES#", mess );
            sql = sql.replaceAll("#TIPO#", tipo );

            //System.out.println("INGRESO\n"+sql);

            rs = st.executeQuery( sql );

            BPCuentas2 c = new BPCuentas2();

            String elemento = "";
            String unidad   = "";

            //Totales por unidad de negocio
            double t_unidad_anterior = 0 ;
            double t_unidad_debito   = 0 ;
            double t_unidad_credito  = 0 ;
            double t_unidad_actual   = 0 ;

            //Totales por elemento del gasto
            double t_elemento_anterior = 0 ;
            double t_elemento_debito   = 0 ;
            double t_elemento_credito  = 0 ;
            double t_elemento_actual   = 0 ;

            //totales por tipo de cuenta
            double t_tipo_anterior = 0 ;
            double t_tipo_debito   = 0 ;
            double t_tipo_credito  = 0 ;
            double t_tipo_actual   = 0 ;

            while( rs.next() ){

                c = new BPCuentas2();

                //Si es la misma unidad de negocio se acumulan los valores
                if( unidad.equals( r( rs.getString("unidad" ) ) ) || unidad.equals("") ){
                    //Se subtotaliza por unidad
                    t_unidad_anterior += rs.getDouble("anterior" );
                    t_unidad_debito   += rs.getDouble("debito" );
                    t_unidad_credito  += rs.getDouble("credito" );
                    t_unidad_actual   += rs.getDouble("actual" );

                    unidad = r( rs.getString("unidad" ) );


                }else{
                    //Se agrega el subtotal por unidad
                    c.setUnidad(                "SUBTOTAL_UNIDAD" );

                    c.setVlr_saldo_anterior(    t_unidad_anterior );
                    c.setVlr_debito(            t_unidad_debito );
                    c.setVlr_credito(           t_unidad_credito );
                    c.setVlr_saldo_actual(      t_unidad_actual );

                    //Se empieza acumular el nuevo subtotal por unidad
                    t_unidad_anterior = rs.getDouble("anterior" );
                    t_unidad_debito   = rs.getDouble("debito" );
                    t_unidad_credito  = rs.getDouble("credito" );
                    t_unidad_actual   = rs.getDouble("actual" );

                    unidad = r( rs.getString("unidad" ) );
                    v.add(c);
                    c = new BPCuentas2();
                }

                //Si es el mismo elememto se acumulan los valores
                if( elemento.equals( r( rs.getString("elemento" ) ) ) || elemento.equals("") ){
                    //Se subtotaliza por elemento
                    t_elemento_anterior += rs.getDouble("anterior" );
                    t_elemento_debito   += rs.getDouble("debito" );
                    t_elemento_credito  += rs.getDouble("credito" );
                    t_elemento_actual   += rs.getDouble("actual" );

                    elemento = r( rs.getString("elemento" ) );
                }else{
                    //Se agrega el subtotal por elemento
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_ELEMENTO" );

                    c.setVlr_saldo_anterior(    t_elemento_anterior );
                    c.setVlr_debito(            t_elemento_debito );
                    c.setVlr_credito(           t_elemento_credito );
                    c.setVlr_saldo_actual(      t_elemento_actual );

                    //Se acumulan los totales por tipo
                    t_tipo_anterior += t_elemento_anterior;
                    t_tipo_debito   += t_elemento_debito;
                    t_tipo_credito  += t_elemento_credito;
                    t_tipo_actual   += t_elemento_actual;

                    //Se empieza acumular el nuevo subtotal por elemento
                    t_elemento_anterior = rs.getDouble("anterior" );
                    t_elemento_debito   = rs.getDouble("debito" );
                    t_elemento_credito  = rs.getDouble("credito" );
                    t_elemento_actual   = rs.getDouble("actual" );

                    elemento = r( rs.getString("elemento" ) );
                    v.add(c);
                    c = new BPCuentas2();
                }

                //Se agrega al reporte la cuenta con sus valores
                c.setElemento(              r( rs.getString("elemento" ) ) );
                c.setUnidad(                r( rs.getString("unidad" ) ) );
                c.setDesc_elemento(         r( rs.getString("desc_elemento" ) ) );
                c.setDesc_unidad(           r( rs.getString("desc_unidad" ) ) );
                c.setCuenta(                r( rs.getString("cuenta" ) ) );
                c.setDesc_cuenta(           r( rs.getString("nombre_largo" ) ) );
                c.setVlr_saldo_anterior(    rs.getDouble("anterior" ) );
                c.setVlr_debito(            rs.getDouble("debito" ) );
                c.setVlr_credito(           rs.getDouble("credito" ) );
                c.setVlr_saldo_actual(      rs.getDouble("actual" ) );

                v.add(c);

                //Si es el ultimo registro se agregan los subtotales correspondientes
                if( rs.isLast() ){

                    //Subtotal por unidad
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_UNIDAD" );

                    c.setVlr_saldo_anterior(    t_unidad_anterior );
                    c.setVlr_debito(            t_unidad_debito );
                    c.setVlr_credito(           t_unidad_credito );
                    c.setVlr_saldo_actual(      t_unidad_actual );

                    v.add(c);

                    //Subtotal por elemento
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_ELEMENTO" );

                    c.setVlr_saldo_anterior(    t_elemento_anterior );
                    c.setVlr_debito(            t_elemento_debito );
                    c.setVlr_credito(           t_elemento_credito );
                    c.setVlr_saldo_actual(      t_elemento_actual );

                    v.add(c);

                    //Se acumula el ultimo elemento al tipo
                    t_tipo_anterior += t_elemento_anterior;
                    t_tipo_debito   += t_elemento_debito;
                    t_tipo_credito  += t_elemento_credito;
                    t_tipo_actual   += t_elemento_actual;

                    //Subtotal por tipo
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_TIPO" );

                    c.setVlr_saldo_anterior(    t_tipo_anterior );
                    c.setVlr_debito(            t_tipo_debito );
                    c.setVlr_credito(           t_tipo_credito );
                    c.setVlr_saldo_actual(      t_tipo_actual );

                    v.add(c);
                }

            }



        }catch (Exception ex){
            ex.printStackTrace();
            throw new SQLException("Error en BalancePruebaDAO.cuentasBalance "+ex.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            desconectar("CUENTAS_INGRESO");
        }

        return v;
    }*/

        /*************************************************************************
     * metodo cuentasBalance, obtiene los datos de las cuentas tipo ingreso
     * @author: Ing. Osvaldo P�rez Ferrer , Mod TMOLINA, 24-DIC-2008
     * @param: dstrct, distrito
     * @param: anio, a�o del reporte
     * @param: mes, mes del reporte
     * @return: vector con objetos de datos del reporte
     * @throws: en caso de un error de BD
     *************************************************************************/
    public Vector cuentasIngreso( String dstrct, String anio, int mes, String tipo) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        Vector v             = new Vector();
        String sql           = "";
        String mess          = mes < 10? "0"+String.valueOf(mes) : String.valueOf(mes) ;
        String query = "CUENTAS_INGRESO";
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#ANTERIOR#", this.querySaldoAnterior(mes) ).replaceAll("#MES#", mess ).replaceAll("#TIPO#", tipo ));//JJCastro fase2
            st.setString( 1, dstrct );
            st.setString( 2, anio );
            //rs = st.executeQuery( sql );
            rs = st.executeQuery(  );//20100608

            BPCuentas2 c = new BPCuentas2();

            String elemento = "";
            String unidad   = "";

            //Totales por unidad de negocio
            double t_unidad_anterior = 0 ;
            double t_unidad_debito   = 0 ;
            double t_unidad_credito  = 0 ;
            double t_unidad_actual   = 0 ;

            //Totales por elemento del gasto
            double t_elemento_anterior = 0 ;
            double t_elemento_debito   = 0 ;
            double t_elemento_credito  = 0 ;
            double t_elemento_actual   = 0 ;

            //totales por tipo de cuenta
            double t_tipo_anterior = 0 ;
            double t_tipo_debito   = 0 ;
            double t_tipo_credito  = 0 ;
            double t_tipo_actual   = 0 ;

            while( rs.next() ){

                c = new BPCuentas2();


                //Si es el mismo elememto se acumulan los valores
                if( elemento.equals( r( rs.getString("elemento" ) ) ) || elemento.equals("") ){
                    //Se subtotaliza por elemento
                    t_elemento_anterior += rs.getDouble("anterior" );
                    t_elemento_debito   += rs.getDouble("debito" );
                    t_elemento_credito  += rs.getDouble("credito" );
                    t_elemento_actual   += rs.getDouble("actual" );

                    elemento = r( rs.getString("elemento" ) );
                }else{
                    //Se agrega el subtotal por elemento
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_ELEMENTO" );

                    c.setVlr_saldo_anterior(    t_elemento_anterior );
                    c.setVlr_debito(            t_elemento_debito );
                    c.setVlr_credito(           t_elemento_credito );
                    c.setVlr_saldo_actual(      t_elemento_actual );

                    //Se acumulan los totales por tipo
                    t_tipo_anterior += t_elemento_anterior;
                    t_tipo_debito   += t_elemento_debito;
                    t_tipo_credito  += t_elemento_credito;
                    t_tipo_actual   += t_elemento_actual;

                    //Se empieza acumular el nuevo subtotal por elemento
                    t_elemento_anterior = rs.getDouble("anterior" );
                    t_elemento_debito   = rs.getDouble("debito" );
                    t_elemento_credito  = rs.getDouble("credito" );
                    t_elemento_actual   = rs.getDouble("actual" );

                    elemento = r( rs.getString("elemento" ) );
                    v.add(c);
                    c = new BPCuentas2();
                }

                //Si es la misma unidad de negocio se acumulan los valores
                if( unidad.equals( r( rs.getString("unidad" ) ) ) || unidad.equals("") ){
                    //Se subtotaliza por unidad
                    t_unidad_anterior += rs.getDouble("anterior" );
                    t_unidad_debito   += rs.getDouble("debito" );
                    t_unidad_credito  += rs.getDouble("credito" );
                    t_unidad_actual   += rs.getDouble("actual" );

                    unidad = r( rs.getString("unidad" ) );


                }else{
                    //Se agrega el subtotal por unidad
                    c.setUnidad(                "SUBTOTAL_UNIDAD" );

                    c.setVlr_saldo_anterior(    t_unidad_anterior );
                    c.setVlr_debito(            t_unidad_debito );
                    c.setVlr_credito(           t_unidad_credito );
                    c.setVlr_saldo_actual(      t_unidad_actual );

                    //Se empieza acumular el nuevo subtotal por unidad
                    t_unidad_anterior = rs.getDouble("anterior" );
                    t_unidad_debito   = rs.getDouble("debito" );
                    t_unidad_credito  = rs.getDouble("credito" );
                    t_unidad_actual   = rs.getDouble("actual" );

                    unidad = r( rs.getString("unidad" ) );
                    v.add(c);
                    c = new BPCuentas2();
                }



                //Se agrega al reporte la cuenta con sus valores
                c.setElemento(              r( rs.getString("elemento" ) ) );
                c.setUnidad(                r( rs.getString("unidad" ) ) );
                c.setDesc_elemento(         r( rs.getString("desc_elemento" ) ) );
                c.setDesc_unidad(           r( rs.getString("desc_unidad" ) ) );
                c.setCuenta(                r( rs.getString("cuenta" ) ) );
                c.setDesc_cuenta(           r( rs.getString("nombre_largo" ) ) );
                c.setVlr_saldo_anterior(    rs.getDouble("anterior" ) );
                c.setVlr_debito(            rs.getDouble("debito" ) );
                c.setVlr_credito(           rs.getDouble("credito" ) );
                c.setVlr_saldo_actual(      rs.getDouble("actual" ) );

                v.add(c);

                //Si es el ultimo registro se agregan los subtotales correspondientes
                if( rs.isLast() ){



                    //Subtotal por elemento
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_ELEMENTO" );

                    c.setVlr_saldo_anterior(    t_elemento_anterior );
                    c.setVlr_debito(            t_elemento_debito );
                    c.setVlr_credito(           t_elemento_credito );
                    c.setVlr_saldo_actual(      t_elemento_actual );

                    v.add(c);

                    //Subtotal por unidad
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_UNIDAD" );

                    c.setVlr_saldo_anterior(    t_unidad_anterior );
                    c.setVlr_debito(            t_unidad_debito );
                    c.setVlr_credito(           t_unidad_credito );
                    c.setVlr_saldo_actual(      t_unidad_actual );

                    v.add(c);

                    //Se acumula el ultimo elemento al tipo
                    t_tipo_anterior += t_elemento_anterior;
                    t_tipo_debito   += t_elemento_debito;
                    t_tipo_credito  += t_elemento_credito;
                    t_tipo_actual   += t_elemento_actual;

                    //Subtotal por tipo
                    c = new BPCuentas2();
                    c.setUnidad(                "SUBTOTAL_TIPO" );

                    c.setVlr_saldo_anterior(    t_tipo_anterior );
                    c.setVlr_debito(            t_tipo_debito );
                    c.setVlr_credito(           t_tipo_credito );
                    c.setVlr_saldo_actual(      t_tipo_actual );

                    v.add(c);
                }

            }



        }}catch (Exception ex){
            ex.printStackTrace();
            throw new SQLException("Error en BalancePruebaDAO.cuentasBalance "+ex.getMessage());
        }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return v;
    }


    /*********************************************************************
     * metodo querySaldoAnterior, arma la cadena parra realizar la consulta
     *         para obtener el saldo anterior al  mes dado
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param : int mes, mes a partir del cual se desea calcular el saldo
     *          anterior
     *********************************************************************/
    public String querySaldoAnterior( int mes ){

        String s = "saldoant ";

        if( mes > 1 ){
            String mess = "0";
            for( int i=1; i<mes; i++ ){
                mess = i < 10? "0"+String.valueOf(i) : String.valueOf(i) ;
                s += "+ movdeb"+ mess +" - movcre"+mess +" ";
            }
        }

        s += " AS anterior";
        return s;

    }

    public String r( String value ){
        return value != null? value:"";
    }


/**
 *
 * @throws Exception
 */
    public void buscarElementos_y_unidades() throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "ELEMENTOS_Y_UNIDADES";
        try {

            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                this.elementos_y_unidades = new Vector();

                while (rs.next()) {
                    this.elementos_y_unidades.add(r(rs.getString("cuenta") + "-_-" + rs.getString("nombre_largo")));
                }

            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new SQLException("Error en BalancePruebaDAO.buscarElementos_y_unidades "+ex.getMessage());
        }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
    }


  /**
   *
   * @param cuenta
   * @param tipo
   * @return
   */
    public String getDescElemento_Unidad( String cuenta, String tipo ){

        String desc = "";

        try{

            if( tipo.equals("elemento") )
                cuenta = cuenta.substring(0,4);
            else if( tipo.equals("unidad") )
                cuenta = cuenta.substring(0,6);
            else if( tipo.equals("grupo") )
                cuenta = cuenta.substring(0,2);

            for( int i=0; i< this.elementos_y_unidades.size(); i++ ){
                String cuenta_desc = (String)this.elementos_y_unidades.get(i);
                if( cuenta_desc.startsWith( cuenta ) ){
                    desc = cuenta_desc.split("-_-")[1];
                    //this.elementos_y_unidades.remove(i);
                    break;
                }
            }
        }catch (Exception e){System.out.println("ERROR "+e.getMessage());}

        return desc;
    }

    /**
     * Getter for property elementos_y_unidades.
     * @return Value of property elementos_y_unidades.
     */
    public java.util.Vector getElementos_y_unidades() {
        return elementos_y_unidades;
    }

    /**
     * Setter for property elementos_y_unidades.
     * @param elementos_y_unidades New value of property elementos_y_unidades.
     */
    public void setElementos_y_unidades(java.util.Vector elementos_y_unidades) {
        this.elementos_y_unidades = elementos_y_unidades;
    }



    public static void main(String[]asdf)throws Exception{

        BalancePruebaDAO b = new BalancePruebaDAO();
        //System.out.println( b.querySaldoAnterior( 12 ) );


        Vector v = b.cuentasBalance("FINV", "2006", 2);

        //System.out.println( "tama�o lista: "+v.size());

        //for( int i=0; i<v.size(); i++ ){


        while( v.size() > 0 ){

            BPCuentas2 c = (BPCuentas2)v.remove(0);

            //System.out.println( c.getElemento() );
            //System.out.println( c.getDesc_elemento() );
            //System.out.println( c.getUnidad() );
            //System.out.println( c.getDesc_unidad() );
            //System.out.println( c.getCuenta() );
            //System.out.println( c.getDesc_cuenta() );
            //System.out.println( c.getVlr_saldo_anterior() );
            //System.out.println( c.getVlr_debito() );
            //System.out.println( c.getVlr_credito() );
            //System.out.println( c.getVlr_saldo_actual() );

            //System.out.println( "____________________________" );

            //v.remove(0);
        }
        //}

    }
}
