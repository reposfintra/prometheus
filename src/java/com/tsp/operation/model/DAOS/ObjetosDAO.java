/********************************************************************
 *  Nombre Clase.................   ObjetosDAO.java
 *  Descripci�n..................   DAO del programa para capturar datos y retornarlos
 *  Autor........................   Ing. Leonardo Parodi
 *  Fecha........................   22.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;


import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import javax.swing.*;
import java.text.*;

/**
 *
 * @author  EQUIPO12
 */
public class ObjetosDAO {
        
        /** Creates a new instance of ObjetosDAO */
        public ObjetosDAO() {
        }
        
        /**
         * Metodo que guarda un objeto tipo Class en un Archivo
         * @autor: Leonardo Parody
         * @param: Class Objeto
         * @param: String user
         * @param: String pagina
         * @throws: IOException
         * @version: 1.0.
         */
        public void escribirArchivo(Object Objeto,String user, String pagina)throws IOException{
                boolean sw=true;
                Vector vector;
                Objeto paquete = new Objeto();
                paquete.setObjeto(Objeto);
                paquete.setPagina(pagina);
                vector = this.ObtenerVector(user);
                if (vector==null){
                        vector = new Vector();
                        vector.add(paquete);
                }else{
                        ////System.out.println("VOY A ENTRAR AL FOR");
                        for(int i=0; i<vector.size(); i++){
                                Objeto paq = (Objeto)vector.elementAt(i);
                                ////System.out.println("YA SAQUE EL OBJETO pagina = "+paq.getPagina() +" compara con = "+pagina);
                                if (paq.getPagina().equalsIgnoreCase(pagina)){
                                        
                                        vector.remove(i);
                                        ////System.out.println("ELIMINE EL PAQUETE");
                                        vector.add(i, paquete);
                                        ////System.out.println("METI EL PAQUETE CORREGIDO");
                                        sw=false;
                                }
                        }
                        if(sw==true){
                                vector.add(paquete);
                                ////System.out.println("METI PAQUETE QUE NO EXISTIA");
                        }
                }
                ////System.out.println("YA ARME EL PAQUETE");
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta");
                String Ruta  = path + "/jsp/general/cookies/" + user + "/datos"+user+".txt";
                File archivo1 = new File(path+"/jsp/general/cookies/" + user+"/");
                archivo1.mkdirs();
                File archivo = new File(Ruta);
                ////System.out.println("ALISTE EL ARCHIVO");
                FileOutputStream  salidaArchivo= new FileOutputStream(archivo);
                ////System.out.println("LISTO PARA ESCRIBIR");
                ObjectOutputStream salidaObjeto=new ObjectOutputStream(salidaArchivo);
                ////System.out.println("YA ABRI EL ARCHIVO");
                salidaObjeto.writeObject(vector);
                ////System.out.println("ESCRIBI EL PAQUETE");
                salidaObjeto.close();
                
        }
        
        /**
         * Metodo que lee un objeto tipo Class en un Archivo
         * @autor: Leonardo Parody
         * @param: String user
         * @param: String pagina
         * @throws: IOException
         * @version: 1.0.
         * @return: Class Objeto.
         */
        public Object leerArchivo(String pagina,String user)throws IOException,ClassNotFoundException{
                boolean sw=true;
                Vector vector = new Vector();
                Objeto paquete = new Objeto();
                Object objeto_empaquetado=null;
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta");
                String Ruta  = path + "/jsp/general/cookies/" + user + "/datos"+user+".txt";
                try{
                        File archivo = new File(Ruta);
                        ////System.out.println("ALISTE EL ARCHIVO");
                        FileInputStream entradaArchivo = new FileInputStream(archivo);
                        ObjectInputStream entradaObjeto= new ObjectInputStream(entradaArchivo);
                        ////System.out.println("YA VOY A LEER EL ARCHIVO");
                        while (sw){
                                vector = (Vector)entradaObjeto.readObject();
                                ////System.out.println("LEYENDO EL PAQUETE");
                                for(int i=0; i<vector.size(); i++){
                                        paquete = (Objeto)vector.elementAt(i);
                                        ////System.out.println("pagina del paquete = "+paquete.getPagina());
                                        ////System.out.println("pagina del metodo = "+pagina);
                                        if (paquete.getPagina().equalsIgnoreCase(pagina)){
                                                ////System.out.println("SACANDO EL OBJETO DEL PAQUETE");
                                                objeto_empaquetado = (Object)paquete.getObjeto();
                                                entradaObjeto.close();
                                                sw=false;
                                                break;
                                        }
                                }
                                
                                
                        }
                        
                }
                catch(EOFException ignorar){}
                catch(FileNotFoundException f){}
                catch(ClassNotFoundException e){}
                return objeto_empaquetado;
        }
        /**
         * Metodo que lee un Vector de un Archivo
         * @autor: Leonardo Parody
         * @param: String user
         * @throws: IOException
         * @version: 1.0.
         * @return: Vector vector.
         */
        private Vector ObtenerVector(String user){
                Vector vector = new Vector();
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta");
                String Ruta  = path + "/jsp/general/cookies/" + user + "/datos"+user+".txt";
                try{
                        File archivo = new File(Ruta);
                        ////System.out.println("ALISTE EL ARCHIVO");
                        FileInputStream entradaArchivo = new FileInputStream(archivo);
                        ObjectInputStream entradaObjeto= new ObjectInputStream(entradaArchivo);
                        ////System.out.println("YA VOY A LEER EL ARCHIVO");
                        while (true){
                                vector = (Vector)entradaObjeto.readObject();
                                ////System.out.println("OBTUVE EL VECTOR");
                                break;
                        }
                        
                }
                catch(IOException io){vector=null;}
                catch(ClassNotFoundException e){vector=null;}
                return vector;
        }
}
