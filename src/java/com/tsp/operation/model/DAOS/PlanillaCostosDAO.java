/**
 * Nombre        PlanillaCostosDAO.java
 * Descripci�n   Costeo de planillas
 * Autor         Mario Fontalvo Solano
 * Fecha         8 de mayo de 2006, 04:52 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.DAOS;


import com.tsp.operation.model.beans.Remesa;
import com.tsp.operation.model.beans.Planilla;
import java.util.*;
import java.sql.*;

public class PlanillaCostosDAO extends MainDAO{
    
    /** Crea una nueva instancia de  PlanillaCostosDAO */
    public PlanillaCostosDAO() {
        super("PlanillaCostosDAO.xml");
    }
    
    /**
     * Metodo para extraer las remesas de una planilla
     * @autor mfontalvo
     * @param numpla numero de la planilla.
     * @return Vector listado de remesas.
     * @throws Exception.
     */    
    public Vector obtenerRemesasPlanilla (String numpla) throws Exception {
        PreparedStatement st = null;
        ResultSet         rs = null;
        Vector            lista = new Vector();
        try{
           st = this.crearPreparedStatement("SQL_REMESAS_PLANILLA");
           st.setString(1, numpla);
           rs = st.executeQuery();
           while (rs.next()){
               Remesa r = new Remesa();
               r.setReg_status    ( rs.getString("reg_status"  ) );
               r.setDistrito      ( rs.getString("cia"         ) );
               r.setNumRem        ( rs.getString("numrem"      ) );
               r.setOrirem        ( rs.getString("origen"      ) );
               r.setDesrem        ( rs.getString("destino"     ) );
               r.setAgcrem        ( rs.getString("destinorel"  ) );
               r.setNombreOrigen  ( rs.getString("nomorigen"   ) );
               r.setNombreDestino ( rs.getString("nomdestino"  ) );
               r.setNombreDestinoRelacionado ( rs.getString("nomdestinorel"  ) );
               r.setValorRemesa   ( rs.getDouble("vlrrem"      ) );
               r.setCurrency      ( rs.getString("currency"    ) );
               r.setVlrrem2       ( rs.getFloat ("vlrrem2"     ) );
               r.setStd_job_no    ( rs.getString("std_job_no"  ) );
               r.setStd_job_desc  ( rs.getString("std_job_desc") );
               r.setCliente       ( rs.getString("cliente"     ) );
               r.setNombre_cli    ( rs.getString("nomcli"      ) );
               r.setPadre         ( rs.getString("agduenia"    ) ); // agencia due�a
               r.setNombre_ciu    ( rs.getString("nomagduenia" ) ); // nombre agencia due�a
               r.setTipoViaje     ( rs.getString("tipoviaje"   ) );
               r.setEstado        ( rs.getString("estado"      ) );
               r.setUnitOfWork    ( rs.getString("unit_of_work") );
               r.setOt_padre      ( rs.getString("ot_padre"    ) );
               r.setOt_rela       ( rs.getString("ot_rela"     ) );
               r.setDescripcion   ( rs.getString("descripcion" ) );
               r.setFecRem        ( rs.getDate  ("fecrem"      ) );
               r.setMes           ( rs.getString("mesrem"      ) );
               r.setTasa          ( rs.getDouble("tasa"        ) );               
               if ((r.getReg_status()!=null && r.getReg_status().equals("A")) || (rs.getString("n_facturable")!=null && rs.getString("n_facturable").equals("S")) ){
                   r.setVlrrem2(0);
                   r.setValorRemesa(0);
               }
               lista.add(r);
           }
        }catch (Exception ex){
            throw new Exception ("Error en PlanillaCostosDAO.obtenerRemesasPlanilla() ....\n" + ex.getMessage());
        }finally{
            if (rs!=null) rs.close();
            if (st!=null) st.close();
            this.desconectar("SQL_REMESAS_PLANILLA");
        }
        return lista;
    }
    
    
    /**
     * Metodo para extraer las planillas de una remesa
     * @autor mfontalvo
     * @param numrem numero de la remesa.
     * @return Vector listado de planillas.
     * @throws Exception.
     */    
    public Vector obtenerPlanillasRemesa (String numrem) throws Exception {
        PreparedStatement st = null;
        ResultSet         rs = null;
        Vector            lista = new Vector();
        try{
           st = this.crearPreparedStatement("SQL_PLANILLAS_REMESA");
           st.setString(1, numrem);
           rs = st.executeQuery();
           while (rs.next()){
               Planilla p = new Planilla();
               p.setDistrito      ( rs.getString("cia"     ) );
               p.setNumpla        ( rs.getString("numpla"  ) );
               p.setNumrem        ( rs.getString("numrem"  ) );
               p.setAgcpla        ( rs.getString("agcpla"  ) );
               p.setOripla        ( rs.getString("oripla"  ) );
               p.setDespla        ( rs.getString("despla"  ) );
               p.setDestinoRelacionado ( rs.getString("desrel" ) );
               p.setNomagc        ( rs.getString( "nomagc" ) );
               p.setNomori        ( rs.getString( "nomori" ) );
               p.setNomdest       ( rs.getString( "nomdes" ) );
               p.setNomdesrel     ( rs.getString( "desrel" ) );
               p.setFechadespacho ( rs.getTimestamp( "fecdsp" ) );
               p.setVlrPlanilla   ( rs.getDouble("vlrpla"  ) );
               p.setVlrpla2       ( rs.getFloat ("vlrpla2" ) );
               p.setCurrency      ( rs.getString("currency") );
               p.setPorcentaje    ( rs.getDouble("porcent" ) );
               p.setPlaveh        ( rs.getString("plaveh"  ) );
               p.setTasa          ( rs.getDouble("tasa"    ) );
               p.setTipoviaje     ( rs.getString("tipoviaje"));
               p.setAccount_code_i( rs.getString("account_code_i") );
               p.setAccount_code_c( rs.getString("account_code_c") );
               p.setUnidad_i      ( rs.getString("unidad_i"));
               p.setUnidad_c      ( rs.getString("unidad_c"));
               lista.add(p);
           }
        }catch (Exception ex){
            throw new Exception ("Error en PlanillaCostosDAO.obtenerPlanillasRemesa() ....\n" + ex.getMessage());
        }finally{
            if (rs!=null) rs.close();
            if (st!=null) st.close();
            this.desconectar("SQL_PLANILLAS_REMESA");
        }
        return lista;
    }    
    
/**
     * Metodo para extraer las planillas de una remesa
     * @autor mfontalvo
     * @param numrem numero de la remesa.
     * @return Vector listado de planillas.
     * @throws Exception.
     */    
    public Vector obtenerRemesasPorPeriodosDePlanilla (String fechaInicial, String fechaFinal) throws Exception {
        PreparedStatement st = null;
        ResultSet         rs = null;
        Vector            lista = new Vector();
        try{
           st = this.crearPreparedStatement("SQL_REMESAS_DE_PLANILLAS_POR_PERIODO");
           st.setString(1, fechaInicial + " 00:00");
           st.setString(2, fechaFinal   + " 23:59");           
           rs = st.executeQuery();
           while (rs.next()){
               Remesa r = new Remesa();
               r.setReg_status    ( rs.getString("reg_status"  ) );
               r.setDistrito      ( rs.getString("cia"         ) );
               r.setNumRem        ( rs.getString("numrem"      ) );
               r.setOrirem        ( rs.getString("origen"      ) );
               r.setDesrem        ( rs.getString("destino"     ) );
               r.setAgcrem        ( rs.getString("destinorel"  ) );
               r.setNombreOrigen  ( rs.getString("nomorigen"   ) );
               r.setNombreDestino ( rs.getString("nomdestino"  ) );
               r.setNombreDestinoRelacionado ( rs.getString("nomdestinorel"  ) );
               r.setValorRemesa   ( rs.getDouble("vlrrem"      ) );
               r.setCurrency      ( rs.getString("currency"    ) );
               r.setVlrrem2       ( rs.getFloat ("vlrrem2"     ) );
               r.setStd_job_no    ( rs.getString("std_job_no"  ) );
               r.setStd_job_desc  ( rs.getString("std_job_desc") );
               r.setCliente       ( rs.getString("cliente"     ) );
               r.setNombre_cli    ( rs.getString("nomcli"      ) );
               r.setPadre         ( rs.getString("agduenia"    ) ); // agencia due�a
               r.setNombre_ciu    ( rs.getString("nomagduenia" ) ); // nombre agencia due�a               
               r.setTipoViaje     ( rs.getString("tipoviaje"   ) );
               r.setEstado        ( rs.getString("estado"      ) );
               r.setUnitOfWork    ( rs.getString("unit_of_work") );
               r.setOt_padre      ( rs.getString("ot_padre"    ) );
               r.setOt_rela       ( rs.getString("ot_rela"     ) );
               r.setDescripcion   ( rs.getString("descripcion" ) );
               r.setFecRem        ( rs.getDate  ("fecrem"      ) );
               r.setMes           ( rs.getString("mesrem"      ) );
               r.setTasa          ( rs.getDouble("tasa"        ) );    
               if ((r.getReg_status()!=null && r.getReg_status().equals("A")) || (rs.getString("n_facturable")!=null && rs.getString("n_facturable").equals("S")) ){
                   r.setVlrrem2(0);
                   r.setValorRemesa(0);
               }             
               lista.add(r);
           }
        }catch (Exception ex){
            throw new Exception ("Error en PlanillaCostosDAO.obtenerRemesasPorPeriodosDePlanilla() ....\n" + ex.getMessage());
        }finally{
            if (rs!=null) rs.close();
            if (st!=null) st.close();
            this.desconectar("SQL_REMESAS_DE_PLANILLAS_POR_PERIODO");
        }
        return lista;
    }     
    
    /*Lisset Reales*/
    
    /**
     * Modificado el : 30 de septiembre de 2006, 10:01 AM
     * Modificado por : LREALES
     */
        
    /** Funcion publica que verifica si existe o no un reporte de utilidad ya creado por este usuario */
    public boolean existeReporteControl ( String login_usuario ) throws SQLException {
        
        boolean boo = false;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_EXISTE_REPCONTROL" );
            
            st.setString( 1, login_usuario );
            
            rs = st.executeQuery();
            
            if ( rs.next() ) {
                
                boo = true;
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE existeReporteControl.. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_EXISTE_REPCONTROL" );
            
        }
        
        return boo;
        
    }
    
    /** Funcion publica que inserta un registro en la tabla 'tablagen' */
    public void insertarReporteControl ( String login_usuario, String nombre_reporte ) throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_INSERTAR_REPCONTROL" );
            
            st.setString( 1, login_usuario );
            st.setString( 2, nombre_reporte );
            
            st.execute();
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE insertarReporteControl.. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_INSERTAR_REPCONTROL" );
            
        }
                
    }
    
    /** Funcion publica que actualiza un registro en la tabla 'tablagen' */
    public void actualizarReporteControl ( String nombre_reporte, String login_usuario ) throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_ACTUALIZAR_REPCONTROL" );
            
            st.setString( 1, nombre_reporte );
            st.setString( 2, login_usuario );            
            
            st.executeUpdate();
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE actualizarReporteControl.. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){                
                try{                    
                    st.close();                    
                } catch( SQLException e ){                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );                    
                }                
            }
            
            this.desconectar( "SQL_ACTUALIZAR_REPCONTROL" );
            
        }
                
    }
    
     /**
     * Metodo para actualizar los campos de utilidad en la tabla de plarem.
     * @autor mfontalvo
     * @param numrem, numero de la remesa.
     * @param numpla, numero de la planilla.
     * @param vlrpla, valor de la planilla.
     * @param vlrpla_pro, valor de la plamilla prorrateado.
     * @param vlrrem, valor de la remesa
     * @param vlrrem_pro, valor de la remesa prorrateado.
     * @throws Exception.
     */    
    public void updatePlarem(String numrem, String numpla, double vlrpla, double vlrpla_pro, double vlrrem, double vlrrem_pro) throws Exception {
        PreparedStatement st = null;
        String            query = "SQL_UPDATE_PLAREM";
        try{
           st = this.crearPreparedStatement(query);
           st.setDouble(1, vlrpla    );
           st.setDouble(2, vlrpla_pro);
           st.setDouble(3, vlrrem    );
           st.setDouble(4, vlrrem_pro);
           st.setString(5, numrem    );
           st.setString(6, numpla    );
           st.executeUpdate();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ("Error en PlanillaCostosDAO.updatePlarem() ....\n" + ex.getMessage());
        }finally{
            if (st!=null) st.close();
            this.desconectar(query);
        }
    }  

    
}

