/************************************************************************************
 * Nombre clase : ............... MigracionProveedorEscoltaDAO.java                 *
 * Descripcion :................. Objeto de acceso a datos de que permite recopilar *
 *                                toda la informacion necesaria para generar la     *
 *                                migracion a MIMS de proveedores de escoltas       *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                      *
 * Fecha :....................... 29 de Noviembre de 2005, 09:00 AM                 *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ************************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import com.tsp.operation.model.DAOS.MainDAO;

public class MigracionProveedorEscoltaDAO extends MainDAO {

    public  MigracionProveedorEscoltaDAO(){
        super(" MigracionProveedorEscoltaDAO.xml");
    }


    private Proveedor_Escolta  proveedor;
    
    private Vector vecProv;
    
    private static final String BUSQ_PROV_ESCOLTA  = 
    "select " +
    "      p.numpla," +
    "      p.despla," +
    "      p.plaveh," +
    "      e.placa as escolta," +
    "      n.nombre," +
    "      e.fecasignacion," +
    "      r.nit as proveedor," +
    "      r.tarifa " +
    "from " +
    "      escolta_vehiculo e," +
    "      planilla p,      " +
    "      Nit n," +
    "      placa pl," +
    "      proveedor_escolta r " +
    "where  " +
    "      p.numpla = e.numpla and" +
    "      pl.placa = e.placa and" +
    "      n.cedula = pl.conductor and" +
    "      pl.conductor = r.nit and" +
    "      e.estado='L' and"+ 
    "      e.fecasignacion between ? and ?";    
    
    private static final String BUSQ_PROV_ESC_CARAVANA  = 
    "select " +
    "      p.numpla," +
    "      p.despla," +
    "      p.plaveh," +
    "      e.placa as escolta," +
    "      n.nombre," +
    "      e.fecasignacion," +
    "      r.nit as proveedor," +
    "      r.tarifa " +
    "from " +
    "      escolta_caravana e," +
    "      caravana c," +
    "      planilla p,      " +
    "      Nit n," +
    "      placa pl," +
    "      proveedor_escolta r " +
    "where" +
    "      p.numpla = c.numpla and" +
    "      c.numcaravana = e.numcaravana and" +
    "      pl.placa = e.placa and" +
    "      n.cedula = pl.conductor and" +
    "      pl.conductor = r.nit and" +
    "      e.estado='L' and" +
    "      e.fecasignacion between ? and ?";    
    
    private String BUSCAR_PROVEEDORES = 
    "select" +
    "     P.nit," +
    "     nombre," +
    "     P.origen," +
    "     A.nomciu as nomorigen," +
    "     P.destino," +
    "     B.nomciu as nomdestino," +
    "     P.tarifa," +
    "     P.reg_status," +
    "     P.cod_contable " +
    "from " +
    "     proveedor_escolta P, " +
    "     nit," +
    "     Ciudad A," +
    "     Ciudad B " +
    "where " +
    "     cedula   = nit and" +
    "     A.codciu = origen and" +
    "     B.codciu = destino ";    
    
    
     /**
     * Metodo searchServiciosEscoltas, busca los servicios prestados por  vehiculos 
     * escoltas  entre dos fechas
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String fecha inicial, String fecha final
     * @version : 1.0
     */
    public Vector searchServiciosEscoltasAVehiculos(String feci, String fecf) throws SQLException {        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        Vector datos = null;
        vecProv = new Vector();
        String query = "SQL_BUSQ_PROV_ESCOLTA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, feci);
                st.setString(2, fecf);                
                rs = st.executeQuery();
                while (rs.next()) {
                    datos = new Vector();
                    datos.addElement(rs.getString(1));
                    datos.addElement(rs.getString(2));
                    datos.addElement(rs.getString(3));
                    datos.addElement(rs.getString(4));
                    datos.addElement(rs.getString(5));
                    datos.addElement(rs.getString(6));
                    datos.addElement(rs.getString(7));
                    datos.addElement(rs.getString(8));
                    vecProv.addElement(datos);
                }               
            }    
             return vecProv;
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR ASIGNACION" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
      
     /**
     * Metodo searchServiciosEscoltasACaravana, busca los servicios prestados por  vehiculos 
     * escoltas  a una caravana entre dos fechas
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String fecha inicial, String fecha final
     * @version : 1.0
     */
    public Vector searchServiciosEscoltasACaravana(String feci, String fecf) throws SQLException {        
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        Vector datos = null;
        vecProv = new Vector();
        String query = "SQL_BUSQ_PROV_ESC_CARAVANA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, feci);
                st.setString(2, fecf);                
                rs = st.executeQuery();
                while (rs.next()) {
                    datos = new Vector();
                    datos.addElement(rs.getString(1));
                    datos.addElement(rs.getString(2));
                    datos.addElement(rs.getString(3));
                    datos.addElement(rs.getString(4));
                    datos.addElement(rs.getString(5));
                    datos.addElement(rs.getString(6));
                    datos.addElement(rs.getString(7));
                    datos.addElement(rs.getString(8));
                    vecProv.addElement(datos);
                }                
            }    
             return vecProv;
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR ASIGNACION" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
}

