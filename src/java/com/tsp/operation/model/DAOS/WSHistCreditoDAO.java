package com.tsp.operation.model.DAOS;

import com.fintra.ws.datacredito.HCServiceImpl;
import com.fintra.ws.datacredito.ServicioHistoriaCredito;
import com.fintra.ws.datacredito.ServicioHistoriaCredito_Impl;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.GestionSolicitudAvalService;
import com.tsp.operation.model.services.NegociosGenService;
import com.tsp.operation.model.services.WSHistCreditoService;
import com.tsp.util.Util;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * DAO para las tablas del web service de historia de credito de datacredito<br/>
 * 25/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class WSHistCreditoDAO extends MainDAO {

    public WSHistCreditoDAO() {
        super("WSHistCreditoDAO.xml");
    }

    public WSHistCreditoDAO(String dataBaseName) {
        super("WSHistCreditoDAO.xml", dataBaseName);
    }

    /**
     * Vefifica la existencia de una persona en la bd
     * @throws Exception
     */
    public boolean existePersona(Persona persona) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "BUSCAR_PERSONA";
        boolean existe = false;
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, persona.getIdentificacion());
                st.setInt(2, Integer.parseInt(persona.getTipoIdentificacion()));
                rs = st.executeQuery();
                if(rs.next()){
                    existe = true;
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en existePersona() -->> [WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return existe;
    }

     /**
     * Vefifica la existencia de una persona en la bd
     * @throws Exception
     */
    public boolean existePersonaEmpresa(Persona persona) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "BUSCAR_PERSONA_EMPRESA";
        boolean existe = false;
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, persona.getIdentificacion());
                st.setInt(2, Integer.parseInt(persona.getTipoIdentificacion()));
                  st.setString(3, persona.getNitEmpresa());
                rs = st.executeQuery();
                if(rs.next()){
                    existe = true;
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en existePersona() -->> [WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return existe;
    }


    public String insertarPersona(Persona persona) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();
        
        try{
            campos.add("tipo_identificacion");
            valores.add(persona.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(persona.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(persona.getNitEmpresa())+"'");
            if(persona.getEstadoId()!=null){
                campos.add("estado_id");
                valores.add("'"+Util.escapeSQL(persona.getEstadoId())+"'");
            }
            if(persona.getFechaExpedicionId()!=null){
                campos.add("fecha_expedicion_id");
                valores.add("'"+Util.escapeSQL(persona.getFechaExpedicionId().toString())+"'");
            }
            if(persona.getCiudadId()!=null){
                campos.add("ciudad_id");
                valores.add("'"+Util.escapeSQL(persona.getCiudadId())+"'");
            }
            if(persona.getDepartamentoId()!=null){
                campos.add("departamento_id");
                valores.add("'"+Util.escapeSQL(persona.getDepartamentoId())+"'");
            }
            if(persona.getNombre()!=null){
                campos.add("nombre");
                valores.add("'"+Util.escapeSQL(persona.getNombre())+"'");
            }
            if(persona.getNombreCompleto()!=null){
                campos.add("nombre_completo");
                valores.add("'"+Util.escapeSQL(persona.getNombreCompleto())+"'");
            }
            if(persona.getPrimerApellido()!=null){
                campos.add("primer_apellido");
                valores.add("'"+Util.escapeSQL(persona.getPrimerApellido())+"'");
            }
            if(persona.getSegundoApellido()!=null){
                campos.add("segundo_apellido");
                valores.add("'"+Util.escapeSQL(persona.getSegundoApellido())+"'");
            }
            if(persona.getNacionalidad()!=null){
                campos.add("nacionalidad");
                valores.add("'"+Util.escapeSQL(persona.getNacionalidad())+"'");
            }
            if(persona.getGenero()!=null){
                campos.add("genero");
                valores.add("'"+Util.escapeSQL(persona.getGenero())+"'");
            }
            if(persona.getEstadoCivil()!=null){
                campos.add("estado_civil");
                valores.add("'"+Util.escapeSQL(persona.getEstadoCivil())+"'");
            }
            if(persona.getValidada()!=null){
                campos.add("validada");
                valores.add("'"+persona.getValidada()+"'");
            }
            if(persona.getEdadMin()!=null){
                campos.add("edad_min");
                valores.add("'"+Util.escapeSQL(persona.getEdadMin())+"'");
            }
            if(persona.getEdadMax()!=null){
                campos.add("edad_max");
                valores.add("'"+Util.escapeSQL(persona.getEdadMax())+"'");
            }
            if(persona.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(persona.getCreationUser())+"'");
            }
            if(persona.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(persona.getUserUpdate())+"'");
            }
            if(persona.getWebService().equals("HC")){
                campos.add("ultima_hc");
                valores.add("now()");
            }else{
                campos.add("ultima_localizacion");
                valores.add("now()");
            }

            sql = "insert into wsdc.persona(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";
            System.out.println("sql: "+sql);
        }catch(Exception ex){
            throw new Exception("ERROR en insertarPersona() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }
    
    public String editarPersona(Persona persona) throws Exception{
        StringStatement st = null;
        String query = "UPDATE_PERSONA";
        String sql;
        ArrayList campos = new ArrayList();

        try{

            if(persona.getEstadoId()!=null){
                campos.add("estado_id='"+Util.escapeSQL(persona.getEstadoId())+"'");
            }
            if(persona.getFechaExpedicionId()!=null){
                campos.add("fecha_expedicion_id='"+Util.escapeSQL(persona.getFechaExpedicionId().toString())+"'");
            }
            if(persona.getCiudadId()!=null){
                campos.add("ciudad_id='"+Util.escapeSQL(persona.getCiudadId())+"'");
            }
            if(persona.getDepartamentoId()!=null){
                campos.add("departamento_id='"+Util.escapeSQL(persona.getDepartamentoId())+"'");
            }
            if(persona.getNombre()!=null){
                campos.add("nombre='"+Util.escapeSQL(persona.getNombre())+"'");
            }
            if(persona.getNombreCompleto()!=null){
                campos.add("nombre_completo='"+Util.escapeSQL(persona.getNombreCompleto())+"'");
            }
            if(persona.getPrimerApellido()!=null){
                campos.add("primer_apellido='"+Util.escapeSQL(persona.getPrimerApellido())+"'");
            }
            if(persona.getSegundoApellido()!=null){
                campos.add("segundo_apellido='"+Util.escapeSQL(persona.getSegundoApellido())+"'");
            }
            if(persona.getNacionalidad()!=null){
                campos.add("nacionalidad='"+Util.escapeSQL(persona.getNacionalidad())+"'");
            }
            if(persona.getGenero()!=null){
                campos.add("genero='"+Util.escapeSQL(persona.getGenero())+"'");
            }
            if(persona.getEstadoCivil()!=null){
                campos.add("estado_civil='"+Util.escapeSQL(persona.getEstadoCivil())+"'");
            }
            if(persona.getValidada()!=null){
                campos.add("validada='"+persona.getValidada()+"'");
            }
            if(persona.getEdadMin()!=null){
                campos.add("edad_min='"+Util.escapeSQL(persona.getEdadMin())+"'");
            }
            if(persona.getEdadMax()!=null){
                campos.add("edad_max='"+Util.escapeSQL(persona.getEdadMax())+"'");
            }
            if(persona.getWebService().equals("HC")){
                campos.add("ultima_hc=now()");
            }else{
                campos.add("ultima_localizacion=now()");
            }
            campos.add("user_update='"+persona.getUserUpdate()+"'");
            campos.add("last_update=now()");

            st = new StringStatement(this.obtenerSQL(query).replaceAll("#CAMPOS#", Util.join(campos, ", ")), true);
            st.setString(1, persona.getIdentificacion());
            st.setInt(2, Integer.parseInt(persona.getTipoIdentificacion()));
            st.setString(3, persona.getNitEmpresa());
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en editarPersona() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarCuentasAhorro(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_CUENTAS_AHORRO";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarCuentasAhorro() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarCuentasCorriente(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_CUENTAS_CORRIENTE";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarCuentasCorriente() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarCuentasCartera(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_CUENTAS_CARTERA";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarCuentasCartera() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarTarjetasCredito(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_TARJETAS_CREDITO";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarTarjetasCredito() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarEndeudamientoGlobal(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_ENDEUDAMIENTO_GLOBAL";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarEndeudamientoGlobal() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarConsultas(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_CONSULTAS";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarConsultas() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarAlertas(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_ALERTAS";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarAlertas() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarScores(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_SCORES";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarScores() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarReclamos(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_RECLAMOS";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarReclamos() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarValores(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_VALORES";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarValores() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarAlivios(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_ALIVIOS";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarAlivios() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarComentarios(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_COMENTARIOS";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarComentarios() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de una persona
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarRespuestaPersonalizada(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_RESPUESTA_PERSONALIZADA";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarRespuestaPersonalizada() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Elimina los registros de la respuesta personalizada
     * @param identificacion identificacion de la persona
     * @param tipoIdentificacion tipo de identificacion
     * @return query generado
     * @throws Exception
     */
    public String eliminarRespuestaPersonalizadaDecisor(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        StringStatement st = null;
        String query = "ELIMINAR_RESPUESTA_PERSONALIZADA_DECISOR";
        String sql;
        try{
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, identificacion);
            st.setInt(2, Integer.parseInt(tipoIdentificacion));
            st.setString(3, nitEmpresa);
            sql = st.getSql();

        }catch(Exception ex){
            throw new Exception("ERROR en eliminarRespuestaPersonalizaDecisor() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st = null;  } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }

    /**
     * Inserta una cuenta de ahorro en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaAhorro(CuentaAhorro cuenta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(cuenta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(cuenta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(cuenta.getNitEmpresa())+"'");
            if(cuenta.getEstado()!=null){
                campos.add("estado");
                valores.add("'"+Util.escapeSQL(cuenta.getEstado())+"'");
            }
            if(cuenta.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(cuenta.getEntidad())+"'");
            }
            if(cuenta.getUltimaActualizacion()!=null){
                campos.add("ultima_actualizacion");
                valores.add("'"+cuenta.getUltimaActualizacion()+"'");
            }
            if(cuenta.getNumeroCuenta()!=null){
                campos.add("numero_cuenta");
                valores.add("'"+Util.escapeSQL(cuenta.getNumeroCuenta())+"'");
            }
            if(cuenta.getFechaApertura()!=null){
                campos.add("fecha_apertura");
                valores.add("'"+cuenta.getFechaApertura()+"'");
            }
            if(cuenta.getOficina()!=null){
                campos.add("oficina");
                valores.add("'"+Util.escapeSQL(cuenta.getOficina())+"'");
            }
            if(cuenta.getCiudad()!=null){
                campos.add("ciudad");
                valores.add("'"+Util.escapeSQL(cuenta.getCiudad())+"'");
            }
            if(cuenta.getBloqueada()!=null){
                campos.add("bloqueada");
                valores.add("'"+cuenta.getBloqueada()+"'");
            }
            if(cuenta.getCodSuscriptor()!=null){
                campos.add("cod_suscriptor");
                valores.add("'"+Util.escapeSQL(cuenta.getCodSuscriptor())+"'");
            }
            if(cuenta.getSituacionTitular()!=null){
                campos.add("situacion_titular");
                valores.add("'"+cuenta.getSituacionTitular()+"'");
            }
            if(cuenta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(cuenta.getCreationUser())+"'");
            }
            if(cuenta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(cuenta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.cuenta_ahorro(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarCuentaAhorro() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta una cuenta de ahorro en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaAhorroDecisor(CuentaAhorroDecisor cuenta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(cuenta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(cuenta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(cuenta.getNitEmpresa())+"'");
            if(cuenta.getEstado()!=null){
                campos.add("estado");
                valores.add("'"+Util.escapeSQL(cuenta.getEstado())+"'");
            }
            if(cuenta.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(cuenta.getEntidad())+"'");
            }
            if(cuenta.getUltimaActualizacion()!=null){
                campos.add("ultima_actualizacion");
                valores.add("'"+cuenta.getUltimaActualizacion()+"'");
            }
            if(cuenta.getNumero()!=null){
                campos.add("numero_cuenta");
                valores.add("'"+Util.escapeSQL(cuenta.getNumero())+"'");
            }
            if(cuenta.getFechaApertura()!=null){
                campos.add("fecha_apertura");
                valores.add("'"+cuenta.getFechaApertura()+"'");
            }
            if(cuenta.getOficina()!=null){
                campos.add("oficina");
                valores.add("'"+Util.escapeSQL(cuenta.getOficina())+"'");
            }
            if(cuenta.getCiudad()!=null){
                campos.add("ciudad");
                valores.add("'"+Util.escapeSQL(cuenta.getCiudad())+"'");
            }
            if(cuenta.getBloqueada()!=null){
                campos.add("bloqueada");
                valores.add("'"+cuenta.getBloqueada()+"'");
            }
            /*if(cuenta.getCodigoDaneCiudad()!=null){
                campos.add("cod_suscriptor");
                valores.add("'"+Util.escapeSQL(cuenta.getCodigoDaneCiudad())+"'");
            }*/
            if(cuenta.getSituacionTitular()!=null){
                campos.add("situacion_titular");
                valores.add("'"+cuenta.getSituacionTitular()+"'");
            }
            if(cuenta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(cuenta.getCreationUser())+"'");
            }
            if(cuenta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(cuenta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.cuenta_ahorro(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";
            System.out.println("cuenta ahorros decisor: "+sql);

        }catch(Exception ex){
            throw new Exception("ERROR en insertarCuentaAhorroDecisor() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta una cuenta corriente en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaCorriente(CuentaCorriente cuenta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(cuenta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(cuenta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(cuenta.getNitEmpresa())+"'");
            if(cuenta.getBloqueada()!=null){
                campos.add("bloqueada");
                valores.add("'"+cuenta.getBloqueada()+"'");
            }
            if(cuenta.getEstado()!=null){
                campos.add("estado");
                valores.add("'"+Util.escapeSQL(cuenta.getEstado())+"'");
            }
            if(cuenta.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(cuenta.getEntidad())+"'");
            }
            if(cuenta.getUltimaActualizacion()!=null){
                campos.add("ultima_actualizacion");
                valores.add("'"+cuenta.getUltimaActualizacion()+"'");
            }
            if(cuenta.getNumeroCuenta()!=null){
                campos.add("numero_cuenta");
                valores.add("'"+Util.escapeSQL(cuenta.getNumeroCuenta())+"'");
            }
            if(cuenta.getFechaApertura()!=null){
                campos.add("fecha_apertura");
                valores.add("'"+cuenta.getFechaApertura()+"'");
            }
            if(cuenta.getOficina()!=null){
                campos.add("oficina");
                valores.add("'"+Util.escapeSQL(cuenta.getOficina())+"'");
            }
            if(cuenta.getCiudad()!=null){
                campos.add("ciudad");
                valores.add("'"+Util.escapeSQL(cuenta.getCiudad())+"'");
            }
            if(cuenta.getCodSuscriptor()!=null){
                campos.add("cod_suscriptor");
                valores.add("'"+Util.escapeSQL(cuenta.getCodSuscriptor())+"'");
            }
            if(cuenta.getSituacionTitular()!=null){
                campos.add("situacion_titular");
                valores.add("'"+Util.escapeSQL(cuenta.getSituacionTitular())+"'");
            }
            if(cuenta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(cuenta.getCreationUser())+"'");
            }
            if(cuenta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(cuenta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.cuenta_corriente(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarCuentaAhorro() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    
    /**
     * Inserta una cuenta corriente Decisor en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaCorrienteDecisor(CuentaCorrienteDecisor cuenta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(cuenta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(cuenta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(cuenta.getNitEmpresa())+"'");
            if(cuenta.getBloqueada()!=null){
                campos.add("bloqueada");
                valores.add("'"+cuenta.getBloqueada()+"'");
            }
            if(cuenta.getEstado()!=null){
                campos.add("estado");
                valores.add("'"+Util.escapeSQL(cuenta.getEstado())+"'");
            }
            if(cuenta.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(cuenta.getEntidad())+"'");
            }
            if(cuenta.getUltimaActualizacion()!=null){
                campos.add("ultima_actualizacion");
                valores.add("'"+cuenta.getUltimaActualizacion()+"'");
            }
            if(cuenta.getNumero()!=null){
                campos.add("numero_cuenta");
                valores.add("'"+Util.escapeSQL(cuenta.getNumero())+"'");
            }
            if(cuenta.getFechaApertura()!=null){
                campos.add("fecha_apertura");
                valores.add("'"+cuenta.getFechaApertura()+"'");
            }
            if(cuenta.getOficina()!=null){
                campos.add("oficina");
                valores.add("'"+Util.escapeSQL(cuenta.getOficina())+"'");
            }
            if(cuenta.getCiudad()!=null){
                campos.add("ciudad");
                valores.add("'"+Util.escapeSQL(cuenta.getCiudad())+"'");
            }
            if(cuenta.getCodSuscriptor()!=null){
                campos.add("cod_suscriptor");
                valores.add("'"+Util.escapeSQL(cuenta.getCodSuscriptor())+"'");
            }
            if(cuenta.getSituacionTitular()!=null){
                campos.add("situacion_titular");
                valores.add("'"+Util.escapeSQL(cuenta.getSituacionTitular())+"'");
            }
            if(cuenta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(cuenta.getCreationUser())+"'");
            }
            if(cuenta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(cuenta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.cuenta_corriente(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";
            System.out.println("Cuenta corriente decisor : "+ sql);

        }catch(Exception ex){
            throw new Exception("ERROR en insertarCuentaAhorroDecisor() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta una cuenta de cartera en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaCartera(CuentaCartera cuenta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(cuenta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(cuenta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(cuenta.getNitEmpresa())+"'");
            if(cuenta.getBloqueada()!=null){
                campos.add("bloqueada");
                valores.add("'"+cuenta.getBloqueada()+"'");
            }
            if(cuenta.getPeriodicidad()!=null){
                campos.add("periodicidad");
                valores.add("'"+Util.escapeSQL(cuenta.getPeriodicidad())+"'");
            }
            if(cuenta.getComportamiento()!=null){
                campos.add("comportamiento");
                valores.add("'"+Util.escapeSQL(cuenta.getComportamiento())+"'");
            }
            if(cuenta.getFechaApertura()!=null){
                campos.add("fecha_apertura");
                valores.add("'"+cuenta.getFechaApertura()+"'");
            }
            if(cuenta.getFechaVencimiento()!=null){
                campos.add("fecha_vencimiento");
                valores.add("'"+cuenta.getFechaVencimiento()+"'");
            }
            if(cuenta.getNumeroObligacion()!=null){
                campos.add("numero_obligacion");
                valores.add("'"+Util.escapeSQL(cuenta.getNumeroObligacion())+"'");
            }
            if(cuenta.getUltimaActualizacion()!=null){
                campos.add("ultima_actualizacion");
                valores.add("'"+cuenta.getUltimaActualizacion()+"'");
            }
            if(cuenta.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(cuenta.getEntidad())+"'");
            }
            if(cuenta.getEstado()!=null){
                campos.add("estado");
                valores.add("'"+Util.escapeSQL(cuenta.getEstado())+"'");
            }
            if(cuenta.getEstado48()!=null){
                campos.add("estado48");
                valores.add("'"+Util.escapeSQL(cuenta.getEstado48())+"'");
            }
            if(cuenta.getTipoObligacion()!=null){
                campos.add("tipo_obligacion");
                valores.add("'"+Util.escapeSQL(cuenta.getTipoObligacion())+"'");
            }
            if(cuenta.getTipoCuenta()!=null){
                campos.add("tipo_cuenta");
                valores.add("'"+Util.escapeSQL(cuenta.getTipoCuenta())+"'");
            }
            if(cuenta.getGarante()!=null){
                campos.add("garante");
                valores.add("'"+Util.escapeSQL(cuenta.getGarante())+"'");
            }
            if(cuenta.getFormaPago()!=null){
                campos.add("forma_pago");
                valores.add("'"+Util.escapeSQL(cuenta.getFormaPago())+"'");
            }
            if(cuenta.getCodSuscriptor()!=null){
                campos.add("cod_suscriptor");
                valores.add("'"+Util.escapeSQL(cuenta.getCodSuscriptor())+"'");
            }
            if(cuenta.getPositivoNegativo()!=null){
                campos.add("positivo_negativo");
                valores.add("'"+Util.escapeSQL(cuenta.getPositivoNegativo())+"'");
            }
            if(cuenta.getOficina()!=null){
                campos.add("oficina");
                valores.add("'"+Util.escapeSQL(cuenta.getOficina())+"'");
            }
            if(cuenta.getMesesPermanencia()!=null){
                campos.add("meses_permanencia");
                valores.add("'"+cuenta.getMesesPermanencia()+"'");
            }
            if(cuenta.getSituacionTitular()!=null){
                campos.add("situacion_titular");
                valores.add("'"+Util.escapeSQL(cuenta.getSituacionTitular())+"'");
            }
            if(cuenta.getEstadoOrigen()!=null){
                campos.add("estado_origen");
                valores.add("'"+Util.escapeSQL(cuenta.getEstadoOrigen())+"'");
            }
            if(cuenta.getTipoContrato()!=null){
                campos.add("tipo_contrato");
                valores.add("'"+Util.escapeSQL(cuenta.getTipoContrato())+"'");
            }
            if(cuenta.getEjecucionContrato()!=null){
                campos.add("ejecucion_contrato");
                valores.add("'"+cuenta.getEjecucionContrato()+"'");
            }
            if(cuenta.getPrescripcion()!=null){
                campos.add("prescripcion");
                valores.add("'"+Util.escapeSQL(cuenta.getPrescripcion())+"'");
            }            
            if(cuenta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(cuenta.getCreationUser())+"'");
            }
            if(cuenta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(cuenta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.cuenta_cartera(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarCuentaAhorro() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta una cuenta de cartera Decisor en la tabla
     * @param cuenta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarCuentaCarteraDecisor(CuentaCarteraDecisor cuenta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(cuenta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(cuenta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(cuenta.getNitEmpresa())+"'");
            if(cuenta.getBloqueada()!=null){
                campos.add("bloqueada");
                valores.add("'"+cuenta.getBloqueada()+"'");
            }
            if(cuenta.getPeriodicidad()!=null){
                campos.add("periodicidad");
                valores.add("''");//no esta
            }
            if(cuenta.getComportamiento()!=null){
                campos.add("comportamiento");
                valores.add("'"+Util.escapeSQL(cuenta.getComportamiento())+"'");
            }
            if(cuenta.getFechaApertura()!=null){
                campos.add("fecha_apertura");
                valores.add("'"+cuenta.getFechaApertura()+"'");
            }
            if(cuenta.getFechaVencimiento()!=null){
                campos.add("fecha_vencimiento");
                valores.add("'"+cuenta.getFechaVencimiento()+"'");
            }
            if(cuenta.getNumero()!=null){
                campos.add("numero_obligacion");
                valores.add("'"+Util.escapeSQL(cuenta.getNumero())+"'");
            }
            if(cuenta.getUltimaActualizacion()!=null){
                campos.add("ultima_actualizacion");
                valores.add("'"+cuenta.getUltimaActualizacion()+"'");
            }
            if(cuenta.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(cuenta.getEntidad())+"'");
            }
            if(cuenta.getEstado()!=null){
                campos.add("estado");
                valores.add("'"+Util.escapeSQL(cuenta.getEstado())+"'");
            }
           
            if(cuenta.getTipoObligacion()!=null){
                campos.add("tipo_obligacion");
                valores.add("'"+Util.escapeSQL(cuenta.getTipoObligacion())+"'");
            }
            if(cuenta.getTipoCuenta()!=null){
                campos.add("tipo_cuenta");
                valores.add("'"+Util.escapeSQL(cuenta.getTipoCuenta())+"'");
            }
            
            if(cuenta.getFormaPago()!=null){
                campos.add("forma_pago");
                valores.add("'"+Util.escapeSQL(cuenta.getFormaPago())+"'");
            }
            if(cuenta.getCodSuscriptor()!=null){
                campos.add("cod_suscriptor");
                valores.add("'"+Util.escapeSQL(cuenta.getCodSuscriptor())+"'");
            }
           
            if(cuenta.getOficina()!=null){
                campos.add("oficina");
                valores.add("'"+Util.escapeSQL(cuenta.getOficina())+"'");
            }
            if(cuenta.getMesesPermanencia()!=null){
                campos.add("meses_permanencia");
                valores.add("'"+cuenta.getMesesPermanencia()+"'");
            }
            if(cuenta.getSituacionTitular()!=null){
                campos.add("situacion_titular");
                valores.add("'"+Util.escapeSQL(cuenta.getSituacionTitular())+"'");
            }
            if(cuenta.getEstadoOrigen()!=null){
                campos.add("estado_origen");
                valores.add("'"+Util.escapeSQL(cuenta.getEstadoOrigen())+"'");
            }
            if(cuenta.getTipoContrato()!=null){
                campos.add("tipo_contrato");
                valores.add("'"+Util.escapeSQL(cuenta.getTipoContrato())+"'");
            }
            if(cuenta.getEjecucionContrato()!=null){
                campos.add("ejecucion_contrato");
                valores.add("'"+cuenta.getEjecucionContrato()+"'");
            }
                      
            if(cuenta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(cuenta.getCreationUser())+"'");
            }
            if(cuenta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(cuenta.getUserUpdate())+"'");
            }

            if(cuenta.getGarantia()!=null){
                campos.add("garante");
                valores.add("'"+cuenta.getCalidadDeudor()+"'");
            }

            sql = "insert into wsdc.cuenta_cartera(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";
            System.out.println("Cuenta Cartera Decisor: "+sql);
            

        }catch(Exception ex){
            throw new Exception("ERROR en insertarCuentaAhorroDecisor() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta una cuenta corriente en la tabla
     * @param tarjeta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarTarjetaCredito(TarjetaCredito tarjeta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(tarjeta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(tarjeta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(tarjeta.getNitEmpresa())+"'");
            if(tarjeta.getEstado()!=null){
                campos.add("estado");
                valores.add("'"+Util.escapeSQL(tarjeta.getEstado())+"'");
            }
            if(tarjeta.getEstado48()!=null){
                campos.add("estado48");
                valores.add("'"+Util.escapeSQL(tarjeta.getEstado48())+"'");
            }
            if(tarjeta.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(tarjeta.getEntidad())+"'");
            }
            if(tarjeta.getUltimaActualizacion()!=null){
                campos.add("ultima_actualizacion");
                valores.add("'"+tarjeta.getUltimaActualizacion()+"'");
            }
            if(tarjeta.getNumero()!=null){
                campos.add("numero");
                valores.add("'"+Util.escapeSQL(tarjeta.getNumero())+"'");
            }
            if(tarjeta.getFechaApertura()!=null){
                campos.add("fecha_apertura");
                valores.add("'"+Util.escapeSQL(tarjeta.getFechaApertura().toString())+"'");
            }
            if(tarjeta.getFechaVencimiento()!=null){
                campos.add("fecha_vencimiento");
                valores.add("'"+Util.escapeSQL(tarjeta.getFechaVencimiento().toString())+"'");
            }
            if(tarjeta.getComportamiento()!=null){
                campos.add("comportamiento");
                valores.add("'"+Util.escapeSQL(tarjeta.getComportamiento())+"'");
            }
            if(tarjeta.getAmparada()!=null){
                campos.add("amparada");
                valores.add("'"+tarjeta.getAmparada()+"'");
            }
            if(tarjeta.getFormaPago()!=null){
                campos.add("forma_pago");
                valores.add("'"+Util.escapeSQL(tarjeta.getFormaPago())+"'");
            }
            if(tarjeta.getBloqueada()!=null){
                campos.add("bloqueada");
                valores.add("'"+tarjeta.getBloqueada()+"'");
            }
            if(tarjeta.getCodSuscriptor()!=null){
                campos.add("cod_suscriptor");
                valores.add("'"+Util.escapeSQL(tarjeta.getCodSuscriptor())+"'");
            }
            if(tarjeta.getPositivoNegativo()!=null){
                campos.add("positivo_negativo");
                valores.add("'"+Util.escapeSQL(tarjeta.getPositivoNegativo())+"'");
            }
            if(tarjeta.getOficina()!=null){
                campos.add("oficina");
                valores.add("'"+Util.escapeSQL(tarjeta.getOficina())+"'");
            }
            if(tarjeta.getSituacionTitular()!=null){
                campos.add("situacion_titular");
                valores.add("'"+tarjeta.getSituacionTitular()+"'");
            }
            if(tarjeta.getEstadoOrigen()!=null){
                campos.add("estado_origen");
                valores.add("'"+Util.escapeSQL(tarjeta.getEstadoOrigen())+"'");
            }
            if(tarjeta.getPrescripcion()!=null){
                campos.add("prescripcion");
                valores.add("'"+Util.escapeSQL(tarjeta.getPrescripcion())+"'");
            }
            if(tarjeta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(tarjeta.getCreationUser())+"'");
            }
            if(tarjeta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(tarjeta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.tarjeta_credito(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarTarjetaCredito() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta una tarjeta Credito Decisor en la tabla
     * @param tarjeta informacion de la cuenta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarTarjetaCreditoDecisor(TarjetaCreditoDecisor tarjeta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(tarjeta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(tarjeta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(tarjeta.getNitEmpresa())+"'");
            if(tarjeta.getEstado()!=null){
                campos.add("estado");
                valores.add("'"+Util.escapeSQL(tarjeta.getEstado())+"'");
            }
            if(tarjeta.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(tarjeta.getEntidad())+"'");
            }
            if(tarjeta.getUltimaActualizacion()!=null){
                campos.add("ultima_actualizacion");
                valores.add("'"+tarjeta.getUltimaActualizacion()+"'");
            }
            if(tarjeta.getNumero()!=null){
                campos.add("numero");
                valores.add("'"+Util.escapeSQL(tarjeta.getNumero())+"'");
            }
            if(tarjeta.getFechaApertura()!=null){
                campos.add("fecha_apertura");
                valores.add("'"+Util.escapeSQL(tarjeta.getFechaApertura().toString())+"'");
            }
            if(tarjeta.getFechaVencimiento()!=null){
                campos.add("fecha_vencimiento");
                valores.add("'"+Util.escapeSQL(tarjeta.getFechaVencimiento().toString())+"'");
            }
            if(tarjeta.getComportamiento()!=null){
                campos.add("comportamiento");
                valores.add("'"+Util.escapeSQL(tarjeta.getComportamiento())+"'");
            }
            if(tarjeta.getAmparada()!=null){
                campos.add("amparada");
                valores.add("'"+tarjeta.getAmparada()+"'");
            }
            if(tarjeta.getFormaPago()!=null){
                campos.add("forma_pago");
                valores.add("'"+Util.escapeSQL(tarjeta.getFormaPago())+"'");
            }
            if(tarjeta.getBloqueada()!=null){
                campos.add("bloqueada");
                valores.add("'"+tarjeta.getBloqueada()+"'");
            }
            
            if(tarjeta.getOficina()!=null){
                campos.add("oficina");
                valores.add("'"+Util.escapeSQL(tarjeta.getOficina())+"'");
            }
            if(tarjeta.getSituacionTitular()!=null){
                campos.add("situacion_titular");
                valores.add("'"+tarjeta.getSituacionTitular()+"'");
            }
            if(tarjeta.getEstadoOrigen()!=null){
                campos.add("estado_origen");
                valores.add("'"+Util.escapeSQL(tarjeta.getEstadoOrigen())+"'");
            }
           
            if(tarjeta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(tarjeta.getCreationUser())+"'");
            }
            if(tarjeta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(tarjeta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.tarjeta_credito(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";
            System.out.println("Tarjeta credito decisor: "+sql);

        }catch(Exception ex){
            throw new Exception("ERROR en insertarTarjetaCreditoDecisor() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta un endeudamiento global en la tabla
     * @param endeudamiento informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarEndeudamientoGlobal(EndeudamientoGlobal endeudamiento) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(endeudamiento.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(endeudamiento.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(endeudamiento.getNitEmpresa())+"'");
            if(endeudamiento.getCalificacion()!=null){
                campos.add("calificacion");
                valores.add("'"+Util.escapeSQL(endeudamiento.getCalificacion())+"'");
            }
            if(endeudamiento.getSaldoPendiente()!=null){
                campos.add("saldo_pendiente");
                valores.add("'"+endeudamiento.getSaldoPendiente()+"'");
            }
            if(endeudamiento.getTipoCredito()!=null){
                campos.add("tipo_credito");
                valores.add("'"+Util.escapeSQL(endeudamiento.getTipoCredito())+"'");
            }
            if(endeudamiento.getMoneda()!=null){
                campos.add("moneda");
                valores.add("'"+Util.escapeSQL(endeudamiento.getMoneda())+"'");
            }
            if(endeudamiento.getNumeroCreditos()!=null){
                campos.add("numero_creditos");
                valores.add("'"+endeudamiento.getNumeroCreditos()+"'");
            }
            if(endeudamiento.getFechaReporte()!=null){
                campos.add("fecha_reporte");
                valores.add("'"+endeudamiento.getFechaReporte()+"'");
            }
            if(endeudamiento.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(endeudamiento.getEntidad())+"'");
            }
            if(endeudamiento.getGarantia()!=null){
                campos.add("garantia");
                valores.add("'"+Util.escapeSQL(endeudamiento.getGarantia())+"'");
            }
            if(endeudamiento.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(endeudamiento.getCreationUser())+"'");
            }
            if(endeudamiento.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(endeudamiento.getUserUpdate())+"'");
            }


            sql = "insert into wsdc.endeudamiento_global(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarEndeudamientoGlobal() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta un endeudamiento global Decisor en la tabla
     * @param endeudamiento informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarEndeudamientoGlobalDecisor(EndeudamientoGlobalDecisor endeudamiento) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(endeudamiento.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(endeudamiento.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(endeudamiento.getNitEmpresa())+"'");
            if(endeudamiento.getCalificacion()!=null){
                campos.add("calificacion");
                valores.add("'"+Util.escapeSQL(endeudamiento.getCalificacion())+"'");
            }
            if(endeudamiento.getSaldoPendiente()!=null){
                campos.add("saldo_pendiente");
                valores.add("'"+endeudamiento.getSaldoPendiente()+"'");
            }
            if(endeudamiento.getTipoCredito()!=null){
                campos.add("tipo_credito");
                valores.add("'"+Util.escapeSQL(endeudamiento.getTipoCredito())+"'");
            }
            if(endeudamiento.getMoneda()!=null){
                campos.add("moneda");
                valores.add("'"+Util.escapeSQL(endeudamiento.getMoneda())+"'");
            }
            if(endeudamiento.getNumeroCreditos()!=null){
                campos.add("numero_creditos");
                valores.add("'"+endeudamiento.getNumeroCreditos()+"'");
            }
            if(endeudamiento.getFechaReporte()!=null){
                campos.add("fecha_reporte");
                valores.add("'"+endeudamiento.getFechaReporte()+"'");
            }
            if(endeudamiento.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(endeudamiento.getEntidad())+"'");
            }
            if(endeudamiento.getGarantia()!=null){
                campos.add("garantia");
                valores.add("'"+Util.escapeSQL(endeudamiento.getGarantia())+"'");
            }
            if(endeudamiento.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(endeudamiento.getCreationUser())+"'");
            }
            if(endeudamiento.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(endeudamiento.getUserUpdate())+"'");
            }


            sql = "insert into wsdc.endeudamiento_global(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";
            
            System.out.println("Endeudamiento global decisor: "+sql);

        }catch(Exception ex){
            throw new Exception("ERROR en insertarEndeudamientoGlobal() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }



    /**
     * Inserta una consulta en la tabla
     * @param consulta informacion de la consulta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarConsulta(Consulta consulta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(consulta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(consulta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(consulta.getNitEmpresa())+"'");
            if(consulta.getFecha()!=null){
                campos.add("fecha");
                valores.add("'"+consulta.getFecha()+"'");
            }
            if(consulta.getTipoCuenta()!=null){
                campos.add("tipo_cuenta");
                valores.add("'"+Util.escapeSQL(consulta.getTipoCuenta())+"'");
            }
            if(consulta.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(consulta.getEntidad())+"'");
            }
            if(consulta.getOficina()!=null){
                campos.add("oficina");
                valores.add("'"+Util.escapeSQL(consulta.getOficina())+"'");
            }
            if(consulta.getCiudad()!=null){
                campos.add("ciudad");
                valores.add("'"+Util.escapeSQL(consulta.getCiudad())+"'");
            }
            if(consulta.getRazon()!=null){
                campos.add("razon");
                valores.add("'"+Util.escapeSQL(consulta.getRazon())+"'");
            }
            if(consulta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(consulta.getCreationUser())+"'");
            }
            if(consulta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(consulta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.consulta(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarConsulta() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta una consulta Decisor en la tabla
     * @param consulta informacion de la consulta a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarConsultaDecisor(ConsultaDecisor consulta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(consulta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(consulta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(consulta.getNitEmpresa())+"'");
            if(consulta.getFecha()!=null){
                campos.add("fecha");
                valores.add("'"+consulta.getFecha()+"'");
            }
            if(consulta.getTipoCuenta()!=null){
                campos.add("tipo_cuenta");
                valores.add("'"+Util.escapeSQL(consulta.getTipoCuenta())+"'");
            }
            if(consulta.getEntidad()!=null){
                campos.add("entidad");
                valores.add("'"+Util.escapeSQL(consulta.getEntidad())+"'");
            }
            if(consulta.getOficina()!=null){
                campos.add("oficina");
                valores.add("'"+Util.escapeSQL(consulta.getOficina())+"'");
            }
            if(consulta.getCiudad()!=null){
                campos.add("ciudad");
                valores.add("'"+Util.escapeSQL(consulta.getCiudad())+"'");
            }
            if(consulta.getRazon()!=null){
                campos.add("razon");
                valores.add("'"+Util.escapeSQL(consulta.getRazon())+"'");
            }
            if(consulta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(consulta.getCreationUser())+"'");
            }
            if(consulta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(consulta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.consulta(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";
            
            System.out.println("Consulta decisor :"+sql);

        }catch(Exception ex){
            throw new Exception("ERROR en insertarConsultaDecisor() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta una alerta en la tabla
     * @param alerta informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarAlerta(Alerta alerta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(alerta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(alerta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(alerta.getNitEmpresa())+"'");
            if(alerta.getColocacion()!=null){
                campos.add("colocacion");
                valores.add("'"+alerta.getColocacion()+"'");
            }
            if(alerta.getVencimiento()!=null){
                campos.add("vencimiento");
                valores.add("'"+alerta.getVencimiento()+"'");
            }
            if(alerta.getModificacion()!=null){
                campos.add("modificacion");
                valores.add("'"+alerta.getModificacion()+"'");
            }
            if(alerta.getCodigo()!=null){
                campos.add("codigo");
                valores.add("'"+Util.escapeSQL(alerta.getCodigo())+"'");
            }
            if(alerta.getCodigoFuente()!=null){
                campos.add("codigo_fuente");
                valores.add("'"+Util.escapeSQL(alerta.getCodigoFuente())+"'");
            }
            if(alerta.getTexto()!=null){
                campos.add("texto");
                valores.add("'"+Util.escapeSQL(alerta.getTexto())+"'");
            }
            if(alerta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(alerta.getCreationUser())+"'");
            }
            if(alerta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(alerta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.alerta(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarAlerta() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta una alerta Decisor en la tabla
     * @param alerta informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarAlertaDecisor(AlertaDecisor alerta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(alerta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(alerta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(alerta.getNitEmpresa())+"'");
            if(alerta.getColocacion()!=null){
                campos.add("colocacion");
                valores.add("'"+alerta.getColocacion()+"'");
            }
            if(alerta.getVencimiento()!=null){
                campos.add("vencimiento");
                valores.add("'"+alerta.getVencimiento()+"'");
            }
            if(alerta.getModificacion()!=null){
                campos.add("modificacion");
                valores.add("'"+alerta.getModificacion()+"'");
            }
            if(alerta.getCodigo()!=null){
                campos.add("codigo");
                valores.add("'"+Util.escapeSQL(alerta.getCodigo())+"'");
            }
            if(alerta.getCodigoFuente()!=null){
                campos.add("codigo_fuente");
                valores.add("'"+Util.escapeSQL(alerta.getCodigoFuente())+"'");
            }
            if(alerta.getTexto()!=null){
                campos.add("texto");
                valores.add("'"+Util.escapeSQL(alerta.getTexto())+"'");
            }
            if(alerta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(alerta.getCreationUser())+"'");
            }
            if(alerta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(alerta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.alerta(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";
            
            System.out.println("Alerta decisor: "+sql);

        }catch(Exception ex){
            throw new Exception("ERROR en insertarAlertaDecisor() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta un reclamo en la tabla
     * @param reclamo Bean con la informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarReclamo(Reclamo reclamo) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(reclamo.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(reclamo.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(reclamo.getNitEmpresa())+"'");
            campos.add("tipo_padre");
            valores.add("'"+Util.escapeSQL(reclamo.getTipoPadre())+"'");

            campos.add("id_padre");
            if(reclamo.getTipoPadre().equals("CAH")){
                valores.add("currval('wsdc.cuenta_ahorro_id_seq')");
            }else if(reclamo.getTipoPadre().equals("CCO")){
                valores.add("currval('wsdc.cuenta_corriente_id_seq')");
            }else if(reclamo.getTipoPadre().equals("CCA")){
                valores.add("currval('wsdc.cuenta_cartera_id_seq')");
            }else if(reclamo.getTipoPadre().equals("TCR")){
                valores.add("currval('wsdc.tarjeta_credito_id_seq')");
            }
            if(reclamo.getTipoLeyenda()!=null){
                campos.add("tipo_leyenda");
                valores.add("'"+Util.escapeSQL(reclamo.getTipoLeyenda())+"'");
            }
            if(reclamo.getFechaCierre()!=null){
                campos.add("fecha_cierre");
                valores.add("'"+reclamo.getFechaCierre()+"'");
            }
            if(reclamo.getEstado()!=null){
                campos.add("estado");
                valores.add("'"+Util.escapeSQL(reclamo.getEstado())+"'");
            }
            if(reclamo.getTipo()!=null){
                campos.add("tipo");
                valores.add("'"+Util.escapeSQL(reclamo.getTipo())+"'");
            }
            if(reclamo.getFecha()!=null){
                campos.add("fecha");
                valores.add("'"+reclamo.getFecha()+"'");
            }
            if(reclamo.getRatificado()!=null){
                campos.add("ratificado");
                valores.add("'"+reclamo.getRatificado()+"'");
            }
            if(reclamo.getTexto()!=null){
                campos.add("texto");
                valores.add("'"+Util.escapeSQL(reclamo.getTexto())+"'");
            }
            if(reclamo.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(reclamo.getCreationUser())+"'");
            }
            if(reclamo.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(reclamo.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.reclamo(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarReclamo() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }


    /**
     * Inserta un registro de valores en la tabla
     * @param valor Bean con la informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarValor(Valor valor) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(valor.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(valor.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(valor.getNitEmpresa())+"'");
            campos.add("tipo_padre");
            valores.add("'"+Util.escapeSQL(valor.getTipoPadre())+"'");

            campos.add("id_padre");
            if(valor.getTipoPadre().equals("CAH")){
                valores.add("currval('wsdc.cuenta_ahorro_id_seq')");
            }else if(valor.getTipoPadre().equals("CCO")){
                valores.add("currval('wsdc.cuenta_corriente_id_seq')");
            }else if(valor.getTipoPadre().equals("CCA")){
                valores.add("currval('wsdc.cuenta_cartera_id_seq')");
            }else if(valor.getTipoPadre().equals("TCR")){
                valores.add("currval('wsdc.tarjeta_credito_id_seq')");
            }
            if(valor.getValorInicial()!=null){
                campos.add("valor_inicial");
                valores.add("'"+valor.getValorInicial()+"'");
            }
            if(valor.getCupo()!=null){
                campos.add("cupo");
                valores.add("'"+valor.getCupo()+"'");
            }
            if(valor.getSaldoActual()!=null){
                campos.add("saldo_actual");
                valores.add("'"+valor.getSaldoActual()+"'");
            }
            if(valor.getSaldoMora()!=null){
                campos.add("saldo_mora");
                valores.add("'"+valor.getSaldoMora()+"'");
            }
            if(valor.getCuota()!=null){
                campos.add("cuota");
                valores.add("'"+valor.getCuota()+"'");
            }
            if(valor.getCuotasCanceladas()!=null){
                campos.add("cuotas_canceladas");
                valores.add("'"+valor.getCuotasCanceladas()+"'");
            }
            if(valor.getTotalCuotas()!=null){
                campos.add("total_cuotas");
                valores.add("'"+valor.getTotalCuotas()+"'");
            }
            if(valor.getMaximaMora()!=null){
                campos.add("maxima_mora");
                valores.add("'"+valor.getMaximaMora()+"'");
            }
            if(valor.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(valor.getCreationUser())+"'");
            }
            if(valor.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(valor.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.valor(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarAlerta() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta un registro de alivio en la tabla
     * @param alivio Bean con la informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarAlivio(Alivio alivio) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(alivio.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(alivio.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(alivio.getNitEmpresa())+"'");
            campos.add("tipo_padre");
            valores.add("'"+Util.escapeSQL(alivio.getTipoPadre())+"'");

            campos.add("id_padre");
            if(alivio.getTipoPadre().equals("CAH")){
                valores.add("currval('wsdc.cuenta_ahorro_id_seq')");
            }else if(alivio.getTipoPadre().equals("CCO")){
                valores.add("currval('wsdc.cuenta_corriente_id_seq')");
            }else if(alivio.getTipoPadre().equals("CCA")){
                valores.add("currval('wsdc.cuenta_cartera_id_seq')");
            }else if(alivio.getTipoPadre().equals("TCR")){
                valores.add("currval('wsdc.tarjeta_credito_id_seq')");
            }
            if(alivio.getEstado()!=null){
                campos.add("estado");
                valores.add("'"+Util.escapeSQL(alivio.getEstado())+"'");
            }
            if(alivio.getMes()!=null){
                campos.add("mes");
                valores.add("'"+Util.escapeSQL(alivio.getMes())+"'");
            }
            if(alivio.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(alivio.getCreationUser())+"'");
            }
            if(alivio.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(alivio.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.alivio(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarAlivio() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta un comentario en la tabla
     * @param comentario informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarComentario(ComentarioInforme comentario) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(comentario.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(comentario.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(comentario.getNitEmpresa())+"'");
            if(comentario.getTipo()!=null){
                campos.add("tipo");
                valores.add("'"+Util.escapeSQL(comentario.getTipo())+"'");
            }
            if(comentario.getFechaVencimiento()!=null){
                campos.add("fecha_vencimiento");
                valores.add("'"+comentario.getFechaVencimiento()+"'");
            }
            if(comentario.getTexto()!=null){
                campos.add("texto");
                valores.add("'"+Util.escapeSQL(comentario.getTexto())+"'");
            }
            if(comentario.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(comentario.getCreationUser())+"'");
            }
            if(comentario.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(comentario.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.comentario(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarComentario() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta un score en la tabla
     * @param score informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarScore(Score score) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(score.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(score.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(score.getNitEmpresa())+"'");
            campos.add("tipo");
            valores.add(score.getTipo());
            campos.add("puntaje");
            valores.add(score.getPuntaje());
            if(score.getClasificacion()!=null){
                campos.add("clasificacion");
                valores.add("'"+Util.escapeSQL(score.getClasificacion())+"'");
            }
            if(score.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(score.getCreationUser())+"'");
            }
            if(score.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(score.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.score(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarScore() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }

    /**
     * Inserta un score Decisor en la tabla
     * @param score informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarScoreDecisor(ScoreDecisor score) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(score.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(score.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(score.getNitEmpresa())+"'");
            campos.add("tipo");
            valores.add(score.getTipo());
            campos.add("puntaje");
            valores.add(score.getPuntaje());
            if(score.getClasificacion()!=null){
                campos.add("clasificacion");
                valores.add("'"+Util.escapeSQL(score.getClasificacion())+"'");
            }
            if(score.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(score.getCreationUser())+"'");
            }
            if(score.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(score.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.score(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";
            System.out.println("Score :"+sql);

        }catch(Exception ex){
            throw new Exception("ERROR en insertarScoreDecisor() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }


    /**
     * Inserta una razon de un score en la tabla
     * @param razon informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarRazon(Razon razon) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("score_id");
            valores.add("currval('wsdc.score_id_seq')");
            if(razon.getCodigo()!=null){
                campos.add("codigo");
                valores.add("'"+Util.escapeSQL(razon.getCodigo())+"'");
            }
            if(razon.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(razon.getCreationUser())+"'");
            }
            if(razon.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(razon.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.razon(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarRazon() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }
    

    /**
     * Inserta una linea de respuesta personalizada
     * @param respuesta informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarRespuestaPersonalizada(RespuestaPersonalizada respuesta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(respuesta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(respuesta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(respuesta.getNitEmpresa())+"'");
            if(respuesta.getLinea()!=null){
                campos.add("linea");
                valores.add("'"+Util.escapeSQL(respuesta.getLinea())+"'");
            }
            if(respuesta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(respuesta.getCreationUser())+"'");
            }
            if(respuesta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(respuesta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.respuesta_personalizada(" +Util.join(campos, ",")+ ") values(" +Util.join(valores, ",")+ ")";

        }catch(Exception ex){
            throw new Exception("ERROR en insertarRespuestaPersonalizada() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }
    

    /**
     * Inserta una linea de respuesta personalizada
     * @param respuesta informacion del registro a insertar
     * @return sql generado
     * @throws Exception
     */
    public String insertarRespuestaPersonalizadaDecisor(RespuestaPersonalizadaDecisor respuesta) throws Exception{
        String sql;
        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();

        try{
            campos.add("tipo_identificacion");
            valores.add(respuesta.getTipoIdentificacion());
            campos.add("identificacion");
            valores.add("'"+Util.escapeSQL(respuesta.getIdentificacion())+"'");
            campos.add("nit_empresa");
            valores.add("'"+Util.escapeSQL(respuesta.getNitEmpresa())+"'");
            //valores respuesta personalizada
          /*  if (respuesta.getConsecutivo() != null) {
                campos.add("linea");
                Iterator it = respuesta.getConsecutivo().entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry e;
                    e = (Map.Entry) it.next();
                    //System.out.println("el malparido mapa: " + e.getKey() + " : " + e.getValue());
                    String llave=(String) e.getKey();
                    if(llave.equals("Var_Descripcion_Causal")){
                       valores.add("'"+Util.escapeSQL((String)e.getValue())+"'");
                    }
                    
                }
            }*/
            
            if(respuesta.getLinea()!=null){
                campos.add("linea");
                valores.add("'"+Util.escapeSQL(respuesta.getLinea())+"'");
            }
            if(respuesta.getCreationUser()!=null){
                campos.add("creation_user");
                valores.add("'"+Util.escapeSQL(respuesta.getCreationUser())+"'");
            }
            if(respuesta.getUserUpdate()!=null){
                campos.add("user_update");
                valores.add("'"+Util.escapeSQL(respuesta.getUserUpdate())+"'");
            }

            sql = "insert into wsdc.respuesta_personalizada(" + Util.join(campos, ",") + ") values(" + Util.join(valores, ",") + ")";
           // sql += this.guardarRepuestaPerDecisor(respuesta.getConsecutivo());
            
            System.out.println("Respuesta personalizada decisor: " + sql);

        }catch(Exception ex){
            throw new Exception("ERROR en insertarRespuestaPersonalizadaDecisor() -->> [WSHistCreditoDAO] "+ex.getMessage());
        }
        return sql;
    }
  
    public String guardarRepuestaPerDecisor(HashMap map, RespuestaPersonalizadaDecisor resp) {

        ArrayList campos = new ArrayList();
        ArrayList valores = new ArrayList();
        String sql = "";
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e;
            e = (Map.Entry) it.next();

            System.out.println("el malparido mapa: " + e.getKey().toString().toLowerCase() + " : " + e.getValue());
            campos.add("" + e.getKey().toString().toLowerCase());
            valores.add("'" + e.getValue()+"'");


        }
        
           campos.add("tipo_identificacion");
           valores.add(resp.getTipoIdentificacion());
           campos.add("identificacion");
           valores.add("'" + Util.escapeSQL(resp.getIdentificacion()) + "'");
           campos.add("nit_empresa");
           valores.add("'" + Util.escapeSQL(resp.getNitEmpresa()) + "'");
           campos.add("user_update");
           valores.add("'"+Util.escapeSQL(resp.getUserUpdate())+"'");
           campos.add("creation_user");
           valores.add("'"+Util.escapeSQL(resp.getCreationUser())+"'");

        sql = "INSERT INTO wsdc.respuesta_personalizada_decisor(" + Util.join(campos, ",") + ") VALUES(" + Util.join(valores, ",") + ")";
        System.out.println("respuesta_personalizada_decisor :"+sql);

        return sql;
    }


    /**
     * Obtiene el score de una persona
     * @throws Exception
     */
    public Score buscarScore(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "BUSCAR_SCORE_PERSONA";
        Score score=new Score();
        score.setPuntaje(-1);
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                st.setString(3, nitEmpresa);
                rs = st.executeQuery();
                if(rs.next()){
                    score.setPuntaje(rs.getInt("puntaje"));
                    score.setClasificacion(rs.getString("clasificacion"));
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en buscarScore() -->> [WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return score;
    }

    /**
     * Obtiene el numero de dias desde la ultima consulta de historia de credito de una persona
     * @throws Exception
     */
    public int buscarDiasHC(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "DIAS_HC";
        int dias = -1;
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                st.setString(3, nitEmpresa);
                rs = st.executeQuery();
                if(rs.next()){
                    dias=rs.getInt("num_dias");
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en buscarDiasHC() -->> [WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return dias;
    }

    /**
     * Inserta un registro en la tabla de historico de peticiones
     * @throws Exception
     */
    public void insertarHistoricoPeticion(String webService, Timestamp fecha, String usuario, int tipoId, String id, String papellido, String paramForm, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query = "INSERTAR_PETICION";
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, webService);
                st.setTimestamp(2, fecha);
                st.setString(3, usuario);
                st.setString(4, id);
                st.setInt(5, tipoId);
                st.setString(6, papellido);
                st.setString(7, paramForm);
                st.setString(8, usuario);
                st.setString(9, usuario);
                st.setString(10, nitEmpresa);
                st.executeUpdate();
            }
        }catch(Exception e){
            throw new Exception("ERROR en insertarHistoricoPeticion() -->> [WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
    }
    
    /**
     * Actualiza el codigo de respuesta de una peticion realizada
     * @param webService web service por el cual se realiza la peticion
     * @param fecha fecha de la peticion
     * @param usuario usuario que realizo la peticion
     * @param respuesta respuesta que se recibió del web service
     * @throws Exception 
     */
    public void actualizarRespuesta(String webService, Timestamp fecha, String usuario, String respuesta){
        PreparedStatement st = null;
        Connection con = null;
        String query = "ACTUALIZAR_RESPUESTA_PETICION";
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, respuesta);
                st.setString(2, webService);
                st.setTimestamp(3, fecha);
                st.setString(4, usuario);
                st.executeUpdate();
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if (st  != null){ try{ st = null;             } catch(Exception e){ }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ }}
        }
    }
    
     /**
     * Busca listado de registro de un tabla especifica
     * @param webService
     * @param tabla
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList buscarTabla(String webService,String tabla) throws Exception{
        ArrayList <Codigo> registros= new ArrayList <Codigo>();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_CODIGOS_TABLA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setString(1, webService);
            ps.setString(2, tabla);
            rs=ps.executeQuery();
            while(rs.next()){
                Codigo registro=new Codigo();
                registro.setCodigo(rs.getString("codigo"));
                registro.setValor(rs.getString("valor"));
                registro.setEquivalencia(rs.getString("equivalencia"));
                registros.add(registro);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarTabla (WSHistCreditoDAO): "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return registros;
    }

     /**
     * Busca listado de registro de un tabla y referencia especifica
     * @param webService
     * @param tabla
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList buscarTablaRef(String webService,String tabla, String ref) throws Exception{
        ArrayList <Codigo> registros= new ArrayList <Codigo>();
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_CODIGOS_TABLA_REF";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setString(1, webService);
            ps.setString(2, tabla);
            ps.setString(3, ref);
            rs=ps.executeQuery();
            while(rs.next()){
                Codigo registro=new Codigo();
                registro.setCodigo(rs.getString("codigo"));
                registro.setValor(rs.getString("valor"));
                registro.setEquivalencia(rs.getString("equivalencia"));
                registros.add(registro);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarTablaRef (WSHistCreditoDAO): "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return registros;
    }

    /**
     * Busca el codigo por medio de la tabla y la equivalencia
     * @param webService
     * @param tabla
     * @param equivalencia
     * @return codigo
     * @throws Exception cuando hay error
     */
    public String buscarCodigo(String webService,String tabla, String equivalencia) throws Exception{
        String codigo=null;
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_CODIGO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setString(1, webService);
            ps.setString(2, tabla);
            ps.setString(3, equivalencia);
            rs=ps.executeQuery();
            if(rs.next()){
                codigo=rs.getString("codigo");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarCodigo (WSHistCreditoDAO): "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return codigo;
    }

     /* Busca el valor por medio de la tabla y el codigo
     * @param webService
     * @param tabla
     * @param codigo
     * @return codigo
     * @throws Exception cuando hay error
     */
    public String buscarValor(String webService,String tabla, String codigo) throws Exception{
        String valor="";
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_VALOR";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setString(1, webService);
            ps.setString(2, tabla);
            ps.setString(3, codigo);
            rs=ps.executeQuery();
            if(rs.next()){
                valor=rs.getString("valor");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarValor (WSHistCreditoDAO): "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return valor;
    }

     /* Busca trae el valor total de las cuotas vigentes como titular de una persona
     * @param identificacion
     * @throws Exception cuando hay error
     */
    public double buscarSumCuotas(String identificacion, String nitEmpresa) throws Exception{
        double valor=0;
        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="SRC_SUM_CUOTAS_VIGENTES";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setString(1, identificacion);
            ps.setString(2, nitEmpresa);
            ps.setString(3, identificacion);
            ps.setString(4, nitEmpresa);
            rs=ps.executeQuery();
            if(rs.next()){
                valor=rs.getDouble("total_cuota");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarSumCuotas (WSHistCreditoDAO): "+e.toString());
        }
        finally {
            if(ps!=null){ try{ps.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return valor;
    }
    
    /**
     * Consulta la informacion de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return bean Persona con la informacion consultada
     * @throws Exception 
     */
    public Persona consultarPersona(String identificacion, String tipoIdentificacion, boolean fenalco) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_PERSONA";
        Persona persona = null;
        String sql=this.obtenerSQL(query);
        
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                sql=sql.replaceAll("#WHERE#", fenalco?"AND nit_empresa in ('8901009858','8020220161')":"AND nit_empresa not in ('8901009858','8904800244')");
                st = con.prepareStatement(sql);
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                rs = st.executeQuery();
                if(rs.next()){
                    persona = new Persona();
                    persona.Load(rs);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarPersona[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return persona;
    }
    
    /**
     * Consulta la informacion de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return bean Persona con la informacion consultada
     * @throws Exception 
     */
    public Persona consultarPersonaDecisor(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_PERSONA";
        Persona persona = null;
        String sql=this.obtenerSQL(query);
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                sql = sql.replaceAll("#WHERE#", "AND nit_empresa in ('" + nitEmpresa + "')");
                st = con.prepareStatement(sql);
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                rs = st.executeQuery();
                if (rs.next()) {
                    persona = new Persona();
                    persona.Load(rs);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarPersonaDecisor[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return persona;
    }
    
    
    /**
     * Obtiene los comentarios de la historia de credito de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<ComentarioInforme>
     * @throws Exception 
     */
    public ArrayList<ComentarioInforme> consultarComentarios(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_COMENTARIOS";
        ArrayList<ComentarioInforme> comentarios = new ArrayList<ComentarioInforme>();
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                st.setString(3, nitEmpresa);
                rs = st.executeQuery();
                while(rs.next()){
                    ComentarioInforme comentario = new ComentarioInforme();
                    comentario.setTipo(rs.getString("tipo"));
                    comentario.setFechaVencimiento(rs.getTimestamp("fecha_vencimiento"));
                    comentario.setTexto(rs.getString("texto"));
                    comentarios.add(comentario);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarComentarios[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return comentarios;
    }
    
    /**
     * Obtiene las alertas de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<Alerta>
     * @throws Exception 
     */
    public ArrayList<Alerta> consultarAlertas(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_ALERTAS";
        ArrayList<Alerta> comentarios = new ArrayList<Alerta>();
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                st.setString(3, nitEmpresa);
                rs = st.executeQuery();
                while(rs.next()){
                    Alerta alerta = new Alerta();
                    alerta.setColocacion(rs.getTimestamp("colocacion"));
                    alerta.setTexto(rs.getString("texto"));
                    alerta.setFuente(rs.getString("nombre_fuente"));
                    comentarios.add(alerta);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarAlertas[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return comentarios;
    }
    
    /**
     * Obtiene las consultas de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<Consulta>
     * @throws Exception 
     */
    public ArrayList<Consulta> consultarConsultas(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_CONSULTAS";
        ArrayList<Consulta> consultas = new ArrayList<Consulta>();
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                st.setString(3, nitEmpresa);
                rs = st.executeQuery();
                while(rs.next()){
                    Consulta consulta = new Consulta();
                    consulta.setFecha(rs.getTimestamp("fecha"));
                    consulta.setTipoCuenta(rs.getString("tipo_cuenta"));
                    consulta.setEntidad(rs.getString("entidad"));
                    consulta.setOficina(rs.getString("oficina"));
                    consulta.setCiudad(rs.getString("ciudad"));
                    consulta.setNumConsultas(rs.getInt("num_consultas"));
                    consultas.add(consulta);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarConsultas[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return consultas;
    }
    
    /**
     * Obtiene los reclamos de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<Reclamo>
     * @throws Exception 
     */
    public ArrayList<Reclamo> consultarReclamos(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_RECLAMOS";
        ArrayList<Reclamo> reclamos = new ArrayList<Reclamo>();
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                st.setString(3, nitEmpresa);
                rs = st.executeQuery();
                while(rs.next()){
                    Reclamo reclamo = new Reclamo();
                    reclamo.setTipo(rs.getString("tipo"));
                    reclamo.setTexto(rs.getString("texto"));
                    reclamo.setTipoLeyenda(rs.getString("tipo_leyenda"));
                    reclamo.setFecha(rs.getTimestamp("fecha"));
                    reclamo.setEntidad(rs.getString("entidad"));
                    reclamo.setNumeroCuenta(rs.getString("numero_cuenta"));
                    reclamos.add(reclamo);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarReclamos[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return reclamos;
    }
    
    /**
     * Obtiene los scores y razones de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<Score>
     * @throws Exception 
     */
    public ArrayList<Score> consultarScores(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_SCORE";
        ArrayList<Score> scores = new ArrayList<Score>();
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                st.setString(3, nitEmpresa);
                rs = st.executeQuery();
                while(rs.next()){
                    Score score = new Score();
                    score.setId(rs.getInt("id"));
                    score.setPuntaje(rs.getInt("puntaje"));
                    score.setClasificacion(rs.getString("clasificacion"));
                    score.setRazon(rs.getString("razones"));
                    scores.add(score);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarScores[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return scores;
    }
    
    /**
     * Obtiene las respuestas personalizadas de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<RespuestaPersonalizada>
     * @throws Exception 
     */
    public ArrayList<RespuestaPersonalizada> consultarRespuestaPersonalizada(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_RESPUESTA_PERSONALIZADA";
        ArrayList<RespuestaPersonalizada> respuestas = new ArrayList<RespuestaPersonalizada>();
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                st.setString(3, nitEmpresa);
                rs = st.executeQuery();
                while(rs.next()){
                    RespuestaPersonalizada respuestaPersonalizada = new RespuestaPersonalizada();
                    respuestaPersonalizada.setLinea(rs.getString("linea"));
                    respuestas.add(respuestaPersonalizada);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarRespuestaPersonalizada[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return respuestas;
    }
    
    /**
     * Obtiene los totales, saldos, cupos y moras de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<Valor>
     * @throws Exception 
     */
    public ArrayList<Valor> consultarTotales(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_TOTALES";
        ArrayList<Valor> totales = new ArrayList<Valor>();
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setInt(1, Integer.parseInt(tipoIdentificacion));
                st.setString(2, identificacion);
                st.setString(3, nitEmpresa);
                st.setInt(4, Integer.parseInt(tipoIdentificacion));
                st.setString(5, identificacion);
                st.setString(6, nitEmpresa);
                rs = st.executeQuery();
                while(rs.next()){
                    Valor total = new Valor();
                    total.setTipoPadre(rs.getString("tipo_cuenta"));
                    total.setCalidad(rs.getString("garante"));
                    total.setValorInicial(rs.getDouble("cupo_valor_inicial"));
                    total.setSaldoActual(rs.getDouble("saldo_actual"));
                    total.setSaldoMora(rs.getDouble("saldo_mora"));
                    total.setCuota(rs.getDouble("cuota"));
                    totales.add(total);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarTotales[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return totales;
    }
    
    /**
     * Consulta las obligaciones abiertas de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<CuentaCartera> con la informacion de las cuentas de ahorro, corriente, cartera y tarjetas de credito de la persona
     * @throws Exception 
     */
    public ArrayList<CuentaCartera> consultarObligacionesAbiertas(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_OBLIGACIONES_ABIERTAS";
        ArrayList<CuentaCartera> obligaciones = new ArrayList<CuentaCartera>();
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setInt(1, Integer.parseInt(tipoIdentificacion));
                st.setString(2, identificacion);
                st.setString(3, nitEmpresa);
                st.setInt(4, Integer.parseInt(tipoIdentificacion));
                st.setString(5, identificacion);
                st.setString(6, nitEmpresa);
                st.setInt(7, Integer.parseInt(tipoIdentificacion));
                st.setString(8, identificacion);
                st.setString(9, nitEmpresa);
                st.setInt(10, Integer.parseInt(tipoIdentificacion));
                st.setString(11, identificacion);
                st.setString(12, nitEmpresa);
                rs = st.executeQuery();
                while(rs.next()){
                    CuentaCartera obligacion = new CuentaCartera();
                    obligacion.setEntidad(rs.getString("entidad"));
                    obligacion.setSector(rs.getString("sector"));
                    obligacion.setTipoCuenta(rs.getString("tipo_cuenta"));
                    obligacion.setNumeroObligacion(rs.getString("numero"));
                    obligacion.setEstado(rs.getString("estado"));
                    obligacion.setUltimaActualizacion(rs.getTimestamp("ultima_actualizacion"));
                    obligacion.setFechaApertura(rs.getTimestamp("fecha_apertura"));
                    obligacion.setFechaVencimiento(rs.getTimestamp("fecha_vencimiento"));
                    obligacion.setComportamiento(rs.getString("comportamiento"));
                    obligacion.setReclamo(rs.getString("reclamo"));
                    obligacion.setSituacionTitular(rs.getString("situacion_titular"));
                    obligacion.setMesesPermanencia(rs.getShort("permanencia"));
                    obligacion.setPeriodicidad(rs.getString("periodicidad"));
                    obligacion.setTipoContrato(rs.getString("tipo_contrato"));
                    obligacion.setEjecucionContrato(rs.getShort("ejecucion_contrato"));
                    obligacion.setOficina(rs.getString("oficina"));
                    obligacion.setGarante(rs.getString("garante"));
                    
                    Valor valor = new Valor();
                    valor.setMaximaMora(rs.getInt("maxima_mora"));                    
                    valor.setCupo(rs.getDouble("cupo"));
                    valor.setSaldoActual(rs.getDouble("saldo_actual"));
                    valor.setSaldoMora(rs.getDouble("saldo_mora"));
                    valor.setCuota(rs.getDouble("cuota"));
                    valor.setCuotasCanceladas(rs.getInt("cuotas_canceladas"));
                    valor.setTotalCuotas(rs.getInt("total_cuotas"));
                    obligacion.setValor(valor);
                    
                    obligaciones.add(obligacion);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarObligacionesAbiertas[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return obligaciones;
    }
    
    /**
     * Consulta las obligaciones cerradas de una persona
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<CuentaCartera> con la informacion de las cuentas de ahorro, corriente, cartera y tarjetas de credito cerradas de la persona
     * @throws Exception 
     */
    public ArrayList<CuentaCartera> consultarObligacionesCerradas(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_OBLIGACIONES_CERRADAS";
        ArrayList<CuentaCartera> obligaciones = new ArrayList<CuentaCartera>();
        try{
            con = this.conectarJNDI(query);
            
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setInt(1, Integer.parseInt(tipoIdentificacion));
                st.setString(2, identificacion);
                st.setString(3, nitEmpresa);
                st.setInt(4, Integer.parseInt(tipoIdentificacion));
                st.setString(5, identificacion);
                st.setString(6, nitEmpresa);
                st.setInt(7, Integer.parseInt(tipoIdentificacion));
                st.setString(8, identificacion);
                st.setString(9, nitEmpresa);
                st.setInt(10, Integer.parseInt(tipoIdentificacion));
                st.setString(11, identificacion);
                st.setString(12, nitEmpresa);
                rs = st.executeQuery();
                
                while(rs.next()){
                    CuentaCartera obligacion = new CuentaCartera();
                    obligacion.setEntidad(rs.getString("entidad"));
                    obligacion.setSector(rs.getString("sector"));
                    obligacion.setTipoCuenta(rs.getString("tipo_cuenta"));
                    obligacion.setNumeroObligacion(rs.getString("numero"));
                    obligacion.setEstado(rs.getString("estado"));
                    obligacion.setUltimaActualizacion(rs.getTimestamp("ultima_actualizacion"));
                    obligacion.setFechaApertura(rs.getTimestamp("fecha_apertura"));
                    obligacion.setFechaVencimiento(rs.getTimestamp("fecha_vencimiento"));
                    obligacion.setComportamiento(rs.getString("comportamiento"));
                    obligacion.setOficina(rs.getString("oficina"));
                    obligacion.setGarante(rs.getString("garante"));
                    obligacion.setValor_Inicial(rs.getDouble("cupo_inicial"));
                    
                    obligaciones.add(obligacion);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarObligacionesCerradas[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return obligaciones;
    }
    
    /**
     * Obtiene el endeudamiento global clasificado de una identificación
     * @param identificacion
     * @param tipoIdentificacion
     * @return ArrayList<EndeudamientoGlobal> con los resultados obtenidos
     * @throws Exception 
     */
    public ArrayList<EndeudamientoGlobal> consultarEndeudamientoGlobal(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTAR_ENDEUDAMIENTO_GLOBAL";
        ArrayList<EndeudamientoGlobal> endeudamientos = new ArrayList<EndeudamientoGlobal>();
        try{
            con = this.conectarJNDI(query);
            
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setInt(1, Integer.parseInt(tipoIdentificacion));
                st.setString(2, identificacion);
                st.setString(3, nitEmpresa);
                rs = st.executeQuery();                
                while(rs.next()){
                    EndeudamientoGlobal obligacion = new EndeudamientoGlobal();
                    obligacion.setEntidad(rs.getString("entidad"));
                    obligacion.setFechaReporte(rs.getTimestamp("fecha_reporte"));
                    obligacion.setCalificacion(rs.getString("calificacion"));
                    obligacion.setGarantia(rs.getString("garantia"));
                    obligacion.setMoneda(rs.getString("moneda"));
                    obligacion.setSaldoPendiente(rs.getDouble("total_saldo"));
                    obligacion.setNumeroCreditos(rs.getInt("total_num"));
                    obligacion.setTotalConsumo(rs.getDouble("saldo_consumo"));
                    obligacion.setNumConsumo(rs.getInt("num_consumo"));
                    obligacion.setTotalComercial(rs.getDouble("saldo_comercial"));
                    obligacion.setNumComercial(rs.getInt("num_comercial"));
                    obligacion.setTotalHipotecario(rs.getDouble("saldo_hipotecario"));
                    obligacion.setNumHipotecario(rs.getInt("num_hipotecario"));
                    obligacion.setTotalMicrocredito(rs.getDouble("saldo_micro"));
                    obligacion.setNumMicrocredito(rs.getInt("num_micro"));
                    
                    endeudamientos.add(obligacion);
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en consultarEndeudamientoGlobalClasificado[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return endeudamientos;
    }
    
    
    public void formulario(Usuario  usuario, String nitEmpresa) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "CONSULTA";
        
        //ArrayList<FormularioSuperfil> formulario = new ArrayList<FormularioSuperfil>();
        NegociosDAO negdao = new NegociosDAO(this.getDatabaseName());
        GestionSolicitudAvalService gsas = new GestionSolicitudAvalService(this.getDatabaseName());
        WSHistCreditoService wsdc = new WSHistCreditoService(this.getDatabaseName());
        WSHistCreditoDAO dao = new WSHistCreditoDAO(this.getDatabaseName());
        NegociosGenService negociosGenService = new NegociosGenService(this.getDatabaseName());
        
        RespuestaSuperfil respuesta = new RespuestaSuperfil();
        Timestamp fecha = new Timestamp(new java.util.Date().getTime());
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                rs = st.executeQuery();
                while (rs.next()) {

                    SolicitudPersona bean_pers = gsas.buscarPersona(Integer.parseInt(rs.getString("numero_solicitud")), "S");
                    SolicitudLaboral bean_lab = gsas.datosLaboral(Integer.parseInt(rs.getString("numero_solicitud")), "S");
                    SolicitudAval bean_sol = gsas.buscarSolicitud(Integer.parseInt(rs.getString("numero_solicitud")));
                    Negocios negocio = negociosGenService.buscarNegocio(bean_sol.getCodNegocio());
                    
                    FormularioSuperfil form = new FormularioSuperfil();

                    int num_doc = negocio.getNodocs();
                    double tot_pag = negocio.getTotpagado();
                    form.setTipoIdentificacion("1");
                    form.setIdentificacion(bean_pers.getIdentificacion());
                    form.setPrimerApellido(bean_pers.getPrimerApellido());
                    form.setFechaNacimiento(bean_pers.getFechaNacimiento().substring(0, 10).replaceAll("-", ""));
                    form.setTipoVivienda(bean_pers.getTipoVivienda());
                    form.setHaTenidoCreditoFintra(negdao.tieneCreditoFintra(bean_pers.getIdentificacion()) ? "01" : "02");

                    form.setTipoProducto(bean_sol.getProducto());
                    form.setMontoSolicitado((int) Double.parseDouble(bean_sol.getValorSolicitado()) + "");
                    form.setArriendo((int) Double.parseDouble(bean_lab.getGastosArriendo()) + "");
                    form.setEstadoCivil(wsdc.buscarCodigo("H", "estado_civil", bean_pers.getEstadoCivil()));
                    form.setConyugueTrabaja(Double.parseDouble(bean_pers.getSalarioCony()) > 0 ? "01" : "02");
                    if (bean_sol.getProducto().equals("02")) {
                        form.setServicio(bean_sol.getServicio());
                        form.setCiudadMatricula(wsdc.buscarCodigo("H", "matricula", bean_sol.getCiudadMatricula()));
                        form.setVlrVehiculo((int) Double.parseDouble(bean_sol.getValorProducto()) + "");
                    }
                    if (bean_sol.getProducto().equals("01")) {
                        form.setVlrSemestre((int) Double.parseDouble(bean_sol.getValorProducto()) + "");
                        form.setPlazoUniversitario(wsdc.buscarCodigo("H", "plazo_uni", num_doc + ""));
                    } else {
                        ArrayList<Codigo> plazo = wsdc.buscarTabla("H", "plazo");
                        for (int i = 0; i < plazo.size(); i++) {
                            if (num_doc <= Integer.parseInt(plazo.get(i).getValor())) {
                                form.setPlazo(plazo.get(i).getCodigo());
                                break;
                            }
                        }
                    }

                    int ingresos = (int) (Double.parseDouble(bean_lab.getSalario()) + Double.parseDouble(bean_lab.getOtrosIngresos()));
                    form.setIngreso(ingresos + "");

                    int cuota = (int) (tot_pag / num_doc);
                    form.setVlrCuotaMensual(cuota + "");
                    form.setNitEmpresa(nitEmpresa);

                    TablaGenManagerDAO tgendao = new TablaGenManagerDAO(this.getDatabaseName());
                    TablaGen t = tgendao.obtenerInformacionDato("PARAM_WSDC", "DIAS_CONSULTA_HC");

                   
                        //Obtener el id y clave para consultar en datacredito
                        t = this.obtenerConexion(nitEmpresa);
                        if (t != null) {
                            ServicioHistoriaCredito hc = new ServicioHistoriaCredito_Impl();
                            HCServiceImpl hcPort = hc.getServicioHistoriaCredito();

                            String xmlFormulario = null;
                            xmlFormulario = wsdc.generarXmlFormulario(form);
                            dao.insertarHistoricoPeticion("H", fecha, usuario.getLogin(), Integer.parseInt(form.getTipoIdentificacion()), form.getIdentificacion(), form.getPrimerApellido(), xmlFormulario, form.getNitEmpresa());
                            generarTXT("tipo:" + form.getTipoIdentificacion() + " id:" + form.getIdentificacion() + " apellido:" + form.getPrimerApellido() + " paramForm:" + xmlFormulario, null);
                            String xml = hcPort.consultarHC(t.getReferencia(), t.getDato(), form.getTipoIdentificacion(), form.getIdentificacion(), form.getPrimerApellido(), xmlFormulario);
                            generarTXT(null, xml);
                            respuesta = wsdc.parseXML(xml);
                        } else {
                            respuesta.setCodigoRespuesta("F05");
                        }
                }
               this.actualizarRespuesta("H", fecha, usuario.getLogin(), respuesta.getCodigoRespuesta()); 
            }
        } catch (Exception e) {
            throw new Exception("ERROR en consultarReclamos[WSHistCreditoDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                }
            }
        }
       
    }
    
    
    
    public void generarTXT(String solicitud, String xml) throws IOException{
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta") + "/WEB-INF/classes/logsDatacredito/";
            File archivo = new File(ruta);            
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

            FileWriter fw = new FileWriter(ruta + "hcredito_"+Util.getFechaActual_String(8)+".txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter salida = new PrintWriter(bw);
            if(solicitud!=null){
                salida.println("SOLICITUD "+Util.getFechaActual_String(9)+":");
                salida.println(solicitud);
            }
            if(xml!=null){
                salida.println("RESPUESTA:");
                salida.print(xml);
                salida.println();
                salida.println();
            }
            salida.println();
            salida.close();
        } catch(java.io.IOException ioex) {
          throw ioex;
        }
    }


    public TablaGen obtenerConexion(String nit_empresa)throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        TablaGen tblgen = null;
        String query = "SQL_OBNETER_CONEXION";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit_empresa);
            rs = ps.executeQuery();
            while(rs.next()){
                tblgen = new TablaGen();
                tblgen.setDescripcion (rs.getString ("descripcion"));
                tblgen.setReferencia (rs.getString ("referencia"));
                tblgen.setTable_code (rs.getString ("table_code"));
                tblgen.setTable_type (rs.getString ("table_type"));
                tblgen.setDato (rs.getString ("dato"));
            }
        }
        }catch( Exception ex ){
            throw new SQLException("ERROR SQL_OBNETER_CONEXION: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return tblgen;
    }

     public boolean validarCausalMLE(String identificacion, String tipoIdentificacion, String nitEmpresa) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "VALIDAR_RESPUESTA_PERSONALIZADA_LME";
        boolean sw=false;
        try{
            con = this.conectarJNDI(query);
            if ( con != null ){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, identificacion);
                st.setInt(2, Integer.parseInt(tipoIdentificacion));
                st.setString(3, nitEmpresa);
                rs = st.executeQuery();
                if(rs.next()){
                   sw=true;
                }
            }
        }catch(Exception e){
            throw new Exception("ERROR en validarCausalMLE[WSHistCreditoDAO] " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st = null;             } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage()); }}
        }
        return sw;
    }

      public String  obtenerNumeroFormulario(String nit_empresa)throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String num_form="";
        String query = "SQL_OBNETER_NO_FORMULARIO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit_empresa);
            rs = ps.executeQuery();
            while(rs.next()){
                num_form= (rs.getString ("table_code"));
            }
        }
        }catch( Exception ex ){
            throw new SQLException("ERROR SQL_OBNETER_CONEXION: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return num_form;
    }

   public TablaGen obtenerConexionDecisor(String nit_empresa)throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        TablaGen tblgen = null;
        String query = "SQL_OBNETER_CONEXION_DECISOR";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit_empresa);
            rs = ps.executeQuery();
            while(rs.next()){
                tblgen = new TablaGen();
                tblgen.setDescripcion (rs.getString ("descripcion"));
                tblgen.setReferencia (rs.getString ("referencia"));
                tblgen.setTable_code (rs.getString ("table_code"));
                tblgen.setTable_type (rs.getString ("table_type"));
                tblgen.setDato (rs.getString ("dato"));
}
        }
        }catch( Exception ex ){
            throw new SQLException("ERROR SQL_OBNETER_CONEXION: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return tblgen;
    }   
   
   
    public String  getStraid_and_Strnam(String nit_empresa, String descripcion)throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String result="";
        String query = "SQL_OBNETER_STRAID";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit_empresa);
            ps.setString(2, descripcion);
            rs = ps.executeQuery();
            while(rs.next()){
                result= (rs.getString ("table_code"));
            }
        }
        }catch( Exception ex ){
            throw new SQLException("ERROR SQL_OBNETER_CONEXION: "+ex.getMessage());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
        return result;
    }
      

}
