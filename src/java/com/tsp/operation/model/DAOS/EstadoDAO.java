/*
 * EstadoDAO.java
 *
 * Created on 1 de marzo de 2005, 12:14 PM
 */

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  DIBASMO
 */

public class EstadoDAO extends MainDAO {
    
    /** Creates a new instance of EstadoDAO */
    public EstadoDAO() {
        super("EstadoDAO.xml");//JJCastro fase2
    }
    public EstadoDAO(String dataBaseName) {
        super("EstadoDAO.xml", dataBaseName);//JJCastro fase2
    }

    
    private Estado estado;
    private List Estados;
    private Vector VecEstados;
    private static final  String Select_Consulta = "select e.department_code as departament_code, " +
    "                                               e.department_name as departament_name," +
    "                                               p.country_code, p.country_name, " +
    "                                               e.zona from estado e, pais p " +
    "                                               where e.country_code= ? and " +
    "                                               e.rec_status='' and p.reg_status='' " +
    "                                               and p.country_code=e.country_code " +
    "                                               order by e.department_name";
    
    private static final  String Select_ConsultaGeneral = "select e.department_code as departament_code, " +
    "                                               e.department_name as departament_name," +
    "                                               p.country_code, p.country_name, " +
    "                                               e.zona from estado e, pais p where e.rec_status='' " +
    "                                               and p.reg_status='' and p.country_code=e.country_code " +
    "                                               order by p.country_name, e.department_name";   
    //Sandra 
    private static String SQL_BUSCARXNOMBRE = "  select 	department_code as codigo,  " +
   "                                                    department_name as nombre	" +
   "                                            from 	estado " +
   "                                            where 	country_code = ? "+
   "                                                    and department_name like ? "+
   "                                                    and rec_status <> 'A' 		" +
   "                                            order by department_name";
    
    
    public void setEstado(Estado estado){
        
        this.estado = estado;
        
    }
    
/**
 * 
 * @throws SQLException
 */
    public void insertarEstado() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_INSERT_ESTADO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, estado.getdepartament_code());
                st.setString(2, estado.getdepartament_name());
                st.setString(3, estado.getpais_code());
                st.setString(4, estado.getzona());
                st.setString(5, estado.getUser_update() );
                st.setString(6, estado.getCreation_user() );
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR EL ESTADO" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }




/**
 *
 * @param cod_pais
 * @param cod_depto
 * @return
 * @throws SQLException
 */
    public boolean existeEstado (String cod_pais, String cod_depto ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        String query = "SQL_EXISTE_ESTADO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod_pais);
                st.setString(2, cod_depto);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true; 
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL DEPARTEMENTO" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }


/**
 * 
 * @param cod_pais
 * @param cod_depto
 * @return
 * @throws SQLException
 */
    public boolean existeEstadoAnulado (String cod_pais, String cod_depto ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        String query = "SQL_EXISTE_ESTADO_ANULADO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod_pais);
                st.setString(2, cod_depto);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true; 
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL DEPARTEMENTO" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }


/**
 *
 * @param nom_pais
 * @param nom_depto
 * @return
 * @throws SQLException
 */
    public boolean existeEstadonom (String nom_pais, String nom_depto ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        String query = "SQL_EXISTE_ESTADONOM";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nom_depto);
                st.setString(2, nom_pais);
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true; 
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL DEPARTEMENTO" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }


/**
 *
 * @param pais
 * @throws SQLException
 */
    public void listarEstado (String pais)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        Estados = null;
        String query = "Select_Consulta";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,pais);
                rs = st.executeQuery();
                
                Estados =  new LinkedList();
                estado = null;
                while (rs.next()){
                    estado = new Estado();
                    estado.setdepartament_code( rs.getString( "departament_code") );
                    estado.setdepartament_name( rs.getString("departament_name" ) );
                    estado.setpais_code( rs.getString("country_code") );
                    estado.setpais_name( rs.getString("country_name") );
                    estado.setzona( rs.getString("zona") );
                    Estados.add(estado);
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


 /**
  *
  * @param pais
  * @throws SQLException
  */
    public void Estados (String pais)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        VecEstados = null;
        String query = "Select_Consulta";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,pais);
                rs = st.executeQuery();
                
                VecEstados =  new Vector();
                estado = null;
                while (rs.next()){
                    estado = new Estado();
                    estado.setdepartament_code( rs.getString( "departament_code") );
                    estado.setdepartament_name( rs.getString("departament_name" ) );
                    estado.setpais_code( rs.getString("country_code") );
                    estado.setpais_name( rs.getString("country_name") );
                    estado.setzona( rs.getString("zona") );                    
                    VecEstados.add(estado);
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }



/**
 *
 * @throws SQLException
 */
    public void listarEstadoGeneral ( )throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        VecEstados = null;
        String query = "Select_ConsultaGeneral";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                VecEstados =  new Vector();
                estado = null;
                while (rs.next()){
                    estado = new Estado();
                    estado.setdepartament_code( rs.getString( "departament_code") );
                    estado.setdepartament_name( rs.getString("departament_name" ) );
                    estado.setpais_code( rs.getString("country_code") );
                    estado.setpais_name( rs.getString("country_name") );
                    estado.setzona( rs.getString("zona") );  
                    VecEstados.add(estado);
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }
    
    public List obtenerEstados (){
        return Estados;
    }
    public Vector obtEstados (){
        return VecEstados;
    }
    public Estado obtenerEstado()throws SQLException{
        return estado;
    }


/**
 * 
 * @param cod_pais
 * @param cod_depto
 * @throws SQLException
 */
    public void buscarestado (String cod_pais, String cod_depto) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        String query = "SQL_BUSACAR_ESTADO";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod_depto);
                st.setString(2, cod_pais);
                rs = st.executeQuery();
                estado = null;
                if (rs.next()){
                    estado = new Estado();////System.out.println("---------------"+rs.getString( "departament_code"));
                    estado.setdepartament_code( rs.getString( "departament_code") );
                    estado.setdepartament_name( rs.getString("departament_name" ) );
                    estado.setpais_code( rs.getString("country_code") );
                    estado.setpais_name( rs.getString("country_name") );
                    estado.setzona( rs.getString("zona") );  
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL ESTADO" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


/**
 * 
 * @param nom_pais
 * @param nom_depto
 * @param zona
 * @throws SQLException
 */
    public void buscarestadosXnom (String nom_pais, String nom_depto, String zona) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        VecEstados = null;
        String query = "SQL_BUSCAR_ESTADO_XNOM";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, nom_pais);
                st.setString(2, nom_depto);
                st.setString(3, zona);
                rs = st.executeQuery();
                
                VecEstados =  new Vector();
                estado = null;
                while (rs.next()){
                    estado = new Estado();
                    estado.setdepartament_code( rs.getString( "departament_code") );
                    estado.setdepartament_name( rs.getString("departament_name" ) );
                    estado.setpais_code( rs.getString("country_code") );
                    estado.setpais_name( rs.getString("country_name") );
                    estado.setzona( rs.getString("zona") );  
                    VecEstados.add(estado);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL ESTADO" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }



/**
 *
 * @throws SQLException
 */
    public void modificarestado () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_MODIFICAR_ESTADO";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, estado.getdepartament_name() );
                st.setString(2, estado.getzona() );
                st.setString(3,  estado.getUser_update() );
                st.setString(4, estado.getpais_code() );
                st.setString(5, estado.getdepartament_code() );
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR EL PAIS" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


/**
 * 
 * @throws SQLException
 */
    public void Activarestado () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_ACTIVAR_ESTADO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,  estado.getUser_update() );
                st.setString(2, estado.getpais_code() );
                st.setString(3, estado.getdepartament_code() );
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR EL PAIS" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }


 /**
  * 
  * @throws SQLException
  */
    public void anularestado () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String query = "SQL_ANULAR_ESTADO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, estado.getUser_update() );
                st.setString(2, estado.getpais_code() );
                st.setString(3, estado.getdepartament_code() );
                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR EL ESTADO" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }
  
/**
 * 
 * @param cpais
 * @return
 * @throws SQLException
 */
   public boolean existeEstadosxPais(String cpais)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
       String query = "SQL_EXISTE_ESTADOXPAIS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cpais);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS ESTADOS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
   /**
     * Metodo <tt>buscarEstadosxNombre</tt>, obtiene los estados segun paramtros de busqueda
     * @autor : Ing. Sandra Escalante
     * @param codigo del pais, frase nombre de estado (String)     
     * @version : 1.0
     */
   public void buscarEstadosxNombre (String pais, String frase) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        VecEstados = null;
       String query = "SQL_BUSCARXNOMBRE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, pais);
                st.setString(2, frase + "%");  ////System.out.println("STTT " + st);              
                rs = st.executeQuery();
                VecEstados =  new Vector();
                estado = null;
                while (rs.next()){
                    estado = new Estado();
                    estado.setdepartament_code( rs.getString( "codigo") );
                    estado.setdepartament_name( rs.getString("nombre" ) );                    
                    VecEstados.add(estado);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR EL ESTADO POR NOMBRE" + e.getMessage() + "" + e.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }

}
