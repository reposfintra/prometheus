/****
 * Nombre clase :              IngresoDAO.java
 * Descripcion :              Clase que maneja los DAO ( Data Access Object )
 *                            los cuales contienen los metodos que interactuan
 *                            con la BD.
 * Autor :                    Ing. Diogenes Antonio Bastidas Morales
 * Fecha :                    9 de mayo de 2006, 09:19 AM
 * Version :  1.0
 * Copyright : Fintravalores S.A.
 ****/


package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.EncriptionException;

public class ProcesarRecaudosFenalcoDAO extends MainDAO {
    private LinkedList listadoingreso;
    /** Creates a new instance of IngresoDAO */
    public ProcesarRecaudosFenalcoDAO() {
        super("ProcesarRecaudosFenalcoDAO.xml");
    }
    public ProcesarRecaudosFenalcoDAO(String dataBaseName) {
        super("ProcesarRecaudosFenalcoDAO.xml", dataBaseName);
    }

    /**
     * Metodo insertarIngreso, ingresa un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public ArrayList  buscarRecaudos() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl=new ArrayList();
        String query = "SQL_BUSCAR_RECAUDOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while(rs.next()){
                Recaudos rec=new Recaudos();
                rec.setOid(rs.getString("OID"));
                rec.setBanco(rs.getString("banco"));
                rec.setFacturas(rs.getString("facturas"));
                rec.setFecha(rs.getString("fecha"));
                rec.setSucursal(rs.getString("suscursal"));
                rec.setValor(rs.getDouble("valor"));
                rec.setIntereses(rs.getDouble("intereses"));
                rec.setCuenta(rs.getString("cuenta"));
                rec.setCod(rs.getString("cod"));
                arl.add(rec);
            }
            }}catch(Exception e) {
            throw new Exception("Error al buscar los recaudos ... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }

 /**
  *
  * @param facturas
  * @return
  * @throws Exception
  */
    public double  ValorFacturas(String facturas[]) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Recaudos rec=new Recaudos();
        double ret=0;
        String query = "SQL_SUMA_FACTURAS";
        String dato="(";
                    for (int i = 0; i < facturas.length; i++) {
                        dato = dato + "'" + facturas[i] + "'";
                        if (i != facturas.length - 1) {
                            dato = dato + ",";
                        }
                    }
        dato=dato+")";
        try{
            String sql=this.obtenerSQL("SQL_SUMA_FACTURAS");
            System.out.println("sql:"+sql);
            sql=sql.replaceAll("\\?", dato);
            System.out.println("sql:"+sql);

            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(sql);//JJCastro fase2
            rs = st.executeQuery();
            if(rs.next()){
                ret=rs.getDouble("valor");
            }
            }}catch(Exception e) {
            throw new Exception("Error al buscar los recaudos ... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ret;
    }



  /**
   *
   * @param factura
   * @return
   * @throws Exception
   */
    public String [] cancelarFactura(String factura)throws Exception{
        Connection con = null;
        PreparedStatement st2 = null;
        StringStatement st = null;
        ResultSet rs = null;
        String sql[] = new String[3];
        System.out.println(factura);
        try {
            con = this.conectarJNDI("SQL_BUSCAR_SALDO");
            if (con != null) {

                st = new StringStatement(this.obtenerSQL("SQL_CANCELAR_FACTURA"), true);//JJCastro fase2
                st.setString(1, factura);
                sql[0] = st.getSql();//JJCastro fase2


                st2 = con.prepareStatement(this.obtenerSQL("SQL_BUSCAR_SALDO"));//JJCastro fase2
                st2.setString(1, factura);
                rs = st2.executeQuery();
                if (rs.next()) {
                    sql[1] = rs.getDouble("valor_saldo") + "";
                    sql[2] = rs.getString("nit");
                }
            }
        }catch(Exception e) {
            throw new Exception("Error al Cancelar Factura ... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st2  != null){ try{ st2.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }




    /**
     *
     * @param oid
     * @return
     * @throws Exception
     */
    public String actualizarRecaudo(String oid)throws Exception{
        StringStatement st = null;
        String sql="";
        String query = "SQL_ACTUALIZAR_RECAUDO";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, oid);
            sql= st.getSql();
        }catch(Exception e) {
            throw new Exception("Error al Actualizar Recaudo ... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }



    public String insertarIngreso(Ingreso ingreso,String factura,String cod) throws Exception {
        StringStatement st = null;
        ResultSet rs = null;
        String sql = "";
        String query = "SQL_INGRESAR";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, ingreso.getReg_status());
            st.setString(2, ingreso.getDstrct());
            st.setString(3, factura);
            st.setString(4, cod);
            st.setString(5, factura);
            st.setString(6, cod);
            st.setString(7, ingreso.getNum_ingreso());
            st.setString(8, ingreso.getConcepto());
            st.setString(9, ingreso.getTipo_ingreso());
            st.setString(10, ingreso.getFecha_consignacion());
            st.setString(11, ingreso.getBranch_code());
            st.setString(12, ingreso.getBank_account_no());
            st.setString(13, ingreso.getCodmoneda());
            st.setString(14, ingreso.getAgencia_ingreso());
            st.setString(15, ingreso.getPeriodo());
            st.setDouble(16, ingreso.getVlr_ingreso());
            st.setDouble(17, ingreso.getVlr_ingreso_me());
            st.setInt(18, ingreso.getTransaccion());
            st.setDouble(19, ingreso.getVlr_tasa());
            st.setString(20, ingreso.getFecha_tasa());
            st.setInt(21, ingreso.getCant_item());
            st.setString(22, ingreso.getCreation_user());
//            st.setString(22, ingreso.getCreation_date());
            st.setString(23, ingreso.getUser_update());
//            st.setString(24, ingreso.getLast_update());
            st.setString(24, ingreso.getBase());
            st.setInt(25, ingreso.getTransaccion_anulacion());
            st.setString(26, ingreso.getTipo_documento());
            st.setString(27, factura);
            st.setString(28, cod);
            st.setString(29, ingreso. getNro_consignacion());
            st.setString(30, "");
            st.setString(31, ingreso.getAuxiliar());
            st.setDouble(32, ingreso.getTasaDolBol());
            st.setString(33, ingreso.getAbc());
            st.setDouble(34, ingreso.getVlr_saldo_ing());
            st.setString(35, "");
            sql = st.getSql();
        }catch(Exception e) {
            throw new Exception("Error al Insertar Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }


    /**
     *
     * @param factura
     * @return
     * @throws Exception
     */
    public String getCuentaCmc(String factura) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String cuenta = "";
        String query = "SQL_BUSCAR_CUENTA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, factura);
                rs = st.executeQuery();
                if (rs.next()) {
                    cuenta = rs.getString("cuenta");
                }
            }
        }catch(Exception e) {
            throw new Exception("Error getCuentaCmc \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cuenta;
    }




    /**
     *
     * @param ingreso_detalle
     * @param item
     * @param factura
     * @param descripcion
     * @return
     * @throws SQLException
     */
    public String insertarIngresoDetalle (Ingreso ingreso_detalle,int item,String factura,String descripcion) throws SQLException {

        StringStatement st = null;
        String sql ="";
        String query = "SQL_INGRESAR_DETALLE";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString (1,  ingreso_detalle.getDstrct ());
            st.setString (2,  ingreso_detalle.getNum_ingreso ());//nit
            st.setString (3,  ingreso_detalle.getNum_ingreso ());
            st.setInt    (4,  item);
            st.setDouble (5,  ingreso_detalle.getVlr_ingreso ());
            st.setDouble (6,  ingreso_detalle.getVlr_ingreso_me ());
            st.setString (7,  factura);
            st.setString (8,  factura);//fecha_factura
            st.setString (9,  "");
            st.setDouble (10, 0.00);
            st.setDouble (11, 0.00);
            st.setString (12, "");
            st.setDouble (13, 0.00);
            st.setDouble (14, 0.00);
            st.setDouble (15, 0.00);
            st.setString (16, ingreso_detalle.getCreation_user ());
            st.setString (17, ingreso_detalle.getBase ());
            st.setString (18, ingreso_detalle.getTipo_documento ());
            st.setString (19, factura);//tipodoc
            st.setString (20, factura);
            st.setDouble (21, ingreso_detalle.getVlr_tasa ());
	    st.setDouble (22, ingreso_detalle.getVlr_ingreso ());
            st.setString (23, ingreso_detalle.getCuenta());
            st.setString (24, ( !ingreso_detalle.getTipo_aux().equals("") && !ingreso_detalle.getAuxiliar().equals("") )?ingreso_detalle.getTipo_aux()+"-"+ingreso_detalle.getAuxiliar():"");
            st.setString (25, descripcion);
            sql = st.getSql();
        }catch(Exception ex){
            throw new SQLException ("ERROR AL INGRESAR LOS ITEMS DE LAS FACTURAS, "+ex.getMessage ());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }

 /**
  *
  * @param tipo
  * @return
  * @throws Exception
  */
    public String  buscarSerie(String tipo) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        PreparedStatement st2 = null;
        ResultSet rs = null;
        int num = 0;
        String serie = "";
        String query = "SQL_BUSCAR_SERIE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tipo);
                rs = st.executeQuery();
                if (rs.next()) {
                    num = rs.getInt("last_number");
                    serie = rs.getString("prefix") + Util.llenarConCerosALaIzquierda(num, 6);
                } else {
                    serie = "";
                }
                if (num > 0) {
                    st2 = con.prepareStatement(this.obtenerSQL("SQL_INC_SERIE"));//JJCastro fase2
                    st2.setString(1, tipo);
                    st2.execute();
                }
            }
        }catch(Exception e) {
            throw new Exception("Error al buscar la serie Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (st2  != null){ try{ st2.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return serie;
    }


    /**
     *
     * @param opcion
     * @return
     * @throws Exception
     */
        public String ProcesarNotas(int opcion) throws Exception{
        PreparedStatement st =  null;
        ResultSet rs =  null;
        Connection con = null;
        String ret="";
        try{
            con = this.conectarJNDI("SQL_PROCESAR_NOTAS");
            st  = con.prepareStatement(this.obtenerSQL("SQL_PROCESAR_NOTAS"));
            if(opcion==2){
                st.setString(1, "IAS");
            }else{
                st.setString(1, "NAS");
            }
            rs  = st.executeQuery();
            if(rs.next()){
                ret = rs.getString("log");
            }
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
            throw e;
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ret;
    }


}