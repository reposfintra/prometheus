/*
 * Nombre        MovimientoAduanaDAO.java
 * Descripci�n   Se encarga de realizar las consultas a la bd referentes a los movimientos de aduana
 * Autor         Alejandro Payares
 * Fecha         19 de enero de 2006, 04:24 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.DAOS;

import java.util.LinkedList;
import java.sql.*;
import com.tsp.operation.model.beans.MovimientoAduana;

/**
 * Se encarga de realizar las consultas a la bd referentes a los movimientos de aduana
 * @author Alejandro Payares
 */
public class MovimientoAduanaDAO extends MainDAO{
    
    /**
     * Una lista donde se guardan los datos de los movimientos de aduana
     */    
    public LinkedList datos;
    
    /**
     * Crea una nueva instancia de MovimientoAduanaDAO
     * @autor  Alejandro Payares
     */
    public MovimientoAduanaDAO() {
        super("MovimientoAduanaDAO.xml");
    }
    
    /**
     * Busca los movimientos de aduana para la remesa dada, el rsultado de la busqueda lo guarda en una lista
     * @param numrem el numero de la remesa
     * @throws SQLException si algun error en la base de datos ocurre
     */    
    public void buscarMovimientos(String numrem)throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        datos = new LinkedList();
        String query = "SQL_OBTENER_MOVIMIENTOS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, numrem);
                ////System.out.println("query movimientos aduana: "+ps);
                rs = ps.executeQuery();
                while (rs.next()) {
                    datos.add(MovimientoAduana.load(rs));
                }
            }
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Devuelve la lista con los movimientos de aduana encontrados por el m�todo buscarMovimientos
     * @return La lista con los movimientos de aduana encontrados
     */    
    public LinkedList obtenerMovimientos(){
        return datos;
    }
}
