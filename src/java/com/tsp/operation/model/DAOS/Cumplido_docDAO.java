/************************************************************************
 * Nombre clase: CumplidoDAO.java                                       *
 * Descripci�n: Clase que maneja las consultas de los docuemntos        *
 *              cumplidos y                                             *
 * Autor: Ing. Rodrigo Salazar                                          *
 * Fecha: Created on 5 de octubre de 2005, 04:39 PM                     *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  R.SALAZAR
 */
public class Cumplido_docDAO {
    
    /** Creates a new instance of Cumplido_docDAO */
    public Cumplido_docDAO () {
    }
    private Cumplido_doc cumpdoc;
    private Vector cumps;
    private final String SQL_SEARCH_CUMPLIDO_DOC = "select * from cumplido_doc where reg_status != 'A' and dstrct = ? and numrem = ? and numpla = ? and tipo_doc = ? and documento = ? and tipo_doc_rel = ? and documento_rel = ?";
    private final String SQL_CLIASIG = "SELECT clientedestinat FROM usuarios WHERE idusuario = ? ";    
    
    
    private final String SQL_DOCSCAN =            
            "\nSELECT " +        
            "\n        numrem, " +
            "\n        numpla, " + 
            "\n        plaveh, " + 
            "\n        descripcion,  " +
            "\n        destin,  " +
            "\n        feccum,  " +
            "\n        dstrct,  " +	
            "\n        fecent,  " +
            "\n        cliente,  " +
            "\n        codcliente,  " +
            "\n       COALESCE(array_to_string(array_accum(COALESCE(documento, '')),','),'NR') AS docs,  " +
            "\n        COALESCE(array_to_string(array_accum(COALESCE(get_destablagen(tipo_doc, 'TDOC'), '')),','),'NR') AS tdocs  " +
            "\nFROM (  " +
            "\n        SELECT DISTINCT c1.*, documento, tipo_doc  " +
            "\n        FROM (  " +
            "\n           SELECT  " +
            "\n                   rem.numrem,  " +
            "\n                   pl.numpla,  " +
            "\n                   pl.plaveh,  " +
            "\n                   descripcion,  " +
            "\n                   array_to_string(array_accum(get_nombreremidest(rd.codigo)),',<br>') AS destin,  " +
            "\n                   TO_CHAR(cum.creation_date, 'YYYY-mm-DD HH:MI') AS feccum,  " +
            "\n                   cum.dstrct,	 " +
            "\n                   COALESCE(TO_CHAR(fechareporte, 'YYYY-mm-DD HH:MI'),'NR') AS fecent,  " +
            "\n                   get_nombrecliente(rem.cliente) AS cliente,  " +
            "\n                   rem.cliente AS codcliente  " +
            "\n           FROM    cumplido cum  " +
            "\n           JOIN remesa rem ON ( cum.dstrct = rem.cia AND cum.tipo_doc = '002' AND cum.cod_doc = rem.numrem )  " +
            "\n           JOIN plarem plr ON ( plr.numrem = rem.numrem )  " +
            "\n           JOIN planilla pl ON ( pl.numpla = plr.numpla )   " +	
            "\n           JOIN remesadest rd ON ( rd.numrem = rem.numrem )  " +
            "\n           LEFT JOIN rep_mov_trafico rmt ON ( rmt.numpla = pl.numpla AND rmt.tipo_reporte = 'ECL' )  " +
            "\n           WHERE    " +
            "\n                   cum.creation_date BETWEEN ? AND ?  " +
            "\n           GROUP BY 1,2,3,4,6,7,8,9,10 ) AS c1  " +
            "\n        LEFT JOIN remesa_docto doc ON ( doc.dstrct = c1.dstrct AND doc.numrem = c1.numrem )  " +
            "\n) c2  " +
            "\n        GROUP BY 1,2,3,4,5,6,7,8,9,10  " +
            "\nORDER BY c2.numrem, c2.numpla ";
    /**
     * Metodo: insertarCumplidos, permite ingresar un documento cumplido.
     * @autor : Ing. rodrigo salazar
     * @param : objeto Cumplido_doc
     * @version : 1.0
     */
    public void insertarCumplidos (Cumplido_doc var) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            ////System.out.println ("insertarCumplidos");
            if (con != null){
                st = con.prepareStatement ("insert into cumplido_doc values (?,?,?,?,?,?, '',?, 'now()', ?, 'now()',?,?,?)");
                st.setString (1, var.getNumrem ());
                st.setString (2, var.getNumpla ());
                st.setString (3, var.getTipo_doc());
                ////System.out.println("**--***"+var.getTipo_doc ());
                st.setString (4, var.getDocumento ());
                st.setString (5, var.getTipo_doc_rel ());
                st.setString (6, var.getDocumento_rel ());
                st.setString (7, var.getDistrict ());
                st.setString (8, var.getMod_user ());
                st.setString (9, var.getCrea_user ());
                st.setString (10, var.getBase ());
                st.setString (11, var.getAgencia ());
                ////System.out.println(st+"///");
                st.executeUpdate ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL INSERTAR EL CUMPLIDO" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    /**
     * Metodo: searchCumplido_doc, permite buscar una documento cumplido dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : distrito, numero de la remesa, numero de la planilla,tipo de documento, documento, tipo de documento relacionado y documento relacionado.
     * @version : 1.0
     */
    public boolean searchCumplido_doc (String dstrct,String numrem,String numpla,String tipo_doc,String documento,String tipo_doc_rel,String documento_rel)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try {
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if(con!=null){
                st = con.prepareStatement (this.SQL_SEARCH_CUMPLIDO_DOC);
                st.setString (1, dstrct);
                st.setString (2, numrem);
                st.setString (3, numpla);
                st.setString (4, tipo_doc);
                st.setString (5, documento);
                st.setString (6, tipo_doc_rel);
                st.setString (7, documento_rel);
                ////System.out.println(st+"---");
                rs= st.executeQuery ();
                //cumps = new Vector ();
                if (rs.next ()){
                    sw = true;
                }
                /*while(rs.next ()){
                    cumpdoc = new Cumplido_doc ();
                    cumpdoc.setDistrict (rs.getString ("dstrct"));
                    cumpdoc.setNumpla (rs.getString ("numpla"));
                    cumpdoc.setNumrem (rs.getString ("numrem"));
                    cumpdoc.setTipo_doc (rs.getString ("tipo_doc"));
                    cumpdoc.setDocumento (rs.getString ("documento"));
                    cumpdoc.setTipo_doc_rel (rs.getString ("tipo_doc_rel"));
                    cumpdoc.setDocumento_rel (rs.getString ("documento_rel"));
                    cumps.add (cumpdoc);
                }*/
            }
            
        }catch(SQLException e){
            throw new SQLException ("ERROR DURANTE LA LISTA DEL CUMPLIDO DOC" + e.getMessage () + " " + e.getErrorCode ());
        }
        finally{
            if (st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR CERRANDO EL ESTAMENTO" + e.getMessage ());
                }
            }
            
            if (con != null){
                poolManager.freeConnection ("fintra", con);
            }
        }
        return sw;
    }
    /**
     * Metodo: getCumplido_docs, permite retornar un vector de registros de documentos cumplidos.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getCumplido_docs () {
        return cumps;
    }
    /**
     * Metodo: setCumplido_docs, permite obtener un vector de registros de documentos cumplidos.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setCumplido_docs (Vector cumps) {
        this.cumps = cumps;
    }
    
    
    /**
     * Obtiene los clientes asignados a un usuario
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param login Login del usuario
     * @throws <code>SQLException</code>
     * @version 1.0
     */
    public String clientesAdignados(String login) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        String usuarios = "";
        
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            
            if (con != null){
                st = con.prepareStatement (this.SQL_CLIASIG);
                st.setString (1, login);
                //////System.out.println(st+"///");
                
                rs = st.executeQuery ();
                
                if( rs.next() )
                    usuarios = rs.getString(1);
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL CONSULTAR CLIENTES ASIGNADOS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
        return usuarios;
    }
    
    /**
     * Obtiene los documentos cumplidos en un per�do dado.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param fechai Fecha inicial del per�odo
     * @param fechaf Fecha final del per�odo
     * @throws <code>SQLException</code>
     * @version 1.0
     */
    public void documentosCumplidos(String fechai, String fechaf) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            
            if (con != null){
                st = con.prepareStatement (this.SQL_DOCSCAN);
                st.setString (1, fechai + " 00:00:00");
                st.setString (2, fechaf + " 23:59:59");
                //////System.out.println(st+"///");
                
                rs = st.executeQuery ();
                
                this.cumps = new Vector();
                while( rs.next() ){
                    Hashtable ht = new Hashtable();
                    String numrem = rs.getString(1);
                    String numpla = rs.getString(2);
                    String placa = rs.getString(3);
                    String sj_desc = rs.getString(4);
                    String destin = rs.getString(5);
                    String feccum = rs.getString(6);
                    String fecent = rs.getString(8);
                    String docs = rs.getString(11);
                    String tdocs = rs.getString(12);
                    String nomcli =  rs.getString(9);
                    String codcli =  rs.getString(10);
                    
                    ht.put("numrem", numrem);
                    ht.put("numpla", numpla);
                    ht.put("sj_desc", sj_desc);
                    ht.put("destin", destin);
                    ht.put("feccum", feccum);
                    ht.put("nomcli", nomcli);
                    ht.put("codcli", codcli);
                    ht.put("placa", placa);
                    ht.put("fecent", fecent);
                    
                    Vector docVec = new Vector();
                    Vector tdocVec = new Vector();
                    StringTokenizer stk;
                    stk = new StringTokenizer(docs,",");
                    while( stk.hasMoreTokens()){
                        docVec.add(stk.nextToken());
                    }
                    stk = new StringTokenizer(tdocs,",");
                    while( stk.hasMoreTokens()){
                        tdocVec.add(stk.nextToken());
                    }
                    ht.put("docs", docVec);
                    ht.put("tdocs", tdocVec);
                    
                    this.cumps.add(ht);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL CONSULTAR CLIENTES ASIGNADOS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
}
