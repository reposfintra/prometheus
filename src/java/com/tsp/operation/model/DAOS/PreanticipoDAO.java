/*
 * PreanticipoDAO.java
 *
 * Created on 11 de abril de 2008, 06:47 PM
 */

package com.tsp.operation.model.DAOS;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.SQLException;


/**
 *
 * @author  Administrador
 */
public class PreanticipoDAO extends MainDAO{
    
    /** Creates a new instance of PreanticipoDAO */
    public PreanticipoDAO() {
        super("PreanticipoDAO.xml");
    }
    public PreanticipoDAO(String dataBaseName) {
        super("PreanticipoDAO.xml", dataBaseName);
    }
 
    public  ArrayList getPreanticiposImprimibles( String[] preants ) throws Exception{
        
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_CONSULTA_PREANTICIPOS_IMPRIMIBLES";
        ArrayList               lista   = new ArrayList();
        String conjunto_preanticipos="'0'";

        if (preants.length>0){
            conjunto_preanticipos="'"+preants[0]+"'";
        }
                
        for (int i=1;i<preants.length;i++){
            conjunto_preanticipos=conjunto_preanticipos+",'"+preants[i]+"'";
        }


        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
            String sql   =   this.obtenerSQL( query );
            sql=sql.replaceAll("imprimibles", conjunto_preanticipos);
            st= con.prepareStatement(sql);
                        
            rs=st.executeQuery();
            while(rs.next()){
                String[] preanticipo=new String[6];
                preanticipo[0]=rs.getString("cedcon");
                preanticipo[1]=rs.getString("val");
                preanticipo[2]=rs.getString("creation_date");
                preanticipo[3]=rs.getString("creation_user");
                preanticipo[4]=rs.getString("estado_pago");
                preanticipo[5]=rs.getString("id");
                lista.add(preanticipo);
                
            }
            
        }}catch(Exception e){
            System.out.println("en getPreanticiposImprimibles"+e.toString()+"_"+e.getMessage());
            throw new Exception( "getPreanticiposImprimibles " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

}
