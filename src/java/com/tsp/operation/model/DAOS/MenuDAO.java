/*
 * Nombre        MenuDAO.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         26 de abril de 2005, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Administrador
 */
public class MenuDAO extends MainDAO{
    
    /** Creates a new instance of MenuDAO */
    public MenuDAO() {
        super("MenuDAO.xml");//JJCastro fase2
    }
    public MenuDAO(String dataBaseName) {
        super("MenuDAO.xml", dataBaseName);//JJCastro fase2
    }
    
    private Menu menu;
    private Vector vmenu;
    private Vector vhijos;   
    
     private static final String S_CARGARMENU_USUARIO = "SELECT  DISTINCT on (m1.nivel, m1.orden, m1.id_opcion)  m1.*, " +
    "	op.id_perfil     " +
    "FROM    menu_dinamico m1 JOIN menu_dinamico m2 ON (m1.id_padre = m2.id_opcion )        " +
    "	JOIN perfilopcion_trf op  ON ( m1.id_opcion= op.id_opcion )        " +
    "	JOIN perfilusuarios d ON (op.id_perfil = d.perfil)    " +
    "WHERE   d.usuarios = ?     " +
    "	AND op.rec_status=''         " +
    "	AND m1.rec_status=''       " +
    "	AND d.status != 'A'" +
    "ORDER BY    m1.nivel, m1.orden, m1.id_opcion";
    
    private String S_MAXOPCION = "  SELECT  MAX(id_opcion)+1 AS maximo " +
    "                               FROM    menu_dinamico";
    private String S_INSERT = " INSERT INTO menu_dinamico " +
    "                           VALUES(?,?,?,?,?,?,?,'','now()',?,'now()','',?)";
    private String S_UPDATE = " UPDATE menu_dinamico " +
    "                           SET nombre =?, " +
    "                               descripcion=?, " +
    "                               url=?, " +
    "                               user_update=?, " +
    "                               last_update='now()', " +
    "                               orden=?, " +
    "                               id_padre=?, " +
    "                               nivel=? " +
    "                           WHERE id_opcion = ?";
    private String S_ANULAR = " UPDATE  menu_dinamico " +
    "                           SET     rec_status='A', " +
    "                                   user_update= ?, " +
    "                                   last_update='now()' " +
    "                           WHERE   id_opcion= ?";
    private String S_CARGARMENU = " SELECT DISTINCT  m1.*, " +
    "                                               op.id_perfil " +
    "                               FROM    menu_dinamico m1, " +
    "                                       menu_dinamico m2, " +
    "                                       perfilopcion_trf op " +
    "                               WHERE   m1.id_padre = m2.id_opcion " +
    "                                       AND m1.id_opcion= op.id_opcion " +
    "                                       AND op.id_perfil = ? " +
    "                                       AND op.rec_status='' " +
    "                                       AND m1.rec_status='' " +
    "                               ORDER BY    m1.nivel, m1.orden, m1.id_opcion";
    private String S_OPRAIZ = " SELECT  id_opcion, " +
    "                                   nombre, " +
    "                                   nivel, " +
    "                                   orden, " +
    "                                   submenu_programa " +
    "                           FROM    menu_dinamico " +
    "                           WHERE   id_padre = '0' " +
    "                                   AND id_opcion != '0' " +
    "                                   AND rec_status = '' " +
    "                           ORDER BY orden";
    private String S_OPHIJO = " SELECT  nombre, " +
    "                                   id_opcion, " +
    "                                   submenu_programa, " +
    "                                   orden, " +
    "                                   nivel, " +
    "                                   id_padre, " +
    "                                   url, " +
    "                                   descripcion " +
    "                           FROM    menu_dinamico " +
    "                           WHERE   id_padre = ? " +
    "                                   AND rec_status='' " +
    "                                   AND id_opcion <> 0 " +
    "                           ORDER BY orden, nivel";        
    private String S_OPCION1 = "SELECT  m1.id_opcion, " +
    "                                   m1.nombre, " +
    "                                   m1.id_padre, " +
    "                                   m1.nivel, " +
    "                                   m1.orden, " +
    "                                   m1.submenu_programa," +
    "                                   m2.id_opcion as padre," +
    "  			                m2.nombre as pnombre, " +
    "				        m2.orden as porden   " +
    "                           FROM    menu_dinamico m1, " +
    "                                   menu_dinamico m2 " +
    "                           WHERE   m1.id_padre = m2.id_opcion " +
    "                                   AND m1.id_opcion= ? " +
    "                                   AND m1.rec_status=''";
    private String S_OPCION2 = "SELECT  * " +
    "                           FROM    menu_dinamico " +
    "                           WHERE   id_opcion = ? " +
    "                                   AND rec_status=''";    
    private String S_SPADRES = "SELECT  * " +
    "                           FROM    menu_dinamico " +
    "                           WHERE   submenu_programa = 1 " +
    "                                   AND rec_status='' " +
    "                           ORDER BY nivel, id_padre";
    private String S_UPDATENIVEL = "UPDATE  menu_dinamico " +
    "                               SET     last_update='now()', " +
    "                                       nivel=? " +
    "                               WHERE   id_opcion=?";
    private String S_TOPCIONES = "SELECT    id_opcion, " +
    "                                       id_padre " +
    "                               FROM    menu_dinamico " +
    "                               WHERE   id_opcion <> 0";
    
    
    
    private static final String PERFILES              = "   SELECT  id_perfil, " +
    "                                                               nombre " +
    "                                                       FROM    perfil_trf      " +
    "                                                       WHERE   rec_status=''   " +
    "                                                       ORDER BY 1     ";     
    
    private static final String USUARIOS              = "   SELECT  nombre, " +
    "                                                               idusuario " +
    "                                                       FROM    usuarios" +
    "                                                       WHERE   estado ='A'" +
    "                                                       ORDER BY 1     ";    
    
    private static final String SEARCH_PERFIL_USUARIO = "   SELECT  usuarios " +
    "                                                       FROM    perfilusuarios  " +
    "                                                       WHERE   perfil    =?  " +
    "                                                               AND  usuarios=?  ";
    
    private static final String INSERT_PERFIL_USUARIO = "   INSERT  INTO perfilusuarios (perfil,usuarios,usuario_creacion) " +
    "                                                       VALUES(?,?,?)         ";
    
    private static final String UPDATE_PERFIL_USUARIO = "   UPDATE  perfilusuarios " +
    "                                                       SET     status='',  " +
    "                                                               usuario_actualizacion=? " +
    "                                                       WHERE   perfil =? " +
    "                                                               AND  usuarios=?  ";
 
    private static final String VERIFICACION          = "   SELECT  usuarios" +
    "                                                       FROM    perfilusuarios  " +
    "                                                       WHERE   perfil=? " +
    "                                                               AND status=''                      ";
    
    private static final String ANULAR                = "   UPDATE  perfilusuarios " +
    "                                                       SET     status='A', " +
    "                                                               usuario_actualizacion=? " +
    "                                                       WHERE   perfil =? " +
    "                                                               AND  usuarios=?  ";
 
    private static final String PERFIL_USUARIO        = "   SELECT  a.perfil, " +
    "                                                               a.usuarios, " +
    "                                                               b.nombre    "+
                                                        "   FROM    perfilusuarios a, " +
                                                        "           perfil_trf b    "+
                                                        "   WHERE   a.perfil = b.id_perfil           "+
                                                        "           AND b.rec_status=''                  "+
                                                        "           AND a.status=''                      "+
                                                        "   ORDER BY #INDICE#                         ";
        
    private static final String SEARCH_USUARIO        = "   SELECT  nombre   " +
    "                                                       FROM    usuarios " +
    "                                                       WHERE   estado='A' " +
    "                                                               AND idusuario=?        ";

    private static int SW = 0;

   /**
     * Metodo <tt>getPerfiles()</tt>, obtiene la lista de los perfiles del sistema.     
     * @autor : Ing. Fernell Villacob
     * @param : no parameters
     * @return : <tt>List</tt>, lista de perfiles
     * @version : 1.0
     */
    
    public List getPerfiles() throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = null;     
        String query = "PERFILES";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();        
            if(rs!=null){
                lista = new LinkedList();
                while(rs.next()){
                   Perfil perfil = new Perfil();
                     perfil.setId     ( rs.getString(1));
                     perfil.setNombre (rs.getString(2));
                  lista.add(perfil);
                }
            }
            
        }}catch(Exception e){
            throw new SQLException( e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;     
    }
    
    /**
     * Metodo <tt>getusuarios()</tt>, obtiene la lista de los usuarios del sistema.     
     * @autor : Ing. Fernell Villacob
     * @param : no parameters
     * @return : <tt>List</tt>, lista de usuarios
     * @version : 1.0
     */
    public List getUsuarios() throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = null;     
        String query = "USUARIOS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();        
            if(rs!=null){
                lista = new LinkedList();
                while(rs.next()){
                  Usuario usuario = new Usuario();
                    usuario.setNombre( rs.getString(1));
                    usuario.setLogin ( rs.getString(2));
                  lista.add(usuario);
                }
            }
            
        }}catch(Exception e){
            throw new SQLException( e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;     
    }
    
    
    /**
     * Metodo <tt>getPerfilUsuarios</tt>, obtiene la lista de los perfiles con los
     *   usuarios asosciados al mismo
     * @autor : Ing. Fernell Villacob
     * @param : String <tt>parametro</tt>, parametro de busqueda
     * @return : <tt>List</tt>, lista de perfiles-usuarios
     * @version : 1.0
     */
     public List getPerfilUsuarios(String parametro) throws SQLException {
         Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = null;     
         String query = "PERFIL_USUARIO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            String sql = this.obtenerSQL(query).replaceAll("#INDICE#",parametro);
            st= con.prepareStatement(sql);
            rs=st.executeQuery();        
            if(rs!=null){
                lista = new LinkedList();
                while(rs.next()){
                  PerfilUsuario usuario = new PerfilUsuario();
                    usuario.setPerfil  ( rs.getString(1));
                    usuario.setUsuarios( rs.getString(2));
                    usuario.setPerName ( rs.getString(3));
                  lista.add(usuario);
                }
            }
            
        }}catch(Exception e){
            throw new SQLException( e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        lista = getNameUsuarios(lista);
        return lista;     
    }
     
   /**
     * Metodo <tt>getNameUsuarios</tt>, obtiene la lista de los nombres de los usuarios.     
     * @autor : Ing. Fernell Villacob
     * @param : List <tt>lista</tt>, lista de usuarios
     * @return : <tt>List</tt>, lista de nombres de usuarios
     * @version : 1.0
     */
     public List getNameUsuarios(List lista) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs         = null;   
         String query = "SEARCH_USUARIO";//JJCastro fase2
        try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 if (lista != null && lista.size() > 0) {
                     Iterator it = lista.iterator();
                     while (it.hasNext()) {
                         PerfilUsuario usuario = (PerfilUsuario) it.next();
                         st.setString(1, usuario.getUsuarios());
                         rs = st.executeQuery();
                         while (rs.next()) {
                             usuario.setUserName(rs.getString(1));
                         }
                     }
                 }
             }
         }catch(Exception e){
            throw new SQLException( e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;     
    }
     
    /**
     * Metodo <tt>insert</tt>, ingresa o actualiza en el sistema los usuarios asociados a un perfil(es)
     * @autor : Ing. Fernell Villacob
     * @param : lista perfiles, lista usuarios     
     * @version : 1.0
     */
    public void insert (String[] GRUPOS, String[] USUARIOS, String user) throws Exception {
        try{
            for(int i=0;i<=GRUPOS.length-1;i++){
               String perfil   = GRUPOS[i];
               for(int j=0;j<=USUARIOS.length-1;j++){
                 String usuario =  USUARIOS[j]; 
                 if(!existe(perfil,usuario))  insertPU(perfil, usuario, user); 
                 else                         update(perfil, usuario, user);
               }
               verificar(perfil,USUARIOS,user);
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en Insert [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }        
    }
    
    /**
     * Metodo <tt>existe</tt>, obtiene la lista de los nombres de los usuarios.     
     * @autor : Ing. Fernell Villacob
     * @param : perfil, usuario
     * @return : <tt>boolean</tt>, verifica la existencia de uan relacion usuario-perfil
     * @version : 1.0
     */   
     public boolean existe(String PERFIL, String USUARIO) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement stmt  = null;
        ResultSet rs            = null;
        boolean   estado        = false;
         String query = "SEARCH_PERFIL_USUARIO";//JJCastro fase2

         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 stmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 stmt.setString(1, PERFIL);
                 stmt.setString(2, USUARIO);
                 rs = stmt.executeQuery();
                 while (rs.next()) {
                     estado = true;
                 }
             }
         }
        catch(SQLException e){
            throw new SQLException("Error en busqueda Usuarios para perfil [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (stmt  != null){ try{ stmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return estado;
    }
     
    /**
     * Metodo <tt>Insert</tt>, inserta una relacion usuario-perfil
     * @autor : Ing. Fernell Villacob
     * @param : perfil, usuario y usuario de creacion del registro     
     * @version : 1.0
     */
     public void insertPU (String GRUPOS, String USUARIOS, String user) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement stmt = null;
        String query = "INSERT_PERFIL_USUARIO";//JJCastro fase2
        try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 stmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 stmt.setString(1, GRUPOS);
                 stmt.setString(2, USUARIOS);
                 stmt.setString(3, user);
                 stmt.executeUpdate();
             }
         }
        catch(SQLException e){
            throw new SQLException("Error en Insert [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (stmt  != null){ try{ stmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
    /**
     * Metodo <tt>update</tt>, inserta una relacion usuario-perfil
     * @autor : Ing. Fernell Villacob
     * @param : perfil, usuario y usuario de creacion del registro     
     * @version : 1.0
     */ 
    public void update (String PERFIL, String USUARIOS, String user) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement stmt = null;
        String query = "UPDATE_PERFIL_USUARIO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                stmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                stmt.setString(1, user);
                stmt.setString(2, PERFIL);
                stmt.setString(3, USUARIOS);
                stmt.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("Error en Update [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (stmt  != null){ try{ stmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>verificar</tt>, comprueba el ingreso o remocion de usuarios de 
     *  un perfil
     * @autor : Ing. Fernell Villacob
     * @param : perfil, usuarios y usuario de creacion/anulacion del registro    
     * @version : 1.0
     */ 
    public void verificar(String PERFIL, String[] USUARIOS, String user) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;        
        ResultSet         rs = null;
        String query = "VERIFICACION";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  PERFIL);
            rs=st.executeQuery();        
            if(rs!=null){
               while(rs.next()){
                 String usuario = rs.getString(1);
                 int sw = 0;
                 for(int j=0;j<=USUARIOS.length-1;j++){
                      String usuArray =  USUARIOS[j]; 
                      if(usuario.equals(usuArray)){
                         sw=1;
                         break;
                      }
                 }
                 if (sw==0) anular(PERFIL, usuario, user);
               }
            }
        }}
        catch(SQLException e){
            throw new SQLException("Error en Update [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>anular</tt>, anula un usuario de un perfil     
     * @autor : Ing. Fernell Villacob
     * @param : perfil, usuario y usuario de anulacion del registro    
     * @version : 1.0
     */ 
     public void anular (String PERFIL, String USUARIOS, String user) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement stmt = null;        
        String query = "ANULAR";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            stmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            stmt.setString(1, user);
            stmt.setString(2, PERFIL);
            stmt.setString(3, USUARIOS);             
            stmt.executeUpdate();
            }}
        catch(SQLException e){
            throw new SQLException("Error en Update [MenuDAO] " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (stmt  != null){ try{ stmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>getVMenu</tt>, obtiene el Vector vmenu
     * @autor : Ing. Sandra M Escalante G
     * @param : no parameters
     * @return: <tt>Vector</tt>, vector de opciones
     * @version : 1.0
     */ 
    public  Vector getVMenu(){
        return vmenu;
    }
    
    /**
     * Metodo <tt>obtenerPadresRaizMenu</tt>, obtiene las opciones cuyo padre es 0
     *  por perfil
     * @autor : Ing. Sandra M Escalante G
     * @param : no parameters     
     * @version : 1.0
     */ 
    public void obtenerPadresRaizMenu () throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_OPRAIZ";//JJCastro fase2
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                
                vmenu = new Vector();
                
                while(rs.next()){
                    
                    Menu menu = new Menu();
                    menu.setIdopcion(rs.getInt("id_opcion"));
                    menu.setNombre(rs.getString("nombre"));
                    menu.setNivel(rs.getInt("nivel"));                
                    menu.setOrden(rs.getInt("orden"));
                    menu.setSubmenu(rs.getInt("submenu_programa"));
                    vmenu.add(menu);
                    menu = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PADRES (RAIZ) MENU " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>obtenerPadreMenuxIDopcion</tt>, obtiene la opcion padre de una
     *  opcion     
     * @autor : Ing. Sandra M Escalante G
     * @param : <tt>int</tt> codigo de la opcion     
     * @version : 1.0
     */ 
    public void obtenerPadreMenuxIDopcion ( int ido ) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_OPCION1";//JJCastro fase2
        
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, ido);                
                rs = st.executeQuery();
                
                vmenu = new Vector();
                
                while(rs.next()){
                    
                    Menu menu = new Menu();
                    menu.setIdopcion(rs.getInt("id_opcion"));
                    menu.setNivel(rs.getInt("nivel"));
                    menu.setIdpadre(rs.getInt("id_padre"));                                                            
                    menu.setNombre(rs.getString("nombre"));
                    menu.setOrden(rs.getInt("orden"));   
                    menu.setSubmenu(rs.getInt("submenu_programa"));
                    menu.setPadre(rs.getInt("padre"));
                    menu.setPnombre(rs.getString("pnombre"));
                    menu.setPorden(rs.getString("porden"));                    
                    vmenu.add(menu);
                menu = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PADRES MENU POR ID " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>buscarOpcionMenuxIdopcion</tt>, obtiene una opcion del menu 
     *  dado un codigo
     * @autor  Ing. Sandra M Escalante G
     * @param  <tt>int</tt> codigo de la opcion     
     * @return <tt>Menu</tt>, opcion
     * @version  1.0
     */ 
    public Menu buscarOpcionMenuxIdopcion(int ido)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        menu = new Menu();
        String query = "S_OPCION2";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, ido);                
                rs = st.executeQuery();                
                
                if(rs.next()){                                                          
                    menu.setIdopcion(rs.getInt("id_opcion"));
                    menu.setNivel(rs.getInt("nivel"));
                    menu.setIdpadre(rs.getInt("id_padre"));
                    menu.setDescripcion(rs.getString("descripcion"));        
                    menu.setSubmenu(rs.getInt("submenu_programa"));
                    menu.setUrl(rs.getString("url"));
                    menu.setNombre(rs.getString("nombre"));                    
                    menu.setOrden(rs.getInt("orden"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA OPCION (MENU) " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return menu;
    }
    
    /**
     * Metodo <tt>cargarMenuxPerfil</tt>, establece un vector con las opciones 
     *  del menu por perfil
     * @autor : Ing. Sandra M Escalante G
     * @param : <tt>String</tt> perfil del usuario
     * @version : 1.0
     */
    public   void cargarMenuxPerfil(String prf) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_CARGARMENU";//JJCastro fase2
        
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, prf);                
                rs = st.executeQuery();                
                vmenu = new Vector();
                
                while(rs.next()){                    
                    Menu menu = new Menu();
                    menu.setIdopcion(rs.getInt("id_opcion"));
                    menu.setNivel(rs.getInt("nivel"));
                    menu.setIdpadre(rs.getInt("id_padre"));
                    menu.setDescripcion(rs.getString("descripcion"));        
                    menu.setSubmenu(rs.getInt("submenu_programa"));
                    menu.setUrl(rs.getString("url"));
                    menu.setNombre(rs.getString("nombre"));                    
                    vmenu.add(menu);
                    menu = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CARGA DEL MENU " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
     /**
     * Metodo <tt>setMenu</tt>, instancia un objeto Menu     
     * @autor : Ing. Sandra M Escalante G
     * @param : <tt>Menu</tt> objeto
     * @version : 1.0
     */
    public void setMenu(Menu m){
        this.menu = m;
    }
    
    /**
     * Metodo <tt>nuevaOpcionMenu</tt>, registra una nueva opcion en el sistema
     * @autor : Ing. Sandra M Escalante G
     * @return : <tt>int</tt>, id de la nueva opcion
     * @version : 1.0
     */
    public int nuevaOpcionMenu()throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector vmenu = null;        
        int idopcion = 0;
        String query = "S_MAXOPCION";//JJCastro fase2
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ResultSet rs1 = st.executeQuery();
            rs1.next();
                    
            idopcion = rs1.getInt("maximo");
            

                st = con.prepareStatement(this.obtenerSQL("S_INSERT"));//JJCastro fase2
                st.setInt (1, idopcion);
                st.setInt(2, menu.getNivel());
                st.setInt(3, menu.getIdpadre());
                st.setString(4, menu.getDescripcion());
                st.setInt(5, menu.getSubmenu());
                st.setString(6, menu.getUrl());
                st.setString(7, menu.getNombre());                
                st.setString(8, menu.getCreado_por());
                st.setInt(9, menu.getOrden());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA OPCION " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return idopcion;
    }
    
    /**
     * Metodo <tt>updateOpcionMenu</tt>, actualiza uan opcion del sistema
     * @autor : Ing. Sandra M Escalante G     
     * @version : 1.0
     */
    public void updateOpcionMenu()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_UPDATE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, menu.getNombre());
                st.setString(2, menu.getDescripcion());
                st.setString(3, menu.getUrl());
                st.setString(4, menu.getUser_update());
                st.setInt(5, menu.getOrden());
                st.setInt(6, menu.getIdpadre());
                st.setInt(7, menu.getNivel());
                st.setInt(8, menu.getIdopcion());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA OPCION DEL MENU " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>anularOpcionMenu</tt>, anula una nueva opcion del sistema
     * @autor : Ing. Sandra M Escalante G     
     * @version : 1.0
     */
    public void anularOpcionMenu()throws SQLException{
         
         Connection con = null;
         PreparedStatement st = null;
         ResultSet rs = null;
         String query = "S_ANULAR";//JJCastro fase2
         
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, menu.getUser_update());
                 st.setInt(2, menu.getIdopcion());
                 st.executeUpdate();
             }
         }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA ANULACION DE LA OPCION " + e.getMessage() + " " + e.getErrorCode());
         }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>obtenerOpcion</tt>, instancia una opcion con codigo dado
     * @autor : Ing. Sandra M Escalante G
     * @param : <tt>int</tt>, id de la opcion
     * @version : 1.0
     */    
    public void obtenerOpcion ( int ido)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_OPCION2";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, ido);                
                rs = st.executeQuery();
                
                if(rs.next()){
                    menu = Menu.load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA OPCION " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>getMenuOpcion</tt>, obtiene el objeto menu insanciado anteriormente
     * @autor : Ing. Sandra M Escalante G
     * @return : <tt>Menu</tt>, objeto
     * @version : 1.0
     */
    public Menu getMenuOpcion() throws SQLException{
        return menu;
    }
    
    /**
     * Metodo <tt>obtenerPadres</tt>, obtiene las opciones (carpetas)
     * @autor : Ing. Sandra M Escalante G     
     * @version : 1.0
     */
    public   void obtenerPadres( ) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_SPADRES";//JJCastro fase2
        
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
        
                vmenu = new Vector();
                
                while(rs.next()){
                    
                    Menu menu= new Menu();
                    menu.setIdopcion(rs.getInt("id_opcion"));
                    menu.setNivel(rs.getInt("nivel"));
                    menu.setIdpadre(rs.getInt("id_padre"));
                    menu.setDescripcion(rs.getString("descripcion"));        
                    menu.setSubmenu(rs.getInt("submenu_programa"));
                    menu.setUrl(rs.getString("url"));
                    menu.setNombre(rs.getString("nombre"));                    
                    vmenu.add(menu);
                    menu = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL MENU (PADRES) " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * Metodo <tt>obtenerHijos</tt>, obtiene las opciones (carpetas)
     * @autor  Ing. Sandra M Escalante G     
     * @param <tt>int</tt> codigo de la opcion
     * @version  1.0
     */
    public  void obtenerHijos( int idpadre ) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_OPHIJO";//JJCastro fase2
        
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt( 1, idpadre);                
                rs = st.executeQuery();
                vhijos = new Vector();
                
                while(rs.next()){
                    Menu menu= new Menu();
                    menu.setIdopcion(rs.getInt("id_opcion"));                    
                    menu.setSubmenu(rs.getInt("submenu_programa"));                    
                    menu.setNombre(rs.getString("nombre"));
                    menu.setOrden(rs.getInt("orden"));
                    menu.setNivel(rs.getInt("nivel"));
                    menu.setIdpadre(rs.getInt("id_padre"));
                    menu.setUrl(rs.getString("url"));
                    menu.setDescripcion(rs.getString("descripcion"));
                    vhijos.add(menu);
                    menu = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL MENU (HIJOS) " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * Metodo <tt>cambiarNivel</tt>, actualiza el nivel de las opciones
     * @autor Ing. Sandra M Escalante G     
     * @param <tt>int</tt> opcion, nivel 
     * @version 1.0
     */
    public void cambiarNivel(int opcion, int nivel)throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_UPDATENIVEL";//JJCastro fase2
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt( 1, nivel);
                st.setInt(2, opcion);
                st.executeUpdate();                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL NIVEL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * Metodo <tt>obtenerTOpciones</tt>, obtiene las todas opciones del sistema
     * @autor : Ing. Sandra M Escalante G     
     * @version : 1.0
     */
    
    public void obtenerTOpciones () throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String query = "S_TOPCIONES";//JJCastro fase2
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                
                vmenu = new Vector();
                
                while(rs.next()){
                    Menu menu = new Menu();
                    menu.setIdopcion(rs.getInt("id_opcion"));
                    menu.setNivel(rs.getInt("id_padre"));                
                    vmenu.add(menu);
                    menu = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS OPCIONES MENU " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Getter for property vhijos.
     * @return Value of property vhijos.
     */
    public java.util.Vector getVhijos() {
        return vhijos;
    }
    
    /**
     * Setter for property vhijos.
     * @param vhijos New value of property vhijos.
     */
    public void setVhijos(java.util.Vector vhijos) {
        this.vhijos = vhijos;
    }
    
    /**
     * Getter for property menu.
     * @return Value of property menu.
     */
    public com.tsp.operation.model.beans.Menu getMenu() {
        return menu;
    }
    private static String S_OPACTIVOSXPERFIL = "SELECT  m1.id_opcion, " +
    "                                                   m1.rec_status" +
    "                                           FROM	menu_dinamico m1 " +
    "                                                   JOIN perfilopcion_trf po ON (m1.id_opcion = po.id_opcion)" +
    "                           		WHERE 	po.id_perfil=? "+
    "                                                   AND m1.id_padre = ?" +
    "                           			AND po.rec_status = ''"; 
    
    /**
     * Metodo <tt>tieneHijosActivosxPerfil</tt>, verifica si una opcion tiene opciones hijas activas (rec_status = '')
     * @autor : Ing. Sandra M Escalante G     
     * @version : 1.0
     */
    public boolean tieneHijosActivosxPerfil (String perfil, int opcion) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean resp = false;
        String query = "S_OPACTIVOSXPERFIL";//JJCastro fase2
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, perfil);
                st.setInt(2, opcion);
                rs = st.executeQuery();
                
                if (rs.next()){//tiene hijos activos
                    resp = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS OPCIONES ACTIVAS DE UNA CARPETA POR PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resp;
    }
    
   
    
     /**
     * Metodo <tt>cargarMenuxUsuario</tt>, establece un vector con las opciones 
     *  del menu por perfiles de usuario
     * @autor : Ing. Sandra M Escalante G
     * @param : <tt>String</tt> perfil del usuario
     * @version : 1.0
     */
    public   void cargarMenuxUsuario(String u) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "S_CARGARMENU_USUARIO";//JJCastro fase2
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, u);             
                rs = st.executeQuery();
                vmenu = new Vector();
                while(rs.next()){
                    
                    Menu menu = new Menu();
                    menu.setIdopcion(rs.getInt("id_opcion"));
                    menu.setNivel(rs.getInt("nivel"));
                    menu.setIdpadre(rs.getInt("id_padre"));
                    menu.setDescripcion(rs.getString("descripcion"));        
                    menu.setSubmenu(rs.getInt("submenu_programa"));
                    menu.setUrl(rs.getString("url"));
                    menu.setNombre(rs.getString("nombre"));                    
                    vmenu.add(menu);
                    menu = null;//Liberar Espacio JJCastro
                }

                setVMenu(vmenu);
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CARGA DEL MENU " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }  
    
    /**
     * Metodo <tt>setVMenu</tt>, setea el Vector vmenu
     * @autor : Ing. Sandra M Escalante G
     * @param : vector
     * @version : 1.0
     */ 
    public  Vector setVMenu(Vector v){
        return vmenu=v;
    }
    
    /**
     * Metodo <tt>cargarMenuJson</tt>, establece un json con las opciones 
     *  del menu por perfiles de usuario
     * @autor : Ing. Sandra M Escalante G
     * @param : <tt>String</tt> perfil del usuario
     * @version : 1.0
     */
    public String cargarMenuJson(String u, String login, String distrito) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Gson gson = new Gson();
        JsonObject objeto = new JsonObject();
        JsonArray datos = new JsonArray();
        JsonObject elemento = new JsonObject();
        
        String query = "S_CARGARMENU_USUARIO";//JJCastro fase2
        try {
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);             
                st.setString(2, u);
                rs = st.executeQuery();
                objeto.addProperty("contenedor", "menu");
                objeto.addProperty("label", login);
                while(rs.next()){
                    elemento = new JsonObject();
                    elemento.addProperty("id_opcion",rs.getInt("id_opcion"));
                    elemento.addProperty("nivel",rs.getInt("nivel"));
                    elemento.addProperty("id_padre",rs.getInt("id_padre"));
                    elemento.addProperty("title",rs.getString("descripcion"));     
                    elemento.addProperty("url",rs.getString("url"));
                    elemento.addProperty("label",rs.getString("nombre"));                    
                    datos.add(elemento);
                    elemento = null;//Liberar Espacio JJCastro
                }
                objeto.add("datos", datos);
            }
            return gson.toJson(objeto);
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CARGA DEL MENU " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    public String cargarNombreEmpresa(String distrito) throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String resp = "FINTRA S.A.";
        String query = "SQL_NOMBRE_EMPRESA";//JJCastro fase2
        try {        
            con = this.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                rs = st.executeQuery();
                
                if (rs.next()){//tiene hijos activos
                    resp = rs.getString(1);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE NOMBRE DE LA EMPRESA " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resp;
    }
}
