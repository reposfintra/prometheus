/*
 * TipoDocumentoDAO.java
 *
 * Created on 30 de marzo de 2005, 08:44 AM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;


/**
 *
 * @author  Administrador
 */
public class TipoDocumentoDAO extends MainDAO{
    
    /** Creates a new instance of TipoDocumentoDAO */
    public TipoDocumentoDAO() {
        super("TipoDocumentoDAO.xml");//JJCastro fase2
    }
    
    public TipoDocumentoDAO(String dataBaseName) {
        super("TipoDocumentoDAO.xml", dataBaseName);//JJCastro fase2
    }
    
    private Tipo_Documento tdoc;
    private Vector vtdocumentos;

/**
 * 
 * @return
 * @throws SQLException
 */
    public Vector listarTDocumentos ()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        vtdocumentos = null;
        String query = "SQL_LISTAR_DOCUMENTOS";//JJCastro fase2
        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                vtdocumentos = new Vector();
                
                while(rs.next()){
                    tdoc = new Tipo_Documento();
                    tdoc.setId(rs.getString("doc_id"));
                    tdoc.setDescripcion(rs.getString("descripcion"));
                    vtdocumentos.add(tdoc);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE DOCS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return vtdocumentos;
    }
    
   
/**
 * 
 * @param id
 * @return
 * @throws SQLException
 */
    public String getDescripcion( String id )throws SQLException{

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String desc = "";
        String query = "SQL_GETDESCRIPCION";//JJCastro fase2
        try {

            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, id);
                rs = st.executeQuery();
                if( rs.next() ){
                    desc = rs.getString( "descripcion" );
                }

            }

        }catch( SQLException e ){

            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TIPO DE DOCUMENTO " + e.getMessage() + " " + e.getErrorCode());
        
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return desc;
    }
    
/**
 * 
 * @param desc
 * @return
 * @throws SQLException
 */
    public String getId(String desc)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String id = "";
        String query = "SQL_GETID";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, desc+"%");
                rs = st.executeQuery();
                if(rs.next()){                    
                    id = rs.getString("doc_id");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TIPO DE DOCUMENTO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return id;
    }
}
