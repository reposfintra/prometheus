/*
 * CambioDestinoDAO.java
 *
 * Created on 24 de abril de 2007, 09:32 AM
 */

package com.tsp.operation.model.DAOS;


import java.util.*;
import java.sql.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.Remesa2;
import com.tsp.operation.model.beans.Auditoria;
import com.tsp.operation.model.beans.RemiDest;
import com.tsp.operation.model.beans.Ciudad;


public class CambioDestinoDAO extends  MainDAO {
    
    
    
    
    
    /** Creates a new instance of CambioDestinoDAO */
    public CambioDestinoDAO() {
        super("CambioDestinoDAO.xml");
    }
    public CambioDestinoDAO(String dataBaseName) {
        super("CambioDestinoDAO.xml", dataBaseName);
    }
    
    
    /**
     * Metodo para extraer los datos de la remesa
     * @autor mfontalvo
     * @param dstrct, distrito de la remesa
     * @param numrem, numero de la remesa
     * @throws Exception.
     * @return bean de Remesa2
     */
    public Remesa2 obtenerRemesa(String dstrct, String numrem) throws Exception{
        
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            qy = "SQL_DATOS_REMESA";
        Remesa2 remesa = null;
        try{
            ps = this.crearPreparedStatement( qy );
            ps.setString(1, dstrct);
            ps.setString(2, numrem);
            rs = ps.executeQuery();
            
            if (rs.next()){
                remesa = Remesa2.load(rs);
                
                remesa.setRemitentes2   ( this.obtenerRemitentes   (remesa) );
                remesa.setDestinatarios2( this.obtenerDestinatarios(remesa) );
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(qy);
        }
        return remesa;
    }
    
    
    /**
     * Metodo para extraer los origenes apartir de un cliente
     * @autor mfontalvo
     * @param dstrct, distrito de los estandares
     * @param cliente, codigo de cliente
     * @throws Exception.
     * @return bean de TreeMap
     */
    public TreeMap obtenerOrigenes(String dstrct, String cliente) throws Exception{
        
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            qy = "SQL_ORIGENES";
        TreeMap           dt = new TreeMap();
        try{
            ps = this.crearPreparedStatement( qy );
            ps.setString(1, dstrct);
            ps.setString(2, cliente);
            rs = ps.executeQuery();
            while (rs.next()){
                dt.put( rs.getString("descripcion"), rs.getString("codigo") );
            }
            dt.put("Seleccione un origen", "NINGUNO");
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(qy);
        }
        return dt;
    }
    
    
    /**
     * Metodo para extraer los destino apartir de un cliente
     * @autor mfontalvo
     * @param dstrct, distrito de los estandares
     * @param cliente, codigo de cliente
     * @param origen, codigo del origen
     * @throws Exception.
     * @return bean de TreeMap
     */
    public TreeMap obtenerDestinos(String dstrct, String cliente, String origen) throws Exception{
        
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            qy = "SQL_DESTINOS";
        TreeMap           dt = new TreeMap();
        try{
            ps = this.crearPreparedStatement( qy );
            ps.setString(1, dstrct);
            ps.setString(2, cliente);
            ps.setString(3, origen);
            rs = ps.executeQuery();
            while (rs.next()){
                dt.put( rs.getString("descripcion"), rs.getString("codigo") );
            }
            dt.put("Seleccione un destino", "NINGUNO");
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(qy);
        }
        return dt;
    }
    
    
    /**
     * Metodo para extraer los destino apartir de un cliente
     * @autor mfontalvo
     * @param dstrct, distrito de los estandares
     * @param cliente, codigo de cliente
     * @param origen, codigo del origen
     * @throws Exception.
     * @return bean de TreeMap
     */
    public TreeMap obtenerEstandares(String dstrct, String cliente, String origen, String destino) throws Exception{
        
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            qy = "SQL_ESTANDARES";
        TreeMap           dt = new TreeMap();
        try{
            ps = this.crearPreparedStatement( qy );
            ps.setString(1, dstrct);
            ps.setString(2, cliente);
            ps.setString(3, origen);
            ps.setString(4, destino);
            rs = ps.executeQuery();
            while (rs.next()){
                dt.put( rs.getString("descripcion"), rs.getString("codigo") );
            }
            dt.put("Seleccione un estandar", "NINGUNO");
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(qy);
        }
        return dt;
    }
    
    
    
    
    /**
     * Metodo para extraer las ciudades
     * @autor mfontalvo
     * @throws Exception.
     * @return bean de TreeMap
     */
    public TreeMap obtenerCiudades() throws Exception{
        
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            qy = "SQL_CIUDADES";
        TreeMap           dt = new TreeMap();
        try{
            ps = this.crearPreparedStatement( qy );
            rs = ps.executeQuery();
            while (rs.next()){
                dt.put( rs.getString("nomciu"), rs.getString("codciu") );
            }
            dt.put("Seleccione una ciudad", "NINGUNO");
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(qy);
        }
        return dt;
    }
    
    
    /**
     * Metodo para extraer los remitentes y destinatarios.
     * @autor mfontalvo
     * @param ciudad, codigo de la ciudad
     * @param cliente, clodigo del cliente
     * @param tipo, {RE,DE} remitentes o destinatarios
     * @throws Exception.
     * @return bean de TreeMap
     */
    public Vector obtenerREMIDEST(String ciudad, String cliente, String tipo) throws Exception{
        
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            qy = "SQL_REMIDES";
        Vector            dt = new Vector();
        try{
            ps = this.crearPreparedStatement( qy );
            ps.setString(1, cliente );
            ps.setString(2, tipo    );
            ps.setString(3, ciudad  );
            rs = ps.executeQuery();
            while (rs.next()){
                RemiDest d = new RemiDest();
                d.setCodigo   ( Util.coalesce(  rs.getString("codigo") ,"" ) );
                d.setNombre   ( Util.coalesce(  rs.getString("nombre") ,"" ) );
                d.setDireccion( Util.coalesce(  rs.getString("direccion") ,"" ) );
                d.setTipo     ( "" );
                dt.add(d);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(qy);
        }
        return dt;
    }
    
    /**
     * Metodo para extraer los datos de un estandar, en el formato de Remesa2
     * @autor mfontalvo
     * @param dstrct, distrito de la remesa
     * @param estandar, numero del estandar
     * @param cliente,l codigo del cliente
     * @throws Exception.
     * @return bean de Remesa2
     */
    public Remesa2 obtenerRemesaRemplazo(String dstrct, String estandar, String cliente) throws Exception{
        
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            qy = "SQL_DATOS_ESTANDAR";
        Remesa2           r  = null;
        try{
            ps = this.crearPreparedStatement( qy );
            ps.setString(1, dstrct   );
            ps.setString(2, estandar );
            ps.setString(3, cliente  );
            rs = ps.executeQuery();
            if (rs.next()){
                r = new Remesa2();
                r.setCia         ( Util.coalesce( rs.getString("dstrct_code")       ,"" ) );
                r.setStd_job_no  ( Util.coalesce( rs.getString("std_job_no")        ,"" ) );
                r.setStd_job_desc( Util.coalesce( rs.getString("std_job_desc")      ,"" ) );
                r.setDescripcion( Util.coalesce( rs.getString("std_job_desc")      ,"" ) );
                r.setOrirem      ( Util.coalesce( rs.getString("origin_code")       ,"" ) );
                r.setDesrem      ( Util.coalesce( rs.getString("destination_code")  ,"" ) );
                r.setNomori      ( Util.coalesce( rs.getString("origin_name")       ,"" ) );
                r.setNomdes      ( Util.coalesce( rs.getString("destination_name")  ,"" ) );
                r.setPesoreal    ( rs.getDouble("units_required") );
                r.setTarifa      ( rs.getDouble("vlr_freight") );
                r.setQty_value   ( rs.getDouble("vlr_freight") );
                r.setCod_unidad  ( Util.coalesce( rs.getString("cod_unidad")        ,"" ) );
                r.setDesc_unidad( Util.coalesce( rs.getString("desc_unidad")       ,"" ) );
                r.setCurrency    ( Util.coalesce( rs.getString("currency")          ,"" ) );
                r.setUnidcam     ( Util.coalesce( rs.getString("currency")          ,"" ) );
                r.setVlrrem      ( rs.getDouble("vlrrem") );
                r.setVlrrem2     ( 0 );
                r.setTipoviaje   ( Util.coalesce( rs.getString("wo_type")           ,"" ) );
                r.setUnit_of_work( Util.coalesce( rs.getString("unit_of_work")      ,"" ) );
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(qy);
        }
        return r;
    }
    
    
    
    
    
    
    
    /**
     * FUNCIONES DE CONSECUTIVO DEL COMPROBANTE
     */
    
    
    
    /**
     * Metodo para obtener el proximo numero deL movimiento de la remesa
     * @autor mfontalvo
     * @param con, conexion
     * @throws Exception.
     * @return int, proximo numero de comprobante
     */
    private int nextID(Connection con) throws Exception {
        PreparedStatement ps   = null;
        ResultSet         rs   = null;
        int               next = -1;
        String query = "SQL_NEXT_ID";
        try{
            if (con == null)
                throw new Exception("SIN CONEXION.");
            ps = con.prepareStatement( this.obtenerSQL(query));
            rs = ps.executeQuery();
            if (rs.next()){
                next = rs.getInt("last_value");
            }
            
            return next;
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally{
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
        }
    }
    
    
    /**
     * Metodo para obtener el ultimo numero deL movimiento de la remesa
     * @autor mfontalvo
     * @param con, conexion
     * @throws Exception.
     * @return int, numero del ultimo comprobante
     */
    private int currentID(Connection con) throws Exception {
        PreparedStatement ps   = null;
        ResultSet         rs   = null;
        int               current = -1;
        String query = "SQL_CURRENT_ID";
        try{
            if (con==null)
                throw new Exception("SIN CONEXION.");
            
            ps = con.prepareStatement( this.obtenerSQL(query) );
            rs = ps.executeQuery();
            if (rs.next()){
                current = rs.getInt("current_value");
            }
            return current;
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally{
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
        }
    }
    
    
    /**
     * Metodo para modificar el conscutivo deL movimiento de la remesa
     * @autor mfontalvo
     * @param value, consecutivo del comprobante
     * @param con, conexion
     * @throws Exception.
     */
    private void setID(int value, Connection con ) throws Exception {
        PreparedStatement ps   = null;
        String query = "SQL_SET_ID";
        try{
            if ( con == null )
                throw new Exception("SIN CONEXION");
            
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, value );
            ps.executeQuery();
            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally{
            if (ps!=null) ps.close();
        }
    }
    
    
    
    
    /**
     * Metodo que devuelve la consulta sql para insertar un comprobante
     * @autor mfontalvo
     * @throws: En caso de que un error de base de datos ocurra.
     * @return Un String con el sql
     */
    public String getInsertMovrem(Connection con, Remesa2 r, int id) throws Exception {
        if (r==null)
            throw new Exception("MOVIMIENTO NO VALIDO.");
        
        PreparedStatement st = null;
        String result = "";
        try {
            if (con == null)
                throw new Exception("SIN CONEXION.");
            st = con.prepareStatement( this.obtenerSQL("SQL_INSERT_MOVREM" ) );
            st.setInt    (1  , id                  );
            st.setString(2  , ""                  );
            st.setString(3  , r.getCia()          );
            st.setString(4  , r.getNumrem()       );
            st.setString(5  , r.getConcepto()     );
            // valor ajuste
            st.setDouble(6  , r.getVlrrem_aj()    );
            st.setDouble(7  , r.getVlrrem2_aj()   );
            st.setDouble(8  , r.getPesoreal_aj()  );
            st.setDouble(9  , r.getQty_value_aj() );
            st.setDouble(10 , r.getTasa_aj()      );
            // valores que provocan el ajuste
            st.setDouble(11 , r.getVlrrem()       );
            st.setDouble(12 , r.getVlrrem2()      );
            st.setDouble(13 , r.getPesoreal()     );
            st.setDouble(14 , r.getQty_value()    );
            st.setDouble(15 , r.getTasa()         );
            // otros datos
            st.setString(16, r.getAgcrem()        );
            st.setString(17, r.getCreation_date() );
            st.setString(18, r.getUsuario()       );
            st.setString(19, r.getBase()          );
            result = st.toString();
        }catch( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }finally{
            if ( st != null ) st.close();
        }
        return result;
    }
    
    
   public String getRegistrosAuditoria(Connection con, Remesa2 ro, Remesa2 ra) throws Exception {
        
        Auditoria aud = new Auditoria();
        aud.setDstrct        ( ra.getCia()           );
        aud.setTabla         ( "remesa"              );
        aud.setLlave         ( "numrem"              );
        aud.setCampollave    ( ra.getNumrem()        );
        aud.setCreation_date( ra.getCreation_date() );
        aud.setCreation_user( ra.getUsuario()       );
        aud.setBase          ( ra.getBase()          );
        
        
        String sql = "";
        if ( !ro.getStd_job_no().equals( ra.getStd_job_no() ) ){
            aud.setCampo        ( "std_job_no");
            aud.setCont_original( ro.getStd_job_no() );
            aud.setCont_nuevo   ( ra.getStd_job_no() );
            aud.setFormato      ( "varchar (10)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        if ( !ro.getDescripcion().equals( ra.getDescripcion() ) ){
            aud.setCampo        ( "descripcion");
            aud.setCont_original( ro.getDescripcion() );
            aud.setCont_nuevo   ( ra.getDescripcion() );
            aud.setFormato      ( "varchar (50)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        if ( !ro.getOrirem().equals( ra.getOrirem() ) ){
            aud.setCampo        ( "orirem");
            aud.setCont_original( ro.getOrirem() );
            aud.setCont_nuevo   ( ra.getOrirem() );
            aud.setFormato      ( "varchar (20)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        if ( !ro.getDesrem().equals( ra.getDesrem() ) ){
            aud.setCampo        ( "desrem");
            aud.setCont_original( ro.getDesrem() );
            aud.setCont_nuevo   ( ra.getDesrem() );
            aud.setFormato      ( "varchar (20)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        if ( !ro.getTipoviaje().equals( ra.getTipoviaje() ) ){
            aud.setCampo        ( "tipoviaje");
            aud.setCont_original( ro.getTipoviaje() );
            aud.setCont_nuevo   ( ra.getTipoviaje() );
            aud.setFormato      ( "varchar (2)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        if ( !ro.getUnit_of_work().equals( ra.getUnit_of_work() ) ){
            aud.setCampo        ( "unit_of_work");
            aud.setCont_original( ro.getUnit_of_work() );
            aud.setCont_nuevo   ( ra.getUnit_of_work() );
            aud.setFormato      ( "varchar (4)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        if ( !ro.getN_facturable().equals( ra.getN_facturable() ) ){
            aud.setCampo        ( "n_facturable");
            aud.setCont_original( ro.getN_facturable() );
            aud.setCont_nuevo   ( ra.getN_facturable() );
            aud.setFormato      ( "varchar (1)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        if ( !ro.getCrossdocking().equals( ra.getCrossdocking() ) ){
            aud.setCampo        ( "crossdocking");
            aud.setCont_original( ro.getCrossdocking() );
            aud.setCont_nuevo   ( ra.getCrossdocking() );
            aud.setFormato      ( "varchar (1)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        if ( !ro.getCadena().equals( ra.getCadena() ) ){
            aud.setCampo        ( "cadena");
            aud.setCont_original( ro.getCadena() );
            aud.setCont_nuevo   ( ra.getCadena() );
            aud.setFormato      ( "varchar (1)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        
        if ( !ro.getUnidcam().equals( ra.getUnidcam() ) ){
            aud.setCampo        ( "unidcam");
            aud.setCont_original( ro.getUnidcam() );
            aud.setCont_nuevo   ( ra.getUnidcam() );
            aud.setFormato      ( "varchar (3)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        
        if ( !ro.getCurrency().equals( ra.getCurrency() ) ){
            aud.setCampo        ( "currency");
            aud.setCont_original( ro.getCurrency() );
            aud.setCont_nuevo   ( ra.getCurrency() );
            aud.setFormato      ( "varchar (3)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        
        
        if ( ro.getVlrrem() != ra.getVlrrem() ){
            aud.setCampo        ( "vlrrem");
            aud.setCont_original( Util.customFormat("0.00", ro.getVlrrem()) );
            aud.setCont_nuevo   ( Util.customFormat("0.00", ra.getVlrrem()) );
            aud.setFormato      ( "numeric (15,2)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        
        if ( ro.getVlrrem2() != ra.getVlrrem2() ){
            aud.setCampo        ( "vlrrem2");
            aud.setCont_original( Util.customFormat("0.00", ro.getVlrrem2()) );
            aud.setCont_nuevo   ( Util.customFormat("0.00", ra.getVlrrem2()) );
            aud.setFormato      ( "numeric (15,2)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        
        if ( ro.getPesoreal() != ra.getPesoreal() ){
            aud.setCampo        ( "pesoreal");
            aud.setCont_original( Util.customFormat("0.000", ro.getPesoreal()) );
            aud.setCont_nuevo   ( Util.customFormat("0.000", ra.getPesoreal()) );
            aud.setFormato      ( "numeric (10,3)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        
        if ( ro.getQty_value() != ra.getQty_value() ){
            aud.setCampo        ( "qty_value");
            aud.setCont_original( Util.customFormat("0.000", ro.getQty_value()) );
            aud.setCont_nuevo   ( Util.customFormat("0.000", ra.getQty_value()) );
            aud.setFormato      ( "numeric (10,3)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        
        if ( !ro.getRemitente().equals(ra.getRemitente()) ){
            aud.setCampo        ( "remitente");
            aud.setCont_original( ro.getRemitente() );
            aud.setCont_nuevo   ( ra.getRemitente() );
            aud.setFormato      ( "varchar (40)" );
            sql += this.getInsertAuditoria(con, aud );
        }
        
        if ( !ro.getDestinatario().equals(ra.getDestinatario()) ){
            aud.setCampo        ( "destinatario");
            aud.setCont_original( ro.getDestinatario() );
            aud.setCont_nuevo   ( ra.getDestinatario() );
            aud.setFormato      ( "text" );
            sql += this.getInsertAuditoria(con, aud );
        }      
        return sql;
        
    }
    
    
    
    /**
     * Metodo que devuelve la consulta sql para insertar un registro de auditoria
     * @autor mfontalvo
     * @throws: En caso de que un error de base de datos ocurra.
     * @return Un String con el sql
     */
    public String getInsertAuditoria(Connection con, Auditoria a) throws Exception {
        if(a==null) {
            System.out.println(a);
            return "";
        }
        
        PreparedStatement st = null;
        String result = "";
        try {
            if (con == null)
                throw new Exception("SIN CONEXION.");
            st = con.prepareStatement( this.obtenerSQL("SQL_INSERT_AUDITORIA" ) );
            st.setString(1  , a.getDstrct()        );
            st.setString(2  , a.getTabla()         );
            st.setString(3  , a.getLlave()         );
            st.setString(4  , a.getCampollave()    );
            st.setString(5  , a.getCampo()         );
            st.setString(6  , a.getCont_original() );
            st.setString(7  , a.getCont_nuevo()    );
            st.setString(8  , a.getFormato()       );
            st.setString(9  , a.getCreation_date() );
            st.setString(10 , a.getCreation_user() );
            st.setString(11 , a.getCreation_date() );
            st.setString(12 , a.getCreation_user() );
            st.setString(13 , a.getBase()          );
            result = st.toString();
        }catch( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }finally{
            if ( st != null ) st.close();
        }
        return result;
    }
    
    
    /**
     * Metodo que para actualizar los datos de la remesa
     * @autor mfontalvo
     * @throws: En caso de que un error de base de datos ocurra.
     * @return Un String con el sql
     */
    public String getUpdateRemesa(Connection con, Remesa2 r) throws Exception {
        if (r==null)
            throw new Exception("MOVIMIENTO NO VALIDO.");
        
        PreparedStatement st = null;
        String result = "";
        try {
            if (con == null)
                throw new Exception("SIN CONEXION.");
            
            st = con.prepareStatement( this.obtenerSQL("SQL_UPDATE_REMESA" ) );
            st.setString(1  , r.getStd_job_no()    );
            st.setString(2  , r.getStd_job_desc()  );
            st.setString(3  , r.getOrirem()        );
            st.setString(4  , r.getDesrem()        );
            st.setString(5  , r.getTipoviaje()     );
            st.setString(6  , r.getUnit_of_work()  );
            st.setString(7  , r.getUnidcam()       );
            st.setString(8  , r.getCurrency()      );
            st.setString(9  , r.getCrossdocking()  );
            st.setString(10 , r.getCadena()        );
            st.setString(11 , r.getN_facturable()  );
            st.setString(12 , r.getCreation_date() );
            st.setString(13 , r.getRemitente()     );
            st.setString(14 , r.getDestinatario()  );
            st.setString(15 , r.getCia()           );
            st.setString(16 , r.getNumrem()        );
            result = st.toString();
        }catch( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }finally{
            if ( st != null ) st.close();
        }
        return result;
    }
    
    
    
    /**
     * Metodo que para actualizar los datos de la remesa
     * @autor mfontalvo
     * @throws: En caso de que un error de base de datos ocurra.
     * @return Un String con el sql
     */
    public String getUpdateRemesaNoContabilizada(Connection con, Remesa2 r) throws Exception {
        if (r==null)
            throw new Exception("MOVIMIENTO NO VALIDO.");
        
        PreparedStatement st = null;
        String result = "";
        try {
            if (con == null)
                throw new Exception("SIN CONEXION.");
            st = con.prepareStatement( this.obtenerSQL("SQL_UPDATE_REMESA_NO_CONTABILIZADA" ) );
            st.setString(1  , r.getStd_job_no()    );
            st.setString(2  , r.getStd_job_desc()  );
            st.setString(3  , r.getOrirem()        );
            st.setString(4  , r.getDesrem()        );
            st.setString(5  , r.getTipoviaje()     );
            st.setString(6  , r.getUnit_of_work()  );
            st.setString(7  , r.getUnidcam()       );
            st.setString(8  , r.getCurrency()      );
            st.setString(9  , r.getCrossdocking()  );
            st.setString(10 , r.getCadena()        );
            st.setString(11 , r.getN_facturable()  );
            st.setString(12 , r.getCreation_date() );
            st.setDouble(13 , r.getVlrrem()        );
            st.setDouble(14 , r.getVlrrem2()       );
            st.setDouble(15 , r.getPesoreal()      );
            st.setDouble(16 , r.getQty_value()     );
            st.setString(17 , r.getRemitente()     );
            st.setString(18 , r.getDestinatario()  );
            st.setString(19 , r.getCia()           );
            st.setString(20 , r.getNumrem()        );
            result = st.toString();
        }catch( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }finally{
            if ( st != null ) st.close();
        }
        return result;
    }
    
    
    private void setearREMIDEST_toString (Remesa2 rd, String tipo){
        
        String result = "";
        TreeMap ciudades = ( tipo.equals("RE")? rd.getRemitentes2():rd.getDestinatarios2()  );
        if (ciudades!=null){
            Iterator it = ciudades.values().iterator();
            while( it.hasNext() ){
                Ciudad c = (Ciudad) it.next();
                Vector d = c.getDetalle();
                for (int i = 0; d!=null && i < d.size(); i++){
                    RemiDest r = (RemiDest) d.get(i);
                    result += ( result.equals("")? "":"," ) + r.getCodigo();
                }
            }
        }
        
        if ( tipo.equals("RE") )
            rd.setRemitente( result );
        else
            rd.setDestinatario ( result );
    }
    
    
    /**
     * FUNCIONES DE GRABACION DE UN COMPROBANTE CONTABLE.
     */
    
    /**
     * Metodo para guardar el cambio de destino de la remesa.
     * @autor mfontalvo
     * @param ro, remesa original
     * @param ra, remesa actual
     * @throws Exception.
     */
    public void saveCambioDestino(Remesa2 ro, Remesa2 ra) throws Exception {
        
        if (ro==null && ra!=null)
            throw new Exception("NO SE PUEDE REALIZAR EL CAMBIO DE DESTINO PARAMETROS NO VALIDOS.");
        
        
        Connection  con   = null;
        Statement   st    = null;
        String      query = "SQL_INSERT_MOVREM";
        try{
            
            con = this.conectar(query);
            if (con == null)
                throw new Exception("SIN CONEXION");
            
            
            
            // numero de la transaccion
            int next    = this.nextID(con);
            int current = next-1;
            if (next==-1) throw new Exception("NO SE PUDO OBTENER EL GRUPO DE LA TRANSACCION.");
            
            try{
                String sql = "";
                
                
                setearREMIDEST_toString(ra,"RE");                
                setearREMIDEST_toString(ra,"DE");
                
                
                
                if ( !ro.getTransaccion().equals("0") ){
                    sql += this.getInsertMovrem  (con, ra, next);
                    sql += this.getUpdateRemesa  (con, ra);
                } else
                    sql += this.getUpdateRemesaNoContabilizada(con, ra);
                
                sql += this.getDeleteREMIDEST(con, ra);
                sql += this.getRemitentes_y_Destinatarios(con, ra);
                sql += this.getRegistrosAuditoria(con, ro, ra);
                
                
                if (sql.trim().equals(""))
                    throw new Exception("NO SE PUDO GENERAR LOS SQL PARA EL PROCESO.");
                
                st = con.createStatement();
                st.addBatch(sql);
                st.executeBatch();
                
            } catch (Exception e){
                e.printStackTrace();
                
                // restauramos la transaction
                int currentA = this.currentID(con);
                if (currentA == next ) {
                    this.setID(current, con);
                }
                throw new Exception(e);
            }
            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally {
            if (st!=null) st.close();
            this.desconectar(query);
        }
    }
    
    
    
    /**
     * Metodo que devuelve la consulta sql para insertar registro en remesadest
     * @autor mfontalvo
     * @throws: En caso de que un error de base de datos ocurra.
     * @return Un String con el sql
     */
    public String getInsertREMIDEST(Connection con, RemiDest r, Remesa2 rem, String tipo) throws Exception {
        if (r==null)
            throw new Exception("OBJETO NO NO VALIDO.");
        
        PreparedStatement st = null;
        String result = "";
        try {
            if (con == null)
                throw new Exception("SIN CONEXION.");
            st = con.prepareStatement( this.obtenerSQL("SQL_INSERT_REMESADEST" ) );
            st.setString(1 , ""                     );
            st.setString(2 , rem.getCia()           );
            st.setString(3 , r.getCodigo()          );
            st.setString(4 , rem.getNumrem()        );
            st.setString(5 , tipo                   );
            st.setString(6 , rem.getCreation_date() );
            st.setString(7 , rem.getUsuario()       );
            st.setString(8 , rem.getCreation_date() );
            st.setString(9 , rem.getUsuario()       );
            st.setString(10, rem.getBase()          );
            result = st.toString();
        }catch( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }finally{
            if ( st != null ) st.close();
        }
        return result;
    }
    
    
    
    
    /**
     * Metodo que devuelve la consulta sql para eliminar los remitentes y des-
     * tinatarios de la tabla remesadest
     * @autor mfontalvo
     * @throws: En caso de que un error de base de datos ocurra.
     * @return Un String con el sql
     */
    public String getDeleteREMIDEST(Connection con, Remesa2 rem) throws Exception {
        if (rem==null)
            throw new Exception("OBJETO NO NO VALIDO.");
        
        PreparedStatement st = null;
        String result = "";
        try {
            if (con == null)
                throw new Exception("SIN CONEXION.");
            st = con.prepareStatement( this.obtenerSQL("SQL_DELETE_REMESADEST" ) );
            st.setString(1 , rem.getCia()    );
            st.setString(2 , rem.getNumrem() );
            result = st.toString();
        }catch( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }finally{
            if ( st != null ) st.close();
        }
        return result;
    }
    
    
    public String getRemitentes_y_Destinatarios(Connection con, Remesa2 r) throws Exception{
        
        String sql = "";
        
        TreeMap ciudades = r.getRemitentes2();
        Iterator it = ciudades.values().iterator();
        while (it.hasNext()){
            Ciudad c = (Ciudad) it.next();
            if ( c.getDetalle()!=null && !c.getDetalle().isEmpty() ){
                for (int i = 0; i<c.getDetalle().size(); i++){
                    RemiDest rd = (RemiDest) c.getDetalle().get(i);
                    sql += this.getInsertREMIDEST(con, rd, r, "RE");
                }
            }
        }
        
        ciudades = r.getDestinatarios2();
        it = ciudades.values().iterator();
        while (it.hasNext()){
            Ciudad c = (Ciudad) it.next();
            if ( c.getDetalle()!=null && !c.getDetalle().isEmpty() ){
                for (int i = 0; i<c.getDetalle().size(); i++){
                    RemiDest rd = (RemiDest) c.getDetalle().get(i);
                    sql += this.getInsertREMIDEST(con, rd, r, "DE");
                }
            }
        }
        
        return sql;
    }
    
    
    
    /**
     * Metodo que devuelve la consulta sql para eliminar los remitentes y des-
     * tinatarios de la tabla remesadest
     * @autor mfontalvo
     * @throws: En caso de que un error de base de datos ocurra.
     * @return Un String con el sql
     */
    public TreeMap obtenerRemitentes(Remesa2 rem) throws Exception {
        if (rem==null)
            throw new Exception("OBJETO NO NO VALIDO.");
        
        PreparedStatement st = null;
        ResultSet         rs = null;
        TreeMap           datos = new TreeMap();
        String query  = "SQL_SELECT_REMESADEST";
        try {
            st = this.crearPreparedStatement( query );
            st.setString(1 , rem.getCia()    );
            st.setString(2 , rem.getNumrem() );
            st.setString(3 , "RE"            );
            rs = st.executeQuery();
            while ( rs.next() ){
                
                Ciudad c = (Ciudad) datos.get( rs.getString("ciudad") );
                if (c==null){
                    c = new Ciudad();
                    c.setCodigo( rs.getString("ciudad") );
                    c.setDetalle( new Vector() );
                }
                
                
                RemiDest rd =  new RemiDest();
                rd.setCodigo   ( Util.coalesce( rs.getString("codigo")    ,"" ) );
                rd.setNombre   ( Util.coalesce( rs.getString("nombre")    ,"" ) );
                rd.setDireccion( Util.coalesce( rs.getString("direccion") ,"" ) );
                rd.setTipo     ( "" );
                c.getDetalle().add(rd);
                datos.put   (c.getCodigo(), c);
            }
        }catch( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }finally{
            if ( st != null ) st.close();
        }
        return datos;
    }
    
    
    /**
     * Metodo que devuelve la consulta sql para eliminar los remitentes y des-
     * tinatarios de la tabla remesadest
     * @autor mfontalvo
     * @throws: En caso de que un error de base de datos ocurra.
     * @return Un String con el sql
     */
    public TreeMap obtenerDestinatarios(Remesa2 rem) throws Exception {
        if (rem==null)
            throw new Exception("OBJETO NO NO VALIDO.");
        
        PreparedStatement st = null;
        ResultSet         rs = null;
        TreeMap           datos = new TreeMap();
        String query  = "SQL_SELECT_REMESADEST";
        try {
            st = this.crearPreparedStatement( query );
            st.setString(1 , rem.getCia()    );
            st.setString(2 , rem.getNumrem() );
            st.setString(3 , "DE"            );
            rs = st.executeQuery();
            while ( rs.next() ){
                
                Ciudad c = (Ciudad) datos.get( rs.getString("ciudad") );
                if (c==null){
                    c = new Ciudad();
                    c.setCodigo( rs.getString("ciudad") );
                    c.setDetalle( new Vector() );
                    datos.put   (c.getCodigo(), c);
                }
                
                
                RemiDest rd =  new RemiDest();
                rd.setCodigo   ( Util.coalesce( rs.getString("codigo")    ,"" ) );
                rd.setNombre   ( Util.coalesce( rs.getString("nombre")    ,"" ) );
                rd.setDireccion( Util.coalesce( rs.getString("direccion") ,"" ) );
                rd.setTipo     ( "" );
                c.getDetalle().add(rd);
            }
        }catch( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }finally{
            if ( st != null ) st.close();
        }
        return datos;
    }
    
    
    //Kreales 28-04-07
     /**
     * Metodo para actualizar el valor de la remesa
     * @autor kreales
     * @param ro, remesa original
     * @param ra, remesa actual
     * @throws Exception.
     */
    public String actualizarValor(Remesa2 ro) throws SQLException {

        if (ro==null)
            throw new SQLException("NO SE PUEDE REALIZAR EL CAMBIO DE DESTINO PARAMETROS NO VALIDOS.");
        
        String sql = "";
        Connection  con   = null;
        Statement   st    = null;
        String      query = "SQL_INSERT_MOVREM";            
        try{
            
            con = this.conectar(query);
            if (con == null)
                throw new Exception ("SIN CONEXION");
            
            
            
            // numero de la transaccion
            int next    = this.nextID(con);
            int current = next-1;            
            if (next==-1) throw new Exception ("NO SE PUDO OBTENER EL GRUPO DE LA TRANSACCION.");            
            
            try{
                
                
                sql += this.getInsertMovrem(con, ro, next);
              //  sql += this.getMarcaRemesa(con, ro.getNumrem());
               // sql += this.getAuditoriaModifRemesa(con, ro);
                
                
                if (sql.trim().equals(""))
                    throw new Exception("NO SE PUDO GENERAR LOS SQL PARA EL PROCESO.");
                
                
                
            } catch (Exception e){
                e.printStackTrace();
                
                // restauramos la transaction
                int currentA = this.currentID(con);
                if (currentA == next ) {
                    this.setID(current, con);
                }
                throw new Exception (e);
            } 
           
        } catch (Exception ex){
            ex.printStackTrace();
            throw new SQLException(ex.getMessage());
        } 
        finally {
            if (st!=null) st.close();
            this.desconectar(query);
        }
        return sql;
    }    
    
    public String getAuditoriaModifRemesa(Connection con, Remesa2 ro) throws Exception {
        
        String sql = "";
        Auditoria aud = new Auditoria();
        aud.setDstrct        ( ro.getCia()           );
        aud.setTabla         ( "remesa"              );
        aud.setLlave         ( "numrem"              );
        aud.setCampollave    ( ro.getNumrem()        );
        aud.setCreation_date ( "now()");
        aud.setCreation_user ( ro.getUsuario()       );
        aud.setBase          ( ro.getBase()          );
        aud.setCampo        ( "pesoreal");
        aud.setCont_original( ""+ro.getPesoreal());
        aud.setCont_nuevo   ( ""+(ro.getPesoreal()+ro.getPesoreal_aj()) );
        aud.setFormato      ( "numeric(10,3)" );
        sql += this.getInsertAuditoria(con, aud );
        
        return sql;
        
    }
     /**
     * Metodo que para actualizar los datos de la remesa
     * @autor mfontalvo
     * @throws: En caso de que un error de base de datos ocurra.
     * @return Un String con el sql
     */
    public String getMarcaRemesa(Connection con, String numrem) throws Exception {
        
        PreparedStatement st = null;
        String result = "";
        try {
            if (con == null)
                throw new Exception("SIN CONEXION.");
            st = con.prepareStatement( this.obtenerSQL("SQL_MARCAR_REMESA" ) );
            st.setString (1  , numrem    );
            result = st.toString();      
        }catch( Exception e ){            
            e.printStackTrace();
            throw new Exception( e.getMessage() );            
        }finally{            
            if ( st != null ) st.close();                    
        }
        return result;
    }    
    
    
}


