/********************************************************************
 *      Nombre Clase.................   Perfil_vistaDAO.java
 *      Descripci�n..................   DAO de la tabla perfil_vista
 *      Autor........................   Rodrigo Salazar
 *      Fecha........................   13.07.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;


public class Perfil_vistaDAO {
    private Perfil_vista perfil_vista;
    private Vector perfil_vistas;
    private String tipo;
    
    /** Creates a new instance of Perfil_vistaDAO */
    public Perfil_vistaDAO() {
    }
    
    /**
     * Obtiene la propiedad tipo
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public String getTipo(){
        return this.tipo;
    }
    
    /**
     * Obtiene la propiedad perfil_vista
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Perfil_vista getPerfil_vista() {
        return perfil_vista;
    }
    
    /**
     * Establece la propiedad perfil_vista
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public void setPerfil_vista(Perfil_vista perfil_vista) {
        this.perfil_vista = perfil_vista;
    }
    
    /**
     * Obtiene la propiedad perfil_vistas
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Vector getPerfil_vistas() {
        return perfil_vistas;
    }
    
    /**
     * Establece la propiedad perfil_vistas
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public void setPerfil_vistas(Vector perfil_vistas) {
        this.perfil_vistas = perfil_vistas;
    }
    
    /**
     * Inserta un nuevo registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void insertPerfil_vista() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ////System.out.println("insertar1");
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            ////System.out.println("insertar2");
            if (con != null){
                st = con.prepareStatement("Insert into perfil_vista (perfil,pagina,campo,visible,editable,creation_user,creation_date,user_update,last_update,cia,rec_status) values(?,?,?,?,?,?,'now()',?,'now()',?,' ')");
                st.setString(1,perfil_vista.getPerfil());
                st.setString(2,perfil_vista.getPagina());
                st.setString(3,perfil_vista.getCampo());
                st.setString(4,perfil_vista.getVisible());
                st.setString(5,perfil_vista.getEditable());
                st.setString(6,perfil_vista.getCreation_user());
                st.setString(7,perfil_vista.getUser_update());
                st.setString(8,perfil_vista.getCia());
                ////System.out.println("insertar3"+perfil_vista.getPerfil()+" "+perfil_vista.getPagina()+" "+perfil_vista.getCampo()+" "+perfil_vista.getVisible()+" "+perfil_vista.getEditable());
                st.executeUpdate();
                ////System.out.println("insertar4");
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL PERFIL DE VISTA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Obtiene un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param cod C�digo del perfil-vista
     * @throws SQLException
     * @version 1.0
     */
    public void searchPerfil_vista(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select distinct perfil from perfil_vista where perfil like ? and rec_status != 'A'");
                st.setString(1,cod);
                rs= st.executeQuery();
                perfil_vistas = new Vector();
                while(rs.next()){
                    perfil_vista = new Perfil_vista();
                    perfil_vista.setPerfil(rs.getString("perfil"));
                    perfil_vistas.add(perfil_vista);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL PERFIL DE VISTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Verifica la existencia de un nuevo registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param perfil Nombre del perfil
     * @throws SQLException
     * @version 1.0
     */
    public boolean existPerfil_vista(String perfil) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select * from perfil_vista where perfil = ? and rec_status != 'A'");
                st.setString(1,perfil);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PERFIL" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Obtiene todos los registros no anulados de la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void listPerfil_vista()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from perfil_vista where rec_status != 'A' order by perfil");
                rs= st.executeQuery();
                perfil_vistas = new Vector();
                while(rs.next()){
                    perfil_vista = new Perfil_vista();
                    perfil_vista.setPerfil(rs.getString("perfil"));
                    perfil_vistas.add(perfil_vista);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Actualiza un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param perfil Nombre del perfil
     * @param pagina C�digo de la p�gina JSP
     * @param campo Nombre del campo
     * @param visible Propiedad de visibilidad del campo
     * @param editable Propiedad de edici�n del campo
     * @param usu Login del usuario
     * @throws SQLException
     * @version 1.0
     */
    public void updatePerfil_vista( String perfil,String pagina,String campo,String visible, String editable, String usu) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update perfil_vista set visible=?, editable=?, last_update='now()', user_update=?, rec_status=' ' where perfil = ? and pagina = ? and campo = ?");
                st.setString(1,visible);
                st.setString(2,editable);
                st.setString(3,usu);
                st.setString(4,perfil);
                st.setString(5,pagina);
                st.setString(6,campo);
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DEL PERFIL DE VISTA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Anula un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void anularPerfil_vista() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update perfil_vista set rec_status='A',last_update='now()', user_update=? where perfil = ? and pagina = ? and campo = ?");
                st.setString(1,perfil_vista.getUser_update());
                st.setString(2,perfil_vista.getPerfil());
                st.setString(3,perfil_vista.getPagina());
                st.setString(4,perfil_vista.getCampo());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL PERFL DE VISTA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Obtiene un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param perfil Nombre del perfil
     * @param pagina C�digo de la p�gina
     * @throws SQLException
     * @version 1.0
     */
    public Vector searchDetallePerfil_vistas(String perfil, String pagina) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        perfil_vistas=null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                
                st = con.prepareStatement("Select * from perfil_vista where perfil like ? and pagina like ? and rec_status != 'A'");
                st.setString(1, perfil+"%");
                st.setString(2, pagina+"%");
                
                ////System.out.println(st.toString());
                
                rs = st.executeQuery();
                perfil_vistas = new Vector();
                while(rs.next()){
                    perfil_vista = new Perfil_vista();
                    perfil_vista.setPerfil(rs.getString("perfil"));
                    perfil_vista.setPagina(rs.getString("pagina"));
                    perfil_vista.setCampo(rs.getString("campo"));
                    perfil_vista.setVisible(rs.getString("visible"));
                    perfil_vista.setEditable(rs.getString("editable"));
                    perfil_vista.setRec_status(rs.getString("rec_status"));
                    perfil_vista.setUser_update(rs.getString("user_update"));
                    perfil_vista.setCreation_user(rs.getString("creation_user"));
                    perfil_vistas.add(perfil_vista);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE CONTRATOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return perfil_vistas;
    }
    
    /**
     * Obtiene todos los registros no anulados de la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public Vector listarPerfil_vistas()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        perfil_vistas = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select distinct perfil from perfil_vista where rec_status!='A' order by perfil");
                
                rs = st.executeQuery();
                perfil_vistas = new Vector();
                
                while(rs.next()){
                    perfil_vista = new Perfil_vista();
                    perfil_vista.setPerfil(rs.getString("perfil"));
                    perfil_vistas.add(perfil_vista);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE CONTRATO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return perfil_vistas;
    }
    
    /**
     * Obtiene un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param perfil Nombre del perfil
     * @param pagina C�digo de la p�gina
     * @param campo Nombre del campo
     * @throws SQLException
     * @version 1.0
     */
    public Perfil_vista buscarPerfil_vista(String perfil, String pagina, String campo) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        perfil_vistas = null;
        PoolManager poolManager = null;
        ////System.out.println("dao1"+perfil+" "+pagina+" "+campo);
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select * from perfil_vista where perfil=? and pagina=? and campo=? ");
                st.setString(1,perfil);
                st.setString(2,pagina);
                st.setString(3,campo);
                
                rs = st.executeQuery();
                perfil_vistas = new Vector();
                perfil_vista = new Perfil_vista();
                
                if(rs.next()){
                    perfil_vista.setPerfil(rs.getString("perfil"));
                    perfil_vista.setPagina(rs.getString("pagina"));
                    perfil_vista.setCampo(rs.getString("campo"));
                    perfil_vista.setVisible(rs.getString("visible"));
                    perfil_vista.setEditable(rs.getString("editable"));
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE CONTRATO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        ////System.out.println("dao2");
        
        return perfil_vista;
    }
    
    /**
     * Elimina un registro en la tabla perfil_vista
     * @autor Rodrigo Salazar
     * @param perfil Nombre del perfil
     * @param pagina C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public void eliminarPerfil_vista(String perfil, String pagina) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("delete from perfil_vista where perfil = ? and pagina = ? ");
                st.setString(1,perfil);
                st.setString(2,pagina);
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL PERFL DE VISTA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Consulta las restricciones y/o permisos de un usuario sobre 
     * una determinada p�gina JSP
     * @autor Rodrigo Salazar
     * @param login Login del usuario
     * @param pagina C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public Vector consultPerfil_vistas(String login, String pagina)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        perfil_vistas = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select distinct a.campo,a.visible,a.editable from perfil_vista a, jsp c, perfil_vista_usuario b where  a.perfil = b.perfil and b.usuario=? and a.pagina = c.codigo and c.nombre = ? ");
                st.setString(1,login);
                st.setString(2,pagina);
                
                rs = st.executeQuery();
                perfil_vistas = new Vector();
                
                while(rs.next()){
                    perfil_vista = new Perfil_vista();
                    perfil_vista.setCampo(rs.getString("campo"));
                    perfil_vista.setEditable(rs.getString("editable"));
                    perfil_vista.setVisible(rs.getString("visible"));
                    //perfil_vista.setPagina(rs.getString("pagina"));//tamatu 16.11.2005
                    perfil_vistas.add(perfil_vista);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE CONTRATO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return perfil_vistas;
    }
    
    /**
     * Consulta las p�ginas asociadas a un determinado perfil
     * @autor Tito Andr�s Maturana
     * @param login Nombre del perfil
     * @throws SQLException
     * @version 1.0
     */
    public Vector consultarPerfil(String login)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        perfil_vistas = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("SELECT DISTINCT perfil, PAGINA FROM PERFIL_VISTA WHERE perfil=?");
                st.setString(1,login);
                
                rs = st.executeQuery();
                perfil_vistas = new Vector();
                
                while(rs.next()){
                    perfil_vista = new Perfil_vista();
                    perfil_vista.setPagina(rs.getString("pagina"));
                    perfil_vista.setPerfil(rs.getString("perfil"));
                    perfil_vistas.add(perfil_vista);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA POR PERFIL: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return perfil_vistas;
    }
    
    /**
     * Consulta los perfiles asociados a una determinada p�gina
     * @autor Tito Andr�s Maturan
     * @param vista C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public Vector consultarVista(String vista)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        perfil_vistas = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("SELECT DISTINCT perfil, PAGINA FROM PERFIL_VISTA WHERE pagina=?");
                st.setString(1,vista);
                
                rs = st.executeQuery();
                perfil_vistas = new Vector();
                
                while(rs.next()){
                    perfil_vista = new Perfil_vista();
                    perfil_vista.setPagina(rs.getString("pagina"));
                    perfil_vista.setPerfil(rs.getString("perfil"));
                    perfil_vistas.add(perfil_vista);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA POR VISTA: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return perfil_vistas;
    }
    
    /**
     * Consulta la clasificaci�n de un determinado campo
     * @autor Rodrigo Salazar
     * @param campo Nombre del campo
     * @param pagina C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public void consultarTipo_campo(String campo,String pagina) throws SQLException{
        String codigo="";
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        perfil_vistas = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement("select codigo from jsp where nombre = ?");
                st.setString(1,pagina);
                rs = st.executeQuery();
                
                while(rs.next()){
                    codigo = rs.getString("codigo");
                }
            }
            if(con!=null){
                st = con.prepareStatement("select tipo_campo from campos_jsp where pagina = ? and campo = ?");
                st.setString(1,codigo);
                st.setString(2,campo);
                rs = st.executeQuery();
                
                while(rs.next()){
                    this.tipo = rs.getString("tipo_campo");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TIPO DE CAMPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
}
