
/***********************************************************************************
 * Nombre clase : ............... ReimpresionDAO.java                              *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Ivan Dario Gomez                            *
 * Fecha :....................... Created on 23 de febrero de 2007, 08:34 AM       *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import javax.swing.*;
import java.text.*;
import java.util.Date;

/**
 *
 * @author  Ivan DArio Gomez
 */
public class ReimpresionDAO extends MainDAO {
    
    /** Creates a new instance of ReimpresionDAO */
    public ReimpresionDAO() {
        super("ReimpresionDAO.xml");
    }
    public ReimpresionDAO(String dataBaseName) {
        super("ReimpresionDAO.xml", dataBaseName);
    }
    
    
     
     /**
     * Obtiene los cheques por un rango de cheques
     * @autor Ing. Ivan Gomez
     * @param String dstrct, String banco, String sucursal, String Cheque inicial, String Cheque final
     * @throws SQLException
     * @version 1.0
     */
    public List buscarCheques(String dstrct, String banco, String sucursal, String Cinicial, String Cfinal) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        List listacheques = new LinkedList();
        String query = "SQL_BUSCAR_CHEQUES";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, dstrct);
            ps.setString(2, banco);
            ps.setString(3, sucursal);
            ps.setString(4, Cinicial);
            ps.setString(5, Cfinal);
            rs = ps.executeQuery();
            
           
            while( rs.next() ){
                Egreso eg  = new Egreso();
                eg.setDstrct(rs.getString("dstrct"));
                eg.setBranch_code(rs.getString("branch_code")) ; 
                eg.setBank_account_no(rs.getString("bank_account_no"));
                eg.setDocument_no(rs.getString("document_no"));
                eg.setReimpresion((rs.getString("reimpresion").equals("S"))?true:false);
                
                listacheques.add(eg); 
            eg = null;//Liberar Espacio JJCastro
            }
            
            
        }} catch(SQLException e){
            throw new SQLException("ERROR EN SQL_BUSCAR_CHEQUES" + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listacheques;
    }
    
    
    /**
     * Metodo updateCheques, metodo para actualizar la reimpresion
     * @autor  Ing. Ivan Dario Gomez
     * @param  List chequesSelec,List chequesNoSelec
     * @version  1.0
     */
    public void updateCheques(List chequesSelec,List chequesNoSelec) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_UPDATE_REIMPRESION";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            if(chequesSelec != null){
                Iterator it = chequesSelec.iterator(); 
                while(it.hasNext()){
    		    Egreso eg = (Egreso) it.next();
                    st.setString(1,"S");
                    st.setString(2, eg.getDstrct()         );
                    st.setString(3, eg.getBranch_code()    );
                    st.setString(4, eg.getBank_account_no());
                    st.setString(5, eg.getDocument_no()    );
                    st.executeUpdate();
                }
            }
            
            if(chequesNoSelec != null){
                Iterator it = chequesNoSelec.iterator(); 
                while(it.hasNext()){
    		    Egreso eg = (Egreso) it.next();
                    st.setString(1,"N");
                    st.setString(2, eg.getDstrct()         );
                    st.setString(3, eg.getBranch_code()    );
                    st.setString(4, eg.getBank_account_no());
                    st.setString(5, eg.getDocument_no()    );
                    st.executeUpdate();
                }
            }
        }}catch(SQLException e){
            throw new SQLException("ERROR en SQL_UPDATE_REIMPRESION " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 

    
    
}
