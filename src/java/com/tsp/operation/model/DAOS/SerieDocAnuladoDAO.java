/*
 * SerieDocAnulado.java
 *
 * Created on 19 de marzo de 2007, 10:41 AM
 */
/*
* Nombre        SerieDocAnulado.java
* Descripci�n   Clase para el acceso a los datos del archivo de serie de documentos anulados
* Autor         Ing. Andr�s Maturana D.
* Fecha         19 de marzo de 2007
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import javax.swing.*;
import java.text.*;
import java.util.Date;
import org.apache.log4j.*;

/**
 *
 * @author  Andres
 */
public class SerieDocAnuladoDAO extends MainDAO{
    
    private SerieDocAnulado doc;
    private Vector documentos;
    
    private Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of SerieDocAnulado */
    public SerieDocAnuladoDAO() {
        super("DocumentoAnulado.xml");
    }
    public SerieDocAnuladoDAO(String dataBaseName) {
        super("DocumentoAnulado.xml", dataBaseName);
    }
    
    /**
     * Metodo de insercion
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param doc Documento anulado a insertar
     * @throws SQLException
     * @version 1.0
     */
    public void insertar() throws SQLException{
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INSERT";
        
        try{
            ps = this.crearPreparedStatement(query);
            ps.setString(1, doc.getDstrct());
            ps.setString(2, doc.getTipo_documento());
            ps.setString(3, doc.getNo_documento());
            ps.setString(4, doc.getReferencia_1().trim());
            ps.setString(5, doc.getReferencia_2().trim());
            ps.setString(6, doc.getReferencia_3().trim());
            ps.setString(7, doc.getCausa_anulacion());
            ps.setString(8, doc.getObservacion());
            ps.setString(9, doc.getCreation_user());
            ps.setString(10, doc.getCreation_user());
            ps.setString(11, doc.getBase());
            
            ps.executeUpdate();

        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertar " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (ps != null){ ps.close(); }
            this.desconectar(query);
        }
    }
    
    /**
     * Getter for property doc.
     * @return Value of property doc.
     */
    public com.tsp.operation.model.beans.SerieDocAnulado getDoc() {
        return doc;
    }
    
    /**
     * Setter for property doc.
     * @param doc New value of property doc.
     */
    public void setDoc(com.tsp.operation.model.beans.SerieDocAnulado doc) {
        this.doc = doc;
    }
    
    /**
     * Verifica la existencia de un registro
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param doc Documento anulado a insertar
     * @throws SQLException
     * @version 1.0
     */
    public String existe() throws SQLException{
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE";
        
        try{
            ps = this.crearPreparedStatement(query);
            ps.setString(1, doc.getDstrct());
            ps.setString(2, doc.getTipo_documento());
            ps.setString(3, doc.getNo_documento());
            ps.setString(4, doc.getReferencia_1());
            ps.setString(5, doc.getReferencia_2());
            ps.setString(6, doc.getReferencia_3());
            
            rs = ps.executeQuery();
            
            if( rs.next() ){
                return rs.getString("reg_status");
            }

        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE existe " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (ps != null){ ps.close(); }
            this.desconectar(query);
        }
        
        return null;
    }
    
    /**
     * Actualiza un registro
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param doc Documento anulado a insertar
     * @throws SQLException
     * @version 1.0
     */
    public void actualizar() throws SQLException{
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE";
        
        try{
            ps = this.crearPreparedStatement(query);
            ps.setString(1, doc.getReg_status());
            ps.setString(2, doc.getCausa_anulacion());
            ps.setString(3, doc.getObservacion());
            ps.setString(4, doc.getUser_update());
            ps.setString(5, doc.getDstrct());
            ps.setString(6, doc.getTipo_documento());
            ps.setString(7, doc.getNo_documento());
            ps.setString(8, doc.getReferencia_1());
            ps.setString(9, doc.getReferencia_2());
            ps.setString(10, doc.getReferencia_3());
            //logger.info("?update: " + ps);
            ps.executeUpdate();

        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE actualizar " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (ps != null){ ps.close(); }
            this.desconectar(query);
        }
    }
    
    /**
     * Busqueda con los filtros dados
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param fechai Fecha inicial
     * @param fechaf Fecha final
     * @param login Login del Usuario de Creaci�n del registro
     * @throws SQLException
     * @version 1.0
     */
    public void search(String fechai, String fechaf, String login) throws SQLException{
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_SEARCH";
        String sql = null;
        Connection con = null;
        try{
            
            con = this.conectar(query);
            
            sql = this.obtenerSQL(query);
            if( fechai.length()!=0 && fechaf.length()!=0 )
                sql = sql.replaceAll("#DATE", "AND creation_date BETWEEN '" + fechai + " 00:00:00' AND '" + fechaf + " 23:59:59' ");
            else
                sql = sql.replaceAll("#DATE", "");
            
            ps = con.prepareStatement(sql);
            ps.setString(1, doc.getDstrct());
            ps.setString(2, doc.getTipo_documento() + "%");
            ps.setString(3, doc.getNo_documento() + "%");
            ps.setString(4, doc.getReferencia_1() + "%");
            ps.setString(5, doc.getReferencia_2() + "%");
            ps.setString(6, doc.getReferencia_3() + "%");
            ps.setString(7, login + "%");
            
            //logger.info("?sql search: " + ps);
            
            rs = ps.executeQuery();
            this.documentos = new Vector();
            
            while( rs.next() ){
                SerieDocAnulado obj = new SerieDocAnulado();
                obj.load(rs);
                obj.setTipo_documento_desc(rs.getString("tipo_documento_desc"));
                obj.setCausa_anulacion_desc(rs.getString("causa_anulacion_desc"));
                this.documentos.add(obj);
            }

        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE search " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (ps != null){ ps.close(); }
            this.desconectar(query);
        }
    }
    
    /**
     * Getter for property documentos.
     * @return Value of property documentos.
     */
    public java.util.Vector getDocumentos() {
        return documentos;
    }
    
    /**
     * Setter for property documentos.
     * @param documentos New value of property documentos.
     */
    public void setDocumentos(java.util.Vector documentos) {
        this.documentos = documentos;
    }
    
    /**
     * Obtener un registro
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException
     * @version 1.0
     */
    public void obtener() throws SQLException{
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GET";
        
        try{
            
            ps = this.crearPreparedStatement(query);
            
            ps.setString(1, doc.getDstrct());
            ps.setString(2, doc.getTipo_documento());
            ps.setString(3, doc.getNo_documento());
            ps.setString(4, doc.getReferencia_1());
            ps.setString(5, doc.getReferencia_2());
            ps.setString(6, doc.getReferencia_3());
            
            //logger.info("?obtener: " + ps);
            rs = ps.executeQuery();
            
            this.doc = null;
            if( rs.next() ){                
                this.doc = new SerieDocAnulado();
                this.doc.load(rs);
                this.doc.setTipo_documento_desc(rs.getString("tipo_documento_desc"));
                this.doc.setCausa_anulacion_desc(rs.getString("causa_anulacion_desc"));
            }

        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE obtener " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (ps != null){ ps.close(); }
            this.desconectar(query);
        }
    }
    
}
