/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.ParametrosBeans;
import com.tsp.operation.model.beans.Usuario;

/**
 *
 * @author mcastillo
 */
public interface GarantiasComunitariasDAO {
    
    public String listarTipoDocumentos(String mostrarTodos);

    public String cargarConfigDocs();

    public String guardarConfigDocs(String tipo_doc, String content, Usuario usuario);

    public String cargarEquivalencias();
    
    public String cargarComboUnidad();
    
    public boolean existePlazoFactor(String id_unidad, String plazo_inicial, String plazo_final, String nit_empresa_fianza,String producto,String cobertura);
             
    public boolean existePlazoFactor(String id, String id_unidad, String plazo_inicial, String plazo_final,String nit_empresa_fianza,String producto,String cobertura);

    public String cargarConfigFactor();

    public String guardarConfigFactor(String codigo_convenio, String id_unidad, String nit_empresa, String plazo_inicial, String plazo_final, String porc_comision, String valor_comision, String porc_iva, Usuario usuario, String financiado,String producto, String cobertura);

    public String actualizarConfigFactor(String id, String codigo_convenio, String id_unidad, String nit_empresa, String plazo_inicial, String plazo_final, String porc_comision, String valor_comision, String porc_iva, Usuario usuario, String financiado,String porcentaje_aval,String porcentaje_aval_finan,String producto, String cobertura);

    public String activaInactivaConfigFactor(String id, Usuario usuario);
    
    public String cargarCboEmpresasFianza();
    
    /**
     *
     * @param id_unidad
     * @param nit_empresa
     * @param periodo
     * @param agencia
     * @return
     */
    public String buscarNegociosProcesarFianza(String id_unidad, String nit_empresa, String periodo,String agencia);
     
    /**
     *
     * @param id_unidad
     * @param agencia
     * @param nit_empresa
     * @param periodo
     * @param x
     * @param valor_cxp
     * @param usuario
     * @return
     */
    public String generarCxPFianza(String id_unidad, String agencia, String nit_empresa, String periodo, String[] x, String valor_cxp, String usuario);
    
     /**
     *Metodo que buscas las facturas a indemnizar a corte de foto cartera.
     * @param query := nombre de la consulta que se va ejecutar  
     * @param parametrosBeans  :=Parametros de la consulta
     * @param user  := beans con usuario de session.
     * @return
     */
    public String getSelectDataBaseJson(String query, ParametrosBeans parametrosBeans ,Usuario user);
    
     /**
     * Metodo que construye insert para almacenar las facturas a indemnizar
     * 
     * @param query := nombre de la consulta que se va ejecutar  
     * @param empresa_fianza  
     * @param objects :=Objeto json con los datos de la factura
     * @param user := beans con usuario de session.
     * @return
     */
    public String getInsertDataBaseJson(String query, String empresa_fianza, JsonObject objects ,Usuario user);
    
    /**
     * Metodo para la creacion de IC para cancelar cartera del cliente.
     * @param acelerarPagare
     * @param user
     * @return
     */
    public String crearICIndemnizacionFianza(boolean acelerarPagare, Usuario user);
    
     /**
     * Metodo para la creacion de CxC de fianza.
     * @param acelerarPagare
     * @param user
     * @return
     */
    public String crearCxCIndemnizacionFianza(boolean acelerarPagare, Usuario user);
    
    /**
     * Metodo que guarda las facturas desistidas.
     * @param query
     * @param objects
     * @param user
     * @return
    */
    public String getInsertFacturaXdesistir(String query, JsonObject objects ,Usuario user);
    
    /**
     * Metodo para crear comprobante diario para el desistimiento de facturas.
     * @param user
     * @return
    */
    public String crearComprobanteDiarioDesistimiento(Usuario user);
    
    /**
     * Metodo que imprime pdf generado al indemnizar los negocios seleccionados a garantias comunitarias
     * @param acelerarPagare
     * @param numCxC
     * @param ruta
     * @param peridoCorte
     * @param diasMora
     * @param usuario
     * @return
    */
    public String generarCxCGarantias(boolean acelerarPagare, String numCxC, String ruta, String peridoCorte, String diasMora, String usuario);
    
    public String generarNotificacionCliente(String nit_cliente, String negocio, String mora, String ruta, Usuario user);
        
    public String cargarResumenIndemnizados(String periodo);
        
    public String cargarReportesFianza(String query, String id_unidad, String empresa_fianza, String periodo, Usuario usuario);

    public String cargarReporteReclamaciones();

    public String cargarReportePrepagos();

    public String cargarReporteRecuperaciones();
    
    public String cargarCboVencimientos();
    
     /**
     * Metodo que guarda las facturas por indemnizar cuando.
     * @param query
     * @param objects
     * @param empresa_fianza
     * @param user
     * @return
    */
    public String getInsertFacturasXindemnizarMicro(String query, JsonObject objects, String empresa_fianza, Usuario user);
    
    /**
     *
     * @param negocio
     * @param beans
     * @param query
     * @return
     */
    public String cargarDetalleFacturasNegocio(String negocio,ParametrosBeans beans, String query);
    
    public String cargarNegociosVistoBuenoFianza(String unidad_negocio);
    
    public String generarVistoBuenoFianza(String negocio, Usuario usuario);
    
    public String generarVistoBuenoPoliza(String negocio, Usuario usuario);
     
    /**
     *
     * @param idUnidadNegocio
     * @return
     */
    public String cargarAgenciaUnidiad(int idUnidadNegocio);  

    public String indemnizarNegocios(JsonObject obj, Usuario usuario);

    public String cargarNegociosIndemnizados();
        
    public String verNegociosIndemnizados(String periodo);

    public String generarPdf( String usuario , String negocio);
    
    public String cargarProveedorFianza();
    
    /**
     * Busca proveedores
     * @param campo campo de la tabla por el que desea buscar, nit o nombre
     * @param texto texto a buscar en la base de datos
     * @return JSON con la lista de proveedores encontrados
     */
    public String buscarProveedorFianza(String campo, String texto);
    
    /**
     * Guarda un proveedor fianza
     * @param nit nit del proveedor
     * @param usuario usuario de la sesión
     * @return mensaje de confirmación o error
     */
    public String guardarProveedorFianza(String nit, Usuario usuario);

    /**
     * Activa o desactiva un proveedor fianza
     * @param id identificador del proveedor fianza
     * @param usuario usuario de la sesión
     * @return mensaje de confirmación o error
     */
    public String cambiarEstadoProveedorFianza(String id, Usuario usuario);     

    public String cargarCboProducto();

    public String cargarCboCobertura();
}
