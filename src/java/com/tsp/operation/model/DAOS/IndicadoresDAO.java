/********************************************************************
 *      Nombre Clase.................   IndicadoresDAO.java
 *      Descripci�n..................   DAO del archivo tblindicadores
 *      Autor........................   Ing. Tito Andr�s Maturana De La Cruz
 *      Fecha........................   2 de marzo de 2006, 10:10 AM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

/**
 *
 * @author  Ing. Tito Andr�s Maturana De La Cruz
 */
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class IndicadoresDAO extends MainDAO{
    
    private List lista;
    private List padres;
    private Indicador indicador;
    
    /** Crea una nueva instancia de  IndicadoresDAO */
    public IndicadoresDAO() {
        super("IndicadoresDAO.xml");
    }
    
    /**
     * Lista los indicadores con sus respectivos padres
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public List mostrarContenido(String cia) throws SQLException{        
        PreparedStatement st = null;
        ResultSet rs = null;
        List list = new LinkedList();
        
        try{
            st = this.crearPreparedStatement("SQL_SHOW");
            st.setString(1, cia);
            rs = st.executeQuery();
            
            while (rs.next()) {
                Indicador indic = new Indicador();
                indic.load0(rs);
                list.add(indic);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR EN ARMANDO LOS INDICADORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_SHOW");
        }
        
        return list;
    }
    
    /**
     * Inserta un indicadore por defecto denominado "ra�z"
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void insertDefault(String cia) throws SQLException{        
        PreparedStatement st = null;
        ResultSet rs = null;
        List list = new LinkedList();
        
        try{
            st = this.crearPreparedStatement("SQL_OBTENER");
            st.setString(1, cia);
            st.setInt(2, 0);
            
            rs = st.executeQuery();
            
            int cent = 0;
            
            if( rs.next() ){
                cent = 1;
            }
            
            if( cent==0 ){
                st = this.crearPreparedStatement("SQL_INSERTDEFAULT");
                st.setInt(1, 0);
                st.setString(2, "menu");
                st.setString(3, "-1");
                st.setInt(4, 0);
                st.setInt(5, 0);
                st.setString(6, "Y");
                
                st.executeUpdate();
            }
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR EN LOS INDICADORES - INSERTANDO EL DEFAULT " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_OBTENER");
            this.desconectar("SQL_INSERTDEFAULT");
        }
    }
    
    /**
     * Obtiene los hijos de un padre
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del distrito
     * @param id C�digo del padre
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void buscarHijos(String cia, String id) throws SQLException{        
        PreparedStatement st = null;
        ResultSet rs = null;
        lista = new LinkedList();
        
        try{
            st = this.crearPreparedStatement("SQL_BUSCARHIJOS");
            st.setString(1, cia);
            st.setString(2, id);
            
            rs = st.executeQuery();
            
            int cent = 0;
            
            while(rs.next()){
                Indicador tmp = new Indicador();
                tmp.load1(rs);
                lista.add(tmp);
            }
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR EN LOS INDICADORES - BUSCANDO HIJOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_BUSCARHIJOS");
        }
    }
    
    /**
     * Obtiene los indicadores con sus respectivos padres
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param cia C�digo del distrito
     * @param codigo C�digo del indicador      
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public List mostrarContenido(String cia, String codigo) throws SQLException{        
        PreparedStatement st = null;
        ResultSet rs = null;
        List list = new LinkedList();
        
        try{
            st = this.crearPreparedStatement("SQL_SHOWC");
            st.setString(1, cia);
            st.setString(2, codigo);
            rs = st.executeQuery();
            
            while (rs.next()) {
                Indicador indic = new Indicador();
                indic.load1(rs);
                list.add(indic);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR EN ARMANDO LOS INDICADORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_SHOWC");
        }
        
        return list;
    }
    
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.List getLista() {
        return lista;
    }
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.List lista) {
        this.lista = lista;
    }
    
    /**
     * Inserta un registro en el archivo tblindicadores
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void insertar() throws SQLException{        
        PreparedStatement st = null;
        
        try{
            st = this.crearPreparedStatement("SQL_INSERT");
            
            st.setString(1, indicador.getDistrict());
            st.setString(2, indicador.getCodpadre());
            st.setInt(3, indicador.getOrden());
            st.setInt(4, indicador.getNivel());
            st.setString(5, indicador.getId_folder());
            st.setString(6, indicador.getDescripcion());
            st.setLong(7, indicador.getVlr_min());
            st.setLong(8, indicador.getVlr_max());
            st.setLong(9, indicador.getValor());
            st.setInt(10, indicador.getPorcentaje());
            st.setString(11, indicador.getMetodo());
            st.setString(12, indicador.getFormula());
            st.setString(13, indicador.getEjecucion());
            st.setString(14, indicador.getBase());
            st.setString(15, indicador.getUsuario());
            st.setString(16, indicador.getUsuario());
            
            //////System.out.println(".................... INSERT: " + st);
            
            st.executeUpdate();
            
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR EN LOS INDICADORES - INSERTANDO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_INSERT");
        }
    }
    
    /**
     * Getter for property indicador.
     * @return Value of property indicador.
     */
    public com.tsp.operation.model.beans.Indicador getIndicador() {
        return indicador;
    }
    
    /**
     * Setter for property indicador.
     * @param indicador New value of property indicador.
     */
    public void setIndicador(com.tsp.operation.model.beans.Indicador indicador) {
        this.indicador = indicador;
    }
    
    /**
     * Actualiza un registro en el archivo tblindicadores
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizar() throws SQLException{        
        PreparedStatement st = null;
        
        try{
            st = this.crearPreparedStatement("SQL_UPDATE");
            
            st.setString(1, indicador.getCodpadre());
            st.setInt(2, indicador.getOrden());
            st.setInt(3, indicador.getNivel());
            st.setString(4, indicador.getId_folder());
            st.setString(5, indicador.getDescripcion());
            st.setLong(6, indicador.getVlr_min());
            st.setLong(7, indicador.getVlr_max());
            st.setLong(8, indicador.getValor());
            st.setInt(9, indicador.getPorcentaje());
            st.setString(10, indicador.getMetodo());
            st.setString(11, indicador.getFormula());
            st.setString(12, indicador.getEjecucion());
            st.setString(13, indicador.getUsuario());
            st.setString(14, indicador.getDistrict());
            st.setString(15, indicador.getCodigo());
                        
            //////System.out.println(".................... UPDATE: " + st);
            
            st.executeUpdate();
            
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR EN LOS INDICADORES - ACTUALIZANDO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_UPDATE");
        }
    }
    
    /**
     * Obtiene los indicadores que son del tipo padres
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param con Conexi�n a la Base de Datos
     * @param cia C�digo del Distrito
     * @param id C�digo del padre
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void soloPadres(Connection con, String cia, String id) throws SQLException{        
        PreparedStatement st = null;
        ResultSet rs = null;
        if( this.padres == null ) this.padres = new LinkedList();
        
        try{
            
            if( con == null ){
                con = this.conectar("SQL_BUSCARPADRES");
            }
            
            st = con.prepareStatement(this.obtenerSQL("SQL_BUSCARPADRES"));
            
            st.setString(1, cia);
            st.setString(2, id);
            
            rs = st.executeQuery();
            
            //////System.out.println("...................... PADRES: " + st);
            
            while(rs.next()){
                Indicador tmp = new Indicador();
                tmp.load1(rs);
                this.padres.add(tmp);
                this.soloPadres(con, cia, tmp.getCodigo());
            }
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR EN LOS INDICADORES - BUSCANDO PADRES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_BUSCARPADRES");
        }
    }
    
    /**
     * Getter for property padres.
     * @return Value of property padres.
     */
    public java.util.List getPadres() {
        return padres;
    }
    
    /**
     * Setter for property padres.
     * @param padres New value of property padres.
     */
    public void setPadres(java.util.List padres) {
        this.padres = padres;
    }
    
    /**
     * Elimina un registro en el archivo tblindicadores
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void delete() throws SQLException{        
        PreparedStatement st = null;
        
        try{
            st = this.crearPreparedStatement("SQL_DELETE");
            
            st.setString(1, indicador.getDistrict());
            st.setString(2, indicador.getCodigo());
                        
            //////System.out.println(".................... DELETE: " + st);
            
            st.executeUpdate();
            
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR EN LOS INDICADORES - ELIMINANDO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_DELETE");
        }
    }
    
    
}
