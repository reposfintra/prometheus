/*
 * CausaDiscrepaciaDAO.java
 *
 * Created on 13 de octubre de 2005, 04:15 PM
 */

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;
//import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  dbastidas
 */
public class CausaDiscrepaciaDAO extends MainDAO{
    
    /** Creates a new instance of CausaDiscrepaciaDAO */
    public CausaDiscrepaciaDAO() {
        super("CausasDiscrepanciaDAO.xml");
    }
    private CausaDiscrepancia causadis;
    private Vector Veccausa;
    
    private static final String select="select table_code AS codigo, descripcion from tablagen where table_type='CAUDIS' AND reg_status != 'A'";
    
    private static final String obtener ="select table_code AS codigo, descripcion from tablagen where table_type='CAUDIS' AND reg_status != 'A' AND table_code = ?";


/**
 *
 * @return
 * @throws SQLException
 */
    public Vector listarCausas( ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        Veccausa = null;
        String query = "SQL_CONSULTAR";

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                Veccausa = new Vector();
                ////System.out.println(st);
                rs = st.executeQuery();
                
                while (rs.next()){
                    Veccausa.add(CausaDiscrepancia.load(rs)); 
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL LISTAR CAUSA DISCREPANCIA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Veccausa;
    }
    
    public String descripcionCausa(String codigo ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        String des="";
        String query = "SQL_OBTENER";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, codigo);
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    des = rs.getString("descripcion");
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL OBTENER LA DESCRIPCION DE LA CAUSA " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return des;
    }
    
}
