/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;
import com.tsp.operation.model.beans.Siniestros;
import com.tsp.operation.model.beans.TrayectoriaCliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
/**
 *
 * @author jpinedo
 */
public class TrayectoriaClienteDAO extends MainDAO{


    public TrayectoriaClienteDAO() {
        super("TrayectoriaClienteDAO.xml");
    }

     public TrayectoriaClienteDAO(String dataBaseName) {
        super("TrayectoriaClienteDAO.xml", dataBaseName);
    }






       /**
     * M�todo que lista siniestros enviados
     * @autor.......jpinedo
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ArrayList< TrayectoriaCliente> ListarTrayectoriaCliente( String id) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_LISTAR_TRAYECTORIA_CLIENTE";
        ArrayList lista = new ArrayList();

        try{

            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, id);
            st.setString(2, id);
            st.setString(3, id);
            rs=st.executeQuery();
            while(rs.next()){
                TrayectoriaCliente trayectoria = new  TrayectoriaCliente();
                trayectoria.setTipo(rs.getString("Tipo"));
                trayectoria.setTipo_Documento(rs.getString("Tipo_Documento"));
                trayectoria.setNro_Documento_Identidad(rs.getString("Nro_Documento_Identidad"));
                trayectoria.setBanco(rs.getString("nombre_banco"));
                trayectoria.setCodigo_de_Sucursal(rs.getString("Codigo_de_Sucursal"));
                trayectoria.setNumero_de_Cuenta_Exitosas(rs.getString("Numero_de_Cuenta"));
                trayectoria.setCantidad_Consultas_Exitosas(rs.getString("Cantidad_Consultas_Exitosas"));
                trayectoria.setValor_Consultas_Exitosas(rs.getDouble("Valor_Consultas_Exitosas"));
                trayectoria.setFecha_Ultima_actualizacion(rs.getString("Fecha_Ultima_actualizacion"));
                trayectoria.setIndicador_Siniestro(rs.getString("Indicador_Siniestro"));
                trayectoria.setNumero_de_Cheque(rs.getString("Numero_de_Cheque"));
                trayectoria.setValor_de_Cheque(rs.getString("Valor_de_Cheque"));
                trayectoria.setFecha_Consignacion(rs.getString("Fecha_Consignacion"));
                trayectoria.setFecha_Recuperacion(rs.getString("Fecha_Recuperacion"));
                trayectoria.setDias_Recuperados(rs.getInt("Dias_Recuperados"));
                trayectoria.setEstado_del_Cheque(rs.getString("Estado_del_Cheque"));
                trayectoria.setNombre(rs.getString("nomblre_cliente"));
                lista.add( trayectoria );
            }

        }catch(Exception e){
            throw new Exception( "listarsiniestros " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }



}
