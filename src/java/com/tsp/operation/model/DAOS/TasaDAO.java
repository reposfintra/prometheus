/*
 * TasaDAO.java
 *
 * Created on 28 de junio de 2005, 10:25 AM
 */

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  DIOGENES
 */
public class TasaDAO extends MainDAO {
    
    /** Creates a new instance of TasaDAO */
    public TasaDAO() {
        super("TasaDAO.xml");//JJCastro fase2
    }
    public TasaDAO(String dataBaseName) {
        super("TasaDAO.xml", dataBaseName);//JJCastro fase2
    }
    private Tasa tasa;
    private Vector VecTasa;
    
    private static final  String insertar = "insert into tasa (cia,moneda1,moneda2,vlr_conver,compra,venta,fecha,last_update,user_update,creation_date,creation_user, base)"+
    "values (?,?,?,?,?,?,?,'now()',?,'now()',?,?)";
    
    private static final  String existe = " select *               "+
    " from                   "+
    "     tasa               "+
    " where                  "+
    "       moneda1 = ?      "+
    "   and moneda2 = ?      "+
    "   and fecha = ?        "+
    "   and cia = ?          "+
    "   and estado = ''      ";
    
    private static final  String existeanulado = " select *               "+
    " from                   "+
    "     tasa               "+
    " where                  "+
    "       moneda1 = ?      "+
    "   and moneda2 = ?      "+
    "   and fecha = ?        "+
    "   and cia = ?          "+
    "   and estado = 'A'     ";
    
    private static final  String listarTasa = " select  *                       "+
    " from                            "+
    "                                 "+
    "      tasa t                     "+
    " where                           "+
    "         estado = ''             "+
    "         order by fecha          ";
    
    
    private static final String anular = " update                      "+
    "        tasa                 "+
    " set                         "+
    "    estado = 'A',            "+
    "    last_update = 'now()',   "+
    "    user_update = ?,          "+
    "    base = ?                 "+
    " where                       "+
    "       moneda1 = ?           "+
    "    and moneda2 = ?          "+
    "    and fecha = ?            "+
    "    and cia = ?              ";
    
    private static final String activar = " update                     "+
    "        tasa                 "+
    " set                         "+
    "    estado = '',             "+
    "    last_update = 'now()',   "+
    "    user_update = ?,         "+
    "    base = ?                 "+
    " where                       "+
    "       moneda1 = ?           "+
    "    and moneda2 = ?          "+
    "    and fecha = ?            "+
    "    and cia = ?              ";
    private static final String modificar = " update                   "+
    "       tasa               "+
    " set                      "+
    "     vlr_conver = ?,      "+
    "     compra = ?,          "+
    "     venta = ?,           "+
    "    last_update = 'now()',"+
    "    user_update = ?,       "+
    "    base = ?                 "+
    " where                    "+
    "        moneda1 = ?       "+
    "    and moneda2 = ?       "+
    "    and fecha = ?         "+
    "    and cia = ?           ";
    
    private static final String buscar = "   select *        "+
    "   from            "+
    "       tasa t,     "+
    "       (select codmoneda" +
    "        from monedas where " +
    "             nommoneda like ? ) a," +
    "       (select codmoneda " +
    "        from monedas where " +
    "             nommoneda like ? ) b "+
    "   where                            " +
    "         t.moneda1 like a.codmoneda" +
    "     and t.moneda2 like b.codmoneda  " +
    "     and t.fecha  like ?        " +
    "     and t.cia like ?            " +
    "     and t. estado = ''";
    private String SQL_TASA_FECHA = "SELECT (1/vlr_conver) as valor" +
    "               FROM   TASA" +
    "               WHERE  FECHA =?" +
    "                      AND MONEDA1 = ?" +
    "                      AND MONEDA2 = ?";
    
    private String SQL_TASA_NO_FECHA = " SELECT (1/vlr_conver) as valor" +
    "   FROM   TASA" +
    "   WHERE  MONEDA1 = ?" +
    "          AND MONEDA2 = ?" +
    "ORDER BY FECHA  DESC";
    
    //diogenes
    private String SQL_TASA =   " SELECT " +
    "       vlr_conver as valor,         " +
    "       fecha,                       " +
    "       ? - FECHA AS dif             " +
    "   FROM   TASA                          " +
    "   WHERE      FECHA <=  ?               " +
    "          AND ? - FECHA < 6000             " +
    "          AND MONEDA1 = ?               " +
    "          AND MONEDA2 = ?               " +
    "ORDER BY FECHA  DESC limit 1            ";
    
    public void  setTasa(Tasa tasa){
        
        this.tasa=tasa;
        
    }
    public Vector obtVecTasa(){
        return VecTasa;
    }
    
    public Tasa obtenerTasa(){
        return tasa;
    }


/**
 * 
 * @throws SQLException
 */
    public void insertarTasa() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "insertar";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tasa.getCia() );
                st.setString(2, tasa.getMoneda1() );
                st.setString(3, tasa.getMoneda2() );
                st.setFloat(4, tasa.getVlr_conver() );
                st.setFloat(5, tasa.getCompra() );
                st.setFloat(6, tasa.getVenta() );
                st.setString(7, tasa.getFecha() );
                st.setString(8, tasa.getUser_update() );
                st.setString(9, tasa.getCreation_user() );
                st.setString(10, tasa.getBase());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR LA TASA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }



/**
 * 
 * @param moneda1
 * @param moneda2
 * @param fecha
 * @param cia
 * @return
 * @throws SQLException
 */
    public boolean existeTasa(String moneda1, String moneda2, String fecha, String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "existe";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, moneda1 );
                st.setString(2, moneda2 );
                st.setString(3, fecha );
                st.setString(4, cia );
                rs = st.executeQuery();
                
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICAR SI EXISTE TASA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return sw;
    }


 /**
  * 
  * @param moneda1
  * @param moneda2
  * @param fecha
  * @param cia
  * @return
  * @throws SQLException
  */
    public boolean existeTasaAnulado(String moneda1, String moneda2, String fecha, String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "existeanulado";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, moneda1 );
                st.setString(2, moneda2 );
                st.setString(3, fecha );
                st.setString(4, cia );
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICAR SI EXISTE TASA ANULADA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return sw;
    }

/**
 *
 * @throws SQLException
 */
    public void listarTasa() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        VecTasa = null;
        String query = "listarTasa";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                VecTasa = new Vector();
                rs = st.executeQuery();
                while (rs.next()){
                    VecTasa.add(tasa.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR TASA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }


/**
 * 
 * @param moneda1
 * @param moneda2
 * @param fecha
 * @param cia
 * @throws SQLException
 */
    public void buscarTasa(String moneda1, String moneda2, String fecha, String cia ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "existe";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, moneda1 );
                st.setString(2, moneda2 );
                st.setString(3, fecha );
                st.setString(4, cia );
                rs = st.executeQuery();
                
                if (rs.next()){
                    tasa = Tasa.load(rs);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA TASA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    
/**
 *
 * @throws SQLException
 */
    public void modificarTasa() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "modificar";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setFloat(1, tasa.getVlr_conver() );
                st.setFloat(2, tasa.getCompra() );
                st.setFloat(3, tasa.getVenta() );
                st.setString(4, tasa.getUser_update() );
                st.setString(5, tasa.getBase() );
                st.setString(6, tasa.getMoneda1() );
                st.setString(7, tasa.getMoneda2() );
                st.setString(8, tasa.getFecha());
                st.setString(9, tasa.getCia() );
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL MODIFICAR TASA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }

 /**
  *
  * @throws SQLException
  */
    public void anularTasa() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "anular";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tasa.getUser_update() );
                st.setString(2, tasa.getBase() );
                st.setString(3, tasa.getMoneda1() );
                st.setString(4, tasa.getMoneda2() );
                st.setString(5, tasa.getFecha() );
                st.setString(6, tasa.getCia() );
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR TASA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }


/**
 * 
 * @throws SQLException
 */
    public void activarTasa() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "activar";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tasa.getUser_update() );
                st.setString(2, tasa.getBase() );
                st.setString(3, tasa.getMoneda1() );
                st.setString(4, tasa.getMoneda2() );
                st.setString(5, tasa.getFecha() );
                st.setString(6, tasa.getCia() );
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTIVAR TASA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }


/**
 * 
 * @param moneda1
 * @param moneda2
 * @param fecha
 * @param cia
 * @throws SQLException
 */
    public void BuscarTasaX(String moneda1, String moneda2, String fecha, String cia) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "buscar";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, moneda1 );
                st.setString(2, moneda2 );
                st.setString(3, fecha );
                st.setString(4, cia );
                
                VecTasa = new Vector();
                rs = st.executeQuery();
                while (rs.next()){
                    VecTasa.add(tasa.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICAR SI EXISTE TASA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /**
     * Metodo buscarValor, busca el valor de la tasa de cambio dada dos monedas
     * @param: Moneda1 moneda que se va a cambiar
     * @param: Moneda2 moneda a la que se va a cambiar
     * @param: fecha Fecha del cambio
     * @param: valor Valor a cambiar
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public double buscarValor(String moneda1, String moneda2, String fecha, float valor ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double valor_cambiado =0;
        String query = "SQL_TASA_FECHA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, fecha );
                st.setString(2, moneda1 );
                st.setString(3, moneda2 );
                rs = st.executeQuery();
                
                if (rs.next()){
                    valor_cambiado =valor*rs.getDouble("valor");
                }
                else{
                    st = con.prepareStatement(this.obtenerSQL("SQL_TASA_NO_FECHA")) ;//JJCastro fase2
                    st.setString(1, moneda1 );
                    st.setString(2, moneda2 );
                    rs = st.executeQuery();
                    
                    if (rs.next()){
                        valor_cambiado =valor*rs.getDouble("valor");
                    }
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR LA TASA" + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return valor_cambiado;
    }
    
     /**
     * Metodo buscarDatosTasa, busca el valor de la tasa de cambio dada dos monedas,
     * obteniendo un objeto que tiene la fecha, el valor de la tasa.
     * Si la no encuentra informacion de la moneda 1 a la moneda 2 realiza la busqueda de 
     * la moneda 2 a la moneda 1 realizando la inversion del valor de la tasa 1/vrl_convercion
     * de lo contrario retorna el objeto null
     * @param: Moneda1 moneda inicial
     * @param: Moneda2 moneda final
     * @param: fecha Fecha del cambio
     * @autor : Ing. Diogenes Bastidas.
     * @version : 1.0
     */
    public Tasa buscarDatosTasa(String moneda1, String moneda2, String fecha ) throws Exception {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tasa = null;
        String query = "SQL_TASA";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, fecha );
                st.setString(2, fecha );
                st.setString(3, fecha );
                st.setString(4, moneda1 );
                st.setString(5, moneda2 );

                rs = st.executeQuery();
                
                if (rs.next()){
                    tasa = new Tasa();
                    tasa.setFecha(rs.getString("fecha"));
                    tasa.setValor_tasa1(rs.getDouble("valor"));
                    tasa.setValor_tasa2(rs.getDouble("valor"));
                    tasa.setValor_tasa(rs.getDouble("valor"));
                    tasa.setDiferencia(rs.getInt("dif"));
                    
                }
                else{
                    st.setString(4, moneda2 );
                    st.setString(5, moneda1 );
                  
                    rs = st.executeQuery();
                    if (rs.next()){
                        tasa = new Tasa();
                        tasa.setFecha(rs.getString("fecha"));
                        tasa.setValor_tasa1(1/rs.getDouble("valor"));
                        tasa.setValor_tasa2(1/rs.getDouble("valor"));
                        tasa.setValor_tasa(1/rs.getDouble("valor"));
                        tasa.setDiferencia(rs.getInt("dif"));
                        
                    }
                }
                
            }
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return tasa;
    }
    
    
}
