/********************************************************************
 *  Nombre Clase.................   ReporteMovTraficoCerromatosoDAO.java
 *  Descripci�n..................   DAO de la tabla (rep_mov_trafico, cliente, remesa, plarem)
 *  Autor........................   Ing. Leonardo Parodi
 *  Fecha........................   12.01.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import javax.swing.*;
import java.text.*;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  EQUIPO12
 */
public class ReporteMovTraficoCerromatosoDAO extends MainDAO{
        
        /** Creates a new instance of ReporteMovTraficoCerromatosoDAO */
        public ReporteMovTraficoCerromatosoDAO() {
            super("ReporteMovTraficoCerromatosoDAO.xml");
        }
        
        private final String SQL_RepMovTraCerromatoso =
        
        "SELECT                                                                 "+
        "        d.*,                                                           "+
        "        tg.descripcion AS nom_tipo_reporte                             "+
        "FROM                                                                   "+
	"       (SELECT                                                         "+
	"       	c.*,                                                    "+
	"               ciu.nomciu AS nom_ubicacion_procedencia                 "+
	"       FROM                                                            "+
	"               (SELECT                                                 "+
	"                       b.*,                                            "+
        "                       z.desczona AS nom_zona                          "+
        "               FROM                                                    "+
	"                       (SELECT                                         "+
	"                               a.*,                                    "+
	"                               t.desc_tubicacion AS nom_tipo_procedencia       "+
	"                       FROM                                            "+
	"                               (SELECT                                 "+
	"                                       rep.creation_date,              "+
	"                                       rep.tipo_reporte,               "+
	"                                       rep.ubicacion_procedencia,      "+
	"                                       rep.zona,                       "+
	"                                       rep.tipo_procedencia,           "+
	"                                       pr.numpla,                      "+
	"                                       r.numrem,                       "+
	"                                       r.cliente,                      "+
	"                                       get_nombrecliente (r.cliente) AS nom_cliente    "+
	"                               FROM                                    "+
        "                                       remesa r,                       "+
	"                                       plarem pr,                      "+
	"                                       rep_mov_trafico rep             "+
        "                                                                       "+
	"                               WHERE                                   "+
	"                                       r.cliente = '000031'            "+
	"                                   AND rep.numpla = pr.numpla          "+
	"                                   AND pr.numrem = r.numrem            "+
	"                                                                       "+
	"                               ORDER BY 3, 1) a                        "+
	"                       LEFT JOIN                                       "+
        "                                tipo_ubicacion t                       "+
	"                       ON                                              "+	
        "                                a.tipo_procedencia = t.cod_tubicacion) b       "+
	"               LEFT JOIN                                               "+
	"                       zona z                                          "+
	"               ON                                                      "+
	"                       b.zona = z.codzona) c                           "+
	"       LEFT JOIN                                                       "+
	"               ciudad ciu                                              "+
	"       ON                                                              "+
	"               c.ubicacion_procedencia = ciu.codciu) d                 "+
        "LEFT JOIN                                                              "+
	"       tablagen tg                                                     "+        
        "ON                                                                     "+
	"       d.tipo_reporte = tg.table_code                                  ";
        
        
        /**
         * Metodo ReportePlacaFoto , Metodo que obtiene si una placa tiene foto o no
         * @autor : Ing. Leonardo Parody
         * @throws : SQLException
         * @parameter : String fechaI
         * @parameter : String fechaF
         * @parameter : String agencia
         * @retorna una lista de registros de placas y sus fotos
         * @version : 1.0
         */
                public List ReporteMovTrafCerromatoso() throws SQLException {
                        //////System.out.println("ESTOY EN EL DAO EXISTE SERIE");
                    LinkedList reportesMovTraficoCerromatosos = new LinkedList();
                    ReporteMovTraficoCerromatoso reportesMovTraficoCerromatoso = new ReporteMovTraficoCerromatoso();
                    Connection con= null;
                    PreparedStatement st = null;
                    ResultSet rs = null;
                   String query = "SQL_RepMovTraCerromatoso";
                    try{

                            con = this.conectarJNDI(query);
                            if (con != null){
                                   // ////System.out.println("VOY A EJECUTAR EL QUERY");
                                    st = con.prepareStatement(SQL_RepMovTraCerromatoso);
                                   // ////System.out.println("SQL Reporte Placa Foto  = "+st);
                                    rs = st.executeQuery();
                                    while (rs.next()) {
                                            reportesMovTraficoCerromatoso = reportesMovTraficoCerromatoso.load(rs);
                                            reportesMovTraficoCerromatosos.add(reportesMovTraficoCerromatoso);
                                    }
                                    ////System.out.println("Query  "+st);
                                  //  ////System.out.println("YA EJECUTE EL QUERY");
                            }


                    }catch(SQLException e){
                            throw new SQLException("ERROR AL REALIZAR LA CONSULTA" + e.getMessage()+"" + e.getErrorCode());
                    }finally{
                    if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
                        return reportesMovTraficoCerromatosos;
                }

                
}
