/*
 * Nombre        ReporteInformacionAlClienteDAO.java
 * Descripci�n   Clase para el acceso a los datos del reporte de informaci�n al cliente
 * Autor         Alejandro Payares
 * Fecha         16 de septiembre de 2005, 09:34 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import java.util.LinkedList;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Iterator;
import java.util.Calendar;
import java.util.GregorianCalendar;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DatabaseMetaData;
import java.sql.Connection;
import com.tsp.operation.model.*;


/**
 * Clase para el acceso a los datos del reporte de informaci�n al cliente
 * @author  Alejandro Payares
 */
public class ReporteInformacionAlClienteDAO extends MainDAO {
    
    /**
     * Un array de tipo String para almacenar los nombres de los campos en la base de
     * datos que conforman el reporte de infrmaci�n al cliente.
     */
    private String [] camposReporte;
    
    /**
     * Una lista que almacena todos los datos del reporte.
     */
    private Vector datosReporte;
    
    /**
     * Un TreeMap para guardar los tipos de carga disponibles para consultar
     * en el reporte.
     */
    private TreeMap tiposDeCarga;
    
    /**
     * Una bandera para saber si se est�n consultados los viajes de tipo nacional
     */
    private boolean esReporteDeTipoDeViajeNacional = false;
    
    private ConsultasGeneralesDeReportesDAO cgr;
    
    
    /** Creates a new instance of ReporteInformacionAlClienteDAO */
    public ReporteInformacionAlClienteDAO() {
        super(  "ReporteInformacionAlClienteDAO.xml",
        org.apache.log4j.Logger.getLogger(ReporteInformacionAlClienteDAO.class)
        );
        cgr = new ConsultasGeneralesDeReportesDAO();
    }
    public ReporteInformacionAlClienteDAO(String dataBaseName) {
        super(  "ReporteInformacionAlClienteDAO.xml", dataBaseName);
        cgr = new ConsultasGeneralesDeReportesDAO(dataBaseName);
    }
    
    /**
     * es el encargado de ejecutar el query principal del reporte
     * @param args los argumentos del reporte
     * @throws SQLException si algun error ocurre en el acceso a la base de datos
     */
    public void buscarDatosDeReporte(String args[]) throws SQLException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        boolean est = false;
        String userDefValue = "";
        try {
            StringBuffer sql = new StringBuffer(obtenerSQL("SQL_REPORTE_DE_INFO_CLIENTE"));
            sql.append(" ");
            //boolean ocultarPernoctandos = !args[8].equals("true");
            boolean todosLosClientes = args[9].equals("true");
            String cliente  = todosLosClientes?"(TODOS)":args[0]; //request.getParameter("cliente");
            String fechaini = args[1]; //request.getParameter("fechaini");
            String fechafin = args[2]; //request.getParameter("fechafin");
            String listaTipoViaje = args[3]; //request.getParameter("listaTipoViaje");
            esReporteDeTipoDeViajeNacional = listaTipoViaje.equals("NA");
        /* ��� OJO !!!:
         * Queda pendiente el par�metro: "Estado de viaje".
         * Se dejar� su espacio en el arreglo de par�metros hasta que se den
         * las especificaciones para su utilizaci�n.
         */
            userDefValue = args[5]; //request.getParameter("userDefValue");
            
            String tipoCarga = args[7];
            
            // Si se utiliz� uno de los 3 criterios definidos en la parte inferior
            // de la p�gina de filtros para este reporte, utilice �ste para filtrar.
            
            //Busca filtro par alos tipo de documentos
            StringBuffer otrosFiltros = new StringBuffer();
            if( !userDefValue.equals("") ) {
                ////System.out.println("UserDefValue Diferente de Vacio "+ userDefValue);
                String searchCriteria = args[6];
                if( !userDefValue.equals("") ) {
                    ////System.out.println("Realiza la busqueda por el tipo de documento ");
                    String reemplazoCliente1 = todosLosClientes?"":"AND cliente = '"+cliente+"'";
                    if( !fechaini.equals("") ){
                        otrosFiltros.append("\n\t\t\tfecremori BETWEEN '" + fechaini + " 00:00' AND '" + fechafin + " 23:59'");
                        otrosFiltros.append("\n\t\t\t"+reemplazoCliente1);
                    }
                }
                // Si no, filtre por los dem�s campos.
            }else{
                ////System.out.println("UserDefValue Igual Vacio "+ userDefValue);
                String reemplazoCliente1 = todosLosClientes?"":"AND cliente = '"+cliente+"'";
                if( !fechaini.equals("") ){
                    otrosFiltros.append("\n\t\t\tfecremori BETWEEN '" + fechaini + " 00:00' AND '" + fechafin + " 23:59'");
                    otrosFiltros.append("\n\t\t\t"+reemplazoCliente1);
                }
                if( !listaTipoViaje.equals("") ) {
                    otrosFiltros.append("\n\t\t\tAND tipoviaje IN (");
                    String [] tiposDeViaje = listaTipoViaje.split(",");
                    for( int tv = 0; tv < tiposDeViaje.length; tv++ ) {
                        otrosFiltros.append("'"+tiposDeViaje[tv]+"'");
                        if ( tv < tiposDeViaje.length -1 ){
                            otrosFiltros.append(",");
                        }
                    }
                    otrosFiltros.append(")");
                }
            }
            String consulta = sql.toString();//WHERE rc.ref_code IS NOT NULL
            consulta = consulta.replaceFirst("'filtroTipoCarga'",tipoCarga.equals("ALL")?"":" AND ref_code = '"+tipoCarga+"'");
            consulta = consulta.replaceFirst("'whereTipoCarga'",tipoCarga.equals("ALL")?"":"WHERE rc.ref_code IS NOT NULL");
            
            String reemplazoCliente2 = todosLosClientes?"":"AND '000'||substr(Entity_Value,6,3)  = '"+cliente+"'";
            consulta = consulta.replaceAll("'cliente2'", reemplazoCliente2);
            consulta = consulta.replaceFirst("'otrosFiltros'", otrosFiltros.length() == 0?"":otrosFiltros.toString());
            
            Connection con = conectar("SQL_REPORTE_DE_INFO_CLIENTE");
            ps = con.prepareStatement(consulta);
            ////System.out.println("query INFO_CLIENTE: "+ps);
            
            long inicio = System.currentTimeMillis();
            rs = ps.executeQuery();
            buscarCamposDeReporte(cliente,args[10]);
            String camposReporte [] = obtenerCamposDeReporte();
            datosReporte = new Vector();
            if ( rs.next() ){
                CiudadDAO ciudadDAO = new CiudadDAO(this.getDatabaseName());
                do {
                    Hashtable fila = new Hashtable();
                    for( int i=0; i<camposReporte.length; i++ ){
                        String valor = "";
                        try {
                            valor = rs.getString(camposReporte[i]);
                            //////System.out.println("campo["+i+"] = "+camposReporte[i]+", valor = "+valor);
                            if ( camposReporte[i].equals("ultreporte") ){
                                boolean hayPlanilla = false;
                                String planilla = "";
                                String placa = "";
                                String estado = "";
                                int inicioPlanilla = 0;
                                try {
                                    inicioPlanilla = valor.indexOf("_")+1;
                                    planilla = valor.substring(inicioPlanilla,inicioPlanilla+6);
                                    int inicioPlaca = inicioPlanilla+7;
                                    placa = valor.substring(inicioPlaca,inicioPlaca+6);
                                    hayPlanilla = true;
                                    throw new Exception();
                                }
                                catch(Exception ex){
                                    if ( valor != null && inicioPlanilla != 0 && valor.length() > inicioPlanilla ){
                                        valor = valor.substring(0,inicioPlanilla-1);
                                        if ( valor.endsWith("-E") ) {
                                            estado = "Con Entrega"  ;
                                        }
                                        else if (valor.trim().equals("") || valor.equals("-")) {
                                            estado = "Por Confirmar Salida"  ;
                                        }  else {
                                            estado = "En Ruta";
                                        }
                                    }
                                }
                                fila.put("hay_planilla",new Boolean(hayPlanilla));
                                if ( hayPlanilla ){
                                    fila.put("planilla", planilla);
                                    fila.put("placa", placa);
                                    fila.put("estado", estado);
                                }
                            }
                            else if ( camposReporte[i].equals("observacion")){
                                String aux = rs.getString("ultreporte");
                                int fin = aux.indexOf("_");
                                if ( fin > 1 ){
                                    aux = aux.substring(0,fin);
                                    if ( aux != null && aux.toUpperCase().endsWith("-F") ){
                                        valor = "EN FRONTERA";
                                    }
                                }
                            }
                            else if ( camposReporte[i].equals("peso_cargado")  ){
                                valor = this.obtenerPesoCargado(rs.getString("numrem"));
                            }else if(camposReporte[i].equals("fechacargue")){
                                String fec =  obtenerFechaCarga(rs.getString("numrem"));
                                if(!fec.equals("")){
                                    valor = fec;
                                }
                            }else if(camposReporte[i].equals("fechaentrega")){
                                String fecEnt =  obtenerFechaEntrega(rs.getString("numrem"));
                                if(!fecEnt.equals("")){
                                    valor = fecEnt;
                                }
                            }
                            fila.put(camposReporte[i], valor == null?"":valor);
                            
                            //VErifico el documento si existe
                            if(!userDefValue.equals("")  && est == false ){
                                if ( camposReporte[i].equals("docuinterno") ){
                                    est = existeDocumento(rs.getString("docuinterno"),userDefValue);
                                }
                                if ( camposReporte[i].equals("docs_rel") ){
                                    est = existeDocumento(rs.getString("docs_rel"),userDefValue);
                                }
                                if ( camposReporte[i].equals("facturacial")){
                                    est = existeDocumento(rs.getString("facturacial"),userDefValue);
                                }
                            }
                        }
                        catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                    fila.put("numrem", rs.getString("numrem"));
                    fila.put("ot_rela", rs.getString("ot_rela"));
                    obtenerDatosDePlanilla(fila,ciudadDAO);
                    
                    if( !userDefValue.equals("") ){
                        if(est){
                            datosReporte.addElement(fila);
                        }
                        est = false;
                    }else{
                        datosReporte.addElement(fila);
                    }
                }while(rs.next());
                
                ordenarDatos();
            }
            //////System.out.println("datos = "+datosReporte);
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
            desconectar("SQL_REPORTE_DE_INFO_CLIENTE");
        }
    }
    
    /**
     * Metodo que aplica el m�todo de ordenamiento r�pido (quicksort) para ordenar
     * los registros del reporte por la primera planilla de un viaje.
     */
    private void ordenarDatos(){
        Object datos [] = datosReporte.toArray();
        int inicioGrupo = 0,finGrupo = 0;
        Object vacio = "";
        Object fechaAnterior = vacio;
        for( int i=0; i< datos.length; i++ ){
            Hashtable fila = (Hashtable)datos[i];
            Object fechaActual = fila.get("fechacargue");
            //////System.out.println("remesa = "+fila.get("numrem")+", fechaActual = "+fila.get("fechacargue"));
            if ( fechaActual.equals(fechaAnterior) ) {
                finGrupo = i;
            }
            else{
                if ( fechaAnterior != vacio && i - inicioGrupo > 1 ) {
                    Object [] subArray = new Object[i - inicioGrupo];
                    System.arraycopy(datos, inicioGrupo, subArray, 0, subArray.length);
                    quicksort(subArray,0,subArray.length-1);
                    for( int j=0; j< subArray.length; j++ ){
                        datos[inicioGrupo+j] = subArray[j];
                    }
                }
                inicioGrupo = i;
            }
            fechaAnterior = fechaActual;
        }
        
        datosReporte.clear();
        for( int i=0; i< datos.length; i++ ){
            datosReporte.addElement(datos[i]);
        }
    }
    
    /**
     * M�todo recursivo de ordenamiento r�pido (quicksort)
     * @param lista la lista con los registros del reporte a ordenar
     * @param inf indice inferior
     * @param sup indice superior
     */
    private void quicksort(Object [] lista, int inf, int sup){
        Hashtable pivote = (Hashtable) lista[sup];
        Object temp;
        int i = inf - 1;
        int j = sup;
        boolean cont = true;
        
        if (inf >= sup)     //Se cruzaron los indices
            return;
        
        while (cont) {
            while (i < lista.length && dato1esMenorQueDato2((Hashtable)lista[++i], pivote));
            while (j > 0 && dato1esMenorQueDato2(pivote,(Hashtable)lista[--j]));
            // �Se cumple la condici�n ?
            if (i < j) {
                temp = lista[i];
                lista[i] = lista[j];
                lista[j] = temp;
            }
            else
                cont = false;
        }
        
        // Dejamos el elemento de divisi�n en su posici�n final
        temp = lista[i];
        lista[i] = lista[sup];
        lista[sup] = temp;
        
        // Aplicamos recursivamente a los subarreglos generados
        quicksort(lista, inf, i == 0? 0:i - 1);
        quicksort(lista, i == lista.length - 1?i:i + 1, sup);
    }
    
    /**
     * Metodo para compara una fila con otra por el n�mero de la primera planilla
     * @param dato1 la primera fila
     * @param dato2 la segunda fila
     * @return true si la primera planilla de la fila1 o dato1 es menor que la primera planilla de
     * la fila2 o dato2
     */
    public boolean dato1esMenorQueDato2(Hashtable dato1, Hashtable dato2){
        /*Calendar fecha1 = extraerCalendar((String)dato1.get("fechacargue"));
        Calendar fecha2 = extraerCalendar((String)dato2.get("fechacargue"));*/
        String planilla1 = extraerPlanilla((String)dato1.get("planillas"));
        String planilla2 = extraerPlanilla((String)dato2.get("planillas"));
        boolean res = /*fecha1.before(fecha2);*/ planilla1.compareTo(planilla2) < 0;
        //////System.out.println("comparando: "+dato1.get("fechacargue")+" es menor que "+dato2.get("fechacargue")+"? "+fecha1.before(fecha2)+" y "+planilla1+" es menor que "+planilla2+"? "+(planilla1.compareTo(planilla2) < 0)+" ... resultado = "+res);
        return res;
    }
    
    
    /**
     * Permite extraer la primera planilla de las planillas encontradas para un registro
     * del reporte.
     * @param str el String que contiene el texto del cual se extraer� la planilla
     * @return la primera planilla encontrada en str
     */
    private String extraerPlanilla(String str){
        int inicio = str.indexOf(":");
        int fin = str.indexOf("\n<br>",inicio);
        if ( inicio == -1 ) {
            return str.substring(0,fin == -1?str.length():fin).trim();
        }
        String strres = str.substring(inicio+2,fin == -1?str.length():fin).trim();
        return strres;
    }
    
    
    /**
     * Busca los tipos de carga para el cliente dado que pueden ser vistos por el loggedUser dado
     * @param codigoCliente el codigo del cliente del cual se buscaran los tipos de carga a partir de sus estandares
     * creados
     * @param loggedUser el usuario en sesi�n al cual se le restringir�n los tipos de carga que puede ver si
     * existe dicha restricci�n.
     * @throws SQLException si algun error ocurre en el acceso a la base de datos
     */
    public void buscarTiposDeCarga(String codigoCliente, String loggedUser)throws SQLException{
        ResultSet rs         = null;
        PreparedStatement ps = null;
        if ( tiposDeCarga != null ){
            tiposDeCarga.clear();
        }
        tiposDeCarga = new TreeMap();
        try{
            Connection con = conectar("SQL_OBTENER_STD_JOB_USUARIO");
            ps = con.prepareStatement(obtenerSQL("SQL_OBTENER_STD_JOB_USUARIO"));
            ps.setString(1, loggedUser );
            ps.setString(2, codigoCliente );
            rs = ps.executeQuery();
            if ( rs.next() ) {
                String vec [] = rs.getString(1).split(",");
                StringBuffer standares = new StringBuffer();
                for( int i=0; i< vec.length; i++ ){
                    standares.append("'"+vec[i]+"',");
                }
                if ( standares.length() > 0 ) {
                    standares.deleteCharAt(standares.length()-1);
                    String sql = this.obtenerSQL("SQL_OBTENER_TIPO_CARGA_LOGGED_USER");
                    sql = sql.replaceAll("'standares'",standares.toString());
                    ps.close();
                    ps = con.prepareStatement(sql);
                }
            }
            else {
                tiposDeCarga.put("TODOS", "ALL");
                ps.close();
                ps = con.prepareStatement(obtenerSQL("SQL_FILTRO_TIPO_CARGA"));
                ps.setString(1, codigoCliente );
            }
            logg("query tipos de carga: "+ps);
            rs = ps.executeQuery();
            while(rs.next()){
                tiposDeCarga.put(rs.getString("ref_code"), rs.getString("std_job"));
            }
        }
        catch(SQLException e){
            logg(e);
            throw new SQLException("REPORTES DE INFORMACION AL CLIENTE: ERROR OBTENIENDO LOS TIPOS DE CARGA -> " + e.getMessage());
        }
        catch(Exception ex){
            logg(ex);
        }
        finally{
            if (ps != null) try { ps.close(); }catch(SQLException e){}
            desconectar("SQL_OBTENER_STD_JOB_USUARIO");
        }
    }
    
    /**
     * Devuelve los tipos de carga buscado por el metodo buscarTiposDeCarga
     * @throws SQLException si algun error ocurre en el acceso a la base de datos
     * @return Un <CODE>TreeMap</CODE> con los tipos de carga encontrados, si ningun tipo de carga
     * es encontrado el TreeMap tendr� un solo nodo con clave = 'ALL' y valor = 'TODOS'
     */
    public TreeMap obtenerTiposDeCarga() throws SQLException{
        return tiposDeCarga;
    }
    
    
    /**
     * Este m�todo solo puede ser accesado dentro del DAO, y permite buscar los
     * nombres de los campos del reporte de informaci�n al cliente en la base de datos. Es
     * invocado por el m�todo <code>void buscarDatosDeReporte(String [])</code> y el resultado
     * de la busqueda el devuelto por el m�todo
     * <CODE>String [] obtenerCamposReporte()</CODE>.
     * @param loggedUser El id del usuario en sesi�n.
     * @param codigoCliente El codigo del cliente que ver� el reporte, este codigo es obtenido del usuario
     * que est� actualmente activo en la sesi�n.
     * @throws SQLException Si algun problema ocurre con el acceso a la Base de datos.
     */
    private void buscarCamposDeReporte(String codigoCliente, String loggedUser) throws SQLException{
        camposReporte = cgr.buscarCamposDeReporte(codigoCliente, "infocliente", "C", loggedUser );
        if ( esReporteDeTipoDeViajeNacional ){
            Vector v = new Vector(camposReporte.length);
            String camposExcluidos [] = {"llefronexpo","salfronexpo","llefronimpo","salfronimpo","veridex","certpree","verifondos","libeaduana"};
            for(int i=0; i<camposReporte.length; i++ ){
                String campo = camposReporte[i];
                boolean elCampoEstaExcluido = false;
                for( int j=0; j<camposExcluidos.length; j++){
                    if ( campo.equals(camposExcluidos[j]) ){
                        elCampoEstaExcluido = true;
                        break;
                    }
                }
                if ( !elCampoEstaExcluido ){
                    v.addElement(campo);
                }
            }
            String [] aux = new String[v.size()];
            ////System.out.println("campos = "+v);
            
            camposReporte = (String [])v.toArray(aux);
        }
    }
    
    /**
     * Devuelve un array de tipo String [] donde se encuentran almacenados los nombres
     * de los campos del reporte de informaci�n al cliente.
     * @return El array con los nombres de los campos del reporte de informaci�n al cliente.
     */
    public String [] obtenerCamposDeReporte(){
        return camposReporte;
    }
    
    /**
     * Devuelve todos los campos del reporte de informaci�n al cliente registrados en la
     * tabla reporte.
     * @throws SQLException si algun error ocurre en el acceso a la base de datos
     * @return un arreglo que contiene los nombres de todos los campos
     */
    public String [] obtenerTodosLosCampos() throws SQLException {
        return cgr.buscarCamposDeReporte("(ninguno)", "infocliente", "C",  "(ninguno)" );
    }
    
    /**
     * Permite obtener la lista donde estan almacenados los datos del reporte,
     * esta lista es llenada por el metodo buscarDatosDeReporte(...). , la lista
     * contiene en cada nodo un objeto Hashtable cuyas claves son los nombres de
     * los campos del reporte. Para obtener acceso a cada registro, basta con
     * recorrer la lista y extraer cada elemento Hashtable, y para obtener el valor
     * de un campo, basta con pasarle por parametro al metodo get(...) del Hashtable
     * el nombre del campo requerido, un ejemplo de esto ser�a asi:
     * <code>
     * <u>
     *    dao.buscarDatosDeReporte(... parametros ...);
     *    String [] campos = dao.obtenerCamposDeReporte();
     *    LinkedList lista = obtenerDatosReporte();
     *    Iterator ite = lista.iterator();
     *    while( ite.hasNext() ) {
     *    <u>
     *        Hashtable fila = (Hashtable) ite.next();
     *        for( int i = 0; i < campos.length; i++ ) {
     *        <u>
     *            String valor = ((String)fila.get(campos[i]))
     *            ////System.out.println(campos[i] + " = " + valor );
     *        </u>
     *        }
     *    </u>
     *    }
     * </u>
     * <code>
     * @return La lista con los datos del reporte.
     */
    public Vector obtenerDatosReporte(){
        return datosReporte;
    }
    
    /**
     * Este m�todo permite obtener los titulos del encabezado del reporte de
     * informaci�n al cliente. El Hashtable que es devuelto contiene los titulos de todos
     * los campos del reporte y la clave de cada titulo es su respectivo nombre
     * de campo en la Base de datos, para obtener los nombre de los campos del
     * reporte utilize el metodo <CODE>String [] obtenerCamposDeReporte()</CODE>.
     * @throws SQLException Si algun problema ocurre en el acceso a la Base de Datos.
     * @return Una tabla con los titulos de los campos del reporte de informaci�n al cliente.
     */
    public Hashtable obtenerTitulosDeReporte() throws SQLException{
        return cgr.obtenerTitulosDeReporte("infocliente");
    }
    
  
    
    /**
     * Reemplaza un String nulo por uno vac�o ''
     * @param x El string que de ser nulo ser� reemplazado por uno vac�o
     * @return el string no nulo
     */
    private String noNull(String x){
        return x == null?"":x;
    }
    
    /**
     * Devuelve un String con formato HTML que representa el encabezado de la tabla del
     * reporte. Este trabajo se realiza aqui porque el orden de los campos del encabezado
     * puede ser variable de acuerdo a la configuraci�n del usuario en sesi�n y por lo tanto
     * los grupos de los campos tambien puede cambiar y resultaba poco practico hacer este
     * complicado trabajo en la pagina JSP.
     * @param isListaTipoViajeNA una bandera para saber si el encabezado incluira la lista de viaje nacional
     * @return El string correspondiente a la fila del encabezado de la tabla del reporte.
     */
    public String obtenerEncabezadoTablaReporte(boolean isListaTipoViajeNA){
        StringBuffer sb = new StringBuffer();
        try {
            cgr.buscarDetalleDeCampos("infocliente",this.camposReporte);
            Hashtable detalleDeCampos = cgr.obtenerDetalleDeCampos();
            sb.append("<tr bgcolor='#0099FF'>");
            StringBuffer filaTitulos = new StringBuffer("<tr bgcolor='#0099FF'>");
            String grupoAnterior = "";
            int contadorColSpan = 0;
            for( int i=0; i<camposReporte.length; i++ ){
                Hashtable detalle = (Hashtable) detalleDeCampos.get(camposReporte[i]);
                String grupoActual = "";
                filaTitulos.append("<th width='*' bgcolor='"+detalle.get("colorgrupo")+"' scope='col'><span class='Estilo22'>" +
                detalle.get("titulocampo") + "</span></th>");
                grupoActual = "bgcolor='"+detalle.get("colorgrupo")+"' scope='col'><div align='center'>"+detalle.get("grupo")+"</div></th>";
                if ( grupoAnterior.length() > 0 && !grupoActual.equals(grupoAnterior) ){
                    sb.append("<th colspan='"+contadorColSpan+"' "+grupoAnterior);
                    contadorColSpan = 0;
                }
                contadorColSpan++;
                grupoAnterior = grupoActual;
            }
            sb.append("<th colspan='"+contadorColSpan+"' "+grupoAnterior);
            sb.append("</tr>");
            filaTitulos.append("</tr>");
            sb.append(filaTitulos);
            cgr.eliminarDetalle();
            return sb.toString();
        }
        catch(Exception ex){
            logg(ex);
            return null;
        }
    }
    
    /**
     * Busca el detalle de los campos del reporte dado, pero solo trae el detalle de los campos dados.
     * @param codReporte El codigo del reporte a buscar
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos
     * @autor Alejandro Payares
     */
    public void buscarDetalleDeCampos(String codReporte, String [] campos)throws SQLException {
        cgr.buscarDetalleDeCampos(codReporte, campos);
    }
    
    /**
     * Devuelve el detalle de los campos del reporte buscado por el metodo buscarDetalleDeCampos
     * @return Un Vector que contiene un Hashtable por cada fila encontrada para el reporte.
     * @autor Alejandro Payares
     */
    public Hashtable obtenerDetalleDeCampos(){
        return cgr.obtenerDetalleDeCampos();
    }
    
    /**
     * Devuelve el valor del peso real cargado de la remesa dada
     * @param numrem El numero de la remesa
     * @throws SQLException Si algun error ocurre en la base de datos
     * @return El texto con el valor real cargado con la unidad
     */
    private String obtenerPesoCargado(String numrem) throws SQLException {
        PreparedStatement ps = null;
        String valor = "";
        String unidad = "";
        try {
            ps = this.crearPreparedStatement("SQL_OBTENER_CARGA_REAL");
            ps.setString(1, numrem);
            ResultSet rs = ps.executeQuery();
            if ( rs.next() ) {
                valor = rs.getString("valor");
                unidad = rs.getString("unidad");
                /*try {
                    valor = ""+Integer.parseInt(valor);
                }
                catch( NumberFormatException ignorar ){}*/
                valor = valor + " " + noNull(unidad);
            }
            
        }
        finally {
            if(ps != null){ ps.close(); }
            this.desconectar("SQL_OBTENER_CARGA_REAL");
            return valor;
        }
    }
    
    /**
     * busca el nuemro del documento
     * @param numrem El numero del documento
     * @throws 
     * @return true si lo encuentra de lo contrario false
     */
    ///-------------------< Diogenes >---------------------
    public boolean existeDocumento(String lisdoc, String doc){
        boolean estado =  false;
        String [] datos = null;
        ////System.out.println(doc.length());
        if(lisdoc.length()>0){
            datos = lisdoc.split("-");
            for(int i=0; i < datos.length ;i++){
                if( doc.toUpperCase().equals(datos[i].trim().toUpperCase()) ){
                    estado =  true;
                }
            }
        }
        return estado;
    }
    
    
     /**
     * Devuelve la fecha de carga buscandola en planilla_tiempo
     * @param numrem El numero de la remesa
     * @throws SQLException Si algun error ocurre en la base de datos
     * @return El texto con el valor real cargado con la unidad
     */
    private String obtenerFechaCarga(String numrem) throws SQLException {
        PreparedStatement ps = null;
        String valor = "";
        try {
            ps = this.crearPreparedStatement("SQL_FECHA_CARGA");
            ps.setString(1, numrem);
            ResultSet rs = ps.executeQuery();
            if ( rs.next() ) {
                valor = rs.getString("date_time_traffic");
                valor =  noNull(valor);
            }
            
        }
        finally {
            if(ps != null){ ps.close(); }
            this.desconectar("SQL_FECHA_CARGA");
            return valor;
        }
    }
    
    
     /**
     * Devuelve la fecha de carga buscandola en planilla_tiempo
     * @param numrem El numero de la remesa
     * @throws SQLException Si algun error ocurre en la base de datos
     * @return El texto con el valor real cargado con la unidad
     */
    private String obtenerFechaEntrega(String numrem) throws SQLException {
        PreparedStatement ps = null;
        String valor = "";
        try {
            ps = this.crearPreparedStatement("SQL_FECHA_ENTREGA");
            ps.setString(1, numrem);
            ResultSet rs = ps.executeQuery();
            if ( rs.next() ) {
                valor = rs.getString("fechareporte");
                valor =  noNull(valor);
            }
            
        }
        finally {
            if(ps != null){ ps.close(); }
            this.desconectar("SQL_FECHA_CARGA");
            return valor;
        }
    }
    
      /**
     * Obtiene una lista con la informacion de planilla, placa(s)
     * y conductor(es) en el reporte de informaci�n al cliente,
     * utilizando como criterio de selecci�n el c�digo de la remesa.
     * @param codRemesa N�mero de la remesa.
     * @return Lista de planillas asociadas a la remesa.
     * @throws SQLException si aparece un error en la base de datos
     */
    private Vector getInfoPlanillas(String codRemesa) throws SQLException {
        ResultSet rs = null;
        PreparedStatement ps = null;
        Vector infoPlanillas = null;
        
        try {
            
            // Buscar los clientes y meterlos en una lista
            ps =  this.crearPreparedStatement("SQL_PLANILLA_DE_REMESA");
            ps.setString(1, codRemesa );
            rs = ps.executeQuery();
            infoPlanillas = new Vector();
            while( rs.next() ) {
                Hashtable infoPlanilla = new Hashtable();
                infoPlanilla.put("tipoViaje", noNull(rs.getString("tipoViaje")));
                infoPlanilla.put("celularCon", noNull(rs.getString("celularCon")));
                infoPlanilla.put("oriPla", noNull(rs.getString("oriPla")));
                infoPlanilla.put("numPla", noNull(rs.getString("numPla")));
                infoPlanilla.put("plaVeh", noNull(rs.getString("plaVeh")));
                infoPlanilla.put("nomCon", noNull(rs.getString("nomCon")));
                infoPlanilla.put("capacidad", noNull(rs.getString("capacidad")));
                infoPlanilla.put("tipo", noNull(rs.getString("tipo")));
                //diogenes
                infoPlanilla.put("desPla",noNull(rs.getString("desPla")));
                infoPlanilla.put( "contenedores", noNull(rs.getString("contenedores")));//Osvaldo
                infoPlanillas.addElement(infoPlanilla);
                
                
            }
            return infoPlanillas;
        } finally {
            if (rs != null) rs.close();
            if ( ps != null ) ps.close();
            desconectar("SQL_PLANILLA_DE_REMESA");
        }
        
    }
    /**
     * Busca las planillas, conductores y celulares de los conductores que intervienen
     * en los diferentes tramos del viaje. estos datos encontrados son agregados a la filaReporte
     * y son preparados con un formato especial exigido por el reporte.
     * @param filaReporte la fila del reporte de la cual se obtienen el numero de la OT y la OT relacionada
     * para buscar los datos de las planillas.
     * @param ciudadDAO Un objeto para tener acceso a los datos de la tabla de ciudad, se recibe por par�metro
     * para evitar la creaci�n multiple del DAO, lo cual perjudicar�a el rendimiento del reporte.
     * @throws SQLException si algun error ocurre en el acceso a la base de datos
     */
    private void obtenerDatosDePlanilla(Hashtable filaReporte,CiudadDAO ciudadDAO)throws SQLException{
        //Diogenes
        String nroPlanilla = "";
        
        String antNumPla = "";
        StringBuffer numPlanillas = new StringBuffer(0);
        StringBuffer numPlacas = new StringBuffer(0);
        StringBuffer nomConductores = new StringBuffer(0);
        StringBuffer contenedores = new StringBuffer(0);//Osvaldo
        StringBuffer capacidades = new StringBuffer(0);
        StringBuffer tipos = new StringBuffer(0);
        String corchetes[] = {"[","]","",""};
        int ABIERTO = 0, CERRADO = 1;
        String remesas [] = {""+filaReporte.get("ot_rela"),""+filaReporte.get("numrem")};
        for( int i=0; i<remesas.length; i++ ){
            Vector planillas = getInfoPlanillas(remesas[i]);
            Iterator ite = planillas.listIterator();
            for( int j=0, kdx = 0; j<planillas.size(); j++){
                Hashtable planilla = (Hashtable) planillas.elementAt(j);
                String celConductor = planilla.get("celularCon").toString().trim();
                if( !planilla.get("tipoViaje").toString().toUpperCase().equals("VAC") ) {
                    String saltoLinea = (kdx < planillas.size() - 1 ? "\n<br>" : "");
                    String tramo = "Tramo ";
                    String numpla = planilla.get("numPla").toString();
                    
                    if( planillas.size() > 1 ) {
                        com.tsp.operation.model.beans.Ciudad ciudad = ciudadDAO.obtenerCiudad( planilla.get("desPla").toString() );
                        String pais = "  ";
                        if (ciudad != null) {
                            pais = ciudad.getPais();
                        }
                        tramo += pais + ": ";
                        if( !antNumPla.equalsIgnoreCase(numpla) ) {
                            numPlanillas.append( corchetes[ABIERTO] + tramo + numpla +corchetes[CERRADO] + saltoLinea );
                            antNumPla = numpla;
                            //Diogenes
                            nroPlanilla += numpla+",";
                        }
                        numPlacas.append( corchetes[ABIERTO] + tramo + planilla.get("plaVeh") + corchetes[CERRADO] + saltoLinea );
                        capacidades.append( corchetes[ABIERTO] + tramo + planilla.get("plaVeh") + ": " + planilla.get("capacidad") + corchetes[CERRADO] + saltoLinea );
                        tipos.append( corchetes[ABIERTO] + tramo + planilla.get("plaVeh") + ": " + planilla.get("tipo") + corchetes[CERRADO] + saltoLinea );
                        nomConductores.append( corchetes[ABIERTO] + tramo + planilla.get("nomCon") +
                        " (" + (celConductor.equals("") ? "-" : celConductor) +
                        ")" + corchetes[CERRADO] + saltoLinea );
                        contenedores.append( corchetes[ABIERTO] + planilla.get("contenedores") +  corchetes[CERRADO] + saltoLinea );//Osvaldo
                    }else{
                        numPlanillas.append( corchetes[ABIERTO] + numpla + corchetes[CERRADO] + "\n<br>" );
                        numPlacas.append( corchetes[ABIERTO] + planilla.get("plaVeh") + corchetes[CERRADO] + "\n<br>" );
                        capacidades.append( corchetes[ABIERTO] + planilla.get("plaVeh") + ": " + planilla.get("capacidad") + corchetes[CERRADO] + "\n<br>" );
                        tipos.append( corchetes[ABIERTO] + planilla.get("plaVeh") + ": " + planilla.get("tipo") + corchetes[CERRADO] + "\n<br>" );
                        nomConductores.append( corchetes[ABIERTO] + planilla.get("nomCon") +
                        " (" + (celConductor.equals("") ? "-" : celConductor) + ")" + corchetes[CERRADO] + "\n<br>" );
                        //Diogenes
                        nroPlanilla += numpla+",";
                        contenedores.append( corchetes[ABIERTO] + planilla.get("contenedores") +  corchetes[CERRADO] + saltoLinea );//Osvaldo
                    }
                }
                kdx++;
            }
            ABIERTO += 2;
            CERRADO += 2;
        }
        
        
        if( numPlanillas.length() == 0 ) {
            numPlanillas.append( "-" );
        }
        if( numPlacas.length() == 0 ) {
            numPlacas.append( "-" );
        }
        if( nomConductores.length() == 0 ) {
            nomConductores.append( "-" );
        }
        if( capacidades.length() == 0 ) {
            capacidades.append( "-" );
        }
        if( tipos.length() == 0 ) {
            tipos.append( "-" );
        }
        //Osvaldo
        if( contenedores.length() == 0 ){
            contenedores.append( "-" );
        }
        filaReporte.put("planillas", numPlanillas.toString() );
        filaReporte.put("placas", numPlacas.toString() );
        filaReporte.put("conductores", nomConductores.toString() );
        filaReporte.put("capacidad", capacidades.toString() );
        filaReporte.put("tipo_vehiculo", tipos.toString() );//peso_cargado,capacidad,tipo_vehiculo
        filaReporte.put("contenedores" , contenedores.toString() );//Osvaldo
        //Diogenes
        filaReporte.put("nroPlanilla",nroPlanilla);
        
    }
    
}

