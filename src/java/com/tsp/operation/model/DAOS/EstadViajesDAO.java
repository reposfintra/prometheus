/********************************************************************
 *      Nombre Clase.................   EstadViajesDAO.java
 *      Descripci�n..................   Dao de la tabla estadistica_viajes
 *      Autor........................   Ing. Tito Andr�s Maturana De La Cruz
 *      Fecha........................   15 de febrero de 2006, 02:09 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.Vector;
import java.util.Hashtable;
import com.tsp.util.UtilFinanzas;

/**
 *
 * @author  Ing. Tito Andr�s Maturana De La Cruz
 */
public class EstadViajesDAO {
    
    private Vector vector;
    private EstadViajes nviajes;
    
    private static final String SQL_ESTADISTICAS_VIAJES = 
            "SELECT COALESCE(nit, '') AS nit, " +
            "       COALESCE(codtipocarga, '') AS codtipocarga, " +
            "       tipo, " +
            "       COUNT(pl.numpla) AS nviajes " +
            "FROM ( " +
            "   (" +
            "       SELECT  numpla, " +
            "               cedcon AS nit, " +
            "               'C' AS tipo " +
            "       FROM    planilla " +
            "       WHERE   fecdsp between ? AND ? AND cia=? AND reg_status!='A' " +
            "   ) UNION ( " +
            "       SELECT  numpla, " +
            "               nitpro AS nit, " +
            "               'P' AS tipo " +
            "       FROM    planilla " +
            "       WHERE   fecdsp between ? AND ? AND cia=? AND reg_status!='A' " +
            "   )" +
            ")pl " +
            "JOIN plarem plr ON ( plr.numpla=pl.numpla ) " +
            "JOIN remesa rem ON ( rem.numrem=plr.numrem ) " +
            "GROUP BY COALESCE(nit,''), COALESCE(codtipocarga,''), tipo ";
    
    private static final String SQL_INSERT_A =
            "INSERT INTO estadistica_viajes(dstrct, ano, tipo, codtipocarga, nit, creation_date, creation_user, base, n_viajes_";
    private static final String SQL_INSERT_B = 
            ") VALUES (?, ?, ?, ?, ?, 'now()', ?, ?, ?)";
    
    private static final String SQL_UPDATE_A = 
            "UPDATE estadistica_viajes SET n_viajes_";
    private static final String SQL_UPDATE_B = 
            "=? WHERE  dstrct=? AND ano=? AND tipo=? AND codtipocarga=? AND nit=?";
    
    private static final String SQL_EXISTE = 
            "SELECT reg_status " +
            "FROM estadistica_viajes " +
            "WHERE dstrct=? AND ano=? AND tipo=? AND codtipocarga=? AND nit=?";
    
    private static final String SQL_LISTAR_DESDE_ANO =
            "SELECT ano, " +
            "       nit, " +
            "       SUM(n_viajes_01) AS n_viajes_01, " +
            "       SUM(n_viajes_02) AS n_viajes_02, " +
            "       SUM(n_viajes_03) AS n_viajes_03, " +
            "       SUM(n_viajes_04) AS n_viajes_04, " +
            "       SUM(n_viajes_05) AS n_viajes_05, " +
            "       SUM(n_viajes_06) AS n_viajes_06, " +
            "       SUM(n_viajes_07) AS n_viajes_07, " +
            "       SUM(n_viajes_08) AS n_viajes_08, " +
            "       SUM(n_viajes_09) AS n_viajes_09, " +
            "       SUM(n_viajes_10) AS n_viajes_10, " +
            "       SUM(n_viajes_11) AS n_viajes_11, " +
            "       SUM(n_viajes_12) AS n_viajes_12 " +
            "FROM   estadistica_viajes  " +
            "WHERE  ano>=? AND tipo=? AND dstrct=? " +
            "GROUP BY ano, nit " +
            "ORDER BY nit, ano";
    
    private static final String SQL_UPDATE_TOTALVIAJES = 
            "UPDATE conductor SET totalviajes=?, fechaultact='now()', usuario=? WHERE cedula=?;";

    /** Crea una nueva instancia de  EstadViajesDAO */
    public EstadViajesDAO() {
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public  synchronized java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public  synchronized void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
    /**
     * Obtiene la cantidad de viajes por Propietario/Conductor y Tipo de carga.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param ano A�o del per�odo
     * @param mes Mes del per�odo
     * @param cia Distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public synchronized void obtenerViajes(int ano, String mes, String cia) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vector = new Vector();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                
                
                st = con.prepareStatement(this.SQL_ESTADISTICAS_VIAJES);
                String fechai = ano + "-" + mes + "-01";
                String fechaf = ano + "-" + mes + "-" + UtilFinanzas.diaFinal(ano + mes);
                st.setString(1, fechai);
                st.setString(2, fechaf);
                st.setString(3, cia);
                st.setString(4, fechai);
                st.setString(5, fechaf);
                st.setString(6, cia);
                
                //////System.out.println("................ query: " + st);
                
                rs = st.executeQuery();
                
                while (rs.next()){
                    this.nviajes = new EstadViajes();
                    this.nviajes.setNit(rs.getString("nit"));
                    this.nviajes.setCodtipocarga(rs.getString("codtipocarga"));
                    this.nviajes.setTipo(rs.getString("tipo"));
                    this.nviajes.setCant_mes(rs.getInt("nviajes"));
                    
                    this.vector.add(this.nviajes);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN OBTENER LOS VIAJES DE PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        } catch (Exception e){
            throw new SQLException("ERROR EN OBTENER LOS VIAJES DE PLANILLA " + e.getMessage());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Getter for property nviajes.
     * @return Value of property nviajes.
     */
    public synchronized com.tsp.operation.model.beans.EstadViajes getNviajes() {
        return nviajes;
    }
    
    /**
     * Setter for property nviajes.
     * @param nviajes New value of property nviajes.
     */
    public  synchronized void setNviajes(com.tsp.operation.model.beans.EstadViajes nviajes) {
        this.nviajes = nviajes;
    }
    
    /**
     * Verifica si existe un registro en el archivo estadistica_viajes
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public synchronized boolean existe() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean existe = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                
                st = con.prepareStatement(this.SQL_EXISTE);
                st.setString(1, this.nviajes.getDstrct());
                st.setInt(2, this.nviajes.getAno());
                st.setString(3, this.nviajes.getTipo());
                st.setString(4, this.nviajes.getCodtipocarga());
                st.setString(5, this.nviajes.getNit());
                
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    existe = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EN OBTENER DEL ARCHIVO estadistica_viajes " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return existe;
    }
    
    /**
     * Inserta un registro en el archivo estadistica_viajes
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public synchronized void insertar() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                
                String sql = this.SQL_INSERT_A + this.nviajes.getMes() + this.SQL_INSERT_B;
                st = con.prepareStatement(sql);
                
                
                
                st.setString(1, this.nviajes.getDstrct());
                st.setInt(2, this.nviajes.getAno());
                st.setString(3, this.nviajes.getTipo());
                st.setString(4, this.nviajes.getCodtipocarga());
                st.setString(5, this.nviajes.getNit());
                st.setString(6, this.nviajes.getCreation_user());
                st.setString(7, this.nviajes.getBase());
                st.setInt(8, this.nviajes.getCant_mes());                
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EN EL ARCHIVO estadistica_viajes " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Actualiza un registro en el archivo estadistica_viajes
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public synchronized void actualizar() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                String sql = this.SQL_UPDATE_A + this.nviajes.getMes() + this.SQL_UPDATE_B;
                st = con.prepareStatement(sql);
                
                st.setInt(1, this.nviajes.getCant_mes());                
                st.setString(2, this.nviajes.getDstrct());
                st.setInt(3, this.nviajes.getAno());
                st.setString(4, this.nviajes.getTipo());
                st.setString(5, this.nviajes.getCodtipocarga());
                st.setString(6, this.nviajes.getNit());               
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR EN EL ARCHIVO estadistica_viajes " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Obtiene todos los registro del archivo estadistica_viajes cuyo a�o sea mator o igual que el suministrado.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param ano A�o. 
     * @param tipo C - Conductor, P - Propietario.
     * @param cia Distrito
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public synchronized void listar(int ano, String tipo, String cia) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vector = new Vector();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                
                st = con.prepareStatement(this.SQL_LISTAR_DESDE_ANO);
                st.setInt(1, ano);
                st.setString(2, tipo);
                st.setString(3, cia);
                               
                rs = st.executeQuery();
                
                //////System.out.println("................... LISTAR DESDE A�O: " + st);
                
                while (rs.next()){
                    EstadViajes obj = new EstadViajes();
                    obj.LoadGroupByNitAno(rs);
                    obj.setDstrct(cia);
                    this.vector.add(obj);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL LISTAR DEL ARCHIVO estadistica_viajes " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Actualiza el total de viajes en el archivo conductor
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param nit Nit del conductor
     * @param nviajes Cantidad de viajes realizados 
     * @param user Login del usuario que actualiza
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public synchronized void actualizarViajes(String nit, int nviajes, String user) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                
                st = con.prepareStatement(this.SQL_UPDATE_TOTALVIAJES);
                
                st.setInt(1, nviajes);
                st.setString(2, user);
                st.setString(3, nit);
                
                //////System.out.println(".................... ACTUALIZACION VIAJES: " + st.toString());
                
                boolean autocommit = con.getAutoCommit();
                st.executeUpdate();
                st.executeBatch();
                con.commit();
                con.setAutoCommit(autocommit);           
                
                
            }
        }catch(SQLException e){
            con.rollback();
            throw new SQLException("ERROR AL ACTUALIZAR EL TOTAL DE VIAJES EN EL ARCHIVO CONDUCTOR " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
}
