/***********************************************************************************
 * Nombre clase :                 ReporteVehPerdidosDAO.java                       *
 * Descripcion :                  Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD para generar los datos del reporte     *
 *                                Vehiculos perdidos.                              *
 * Autor :                        Ing. Juan Manuel Escandon Perez                  *
 * Fecha :                        10 de enero de 2006, 10:59 AM                    *
 * Version :                      1.0                                              *
 * Copyright :                     Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.MainDAO;



public class ReporteVehPerdidosDAO extends MainDAO {
    
    /** Creates a new instance of ReporteVehPerdidosDAO */
    public ReporteVehPerdidosDAO() {
        super("ReporteVehPerdidosDAO.xml");
    }
    
    private static String SQLCONSULTA =     "   SELECT DISTINCT placa_rec as placa , numpla_rec as planilla, "+
                                            "   fecha_disp_rec as disponibilidad, get_nombreciudad(origen_rec) as origen,       "+
                                            "   get_nombreciudad(destino_rec) as destino,                                       "+
                                            "   get_nombreciudad(agasoc) as agencia,tipo_recurso_rec as tipo                    "+
                                            "   FROM registro_asignacion                                                        "+  
                                            "   where placa_rec <> '' AND reg_status_rec <> 'A'                                 "+
                                            "   order by placa_rec                                                              ";
    
    
    /**
     * Metodo listVehPerdidos, lista todas las recursos que no han sido asignados aun requerimiento de un cliente
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public List listVehPerdidos() throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        String query = "SQLCONSULTA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(ReporteVehPerdidos.load(rs));
                }
            }
            }}catch(Exception e){
            throw new SQLException("Error en rutina listVehPerdidos [ReporteVehPerdidosDAO].... \n"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
}
