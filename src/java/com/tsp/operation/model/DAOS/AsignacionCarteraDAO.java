/*
     JPACOSTA. Febrero 2014
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.Usuario;

/**
 * @author jpacosta
 */
public interface AsignacionCarteraDAO {

    /**
     * @param tipo
     * @param unidad_negocio 
     * @param rangoMayor
     * @param periodo
     * @param reasigna
     * @return 
     */
    public abstract String getNegocios(String tipo, String unidad_negocio, String rangoMayor, String periodo, String reasigna, String agencia);
    
    /**
     * @param unidad_negocio
     * @param periodo
     * @param agente
     * @return 
   
    public abstract String obtenerNegocios(String unidad_negocio, String periodo, String agente);*/
    
    /**
     * @param unidades
     * @param periodos
     * @param vencimientos
     * @param asesores
     * @param agentes
     * @return 
     */
    public abstract String getFiltros(boolean unidades, boolean agencias, boolean periodos, boolean vencimientos, boolean asesores, boolean agentes);
    
    /**
     * @param tipo
     * @param periodo
     * @param asesor
     * @param usuario
     * @param negocios
     * @return 
     */
    public abstract String asignar(String tipo, String periodo, String asesor, String usuario, String negocios);
    
    /**
     *
     * @param user
     * @param filtro
     * @param agente
     * @return
     */
    public abstract String getFichas(Usuario user, String filtro,String agente, String campoOrden, String agencia);
    
    /**
     *
     * @param tipo
     * @param periodo
     * @param unegocio
     * @return
     */
    public abstract String resumen(String tipo, String periodo, String unegocio);
    
    /**
     *
     * @param periodo
     * @param agente
     * @param vencimiento
     * @param unegocio
     * @param juridica
     * @param ciclo
     * @param user
     * @return
     */
    public abstract String previsualizarFichas(String periodo, String agente, String vencimiento, String unegocio,String juridica,String ciclo,Usuario user, String agencia);
    
    
}
