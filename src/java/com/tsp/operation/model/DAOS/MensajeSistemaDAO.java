/*
 * MensajeSistemaDAO.java
 *
 * Created on 3 de abril de 2008
 */

package com.tsp.operation.model.DAOS;
/*
import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import java.io.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.lang.*;*/
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import com.tsp.operation.model.beans.MensajeSistema;//20100722

/**
 * @author NAVI
 */
public class MensajeSistemaDAO extends MainDAO  {
    
   
    public MensajeSistemaDAO() {
        super("MensajeSistemaDAO.xml");   
    }
   
    public MensajeSistemaDAO(String dataBaseName) {
        super("MensajeSistemaDAO.xml", dataBaseName);   
    }
   
    public String getMensajeSistema(){
        String respuesta=".";
        String sw="";
        PreparedStatement ps = null;
        try{            
            ps = this.crearPreparedStatement("SQL_GETMENSAJE_SISTEMA");
            ResultSet rs = null;
          
            rs = ps.executeQuery();
           
            if (rs.next()) {
                sw=rs.getString("referencia");
                //ystem.out.println("sw"+sw);
                if (sw.equals("2")){
                    respuesta="2msg"+rs.getString("dato");
                }
                if (sw.equals("1")){
                    respuesta="1msg"+rs.getString("dato");
                }
            }
        }
        catch(Exception e){
            System.out.println("error al obtener mensaje de sistema" +e.toString()+"...."+e.getMessage());                      
        }
        finally{
            if ( ps != null ){
                try {ps.close();}catch(SQLException ex){
                    System.out.println("error en ps.close");
                }
            }
            try {desconectar("SQL_GETMENSAJE_SISTEMA");}catch(SQLException ex){
                System.out.println("error en desconectar");
            }
            
        }
        //ystem.out.println("respuesta"+respuesta);
        return respuesta;        

    }
    
    public String  actualizarMensajeSistema() {
        String respuesta="";
        Connection         con     = null;
        PreparedStatement  st      = null;
        
        String             query   = "SQL_UPDATEMENSAJE_SISTEMA";
        
        try{
            //ystem.out.println("en dao actualizar");
            con = this.conectar(query);
            
            String sql    =   this.obtenerSQL( query );
            
            st = con.prepareStatement( sql );
                        
            int affected =st.executeUpdate();
            if (affected>0){
                respuesta="ok";
                
            }else{
                respuesta="full raro...";
                
            }
                        
        }catch(Exception e){
            System.out.println("error en actualizarMensajeSistema..."+e.toString()+"__"+e.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ System.out.println("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ System.out.println("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }

    public MensajeSistema obtainMensajeSistema(String loginn, String hora_actual) throws Exception{//obtiene variables del sistema
        MensajeSistema mensajeSistema=new MensajeSistema();
        PreparedStatement st       = null;
        Connection        con      = null;
        try{            
            ResultSet rs = null;
            String            query    = "SQL_GET_DATOS_SYSTEM_USR";

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,hora_actual);
            st.setString(2,hora_actual);
            st.setString(3,loginn);
            rs = st.executeQuery();
            //System.out.println("sql"+sql+"hora_actual"+hora_actual+"loginn"+loginn);
            if (rs.next()){
                mensajeSistema.setEstadoMensaje(rs.getString("estado_mensaje"));
                mensajeSistema.setMensaje(rs.getString("mensaje"));
                mensajeSistema.setTipoMensaje(rs.getString("tipo_mensaje"));
                String cadfrecuencias=rs.getString("frecuencias");
                String[] frecuencias=cadfrecuencias.split(",");
                mensajeSistema.setFrecuenciaRelojJs(Integer.parseInt(frecuencias[0]));
                mensajeSistema.setFrecuenciaRelojServer(Integer.parseInt(frecuencias[1]));
                mensajeSistema.setFrecuenciaConsultaBd(Integer.parseInt(frecuencias[2]));
                String cadestetica=rs.getString("estetica_mensaje");
                String[] estetica=cadestetica.split(",");
                mensajeSistema.setAncho(Integer.parseInt(estetica[0]));
                mensajeSistema.setAltura(Integer.parseInt(estetica[1]));
                mensajeSistema.setCss(estetica[2]);
                mensajeSistema.setShowProgress(estetica[3]); 
                mensajeSistema.setMostrarSegundos(estetica[4]);

            }            
        }catch(Exception ee){
            System.out.println("error en obtainMensajeSistema:"+ee.toString());
            ee.printStackTrace();
            throw new Exception("ERROR DURANTE obtainMensajeSistema. \n " + ee.toString());
        }finally{
            st.close();
            this.desconectar(con);
        }
        return mensajeSistema;
    }

    public String obtainCiaMostrable(String loginn) throws Exception{
        String respuesta="";
        PreparedStatement st       = null;
        Connection        con      = null;
        try{
            ResultSet rs = null;
            String            query    = "SQL_GET_CIA_MOSTRABLE_USR";

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,loginn);
            rs = st.executeQuery();
            if (rs.next()){
                respuesta=rs.getString("ciamostrable");
            }
        }catch(Exception ee){
            System.out.println("error en obtainCiaMostrable:"+ee.toString());
            ee.printStackTrace();
            throw new Exception("ERROR DURANTE obtainCiaMostrable. \n " + ee.toString());
        }finally{
            st.close();
            this.desconectar(con);
        }
        return respuesta;
    }

}
