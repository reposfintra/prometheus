/********************************************************************
 *  Nombre Clase.................   CitaCargueDAO.java
 *  Descripci�n..................   DAO para manejar las citas de cargue de carbon
 *  Autor........................   Ing. Karen Reales
 *  Fecha........................   08.02.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.*;
import java.net.URL;

public class CitaCargueDAO {
    
    CitaCargue cita;
    Vector cargue;
    Vector descargue;
    Vector causas;
    Vector placas;
    private static Properties dbProps = new Properties();
    
    String SQL_INSERT="insert into citas_cargue (plaveh, tipo, causa, observacion,user_update,creation_user,fecha_pla) values (?,?,?,?,?,?,?)";
    
    String SQL_UPDATE="update citas_cargue set  causa=? ,observacion=?,user_update=?,numpla=?, fecha_real=? where plaveh=? and tipo=? and  numpla=''";
    
    String SQL_BUSCAR="select * from citas_cargue where plaveh = ? and tipo =? and numpla=?";
    
    
    String SQL_REPORTE_CITACARGUE="select  totalc.total," +
    "                totald.total," +
    "                ccargue.ccumplida," +
    "                dcargue.ccumplida," +
    "                (ccargue.ccumplida/ totalc.total:: numeric)*100 as cumplimiento_cargue," +
    "                (dcargue.ccumplida/ totald.total:: numeric)*100 as cumplimiento_dcargue" +
    "                from 	(select count(plaveh) as total from citas_cargue where creation_date between ? and ? and tipo ='C') as totalc,      " +
    "                (select count(plaveh) as total from citas_cargue where creation_date between ? and ? and tipo ='D') as totald," +
    "                (select count(plaveh) as ccumplida from citas_cargue where creation_date between ? and ? and causa='' and tipo ='C') as ccargue," +
    "                (select  count(plaveh) as ccumplida from citas_cargue where creation_date between ? and ? and causa='' and tipo ='D') as dcargue";
    
    
    String SQL_CAUSAS_CITACARGUE = "select causas.descripcion," +
    "       cargue.cant/totalc.total:: numeric*100 as particcar" +
    " from (	SELECT  a.table_code as codigo,   " +
    "		a.DESCRIPCION, " +
    "		A.DATO " +
    "	FROM  	tablagen a, " +
    "		tablagen_prog b " +
    "	WHERE   a.REG_STATUS!='A'  " +
    "		AND a.table_type='CS'  " +
    "		and a.table_type=b.table_type " +
    "		and a.table_code = b.table_code " +
    "		AND b.program ='CITACARGUE' ) causas" +
    " left outer join (select count(plaveh) as cant, tipo, causa from  citas_cargue where creation_date between ? and ? group by tipo,causa)cargue " +
    "	on (cargue.tipo =? and cargue.causa = causas.codigo)," +
    " (select count(plaveh) as total from citas_cargue where  creation_date between ? and ? and  tipo =? and causa<>'' ) as totalc";
    
    
    String SQL_REPORTE_RESUMENPLACA ="select  ccargue.plaveh, " +
    "	get_nombrenit(nitpro) as nombre," +
    "    	(ccargue.ccumplida/ totalc.total:: numeric )*100 as incumplimiento_cargue, " +
    "	ccargue.ccumplida as cantIncCargue," +
    "    	(dcargue.ccumplida/ totald.total:: numeric )*100 as incumplimiento_dcargue," +
    "	dcargue.ccumplida as cantIncDCargue" +
    "	from 	(select count(citas_cargue.plaveh) as ccumplida, " +
    "			citas_cargue.plaveh," +
    "			nitpro" +
    "		from 	citas_cargue" +
    "		inner join planilla on (planilla.numpla =citas_cargue.numpla )" +
    "		where 	citas_cargue.creation_date between ? and ?  and causa<>'' and tipo ='C' group by citas_cargue.plaveh, planilla.nitpro) as ccargue " +
    "	left outer join  " +
    "    	(select count (plaveh) as ccumplida,plaveh from citas_cargue where creation_date between ? and ? and causa<>'' and tipo ='D' group by plaveh) as dcargue " +
    "    	on (dcargue.plaveh=ccargue.plaveh), " +
    "   	(select count(plaveh) as total from citas_cargue where creation_date between ? and ? and causa<>'' and tipo ='C') as totalc, " +
    "    	(select count(plaveh) as total from citas_cargue where creation_date between ? and ?  and causa<>'' and tipo ='D') as totald " +
    " order by plaveh";
    
    
    String SQL_REPORTE_PLANPLACA ="select plaveh, fecha_pla from citas_cargue where creation_date between ? and 'now()' and fecha_real = '0099-01-01 00:00:00' and tipo = 'C' order by fecha_pla ";
    /** Creates a new instance of CumplidoDAO */
    public CitaCargueDAO() {
    }
    
    /**
     * Getter for property cita.
     * @return Value of property cita.
     */
    public com.tsp.operation.model.beans.CitaCargue getCita() {
        return cita;
    }
    
    /**
     * Setter for property cita.
     * @param cita New value of property cita.
     */
    public void setCita(com.tsp.operation.model.beans.CitaCargue cita) {
        this.cita = cita;
    }
    
    /**
     * Metodo insertCitaCargue ,Metodo que actualiza un registro en la tabla de citas de cargue
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @version : 1.0
     */
    
    public void insertCitaCargue()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_UPDATE);
                st.setString(1,cita.getCausa());
                st.setString(2,cita.getObservacion());
                st.setString(3,cita.getUsuario());
                st.setString(4,cita.getNumpla());
                st.setString(5,cita.getFecha_pla());
                st.setString(6,cita.getPlaca());
                st.setString(7,cita.getTipo());
                ////System.out.println("Sql ="+st);
                st.executeUpdate();
                AutorizacionFleteDAO aflete = new AutorizacionFleteDAO();
                aflete.insertControlActualizacion(st.toString()+";", "");
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS CITAS DE CARGUE... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     * Metodo buscarCitaCargue , Metodo que busca una cita de cargue.
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String placa
     * @parameter : String numpla
     * @parameter : String tipo  si es cargue C si es descargue D
     * @version : 1.0
     */
    
    public void buscarCitaCargue(String placa,String numpla,String tipo)throws SQLException{
        cita =null;
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_BUSCAR);
                st.setString(1,placa);
                st.setString(2,tipo);
                st.setString(3,numpla);
                rs = st.executeQuery();
                if(rs.next()){
                    cita = new CitaCargue();
                    cita.setPlaca(rs.getString("plaveh"));
                    cita.setFecha_pla(rs.getString("fecha_pla"));
                    cita.setFecha_real(rs.getString("fecha_real"));
                    cita.setTipo(rs.getString("tipo"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CITAS DE CARGUE... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo insertCitaCargue, Metodo que inserta un nuevo registro en la tabla de citas de cargue
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @retorna Un string con el comando slq para utilzarlo en una transaccion
     * @version : 1.0
     */
    
    public String insertCitaCargueNueva()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_INSERT);
                st.setString(1,cita.getPlaca());
                st.setString(2,cita.getTipo());
                st.setString(3,cita.getCausa());
                st.setString(4,cita.getObservacion());
                st.setString(5,cita.getUsuario());
                st.setString(6,cita.getUsuario());
                st.setString(7,cita.getFecha_pla());
                sql = st.toString();
                AutorizacionFleteDAO aflete = new AutorizacionFleteDAO();
                aflete.insertControlActualizacion(st.toString()+";", "");
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS CITAS DE CARGUE... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    /**
     * Metodo reporteCitaCargue , Metodo que busca una lista con informacion acerca de
     * indicadores de cumplimiento de citas de cargue
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String fecini
     * @parameter : String fecfin
     * @version : 1.0
     */
    public void reporteCitaCargue(String fecini,String fecfin)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_REPORTE_CITACARGUE);
                st.setString(1,fecini);
                st.setString(2,fecfin);
                st.setString(3,fecini);
                st.setString(4,fecfin);
                st.setString(5,fecini);
                st.setString(6,fecfin);
                st.setString(7,fecini);
                st.setString(8,fecfin);
                rs = st.executeQuery();
                ////System.out.println("Query: "+st);
                cargue=new Vector();
                if(rs.next()){
                    cita = new CitaCargue();
                    cita.setCumplido_cargue(com.tsp.util.Util.redondear(rs.getDouble("cumplimiento_cargue"),2));
                    cita.setCumplido_dcargue(com.tsp.util.Util.redondear(rs.getDouble("cumplimiento_dcargue"),2));
                    cargue.add(cita);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CITAS DE CARGUE PARA EL REPORTE... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo causasCitaCargue , Metodo que busca una lista con informacion acerca de
     * indicadores de utilizacion de las causas de cita de cargue
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String tipo C para cargue, D para descargue
     * @parameter : String fecini
     * @parameter : String fecfin
     * @version : 1.0
     */
    public void causasCitaCargue(String tipo,String fecini,String fecfin)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        causas=new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_CAUSAS_CITACARGUE);
                st.setString(1, fecini);
                st.setString(2, fecfin);
                st.setString(3,tipo);
                st.setString(4, fecini);
                st.setString(5, fecfin);
                st.setString(6,tipo);
                rs = st.executeQuery();
                while(rs.next()){
                    TblGeneral tg = new TblGeneral();
                    tg.setDescripcion(rs.getString("DESCRIPCION"));
                    tg.setPorcentaje(com.tsp.util.Util.redondear(rs.getDouble("particcar"),2));
                    causas.add(tg);
                }
                
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CAUSAS DE CITAS DE CARGUE PARA EL REPORTE... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     * Metodo reporteCitaCarguePlaca , Metodo que busca una lista con informacion acerca de
     * las placas que incumplieron la cita de cargue
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String fecini
     * @parameter : String fecfin
     * @version : 1.0
     */
    public void reporteCitaCarguePlaca(String fecini,String fecfin)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        placas=new Vector();
        
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_REPORTE_RESUMENPLACA);
                st.setString(1,fecini);
                st.setString(2,fecfin);
                st.setString(3,fecini);
                st.setString(4,fecfin);
                st.setString(5,fecini);
                st.setString(6,fecfin);
                st.setString(7,fecini);
                st.setString(8,fecfin);
                rs = st.executeQuery();
                ////System.out.println("Query CUMPLIMIENTO CARGUES: "+st);
                while(rs.next()){
                    cita = new CitaCargue();
                    cita.setPlaca(rs.getString("plaveh"));
                    cita.setCumplido_cargue(com.tsp.util.Util.redondear(rs.getDouble("incumplimiento_cargue"),2));
                    cita.setCumplido_dcargue(com.tsp.util.Util.redondear(rs.getDouble("incumplimiento_dcargue"),2));
                    cita.setPropietario(rs.getString("nombre"));
                    cita.setInCargue(rs.getInt("cantIncCargue"));
                    cita.setInDCargue(rs.getInt("cantIncDCargue"));
                    placas.add(cita);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CITAS DE CARGUE PARA EL REPORTE... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     * Getter for property descargue.
     * @return Value of property descargue.
     */
    public java.util.Vector getDescargue() {
        return descargue;
    }
    
    /**
     * Setter for property descargue.
     * @param descargue New value of property descargue.
     */
    public void setDescargue(java.util.Vector descargue) {
        this.descargue = descargue;
    }
    
    /**
     * Getter for property cargue.
     * @return Value of property cargue.
     */
    public java.util.Vector getCargue() {
        return cargue;
    }
    
    /**
     * Setter for property cargue.
     * @param cargue New value of property cargue.
     */
    public void setCargue(java.util.Vector cargue) {
        this.cargue = cargue;
    }
    
    /**
     * Getter for property causas.
     * @return Value of property causas.
     */
    public java.util.Vector getCausas() {
        return causas;
    }
    
    /**
     * Setter for property causas.
     * @param causas New value of property causas.
     */
    public void setCausas(java.util.Vector causas) {
        this.causas = causas;
    }
    
    /**
     * Getter for property placas.
     * @return Value of property placas.
     */
    public java.util.Vector getPlacas() {
        return placas;
    }
    
    /**
     * Setter for property placas.
     * @param placas New value of property placas.
     */
    public void setPlacas(java.util.Vector placas) {
        this.placas = placas;
    }
    /**
     * Metodo reportePlacasCitaCargue , Metodo que busca una lista con informacion acerca de
     * las placas con fecha planeada de cargue en la mina, a partir de una fecha dada hasta hoy
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String fecini
     * @version : 1.0
     */
    public void reportePlacasCitaCargue(String fecini)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        cargue = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_REPORTE_PLANPLACA);
                st.setString(1, fecini);
                rs = st.executeQuery();
                ////System.out.println("Query: "+st);
                cargue=new Vector();
                while(rs.next()){
                    cita = new CitaCargue();
                    cita.setPlaca(rs.getString("plaveh"));
                    cita.setFecha_pla(rs.getString("fecha_pla"));
                    cargue.add(cita);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CITAS DE CARGUE PARA EL REPORTE DE MINA... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo cargarPropiedades, Metodo que carga en una variable
     * tipo properties los valores guardados en un archivo properties
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @version : 1.0
     */
    public void cargarPropiedades() throws IOException{
        InputStream is = getClass().getResourceAsStream("StoreInf.properties");
        dbProps.load(is);
    }
    /**
     * Metodo cargarPropiedades, Metodo que guarda en un archivo
     * tipo properties los valores guardados en una variable properties
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @version : 1.0
     */
    public void setUltimafechaCreacion() throws IOException{
        try{
            cargarPropiedades();
            java.util.Date hoy = new java.util.Date();
            java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
            String fecha = s.format(hoy);
            ////System.out.println("Fecha en el archivo "+dbProps.getProperty("fechaCargue"));
            //SE ACTUALIZAN LOS DATOS EN EL ARCHIVO DE CONFIGURACION..
            dbProps.setProperty("fechaCargue", fecha);
            URL u = getClass().getResource("StoreInf.properties");
            dbProps.store(new FileOutputStream(u.getPath()),"");
            ////System.out.println("Listo escribi en el archivo...");
            
        }catch(IOException e){
            throw new IOException("ERROR ESCRIBIENDO EN EL ARCHIVO DE PROPIEDADES LA FECHA DE CORTE "+e.getMessage());
        }
    }
    /**
     * Metodo cantCargues , Metodo que busca la cantidad de cargues o descargues planeado
     * y automaticos
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String fecini
     * @parameter : String fecfin
     * @parameter : String tipo
     * @version : 1.0
     */
    public void cantCargues(String fecini,String fecfin, String tipo)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT  cantM," +
                "	cantA," +
                "	cantM/(cantM+cantA) ::numeric *100 as porcM," +
                "	cantA/(cantM+cantA) ::numeric *100 as porcA" +
                " FROM 	(select count(plaveh) as cantM" +
                "	 from 	citas_cargue " +
                "	 where 	creation_date between ?  and ? " +
                "		and causa <> 'FPANI' " +
                "       	and tipo =?) cargueM," +
                "	(select count(plaveh) as cantA" +
                "	 from 	citas_cargue " +
                "	 where 	creation_date between ? and ? " +
                "		and causa = 'FPANI' " +
                "		and tipo =?) cargueA");
                st.setString(1,fecini);
                st.setString(2,fecfin);
                st.setString(3,tipo);
                st.setString(4,fecini);
                st.setString(5,fecfin);
                st.setString(6,tipo);
                ////System.out.println("Query CANT MANUAL: "+st);
                rs = st.executeQuery();
                while(rs.next()){
                    cita = new CitaCargue();
                    cita.setPorcCargueM(com.tsp.util.Util.redondear(rs.getDouble("porcM"),2));
                    cita.setPorcCargueA(com.tsp.util.Util.redondear(rs.getDouble("porcA"),2));
                    cita.setCantCargueA(rs.getInt("cantA"));
                    cita.setCantCargueM(rs.getInt("cantM"));
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CITAS DE CARGUE MANUALES Y AUTOMATICAS... " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
}
