/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

/**
 *
 * @author Ing Jesus Pinedo
 * WebSErvice Mensaje De texto Celular
 */

import com.tsp.operation.model.beans.Sms;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.net.ssl.HttpsURLConnection;



import java.io.IOException;


import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import com.tsp.operation.model.beans.TablaGen;
import com.tsp.operation.model.services.TablaGenManagerService;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class SmsDAO extends MainDAO{
Calendar calendario = new GregorianCalendar();
int hora =calendario.get(Calendar.HOUR_OF_DAY);


    public SmsDAO() {
        super("SmsDAO.xml");
    }
    public SmsDAO(String dataBaseName) {
        super("SmsDAO.xml", dataBaseName);
    }



/*------------------Enviar Mensaje De Alerta ------------------------------------*/
   public Sms envia_SMS_HTTP(Sms sms) throws Exception
   {
      //sms.setCell("3016996746");
      TablaGenManagerService tbgserv= new TablaGenManagerService (this.getDatabaseName());
      TablaGen tblgen=new TablaGen();     
       int ret=0;
       try
        {
            URL url_sms = new URL ("https://www.elibom.com/api/rest/Messages");
            String response="";
            OutputStreamWriter wr = null;
            BufferedReader rd = null;
            HttpsURLConnection con = (HttpsURLConnection)url_sms.openConnection();
            con.setRequestProperty ("Authorization", "Basic " + getEcoding());

             con.setDoInput(true);
             con.setDoOutput (true);
             wr = new OutputStreamWriter(con.getOutputStream());
             wr.write("destinations="+sms.getCell()+"&message="+sms.getSMS());
             wr.flush();


             BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
            //Recibir respuesta
             rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
             String line;
                while ((line = in.readLine()) != null)
                {
                response += line;
                }

            sms= this.getToken(response,sms);
            sms=Consulta_SMS_HTTP(sms);

            //validar hora
            if (hora >=8 && hora<9)
            {
                sms=Consulta_Cuenta_SMS_HTTP(sms);
                if((sms.getCreditos_cuenta()<Float.valueOf(this.Consultar_Dato_Sms("TOPE_CREDITOS")))  && (this.Consultar_Alerta_Sms().equals("NO")))
                {
                    //envio mensaje de alerta
                    List lista_dtn=this.ListarDestinatariosAlerta();
                    for (int i = 0; i < lista_dtn.size(); i ++)
                    {
                    Sms sms_alert= (Sms)lista_dtn.get(i);
                    sms_alert.setCreditos_cuenta(sms.getCreditos_cuenta());
                    this.envia_SMS_Alerta(sms_alert);
                    }
                    Actualizar_Alarma_Sms("SI");                     
               }
                else
                {
                    if(sms.getCreditos_cuenta()>Float.valueOf(this.Consultar_Dato_Sms("TOPE_CREDITOS")))
                    {
                     this.Actualizar_Alarma_Sms("NO");
                    }
                }
            }   
       // con.disconnect();
        }
         catch (Exception e)
        {
           sms.setEstado("SENDING_ERROR");
           sms.setComentario(e.getMessage());
           this.InsertarSMS(sms);
           sms.setComentario("Error:"+e.getMessage());
           sms.setEstado_fintra("N");
           System.out.println("Exception: " + e.getMessage());
           e.printStackTrace();
        }
    return sms;
    }



  public Sms envia_SMS_Alerta(Sms sms) throws Exception
   {
        sms.setSMS(this.Consultar_Dato_Sms("SMS_ALARMA").replace("xcreditox", String.valueOf(sms.getCreditos_cuenta())));
       try
        {
            URL url_sms = new URL ("https://www.elibom.com/api/rest/Messages");
            String response="";
            OutputStreamWriter wr = null;
            BufferedReader rd = null;
            HttpsURLConnection con = (HttpsURLConnection)url_sms.openConnection();
            con.setRequestProperty ("Authorization", "Basic " + getEcoding());

             con.setDoInput(true);
             con.setDoOutput (true);
             wr = new OutputStreamWriter(con.getOutputStream());
             wr.write("destinations="+sms.getCell()+"&message="+sms.getSMS());
             wr.flush();


             BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
            //Recibir respuesta
             rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
             String line;
                while ((line = in.readLine()) != null)
                {
                response += line;
                }

            sms= this.getToken(response,sms);
            sms=Consulta_SMS_HTTP(sms);
            sms.setEstado_fintra("S");                  
            this.InsertarSMS(sms);
            con.disconnect();
        }
         catch (Exception e)
        {
           sms.setComentario("Error:"+e.getMessage());
           sms.setEstado_fintra("N");
           System.out.println("Exception: " + e.getMessage());
           e.printStackTrace();
        }
    return sms;
    }




/*----------------------------Consultar estado del mensaje enviado-----------------------------------------*/
 public Sms Consulta_SMS_HTTP(Sms sms)
   {
       try
        {
            URL url_sms = new URL ("https://www.elibom.com/api/rest/Messages/"+sms.getToken());
            String response="";
            OutputStreamWriter wr = null;
            BufferedReader rd = null;
            HttpsURLConnection con = (HttpsURLConnection)url_sms.openConnection();
            con.setRequestProperty ("Authorization", "Basic " + getEcoding());

             con.setDoInput(true);
             con.setDoOutput (true);

             BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
            //Recibir respuesta
             rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
             String line;
                while ((line = in.readLine()) != null)
                {
                response += line;
                }

            
            sms= this.getSms_xml("message", response,  sms);


            con.disconnect();
        }
         catch (Exception e)
        {
           sms.setComentario("Error:"+e.getMessage());
           sms.setEstado_fintra("N");
           System.out.println("Exception: " + e.getMessage());
           e.printStackTrace();
        }
    return sms;
    }



  public Sms Consulta_Cuenta_SMS_HTTP(Sms sms)
   {
       try
        {
            URL url_sms = new URL ("https://www.elibom.com/api/rest/Account");
            String response="";
            OutputStreamWriter wr = null;
            BufferedReader rd = null;
            HttpsURLConnection con = (HttpsURLConnection)url_sms.openConnection();
            con.setRequestProperty ("Authorization", "Basic " + getEcoding());

             con.setDoInput(true);
             con.setDoOutput (true);

             BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
            //Recibir respuesta
             rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
             String line;
                while ((line = in.readLine()) != null)
                {
                response += line;
                }


            sms= this.getAccount(response,  sms);


            con.disconnect();
        }
         catch (Exception e)
        {
           sms.setComentario("Error:"+e.getMessage());
           sms.setEstado_fintra("N");
           System.out.println("Exception: " + e.getMessage());
           e.printStackTrace();
        }
    return sms;
    }



















/************************************obtener un elemeno de un nodo***************************************************/
    public static Sms getSms_xml(String nodo,String response,Sms sms) throws SAXException, IOException
    {
    
        Element elemento= null;
        DOMParser parser = new DOMParser();
        parser.parse(new InputSource(new java.io.StringReader(response))); // xml is a
        Document doc = parser.getDocument();
        doc.getDocumentElement().normalize();
        NodeList lista = doc.getElementsByTagName(nodo);
        for (int i = 0; i < lista.getLength(); i ++) {
          Node etq = lista.item(0);
             if (etq.getNodeType() == Node.ELEMENT_NODE)
            {
              elemento = (Element) etq;
              sms.setIdsms(getTagValue("id", elemento));
              sms.setToken(getTagValue("deliveryToken", elemento));
              sms.setEstado_fintra(getTagValue("state", elemento));
              sms.setEstado(getTagValue("state", elemento));
              sms.SetCreditos(Float.valueOf(getTagValue("credits", elemento)));
              sms.setSMS(getTagValue("text", elemento));   
              sms.setFechaEnvio(getTagValue("sentTime", elemento));
            }
        }

        return sms;
    }






    /************************************obtener token del mensaje enviado***************************************************/
    public static Sms getToken(String response,Sms sms) throws SAXException, IOException
    {
       
        Element elemento= null;
        DOMParser parser = new DOMParser();
        parser.parse(new InputSource(new java.io.StringReader(response))); // xml is a
        Document doc = parser.getDocument();
        doc.getDocumentElement().normalize();
        NodeList lista = doc.getElementsByTagName("response");
        for (int i = 0; i < lista.getLength(); i ++) {
          Node etq = lista.item(0);
             if (etq.getNodeType() == Node.ELEMENT_NODE)
            {
              elemento = (Element) etq;
              System.out.println("Nombre : " + getTagValue("deliveryToken", elemento));
              sms.setToken(getTagValue("deliveryToken", elemento));
            }
        }

        return sms;
    }




    /************************************obtener creditos del la cuenta***************************************************/
    public static Sms getAccount(String response,Sms sms) throws SAXException, IOException
    {

        Element elemento= null;
        DOMParser parser = new DOMParser();
        parser.parse(new InputSource(new java.io.StringReader(response))); // xml is a
        Document doc = parser.getDocument();
        doc.getDocumentElement().normalize();
        NodeList lista = doc.getElementsByTagName("account");
        for (int i = 0; i < lista.getLength(); i ++) {
          Node etq = lista.item(0);
             if (etq.getNodeType() == Node.ELEMENT_NODE)
            {
              elemento = (Element) etq;
               System.out.println("Nombre : " + getTagValue("credits", elemento));
              sms.setCuenta(getTagValue("name", elemento));
              sms.setUsuario(getTagValue("owner", elemento));
              sms.setCreditos_cuenta(Float.valueOf(getTagValue("credits", elemento)));
            }
        }

        return sms;
    }



 private static String getTagValue(String sTag, Element eElement)
 {
	  NodeList nlList= eElement.getElementsByTagName(sTag).item(0).getChildNodes();
	  Node nValue = (Node) nlList.item(0);
	  return nValue.getNodeValue();

 }




   



  /*----------------Enviuar SMS por conexion web service ----------------------*/
  /*
  * Jpinedo
  */
  /*  public int envia_SMS_WS(String sms,String cell)
    {

        int ret=0;
        try{
         String endpoint = "http://www.elibom.com/services/sendmessagews";
         Service  service = new Service();
         Call     c    = (Call) service.createCall();
         c.setTargetEndpointAddress( new java.net.URL(endpoint) );
         c.setOperationName(new QName("http://www.elibom.com/sendmessage", "sendMessage"));
         ret = ((Integer) c.invoke( new Object[] { "jesus.pinedo@hotmail.com", "jesus.pinedo1325", cell, sms } )).intValue();

        }
        catch (Exception e)
        {
           ret=-1;
            System.out.println("Exception: " + e.getMessage());
           e.printStackTrace();
        }
    return ret;
    }

*/



   public  String getEcoding() throws Exception
{
  String pass="";
  pass=this.Consultar_Pass_Sms();
  String userPassword = "soporte@geotech.com.co:"+pass;
  String encoding = new sun.misc.BASE64Encoder().encodeBuffer(userPassword.getBytes());
  String code=  encoding.replace("\n", "");

  return  code;
}






    /**
     * M�todo que lista sms enviados
     * @autor.......jpinedo
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List ListarSms( String nit,String cell,String estado,String tipo,String fecha_inicial,String fecha_final) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_LISTAR_SMS";
        List               lista   = new LinkedList();

        try{

            con = this.conectarJNDI(query);            
            String sql   =   this.obtenerSQL( query );


            if(nit.equals("")){sql = sql.replaceAll("#nit", "");}else
            {
                sql = sql.replaceAll("#nit", "AND s.nit =\'" + nit + "\' ");
            }

            if(cell.equals("")){sql = sql.replaceAll("#celular", "");}else
            {
                sql = sql.replaceAll("#celular", "AND s.celular =\'" + cell + "\' ");
            }

            if(estado.equals("")){sql = sql.replaceAll("#estado", "");}else
            {
                sql = sql.replaceAll("#estado", "AND s.estado =\'" + estado + "\' ");
            }

            if(tipo.equals("")){sql = sql.replaceAll("#tipo", "");}else
            {
                sql = sql.replaceAll("#tipo", "AND s.tipo =\'" + tipo + "\' ");
            }

            if(fecha_inicial.equals("")){sql = sql.replaceAll("#fecha_inicial", "");}else
            {
                sql = sql.replaceAll("#fecha_inicial", "AND s.creation_date >=\'" + fecha_inicial + "\' ");
            }

            if(fecha_final.equals("")){sql = sql.replaceAll("#fecha_final", "");}else
            {
                sql = sql.replaceAll("#fecha_final", "AND substring(s.fecha_envio,1,10) <=\'" + fecha_final + "\' ");
            }



            st= con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                Sms sms = new  Sms();
                sms.setIdsms(rs.getString("idsms"));
                sms.setToken(rs.getString("Token"));
                sms.setNit(rs.getString("nit"));
                sms.setCell(rs.getString("celular"));
                sms.setSMS(rs.getString("sms"));
                sms.setEstado(rs.getString("estado"));
                sms.SetCreditos(rs.getFloat("creditos"));
                sms.setFechaEnvio(rs.getString("fecha_envio"));
                sms.setComentario(rs.getString("comentario"));            
                lista.add( sms );
            }


        }catch(Exception e){
            throw new Exception( "listar sms " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;

    }



        /**
     * M�todo que lista destinatarios para enviar sms de alerta
     * @autor.......jpinedo
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List ListarDestinatariosAlerta() throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_LISTAR_DESTINATARIOS_ALERTA";
        List               lista   = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next())
            {
                Sms sms = new  Sms();
                sms.setNit(rs.getString("table_code"));
                sms.setCell(rs.getString("dato"));               
                lista.add( sms );
            }


        }catch(Exception e){
            throw new Exception( "ListarDestinatariosAlerta" + e.getMessage());
        }
        finally
        {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;




    }







 





   /*--------------------------------------------------------
    * Autor jpinedo
    * Actualiza  la alarma de creditos insuficientes en tablagen
    */
       public void   Actualizar_Alarma_Sms(String estado) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_ACTUALIZA_ALARMA";

        try
        {
            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, estado);
            st.execute();

        }
        catch(Exception e)
        {
            throw new Exception( " Error ACTUALIZANDO ALARMA " + e.getMessage());
        }
        finally
        {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }




   /*--------------------------------------------------------
    * Autor jpinedo
    * Consulta estado   la alarma de creditos insuficientes en tablagen
    */
       public String   Consultar_Alerta_Sms() throws Exception
       {
         return this.Consultar_Dato_Sms("ALERTA");
       }



          /*--------------------------------------------------------
    * Autor jpinedo
    * Consulta estado   la alarma de creditos insuficientes en tablagen
    */
       public String   Consultar_Pass_Sms() throws Exception
       {
         return this.Consultar_Dato_Sms("PASS");
       }








       /*--------------------------------------------------------
    * Autor jpinedo
    * Consulta   datos utilizados para el programa sms  en tablagen
    */
       public String   Consultar_Dato_Sms(String code) throws Exception
       {
        String estado="";
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_CONSULTAR_DATO";
        try
        {
            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, code);
            rs=st.executeQuery();
            while(rs.next())
            {
              estado=rs.getString("dato");
            }

        }
        catch(Exception e)
        {
            throw new Exception( " Error CONSULTANDO DATO SMS " + e.getMessage());
        }
        finally
        {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return estado;
    }











       /*
    * Autor jpinedo
    * Insertar un registro para el log de sms enviados por tranferencias
    */
       public void   InsertarSMS(Sms sms) throws Exception{

        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        Connection         con     = null;
        String             query   = "SQL_INSERTAR_SMS";

        try{
            con = this.conectarJNDI(query);
            String sql   =   this.obtenerSQL( query );
            st= con.prepareStatement(sql);
            st.setString(1, sms.getIdsms());
            st.setString(2, sms.getNit());
            st.setString(3, sms.getCell());
            st.setString(4,  sms.getSMS());
            st.setString(5, sms.getEstado());
            st.setString(6, sms.getEstado_fintra());
            st.setString(7, sms.getEstado());
            st.setString(8, sms.getFechaEnvio());
            st.setFloat(9, sms.getCreditos());
            st.setString(10, sms.getToken());
            st.setString(11, sms.getTipo());

            st.execute();

        }
        catch(Exception e){
            throw new Exception( " Error INSERTANDO sms" + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }

}



