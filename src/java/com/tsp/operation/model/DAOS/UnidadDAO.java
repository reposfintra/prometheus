/*
 * Nombre        UnidadDAO.java
 * Autor         Ing Jose de la Rosa
 * Modificado    Ing Sandra Escalante
 * Fecha         26 de junio de 2005, 11:15 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;


public class UnidadDAO extends MainDAO{
    private Unidad unidad;
    private Vector unidades;
    private Vector detunidad;
    
    private TreeMap tipos;//sescalante
    
    private static String SQL_INSERTAR = "INSERT INTO  tablagen (   table_type, " +
    "                                                               table_code, " +
    "                                                               descripcion, " +
    "                                                               creation_user, " +
    "                                                               creation_date, " +
    "                                                               user_update, " +
    "                                                               last_update, " +
    "                                                               reg_status, " +
    "                                                               referencia ) " +
    "                                      VALUES   ('TUNIDAD', ?, ?, ?, 'now()', ?, 'now()', '', ?)";
    
    private static String SQL_BUSCAR = "SELECT  table_code AS cod_unidad, " +
    "                                           descripcion AS desc_unidad, " +
    "                                           referencia AS tipo " +
    "                                   FROM    tablagen " +
    "                                   WHERE   table_type = 'TUNIDAD'" +
    "                                           AND table_code = ? " +
    "                                           AND referencia = ? " +
    "                                           AND reg_status != 'A'";
    
    
    
    private static String SQL_VERIFICAR = " SELECT  table_code AS cod_unidad  " +
    "                                       FROM    tablagen " +
    "                                       WHERE   table_type = 'TUNIDAD'" +
    "                                               AND table_code = ?" +
    "                                               AND reg_status != 'A'";
    
    private static String SQL_MODIFICAR = " UPDATE  tablagen " +
    "                                       SET     descripcion = ?, " +
    "                                               last_update = 'now()', " +
    "                                               user_update = ?, " +
    "                                               reg_status = ''," +
    "                                               referencia = ? " +
    "                                       WHERE   table_type = 'TUNIDAD'" +
    "                                               AND table_code = ?  "+				
    "						    AND referencia = ?";
    
    private static String SQL_ELIMINAR = "  DELETE FROM tablagen" +
    "                                       WHERE   table_type = 'TUNIDAD'" +
    "                                               AND table_code = ?" +
    "                                               AND referencia = ?";
    
    private static String SQL_LISTAR = "SELECT  table_code AS cod_unidad, " +
    "                                           descripcion AS desc_unidad, " +
    "                                           referencia AS tipo, " +
    "                                               CASE WHEN referencia = 'D' THEN 'DISTANCIA'" +
    "							 WHEN referencia = 'P' THEN 'PESO'" +
    "							 WHEN referencia = 'T' THEN 'TIEMPO'" +
    "                                                    WHEN referencia = 'V' THEN 'VOLUMEN' " +
    "                                                    ELSE '(vacio)' " +
    "						    END AS ntipo	"+
    "                                   FROM    tablagen  " +
    "                                   WHERE   table_type = 'TUNIDAD'" +
    "                                           AND reg_status != 'A'  " +
    "                                   ORDER BY referencia, table_code";

private static String SQL_BDETALLE = "  SELECT  table_code AS cod_unidad, " +
    "                                               descripcion AS desc_unidad, " +
    "                                               referencia AS tipo, " +
    "                                               CASE WHEN referencia = 'D' THEN 'DISTANCIA'" +
    "							 WHEN referencia = 'P' THEN 'PESO'" +
    "							 WHEN referencia = 'T' THEN 'TIEMPO'" +
    "                                                    WHEN referencia = 'V' THEN 'VOLUMEN' " +
    "                                                    ELSE '(vacio)' " +
    "						    END AS ntipo	"+
    "                                       FROM    tablagen" +
    "                                       WHERE   table_type = 'TUNIDAD'" +
    "                                               AND table_code LIKE ? " +
    "                                               AND descripcion LIKE ? " +
    "                                               AND referencia LIKE ? " +
    "                                               AND reg_status != 'A' ";
    
    private static String SQL_BXTIPO = "SELECT  table_code AS cod_unidad," +
    "                                           descripcion AS desc_unidad, " +
    "                                           referencia AS tipo" +
    "                                   FROM    tablagen" +
    "                                   WHERE   table_type = 'TUNIDAD'" +
    "                                           AND referencia = ? " +
    "                                           AND reg_status != 'A'" +
    "                                   ORDER BY table_code";
    
    /** Creates a new instance of UnidadDAO */
    public UnidadDAO() {
        super("UnidadDAO.xml");
    }
    
    /**
     * Getter for property unidad
     * @return Value of property unidad
     */
    public Unidad getUnidad() {
        return unidad;
    }
    
    /**
     * Setter for property unidad
     * @param base New value of property unidad
     */
    public void setUnidad(Unidad unidad) {
        this.unidad = unidad;
    }
    
    /**
     * Getter for property unidades
     * @return Value of property unidades
     */
    public Vector getUnidades() {
        return unidades;
    }
    
    /**
     * Setter for property unidades
     * @param base New value of property unidades
     */
    public void setUnidades(Vector unidades) {
        this.unidades = unidades;
    }
    
    /**
     * Metodo <tt>insertUnidad</tt>, registra una unidad
     * @autor : Ing. Jose de la Rosa
     * @version : 1.0
     */
    public void insertUnidad() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERTAR";
        
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1,unidad.getCodigo());
                st.setString(2,unidad.getDescripcion());
                st.setString(3,unidad.getCreation_user());
                st.setString(4,unidad.getUser_update());
                st.setString(5,unidad.getTipo());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA UNIDAD (tablagen)" + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>searchUnidad</tt>, registra una unidad
     * @autor : Ing. Jose de la Rosa
     * @param codigo y tipo de unidad (String)
     * @version : 1.0
     */
    public void searchUnidad(String cod, String tipo)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        unidad = null;
        String query = "SQL_BUSCAR";
        try {
            con = this.conectarJNDI(query);
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1,cod);
                st.setString(2,tipo);
                rs = st.executeQuery();
                while(rs.next()){
                    unidad = new Unidad();
                    unidad.setCodigo(rs.getString("cod_unidad"));
                    unidad.setDescripcion(rs.getString("desc_unidad"));
                    unidad.setTipo(rs.getString("tipo"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA UNIDAD (searchUnidad) " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }
    
    /**
     * Metodo <tt>existUnidad</tt>, verifica la existencia de  una unidad en la tabla
     * @autor : Ing. Jose de la Rosa
     * @return boolean
     * @version : 1.0
     */
    public boolean existUnidad(String cod) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_VERIFICAR";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            
            if (con != null){
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,cod);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA VERIFICACION DE LA UNIDAD (existUnidad) " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Metodo <tt>listUnidad</tt>, obtiene las unidades registradas
     * @autor : Ing. Jose de la Rosa
     * @version : 1.0
     */
    public void listUnidad()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        unidades = null;
        String query = "SQL_LISTAR";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                unidades = new Vector();
                while(rs.next()){
                    unidad = new Unidad();
                    unidad.setCodigo(rs.getString("cod_unidad"));
                    unidad.setDescripcion(rs.getString("desc_unidad"));
                    unidad.setTipo(rs.getString("tipo"));
                    unidad.setNtipo(rs.getString("ntipo"));
                    unidades.add(unidad);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA UNIDADES (listUnidad) " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>updateUnidad</tt>, modifica la informacion de una unidad
     * @autor : Ing. Jose de la Rosa
     * @param codigo, descripcion, usuario modificacion, tipo de unidad, base, antiguo tipo de unidad (String)
     * @version : 1.0
     */
    public void updateUnidad(String cod, String desc, String usu, String tipo, String base, String atipo) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,desc);
                st.setString(2,usu);
                st.setString(3,tipo);
                st.setString(4,cod);
                st.setString(5,atipo);
                st.executeUpdate();
            }            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA  (updateUnidad) " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>anularUnidad</tt>, elimina una unidad de la tabla
     * @autor : Ing. Jose de la Rosa
     * @version : 1.0
     */
    public void anularUnidad() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, unidad.getCodigo());
                st.setString(2, unidad.getTipo());
                st.executeUpdate();
            }
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ELIMINACIN DE LA UNIDAD (anularUnidad) " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>getDetalleUnidades</tt>, obtiene el valor del vector unidades
     * @autor : Ing. Jose de la Rosa
     * @return Vector
     * @version : 1.0
     */
    public Vector getDetalleUnidades() throws SQLException{
        return unidades;
    }
    
    /**
     * Metodo <tt>searchDetalleUnidades</tt>, busca una unidad dado unos parametros
     * @autor : Ing. Jose de la Rosa
     * @param codigo, descripcion y tipo de unidad
     * @version : 1.0
     */
    public void searchDetalleUnidades(String cod, String desc, String tipo) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        unidades = null;
        String query = "SQL_BDETALLE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod+"%");
                st.setString(2, desc+"%");
                st.setString(3, tipo+"%");
                rs = st.executeQuery();
                unidades = new Vector();
                
                while(rs.next()){
                    unidad = new Unidad();
                    unidad.setCodigo(rs.getString("cod_unidad"));
                    unidad.setDescripcion(rs.getString("desc_unidad"));
                    unidad.setTipo(rs.getString("tipo"));
                    unidad.setNtipo(rs.getString("ntipo"));
                    unidades.add(unidad);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DETALLADA DE LAS UNIDADES (searchDetalleUnidades) " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>listarUnidades</tt>, obtiene las unidades registradas
     * @autor : Ing. Jose de la Rosa
     * @return Vector
     * @version : 1.0
     */
    public Vector listarUnidades()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        unidades = null;
        String query = "SQL_LISTAR";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                unidades = new Vector();
                
                while(rs.next()){
                    unidad = new Unidad();
                    unidad.setCodigo(rs.getString("cod_unidad"));
                    unidad.setDescripcion(rs.getString("desc_unidad"));
                    unidades.add(unidad);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE UNIDADES (listarUnidades) " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return unidades;
    }
    
    /**
     * Metodo <tt>listarUnidadesxTipo</tt>, obtiene las unidades segun el tipo
     * @autor : Ing. Sandra Escalante
     * @version : 1.0
     */
    public void listarUnidadesxTipo( String tipo )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        unidades = null;
        String query = "SQL_BXTIPO";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,  tipo);
                rs = st.executeQuery();
                unidades = new Vector();
                
                while(rs.next()){
                    unidad = new Unidad();
                    unidad.setCodigo(rs.getString("cod_unidad"));
                    unidad.setDescripcion(rs.getString("desc_unidad"));
                    unidad.setTipo(rs.getString("tipo"));
                    unidades.add(unidad);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS UNIDADES POR TIPO (listarUnidadesxTipo) " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo <tt>cargarTipoUnidad</tt>, define un vector con 4 tipos de unidades 
     * @autor : Ing. Sandra Escalante
     * @version : 1.0
     */
    public void cargarTipoUnidad()throws Exception{
                
        tipos = null;
        
        try {
            
                tipos = new TreeMap ();
                
                tipos.put("VOLUMEN","V");
                tipos.put("PESO","P");
                tipos.put("DISTANCIA","D");
                tipos.put("TIEMPO","T");
                
            
        }catch(Exception e){
            throw new Exception("ERROR DURANTE LA CARGA DE LOS TIPOS DE UNIDAD (cargarTipoUnidad) " + e.getMessage() );
        }
    }
    
    /**
     * Getter for property tipos.
     * @return Value of property tipos.
     */
    public java.util.TreeMap getTipos() {
        return tipos;
    }
    
    /**
     * Setter for property tipos.
     * @param tipos New value of property tipos.
     */
    /*public void setTipos(java.util.TreeMap tipos) {
        this.tipos = tipos;
    }*/
    
}
