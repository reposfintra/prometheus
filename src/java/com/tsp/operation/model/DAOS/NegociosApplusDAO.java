/*
 * NegociosApplusDAO.java
 * Created on 2 de febrero de 2009, 17:33
 */
package com.tsp.operation.model.DAOS;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Accord;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.FacturaContratista;
import com.tsp.operation.model.beans.Imagen;
import com.tsp.operation.model.beans.Material;
import com.tsp.operation.model.beans.NegocioApplus;
import com.tsp.operation.model.threads.HSendMail2;
import com.tsp.util.Util;
import com.tsp.util.Utility;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

/** * @author  Fintra */
public class NegociosApplusDAO  extends MainDAO {

    public NegociosApplusDAO() {
        super("NegociosApplusDAO.xml");
    }

    public NegociosApplusDAO(String dataBaseName) {
        super("NegociosApplusDAO.xml", dataBaseName);
    }

    public ArrayList getNegociosApplus(String estado,String contratista,String numosxi,String factconformed,String loginx) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_NEGOCIOS";
        ArrayList listNegocios=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );

            if (contratista!=null && !(contratista.equals(""))){
                sql=sql.replaceAll("contratixx", "'"+contratista+"'");
            }else{
                sql=sql.replaceAll("contratixx", "a.id_contratista");
            }

            if (factconformed!=null && !(factconformed.equals(""))){
                sql=sql.replaceAll("factconformedxx", "'"+factconformed+"'");
            }else{
                sql=sql.replaceAll("factconformedxx", "a.fact_conformada");
            }

            if (numosxi!=null && !(numosxi.equals(""))){
                sql=sql.replaceAll("numosxx", "'"+numosxi+"'");
            }else{
                sql=sql.replaceAll("numosxx", "o.num_os");
            }

            if (estado!=null && !(estado.equals("")) && !(estado.equals("99"))){
                sql=sql.replaceAll("estadito", "'"+estado+"'");
            }else{
                if (numosxi!=null && !(numosxi.equals(""))){
                    sql=sql.replaceAll("estadito", "o.id_estado_negocio");
                }else{
                    sql=sql.replaceAll("estadito", "'"+estado+"'");
                }
            }

            //sql=sql.replaceAll("condicionrara1", "  AND ('"+loginx+"' IN (SELECT table_code FROM tablagen WHERE table_type ='USR_VIP_MS') OR f_facturado_cliente>='2009-01-01' OR f_facturado_cliente='--' ) ");

            //sql=sql.replaceAll("condicionrara2", "  AND ('"+loginx+"' IN (SELECT table_code FROM tablagen WHERE table_type ='USR_RED_MS') OR id_estado_negocio!='11' OR (id_estado_negocio='11' AND (SELECT fecha_prefactura FROM app_accord WHERE id_accion=a.id_accion)<='2009-04-01')) ");

            //sql=sql.replaceAll("condicionrara1", "  AND ('"+loginx+"' IN (SELECT table_code FROM tablagen WHERE table_type ='USR_VIP_MS') OR f_facturado_cliente>='2009-01-01' OR f_facturado_cliente='--' OR (SELECT prefactura FROM app_accord WHERE id_accion=a.id_accion)!='') ");
            sql=sql.replaceAll("condicionrara1", " ");
            sql=sql.replaceAll("condicionrara2", "  AND ('"+loginx+"' IN (SELECT table_code FROM tablagen WHERE table_type ='USR_RED_MS') OR (f_facturado_cliente>='2009-04-01'  OR f_facturado_cliente='--' OR (SELECT prefactura FROM app_accord WHERE id_accion=a.id_accion)!='')) ");

            st            =   con.prepareStatement( sql );

            //.out.println("sql"+sql);
            //st.setString(1,estado);
            rs = st.executeQuery();
            while (rs.next()){
                //ystem.out.println("1 fila");
                //ystem.out.println("un dato leido::"+rs.getString("id"));
                NegocioApplus negocioApplus=new NegocioApplus();
                //anticipoGasolina.setDstrct(rs.getString("dstrct"));
                negocioApplus.setNumOs(rs.getString("num_os"));
                negocioApplus.setId(rs.getString("id_orden"));
                negocioApplus.setIdAccion(rs.getString("id_accion"));
                negocioApplus.setVlr(rs.getString("total_prev1"));
                negocioApplus.setIdCliente(rs.getString("id_cliente"));
                negocioApplus.setNombreCliente(rs.getString("nombre_cliente"));
                negocioApplus.setTelCli(rs.getString("telefono"));
                negocioApplus.setContacto(rs.getString("contacto"));
                negocioApplus.setIdContratista(rs.getString("id_contratista"));
                negocioApplus.setNombreContratista(rs.getString("descripcion"));
                negocioApplus.setCuotas(rs.getString("cuotas"));
                negocioApplus.setValCuotas(rs.getString("valor_cuotas_r"));
                negocioApplus.setEstudio(rs.getString("estudio_economico"));
                negocioApplus.setSimbolo(rs.getString("simbolo_variable"));
                negocioApplus.setFacturaConformada(rs.getString("fact_conformada"));
                negocioApplus.setFecha(rs.getString("f_facturado_cliente"));
                negocioApplus.setObservacion(rs.getString("detalle_inconsistencia"));

                negocioApplus.setObservacionOpen(rs.getString("observacion_open"));

                negocioApplus.setOferta(rs.getString("oferta"));
                if (negocioApplus.getObservacion()==null){negocioApplus.setObservacion("");}
                listNegocios.add(negocioApplus);
                negocioApplus.setEcaOferta(rs.getString("eca_oferta"));
                //negocioApplus.setDifEcaOferta(rs.getString("dif_eca_oferta"));
                //negocioApplus.setDifOferta(rs.getString("dif_oferta"));
                negocioApplus.setDifEcaOfertaConsorcioAntiguo(rs.getString("dif_ecaoferta_consorcio_antiguo"));
                negocioApplus.setDifEcaOfertaConsorcioNuevo(rs.getString("dif_ecaoferta_consorcio_nuevo"));
                negocioApplus.setDifOfertaApplusAntiguo(rs.getString("dif_oferta_applus_antiguo"));
                negocioApplus.setDifOfertaApplusNuevo(rs.getString("dif_oferta_applus_nuevo"));

                negocioApplus.setEsquemaComision(rs.getString("esquema_comision"));

                negocioApplus.setPrefactura(rs.getString("prefactura"));
                negocioApplus.setFacturaEca(rs.getString("factura_eca"));
                negocioApplus.setFacturaContratista(rs.getString("factura_contratista"));
                negocioApplus.setFacturaRetencion(rs.getString("factura_retencion"));
                negocioApplus.setFacturaBoni(rs.getString("factura_bonificacion"));
                negocioApplus.setNicClient(rs.getString("nic"));
                negocioApplus.setNitClient(rs.getString("nit"));

                negocioApplus.setFacturaApp(rs.getString("factura_app"));
                negocioApplus.setFacturaPro(rs.getString("factura_pro"));
                negocioApplus.setFacturaComiEca(rs.getString("factura_comision_eca"));
                negocioApplus.setEsquemaFinanciacion(rs.getString("esquema_financiacion"));

                negocioApplus.setEstado(rs.getString("estado"));
                negocioApplus.setFRecepcion(rs.getString("f_recepcion"));
                negocioApplus.setTipoCliente(rs.getString("tipo_identificacion"));

                negocioApplus.setFacturaFactoringPro(rs.getString("factura_formula_provintegral"));

                negocioApplus.setAcciones(rs.getString("acciones"));
                //negocioApplus.setIdSolicitud(rs.getString("id_solicitud"));//090922
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listNegocios;
    }
    public String CambiarEstado(String[] ofertas,String nuevo_estado,String observacion,String esquema_comision,String svx,
     String contratista_consultar,String fact_conformed,String cuoticas,String fecfaccli,String fact_conformada_consultar,
     String esquema_financiacion,String userx) throws Exception{
        String oferticas="";
        for (int i=0;i<ofertas.length;i++){
            oferticas=oferticas+ofertas[i]+",";
        }

        Connection         con     = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_CAMBIAR_ESTADO";
        String consultaSQL ="";
        int affected=0 ;
        try{
            con = this.conectar(query);

            String sql    =   this.obtenerSQL( query );

            if (observacion!=null && !(observacion.equals(""))){
            	//sql=sql.replaceAll("detallexx","'"+observacion+"'");
                sql=sql.replaceAll("detallexx","detalle_inconsistencia || '<br>------- ' || current_date || ' _ " + userx+": "+""+observacion+"'");
            }else{
                sql=sql.replaceAll("detallexx","detalle_inconsistencia");
            }

            if (esquema_comision!=null && !(esquema_comision.equals(""))){
            	sql=sql.replaceAll("esquemaxx","'"+esquema_comision+"'");
            }else{
                sql=sql.replaceAll("esquemaxx","esquema_comision");
            }

            if (svx!=null && !(svx.equals(""))){
            	sql=sql.replaceAll("svxx","'"+svx+"'");
            }else{
                sql=sql.replaceAll("svxx","simbolo_variable");
            }

            if (cuoticas!=null && !(cuoticas.equals(""))){
            	sql=sql.replaceAll("cuoticasxx","'"+cuoticas+"'");
            }else{
                sql=sql.replaceAll("cuoticasxx","cuotas");
            }

            if (contratista_consultar!=null && !(contratista_consultar.equals(""))){
            	sql=sql.replaceAll("contratistaxx","'"+contratista_consultar+"'");
            }else{
                sql=sql.replaceAll("contratistaxx","id_contratista");
            }

            if (fact_conformada_consultar!=null && !(fact_conformada_consultar.equals(""))){
            	sql=sql.replaceAll("fact_conformadaxx","'"+fact_conformada_consultar+"'");
            }else{
                sql=sql.replaceAll("fact_conformadaxx","fact_conformada");
            }

            if (fact_conformed!=null && !(fact_conformed.equals(""))){
            	sql=sql.replaceAll("factconfxx","'"+fact_conformed+"'");
            }else{
                sql=sql.replaceAll("factconfxx","fact_conformada");
            }

            if (fecfaccli!=null && !(fecfaccli.equals(""))){
            	sql=sql.replaceAll("fecfacclixx","'"+fecfaccli+"'");
            }else{
                sql=sql.replaceAll("fecfacclixx","f_facturado_cliente");
            }
            if (esquema_financiacion!=null && !(esquema_financiacion.equals(""))){
            	sql=sql.replaceAll("esquema_financiacionxx","'"+esquema_financiacion+"'");
            }else{
                sql=sql.replaceAll("esquema_financiacionxx","esquema_financiacion");
            }
            sql=sql.replaceAll("user_updatexx","'"+userx+"'");

            sql=sql.replaceAll("oferticas",oferticas.substring(0,(oferticas.length()-1) ));
            //ystem.out.println("sql::"+sql);

            if (nuevo_estado!=null && !(nuevo_estado.equals("nada"))){//090721
                sql=sql.replaceAll("estadinho","'"+nuevo_estado+"'");
            }else{
                sql=sql.replaceAll("estadinho","id_estado_negocio");
            }

            st = con.prepareStatement( sql );
            //st.setString(1, nuevo_estado);
            //st.setString(2, observacion);
            //.out.println("sqlll:"+st.toString());
            st.execute();

        }catch(Exception e){
            System.out.println("error en CambiarEstado en NegociosApplusDAO..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return "prueba";
    }

    public ArrayList getEstadosApplus() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ESTADOS";
        ArrayList listEstados=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] estado;
            rs = st.executeQuery();
            while (rs.next()){
                estado=new String[2];
                estado[0]=rs.getString("codigo");
                estado[1]=rs.getString("estado");
                listEstados.add(estado);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listEstados;
    }

    public void insertImagen(ByteArrayInputStream bfin, int longitud,  Dictionary fields, String idxx5,String tipito ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql="";

        String             query = "SQL_INSERT_ARCHIVO";
        if (idxx5!=null && !(idxx5.equals(""))){query = "SQL_UPDATE_ARCHIVO";}
       try{
                    sql = this.obtenerSQL(query);
                    con = this.conectarJNDI(query);
                    st = con.prepareStatement(sql);

                  String actividad      =  (String)fields.get("actividad");
                  String tipoDocumento  =  (String)fields.get("tipoDocumento");
                  String documento      =  (String)fields.get("documento");
                  String user           =  (String)fields.get("usuario");
                  String agencia        =  (String)fields.get("agencia");
                  String filename       =  (String)fields.get("archivo");

                  if(documento.equals(""))
                       documento = Utility.getHoy("");


                  st.setString       (1, documento);
                  st.setString       (2, filename);

                  st.setBinaryStream (3, bfin, longitud);
                  st.setString       (4, user);
                  st.setString       (5, agencia);
                  st.setString       (6, tipito);

                  if (idxx5!=null && !(idxx5.equals(""))){st.setString       (7, idxx5);}


            st.executeUpdate();

        }catch(Exception e){
            System.out.println("nooo:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error insertImagen() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
             if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
     public void insertRegistroArchivo(String ruta,  Dictionary fields, String idxx5,String tipito, String bd ) throws SQLException {
      
        Connection con = null;
        PreparedStatement  st    = null;

        String             query = "SQL_INSERT_RUTA_ARCHIVO";
        if (idxx5!=null && !(idxx5.equals(""))){
           query = "SQL_UPDATE_RUTA_ARCHIVO";
        }
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2;

                  String documento      =  (String)fields.get("documento");
                  String user           =  (String)fields.get("usuario");
                  String agencia        =  (String)fields.get("agencia");
                  String filename       =  (String)fields.get("archivo");
                  int idCategoria       =  (int)fields.get("categoria");

                  if(documento.equals(""))
                       documento = Utility.getHoy("");
                                   
                  st.setString       (1, ruta);
                  st.setString       (2, user);                  
                  st.setString       (3, agencia);
                  st.setString       (4, tipito);
                  st.setString       (5, documento);
                  st.setString       (6, filename);
                  st.setInt          (7, idCategoria);
                  st.execute();

            } }catch(Exception e){
            System.out.println("nooo:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error insertImagen() : "+ e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    public List searcArchivos(

                                 String documento
                                ,String rutaOrigen, String rutaDestino , 
                               String tipito
                               ) throws SQLException {

        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        List               lista = null;
        String             query = "SQL_SEARCH_ARCHIVOS";
        Connection         con   = null;

        String             sql   = "";

        try{   
            File carpetaDestino =new File(rutaDestino);
          
            deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);
            con = this.conectar(query);

            sql          =  this.obtenerSQL( query );

            String where = " WHERE  reg_status ='' ";
            String resto = " and tipo = '"+tipito+"' ";
            resto += " and  (id)               = ('"+ documento + "') ";

            sql += where + resto;

            //ystem.out.println("La consulta sql es: "+sql);
            st  =   con.prepareStatement( sql );
            rs  =   st.executeQuery();
            while(rs.next()){
                if( lista == null) lista = new LinkedList();
                Imagen imagen = new Imagen();
                   imagen.setActividad     ( rs.getString("id")      );

                   imagen.setDocumento     ( rs.getString("document")      );
                   imagen.setNameImagen    ( rs.getString("filename")      );
                   imagen.setBinary        ( rs.getBinaryStream("filebinary"));
                   imagen.setCreation_user ( rs.getString("creation_user"));
                   String fileName        = imagen.getNameImagen();
                   String[] name          = fileName.split(".");
                   if(name.length>0)
                          fileName        = name[0] + rs.getString("filename").replaceAll(" |.|-|:","") + name[1];

                   imagen.setFileName      ( fileName);
                   imagen.setFecha_creacion (rs.getString("creation_date").substring(0,16));
                   try{

                 //--- Eliminamos la Imagen
                 //      delete(ruta,fileName);
                 //Copiamos el archivo de la carpeta origen a la carpeta destino
                    InputStream in = new FileInputStream(rutaOrigen + imagen.getDocumento()+ "/" +fileName);
                    OutputStream out = new FileOutputStream(rutaDestino + imagen.getActividad()+"__"+fileName);

                    // Copy the bits from instream to outstream
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                       
                 //--- Escribimos el archivo :
                     /*  InputStream      in   = rs.getBinaryStream("filebinary");
                       int data;

                       File             f    = new File( ruta + imagen.getActividad()+"__"+fileName );
                       FileOutputStream out  = new FileOutputStream(f);
                       while( (data = in.read()) != -1 )
                           out.write( data );

                       in.close();
                       out.close();*/
                   }catch(Exception k){ throw new Exception(" FILE: " + k.getMessage());}


                lista.add( imagen );
            }


        }catch(Exception e){
            System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error searcImagenes() [DAO] : " + sql +" ->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return lista;
    }
    
     public boolean copiarArchivo(
                                 String negocio
                                ,String rutaOrigen, String rutaDestino
                                ,String filename
                               ) throws SQLException {

        boolean swFileCopied = false;
        try{            
          
            File carpetaDestino =new File(rutaDestino);
          
            deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);
                          
            try{
                    //Copiamos el archivo de la carpeta origen a la carpeta destino
                    InputStream in = new FileInputStream(rutaOrigen + negocio + "/" +filename);
                    OutputStream out = new FileOutputStream(rutaDestino + filename);

                    // Copy the bits from instream to outstream
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                    swFileCopied = true;
               
                   }catch(Exception k){ throw new Exception(" FILE: " + k.getMessage());}

            }catch(Exception e){
                System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
                throw new SQLException("Error searchImagenes() [DAO] : ->"+ e.getMessage());
            }
       
        return swFileCopied;
    }
   
    public void createDir(String dir) throws Exception{
        try{
        File f = new File(dir);
        if(! f.exists() ) f.mkdir();
     /*   else{
            File []arc =  f.listFiles();
            while( arc.length >0  ){
               File  imagen = arc[0];
               imagen.delete();
               arc =  f.listFiles();
            }
        }*/
        }catch(Exception e){ throw new Exception ( e.getMessage());}
   }

    static public boolean deleteDirectory(File path) {
    if( path.exists() ) {
      File[] files = path.listFiles();
      for(int i=0; i<files.length; i++) {
         if(files[i].isDirectory()) {
           deleteDirectory(files[i]);
         }
         else {
           files[i].delete();
         }
      }
    }
    return( path.delete() );
  }
    
    public String getPathDocument(String documento)  throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_PATH_DOCUMENTO";
        String ruta="";
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,documento);

            rs = st.executeQuery();
            if (rs.next()){
                ruta=rs.getString("filepath");
            }
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getPathDocument.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ruta;
    }

    public ArrayList searchNombresArchivos(String numosx,String loginx,String tipito) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_NOMBRES_ARCHIVOS";
        ArrayList nombresArchivos=new ArrayList();

        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,numosx);
            st.setString(2,tipito);
            String[] nombre_archivo;
            rs = st.executeQuery();
            while (rs.next()){
                nombre_archivo=new String[3];
                nombre_archivo[0]=rs.getString("id");
                nombre_archivo[1]=rs.getString("filename");
                nombre_archivo[2]=rs.getString("filepath");
                nombresArchivos.add(nombre_archivo);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nombresArchivos;
    }
    
    public ArrayList searchNombresArchivos(String directorioArchivos, String numosx) throws Exception{        
        ArrayList nombresArchivos=new ArrayList();
        File dir = new File(directorioArchivos + numosx);
        try{
            if (dir.exists()){
                String[] ficheros = dir.list();
                for (int x=0;x<ficheros.length;x++) {
                      nombresArchivos.add(ficheros[x]);
                }
            }
           Util.ordenarListaStringConNumeros(nombresArchivos);
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        return nombresArchivos;
    }

    public String[][] obtenerNombresArchivos(String codigoNegocio) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String[][] nombres = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("OBTENER_NOMBRES_ARCHIVOS"), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ps.setString(1, codigoNegocio);
            rs = ps.executeQuery();
            
            if (rs.last()) {                
                nombres = new String[rs.getRow()][3];
                rs.beforeFirst();
                int i = 0;
                while (rs.next()) {
                    nombres[i][0] = rs.getString(1);
                    nombres[i][1] = rs.getString(2);
                    nombres[i][2] = rs.getString(3);
                    i++;
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
        return nombres;
    }   
    
    /**
     * Obtiene el listado de nombres de las categor�as de archivos asociados a un cr�dito
     * @param codigoNegocio codigo del cr�dito
     * @return lista de categor�as
     */
    public List<Map<String, String>> obtenerCategoriaArchivos(String codigoNegocio) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Map<String, String>> lista = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("OBTENER_CATEGORIA_ARCHIVOS"));
            ps.setString(1, codigoNegocio);
            rs = ps.executeQuery();
            
            lista = new ArrayList<>();
            Map<String, String> m = null;
            while (rs.next()) {
                m = new HashMap<>();
                m.put("id", rs.getString(1));
                m.put("nombre", rs.getString(2));
                m.put("unidadNegocio", rs.getString(3));
                m.put("cantidad", rs.getString(4));
                m.put("tipo_persona", rs.getString(5));
                lista.add(m);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
        return lista;
    }
    
    /**
     * Obtiene el listado de nombres de las categor�as de archivos asociados a un cr�dito
     * @param idCategoria categoria de archivos
     * @param codigoNegocio codigo del cr�dito
     * @param numeroSolicitud n�mero de la solicitud
     * @param tipo tipo de archivo
     * @param tipoPersona categoria de la persona, si es titular o codeudor
     * @return lista de archivos
     */
    public List<Map<String, String>> obtenerArchivosCr�dito(String idCategoria, String codigoNegocio, String numeroSolicitud, String tipo, String tipoPersona) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Map<String, String>> lista = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("OBTENER_URL_ARCHIVOS"));
            ps.setString(1, codigoNegocio);
            ps.setString(2, tipo);
            ps.setString(3, idCategoria);
            ps.setString(4, tipoPersona);
            rs = ps.executeQuery();
            
            lista = new ArrayList<>();
            while (rs.next()) {
                Map<String, String> m = new HashMap<>();
                m.put("path", rs.getString(1));
                m.put("nombre", rs.getString(2));
                lista.add(m);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
        return lista;
    }
    
   /**
     * Busca en la base de datos el nombre correspondiente del archivo a cargar
     * @param id id del nombre del archivo
     * @return 
     */
    public String obtenerNombreArchivo(int id) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String archivo = null;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("OBTENER_NOMBRE_ARCHIVO"));
            ps.setInt(1, id);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                archivo = rs.getString(1);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
        return archivo;
    }
    
    /**
     * Valida si es un negocio con listado viejo (sigue la empanada)
     * @param codigoNegocio
     * @param tipito
     * @return 
     */
    public boolean esListadoViejo(String codigoNegocio, String tipito) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean esViejo = false;
        try {
            conn = this.conectarJNDI();
            ps = conn.prepareStatement(this.obtenerSQL("VALIDAR_ARCHIVOS_VIEJOS"));
            ps.setString(1, codigoNegocio);
            ps.setString(2, tipito);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                esViejo = rs.getInt(1) == 0;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
        return esViejo;
    }
    
    /*public ArrayList searchDatosFacturaEca(String factura_eca) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_FACTURAECA";
        ArrayList AccionesFacturaEca=new ArrayList();

        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,factura_eca);



            rs = st.executeQuery();
            while (rs.next()){
                nombre_archivo=new String[2];
                nombre_archivo[0]=rs.getString("id");
                nombre_archivo[1]=rs.getString("filename");
                AccionesFacturaEca.add(nombre_archivo);
            }

        }catch(Exception e){
            ystem.out.println("error en NegociosApplusDAO.getEstadosApplus.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return AccionesFacturaEca;
    }*/

    public ArrayList getContratistas() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_CONTRATISTAS";
        ArrayList listContratistas=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] contratista;
            rs = st.executeQuery();
            while (rs.next()){
                contratista=new String[2];
                contratista[0]=rs.getString("id_contratista");
                contratista[1]=rs.getString("descripcion");
                listContratistas.add(contratista);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.SQL_SEARCH_CONTRATISTAS.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listContratistas;
    }

    public String[] getExPrefacturaEca(String id_orde)  throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_EXPREFACTURAECA";
        String[] ExPrefacturaEca={"","","",""};
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,id_orde);
            String[] nombre_archivo;
            rs = st.executeQuery();
            if (rs.next()){
                ExPrefacturaEca[0]=rs.getString("exprefactura_eca");
                ExPrefacturaEca[1]=rs.getString("simbolo_variable");
                ExPrefacturaEca[2]=rs.getString("observacion");
                ExPrefacturaEca[3]=rs.getString("fecha_factura_eca");
            }
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getExPrefacturaEca.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ExPrefacturaEca;
    }

    public String[] getExPrefacturaContratista(String id_orde,String id_accio)  throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_EXPREFACTURA";
        String[] ExPrefactura={""};
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,id_orde);
            st.setString(2,id_accio);
            String[] nombre_archivo;
            rs = st.executeQuery();
            if (rs.next()){
                ExPrefactura[0]=rs.getString("exprefactura");


            }
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getExPrefacturaContratista.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ExPrefactura;

    }

    public String getEsquemaComision(String id_orde)  throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ESQUEMA";
        String esquema="";
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,id_orde);

            rs = st.executeQuery();
            if (rs.next()){
                esquema=rs.getString("esquema_comision");
            }
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEsquemaComision.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return esquema;
    }

    public List obtainCxpsContratista(  ) throws SQLException {
        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        List               lista = null;
        String             query = "SQL_SEARCH_CXPS_CONTRATISTAS";
        Connection         con   = null;
        String             sql   = "";
        try{
            lista=new LinkedList();
            con = this.conectar(query);
            sql          =  this.obtenerSQL( query );
            st  =   con.prepareStatement( sql );
            rs  =   st.executeQuery();
            while(rs.next()){
                FacturaContratista facturaContratista= FacturaContratista.load(rs);
                lista.add( facturaContratista);
            }
        }catch(Exception e){
            System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error searcImagenes() [DAO] : " + sql +" ->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return lista;
    }

    public String updateAccord (String factura_contratista,String factura_formula_prov) throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_ACCORD_PROV";

        try {
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,factura_formula_prov);
            st.setString(2,factura_contratista);

            comando_sql = st.toString();

        }catch(Exception e){
            System.out.println("ERROR DURANTE updateAccord. :"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE updateAccord. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return comando_sql;
    }

    public ArrayList obtainAccords(String[] negocios) throws Exception{
        ArrayList respuesta=new ArrayList();
        String oferticas="";
        for (int i=0;i<negocios.length;i++){
            oferticas=oferticas+negocios[i]+",";
        }
        oferticas=oferticas.substring(0,(oferticas.length()-1) );

        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ACCORDS";

        String             sql   = "";
        try{
            con = this.conectar(query);
            sql          =  this.obtenerSQL( query );
            sql=sql.replaceAll("oferticas",oferticas);
            st  =   con.prepareStatement( sql );
            rs  =   st.executeQuery();
            while(rs.next()){
                NegocioApplus negocioApplus =new   NegocioApplus();
                negocioApplus.setIdEstado(rs.getString("id_estado_negocio"));
                negocioApplus.setNumOs(rs.getString("num_os"));
                negocioApplus.setNombreCliente(rs.getString("nombre_cliente"));
                negocioApplus.setIdCliente(rs.getString("id_cliente"));
                negocioApplus.setAcciones(rs.getString("acciones"));
                negocioApplus.setIdContratista(rs.getString("id_contratista"));
                negocioApplus.setEstado(rs.getString("estado"));
                respuesta.add( negocioApplus);
            }
        }catch(Exception e){
            System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error obtainAccords() [DAO] : " + sql +" ->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }

        return respuesta;
    }

    public String validarCambioEstado(String[] acciones,String nuevo_estado) throws Exception{
        if (nuevo_estado.equals("nada")) {return "ok";}
        String respuesta="ok";
        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        String             query = "SQL_SEARCH_CAMBIO_ESTADO";
        Connection         con   = null;
        String             sql   = "";
        try{

            con = this.conectar(query);
            sql          =  this.obtenerSQL( query );

            for (int i=0;i<acciones.length;i++){

                st  =   con.prepareStatement( sql );
                st.setString(1,acciones[i]);
                st.setString(2,nuevo_estado);
                //.out.println("query::"+st.toString());
                rs  =   st.executeQuery();
                if(!(rs.next())){
                    respuesta="mal";
                    i=acciones.length;
                }
            }
        }catch(Exception e){
            System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error validarCambioEstado() [DAO] : " + sql +" ->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        //.out.println("respuesta"+respuesta);
        return respuesta;
    }

    public ArrayList getEstadosApplusUser(String loginxx) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ESTADOS_USR";
        ArrayList listEstados=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] estado;
            st.setString(1, loginxx);
            rs = st.executeQuery();
            while (rs.next()){
                estado=new String[2];
                estado[0]=rs.getString("codigo");
                estado[1]=rs.getString("estado");
                listEstados.add(estado);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplusUser.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listEstados;
    }


    public ArrayList getNegociosApplus2(String estado,String contratista,String numosxi,String factconformed,String loginx,String id_solici,String nicc ,String nomclie,String id_cliente) throws Exception{
		Connection         con     = null;
		PreparedStatement  st      = null;
		ResultSet          rs      = null;
		String             query   = "SQL_SEARCH_NEGOCIOS2_REMIX";

		ArrayList listNegocios=new ArrayList();
		boolean respuesta=false;
		try{
		    con = this.conectar(query);
		    String sql    =   this.obtenerSQL( query );
		    if (contratista!=null && !(contratista.equals(""))){
		        sql=sql.replaceAll("contratixx", " AND aa.contratista= '"+contratista+"'");//contratixx

		    }else{
		        sql=sql.replaceAll("contratixx", " ");
		    }

		    if (numosxi!=null && !(numosxi.equals(""))){
		        sql=sql.replaceAll("numosxx", " AND (o.num_os LIKE '%' ||  '"+numosxi+"' || '%' OR o.num_os IS NULL)");//AND (o.num_os LIKE '%' || numosxx || '%' OR o.num_os IS NULL)
		    }else{
		        sql=sql.replaceAll("numosxx", " ");
		    }

		    if (estado!=null && !(estado.equals("0"))){
		        sql=sql.replaceAll("estadito", " AND aa.estado='"+estado+"'");//AND a.estado=estadito
		    }else{
		        sql=sql.replaceAll("estadito", " ");
		    }

		    if (id_solici!=null && !(id_solici.equals(""))){//090924
		        sql=sql.replaceAll("id_solicixx", " AND o.id_solicitud='"+id_solici+"'");//
		    }else{
		        sql=sql.replaceAll("id_solicixx", " ");
		    }

		    if ((id_cliente!=null && !(id_cliente.equals("")))&&(nomclie==null || (nomclie.equals("")))){//090924
		        sql=sql.replaceAll("id_clientexxx", " AND o.id_cliente='"+id_cliente+"'");//
		    }else{
		        sql=sql.replaceAll("id_clientexxx", " ");
		    }

		    if (nicc!=null && !(nicc.equals(""))){//090924
		        sql=sql.replaceAll("niccxx", " AND o.nic='"+nicc+"'");//
		    }else{
		        sql=sql.replaceAll("niccxx", " ");
		    }

		    if (nomclie!=null && !(nomclie.equals(""))){//090924
		        sql=sql.replaceAll("nomcliexx", " AND UPPER(cl.nombre) LIKE UPPER('%"+nomclie+"%') ");//
		    }else{
		        sql=sql.replaceAll("nomcliexx", " ");
		    }

		    if ((estado==null || estado.equals("0")) && (numosxi==null || numosxi.equals("")) &&
		        //(contratista==null || contratista.equals("")) &&
		        (id_solici==null || id_solici.equals("")) &&
		        (nicc==null || nicc.equals("")) &&
		        (id_cliente==null || id_cliente.equals("")) &&
		        (nomclie==null || nomclie.equals(""))
		        ){
		        sql=sql.replaceAll("nada", " AND 1=2 ");
		    }else{
		        sql=sql.replaceAll("nada", " ");
		    }

		    if ((estado==null || estado.equals("0") || estado.equals("010")) && (contratista==null || contratista.equals(""))){
		        sql=sql.replaceAll("validacionx", " ");
		    }else{
		        sql=sql.replaceAll("validacionx", " AND 1=3 ");
		    }

		    st            =   con.prepareStatement( sql );
		    System.out.println("sql query:"+sql);
		    rs = st.executeQuery();
		    while (rs.next()){
		        /*o.id_solicitud, cl.id_cliente, cl.nic, cl.nit, cl.nombre, cl.tipo,co.descripcion,
		        (a.administracion+a.imprevisto+a.utilidad+a.material+a.mano_obra+a.transporte) AS total_prev1_calculado,
		        a.estado,o.id_oferta,o.num_os,a.id_accion*/
		        NegocioApplus negocioApplus=new NegocioApplus();
		        negocioApplus.setIdSolicitud(reset(rs.getString("id_solicitud")));
		        negocioApplus.setIdCliente(reset(rs.getString("id_cliente")));
		        negocioApplus.setNicClient(reset(rs.getString("nic")));
		        negocioApplus.setNitClient(reset(rs.getString("nit")));
		        negocioApplus.setNombreCliente(reset(rs.getString("nombre")));
		        negocioApplus.setTipoCliente(reset(rs.getString("tipo")));
		        negocioApplus.setNombreContratista(reset(rs.getString("descripcion")));
		        negocioApplus.setVlr(resetNum(rs.getString("total_prev1_calculado")));
		        negocioApplus.setEstado(reset(rs.getString("estado")));
		        negocioApplus.setId(reset(rs.getString("id_oferta")));
		        negocioApplus.setNumOs(reset(rs.getString("num_os")));
		        negocioApplus.setIdAccion(reset(rs.getString("id_accion")));
		        negocioApplus.setAcciones(reset(rs.getString("acciones")));
		        negocioApplus.setCreacionFechaEntregaOferta(resetFecha1(rs.getString("creacion_fecha_entrega_oferta")));
		        negocioApplus.setFechaEntregaOferta(resetFecha2(rs.getString("fecha_entrega_oferta")));
		        negocioApplus.setUsuarioEntregaOferta(reset(rs.getString("usuario_entrega_oferta")));
		        negocioApplus.setAlcances(reset(rs.getString("alcances")));
		        negocioApplus.setAdiciones(reset(rs.getString("adicionales")));
		        negocioApplus.setConsecutivo_oferta(reset(rs.getString("consecutivo_oferta")));
		        //System.out.println("rs.getString(precio_total)"+rs.getString("precio_total")+"_");//091119
		        negocioApplus.setEcaOferta(resetNum(rs.getString("precio_total")));                                //091119

                        negocioApplus.setMaterial(resetNum(rs.getString("material")));
                        negocioApplus.setMano_obra(resetNum(rs.getString("mano_obra")));
                        negocioApplus.setOtros(resetNum(rs.getString("transporte")));
                        negocioApplus.setPorc_a(resetNum(rs.getString("porc_administracion")));
                        negocioApplus.setPorc_i(resetNum(rs.getString("porc_imprevisto")));
                        negocioApplus.setPorc_u(resetNum(rs.getString("porc_utilidad")));
                        negocioApplus.setAdministracion(resetNum(rs.getString("administracion")));
                        negocioApplus.setImprevisto(resetNum(rs.getString("imprevisto")));
                        negocioApplus.setUtilidad(resetNum(rs.getString("utilidad")));

                        negocioApplus.setIdEstado( reset(rs.getString("id_estado")) );//20100214

		        listNegocios.add(negocioApplus);
		    }
		}catch(Exception e){
		    System.out.println("error en NegociosApplusDAO..."+e.toString()+"__"+e.getMessage());
		    throw new Exception(e.getMessage());
		}
		finally{
		    if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
		    if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
		    if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
		}
		return listNegocios;
	}


    public ArrayList getEstadosApplus2() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ESTADOS2";
        ArrayList listEstados=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] estado;
            rs = st.executeQuery();
            while (rs.next()){
                estado=new String[2];
                estado[0]=rs.getString("table_code");
                estado[1]=rs.getString("referencia");
                listEstados.add(estado);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplus2.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listEstados;
    }

    public NegocioApplus obtainAccione(String solicitud_consultable,String id_accion,String loginx) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ACCIONE";
        Accord accione=new Accord();
        NegocioApplus negapp=new NegocioApplus();
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, id_accion);
            st.setString(2, solicitud_consultable);

            rs = st.executeQuery();

            System.out.println("st"+st.toString());
            if (rs.next()){
                negapp.setAcciones(reset(rs.getString("descripcion")));
                negapp.setObservacion(reset(rs.getString("observaciones")));
                negapp.setSolicitud(reset(rs.getString("descripcion_solicitud")));
                negapp.setFecha(reset(rs.getString("creation_date")));
                negapp.setTipoTrabajo(reset(rs.getString("tipo_trabajo")));
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.obtainAccione.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return negapp;
    }

    public java.util.TreeMap getClientesEca() throws Exception
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_CLIENTES_ECA";
        TreeMap Cust = new TreeMap();
        try{
            ps = this.crearPreparedStatement(Query);
            rs = ps.executeQuery();
            while (rs.next())
            {
                Cust.put(rs.getString("nombre") + "_"+rs.getString("nit") , rs.getString("id_cliente"));
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(Query);
        }
       return Cust;
    }
    private String reset(String val){
            if(val==null)
               val = "";
            return val;
    }

    public String getClientesEca(String cl,String nomselect,String def) throws Exception
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_CLIENTES_ECA2";
        String ret="";
        try{
            ps = this.crearPreparedStatement(Query);
            ps.setString(1, nomselect);
            ps.setString(2, nomselect);
            ps.setString(3, cl);
            ps.setString(4, def);
            ps.setString(5, cl);
            System.out.println(ps.toString());
            rs = ps.executeQuery();
            ret=cl+";;;;;;;;;;";
            if (rs.next())
            ret=ret+rs.getString("nombre");
            /*while (rs.next())
            {
                Cust.put(rs.getString("nombre") + "_"+rs.getString("nit") , rs.getString("id_cliente"));
            }*/
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(Query);
        }
       return ret;
    }



    public String marcarTipoTrabajo(String solicitud_consultable,String id_accion,String loginx,String tipotraba,
            String observaci,String extipotraba,String fecvisitaplan,String fecvisitareal) throws Exception{

        java.util.Date fechaActual = new java.util.Date();
        java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("ddMMyyyy hh:mm");
        String cadenaFecha = formato.format(fechaActual);

        String respuesta="ok";
        PreparedStatement  st    = null;
        String             query = "SQL_MARCAR_TIPO_TRABAJO";
        Connection con=null;
        String ejecutable="";
        try{
            String sql    =   this.obtenerSQL( query );

            if (fecvisitareal!=null && !(fecvisitareal.equals(""))){//091030
                //sql=sql.replaceAll("estaditox", " ,estado='030' ");//20100216
                sql=sql.replaceAll("estaditox", " ,estado=CASE WHEN estado='020' THEN '030' ELSE estado END ");//20100216

            }else{
                sql=sql.replaceAll("estaditox", " ");
            }

            if (fecvisitareal!=null && !(fecvisitareal.equals(""))){//091030
                sql=sql.replaceAll("fecvisitarealx", " ,fec_visita_hecha='"+fecvisitareal+"' , user_visita_hecha='"+loginx+"', creation_fec_visita_hecha=NOW() ");
            }else{
                sql=sql.replaceAll("fecvisitarealx", " ");
            }

            if (fecvisitaplan!=null && !(fecvisitaplan.equals(""))){//091030
                sql=sql.replaceAll("fecvisitaplanx", " ,fec_visita_planeada='"+fecvisitaplan+"' ");
            }else{
                sql=sql.replaceAll("fecvisitaplanx", " ");
            }

            con= this.conectar(query);
            st            =   con.prepareStatement( sql );
            //st = this.crearPreparedStatement(query);
            st.setString       (1, tipotraba);
            st.setString       (2, "\n("+cadenaFecha+") "+loginx+": "+observaci);
            st.setString       (3, loginx);
            st.setString       (4, id_accion);
            st.setString       (5, solicitud_consultable);

            ejecutable=ejecutable+st.toString();

            //st.execute();

            if (extipotraba.equals("")){//si se puso el tipo de trabajo y era el unico tipo de trabajo pendiente para esa solicitud se cambia el estado a pendiente por hacer cotizacion

                    PreparedStatement ps = null;
                    ResultSet rs = null;
                    String  Query = "SQL_ESTADO_TIPO_TRABAJO";
                    try{
                        ps = this.crearPreparedStatement(Query);

                        ps.setString(1,solicitud_consultable);
                        ps.setString(2,id_accion);

                        rs = ps.executeQuery();
                        if (rs.next()){//si no era el unico tipo de trabajo pendiente para esa solicitud
                            //ejecutable.replaceAll("estaditox", " ");
                            HSendMail2 hSendMail2=new HSendMail2();
                            hSendMail2.start("imorales@fintravalores.com",
                                "imorales@fintravalores.com", "", "",
                                "tipo de trabajo",
                                "Ha sido puesto el tipo de trabajo para la solicitud "+solicitud_consultable+" en el item "+id_accion+" . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);
                        }else{             //si era el unico tipo de trabajo pendiente para esa solicitud

                            HSendMail2 hSendMail2=new HSendMail2();
                            hSendMail2.start("imorales@fintravalores.com",
                                "imorales@fintravalores.com", "", "",
                                "pendiente cotizacion",
                                "Ha sido puesto el tipo de trabajo para la solicitud "+solicitud_consultable+" en el item "+id_accion+" . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);
                        }




                    }
                    catch (Exception ex)
                    {
                        System.out.println("error en SQL_ESTADO_TIPO_TRABAJO"+ex.toString());
                        ex.printStackTrace();
                        throw new Exception(ex.getMessage());
                    }
                    finally
                    {
                        if (rs!=null) rs.close();
                        if (ps!=null) ps.close();
                        this.desconectar(Query);
                    }



            }

            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
            if(!ejecutable.equals("")){
                try{
                    scv.crearStatement();
                    scv.getSt().addBatch(ejecutable);
                    scv.execute();
                }catch(SQLException e){
                    throw new SQLException("ERROR DURANTE INSERCION DE FACTURAS"+e.getMessage());
                }
            }
            ejecutable=null;

        }catch(Exception e){
            System.out.println("marcarTipoTrabajo error:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error marcarTipoTrabajo() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public String asignar(String loginx, String fecof,String solicitudes[]) throws Exception{
        String oferticas="";
        for (int i=0;i<solicitudes.length;i++){
            oferticas=oferticas+"'"+solicitudes[i]+"',";
        }
        if (oferticas.length()>1){
            oferticas=oferticas.substring(0,(oferticas.length()-1) );
        }

        String respuesta="ok";
        PreparedStatement  st    = null;
        String             query = "SQL_ASIGNAR";
        Connection con=null;
        try{
            String sql    =   this.obtenerSQL( query );
            con = this.conectar(query);
            sql=sql.replaceAll("solicitudesx",oferticas);
            st            =   con.prepareStatement( sql );
            System.out.println("sqll:::"+sql);
            st.setString       (1, loginx);
            st.setString       (2, loginx);
            st.setString       (3, fecof);
            String estad_tem="060";
            st.setString       (4, estad_tem);
            st.setString       (5, estad_tem);
            int affectadas=st.executeUpdate();
            if (affectadas>0 ){
                HSendMail2 hSendMail2=new HSendMail2();
                            hSendMail2.start("imorales@fintravalores.com",
                                "imorales@fintravalores.com", "", "",
                                "pendiente aceptacion",
                                "Ha sido entregada la oferta de las "+affectadas+" solicitudes "+oferticas+". Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);
            }
        }catch(Exception e){
            System.out.println("asignar error:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("Error asignar() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return respuesta;
    }
    private String resetFecha1(String val){
            if(val==null){val = "";}
            if (val.equals("0099-01-01 00:00:00")){val="";}
            if (val.length()>16){val=val.substring(0,16);}
            return val;
    }
    private String resetFecha2(String val){
            if(val==null){val = "";}
            if (val.equals("0099-01-01 00:00:00")){val="";}
            if (val.length()>10){val=val.substring(0,10);}
            return val;
    }

    public String validacionSeguridadSolicitud(String id_solicitudx,String id_accionx,String loginxx,String pasoadar){
        return "";
    }

    private String resetNum(String val){
            if(val==null)
               val = "0";
            return val;
    }

    public String getIdClie(String nic)  throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_ID_CLIENTE_NIC";
        String respuesta="";
        try{
            ps = this.crearPreparedStatement(Query);
            ps.setString(1, nic);
            rs = ps.executeQuery();
            if (rs.next())            {
                respuesta=rs.getString("id_cliente");
            }
        }
        catch (Exception ex)
        {
            System.out.println("error en getIdClie__"+ex.toString());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(Query);
        }
       return respuesta;
    }

    public NegocioApplus obtainAlcanc(String solicitud_consultable,String id_accion,String loginx) throws Exception{//091016
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ALCANCES";
        Accord accione=new Accord();
        NegocioApplus negapp=new NegocioApplus();
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, id_accion);
            st.setString(2, solicitud_consultable);

            rs = st.executeQuery();

            //System.out.println("st"+st.toString());
            if (rs.next()){
                negapp.setAcciones(reset(rs.getString("descripcion")));
                negapp.setObservacion(reset(rs.getString("observaciones")));
                negapp.setSolicitud(reset(rs.getString("descripcion_solicitud")));
                negapp.setFecha(reset(rs.getString("creation_date")));
                negapp.setTipoTrabajo(reset(rs.getString("tipo_trabajo")));

                negapp.setAlcances(reset(rs.getString("alcances")));
                negapp.setAdiciones(reset(rs.getString("adicionales")));
                negapp.setTrabajo(reset(rs.getString("trabajo")));

                negapp.setFecVisitaPlan(reset(rs.getString("fec_visita_planeada")));//091030
                negapp.setFecVisitaReal(reset(rs.getString("fec_visita_hecha")));//091030
                negapp.setUsrVisitaHecha(reset(rs.getString("user_visita_hecha")));//091030
                negapp.setCreationFecVisitaHecha(reset(rs.getString("creation_fec_visita_hecha")));//091030

            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.obtainALCANCES.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return negapp;
    }

    public String marcarAvance(String solicitud_consultable,String id_accion,String loginx,String alcanci,String adicioni,String trabaji)
         throws Exception {

        System.out.println("solicitud_consultable"+solicitud_consultable+"id_accion"+id_accion+"loginx"+loginx+"alcanci"+alcanci+"adicion1"+adicioni);
        java.util.Date fechaActual = new java.util.Date();
        java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("ddMMyyyy hh:mm");
        String cadenaFecha = formato.format(fechaActual);
        String respuesta="ok";
        PreparedStatement  st    = null;
        String             query = "SQL_MARCAR_ALCANCE";
        Connection con=null;
        String ejecutable="";
        try{
            String sql    =   this.obtenerSQL( query );
            con= this.conectar(query);
            st            =   con.prepareStatement( sql );
            //st = this.crearPreparedStatement(query);
            st.setString       (1, alcanci);
            st.setString       (2, adicioni);
            st.setString       (3, loginx);
            st.setString       (4, trabaji);

            st.setString       (5, id_accion);
            st.setString       (6, solicitud_consultable);

            ejecutable=ejecutable+st.toString();

            //st.execute();

            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
            if(!ejecutable.equals("")){
                try{
                    scv.crearStatement();
                    scv.getSt().addBatch(ejecutable);
                    scv.execute();
                }catch(SQLException e){
                    throw new SQLException("ERROR DURANTE SQL_MARCAR_AVANCE"+e.getMessage());
                }
            }
            ejecutable=null;

        }catch(Exception e){
            System.out.println("marcarAvance error:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error marcarAvance() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public String modificarProducto(String exproducto,String consec,String descr,double pre,String tipo,String medida,String loginx, String categoria)  throws Exception{
        String respuesta="ok";
        PreparedStatement  st    = null;
        String             query1 = "SQL_INSERT_PRODUCT";
        String             query2 = "SQL_UPDATE_PRODUCT";
        Connection con=null;
        String ejecutable="";
        try{
            String sql    =   this.obtenerSQL( query1 );
            con= this.conectar(query1);
            st            =   con.prepareStatement( sql );

            st.setString       (1, descr);
            st.setString       (2, ""+pre);
            st.setString       (3, consec);
            st.setString       (4, tipo);
            st.setString       (5, loginx);
            st.setString       (6, medida);
            st.setString       (7, exproducto);
            st.setString       (8, categoria);//20100218

            ejecutable=ejecutable+st.toString();

            sql    =   this.obtenerSQL( query2 );
            st            =   con.prepareStatement( sql );

            st.setString       (1, loginx);
            st.setString       (2, exproducto);

            ejecutable=ejecutable+st.toString();

            System.out.println("ejecutable"+ejecutable);

            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
            if(!ejecutable.equals("")){
                try{
                    scv.crearStatement();
                    scv.getSt().addBatch(ejecutable);
                    scv.execute();
                }catch(SQLException e){
                    throw new SQLException("ERROR DURANTE SQL_INSERT_PRODUCT"+e.getMessage());
                }
            }
            ejecutable=null;

        }catch(Exception e){
            System.out.println("modificarProducto error:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error modificarProducto() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query1);
        }
        return respuesta;
    }

    public Material getMaterial(String pk_material) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_MATERIAL";
        Material m=new Material();
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, pk_material);

            rs = st.executeQuery();

            if (rs.next()){
                //semi load de material
                m.setRegStatus(reset(rs.getString("reg_status")));
                m.setIdMaterial(reset(rs.getString("idmaterial")));
                m.setDescripcion(reset(rs.getString("descripcion")));
                m.setValor(Double.parseDouble((rs.getString("precio"))));
                m.setCodigo(reset(rs.getString("cod_material")));
                m.setTipo(reset(rs.getString("tipo_material")));
                m.setLastUpdate(reset(rs.getString("last_update")));
                m.setUserUpdate(reset(rs.getString("user_update")));
                m.setAprobacion(reset(rs.getString("aprobacion")));
                m.setMedida(reset(rs.getString("medida")));
                m.setValorCompra(Double.parseDouble(rs.getString("valor_compra")));
                m.setIdMaterialAsociado(reset(rs.getString("idmaterial_asociado")));
                m.setFechaAnulacion(reset(rs.getString("fecha_anulacion")));
                m.setUserAnulacion(reset(rs.getString("user_anulacion")));
                m.setCategoria(reset(rs.getString("categoria")));
                /*reg_status, idmaterial, descripcion, precio, cod_material, tipo_material,
               last_update, user_update, aprobacion, medida, valor_compra, idmaterial_asociado,
               fecha_anulacion, user_anulacion*/
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getMaterial.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return m;
    }

    public String asignarEstado(String loginx, String estado_asignable,String acciones[]) throws Exception{
        String accioncitas="";
        for (int i=0;i<acciones.length;i++){
            accioncitas=accioncitas+"'"+acciones[i]+"',";
        }
        if (accioncitas.length()>1){
            accioncitas=accioncitas.substring(0,(accioncitas.length()-1) );
        }

        String respuesta="ok";
        PreparedStatement  st    = null;
        String             query = "SQL_ASIGNAR_ESTADO";
        Connection con=null;
        try{
            String sql    =   this.obtenerSQL( query );
            con = this.conectar(query);
            sql=sql.replaceAll("accionesx",accioncitas);
            st            =   con.prepareStatement( sql );
            System.out.println("sqll:::"+sql);
            st.setString       (1, loginx);
            st.setString       (2, estado_asignable);
            //st.setString       (3, accioncitas);

            int affectadas=st.executeUpdate();

        }catch(Exception e){
            System.out.println("asignarEstado error:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("Error asignarEstado() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public BeanGeneral getDatRecepObra(String solicitud) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DAT_RECEP_OBRA";
        BeanGeneral respuesta =new BeanGeneral();

        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, solicitud);

            rs = st.executeQuery();
            String num_os="";
            Vector  dat_recep_obra=new Vector();
            while (rs.next()){
                BeanGeneral bg =new BeanGeneral();
                respuesta.setValor_01(reset(rs.getString("nombre_cliente")));
                respuesta.setValor_02(reset(rs.getString("consecutivo_oferta")));
                respuesta.setValor_03(reset(rs.getString("num_os")));
                bg.setValor_01(resetFecHoy(rs.getString("fecha_finalizacion")));
                bg.setValor_02(reset(rs.getString("contratista")));
                bg.setValor_03(reset(rs.getString("id_accion")));
                bg.setValor_04(reset(rs.getString("descripcion")));
                bg.setValor_05(resetFecHoy(rs.getString("fecha_interventoria")));
                bg.setValor_06(reset(rs.getString("estado")));
                bg.setValor_07(reset(rs.getString("estadito")));
                bg.setValor_08(reset(rs.getString("observacion_recepcion")));
                bg.setValor_09(reset(rs.getString("fec_creacion_recepcion")));
                bg.setValor_10(reset(rs.getString("user_recepcion")));

                dat_recep_obra.add(bg);

            }
            respuesta.setVec(dat_recep_obra);

        }catch(Exception e){
            System.out.println("error en getDatRecepObra en negappdao.."+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }

    public String aceptarRecepObra(String solicitud,String[] observaciones,String[] acciones,String[] fecFin,String[] fecInterventor,String loginx) throws Exception{

        String respuesta="ok";
        PreparedStatement  st    = null;
        String             query = "SQL_RECIBIR_OBRA";
        Connection con=null;
        String gransql="";
        try{
            String sql    =   this.obtenerSQL( query );
            con = this.conectar(query);
            st            =   con.prepareStatement( sql );

            for (int i=0;i<observaciones.length;i++){
                st.setString (1, loginx);
                st.setString (2, loginx);
                st.setString (3, fecFin[i]);
                st.setString (4, fecInterventor[i]);
                st.setString (5, observaciones[i]);
                st.setString (6, acciones[i]);

                gransql=gransql+st.toString();
            }

            int affectadas=st.executeUpdate(gransql);
            respuesta="Obra recibida.";
        }catch(Exception e){
            respuesta="Error:"+e.toString();
            System.out.println("aceptarRecepObra error:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("Error aceptarRecepObra() : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    private String resetFecHoy(String val){//20100222
        if(val==null || val.startsWith("0099-01-01")){
            val = Utility.getHoy("-");;
        }
        return val;
    }
}
