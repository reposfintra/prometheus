/************************************************************************
 * Nombre clase: Ingreso_detalleDAO.java                                *
 * Descripci�n: Clase que maneja las consultas de los Items             *
 *              de Las Facturas                                         *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: 9 de mayo de 2006, 10:19 AM                                   *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

public class Ingreso_detalleDAO extends MainDAO{
    private Ingreso_detalle ingreso_detalle;
    private Ingreso_detalle cabecera;
    private Ingreso_detalle factura;
    private Vector vecIngreso_detalle = new Vector ();
    private Vector vecFactura = new Vector ();
    /** Creates a new instance of Ingreso_detalleDAO */
    public Ingreso_detalleDAO () {
        super ( "Ingreso_detalleDAO.xml" );
    }
    public Ingreso_detalleDAO (String dataBaseName) {
        super ( "Ingreso_detalleDAO.xml", dataBaseName);
    }
    
    /**
     * Getter for property ingreso_detalle.
     * @return Value of property ingreso_detalle.
     */
    public com.tsp.operation.model.beans.Ingreso_detalle getIngreso_detalle () {
        return ingreso_detalle;
    }
    
    /**
     * Setter for property ingreso_detalle.
     * @param ingreso_detalle New value of property ingreso_detalle.
     */
    public void setIngreso_detalle (Ingreso_detalle ingreso_detalle) {
        this.ingreso_detalle = ingreso_detalle;
    }
    
    /**
     * Getter for property vecIngreso_detalle.
     * @return Value of property vecIngreso_detalle.
     */
    public Vector getVecIngreso_detalle () {
        return vecIngreso_detalle;
    }
    
    /**
     * Setter for property vecIngreso_detalle.
     * @param vecIngreso_detalle New value of property vecIngreso_detalle.
     */
    public void setVecIngreso_detalle (Ingreso_detalle ingreso) {
        this.vecIngreso_detalle.addElement ( ingreso );
    }
    
    /**
     * Setter for property vecIngreso_detalle.
     * @param vecIngreso_detalle New value of property vecIngreso_detalle.
     */
    public void setVectorIngreso_detalle (Vector VIngreso) {
        this.vecIngreso_detalle = null;
        this.vecIngreso_detalle = new Vector();
        this.vecIngreso_detalle = VIngreso;
    }    
    
    /**
     * Setter for property vecIngreso_detalle.
     * @param vecIngreso_detalle New value of property vecIngreso_detalle.
     */
    public void resetVecIngreso_detalle () {
        this.vecIngreso_detalle = null;
        this.vecIngreso_detalle = new Vector ();
    }    
    
    /**
     * Setter for property vecIngreso_detalle.
     * @param vecIngreso_detalle New value of property vecIngreso_detalle.
     */
    public void deleteVecIngreso_detalle (int pos){
        vecIngreso_detalle.remove (pos);
    }
    
    /**
     * Getter for property cabecera.
     * @return Value of property cabecera.
     */
    public Ingreso_detalle getCabecera () {
        return cabecera;
    }
    
    /**
     * Setter for property cabecera.
     * @param cabecera New value of property cabecera.
     */
    public void setCabecera (Ingreso_detalle cabecera) {
        this.cabecera = null;
        this.cabecera = new Ingreso_detalle();
        this.cabecera = cabecera;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public Ingreso_detalle getFactura () {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura (Ingreso_detalle factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property vecFactura.
     * @return Value of property vecFactura.
     */
    public java.util.Vector getVecFactura () {
        return vecFactura;
    }
    
    /**
     * Setter for property vecFactura.
     * @param vecFactura New value of property vecFactura.
     */
    public void setVecFactura (java.util.Vector vecFactura) {
        this.vecFactura = vecFactura;
    }    
   
    /**
     * Metodo: anularIngresoDetalle, permite anular un registro en la tabla ingreso_detalle en la BD.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String anularIngresoDetalle () throws Exception {
        Connection con = null;
        StringStatement st = null;
        String sql ="";
        String query = "SQL_ANULAR_ITEM";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = new StringStatement(this.obtenerSQL(query),true);//JJCastro fase2
            st.setString (1,  ingreso_detalle.getCreation_user ());
            st.setString (2,  ingreso_detalle.getDistrito ());
            st.setString (3,  ingreso_detalle.getTipo_documento ());
            st.setString (4,  ingreso_detalle.getNumero_ingreso ());
            st.setInt    (5,  ingreso_detalle.getItem ());
            sql = st.getSql ();
        }}catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LOS ITEMS DE LOS INGRESOS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (st  != null){ st=null;}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }
    
    
    
    /**
     * Metodo: monedaLocal, permite buscar la moneda local del distrito
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String monedaLocal ( String distrito ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String moneda = "";
        String query = "SQL_MONEDA_LOCAL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            rs = st.executeQuery ();
            if( rs.next () ){
                moneda =  rs.getString ("moneda") ;
            }
        }}catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LA MONEDA" + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return moneda;
    }
    
    /**
     * Metodo: datosListadoFactura, permite listar las facturas de un cliente
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void datosListadoFactura ( String distrito, String nit_cliente, String tipo_documento ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        factura = null;
        vecFactura = null;
        String query = "SQL_CONSULTA_LISTADO_FACTURA";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, nit_cliente );
            rs = st.executeQuery ();
            vecFactura = new Vector ();
            while( rs.next () ){
                factura = new Ingreso_detalle ();
                factura.setNegaso(rs.getString("negocio"));
                factura.setFactura ( rs.getString ("documento") );
                factura.setMoneda_factura ( rs.getString ("moneda_factura")!=null?!rs.getString ("moneda_factura").equals ("")?rs.getString ("moneda_factura"):"PES":"PES" );
                factura.setFecha_factura ( rs.getString ("fecha_factura") );
                factura.setDescripcion_factura ( rs.getString ("descripcion") );
                factura.setValor_saldo_factura ( rs.getDouble ("valor_saldo") );
                factura.setValor_saldo_fact ( rs.getDouble ("valor_saldo") );
                factura.setValor_saldo_factura_me ( rs.getDouble ("valor_saldome") );
                factura.setValor_factura ( rs.getDouble ("valor_factura") );
                factura.setValor_factura_me ( rs.getDouble ("valor_facturame") ); 
                factura.setValor_tasa_factura ( rs.getDouble ("valor_tasa"));
                factura.setAgencia_facturacion ( rs.getString ("agencia_facturacion"));
                factura.setCuenta( rs.getString ("cuenta")!=null?rs.getString ("cuenta"):"");
                factura.setNumCuota(rs.getString ("cuota")!=null?rs.getString ("cuota"):"-");
                factura.setAuxiliar( "" );
                factura.setTipo_aux( "" );
                
                vecFactura.add ( factura );
            }
        }} catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LAS FACTURAS" + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }    
    
    
    /**
     * Metodo: datosListadoFactura, permite listar las facturas de un cliente
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void datosListadoFacturaUpdate ( String distrito, String nit_cliente, String tipo_documento ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        factura = null;
        vecFactura = null;
        String query = "SQL_CONSULTA_LISTADO_FACTURA_UPDATE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, tipo_documento );
            st.setString ( 2, distrito );
            st.setString ( 3, nit_cliente );
            rs = st.executeQuery ();
            vecFactura = new Vector ();
            while( rs.next () ){
                factura = new Ingreso_detalle ();
                factura.setFactura ( rs.getString ("documento") );
                factura.setMoneda_factura ( rs.getString ("moneda_factura")!=null?!rs.getString ("moneda_factura").equals ("")?rs.getString ("moneda_factura"):"PES":"PES" );
                factura.setFecha_factura ( rs.getString ("fecha_factura") );
                factura.setDescripcion_factura ( rs.getString ("descripcion") );
                factura.setValor_saldo_factura ( rs.getDouble ("valor_saldo") );
                factura.setValor_saldo_fact ( rs.getDouble ("valor_saldo") );
                factura.setValor_saldo_factura_me ( rs.getDouble ("valor_saldome") );
                factura.setValor_factura ( rs.getDouble ("valor_factura") );
                factura.setValor_factura_me ( rs.getDouble ("valor_facturame") ); 
                factura.setValor_tasa_factura ( rs.getDouble ("valor_tasa"));
                factura.setAgencia_facturacion ( rs.getString ("agencia_facturacion"));
                factura.setCuenta( rs.getString("cuenta")!=null?rs.getString ("cuenta"):"" );
                factura.setAuxiliar( "" );
                factura.setTipo_aux( "" );
                vecFactura.add ( factura );
            }
            }} catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LAS FACTURAS" + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }   
    
    /**
     * Metodo: valorConversionTasa, permite buscar el valor de convercion de la tasa
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public double valorConversionTasa ( String moneda_origen, double valor_origen, String moneda_destino, String distrito, String valor ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double tasa = 0;
        String query = "SQL_CONSULTA_TASA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, moneda_origen );
            st.setDouble ( 2, valor_origen );
            st.setString ( 3, moneda_destino );
            st.setString ( 4, distrito );
            st.setString ( 5, valor );
            rs = st.executeQuery ();
            while( rs.next () ){
                tasa=rs.getDouble("convertir_moneda");
            }
        }}catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LAS FACTURAS" + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return tasa;
    }  
    
     /* Metodo: updateCantidadItemsIngreso, permite actualizar la cantidad de item de la tabla ingreso en la BD.
     * @autor : Ing. Jose de la rosa
     * @param : cantidad de items de ingreso, numero del ingreso, distrito, el tipo de documento
     * @version : 1.0
     */
    public String updateCantidadItemsIngreso ( int cantidad, String num_ingreso, String distrito,String tipo_ingreso, String tipo_documento) throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_CANT_ITEMS";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setInt    (1,  cantidad);
            st.setString (2,  num_ingreso);
            st.setString (3,  distrito);
            st.setString (4,  tipo_ingreso);
            st.setString (5,  tipo_documento);
            sql = st.getSql();
        }catch(Exception ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LA CANTIDAD DE ITEMS DEL INGRESO, "+ex.getMessage ());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
        return sql;
    }    
    
     /* Metodo: updateSaldoFactura, permite actualizar el saldo de la factura
      * @autor : Ing. Jose de la rosa
      * @param : valor del abono de la factura, valor del abono moneda extranjera de la factura, factura, distrito, el tipo de documento
      * @version : 1.0
      */
    public String updateSaldoFactura (double abono, double abono_me, String factura, String distrito, String tipo_documento) throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_FACTURA";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setDouble (1,  abono);
            st.setDouble (2,  abono);
            st.setDouble (3,  abono_me);
            st.setDouble (4,  abono_me);
            st.setString (5,  factura);
            st.setString (6,  distrito);
            sql = st.getSql();//JJCastro fase2
        }catch(Exception ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LOS SALDOS DE LAS FACTURAS, "+ex.getMessage ());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
         }
        return sql;
    }    
    
    /* Metodo: updateSaldoFactura, permite actualizar el saldo de la factura
      * @autor : Ing. Jose de la rosa
      * @param : valor del abono de la factura, valor del abono moneda extranjera de la factura, factura, distrito, el tipo de documento
      * @version : 1.0
      */
    public String anularSaldoFactura (double abono, double abono_me, String factura, String distrito, String tipo_documento) throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_ANULAR_SALDO_FACTURA";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2 sub
            st.setDouble (1,  abono);
            st.setDouble (2,  abono);
            st.setDouble (3,  abono_me);
            st.setDouble (4,  abono_me);
            st.setString (5,  factura);
            st.setString (6,  distrito);
            sql = st.getSql();
        }catch(Exception ex){
            throw new SQLException ("ERROR AL ANULAR LOS SALDOS DE LAS FACTURAS, "+ex.getMessage ());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
         }
        return sql;
    } 
    
    /* Metodo: updateSaldoFactura_modificacion, permite actualizar el saldo de la factura
      * @autor : Ing. Jose de la rosa
      * @param : valor del abono de la factura, valor del abono moneda extranjera de la factura, factura, distrito, el tipo de documento
      * @version : 1.0
      */
    public String updateSaldoFactura_modificacion (double abonoA, double abono_meA, double abonoV, double abono_meV, String factura, String distrito, String tipo_documento) throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_FACTURA_MODIFCACION";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setDouble (1,  abonoV );
            st.setDouble (2,  abonoA);
            st.setDouble (3,  abonoV );
            st.setDouble (4,  abonoA);
            st.setDouble (5,  abono_meV);
            st.setDouble (6,  abono_meA);
            st.setDouble (7,  abono_meV);
            st.setDouble (8,  abono_meA);
            st.setString (9,  factura);
            st.setString (10,  distrito);
            st.setString (11,  tipo_documento);
            sql = st.getSql();
        }catch(Exception ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LOS SALDOS DE LAS FACTURAS, "+ex.getMessage ());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    } 
    
    /**
     * Metodo: existeItem, permite verificar si existe el item de ingreso
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public boolean existeItem ( String distrito, String numero_ingreso, String tipo_documento, int item ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw=false;
        String query = "SQL_EXISTE_ITEM";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, numero_ingreso );
            st.setString ( 2, tipo_documento );
            st.setString ( 3, distrito );
            st.setInt    ( 4, item );
            rs = st.executeQuery ();
            sw = rs.next ();
            }}catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LA CABECERA " + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
        /**
     * Metodo: insertarIngresoMiscelaneoDetalle, permite ingresar un registro
     * en la tabla ingreso_detalle en la BD.
     * @autor : Ing. Diogenes Bastidas M
     * @param :
     * @version : 1.0
     */
    public String insertarIngresoMiscelaneoDetalle(Vector vecitems) throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_INGRESAR_MISCELANEO";
        String cad = "";
        try{
            cad = this.obtenerSQL(query);//JJCastro fase2
            for(int i=0; i< vecitems.size(); i++ ){

                st = new StringStatement(cad, true);//JJCastro fase2 sin

                Ingreso_detalle ingreso_detalle = (Ingreso_detalle) vecitems.get(i);
                st.setString(1,  ingreso_detalle.getDistrito());
                st.setString(2,  ingreso_detalle.getNumero_ingreso());
                st.setInt  (3,  ingreso_detalle.getItem() );
                st.setString(4,  ingreso_detalle.getCuenta());
                st.setString(5,  ingreso_detalle.getAuxiliar());
                st.setString(6,  ingreso_detalle.getDescripcion());
                st.setString(7, ingreso_detalle.getTipo_doc());
                st.setString(8, ingreso_detalle.getDocumento());
                st.setDouble(9,  ingreso_detalle.getValor_ingreso());
                st.setDouble(10,  ingreso_detalle.getValor_ingreso_me());
                st.setString(11,  ingreso_detalle.getCodigo_retefuente());
                st.setDouble(12, ingreso_detalle.getValor_retefuente());
                st.setDouble(13, ingreso_detalle.getValor_retefuente_me());
                st.setString(14, ingreso_detalle.getCodigo_reteica());
                st.setDouble(15, ingreso_detalle.getValor_reteica());
                st.setDouble(16, ingreso_detalle.getValor_reteica_me());
                st.setString(17, ingreso_detalle.getCreation_user());
                st.setString(18, ingreso_detalle.getBase());
                st.setString(19, ingreso_detalle.getTipo_documento());
                sql += st.getSql();//JJCastro fase2
                st =null;
            }
            
        }catch(Exception ex){
            throw new SQLException("ERROR AL INGRESAR LOS ITEMS MISCELANEOS, "+ex.getMessage());
        }finally{
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }
    
    
    /**
     * Metodo: itemsMiscelaneos, permite listar los items de ingreso miscelaneo
     * @autor : Ing. Diogenes Bastidas
     * @param : numero de ingreso, tipo documento, ditrito
     * @version : 1.0
     */
    public Vector itemsMiscelaneo( String num_ingreso, String tipo_doc, String distrito) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        factura = null;
        Vector vec = null;
        double tot = 0;
        String query = "SQL_SEARCH_ITEMS_MIS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, num_ingreso );
            st.setString( 2, tipo_doc );
            st.setString( 3, distrito );
            vec = new Vector();
            rs = st.executeQuery();
                        while( rs.next() ){
                                    Ingreso_detalle items = new Ingreso_detalle();
                                    items.setDistrito(rs.getString("dstrct"));
                                    items.setNumero_ingreso(rs.getString("num_ingreso"));
                                    items.setItem(rs.getInt("item"));
                                    items.setCuenta(rs.getString("cuenta"));
                                    if(rs.getString("auxiliar").length() > 0 ){
                                        String[] aux = rs.getString("auxiliar").split("-");
                                        items.setTipo_aux(aux[0]);
                                        items.setAuxiliar(aux[1]);
                                    }else{
                                        items.setTipo_aux("");
                                        items.setAuxiliar("");
                                    }
                                    items.setDescripcion(rs.getString("descripcion"));
                                    items.setTipo_doc(rs.getString("tipo_doc"));
                                    items.setDocumento(rs.getString("documento"));
                                    items.setValor_ingreso(rs.getDouble("valor_ingreso"));
                                    items.setValor_ingreso_me(rs.getDouble("valor_ingreso_me"));
                                    items.setFecha_anulacion( rs.getString("fecha_anulacion") );

                                    items.setCodigo_retefuente( rs.getString("codigo_retefuente") );
                                    items.setCodigo_reteica( rs.getString("codigo_reteica") );
                                    items.setValor_retefuente( rs.getDouble("valor_retefuente") );
                                    items.setValor_reteica( rs.getDouble("valor_reteica") );
                                    items.setValor_retefuente_me( rs.getDouble("valor_retefuente_me") );
                                    items.setValor_reteica_me( rs.getDouble("valor_reteica_me") );

                                    items.setValor_total(rs.getDouble("valor_ingreso_me") - ( rs.getDouble("valor_retefuente_me")+rs.getDouble("valor_reteica_me")));
                                    items.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
                                    vec.add( items );
                                    items = null;//Liberar Espacio JJCastro
                        }
        }}catch( SQLException e ){
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LOS ITEMS" + e.getMessage() + " " + e.getErrorCode() );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vec;
    }
    
    /**
     * Metodo: actualizarIngresoMiscelaneo,
     * actualiza el registro items de ingreso miscelaneo
     * @autor : Ing. Diogenes Bastidas
     * @param :
     * @version : 1.0
     */
    public String actualizarIngresoMiscelaneo(Vector itemsMiscelaneo) throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_ITEMS_MIS";
        String cad = "";
        try{
            cad = this.obtenerSQL(query);//JJCastro fase2
            for(int i=0; i< itemsMiscelaneo.size(); i++ ){

                st = new StringStatement(cad, true);//JJCastro fase2 sin

                Ingreso_detalle ingreso_detalle = (Ingreso_detalle) itemsMiscelaneo.get(i);
                st.setString(1, ingreso_detalle.getCuenta());
                st.setString(2, ingreso_detalle.getAuxiliar());
                st.setString(3, ingreso_detalle.getDescripcion());
                st.setString(4, ingreso_detalle.getTipo_doc());
                st.setString(5, ingreso_detalle.getDocumento());
                
                st.setDouble(6,  ingreso_detalle.getValor_ingreso());
                st.setDouble(7,  ingreso_detalle.getValor_ingreso_me());
                
                st.setString(8,  ingreso_detalle.getCodigo_retefuente());
                st.setDouble(9,  ingreso_detalle.getValor_retefuente());
                st.setDouble(10,  ingreso_detalle.getValor_retefuente_me());
                
                st.setString(11, ingreso_detalle.getCodigo_reteica());
                st.setDouble(12,  ingreso_detalle.getValor_reteica());
                st.setDouble(13,  ingreso_detalle.getValor_reteica_me());
                st.setString(14,  ingreso_detalle.getCreation_user());
                
                st.setString(15,  ingreso_detalle.getDistrito());
                st.setString(16, ingreso_detalle.getTipo_documento());
                st.setString(17, ingreso_detalle.getNumero_ingreso());
                st.setInt    (18, ingreso_detalle.getItem());
                
                sql += st.getSql();
                st = null;
            }
        }catch(Exception ex){
            throw new SQLException("ERROR AL ACTUALIZAR LOS ITEMS DE LOS INGRESOS, "+ex.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }
    
    /**
     * Metodo: itemsMiscelaneos, permite listar los items de ingreso miscelaneo
     * @autor : Ing. Diogenes Bastidas
     * @param : numero de ingreso, tipo documento, ditrito
     * @version : 1.0
     */
    public Vector codigositemsMiscelaneo( String num_ingreso, String tipo_doc, String distrito) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        factura = null;
        Vector vec = null;
        String query = "SQL_ITEMS_MIS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, num_ingreso );
            st.setString( 2, tipo_doc );
            st.setString( 3, distrito );
            vec = new Vector();
            rs = st.executeQuery();
            while( rs.next() ){
                Ingreso_detalle items = new Ingreso_detalle();
                items.setDistrito(rs.getString("dstrct"));
                items.setNumero_ingreso(rs.getString("num_ingreso"));
                items.setItem(rs.getInt("item"));
                vec.add( items );
               items = null;//Liberar Espacio JJCastro
            }
        }} catch( SQLException e ){
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LOS ITEMS" + e.getMessage() + " " + e.getErrorCode() );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vec;
    }
    
    
    /**
     * Metodo: anularIngresoMiscelaneo,
     * anula los items de un ingreso
     * @autor : Ing. Diogenes Bastidas
     * @param :
     * @version : 1.0
     */
    public String anularIngresoMiscelaneo(String dstrct, String tipo, String nroing, String items, String usuario) throws SQLException {
        StringStatement st = null;
        Connection con =null;
        String sql ="";
        String query = "ANULAR_ITEMS_MISCELANEO";
        try{
            sql = this.obtenerSQL("ANULAR_ITEMS_MISCELANEO").replaceAll("#ITEMS#",items);
            
            st = new StringStatement(sql, true);//JJCastro fase2 sin

            st.setString(1, usuario);
            st.setString(2,  dstrct);
            st.setString(3, tipo);
            st.setString(4, nroing);
            sql = st.toString();
            
        }catch(Exception ex){
            throw new SQLException("ERROR AL ANULAR LOS ITEMS DE INGRESO MISCELANEO, "+ex.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }


/**
 *
 * @param dstrct
 * @param tipo
 * @param nroing
 * @return
 * @throws SQLException
 */
    public int nroMayorItems(String dstrct, String tipo, String nroing) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int num = 1;
        String query = "NRO_MAX_ITEMS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, dstrct );
            st.setString( 2, tipo );
            st.setString( 3, nroing );
            rs = st.executeQuery();
            if(rs.next()){
                num = rs.getInt("num") ;
            }
        }}catch( SQLException e ){
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DEL ITEMS MAXIMO" + e.getMessage() + " " + e.getErrorCode() );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return num;
    }
    /**
     * Metodo: datosFactura, permite buscar los datos de la factura
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    
    public void datosFactura ( String distrito, String numero_factura, String tipo_documento, String cliente ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        factura = null;
        String query = "SQL_CONSULTA_DATOS_FACTURA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, tipo_documento );
            st.setString ( 2, distrito );
            st.setString ( 3, numero_factura );
            st.setString ( 4, cliente );
            rs = st.executeQuery ();
            while( rs.next () ){
                factura = new Ingreso_detalle ();
                factura.setFactura ( rs.getString ("documento") );
                factura.setMoneda_factura ( rs.getString ("moneda_factura")!=null?!rs.getString ("moneda_factura").equals ("")?rs.getString ("moneda_factura"):"PES":"PES" );
                factura.setFecha_factura ( rs.getString ("fecha_factura") );
                factura.setDescripcion_factura ( rs.getString ("descripcion") );
                factura.setValor_saldo_factura ( rs.getDouble ("valor_saldo") );
                factura.setValor_saldo_fact ( rs.getDouble ("valor_saldo") );
                factura.setValor_saldo_factura_me ( rs.getDouble ("valor_saldome") );
                factura.setValor_factura ( rs.getDouble ("valor_factura") );
                factura.setValor_factura_me ( rs.getDouble ("valor_facturame") );   
                factura.setValor_tasa_factura ( rs.getDouble ("valor_tasa"));
                factura.setAgencia_facturacion ( rs.getString ("agencia_facturacion"));
                factura.setCuenta( rs.getString("cuenta")!=null?rs.getString ("cuenta"):"" );
                factura.setAuxiliar( "" );
                factura.setTipo_aux( "" );
                factura.setSeleccionado (true);
            }
            }}catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LAS FACTURAS" + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * Metodo: numero_items, permite buscar el numero de items de un ingreso
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public int numero_items ( String distrito, String tipo_doc, String no_ingreso ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int item = 0;
        String query = "SQL_CANTIDAD_ITEMS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, tipo_doc );
            st.setString ( 3, no_ingreso );
            rs = st.executeQuery ();
            while( rs.next () ){
                item = rs.getInt("items");
            }
        }}catch(Exception e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DEL NUMERO DE ITEMS" + e.getMessage ());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return item;
    }
    
    /**
     * Metodo: existeItem, permite obtener la agencia del banco
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String agenciaBanco ( String distrito, String numero_ingreso, String tipo_documento, int item ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sw = "";
        String query = "SQL_AGENCIA_BANCO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, numero_ingreso );
            st.setString ( 2, tipo_documento );
            st.setString ( 3, distrito );
            st.setInt    ( 4, item );
            rs = st.executeQuery ();
            if( rs.next () ){
                sw = rs.getString ("");
            }
            }} catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LA CABECERA " + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
     /**
     * Metodo: updateIngresoDetalle, permite actualizar un registro en la tabla ingreso_detalle en la BD.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String updateIngresoDetalle () throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_ITEM";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setDouble (1,  ingreso_detalle.getValor_ingreso ());
            st.setDouble (2,  ingreso_detalle.getValor_ingreso_me ());
            st.setDouble (3,  ingreso_detalle.getValor_retefuente ());
            st.setDouble (4,  ingreso_detalle.getValor_retefuente_me ());
            st.setDouble (5,  ingreso_detalle.getValor_reteica ());
            st.setDouble (6,  ingreso_detalle.getValor_reteica_me ());
            st.setDouble (7,  ingreso_detalle.getValor_diferencia ());
            st.setString (8,  ingreso_detalle.getCreation_user ());
            st.setString (9,  ingreso_detalle.getCodigo_retefuente());
            st.setString (10, ingreso_detalle.getCodigo_reteica());
            st.setString (11, ingreso_detalle.getCuenta());
            st.setString (12, ( !ingreso_detalle.getTipo_aux().equals("") && !ingreso_detalle.getAuxiliar().equals("") )?ingreso_detalle.getTipo_aux()+"-"+ingreso_detalle.getAuxiliar():"");
            st.setString (13, ingreso_detalle.getDistrito ());
            st.setString (14, ingreso_detalle.getTipo_documento ());
            st.setString (15, ingreso_detalle.getNumero_ingreso ());
            st.setInt    (16, ingreso_detalle.getItem ());
            sql = st.getSql();
        }catch(Exception ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LOS ITEMS DE LOS INGRESOS, "+ex.getMessage ());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    
            /**
     * Metodo: updateIngresoDiferencia, permite actualizar un registro en la tabla ingreso_detalle en la BD.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String updateIngresoDiferencia () throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_DIFERENCIA";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setString (1, ingreso_detalle.getDistrito ());
            st.setString (2, ingreso_detalle.getTipo_documento ());
            st.setString (3, ingreso_detalle.getDocumento());
            sql = st.getSql();
        }catch(Exception ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LAS DIFERENCIAS DE LOS ITEMS EN 0, "+ex.getMessage ());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    
    /**
     * Metodo: existeItem, permite obtener la agencia del banco
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public double valorDiferencia ( double val ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double sw = 0;
        String query = "SQL_ITEMS_FACURAS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setDouble (1, val);
            st.setString (2, ingreso_detalle.getDistrito ());
            st.setString (3, ingreso_detalle.getTipo_documento ());
            st.setString (4, ingreso_detalle.getFactura());
            rs = st.executeQuery ();
            if( rs.next () ){
                sw = rs.getDouble("diferencia");
            }
            }} catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS EL VALOR DE LOS ITEMS DE LA FACTURA " + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    
    
        /* Metodo: updateSaldoFactura_modificacion, permite actualizar el saldo de la factura
      * @autor : Ing. Jose de la rosa
      * @param : valor del abono de la factura, valor del abono moneda extranjera de la factura, factura, distrito, el tipo de documento
      * @version : 1.0
      */
    public double esTotal ( double abono_meA, double abono_meV, String factura, String distrito, String tipo_documento) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double sql = 0;
        String query = "SQL_ES_TOTAL";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setDouble (1,  abono_meV);
            st.setDouble (2,  abono_meA);
            st.setString (3,  factura);
            st.setString (4,  distrito);
            st.setString (5,  tipo_documento);
            rs = st.executeQuery ();
            if( rs.next () ){
                sql = rs.getDouble("VALOR");
            }
            }}catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LOS SALDOS DE LAS FACTURAS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    } 
   
    
    /**
     * Metodo: datosCabecera, permite buscar los datos de la cabecera del ingreso
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void datosCabecera ( String distrito, String numero_ingreso, String tipo_documento, String tipo_ingreso ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CONSULTA_DATOS_CABECERA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, numero_ingreso );
            st.setString ( 3, tipo_documento );
            st.setString ( 4, tipo_ingreso );
            
            rs = st.executeQuery ();
            while( rs.next () ){
                cabecera = new Ingreso_detalle ();
                cabecera.setCliente             ( rs.getString ("codcli") );
                cabecera.setNit_cliente         ( rs.getString ("nitcli") );
                cabecera.setConcepto            ( rs.getString ("concepto") );
                cabecera.setNumero_ingreso      ( rs.getString ("num_ingreso") );
                cabecera.setNom_cliente         ( rs.getString ("nomcli") );
                cabecera.setMoneda              ( rs.getString ("codmoneda") );
                cabecera.setValor_ingreso       ( rs.getDouble ("vlr_ingreso") );
                cabecera.setValor_ingreso_me    ( rs.getDouble ("vlr_ingreso_me") );
                cabecera.setFecha_consignacion  ( rs.getString ("fecha_consignacion") );
                cabecera.setCodigo_retefuente   ( rs.getString ("codigo_impuesto") );
                cabecera.setPorcentaje_rfte     ( rs.getDouble ("porcentaje1") );
                cabecera.setValor_tasa_ingreso  ( rs.getDouble ("vlr_tasa") );
                cabecera.setCantidad_item       ( rs.getInt    ("cant_item") );
                cabecera.setBanco               ( rs.getString ("branch_code") );
                cabecera.setSucursal            ( rs.getString ("bank_account_no") );
                cabecera.setValor_factura       ( 0 );
                cabecera.setValor_tasa          ( 0 );
                cabecera.setTipo_documento      ( rs.getString ("tipo_documento") );
                cabecera.setDescripcion         ( rs.getString ("descripcion_ingreso") );
                cabecera.setNumero_consignacion ( rs.getString ("nro_consignacion") );
                cabecera.setVlr_tasa            ( rs.getDouble ("vlr_tasa") );
                cabecera.setFecha_tasa          ( rs.getString ("fecha_tasa") );
                /*Modificacion Jescandon 15-02-07*/
                cabecera.setCuenta              ( rs.getString("cuenta") );
                cabecera.setAuxiliar            ( rs.getString("auxiliar") );
                cabecera.setTipo_aux            ( rs.getString("tipo") );
                cabecera.setCreation_date       ( rs.getString("fecha_ingreso"));                
                cabecera.setCuenta_banco        ( rs.getString("cuenta_banco"));
                cabecera.setFecha_impresion     ( rs.getString("fecha_impresion"));
                cabecera.setFecha_contabilizacion( rs.getString("fecha_contabilizacion"));
                cabecera.setTasaDB              ( rs.getDouble ("tasa_dol_bol") );
                /*Jescandon 09-03-07*/
                cabecera.setCiudad_cliente      ( ( rs.getString("ciudad") != null )?rs.getString("ciudad"):""      ) ;
                cabecera.setDireccion_cliente   ( ( rs.getString("direccion") != null)?rs.getString("direccion"):"" );
                cabecera.setTelefono_cliente    ( ( rs.getString("telefono") != null )?rs.getString("telefono"):""  );
                /*Jescandon 12-03-07*/
                String estado                   = ( rs.getString("reg_status") != null )?rs.getString("reg_status"):"";
                cabecera.setReg_status          ( estado.equals("A") ?"ANULADO":"" );
                cabecera.setAbc                 ( ( rs.getString("abc") != null )?rs.getString("abc"):""  );
                cabecera.setSaldo_ingreso       ( rs.getDouble ("saldo_ingreso") );
                cabecera.setCreation_date       ( rs.getString("creation_date"));
                cabecera.setNro_extracto(Integer.parseInt(rs.getString("nro_extracto")));
            }
        }} catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LA CABECERA " + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo: insertarIngresoDetalle, permite ingresar un registro en la tabla ingreso_detalle en la BD.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String insertarIngresoDetalle () throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_INGRESAR";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setString (1,  ingreso_detalle.getDistrito ());
            st.setString (2,  ingreso_detalle.getNit_cliente ());
            st.setString (3,  ingreso_detalle.getNumero_ingreso ());
            st.setInt    (4,  ingreso_detalle.getItem ());
            st.setDouble (5,  ingreso_detalle.getValor_ingreso ());
            st.setDouble (6,  ingreso_detalle.getValor_ingreso_me ());
            st.setString (7,  ingreso_detalle.getFactura ());
            st.setString (8,  ingreso_detalle.getFecha_factura ());
            st.setString (9,  ingreso_detalle.getCodigo_retefuente ());
            st.setDouble (10, ingreso_detalle.getValor_retefuente ());
            st.setDouble (11, ingreso_detalle.getValor_retefuente_me ());
            st.setString (12, ingreso_detalle.getCodigo_reteica ());
            st.setDouble (13, ingreso_detalle.getValor_reteica ());
            st.setDouble (14, ingreso_detalle.getValor_reteica_me ());
            st.setDouble (15, ingreso_detalle.getValor_diferencia ());
            st.setString (16, ingreso_detalle.getCreation_user ());
            st.setString (17, ingreso_detalle.getBase ());
            st.setString (18, ingreso_detalle.getTipo_documento ());
            st.setString (19, ingreso_detalle.getTipo_doc());
            st.setString (20, ingreso_detalle.getDocumento ());
            st.setDouble (21, ingreso_detalle.getValor_tasa ());
	    st.setDouble (22, ingreso_detalle.getValor_saldo_factura_me ());
            st.setString (23, ingreso_detalle.getCuenta());
            st.setString (24, ( !ingreso_detalle.getTipo_aux().equals("") && !ingreso_detalle.getAuxiliar().equals("") )?ingreso_detalle.getTipo_aux()+"-"+ingreso_detalle.getAuxiliar():"");
            st.setString (25, ingreso_detalle.getDescripcion_factura());
            sql = st.getSql();
        }catch(Exception ex){
            throw new SQLException ("ERROR AL INGRESAR LOS ITEMS DE LAS FACTURAS, "+ex.getMessage ());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    /**
     * Metodo: datosListadoItems, permite listar los items de ingreso de un cliente
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
   public Vector datosListadoItems ( String num_ingreso, String tipo_doc, String distrito) throws SQLException {
       Connection con = null;
       PreparedStatement st = null;
        ResultSet rs = null;
        factura = null;
        Vector vec = null;
        double tot = 0;
        String query = "SQL_SEARCH_ITEMS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, num_ingreso );
            st.setString ( 2, tipo_doc );
            st.setString ( 3, distrito );
            vec = new Vector();
            rs = st.executeQuery ();
            while( rs.next () ){
                Ingreso_detalle items = new Ingreso_detalle ();
                items.setFactura ( rs.getString ("factura") );
                items.setItem ( rs.getInt ("item") );
                items.setDescripcion_factura ( rs.getString ("descripcion") );  
                items.setFecha_factura ( rs.getString ("fecha_factura") );
                
                items.setValor_tasa ( rs.getDouble ("valor_tasa") );
                items.setValor_saldo_fing ( rs.getString ("factura").equals("")?rs.getDouble ("valor_ingreso_me"):rs.getDouble ("saldo_factura") );
                
                items.setValor_factura ( rs.getString ("factura").equals("")?rs.getDouble ("valor_ingreso_me"):rs.getDouble ("valor_factura") );
                items.setValor_factura_me ( rs.getString ("factura").equals("")?rs.getDouble ("valor_ingreso_me"):rs.getDouble ("valor_facturame") );
                
                items.setValor_abono ( rs.getDouble ("valor_ingreso_me") );
                items.setValor_ingreso_me ( rs.getDouble ("valor_ingreso") );
                
                items.setValor_saldo_factura_me( rs.getString ("factura").equals("")?rs.getDouble ("valor_ingreso_me"):rs.getDouble ("valor_saldome") );
                items.setValor_saldo_factura ( rs.getString ("factura").equals("")?rs.getDouble ("valor_ingreso_me"):rs.getDouble ("valor_saldo") );
                items.setValor_saldo_fact (rs.getString ("factura").equals("")?rs.getDouble ("valor_ingreso_me"): rs.getDouble ("valor_saldo") );
                items.setSal_fact_mlocal ( rs.getString ("factura").equals("")?rs.getDouble ("valor_ingreso_me"):rs.getDouble ("valor_saldo") );
                items.setMoneda_factura ( rs.getString ("moneda_factura")!=null?!rs.getString ("moneda_factura").equals ("")?rs.getString ("moneda_factura"):"PES":"PES" );
                items.setFecha_contabilizacion ( rs.getString ("fecha_contabilizacion") );
                items.setFecha_anulacion ( rs.getString ("fecha_anulacion") );
                items.setCodigo_retefuente ( rs.getString ("codigo_retefuente") );
                items.setCodigo_reteica ( rs.getString ("codigo_reteica") );
                items.setValor_retefuente ( rs.getDouble ("valor_retefuente_me") );
                items.setValor_reteica ( rs.getDouble ("valor_reteica_me") );
                items.setValor_retefuente_me ( rs.getDouble ("valor_retefuente") );
                items.setValor_reteica_me ( rs.getDouble ("valor_reteica") );
                items.setAgencia_facturacion ( rs.getString ("agencia_facturacion"));
                items.setCuenta( rs.getString("cuenta")!=null?rs.getString ("cuenta"):"" );
                items.setAuxiliar( rs.getString("auxiliar") );
                items.setTipo_aux( rs.getString("tipo") );
                items.setValor_diferencia( rs.getDouble ("valor_diferencia_tasa") );
                items.setSaldo_facMI( rs.getString ("factura").equals("")?rs.getDouble ("valor_ingreso_me"):rs.getDouble ("saldo_factura") );
                tot += items.getValor_abono () - ( items.getValor_retefuente () + items.getValor_reteica () );
                vec.add ( items );
            }
            cabecera.setValor_total (tot);
        }} catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LOS ITEMS" + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vec;
    }
   
   /**
     * Metodo: datosListadoFactura, permite listar las facturas de un cliente
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0 
     */
    public void datosListadoFacturaUpdate ( String distrito, String nit_cliente, String tipo_documento, String num_ingreso ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        factura = null;
        vecFactura = null;
        String query = "SQL_CONSULTA_LISTADO_FACTURA_UPDATE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, tipo_documento );
            st.setString ( 2, distrito );
            st.setString ( 3, nit_cliente );
            st.setString ( 4, nit_cliente );
            st.setString ( 5, tipo_documento );
            st.setString ( 6, distrito );
            st.setString ( 7, num_ingreso );
            st.setString ( 8, tipo_documento );
            rs = st.executeQuery ();
            vecFactura = new Vector ();
            while( rs.next () ){
                factura = new Ingreso_detalle ();
                factura.setFactura ( rs.getString ("documento") );
                factura.setMoneda_factura ( rs.getString ("moneda_factura")!=null?!rs.getString ("moneda_factura").equals ("")?rs.getString ("moneda_factura"):"PES":"PES" );
                factura.setFecha_factura ( rs.getString ("fecha_factura") );
                factura.setDescripcion_factura ( rs.getString ("descripcion") );
                factura.setValor_saldo_factura ( rs.getDouble ("valor_saldo") );
                factura.setValor_saldo_fact ( rs.getDouble ("valor_saldo") );
                factura.setValor_saldo_factura_me ( rs.getDouble ("valor_saldome") );
                factura.setValor_factura ( rs.getDouble ("valor_factura") );
                factura.setValor_factura_me ( rs.getDouble ("valor_facturame") ); 
                factura.setValor_tasa_factura ( rs.getDouble ("valor_tasa"));
                factura.setAgencia_facturacion ( rs.getString ("agencia_facturacion"));
                factura.setCuenta( rs.getString("cuenta")!=null?rs.getString ("cuenta"):"" );
                factura.setAuxiliar( "" );
                factura.setTipo_aux( "" );
                vecFactura.add ( factura );
            }
        }} catch( SQLException e ){
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS DATOS DE LAS FACTURAS" + e.getMessage () + " " + e.getErrorCode () );
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    } 

    /**
     * Metodo: numeroIngresoRI, devuelve el consecutivo del ingreso en RI
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String numeroIngresoRI ( String num_ingreso, String tipo, String distrito ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql ="";
        String query = "SQL_NUM_INGRESO_RI";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1,  num_ingreso);
            st.setString (2, tipo.equals("ING")?"IC":"NC");
            st.setString (3, tipo.equals("ING")?"RI":"RC");
            st.setString (4,  distrito);
            st.setString (5,  tipo);
            st.setString (6,  num_ingreso);
            st.setString (7, tipo.equals("ING")?"IC":"NC");
            st.setString (8, tipo.equals("ING")?"RI":"RC");
            rs = st.executeQuery ();
            if( rs.next () )
                sql = rs.getString("numero");
        }}catch(SQLException ex){
            throw new SQLException ("ERROR numeroIngresoRI ( String num_ingreso ), "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }
    
    /**
     * Metodo: numeroIngresoRI, devuelve el consecutivo del ingreso en RI
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String numeroSiguienteIngresoRI ( String num_ingreso, String tipo, String distrito ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql ="";
        String query = "SQL_SIGUIENTE_INGRESO_RI";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1,  num_ingreso);
            st.setString (2,  distrito);
            st.setString (3,  tipo);
            st.setString (4,  num_ingreso);
            rs = st.executeQuery ();
            if( rs.next () )
                sql = rs.getString("numero");
        }}catch(SQLException ex){
            throw new SQLException ("ERROR numeroIngresoRI ( String num_ingreso ), "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }  
    
     /**
     * Metodo: updateSaldoIngreso, permite actualizar el saldo del ingreso
     * @autor : Ing. Jose de la rosa
     * @param : valor viejo, nuevo valor. distrito, tipo de ingreso, numero de ingreso
     * @version : 1.0
     */
    public String updateSaldoIngreso ( double Val_v, String distrito, String tipo, String num_ing ) throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_SAL_INGRESO";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setDouble (1, Val_v);
            st.setDouble (2, Val_v);
            st.setString (3, distrito);
            st.setString (4, tipo);
            st.setString (5, num_ing);
            sql = st.getSql();
        }catch(Exception ex){
            throw new SQLException ("ERROR updateSaldoIngreso ( double Val_v, double Val_n, String distrito, String tipo, String num_ing ), "+ex.getMessage ());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }    
    
     /**
     * Metodo: updateSaldoConsignacionIngreso, permite actualizar el saldo y la consignacion del ingreso
     * @autor : Ing. Jose de la rosa
     * @param : valor, distrito, tipo de ingreso, numero de ingreso
     * @version : 1.0
     */
    public String updateSaldoConsignacionIngreso ( String distrito, String tipo, String num_ing ) throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_SAL_CONS_INGRESO";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setString (1, distrito);
            st.setString (2, tipo);
            st.setString (3, num_ing);
            st.setString (4, distrito);
            st.setString (5, tipo);
            st.setString (6, num_ing);
            st.setString (7, distrito);
            st.setString (8, tipo);
            st.setString (9, num_ing);
            st.setString (10, tipo.equals("ING")?"RI":"RC");
            st.setString (11, tipo.equals("ING")?"IC":"NC");
            st.setString (12, distrito);
            st.setString (13, tipo);
            st.setString (14, num_ing);
            st.setString (15, distrito);
            st.setString (16, tipo);
            st.setString (17, num_ing);
            st.setString (18, distrito);
            st.setString (19, tipo);
            st.setString (20, num_ing);
            sql = st.getSql();
        }catch(Exception ex){
            throw new SQLException ("ERROR updateSaldoConsignacionIngreso ( double total, String distrito, String tipo, String num_ing ), "+ex.getMessage ());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }    



/**
 *
 * @return
 * @throws SQLException
 */
    public String insertarNewIngresoDetalle () throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_NEW_INGRESAR";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2 sin
            st.setString (1,  ingreso_detalle.getDistrito ());
            st.setString (2,  ingreso_detalle.getNit_cliente ());
            st.setInt    (3,  ingreso_detalle.getItem ());
            st.setDouble (4,  ingreso_detalle.getValor_ingreso ());
            st.setDouble (5,  ingreso_detalle.getValor_ingreso_me ());
            st.setString (6,  ingreso_detalle.getFactura ());
            st.setString (7,  ingreso_detalle.getFecha_factura ());
            st.setString (8,  ingreso_detalle.getCodigo_retefuente ());
            st.setDouble (9, ingreso_detalle.getValor_retefuente ());
            st.setDouble (10, ingreso_detalle.getValor_retefuente_me ());
            st.setString (11, ingreso_detalle.getCodigo_reteica ());
            st.setDouble (12, ingreso_detalle.getValor_reteica ());
            st.setDouble (13, ingreso_detalle.getValor_reteica_me ());
            st.setDouble (14, ingreso_detalle.getValor_diferencia ());
            st.setString (15, ingreso_detalle.getCreation_user ());
            st.setString (16, ingreso_detalle.getBase ());
            st.setString (17, ingreso_detalle.getTipo_documento ());
            st.setString (18, ingreso_detalle.getTipo_doc());
            st.setString (19, ingreso_detalle.getDocumento ());
            st.setDouble (20, ingreso_detalle.getValor_tasa ());
	    st.setDouble (21, ingreso_detalle.getValor_saldo_factura_me ());
            st.setString (22, ingreso_detalle.getCuenta());
            st.setString (23, ( !ingreso_detalle.getTipo_aux().equals("") && !ingreso_detalle.getAuxiliar().equals("") )?ingreso_detalle.getTipo_aux()+"-"+ingreso_detalle.getAuxiliar():"");
            st.setString (24, ingreso_detalle.getDescripcion_factura());
            sql = st.getSql();
        }catch(Exception ex){
            throw new SQLException ("ERROR AL INGRESAR LOS ITEMS DE LAS FACTURAS, "+ex.getMessage ());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
}
