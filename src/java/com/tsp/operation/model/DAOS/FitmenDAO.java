/******************************************************************
 * Nombre                        FitmenDAO.java
 * Descripci�n                   Clase DAO para la tabla fitmen
 * Autor                         ricardo rosero
 * Fecha                         06/01/2006
 * Versi�n                       1.0
 * Coyright                      Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  rrosero
 */
public class FitmenDAO {
    //atributos principales
    private Fitmen fit;
    private Connection con;
    private PoolManager pm;
    private Vector fitVec;
    
    //atributos SQL
    private static String SQL1 = "SELECT * FROM fitmen ORDER BY plaveh, numpla";
    private static String SQL2 = "SELECT f.platlr AS platlr, f.numpla AS numpla, pl.plaveh AS plaveh, " +
    "pl.fecdsp AS fecdsp, get_nombreciudad(pl.oripla) AS oripla, " +
    "get_nombreciudad(pl.despla) AS despla, get_nombreagencia(pl.agcpla) " +
    "AS agcplao, get_nombreagencia(pl.agcpla) AS agcplad, f.numpla_ant AS " +
    "numpla_ant, f.feccum AS feccum, f.feccum_ant AS feccum_ant FROM fitmen " +
    "AS f JOIN planilla AS pl ON (f.numpla = pl.numpla)" +
    "ORDER BY f.platlr, f.numpla";
    
    private static String INSERT = "insert into fitmen (platlr,numpla,creation_user,dstrct,base)values (?,?,?,?,?)";
    
    private static String ANULAR = "UPDATE fitmen SET reg_status = 'A' WHERE platlr = ?";
    
    private static String UPDATE = "UPDATE fitmen SET numpla = ?, feccum = ?, feccum_ant = ?, " +
    "numpla_ant = ?, user_update = ?" +
    " WHERE platlr = ?";
    
    private static String DELETE = "DELETE FROM fitmen";
    
    private static String EXISTE = "SELECT * FROM fitmen WHERE platlr = ?";
    /** Creates a new instance of FitmenDAO */
    public FitmenDAO() {
    }
    
    /**
     * Getter for property fit.
     * @return Value of property fit.
     */
    public com.tsp.operation.model.beans.Fitmen getFit() {
        return fit;
    }
    
    /**
     * Setter for property fit.
     * @param fit New value of property fit.
     */
    public void setFit(com.tsp.operation.model.beans.Fitmen fit) {
        this.fit = fit;
    }
    
    /**
     * Genera el reporte ubicacion
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public Vector obtenerInformeUbicacion() throws SQLException{
        ////System.out.println("ya lleguie al DAO");
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ////System.out.println("Estoy en el DAO");
        
        Vector informe = new Vector();
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL2);
                ////System.out.println("consulta: " + st.toString());
                
                rs = st.executeQuery();
                ////System.out.println("ResultSet----> " + rs.toString());
                while(rs.next()){
                    fit = fit.load2(rs);
                    informe.add(fit);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA GENERACION DEL REPORTE DE ANTICIPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return informe;
    }
    
    /**
     * M�todo que retorna un boolean si existe un fitmen o no
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0.
     **/
    public boolean existeFitmen() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean sw= false;
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(EXISTE);
            ps.setString(1, fit.getPlatlr());
            rs = ps.executeQuery();
            sw= rs.next();
            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    
    /**
     * M�todo que agrega un fitmen
     * @autor                       Karen Reales
     * @throws                      SQLException
     * @version                     2.0.
     **/
    public String addFitmen() throws SQLException{
        PreparedStatement ps = null;
        String sql = "";
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(INSERT);
            ps.setString(1, fit.getPlatlr());
            ps.setString(2, fit.getNumpla());
            ps.setString(3, fit.getCreation_user());
            ps.setString(4, fit.getDstrct());
            ps.setString(5, fit.getBase());
            //ps.executeUpdate();
            sql = ps.toString();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return sql;
    }
    
    /**
     * M�todo que anula un fitmen
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0.
     **/
    public void anularFitmen() throws SQLException{
        PreparedStatement ps = null;
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(ANULAR);
            ps.setString(1, fit.getPlatlr());
            ps.executeUpdate();
            ////System.out.println("ANULAR ---->> "+ANULAR);
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que elimina todos los registros de la tabla fitmen
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0.
     **/
    public void deleteFitmen() throws SQLException{
        PreparedStatement ps = null;
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(DELETE);
            ps.executeUpdate();
            ////System.out.println("DELETE ---->> "+DELETE);
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * M�todo que actualiza un fitmen
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0.
     **/
    public String updateFitmen() throws SQLException{
        PreparedStatement ps = null;
        String sql="";
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(UPDATE);
            ps.setString(1, fit.getNumpla());
            ps.setString(2, fit.getFeccum());
            ps.setString(3, fit.getFeccum_ant());
            ps.setString(4, fit.getNumpla_ant());
            ps.setString(5, fit.getUser_update());
            ps.setString(6, fit.getPlatlr());
            //ps.executeUpdate();
            sql = ps.toString();
            ////System.out.println("UPDATE ---->> "+UPDATE);
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return sql;
    }
    
    /**
     * Getter for property fitVec.
     * @return Value of property fitVec.
     */
    public java.util.Vector getFitVec() {
        return fitVec;
    }
    
    /**
     * Setter for property fitVec.
     * @param fitVec New value of property fitVec.
     */
    public void setFitVec(java.util.Vector fitVec) {
        this.fitVec = fitVec;
    }
    /**
     * M�todo que busca un fitmen dada la placa del trailer
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0.
     **/
    public void buscarFitmen(String trailer) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        fit=null;
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(EXISTE);
            ps.setString(1, trailer);
            rs = ps.executeQuery();
            if(rs.next()){
                fit = new Fitmen();
                fit.setPlatlr(rs.getString("platlr"));
                fit.setNumpla_ant(rs.getString("numpla"));
                fit.setFeccum_ant(rs.getString("feccum"));
                
            }
            
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    
}
