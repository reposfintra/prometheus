/************************************************************************
 * Nombre clase: MantenimientoAbcFestivosDAO.java                       *
 * Descripción: Clase que maneja los querys asociados al mantenimiento de la 
   tabla abc_Festivos.         *
 * Autor: Ing. Luis Eduardo Frieri                                          *
 * Fecha: 10 de enero de 2007                                *                                                 *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;
import java.io.*;

public class MantenimientoFestivosDAO extends MainDAO{
    
    private Vector vector = null;
    private Festivos fes;
    
    public MantenimientoFestivosDAO() {
        super ( "MantenimientoFestivosDAO.xml" );
    }
    
    public boolean existeFestivos (String fecha, String pais) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_FESTIVOSFECHA";
        boolean sw=false;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString ( 1, fecha );
            ps.setString ( 2, pais );
            rs = ps.executeQuery ();
            
            if(rs.next ()){
               sw = true;
            }
            }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }


 /**
  * 
  * @param reg_status
  * @param dstrct
  * @param fecha
  * @param descripcion
  * @param user_update
  * @param creation_user
  * @param base
  * @param pais
  * @throws SQLException
  */
    public void insertarFestivos (String reg_status, String dstrct, String fecha, String descripcion, String user_update, String creation_user, String base, String pais) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_INSERTAR_FESTIVOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString ( 1, reg_status );
            ps.setString ( 2, dstrct );
            ps.setString ( 3, fecha );
            ps.setString ( 4, descripcion );
            ps.setString ( 5, user_update );
            ps.setString ( 6, creation_user );
            ps.setString ( 7, base );
            ps.setString ( 8, pais );
            ps.executeUpdate();
            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
/**
 * 
 * @param pais
 * @param descripcion
 * @param usuario
 * @param buscfecha
 * @throws SQLException
 */
    public void modificarFestivos (String pais, String descripcion, String usuario, String buscfecha) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_MODIFICAR_FESTIVOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString ( 1, pais );
            ps.setString ( 2, descripcion );
            ps.setString ( 3, usuario );
            ps.setString ( 4, buscfecha );
            ps.executeUpdate();
            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



/**
 *
 * @param fecha
 * @throws SQLException
 */
    public void eliminarFestivos (String fecha) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_ELIMINAR_FESTIVOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString ( 1, fecha );
            ps.executeUpdate();
            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 * 
 * @throws SQLException
 */
    public void ListarFestivos () throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String query = "SQL_LISTAR_FESTIVOS";
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery ();
                vector = new Vector();
                while(rs.next ()){
                Festivos info = new Festivos();
                info.setFecha( rs.getString("fecha")==null?"":rs.getString("fecha") );
                info.setDescripcion( rs.getString("descripcion")==null?"":rs.getString("descripcion"));
                vector.add(info);
                }
                }}catch(SQLException ex){
                ex.printStackTrace();
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 *
 * @param fechai
 * @param fechaf
 * @param pais
 * @throws SQLException
 */
    public void buscarFestivo (String fechai, String fechaf, String pais) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_FESTIVOS";
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString ( 1, fechai);
                ps.setString ( 2, fechaf);
                ps.setString ( 3, pais);
                rs = ps.executeQuery ();
                vector = new Vector();
                while(rs.next ()){
                Festivos info = new Festivos();
                info.setFecha( rs.getString("fecha")==null?"":rs.getString("fecha") );
                info.setDescripcion( rs.getString("descripcion")==null?"":rs.getString("descripcion"));
                info.setPais( rs.getString("pais")==null?"":rs.getString("pais") );
                vector.add(info);
                }
            }}catch(SQLException ex){
                ex.printStackTrace();
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
        
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
}

