/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonObject;

/**
 *
 * @author jpacosta
 */
public interface SancionesCondonacionesDAO {
    public abstract String getFiltros();
    public abstract String getInfo();
    public abstract String buscar(String unidad_negocio, String concepto_recaudo, String periodo);
    public abstract String modificar(JsonObject json, String usuario);
}
