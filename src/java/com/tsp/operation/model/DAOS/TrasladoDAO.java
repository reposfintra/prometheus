package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import java.util.*;

public class TrasladoDAO extends MainDAO {
    private BeanGeneral comprobante;
    private Banco banco;
    private Vector vector;
    private Vector vectorSerie;
    private Vector vectorBanco;
    private Vector vectorTraslado;
    private Vector vectorDatosBanco;
   
   
    public TrasladoDAO() {
        super("TrasladoDAO.xml");
    }
    public TrasladoDAO(String dataBaseName) {
        super("TrasladoDAO.xml", dataBaseName);
    }
  /*
  Funcion que agrega en traslado bancario se le ingresa un bean
  fecha:  23 de enero del 2007
  Autor :  fily steven fernandez
  Trasporte sabchez Polo
  */
  public void agregarTraslado_bank(BeanGeneral comprobante)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
      String query = "SQL_INGRESAR_TRASLADOS_BANCARIOS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getValor_01());
            st.setString(2, comprobante.getValor_02());
            st.setString(3, comprobante.getValor_03());
            st.setString(4, comprobante.getValor_04());
            st.setString(5, comprobante.getValor_05());
            st.setString(6, comprobante.getValor_06());
            st.executeUpdate();
        }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION TRASLADOS BANCARIOS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

  }

    /*
    funcion que reliaza una busqueda en la tabla de traslado bancario dependiendo de los campos que se le
    ingresen por pantalla y se carga en el vector para despues ser ejecutado y cargar en la lista
    fecha:  23 de enero del 2007
    Autor :  fily steven fernandez
    Trasporte sabchez Polo
    */
    public void BusquedaTraslado(String fechaI,String fechaF,String banco_origen,String banco_destino ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        vector = new Vector();
        Connection con = null;
        String query = "SQL_BUSCAR_TRASLADO_BANCARIO";//JJCastro fase2
        try{
            String remplazar = "";
            if(banco_origen.equals("") && banco_destino.equals("")){
                  remplazar = "fecha BETWEEN '"+fechaI+"' AND '"+fechaF+ "' and reg_status = ''";
            }else {
                  remplazar =  "banco_origen like '%"+banco_origen+"%' AND banco_destino like '%"+banco_destino+ "%' and reg_status = ''";


            }
            String agregar = "";
            String sql = this.obtenerSQL(query);//JJCastro fase2

            agregar = sql.replaceAll("#CONDICION#", remplazar);
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement( agregar );
            rs = st.executeQuery();

            while(rs.next()){

                comprobante = new BeanGeneral();
                comprobante.setValor_01 ((rs.getString("banco_origen")!=null)?rs.getString("banco_origen"):"");
                comprobante.setValor_02 ((rs.getString("sucursal_origen")!=null)?rs.getString("sucursal_origen"):"");
                comprobante.setValor_03 ((rs.getString("banco_destino")!=null)?rs.getString("banco_destino"):"");
                comprobante.setValor_04 ((rs.getString("sucursal_destino")!=null)?rs.getString("sucursal_destino"):"");
                comprobante.setValor_05 ((rs.getString("valor")!=null)?rs.getString("valor"):"");
                comprobante.setValor_06 ((rs.getString("fecha")!=null)?rs.getString("fecha"):"");
                comprobante.setValor_07 ((rs.getString("comprobante")!=null)?rs.getString("comprobante"):"");
                comprobante.setValor_08 ((rs.getString("aprobado")!=null)?rs.getString("aprobado"):"");
                comprobante.setValor_09 ((rs.getString("llave")!=null)?rs.getString("llave"):"");
                this.vector.addElement( comprobante );
        }

        }}catch(Exception e) {
            throw new Exception("Error al Busqueda del  traslado bancario [Comprobante.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
        /*
          funcion que busca exactamente el traslado que va hacer modificado por el usuario me carga los valores en un vector
          fecha:  23 de enero del 2007
          Autor :  fily steven fernandez
          Trasporte sabchez Polo
      */
    public void BusquedaTrasladoModificar(String bancoOri,String bancoDes,String sucursalOri,String sucursalDes,String valores, String fecha ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral compro    = null;
        vector = new Vector();
        String query = "SQL_BUSQUEDA_MODIFICAR_TRASLADO";//JJCastro fase2

         try {
             con = this.conectarJNDI(query);
             if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, bancoOri );
            st.setString( 2, sucursalOri );
            st.setString( 3, bancoDes );
            st.setString( 4, sucursalDes );
            st.setString( 5, valores );
            st.setString( 6, fecha );
             rs = st.executeQuery();
            if(rs.next()){
                compro = new BeanGeneral();

                compro.setValor_01 ((rs.getString("banco_origen")!=null)?rs.getString("banco_origen"):"");
                compro.setValor_02 ((rs.getString("sucursal_origen")!=null)?rs.getString("sucursal_origen"):"");
                compro.setValor_03 ((rs.getString("banco_destino")!=null)?rs.getString("banco_destino"):"");
                compro.setValor_04 ((rs.getString("sucursal_destino")!=null)?rs.getString("sucursal_destino"):"");
                compro.setValor_05 ((rs.getString("val")!=null)?rs.getString("val"):"");
                compro.setValor_06 ((rs.getString("fecha")!=null)?rs.getString("fecha"):"");
                compro.setValor_07 ((rs.getString("comprobante")!=null)?rs.getString("comprobante"):"");
                compro.setValor_08 ((rs.getString("aprobado")!=null)?rs.getString("aprobado"):"");
       
                this.vector.addElement( compro );
            }
            //return BeanCliente;
        }
         }catch(Exception e){
             e.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
      /*
      FUNCION QUE MODIFICA EL TRASLADO BANCARIO  Y TODOS LOS CAMPOS ESTAN ALMACENADOS EN EL BEAN COMPROBANTE
      fecha:  23 de enero del 2007
      Autor :  fily steven fernandez
      Trasporte sabchez Polo
      */
    public void modificarTrasladoBancario(BeanGeneral comprobante) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR_TRASLADO";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getValor_01());
            st.setString(2, comprobante.getValor_02());
            st.setString(3, comprobante.getValor_03());
            st.setString(4, comprobante.getValor_04());
            st.setString(5, comprobante.getValor_05());
            st.setString(6, comprobante.getValor_06());
            st.setString(7, comprobante.getValor_07());
            st.setString(8, comprobante.getValor_08());
            st.setString(9, comprobante.getValor_09());
            st.setString(10, comprobante.getValor_10());
            st.setString(11, comprobante.getValor_11());
            st.setString(12, comprobante.getValor_12());
            st.executeUpdate();
        }}catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al MODIFICAR TRASLADO BANCARIO [COMPROBANTE.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
      /*
        Funcion que anula el traslado bancario dependiendo de los filtros que se le ingresen por pantalla
        fecha:  23 de enero del 2007
        Autor :  fily steven fernandez
        Trasporte sabchez Polo
        */
    public void AnularTraslado(BeanGeneral comprobante) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR_TRASLADO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getValor_01());
            st.setString(2, comprobante.getValor_02());
            st.setString(3, comprobante.getValor_03());
            st.setString(4, comprobante.getValor_04());
            st.setString(5, comprobante.getValor_05());
            st.setString(6, comprobante.getValor_06());
            st.executeUpdate();
        }}catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al Anular TRASLADO BANCARIO [COMPROBANTE.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
      /*
    Funcion que anula el traslado bancario de comprobante y comprodect
    fecha:  23 de enero del 2007
    Autor :  fily steven fernandez
    Trasporte sabchez Polo
    */
    public void AnularComprobanteYcomprodec(String pk) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR_COMPRABANTExCOMPRODECT";//JJCastro fase2

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,pk);
            st.setString(2,pk);
            st.executeUpdate();
        }}catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al  AnularComprobanteYcomprodec [COMPROBANTE.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


        /*
      Funcion quye almacena todos los query de aqullos traslados que van hacer aprovados
      fecha:  23 de enero del 2007
      Autor :  fily steven fernandez
      Trasporte sabchez Polo
      */
    public String Traslado_cambio_aprobado(String aprobado) throws Exception {
        StringStatement st = null;
        String query = "SQL_TRASLADO_APROBADO";//JJCastro fase2
        String sql = "";//JJCastro fase2
        try{
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2

            st.setString(1, aprobado);
            sql = st.getSql();//JJCastro fase2
            
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al Traslado_cambio_aprobado [COMPROBANTE.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
          if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql+";";
    }
    /*
      Funcion que realiza la busqueda de seria para armar el codigo de comprobante
      fecha:  23 de enero del 2007
      Autor :  fily steven fernandez
      Trasporte sabchez Polo
      */
    public void Traslado_buscar_serie() throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral seire    = null;
        vectorSerie = new Vector();
        String query = "SQL_TRASLADO_BUSCAR_SERIE";//JJCastro fase2
       
         try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            if(rs.next()){
                seire = new BeanGeneral();
                seire.setValor_01 ((rs.getString("prefijo")!=null)?rs.getString("prefijo"):"");
                seire.setValor_02 ((rs.getString("prefijo_anio")!=null)?rs.getString("prefijo_anio"):"");
                seire.setValor_03 ((rs.getString("prefijo_mes")!=null)?rs.getString("prefijo_mes"):"");
                seire.setValor_04 ((rs.getString("serie_act")!=null)?rs.getString("serie_act"):"");
                seire.setValor_05 ((rs.getString("long_serie")!=null)?rs.getString("long_serie"):"");
                this.vectorSerie.addElement( seire );
                seire =   null;//Liberar Espacio JJCastro
            }
        }
         }catch(Exception e){
             e.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
         /*
      funcion que busca el numero de la cuenta dependiendo del banco y la sucursal
      fecha:  23 de enero del 2007
      Autor :  fily steven fernandez
      Trasporte sabchez Polo
      */
    public void Buscar_cuenta_banco(String bancos, String sucursal) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        Banco banco    = null;
        vectorBanco = new Vector();
        String query = "SQL_BUSCAR_CUENTA_BANCO";//JJCastro fase2
       
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, bancos);
            st.setString(2, sucursal);
            rs = st.executeQuery();
            
            if(rs.next()){
                banco = new Banco();
                banco.setCodigo_cuenta((rs.getString("codigo_cuenta")!=null)?rs.getString("codigo_cuenta"):"");
                this.vectorBanco.addElement( banco );
            }
            //return BeanCliente;
        }
         }catch(Exception e){
             e.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
        /*
      Funcion que busca  traslado bancario por la llave primaria
      fecha:  23 de enero del 2007
      Autor :  fily steven fernandez
      Trasporte sabchez Polo
      */
    public void Buscar_traslado_x_id(String id) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        Banco banco    = null;
        vectorTraslado = new Vector();
        BeanGeneral traslado;
        String query = "SQL_TRASLADOXID";//JJCastro fase2
       
         try {
            con = this.conectarJNDI(query);
           if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, id);
            rs = st.executeQuery();
            
             rs = st.executeQuery();
            if(rs.next()){
                traslado = new BeanGeneral();
                
                traslado.setValor_01 ((rs.getString("banco_origen")!=null)?rs.getString("banco_origen"):"");
                traslado.setValor_02 ((rs.getString("sucursal_origen")!=null)?rs.getString("sucursal_origen"):"");
                traslado.setValor_03 ((rs.getString("banco_destino")!=null)?rs.getString("banco_destino"):"");
                traslado.setValor_04 ((rs.getString("sucursal_destino")!=null)?rs.getString("sucursal_destino"):"");
                traslado.setValor_05 ((rs.getString("val")!=null)?rs.getString("val"):"");
                traslado.setValor_06 ((rs.getString("fecha")!=null)?rs.getString("fecha"):"");
                traslado.setValor_07 ((rs.getString("comprobante")!=null)?rs.getString("comprobante"):"");
                traslado.setValor_08 ((rs.getString("aprobado")!=null)?rs.getString("aprobado"):"");
               
       
                this.vectorTraslado.addElement( traslado );
                traslado = null;//Liberar Espacio JJCastro
            }
            //return BeanCliente;
        }
         }catch(Exception e){
             e.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /*
      Funcion que agrega en traslado bancarioen la tabla comprobante
      fecha:  23 de enero del 2007
      Autor :  fily steven fernandez
      Trasporte sabchez Polo
      */
    public void agregarComprobante(int grupo_t,String numdoc,String detalle,double totaldebito,double totalcredito, BeanGeneral traslado)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String Origen = "";
        String Sucursal = "";
        String query = "SQL_GENERAR_COMPROBANTE";//JJCastro fase2
        try {
            Origen = traslado.getValor_09();
            Sucursal = traslado.getValor_10();
            //saco la moneda del banco
            Buscar_DatosBanco(Origen,Sucursal);
            Vector vectoresL =  this.getVectorDatosBanco();
            BeanGeneral bean = (BeanGeneral) vectoresL.lastElement();
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setInt(1, grupo_t);
            st.setString(2, numdoc);
            st.setString(3, detalle);
            st.setDouble(4,  totaldebito);
            st.setDouble(5, totalcredito);
            st.setString(6, traslado.getValor_01());
            st.setString(7, traslado.getValor_02());
            st.setString(8, traslado.getValor_03());
            st.setString(9, traslado.getValor_04());
            st.setString(10, traslado.getValor_05());
            st.setString(11, traslado.getValor_06());
            st.setString(12, traslado.getValor_07());
            st.setString(13, traslado.getValor_08());
            st.setString(14, "PES");
            st.executeUpdate();
        }}catch(Exception e){
             e.printStackTrace();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
  }

     /*
    Funcion que incrementa el numero de serie de la tabla tipodocto
    fecha:  23 de enero del 2007
    Autor :  fily steven fernandez
    Trasporte sabchez Polo
    */
  public void Incrementar_tipo_docto(int codigo) throws Exception {
      Connection con = null;//JJCastro fase2
      PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INCREMENTAR_NUMTIPODOCTO";//JJCastro fase2
       
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setInt(1, codigo);
            st.executeUpdate();  
        }
         }catch(Exception e){
             e.printStackTrace();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }  
        /*
      funcion que busca si existe el traslado y fue aprobado
      fecha:  23 de enero del 2007
      Autor :  fily steven fernandez
      Trasporte sabchez Polo
      */
    public boolean existe_traslado_aprobado(String fechaI,String fechaF,String banco_origen,String banco_destino ) throws Exception {
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String remplazar ="";
        boolean flag = false;
        try {
               if(banco_origen.equals("") && banco_destino.equals("")){
                     remplazar = "fecha BETWEEN '"+fechaI+"' AND '"+fechaF+ "' and reg_status = '' and aprobado = 'N'";
               }else {
                    remplazar =  "banco_origen like '%"+banco_origen+"%' AND banco_destino like '%"+banco_destino+ "%' and reg_status = '' and aprobado = 'N'";
               }
                String agregar = "";
                String sql = this.obtenerSQL("SQL_EXISTE_TRASLADO_APROBADO");
                agregar = sql.replaceAll("#CONDICION#", remplazar);
                con = this.conectarJNDI("SQL_EXISTE_TRASLADO_APROBADO");//JJCastro fase2
                st = con.prepareStatement( agregar );  
                rs = st.executeQuery();
                if (rs.next()){
                    flag = true;
                }
        }
         catch(Exception e){
             e.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return flag;
    }
        /*
      Funcion que modifica el traslado de comprobante  en la tabla comprobante
      fecha:  23 de enero del 2007
      Autor :  fily steven fernandez
      Trasporte sabchez Polo
      */
    public void modificar_traslado_comprobante(String banco_origen, String  banco_destino, String sucursal_origen, String sucursal_destino,String valor, String comprobante) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR_TRASLADO_COMPROBANTE";//JJCastro fase2
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante);
            st.setString(2, banco_origen);
            st.setString(3, banco_destino);
            st.setString(4, sucursal_origen);
	    st.setString(5, sucursal_destino); 
            st.setString(6, valor);
            st.executeUpdate();  
        }
         }catch(Exception e){
             e.printStackTrace();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    
    public java.util.Vector getVector() {
        return vector;
    }
    
    //funcion que modifica el comprobante
/**
 * 
 * @param llave
 * @param Borigen
 * @param Sorigen
 * @param Bdestino
 * @param Sdestino
 * @param totalCredDeb
 * @throws Exception
 */
    public void modificarComrpobante(String llave,String Borigen,String Sorigen,String Bdestino,String Sdestino,double totalCredDeb) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String detalles = "";
        detalles = Borigen + " - "+ Sorigen + " == "+ Bdestino + " - "+Sdestino;
        String query = "SQL_MODIFICAR_COMPROBANTES_TR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, detalles);
            st.setDouble(2, totalCredDeb);
            st.setDouble(3, totalCredDeb);
            st.setString(4, llave);
            st.executeUpdate();
        }}catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al MODIFICAR COMPROBANTE[COMPROBANTE.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
     
    //funcion que modifica el comprodect 
    public void modificarComprodec(String llave,String banco,String sucursal,double totalDebito,double totalCredito,String detallesOri) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String detalles = "";
        detalles = banco + "-"+ sucursal;
        String query = "SQL_MODIFICAR_COMPRODECT_TR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, detalles);
            st.setDouble(2, totalDebito);
            st.setDouble(3, totalCredito);
            st.setString(4, llave);
            st.setString(5, detallesOri.trim());
            st.executeUpdate();
        }}catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error al MODIFICAR MODIFICAR_COMPRODECT_TR[COMPROBANTE.xml]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
/**
 * 
 * @param Banco
 * @param Sucursal
 * @throws Exception
 */
        public void Buscar_DatosBanco(String Banco,String Sucursal) throws Exception {
            Connection con = null;//JJCastro fase2
            PreparedStatement st = null;
            ResultSet rs = null;
            Banco banco = null;
            vectorDatosBanco = new Vector();
            BeanGeneral Bean;
            String query = "SQL_BUCAR_DATOS_BANCO";//JJCastro fase2
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Banco);
            st.setString(2, Sucursal);
            rs = st.executeQuery();
            System.out.println("query banco "+st.toString());
             rs = st.executeQuery();
            if(rs.next()){
                Bean = new BeanGeneral();
                
                Bean.setValor_01 ((rs.getString("cuenta")!=null)?rs.getString("cuenta"):"");
                Bean.setValor_02 ((rs.getString("moneda")!=null)?rs.getString("moneda"):"");
                this.vectorDatosBanco.addElement( Bean );
                Bean = null;//Liberar Espacio JJCastro
            }
        }
         } catch(Exception e){
             e.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    public java.util.Vector getVectorSerie() {
        return vectorSerie;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVectorSerie(java.util.Vector vectorSerie) {
        this.vectorSerie = vectorSerie;
    }
    
    /**
     * Getter for property vectorBanco.
     * @return Value of property vectorBanco.
     */
    public java.util.Vector getVectorBanco() {
        return vectorBanco;
    }
    
    /**
     * Setter for property vectorBanco.
     * @param vectorBanco New value of property vectorBanco.
     */
    public void setVectorBanco(java.util.Vector vectorBanco) {
        this.vectorBanco = vectorBanco;
    }
    
    /**
     * Getter for property vectorTraslado.
     * @return Value of property vectorTraslado.
     */
    public java.util.Vector getVectorTraslado() {
        return vectorTraslado;
    }
    
    /**
     * Setter for property vectorTraslado.
     * @param vectorTraslado New value of property vectorTraslado.
     */
    public void setVectorTraslado(java.util.Vector vectorTraslado) {
        this.vectorTraslado = vectorTraslado;
    }
    
    /**
     * Getter for property vectorDatosBanco.
     * @return Value of property vectorDatosBanco.
     */
    public java.util.Vector getVectorDatosBanco() {
        return vectorDatosBanco;
    }
    
    /**
     * Setter for property vectorDatosBanco.
     * @param vectorDatosBanco New value of property vectorDatosBanco.
     */
    public void setVectorDatosBanco(java.util.Vector vectorDatosBanco) {
        this.vectorDatosBanco = vectorDatosBanco;
    }
    
      /*
    Funcion que agrega en traslado bancario en la tabla comprodect
    fecha:  23 de enero del 2007
    Autor :  fily steven fernandez
    Trasporte sabchez Polo
    */
    public void agregarComprodect(int grupo_t,String numdoc,String detalle,double totaldebito,double totalcredito, BeanGeneral traslado)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        BeanGeneral bank;
        String auxiliar="";
        String auxi="";
        String Ini = "";
        String Fin = "";
        int inicio =0;
        int fin = 1;
        int contador = 0;
        String Origen = "";
        String Sucursal = "";
        String tipo = "";
        String Ncuenta = "";
        int cont = 0;
        int temp = 0;
        String query = "SQL_GENERAR_COMPRODECT";//JJCastro fase2
        try {
            tipo = traslado.getValor_13();
            if(tipo.equals("1")){
                
                Origen = traslado.getValor_11();
                Sucursal = traslado.getValor_12();
                Buscar_DatosBanco(Origen,Sucursal);
            }else{
                Origen = traslado.getValor_09();
                Sucursal = traslado.getValor_10();
                Buscar_DatosBanco(Origen,Sucursal);
            }
            Vector vectoresL =  this.getVectorDatosBanco();
            BeanGeneral bean = (BeanGeneral) vectoresL.lastElement();
            if(bean.getValor_01().equals("")){
                auxiliar = bean.getValor_01();
                auxi = auxiliar;
            }else{
                auxiliar = bean.getValor_01();
                for(int i = 0; i< auxiliar.length(); i++){
                    Ini = auxiliar.substring(inicio,fin);
                    if(Ini.equals(";")){
                        contador = auxiliar.length();
                        auxi= auxiliar.substring(fin,contador);
                        cont = inicio - 1;
                        Ncuenta = auxiliar.substring(0,inicio);
                        temp = 1;
                    }
                    inicio++;
                    fin++;
                }
                if (temp == 0){
                    Ncuenta  = auxiliar;
                }
            }
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setInt(1, grupo_t);
            st.setString(2, numdoc);
            st.setString(3, detalle);
            st.setDouble(4,  totaldebito);
            st.setDouble(5, totalcredito);
            st.setString(6, traslado.getValor_01());
            st.setString(7, traslado.getValor_02());
            st.setString(8, traslado.getValor_04());
            st.setString(9, traslado.getValor_06());
            st.setString(10, traslado.getValor_07());
            st.setString(11,Ncuenta);
            st.setString(12,auxi);
            st.executeUpdate();
        }}catch(Exception e){
            e.printStackTrace();
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
  }

}


//Fily 23 Marzo 2007
