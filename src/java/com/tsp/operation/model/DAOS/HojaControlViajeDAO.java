
/***********************************************************************************
 * Nombre clase :                 HojaControlViajeDAO.java                         *
 * Descripcion :                  Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD para generar los datos de la impresion *
 *                                de la hoja de control de viaje                   *
 * Autor :                        Ing. Juan Manuel Escandon Perez                  *
 * Fecha :                        29 de diciembre de 2005, 11:48 AM               *
 * Version :                      1.0                                             *
 * Copyright :                     Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.MainDAO;


public class HojaControlViajeDAO extends MainDAO{
    
    private static String SQLCONSULTA       =   "   SELECT p.numpla, get_nombreciudad(p.oripla) as origen, get_nombreciudad(p.despla) as destino, " +
                                                "   substring(p.fecdsp,0,12)as fecha, p.platlr,                                         "+
                                                "   r.numrem, get_nombrecliente(r.cliente) AS cliente,  p.plaveh,                       "+
                                                "   p.precinto, pt.clase                                                                "+
                                                "   FROM planilla p                                                                     "+        
                                                "   LEFT OUTER JOIN plarem nr ON ( nr.numpla = p.numpla )                               "+
                                                "   LEFT OUTER JOIN remesa r  ON ( r.numrem = nr.numrem )                               "+
                                                "   LEFT OUTER JOIN placa pl  ON ( pl.placa = p.plaveh  )                               "+ 
                                                "   LEFT OUTER JOIN placa pt  ON ( pt.placa = p.platlr  )                               "+
                                                "   WHERE p.numpla = ?                                                                  ";
    
    /** Creates a new instance of HojaControlViajeDAO */
    public HojaControlViajeDAO() {
   super("HojaControlViajeDAO.xml");//JJCastro fase2
    }
      /**
     * Metodo ControlViaje, busca un objeto de tipo HojaControlViaje dada una
     * determinanda numero planilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numpla 
     * @version : 1.0
     */
    public HojaControlViaje ControlViaje( String numpla ) throws Exception{

        Connection con    = null;
        ResultSet       rs   = null;
        String query = "SQLCONSULTA";
        PreparedStatement st = null;

        HojaControlViaje hvc = new HojaControlViaje();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, numpla);            
            rs=st.executeQuery();
            while(rs.next()){
                hvc =  HojaControlViaje.load(rs);
                break;
            }
        }}catch(Exception e){
            throw new SQLException("Error en rutina ControlViaje [HojaControlViajeDAO].... \n"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return hvc;
    }
    
}
