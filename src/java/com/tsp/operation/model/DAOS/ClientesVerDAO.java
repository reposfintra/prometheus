/* * electricaribeDAO.java * * Created on 1 de junio de 2009, 10:52 */
package com.tsp.operation.model.DAOS;

/** * * @author  Fintra */
import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;

public class ClientesVerDAO extends MainDAO {

    String idusuario;
    private List listaPrefacturas;

    public ClientesVerDAO() {
        super("ClientesVerDAO.xml");
    }
    public ClientesVerDAO(String dataBaseName) {
        super("ClientesVerDAO.xml", dataBaseName);
    }

    public String insertarCl(ClienteEca cleca, String usuario) throws Exception {
        String respuesta = "";
        PreparedStatement st = null;
        String query = "SQL_INSERT_CLIENTE_ECA";
        try {
            st = crearPreparedStatement(query);

            SerieGeneral s_id_cliente = getSerie("FINV", "OP", "CLIMS");
            setSerie("FINV", "OP", "CLIMS");
            String id_cliente = s_id_cliente.getUltimo_prefijo_numero();
            cleca.setId_cliente(id_cliente);
            st.setString(1, cleca.getId_cliente());
            st.setString(2, cleca.getNit());
            st.setString(3, cleca.getNombre());
            st.setString(4, cleca.getNombre_contacto());
            st.setString(5, cleca.getTel1());
            st.setString(6, cleca.getTel2());
            st.setString(7, cleca.getTipo());
            st.setString(8, cleca.getDepartamento());
            st.setString(9, cleca.getCiudad());
            st.setString(10, cleca.getDireccion());
            st.setString(11, cleca.getSector());
            st.setString(12, cleca.getNombre_representante());
            st.setString(13, cleca.getCargo_contacto());
            st.setString(14, cleca.getTel_representante());
            st.setString(15, cleca.getCelular_representante());
            st.setString(16, usuario);
            st.setString(17, usuario);
            st.setString(18, cleca.getId_ejecutivo());
            if (cleca.isOficial()) {
                st.setString(19, "S");
            } else {
                st.setString(19, "N");
            }
            respuesta = st.toString();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public String insertSubcliente(String hijo, String padre, String user) throws Exception {
        String query = "SQL_INSERTAR_SUBCLIENTE";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, hijo);
            st.setString(2, padre==null?"":padre);//13/04/2010 JJCastro
            st.setString(3, user);
            st.setString(4, user);
            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_INSERTAR_SUBCLIENTE. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }

        return respuesta;
    }

    public String updatePadre(ClienteEca cleca, String usuario) throws Exception {
        String respuesta = "";
        PreparedStatement st = null;
        String query = "SQL_UPDATE_PADRE";
        String sql = "";
        Connection conn = null;
        try {

            st = crearPreparedStatement(query);
            st.setString(1, cleca.getId_padre());
            st.setString(2, usuario);
            st.setString(3, cleca.getId_cliente());
            respuesta = st.toString();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public String insertarOf(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        PreparedStatement st = null;
        String query = "SQL_INSERT_OFERTA_ECA";
        try {
            st = crearPreparedStatement(query);

            SerieGeneral s_id_cliente = getSerie("FINV", "OP", "OFMS");
            setSerie("FINV", "OP", "OFMS");
            ofca.setId_solicitud(s_id_cliente.getUltimo_prefijo_numero());


            st.setString(1, ofca.getId_solicitud());
            st.setString(2, ofca.getId_cliente());
            st.setString(3, ofca.getDescripcion());
            st.setString(4, usuario);
            st.setString(5, ofca.getNic());
            st.setString(6, ofca.getTipo_solicitud());
            st.setString(7, ofca.getTipo_solicitud());
            st.setString(8, ofca.getOficial());
            st.setString(9, ofca.getAviso());

            respuesta = st.toString();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public String insertarAcc(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        PreparedStatement st = null;
        String query = "SQL_INSERT_ACCION_ECA";
        try {
            st = crearPreparedStatement(query);

            SerieGeneral s_id_cliente = getSerie("FINV", "OP", "ACMS");
            setSerie("FINV", "OP", "ACMS");
            acceca.setId_accion(s_id_cliente.getUltimo_prefijo_numero());
            st.setString(1, acceca.getId_accion());
            st.setString(2, acceca.getId_solicitud());
            st.setString(3, acceca.getContratista());
            st.setString(4, acceca.getDescripcion());
            st.setString(5, usuario);
            respuesta = st.toString();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public String delAcc(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        PreparedStatement st = null;
        String query = "SQL_DELETE_ACCION_ECA";
        try {
            st = crearPreparedStatement(query);

            st.setString(1, usuario);
            st.setString(2, acceca.getId_accion());
            respuesta = st.toString();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public String updateCl(ClienteEca cleca, String usuario, boolean sw) throws Exception {
        String respuesta = "";
        PreparedStatement st = null;
        String query = "SQL_UPDATE_CLIENTE_ECA";
        String sql = "";
        Connection conn = null;
        try {

            conn = this.conectar("SQL_UPDATE_CLIENTE_ECA");
            String param = "";
            param = param + "nit='" + cleca.getNit() + "', ";
            param = param + "nombre='" + cleca.getNombre() + "', ";
            param = param + "nombre_contacto='" + cleca.getNombre_contacto() + "', ";
            param = param + "tel1='" + cleca.getTel1() + "', ";
            param = param + "tel2='" + cleca.getTel2() + "', ";
            param = param + "tipo='" + cleca.getTipo() + "', ";
            param = param + "departamento='" + cleca.getDepartamento() + "', ";
            param = param + "ciudad='" + cleca.getCiudad() + "', ";
            param = param + "direccion='" + cleca.getDireccion() + "', ";
            param = param + "sector='" + cleca.getSector() + "', ";
            param = param + "nombre_representante='" + cleca.getNombre_representante() + "', ";
            param = param + "cargo_contacto='" + cleca.getCargo_contacto() + "', ";
            param = param + "tel_representante='" + cleca.getTel_representante() + "', ";
            param = param + "celular_representante='" + cleca.getCelular_representante() + "', ";
            param = param + "user_update='" + usuario + "', ";
            if (sw) {
                param = param + "id_ejecutivo='" + cleca.getId_ejecutivo() + "', ";
            }
            param = param + "esoficial='" + ((cleca.isOficial()) ? "S" : "N") + "', ";

            st = conn.prepareStatement(obtenerSQL("SQL_UPDATE_CLIENTE_ECA").replaceAll("#PARAM#", param));
            st.setString(1, cleca.getId_cliente());
            respuesta = st.toString();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public String updateOf(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        PreparedStatement st = null;
        String query = "SQL_UPDATE_OFERTA_ECA";
        String sql = "";
        Connection conn = null;
        try {

            conn = this.conectar("SQL_UPDATE_OFERTA_ECA");

            st = conn.prepareStatement(obtenerSQL("SQL_UPDATE_OFERTA_ECA"));
            st.setString(1, ofca.getDescripcion());
            st.setString(2, ofca.getOficial());
            st.setString(3, usuario);
            st.setString(4, ofca.getId_solicitud());
            respuesta = st.toString();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public String insertarNics(String idcliente, String usuario, String[] nics) throws Exception {
        String respuesta = "";
        PreparedStatement st = null;
        String query = "SQL_INSERT_NICS";
        try {
            st = crearPreparedStatement(query);
            for (int i = 0; nics != null && i < nics.length; i++) {
                st.setString(1, nics[i]);
                st.setString(2, idcliente);
                st.setString(3, usuario);
                st.setString(4, usuario);
                respuesta = respuesta + st.toString();
            }
            System.out.println(respuesta);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

    public String buscarCl(ClienteEca cleca) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = null;
        String respuesta = "";
        boolean sw = false;
        cleca.setCiudad("");
        cleca.setNombre_contacto("");
        cleca.setCelular_representante("");
        cleca.setDepartamento("");
        cleca.setDireccion("");
        cleca.setNit("");
        cleca.setCargo_contacto("");
        cleca.setNombre("");
        cleca.setNombre_representante("");
        cleca.setSector("");
        cleca.setTel1("");
        cleca.setTel2("");
        cleca.setTel_representante("");
        cleca.setTipo("");
        cleca.setCreation_user("");
        cleca.setId_ejecutivo("");
        cleca.setId_padre("");
        cleca.setNombre_padre("");

        try {
            conn = this.conectar("SQL_GET_CLIENTE");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_CLIENTE"));
            st.setString(1, cleca.getId_cliente());
            rs = st.executeQuery();

            if (rs.next()) {
                cleca.setCiudad(rs.getString("ciudad"));
                cleca.setNombre_contacto(rs.getString("nombre_contacto"));
                cleca.setCelular_representante(rs.getString("celular_representante"));
                cleca.setDepartamento(rs.getString("departamento"));
                cleca.setDireccion(rs.getString("direccion"));
                cleca.setNit(rs.getString("nit"));
                cleca.setCargo_contacto(rs.getString("cargo_contacto"));
                cleca.setNombre(rs.getString("nombre"));
                cleca.setNombre_representante(rs.getString("nombre_representante"));
                cleca.setSector(rs.getString("sector"));
                cleca.setTel1(rs.getString("tel1"));
                cleca.setTel2(rs.getString("tel2"));
                cleca.setTel_representante(rs.getString("tel_representante"));
                cleca.setTipo(rs.getString("tipo"));
                cleca.setId_ejecutivo(rs.getString("nombre_ejecutivo"));
                cleca.setCreation_user(rs.getString("creation_user"));
                cleca.setNics(getNics(cleca.getId_cliente()));
                sw = true;
            }

            this.desconectar("SQL_GET_CLIENTE");

            if (sw) {
                conn = this.conectar("SQL_GET_PADRE");
                st = conn.prepareStatement(this.obtenerSQL("SQL_GET_PADRE"));
                st.setString(1, cleca.getId_cliente());
                rs = st.executeQuery();

                if (rs.next()) {
                    cleca.setId_padre(rs.getString("id_cliente_padre"));
                    cleca.setNombre_padre(rs.getString("nombre"));
                }

                this.desconectar("SQL_GET_PADRE");
            }


            if (!sw) {
                respuesta = "No hay ningun usuario con ese id";
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_CLENTE \n " + e.getMessage());
        } finally {
            st.close();
            if (conn != null) {
                this.desconectar("SQL_GET_CLIENTE");
            }
        }
        return respuesta;
    }

    public String buscarOf(OfertaElca ofca) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = null;
        String respuesta = "";
        boolean sw = false;
        ofca.setCreacion_fecha_entrega_oferta("");
        ofca.setCreation_date("");
        ofca.setCreation_user("");
        ofca.setDescripcion("");
        ofca.setFecha_entrega_oferta("");
        ofca.setId_cliente("");
        ofca.setId_oferta("");
        ofca.setLast_update("");
        ofca.setNic("");
        ofca.setNum_os("");
        ofca.setReg_status("");
        ofca.setUser_update("");
        ofca.setUsuario_entrega_oferta("");

        try {
            conn = this.conectar("SQL_GET_OFERTA");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_OFERTA"));
            st.setString(1, ofca.getId_solicitud());
            rs = st.executeQuery();
            if (rs.next()) {
                ofca.setCreacion_fecha_entrega_oferta(rs.getString("creacion_fecha_entrega_oferta"));
                ofca.setCreation_date(rs.getString("creation_date"));
                ofca.setCreation_user(rs.getString("creation_user"));
                ofca.setDescripcion(rs.getString("descripcion"));
                ofca.setFecha_entrega_oferta(rs.getString("fecha_entrega_oferta"));
                ofca.setId_cliente(rs.getString("id_cliente"));
                ofca.setId_oferta(rs.getString("id_oferta"));
                ofca.setLast_update(rs.getString("last_update"));
                ofca.setNic(rs.getString("nic"));
                ofca.setNum_os(rs.getString("num_os"));
                ofca.setReg_status(rs.getString("reg_status"));
                ofca.setUser_update(rs.getString("user_update"));
                ofca.setUsuario_entrega_oferta(rs.getString("usuario_entrega_oferta"));
                ofca.setTipo_solicitud(rs.getString("tipo_solicitud"));
                sw = true;
            }
            if (!sw) {
                respuesta = "No hay ninguna solicitud con ese id";
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_OFERTA \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_OFERTA");
        }
        return respuesta;
    }

    public ArrayList buscarAcc(AccionesEca acceca, String nitprop) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = null;
        String respuesta = "";
        ArrayList arl = new ArrayList();
        boolean sw = false;
        try {
            conn = this.conectar("SQL_GET_ACCIONES_ECA");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_ACCIONES_ECA"));
            st.setString(1, acceca.getId_solicitud());
            if (nitprop.length() > 2 && nitprop.substring(0, 2).equals("CC")) {
                st.setString(2, nitprop);
            } else {
                st.setString(2, "%");
            }

            System.out.println("sqllll" + st.toString());
            rs = st.executeQuery();
            while (rs.next()) {
                AccionesEca newacc = new AccionesEca();
                newacc.setId_accion(rs.getString("id_accion"));
                newacc.setAdministracion(rs.getString("administracion"));
                newacc.setContratista(rs.getString("contratista"));
                newacc.setCreation_date(rs.getString("creation_date"));
                newacc.setCreation_user(rs.getString("creation_user"));
                newacc.setDescripcion(rs.getString("descripcion"));
                newacc.setEstado(rs.getString("estado"));
                newacc.setId_solicitud(rs.getString("id_solicitud"));
                newacc.setImprevisto(rs.getString("imprevisto"));
                newacc.setLast_update(rs.getString("last_update"));
                newacc.setMano_obra(rs.getString("mano_obra"));
                newacc.setMaterial(rs.getString("material"));
                newacc.setObservaciones(rs.getString("observaciones"));
                newacc.setPorc_administracion(rs.getString("porc_administracion"));
                newacc.setPorc_imprevisto(rs.getString("porc_imprevisto"));
                newacc.setPorc_utilidad(rs.getString("porc_utilidad"));
                newacc.setReg_status(rs.getString("reg_status"));
                newacc.setTipo_trabajo(rs.getString("tipo_trabajo"));
                newacc.setTransporte(rs.getString("transporte"));
                newacc.setUser_update(rs.getString("user_update"));
                newacc.setUtilidad(rs.getString("utilidad"));
                arl.add(newacc);
                sw = true;
            }
            if (!sw) {
                respuesta = "No hay ninguna accion con ese id";
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_ACCION \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_ACCIONES_ECA");
        }
        return arl;
    }

    public String[] getNics(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        String[] arll = null;

        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_NICS");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_NICS"));
            st.setString(1, param);
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(rs.getString("nic"));
            }
            arll = new String[arl.size()];
            for (int i = 0; i < arl.size(); i++) {
                arll[i] = (String) arl.get(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_NICS \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_NICS");
        }
        return arll;
    }

    public String buscarMails(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";

        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_MAILS");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_MAILS"));
            st.setString(1, param);
            rs = st.executeQuery();
            while (rs.next()) {
                arl = arl + rs.getString("email") + ";";
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_MAILS \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_MAILS");
        }
        return arl;
    }

    public String getMailEjecutivo(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";

        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_MAIL_EJECUTIVO");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_MAIL_EJECUTIVO"));
            st.setString(1, param);
            rs = st.executeQuery();
            if (rs.next()) {
                arl = rs.getString("correo");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_MAIL_EJECUTIVO \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_MAIL_EJECUTIVO");
        }
        return arl;
    }

    public String getMailContratista(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";

        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_MAIL_CONTRATISTA");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_MAIL_CONTRATISTA"));
            st.setString(1, param);
            rs = st.executeQuery();
            if (rs.next()) {
                arl = rs.getString("email");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_MAIL_CONTRATISTA \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_MAIL_CONTRATISTA");
        }
        return arl;
    }

    public String getLoginEjecutivo(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";

        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_LOGIN_EJECUTIVO");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_LOGIN_EJECUTIVO"));
            st.setString(1, param);
            rs = st.executeQuery();
            if (rs.next()) {
                arl = rs.getString("idusuario");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_LOGIN_EJECUTIVO \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_LOGIN_EJECUTIVO");
        }
        return arl;
    }

    public ArrayList getDatos(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_DATOS");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_DATOS").replaceAll("#PARAM", param));
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(rs.getString("datos"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_SET_MS. \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_DATOS");
        }
        return arl;
    }

    public ArrayList getEjecutivos() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_EJECUTIVOS");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_EJECUTIVOS"));
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("nombre"), rs.getString("id_ejecutivo") + "," + rs.getString("nombre")});
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_EJECUTIVOS. \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_EJECUTIVOS");
        }
        return arl;
    }

    public ArrayList getPadres() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_PADRES");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_PADRES"));
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("nombre"), rs.getString("id_cliente_padre")});
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_PADRES. \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_PADRES");
        }
        return arl;
    }

    public String getAviso(String id_sol) throws SQLException{
        PreparedStatement   st    = null;
        ResultSet           rs    = null;
        ArrayList           arl   = new ArrayList();
        Connection          conn  = null;
        String              aviso = "";

        try {
            conn = this.conectar("SQL_GET_AVISO");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_AVISO"));
            st.setString(1, id_sol);
            rs = st.executeQuery();

            if (rs.next()) {
                aviso = rs.getString("aviso");
            }

        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_AVISO. \n " + e.getMessage());
        }
        finally {
            st.close();
            this.desconectar("SQL_GET_AVISO");
        }

        return aviso;
    }

    public ArrayList getContratistas() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_CONTRATISTAS");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_CONTRATISTAS"));
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("descripcion"), rs.getString("id_contratista")});
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_CONTRATISTAS. \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_CONTRATISTAS");
        }
        return arl;
    }

    public ArrayList getEstado() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_ESTADOS");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_ESTADOS"));
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("estado"), rs.getString("id_estado")});
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_ESTADOS. \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_ESTADOS");
        }
        return arl;
    }

    public boolean ispermitted(String perfil, String accion) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        String answ = "N";
        try {
            conn = this.conectar("SQL_GET_PERMISO");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_PERMISO"));
            st.setString(1, perfil);
            st.setString(2, accion);
            rs = st.executeQuery();
            if (rs.next()) {
                answ = rs.getString("dato");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_PERMISO. \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_PERMISO");
        }
        if (answ.equals("S")) {
            return true;
        } else {
            return false;
        }
    }

    public String getPerfil(String usuario) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection conn = null;
        String answ = "";
        try {
            conn = this.conectar("SQL_GET_PERFIL");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_PERFIL"));
            st.setString(1, usuario);
            rs = st.executeQuery();
            if (rs.next()) {
                answ = rs.getString("dato");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_PERMISO. \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_PERFIL");
        }
        return answ;
    }

    public SerieGeneral getSerie(String dstrct, String agency_id, String document_type) throws SQLException {

        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;


        String query = "SQL_GET_ULTIMO_NUMERO";
        SerieGeneral serie = null;


        try {


            con = this.conectar(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);
            st.setString(2, agency_id);
            st.setString(3, document_type);


            rs = st.executeQuery();

            if (rs.next()) {
                serie = new SerieGeneral();
                serie = (SerieGeneral.load(rs));
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA BUSQUEDA DE UNA SERIE. \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar(query);
        }
        return serie;

    }

    public void setSerie(String dstrct, String agency_id, String document_type) throws SQLException {

        PreparedStatement st = null;
        Connection con = null;

        String query = "SQL_SET_ULTIMO_NUMERO";

        try {
            con = this.conectar(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, dstrct);
            st.setString(2, agency_id);
            st.setString(3, document_type);

            st.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA SERIE.  \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar(query);
        }

    }

    public void ejecutarSQL(Vector comandosSQL) throws SQLException {

        Connection con = null;
        Statement stmt = null;

        String query = "SQL_GET_CONECTION";

        try {
            con = this.conectar(query);

            if (con != null) {

                con.setAutoCommit(false);
                stmt = con.createStatement();
                for (int i = 0; i < comandosSQL.size(); i++) {
                    String comando = (String) comandosSQL.elementAt(i);
                    ////System.out.println(comando);
                    stmt.executeUpdate(comando);

                }

                con.commit();
            }
        } catch (SQLException e) {
            // Efectuando rollback en caso de error
            try {

                con.rollback();
            } catch (SQLException ignored) {
                System.out.println("error en execuuute" + ignored.toString());
                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode() + " <br> La siguiente exception es : ----" + ignored.getNextException());
            }
            throw new SQLException("ERROR DURANTE LA TRANSACCION, LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode() + " <br> La siguiente exception es : ----" + e.getNextException());
        } finally {
            stmt.close();
            this.desconectar(query);
        }


    }

    public String[] getDatosMensaje(String accion) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;

        String[] arll = new String[4];

        Connection conn = null;
        try {
            conn = this.conectar("SQL_GET_DATOS_MSG");
            st = conn.prepareStatement(this.obtenerSQL("SQL_GET_DATOS_MSG"));
            st.setString(1, accion);
            rs = st.executeQuery();
            if (rs.next()) {
                arll[0] = rs.getString("nombre_contra");
                arll[1] = rs.getString("sector");
                arll[2] = rs.getString("nombre_cli");
                arll[3] = rs.getString("mail_contra");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("error en getDatosMensaje:" + e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_DATOS_MSG \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar("SQL_GET_DATOS_MSG");
        }
        return arll;
    }

    public boolean isOficial(String id) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_IS_OFICIAL";
        String sql = "";
        boolean ok = false;

        try {
            con = this.conectar(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();

            if (rs.next()) {
                ok = true;
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(query);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return ok;
    }
    //2010-02-04
    //Busca en que estado se encuentra una accion
    public String estadoAccion(String accion) throws Exception{
        String estado="";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ESTADO_ACCION";
        String sql = "";
        try{
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, accion);
            rs = ps.executeQuery();
            if(rs.next()) estado = rs.getString("estado");
        }
        catch (Exception e) {
            throw new Exception("Ha ocurrido un error al buscar el estado de la accion "+accion+" :"+e.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return estado;
    }


}  