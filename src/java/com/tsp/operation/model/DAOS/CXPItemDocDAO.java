/*
 * CXP_DocsDAO.java
 *
 * Created on 10 de Octubre de 2005, 12:14 PM
 */

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

/**
 *
 * @author  hosorio
 */

public class CXPItemDocDAO extends MainDAO{
    
    /** Creates a new instance of EstadoDAO */
    public CXPItemDocDAO() {
        super("CXPItemDocDAO.xml");
    }
    public CXPItemDocDAO(String dataBaseName) {
        super("CXPItemDocDAO.xml", dataBaseName);
    }
    private Vector vecCxpItemsDoc;
    private CXPItemDoc itemFactura = new CXPItemDoc();
    
    
        private static final  String SQL_ITEMS_FACTURA = " select" +
    "       C.documento," +
    "       C.tipo_documento," +
    "       C.proveedor," +
    "       C.dstrct," +
    "       C.item, " +
    "       C.descripcion,  " +
    "       C.vlr,  " +
    "       C.codigo_cuenta,  " +
    "       C.codigo_abc," +
    "       C.planilla," +
    "       A.textoactivo, " +
    "       cta.nombre_largo "+
    "from  " +
    "       fin.cxp_items_doc C LEFT join fin.cxp_obs_aprob_item A on " +
    "       (C.dstrct=A.dstrct and" +
    "       C.proveedor=A.proveedor and" +
    "       C.tipo_documento=A.tipo_documento and" +
    "       C.documento=A.documento and" +
    "       C.item=A.item) " +
    "       inner join con.cuentas cta on (C.codigo_cuenta=cta.cuenta)"+
    "where "+
    "       C.reg_status<>'A' and" +
    "       C.dstrct=? and" +
    "       C.proveedor=? and" +
    "       C.documento=? and "+
    "       C.tipo_documento=?";
    
    //Jose 26/12/2005
    private static final String SQL_INSERT_CXP_ITEM_DOC = "INSERT INTO cxp_items_doc "+
        "(dstrct, proveedor, tipo_documento, documento, item, descripcion, vlr, codigo_cuenta, codigo_abc, planilla, creation_date, creation_user, base) "+
        "VALUES (?,?,'038',?,?,?,?,?,?,?,'now()',?,?)";
    
  
    //Ivan DArio Gomez
    /**
         * Metodo buscarItemsFactura, recibe el objeto Documento por pagar y realiza una busqueda
         * de los items relacionados con ese documento
         * @autor : Ing. Henry A. Osorio Gonz�lez
         * @param : CXP_Doc factura
         * @version : 1.0
         */
        public void buscarItemsFactura(CXP_Doc factura) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
                vecCxpItemsDoc = null;
                vecCxpItemsDoc = new Vector();
                itemFactura = null;
                String query = "SQL_ITEMS_FACTURA";
                try{

                        con = this.conectarJNDI(query);//JJCastro fase2
                        if (con != null) {
                                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                st.setString(1, factura.getDstrct());
                                st.setString(2, factura.getProveedor());
                                st.setString(3, factura.getDocumento());
                                st.setString(4, factura.getTipo_documento());
                                rs = st.executeQuery();
                                String textoactivo = "";
                                while (rs.next()) {
                                        itemFactura = new CXPItemDoc();
                                        itemFactura.setDocumento(rs.getString("documento"));
                                        itemFactura.setTipo_documento(rs.getString("tipo_documento"));
                                        itemFactura.setItem(rs.getString("item"));
                                        itemFactura.setProveedor(rs.getString("proveedor"));
                                        itemFactura.setDstrct(rs.getString("dstrct"));
                                        itemFactura.setDescripcion(rs.getString("descripcion"));
                                        itemFactura.setVlr(rs.getDouble("vlr"));
                                        itemFactura.setCodigo_cuenta(rs.getString("codigo_cuenta"));
                                        itemFactura.setCodigo_abc(rs.getString("codigo_abc"));
                                        itemFactura.setPlanilla(rs.getString("planilla"));
                                        itemFactura.setDstrct(rs.getString("dstrct"));
                                        textoactivo = rs.getString("textoactivo");
                                        itemFactura.setRef3(rs.getString("nombre_largo"));
                                        if (textoactivo==null) {
                                                textoactivo = "N";
                                        }
                                        itemFactura.setTextoactivo(textoactivo);
                                        if (textoactivo.equals("R")) {
                                                itemFactura.setBanderaRoja(true);
                                        } else if (textoactivo.equals("V")) {
                                                itemFactura.setBanderaVerde(true);
                                        } else if (textoactivo.equals("N")) {
                                                itemFactura.setBanderaVerde(false);
                                                itemFactura.setBanderaRoja(false);
                                        }
                                        vecCxpItemsDoc.addElement(itemFactura);
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL BUSCAR ITEMS FACTURA" + e.getMessage()+"" + e.getErrorCode());
                }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        }
    
    /**
     * Getter for property vectorItemFacturas.
     * @return Value of property vecCxpItemsDoc.
     */
    public Vector vectorItemsFacturas(){
        return vecCxpItemsDoc;
    }
    
    /**
     * Getter for property itemFactura.
     * @return Value of property itemFactura.
     */
    public com.tsp.operation.model.beans.CXPItemDoc getItemFactura(){
        return itemFactura;
    }
    
    /**
     * Setter for property itemFactura.
     * @param itemFactura New value of property itemFactura.
     */
    public void setItemFactura(com.tsp.operation.model.beans.CXPItemDoc itemFactura) {
        this.itemFactura = itemFactura;
    }
    
    /**
     * Getter for property vecCxpItemsDoc.
     * @return Value of property vecCxpItemsDoc.
     */
    public java.util.Vector getVecCxpItemsDoc() {
        return vecCxpItemsDoc;
    }
    
    /**
     * Setter for property vecCxpItemsDoc.
     * @param vecCxpItemsDoc New value of property vecCxpItemsDoc.
     */
    public void setVecCxpItemsDoc(java.util.Vector vecCxpItemsDoc) {
        this.vecCxpItemsDoc = vecCxpItemsDoc;
    }
    
    /**
     * Metodo: getCXPItem_doc, permite retornar un objeto de registros de CXPItem_doc.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public CXPItemDoc getCXPItem_doc () {
        return itemFactura;
    }
    
    /**
     * Metodo: setCXPItem_doc, permite obtener un objeto de registros de CXPItem_doc.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setCXPItem_doc (CXPItemDoc itemFactura) {
        this.itemFactura = itemFactura;
    }
    
    /**
     * Metodo ingresarCXPItem_doc, permite ingrear una factura
     * recibe por parametro
     * @autor : Ing. Jose de la rosa
     * @param : 
     * @version : 1.0
     */
    public void ingresarCXPItem_doc() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        String query = "SQL_INSERT_CXP_ITEM_DOC";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null){
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(1,itemFactura.getDstrct ());
                st.setString(2,itemFactura.getProveedor ());
                st.setString(3,itemFactura.getDocumento ());
                st.setString(4,itemFactura.getItem ());
                st.setString(5,itemFactura.getDescripcion ());
                st.setDouble(6,itemFactura.getVlr ());
                st.setString(7,itemFactura.getCodigo_cuenta ());
                st.setString(8,itemFactura.getCodigo_abc ());
                st.setString(9,itemFactura.getPlanilla ());
                st.setString(10,itemFactura.getCreation_user ());
                st.setString(11,itemFactura.getBase ());              
                st.execute();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL Ingresar CXPItem DOC " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }    
   
    
}
