/********************************************************************
 *  Nombre Clase.................   ReporteRetroactivoDAO.java
 *  Descripci�n..................   DAO del reporte de retroactivos
 *  Autor........................   Ing. Tito Andr�s Maturana
 *                                  Ing. Leonardo Parodi
 *  Fecha........................   13.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import java.util.*;
import java.text.*;

public class ReporteRetroactivoDAO {
    
    private Vector Unicos ;
    private Vector Repetidos;
    
    /** Creates a new instance of ReporteRetroactivoDAO */
    public ReporteRetroactivoDAO() {
    }
    
    
   
    private final String SQL_INICIAL = 
            "DELETE FROM tem.reporte_retroactivos;" +
            "INSERT INTO tem.reporte_retroactivos (" +
            "    SELECT COALESCE(nit.nombre, '') AS nomprop, " +
            "            c1.*," + 
            "            COALESCE(ruta, '') AS ruta," +
            "            COALESCE(valor_retro, 0.0) AS vr_retro," +
            "            case when COALESCE(flt.placa, '')='' then 'DIR' else 'INT' end AS tipo" +
            "    FROM " +
            "    (" +
            "            SELECT  p.nitpro, " +
            "                    p.plaveh, " +
            "                    p.numpla, " +
            "                    p.fecpla, " +
            "                    r.pesoreal, " +
            "                    r.std_job_no" +
            "            FROM " +
            "                    planilla p, " +
            "                    plarem pr, " +
            "                    remesa r, " +
            "                    tablagen tblgen " + 
            "            WHERE p.numpla = pr.numpla AND pr.numrem = r.numrem" + 
            "            AND (" +
            "                    tblgen.table_code = p.base" +
            "                    AND tblgen.table_type='BASE'" +
            "                    AND tblgen.referencia='CARBON'" +
            "                 )" +
            "            AND p.fecpla BETWEEN ? AND ?" +
            "            AND p.reg_status <> 'A'" +
            "    ) c1 LEFT JOIN nit ON ( c1.nitpro = nit.cedula )" +
            "    LEFT JOIN retroactivos retr ON (retr.std_job_no=c1.std_job_no AND fecha1<='now()' AND fecha2>='now()')" +
            "    LEFT JOIN flota_directa flt ON (c1.plaveh=flt.placa)" +
            "    ORDER BY nomprop" +
            ");"; 

    
    private final String SQL_VR_RETROACTIVO = 
            "SELECT DISTINCT(COALESCE(valor_retro, 0.0)) AS valor_retro " +
            "FROM retroactivos " +
            "WHERE ruta=? " +
            "AND fecha1<='now()' AND fecha2>='now()'";
    
    private final String SQL_CLASIFICADOS_DIR = 
            "SELECT plaveh, " +
            "       nomprop, " +
            "       nitpro, " +
            "       ruta, " +
            "       tipo, " +
            "       sum(pesoreal) AS pesoreal, " +
            "       count(* ) AS viajes " +
            "FROM tem.reporte_retroactivos " +
            "WHERE tipo = 'DIR'" +
            "GROUP BY plaveh, nomprop, nitpro, ruta, tipo " +
            "ORDER BY plaveh";
    
    private final String SQL_CLASIFICADOS_INT = 
            "SELECT plaveh, " +
            "       nomprop, " +
            "       nitpro, " +
            "       ruta, " +
            "       tipo, " +
            "       sum(pesoreal) AS pesoreal, " +
            "       count(* ) AS viajes " +
            "FROM tem.reporte_retroactivos " +
            "WHERE tipo = 'INT'" +
            "GROUP BY plaveh, nomprop, nitpro, ruta, tipo " +
            "ORDER BY plaveh";
    
    private String SQL_UnicosRepetidos = 
            "SELECT *                        "+
            "FROM                            "+
            "       tem.reporte_retroactivos ";
    
    /**
     * Realiza la consulta del reporte de retroactivos y los almacena en una
     * la tabala 'reportes_retroactivos' en ele schema 'tem'.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param fechaI Fecha inicial del reporte
     * @param fechaF Fecha final del reporte
     * @version 1.0
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    public void reporteRetroactivoStart(String fechaI, String fechaF)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_INICIAL);
                st.setString(1,fechaI);
                st.setString(2,fechaF);
                //////System.out.println(st);
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DE LA TABLA DEL REPORTE RETROACTIVOS " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    
    /**
     * Obtiene el valor del retroactivo de la ruta determinada
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param ruta Ruta del estandar
     * @version 1.0
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    public float obtenerValorRetroactivo(String ruta)throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_VR_RETROACTIVO);
                st.setString(1, ruta);                
                
                rs = st.executeQuery();
                
                if( rs.next() ){
                    return rs.getFloat("valor_retro");
                }                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL VALOR DE RETROACTIVO " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
        return 0;
        
    }
    
    /**
     * Realiza la consulta de la obtenci�n de los viajes 'directos'
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @version 1.0
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    public Vector obtenerClasificadosDirectos()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector clasif = new Vector();
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_CLASIFICADOS_DIR);             
                
                rs = st.executeQuery();
                
                while( rs.next() ){
                    ReporteRetroactivo ln = new ReporteRetroactivo();
                                        
                    ln.setPlaca( rs.getString("plaveh") );
                    ln.setNit(rs.getString("nitpro") );
                    ln.setPropietario(rs.getString("nomprop") );
                    ln.setRuta(rs.getString("ruta") );
                    ln.setTipo(rs.getString("tipo") );
                    ln.setToneladas(rs.getDouble("pesoreal"));
                    ln.setViajes(rs.getInt("viajes"));
                    
                    clasif.add(ln);
                }                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA GENERACION DE LOS CLASIFICADOS " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
        return clasif;
                
    }
    
    /**
     * Realiza la consulta de la obtenci�n de los viajes 'intermediarios'
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @version 1.0
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    public Vector obtenerClasificadosIntermediarios()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector clasif = new Vector();
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_CLASIFICADOS_INT);             
                
                rs = st.executeQuery();
                
                while( rs.next() ){
                    ReporteRetroactivo ln = new ReporteRetroactivo();
                                        
                    ln.setPlaca( rs.getString("plaveh") );
                    ln.setNit(rs.getString("nitpro") );
                    ln.setPropietario(rs.getString("nomprop") );
                    ln.setRuta(rs.getString("ruta") );
                    ln.setTipo(rs.getString("tipo") );
                    ln.setToneladas(rs.getDouble("pesoreal"));
                    ln.setViajes(rs.getInt("viajes"));
                    
                    clasif.add(ln);
                }                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA GENERACION DE LOS CLASIFICADOS " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
        return clasif;
                
    }
    
    /**
     * Realiza la consulta de la obtenci�n de los viajes unicos y repetidos
     * @autor Ing. Leonardo Parodi
     * @version 1.0
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    public void reporteRetroactivoUnicosRepetidos()throws SQLException{
        this.Unicos = new Vector();
        this.Repetidos = new Vector();
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_UnicosRepetidos);
                //////System.out.println(st);
                rs = st.executeQuery();
                //////System.out.println("Se ejecuto la consulta");
                Vector temp = new Vector();
                ReporteRetroactivo report = new ReporteRetroactivo();
                ReporteRetroactivo repret = new ReporteRetroactivo();
                
                while (rs.next()){
                    //////System.out.println("---------------ingresando al vector temporal");
                    repret = report.load(rs);
                    temp.add(repret);
                }
                this.UnicosRepetidos(temp);
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE DATOS EN LOS VECTORES " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    
    /**
     * Divide los viajes UNICOS y REPETIDOS
     * @autor Ing. Leonardo Parodi
     * @version 1.0
     * @throws SQLException Si se presenta un error con la Base de Datos
     */
    private Vector UnicosRepetidos(Vector temp){
        //////System.out.println("vamos a dividir los datos en Unicos y Repetidos");
        Vector vec = new Vector();
        String placa;
        String placaux;
        int cont=0;
        ReporteRetroactivo rep = new ReporteRetroactivo();
        ReporteRetroactivo reptemp = new ReporteRetroactivo();
        for (int i=0; i<temp.size(); i++) {
            rep = (ReporteRetroactivo)temp.get(i);
            placa = rep.getPlaca();
            //////System.out.println("placa = "+placa);
            cont = 0;
            for (int j=0; j<temp.size();j++) {
                reptemp = (ReporteRetroactivo)temp.get(j);
                placaux = reptemp.getPlaca();
                if (i!=j) {
                    //////System.out.println("placa="+placa+"     placaux="+placaux);
                    if (placa.equals(placaux)) cont++;
                }
            }
            //////System.out.println("------cont = "+cont);
            if ( cont == 0 ){
                this.Unicos.add(rep);
                //////System.out.println("Agregando a unicos");
            }else{
                this.Repetidos.add(rep);
                //////System.out.println("Agregando a Repetidos");
            }
        }
        
        return vec;
    }
    
    /**
     * Retorna el valor de la propiedad Unicos
     * @autor Ing. Leonardo Parodi
     * @version 1.0
     */
    public Vector getUnicos(){
        return this.Unicos;
    }
    
    /**
     * Retorna el valor de la propiedad Repetidos
     * @autor Ing. Leonardo Parodi
     * @version 1.0
     */
    public Vector getRepetidos(){
        return this.Repetidos;
    }
}
