/********************************************************************
 *      Nombre Clase.................   DemorasDAO.java    
 *      Descripci�n..................   DAO de la tabla demora
 *       Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   05.08.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;


public class DemorasDAO extends MainDAO {
    
    
    
    private final String SQL_Planilla_Manual = "select * from despacho_manual where numpla=? and reg_status <> 'A'";
    private final String SQL_INSERT = "insert into demora(numpla,fecdem,fecfindem,sitiodem,duracion,observacion,creation_user,creation_date,cod_cdemora,cod_cjerarquia,dstrct,base)" + 
        " values(?,?,?,?,?,?,?,now(),?,?,?,?)";
    private final String SQL_FINALIZAR_DEMORA = "update demora set usu_finalizacion=?," + 
        "finalizada='si', fecfin='now()', user_update=?, last_update='now()'" + 
        " where numpla=?";
    private final String SQL_OBTENER_DEMORA = "select * from demora where numpla=? order by fecdem desc";
    private final String SQL_OBTENER_DEMORA_ID = "select get_destablagen(cod_cjerarquia, 'CD') AS jerarquia, * from demora where id=?";
    private final String SQL_Planilla = "select * from planilla where numpla=? and reg_status <> 'A'";
    private final String SQL_LASTPLANILLA_VEH = "select numpla from planilla where plaveh=?" + 
        " order by creation_date desc limit 1";
    private final String SQL_VEHICULOS_CARAVANA = "select numpla from caravana where numcaravana=?";
    private final String SQL_CARAVANA = "select * from caravana where numcaravana=?";
    private final String SQL_PLANILLA_EN_INGRESO_TRAFICO = "select * from ingreso_trafico where planilla=?";
    private final String SQL_EXISTE_DEMORA = "select * from demora where numpla=? and fecdem=? and duracion=?";
    
    private final String SQL_TRAFICO_UPDATE = "update trafico set zona = ? where planilla = ?";
    private final String SQL_LISTAR = "select * from demora where numpla=? and reg_status<>'A' order by fecdem desc";
    /*tito 05-05/2006*/    
    
    private final String SQL_INGRESO_TRAFICO_UPDATE2 = "update ingreso_trafico set ult_observacion = ? where planilla = ? AND reg_status!='D'";
    private final String SQL_INGRESO_TRAFICO_UPDATE = "update ingreso_trafico set demora = demora + ? where planilla = ?";

    
    /** Creates a new instance of DemorasDAO */
    public DemorasDAO() {
        super("DemorasDAO");
    }
    
    
    /**
     * M�todo para agregar una demora al archivo de demoras. 
     * @autor Tito Andr�s Maturana
     * @param demora Instandstrct de la clase Demora.
     * @throws SQLException
     * @version 1.0
     */
    public void agregarDemora(Demora demora)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERT";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, demora.getPlanilla());
                st.setString(2, demora.getFecha_demora());
                st.setString(3, demora.getFecha_fin_demora());
                st.setString(4, demora.getSitio_demora());
                st.setInt(5, demora.getDuracion());
                st.setString(6, demora.getObservacion());
                st.setString(7, demora.getUsuario_creacion());
                st.setString(8, demora.getCodigo_demora());
                st.setString(9, demora.getCodigo_jerarquia());
                st.setString(10, demora.getDistrito());
                st.setString(11, demora.getBase());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA DEMORA " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * M�todo para finalizar una demora dentro del archivo de demoras. 
     * @autor Tito Andr�s Maturana
     * @param demora Instandstrct de la clase Demora.
     * @throws SQLException
     * @version 1.0
     */
    public void finalizarDemora(Demora demora)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_FINALIZAR_DEMORA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, demora.getUsuario_finalizacion());
                st.setString(2, demora.getUsuario_modificacion());
                st.setString(3, demora.getPlanilla());
               // st.setString(4, demora.getFecha_demora());
                ////System.out.println(st);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA FINALIZACION DE LA DEMORA " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Verifica la existendstrct de una planilla en el archivo planilla. 
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @throws SQLException
     * @return true si existe o false sino.
     * @version 1.0
     */
    public boolean existePlanilla(String numpla)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_Planilla";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, numpla);                                
                rs = st.executeQuery();
                if(rs.next()) existe = true;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE VERIFICACION DE LA EXISTENdstrct DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;
    }
    
    /**
     * Verifica la existendstrct de una caravana en el archivo de caravanas. 
     * @autor Tito Andr�s Maturana
     * @param numcaravana N�mero de la caravana.
     * @return true si existe o false sino.
     * @throws SQLException
     * @version 1.0
     */
    public boolean existeCaravana(int numcaravana)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_CARAVANA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, numcaravana);                                
                rs = st.executeQuery();
                if(rs.next()) existe = true;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA AL ARCHIVO DE CARAVANAS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        return existe;
    }
    
    /**
     * Obtiene la ultima planilla de un veh�culo. 
     * @autor Tito Andr�s Maturana
     * @param numcaravana N�mero de la caravana.
     * @return N�mero de la �ltima planilla
     * @throws SQLException
     * @version 1.0
     */
    public String ultimaPlanilla(String plaveh)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String numpla = "";
        String query = "SQL_LASTPLANILLA_VEH";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, plaveh);                                
                rs = st.executeQuery();
                if(rs.next()){ 
                   numpla = rs.getString("numpla");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        return numpla;
    }
    
    /**
     * Obtiene los numeros de las planillas de los veh�vulos
     * que conforman una caravana. 
     * @autor Tito Andr�s Maturana
     * @param numcaravana N�mero de la caravana.
     * @return Vector con los numeros de planilla que conforman la caravana
     * @throws SQLException
     * @version 1.0
     */
    public Vector vechiculosCaravana(int numcaravana)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector planillas = new Vector();
        String query = "SQL_VEHICULOS_CARAVANA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, numcaravana);                                
                rs = st.executeQuery();
                
                while(rs.next()){ 
                   String numpla = rs.getString("numpla");
                   planillas.add(numpla);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        return planillas;
    }
    
    
    /**
     * Obtiene una demora del archivo de demoras
     * que conforman una caravana. 
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @return La �ltima demora de una planilla
     * @throws SQLException
     * @version 1.0
     */
    public Demora obtenerDemora(String numpla)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector planillas = new Vector();
        Demora dem = null;
        String query = "SQL_OBTENER_DEMORA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, numpla);                                
                rs = st.executeQuery();
                
                if(rs.next()){ 
                    dem = new Demora();
                    dem.setPlanilla(rs.getString("numpla"));
                    dem.setFecha_demora(rs.getString("fecdem"));
                    dem.setSitio_demora(rs.getString("sitiodem"));
                    dem.setDuracion(Integer.valueOf(String.valueOf(rs.getInt("duracion"))));
                    dem.setObservacion(rs.getString("observacion"));
                    dem.setUsuario_finalizacion(rs.getString("usu_finalizacion"));
                    dem.setFinalizada(rs.getString("finalizada"));
                    dem.setFecha_finalizada(rs.getString("fecfin"));
                    dem.setCodigo_demora(rs.getString("cod_cdemora"));
                    dem.setCodigo_jerarquia(rs.getString("cod_cjerarquia"));
                    dem.setUsuario_creacion(rs.getString("creation_user"));
                    dem.setFecha_creacion(rs.getString("creation_date"));
                    dem.setUsuario_modificacion(rs.getString("user_update"));
                    dem.setUltima_modificacion(rs.getString("last_update"));
                    dem.setDistrito(rs.getString("dstrct"));
                    dem.setEstado(rs.getString("reg_status"));
                    dem.setBase(rs.getString("base"));
                    dem.setId(Integer.valueOf(String.valueOf(rs.getInt("id"))));
                    dem.setFecha_fin_demora(rs.getString("fecfindem"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return dem;
    }
    
    /**
     * Verifica que la planilla se encuentre registrada en la tabla
     * ingreso_trafico
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @throws SQLException
     * @return true si se encuentra false sino.
     */
    public boolean planillaEnIngresoTrafico(String numpla)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_PLANILLA_EN_INGRESO_TRAFICO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, numpla);                                
                rs = st.executeQuery();
                
                if(rs.next()){ 
                    return true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR LA CONSULTA DEL NUMERO DE LA PLANILLA EN INGRESO_TRAFICO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return false;
    }
    
    /**
     * Verifica la existendstrct de una demora.
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @param fecdem Fecha de la demora.
     * @param duracion Duraci�n de la demora.
     * @throws SQLException
     * @return true si existe, false sino.
     */
    public boolean existeDemora(String numpla, String fecdem, int duracion)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE_DEMORA";//JJCastro fase2
        boolean existe = false;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, numpla);   
                st.setString(2, fecdem);
                st.setInt(3, duracion);
                rs = st.executeQuery();
                
                if(rs.next()){ 
                    existe = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR LA CONSULTA DEL NUMERO DE LA PLANILLA EN INGRESO_TRAFICO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;
    }
    
    /**
     * Actualiza el campo duracion en la tabla ingreso_trafico en la
     * planilla correspondiente.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla N�mero de la planilla.
     * @param duracion Duracion de la demora.
     * @param itraf Registro del archivo ingreso_trafico
     * @throws SQLException
     */
    public void actualizarIngresoTrafico(String numpla, Integer duracion)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INGRESO_TRAFICO_UPDATE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, duracion.intValue());                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR ACTUALIZANDO INGRESO_TRAFICO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * Obtiene un vector con las demoras de una planilla
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @return Arreglo de demoras.
     * @throws SQLException
     * @version 1.0
     */
    public Vector listarDemorasPlanilla(String numpla)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector demoras = new Vector();
        Demora dem = null;
        String query = "SQL_LISTAR";//JJCastro fase2
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, numpla);                                
                rs = st.executeQuery();
                
                while(rs.next()){ 
                    dem = new Demora();
                    dem.setPlanilla(rs.getString("numpla"));
                    dem.setFecha_demora(rs.getString("fecdem"));
                    dem.setSitio_demora(rs.getString("sitiodem"));
                    dem.setDuracion(Integer.valueOf(String.valueOf(rs.getInt("duracion"))));
                    dem.setObservacion(rs.getString("observacion"));
                    dem.setUsuario_finalizacion(rs.getString("usu_finalizacion"));
                    dem.setFinalizada(rs.getString("finalizada"));
                    dem.setFecha_finalizada(rs.getString("fecfin"));
                    dem.setCodigo_demora(rs.getString("cod_cdemora"));
                    dem.setCodigo_jerarquia(rs.getString("cod_cjerarquia"));
                    dem.setUsuario_creacion(rs.getString("creation_user"));
                    dem.setFecha_creacion(rs.getString("creation_date"));
                    dem.setUsuario_modificacion(rs.getString("user_update"));
                    dem.setUltima_modificacion(rs.getString("last_update"));
                    dem.setDistrito(rs.getString("dstrct"));
                    dem.setEstado(rs.getString("reg_status"));
                    dem.setBase(rs.getString("base"));
                    dem.setId(Integer.valueOf(String.valueOf(rs.getInt("id"))));
                    dem.setFecha_fin_demora(rs.getString("fecfindem"));
                    
                    demoras.add(dem);
                    dem = null;//Liberar Espacio JJCastro
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        return demoras;
    }
    
    /**
     * Obtiene una demora del archivo de demoras por el codigo
     * @autor Tito Andr�s Maturana
     * @param id C�digo de la demora.
     * @return La �ltima demora de una planilla
     * @throws SQLException
     * @version 1.0
     */
    public Demora obtenerDemoraID(String id) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector planillas = new Vector();
        Demora dem = null;
        String query = "SQL_OBTENER_DEMORA_ID";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, id);                                
                rs = st.executeQuery();
                
                if(rs.next()){ 
                    dem = new Demora();
                    dem.setPlanilla(rs.getString("numpla"));
                    dem.setFecha_demora(rs.getString("fecdem"));
                    dem.setSitio_demora(rs.getString("sitiodem"));
                    dem.setDuracion(Integer.valueOf(String.valueOf(rs.getInt("duracion"))));
                    dem.setObservacion(rs.getString("observacion"));
                    dem.setUsuario_finalizacion(rs.getString("usu_finalizacion"));
                    dem.setFinalizada(rs.getString("finalizada"));
                    dem.setFecha_finalizada(rs.getString("fecfin"));
                    dem.setCodigo_demora(rs.getString("cod_cdemora"));
                    dem.setCodigo_jerarquia(rs.getString("jerarquia"));
                    dem.setUsuario_creacion(rs.getString("creation_user"));
                    dem.setFecha_creacion(rs.getString("creation_date"));
                    dem.setUsuario_modificacion(rs.getString("user_update"));
                    dem.setUltima_modificacion(rs.getString("last_update"));
                    dem.setDistrito(rs.getString("dstrct"));
                    dem.setEstado(rs.getString("reg_status"));
                    dem.setBase(rs.getString("base"));
                    dem.setId(Integer.valueOf(String.valueOf(rs.getInt("id"))));
                    dem.setFecha_fin_demora(rs.getString("fecfindem"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL PERFIL " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return dem;
    }
    
    /**
     * M�todo para agregar una demora al archivo de demoras. 
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param demora Instandstrct de la clase Demora.
     * @param sw Devolver cadena
     * @throws SQLException
     * @version 1.0
     */
    public String agregarDemora(Demora demora, boolean sw)throws SQLException{
        Connection con=null;
        StringStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERT";//JJCastro fase2
        String sql = "";//JJCastro fase2

        try {
                st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                st.setString(1, demora.getPlanilla());
                st.setString(2, demora.getFecha_demora());
                st.setString(3, demora.getFecha_fin_demora());
                st.setString(4, demora.getSitio_demora());
                st.setInt(5, demora.getDuracion());
                st.setString(6, demora.getObservacion());
                st.setString(7, demora.getUsuario_creacion());
                st.setString(8, demora.getCodigo_demora());
                st.setString(9, demora.getCodigo_jerarquia());
                st.setString(10, demora.getDistrito());
                st.setString(11, demora.getBase());
                
                sql =  st.getSql();//JJCastro fase2
            
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA DEMORA " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
         }        
        return sql;
    }
    
    /**
     * Actualiza el campo duracion en la tabla ingreso_trafico en la
     * planilla correspondiente.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla N�mero de la planilla.
     * @param duracion Duracion de la demora.
     * @param itraf Registro del archivo ingreso_trafico
     * @throws SQLException
     */
    public String actualizarIngresoTrafico(String numpla, Integer duracion, boolean SW)throws SQLException{
        Connection con=null;
        StringStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INGRESO_TRAFICO_UPDATE";//JJCastro fase2
        String sql = "";//JJCastro fase2
        try {
                st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                st.setInt(1, duracion.intValue());
                st.setString(2, numpla);                
                sql =  st.getSql();//JJCastro fase2
            
        }catch(Exception e){
            throw new SQLException("ERROR ACTUALIZANDO INGRESO_TRAFICO " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
         }        
        return sql;
    }
     /**
     * Actualiza el campo de observacion en la tabla ingreso_trafico en la
     * planilla correspondiente siempre y cuando tenda el reg_status iguala a "D".
     * @autor Ing. Andr�s Maturana De La Cruz.
     * @param numpla N�mero de la planilla.
     * @param observacion Observacion de la demora.
     * @param itraf Registro del archivo ingreso_trafico
     * @throws SQLException
     */
    public String actualizarIngresoTrafico(String numpla, String observacion)throws SQLException{
        Connection con=null;
        StringStatement st = null;       
        String query = "SQL_INGRESO_TRAFICO_UPDATE2";//JJCastro fase2
        String sql = "";//JJCastro fase2
        
        try {
                st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                st.setString(1, observacion);
                st.setString(2, numpla);
                sql = st.getSql();//JJCastro fase2

        }catch(Exception e){
            throw new SQLException("ERROR ACTUALIZANDO INGRESO_TRAFICO " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
         }
        
        return sql;
    }
    /**
     * Verifica la existendstrct de una planilla en el archivo despacho_manual. 
     * @autor Tito Andr�s Maturana
     * @param numpla N�mero de la planilla.
     * @throws SQLException
     * @return true si existe o false sino.
     * @version 1.0
     */
    public boolean existeDespachoManual(String numpla)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_Planilla_Manual";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, numpla);                                
                rs = st.executeQuery();
                //////System.out.println("wasnull: " + !rs.wasNull());
                if(rs.next()) existe = true;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE VERIFICACION DE LA EXISTENdstrct DE LA PLANILLA " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;
    }
}
