/*
* Nombre        ConciliacionDAO.java
* Descripci�n   Clase que maneja las consultas para la conciliacion de placas 
* Autor         Sandra Escalante
* Fecha         5 de abril de 2006, 09:41 AM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/



package com.tsp.operation.model.DAOS;
import java.sql.*;
import java.io.*;
import java.util.*;
/**
 *
 * @Modificado por :   Ivan Dario Gomez Vanegas
 */
public class ConciliacionDAO extends MainDAO {
    
    /** Creates a new instance of ConciliacionDAO */
    public ConciliacionDAO() {
        super("ConciliacionDAO.xml");
    }
    
    
    /**
     * M�todo compararconPlacas, realiza la conciliacion de las placas del archivo contra las 
     *                           que estan en la tabla plainlla
     * @autor       Sescalante
     * @modificado  Ivan Gomez Vanegas
     * @param       la linea leida del archivo csv, y la fecha de corte
     * @throws      Exception
     * @version     1.0.
     **/
    public String compararconPlacas(String line, String fch)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        PreparedStatement st1 = null;
        ResultSet         rs1 = null;
        
        String [] testString = null;
        
        int swp = 0;//0= Placa vacia 1=Placa llena
        
        String resul= "";
        
        ///linea -> vector
        testString = line.split(";");
       
        
        //imprimiendo placas
        String placa = "";
        if(!testString[0].equals("")){
            if (!testString[5].equalsIgnoreCase("")){
                //obtengo placa de archivo
                placa =  testString[5];
                swp = 1;
            }
        }
        
        try{
            String query = "SQL_PLACA";//JJCastro fase2
            ///sincronizacion con placas en bd
            if ( swp == 1 ){
                
                con = this.conectarJNDI(query);
                if (con != null) {
                    st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                    st.setString(1, placa);
                    rs = st.executeQuery();
                    
                    //si la placa esta en la bd
                    if(rs.next()){                        
                        //buscar placa en la planilla
                            st1 = con.prepareStatement(this.obtenerSQL("SQL_PLANILLA"));//JJCastro fase2
                            st1.setString(1, fch + "%");
                            st1.setString(2, placa);
                            rs1 = st1.executeQuery();
                            
                            if (!rs1.next()){
                                resul = " -> " + placa + " \n";
                                //////System.out.println(" NO ESTA EN LA PLANILLA: " + placa) ;
                            }
                            else{                                
                                resul = "vacio";
                                //////System.out.println(" SI ESTA EN LA PLANILLA: " + placa) ;
                            }
                    }
            }
        }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONCILIACION DE LAS PLACAS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (rs1  != null){ try{ rs1.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st1  != null){ try{ st1.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resul;
    } 
   
     /**
     * M�todo enviarCorreo, realiza el proceso de envio de correo
     * @autor       Sescalante
     * @modificado  Ivan Gomez Vanegas
     * @param       un strin con las placas, la fecha, y la ruta
     * @throws      Exception
     * @version     1.0.
     **/
    
     public void enviarCorreo(String placas, String fecha, String ruta)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SENDMAIL";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, "masivo@mail.tsp.com");
                st.setString(2, "kreales@mail.tsp.com");
                st.setString(3, "Masivo Informe de Conciliacion");
                st.setString(4, "La conciliacion del Archivo " + ruta + "\n en la fecha " + fecha + " arrojo como resultado: \n Las siguientes placas no se encuentran en las planillas asociadas segun los datos ingresados: \n " + placas);
                st.setString(5, "- Transporte Masivo -");
                st.executeUpdate();
        }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL CORREO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
}
