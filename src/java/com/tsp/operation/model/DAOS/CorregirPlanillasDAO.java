/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.AnticiposTerceros;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.CorregirPlanillaBeans;
import com.tsp.operation.model.beans.ExtractoDetalleBeans;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author hcuello
 */
public interface CorregirPlanillasDAO {
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<CorregirPlanillaBeans> buscarPlanilla() throws SQLException;
    
    /**
     *
     * @param numeroOperacion
     * @param clase
     * @param documento
     * @return
     * @throws SQLException
     */
    public ArrayList<ExtractoDetalleBeans> buscarExtractoDetalle(int numeroOperacion, String documento, String clase) throws SQLException;
    
    /**
     *
     * @param extractoDetalleBeans
     * @param clase
     * @param user
     * @return
     * @throws SQLException
     */
    public String actualizarExtractoDetalle(ExtractoDetalleBeans extractoDetalleBeans, String clase, String user)throws SQLException;
    
     /**
     *
     * @return
     * @throws SQLException
     */
    public String buscarDiferenciaTablas() throws SQLException;
    
     /**
     *
     * @param secuencia
     * @return
     * @throws SQLException
     */
    public String buscarAnticipos(int secuencia) throws SQLException;
    
    /**
     *
     * @param id
     * @param valor
     * @param valor_for
     * @param user
     * @return
     * @throws SQLException
     */
    public String actualizarAnticipo(String id,double valor,double valor_for, String user)throws SQLException;
    
    /**
     *
     * @param secuencia
     * @param valor
     * @param user
     * @return
     * @throws SQLException
     */
    public String actualizarExtracto(int secuencia,double valor,String user)throws SQLException;

    public ArrayList<AnticiposTerceros> buscarPlanillasAnticipo(String user, String fecha) throws SQLException;

    public ArrayList<AnticiposTerceros> detallePlanillaAnticipo(String user, String fecha_transf) throws SQLException;

    public String reversarPlanillasAnticipo(String user, String fecha, String login) throws SQLException;
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public String cargarComboUsuarios() throws SQLException;

}
