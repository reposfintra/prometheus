/*************************************************************
 * Nombre      ............... SoporteClientesDAO.java
 * Descripcion ............... Clase de manipulacion soportes
 *                             asignados a los clientes.
 * Autor       ............... mfontalvo
 * Fecha       ............... Diciembre - 12 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;

import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;


public class SoporteClientesDAO extends MainDAO {

    // Declaracion de Tipos de Consultas
    public static final int SELECT_SOPORTE    = 1;
    public static final int SELECT_CLIENTES   = 2;
    public static final int SELECT_RELACION   = 3;
    public static final int SELECT_AGENCIAS   = 4;
       
    private final String SQL_INSERT_RELACION = " insert into soporte_clientes ( reg_status, dstrct, soporte, cliente, creation_user, creation_date, user_update, last_update ) values (?,?,?,?,?,?,?,?) ";        
    private final String SQL_DELETE_RELACION = " delete from soporte_clientes where dstrct = ? and cliente = ? ";
    
    
    
    
    /** Creates a new instance of SoporteClientesDAO */
    public SoporteClientesDAO() {
        super("SoporteClientesDAO.xml");
    }
    
    /**
     * Funcion Que busca que ejecuta una consulta [SQL_SOPORTE, SQL_RELACION, SQL_CLIENTES, SQL_AGENCIAS]
     * @autor  mfontalvo
     * @params Tipo ........ Tipo de Consulta a ejecutar.
     * @params args ........ Parametros para setear la consulta
     * @return Resultado ... Listado de [SOPORTES, CLIENTES, RELACION-SOP.CLI, AGENCIAS]
     * @throws Exception.
     **/
    
  
    public List EXECUTE_QUERY(int Tipo, String []args) throws SQLException {
        Connection        con         = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              Resultado   = new LinkedList();
        String            SQL         = "";
        try {
            SQL = (Tipo == SELECT_SOPORTE  ? "SQL_SOPORTES"     : 
                   Tipo == SELECT_RELACION ? "SQL_RELACIONES"   :
                   Tipo == SELECT_CLIENTES ? "SQL_CLIENTES"     : 
                   Tipo == SELECT_AGENCIAS ? "SQL_AGENCIAS"     : "" );
                          
            if (!SQL.equals("")){
                con = this.conectarJNDI(SQL);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(SQL));//JJCastro fase2

                if (args!=null){
                    for (int i=0;args!=null && i<args.length;i++)
                        st.setString((i+1),  (args[i].equals("ALL")?"%":args[i]));
                }
                rs = st.executeQuery();
                while (rs.next()){
                    Object datos = null;
                    datos = General.load(rs);
                    Resultado.add(datos);
                }
            }
        }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina EXECUTE_QUERY [SoporteClientesDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Resultado;
    }
    
    
    
    /**
     * Procedimiento para adicionar una relacion entre soportes y clientes
     * @autor  mfontalvo
     * @params Soportes[]  ........ Listado de Soportes a relacionar
     * @params Clientes[] ......... Lista de Clientes a relacionar
     * @params Usuario ............ Usuario quien realiza la operacion
     * @throws Exception.
     **/
    
    
    public void addRelacionSoporteCliente ( String [] Soportes, String [] Clientes, String Usuario) throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_DELETE_RELACION";
        String query2 = "SQL_INSERT_RELACION" ;

        try {
            
            con =this.conectarJNDI(query);
            if(con!=null){
            // eliminacion de las relaciones existentes con los soportes
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for (int i=0; i<Clientes.length;i++){
                st.clearParameters();
                st.setString(1, "FINV");
                st.setString(2, Clientes[i]);
                st.executeUpdate();
            }
            
            
            // adicion de los clientes a los soportes
            st = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
            for (int i=0; i<Soportes.length;i++){
                for (int j=0 ; j < Clientes.length ; j++){
                    st.clearParameters();
                    st.setString(1, "");
                    st.setString(2, "FINV");
                    st.setString(3, Soportes[i]);
                    st.setString(4, Clientes[j]);
                    st.setString(5, Usuario);
                    st.setString(6, Util.getFechaActual_String(6));
                    st.setString(7, Usuario);
                    st.setString(8, Util.getFechaActual_String(6));
                    st.executeUpdate();
                }
            }
            
        }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina EXECUTE_QUERY [SoporteClientesDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    
    
    
    
    
    
    
    ////////////////////////////////////////////////////////////
    // METODOS DE ACTUALIZACION DE SOPORTES DE REMESAS
    ////////////////////////////////////////////////////////////
    
    
    

    /**
     * Metodo,que obtiene el listado de documentos soporte de una remesa
     * @autor mfontalvo
     * @param 2006-01-17
     * @param Remesa, filtro a consultar
     * @param Estado, filtro a consultar
     * @param Tipo, fisicos o logicos
     * @throws Exception.
     * @return List, Listado tipo de documentos soportes
     * @version : 1.0
     */
    private Vector obtenerSoportesRemesa(String restriccionC, String restriccionI)throws Exception{
        
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Vector            soportes = new Vector();

        String queryI = "SQL_SOPORTES_FALTANTES";
        String queryC = "SQL_BUSCAR_SOPORTES_REMESA";
        try {
            
            con = this.conectarJNDI(queryC);//JJCastro fase2
            
            if (con!=null){

            String SQLInsert = this.obtenerSQL(queryI);
            SQLInsert = SQLInsert.replaceAll("#OTROS_FILTROS#", restriccionI);
            st = con.prepareStatement(SQLInsert);
            st.executeUpdate();
            
            String SQLQuery  = this.obtenerSQL(queryC);
            SQLQuery = SQLQuery.replaceAll("#OTROS_FILTROS#", restriccionC);            
            st = con.prepareStatement(SQLQuery + " ORDER BY c.numrem, c.soporte " );
            
            rs = st.executeQuery();
            while (rs.next()){
                Cumplido c = new Cumplido();
                c.setDistrict             ( rs.getString("dstrct"));
                c.setRemesa               ( rs.getString("numrem"));
                c.setSoporte              ( rs.getString("soporte"));
                c.setComent               ( rs.getString("descripcion"));
                c.setAgencia_envio        ( rs.getString("agencia_envio"));
                c.setEstado               ( rs.getString("nombre_agencia"));
                c.setFecha_envio          ( rs.getString("fecha_envio"));
                c.setFecha_recibido       ( rs.getString("fecha_recibido"));
                c.setFecha_envio_logico   ( rs.getString("fecha_envio_logico") );
                c.setFecha_recibido_logico( rs.getString("fecha_recibido_logico"));
                soportes.add(c);
            }
            
            }}catch(Exception e){
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return soportes;
    }


 /**
  * 
  * @param tipo
  * @param clase
  * @return
  * @throws Exception
  */
    private String obtenerRestriccionBase (String tipo, String clase) throws Exception {
        String base = "";
        try {
   
            // tipo de envios
            if (clase.equalsIgnoreCase("FISICA")){
                if (tipo.equalsIgnoreCase("ENVIO"))
                    base += " and c.fecha_envio = '0099-01-01 00:00:00' ";
                else if (tipo.equalsIgnoreCase("RECIBIDO"))
                    base += " and c.fecha_envio != '0099-01-01 00:00:00' and c.fecha_recibido = '0099-01-01 00:00:00' ";
            }else if(clase.equalsIgnoreCase("LOGICA")){
                if (tipo.equalsIgnoreCase("ENVIO"))
                    base += " and c.fecha_envio_logico  = '0099-01-01 00:00:00' ";
                else if (tipo.equalsIgnoreCase("RECIBIDO"))
                    base += " and c.fecha_envio_logico != '0099-01-01 00:00:00' and c.fecha_recibido_logico = '0099-01-01 00:00:00' ";
            }
            else if(clase.equalsIgnoreCase("TODAS")){
                if (tipo.equalsIgnoreCase("ENVIO"))
                    base += " and (c.fecha_envio = '0099-01-01 00:00:00' ) ";
                else if (tipo.equalsIgnoreCase("RECIBIDO"))
                    base += " and (c.fecha_envio    != '0099-01-01 00:00:00' ) " +
                            " and (c.fecha_recibido  = '0099-01-01 00:00:00' ) ";
            }        
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        return base;
        
    }


    
    
    public Vector obtenerSorportes_x_Remesa(String remesa , String tipo, String clase) throws Exception{
        try {
            String restriccionC = " AND c.numrem  = '"+ remesa +"' ";
            String restriccionI = " AND a.cod_doc = '"+ remesa +"' ";
            restriccionC += this.obtenerRestriccionBase(tipo, clase);
            return this.obtenerSoportesRemesa(restriccionC, restriccionI);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }
    
    
    public Vector obtenerSorportes_x_Cliente(String cliente, String fi, String ff, String tipo, String clase) throws Exception{
        try {
            String restriccionC = " AND rem.cliente  = '"+ cliente +"' AND cum.creation_date BETWEEN '"+ fi +"' AND '"+ ff +"' ";
            String restriccionI = " AND b.cliente    = '"+ cliente +"' AND a.creation_date   BETWEEN '"+ fi +"' AND '"+ ff +"' ";
            restriccionC += this.obtenerRestriccionBase(tipo, clase);
            return this.obtenerSoportesRemesa(restriccionC, restriccionI);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }    
    
    public Vector obtenerSorportes_x_AgenciaFacturadora(String agencia, String fi, String ff, String tipo, String clase) throws Exception{
        try {
            String restriccionC = " AND cli.agfacturacion  = '"+ agencia +"' AND cum.creation_date BETWEEN '"+ fi +"' AND '"+ ff +"' ";
            String restriccionI = " AND c.agfacturacion    = '"+ agencia +"' AND a.creation_date   BETWEEN '"+ fi +"' AND '"+ ff +"' ";
            restriccionC += this.obtenerRestriccionBase(tipo, clase);
            return this.obtenerSoportesRemesa(restriccionC, restriccionI);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }    
        
    
    
    /**
     * Metodo, que modifica las fechas de envio y recibido tanto
     * fisico como logico y agencia de envio de los soportes de
     * una remesa
     * @fecha 2006-01-18
     * @autor mfontalvo
     * @param Remesa, que se va a actualizar
     * @param Soporte, que se va a actualizar
     * @param editAGE, indica si se puede modificar o no la agencia
     * @param Agencia, nueva agencia de envio
     * @param editFEF, indica si se puede modificar o no la fecha de envio fisico
     * @param fechaEnvioFisico, nueva fecha de envio fisico
     * @param editFRF, indica si se puede modificar o no la fecha de recibido fisico
     * @param fechaRecibidoFisico, nueva fecha de recibido fisico
     * @param editFEL, indica si se puede modificar o no la fecha de envio logico
     * @param fechaEnvioLogico, nueva fecha de envio logico
     * @param editFRL, indica si se puede modificar o no la fecha de recibido logico
     * @param fechaRecibidoLogico, nueva fecha de recibido logico
     * @param Usuario, que registra el envio
     * @throws Exception.
     * @version : 1.0
     */
    
    public void ActualizarFechas(
    String Remesa,
    String Soporte,
    boolean editAGE, String Agencia,
    boolean editFEF, String fechaEnvioFisico,
    boolean editFRF, String fechaRecibidoFisico,
    boolean editFEL, String fechaEnvioLogico,
    boolean editFRL, String fechaRecibidoLogico,
    String Usuario
    )throws Exception{
        
        
        Connection        con = this.conectarBDJNDI("fintra");//JJCastro fase2
        PreparedStatement st  = null;
        
        if (con==null)
            throw new Exception("sin Conexion");
        try {
            String fecha = Util.getFechaActual_String(6);
            String Query = " UPDATE control_soporte SET #CAMPOS# WHERE numrem = ? and soporte = ? " ;
            
            String campos = "";
            
            // agencia envio
            if (editAGE && !Agencia.equals("")){
                campos += " agencia_envio = '"+ Agencia +"' ,";
            }
            
            // fecha envio fisico
            if (editFEF && !fechaEnvioFisico.equals("")){
                campos +=
                " fecha_envio            = '"+ fechaEnvioFisico +"' ," +
                " usuario_registro_envio = '"+ Usuario          +"' ," +
                " fecha_registro_envio   = '"+ fecha            +"' ,";
            }
            
            // fecha recibido fisico
            if (editFRF && !fechaRecibidoFisico.equals("")){
                campos +=
                " fecha_recibido            = '"+ fechaRecibidoFisico +"' ," +
                " usuario_registro_recibido = '"+ Usuario             +"' ," +
                " fecha_registro_recibido   = '"+ fecha               +"' ," ;
            }
            
            // fecha envio logico
            if (editFEL && !fechaEnvioLogico.equals("")){
                campos +=
                " fecha_envio_logico            = '"+ fechaEnvioLogico +"' ," +
                " usuario_registro_envio_logico = '"+ Usuario          +"' ," +
                " fecha_registro_envio_logico   = '"+ fecha            +"' ," ;
            }
            
            // fecha recibido logico
            if (editFRL && !fechaRecibidoLogico.equals("")){
                campos +=
                " fecha_recibido_logico            = '"+ fechaRecibidoLogico +"' ," +
                " usuario_registro_recibido_logico = '"+ Usuario             +"' ," +
                " fecha_registro_recibido_logico   = '"+ fecha               +"' ," ;
            }
            
            
            
            if (!campos.equals("")){
                Query = Query.replaceAll("#CAMPOS#", campos.substring(0,campos.length()-1));
                st = con.prepareStatement(Query);
                st.setString(1, Remesa  );
                st.setString(2, Soporte );
                //////System.out.println("UPDATE : " + st.toString());
                st.executeUpdate();
            }
            
            
        }catch(SQLException e){
            throw new SQLException("Error en ActualizarFechas en CumplidoDAO ... " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
    
    
    /**
     * Metodo para obtener la causa de por que la remesa  no aparece en el 
     * listado de asignacion de fechas
     */
    public String obtenerCausa(String remesa , String tipo, String clase) throws Exception{
        
        Connection        con = null;
        PreparedStatement ps  = null;
        ResultSet         rs  = null;
        String            qry = "SQL_OBTENER_CAUSA";
        String            msg = "";
        try {
            con = this.conectarJNDI(qry);//JJCastro fase2
            if (con == null )
                throw new Exception ("Sin conexion");
            
            ps = con.prepareStatement( this.obtenerSQL(qry) );
            ps.setString(1, remesa );
            rs = ps.executeQuery();
            if ( rs.next() ){
                if ( rs.getString("estado").equals("A") )
                    msg += "La remesa esta ANULADA.";
                else if ( !rs.getString("estado").equals("C") )
                    msg += "La remesa no esta CUMPLIDA.";
                else if ( rs.getString("estado").equals("C") && rs.getString("cumplida").equals("NC")  )
                    msg += "La remesa esta macarda como CUMPLIDA, pero no el cumplido de la misma no existe en la base de datos.";
                else if ( rs.getString("cliente").equals("") )
                    msg += "El cliente registrado para la remesa NO EXISTE.";
                else if ( rs.getString("soportes").equals("") )
                    msg += "El cliente asociado a la remesa NO TIENE SOPORTES relacionados.";
                
            } else
                msg = "No se encontro la remesa.";
            
            
            
            if ( msg.equals("") ){
                
                String estado = "";
                ps = con.prepareStatement(this.obtenerSQL("SQL_ESTADO_SOPORTES") );
                ps.setString(1, remesa );
                rs = ps.executeQuery();
                if (rs.next()){
                    estado = rs.getString("estado");
                }
                if (tipo.equalsIgnoreCase("RECIBIDO") && estado.equalsIgnoreCase("RECIBIDO") )
                    msg += "Los soportes ya fueron recibidos";
                else if ( tipo.equalsIgnoreCase("RECIBIDO") && estado.equalsIgnoreCase("POR ENVIAR") )
                    msg += "Los soportes no han sido enviados";
                else if (tipo.equalsIgnoreCase("ENVIO") && !estado.equalsIgnoreCase("POR ENVIAR") )
                    msg += "Los soportes ya fueron enviados";
            }

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        } finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return msg;
    }    
    
    

    /**
     * Metodo,que obtiene el listado de documentos soporte de una remesa
     * @autor mfontalvo
     * @param Remesa, filtro a consultar    
     * @throws Exception.
     * @return List, Listado tipo de documentos soportes
     * @version : 1.0
     */
    public Vector obtenerSoportesRemesa(String remesa)throws Exception{
        
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Vector            soportes = new Vector();

        String query = "SQL_CONSULTA_SOPORTES_REMESA";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con==null)
                throw new Exception("sin Conexion");

            st = con.prepareStatement( this.obtenerSQL(query)) ;
            st.setString(1, remesa );
            rs = st.executeQuery();
            while (rs.next()){
                Cumplido c = new Cumplido();
                c.setSoporte              ( rs.getString("soporte"));
                c.setComent               ( rs.getString("descripcion"));
                c.setAgencia_envio        ( rs.getString("agencia_envio"));
                c.setEstado               ( rs.getString("nombre_agencia"));
                c.setFecha_envio          ( rs.getString("fecha_envio"));
                c.setUr_envio             ( rs.getString("usuario_registro_envio"));
                c.setFecha_recibido       ( rs.getString("fecha_recibido"));
                c.setUr_recibido          ( rs.getString("usuario_registro_recibido"));                
                c.setFecha_envio_logico   ( rs.getString("fecha_envio_logico") );
                c.setUr_envio_logico      ( rs.getString("usuario_registro_envio_logico"));
                c.setFecha_recibido_logico( rs.getString("fecha_recibido_logico"));
                c.setUr_recibido_logico   ( rs.getString("usuario_registro_recibido_logico"));                
                soportes.add(c);
            }
            
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        } finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return soportes;
    }
        
    
    
    
    
    
    
    
    
    
}



