/********************************************************************
 *      Nombre Clase.................   AplicacionDAO.java    
 *      Descripci�n..................   DAO de la tabla tbldoc    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;

/**
 *
 * @author  Andres
 */
public class AplicacionDAO {
        
        private Aplicacion app;  
        private Vector apps;
        
        private static final String SQL_INSERT = "insert into tblapl(reg_status, dstrct, codigo," +
                " descripcion, last_update, user_update, creation_date, creation_user, base) " +
                "values('',?,?,?,'now()',?,'now()',?,?)";
        
        private static final String SQL_UPDATE = "update tblapl set reg_status=?," +
                " descripcion=?, last_update='now()', user_update=?" +
                " where dstrct=? and codigo=?";
        
        private static final String SQL_ANULAR = "update tblapl set reg_status='A' " +
                " where dstrct=? and codigo=?";
        
        private static final String SQL_OBTENER = "select * from tblapl where dstrct=? and codigo=?";
        
        private static final String SQL_LISTAR = "select * from tblapl where reg_status<>'A' order by codigo";
        
        private static final String SQL_LISTAR_CIA = "select * from tblapl where reg_status<>'A' and dstrct=? order by codigo";
              
        
        /** Creates a new instance of DocumentoDAO */
        public AplicacionDAO() {
        }
        
        /**
         * Inserta un nuevo registro.
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public void insertar() throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_INSERT);
                                st.setString(1, app.getDistrito());
                                st.setString(2, app.getC_codigo().toUpperCase());
                                st.setString(3, app.getC_descripcion());
                                st.setString(4, app.getUsuario_creacion());
                                st.setString(5, app.getUsuario_creacion());
                                st.setString(6, app.getBase());

                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL INSERTAR LA APLICACION: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }      
        }
        
        /**
         * Actualiza un registro
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public void actualizar() throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_UPDATE);
                                                                
                                st.setString(1, app.getEstado());
                                st.setString(2, app.getC_descripcion());
                                st.setString(3, app.getUsuario_modificacion());
                                st.setString(4, app.getDistrito());
                                st.setString(5, app.getC_codigo().toUpperCase());

                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL ACTUALIZAR LA APLICACION: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }      
        }
        
        /**
         * Anula un registro. Establece el reg_status en 'A'.
         * @autor Tito Andr�s Maturana
         * @param cia Distrito
         * @param codigo Codigo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public void anular(String cia, String codigo) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_ANULAR);
                                                                
                                st.setString(1, cia);
                                st.setString(2, codigo.toUpperCase());                               

                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL ANULAR LA APLICACION: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }      
        }
        
        /**
         * Obtiene un registro
         * @autor Tito Andr�s Maturana
         * @param cia Distrito
         * @param codigo Codigo de la aplicaci�n
         * @throws SQLException
         * @version 1.0
         */
        public void obtener(String cia, String codigo) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
                this.app = null;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_OBTENER);
                                
                                st.setString(1, cia);
                                st.setString(2, codigo.toUpperCase());
                                
                                rs = st.executeQuery();
                                
                                if( rs.next() ){
                                        app = new Aplicacion();
                                        app.Load(rs);
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL OBTENER LA APLICACION: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }                  
                
        }
        
        /**
         * Lista todos los registros que no est�n anulados
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public void listar() throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
                this.app = null;
                this.apps = new Vector();
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_LISTAR);
                                                               
                                rs = st.executeQuery();
                                
                                while( rs.next() ){
                                        app = new Aplicacion();
                                        app.Load(rs);
                                        apps.add(app);
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL LISTAR LAS APLICACIONES: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }                  
                
        }
        
        /**
         * Lista todas las aplicaciones de un distrtito que no esten anuladas.
         * @autor Tito Andr�s Maturana
         * @param cia Distrito
         * @throws SQLException
         * @version 1.0
         */
        public void listar(String distrito) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
                this.app = null;
                this.apps = new Vector();
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_LISTAR_CIA);
                                st.setString(1, distrito);
                                                               
                                rs = st.executeQuery();
                                
                                while( rs.next() ){
                                        app = new Aplicacion();
                                        app.Load(rs);
                                        apps.add(app);
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL LISTAR LOS DOCUMENTOS: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }                  
                
        }
        
        /**
         * Getter for property app.
         * @return Value of property app.
         */
        public com.tsp.operation.model.beans.Aplicacion getApp() {
                return app;
        }        
        
        /**
         * Setter for property app.
         * @param app New value of property app.
         */
        public void setApp(com.tsp.operation.model.beans.Aplicacion app) {
                this.app = app;
        }        
        
        /**
         * Getter for property apps.
         * @return Value of property apps.
         */
        public java.util.Vector getApps() {
                return apps;
        }
        
        /**
         * Setter for property apps.
         * @param apps New value of property apps.
         */
        public void setApps(java.util.Vector apps) {
                this.apps = apps;
        }
        
}
