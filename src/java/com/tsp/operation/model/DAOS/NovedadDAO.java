/*
 * NovedadDAO.java
 *
 * Created on 13 de julio de 2005, 09:00 AM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;


/**
 *
 * @author  Henry
 */
public class NovedadDAO {
          
    public NovedadDAO() {
    }
    
    private Novedad novedad;
    private List novedades;
    private Vector VecNovedad; 
    private static final String SQL_LISTAR_ZONAS    = "SELECT * FROM NOVEDAD WHERE REC_STATUS<>'A' ORDER BY NOMNOVEDAD";
    private static final String SQL_INSERTAR_ZONA   = "INSERT INTO NOVEDAD (codnovedad, nomnovedad, base, creation_user) VALUES(?,?,?,?)";
    private static final String SQL_NOVEDAD_ANULADA = "SELECT * FROM NOVEDAD WHERE CODNOVEDAD=? AND REC_STATUS='A'";
    private static final String SQL_DESANULAR       = "UPDATE NOVEDAD SET REC_STATUS='' WHERE CODNOVEDAD=?";
    private static final String SQL_EXISTE_ZONA     = "SELECT * FROM NOVEDAD WHERE CODNOVEDAD=? AND REC_STATUS<>'A'";
    private static final String SQL_BUSCAR_ZONA     = "SELECT * FROM NOVEDAD WHERE CODNOVEDAD= ?";
    private static final String SQL_ACTUALIZAR_ZONA = "UPDATE NOVEDAD SET nomnovedad=?, user_update = ?, last_update = 'now()' where CODNOVEDAD=?";
    private static final String SQL_ELIMINAR_ZONA   = "UPDATE NOVEDAD SET rec_status = 'A', user_update = ?, last_update = 'now()' where CODNOVEDAD=?";

    public void setNovedad(Novedad nov){        
        this.novedad = nov;        
    }
    
    // insertar novedad en BD
    public void insertarNovedad() throws SQLException {
        Connection con= null;
        PreparedStatement st = null, st2= null;
        ResultSet rs = null, rs2 = null;       
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st2 = con.prepareStatement(SQL_NOVEDAD_ANULADA);
                st2.setString(1, novedad.getCodNovedad());
                rs2 = st2.executeQuery();
                if (rs2.next()){
                    st = con.prepareStatement(SQL_DESANULAR);
                    st.setString(1, novedad.getCodNovedad());
                    st.execute();
                } else {
                    st = con.prepareStatement(SQL_INSERTAR_ZONA);
                    st.setString(1, novedad.getCodNovedad());
                    st.setString(2, novedad.getNomNovedad());   
                    st.setString(3, novedad.getBase());
                    st.setString(4, novedad.getCreation_user());
                    st.executeUpdate();
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL INSERTAR NOVEDAD" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
    }    
   
    public boolean existeNovedad (String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(SQL_EXISTE_ZONA);
                st.setString(1, cod);
                rs = st.executeQuery();
                return rs.next();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        return false;
    }    
    //obtiene la lista de ZONAS
    public List obtenerNovedades (){
        return novedades;
    }
    public Vector obtNovedades (){
        return VecNovedad;
    }   
    public void listarNovedades ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        novedades = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_LISTAR_ZONAS);
                rs = st.executeQuery();
                
                novedades =  new LinkedList();
                
                while (rs.next()){
                    novedades.add(Novedad.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                      throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
        
    }
    public Novedad obtenerNovedad()throws SQLException{
        return novedad;
    }          
    public void novedades ()throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        VecNovedad = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_LISTAR_ZONAS);
                rs = st.executeQuery();
                
                VecNovedad =  new Vector();
                
                while (rs.next()){
                    VecNovedad.add(Novedad.load(rs));
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR DURANTE LA BUSQUEDA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                      throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
        
    }
     // retorna el objeto ZONA de pendiendo el codigo
    public void buscarNovedad (String cod) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        boolean sw = false;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement(SQL_BUSCAR_ZONA);
                st.setString(1, cod);
                rs = st.executeQuery();                
                if (rs.next()){
                   novedad = Novedad.load(rs);                                     
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        
    }
    
    public void buscarNovedadNombre (String nom) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        boolean sw = false;
        VecNovedad = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null ){
                st = con.prepareStatement("select * from novedad where rec_status<>'A' and nomnovedad like ? order by nomnovedad");
                st.setString(1, nom);
                rs = st.executeQuery();                
                VecNovedad =  new Vector();                
                while (rs.next()){
                    VecNovedad.add(Novedad.load(rs));
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR ZONA" + e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if (st != null){
                try{
                    st.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage());
                }
                if ( con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
        }
        
    }    
    public void modificarNovedad () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ACTUALIZAR_ZONA);
                st.setString(1, novedad.getNomNovedad());
                st.setString(2, novedad.getUser_update());
                st.setString(3, novedad.getCodNovedad());                
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL MODIFICAR ZONA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
        
    }
    public void eliminarNovedad () throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        PoolManager poolManager = null;        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ELIMINAR_ZONA);
                st.setString(1, novedad.getUser_update());
                st.setString(2, novedad.getCodNovedad());   
                st.executeUpdate();
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL ANULAR ZONA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }      
        
    }
}
