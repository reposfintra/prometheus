/**************************************************************************
 * Nombre clase: DescuentoTercmDAO.java                                      *
 * Descripci�n: Clase que maneja los metodos para las consulta de las boletas*
 * Autor: Ing. Ivan DArio Gomez Vanegas                                      *
 * Fecha: Created on 22 de diciembre de 2005, 08:20 AM                       *
 * Versi�n: Java 1.0                                                         *
 * Copyright: Fintravalores S.A. S.A.                                   *
 ***************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger; 
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
/**
 *
 * @author  Ivan DArio Gomez
 */
public class DescuentoTercmDAO extends MainDAO {
    private static Logger logger = Logger.getLogger(DescuentoTercmDAO.class);
    
    /** Creates a new instance of DescuentoTercmDAO */
    public DescuentoTercmDAO() {
        super("DescuentoTercmDAO.xml");
    }
    
     /************************************************************************
     * Metodo   : BuscarValor, retorna una lista con los datos de la planilla.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
     public List   BuscarValor(String OC) throws SQLException {
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        String            valor   = "";
        int               pos   = 0;   
        try {
            
            st = crearPreparedStatement("SQL_OC_VALOR");
            st.setString(1, OC);
            st.setString(2, OC);
            rs= st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                if(rs.next()){
                    DescuentoTercm   tercm    = new DescuentoTercm();
                    tercm.setOC      (OC);
                    tercm.setPlaca(rs.getString(1));
                    pos = rs.getString(2).indexOf("TERCM");
                    if(pos!=-1){
                      pos+=5;  
                    }
                    valor = rs.getString(2).substring(pos,pos+=11); 
                    int valor1 = Integer.parseInt(valor)/100 ;  
                    tercm.setValor(valor1);
                    tercm.setCedula(Integer.parseInt(rs.getString(3)));
                    tercm.setNombre(rs.getString(4));
                    listado.add(tercm);
                }
            }
        } catch(SQLException e) {
            throw new SQLException("Error en rutina BuscarValor  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            desconectar("SQL_OC_VALOR");
        }
        return listado;
    }
     
       /************************************************************************
     * Metodo   : Valor(), retorna una lista con los valores disponibles para una boleta.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : 
     * @version : 1.0
     *************************************************************************/
       public List Valor() throws SQLException {
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              ListValores = null;
        try {
            
            st = crearPreparedStatement("SQL_BOLETA");
            rs= st.executeQuery();
            if(rs!=null){
                ListValores = new LinkedList();
                while(rs.next()){
                    DescuentoTercm   tercm    = new DescuentoTercm();
                    tercm.setValorDisp(rs.getString(1));
                    ListValores.add(tercm);
                }
            }
        } catch(SQLException e) {
            throw new SQLException("Error en rutina Valor  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            desconectar("SQL_BOLETA");
        }
        return ListValores;
    }
     
     /************************************************************************
     * Metodo   : GuardarBoletas, Guarda las boletas creadas, con todos
     *            los param recibidos en la jsp.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : la planilla, placa, cedula, y  el numero de boletas a guardar 
     * @version : 1.0
     *************************************************************************/  
     public void GuardarBoletas(String OC, String Placa, int Conductor, String[]Boletas,int ValorOC,String Usuario) throws SQLException {
        PreparedStatement st      = null;
        int  valor = 0;
        try {
            //EliminarBoletas(OC);
            st = crearPreparedStatement("SQL_INSERT_BOLETA");
            for (int i=0; i<=9; i++) {
                if (!Boletas[i].equals("")){
                     valor +=  Integer.parseInt(Boletas[i]);
                }
            }
            
           if (valor == ValorOC ) {
               
              for (int i=0; i<=9; i++){
                   if (!Boletas[i].trim().equals("")) {               
                        st.setString(1, OC);
                        st.setString(2, Utility.getDate(6).replaceAll("/","-"));
                        st.setString(3, Placa);
                        st.setString(4, String.valueOf(Conductor));
                        st.setString(5, Boletas[i]);
                        st.setString(6,  Usuario);
                        st.executeUpdate();
                        st.clearParameters();
                   }
              }  
           }
        } catch(SQLException e) {
               throw new SQLException("Error en rutina GuardarBoletas  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
             if(st!=null)  st.close();
             desconectar("SQL_INSERT_BOLETA");
        }
       
    }   
     
     /************************************************************************
     * Metodo   : EliminarBoletas, Elimina las boletas creadas de una planilla
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/  
     public void EliminarBoletas(String OC) throws SQLException {
        PreparedStatement st      = null;
        try {
            st = crearPreparedStatement("SQL_ELIMINAR_BOLETA");
            st.setString(1, OC);
            st.executeUpdate();
          
        } catch(SQLException e) {
            throw new SQLException("Error en rutina GuardarBoletas  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            desconectar("SQL_ELIMINAR_BOLETA");
        }
       
    }   
     /************************************************************************
     * Metodo   : Verificar, Verifica si las boletas de una planilla fueron creadas
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
     public List Verificar(String OC) throws SQLException {
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              Existe  = null;     
        
        try {
            st = crearPreparedStatement("SQL_VERIFICAR");
            st.setString(1, OC);
            rs = st.executeQuery();
             if(rs!=null){
                 Existe = new LinkedList(); 
                if(rs.next()){
                 DescuentoTercm   tercm    = new DescuentoTercm();
                    tercm.setOC(rs.getString(1));
                    Existe.add(tercm);
                }
             }
          
        } catch(SQLException e) {
            throw new SQLException("Error en rutina Verificar  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            desconectar("SQL_VERIFICAR");
        }
      return Existe;  
    }
     
      /************************************************************************
     * Metodo   : VerificarImpresa, Verifica si las boletas de una planilla 
     *            fueron creadas y si fueron impresas
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
      public List VerificarImpresa(String OC) throws SQLException {
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              Existe  = null;     
        
        try {
            st = crearPreparedStatement("SQL_VERIFICAR_IMPRESA");
            st.setString(1, OC);
            rs = st.executeQuery();
             if(rs!=null){
                 Existe = new LinkedList(); 
                if(rs.next()){
                 DescuentoTercm   tercm    = new DescuentoTercm();
                    tercm.setOC(rs.getString(1));
                    Existe.add(tercm);
                }
             }
          
        } catch(SQLException e) {
            throw new SQLException("Error en rutina VerificarImpresa  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            desconectar("SQL_VERIFICAR_IMPRESA");
        }
      return Existe;  
    }
     
     /************************************************************************
     * Metodo   : UpdateFechaImpresion, guarda la fecha de impresion
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
     public void UpdateFechaImpresion(String OC) throws SQLException {
        PreparedStatement st      = null;
        try {
            st = crearPreparedStatement("SQL_UPDATE_FECHA_IMPRESION");
            st.setString(1, Util.getFechaActual_String(6) );
            st.setString(2, OC);
            st.executeUpdate();
          
        } catch(SQLException e) {
            throw new SQLException("Error en rutina UpdateFechaImpresion  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            desconectar("SQL_UPDATE_FECHA_IMPRESION");
        }
       
    }   
      /************************************************************************
     * Metodo   : BuscarBoleta, Busca las boletas que no se han impreso
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
     public List   BuscarBoleta(String OC) throws SQLException {
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        try {
            
            st = crearPreparedStatement("SQL_BUSCAR_BOLETA");
            st.setString(1, OC);
            rs= st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    DescuentoTercm   tercm    = new DescuentoTercm();
                    tercm.setOC       (rs.getString(1));
                    tercm.setPlaca    (rs.getString(2));
                    tercm.setCedula   (Integer.parseInt(rs.getString(3)));
                    tercm.setValorPost(rs.getString(4));
                    tercm.setBoleta   (rs.getString(5));
                    listado.add(tercm);
                }
            }
        } catch(SQLException e) {
            throw new SQLException("Error en rutina BuscarBoleta  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            desconectar("SQL_BUSCAR_BOLETA");
        }
        return listado;
    }
     
      /************************************************************************
     * Metodo   : UpdateFacturada, desmarca las boletas que no entran en la prefactura 
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : recibe array con los numeros de las boletas
     * @version : 1.0
     *************************************************************************/
     public void UpdatePrefactura(String[] boletas) throws SQLException {
        PreparedStatement st      = null;
        try {
            st = crearPreparedStatement("UPDATE_PREFACTURA");
            for(int i=0; i<boletas.length;i++){
               st.setString(1, boletas[i] );
               st.executeUpdate();
               st.clearParameters();
            }
            
          
        } catch(SQLException e) {
            throw new SQLException("Error en rutina UpdateFechaImpresion  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            desconectar("UPDATE_PREFACTURA");
        }
       
    }   
     
       /************************************************************************
     * Metodo   : Prefacturar, marca las boletas Prefacturadas 
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : recibe la fecha inicial y fecha final
     * @version : 1.0
     *************************************************************************/
     public void Prefacturar(String fecini, String fecfin) throws SQLException {
        PreparedStatement st      = null;
        try {
            st = crearPreparedStatement("SQL_PREFACTURAR");
            String fecha = Util.addFecha(fecini,-3,1);
            String NumPrefactura = Util.getFechaActual_String(Util.FORMATO_YYYYMMDD);
            st.setString(1, NumPrefactura);
            st.setString(2, fecha);
            st.setString(3, fecfin);
            st.executeUpdate();
          
        } catch(SQLException e) {
            throw new SQLException("Error en rutina Prefacturar  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            desconectar("SQL_PREFACTURAR");
        }
       
    }   
     
        /************************************************************************
     * Metodo   : PreFActura, Lista todas las boletas que ya estan impresas
     *            y no se han facturado
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : 
     * @version : 1.0
     *************************************************************************/
      public List PreFActura(String fecini, String fecfin) throws SQLException {
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        try {
            
            st = crearPreparedStatement("SQL_PREFACTURA");
            String fecha = Util.addFecha(fecini,-3,1);
            st.setString(1, fecha);
            st.setString(2, fecfin);
            ////System.out.println(st);
            rs= st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    DescuentoTercm   tercm    = new DescuentoTercm();
                    tercm.setBoleta   (rs.getString(1));
                    tercm.setFecha    (rs.getString(2));
                    tercm.setPlaca    (rs.getString(3));
                    tercm.setOC       (rs.getString(4));
                    tercm.setValorPost(rs.getString(5));
                    listado.add(tercm);
                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina PreFActura  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            desconectar("SQL_PREFACTURA");
        }
        return listado;
    }
      
      
        /************************************************************************
     * Metodo   : PreFActura, Lista todas las boletas que ya estan impresas
     *            y no se han facturado
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : 
     * @version : 1.0
     *************************************************************************/
      public List BuscarPreFactura(String NunPrefactura) throws SQLException {
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        try {
            
            st = crearPreparedStatement("SQL_BUSCAR_PREFACTURA");
            //String NP = NunPrefactura.replaceAll("/","").substring(0,9).toString();
            st.setString(1, NunPrefactura);
            rs= st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    DescuentoTercm   tercm    = new DescuentoTercm();
                    tercm.setBoleta    (rs.getString(1));
                    tercm.setFecha     (rs.getString(2));
                    tercm.setPlaca     (rs.getString(3));
                    tercm.setOC        (rs.getString(4));
                    tercm.setValorPost (rs.getString(5));
                    tercm.setPrefactura(rs.getString(6));
                    listado.add(tercm);
                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina BuscarPreFactura  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            desconectar("SQL_BUSCAR_PREFACTURA");
        }
        return listado;
    }
      /************************************************************************
     * Metodo   : BuscarBoletaAnular, Busca las boletas de una OC 
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
     public List   BuscarBoletaAnular(String OC) throws SQLException {
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        List              listado = null;
        try {
            
            st = crearPreparedStatement("SQL_BOLETAS_ANULAR");
            st.setString(1, OC);
            rs= st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    DescuentoTercm   tercm    = new DescuentoTercm();
                    tercm.setOC       (rs.getString(1));
                    tercm.setPlaca    (rs.getString(2));
                    tercm.setCedula   (Integer.parseInt(rs.getString(3)));
                    tercm.setValorPost(rs.getString(4));
                    tercm.setBoleta   (rs.getString(5));
                    listado.add(tercm);
                }
            }
        } catch(SQLException e) {
            throw new SQLException("Error en rutina BuscarBoletaAnular  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            desconectar("SQL_BOLETAS_ANULAR");
        }
        return listado;
    }  
     /************************************************************************
     * Metodo   : AnularBoletas, Anula las boletas de una oc
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : recibe la planilla
     * @version : 1.0
     *************************************************************************/
     public void AnularBoletas(String OC, String Usuario) throws SQLException {
        PreparedStatement st      = null;
        PreparedStatement st2      = null;
        try {
             st2 = crearPreparedStatement("SQL_UPDATE_FECHA_CHEQUE");
             st  = crearPreparedStatement("SQL_ANULAR_BOLETAS");
             st.setString(1,Usuario);
             st.setString(2,OC);
             st.executeUpdate();

             st2.setString(1,OC);
             st2.executeUpdate();



             logger.info("El Usuario:  "+Usuario+"  Anulo las boletas de la planilla :  "+OC+"  Fecha: "+Util.getFechaActual_String(6));          
        } catch(SQLException e) {
            throw new SQLException("Error en rutina AnularBoletas  [DescuentoTercmDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(st!=null)  st2.close();
            desconectar("SQL_ANULAR_BOLETAS");
            desconectar("SQL_UPDATE_FECHA_CHEQUE");
        }

    }   
   
  /*   public static void main (String []args) throws Exception {
        DescuentoTercmDAO co = new DescuentoTercmDAO();
//        String OC = "442321";
//        co.BuscarValor(OC);
         String fecini = "20060112"; 
         //String fecfin = "2006-01-05";
         co.BuscarPreFactura(fecini);
        
    }*/
}
