
/***********************************************************************************
 * Nombre clase : ............... ReporteOCAnticipoDAO.java                        *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD para generar los datos del reporte     *
 *                                de OC con Anticipo sin tr�fico.                  *
 * Autor :....................... Ing. Iv�n Dar�o Devia Acosta                     *
 * Fecha :........................ 23 de noviembre de 2006, 01:36 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;


import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;

public class ReporteOCAnticipoDAO extends MainDAO{
    
    /** Creates a new instance of ReporteOCAnticipoDAO */
    public ReporteOCAnticipoDAO() {
        super("ReporteOCAnticipoDAO.xml");
    }
    public ReporteOCAnticipoDAO(String dataBaseName) {
        super("ReporteOCAnticipoDAO.xml", dataBaseName);
    }
    
    
    
    
    /* LISTADO
     * @autor : Ing. Iv�n Dar�o Devia Acosta
     * @param : String numpla
     */
    public String getDiscrepancias( String numpla ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_DISCREPANCIA_OC";
        String discrepancia  = "";
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, numpla);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    discrepancia = rs.getString("discrepancia");
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return discrepancia;
    }
    
    /* LISTADO
     * @autor : Ing. Iv�n Dar�o Devia Acosta
     * @param : String numrem
     * @param : String ruta_pla
     * @param : String fechai
     * @param : String fechaf
     * @param : String distrito
     */
    public String getOcasociadas( String numrem , String ruta_pla, String fechai, String fechaf, String distrito ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_OC_ASOCIADAS";
        String ocasociadas   = "";
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, numrem );
            st.setString(2, ruta_pla);
            st.setString(3, fechai);
            st.setString(4, fechaf);
            st.setString(5, distrito);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    ocasociadas = rs.getString("ocasociadas");
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return ocasociadas;
    }
    
      /* LISTADO
       * @autor : Ing. Iv�n Dar�o Devia Acosta
       * @param : String numpla
       */
    public String getContaroc( String numrem , String ruta_pla ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_CONTAR_OC";
        String contaroc  = "";
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, numrem);
            st.setString(2, ruta_pla);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    contaroc = rs.getString("contaroc");
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return contaroc;
    }
    
    /* LISTADO
     * @autor : Ing. Iv�n Dar�o Devia Acosta
     * @param : String fechainicial
     * @param : String fechafinal
     * @param : String distrito
     */
    public List List( String fechai, String fechaf, String distrito ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_REPORTE_OC_ANTICIPO";
        List lista           = new LinkedList();
        ReporteOCAnticipo  reporte = new ReporteOCAnticipo();
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, fechai );
            st.setString(2, fechaf );
            st.setString(3, distrito );
            rs=st.executeQuery();
            
            
            
            if(rs!=null){
                while(rs.next()){
                    reporte = ReporteOCAnticipo.load(rs);
                    boolean flag = true;
                    
                    //Vacios
                    if( reporte.getTipo_viaje().equals("VACIO"))
                        flag =  false;
                    
                   
                    //Vlr Planilla = 0
                    if( reporte.getVlrpla2().equals("0") )
                        flag =  false;
                    
                    reporte.setContaroc(this.getContaroc(reporte.getNumrem(), reporte.getRuta_pla()));
                    
                    //COntador OC
                    if( reporte.getContaroc().equals("0") )
                        flag =  false;
                    
                    if( ( reporte.getPlatlr().equals("TPO518") || reporte.getPlatlr().equals("KDC513") || reporte.getPlatlr().equals("SRJ621") || reporte.getPlatlr().equals("QGU515") ) && /*Viajes*/( reporte.getAgc_destino().equals(reporte.getAgc_origen())  ) )
                        flag =  false;
                    
                    
                    reporte.setDiscrepancia(this.getDiscrepancias(reporte.getNumpla()));
                    
                    //discrepancias
                    if( !reporte.getDiscrepancia().equals(""))
                        flag = false;
                    
                    if( flag == true  ){                        
                        reporte.setOcasociadas(this.getOcasociadas(reporte.getNumrem(),reporte.getRuta_pla(),fechai,  fechaf,  distrito));                        
                        lista.add(reporte);
                    }
                    
                    
                }
            }
            
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Error en rutina LIST [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return lista;
    }
   
    
     /* LISTADO
     * @autor : Ing. Enrique De Lavalle
     * @param : String fechainicial
     * @param : String fechafinal
     * @param : String distrito
     */
    public List List_oc_vacias( String fechai, String fechaf, String distrito ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_OC_VACIOS";
        List lista           = new LinkedList();
        boolean sw;
        ReporteOCAnticipo  reporte = new ReporteOCAnticipo();
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, fechai );
            st.setString(2, fechaf );
            st.setString(3, distrito );
            ////System.out.println("QUERY " + st);
            
            rs=st.executeQuery();
            
            if(rs!=null){
                
                String contOC;
                while(rs.next()){
                    sw = true;  
                    reporte = ReporteOCAnticipo.load2(rs);
                    
                    contOC = this.getContaroc(reporte.getNumrem(), reporte.getRuta_pla());
                    
                    /*Discrepancia*/
                    if( !this.getDiscrepancias(reporte.getNumpla()).equals(""))
                        sw = false;
                    
                    /*Validacion OC*/
                    if(contOC.equals("0"))
                        sw = false;
                    
                    /*Placa*/
                    if( ( reporte.getPlatlr().equals("TPO518") || reporte.getPlatlr().equals("KDC513") || reporte.getPlatlr().equals("SRJ621") || reporte.getPlatlr().equals("QGU515") ) && /*Viajes*/( reporte.getAgc_destino().equals(reporte.getAgc_origen())  ) )
                        sw = false;
                    
                    /*Validaciones Placas*/
                    
                    if( sw ){
                        int genOc =0, dias =0;                  
                        
                        if (Integer.parseInt(reporte.getDif())>0)
                            genOc = Integer.parseInt(reporte.getDif()) - this.getFestivos(reporte.getCreation_date(),reporte.getPrinter_date(), distrito);
                        
                        if (Integer.parseInt(reporte.getDias())>0)
                            dias = Integer.parseInt(reporte.getDias()) - this.getFestivos(reporte.getPrinter_date(),Util.getFechaActual_String(6), distrito);

                        reporte.setDif(String.valueOf(genOc));
                        reporte.setDias(String.valueOf(dias));
                        reporte.setOcasociadas(this.getOcasociadas(reporte.getNumrem(),reporte.getRuta_pla(),fechai,  fechaf,  distrito));
                        
                        reporte.setContaroc(contOC);
                        lista.add(reporte);
                    }
                    
                   
                }
            }
            
        }catch(Exception e){
            throw new SQLException("Error en rutina LIST [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return lista;
    }
    
    
     /* getFestivos
     * @autor : Ing. Enrique De Lavalle Rizo
     * @param : String 
     */
    public int getFestivos( String fechai, String fechaf, String dist ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_FESTIVOS";
        int festivos = 0;
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, fechai );
            st.setString(2, fechaf );
            st.setString(3, dist );
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    festivos = rs.getInt("cant_festivos");
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina secuencia [ReporteOCAnticipoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return festivos;
    }
    
    /**
     * Meotodo para obtener las planillas asociadas por remesa y orides
     * @autor mfontalvo
     * @param planilla, datos de la planilla
     * @return String, lista de ocs asociadas
     * @throws Exception.
     */
    public String getOcasociadas( ReporteOCAnticipo planilla ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_OCS_ASOCIADAS_POR_ORIDES";
        String ocasociadas   = "";
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, planilla.getNumrem()   );
            st.setString(2, planilla.getNumpla()   );
            st.setString(3, planilla.getRuta_pla().substring(0,2) );
            st.setString(4, planilla.getRuta_pla().substring(2,4) );
            rs = st.executeQuery();
            if (rs.next()){
                ocasociadas = rs.getString("ocasociadas");
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return ocasociadas;
    }    
    
    /**
     * Metodo para obtener el numero de puestos de control
     * @autor mfontalvo.
     * @param via, cadena q alamacena la via
     * @throws Exception.
     * @return int, numero de puestos de control q contiene la via
     */
    public int obtenerNumeroPuestosControl( String via ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_PUESTO_DE_CONTROL";
        String ocasociadas   = "";
        int numeropuestoscontrol = 0;
        try{
            
            if (via != null && !via.trim().equals("") && via.trim().length()%2==0){
                
                st = this.crearPreparedStatement( query );
                for (int i=0; i<via.length(); i+=2){
                    String codciu = via.trim().substring(i,i+2);
                    st.clearParameters();
                    st.setString(1, codciu );
                    rs = st.executeQuery();
                    if (rs.next()){
                        numeropuestoscontrol++;
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return numeropuestoscontrol;        
    }
    
     /**
     * Metodo para obtener las ocs generadas con anticipos sin reporte en trafico
     * @autor mfontalvo
     * @param fechainicial, fecha inicial donde se va a correr el proceso
     * @param fechafinal, fecha final donde se va a correr el proceso
     * @param distrito, distrito de las plamillas a consultar
     * @throws Exception.
     * @return Vector, arreglo de datos q contine las ocs.
     */
    public Vector obtenerOcsConAnticiposSinReporte( String fechai, String fechaf, String distrito ) throws Exception{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        String            query = "SQL_OCS_GENERADAS_CON_ANTICIPOS_SIN_REPORTE";
        Vector            lista = new Vector();
        TreeMap           ocs   = new TreeMap();
        try{
            st = this.crearPreparedStatement( query );
            st.setString(1, distrito );
            st.setString(2, fechai + " 00:00:00" );
            st.setString(3, fechaf + " 23:59:59");
            rs = st.executeQuery();
            while(rs.next()){
                ReporteOCAnticipo reporte = ReporteOCAnticipo.loadOcsConAnticipoSinReportes(rs);

                if ( ocs.get(reporte.getNumpla()) == null  ){
                    reporte.setContaroc("1");
                    ocs.put( reporte.getNumpla() , reporte.getNumpla() );
                } else
                    reporte.setContaroc("0");

                reporte.setPuestocontrol( "" + this.obtenerNumeroPuestosControl( reporte.getVia() ) );
                int diferencia = rs.getInt("diferencia");
                if ( this.isVacio(reporte.getNumpla(), reporte.getNumrem() ) ) {                    
                    reporte.setTipo_viaje("VACIO");                    
                    reporte.setRango  (diferencia >= 4 ? "4 y mas Dias" : diferencia >=2 && diferencia <=3 ? "2 y 3 Dias" : "1 Dia" );
                } else {
                    reporte.setTipo_viaje("");                    
                    reporte.setRango  (diferencia >  5 ? "Mayor a 5" : diferencia >=4 && diferencia <=5 ? "Entre 4 y 5 Dias" : "Menor a 3 Dias" );
                }

                reporte.setOcasociadas( this.getOcasociadas(reporte) );
                lista.add(reporte);
            }
            
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return lista;
    } 
    /**
     * Metodo para obtener el numero de puestos de control
     * @autor mfontalvo.
     * @param via, cadena q alamacena la via
     * @throws Exception.
     * @return int, numero de puestos de control q contiene la via
     */
    public boolean isVacio( String numpla, String numrem ) throws Exception {
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_ES_VACIO";
        String ocasociadas   = "";
        int numeropuestoscontrol = 0;
        try{            
            st = this.crearPreparedStatement( query );
            st.setString(1, numrem );
            st.setString(2, numpla );
            rs = st.executeQuery();
            if (rs.next()){
                return true;
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar( query );
        }
        return false;
    }   

}
