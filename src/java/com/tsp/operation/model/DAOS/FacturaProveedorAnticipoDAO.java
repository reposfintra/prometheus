
/***********************************************************************************
 * Nombre clase : ............... FacturaProveedorAnticipoDAO                      *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD para generar los datos para listar los *
 *                                item q contendran las facturas de pago anticipo  *
 *                                a proveedores                                    *
 * Autor :....................... Ing. Enrique De Lavalle Rizo                     *
 * Fecha :........................ 30 de Enero de 2007, 01:36 AM                   *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;

public class FacturaProveedorAnticipoDAO extends MainDAO{
    
    /** Creates a new instance of ReporteOCAnticipoDAO */
    public FacturaProveedorAnticipoDAO() {
        super("FacturaProveedorAnticipoDAO.xml");
    }
    public FacturaProveedorAnticipoDAO(String dataBaseName) {
        super("FacturaProveedorAnticipoDAO.xml", dataBaseName);
    }

    /* LISTADO
     * @autor : Ing. Enrique De Lavalle
     * @param : String fechainicial
     * @param : String fechafinal
     * @param : String proveedor
     * @param : String distrito
     */
    public List List_Items( String distrito, String proveedor, String fechai, String fechaf  ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_LISTAR_ITEMS";
        List lista           = new LinkedList();
      
        FacturaProveedorAnticipo  items = new FacturaProveedorAnticipo();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );
            st.setString(2, proveedor );
            st.setString(3, fechai+" 00:00" );
            st.setString(4, fechaf +" 23:59");
            rs=st.executeQuery();
            
            if(rs!=null){                
                while(rs.next()){                 
                    items = FacturaProveedorAnticipo.load(rs);
                    lista.add(items);                          
                }
            }
            
            }}catch(Exception e){
            throw new SQLException("Error en rutina LIST [FacturaProveedorAnticipoDAO].... \n"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }


 /**
  *
  * @param distrito
  * @param proveedor
  * @param documento
  * @param descrip
  * @param agencia
  * @param hc
  * @param idmims
  * @param banco
  * @param sucursal
  * @param moneda
  * @param vlr_neto
  * @param vlr_total_abonos
  * @param vlr_saldo
  * @param vlr_neto_me
  * @param vlr_total_abonos_me
  * @param vlr_saldo_me
  * @param tasa
  * @param userUp
  * @param creationUser
  * @param base
  * @param moneda_banco
  * @param fechaVenc
  * @param lastNum
  * @param vec
  * @param conceptCode
  * @param aprob
  * @throws Exception
  */
    public void insertarFacturaCab (String distrito, String proveedor, String documento, String descrip, String agencia, String hc, String idmims, String banco, String sucursal, String moneda, double vlr_neto, double vlr_total_abonos, double vlr_saldo, double vlr_neto_me, double vlr_total_abonos_me, double vlr_saldo_me, double tasa, String userUp, String creationUser, String base, String moneda_banco, String fechaVenc,  int lastNum, Vector vec, String conceptCode, String aprob)throws Exception{
        
        Connection       con = null;
        Statement         st = null;
        PreparedStatement ps = null;
        ResultSet        rs  = null;
        String           sql = "";
        
        String SQL_INSERTAR_CPX_DOC             = "";
        String SQL_INCREMENTAR_LASTNUMBER_SERIE = "";
        String SQL_INSERTAR_CXP_ITEM_DOC        = "";
        String SQL_UPDATE_MOVPLA_ITEM_FACTURA   = "";
        
        
         try {
            con = this.conectarJNDI("SQL_INSERTAR_CPX_DOC");//JJCastro fase2
            if(con!=null){
            con.setAutoCommit(false);
            st = con.createStatement();
            
            SQL_INSERTAR_CPX_DOC              = this.obtenerSQL("SQL_INSERTAR_CPX_DOC");
            SQL_INCREMENTAR_LASTNUMBER_SERIE  = this.obtenerSQL("SQL_INCREMENTAR_LASTNUMBER_SERIE");
            SQL_INSERTAR_CXP_ITEM_DOC         = this.obtenerSQL("SQL_INSERTAR_CXP_ITEM_DOC");
            SQL_UPDATE_MOVPLA_ITEM_FACTURA    = this.obtenerSQL("SQL_UPDATE_MOVPLA_ITEM_FACTURA");
            
            
            ps = con.prepareStatement(SQL_INSERTAR_CPX_DOC);
            ps.setString(1, distrito);
            ps.setString(2, proveedor);
            ps.setString(3, documento);
            ps.setString(4, descrip);
            ps.setString(5, agencia);
            ps.setString(6, hc);
            ps.setString(7, idmims);
            ps.setString(8, aprob);
            ps.setString(9, banco);
            ps.setString(10, sucursal);
            ps.setString(11, moneda );            
            ps.setDouble(12, vlr_neto);
            ps.setDouble(13, vlr_total_abonos);
            ps.setDouble(14, vlr_saldo);
            ps.setDouble(15, vlr_neto_me);
            ps.setDouble(16, vlr_total_abonos_me);
            ps.setDouble(17, vlr_saldo_me);
            ps.setDouble(18, tasa);          
            ps.setString(19, userUp);
            ps.setString(20, creationUser);
            ps.setString(21, base);
            ps.setString(22, moneda_banco);
            ps.setString(23, fechaVenc );            
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(SQL_INCREMENTAR_LASTNUMBER_SERIE);            
            ps.setInt(1, lastNum);
            st.addBatch(ps.toString());
            
            for (int i=0; i<vec.size();i++){
                CXPItemDoc item = (CXPItemDoc) vec.get(i);
                
                ps = con.prepareStatement(SQL_INSERTAR_CXP_ITEM_DOC);                
                ps.setString(1, item.getDstrct());
                ps.setString(2, item.getProveedor());
                ps.setString(3, item.getTipo_documento());
                ps.setString(4, item.getDocumento());
                ps.setString(5, item.getItem());
                ps.setString(6, item.getDescripcion());
                ps.setDouble(7, item.getVlr());
                ps.setDouble(8, item.getVlr_me());
                ps.setString(9, item.getCodigo_cuenta());
                ps.setString(10, item.getCodigo_abc());
                ps.setString(11, item.getPlanilla());
                ps.setString(12, item.getUser_update());
                ps.setString(13, item.getCreation_user());
                ps.setString(14, item.getBase());
                ps.setString(15, item.getCodcliarea());
                ps.setString(16, item.getTipcliarea());
                ps.setString(17, item.getConcepto());
                ps.setString(18, item.getAuxiliar());
                st.addBatch(ps.toString());
                
                ps = con.prepareStatement(SQL_UPDATE_MOVPLA_ITEM_FACTURA); 
                ps.setString(1, documento);
                ps.setString(2, item.getPlanilla());
                ps.setString(3, item.getCreation_date());
                                
                st.addBatch(ps.toString());
                
            }
            
            
            st.executeBatch();            
            con.commit();
            con.setAutoCommit(true);     
            
       }}
        catch(Exception e) {
            e.printStackTrace();
            if(con!=null) {
                con.rollback();
            }
            throw new SQLException("Error en rutina insertarFacturaCab  [FacturaProveedorAnticipoDAO].... \n"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            
    
    }
    
}
