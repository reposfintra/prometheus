/***************************************
 * Nombre Clase ............. ArchivoMovimientoDAO.java
 * Descripci�n  .. . . . . .  Generamos Los Cheques de las facturas aprobada para pago dentro de una corrida.
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  07/03/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.model.DAOS;



import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;


public class ArchivoMovimientoDAO extends MainDAO{
    
    private String  TABLA_CODIGO_BANCOS        = "CBANCO";
    
    private String  NIT_TSP                    = "08901031611";
    private String  No_CUENTA_TSP              = "201354719";
    private String  TIPO_CUENTA_TSP            = "RO";        // CA: AHORRO   CC: CORRIENTE
    private String  TIPO_OPERACION_CLIENTE     = "DB";
    
    private String  TIPO_TRANSACCION_PROVEEDOR = "CR";
    private String  SECUENCIA_NOTA_ADICIONAL   = "01";
    private String  OBSERVACION                = "CANCELACION CHEQUE ";
    
    
    
    
    
    
    public ArchivoMovimientoDAO() {
         super("ArchivoMovimientoDAO.xml") ;
    }
    public ArchivoMovimientoDAO(String dataBaseName) {
         super("ArchivoMovimientoDAO.xml",dataBaseName) ;
    }
    
    
    
    
     /**
       * M�todo que busca los cheques de la corrida establecida en mims: 281
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List searchCheques(String distrito, String banco, String sucursal, String corrida ) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_CHEQUES_MIMS";
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, corrida    );
                st.setString(2, distrito   );
                st.setString(3, banco      );
                st.setString(4, sucursal   );
                st.setString(5, corrida    );
                
                rs=st.executeQuery(); 
                
                
                while(rs.next()){
                    ArchivoMovimiento  am = this.load(rs);
                   
                    am.setObservacion             ( this.OBSERVACION + am.getCheque()   );
                    am.setSecuenciaNotaAdicional  ( this.SECUENCIA_NOTA_ADICIONAL       );
                    
                // Cliente:                    
                    am.setNitCliente              ( this.NIT_TSP                );
                    am.setNoCuentaCliente         ( this.No_CUENTA_TSP          );
                    am.setTipoCuentaCliente       ( this.TIPO_CUENTA_TSP        );
                    am.setTipoOperacionCliente    ( this.TIPO_OPERACION_CLIENTE );
                    
                    
                // Proveedor:
                    Hashtable  datos            = this.searchDatosProveedor( am.getDistrito(), am.getIdMims() );
                    String     nit              = resetNull  ( (String) datos.get("nit")             );
                    String     bancoTransf      = resetString( (String) datos.get("banco_trans")     );
                    String     tipoCuentaTransf = resetString( (String) datos.get("tipoCuenta_trans"));
                    String     noCuentaTransf   = resetValor ( (String) datos.get("noCuenta_trans")  );
                    String     cedulaCuenta     = resetValor ( (String) datos.get("cedula_cuenta")   );
                    String     nombreCuenta     = resetString( (String) datos.get("nombre_cuenta")   );
                    
                   
                    if (    !bancoTransf.trim().equals("X")   &&  !tipoCuentaTransf.trim().equals("X")  &&  !noCuentaTransf.trim().equals("0")  
                        &&  !cedulaCuenta.trim().equals("0")  &&  !nombreCuenta.trim().equals("X")                    
                     ){                      
                        
                        am.setNitProveedor            ( nit ); 
                        am.setCodigoBancoProveedor    ( resetString(this.searchCodeBanco( bancoTransf ))  );
                        am.setTipoCuentaProveedor     ( tipoCuentaTransf  );
                        am.setNoCuentaProveedor       ( noCuentaTransf    );
                        am.setCedulaCuenta            ( cedulaCuenta      );
                        am.setNombreCuenta            ( nombreCuenta      );
                        am.setTipoTransaccionProveedor( this.TIPO_TRANSACCION_PROVEEDOR        );

                        lista.add(am);
                        
                    }else{
                        //System.out.println("NO SUBIO " +  am.getIdMims() + "->" + nit );
                    }
                    
                    
                    
                    
                }
                
                
                if(lista.size()>0){
                   lista  =  Ordenar          ( lista );
                   lista  =  agregarSecuencia ( lista );
                }
                
                
                }}catch(Exception e){
                  throw new Exception( "searchCheques " + e.getMessage());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return lista; 
      }
      
    
    
      
      
      
      
      
      
      /**
       * M�todo que ordena por cedula de cuenta
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      private  List Ordenar(List lista) throws Exception{
           
            PreparedStatement  st       = null;
            ResultSet          rs       = null;
            Connection         con      = null;
            String             query    = "SQL_ORDENAR_CEDULA_CUENTA";
            
            List               ordenada = new LinkedList();
            
            try{
                
                
             // Formamos la cadena de id mims:
                String parameter = "";
                for(int i=0;i<lista.size();i++){
                    ArchivoMovimiento  am     = (ArchivoMovimiento)lista.get(i);
                    String             idMims =  "'" + am.getIdMims() +"'";
                    if( !parameter.equals("") )
                        parameter += ",";
                    parameter += idMims;
                }
                
                
                
                con          =   this.conectarJNDI(query);//JJCastro fase2
                if(con!=null){
                String sql   =   this.obtenerSQL( query ).replaceAll("#IDMIMS#", parameter );
                st           =   con.prepareStatement(sql);
                rs           =   st.executeQuery(); 
                
                ////System.out.println("SQL ORDEN:"  + sql ) ;
                
                while(rs.next()){
                      String   idMims   = rs.getString(1);
                      List     aux      =  getFilas(lista, idMims  );
                      
                      ordenada.addAll(aux);
                      
                }
                
                
            }}catch(Exception e){
                  throw new Exception( "ordenar " + e.getMessage());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return ordenada; 
      }
      
      
      
      
      
      
      
      
    /**
    * M�todo que devuelve los registros con el mismo idmims
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/   
   public  List getFilas(List lista, String idMims)throws Exception{
      List aux = new LinkedList();
      try{
          
         for (int i = 0; i<lista.size();i++){              
              ArchivoMovimiento  am  = (ArchivoMovimiento)lista.get(i); 
              if ( am.getIdMims().equals( idMims )  )
                  aux.add( am );
         }
        
      }catch(Exception e){
          throw new Exception( "getFilas: " + e.getMessage() );
      }
      return aux;
    }
   
   
   
      
      
   /**
    * M�todo que agrega la secuencia  por cedula de cuenta
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/   
   public  List agregarSecuencia(List lista)throws Exception{
      try{
          
         String  idCuentaAnt = "";
         int     cont        = 0;
         
         for (int i = 0; i<lista.size();i++){              
              ArchivoMovimiento  am  = (ArchivoMovimiento)lista.get(i); 
              String cedulaCuenta    =  am.getCedulaCuenta();
              
              if (!cedulaCuenta.equals(idCuentaAnt) ){
                   idCuentaAnt = cedulaCuenta;
                   cont = 1;
              }else
                   cont ++;
              
              am.setSecuencia         ( String.valueOf( i+1  ) );
              am.setSecuenciaProveedor( String.valueOf( cont ) );
         }
        
      }catch(Exception e){
          throw new Exception( "agregarSecuencia: " + e.getMessage() );
      }
      return lista;
    }
   
   
   
   
   
   
   
   
       
       
      
      
      
       /**
       * M�todo que busca los cheques de la corrida establecida en mims: 281
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      private  Hashtable searchDatosProveedor(String distrito, String idMims ) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            Hashtable          datos   = new Hashtable();
            String             query   = "SQL_DATOS_PROVEEDOR";
            try{
                con = this.conectarJNDI(query);//JJCastro fase2
                if(con!=null){
                String sql   =   this.obtenerSQL( query );
                st= con.prepareStatement(sql);
                st.setString(1, distrito   );
                st.setString(2, idMims     );
                rs=st.executeQuery(); 
                if(rs.next()){
                    
                    datos.put("distrito",          rs.getString("dstrct")           );
                    datos.put("nit",               rs.getString("nit")              );
                    datos.put("idmims",            rs.getString("id_mims")          );
                    datos.put("nombre",            rs.getString("payment_name")     );
                    datos.put("banco",             rs.getString("branch_code")      );
                    datos.put("sucursal",          rs.getString("bank_account_no")  );
                    datos.put("agencia",           rs.getString("agency_id")        );
                    datos.put("banco_trans",       rs.getString("banco_transfer")   );
                    datos.put("sucursal_trans",    rs.getString("suc_transfer")     );
                    datos.put("tipoCuenta_trans",  rs.getString("tipo_cuenta")      );
                    datos.put("noCuenta_trans",    rs.getString("no_cuenta")        );
                    datos.put("nombre_cuenta",     rs.getString("nombre_cuenta")    );
                    datos.put("cedula_cuenta",     rs.getString("cedula_cuenta")    );
                }
                
            }}catch(Exception e){
                  throw new Exception( "searchDatosProveedor " + e.getMessage());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return datos; 
      }
      
      
      
      
      
      
      
      
      
      /**
       * M�todo que busca los cheques de la corrida establecida en mims: 281
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      private  String searchCodeBanco(String banco) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            String             code    = "";
            String             query   = "SQL_CODIGO_BANCO";
            try{
                
                banco = banco.toUpperCase();
                
                con = this.conectarJNDI(query);
                if (con != null) {
                String sql   =   this.obtenerSQL( query );
                
                st= con.prepareStatement(sql);
                st.setString(1, this.TABLA_CODIGO_BANCOS );
                st.setString(2, banco   );
                rs = st.executeQuery(); 
                if( rs.next() )
                    code = this.resetNull( rs.getString("descripcion") );
                
                
                }}catch(Exception e){
                  throw new Exception( "searchCodeBanco " + e.getMessage());
            }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return code; 
      }
      
      
      
      
      
      
      
       /**
       * M�todo que determina si esa transferencia ya se realizo o no
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  boolean  existeTransferencia(String distrito, String banco, String sucursal, String corrida) throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          boolean estado = false;
          String query = "SQL_EXISTE_TRANSFERENCIA";
          try {
              con = this.conectarJNDI(query);
              if (con != null) {
                  String sql = this.obtenerSQL(query);
                  st = con.prepareStatement(sql);
                  st.setString(1, distrito);
                  st.setString(2, banco);
                  st.setString(3, sucursal);
                  st.setString(4, corrida);
                  rs = st.executeQuery();
                  while (rs.next()) {
                      estado = true;
                      break;
                  }

              }
          } catch (Exception e) {
              throw new Exception("existeTransferencia " + e.getMessage());
          }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            return estado; 
      }
      
      
      
      
      
       /**
       * M�todo que inserta la transferencia migrada
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  void  insertTransferencia(String distrito, String banco, String sucursal, String corrida, String user) throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          String query = "SQL_INSERT_TRANSFERENCIA";
          try {
              con = this.conectarJNDI(query);
              if (con != null) {
                  String sql = this.obtenerSQL(query);
                  st = con.prepareStatement(sql);
                  st.setString(1, distrito);
                  st.setString(2, banco);
                  st.setString(3, sucursal);
                  st.setString(4, corrida);
                  st.setString(5, user);
                  st.execute();
              }
          } catch (Exception e) {
              throw new Exception("insertTransferencia " + e.getMessage());
          } finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
      }
      
      
      
      
      
      
      
      
      
      /**
       * M�todo que carga los datos obtenidos de mims
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      private  ArchivoMovimiento load(ResultSet rs)throws Exception{
          ArchivoMovimiento  am = new ArchivoMovimiento();
          try{
             
              am.setDistrito( resetNull(rs.getString  (1)   ));
              am.setBanco   ( resetNull(rs.getString  (2)   ));
              am.setSucursal( resetNull(rs.getString  (3)   ));
              am.setCorrida ( resetNull(rs.getString  (4)   ));
              am.setCheque  ( resetNull(rs.getString  (5)   ));
              am.setIdMims  ( resetNull(rs.getString  (6)   ));
              am.setNombre  ( resetNull(rs.getString  (7)   ));
              
              int  vlr   =   (int)  Math.round( rs.getDouble(8) ) ;
              am.setValor   ( resetNull(  String.valueOf(  vlr )) ); 
              
              
              am.setMoneda  ( resetNull(rs.getString  (9)   ));
              String fecha  = resetNull(rs.getString (10)    );
              
              if(!fecha.equals("")){
                  fecha =  fecha.substring(4,6)  + fecha.substring(6,8)  +  fecha.substring(2,4) ; 
              }else
                  fecha  = Util.getFechaActual_String(3) + Util.getFechaActual_String(5) + Util.getFechaActual_String(1).substring(2,4);

              am.setFecha   ( fecha );
              
              
          }
          catch(Exception e){
              throw new Exception(" load "+e.getMessage());
          }
          return am;
      }
    
      
      
      
      private String resetNull(String val){
          if(val==null)
              val="";
          return val;
      }
    
      
      private String resetString(String val){
          if(val==null || val.equals(""))
              val="X";
          return val;
      }
      
      
      private String resetValor(String val){
          if(val==null || val.equals("") )
              val="0";
          return val;
      }
      
      
      
      
    
    
}
