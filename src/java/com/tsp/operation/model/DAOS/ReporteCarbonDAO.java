/*********************************************************************************
 * Nombre clase :      ReporteCarbonDAO.java                                     *
 * Descripcion :       DAO del ReporteCarbonDAO.java                             *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  *
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             LREALES                                                   *
 * Fecha :             27 de octubre de 2006, 11:00 AM                           *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import java.text.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.util.connectionpool.PoolManager;
import org.apache.log4j.Logger;

public class ReporteCarbonDAO extends MainDAO {
    
    private BeanGeneral bean;
    
    private Vector vector;
         
    /** Creates a new instance of ReporteCarbonDAO */
    public ReporteCarbonDAO () {
        
        super ( "ReporteCarbonDAO.xml" );
        
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector () {
        
        return vector;
        
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector ( java.util.Vector vector ) {
        
        this.vector = vector;
        
    }
    
    /** 
     * Funcion publica que obtiene la información que se va a generar
     * en el reporte de carbon. 
     */
    public void reporte ( boolean todos, String nit_proveedor, String fecini, String fecfin ) throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        String to = "";
        String temp = "";
        String var = "";
        
        try {
            
            String agregar = "";
            String sql = this.obtenerSQL("SQL_REPORTE");
            
            if( todos ) {              
                agregar = sql.replaceAll("#FILTRO#", "LEFT JOIN movpla m ON ( m.concept_code = '01' AND m.planilla = p.numpla )");
            } else {
                agregar = sql.replaceAll("#FILTRO#", "INNER JOIN movpla m ON ( m.concept_code = '01' AND m.planilla = p.numpla AND m.proveedor_anticipo = '" + nit_proveedor + "' )");
            }
            
            con = this.conectarJNDI("SQL_REPORTE"); //JJCastro fase2
if(con!=null){
            st = con.prepareStatement( agregar );  
                        
            st.setString( 1, fecini + ":00" );
            st.setString( 2, fecfin + ":00" );
            
            rs = st.executeQuery();
            
            this.vector = new Vector();
            
            while( rs.next() ){
                
                String proyecto = rs.getString( "base" )!=null?rs.getString( "base" ).toUpperCase():"";           
                String planilla = rs.getString( "numpla" )!=null?rs.getString( "numpla" ).toUpperCase():"";
                String agencia = rs.getString( "agepla" )!=null?rs.getString( "agepla" ).toUpperCase():"";
                String origen = rs.getString( "oripla" )!=null?rs.getString( "oripla" ).toUpperCase():"";
                String destino = rs.getString( "despla" )!=null?rs.getString( "despla" ).toUpperCase():"";
                String peso = rs.getString( "pesoreal" )!=null?rs.getString( "pesoreal" ):"0";
                String fecha_cumplido = rs.getString( "feccum" )!=null?rs.getString( "feccum" ):"0099-01-01 00:00:00"; 
                fecha_cumplido = fecha_cumplido.equals("0099-01-01 00:00:00")?"":fecha_cumplido;
                String std_job = rs.getString( "std_job_no" )!=null?rs.getString( "std_job_no" ).toUpperCase():"";
                String remesa = rs.getString( "remesa" )!=null?rs.getString( "remesa" ).toUpperCase():"";
                String valor = rs.getString( "vlr" )!=null?rs.getString( "vlr" ):"0.00";
                
                String fecha_despacho = rs.getString( "fecdsp" )!=null?rs.getString( "fecdsp" ):"0099-01-01 00:00:00";  
                fecha_despacho = fecha_despacho.equals("0099-01-01 00:00:00")?"":fecha_despacho;
                String nit_proveedor_anticipo = rs.getString( "proveedor_anticipo" )!=null?rs.getString( "proveedor_anticipo" ).toUpperCase():"";
                String nombre_proveedor_anticipo = rs.getString( "nombre_proveedor_anticipo" )!=null?rs.getString( "nombre_proveedor_anticipo" ).toUpperCase():"";
                
                String e_mail = rs.getString( "e_mail" )!=null?rs.getString( "e_mail" ):"";
                
                if ( proyecto.equals("PCO") )
                    proyecto = "Puerto Prodeco";
                if ( proyecto.equals("SPO") )
                    proyecto = "Puerto Carbosan";
                if ( proyecto.equals("PRC") )
                    proyecto = "Puerto Rio Cordoba";
                
                bean = new BeanGeneral ();
                
                bean.setValor_01( proyecto );
                bean.setValor_02( planilla );
                bean.setValor_03( agencia );
                bean.setValor_04( origen );
                bean.setValor_05( destino );
                bean.setValor_06( peso );
                bean.setValor_07( fecha_cumplido );
                bean.setValor_08( std_job );
                bean.setValor_09( remesa );
                bean.setValor_10( valor );
                
                bean.setValor_11( fecini );// fecini
                bean.setValor_12( fecfin );// fecfin
                
                bean.setValor_13( fecha_despacho );
                bean.setValor_14( nit_proveedor_anticipo );
                bean.setValor_15( nombre_proveedor_anticipo );
                
                String tod = "";
                tod = todos==true?"1":"2";
                bean.setValor_16( tod );
                bean.setValor_17( nit_proveedor );
                
                if ( temp.equals("") || !temp.equals( nit_proveedor_anticipo ) ) {
                    
                    if ( !nit_proveedor_anticipo.trim().equals("") && !e_mail.trim().equals("") ){
                        to += e_mail + ";";
                    }
                    
                }
                temp = nit_proveedor_anticipo;
                
                bean.setValor_18( e_mail );
                bean.setValor_19( to );
                
                this.vector.add( bean );
                
            }
            
        }} catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR DURANTE 'reporte()' - [ReporteCarbonDAO].. " + e.getMessage() );
            
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
}