/********************************************************************
 *      Nombre Clase.................   WGrpStdjobDAO.java
 *      Descripci�n..................   DAO del archivo wgroup_stadart
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   16.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;

public class WGrpStdjobDAO {
    private Vector vector = new Vector();
    private WgroupStd wg;
    
    private static final String SQL_LISTAR = 
            "SELECT work_group AS codigo,  " +
            "       COALESCE(a.descripcion, '') AS descripcion " +
            "FROM   wgroup_standard wstd, " +
            "       tablagen a, " +
            "       tablagen_prog b " +
            "WHERE  a.REG_STATUS!='A' AND a.table_type='WG' AND " +
            "       a.table_type=b.table_type AND a.table_code = b.table_code " +
            "       AND b.program = 'WORKGROUP' AND work_group = a.table_code " +
            "       AND wstd.reg_status<>'A' AND std_job_no = ? " +
            "ORDER BY a. descripcion";
    
    private static final String SQL_INSERT =
            "INSERT INTO " +
            "   wgroup_standard(" +
            "       reg_status, " +
            "       dstrct, " +
            "       std_job_no, " +
            "       work_group, " +
            "       creation_date, " +
            "       creation_user, " +
            "       base) " +
            "VALUES('', ?, ?, ?, 'now()', ?, ?)";
    
    private static final String SQL_REGSTATUS_UPDATE = 
            "UPDATE wgroup_standard SET reg_status=?, last_update='now()', user_update=? WHERE std_job_no=? AND work_group=?";
    
    private static final String SQL_EXISTE =
            "SELECT reg_status FROM wgroup_standard WHERE std_job_no=? AND work_group=?";
    
    /** Creates a new instance of WGrpStdjobDAO */
    
    public WGrpStdjobDAO() {
    }
    
    /**
     * Obtiene el c�digo y la descripci�n de todos los work groups asociados
     * a un stdjob.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param stdjob N�mero de stdjob
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void workGroups(String stdjob)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        this.vector = new Vector();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_LISTAR);
                st.setString(1, stdjob);
                
                //////System.out.println("............... consulta wgroups del stdjob: " + st.toString());
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    Hashtable ht = new Hashtable();
                    
                    String codigo = rs.getString("codigo");
                    String desc = rs.getString("descripcion");
                    
                    ht.put("codigo", codigo);
                    ht.put("descripcion", desc);                                       
                    
                    this.vector.add(ht);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS WORK GROUPS DEL SATADNARD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Ingresa un registro en el archivo wgroup_standard.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void ingresar()throws SQLException{

        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_INSERT);
                
                st.setString(1, wg.getDstrct());
                st.setString(2, wg.getStd_job_no());
                st.setString(3, wg.getWork_group());
                st.setString(4, wg.getCreation_user());
                st.setString(5, wg.getBase());
                
                //////System.out.println("............. insercion en wgroup_standard: " + st.toString());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LOS WORK GROUPS DEL SATADNARD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Actualiza el campo reg_status del archivo wgroup_standard.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param reg_status Valor del reg_status
     * @param user Usuario que realiza la modificaci�n
     * @param wgroup C�digo del work group
     * @param stdjob N�mero del standard job
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void reg_statusUpdate(String reg_status, String user, String wgroup, String stdjob) throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_REGSTATUS_UPDATE);
                
                st.setString(1, reg_status);
                st.setString(2, user);
                st.setString(3, stdjob);
                st.setString(4, wgroup);
                
                //////System.out.println("............. actualizacion del reg_status en wgroup_standard: " + st.toString());
                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL REG STATUS EN WGROUP_STANDARD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Verifica la existencia de un registro en el archivo wgroup_standard.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param wgroup C�digo del work group
     * @param stdjob N�mero del standard job
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String existe(String wgroup, String stdjob)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_EXISTE);
                st.setString(1, stdjob);
                st.setString(2, wgroup);
                
                //////System.out.println("....................... existencia del wgroup-stdjob : " + st.toString());
                
                rs = st.executeQuery();
                
                if (rs.next()){
                    return rs.getString("reg_status");                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN LA BUSQUEDA DEL WORK GROUP - SATADNARD " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return null;
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
    /**
     * Getter for property wg.
     * @return Value of property wg.
     */
    public com.tsp.operation.model.beans.WgroupStd getWg() {
        return wg;
    }
    
    /**
     * Setter for property wg.
     * @param wg New value of property wg.
     */
    public void setWg(com.tsp.operation.model.beans.WgroupStd wg) {
        this.wg = wg;
    }
    
}
