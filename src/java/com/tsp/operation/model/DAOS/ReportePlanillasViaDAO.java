/***************************************
 * Nombre Clase ............. ReportePlanillasViaDAO.java
 * Descripci�n  .. . . . . .  Generamos Reporte de planillas
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  10/08/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/




package com.tsp.operation.model.DAOS;




import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;




public class ReportePlanillasViaDAO extends MainDAO{
    
    
    /** ____________________________________________________________________________________________
     * ATRIBUTOS
     * ____________________________________________________________________________________________ */
    
    
    
    
    
    
    /** ____________________________________________________________________________________________
     * METODOS
     * ____________________________________________________________________________________________ */
    
    
    
    public ReportePlanillasViaDAO() {
        super("ReportePlanillasViaDAO.xml");
    }
    
    
    
    
    public  String reset(String val){
        if( val==null )
            val = "";
        return val;
    }
    
    
    
    
    /**
     * M�todo que busca las planillas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getPlanillas( String distrito, String cliente, String fechaIni, String fechaFin, String origen, String destino, String via) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     =  "SQL_BUSCAR_PLANILLAS";
        List              lista     = new LinkedList();
        
        try {
            
            String horaHoy = Util.getFechaActual_String(6).replaceAll("/","-");
            
            st= this.crearPreparedStatement(query);
            st.setString(1, fechaIni  + " 00:00:00" );
            st.setString(2, fechaFin  + " 23:59:59" );
            st.setString(3, distrito );
            st.setString(4, origen   );
            st.setString(5, destino  );
            st.setString(6, via      );
            st.setString(7, cliente  );
                        
            
            
            rs = st.executeQuery();
            while(rs.next()){                
                
                   String po_no   =   reset( rs.getString("oc")      );
                   String salida  =   reset( rs.getString("salida")  );                
                   
                // Buscamos la salida en  rep_mov_trafico            
                   if( salida.equals("") )
                       salida     =   getSalidas( po_no );
                // Buscamos la  fecha de despacho   
                   if( salida.equals("") )
                       salida     =   reset( rs.getString("fecdsp")  );
                   
                   
                   
                   if( ! salida.equals("") ){
                   
                       Hashtable  oc    =  new  Hashtable();
                       
                       oc.put("cliente",       cliente                                 );
                       oc.put("cliente_name",  reset( rs.getString("nombre_cliente") ) );
                       oc.put("ot",            reset( rs.getString("ot") )             );
                       oc.put("oc",            po_no                                   );
                       oc.put("placa",         reset( rs.getString("placa") )          );
                       oc.put("conductor",     reset( rs.getString("conductor") )      );
                       oc.put("nit_conductor", reset( rs.getString("cedcon")    )      );
                       oc.put("vacio",         reset( rs.getString("vacio") )          );
                       oc.put("documento",     reset( rs.getString("documento") )      );
                       oc.put("tipocarga",     reset( rs.getString("tipocarga") )      );
                       oc.put("salida",        salida                                  );                                             
                       
                    // Posible LLegada:
                       String entrega  =   getFechaReporte( po_no,  destino);
                       oc.put("entrega",  entrega );
                       
                       String  entregaVal = (entrega.equals(""))? horaHoy : entrega;
                        
                       String tiempoEstimado    = "";
                       String posibleLLegada    = "";  
                       String diferencia        = "00 00:00:00";
                       String estado            = "A TIEMPO";
                       String tViaje            = "";
                       String tViaje_HHMM       = "";
                       
                       Hashtable  duracion =  getDuracion ( via, salida );
                       if( duracion!=null){
                            tiempoEstimado    =  (String) duracion.get("tiempo");
                            posibleLLegada    =  (String) duracion.get("llegada");
                            diferencia        =  this.getDiferencia( posibleLLegada, entregaVal, "DD HH24:MI:SS" );                            
                            if( !diferencia.equals("") )
                                     estado = ( diferencia.indexOf("-")>-1 )?"DEMORADO":"A TIEMPO";                                                         
                       }
                       
                       tViaje            =  this.getDiferencia( entregaVal, salida,  "DD HH24:MI" ); 
                       tViaje_HHMM       =  this.getDiferenciaFormatoHoras( entregaVal, salida); 
                                
                       oc.put("tiempo",       tiempoEstimado   );
                       oc.put("llegada",      posibleLLegada   );
                       oc.put("diferencia",   diferencia       );
                       oc.put("estado",       estado           );
                       oc.put("tViaje",       tViaje           );
                       oc.put("tViaje_HHMM",  tViaje_HHMM      );                       
                       

                      lista.add( oc );
                      
                   }
                   
            } 
            
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception(" DAO: getPlanillas : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return lista;
    }
    
    
    
    
    
    
    
     /**
     * M�todo que busca la facha de salida
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String  getSalidas(String  planilla) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_BUSCAR_SALIDA";
        String            salida    = "";        
        try {
            
            st= this.crearPreparedStatement(query);
            st.setString(1, planilla  );            
            rs = st.executeQuery();
            if( rs.next() )
                salida  =  reset( rs.getString("salida") );            
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO: getSalidas : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return salida;
    }
    
    
    
    
    
    
    
    /**
     * M�todo que busca los  PC de la via
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getPC(String  via) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_BUSCAR_PCS";
        List              pcs       =  new LinkedList();
        
        try {
            
            st= this.crearPreparedStatement(query);
            st.setString(1, via  );            
            rs = st.executeQuery();
            if( rs.next() ){
                String dato  =  reset( rs.getString("via")  );
                
                for (int i=0; i <dato.length(); i += 2 ){
                     String puesto     = dato.substring( i  , i+2 );
                     String nombre     = puesto;
                     String tipo       = "";
                     
                     Hashtable infoPC  = getInfoCiudad(puesto);
                     if( infoPC!=null ){                            
                         nombre =  (String) infoPC.get("nomciu");
                         tipo   =  (String) infoPC.get("tipociu");
                     }
                     
                     Hashtable  pc =  new Hashtable();
                         pc.put("puesto",     puesto    );
                         pc.put("nombre",     nombre    );
                         pc.put("tipo",       tipo      );
                         
                     pcs.add( pc );                     
                }
                
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception(" DAO: getPC : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return pcs;
    }
    
    
    
    
    
    /**
     * M�todo que busca los reportes de la planilla
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getListReportes(String  planilla) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_REPORTE_PLANILLAS";
        List              reportes  =  new LinkedList();
        
        try {
            
            st= this.crearPreparedStatement(query);
            st.setString(1, planilla  );            
            rs = st.executeQuery();
            while (rs.next()){
                Hashtable infoPC  =  new  Hashtable();
                  infoPC.put("fecha",                 reset( rs.getString("fechareporte")          ) );
                  infoPC.put("observacion",           reset( rs.getString("observacion")           ) );
                  infoPC.put("ciudad",                reset( rs.getString("ubicacion_procedencia") ) );
                  infoPC.put("nombre",                reset( rs.getString("nomciu")                ) );
                  infoPC.put("tipo",                  reset( rs.getString("tipo_reporte")          ) );
                  infoPC.put("novedad",               reset( rs.getString("novedad")               ) );
                  infoPC.put("dato",                  reset( rs.getString("dato")                  ) );
                  infoPC.put("programa",              reset( rs.getString("program")               ) );
                
                reportes.add( infoPC );
            }
            
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO: getListReportes : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return reportes;
    }
    
    
    
    
    
    /**
     * M�todo que busca las demoras
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getDemoras(String  planilla) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_DEMORAS_PLANILLAS";
        List              demoras   =  new LinkedList();
        
        try {
            
            st= this.crearPreparedStatement(query);
            st.setString(1, planilla  );            
            rs = st.executeQuery();
            while (rs.next()){
                Hashtable infoPC  =  new  Hashtable();
                  infoPC.put("fechademora",                 reset( rs.getString("fechademora")     ) );
                  infoPC.put("fecfindem",                   reset( rs.getString("fecfindem")       ) );
                  infoPC.put("estado",                      reset( rs.getString("estado")          ) );
                  infoPC.put("duracion",                    reset( rs.getString("duracion")        ) );
                
                demoras.add( infoPC );
            }
            
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO: getDemoras : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return demoras;
    }
    
    
    
    
    
    
    
    
    
    /**
     * M�todo que busca informacion de  de la ciudad
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public Hashtable   getInfoCiudad(String  code ) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_INFO_CIUDAD";
        Hashtable         info      = null;
        
        try {
            
            st= this.crearPreparedStatement(query);
            st.setString(1, code  );            
            rs = st.executeQuery();
            if( rs.next() ){
                info  =  new  Hashtable();
                info.put("nomciu",   reset( rs.getString("nomciu") )   );
                info.put("tipociu",  reset( rs.getString("tipociu") )   );
            }
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO: getInfoCiudad : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return info;
    }
    
    
    
    
    
    /**
     * M�todo que busca posible duracion del viaje
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public Hashtable   getDuracion(String  via, String salida ) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_DURACION_VIA";
        Hashtable         info      = null;
        
        try {
            
            st= this.crearPreparedStatement(query);
            st.setString(1, salida  );            
            st.setString(2, via     );            
            rs = st.executeQuery();
            if( rs.next() ){
                info  =  new  Hashtable();
                info.put("tiempo",   reset( rs.getString("tiempo") )    );
                info.put("llegada",  reset( rs.getString("proxima") )   );
            }
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO: getDuracion : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return info;
    }
    
    
    
    
    
    
    /**
     * M�todo que establece la diferencia de tiempo entre la posible llegada  y la llegada  real
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getDiferencia(String  posibleLLegada, String llegadaReal, String formato ) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_DIFERENCIA";
        String            tiempo    = "";
        
        try {
            
            st= this.crearPreparedStatement(query);
            st.setString(1, posibleLLegada   );            
            st.setString(2, llegadaReal      ); 
            st.setString(3, formato          ); 
            rs = st.executeQuery();
            if( rs.next() )
                tiempo = rs.getString(1);
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO: getDiferencia : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return tiempo;
    }
    
    
    
    
 
    
    
    /**
     * M�todo que establece la diferencia entre horas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getDiferenciaFormatoHoras(String  fecha1, String fecha2 ) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_DIFERENCIA_CONVERTIR_HOURS";
        String            tiempo    = "";
        
        try {            
            
            
            st= this.crearPreparedStatement(query);
            st.setString(1, fecha1   );            
            st.setString(2, fecha2   ); 
            
            st.setString(3, fecha1   );            
            st.setString(4, fecha2   ); 
            
            st.setString(5, fecha1   );            
            st.setString(6, fecha2   ); 
            
            rs = st.executeQuery();
            if( rs.next() )
                tiempo = rs.getString(1);
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO: getDiferenciaFormatoHoras : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return tiempo;
    }
    
    
    
    
    
    
    /**
     * M�todo que busca la info en q se reporto en el pc
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String getFechaReporte(String  planilla,  String puesto ) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_REPORTE_OC_PC";
        String            report    = "";
        
        try {
            
            st= this.crearPreparedStatement(query);
            st.setString(1, planilla  );            
            st.setString(2, puesto    );
            rs = st.executeQuery();
            if( rs.next() )
                report = reset( rs.getString(1) );  
                
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO: getFechaReporte : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return report;
    }
    
    
    
    
    
    
    
    
    /**
     * M�todo que busca los origenes
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getOrigenes() throws Exception{
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_CIUDAD_ALL";
        List              origenes   =  new LinkedList();
        try {
            
            st= this.crearPreparedStatement(query);
            rs = st.executeQuery();
            while (  rs.next() ){
                Hashtable  pc =  new Hashtable();
                  pc.put("codigo",  reset( rs.getString("codciu") ) );
                  pc.put("nombre",  reset( rs.getString("nomciu") ) );
                origenes.add( pc );
            }
            
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO:getOrigenes : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return  origenes;
    }
    
    
    
    
    
    
    /**
     * M�todo que busca los destinos asociados al origen origenes
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getDestino( String origen) throws Exception{
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_BUSCAR_DESTINOS";
        List              destinos   =  new LinkedList();
        try {
            
            st= this.crearPreparedStatement(query);
            st.setString(1 ,  origen );
            rs = st.executeQuery();
            while (  rs.next() ){
                Hashtable  pc =  new Hashtable();
                  pc.put("codigo",  reset( rs.getString("codigo") ) );
                  pc.put("nombre",  reset( rs.getString("descripcion") ) );
                destinos.add( pc );
            }
            
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO:getDestino : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return  destinos;
    }
    
    
    
    
    
    
    
    
    /**
     * M�todo que busca las posibles vias asociadas del origen y destinos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getVIAS( String origen, String destino) throws Exception{
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_BUSCAR_VIAS";
        List              vias       =  new LinkedList();
        try {
            
            st= this.crearPreparedStatement(query);
            st.setString(1 ,  origen  );
            st.setString(2 ,  destino );
            rs = st.executeQuery();
            while (  rs.next() ){
                Hashtable  pc =  new Hashtable();
                  pc.put("codigo",  reset( rs.getString("via")         ) );
                  pc.put("nombre",  reset( rs.getString("descripcion") ) );
                vias.add( pc );
            }
            
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO:getVIAS : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return  vias;
    }
    
    
    
    
    
    
    
    /**
     * M�todo que busca los clientes
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public List getClientes() throws Exception{
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_CLIENTES_ALL";
        List              clientes   =  new LinkedList();
        try {
            
            st= this.crearPreparedStatement(query);
            rs = st.executeQuery();
            while (  rs.next() ){
                Hashtable  pc =  new Hashtable();
                  pc.put("codigo",  reset( rs.getString("codcli") ) );
                  pc.put("nombre",  reset( rs.getString("nomcli") ) );
                clientes.add( pc );
            }
            
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO:getClientes : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return  clientes;
    }
    
     /**
     * M�todo que establece la diferencia entre horas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/    
    
    public String getDiferenciaHoras(String  Hora1, String Hora2 ) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_DIFERENCIA_HOURS";
        String            tiempo    = "";
        
        try {            
            
            if(  Hora1.equals("") ) Hora1 = "00:00";
            if(  Hora2.equals("") ) Hora2 = "00:00";
            
            String[] h1      =  Hora1.split(":");
            String[] h2      =  Hora2.split(":");
            boolean  sw = false;
            
            int ho1 = Integer.parseInt ( h1[0] );
            int mi1 = Integer.parseInt ( h1[1] );
            int ho2 = Integer.parseInt ( h2[0] );
            int mi2 = Integer.parseInt ( h2[1] );
            
            if( ho1 < 0 ){ ho1 = ho1 * (-1) ; sw = true;}
            if( mi1 < 0 ){ mi1 = mi1 * (-1) ; sw = true;}
            if( ho1 < 0 ){ ho2 = ho2 * (-1) ; sw = true;}
            if( mi1 < 0 ){ mi2 = mi2 * (-1) ; sw = true;}
            
            if( sw ){ Hora1 = ho1 + ":" + mi1; Hora2 = ho2 + ":" + mi2; }
            
            st= this.crearPreparedStatement(query);
            st.setString(1, Hora1   );            
            st.setString(2, Hora2   ); 
            
            rs = st.executeQuery();
            if( rs.next() )
                tiempo = rs.getString(1);
            
        }catch(Exception e){ e.printStackTrace();
            throw new Exception(" DAO: getDiferenciaHoras : "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(query);
        }
        return tiempo;
    }
    
    
}
