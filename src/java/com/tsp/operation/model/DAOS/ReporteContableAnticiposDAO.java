/***************************************
 * Nombre Clase ............. ReporteContableAnticiposDAO.java
 * Descripci�n  .. . . . . .  Ejecutamos los SQL para la consulta de produccion
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  22/08/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/



package com.tsp.operation.model.DAOS;



import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;


public class ReporteContableAnticiposDAO extends MainDAO{
    
    
    
    
    /** Creates a new instance of ReporteContableAnticiposDAO */
    public ReporteContableAnticiposDAO() {
        super("ReporteContableAnticiposDAO.xml");
    }
    public ReporteContableAnticiposDAO(String dataBaseName) {
        super("ReporteContableAnticiposDAO.xml", dataBaseName);
    }
    
    
    public String reset(String val){
        if(val==null)
            val="";
        return val;
    }
    
    
    
    
     /**
     * Metodo que busca los anticipos transferidos dentro de un rango de fechas
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
    public List  getTransferidasByFechas(String distrito,  String proveedor, String fechaIni, String fechaFin)throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              lista       = new LinkedList();
        String            query       = "SQL_PRODUCCION_FECHAS";
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito    );
            st.setString(2, proveedor   ); 
            st.setString(3, fechaIni    );
            st.setString(4, fechaFin    );
            ////System.out.println("SQL " + st.toString() );
            rs = st.executeQuery(); 
            while(rs.next()){
                AnticiposTerceros anticipo = load(rs);
                lista.add(  anticipo );
            }           
             
         }}catch(Exception e) {
             throw new SQLException(" getTransferidasByFechas  -> " +e.getMessage());
         }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
        
    }
    
       
    
    
    
      
 /**
   * M�todo que carga datos de los anticipos pagos terceros pendientes por aprobar
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/  
    public    AnticiposTerceros  load(ResultSet  rs)throws Exception{
        AnticiposTerceros anticipo =  new  AnticiposTerceros();
        try{
           
            AnticiposPagosTercerosDAO  dao  = new AnticiposPagosTercerosDAO();
            
            anticipo.setId                       (          rs.getInt   ("id")                          );
            anticipo.setDstrct                   (   reset( rs.getString("dstrct" )                   ) ); 
            anticipo.setAgency_id                (   reset( rs.getString("agency_id" )                ) );
            anticipo.setPla_owner                (   reset( rs.getString("pla_owner" )                ) );
            anticipo.setPlanilla                 (   reset( rs.getString("planilla" )                 ) );            
            anticipo.setSupplier                 (   reset( rs.getString("supplier" )                 ) );
            anticipo.setProveedor_anticipo       (   reset( rs.getString("proveedor_anticipo" )       ) );
            anticipo.setReanticipo               (   reset( rs.getString("reanticipo" )               ) );
            anticipo.setConductor                (   reset( rs.getString("cedcon" )                   ) );
            anticipo.setConcept_code             (   reset( rs.getString("concept_code" )             ) );
            anticipo.setVlr                      (          rs.getDouble("vlr" )                        );            
            anticipo.setVlr_for                  (          rs.getDouble("vlr_for" )                    );
            anticipo.setCurrency                 (   reset( rs.getString("currency" )                 ) );
            anticipo.setFecha_anticipo           (   reset( rs.getString("fecha_anticipo" )           ) ); 
            
            anticipo.setFecha_transferencia      (   reset( rs.getString("fecha_transferencia" )      ) ); 
            anticipo.setUser_transferencia       (   reset( rs.getString("user_transferencia" )       ) );
            anticipo.setBanco_transferencia      (   reset( rs.getString("banco_transferencia" )      ) );
            anticipo.setCuenta_transferencia     (   reset( rs.getString("cuenta_transferencia" )     ) );
            anticipo.setTcta_transferencia       (   reset( rs.getString("tcta_transferencia" )       ) );
            
            anticipo.setBanco                    (   reset( rs.getString("banco" )                    ) );
            anticipo.setSucursal                 (   reset( rs.getString("sucursal" )                 ) );
            anticipo.setNombre_cuenta            (   reset( rs.getString("nombre_cuenta" )            ) );
            anticipo.setCuenta                   (   reset( rs.getString("cuenta" )                   ) );
            anticipo.setTipo_cuenta              (   reset( rs.getString("tipo_cuenta" )              ) );
            anticipo.setNit_cuenta               (   reset( rs.getString("nit_cuenta" )               ) );
            
            anticipo.setPorcentaje               (          rs.getDouble("porcentaje" )                 );
            anticipo.setVlrDescuento             (          rs.getDouble("vlr_descuento")               );
            anticipo.setVlrNeto                  (          rs.getDouble("vlr_neto")                    );
            anticipo.setVlrComision              (          rs.getDouble("vlr_combancaria")             );
            anticipo.setVlrConsignar             (          rs.getDouble("vlr_consignacion")            );
            
            anticipo.setNombreAgencia            (   reset( rs.getString("nombre_agencia" )           ) );            
            anticipo.setNombrePropietario        (  dao.getNameNit( anticipo.getPla_owner()           ) );
            anticipo.setNombreConductor          (  dao.getNameNit(  anticipo.getConductor()          ) );
            
            
              
            
        }catch(Exception e){
            throw new Exception( " load "+ e.getMessage());
        }
        return anticipo;        
    }
    
    
    
    
    
    
}
