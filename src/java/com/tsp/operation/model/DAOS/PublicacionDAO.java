/*********************************************************************************
 * Nombre clase :      PublicacionDAO.java                                       *
 * Descripcion :       DAO del PublicacionDAO.java                               *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  *
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             LREALES                                                   *
 * Fecha :             11 de abril de 2006, 11:52 AM                             *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
//import com.tsp.util.connectionpool.PoolManager;
import java.io.*;

public class PublicacionDAO extends MainDAO {
    
    private TreeMap titulosTree;
    private TblGeneral tit;
    private Vector publicaciones;
    private PublicacionNoVencida pnv = null;
    //AMARTINEZ
    private Vector VecPublicaciones;
    private Vector VecPublic;
    private Vector vecDatos;
    private Vector VecPublic2;
    private String usuario;
    private String perfil;
    private String publica="N"; 
    // jose
    private Publicacion publicacion;
    
    /** Creates a new instance of PublicacionDAO */
    public PublicacionDAO () {
        super ( "PublicacionDAO.xml" );
    }
    public PublicacionDAO (String dataBaseName) {
        super ( "PublicacionDAO.xml", dataBaseName);
    }
    
    /**
     * Getter for property getTitulosTree.
     * @return Value of property getTitulosTree.
     */
    public TreeMap getTitulosTree (){
        
        return titulosTree;
        
    }
    
    /**
     * Setter for property setTitulosTree.
     * @param titulosTree New value of property setTitulosTree.
     */
    public void setTitulosTree (TreeMap t){
        
        titulosTree = t;
        
    }
    
    /** Funcion publica que obtiene el Arbol de los Titulos */
    public void getTreeMapTitulos () throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        tit = null;
        String query = "SQL_SELECT_TITULOS";
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery ();
                this.titulosTree = new TreeMap ();
                
                while(rs.next ()){
                    this.tit = new TblGeneral ();
                    tit.setCodigo (rs.getString ("table_code"));
                    tit.setDescripcion (rs.getString ("descripcion"));
                    this.titulosTree.put ( tit.getDescripcion (), tit.getCodigo () );
                }
            }
            
        } catch( SQLException e ){            
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LOS TITULOS " + e.getMessage () + " " + e.getErrorCode () );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }


/**
 * 
 * @param distrito
 * @param id_opcion
 * @param texto
 * @param tipo
 * @param codigo_titulo
 * @param creation_user
 * @param base
 * @throws Exception
 */
    public void insertPublicacion ( String distrito, String id_opcion, String texto, String tipo, String codigo_titulo, String creation_user, String base ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_INSERT_PUBLICACION";
        String user_update = creation_user;
        
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, id_opcion );
            st.setString ( 3, texto );
            st.setString ( 4, tipo );
            st.setString ( 5, codigo_titulo );
            st.setString ( 6, creation_user );
            st.setString ( 7, user_update );
            st.setString ( 8, base );
            st.execute ();
            
            }} catch( Exception e ){
            
            throw new Exception ( "ERROR DURANTE LA INSERCION DE LA PUBLICACION " + e.getMessage () );
            
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }        
    }
    
    /**
     * Setter for property publicaciones.
     * @param plaman New value of property publicaciones.
     */
    public void setPublicaciones (java.util.Vector publicaciones) {
        
        this.publicaciones = publicaciones;
        
    }
    
    /**
     * Getter for property publicaciones.
     * @return Value of property publicaciones.
     */
    public java.util.Vector getPublicaciones () {
        
        return publicaciones;
        
    }
    
    /** Funcion publica que obtiene las publicaciones del usuario en session */
    public void listaPublicaciones ( String creation_user ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SELECT_PUBLICACION_X_USUARIO";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, creation_user );
            rs = st.executeQuery ();
            
            this.publicaciones = new Vector ();
            
            while( rs.next () ){
                
                pnv = new PublicacionNoVencida ();
                pnv.setTitulo ( rs.getString ("titulo") );
                pnv.setTexto ( rs.getString ("texto") );
                pnv.setFecha_creacion ( rs.getString ("fecha_creacion") );
                pnv.setUltima_modificacion ( rs.getString ("ultima_modificacion") );
                pnv.setOpcion ( Integer.parseInt ( rs.getString ("direccion") ) );
                
                publicaciones.add ( pnv );
                
            }
            
            }} catch( SQLException e ){
            
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LAS PUBLICACIONES DEL USUARIO" + e.getMessage () + " " + e.getErrorCode () );
            
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /** Funcion publica que obtiene una publicacion en especifico */
    public void obtenerPublicacion ( String dstrct, int id_opcion, String creation_date, String creation_user ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SELECT_PUBLICACION_ESPECIFICA";//JJCastro fase2
        
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, dstrct );
            st.setInt ( 2, id_opcion );
            st.setString ( 3, creation_date );
            st.setString ( 4, creation_user );
            
            rs = st.executeQuery ();
            
            this.publicaciones = new Vector ();
            
            while( rs.next () ){
                
                pnv = new PublicacionNoVencida ();
                pnv.setOpcion ( Integer.parseInt ( rs.getString ("direccion") ) );
                pnv.setNombre_dir ( rs.getString ("nombre_dir") );
                pnv.setTitulo ( rs.getString ("titulo") );
                pnv.setTexto ( rs.getString ("texto_completo") );
                pnv.setFecha_creacion ( rs.getString ("fecha_creacion") );
                pnv.setUltima_modificacion ( rs.getString ("ultima_modificacion") );
                
                publicaciones.add ( pnv );
                
            }
            
            }} catch( SQLException e ){
            
            throw new SQLException ( "ERROR DURANTE LA BUSQUEDA DE LA PUBLICACION ESPECIFICA" + e.getMessage () + " " + e.getErrorCode () );
            
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /** Funcion publica que modifica el texto de una publicacion en especifico */
    public void Update_Publicacion_Especifica ( String texto, String dstrct, int id_opcion, String creation_date, String creation_user ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_UPDATE_PUBLICACION_ESPECIFICA";//JJCastro fase2

        try {            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2;
            st.setString (1, texto);
            st.setString (2, dstrct);
            st.setInt (3, id_opcion);
            st.setString (4, creation_date);
            st.setString (5, creation_user);
            
            st.executeUpdate ();
            
            }} catch( SQLException e ){
            
            throw new SQLException ( "Error en Update_Publicacion_Especifica [PublicacionDAO]..." + e.getMessage () + " - " + e.getErrorCode () );
            
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /** Funcion publica que elimina el texto de una publicacion en especifico */
    public void Delete_Publicacion_Especifica ( String dstrct, int id_opcion, String creation_date, String creation_user ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_DELETE_PUBLICACION_ESPECIFICA";//JJCastro fase2

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1, dstrct);
            st.setInt (2, id_opcion);
            st.setString (3, creation_date);
            st.setString (4, creation_user);
            
            st.executeUpdate ();
            
            }} catch( SQLException e ){
            
            throw new SQLException ( "Error en Delete_Publicacion_Especifica [PublicacionDAO]..." + e.getMessage () + " - " + e.getErrorCode () );
            
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Getter for property publica.
     * @return Value of property publica.
     */
    public java.lang.String getPublica () {
        return publica;
    }
    
    /**
     * Setter for property publica.
     * @param publica New value of property publica.
     */
    public void setPublica (java.lang.String publica) {
        this.publica = publica;
    }
    
    /**
     * Metodo Buscar_Publicaciones ,
     * descripcion: busca publicaciones dependiendo del perfil
     * @autor : Ing. Andres Martinez
     * @version : 1.0
     */
    public void buscarPublicaciones (String distrito, String perfi,String usuari)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        VecPublicaciones = new Vector ();
        VecPublic = new Vector ();
        perfil=perfi;
        usuario=usuari;
        String query = "SQL_PUBLICACIONES_NO_VENCIDAS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1, distrito);
            st.setString (2, distrito);
            st.setString (3, usuari);
            rs = st.executeQuery ();
            while (rs.next ()){
                Publicacion p= new Publicacion ();
                VecPublic.add (p.load (rs));
            }

                if (VecPublic.size() > 0) {
                    int i = 0;
                    int ii = 0;
                    while (i < VecPublic.size()) {
                        Publicacion b = (Publicacion) VecPublic.get(i);
                        String titulo = b.getTitulo();
                        Vector a = new Vector();
                        for (ii = i; ii < VecPublic.size(); ii++) {
                            Publicacion bb = (Publicacion) VecPublic.get(ii);
                            if (titulo.equals(bb.getTitulo())) {
                                a.add(bb);
                            } else {
                                break;
                            }
                        }
                        VecPublicaciones.add(a);
                        i = ii;
                    }
                }
            }}catch(Exception e){
            e.printStackTrace ();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * CONTADOR_PUBLICACIONES
     * * descripcion: uctualiza el numero de revision de la publicacion del usuario en la tabla publicacion_novencida
     * @autor : Ing. Andres Martinez
     * @version : 1.0
     * @return :void
     */
    public void contadorPublicaciones () throws SQLException {
        Connection con= null;
        PreparedStatement ps = null;
        String query = "SQL_CONTADOR_PUBLICACIONES";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null){
                con.setAutoCommit (false);
                ps = con.prepareStatement (this.obtenerSQL ("SQL_CONTADOR_PUBLICACIONES"));
                for(int i=0; i<VecPublic.size ();i++){
                    ps.clearParameters ();
                    Publicacion p = (Publicacion)VecPublic.get (i);
                    ps.setInt (1,p.getNumeroVeces ()+1);
                    ps.setInt (2,p.getId_opcion ());
                    ps.setString (3,p.getFecha ());
                    ps.setString (4,usuario);
                    ps.addBatch ();
                }
                ////System.out.println (" query contador_publi: "+ps.toString ());
                ps.executeBatch ();
                con.commit ();
                con.setAutoCommit (true);
            }
        }catch(SQLException e){
            e.printStackTrace ();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * verificarpublicaciones
     * descripcion: CONSULTA EL CAMPO PUBLICACIONES DE LA TABLA USUARIOS PARA VERIFICAR SI EL USUARIO TIENE PUBLICACIONES
     * @autor : Ing. Andres Martinez
     * @version : 1.0
     * @return :void
     */
    public void verificarpublicaciones (String usuari) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        usuario=usuari;
        String query = "SQL_VERIFICAR_PUBLICACIONES";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1, usuari);
            rs = st.executeQuery ();
            while (rs.next ()){
                publica=rs.getString ("publicacion");
            }
            }}catch(Exception e){
            e.printStackTrace ();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * Getter for property VecPublicaciones.
     * @return Value of property VecPublicaciones.
     */
    public java.util.Vector getVecPublicaciones () {
        return VecPublicaciones;
    }
    
    /**
     * Setter for property VecPublicaciones.
     * @param VecPublicaciones New value of property VecPublicaciones.
     */
    public void setVecPublicaciones (java.util.Vector VecPublicaciones) {
        this.VecPublicaciones = VecPublicaciones;
    }
    
    
    /**
     * Metodo: insertarPublicacionNoVencida, permite ingresar un registro en la tabla publicacion_no_vencida de la BD.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String insertarPublicacionNoVencida () throws SQLException {

        StringStatement st = null;
        String sql ="";
        String query = "SQL_INGRESAR_PUBLICACION";//JJCastro fase2
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setInt    (1,  publicacion.getId_opcion ());
            st.setString (2,  publicacion.getUsuario ());
            st.setInt    (3,  publicacion.getNumeroVeces ());
            st.setString (4,  publicacion.getUsuario_creacion ());
            st.setString (5,  publicacion.getFecha_creacion ());
            st.setString (6,  publicacion.getBase ());
            st.setString (7,  publicacion.getDistrito ());
            sql = st.getSql();//JJCastro fase2
        }catch(Exception ex){
            throw new SQLException ("ERROR AL INGRESAR LAS PUBLICACIONES NO VENCIDAS, "+ex.getMessage ());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }
    
    /**
     * Metodo: eliminarPublicacionesNoVencidas, permite eliminar las publicaciones ya leidas
     * @autor : Ing. Jose de la rosa
     * @param : numero de la planilla, numero de la remesa, tipo de documento y documento
     * @version : 1.0
     */
    public String eliminarPublicacionesNoVencidas () throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_DELETE_PUBLICACIONES";
        try{
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            sql = st.getSql();//JJCastro fase2
        }catch(Exception ex){
            throw new SQLException ("ERROR AL INGRESAR LAS PUBLICACIONES NO VENCIDAS, "+ex.getMessage ());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }
    /**
     * Metodo: obtenerPublicacionesActuales, permite listar unas publicaciones que aun no han sido publicadas
     * @autor : Ing. Jose de la rosa
     * @param : fecha de ultima publicacion
     * @version : 1.0
     */
    public void obtenerPublicacionesActuales (String fecha) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_PUBLICACIONES_ACTUALES";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, fecha);
                rs = ps.executeQuery();
                VecPublicaciones = new Vector();

                while (rs.next()) {
                    publicacion = new Publicacion();
                    publicacion.setTipo(rs.getString("tipo"));
                    publicacion.setDescripcion(rs.getString("texto"));
                    publicacion.setTitulo(rs.getString("codigo_titulo"));
                    publicacion.setId_opcion(rs.getInt("id_opcion"));
                    publicacion.setFecha_creacion(rs.getString("creation_date"));
                    publicacion.setUsuario_creacion(rs.getString("creation_user"));
                    publicacion.setBase(rs.getString("base"));
                    publicacion.setUsuario(rs.getString("usuarios"));
                    publicacion.setDistrito(rs.getString("dstrct"));
                    VecPublicaciones.add(publicacion);
                }
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR LAS PUBLICACIONES NO LEIDAS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo: historialPublicaciones, permite listar unas publicaciones que aun no han sido publicadas
     * @autor : Ing. Jose de la rosa
     * @param : fecha de ultima publicacion
     * @version : 1.0
     */
    public void historialPublicaciones (String usuario) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;        
        String query = "SQL_HISTORIAL_PUBLICACIONES";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString (1, usuario);
            rs = ps.executeQuery ();
            vecDatos = new Vector();
            VecPublic2 = new Vector ();
            while (rs.next ()){
                Publicacion p= new Publicacion ();
                VecPublic2.add (p.load2 (rs));
            }
            if(VecPublic2.size ()>0){
                int i=0;
                int ii=0;
                while(i<VecPublic2.size ()){
                    Publicacion b=(Publicacion)VecPublic2.get (i);
                    String titulo = b.getTitulo ();
                    Vector a = new Vector ();
                    for(ii=i;ii<VecPublic2.size ();ii++){
                        Publicacion bb=(Publicacion)VecPublic2.get (ii);
                        if(titulo.equals (bb.getTitulo ())){
                            a.addElement (bb);
                        }
                        else
                            break;
                    }
                    vecDatos.addElement(a);
                    i=ii;
                }
                
            }
        }}catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR EL HISTORIAL DE LAS PUBLICACIONES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }    
    /**
     * Metodo: actualizarPublicacionUsuarios, permite actualizar si los usuarios tienen publicaciones o no en la tabla usuarios.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String actualizarPublicacionUsuarios () throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_PUBLICACION_USUARIOS";//JJCastro fase2

        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            sql = st.getSql();//JJCastro fase2
        }catch(Exception ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LAS PUBLICACIONES DE LOS USUARIOS, "+ex.getMessage ());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }
    
    
    /**
     * Metodo: getFechaUltimoCorte, permite obtiener la fecha del properties
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public String fechaCorte ( ){
        String fecha = "";
        try {
            FileReader fr = new FileReader ( getClass ().getResource ("general.properties").getPath () );
            LineNumberReader lnr = new LineNumberReader (fr);
            String linea = "";
            java.util.Hashtable tabla = new java.util.Hashtable ();
            do {
                linea = lnr.readLine ();
                if ( linea != null && !linea.startsWith ("#") ) {
                    String llave = linea.substring (0,linea.indexOf ("=")).trim ().replaceAll ("\\\\","");
                    String valor = linea.substring (linea.indexOf ("=")+1).trim ().replaceAll ("\\\\","");
                    tabla.put (llave, valor);
                }
                
            }
            while(linea != null);
            ////System.out.println (tabla.get ("fecha_ultima_montada"));
            lnr.close ();
            fr.close ();
            fecha = (String)tabla.get ("fecha_ultima_montada");
        }
        catch( Exception ex ){
            ex.printStackTrace ();
        }
        return fecha;
    }
    
    /**
     * Metodo: setFechaUltimoCorte, permite actualizar la fecha de corte en el properties
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void setFechaUltimoCorte (String fecha) throws Exception{
        try{
            //SE ACTUALIZAN LOS DATOS EN EL ARCHIVO DE CONFIGURACION..
            //Util u = new Util();
            // logger.info ("ultima_fecha_proceso_anticipo:"+ fecha);
            Properties dbProps = new Properties();
            dbProps.setProperty ("fecha_ultima_montada", fecha);
            java.net.URL u = getClass ().getResource ("general.properties");
            // logger.info (u.getPath ());
            dbProps.store (new FileOutputStream (u.getPath ()),"");
            
            
        }catch(IOException e){
            // logger.info ("ERROR ESCRIBIENDO EN EL ARCHIVO DE PROPIEDADES LA FECHA DE CORTE "+e.getMessage ());
            throw new IOException ("ERROR ESCRIBIENDO EN EL ARCHIVO DE PROPIEDADES LA FECHA DE CORTE "+e.getMessage ());
            
        }
    }
    
    /**
     * Getter for property publicacion.
     * @return Value of property publicacion.
     */
    public com.tsp.operation.model.beans.Publicacion getPublicacion () {
        return publicacion;
    }
    
    /**
     * Setter for property publicacion.
     * @param publicacion New value of property publicacion.
     */
    public void setPublicacion (com.tsp.operation.model.beans.Publicacion publicacion) {
        this.publicacion = publicacion;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario () {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario (java.lang.String usuario) {
        this.usuario = usuario;
    }
  
    /**
     * Getter for property vecDatos.
     * @return Value of property vecDatos.
     */
    public java.util.Vector getVecDatos () {
        return vecDatos;
    }    
    
    /**
     * Setter for property vecDatos.
     * @param vecDatos New value of property vecDatos.
     */
    public void setVecDatos (java.util.Vector vecDatos) {
        this.vecDatos = vecDatos;
    }
    
    public void reiniciar(){
        this.vecDatos = null;
    }
    
    public String esUsuarioConsorcio(String usuari) throws Exception{//090710
        Connection con = null;
        String respuesta="";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ES_USUARIO_CONSORCIO";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString (1, usuari);
            rs = ps.executeQuery ();
            if(rs.next ()){
                respuesta="S";
            }
        }}catch(SQLException ex){
            throw new SQLException ("ERROR AL esUsuarioConsorcio, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }
    
}
