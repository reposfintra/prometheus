/********************************************************************
 *      Nombre Clase.................   JspDAO.java    
 *      Descripci�n..................   DAO de la tabla jsp
 *      Autor........................   Rodrigo Salazar
 *      Fecha........................   10.07.2005
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.model.DAOS;

import java.io.*;
import javax.swing.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import org.apache.log4j.*;

public class JspDAO extends MainDAO {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private Jsp jsp;
    private Vector Jsps;
    private List tcontactos;
    
    /** Creates a new instance of JspDAO */
    public JspDAO() {
        super("JspDAO.xml");
    }
    
    /**
     * Obtiene la propiedad jsp
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Jsp getJsp() {
        return jsp;
    }
    
    /**
     * Establece el valor de la propiedad jsp
     * @param jsp Valor de la propiedad jsp
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public void setJsp(Jsp jsp) {
        this.jsp = jsp;
    }
    
    /**
     * Obtiene la propiedad jsps
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public Vector getJsps() {
        return Jsps;
    }
    
    /**
     * Establece el valor de la propiedad jsps
     * @param jsps Valor de la propiedad jsp
     * @autor Rodrigo Salazar
     * @version 1.0
     */
    public void setJsps(Vector Jsps) {
        this.Jsps = Jsps;
    }
    
    /**
     * Inserta un nuevi registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void insertJsp() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "SQL_INSERT";
        
        try{
            st = this.crearPreparedStatement(sql);
            st.setString(1,jsp.getCodigo());
            st.setString(2,jsp.getNombre());
            st.setString(3,jsp.getDescripcion());
            st.setString(4,jsp.getRuta());
            st.setString(5,jsp.getCreation_user());
            st.setString(6,jsp.getUser_update());
            st.setString(7,jsp.getCia());
            st.executeUpdate();
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL LA JSP" + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(sql);
        }
    }
    
    /**
     * Busca un registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @param usuario C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public void searchJsp(String usuario)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "SQL_SEARCH";
        
        try{
            st = this.crearPreparedStatement(sql);
            st.setString(1,usuario);
            rs= st.executeQuery();
            Jsps = new Vector();
            while(rs.next()){
                jsp = new Jsp();
                jsp.setCodigo(rs.getString("codigo"));
                jsp.setNombre(rs.getString("nombre"));
                jsp.setDescripcion(rs.getString("descripcion"));
                jsp.setRuta(rs.getString("ruta"));
                Jsps.add(jsp);
            }
            
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL TIPO DE UBICACI�N " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(sql);
        }
        
    }
    
    /**
     * Verifica la existencia de un registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @param codigo C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public boolean existJsp(String codigo) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "SQL_EXIST";
        boolean sw = false;
        
        try{
            st = this.crearPreparedStatement(sql);
            st.setString(1,codigo);
            rs = st.executeQuery();
            if (rs.next()){
                sw = true;
            }
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE existJsp(String codigo) " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(sql);
        }
        return sw;
    }
    
    
    /**
     * Lista todos los registros q no est�n anulados
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void listJsp()throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "SQL_LIST";
        boolean sw = false;
        
        try{
            st = this.crearPreparedStatement(sql);
            rs= st.executeQuery();
            Jsps = new Vector();
            while(rs.next()){
                jsp = new Jsp();
                jsp.setCodigo(rs.getString("codigo"));
                jsp.setNombre(rs.getString("nombre"));
                jsp.setDescripcion(rs.getString("descripcion"));
                jsp.setRuta(rs.getString("ruta"));
                Jsps.add(jsp);
            }
            
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA UNIDAD " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(sql);
        }
        
    }
    
    /**
     * Modifica un registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public void modificarJsp() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "SQL_UPDATE";
        
        try{
            st = this.crearPreparedStatement(sql);
            st.setString(1,jsp.getCodigo());
            st.setString(2,jsp.getNombre());
            st.setString(3,jsp.getDescripcion());
            st.setString(4,jsp.getRuta());
            st.setString(5,jsp.getUser_update());
            st.setString(6,jsp.getCia());
            st.setString(7,jsp.getCodigo());
            ////System.out.println(jsp.getCodigo()+""+jsp.getNombre()+""+jsp.getDescripcion()+""+jsp.getRuta());
            st.executeUpdate();
                
        } catch(SQLException e){
            throw new SQLException("ERROR AL MODIFICAR EL JSP" + e.getMessage()+"-" + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(sql);
        }
        
    }
    
    /**
     * Anula un registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @param usuario Login del usuario
     * @param cod C�digo de la p�gina JSP a anular
     * @throws SQLException
     * @version 1.0
     */
    public void anularJsp(String usu, String cod) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "SQL_ANULAR";
        
        try{
            st = this.crearPreparedStatement(sql);
            st.setString(1,usu);
            st.setString(2,cod);
            st.executeUpdate();
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE anularJsp(String usu, String cod) " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(sql);
        }
    }
    
    /**
     * Busca un registro en la tabla jsp
     * @autor Rodrigo Salazar
     * @param cod C�digo de la p�gina JSP
     * @param oag Nombre de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public Vector searchDetalleJsps(String cod, String pag) throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "SQL_DETALLE";
        Jsps=null;
        
        try{
            st = this.crearPreparedStatement(sql);
            st.setString(1, cod+"%");
            st.setString(2, pag+"%");
            
            ////System.out.println(st.toString());
            
            rs = st.executeQuery();
            Jsps = new Vector();
            while(rs.next()){
                jsp = new Jsp();
                jsp.setCodigo(rs.getString("codigo"));
                jsp.setNombre(rs.getString("nombre"));
                jsp.setDescripcion(rs.getString("descripcion"));
                jsp.setRuta(rs.getString("ruta"));
                jsp.setRec_status(rs.getString("rec_status"));
                jsp.setUser_update(rs.getString("user_update"));
                jsp.setCreation_user(rs.getString("creation_user"));
                Jsps.add(jsp);
            }
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE CONTACTO " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(sql);
        }
        
        return Jsps;
    }
    
    /**
     * Lista todos los registros de la tabla jsp que no est�n anulados
     * y los alamacena en un vector
     * @autor Rodrigo Salazar
     * @throws SQLException
     * @version 1.0
     */
    public Vector listarJsp()throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "SQL_DETALLE_2";
        Jsps=null;
        
        try{
            st = this.crearPreparedStatement(sql);
            String pag="", ruta="";
            
            rs = st.executeQuery();
            Jsps = new Vector();
            
            while(rs.next()){
                jsp = new Jsp();
                //jsp.loadCampos(rs);//tamatu 11.11.2005
                jsp.setCodigo(rs.getString("codigo"));
                pag = rs.getString("nombre") + "          ";
                ruta = rs.getString("ruta") + "          ";
                jsp.setNombre(pag.trim());
                jsp.setRuta(ruta.trim());
                //jsp.setNombre(pag.trim() + " [ " + ruta.trim() + " ]");//.substring(0,9)+"|"+ruta.substring(0,9));
                Jsps.add(jsp);
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE DOCS " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(sql);
        }
        
        return Jsps;
    }
    
    /**
     * Verifica la existencia de un registro en la tabla jsp
     * @autor Ing. Andr�s Maturana
     * @param codigo C�digo de la p�gina JSP
     * @throws SQLException
     * @version 1.0
     */
    public String existeJsp(String codigo) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "SQL_EXIST";
        String reg_status = null;
        
        try{
            st = this.crearPreparedStatement(sql);
            st.setString(1,codigo);
            rs = st.executeQuery();
            if (rs.next()){
                reg_status = rs.getString("rec_status");
            }
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE existeJsp(String codigo) " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar(sql);
        }
        
        return reg_status;
    }
    
    
}


