/*
 * ResponableActTrfDAO.java
 *
 * Created on 10 de septiembre de 2005, 11:07 AM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  INTEL
 */
public class ResponableActTrfDAO {
    
    private ResponsableActTrf responsable;
    private Vector vector;
    
    /** Creates a new instance of ResponableActTrfDAO */
    public ResponableActTrfDAO() {
    }
    public ResponsableActTrf getResponsableActTrf() {
        return responsable;
    }

    public void setResponsableActTrf(ResponsableActTrf responsable) {
        this.responsable = responsable;
    }

    public Vector getResponsableActTrfs() {
        return vector;
    }

    public void setResponsableActTrfs(Vector vector) {
        this.vector = vector;
    }
    
    public void insertResponsableActTrf() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("Insert into respact_trf (cod_resp,desc_resp,creation_user,creation_date,user_update,last_update,cia,rec_status,base) values(?,?,?,'now()',?,'now()',?,' ',?)");
                st.setString(1,responsable.getCodigo());
                st.setString(2,responsable.getDescripcion());
                st.setString(3,responsable.getCreation_user());
                st.setString(4,responsable.getUser_update());
                st.setString(5,responsable.getCia());
                st.setString(6,responsable.getBase()); 
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL RESPONSABLE ACT TRF " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void searchResponsableActTrf(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from respact_trf where cod_resp=? and rec_status != 'A'");
                st.setString(1,cod);
                rs= st.executeQuery();
                vector = new Vector();
                while(rs.next()){
                    responsable = new ResponsableActTrf();
                    responsable.setCodigo(rs.getString("cod_resp"));
                    responsable.setDescripcion(rs.getString("desc_resp"));
                    vector.add(responsable);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL RESPONSABLE ACT TRF " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public boolean existResponsableActTrf(String cod) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select * from respact_trf where cod_resp=? and rec_status != 'A'");
                st.setString(1,cod);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL RESOPONSABLE ACT TRF" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public void listResponsableActTrf()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from respact_trf where rec_status != 'A' order by cod_resp");
                rs= st.executeQuery();
                vector = new Vector();
                while(rs.next()){
                    responsable = new ResponsableActTrf();
                    responsable.setCodigo(rs.getString("cod_resp"));
                    responsable.setDescripcion(rs.getString("desc_resp"));
                    responsable.setBase(rs.getString("base"));
                    vector.add(responsable);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL RESPONSABLE ACT TRF " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    public void updateResponsableActTrf(String cod, String desc, String usu, String base) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update respact_trf set desc_resp=?, last_update='now()', user_update=?, rec_status=' ', base = ? where cod_resp = ? ");
                st.setString(1,desc);
                st.setString(2,usu);
                st.setString(3,base);
                st.setString(4,cod);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DEL RESPONSABLE ACT TRF" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void anularResponsableActTrf() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update respact_trf set rec_status='A',last_update='now()', user_update=? where cod_resp = ?");
                st.setString(1,responsable.getUser_update());
                st.setString(2,responsable.getCodigo());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL RESPONSABLE ACT TRF " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public Vector searchDetalleResponsableActTrfs(String cod, String desc, String base) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vector=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                st = con.prepareStatement("Select * from respact_trf where cod_resp like ? and desc_resp like ? and base like ? and rec_status != 'A'");
                st.setString(1, cod+"%");
                st.setString(2, desc+"%");
                st.setString(3, base+"%");
                rs = st.executeQuery();
                vector = new Vector();
                while(rs.next()){
                    responsable = new ResponsableActTrf();
                    responsable.setCodigo(rs.getString("cod_resp"));
                    responsable.setDescripcion(rs.getString("desc_resp"));
                    responsable.setRec_status(rs.getString("rec_status"));
                    responsable.setUser_update(rs.getString("user_update"));
                    responsable.setCreation_user(rs.getString("creation_user"));
                    responsable.setBase(rs.getString("base"));
                    vector.add(responsable);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS ACTIVIDADES" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return vector;
    }
    public Vector listarResponsableActTrf()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from respact_trf where rec_status != 'A' order by cod_resp");
                rs= st.executeQuery();
                vector = new Vector();
                while(rs.next()){
                    responsable = new ResponsableActTrf();
                    responsable.setCodigo(rs.getString("cod_resp"));
                    responsable.setDescripcion(rs.getString("desc_resp"));
                    responsable.setBase(rs.getString("base"));
                    vector.add(responsable);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL RESPONSABLE ACT TRF " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return vector;
    }
    
    public String obtenerResponsableActTrf(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String desc = "";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from respact_trf where cod_resp=?");
                st.setString(1,cod);
                rs= st.executeQuery();
                vector = new Vector();
                if(rs.next()){
                    desc = rs.getString("desc_resp");
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL RESPONSABLE ACT TRF " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return desc;
        
    }
}
