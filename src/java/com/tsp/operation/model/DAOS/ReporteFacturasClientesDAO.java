

/*********************************************************************************
 * Nombre clase :      ReporteFacturasClientesDAO.java                           *
 * Descripcion :       DAO del ReporteFacturasClientesDAO.java                   *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  *
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             LREALES                                                   *
 * Fecha :             29 de noviembre de 2006, 03:00 PM                         *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.UtilFinanzas.*;

public class ReporteFacturasClientesDAO extends MainDAO {
    
    private BeanGeneral bean;
    
    private Vector vector;
     
    private Vector otro_vector;
    
    private static final String SQL_OBSERVACIONES_FACTURA = "SQL_OBSERVACIONES_FACTURA";//NOmbre de la consulta en el archivo XML /*javier en 20080610*/
    private static final String SQL_FECHA_CONSIGNACION_FACTURA = "SQL_FECHA_CONSIGNACION_FACTURA";//Ultima fecha de consignacion /*javier en 20080610*/
    
    /** Creates a new instance of ReporteFacturasClientesDAO */
    public ReporteFacturasClientesDAO () {
        super ( "ReporteFacturasClientesDAO.xml" );
    }
    public ReporteFacturasClientesDAO (String dataBaseName) {
        super ( "ReporteFacturasClientesDAO.xml",dataBaseName );
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector () {
        
        return vector;
        
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector ( java.util.Vector vector ) {
        
        this.vector = vector;
        
    }
    
    /**
     * Getter for property otro_vector.
     * @return Value of property otro_vector.
     */
    public java.util.Vector getOtro_vector() {
        return otro_vector;
    }
    
    /**
     * Setter for property otro_vector.
     * @param otro_vector New value of property otro_vector.
     */
    public void setOtro_vector(java.util.Vector otro_vector) {
        this.otro_vector = otro_vector;
    }
    
    /** 
     * Funcion publica que actualiza la informacion de una entrada planeada especifica.
     *
     */
    /*modificado por javier en 20080610*/
    public void buscarFactura ( String distrito, String tipo_documento, String documento ) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_FACTURA";
               
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, distrito );
            st.setString( 2, tipo_documento );
            st.setString( 3, documento );//No de Factura
            //System.out.println( "+++ SQL_FACTURA: " + st.toString() );  
            
            String[] observaciones = this.getObservacionesFactura(documento);//Obtengo la lista de observaciones
            //System.out.println(observaciones.length);//El numero de observaciones....para control...
            
            rs = st.executeQuery();
            
            this.otro_vector = new Vector();
            
            //Deberia ser simplemente un condicional no un ciclo
            while ( rs.next() ) {
                       
                String codcli = rs.getString( "codcli" )!=null?rs.getString( "codcli" ).toUpperCase():""; 
                //System.out.println(codcli);
                String nombre_cliente = rs.getString( "nombre_cliente" )!=null?rs.getString( "nombre_cliente" ).toUpperCase():""; 
                String agencia_facturacion = rs.getString( "agencia_facturacion" )!=null?rs.getString( "agencia_facturacion" ).toUpperCase():""; 
                String moneda_fact = rs.getString( "moneda_fact" )!=null?rs.getString( "moneda_fact" ).toUpperCase():""; 
                String nit = rs.getString( "nit" )!=null?rs.getString( "nit" ).toUpperCase():""; 
                
                String fecha_factura = rs.getString( "fecha_factura" )!=null?rs.getString( "fecha_factura" ):""; 
                String valor_tasa = rs.getString( "valor_tasa" )!=null?rs.getString( "valor_tasa" ):""; 
                String forma_pago = rs.getString( "forma_pago" )!=null?rs.getString( "forma_pago" ).toUpperCase():""; 
                String plazo = rs.getString( "plazo" )!=null?rs.getString( "plazo" ):""; 
                String valor_factura = rs.getString( "valor_factura" )!=null?rs.getString( "valor_factura" ):""; 
                
                String descripcion_fact = rs.getString( "descripcion_fact" )!=null?rs.getString( "descripcion_fact" ).toUpperCase():""; 
                String observacion = rs.getString( "observacion" )!=null?rs.getString( "observacion" ).toUpperCase():""; 
                String item = rs.getString( "item" )!=null?rs.getString( "item" ):""; 
                String numero_remesa = rs.getString( "numero_remesa" )!=null?rs.getString( "numero_remesa" ).toUpperCase():""; 
                String creation_date = rs.getString( "creation_date" )!=null?rs.getString( "creation_date" ):""; 
                
                String descripcion_item = rs.getString( "descripcion_item" )!=null?rs.getString( "descripcion_item" ).toUpperCase():""; 
                String cantidad = rs.getString( "cantidad" )!=null?rs.getString( "cantidad" ):""; 
                String valor_unitariome = rs.getString( "valor_unitariome" )!=null?rs.getString( "valor_unitariome" ):""; 
                String moneda_item = rs.getString( "moneda_item" )!=null?rs.getString( "moneda_item" ).toUpperCase():""; 
                String valor_itemme = rs.getString( "valor_itemme" )!=null?rs.getString( "valor_itemme" ):""; 
                
                String codigo_cuenta_contable = rs.getString( "codigo_cuenta_contable" )!=null?rs.getString( "codigo_cuenta_contable" ).toUpperCase():""; 
                String concepto = rs.getString( "concepto" )!=null?rs.getString( "concepto" ).toUpperCase():""; 
                String auxiliar = rs.getString( "auxiliar" )!=null?rs.getString( "auxiliar" ).toUpperCase():""; 
                
                bean = new BeanGeneral ();
                
                //INFO CABECERA
                bean.setValor_01( codcli );
                bean.setValor_02( nombre_cliente );
                bean.setValor_03( agencia_facturacion );
                bean.setValor_04( moneda_fact );
                bean.setValor_05( nit );
                bean.setValor_06( fecha_factura.equals("0099-01-01")?"":fecha_factura );
                bean.setValor_07( valor_tasa );
                bean.setValor_08( forma_pago );
                bean.setValor_09( plazo );
                bean.setValor_10( valor_factura );                
                bean.setValor_11( descripcion_fact );
                bean.setValor_12( observacion );
                
                //INFO ITEMS
                bean.setValor_13( item );
                bean.setValor_14( numero_remesa );
                bean.setValor_15( creation_date.equals("0099-01-01 00:00:00")?"":creation_date );
                bean.setValor_16( descripcion_item );
                bean.setValor_17( cantidad );
                bean.setValor_18( valor_unitariome );
                bean.setValor_19( moneda_item );
                bean.setValor_20( valor_itemme );                
                bean.setValor_21( codigo_cuenta_contable );
                bean.setValor_22( concepto );
                bean.setValor_23( auxiliar );
                
                //INFO CAPTURADA
                bean.setValor_24( distrito );        // *Escogido por el usuario
                bean.setValor_25( tipo_documento );  // *Escogido por el usuario
                bean.setValor_26( documento );       // *Escogido por el usuario
                
                
                //INGRESAR UN ARRAY DE OBSERVACIONES EN EL BEAN_GENERAL
                bean.setV5(observaciones);//El metodo setV5 recibe un array de String por parametros
                
                this.otro_vector.add( bean );
                
            }
            
        }} catch( Exception e ) {            
            e.printStackTrace();            
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
             if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }

     /**
     * Funcion publica que obtiene la información que se va a generar
     * en el reporte de todas las facturas de un cliente. 
     */
    /*@modificado por Javier Martinez jemartinez en 20080610*/
    public void reporte ( String codcli, String fecini, String fecfin, String numfac, String numrem, String usu_creacion, String fec_creacion, String opcion,String prov,String est,String fven,String est1) throws Exception {
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        Connection con = null;
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        boolean error = true;
        
        try {
            
            String remplazar = "";
            
            if ( opcion.equals("1") ) {
                
                remplazar = "FROM " +
                                "con.factura f " +
                                "LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " +
                                "f.codcli = '" + codcli + "' " +
                                "AND f.creation_date BETWEEN '" + fecini + " 00:00:00' AND '" +fecfin + " 23:59:59'  ";// +
                                //"AND det.numero_remesa != '' ";
                
                error = false;
                
            } else if ( opcion.equals("2") ) {
                
                remplazar = "FROM " +
                                "con.factura f " +
                                "LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " +
                                "f.documento = '" + numfac + "'  ";// +
                                //"AND det.numero_remesa != '' ";
                
                error = false;
                
            } else if ( opcion.equals("3") ) {
                
                remplazar = "FROM " +
                                "con.factura_detalle fd " +
                                "INNER JOIN con.factura f ON ( f.dstrct = fd.dstrct AND f.tipo_documento = fd.tipo_documento AND f.documento = fd.documento ) " +
                                "LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " +
                                "fd.numero_remesa = '" + numrem + "' ";// +
                                //"AND det.numero_remesa != '' ";
                
                error = false;
                
            }else if ( opcion.equals("4") ) {
                
                String sql = "";
                
                if( usu_creacion.equals("") &&  usu_creacion.length() ==  0  ){
                    sql = "f.fecha_factura = '" + fec_creacion + "'";                    
                }
                
                if( fec_creacion.equals("") && fec_creacion.length() == 0 ){
                    sql = "f.creation_user = '" + usu_creacion + "' ";
                }
                
                else if ( !fec_creacion.equals("") && !usu_creacion.equals("") ){
                    sql = "f.creation_user = '" + usu_creacion + "' " +
                          "AND f.fecha_factura = '" + fec_creacion+"' ";
                }
                    
                remplazar = "FROM " +
                                " con.factura f " +
                                " LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " + sql;
                                
                
                error = false;
                
            }else if ( opcion.equals("5") ) {
                
                String sql = "";

                remplazar = "FROM " +
                                " con.factura f " +
                                " LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                                " LEFT JOIN negocios n on f.negasoc=n.cod_neg " +
                            "WHERE " + 
                           "n.nit_tercero = '" + prov + "' ";
                           if(est.equals("P"))
                           {
                               remplazar=remplazar+" AND f.valor_saldo > 0";
                           }else
                           {
                               if(est.equals("A"))
                               {
                                   remplazar=remplazar+" AND f.reg_status = 0";
                               }else
                               {
                                    if(est.equals("PG"))
                                    {
                                        remplazar=remplazar+" AND f.valor_saldo = 0";
                                    }
                               }
                           }
                                
                
                error = false;
                
            }else if ( opcion.equals("6") ) {
                
                remplazar = "FROM " +
                                "con.factura f " +
                                "LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " +
                                "f.fecha_vencimiento = '" + fven + "'  ";
                            if(est1.equals("P"))
                           {
                               remplazar=remplazar+" AND f.valor_saldo > 0";
                           }else
                           {
                               if(est1.equals("A"))
                               {
                                   remplazar=remplazar+" AND f.reg_status = 0";
                               }else
                               {
                                    if(est1.equals("PG"))
                                    {
                                        remplazar=remplazar+" AND f.valor_saldo = 0";
                                    }
                               }
                           }
                            
                error = false;
                
            }
            
            
            
            if ( !error ) {
            
                String agregar = "";

                String sql = this.obtenerSQL("SQL_REPORTE");

                agregar = sql.replaceAll("#FILTROS#", remplazar);
                
                con = this.conectarJNDI("SQL_REPORTE");//JJCastro fase2
                if(con!=null){
                st = con.prepareStatement( agregar ); 
                //System.out.println("query consulta cliente--->  "+st.toString());
                logger.info("?sql consulta fra cliente: " + st);
                rs = st.executeQuery();

                this.vector = new Vector();
                
                //Para cada documento se genera un BeanGeneral y se almacena el Objeto Vector declarado anteriormente
                while( rs.next() ){

                    String distrito = rs.getString( "dstrct" )!=null?rs.getString( "dstrct" ).toUpperCase():""; 
                    String tipo_documento = rs.getString( "tipo_documento" )!=null?rs.getString( "tipo_documento" ).toUpperCase():""; 
                    
                    String codigo_cliente = rs.getString( "codcli" )!=null?rs.getString( "codcli" ).toUpperCase():"";           
                    String nit_cliente = rs.getString( "nit" )!=null?rs.getString( "nit" ).toUpperCase():"";
                    String nombre_cliente = rs.getString( "nombre_cliente" )!=null?rs.getString( "nombre_cliente" ).toUpperCase():"";
                    String estado = rs.getString( "reg_status" )!=null?rs.getString( "reg_status" ).toUpperCase():"";
                    String factura = rs.getString( "documento" )!=null?rs.getString( "documento" ).toUpperCase():"";
                    
                    //Aqui hay que conseguir la fecha de consignacion del documento utilizando el string anterior llamado factura
                    String fechaConsignacion = this.getFechaConsignacionFactura(factura);
                    
                    
                    String valor_factura = rs.getString( "valor_factura" )!=null?rs.getString( "valor_factura" ):"0.00";
                    String valor_abono = rs.getString( "valor_abono" )!=null?rs.getString( "valor_abono" ):"0.00";                
                    String valor_saldo = rs.getString( "valor_saldo" )!=null?rs.getString( "valor_saldo" ):"0.00";
                    String moneda = rs.getString( "moneda" )!=null?rs.getString( "moneda" ).toUpperCase():"";
                    String fecha_factura = rs.getString( "fecha_factura" )!=null?rs.getString( "fecha_factura" ):"0099-01-01";
                    fecha_factura = fecha_factura.equals("0099-01-01")?"":fecha_factura;  
                    String fecha_vencimiento = rs.getString( "fecha_vencimiento" )!=null?rs.getString( "fecha_vencimiento" ):"0099-01-01";
                    fecha_vencimiento = fecha_vencimiento.equals("0099-01-01")?"":fecha_vencimiento;  
                    String fecha_ultimo_pago = rs.getString( "fecha_ultimo_pago" )!=null?rs.getString( "fecha_ultimo_pago" ):"0099-01-01";
                    fecha_ultimo_pago = fecha_ultimo_pago.equals("0099-01-01")?"":fecha_ultimo_pago;  
                    String descripcion = rs.getString( "descripcion" )!=null?rs.getString( "descripcion" ).toUpperCase():"";
                    
                    
                    /*fily steven ferNANDEZ*/
                    String fecha_impresion = rs.getString( "fecha_impresion" )!=null?rs.getString( "fecha_impresion" ):"";
                    fecha_impresion = fecha_impresion.equals("0099-01-01 00:00")?"":fecha_impresion;  
                    String fecha_anulacion = rs.getString( "fecha_anulacion" )!=null?rs.getString( "fecha_anulacion" ):"";
                    fecha_anulacion = fecha_anulacion.equals("0099-01-01 00:00")?"":fecha_anulacion; 
                    String fecha_conabilizacion = rs.getString( "fecha_contabilizacion" )!=null?rs.getString( "fecha_contabilizacion" ):""; 
                    fecha_conabilizacion = fecha_conabilizacion.equals("0099-01-01 00:00")?"":fecha_conabilizacion;
                    String trasaccion = rs.getString( "transaccion" )!=null?rs.getString( "transaccion" ):"";
                    
                    String usuario_creacion = ( rs.getString( "creation_user" )!=null?rs.getString( "creation_user" ):"" );
                    
                    ///rarp
                    String tel = ( rs.getString( "tel" )!=null?rs.getString( "tel" ):"" );
                    
                    //jemartinez
                    String dir = ( rs.getString( "dir" )!=null?rs.getString( "dir" ):"" );//Obtiene la direccion
                    
                    bean = new BeanGeneral ();

                    //bean.setValor_01( codcli );  // *Digitado por el usuario
                    bean.setValor_01( codigo_cliente );
                    bean.setValor_02( fecini );  // *Digitado por el usuario
                    bean.setValor_03( fecfin );  // *Digitado por el usuario

                    bean.setValor_04( nit_cliente );
                    bean.setValor_05( nombre_cliente );

                    if ( estado.equals("A") ) {
                        estado = "Anulada";
                    } else {                    
                        estado = valor_saldo.equals("0.00")?"Pagada":"Pendiente";                    
                    }

                    bean.setValor_06( estado );
                    bean.setValor_07( factura );
                    bean.setValor_08( valor_factura );
                    bean.setValor_09( valor_abono );
                    bean.setValor_10( valor_saldo );
                    bean.setValor_11( moneda );
                    bean.setValor_12( fecha_factura );
                    bean.setValor_13( fecha_vencimiento );                
                    bean.setValor_14( fecha_ultimo_pago );
                    bean.setValor_15( descripcion );

                    bean.setValor_16( numfac );  // *Digitado por el usuario
                    bean.setValor_17( numrem );  // *Digitado por el usuario
                    bean.setValor_18( opcion );  // *Digitado por el usuario
                    
                    bean.setValor_19( distrito );
                    bean.setValor_20( tipo_documento );
                    
                    /*fily steven fernandez*/
                    bean.setValor_21( fecha_impresion );
                    bean.setValor_22( fecha_anulacion );
                    bean.setValor_23( fecha_conabilizacion );
                    bean.setValor_24( trasaccion );
                    
                    bean.setOts(rs.getString("ots"));//AMATURANA
                    
                    //rrocha
                    bean.setValor_25(prov);
                    bean.setValor_26(est);
                    bean.setValor_27(tel);
                    
                    //jemartinez
                    bean.setValor_28(dir);//Aniade la direccion del cliente
                    
                    //jemartinez
                    bean.setValor_29(fechaConsignacion);//Aniade la fecha de consignacion
                    
                    //Jescandon
                    bean.setCreation_user(usuario_creacion);
                    
                    //AMATURANA 28.03.2007
                    bean.setFecha_anulacion(rs.getString("fecha_anulacion"));
                    if( bean.getFecha_anulacion().equals("0099-01-01 00:00") ){
                        bean.setFecha_anulacion("");
                    }
                    bean.setUsuario_anulo(rs.getString("usuario_anulo"));
                    bean.setValor_30(rs.getString("negasoc"));
                    this.vector.add( bean );

                }
                logger.info("? registros encontrados: " + this.vector.size());
            }
            
        }} catch( Exception e ) {            
            e.printStackTrace();
            throw new Exception("Error: "+e.getMessage());            
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
             if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }

    
     /**
     * Esta consulta privada obtiene las observaciones realizadas a una factura especifica
     * @param factura Numero de la Factura
     * @return una lista con todas las observaciones hechas a la factura
     * @throws Exception si ocurre un error en la consulta 
     */
    /*javier en 20080610*/
    private String[] getObservacionesFactura(String factura) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String[] observaciones = null;
        String query = "SQL_OBSERVACIONES_FACTURA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query),ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);//JJCastro fase2
                st.setString(1, factura);
                rs = st.executeQuery();//Ejecuta la consulta SQL_OBSERVACIONES_FACTURA
                rs.last();//Mueve el indice del Resultset al final
                observaciones = new String[rs.getRow()];//Inicializa el array de Strings que contendran las observaciones
                rs.beforeFirst();//Mueve el indice del ResultSet al principio
                int i = 0;
                while (rs.next()) {
                    observaciones[i] = rs.getString(1) + ";" + rs.getString(2) + ";" + rs.getString(3);
                    i++;
                }
                //Hasta este punto se tiene un array con las observaciones de una factura

            }
        }catch(Exception e){
            throw new Exception("Error en getObservacionesFactura(): "+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
             if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return observaciones;
    }
    
    /**
     * Este metodo devuelve la fecha de consignacion de una factura
     * @param numero de factura
     * @return fecha de consignacion de la factura
     * @throws SQLException si ocurre un error en la consulta SQL
     */
    /*author Javier Martinez jemartinez en 20080610*/
    private String getFechaConsignacionFactura(String noFactura) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String fechaConsignacion = "";
        String query = "SQL_FECHA_CONSIGNACION_FACTURA";
        try{
            //Genero la consulta a la base de datos para obtener la fecha de consignacion de la factura
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1,noFactura);
            rs = ps.executeQuery();//Executa la consulta
            //Solo interesa la ultima fecha de consignacion, por eso se toma solo la primera fila de la respuesta
            if(rs.next()){
                fechaConsignacion = rs.getString(1);
            }
            
            //Hasta este punto se tiene la ultima fecha de consignacion de una factura
            
        }}catch(SQLException e){
            throw new SQLException("Error con la base de datos: "+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
             if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return fechaConsignacion;
    }


   /**
    * 
    * @param codcli
    * @param fecini
    * @param fecfin
    * @param numfac
    * @param numrem
    * @param usu_creacion
    * @param fec_creacion
    * @param opcion
    * @param prov
    * @param est
    * @param fven
    * @param est1
    * @param multi
    * @throws Exception
    */
        public void reporte ( String codcli, String fecini, String fecfin, String numfac, String numrem, String usu_creacion, String fec_creacion, String opcion,String prov,String est,String fven,String est1,String multi) throws Exception {
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        Connection con = null;
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        boolean error = true;
        
        try {
            
            String remplazar = "";
            
            if ( opcion.equals("1") ) {
                
                remplazar = "FROM " +
                                "con.factura f " +
                                "LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " +
                                "f.codcli = '" + codcli + "' " +
                                "AND f.creation_date BETWEEN '" + fecini + " 00:00:00' AND '" +fecfin + " 23:59:59'  ";// +
                                //"AND det.numero_remesa != '' ";
                
                error = false;
                
            } else if ( opcion.equals("2") ) {
                
                remplazar = "FROM " +
                                "con.factura f " +
                                "LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " +
                                "f.documento = '" + numfac + "'  ";// +
                                //"AND det.numero_remesa != '' ";
                
                error = false;
                
            } else if ( opcion.equals("3") ) {
                
                remplazar = "FROM " +
                                "con.factura_detalle fd " +
                                "INNER JOIN con.factura f ON ( f.dstrct = fd.dstrct AND f.tipo_documento = fd.tipo_documento AND f.documento = fd.documento ) " +
                                "LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " +
                                "fd.numero_remesa = '" + numrem + "' ";// +
                                //"AND det.numero_remesa != '' ";
                
                error = false;
                
            }else if ( opcion.equals("4") ) {
                
                String sql = "";
                
                if( usu_creacion.equals("") &&  usu_creacion.length() ==  0  ){
                    sql = "f.fecha_factura = '" + fec_creacion + "'";                    
                }
                
                if( fec_creacion.equals("") && fec_creacion.length() == 0 ){
                    sql = "f.creation_user = '" + usu_creacion + "' ";
                }
                
                else if ( !fec_creacion.equals("") && !usu_creacion.equals("") ){
                    sql = "f.creation_user = '" + usu_creacion + "' " +
                          "AND f.fecha_factura = '" + fec_creacion+"' ";
                }
                    
                remplazar = "FROM " +
                                " con.factura f " +
                                " LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " + sql;
                                
                
                error = false;
                
            }else if ( opcion.equals("5") ) {
                
                String sql = "";

                remplazar = "FROM " +
                                " con.factura f " +
                                " LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                                " LEFT JOIN negocios n on f.negasoc=n.cod_neg " +
                            "WHERE " + 
                           "n.nit_tercero = '" + prov + "' ";
                           if(est.equals("P"))
                           {
                               remplazar=remplazar+" AND f.valor_saldo > 0";
                           }else
                           {
                               if(est.equals("A"))
                               {
                                   remplazar=remplazar+" AND f.reg_status = 0";
                               }else
                               {
                                    if(est.equals("PG"))
                                    {
                                        remplazar=remplazar+" AND f.valor_saldo = 0";
                                    }
                               }
                           }
                                
                
                error = false;
                
            }else if ( opcion.equals("6") ) {
                
                remplazar = "FROM " +
                                "con.factura f " +
                                "LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " +
                                "f.fecha_vencimiento = '" + fven + "'  ";
                            if(est1.equals("P"))
                           {
                               remplazar=remplazar+" AND f.valor_saldo > 0";
                           }else
                           {
                               if(est1.equals("A"))
                               {
                                   remplazar=remplazar+" AND f.reg_status = 0";
                               }else
                               {
                                    if(est1.equals("PG"))
                                    {
                                        remplazar=remplazar+" AND f.valor_saldo = 0";
                                    }
                               }
                           }
                            
                error = false;
                
            }else if ( opcion.equals("8") ) {
                
                remplazar = "FROM " +
                                "con.factura f " +
                                "LEFT JOIN con.factura_detalle det ON ( det.dstrct = f.dstrct AND det.tipo_documento = f.tipo_documento AND det.documento = f.documento  )" +
                            "WHERE " +
                                "f.descripcion like '%NUMERO OS:%" + multi + "%'  AND f.reg_status!='A' ";// +
                                //"AND det.numero_remesa != '' ";
                
                error = false;
                
            } 
            
            
            
            if ( !error ) {
            
                String agregar = "";

                String sql = this.obtenerSQL("SQL_REPORTE");

                agregar = sql.replaceAll("#FILTROS#", remplazar);
                
                con = this.conectarJNDI("SQL_REPORTE");//JJCastro fase2
                if(con!=null){
                st = con.prepareStatement( agregar ); 
                //System.out.println("query consulta cliente--->  "+st.toString());
                logger.info("?sql consulta fra cliente: " + st);
                rs = st.executeQuery();

                this.vector = new Vector();
                
                //Para cada documento se genera un BeanGeneral y se almacena el Objeto Vector declarado anteriormente
                while( rs.next() ){

                    String distrito = rs.getString( "dstrct" )!=null?rs.getString( "dstrct" ).toUpperCase():""; 
                    String tipo_documento = rs.getString( "tipo_documento" )!=null?rs.getString( "tipo_documento" ).toUpperCase():""; 
                    
                    String codigo_cliente = rs.getString( "codcli" )!=null?rs.getString( "codcli" ).toUpperCase():"";           
                    String nit_cliente = rs.getString( "nit" )!=null?rs.getString( "nit" ).toUpperCase():"";
                    String nombre_cliente = rs.getString( "nombre_cliente" )!=null?rs.getString( "nombre_cliente" ).toUpperCase():"";
                    String estado = rs.getString( "reg_status" )!=null?rs.getString( "reg_status" ).toUpperCase():"";
                    String factura = rs.getString( "documento" )!=null?rs.getString( "documento" ).toUpperCase():"";
                    
                    //Aqui hay que conseguir la fecha de consignacion del documento utilizando el string anterior llamado factura
                    String fechaConsignacion = this.getFechaConsignacionFactura(factura);
                    
                    
                    String valor_factura = rs.getString( "valor_factura" )!=null?rs.getString( "valor_factura" ):"0.00";
                    String valor_abono = rs.getString( "valor_abono" )!=null?rs.getString( "valor_abono" ):"0.00";                
                    String valor_saldo = rs.getString( "valor_saldo" )!=null?rs.getString( "valor_saldo" ):"0.00";
                    String moneda = rs.getString( "moneda" )!=null?rs.getString( "moneda" ).toUpperCase():"";
                    String fecha_factura = rs.getString( "fecha_factura" )!=null?rs.getString( "fecha_factura" ):"0099-01-01";
                    fecha_factura = fecha_factura.equals("0099-01-01")?"":fecha_factura;  
                    String fecha_vencimiento = rs.getString( "fecha_vencimiento" )!=null?rs.getString( "fecha_vencimiento" ):"0099-01-01";
                    fecha_vencimiento = fecha_vencimiento.equals("0099-01-01")?"":fecha_vencimiento;  
                    String fecha_ultimo_pago = rs.getString( "fecha_ultimo_pago" )!=null?rs.getString( "fecha_ultimo_pago" ):"0099-01-01";
                    fecha_ultimo_pago = fecha_ultimo_pago.equals("0099-01-01")?"":fecha_ultimo_pago;  
                    String descripcion = rs.getString( "descripcion" )!=null?rs.getString( "descripcion" ).toUpperCase():"";
                    
                    
                    /*fily steven ferNANDEZ*/
                    String fecha_impresion = rs.getString( "fecha_impresion" )!=null?rs.getString( "fecha_impresion" ):"";
                    fecha_impresion = fecha_impresion.equals("0099-01-01 00:00")?"":fecha_impresion;  
                    String fecha_anulacion = rs.getString( "fecha_anulacion" )!=null?rs.getString( "fecha_anulacion" ):"";
                    fecha_anulacion = fecha_anulacion.equals("0099-01-01 00:00")?"":fecha_anulacion; 
                    String fecha_conabilizacion = rs.getString( "fecha_contabilizacion" )!=null?rs.getString( "fecha_contabilizacion" ):""; 
                    fecha_conabilizacion = fecha_conabilizacion.equals("0099-01-01 00:00")?"":fecha_conabilizacion;
                    String trasaccion = rs.getString( "transaccion" )!=null?rs.getString( "transaccion" ):"";
                    
                    String usuario_creacion = ( rs.getString( "creation_user" )!=null?rs.getString( "creation_user" ):"" );
                    
                    ///rarp
                    String tel = ( rs.getString( "tel" )!=null?rs.getString( "tel" ):"" );
                    
                    //jemartinez
                    String dir = ( rs.getString( "dir" )!=null?rs.getString( "dir" ):"" );//Obtiene la direccion
                    
                    bean = new BeanGeneral ();

                    //bean.setValor_01( codcli );  // *Digitado por el usuario
                    bean.setValor_01( codigo_cliente );
                    bean.setValor_02( fecini );  // *Digitado por el usuario
                    bean.setValor_03( fecfin );  // *Digitado por el usuario

                    bean.setValor_04( nit_cliente );
                    bean.setValor_05( nombre_cliente );

                    if ( estado.equals("A") ) {
                        estado = "Anulada";
                    } else {                    
                        estado = valor_saldo.equals("0.00")?"Pagada":"Pendiente";                    
                    }

                    bean.setValor_06( estado );
                    bean.setValor_07( factura );
                    bean.setValor_08( valor_factura );
                    bean.setValor_09( valor_abono );
                    bean.setValor_10( valor_saldo );
                    bean.setValor_11( moneda );
                    bean.setValor_12( fecha_factura );
                    bean.setValor_13( fecha_vencimiento );                
                    bean.setValor_14( fecha_ultimo_pago );
                    bean.setValor_15( descripcion );

                    bean.setValor_16( numfac );  // *Digitado por el usuario
                    bean.setValor_17( numrem );  // *Digitado por el usuario
                    bean.setValor_18( opcion );  // *Digitado por el usuario
                    
                    bean.setValor_19( distrito );
                    bean.setValor_20( tipo_documento );
                    
                    /*fily steven fernandez*/
                    bean.setValor_21( fecha_impresion );
                    bean.setValor_22( fecha_anulacion );
                    bean.setValor_23( fecha_conabilizacion );
                    bean.setValor_24( trasaccion );
                    
                    bean.setOts(rs.getString("ots"));//AMATURANA
                    
                    //rrocha
                    bean.setValor_25(prov);
                    bean.setValor_26(est);
                    bean.setValor_27(tel);
                    
                    //jemartinez
                    bean.setValor_28(dir);//Aniade la direccion del cliente
                    
                    //jemartinez
                    bean.setValor_29(fechaConsignacion);//Aniade la fecha de consignacion
                    
                    //Jescandon
                    bean.setCreation_user(usuario_creacion);
                    
                    //AMATURANA 28.03.2007
                    bean.setFecha_anulacion(rs.getString("fecha_anulacion"));
                    if( bean.getFecha_anulacion().equals("0099-01-01 00:00") ){
                        bean.setFecha_anulacion("");
                    }
                    bean.setUsuario_anulo(rs.getString("usuario_anulo"));
                    bean.setValor_30(rs.getString("negasoc"));
                    this.vector.add( bean );

                }
                logger.info("? registros encontrados: " + this.vector.size());
            }
            
        }} catch( Exception e ) {
            
            e.printStackTrace();
            throw new Exception("Error: "+e.getMessage());
            
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
             if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }

    
}