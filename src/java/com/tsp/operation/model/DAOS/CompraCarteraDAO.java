/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

/**
 *
 * @author egonzalez
 */
public interface CompraCarteraDAO {

    /**
     *
     * @param ruta
     * @param fileCredito
     * @param filePersona
     * @param fileEstudiante
     * @param u
     * @return
     * @throws java.lang.Exception
     */
    public ArrayList<String> cargarCarteraNueva(String ruta, String fileCredito,
                          String filePersona, String fileEstudiante, Usuario u)throws Exception;
    
    /**
     *
     * @param query
     * @param parametro
     * @param u
     * @return
     */
    public String getInfoCargaCartera(String query, String parametro, Usuario u ); 
    
    /**
     *
     * @param u
     * @return
     */
    public String crearNuevoNegocioCartera(Usuario u);
    
    /**
     *
     * @param u
     * @param estado
     * @return
     */
    public String buscarNegocioCartera(Usuario u,String estado);
    
    /**
     *
     * @param u
     * @return
     */
    public String descartarNegocios(Usuario u);
    
    /**
     *
     * @param u
     * @return
     */
    public String eliminarCargaCartera(Usuario u);
    
    /**
     *
     * @param u
     * @return
     */
    public String crearDocumentosCartera(Usuario u);
}
