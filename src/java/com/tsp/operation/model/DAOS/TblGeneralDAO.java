/******************************************************************
* Nombre ......................TblGeneralDAO.java
* Descripci�n..................Clase DAO para tabla general
* Autor........................Armando Oviedo
* Fecha........................15/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;

public class TblGeneralDAO extends MainDAO {

    /** Creates a new instance of TblGeneralDAO */
    public TblGeneralDAO() {
     super("TblGeneralDAO.xml");//JJCastro fase2
    }
    public TblGeneralDAO(String dataBaseName) {
     super("TblGeneralDAO.xml", dataBaseName);//JJCastro fase2
    }

    
    private TblGeneral tg;
    private Connection con;
    private PoolManager pm; 
    private Vector tblgens;
    private TreeMap lista_des;
    private TreeMap info;//Henry
    
    /****  SQL QUERIES *****/
    
    //Karen
    
     private static String SQL_EXISTE_CODIGO = "SELECT 	a.table_code as codigo,  " +
                                      "	      a.DESCRIPCION," +
                                      "	      A.DATO" +
                                      " FROM  tablagen a," +
                                      "	      tablagen_prog b" +
                                      " WHERE 	a.REG_STATUS!='A' " +
                                      "	       AND a.table_type=? " +
                                      "	       and a.table_type=b.table_type" +
                                      "	       and a.table_code = b.table_code" +
                                      "	       AND b.program =? AND UPPER(b.table_code) =? ";
     
    private static String SQL_BUSCAR_DESCRIPCIONES = "SELECT 	a.table_code as codigo,  " +
                                      "	      a.DESCRIPCION," +
                                      "	      A.DATO," +
                                      "       a.referencia" +
                                      " FROM  tablagen a," +
                                      "	      tablagen_prog b" +
                                      " WHERE 	a.REG_STATUS!='A' " +
                                      "	       AND a.table_type=? " +
                                      "	       and a.table_type=b.table_type" +
                                      "	       and a.table_code = b.table_code" +
                                      "	       AND b.program =? ";
    
    private static String SQL_CODIGO = "    SELECT 	table_code as codigo        "+
                                       "        FROM  tablagen                      "+
                                       "        WHERE 	REG_STATUS!='A'             "+
                                       "        AND table_type =   ?                "+
                                       "        AND table_code =   ?                ";



public boolean existeRegistro(String tabla, String codigo)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        lista_des = new TreeMap();
        boolean flag = false;
        String query = "SQL_CODIGO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,tabla);
                st.setString(2,codigo);
                
                rs = st.executeQuery();
                if( rs.next()  )
                    flag = true;
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CODIGOS " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return flag;
    }

 
    

    
    /**
     * M�todo que setea un objeto TblGeneral
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......TblGeneral de
     **/     
    public void setTG(TblGeneral tg){
        this.tg = tg;
    }
    
    /**
     * M�todo que retorna un objeto TblGeneral
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......TblGeneral de
     **/     
    public TblGeneral getTblGeneral(){
        return this.tg;
    }
    
  /**
    * obtTablaGen
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public Vector obtTablaGen(){
        return this.tblgens;
    }
    
   /**
     * M�todo que guetea todas los detalles de tblgeneral
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @return......Vector de objetos TblGeneral
     **/     
    public Vector getTodosTblGeneral(){
        return this.tblgens;
    }        
    
        /**
     * M�todo que obtiene el atributo Lista_des
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public java.util.TreeMap getLista_des() {
        return lista_des;
    }
    
    /**
     * M�todo que setea el atributo Lista_des
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void setLista_des(java.util.TreeMap lista_des) {
        this.lista_des = lista_des;
    }
    
    /**
     * M�todo que busca una lista de codigo y descripcion dado un codigo de tabla
     * y un codigo de programa
     * @param.......Recibe un codigo de tabla y un codigo de programa
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void buscarLista(String tabla, String programa)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        lista_des = new TreeMap();
        String query = "SQL_BUSCAR_DESCRIPCIONES";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,tabla);
                st.setString(2,programa);
                
                rs = st.executeQuery();
                lista_des.put(" Seleccione un Item","");
                while(rs.next()){
                    lista_des.put(rs.getString("descripcion"), rs.getString("codigo")+"/"+rs.getString("dato"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CIUDADES " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    /**
     * M�todo que busca una lista de codigo y descripcion dado un codigo de tabla
     * y un codigo de programa
     * @param.......Recibe un codigo de tabla y un codigo de programa
     * @autor.......Leonardo Parody
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void buscarListaCodigo(String tabla, String programa)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        lista_des = new TreeMap();
        String query = "SQL_BUSCAR_DESCRIPCIONES";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,tabla);
                st.setString(2,programa);
                
                rs = st.executeQuery();
                lista_des.put(" Sin Seleccion","");
                while(rs.next()){
                    String descripcion = rs.getString("descripcion"); 
                    String codigo =  rs.getString("codigo");
                    lista_des.put(descripcion,codigo);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CIUDADES " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * M�todo que busca una lista de codigo y descripcion dado un codigo de tabla
     * y un codigo de programa
     * @param.......Recibe un codigo de tabla y un codigo de programa
     * @autor.......Ivan Gomez
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public void buscarListaSinDato(String tabla, String programa)throws SQLException{

        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        lista_des = new TreeMap();
        String query = "SQL_BUSCAR_DESCRIPCIONES";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,tabla);
                st.setString(2,programa);

                rs = st.executeQuery();
                lista_des.put(" Seleccione un Item","");
                while(rs.next()){
                    lista_des.put(rs.getString("descripcion"), rs.getString("codigo"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CIUDADES " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
      /**
     * M�todo que verifica si existe el dato de un programa en tablagen_Prog para una tabla dada
     * @param tabla el nombre de la tabla
     * @param programa el nombre del programa
     * @param codigo el codigo del programa buscado
     * @return Si existen datos
     * @autor David Pi�a Lopez     
     **/     
    public boolean existeCodigo( String tabla, String programa, String codigo )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTE_CODIGO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,tabla);
                st.setString(2,programa);
                st.setString(3,codigo);                
                rs = st.executeQuery();                
                while( rs.next() ){
                    existe = true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR en TblGeneralDAO----> existeCodigo( String tabla, String programa, String codigo ) " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;
    }
    
    
    
     /**
     * M�todo que busca una lista de codigo y descripcion dado un codigo de tabla
     * y un codigo de programa
     * @param.......Recibe un codigo de tabla y un codigo de programa
     * @autor.......Ivan Gomez
     * @throws......SQLException
     * @version.....1.0.     
     **/     
    public List buscarAutXCP(String tabla, String programa)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista = new LinkedList();
        String query = "SQL_BUSCAR_DESCRIPCIONES";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,tabla);
                st.setString(2,programa);
                
                rs = st.executeQuery();
                TablaGen t = new TablaGen();
                  t.setTable_code("");
                  t.setReferencia("");
                  t.setDescripcion("");
                  lista.add(t);
                while(rs.next()){
                      t = new TablaGen();
                      t.setTable_code(rs.getString("codigo"));
                      t.setReferencia(rs.getString("referencia"));
                      t.setDescripcion(rs.getString("descripcion"));
                      lista.add(t);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CIUDADES " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
       return lista;    
        
    }
    
    
}
