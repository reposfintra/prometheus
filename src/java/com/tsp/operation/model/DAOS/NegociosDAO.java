/**
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : DAO que maneja los avales de Fenalco
**/
package com.tsp.operation.model.DAOS;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.finanzas.contab.model.DAO.CmcDAO;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import java.sql.Connection;
import java.sql.ResultSet;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.services.ChequeXFacturaService;
import com.tsp.operation.model.services.InteresesFenalcoService;
import com.tsp.operation.model.services.TasaService;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class NegociosDAO extends MainDAO  {

        //private Aval_Fenalco Aval;
    //private Vector Avales;
    private Vector Datos;
    private Negocios Neg;
    private BeanGeneral bg;
    private Vector Datos2;
    private TreeMap Cust;
        private String bcop="";
    private Vector afil;
        private BeanGeneral letras=null;
    private ArrayList<DocumentosNegAceptado> liquidacion;
    private ArrayList<DocumentosNegAceptado> liquidacionAval;
    private Negocios negocio;
    private Negocios negocioAval;
    private com.itextpdf.text.Font fEncabezado;
    private BaseColor cTitulo;
    private ArrayList chequesx;
    private ArrayList<Negocios> facturas;

    private static final String OBSERVACIONES_FACTURA_FENALCO = "OBSERVACIONES_FACTURA_FENALCO";//Nombre de la consulta de las observaciones para las ND
    private static final String OBSERVACIONES_FACTURA_FENALCO_2 = "OBSERVACIONES_FACTURA_FENALCO_2";//Nombre de la consulta de las observaciones para PL

        
    //variable Global.
        private double valor_cxp=0;


    public NegociosDAO() {
        super("NegociosDAO.xml");
        //ystem.out.println("Entra aqui");
    }

    public NegociosDAO(String dataBaseName) {
        super("NegociosDAO.xml", dataBaseName);
    }

    /**
     * Trae el numero de liquidaciones de un negocio
     * @autor Iris Vargas
     * @throws SQLException
     */
    public int maxDocForm(int solc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int max = 0;
        String query = "SQL_MAX_DOCS_FORM";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setInt(1, solc);
                rs = st.executeQuery();
                if (rs.next()) {
                    max = rs.getInt("max");
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR SQL_MAX_DOCS_FORM " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return max;
    }
public void NegociosUpdateDS(String cod_neg,String actividad,String estado)throws SQLException, Exception{

        Connection con=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        String query="ACTUALIZAR_NEGOCIO_DS";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con=conectarJNDI(query);
            ps=con.prepareStatement(sql);
            ps.setString(1, actividad);
            ps.setString(2, estado);///
            ps.setString(3, cod_neg);
            ps.executeUpdate();

        }
        catch (Exception e) {
            System.out.println("Error al actualizar negocio: "+e.toString());
            e.printStackTrace();

        }
      finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }

    }

    /**
     * Atualiza el negocio cuando ya ha ocurrido una liquidacion.
     * @autor Iris Vargas
     * @throws SQLException
     */
    public void NegociosUpdate(Negocios neg, String numero_solicitud, String ciclo, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws SQLException {

        ResultSet rs = null;
        String nombre_sql = "SQL_UPDATE_NEGOCIOS";
        StringStatement sst2 = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();

        try {
            sst2 = new StringStatement(this.obtenerSQL("SQL_DEL_ANEX_TERCERO"), true);
            sst2.setString(1, neg.getCod_negocio());
            batch.add(sst2.getSql());
        } catch (Exception ex) {
            System.out.println("error por codneg" + ex.toString() + "__" + ex.getMessage());
            ex.printStackTrace();
            throw new SQLException(ex.getMessage());
        } finally {
            if (sst2 != null) {
                try {
                    sst2 = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL STRINGSTATEMENT " + e.getMessage());
                }
            }
        }

        //Segunda parte
        if (numero_solicitud != null) {
            try {
                sst2 = new StringStatement(this.obtenerSQL("SQL_ADD_INF_METROTEL"), true);
                sst2.setString(1, numero_solicitud);
                sst2.setString(2, ciclo);
                sst2.setString(3, neg.getCod_negocio());
                batch.add(sst2.getSql());
            } catch (Exception ex) {
                System.out.println("error por codneg" + ex.toString() + "__" + ex.getMessage());
                ex.printStackTrace();
                throw new SQLException(ex.getMessage());
            } finally {
                if (sst2 != null) {
                    try {
                        sst2 = null;
                    } catch (Exception e) {
                        throw new SQLException("ERROR CERRANDO EL STRINGSTATEMENT " + e.getMessage());
                    }
                }
            }
        }
//Cierre Segunda parte

//Tercera parte
        try {

            sst2 = new StringStatement(this.obtenerSQL(nombre_sql), true);
            sst2.setString(1, neg.getCod_cli());
            sst2.setString(2, Double.toString(neg.getVr_negocio()));
            sst2.setString(3, Integer.toString(neg.getNodocs()));
            sst2.setString(4, Double.toString((numero_solicitud == null) ? neg.getVr_desem() : neg.getVr_negocio()));
            sst2.setString(5, Double.toString(neg.getVr_aval()));
            sst2.setString(6, Double.toString(neg.getVr_cust()));
            sst2.setString(7, neg.getMod_aval());
            sst2.setString(8, neg.getMod_cust());
            sst2.setString(9, Double.toString(neg.getPor_rem()));
            sst2.setString(10, neg.getMod_rem());
            sst2.setString(11, neg.getCreation_user());
            sst2.setString(12, neg.getFpago());
            sst2.setString(13, neg.getTneg());
            sst2.setString(14, neg.getCod_tabla());
            sst2.setString(15, neg.getEsta());
            sst2.setString(16, neg.getFecha_neg());
            sst2.setString(17, neg.getNitp());
            sst2.setString(18, Double.toString(neg.getTotpagado()));
            sst2.setString(19, neg.getvalor_aval());
            sst2.setString(20, neg.getvalor_remesa());
            sst2.setDouble(21, neg.getVr_aval());
            sst2.setInt(22, neg.getId_convenio());
            sst2.setString(23, neg.getCodigoBanco());
            sst2.setString(24, neg.getId_sector());
            sst2.setString(25, neg.getId_subsector());
            sst2.setString(26, neg.getEstado());
            sst2.setString(27, neg.getCod_negocio());
            String sql="";
            if(neg.getId_remesa()==0){
                sql=sst2.getSql().replace("IDREME", "null");
                }else{
                sql=sst2.getSql().replace("IDREME", ""+neg.getId_remesa());
            }
            batch.add(sql);
            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
            batch.add(daotraza.insertNegocioTrazabilidad(negtraza));

            if(neg.getEstado().equals("V")){
                batch.add(daotraza.updateActividad(neg.getCod_negocio(),"DEC"));
            }

            sst2 = new StringStatement(this.obtenerSQL("SQL_DELETE_DOCS"), true);
            sst2.setString(1, neg.getCod_negocio());
            batch.add(sst2.getSql());
            sst2 = null;

            for (int i = 0; i < chequesx.size(); i++) {
                chequeCartera chequeCarterax = (chequeCartera) chequesx.get(i);
                sst2 = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS"), true);

                sst2.setString(1, neg.getCod_negocio());
                sst2.setString(2, chequeCarterax.getItem());
                sst2.setString(3, chequeCarterax.getFechaCheque());
                sst2.setString(4, chequeCarterax.getDias());
                sst2.setString(5, chequeCarterax.getSaldoInicial());
                sst2.setString(6, chequeCarterax.getValorCapital());
                sst2.setString(7, chequeCarterax.getValorInteres());
                sst2.setString(8, chequeCarterax.getValor());
                sst2.setString(9, chequeCarterax.getSaldoFinal());
                sst2.setString(10, chequeCarterax.getNoAval());
                batch.add(sst2.getSql());
                sst2 = null;
            }
            sst2 = null;
            sst2 = new StringStatement(this.obtenerSQL("SQL_ANUL_DOCS_FORM"), true);
            sst2.setString(1,  numero_form);
            batch.add(sst2.getSql());
            int max=this.maxDocForm(Integer.parseInt(numero_form));
            for (int i = 0; i < chequesx.size(); i++) {
                chequeCartera chequeCarterax = (chequeCartera) chequesx.get(i);
                sst2 = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_FORM"), true);
                sst2.setString(1, numero_form);
                sst2.setString(2, chequeCarterax.getItem());
                sst2.setString(3, chequeCarterax.getValor());
                sst2.setString(4, chequeCarterax.getFechaCheque());
                sst2.setInt(5, (max+1));
                sst2.setString(6, neg.getCreation_user());
                batch.add(sst2.getSql());
                sst2 = null;
            }
            sst2 = null;

            batch.add(this.updateFormulario(neg, numero_form));

            sst2 = null;
            sst2 = new StringStatement(this.obtenerSQL("SQL_ELIMINAR_COMISIONES"), true);
            sst2.setString(1,  neg.getCod_negocio());
            batch.add(sst2.getSql());
            Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.getDatabaseName());
            Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(neg.getDist(), convenio.getImpuesto(), neg.getFecha_neg());
            double porcImpuesto = impuesto.getPorcentaje1() / 100;
            for (int i = 0; i < convenio.getConvenioComision().size(); i++) {
                double vlrComision = neg.getTotpagado() * (convenio.getConvenioComision().get(i).getPorcentaje_comision() / 100);
                double impComision = vlrComision * porcImpuesto;
                sst2 = null;
                sst2 = new StringStatement(this.obtenerSQL("SQL_INSERTAR_COMISION"), true);
                sst2.setString(1, convenio.getId_convenio());
                sst2.setString(2, convenio.getConvenioComision().get(i).getId_comision());
                sst2.setString(3, neg.getCod_negocio());
                sst2.setDouble(4, convenio.getConvenioComision().get(i).getPorcentaje_comision());
                sst2.setDouble(5, vlrComision + impComision);
                sst2.setString(6, neg.getCreation_user());
                batch.add(sst2.getSql());
            }


            neg = null;
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE NEGOCIO " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (sst2 != null) {
                try {
                    sst2 = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            //if (con != null){ try{ this.desconectar(con);  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            //rmartinez 2010-05-28
        }

//Cierre Tercera parte

        try {

            apdao.ejecutarSQL(batch);


        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE ACTUALIZACION DE NEGOCIOS" + e.getMessage());
        }


    }

    /*
     * Moficado: 26 Marzo 2010
     * Ing. Jose Castro
     * Conexiones JNDI

     */
    /*
     * Moficado: 26 Marzo 2010
     * Ing. Jose Castro
     * Conexiones JNDI

     */
    public void NegociosSave(Negocios neg, String numero_solicitud, String ciclo, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws SQLException {
        ResultSet rs = null;
        String nombre_sql = "";
        StringStatement sst2 = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();
        if(chequesx==null){
            chequesx=new ArrayList();
        }

        //Segunda parte
        if (numero_solicitud != null) {
            try {
                sst2 = new StringStatement(this.obtenerSQL("SQL_ADD_INF_METROTEL"), true);
                sst2.setString(1, numero_solicitud);
                sst2.setString(2, ciclo);
                sst2.setString(3, neg.getCod_negocio());
                batch.add(sst2.getSql());
            } catch (Exception ex) {
                System.out.println("error por codneg" + ex.toString() + "__" + ex.getMessage());
                ex.printStackTrace();
                throw new SQLException(ex.getMessage());
            } finally {
                if (sst2 != null) {
                    try {
                        sst2 = null;
                    } catch (Exception e) {
                        throw new SQLException("ERROR CERRANDO EL STRINGSTATEMENT " + e.getMessage());
                    }
                }
            }
        }
//Cierre Segunda parte

//Tercera parte
        try {
            if ((neg.getTneg()).equals("02")) {
                nombre_sql = "SQL_INSERTAR_NEGOCIOS";
            } else {
                nombre_sql = "SQL_INSERTAR_NEGOCIOS2";
            }
            sst2 = new StringStatement(this.obtenerSQL(nombre_sql), true);

            sst2.setString(1, neg.getCod_cli());
            sst2.setString(2, Double.toString(neg.getVr_negocio()));
            sst2.setString(3, Integer.toString(neg.getNodocs()));
            sst2.setString(4, Double.toString((numero_solicitud == null) ? neg.getVr_desem() : neg.getVr_negocio()));
            sst2.setString(5, Double.toString(neg.getVr_aval()));
            sst2.setString(6, Double.toString(neg.getVr_cust()));
            sst2.setString(7, neg.getMod_aval());
            sst2.setString(8, neg.getMod_cust());
            sst2.setString(9, Double.toString(neg.getPor_rem()));
            sst2.setString(10, neg.getMod_rem());
            sst2.setString(11, neg.getCreation_user());
            sst2.setString(12, neg.getFpago());
            sst2.setString(13, neg.getTneg());
            sst2.setString(14, neg.getCod_tabla());
            sst2.setString(15, neg.getEsta());
            sst2.setString(16, neg.getFecha_neg());
            sst2.setString(17, neg.getNitp());
            sst2.setString(18, Double.toString(neg.getTotpagado()));
            if ((neg.getTneg()).equals("02")) {
                sst2.setString(19, neg.getNitp());
                sst2.setString(20, neg.getNitp());
                sst2.setString(21, neg.getCod_negocio());
                sst2.setString(22, neg.getvalor_aval());
                sst2.setString(23, neg.getvalor_remesa());
                sst2.setDouble(24, neg.getVr_aval());
                sst2.setInt(25, neg.getId_convenio());
            } else {
                sst2.setString(19, neg.getCod_negocio());
                sst2.setString(20, neg.getvalor_aval());
                sst2.setString(21, neg.getvalor_remesa());
                sst2.setDouble(22, neg.getVr_aval());
                sst2.setString(23, neg.getCnd_aval());
                sst2.setInt(24, neg.getId_convenio());
                sst2.setString(25, neg.getCodigoBanco());
            }
            sst2.setString(26, neg.getId_sector());
            sst2.setString(27, neg.getId_subsector());
            sst2.setString(28, neg.getEstado());
            sst2.setString(29, neg.getTipoProceso());
            sst2.setString(30, negtraza.getActividad());
            sst2.setString(31, neg.getNegocio_rel());
            sst2.setString(32, String.valueOf(neg.isFinanciaAval()));
            String sql="";
            if(neg.getId_remesa()==0){
                sql=sst2.getSql().replace("IDREME", "null");
                }else{
                sql=sst2.getSql().replace("IDREME", ""+neg.getId_remesa());
            }
            batch.add(sql);

            for (int i = 0; i < chequesx.size(); i++) {
                chequeCartera chequeCarterax = (chequeCartera) chequesx.get(i);
                sst2 = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS"), true);

                sst2.setString(1, neg.getCod_negocio());
                sst2.setString(2, chequeCarterax.getItem());
                sst2.setString(3, chequeCarterax.getFechaCheque());
                sst2.setString(4, chequeCarterax.getDias());
                sst2.setString(5, chequeCarterax.getSaldoInicial());
                sst2.setString(6, chequeCarterax.getValorCapital());
                sst2.setString(7, chequeCarterax.getValorInteres());
                sst2.setString(8, chequeCarterax.getValor());
                sst2.setString(9, chequeCarterax.getSaldoFinal());
                sst2.setString(10, chequeCarterax.getNoAval());
                batch.add(sst2.getSql());
                sst2 = null;
            }
            sst2 = null;
            sst2 = new StringStatement(this.obtenerSQL("SQL_DEL_DOCS_FORM"), true);
            sst2.setString(1,  numero_form);
            batch.add(sst2.getSql());

            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
            negtraza.setCodNeg(neg.getCod_negocio());
                batch.add( daotraza.insertNegocioTrazabilidad(negtraza));

            for (int i = 0; i < chequesx.size(); i++) {
                chequeCartera chequeCarterax = (chequeCartera) chequesx.get(i);
                sst2 = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_FORM"), true);

                sst2.setString(1, numero_form);
                sst2.setString(2, chequeCarterax.getItem());
                sst2.setString(3, chequeCarterax.getValor());
                sst2.setString(4, chequeCarterax.getFechaCheque());
                sst2.setInt(5, 1);
                sst2.setString(6, neg.getCreation_user());
                batch.add(sst2.getSql());
                sst2 = null;
            }
            sst2 = null;
            batch.add(this.updateFormulario(neg, numero_form));
            Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.getDatabaseName());
            Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(neg.getDist(), convenio.getImpuesto(), neg.getFecha_neg());
            double porcImpuesto = impuesto.getPorcentaje1() / 100;
            for (int i = 0; i < convenio.getConvenioComision().size(); i++) {

                double vlrComision = neg.getTotpagado() * (convenio.getConvenioComision().get(i).getPorcentaje_comision()/100);
                double impComision = vlrComision * porcImpuesto;
                sst2 = null;
                sst2 = new StringStatement(this.obtenerSQL("SQL_INSERTAR_COMISION"), true);
                sst2.setString(1, convenio.getId_convenio());
                sst2.setString(2, convenio.getConvenioComision().get(i).getId_comision());
                sst2.setString(3,neg.getCod_negocio());
                sst2.setDouble(4, convenio.getConvenioComision().get(i).getPorcentaje_comision());
                sst2.setDouble(5, vlrComision+impComision);
                sst2.setString(6,neg.getCreation_user());
                batch.add(sst2.getSql());
            }
            neg = null;
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE LAS INSERCION DE NEGOCIOS " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (sst2 != null) {
                try {
                    sst2 = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            //if (con != null){ try{ this.desconectar(con);  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            //rmartinez 2010-05-28
        }

//Cierre Tercera parte

        try {

            apdao.ejecutarSQL(batch);


        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE INSERCION DE FACTURAS" + e.getMessage());
        }


    }

    /*
     * Moficado: 2010
     * Ing. Jose Castro
     * Conexiones JNDI

     */
public void ListadoDatos2(String ckest,String est,String ckdate,String fi,String ff,String ckfap,String fi1,String ff1,String ckfde,String fi2,String ff2,String cknitc,String nitc,String cknomc,String nomc,String cknitcod,String nitcod,String cknomcod,String nomcod,String user, String vista) throws Exception{
            PreparedStatement ps       = null;
            PreparedStatement ps2   = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        Datos = new Vector();
            String remp="";
//            if(vista.equals("2")){
//               remp=" n.create_user = '"+user+"'";
//            }else{
               remp="n.cod_neg in (select cod_neg from negocios where n.nit_tercero in (select distinct pc.nit_proveedor from usuario_prov_convenio upc join prov_convenio pc on "
                    + "upc.id_prov_convenio = pc.id_prov_convenio where upc.reg_status != 'A'and upc.idusuario = '"+user+"' or n.nit_tercero in ('8901009858','8904800244')))";
//            }

            String tab="";
        Connection con = null;
            int sw=0;
            if( ckest  !=null )
            {
                tab="Negocios n";
                remp += " AND n.estado_neg ='"+ est     +"'";
        }
            if( ckdate  !=null )
            {
                tab="Negocios n";
                remp += " AND n.fecha_negocio between '"+fi+"' and '"+ff+" 23:59"+"'";
        }

             if( ckfap  !=null )
            {
                tab="Negocios n";
                remp += " AND n.fecha_ap between '"+fi1+"' and '"+ff1+" 23:59"+"'";
        }

             if( ckfde  !=null )
            {
                tab="Negocios n";
                remp += " AND n.f_desem between '"+fi2+"' and '"+ff2+" 23:59"+"'";
        }

            if( cknitc  !=null )
            {
                tab="Negocios n";
                remp += " AND n.cod_cli ='"+ nitc     +"'";
        }

            if( cknitcod  !=null )
            {
                tab="Negocios n";
            //remp += " AND n.id_codeudor ='"+ nitcod     +"'";
            remp += " AND n.cod_neg in (select cod_neg from solicitud_aval sa "
			+"inner join solicitud_persona sp " 
                        +"ON sp.numero_solicitud=sa.numero_solicitud AND sp.tipo in ('E','C') " 
                        +"AND sp.reg_status!='A' AND sp.identificacion='"+ nitcod +"' " 
                        +"WHERE sa.reg_status!='A')";
        }

            if( cknomc  !=null )
            {
                if (sw==0)
                {   sw=2;
                    tab="Negocios n Inner Join Cliente c On n.cod_cli=c.nit ";
            }
                else
                {   sw=3;
                    tab="(Negocios n Inner Join Proveedor p On n.nit_tercero=p.nit)inner join Cliente c on n.cod_cli=c.nit ";
        }
                remp += " AND upper(c.nomcli) like upper('"+ nomc +"%')";
            }

            if( cknomcod  !=null )
            {
                if (sw==0)
                {
                    tab="Negocios n Inner Join solicitud_persona cd On n.id_codeudor=cd.identificacion ";
                    }
                else
                {   if(sw==1)
                    {   tab="(Negocios n Inner Join Proveedor p On n.nit_tercero=p.nit)Inner Join solicitud_persona cd On n.id_codeudor=cd.identificacion ";
                    }
                    else
                    {   if(sw==2)
                        {   tab="(Negocios n Inner Join Cliente c On n.cod_cli=c.nit) Inner Join solicitud_persona cd On n.id_codeudor=cd.identificacion";
                        }
                        else
                        {   tab="((Negocios n Inner Join Proveedor p On n.nit_tercero=p.nit)inner join Cliente c on n.cod_cli=c.nit) Inner Join codeudor cd On n.id_codeudor=cd.id";
                        }

                }

            }
                remp += " AND upper(cd.nombre) like upper('"+ nomcod +"%')";
        }


        try {
            String agregar = "";
            String sql = this.obtenerSQL("SQL_MOSTRAR_NEGOCIOS_2");
            agregar = sql.replaceAll("#TAB#", tab).replaceAll("#COND#", remp);
            //con = this.conectar("SQL_MOSTRAR_NEGOCIOS_2");
            con = this.conectarJNDI("SQL_MOSTRAR_NEGOCIOS_2");//JJCastro fase2
            ps = con.prepareStatement(agregar);
            rs = ps.executeQuery();

            while (rs.next()) {
                Neg = new Negocios();
                Neg.setNom_cli(rs.getString("get_nombc"));
                Neg.setCod_cli(rs.getString("cod_cli"));
                Neg.setNom_cod(rs.getString("nomcod"));
                Neg.setCod_cod(rs.getString("id_codeudor"));
                Neg.setCod_negocio(rs.getString("cod_neg"));
                Neg.setCod_tabla(rs.getString("cod_tabla"));
                Neg.setFecha_neg(rs.getString("fecha_negocio"));
                Neg.setFpago(rs.getString("fpago"));
                Neg.setTneg(rs.getString("tneg"));
                Neg.setMod_aval(rs.getString("mod_aval"));
                Neg.setMod_cust(rs.getString("mod_custodia"));
                Neg.setMod_rem(rs.getString("mod_remesa"));
                Neg.setNodocs(rs.getInt("nro_docs"));
                Neg.setVr_negocio(Double.valueOf(rs.getString("vr_negocio")).doubleValue());
                Neg.setVr_desem(Double.valueOf(rs.getString("vr_desembolso")).doubleValue());
                Neg.setVr_aval(rs.getFloat("vr_aval"));
                Neg.setVr_cust(rs.getFloat("vr_custodia"));
                Neg.setPor_rem(rs.getFloat("porc_remesa"));
                Neg.setEstado(rs.getString("estado_neg"));
                Neg.setNitp(rs.getString("niter"));
                Neg.setEsta(rs.getString("esta"));
                Neg.setObs(rs.getString("obs"));
                Neg.setBcod(rs.getString("nit_tercero"));
                Neg.setFechatran(rs.getString("f_desem"));
                Neg.setCmc(rs.getString("cmc"));
                Neg.setConlet(rs.getString("cletras"));
                Neg.setPagare(rs.getString("cpagare"));
                Neg.setCodigoBanco(rs.getString("banco_cheque"));////SE AGREGO ESTA LINEA
                Neg.setCnd_aval(rs.getString("cnd_aval"));
                Neg.setCreation_user(rs.getString("create_user"));
                Neg.setPrograma(rs.getString("programa"));
                Neg.setCodigou(rs.getString("codigou"));

                Neg.setFecha_ap(rs.getString("fecha_ap"));
                Neg.setCiclo(rs.getString("ciclo"));
                Neg.setNo_solicitud(rs.getString("no_solicitud"));
                Neg.setFormulario(rs.getString("form"));

                Neg.setId_convenio(rs.getInt("id_convenio"));
                Neg.setVr_negocio_solicitado(Double.parseDouble(rs.getString("vr_negocio_solicitado")));
                Neg.setNodocs_solicitado(rs.getInt("nro_docs"));
                Neg.setTipo_credito(rs.getString("tipo_credito"));
                Neg.setAgencia(rs.getString("agencia"));
                
                String sql2 = this.obtenerSQL("SQL_TIENE_AJUSTE");
                ps2 = con.prepareStatement(sql2);
                ps2.setString(1, Neg.getCreation_user());
                rs2 = ps2.executeQuery();
                if (rs2.next()) {
                    Neg.setAjuste(true);
                } else {
                    Neg.setAjuste(false);
                }
                Datos.add(Neg);
                Neg = null;
            if (rs2  != null){ try{ rs2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps2  != null){ try{ ps2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            //this.desconectar("SQL_TIENE_AJUSTE");

            //Datos.add(Neg);
                //Neg = null;
            }
    }
    catch (Exception ex)
    {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
    }finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (rs2  != null){ try{ rs2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (ps2  != null){ try{ ps2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO2 " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }

    }

    /*
     * Moficado: 2010
     * Ing. Jose Castro
     * Conexiones JNDI

     */

      public void ListadoDatos(String ckest,String est,String ckdate,String fi,String ff,String ckfap,String fi1,String ff1,String ckfde,String fi2,String ff2,String cknita, String nita,String cknoma, String noma,String cknitc,String nitc,String cknomc,String nomc,String cknitcod,String nitcod,String cknomcod,String nomcod,String ckcodxc,String codxc,String vista) throws Exception{
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;

        ResultSet rs = null;
        ResultSet rs2 = null;
        Datos = new Vector();
            String remp="";
            String tab="";
        Connection con = null;
            int sw=0;


           if(vista.equals("15")){
               tab="Negocios n";
               remp=" and n.estado_neg='R' ";
        }



            if( ckest  !=null )
            {
                tab="Negocios n";
                remp += " AND n.estado_neg ='"+ est     +"'";
        }
            if( ckdate  !=null )
            {
                tab="Negocios n";
                remp += " AND n.fecha_negocio  between '"+fi+"' and '"+ff+" 23:59"+"'";
        }
            if( ckfap  !=null )
            {
                tab="Negocios n";
                remp += " AND n.fecha_ap between '"+fi1+"' and '"+ff1+" 23:59"+"'";
        }
             if( ckfde  !=null )
            {
                tab="Negocios n";
                remp += " AND n.f_desem between '"+fi2+"' and '"+ff2+" 23:59"+"'";
        }
            if( cknita  !=null )
            {
                tab="Negocios n";
                remp += " AND n.nit_tercero ='"+ nita     +"'";
        }
            if( cknitc  !=null )
            {
                tab="Negocios n";
                remp += " AND n.cod_cli ='"+ nitc     +"'";
        }
            if( cknitcod  !=null )
            {
                tab= "Negocios n";
            remp += " and vista.identificacion ='" + nitcod + "' ";
//                remp +=" AND n.cod_neg in (SELECT cod_neg FROM solicitud_aval sa"
//                     +" INNER JOIN solicitud_persona sp ON sp.numero_solicitud=sa.numero_solicitud AND tipo in ('E','C')"
//                    +" AND identificacion ='"+nitcod+"')";
        }
            if( cknoma  !=null )
            {
                tab="Negocios n Inner Join Proveedor p On n.nit_tercero=p.nit ";
                remp += " AND upper(p.payment_name) like upper('"+ noma +"%')";
                sw=1;
        }

            if( ckcodxc  !=null )
            {
                tab="Negocios n";
                remp += " AND n.cod_neg ='"+ codxc     +"'";
        }

            if( cknomc  !=null )
            {
                if (sw==0)
                {
                    tab="Negocios n Inner Join Cliente c On n.cod_cli=c.nit ";
                    sw=2;
            }
                else
                {
                    tab="(Negocios n Inner Join Proveedor p On n.nit_tercero=p.nit)inner join Cliente c on n.cod_cli=c.nit ";
                    sw=3;
        }
                remp += " AND upper(c.nomcli) like upper('"+ nomc +"%')";
            }

            if( cknomcod  !=null )
            {
                if (sw==0)
                {
                    tab="Negocios n Inner Join solicitud_persona cd On n.id_codeudor=cd.identificacion";
                    }
                else
                {   if(sw==1)
                    {   tab="(Negocios n Inner Join Proveedor p On n.nit_tercero=p.nit)Inner Join solicitud_persona cd On n.id_codeudor=cd.identificacion ";
                    }
                    else
                    {   if(sw==2)
                        {   tab="(Negocios n Inner Join Cliente c On n.cod_cli=c.nit) Inner Join solicitud_persona cd On n.id_codeudor=cd.identificacion";
                        }
                        else
                        {   tab="((Negocios n Inner Join Proveedor p On n.nit_tercero=p.nit)inner join Cliente c on n.cod_cli=c.nit) Inner Join solicitud_persona cd On n.id_codeudor=cd.identificacion";
                        }

                }

            }
                remp += " AND upper(cd.nombre) like upper('"+ nomcod +"%')";
        }


            try{
            String agregar = "";
            String sql = this.obtenerSQL("SQL_MOSTRAR_NEGOCIOS");
            agregar = sql.replaceAll("#TAB#", tab).replaceAll("#COND#", remp);
            //con = this.conectar("SQL_MOSTRAR_NEGOCIOS");
            con = this.conectarJNDI("SQL_MOSTRAR_NEGOCIOS");
                ps = con.prepareStatement( agregar );
            rs = ps.executeQuery();

                while (rs.next())
                {
                Neg = new Negocios();
                Neg.setNom_cli(rs.getString(1));
                Neg.setCod_cli(rs.getString("cod_cli"));
                Neg.setNom_cod(rs.getString("nomcod"));
                Neg.setCod_cod(rs.getString("id_codeudor"));
                Neg.setCod_negocio(rs.getString("cod_neg"));
                Neg.setCod_tabla(rs.getString("cod_tabla"));
                Neg.setFecha_neg(rs.getString("fecha_negocio"));
                Neg.setFpago(rs.getString("fpago"));
                Neg.setTneg(rs.getString("tneg"));
                Neg.setMod_aval(rs.getString("mod_aval"));
                Neg.setMod_cust(rs.getString("mod_custodia"));
                Neg.setMod_rem(rs.getString("mod_remesa"));
                Neg.setNodocs(rs.getInt("nro_docs"));
                Neg.setVr_negocio(Double.valueOf(rs.getString("vr_negocio")).doubleValue());
                Neg.setVr_desem(Double.valueOf(rs.getString("vr_desembolso")).doubleValue());
                Neg.setVr_aval(rs.getFloat("vr_aval"));
                Neg.setVr_cust(rs.getFloat("vr_custodia"));
                Neg.setPor_rem(rs.getFloat("porc_remesa"));
                Neg.setEstado(rs.getString("estado_neg"));
                Neg.setNitp(rs.getString("niter"));
                Neg.setEsta(rs.getString("esta"));
                Neg.setObs(rs.getString("obs"));
                Neg.setBcod(rs.getString("nit_tercero"));
                Neg.setFechatran(rs.getString("f_desem"));
                Neg.setCmc(rs.getString("cmc"));
                Neg.setConlet(rs.getString("cletras"));
                Neg.setTotpagado(Double.parseDouble(rs.getString("tot_pagado")));
                Neg.setNumaval(rs.getString("num_aval"));
                Neg.setPagare(rs.getString("cpagare"));
                Neg.setCodigoBanco(rs.getString("banco_cheque"));///SE AGREGO ESTA LINEA
                Neg.setCnd_aval(rs.getString("cnd_aval"));
                Neg.setCreation_user(rs.getString("create_user"));
                Neg.setPrograma(rs.getString("programa"));
                Neg.setCodigou(rs.getString("codigou"));

                Neg.setFormulario(rs.getString("form"));
                Neg.setFecha_ap(rs.getString("fecha_ap"));
                Neg.setCiclo(rs.getString("ciclo"));
                Neg.setNo_solicitud(rs.getString("no_solicitud"));
                Neg.setNom_convenio(rs.getString("convenio"));
                Neg.setSector(rs.getString("sector"));
                Neg.setSubsector(rs.getString("subsector"));
                Neg.setId_convenio(rs.getInt("id_convenio"));
                Neg.setTipoConv(rs.getString("tipo_conv"));
                Neg.setFecha_liquidacion(rs.getString("fecha_liquidacion"));
                Neg.setVr_negocio_solicitado(Double.parseDouble(rs.getString("vr_negocio_solicitado")));
                Neg.setNodocs_solicitado(rs.getInt("nro_docs"));
                Neg.setValor_total_poliza(rs.getInt("valor_total_poliza"));
                Neg.setValor_renovacion(rs.getDouble("valor_renovacion"));
                Neg.setTipo_credito(rs.getString("tipo_credito"));
                Neg.setAgencia(rs.getString("agencia"));
                Neg.setAsesor_asignado( rs.getString("asesor_asignado"));
                Neg.setPreaprobado_ex_cliente(rs.getString("pre_aprobado_ex_cliente"));

                String sql2 = this.obtenerSQL("SQL_TIENE_AJUSTE");
                    ps2=con.prepareStatement(sql2);
                    ps2.setString(1,Neg.getCreation_user());
                    rs2=ps2.executeQuery();
                    if(rs2.next())
                    {   Neg.setAjuste(true);
                }
                    else
                    {   Neg.setAjuste(false);
                    }
                Datos.add(Neg);
                Neg = null;
                     if (rs2  != null){ try{ rs2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                     if (ps2  != null){ try{ ps2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    //if (rs2!=null) rs2.close();
                //if (ps2!=null) ps2.close();
                //this.desconectar("SQL_TIENE_AJUSTE");

                    //Datos.add(Neg);
                //Neg = null;
            }
            }
            catch (Exception ex)
            {   System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (rs2  != null){ try{ rs2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (ps2  != null){ try{ ps2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO2 " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }

    }

    /*
     * Moficado: 2010
     * Ing. Jose Castro 
     * Conexiones JNDI
    
     */

    public void dennyneg(String ob,String cod,String est)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR_RECHAZO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ob);
                st.setString(2, est);
                st.setString(3, cod);
                st.executeUpdate();
            }
        }
        catch(SQLException e)
        {
            //ystem.out.println("Entra en el error 177");
            throw new SQLException("ERROR DURANTE LAS ANULACION DE NEGOCIOS " + e.getMessage() + " " + e.getErrorCode());
         }finally{
            if (st  != null){ try{ st.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }

    }

    public void habReliquidar(String cod, String tasa) throws SQLException, Exception {
        StringStatement st = null;
        Vector batch = new Vector();
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        try {
            st = new StringStatement(this.obtenerSQL("SQL_HAB_RELIQUIDAR_NEG"), true);
            st.setString(1, tasa);
            st.setString(2, cod);
            batch.add(st.getSql());
            st = null;

            st = new StringStatement(this.obtenerSQL("SQL_HAB_RELIQUIDAR_FORM"), true);
            st.setString(1, cod);
            batch.add(st.getSql());
            st = null;
            apdao.ejecutarSQL(batch);
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE HABILITAR RELIQUIDAR" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
    }

    /*
     * Moficado: 2010
     * Ing. Jose Castro
     * Conexiones JNDI

     */
     public void admitneg(String u,String ob,String cod,String nuav,String bcoch)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR_APROBACION";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, u);
                st.setString(2, ob);
                st.setString(3, nuav);
                st.setString(4, bcoch);
                st.setString(5, cod);
                st.executeUpdate();
            }
         }
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE LAS APROBACION DE NEGOCIOS " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (st  != null){ try{ st.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        //ystem.out.println("El nit q le  llega ++++");
    }



    /*
     * Moficado: 2010
     * Ing. Jose Castro
     * Conexiones JNDI

     */

      public void admitneg(String u,String ob,String cod,String nuav,String bcoch, String numeroCuentaCheque)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR_APROBACION";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, u);
                st.setString(2, ob);
                st.setString(3, nuav);
                st.setString(4, bcoch);
                st.setString(5, numeroCuentaCheque);//Aniadido por jemartinez
                st.setString(6, cod);
                st.executeUpdate();
            }
          }
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE LAS APROBACION DE NEGOCIOS " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (st  != null){ try{ st.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            }




    /**
     *
     * @param
     * @return
     * @throws Exception
     */


      public String admitneg(String u,String ob,String cod,String nuav,String bcoch, String numeroCuentaCheque,String estado_neg)throws SQLException{
          String sql="";
        String query = "SQL_ACTUALIZAR_APROBACION";
          StringStatement st  = null;
            try
            {
                st = new StringStatement (this.obtenerSQL(query), true);
            st.setString(1, u);
            st.setString(2, ob);
                st.setString(3,estado_neg);/////SE AGREGO ESTA LINEA
            st.setString(4, nuav);
            st.setString(5, bcoch);
            st.setString(6, numeroCuentaCheque);//Aniadido por jemartinez
            st.setString(7, cod);
                sql=st.getSql();
                System.out.println("sql1111"+sql);
                }
            catch(Exception e)
            {
                 throw new SQLException("ERROR DURANTE LAS APROBACION DE NEGOCIOS " + e.getMessage() );
             }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }


    //090717miguel
public String fact(BeanGeneral bg)throws SQLException{


            StringStatement up  = null;
            StringStatement st  = null;
            StringStatement detfac  = null;
            StringStatement cxp  = null;
            StringStatement cxp_items  = null;
            StringStatement cxp2  = null;
            StringStatement cxp_items2  = null;
            StringStatement ingfen  = null;

            InteresesFenalcoService interesesFenalcoService=new InteresesFenalcoService(this.getDatabaseName());
            String sql="";
            String sqlintfen="";
            String query="SQL_INSERTAR_FACTURA";
            String query2="SQL_INSERTAR_DET_FACTURA";

            try
            {
            String no = "FAC";
            String no2 = "";
            if (bg.getValor_05().equals("CH")) {
                no = "REDCHEQUE";
            } else {
                if (bg.getValor_05().equals("LT")) {
                    no = "REDLETRA";
                } else {
                    if (bg.getValor_06().substring(0, 2).equals("NF")) {
                        no = "FCMET";
                    } else {
                        no = "REDPAGARE";
                    }
                }
            }
                up = new StringStatement (this.obtenerSQL("SQL_UP"), true);//JJCastro fase2

            for (int i = 1; i < (Integer.parseInt(bg.getValor_02())) + 1; i++) {
                st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                detfac = new StringStatement(this.obtenerSQL(query2), true);//JJCastro fase2
                st.setString(1, "FAC");
                st.setString(2, no);
                st.setString(3, String.valueOf(i));
                st.setString(4, bg.getValor_01());
                st.setString(5, bg.getValor_01());
                st.setString(6, bg.getValor_05());
                st.setString(7, (bg.getV2())[i]);
                st.setString(8, (bg.getV1())[i]);
                st.setString(9, no);
                st.setString(10, bg.getValor_03());
                st.setString(11, bg.getValor_04());
                st.setString(12, bg.getValor_03());
                st.setString(13, bg.getValor_03());
                st.setString(14, bg.getValor_03());
                st.setString(15, bg.getValor_06());
                st.setString(16, bg.getValor_07());
                st.setString(17, (bg.getV4())[i - 1]);
                st.setString(18, "");
                st.setString(19, "");
                if (!bg.getValor_06().substring(0, 2).equals("NF")) {
                    st.setString(20, "01");
                } else {
                    st.setString(20, "04");
                }
                ///detalle
                detfac.setString(1, "FAC");
                detfac.setString(2, no);
                detfac.setString(3, String.valueOf(i));
                detfac.setString(4, String.valueOf(i));
                detfac.setString(5, bg.getValor_01());
                detfac.setString(6, bg.getValor_05());
                detfac.setString(7, bg.getValor_05());
                detfac.setString(8, bg.getValor_03());
                detfac.setString(9, bg.getValor_03());
                detfac.setString(10, bg.getValor_04());
                detfac.setString(11, bg.getValor_03());
                detfac.setString(12, bg.getValor_03());
                detfac.setString(13, no);
                detfac.setString(14, String.valueOf(i));
                detfac.setString(15, bg.getValor_07());
                detfac.setString(16, bg.getValor_08());
                if (!bg.getValor_06().substring(0, 2).equals("NF")) {
                    detfac.setString(17, "13109602");
                } else {
                    detfac.setString(17, "13109902");
                }
                sql += st.getSql() + ";";
                sql += detfac.getSql() + ";";
            }
                up.setString(1,no);
            //almaceno en la otra tabla
                String codip="";
                try{codip=UpCP((bg.getValor_06().substring(0,2).equals("NF"))?"FACTMET":"FACTPROV");}catch(Exception e){throw new SQLException();}
                 cxp                     = new StringStatement (this.obtenerSQL("INSERT_CXP_DOC"), true);//JJCastro fase2
                 cxp_items     = new StringStatement (this.obtenerSQL("INSERT_CXP_DOC_ITEMS"), true);//JJCastro fase2
                 cxp2                  = new StringStatement (this.obtenerSQL("INSERT_NC_DOC"), true);//JJCastro fase2
                 cxp_items2  = new StringStatement (this.obtenerSQL("INSERT_NC_DOC_ITEMS"), true);//JJCastro fase2

            double vn = Double.valueOf(bg.getValor_10()).doubleValue();
            double vd = Double.valueOf(bg.getValor_11()).doubleValue();
            double vs = 0;
            double vs2 = 0;
            if (bg.getValor_14() != null && bg.getValor_14().equals("on")) {
                vs = Double.valueOf(bg.getValor_15()).doubleValue();
            }
            if (bg.getValor_06().substring(0, 2).equals("NF")) {
                bg.setValor_14("off");
                vs = Double.valueOf(bg.getValor_15()).doubleValue();
                vs2 = Double.valueOf(bg.getValor_15()).doubleValue();
            }
            //inicio cx
                        cxp.setString(1,bg.getValor_09() );
                        cxp.setString(2,codip);
                        cxp.setString(3,bg.getValor_09() );
                        cxp.setString(4,bg.getValor_09() );
                        cxp.setString(5,bg.getValor_09() );
                        cxp.setDouble(6,vd-vs2);//vr calculado
                        cxp.setDouble(7,vd-vs );//vr calculado
                        cxp.setDouble(8,vd-vs2);//vr calculado
                        cxp.setDouble(9,vd-vs );//vr calculado
                        cxp.setString(10,bg.getValor_04() );//usuario crea
                        cxp.setString(11,bg.getValor_06() );//Codigo del negocio
                        if(!bg.getValor_06().substring(0,2).equals("NF"))
                        {   cxp.setString(12, "01");//Cuenta
            }
                        else
                        {   cxp.setString(12, "04");//Cuenta
                        }
                        sql+=cxp.getSql();
            cxp = null;//JJCastro fase2

                //fin cxp
            //inicio items
            cxp_items.setString(1, bg.getValor_09());//PROVEEDOR
                        cxp_items.setString(2,codip);
                        cxp_items.setString(3, "1" );
                        cxp_items.setString(4,"DESEMBOLSO "+bg.getValor_09());
                        cxp_items.setDouble(5, vd-vs2);//valores
                        cxp_items.setDouble(6, vd-vs2);//valores
                        if(!bg.getValor_06().substring(0,2).equals("NF"))
                        {   cxp_items.setString(7, "22050102");//Cuenta
            }
                        else
                        {      cxp_items.setString(7, "22050602");//Cuenta
                        }
            cxp_items.setString(8, bg.getValor_06());//CODNEGOCIO
            cxp_items.setString(9, bg.getValor_04());//USUARIO CREA
                        cxp_items.setString(10, "AR-"+bg.getValor_09());//AUXILIAR
                        sql+=cxp_items.getSql();
                        cxp_items =null;
            //fin items*/
                sql+=up.getSql();
                if(bg.getValor_14()!=null&&bg.getValor_14().equals("on"))
                {   //////////////////////AQUI ESTA LA NOTA CREDITO/////////////////////////
                //inicio nc
                            cxp2.setString(1,bg.getValor_09() );
                            cxp2.setString(2,codip+"-1");
                            cxp2.setString(3,bg.getValor_09() );
                            cxp2.setString(4,bg.getValor_09() );
                            cxp2.setString(5,bg.getValor_09() );
                            cxp2.setDouble(6,vs);//vr calculado
                            cxp2.setDouble(7,0 );//vr calculado
                            cxp2.setDouble(8,vs );//vr calculado
                            cxp2.setDouble(9,0);//vr calculado
                            cxp2.setString(10,bg.getValor_04() );//usuario crea
                            cxp2.setString(11,codip );//Codigo de la factura
                            sql+=cxp2.getSql();
                cxp2 = null;
                    //fin nc
                //inicio items
                cxp_items2.setString(1, bg.getValor_09());//PROVEEDOR
                            cxp_items2.setString(2,codip+"-1");
                            cxp_items2.setString(3, "1" );
                            cxp_items2.setString(4,"AJUSTE "+codip);
                cxp_items2.setDouble(5, vs);//valores
                cxp_items2.setDouble(6, vs);//valores
                cxp_items2.setString(7, "26051002");//Cuenta
                cxp_items2.setString(8, codip);//CODNEGOCIO
                cxp_items2.setString(9, bg.getValor_04());//USUARIO CREA
                            cxp_items2.setString(10, "AR-"+bg.getValor_09());//AUXILIAR
                            sql+=cxp_items2.getSql();
                cxp_items2 = null;
                //fin items*/
            }
            //////////////////////AQUI ESTA LA NOTA CREDITO/////////////////////////

                up = new StringStatement (this.obtenerSQL("SQL_UP"), true);//JJCastro fase2

                // inicio Inicio Insert Ingresos
                /*for (int j=0;j<(Integer.parseInt(bg.getValor_02()))+1;j++)
             {
             ingfen = crearPreparedStatement("SQL_INSERT_ING");
             ingfen = new StringStatement (this.obtenerSQL("SQL_INSERT_ING"), true);//JJCastro fase2

             ingfen.setString(1,"FINV");
             ingfen.setInt(2,j);
             ingfen.setString(3,bg.getValor_06());
             ingfen.setDouble(4,(bg.getV3()[2][j]));//valor
             ingfen.setString(5,bg.getValor_01());//nit cliente
             ingfen.setString(6,bg.getValor_04());
             ingfen.setString(7,"COL");
             if(j==0)
             {ingfen.setString(8,(bg.getV2())[1]);}else{ingfen.setString(8,(bg.getV1())[j]);}//fechas
             //sql+=ingfen.getSql();//esta linea se quitara porque los ing_fenalco se generaran diariamente con otro proceso independiente
             }*/

                sqlintfen=interesesFenalcoService.InsertIngFenalcoNegRemix(bg.getValor_06(), bg.getValor_01(), bg.getValor_04());
            //ystem.out.println("sqlintfen"+sqlintfen);
                sql+=sqlintfen;

            up.setString(1, "IF");
                sql+=up.getSql();
                // Fin Insert Ingresos

                //ystem.out.println("SQL "+sql);
                /*if(!sql.equals("")){
             try{
             scv.crearStatement();
             scv.getSt().addBatch(sql);
             scv.execute();

             }catch(SQLException e){
             throw new SQLException("ERROR DURANTE INSERCION DE FACTURAS"+e.getMessage());
             }
             }
             sql=null;*/
             }
            catch(Exception e)
            {   e.printStackTrace();
            System.out.println(e.getMessage());
            //.out.println("Entra en el error 177");
                 throw new SQLException("ERROR DURANTE INSERCION DE FACTURAS " + e.getMessage() );
                }
finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             if (bg  != null){ try{ bg = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
            System.out.println("sql2222222"+sql);
        return sql;
        //.out.println("El nit q le  llega ++++");
    }



     public void seveEGDAO(String codn,String a)throws Exception
          {
        //.out.println("ACA");
        Connection con = null;
        PreparedStatement st = null;
            StringStatement   eg = null;
            StringStatement   egd = null;
            double tot=0;
            String sql="";
            String[] datos=new String[11];
            String codip="";
            try{codip=UpCP("EGRESO");}catch(Exception e){throw new SQLException();}
            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
        ResultSet rs = null;
            try
            {
            con = this.conectarJNDI("SQL_NEG_PROV");//JJCastro fase2
            st = con.prepareStatement(this.obtenerSQL("SQL_NEG_PROV"));
                eg  = new StringStatement (this.obtenerSQL("SQL_INSERT_EGRESO"), true);//JJCastro fase2
                egd  = new StringStatement (this.obtenerSQL("SQL_INSERT_EGRESODET"), true);//JJCastro fase2
            st.setString(1, codn);
            //.out.println("Query 1 "+st.toString());
                 rs=st.executeQuery();
                 if(rs.next())
                 {
                   datos[0]=rs.getString("BCOCODE");
                   datos[1]=rs.getString("BCOD");
                   datos[2]=rs.getString("NIT_TERCERO");
                   datos[3]=rs.getString("PAYMENT_NAME");
                   datos[4]=rs.getString("COD_CLI");
                   datos[5]=rs.getString("COMISION");
                   datos[6]=rs.getString("VR_DESEMBOLSO");
                   datos[7]=rs.getString("VR_GIRADO");
                   datos[8]=a;
                   datos[9]=rs.getString("TIPO_DOCUMENTO");
                   datos[10]=rs.getString("DOCUMENTO");
                //.out.println("ACA2");
            }
                 tot=Double.parseDouble(datos[6])-Double.parseDouble(datos[5]);
            //inicio egreso
            eg.setString(1, datos[0]);
            eg.setString(2, datos[1]);
            eg.setString(3, codip);
            eg.setString(4, datos[2]);
            eg.setString(5, datos[3]);
            eg.setDouble(6, tot);
            eg.setDouble(7, tot);
            eg.setString(8, a);
            eg.setString(9, datos[4]);
            eg.setString(10, datos[2]);
            eg.setString(11, datos[5]);
                 sql=eg.getSql();
                 //.out.println("ACA3");
            //fin egreso

            //inicio egresodet
            egd.setString(1, datos[0]);
            egd.setString(2, datos[1]);
            egd.setString(3, codip);
            egd.setString(4, datos[6]);
            egd.setString(5, datos[6]);
            egd.setString(6, datos[8]);
                 egd.setString(7, datos[9] );
                 egd.setString(8, datos[10] );
            //.out.println("ACA4");
                 sql+=egd.getSql();
            //fn egresodet
                  if(!sql.equals("")){
                    try{
                    scv.crearStatement();
                    scv.getSt().addBatch(sql);
                    scv.execute();
                    //.out.println("bien ");

                    }catch(SQLException e){
                    e.printStackTrace();
                    throw new Exception(e.getMessage());
                }
            }
             }
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE LA INSERCION DE EGRESOS " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (datos  != null){ try{ datos=null;              } catch(Exception e){ throw new SQLException("ERROR LIMPIANDO DATOS " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            if (eg  != null){ try{ eg = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (egd  != null){ try{ egd = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                }
        //.out.println("Querys "+sql);
            sql=null;

    }


    public void ListadoB(String opcion) throws Exception{
        //.out.println("Entra aqui tambien");
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  query = "";
        if(opcion.equals("todos")){
            query = "SQL_BANCOS";
        }else if(opcion.equals("negocios")){
            query = "SQL_BANCOS_NEGOCIOS";
        }
        Datos = new Vector();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
            while (rs.next())
            {
                    Neg = new Negocios();
                    Neg.setNom_cli(rs.getString(1));
                    Neg.setCod_cli(rs.getString(2));
                    Neg.setCod_tabla(rs.getString(3));
                    Neg.setEstado(rs.getString(4));
                    Datos.add(Neg);
                    Neg = null;
                }
        }}
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }

        //.out.println("Sali bien del DAO");
    }

   public void ListadoT(String[] nits) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_TRANFERENCIAS_LIST";
            String sql="";
        Datos2 = new Vector();
            try{
            String ids = "";
            for(int i=0;i<nits.length;i++){
                if(!ids.equals(""))  ids +=",";
                ids += "'" + nits[i] + "'";
            }
            con = this.conectarJNDI(query);
          sql= this.obtenerSQL(query);
          if(nits.length>0)
          {
          sql=sql.replaceAll("#cond#", "and cxp.proveedor in(#nits#) and ");
           sql=sql.replaceAll("#nits#", ids);
            }
          else
          {
               sql=sql.replaceAll("#cond#", " and cxp.proveedor not in (select distinct nit_anombre from convenios where nit_anombre!='') and ");
          }
            if (con != null) {
                ps = con.prepareStatement(sql);//JJCastro fase2
                rs = ps.executeQuery();
                while (rs.next())
                {
                    bg = new BeanGeneral();
                    bg.setValor_01(rs.getString(1));
                    bg.setValor_02(rs.getString(2));
                    bg.setValor_03(rs.getString(3));
                    bg.setValor_04(rs.getString(4));
                    bg.setValor_05(rs.getString(5));
                    bg.setValor_06(rs.getString(6));
                    bg.setValor_07(rs.getString(7));
                    bg.setValor_08(rs.getString(8));
                    bg.setValor_09(rs.getString(9));
                    bg.setValor_10(rs.getString(10));
                    bg.setValor_11(rs.getString(11));
                    bg.setValor_12(rs.getString(12));
                    bg.setValor_13(rs.getString(13));
                    bg.setValor_14(rs.getString(14));//Valor para la repeticion de negocios
                    Datos2.add(bg);
                    bg = null;
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }

        //.out.println("Sali bien del DAO");
    }

   public void ListadoT(String[] nits, String fechaInicio, String fechaFin) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_TRANFERENCIAS_LIST";
            String sql="";
        Datos2 = new Vector();
            try{
            String ids = "";
            for(int i=0;i<nits.length;i++){
                if(!ids.equals(""))  ids +=",";
                ids += "'" + nits[i] + "'";
            }
            con = this.conectarJNDI(query);
          sql= this.obtenerSQL(query);
          if(nits.length>0)
          {
                sql = sql.replaceAll("#cond#", "and cxp.proveedor in(#nits#) and n.fecha_ap between '" + fechaInicio + "' and '" + fechaFin + "' and ");
           sql=sql.replaceAll("#nits#", ids);
            }
          else
          {
               sql=sql.replaceAll("#cond#", " and cxp.proveedor not in (select distinct nit_anombre from convenios where nit_anombre!='') and ");
          }
            if (con != null) {
                ps = con.prepareStatement(sql);//JJCastro fase2
                rs = ps.executeQuery();
                while (rs.next())
                {
                    bg = new BeanGeneral();
                    bg.setValor_01(rs.getString(1));
                    bg.setValor_02(rs.getString(2));
                    bg.setValor_03(rs.getString(3));
                    bg.setValor_04(rs.getString(4));
                    bg.setValor_05(rs.getString(5));
                    bg.setValor_06(rs.getString(6));
                    bg.setValor_07(rs.getString(7));
                    bg.setValor_08(rs.getString(8));
                    bg.setValor_09(rs.getString(9));
                    bg.setValor_10(rs.getString(10));
                    bg.setValor_11(rs.getString(11));
                    bg.setValor_12(rs.getString(12));
                    bg.setValor_13(rs.getString(13));
                    bg.setValor_14(rs.getString(14));//Valor para la repeticion de negocios
                    Datos2.add(bg);
                    bg = null;
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }

        //.out.println("Sali bien del DAO");
    }


    public java.util.Vector getDatos() {
        return Datos;
    }

    public java.util.Vector getDatos2() {
        return Datos2;
    }

    public java.util.TreeMap getCust() throws Exception
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CLIENTES";
        Cust = new TreeMap();
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
                while (rs.next()) {
                    Cust.put(rs.getString(1), rs.getString(2));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return Cust;
    }

     public java.util.TreeMap getProv() throws Exception
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  query = "SQL_PROVEEDORES";
        Cust = new TreeMap();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
            while (rs.next())
            {
                Cust.put(rs.getString(1) + "_"+rs.getString(2) , rs.getString(2));
                }
        }}
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return Cust;
    }

     public String rel(BeanGeneral bg)throws Exception{

        StringStatement  st = null;
        String sql="";
        try
        {
            st = new StringStatement(this.obtenerSQL("SQL_RELACION"), true);//JJCastro fase2
            st.setString(1, bg.getValor_01());
            st.setString(2, bg.getValor_02());
            st.setString(3, bg.getValor_03());
            st.setString(4, bg.getValor_04());
            //st.executeUpdate();
            sql=st.getSql();
         }
        catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }finally{
            if (st  != null){ try{st =null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                }
        return sql;
        //.out.println("El nit q le  llega ++++");
    }

     public boolean exists(BeanGeneral bg,String op) throws Exception
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE";
        boolean sw = false;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, bg.getValor_01());
                ps.setString(2, bg.getValor_02());
                ps.setString(3, op);
                //.out.println("Aja--->"+ps.toString());
                rs = ps.executeQuery();
                if (rs.next()) {
                    sw = true;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return sw;
    }

     public String IntermediarioAval(String negocio) throws Exception
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INTERMEDIARIO_AVAL";
        String sw = "false;0;N";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, negocio);
                rs = ps.executeQuery();
                if (rs.next()) {
                    sw = rs.getBoolean("intermediario_aval")+";"+rs.getString("nit")+";"+rs.getString("aval_manual");
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return sw;
    }

     public void deleteCA(BeanGeneral bg) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_ELIMINAR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1,bg.getValor_03());
            ps.setString(2,bg.getValor_01());
            ps.setString(3,bg.getValor_02());
                ps.executeUpdate();
        }}
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            }




    public void deleteCAU(BeanGeneral bg) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_CREAR_UP";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, bg.getValor_03());
                ps.setString(2, bg.getValor_01());
                ps.setString(3, bg.getValor_02());
                ps.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            }



      public void transfneg(String cod)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR_TRANSF";
          try
        {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cod);
                st.executeUpdate();
         }}
        catch(SQLException e)
        {

            throw new SQLException("ERROR DURANTE LAS ANULACION DE NEGOCIOS " + e.getMessage() + " " + e.getErrorCode());
         }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            }


      
    public String bcop(String negcod) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BANCO_PROV";
        bcop = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, negcod);
                rs = ps.executeQuery();
                if (rs.next()) {
                    bcop = (rs.getString(1));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return bcop;
    }

     public String bcom(String ref) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  query = "BANCO_COM";
        String bcomi="";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1,ref);
                rs = ps.executeQuery();
            if(rs.next())
            {
                bcomi=(rs.getString(1));
                }
        }}
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return bcomi;
    }


       public void actcomD(String codn,String com,String nomb,String codb)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "ACTUALIZAR_COMISION";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, com);
                st.setString(2, nomb);
                st.setString(3, codb);
                st.setString(4, codn);
                st.setString(5, com);
                st.setString(6, codn);
                st.executeUpdate();
            }
           }
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE COMISIONES " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (st  != null){ try{st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }

    }

       public void lafilDAO() throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  query = "AFILIADOS_LISTA";
        String bcomi="";
        afil=new Vector();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
            while(rs.next())
            {
               bg= new BeanGeneral();
                    bg.setValor_01(rs.getString(1));
                    bg.setValor_02(rs.getString(2));
                    afil.add(bg);
               bg=null;
                }
        }}
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }

    }




  public void lcliDAO(String nitcli) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "CLIENTES_LISTA";
        afil = new Vector();
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, nitcli);
                //.out.println("Queryy---"+ps.toString());
                rs = ps.executeQuery();
                while (rs.next()) {
                    bg = new BeanGeneral();
                    bg.setValor_01(rs.getString(1));
                    bg.setValor_02(rs.getString(2));
                    bg.setValor_03(rs.getString(3));
                    afil.add(bg);
                    bg = null;
                }
            }
      }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }

    }


  public Vector getl()
  {
        return afil;
    }

  public void getnull()
  {
      afil=null;
    }




  public void ActAux() throws Exception{

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "LISTADO_AUXILIAR";
        Vector doc = new Vector();
        Vector aux = new Vector();
        try {
            //ps = this.crearPreparedStatement(Query);
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
                while (rs.next()) {
                    doc.add(rs.getString(1));
                    aux.add(rs.getString(2));
                }
                PreparedStatement st = null;
                String sql = this.obtenerSQL("UPDATE_DETFACT");
                for (int i = 0; i < doc.size(); i++) {
                    st = con.prepareStatement(sql);//JJCastro fase2
                    st.setString(1, aux.get(i).toString());
                    st.setString(2, doc.get(i).toString());
                    st.executeUpdate();
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE actualizacion de auxiliares " + e.getMessage() + " " + e.getErrorCode());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
                }


    //modificado por navi en junio 3 de 2008
  public String newfactDAO(String a)throws Exception
          {
        Connection con = null;
            String msg="";
        PreparedStatement fact = null;
        PreparedStatement detfact = null;
        StringStatement infact = null;
        StringStatement indetfact = null;
            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
            String[] datos=new String[31];
            ResultSet rs=null;
            ResultSet rs1=null;
            String codigo_cuenta_contable_x="";
            String cmcx="";
            try
            {
            con = this.conectarJNDI("SQL_FACT");
            if (con != null) {
                fact = con.prepareStatement(this.obtenerSQL("SQL_FACT"));//JJCastro fase2
                fact.setString(1, a);
                 rs=fact.executeQuery();
                fact.close();

                detfact = con.prepareStatement(this.obtenerSQL("SQL_DET_FACT"));//JJCastro fase2
                detfact.setString(1, a);
                 if(rs.next())
                 {
                   datos[0]=rs.getString("tipo_documento");
                   datos[1]=rs.getString("documento");
                   datos[2]=rs.getString("nit");
                   datos[3]=rs.getString("codcli");
                   datos[4]=rs.getString("concepto");
                   datos[5]=rs.getString("fecha_factura");
                   datos[6]=rs.getString("fecha_vencimiento");
                   datos[7]=rs.getString("descripcion");
                   datos[8]=rs.getString("valor_factura");
                   datos[9]=rs.getString("creation_user");
                   datos[10]=rs.getString("valor_facturame");
                   datos[11]=rs.getString("valor_saldo");
                   datos[12]=rs.getString("valor_saldome");
                   datos[13]=rs.getString("negasoc");
                   datos[14]=rs.getString("base");
                   cmcx=rs.getString("cmc");
                }
                 rs1=detfact.executeQuery();
                detfact.close();
                 if(rs1.next())
                 {
                   datos[15]=rs1.getString("tipo_documento");
                   datos[16]=rs1.getString("documento");
                   datos[17]=rs1.getString("item");
                   datos[18]=rs1.getString("nit");
                   datos[19]=rs1.getString("concepto");
                   datos[20]=rs1.getString("descripcion");
                   datos[21]=rs1.getString("valor_unitario");
                   datos[22]=rs1.getString("valor_item");
                   datos[23]=rs1.getString("creation_user");
                   datos[24]=rs1.getString("valor_unitariome");
                   datos[25]=rs1.getString("valor_itemme");
                   datos[26]=rs1.getString("numero_remesa");
                   if (datos[26]==null || datos[26].equals("" )){
                       datos[26]=datos[16];
                    }
                   datos[27]=rs1.getString("base");
                   datos[28]=rs1.getString("auxiliar");
                   codigo_cuenta_contable_x=getCodigoCuentaNd(datos[1]);
                   datos[29]=codigo_cuenta_contable_x;
                }

                //OTRO TRY
             if(rs1.getRow()>0 && rs.getRow() > 0)
             {
             String codip="";
             try{codip=UpCP("NOTASD");}catch(Exception e){throw new SQLException();}
             String sql="";

             infact = new StringStatement (this.obtenerSQL("SQL_INSERTAR_FACTURA_2"), true);//JJCastro fase2
                    infact.setString(1, "ND");
                    infact.setString(2, codip);
                    infact.setString(3, datos[2]);
                    infact.setString(4, datos[3]);
                    infact.setString(5, datos[4]);
                    infact.setString(6, datos[5]);
                    infact.setString(7, datos[6]);
                    infact.setString(8, datos[7]);
                    infact.setString(9, datos[8]);
                    infact.setString(10, datos[9]);
                    infact.setString(11, datos[10]);
                    infact.setString(12, datos[8]);
                    infact.setString(13, datos[8]);
                    infact.setString(14, datos[13]);
                    infact.setString(15, datos[14]);
                    infact.setString(16, cmcx);
                    infact.setString(17, "0"); //Se agrega "0" para que en caso de que este metodo se use, no quede con errores.
             sql=infact.getSql();
                    //numero 2
             indetfact = new StringStatement (this.obtenerSQL("SQL_INSERTAR_DET_FACTURA_2"), true);//JJCastro fase2
                    indetfact.setString(1, "ND");
                    indetfact.setString(2, codip);
                    indetfact.setString(3, datos[17]);
                    indetfact.setString(4, datos[18]);
                    indetfact.setString(5, datos[19]);
                    indetfact.setString(6, datos[20]);
                    indetfact.setString(7, datos[21]);
                    indetfact.setString(8, datos[22]);
                    indetfact.setString(9, datos[23]);
                    indetfact.setString(10, datos[24]);
                    indetfact.setString(11, datos[25]);
                    indetfact.setString(12, datos[26]);
                    indetfact.setString(13, datos[27]);
                    indetfact.setString(14, datos[28]);
                    indetfact.setString(15, datos[29]);
             sql+=indetfact.getSql();

                    if (!sql.equals("")) {
                        try {
                            scv.crearStatement();
                            scv.getSt().addBatch(sql);
                            //.out.println("SQL "+sql);
                            scv.execute();
                            //.out.println("bien ");

                        } catch (SQLException e) {
                            e.printStackTrace();
                            throw new Exception("Error Durante la Insercion de las Notas" + e.getMessage());
                        }
                    }
                } else {
                    msg = "Rectifique el documento, no ha sido encontrado";
                }
            datos=null;

             }}
            catch(SQLException e)
            {    
            throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (rs1  != null){ try{ rs1.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (fact  != null){ try{ fact.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (detfact  != null){ try{ detfact.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (infact  != null){ try{ infact= null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (indetfact  != null){ try{ indetfact = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return msg;
    }


  




    /*
     * Moficado: 11-07-2011
     * Ing. Iris Vargas
     * Envios a fenalco
     */
   public String newfactDAO2(String a,String id_convenio, String b, String c, String cliente, Usuario usuario) throws Exception {

        String msg = "";
        String BolsaSaldoVector = "";

        PreparedStatement fact = null;
        PreparedStatement detfact = null;
        StringStatement infact = null;
        StringStatement indetfact = null;
        String[] datos = new String[35];
        ResultSet rs = null;
        ResultSet rs1 = null;
        Connection con = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();

        try {
            con = this.conectarJNDI("SQL_FACT");
            if (con != null) {
                String cuenta_fac = "";
                String cmc_fac = "";
                try {
                    fact = con.prepareStatement(this.obtenerSQL("SQL_FACT"));//JJCastro fase2
                    fact.setString(1, a);
                    detfact = con.prepareStatement(this.obtenerSQL("SQL_DET_FACT"));//JJCastro fase2
                    detfact.setString(1, a);
                    rs = fact.executeQuery();

                    if (rs.next()) {
                        datos[0] = rs.getString("tipo_documento");
                        datos[1] = rs.getString("documento");
                        datos[2] = cliente;//nit fenalco
                        datos[3] = cliente;//codcliente
                        datos[4] = rs.getString("concepto");
                        datos[5] = b;//parametro
                        datos[6] = c;//parametro
                        datos[7] = rs.getString("descripcion");
                        datos[8] = rs.getString("valor_factura");
                        datos[9] = usuario.getLogin();
                        datos[10] = rs.getString("valor_facturame");
                        datos[11] = rs.getString("valor_saldo");
                        datos[12] = rs.getString("valor_saldome");
                        datos[13] = rs.getString("negasoc");
                        datos[14] = rs.getString("base");
                        cmc_fac = rs.getString("cmc");
                        datos[26] = rs.getString("num_remesa");
                        datos[29] = rs.getString("nit");
                        datos[30] = rs.getString("codcli");
                        datos[31] = rs.getString("fecha_factura");
                        datos[32] = rs.getString("descripcion");
                        //nuevos campos valor_abono y valor_abonome
                        datos[33] = rs.getString("valor_abono");
                        datos[34] = rs.getString("valor_abonome");

                    }
                    rs1 = detfact.executeQuery();

                } catch (SQLException e) {
                    throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS " + e.getMessage() + " " + e.getErrorCode());
                }
                GestionConveniosDAO convdao = new GestionConveniosDAO(this.getDatabaseName());
                if (!id_convenio.equals("null")) {
                    Convenio convenio = convdao.buscar_convenio(usuario.getBd(), id_convenio);
                    //OTRO TRY
                    if (rs.getRow() > 0) {
                        String fecha_actual = Util.getFechaActual_String(4);
                        ChequeXFacturaService md = new ChequeXFacturaService();
                        String monedaLocal = md.getMonedaLocal(usuario.getDstrct());
                        String codip = "";
                        String cmc = "";
                        Ingreso ing = new Ingreso();
                        String fiducia = ";";
                        try {
                            if (!datos[0].equals("NDC")) {
                                fiducia = this.isCXCFiducia(convenio.getId_convenio(), usuario.getDstrct(), a.substring(0, 2), a);
                                if (Boolean.parseBoolean((fiducia).split(";")[0])) {
                                    for (int k = 0; k < convenio.getConvenioFiducias().size(); k++) {
                                        if (convenio.getConvenioFiducias().get(k).getNit_fiducia().equals((fiducia).split(";")[1])) {
                                            codip = UpCP(convenio.getConvenioFiducias().get(k).getPrefijo_end_fiducia());
                                            cmc = convenio.getConvenioFiducias().get(k).getHc_end_fiducia();
                                        }
                                    }
                                    ing.setConcepto("EFP");
                                } else {
                                    codip = UpCP(convenio.getPrefijo_endoso());
                                    cmc = convenio.getHc_endoso();
                                    ing.setConcepto("EFF");
                                }
                            } else {
                                if (Boolean.parseBoolean((this.isCXCFiducia(convenio.getId_convenio(), usuario.getDstrct(), datos[26].substring(0, 2))).split(";")[0])) {
                                    fiducia = this.isCXCFiducia(convenio.getId_convenio(), usuario.getDstrct(), datos[26].substring(0, 2));
                                    for (int k = 0; k < convenio.getConvenioFiducias().size(); k++) {
                                        if ( convenio.getConvenioFiducias().get(k).getNit_fiducia().equals((fiducia).split(";")[1]) ) {
                                            codip = UpCP(convenio.getConvenioFiducias().get(k).getPrefijo_end_fiducia());
                                            cmc = convenio.getConvenioFiducias().get(k).getHc_end_fiducia();
                                        }
                                    }
                                    ing.setConcepto("EFP");
                                } else {
                                    codip = UpCP(convenio.getPrefijo_endoso());
                                    cmc = convenio.getHc_endoso();
                                    ing.setConcepto("EFF");
                                }

                            }
                        } catch (Exception e) {
                            throw new SQLException();
                        }

                        com.tsp.finanzas.contab.model.beans.Cmc cmcob = new com.tsp.finanzas.contab.model.beans.Cmc();
                        cmcob.setDstrct(usuario.getDstrct());
                        cmcob.setTipodoc(datos[0]);
                        cmcob.setCmc(cmc_fac);
                        CmcDAO cmcdao = new CmcDAO(this.getDatabaseName());
                        cmcdao.setCmc(cmcob);
                        cmcdao.obtenerCmcDoc();
                        String cuentacmc_fac = cmcdao.getCmc().getCuenta();

                        infact = new StringStatement(this.obtenerSQL("SQL_INSERTAR_FACTURA_3"), true);//JJCastro fase2
                        infact.setString(1, "FAC");
                        infact.setString(2, codip);
                        infact.setString(3, datos[2]);
                        infact.setString(4, datos[3]);
                        infact.setString(5, datos[4]);
                        infact.setString(6, datos[5]);
                        infact.setString(7, datos[6]);
                        infact.setString(8, datos[7]);
                        infact.setString(9, datos[11]);//8x11
                        infact.setString(10, datos[9]);
                        infact.setString(11, datos[11]);//10x11
                        infact.setString(12, datos[11]);//11x11
                        infact.setString(13, datos[11]);//13x11
                        infact.setString(14, "0");//datos[33]x0
                        infact.setString(15, "0");//datos[34]x0
                        infact.setString(16, datos[13]);
                        infact.setString(17, datos[14]);
                        infact.setString(18, cmc);
                        infact.setString(19, a);

                        //Fin Mod
                        BolsaSaldoVector = datos[11];
                        //int BolsaSaldo = Integer.parseInt(BolsaSaldoVector);
                        double BolsaSaldo = Double.parseDouble(BolsaSaldoVector);
                        //int Diferencia = 0;
                        double Diferencia = 0;
                        String VerifyDetails = "";
                        String ValorFactura = "";

                        batch.add(infact.getSql());

                        while (rs1.next()) {

                            datos[15] = rs1.getString("tipo_documento");
                            datos[16] = rs1.getString("documento");
                            datos[17] = rs1.getString("item");
                            datos[18] = cliente;
                            datos[19] = rs1.getString("concepto");
                            datos[20] = rs1.getString("descripcion");
                            datos[21] = rs1.getString("valor_unitario");
                            datos[22] = rs1.getString("valor_item");
                            datos[23] = usuario.getLogin();
                            datos[24] = rs1.getString("valor_unitariome");
                            datos[25] = rs1.getString("valor_itemme");
                            datos[26] = rs1.getString("numero_remesa");
                            datos[27] = rs1.getString("base");
                            datos[28] = "RD-" + cliente;
                            cuenta_fac = rs1.getString("codigo_cuenta_contable");

                            VerifyDetails = "N";
                            //Diferencia = BolsaSaldo - Integer.parseInt(datos[21]);
                            Diferencia = BolsaSaldo - Double.parseDouble(datos[21]);

                            if ( Diferencia <= 0 && BolsaSaldo > 0 ) {

                                //ValorFactura = Integer.toString(BolsaSaldo);
                                ValorFactura = Double.toString(BolsaSaldo);

                                //BolsaSaldo = BolsaSaldo - Integer.parseInt(datos[21]);
                                BolsaSaldo = BolsaSaldo - Double.parseDouble(datos[21]);
                                VerifyDetails = "S";

                            } else if ( Diferencia > 0 && BolsaSaldo > 0 ) {

                                ValorFactura = datos[21];
                                //BolsaSaldo = BolsaSaldo - Integer.parseInt(datos[21]);
                                BolsaSaldo = BolsaSaldo - Double.parseDouble(datos[21]);
                                VerifyDetails = "S";
                            }

                            if ( VerifyDetails.equals("S") ) {

                                //numero 2
                                indetfact = new StringStatement(this.obtenerSQL("SQL_INSERTAR_DET_FACTURA_3"), true);//JJCastro fase2
                                indetfact.setString(1, "FAC");
                                indetfact.setString(2, codip);
                                indetfact.setString(3, datos[17]);
                                indetfact.setString(4, datos[18]);
                                indetfact.setString(5, datos[19]);
                                indetfact.setString(6, datos[20]);
                                indetfact.setString(7, ValorFactura);//datos[21]xdatos[11] de la cabecera y luego, todo por ValorFactura
                                indetfact.setString(8, ValorFactura);//datos[22]xdatos[11] de la cabecera y luego, todo por ValorFactura
                                indetfact.setString(9, datos[23]);
                                indetfact.setString(10, ValorFactura);//datos[24]xdatos[11] de la cabecera y luego, todo por ValorFactura
                                indetfact.setString(11, ValorFactura);//datos[25]xdatos[11] de la cabecera y luego, todo por ValorFactura
                                indetfact.setString(12, a);
                                indetfact.setString(13, datos[27]);
                                indetfact.setString(14, datos[28]);
                                indetfact.setString(15, cuenta_fac);

                                batch.add(indetfact.getSql());

                            }

                        }

                        IngresoDAO ingdao = new IngresoDAO(this.getDatabaseName());
                        Ingreso_detalleDAO ingdetdao = new Ingreso_detalleDAO(this.getDatabaseName());


                        ing.setReg_status("");
                        ing.setDstrct(usuario.getDstrct());
                        ing.setTipo_documento("ICA");
                        ing.setTipo_ingreso("C");
                        ing.setFecha_consignacion(fecha_actual);
                        ing.setFecha_ingreso(fecha_actual);
                        ing.setBranch_code("");
                        ing.setBank_account_no("");
                        ing.setCodmoneda(monedaLocal);
                        ing.setAgencia_ingreso(usuario.getId_agencia());
                        ing.setPeriodo("000000");
                        ing.setDescripcion_ingreso(a);
                        ing.setVlr_tasa(1);
                        ing.setFecha_tasa(fecha_actual);
                        ing.setCant_item(1);
                        ing.setTransaccion(0);
                        ing.setTransaccion_anulacion(0);
                        ing.setCreation_user(usuario.getLogin());
                        ing.setCreation_date(Util.fechaActualTIMESTAMP());
                        ing.setLast_update(Util.fechaActualTIMESTAMP());
                        ing.setUser_update(usuario.getLogin());
                        ing.setCuenta(cuenta_fac);
                        ing.setBase(usuario.getBase());
                        ing.setNro_consignacion("");
                        ing.setTasaDolBol(0);
                        ing.setCmc(cmc_fac);
                        ing.setAuxiliar("RD-"+datos[29]);
                        ing.setAbc("");
                        ing.setCodcli(datos[30]);
                        ing.setNitcli(datos[29]);
                        ing.setVlr_ingreso(Double.parseDouble(datos[11]));
                        ing.setVlr_ingreso_me(Double.parseDouble(datos[11]));

                        ingdao.setIngreso(ing);
                        batch.add(ingdao.insertarNewIngreso());

                        //Campos Comunes en Ingreso_detalle
                        Ingreso_detalle ingDetalle = new Ingreso_detalle(); //Detalle del Ingreso
                        ingDetalle.setDistrito(usuario.getDstrct());
                        ingDetalle.setTipo_doc(datos[0]);
                        ingDetalle.setItem(1);
                        ingDetalle.setTipo_documento("ICA");
                        ingDetalle.setCodigo_reteica("");
                        ingDetalle.setValor_reteica(0);
                        ingDetalle.setValor_reteica_me(0);
                        ingDetalle.setValor_diferencia(0);
                        ingDetalle.setCreation_user(usuario.getLogin());
                        ingDetalle.setCreation_date(Util.fechaActualTIMESTAMP());
                        ingDetalle.setBase(usuario.getBase());
                        ingDetalle.setCodigo_retefuente("");
                        ingDetalle.setValor_retefuente(0);
                        ingDetalle.setValor_retefuente_me(0);
                        ingDetalle.setTipo_aux("RD");
                        ingDetalle.setCuenta(cuentacmc_fac);
                        ingDetalle.setValor_tasa(1);
                        ingDetalle.setNit_cliente(datos[29]);

                        ingDetalle.setValor_ingreso(Double.parseDouble(datos[11]));
                        ingDetalle.setValor_ingreso_me(Double.parseDouble(datos[11]));
                        ingDetalle.setDocumento(a);
                        ingDetalle.setFactura(a);
                        ingDetalle.setAuxiliar(datos[29]);
                        ingDetalle.setFecha_factura(datos[31]);
                        ingDetalle.setDescripcion_factura(datos[32]);
                        ingDetalle.setValor_saldo_factura_me(Double.parseDouble(datos[11]));

                        ingdetdao.setIngreso_detalle(ingDetalle);
                        batch.add(ingdetdao.insertarNewIngresoDetalle());

                        batch.add(ingdao.incrementarSerie("ICAC"));
                        batch.add(this.updateSaldoFacturaEndoso(Double.parseDouble(datos[11]), Double.parseDouble(datos[11]), a, usuario.getDstrct(), datos[0], cliente));


                        fact.close();
                        detfact.close();
                        infact = null;
                        indetfact = null;

                        try {
                            apdao.ejecutarSQL(batch);
                        } catch (SQLException e) {
                            e.printStackTrace();
                            throw new Exception("Error Durante el Envio de facturas " + e.getMessage());
                        }

                    } else {
                        msg = "Rectifique el documento, no ha sido encontrado";
                    }
                } else {
                    msg = "El negocio no tiene convenio";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new Exception("Error Durante la Insercion de las Notas" + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (rs1 != null) {
                try {
                    rs1.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (fact != null) {
                try {
                    fact.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (detfact != null) {
                try {
                    detfact.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (infact != null) {
                try {
                    infact = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (indetfact != null) {
                try {
                    indetfact = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        datos = null;

        return msg;
    }




  
  
    /*
     * Moficado: 2010
     * Ing. Jose Castro
     * Conexiones JNDI

     */
  public String UpCP(String a) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "UP_SERIES";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2 
                ps.setString(1, a);
                rs = ps.executeQuery();
                if (rs.next()) {
                    ret = rs.getString(1);
                }
            }
      }catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return ret;
    }



  

  public Vector LDim(String a,String b ) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_INGRESOS_COUNT";
        Datos = new Vector();
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, a);
                ps.setString(2, b);
                //.out.println("Count "+ps.toString());
                rs = ps.executeQuery();
                while (rs.next())
                {
                    Datos.add(rs.getString(1));
                    Datos.add(rs.getString(2));
                    Datos.add(rs.getString(3));
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return Datos;
        //.out.println("Sali bien del DAO");
    }


       public Vector LCod(String a,String b ) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_INGRESOS_COD";
        Datos = new Vector();
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, a);
                ps.setString(2, b);
                rs = ps.executeQuery();
                while (rs.next())
                {
                    Datos.add(rs.getString(1));
                    Datos.add(rs.getString(2));
                    Datos.add(rs.getString(3));
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return Datos;
        //.out.println("Sali bien del DAO");
    }


     public double Valor(String a,String b,String c ) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_INGRESOS_VALOR";
        double res = 0;
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, a);
                ps.setString(2, b);
                ps.setString(3, c);
                //.out.println("Query "+ps.toString());
                rs = ps.executeQuery();
                if (rs.next())
                {
                    res=rs.getDouble(1);
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return res;
        //.out.println("Sali bien del DAO");
    }

       public ArrayList<String> subled(BeanGeneral bg)throws SQLException,Exception{
            //TransaccionService  scv = new TransaccionService();

            StringStatement st  = null;
            StringStatement st2  = null;
            ArrayList<String> listSql= new ArrayList<String>();
            String val="";
            try
            {
                 st = new StringStatement (this.obtenerSQL("SQL_SUBLED_CLI"), true);//JJCastro fase2
                 st2 = new StringStatement (this.obtenerSQL("SQL_SUBLED"), true);//JJCastro fase2
                if(!(bg.getValor_06().substring(0,2).equals("NF")))
                {   val=ValSub("13109601","RD",bg.getValor_01() );
                    if(val.equals("A"))
                    {
                        st.setString(1, "13109601" );
                        st.setString(2, bg.getValor_01() );
                        st.setString(3, bg.getValor_01() );
                        st.setString(4, bg.getValor_04() );
                    listSql.add(st.getSql());
                    //Limpiar Parametros
                        st = new StringStatement (this.obtenerSQL("SQL_SUBLED_CLI"), true);//JJCastro fase2
                        st.setString(1, "13109602" );
                        st.setString(2, bg.getValor_01() );
                        st.setString(3, bg.getValor_01() );
                        st.setString(4, bg.getValor_04() );
                    listSql.add(st.getSql());
                }
                //proveedores
                    val=ValSub("22050101","AR",bg.getValor_09() );
                    if(val.equals("A"))
                    {
                    st2.setString(1, "22050101");
                    st2.setString(2, bg.getValor_09());
                    st2.setString(3, bg.getValor_09());
                    st2.setString(4, bg.getValor_04());
                    listSql.add(st2.getSql());
                        st2 = new StringStatement (this.obtenerSQL("SQL_SUBLED"), true);//JJCastro fase2
                    st2.setString(1, "22050102");
                    st2.setString(2, bg.getValor_09());
                    st2.setString(3, bg.getValor_09());
                    st2.setString(4, bg.getValor_04());
                    listSql.add(st2.getSql());
                }
                }
                else
                {      val=ValSub("13109901","RD",bg.getValor_01() );
                    if(val.equals("A"))
                    {
                        st.setString(1, "13109901" );
                        st.setString(2, bg.getValor_01() );
                        st.setString(3, bg.getValor_01() );
                        st.setString(4, bg.getValor_04() );
                    listSql.add(st.getSql());
                    // st.clearParameters();
                        st = new StringStatement (this.obtenerSQL("SQL_SUBLED_CLI"), true);//JJCastro fase2
                        st.setString(1, "13109902" );
                        st.setString(2, bg.getValor_01() );
                        st.setString(3, bg.getValor_01() );
                        st.setString(4, bg.getValor_04() );
                    listSql.add(st.getSql());
                }
                //proveedores
                    val=ValSub("22050601","AR",bg.getValor_09() );
                    if(val.equals("A"))
                    {
                    st2.setString(1, "22050601");
                    st2.setString(2, bg.getValor_09());
                    st2.setString(3, bg.getValor_09());
                    st2.setString(4, bg.getValor_04());
                    listSql.add(st2.getSql());
                    //st2.clearParameters();
                        st2 = new StringStatement (this.obtenerSQL("SQL_SUBLED"), true);//JJCastro fase2
                    st2.setString(1, "22050602");
                    st2.setString(2, bg.getValor_09());
                    st2.setString(3, bg.getValor_09());
                    st2.setString(4, bg.getValor_04());
                    listSql.add(st2.getSql());
                }
            }
             }
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE LAS INSERCION DE SUBLEDGERS " + e.getMessage() + " " + e.getErrorCode());
                }
        finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (st2  != null){ try{ st2 = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return listSql;
    }


      public String ValSub(String a,String b,String c ) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_VALIDAR_SUBLED";
            String res="A";
        Datos = new Vector();
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, a);
                ps.setString(2, b);
                ps.setString(3, c);
                rs = ps.executeQuery();
                if (rs.next())
                {
                    res=rs.getString(1);
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return res;
    }



      

          public String ConseDAO(BeanGeneral bg ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
              String ret="";
        String query = "INSERTAR_CONSECUTIVO";
            try
            {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, bg.getValor_01());
                st.setString(2, bg.getValor_02());
                st.setString(3, bg.getValor_05());
                st.setString(4, bg.getValor_04());
                st.setString(5, bg.getValor_03());
                st.executeUpdate();
             }}
            catch(Exception e)
            {
                 ret="Verifique el nit del proveedor "+e.getMessage();
            }
        finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return ret;
    }

     public void Letras(BeanGeneral bg) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "DATOS_LETRAS";
        letras = new BeanGeneral();
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, bg.getValor_07());
                rs = ps.executeQuery();
                //.out.println("Query - "+ps.toString());
                if (rs.next())
                {
                    letras.setValor_01(rs.getString(1));//numero de documentos
                    letras.setValor_02(rs.getString(2));//nombre del cliente
                    letras.setValor_03(rs.getString(3));//nombre del proveedor
                    letras.setValor_04(rs.getString(4));//consecutivo letra
                    letras.setValor_05(rs.getString(5));//vrletra
                    letras.setValor_06(rs.getString(6));//cc cliente
                    letras.setValor_07(rs.getString(7));//cc proveedor
                    letras.setValor_08(rs.getString(8));//numero del pagare
                    letras.setValor_09(rs.getString(9));//fecha de negocio
                    letras.setValor_10(rs.getString(10));//representante legal
                    letras.setValor_11(rs.getString(11));//cc rep legal
                    letras.setValor_12(rs.getString(12));//exp cc cliente
                    letras.setValor_13(rs.getString(13));//tot pagado
                    letras.setValor_14((bg.getValor_01()).toUpperCase());//nombre del coodeudor
                    letras.setValor_15(bg.getValor_02());//cc coodeudor
                    letras.setValor_16(bg.getValor_03());//Codigo del Afiliado
                    letras.setValor_17(bg.getValor_04());//Codigo de Aprobacion
                    letras.setValor_18(bg.getValor_05());//Domicilio Pago
                    letras.setValor_19(bg.getValor_06());//Ciudad de Otorgamiento
                    letras.setValor_20(bg.getValor_07());//Cod Negocio
                    letras.setValor_21(bg.getValor_08());//cc autoriazada
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
                }





     public BeanGeneral RetLet()
     {
        return letras;
    }

      public BeanGeneral MailDAO(String codneg) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "GET_INFO_MAIL";
        BeanGeneral mail = new BeanGeneral();
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, codneg);
                rs = ps.executeQuery();
                //.out.println("Query - "+ps.toString());
                if (rs.next())
                {
                    mail.setValor_01(rs.getString(1));//nombre cliente
                    mail.setValor_02(rs.getString(2));//id cliente
                    mail.setValor_03(rs.getString(3));//fecha negocio
                    mail.setValor_04(rs.getString(4));//bco
                    mail.setValor_05(rs.getString(5));//vr girado
                    mail.setValor_06(rs.getString(6));//f desembolso
                    //mail.setValor_07(rs.getString(7));//nombre proveedor
                    //mail.setValor_08(rs.getString(8));//mail
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            }        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return mail;
    }

     public ArrayList<String> CCfact(BeanGeneral bg)throws SQLException, Exception{
        StringStatement st = null;
        StringStatement detfac = null;
        StringStatement up = null;
        StringStatement cxp = null;
        StringStatement cxp_items = null;
        //PreparedStatement upcp = null;
        StringStatement ingfen = null;
            TransaccionService  scv = new TransaccionService(this.getDatabaseName());

        ArrayList<String> listSql = new ArrayList<String>();
            try
            {
                String no2="";
            /*bg.setValor_05("CH");
             String no="REDCHEQUE";*/
                String no ="";
                String concepto ="";
                if ( bg.getValor_05().equals("01")){
                    no="REDCHEQUE";
                    concepto ="CH";
                }else{
                    no="REDLETRA";
                    concepto="LT";
            }//TMolina Mod 2008-Oct-08
            //upcp=crearPreparedStatement("SQL_UP");
                up= new StringStatement(this.obtenerSQL("SQL_UP"), true);

                for (int i=0;i<(Integer.parseInt(bg.getValor_02()));i++)
                {
                st = new StringStatement(this.obtenerSQL("SQL_INSERTAR_FACTURA"), true);
                detfac = new StringStatement(this.obtenerSQL("SQL_INSERTAR_DET_FACTURA"), true);
                st.setString(1, "FAC");
                    st.setString(2,no);
                    st.setString(3,String.valueOf(i+1));
                st.setString(4, bg.getValor_01());
                st.setString(5, bg.getValor_01());
                st.setString(6, concepto);//bg.getValor_05());
                st.setString(7, bg.getValor_12());
                st.setString(8, (bg.getV1())[i]);
                    st.setString(9,no);
                st.setString(10, String.valueOf(Util.customFormat((bg.getArr_1())[i])).replace(",", ""));
                st.setString(11, bg.getValor_04());
                st.setString(12, String.valueOf(Util.customFormat((bg.getArr_1())[i])).replace(",", ""));
                st.setString(13, String.valueOf(Util.customFormat((bg.getArr_1())[i])).replace(",", ""));
                st.setString(14, String.valueOf(Util.customFormat((bg.getArr_1())[i])).replace(",", ""));
                st.setString(15, bg.getValor_06());
                st.setString(16, bg.getValor_07());
                    st.setString(17, (bg.getV4())[i]  );
                    st.setString(18, "fec_doc"  );
                    st.setString(19, (bg.getV5())[i]  );
                    st.setString(20, "01"  );
                ///detalle
                detfac.setString(1, "FAC");
                    detfac.setString(2,no);
                    detfac.setString(3,String.valueOf(i+1));
                    detfac.setString(4,String.valueOf(i+1));
                detfac.setString(5, bg.getValor_01());
                detfac.setString(6, concepto);//bg.getValor_05());
                detfac.setString(7, concepto);//bg.getValor_05());
                detfac.setString(8, String.valueOf(Util.customFormat((bg.getArr_1())[i])).replace(",", ""));
                detfac.setString(9, String.valueOf(Util.customFormat((bg.getArr_1())[i])).replace(",", ""));
                detfac.setString(10, bg.getValor_04());
                detfac.setString(11, String.valueOf(Util.customFormat((bg.getArr_1())[i])).replace(",", ""));
                detfac.setString(12, String.valueOf(Util.customFormat((bg.getArr_1())[i])).replace(",", ""));
                    detfac.setString(13,no);
                    detfac.setString(14,String.valueOf(i+1));
                    detfac.setString(15,bg.getValor_07());
                detfac.setString(16, bg.getValor_08());
                    detfac.setString(17,"13109602");

                listSql.add(st.getSql());
                listSql.add(detfac.toString());
                desconectar("SQL_INSERTAR_FACTURA");
                desconectar("SQL_INSERTAR_DET_FACTURA");
            }
                up.setString(1,no);
                //up.executeUpdate();
            //almaceno en la otra tabla
                String codip="";
                try{codip=UpCP("FACTPROV");}catch(Exception e){throw new SQLException();}

            cxp = new StringStatement(this.obtenerSQL("INSERT_CXP_DOC"), true);
            cxp_items = new StringStatement(this.obtenerSQL("INSERT_CXP_DOC_ITEMS"), true);

                double vn=Double.valueOf(bg.getValor_10()).doubleValue();
                double vd=Double.valueOf(bg.getValor_11()).doubleValue();
            //inicio cx
                        cxp.setString(1,bg.getValor_09() );
                        cxp.setString(2,codip);
                        cxp.setString(3,bg.getValor_09() );
                        cxp.setString(4,bg.getValor_09() );
                        cxp.setString(5,bg.getValor_09() );
                        cxp.setDouble(6,vd);//vr calculado
                        cxp.setDouble(7,vd );//vr calculado
                        cxp.setDouble(8,vd );//vr calculado
                        cxp.setDouble(9,vd );//vr calculado
                        cxp.setString(10,bg.getValor_04() );//usuario crea
                        cxp.setString(11,bg.getValor_06() );//Codigo del negocio
            cxp.setString(12, "01");
            listSql.add(cxp.toString());
                       // cxp.clearParameters();

                //fin cxp
            //inicio items
            cxp_items.setString(1, bg.getValor_09());//PROVEEDOR
                        cxp_items.setString(2,codip);
                        cxp_items.setString(3, "1" );
                        cxp_items.setString(4,"DESEMBOLSO "+bg.getValor_09());
            cxp_items.setDouble(5, vd);//valores
            cxp_items.setDouble(6, vd);//valores
            cxp_items.setString(7, "22050102");//Cuenta
            cxp_items.setString(8, bg.getValor_06());//CODNEGOCIO
            cxp_items.setString(9, bg.getValor_04());//USUARIO CREA
                        cxp_items.setString(10, "AR-"+bg.getValor_09());//AUXILIAR
            listSql.add(cxp_items.toString());
                      //  cxp_items.clearParameters();
            //fin items*/
            listSql.add(up.getSql());
                //up.clearParameters();
            // inicio Inicio Insert Ingresos
                int pos=0;
		while ((bg.getArr_2())[pos]!=0)
		{
                ingfen = new StringStatement(("SQL_INSERT_ING"), true);
                    ingfen.setString(1,"FINV");
                    ingfen.setInt(2,pos+1);
                    ingfen.setString(3,bg.getValor_06());
                    ingfen.setDouble(4,(bg.getArr_2())[pos]);//valor
                    ingfen.setString(5,bg.getValor_01());//nit cliente
                    ingfen.setString(6,bg.getValor_04());
                    ingfen.setString(7,"COL");
                    ingfen.setString(8,(bg.getV2())[pos]);//fechas
                listSql.add(ingfen.toString());
                pos++;
                desconectar("SQL_INSERT_ING");
            }
            up.setString(1, "IF");
            listSql.add(up.getSql());
               }
            catch(SQLException e)
            {    e.printStackTrace();
            System.out.println(e.getMessage());

            //.out.println("Entra en el error 177");
            throw new SQLException("ERROR DURANTE INSERCION DE FACTURAS " + e.getMessage() + " " + e.getErrorCode());
                }
            finally
            {
             if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             if (detfac  != null){ try{ detfac = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             if (up  != null){ try{ up = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             if (cxp  != null){ try{ cxp = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             if (cxp_items  != null){ try{ cxp_items = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             if (ingfen  != null){ try{ ingfen = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return listSql;
        //.out.println("El nit q le  llega ++++");
    }




  public java.util.TreeMap GetBankFid() throws Exception
        {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_FIDUCIA";
        Cust = new TreeMap();
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
                while (rs.next())
                {
                    Cust.put(rs.getString(1), rs.getString(2));
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return Cust;
    }


    //navi en junio 3 d 2008
      public String getCodigoCuentaNd(String a) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "CODE_CUENTA_ND";
            String ret="";
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, a);
                rs = ps.executeQuery();
                if(rs.next())
                {
                   ret=rs.getString(1);
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return ret;
    }



    /*
     *Este metodo obtiene las observaciones de la factura a enviar a fenalco
     *jemartinez 13 de junio de 2008
     */
      private String[] getObservacionesFacturaDAO(String noFactura) throws Exception{

        Connection con = null;//JJCastro fase2
        PreparedStatement state = null;
        ResultSet result = null;
        String[] observaciones = null;
        String query = "OBSERVACIONES_FACTURA_FENALCO";
        String query2 = "OBSERVACIONES_FACTURA_FENALCO_2";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                  //Estas condiciones verifican el tipo de observaciones dependiendo de la
                //factura. Si es una PC se deben buscar las ND asociadas a esa factura.
                //Si es PL las observaciones de la PL
                if (noFactura.substring(0, 2).equalsIgnoreCase("PC")) {
                      state = con.prepareStatement(this.obtenerSQL(query),ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);//JJCastro fase2
                } else {
                      state = con.prepareStatement(this.obtenerSQL(query2),ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);//JJCastro fase2
                }
                state.setString(1, noFactura);
                result = state.executeQuery();

                result.last();
                observaciones = new String[result.getRow()];
                result.beforeFirst();

                int i = 0;
                while (result.next()) {
                    observaciones[i] = result.getString(1) + ";" + result.getString(2) + ";" + result.getString(3) + ";" + result.getString(4);
                    i++;
                }

            }
          }catch(SQLException e){
            throw new Exception(e.getMessage());
          }finally{
            if (state  != null){ try{ state .close();              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return observaciones;
    }

    /**
       * Este metodo obtiene los datos correspondientes al informe a enviar a fenalco
     * @param noFactura con el numero de la factura
     * @return un BeanGeneral con los datos
     * @author jemartinez 13 de junio de 2008
     */
      public BeanGeneral getReporteFenalcoDAO(String noFactura) throws Exception{
        Connection con = null;
        PreparedStatement state = null;
        ResultSet r = null;
          String query="REPORTE_NOTA_FENALCO";
        BeanGeneral reporteChequesDevueltos = new BeanGeneral();
          try{
            con = this.conectarJNDI(query);
            if (con != null) {
                state = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              state.setString(1,noFactura);
              state.setString(2,noFactura);
                r = state.executeQuery();
              if(r.next()){
                    reporteChequesDevueltos.setValor_01(r.getString(1));//NIT
                    reporteChequesDevueltos.setValor_02(r.getString(2));//nombre cliente
                    reporteChequesDevueltos.setValor_03(r.getString(3));//direccion
                    reporteChequesDevueltos.setValor_04(r.getString(4));//telefono
                    reporteChequesDevueltos.setValor_05(r.getString(5));//telefono contacto
                    reporteChequesDevueltos.setValor_06(r.getString(6));//codigo del banco
                    reporteChequesDevueltos.setValor_07(r.getString(7));//aval
                    reporteChequesDevueltos.setValor_08(r.getString(8));//numero de cuenta del cheque
                    reporteChequesDevueltos.setValor_09(r.getString(9));//beneficiario
                    reporteChequesDevueltos.setValor_10(r.getString(10));//numero del cheque
                    reporteChequesDevueltos.setValor_11(r.getString(11));//numero de la factura
                    reporteChequesDevueltos.setValor_12(r.getString(12));//valor saldo
                    reporteChequesDevueltos.setV1(this.getObservacionesFacturaDAO(noFactura));//El array de strings con las observaciones
                }

          }}catch(Exception e){
              throw new Exception("Error: "+e.getMessage());
          }finally{
            if (state  != null){ try{state.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return reporteChequesDevueltos;
    }

    public Ingreso getIngreso(Ingreso ingreso) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INGRESO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, ingreso.getDstrct());
                ps.setString(2, ingreso.getTipo_documento());
                ps.setString(3, ingreso.getNum_ingreso());
                rs = ps.executeQuery();
                if (rs.next()) {
                    ingreso.setDstrct(rs.getString("dstrct"));
                    ingreso.setCodcli(rs.getString("codcli"));
                    ingreso.setNitcli(rs.getString("nitcli"));
                    ingreso.setNum_ingreso(rs.getString("num_ingreso"));
                    ingreso.setConcepto(rs.getString("concepto"));
                    ingreso.setTipo_ingreso(rs.getString("tipo_ingreso"));
                    ingreso.setFecha_consignacion(rs.getString("fecha_consignacion"));
                    ingreso.setFecha_ingreso(rs.getString("fecha_ingreso"));
                    ingreso.setBranch_code(rs.getString("branch_code"));
                    ingreso.setBank_account_no(rs.getString("bank_account_no") != null ? rs.getString("bank_account_no") : "");
                    ingreso.setCodmoneda(rs.getString("codmoneda"));
                    ingreso.setAgencia_ingreso(rs.getString("agencia_ingreso"));
                    ingreso.setVlr_ingreso(rs.getDouble("vlr_ingreso"));
                    ingreso.setVlr_ingreso_me(rs.getDouble("vlr_ingreso_me"));
                    ingreso.setFecha_tasa(rs.getString("fecha_tasa"));
                    ingreso.setDescripcion_ingreso(rs.getString("descripcion_ingreso"));
                    ingreso.setCant_item(rs.getInt("cant_item"));
                    ingreso.setTipo_documento(rs.getString("tipo_documento"));
                    ingreso.setFecha_anulacion(rs.getString("fecha_anulacion"));
                    ingreso.setAbc(rs.getString("abc"));
                    ingreso.setTasaDolBol(rs.getDouble("tasa_dol_bol"));
                    ingreso.setCuenta_banco(rs.getString("cuenta_banco"));
                    if (rs.getString("auxiliar").length() > 0) {
                        String[] aux = rs.getString("auxiliar").split("-");
                        ingreso.setTipo_aux(aux[0]);
                        ingreso.setAuxiliar(aux[1]);
                    } else {
                        ingreso.setTipo_aux("");
                        ingreso.setAuxiliar("");
                    }
                    ingreso.setCuenta(rs.getString("codigo_cuenta_contable"));
                    ingreso.setReg_status("");
                    ingreso.setFecha_contabilizacion("0099-01-01 00:00:00");
                    ingreso.setTransaccion(0);

                    ingreso.setPeriodo("000000");
                    ingreso.setCreation_date(Util.fechaActualTIMESTAMP());
                    ingreso.setLast_update(Util.fechaActualTIMESTAMP());
                    ingreso.setBase(rs.getString("base"));
                    ingreso.setNro_consignacion("");

                    ingreso.setVlr_tasa(Double.parseDouble(rs.getString("vlr_tasa")));
                    ingreso.setVlr_saldo_ing(Double.parseDouble(rs.getString("saldo_ingreso")));
                    ingreso.setCmc(rs.getString("cmc"));

                }
            }
        } catch (Exception ex) {
            System.out.println("errror en negocios dao" + ex.toString() + "__" + ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return ingreso;
    }

    public ArrayList<String> newfactDAORemix(String a, String documento) throws Exception {
        Connection con = null;
        String respuesta = "";
        String msg = "";
        PreparedStatement fact = null;
        PreparedStatement detfact = null;
        StringStatement infact = null;
        StringStatement indetfact = null;
        String[] datos = new String[31];
        ResultSet rs = null;
        ResultSet rs1 = null;
        String cmcx = "";
        String query = "SQL_FACT";
        String query2 = "SQL_DET_FACT";
        ArrayList<String> listSql = new ArrayList<String>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                try {
                    fact = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                    detfact = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                    fact.setString(1, a);
                    detfact.setString(1, a);
                    rs = fact.executeQuery();
                    if (rs.next()) {
                        datos[0] = rs.getString("tipo_documento");
                        datos[1] = rs.getString("documento");
                        datos[2] = rs.getString("nit");
                        datos[3] = rs.getString("codcli");
                        datos[4] = rs.getString("concepto");
                        datos[5] = rs.getString("fecha_factura");
                        datos[6] = rs.getString("fecha_vencimiento");
                        datos[7] = rs.getString("descripcion");
                        datos[8] = rs.getString("valor_factura");
                        datos[9] = rs.getString("creation_user");
                        datos[10] = rs.getString("valor_facturame");
                        datos[11] = rs.getString("valor_saldo");
                        datos[12] = rs.getString("valor_saldome");
                        datos[13] = rs.getString("negasoc");
                        datos[14] = rs.getString("base");
                        datos[30] = rs.getString("num_doc_fen");
                        cmcx = rs.getString("cmc");
                    }
                    rs1 = detfact.executeQuery();

                } catch (SQLException e) {
                    throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS " + e.getMessage() + " " + e.getErrorCode());
                }
                if (rs.getRow() > 0) {

                    infact = new StringStatement(this.obtenerSQL("SQL_INSERTAR_FACTURA_2"), true);//JJCastro fase2
                    infact.setString(1, "NDC");
                    infact.setString(2, documento);
                    infact.setString(3, datos[2]);
                    infact.setString(4, datos[3]);
                    infact.setString(5, datos[4]);
                    infact.setString(6, datos[5]);
                    infact.setString(7, datos[6]);
                    infact.setString(8, datos[7]);
                    infact.setString(9, datos[8]);
                    infact.setString(10, datos[9]);
                    infact.setString(11, datos[10]);
                    infact.setString(12, datos[8]);
                    infact.setString(13, datos[8]);
                    infact.setString(14, datos[13]);
                    infact.setString(15, datos[14]);
                    infact.setString(16, cmcx);
                    infact.setString(17, datos[30]);
                    listSql.add(infact.getSql());

                }
                while (rs1.next()) {
                    datos[15] = rs1.getString("tipo_documento");
                    datos[16] = rs1.getString("documento");
                    datos[17] = rs1.getString("item");
                    datos[18] = rs1.getString("nit");
                    datos[19] = rs1.getString("concepto");
                    datos[20] = rs1.getString("descripcion");
                    datos[21] = rs1.getString("valor_unitario");
                    datos[22] = rs1.getString("valor_item");
                    datos[23] = rs1.getString("creation_user");
                    datos[24] = rs1.getString("valor_unitariome");
                    datos[25] = rs1.getString("valor_itemme");
                    datos[26] = rs1.getString("numero_remesa");
                    if (datos[26] == null || datos[26].equals("")) {
                        datos[26] = datos[16];
                    }
                    datos[27] = rs1.getString("base");
                    datos[28] = rs1.getString("auxiliar");
                    datos[29] = rs1.getString("codigo_cuenta_contable");

                    //numero 2
                    indetfact = new StringStatement(this.obtenerSQL("SQL_INSERTAR_DET_FACTURA_2"), true);//JJCastro fase2
                    indetfact.setString(1, "NDC");
                    indetfact.setString(2, documento);
                    indetfact.setString(3, datos[17]);
                    indetfact.setString(4, datos[18]);
                    indetfact.setString(5, datos[19]);
                    indetfact.setString(6, datos[20]);
                    indetfact.setString(7, datos[21]);
                    indetfact.setString(8, datos[22]);
                    indetfact.setString(9, datos[23]);
                    indetfact.setString(10, datos[24]);
                    indetfact.setString(11, datos[25]);
                    indetfact.setString(12, datos[1]);
                    indetfact.setString(13, datos[27]);
                    indetfact.setString(14, datos[28]);
                    indetfact.setString(15, datos[29]);
                    listSql.add(indetfact.getSql());

                }

            } else {
                msg = "Rectifique el documento, no ha sido encontrado";
            }
            datos = null;


        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (rs1 != null) {
                try {
                    rs1.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (fact != null) {
                try {
                    fact.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (detfact != null) {
                try {
                    detfact = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (indetfact != null) {
                try {
                    indetfact = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        //return msg;///
        return listSql;
    }


    public void setCheques(ArrayList cheques) throws Exception {
            chequesx=cheques;
    }



    public ArrayList obtainDocsAceptados(String codnegx) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CONSULTA_INTERESES_NEGS";
        ArrayList docs = null;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, codnegx);

                rs = ps.executeQuery();
                while (rs.next()) {
                    if (docs == null) {
                        docs = new ArrayList();
                    }

                    chequeCartera chequeCarterax = new chequeCartera();
                    chequeCarterax.setFechaCheque(rs.getString("fecha"));
                    chequeCarterax.setDias(rs.getString("dias"));

                    chequeCarterax.setSaldoInicial(rs.getString("saldo_inicial"));
                    chequeCarterax.setValorCapital(rs.getString("capital"));
                    chequeCarterax.setValorInteres(rs.getString("interes"));
                    chequeCarterax.setValor(rs.getString("valor"));
                    chequeCarterax.setSaldoFinal(rs.getString("saldo_final"));
                    chequeCarterax.setNoAval((rs.getString("no_aval")));
                    docs.add(chequeCarterax);
                    chequeCarterax = null;//Liberar Espacio JJCastro
                    //Image x=new Image();

                }
       }}
        catch (Exception ex)
        {
            System.out.println("errror en negocios dao en obtainDocsAceptados"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return docs;
    }

   //////////creado por Miguel ALtamiranda

    public ArrayList getAfiliado(String nombre)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
            ResultSet rs=null;
            ArrayList ret=new ArrayList();
        String query = "SQL_GET_AFILIADO";
            try
            {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,nombre);
                rs=st.executeQuery();
                while(rs.next())
                {   ArrayList ret2=new ArrayList();
                    ret2.add(rs.getString("nombre"));
                    ret2.add(rs.getString("direccion"));
                    ret2.add(rs.getString("ciudad"));
                    ret2.add(rs.getString("codigo_usuario"));
                    ret.add(ret2);
                    ret2 = null;//Liberar Espacio JJCastro
                }
                } }
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE SQL_GET_AFILIADO " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return ret;
    }


    public ArrayList getdatosnegocio(String codn)throws SQLException
    {
        Connection con = null;
        PreparedStatement st = null;
            ResultSet rs=null;
            ArrayList ret=new ArrayList();
        String query = "SQL_GET_OPERACIONES2";
            try
            {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,codn);
                rs=st.executeQuery();
                while(rs.next())
                {   ArrayList ret2=new ArrayList();
                    ret2.add(rs.getString("num_cheque"));
                    ret2.add(rs.getString("cta_girador"));
                    ret2.add(rs.getString("plaza_cheque"));
                    ret2.add(rs.getString("ciud_custod"));
                    ret.add(ret2);
                    ret2 = null;//Liberar Espacio JJCastro
                }
                }}
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE OPERACIONES2 " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return ret;
    }


    public ArrayList getCtasFintra()throws SQLException
    {       
        Connection con = null;
        PreparedStatement st = null;
            ResultSet rs=null;
            ArrayList ret=new ArrayList();
        String query = "SQL_CUENTAS_FINTRA";
            try
            {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs=st.executeQuery();
                while(rs.next())
                {   ArrayList ret2=new ArrayList();
                    ret2.add(rs.getString("No_cta"));
                    ret2.add(rs.getString("table_code"));
                    ret2.add(rs.getString("direccion"));
                    ret2.add(rs.getString("nomciu"));
                    ret.add(ret2);
                    ret2 = null;//Liberar Espacio JJCastro
                }
                }}
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE CUENTAS FINTRA " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return ret;
    }

    public String borrarcomasguiones(String cad)
    {   String resultado="";
        for(int i=0;i<cad.length();i++)
        {   if(!(cad.substring(i,i+1).equals(",")||cad.substring(i,i+1).equals("-")))
            {    resultado=resultado+cad.substring(i,i+1);
            }
        }
        return resultado;
    }


    public String reemplazarguiones(String cad)
    {   String resultado="";
        for(int i=0;i<cad.length();i++)
        {   if(cad.substring(i,i+1).equals("-"))
            {   resultado=resultado+"/";
            }
            else
            {   resultado=resultado+cad.substring(i,i+1);
        }
        }
        return resultado;
    }

    public String fromatearfecha(String cad)
    {   return cad.substring(8,10)+cad.substring(4,8)+cad.substring(0,4);
    }

    public ArrayList getOperaciones(String fechai, String fechaf)throws SQLException
    {
        Connection con = null;
        PreparedStatement st = null;
            ResultSet rs=null;
            ArrayList ret=new ArrayList();
        String query = "SQL_GET_OPERACIONES";
            try
            {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs=st.executeQuery();
                while(rs.next())
                {   ArrayList ret2=new ArrayList();
                    ret2.add(rs.getString("no_orden"));
                    ret2.add(rs.getString("cod_suc"));
                    ret2.add(rs.getString("beneficiario"));
                    ret2.add(rs.getString("ciudad_girad"));
                    ret2.add(rs.getString("nombre"));
                    ret2.add(rs.getString("ced_girador"));
                    ret2.add(borrarcomasguiones(rs.getString("num_cheque")));
                    ret2.add(rs.getString("cta_girador"));
                    ret2.add(rs.getString("cod_ent_cheque"));
                    ret2.add(borrarcomasguiones(rs.getString("valor_cheque")));
                    ret2.add(rs.getString("plaza_cheque"));
                    ret2.add(fromatearfecha(reemplazarguiones(rs.getString("fec_consigna"))));
                    ret2.add(rs.getString("cta_consigna"));
                    ret2.add(rs.getString("nom_consigna"));
                    ret2.add(rs.getString("num_obl_cheq"));
                    ret2.add(rs.getString("dir_destino"));
                    ret2.add(rs.getString("ciud_custod"));
                    ret.add(ret2);
                }
             }}
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE OPERACIONES " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return ret;
    }

    public ArrayList getCiudades()throws SQLException
    {
        Connection con = null;
        PreparedStatement st = null;
            ResultSet rs=null;
            ArrayList ret=new ArrayList();
        String query = "SQL_GET_CIUDADES";
            try
            {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs=st.executeQuery();
                while(rs.next())
                {   ret.add(rs.getString("nomciu"));
                }
             }}
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE SQL_GET_CIUDADES " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return ret;
    }

    public void ingresar_ordenes(String no_orden,String nomafiliado, String ciudad, String nom_cli,String cod_cli,String ch,String no_cta,String cod_banco,String valor,String plaza, String fecha, String c_fintra, String a_favor_de, String num_aval, String c_custodia, String dirbanco, String cod_neg,String fecha_operacion) throws java.sql.SQLException{
        Connection con = null;
        PreparedStatement st= null;
        String query="SQL_INSERTAR_ORDENES_DOMESA";
        if(c_custodia.equals("0"))
        {   c_custodia=ciudad;
        }
        else
        {   c_custodia="BARRANQUILLA";
        }
        try
        {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,no_orden);
            st.setString(2,nomafiliado);
            st.setString(3,ciudad);
            st.setString(4,cod_neg);
            st.setString(5,nom_cli);
            st.setString(6,cod_cli);
            st.setString(7,ch);
            st.setString(8,no_cta);
            st.setString(9,cod_banco);
            st.setString(10,valor);
            st.setString(11,plaza);
            st.setString(12,fecha);
            st.setString(13,c_fintra);
            st.setString(14,a_favor_de);
            st.setString(15,num_aval);
            st.setString(16,dirbanco);
            st.setString(17,c_custodia);
            st.setString(18,fecha_operacion);
                st.execute();
        }}
        catch (Exception ex)
        {   System.out.println("error por ordenes"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
            throw new SQLException(ex.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            }




    public void ingresar_ordenes2(String exno_orden,String exnum_aval,String no_orden,String nomafiliado, String ciudad, String nom_cli,String cod_cli,String ch,String no_cta,String cod_banco,String valor,String plaza, String fecha, String c_fintra, String a_favor_de, String num_aval, String c_custodia, String dirbanco, String fecha_operacion) throws java.sql.SQLException{
        Connection con = null;
        PreparedStatement st= null;
        String query="SQL_MOD_ORDENES_DOMESA";
        if(c_custodia.equals("0"))
        {   c_custodia=ciudad;
        }
        else
        {   c_custodia="BARRANQUILLA";
        }
        try
        {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,no_orden);
            st.setString(2,nomafiliado);
            st.setString(3,ciudad);
            st.setString(4,nom_cli);
            st.setString(5,cod_cli);
            st.setString(6,ch);
            st.setString(7,no_cta);
            st.setString(8,cod_banco);
            st.setString(9,valor);
            st.setString(10,plaza);
            st.setString(11,c_fintra);
            st.setString(12,a_favor_de);
            st.setString(13,num_aval);
            st.setString(14,dirbanco);
            st.setString(15,c_custodia);
            st.setString(16,fecha_operacion);
            st.setString(17,exno_orden);
            st.setString(18,fecha);
            st.setString(19,exnum_aval);
                //ystem.out.println(st.toString());
                st.execute();
        }}
        catch (Exception ex)
        {   System.out.println("error por ordenes"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
            throw new SQLException(ex.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            }

    public String getno_orden(String numaval) throws java.sql.SQLException{
        Connection con = null;
        PreparedStatement st= null;
        ResultSet rs=null;
        String resultado="";
        String query = "SQL_GET_NO_ORDEN";
        try
        {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,numaval);
            rs=st.executeQuery();
            if(rs.next())
            {   resultado=rs.getString("no_orden");
                }
        }}
        catch(SQLException e)
        {
            throw new SQLException("ERROR DURANTE SQL_GET_NO_ORDEN " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return resultado;
    }


    public void admitneg(String cod,String estado_neg)throws SQLException//agregado por Miguel Altamiranda
    {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR_APROBACION_DOMESA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, estado_neg);
                st.setString(2, cod);
                st.executeUpdate();
            }
        }
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE LAS APROBACION DE NEGOCIOS " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            }


      public void admitneg(String u,String ob,String cod,String estado_neg)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR_APROBACION22";
            try
            {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, u);
                st.setString(2, ob);
                st.setString(3,estado_neg);
                st.setString(4, cod);
                st.executeUpdate();
                }}
            catch(SQLException e)
            {
                 throw new SQLException("ERROR DURANTE LAS APROBACION DE NEGOCIOS " + e.getMessage() + " " + 		e.getErrorCode());
             }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
                }

      public BeanGeneral newLetras(String negocio) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_NEW_DATOS_LETRAS";
            String  query2 = "SQL_NEW_DATOS_LETRAS_2";
        BeanGeneral datos = new BeanGeneral();
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, negocio);
                rs = ps.executeQuery();
                if (rs.next()){
                    datos.setValor_01(rs.getString("NO_PAGARE"));            //No. Pagare
                    datos.setValor_02(rs.getString("FECHA_NEGOCIO"));        //Fecha Negocio
                    datos.setValor_03(rs.getString("TOTAL"));                //Total
                    datos.setValor_04(rs.getString("NRO_DOCS"));             //Nro de Documentos
                    datos.setValor_05(rs.getString("TASA_INTERES"));         //Tasa Interes
                    datos.setValor_06(rs.getString("CLIENTE"));              //Nombre Cliente
                    datos.setValor_07(rs.getString("NIT_CLIENTE"));          //CC Cliente
                    datos.setValor_08(rs.getString("DIR_CLIENTE"));          //Direccion Cliente
                    datos.setValor_09(rs.getString("CIUDAD_CLIENTE"));       //Ciudad Cliente
                    datos.setValor_10(rs.getString("AFILIADO"));             //Nombre del Afiliado
                    datos.setValor_11(rs.getString("NIT_AFILIADO"));         //Nit del Afiliado
                    datos.setValor_12(rs.getString("DIR_AFILIADO"));         //Direccion del Afiliado
                    datos.setValor_13(rs.getString("CIUDAD_AFILIADO").toUpperCase());      //Ciudad del Afiliado
                } else {

                    ps = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                    ps.setString(1, negocio);
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        datos.setValor_01(rs.getString("NO_PAGARE"));            //No. Pagare
                        datos.setValor_02(rs.getString("FECHA_NEGOCIO"));        //Fecha Negocio
                        datos.setValor_03(rs.getString("TOTAL"));                //Total
                        datos.setValor_04(rs.getString("NRO_DOCS"));             //Nro de Documentos
                        datos.setValor_05(rs.getString("TASA_INTERES"));         //Tasa Interes
                        datos.setValor_06(rs.getString("CLIENTE"));              //Nombre Cliente
                        datos.setValor_07(rs.getString("NIT_CLIENTE"));          //CC Cliente
                        datos.setValor_08(rs.getString("DIR_CLIENTE"));          //Direccion Cliente
                        datos.setValor_09(rs.getString("CIUDAD_CLIENTE"));       //Ciudad Cliente
                        datos.setValor_10(rs.getString("AFILIADO"));             //Nombre del Afiliado
                        datos.setValor_11(rs.getString("NIT_AFILIADO"));         //Nit del Afiliado
                        datos.setValor_12(rs.getString("DIR_AFILIADO"));         //Direccion del Afiliado
                        datos.setValor_13(rs.getString("CIUDAD_AFILIADO").toUpperCase());      //Ciudad del Afiliado
                    }

                }
            }
          }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            } finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return datos;
    }

     //--Add 01-Junio-2009

    /**
     * Actualiza el valor de la comision de un determinado negocio
     * @param codn Codigo del negocio
     * @param com comision
     * @param nomb Banco
     * @param codb sucursal
     * @autor tmolina
     * @throws Exception
     */
       public String actcomDString(String cxp,String codn,String com,String nomb,String codb)throws SQLException{
        StringStatement st = null;
        String sql = "";
        String query = "ACTUALIZAR_COMISION";
           try{
               st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, com);
            st.setString(2, nomb);
            st.setString(3, codb);
            st.setString(4, codn);
            st.setString(5, cxp);
            st.setString(6, com);
            st.setString(7, codn);


            sql = st.getSql();
               //.out.println(sql);
            //st.executeUpdate();
           }catch(Exception e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE COMISIONES \n" + e.getMessage());
           }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                }
        return sql;
    }

    /**
     * Atualiza el negocio para indicar que fue transferido.
     * @param cod Codigo del Negocio
     * @autor tmolina
     * @throws SQLException
     */
       public String transfnegString(String cod)throws SQLException{
        StringStatement st = null;
        String sql = "";
        String query;
        boolean compraCartera;
        try {
            compraCartera = compraCarteraNegocio(cod);
            if (compraCartera != false) {
                query = "SQL_ACTUALIZAR_NEGOCIO_COMPRA_CARTERA";
            } else {
                query = "SQL_ACTUALIZAR_TRANSF";
            }
               st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, cod);
            sql = st.getSql();
               //.out.println(sql);
            //st.executeUpdate();
           }catch(Exception e){
            throw new SQLException("ERROR DURANTE LAS ACTUALIZACION DE NEGOCIOS \n" + e.getMessage());
           }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                }
        return sql;
    }

    public boolean compraCarteraNegocio(String negocio) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICAR_NEGOCIO_COMPRA_CARTERA";
        boolean compraCartera = false;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, negocio);
                rs = ps.executeQuery();
                if (rs.next()) {
                    compraCartera = (rs.getBoolean(1));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());}}
            if (ps != null) {try {ps.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null) {try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
            return compraCartera;
        }

    }

    /**
         * Obtiene informaci�n referente a un negocio para la elaboraci�n de un e-mail
     * @param a Codigo del Negocio
     * @autor tmolina
     * @throws SQLException
     */
       public BeanGeneral MailDAO2(String codneg) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
           String  query = "GET_INFO_MAIL2";
        BeanGeneral mail = new BeanGeneral();
           try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, codneg);
                rs = ps.executeQuery();
                //.out.println("Query Datos Mail-> "+ps.toString());
               if (rs.next()){
                    mail.setValor_01(rs.getString(1));//nombre cliente
                    mail.setValor_02(rs.getString(2));//id cliente
                    mail.setValor_03(rs.getString(3));//fecha negocio
                    mail.setValor_04(rs.getString(4));//bco
                    mail.setValor_05(rs.getString(5));//vr desembolso
                    mail.setValor_06(rs.getString(6));//now()
                    mail.setValor_07(rs.getString(7));//nombre proveedor
                    mail.setValor_08(rs.getString(8));//mail
                }
           }}catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
           }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return mail;
    }

    /**
     * Crea el egreso del negocio transferido.
     * @param codn Codigo del Negocio
     * @param a Usuario en sesion
     * @param banco Banco de transferencia
     * @param sucursal Sucuarsal trasnferencia
     * @param comision Comision bancaria
     * @autor tmolina
     * @throws SQLException
     */
       public ArrayList saveEGDAOString(String cxp,String codn, Usuario a, String banco, String sucursal, String comision)throws Exception{
        //ArrayList para guardar querys...
        ArrayList listaQuerys = new ArrayList();
        Connection con = null;
        PreparedStatement st = null;
        StringStatement eg = null;
        StringStatement egd = null;
        double tot = 0;
        String sql = "";
        String[] datos = new String[13];
        //TransaccionService  scv = new TransaccionService(a.getBd());
        ResultSet rs = null;
        String query = "SQL_NEG_PROV_II";
            StringStatement upcxp=null;
            try{

            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

                eg = new StringStatement (this.obtenerSQL("SQL_INSERT_EGRESO_II"), true);//JJCastro fase2
                egd = new StringStatement (this.obtenerSQL("SQL_INSERT_EGRESODET_II"), true);//JJCastro fase2
                upcxp=new StringStatement (this.obtenerSQL("SQL_UPDATE_CXP_DOC"), true);
                st.setString(1, cxp);
                st.setString(2, codn);
                rs = st.executeQuery();
                if(rs.next()){
                    datos[0] = banco;
                    datos[1] = sucursal;
                    datos[2] = rs.getString("NIT_TERCERO");
                    datos[3] = rs.getString("PAYMENT_NAME");
                    datos[4] = rs.getString("COD_CLI");
                    datos[5] = comision;
                    datos[6] = rs.getString("VR_DESEMBOLSO");
                    datos[7] = "0";                         //No se utiliza
                    datos[8] = a.getLogin();
                    datos[9] = rs.getString("TIPO_DOCUMENTO");
                    datos[10]= rs.getString("DOCUMENTO");
                    datos[11]= rs.getString("VLR_SALDO_ME");
                    datos[12]= rs.getString("MONEDA");
                }
                tot = Double.parseDouble(datos[6])-Double.parseDouble(datos[5]);
                //INICIO egreso
                eg.setString(1, datos[0]);
                eg.setString(2, datos[1]);
                eg.setString(3, datos[2]);
                eg.setString(4, datos[3]);
                eg.setDouble(5, tot);
                eg.setDouble(6, tot);
                eg.setString(7, a.getLogin());
                eg.setString(8, datos[4]);
                eg.setString(9, datos[2]);
                eg.setString(10, datos[5]);
                // sql=eg.getSql();
                listaQuerys.add(eg.getSql());
                 //FIN egreso

                //inicio egresodet
                egd.setString(1, datos[0]);
                egd.setString(2, datos[1]);
                egd.setString(3, datos[6]);
                egd.setString(4, datos[6]);
                egd.setString(5, datos[8]);
                 egd.setString(6, datos[9] );
                egd.setString(7, datos[10]);
                // sql+=egd.getSql();
                listaQuerys.add(egd.getSql());
                 //fn egresodet

                 //inicio actualizar cxp

                  TasaService  svc   =  new  TasaService(a.getBd());
                  ChequeXFacturaService md= new ChequeXFacturaService(a.getBd());
              String  hoy        =  Util.getFechaActual_String(4);

              String monedaLocal =  md.getMonedaLocal(a.getDstrct());
              String monedaME    =  datos[12];
              double vlrME       =  Double.parseDouble(datos[11]);
              double vlrLocal    =  vlrME;

                // Convertimos Moneda:
              if( !monedaME.equals( monedaLocal )  ){
                  Tasa  obj  =  svc.buscarValorTasa(monedaLocal,  monedaME , monedaLocal , hoy);
                  if(obj!=null)
                        vlrLocal *=  obj.getValor_tasa();
                    }
              vlrME     =  ( monedaME.   equals("DOL")   )? Util.roundByDecimal(vlrME,    2) : (int)Math.round(vlrME);
              vlrLocal  =  ( monedaLocal.equals("DOL")   )? Util.roundByDecimal(vlrLocal, 2) : (int)Math.round(vlrLocal);
                // set:
                upcxp.setString(1,    datos[0]      );
                upcxp.setString(2,    datos[1]   );
                upcxp.setDouble(3,    vlrLocal  );
                upcxp.setDouble(4,    vlrLocal  );
                upcxp.setDouble(5,    vlrME     );
                upcxp.setDouble(6,    vlrME     );
                upcxp.setString(7,    a.getLogin()      );
                // where:
                upcxp.setString(8,   a.getDstrct()         );
                upcxp.setString(9,   datos[2]     );
                upcxp.setString(10,   datos[9] );
                upcxp.setString(11,   datos[10]      );
                //sql+=upcxp.getSql();
                listaQuerys.add(upcxp.getSql());
                 //fin actualizar cxp

                 //INICIO Aumentar Serie de Egreso
                //sql+= this.incrementarSerie("EGRESO");
                listaQuerys.add(this.incrementarSerie("EGRESO"));
                //FIN Aumentar Serie de Egreso
                this.setValor_cxp(tot);

                                
            }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE EGRESOS " + e.getMessage() + " " + e.getErrorCode());
            }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (eg  != null){ try{ eg = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (egd  != null){ try{egd = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                datos=null;
                }
        //.out.println("Querys "+sql);
        return listaQuerys;
    }

    /**
     * Incrementa la serie para un determinado tipo de documento
     * @param tipo tipo de documento
     * @throws Exception
     */
   public String  incrementarSerie(String tipo) throws Exception {
        StringStatement  st = null;
        String sql = "";
        String query = "SQL_INC_SERIE";
        try{
             st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, tipo);
            sql = st.getSql();
        }catch(Exception e){
            throw new Exception("Error al incrementar la serie [NegociosDAO.xml]... \n"+e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                }
        return sql;
    }


   

   public ArrayList getDatos(String tabla, String negocio) throws Exception
   {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String  query = tabla;
        ArrayList datos = new ArrayList();
       try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, negocio);
             rs=st.executeQuery();
            while (rs.next()){
                String [] a=new String[2];
                a[0]=rs.getString("dato");
                a[1]=rs.getString("cod_dato");
                    System.out.println(a[0]);
                    datos.add(a);
                }
        }}catch (Exception ex){
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return datos;
    }




    public String getDatos_pagare(String negocio)throws Exception
    {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";String ret="";
        ResultSet rs = null;
        String query = "SRCH_PAGARE";

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2 
                st.setString(1, negocio);
             rs=st.executeQuery();
             if(rs.next())
             {  ret=rs.getString("num_aval");
                ret=ret+"-_-"+rs.getString("idendoso");
                ret=ret+"-_-"+rs.getString("cp_pagare");
                ret=ret+"-_-"+rs.getString("co_pagare");
                ret=ret+"-_-"+rs.getString("d_afiliado");
                ret=ret+"-_-"+rs.getString("a_favor");
                ret=ret+"-_-"+rs.getString("id_codeudor");
                ret=ret+"-_-"+rs.getString("estado_neg");
                }
             else
             {  ret="-_--_--_--_--_--_--_-";
             }

        }}catch(Exception e){
            throw new Exception("Error al asociar el codeudor al negocio [NegociosDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return ret;
    }


    

    public void setCodeudor(String codneg,String idcodeudor,String idendoso,String domiciliopago,String ciudadotorgamiento,String domi2,String afavor) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "UPDATE_COD";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, idcodeudor);
                st.setString(2, idendoso);
                st.setString(3, domiciliopago);
                st.setString(4, ciudadotorgamiento);
                st.setString(5, domi2);
                st.setString(6, afavor);
                st.setString(7, codneg);
                System.out.println(st.toString());
                st.execute();
            }}catch(Exception e){
            throw new Exception("Error al asociar el codeudor al negocio [NegociosDAO.xml]... \n"+e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
                }






public List<Negocios> Lista_filtro_negocios(String[] facturas) throws SQLException
{
        List<Negocios> lista = new LinkedList<Negocios>();

    String cod_negocio="";
    for (int i = 0; i <  facturas.length; i++)
    {
            String[] fact = facturas[i].split(";");
        cod_negocio=fact[6];
        if(valida_lista_negocio(cod_negocio,lista))
        {
                Negocios neg = new Negocios();
                neg.setCod_negocio(cod_negocio);
                lista.add(neg);
            }

        }


        return lista;
    }





public static boolean valida_lista_negocio(String negocio,List<Negocios> lista)
{
        boolean sw = true;
        int i = 0;
        while (i < lista.size()) {
            if (lista.get(i).getCod_negocio().compareTo(negocio) == 0) {
                sw = false;
            }
            i++;

        }
        return sw;
    }

public String ingresarCXC(BeanGeneral bg, String documentType, int numCuota) throws SQLException{
        String query = "SQL_INSERTAR_CXC";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo documento
                st.setString(2,documentType); //document type para generar el numero de factura
                st.setString(3,String.valueOf(numCuota)); //numero de cuota para generar el numero de factura
            st.setString(4, bg.getValor_01()); //nit
            st.setString(5, bg.getValor_01()); //nit para obtener el codcli
            st.setString(6, bg.getValor_05()); //concepto
            st.setString(7, (bg.getV2())[numCuota]); //Fecha factura
            st.setString(8, (bg.getV1())[numCuota]); //fecha_vencimiento
                st.setString(9,documentType); //descripcion(se guarda el document_type)
            st.setString(10, bg.getValor_03()); //valor_factura
            st.setInt(11, 1); //valor_tasa
            st.setString(12, "PES"); //moneda
            st.setInt(13, 1); //cantidad_items
            st.setString(14, "CREDITO"); //forma_pago
            st.setString(15, "OP"); //agencia_facturacion
            st.setString(16, "BQ"); //agencia_cobro
            st.setString(17, bg.getValor_04()); //creation_user
            st.setString(18, bg.getValor_03()); //valor_facturame
            st.setString(19, bg.getValor_03()); //valor_saldo
            st.setString(20, bg.getValor_03()); //valor_saldome
            st.setString(21, bg.getValor_06()); //negasoc
            st.setString(22, bg.getValor_07()); //base
                st.setString(23, (bg.getV4())[numCuota-1]); //num_doc_fen
            st.setString(24, ""); //tipo_ref1
            st.setString(25, ""); //ref1
            st.setString(26, bg.getValor_22()); //cmc
            st.setString(27, bg.getValor_18()); //dstrct
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXC[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXC(BeanGeneral bg, String documentType, int numCuota) throws SQLException{
        String query = "SQL_INSERTAR_DETALLE_CXC";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo_documento
            st.setString(2, documentType); //document type para generar el numero de factura
            st.setString(3, String.valueOf(numCuota)); //numero de cuota para generar el numero de factura
            st.setString(4, String.valueOf(numCuota)); //item
            st.setString(5, bg.getValor_01()); //nit
            st.setString(6, bg.getValor_05()); //concepto
            st.setString(7, bg.getValor_05()); //descripcion
            st.setInt(8, 1); //cantidad
            st.setString(9, bg.getValor_03()); //valor_unitario
            st.setString(10, bg.getValor_03()); //valor_item
            st.setInt(11, 1); //valor_tasa
            st.setString(12, "PES"); //moneda
            st.setString(13, bg.getValor_04()); //creation_user
            st.setString(14, bg.getValor_03()); //valor_unitariome
            st.setString(15, bg.getValor_03()); //valor_itemme
            st.setString(16, documentType); //numero_remesa (se envia el document_type para generar el numero de factura)
            st.setString(17, String.valueOf(numCuota)); //numero de cuota para generar el numero de factura
            st.setString(18, bg.getValor_07()); //base
            st.setString(19, bg.getValor_08()); //auxiliar
            st.setString(20, bg.getValor_21()); //Cuenta (codigo_cuenta_contable)
            st.setString(21, bg.getValor_18()); //Distrito
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXC[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarCXP(BeanGeneral bg, String documento, Convenio convenio) throws SQLException{
        String query = "SQL_INSERTAR_CXP";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

                double vd=Double.valueOf(bg.getValor_11()).doubleValue();
                double vs=0;
                double vs2=0;
                if(convenio.isDescuenta_gmf()){
                int max_cuota = Integer.parseInt(convenio.getCuota_gmf()); //Numero max de cuota para descontar gmf
                int numCuotas = Integer.parseInt(bg.getValor_02());
                if(max_cuota >= numCuotas){
                    vs += calcularGMF(bg.getValor_11(),(float) convenio.getPorc_gmf());
                    vs += calcularGMF(bg.getValor_11(),(float) convenio.getPorc_gmf2());
                }
            }
                if(convenio.isDescuenta_aval()){
                vs += Double.valueOf(bg.getValor_15()).doubleValue();
            }

            st.setString(1, bg.getValor_09()); //proveedor
            st.setString(2, "FAP");//tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, bg.getValor_09()); //descripcion (Se envia el nit del afiliado para colocarlo en la descripcion)
            st.setString(5, "OP"); //agencia
            st.setString(6, bg.getValor_09()); //banco (Se envia el nit del afiliado)
            st.setString(7, bg.getValor_09()); //sucursal (Se envia el nit del afiliado)
            st.setDouble(8, vd - vs2);//vlr_neto
            st.setDouble(9, vd - vs);//vlr_saldo
            st.setDouble(10, vd - vs2);//vlr_neto_me
            st.setDouble(11, vd - vs);//vlr_saldo_me
            st.setInt(12, 1);//tasa
            st.setString(13, bg.getValor_04());//usuario creacion
            st.setString(14, bg.getValor_07());//base
            st.setString(15, "PES");//moneda_banco
            st.setString(16, "NEG");//clase_documento_rel
            st.setString(17, "PES");//moneda
            st.setString(18, "NEG");//tipo_documento_rel
            st.setString(19, bg.getValor_06());//documento_relacionado
            st.setString(20, bg.getValor_23()); //handle_code
            st.setString(21, bg.getValor_18());//Distrito
            st.setString(22, this.aprobadorCxPNeg());//aprobador
            st.setString(23, bg.getValor_04());//usuario aprobacion
            sql += st.toString();
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXP[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXP(BeanGeneral bg, String documento, Convenio convenio) throws SQLException{
        String query = "INSERTAR_DETALLE_CXP";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

                double vd=Double.valueOf(bg.getValor_11()).doubleValue();
                double vs2=0;
            st.setString(1, bg.getValor_09()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, "1"); //item
            st.setString(5, "DESEMBOLSO " + bg.getValor_09()); //descripcion
            st.setDouble(6, vd - vs2);//vlr
            st.setDouble(7, vd - vs2);//vlr_me
            st.setString(8, bg.getValor_24());//codigo_cuenta
            st.setString(9, bg.getValor_06());//planilla
            st.setString(10, bg.getValor_04());//creation_user
            st.setString(11, bg.getValor_07());//base
            st.setString(12, "AR-" + bg.getValor_09());//AUXILIAR
            st.setString(13, bg.getValor_18());//Distrito
            sql += st.toString();
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXP[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    /**
     * Ingresa una nota credito a la CXP para el gravamen al movimiento financiero
     * @param maxCuota Maximo numero de cuota hasta el que se cobra gmf
     */
    public String ingresarNC_GMF(BeanGeneral bg, String documento, Convenio convenio, double valor) throws SQLException{
        String query = "SQL_INSERTAR_NC_CXP";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

                st.setString(1,bg.getValor_09());
                st.setString(2,documento);
                st.setString(3,bg.getValor_09());
                st.setString(4,bg.getValor_23());
                st.setString(5,bg.getValor_09());
                st.setString(6,bg.getValor_09());
                st.setDouble(7,valor);//vr calculado
                st.setDouble(8,0 );//vr saldo
                st.setDouble(9,valor );//vr calculado
                st.setDouble(10,0);//vr saldo
                st.setString(11,bg.getValor_04());//usuario creacion
                st.setString(12,documento );//Codigo de la factura
                st.setString(13,bg.getValor_18());//Codigo de la factura
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarNC_GMF[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleNC_GMF(BeanGeneral bg, String documento, Convenio convenio, double valor, String sec, String cuenta) throws SQLException{
        String query = "SQL_INSERTAR_DETALLE_NC_CXP";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, bg.getValor_09());//PROVEEDOR
            st.setString(2, documento);
            st.setString(3, sec);
            st.setString(4, "AJUSTE " + documento);
            st.setDouble(5, valor);//valor
            st.setDouble(6, valor);//valor
            st.setString(7, cuenta);//Cuenta
            st.setString(8, "");//CODNEGOCIO
            st.setString(9, bg.getValor_04());//USUARIO CREA
                st.setString(10, "AR-"+bg.getValor_09());//AUXILIAR
                st.setString(11,bg.getValor_18());//Codigo de la factura
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleNC_GMF[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    /**
     * Ingresa una nota credito para descontar el aval
     */
    public String ingresarNC_Aval(BeanGeneral bg, String documento) throws SQLException{
        String query = "SQL_INSERTAR_NC_CXP";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

                double vs=Double.valueOf(bg.getValor_15()).doubleValue();
                st.setString(1,bg.getValor_09());
                st.setString(2,documento);
                st.setString(3,bg.getValor_09());
                st.setString(4,bg.getValor_23());
                st.setString(5,bg.getValor_09());
                st.setString(6,bg.getValor_09());
                st.setDouble(7,vs);//vr calculado
                st.setDouble(8,0 );//vr calculado
                st.setDouble(9,vs );//vr calculado
                st.setDouble(10,0);//vr calculado
                st.setString(11,bg.getValor_04());//usuario crea
                st.setString(12,documento );//Codigo de la factura
                st.setString(13,bg.getValor_18());//Codigo de la factura
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarNC_Aval[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleNC_Aval(BeanGeneral bg, String documento) throws SQLException{
        String query = "SQL_INSERTAR_DETALLE_NC_CXP";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

                double vs=Double.valueOf(bg.getValor_15()).doubleValue();
            st.setString(1, bg.getValor_09());//PROVEEDOR
            st.setString(2, documento);
            st.setString(3, "1");
            st.setString(4, "AJUSTE " + documento);
            st.setDouble(5, vs);//valores
            st.setDouble(6, vs);//valores
            st.setString(7, bg.getValor_26());//Cuenta
            st.setString(8, "");//CODNEGOCIO
            st.setString(9, bg.getValor_04());//USUARIO CREA
                st.setString(10, "AR-"+bg.getValor_09());//AUXILIAR
                st.setString(11,bg.getValor_18());//Codigo de la factura
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleNC_Aval[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public Negocios buscarNegocio(String codNegocio) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_NEGOCIO";
        Negocios negocio = null;
        try {

            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codNegocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                negocio = new Negocios();
                negocio.setCod_negocio(rs.getString("cod_neg"));
                negocio.setCod_cli(rs.getString("cod_cli"));
                negocio.setFecha_neg(rs.getString("fecha_negocio"));
                negocio.setCod_tabla(rs.getString("cod_tabla"));
                negocio.setNodocs(rs.getInt("nro_docs"));
                negocio.setVr_desem(Double.parseDouble(rs.getString("vr_desembolso")));
                negocio.setVr_aval(rs.getFloat("vr_aval"));
                negocio.setVr_cust(rs.getFloat("vr_custodia"));
                negocio.setMod_aval(rs.getString("mod_aval"));
                negocio.setMod_cust(rs.getString("mod_custodia"));
                negocio.setPor_rem(rs.getFloat("porc_remesa"));
                negocio.setMod_rem(rs.getString("mod_remesa"));
                negocio.setVr_negocio(Double.parseDouble(rs.getString("vr_negocio")));
                negocio.setEsta(rs.getString("esta"));
                negocio.setCod_cod(rs.getString("id_codeudor"));
                negocio.setFpago(rs.getString("fpago"));
                negocio.setTneg(rs.getString("tneg"));
                negocio.setEstado(rs.getString("estado_neg"));
                negocio.setNit_tercero(rs.getString("nit_tercero"));
                negocio.setFechatran(rs.getString("f_desem"));
                negocio.setCmc(rs.getString("cmc"));
                negocio.setConlet(rs.getString("cletras"));
                negocio.setTotpagado(Double.parseDouble(rs.getString("tot_pagado")));
                negocio.setNumaval(rs.getString("num_aval"));
                negocio.setPagare(rs.getString("cpagare"));
                negocio.setCodigoBanco(rs.getString("banco_cheque"));
                negocio.setCuenta_cheque(rs.getString("cuenta_cheque"));
                negocio.setCnd_aval(rs.getString("cnd_aval"));
                negocio.setCreation_user(rs.getString("create_user"));
                negocio.setId_convenio(rs.getInt("id_convenio"));
                negocio.setvalor_aval(rs.getString("valor_aval"));
                negocio.setTasa(rs.getString("tasa"));
                negocio.setNuevaTasa(rs.getString("nueva_tasa"));
                negocio.setActividad(rs.getString("actividad"));
                negocio.setNom_cli(rs.getString("cliente"));
                negocio.setValor_capacitacion(rs.getDouble("valor_capacitacion"));
                negocio.setValor_central(rs.getDouble("valor_central"));
                negocio.setValor_seguro(rs.getDouble("valor_seguro"));
                negocio.setPorcentaje_cat(rs.getDouble("porcentaje_cat"));
                negocio.setFecha_liquidacion(rs.getString("fecha_liquidacion"));
                negocio.setDist(rs.getString("dist"));
                negocio.setSector(rs.getString("cod_sector"));
                negocio.setSubsector(rs.getString("cod_subsector"));
                negocio.setTipo_cuota(rs.getString("tipo_cuota"));
                negocio.setTipoProceso(rs.getString("tipo_proceso"));
                negocio.setFinanciaAval(rs.getBoolean("financia_aval"));
                negocio.setNegocio_rel(rs.getString("negocio_rel"));
                negocio.setFecha_factura_aval(rs.getString("fecha_factura_aval"));
                negocio.setNum_pagare(rs.getString("num_pagare"));
                negocio.setValor_fianza(Double.parseDouble(rs.getString("valor_fianza")));
                negocio.setValor_total_poliza(Double.parseDouble(rs.getString("valor_total_poliza")));
                negocio.setPolitica(rs.getString("politica"));
                negocio.setValor_renovacion(rs.getDouble("valor_renovacion"));
                negocio.setPorc_dto_aval(rs.getDouble("porc_dto_fianza"));
                negocio.setPorc_fin_aval(rs.getDouble("porc_fin_fianza"));
                negocio.setValor_refinanciacion(rs.getDouble("valor_refinanciacion"));
                negocio.setNro_docs_ref(rs.getInt("nro_docs_ref"));
                negocio.setSys_date(rs.getString("sys_date"));
                negocio.setTipo_liq("NORMAL");
                negocio.setCuota_aval_mensual(rs.getDouble("cuota_aval_mensual"));
                negocio.setAval_por_cuota(rs.getBoolean("aval_por_cuota"));
             }
        } catch (NumberFormatException | SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en buscarNegocio[NegociosDAO] "+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return negocio;
    }

    /**
     * Busca los detalles de las cuotas de un negocio
     * @param codNegocio codigo del negocio a buscar
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList buscarDetallesNegocio(String codNegocio) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CONSULTA_INTERESES_NEGS";
        ArrayList lista = new ArrayList();
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codNegocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                DocumentosNegAceptado doc = new DocumentosNegAceptado();
                doc.setCod_neg(rs.getString("cod_neg"));
                doc.setItem(rs.getString("item"));
                doc.setFecha(rs.getString("fecha"));
                doc.setDias(rs.getString("dias"));
                doc.setSaldo_inicial(rs.getDouble("saldo_inicial"));
                doc.setCapital(rs.getDouble("capital"));
                doc.setInteres(rs.getDouble("interes"));
                doc.setValor(rs.getDouble("valor"));
                doc.setSaldo_final(rs.getDouble("saldo_final"));
                doc.setNo_aval((rs.getDouble("no_aval")));
                doc.setCapacitacion(rs.getDouble("capacitacion"));
                doc.setSeguro(rs.getDouble("seguro"));
                doc.setCat(rs.getDouble("cat"));
                doc.setRemesa(rs.getDouble("remesa"));
                doc.setCustodia(rs.getDouble("custodia"));
                doc.setCuota_manejo(rs.getDouble("cuota_manejo"));
                doc.setCapital_Poliza(rs.getDouble("capital_poliza"));
                doc.setValor_aval(rs.getDouble("valor_aval"));
                doc.setCapital_aval(rs.getDouble("capital_aval"));
                doc.setInteres_aval(rs.getDouble("interes_aval"));
                doc.setAval(rs.getDouble("aval"));
                lista.add(doc);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarDetallesNegocio[NegociosDAO] "+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return lista;
    }

    public String[] buscarCuentasCXC(String id_convenio, String dstrct, String titulo_valor) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_DATOS_CXC";
        String[] datos = new String[3];
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, dstrct);
            ps.setInt(2, Integer.parseInt(id_convenio));
            ps.setString(3, titulo_valor);
            rs = ps.executeQuery();
            if (rs.next()) {
                datos[0] = rs.getString("prefijo_factura");
                datos[1] = rs.getString("cuenta_cxc");
                datos[2] = rs.getString("hc_cxc");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarCuentasCXC[NegociosDAO] "+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return datos;
    }

    public double calcularGMF(String  valor, float porcentaje){

        double valorGmf = Double.parseDouble(valor)*(porcentaje/100);
        return valorGmf;
    }

     public ArrayList <Aval>searchAvales(String numAval) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_AVALES";
        ArrayList <Aval>listAvales=new ArrayList<Aval>();
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                st.setString(1, numAval);
                rs = st.executeQuery();

                while (rs.next()) {
                    Aval aval = new Aval();
                    aval.Load(rs);
                    listAvales.add(aval);
                }

            }}catch(Exception e){
            System.out.println("error en searchAvales..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }

        return listAvales;

    }

    /**
     * obtiene el aprobador predeterminado para las cxp de negocios
     * @throws Exception
     */
   public String  aprobadorCxPNeg() throws Exception {
        PreparedStatement  st      = null;
        Connection         con     = null;
        String sql = "", aprobador="";
        String query = "SQL_APROBADOR_CXP";
        ResultSet          rs      = null;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                rs = st.executeQuery();

                if (rs.next()) {
                    aprobador=rs.getString("table_code");
                }

            }
        }catch(Exception e){
            throw new Exception("Error al obtener el aprobador de la cxp \n"+e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null) {
                this.desconectar(con);
            }
        }
        return aprobador;
    }

    public String isCXCFiducia(String id_convenio, String dstrct, String prefijo, String factura) throws Exception {
        boolean sw = false;
        String nit_fiducia = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "IS_CXC_FIDUCIA_V2";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, dstrct);
            ps.setInt(2, Integer.parseInt(id_convenio));
            ps.setString(3, prefijo + "%");
            ps.setString(4, factura);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw = true;
                nit_fiducia = rs.getString("nit_fiducia");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en isCXCFiducia[NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return sw + ";" + nit_fiducia;
    }

    public String isCXCFiducia(String id_convenio, String dstrct, String prefijo) throws Exception {
        boolean sw = false;
        String nit_fiducia = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "IS_CXC_FIDUCIA";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, dstrct);
            ps.setInt(2, Integer.parseInt(id_convenio));
            ps.setString(3, prefijo + "%");
            rs = ps.executeQuery();
            if (rs.next()) {
                sw = true;
                nit_fiducia = rs.getString("nit_fiducia");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en isCXCFiducia[NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return sw + ";" + nit_fiducia;
    }

    /* Metodo: updateSaldoFactura, permite actualizar el saldo de la factura
     * @autor : Ing. Iris Vargas
     * @param : valor del abono de la factura, valor del abono moneda extranjera de la factura, factura, distrito, el tipo de documento, nit endoso
     * @version : 1.0
     */
    public String updateSaldoFacturaEndoso (double abono, double abono_me, String factura, String distrito, String tipo_documento, String nit_endoso) throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_FACTURA_ENDOSO";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setDouble (1,  abono);
            st.setDouble (2,  abono);
            st.setDouble (3,  abono_me);
            st.setDouble (4,  abono_me);
            st.setString (5,  nit_endoso);
            st.setString (6,  factura);
            st.setString (7,  distrito);
            sql = st.getSql();//JJCastro fase2
        }catch(Exception ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LOS SALDOS DE LAS FACTURAS, "+ex.getMessage ());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                }
        return sql;
    }

    /* Metodo: updateFacturaDevuelta, permite marcar una factura como devuelta
     * @autor : Ing. Iris Vargas
     * @param :  factura, distrito, usuario
     * @version : 1.0
     */
    public String updateFacturaDevuelta ( String factura, String distrito, String usuario) throws SQLException {
        StringStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_FACTURA_DEVUELTA";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString (1,  usuario);
            st.setString (2,  factura);
            st.setString (3,  distrito);
            sql = st.getSql();//JJCastro fase2
        }catch(Exception ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LA FACTURA DEVUELTA, "+ex.getMessage ());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                }
        return sql;
    }

    
    public void avalarNegocio(String cod, String aval, String usuario) throws SQLException, Exception {
        Vector batch = new Vector();
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        try {

            batch.add(this.avalarNeg(cod, aval, usuario));
            batch.add(this.avalarForm(cod, aval, usuario));
            apdao.ejecutarSQL(batch);

        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE AVALAR NEGOCIO" + e.getMessage());

        }
    }

    /*
     * 
     * Ing. J Pinedo
     * Conexiones JNDI

     */

       public Vector VerInfoPago(String nit, String  negs) throws Exception{
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;

        ResultSet rs = null;
        ResultSet rs2 = null;
        Datos = new Vector();
            String remp="";
            String tab="";
        Connection con = null;
            int sw=0;
            try{
            String agregar = "";
            String sql = this.obtenerSQL("SQL_MOSTRAR_NEGOCIOS_SOPORTE_PAGO");
            agregar = sql.replaceAll("#parametro#", negs);
            con = this.conectarJNDI("SQL_MOSTRAR_NEGOCIOS_SOPORTE_PAGO");
                ps = con.prepareStatement( agregar );
            ps.setString(1, nit);
            rs = ps.executeQuery();

                while (rs.next())
                {
                Neg = new Negocios();
                Neg.setCod_negocio(rs.getString("cod_neg"));
                Neg.setCodigou(rs.getString("codigo"));
                Neg.setCod_cli(rs.getString("nit"));
                Neg.setNom_cli(rs.getString("cliente"));
                Neg.setVr_desem(Double.valueOf(rs.getString("vlr_desembolso")).doubleValue());
                Neg.setFechatran(rs.getString("fecha_desembolso"));
                Datos.add(Neg);
            }
            }
            catch (Exception ex)
            {   System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (rs2  != null){ try{ rs2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (ps2  != null){ try{ ps2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO2 " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }

        return Datos;

    }


 public  Vector VerNits(String negs) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
            Vector nits= new Vector();
        Connection con = null;
            int sw=0;

            try{
            String sql = this.obtenerSQL("SQL_OBTENER_AFILIADOS_SOPORTE_PAGO");
            con = this.conectarJNDI("SQL_OBTENER_AFILIADOS_SOPORTE_PAGO");
                sql=sql.replace("#", negs);
                ps = con.prepareStatement( sql);
            /// ps.setString(1, negs);
            rs = ps.executeQuery();

                while (rs.next())
                {
                     BeanGeneral obj=new BeanGeneral();
                obj.setValor_01(rs.getString("nit_tercero"));
                obj.setValor_02(rs.getString("educativo"));
                nits.add(obj);
                    obj=null;
            }
            }
            catch (Exception ex)
            {   System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}

            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}

            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return nits;
    }

      public boolean  tieneCreditoFintra(String cedula) throws Exception {
       boolean sw=false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "TIENE_CREDITO_FINTRA";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cedula);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en tieneCreditoFintra [NegociosDAO] "+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return sw;
    }

      
      
    public String avalarNeg(String cod, String aval, String usuario) throws SQLException, Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("AVALAR_NEGOCIO"), true);
            st.setString(1, aval);
            st.setString(2, usuario);
            st.setString(3, cod);
            sql = st.getSql();

        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE AVALAR NEGOCIO" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE AVALAR NEGOCIO" + e.getMessage());
                }
            }
        }
        return sql;
    }

    public String avalarForm(String cod, String aval, String usuario) throws SQLException, Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("AVALAR_FORM"), true);
            st.setString(1, aval);
            st.setString(2, usuario);
            st.setString(3, usuario);
            st.setString(4, cod);
            sql = st.getSql();

        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE AVALAR FORMULARIO" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE AVALAR NEGOCIO" + e.getMessage());
                }
            }
        }
        return sql;
    }


    /**
     * Sql para buscar listado de negocios segun codiciones especificadaaas
     * @param tab from del query
     * @param remp where del query
     * @throws Exception cuando hay error
     */
       public void ListadoDatos(String tab, String remp) throws Exception{
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;

        ResultSet rs = null;
        ResultSet rs2 = null;
        Datos = new Vector();
        Connection con = null;

            try{
            String agregar = "";
            String sql = this.obtenerSQL("SQL_MOSTRAR_NEGOCIOS");
            agregar = sql.replaceAll("#TAB#", tab).replaceAll("#COND#", remp);
            con = this.conectarJNDI("SQL_MOSTRAR_NEGOCIOS");
                ps = con.prepareStatement( agregar );
                System.out.println(ps.toString());
            rs = ps.executeQuery();

                while (rs.next())
                {
                Neg = new Negocios();
                Neg.setNom_cli(rs.getString(1));
                Neg.setCod_cli(rs.getString("cod_cli"));
                Neg.setNom_cod(rs.getString("nomcod"));
                Neg.setCod_cod(rs.getString("id_codeudor"));
                Neg.setCod_negocio(rs.getString("cod_neg"));
                Neg.setCod_tabla(rs.getString("cod_tabla"));
                Neg.setFecha_neg(rs.getString("fecha_negocio"));
                Neg.setFpago(rs.getString("fpago"));
                Neg.setTneg(rs.getString("tneg"));
                Neg.setMod_aval(rs.getString("mod_aval"));
                Neg.setMod_cust(rs.getString("mod_custodia"));
                Neg.setMod_rem(rs.getString("mod_remesa"));
                Neg.setNodocs(rs.getInt("nro_docs"));
                Neg.setVr_negocio_solicitado(Double.parseDouble(rs.getString("vr_negocio_solicitado")));
                Neg.setVr_negocio(Double.valueOf(rs.getString("vr_negocio")).doubleValue());
                Neg.setVr_desem(Double.valueOf(rs.getString("vr_desembolso")).doubleValue());
                Neg.setVr_aval(rs.getFloat("vr_aval"));
                Neg.setVr_cust(rs.getFloat("vr_custodia"));
                Neg.setPor_rem(rs.getFloat("porc_remesa"));
                Neg.setEstado(rs.getString("estado_neg"));
                Neg.setNitp(rs.getString("niter"));
                Neg.setEsta(rs.getString("esta"));
                Neg.setObs(rs.getString("obs"));
                Neg.setBcod(rs.getString("nit_tercero"));
                Neg.setFechatran(rs.getString("f_desem"));
                Neg.setCmc(rs.getString("cmc"));
                Neg.setConlet(rs.getString("cletras"));
                Neg.setTotpagado(Double.parseDouble(rs.getString("tot_pagado")));
                Neg.setNumaval(rs.getString("num_aval"));
                Neg.setPagare(rs.getString("cpagare"));
                Neg.setCodigoBanco(rs.getString("banco_cheque"));///SE AGREGO ESTA LINEA
                Neg.setCnd_aval(rs.getString("cnd_aval"));
                Neg.setCreation_user(rs.getString("create_user"));
                Neg.setPrograma(rs.getString("programa"));
                Neg.setCodigou(rs.getString("codigou"));

                Neg.setFormulario(rs.getString("form"));
                Neg.setFecha_ap(rs.getString("fecha_ap"));
                Neg.setCiclo(rs.getString("ciclo"));
                Neg.setNo_solicitud(rs.getString("no_solicitud"));
                Neg.setNom_convenio(rs.getString("convenio"));
                Neg.setSector(rs.getString("sector"));
                Neg.setSubsector(rs.getString("subsector"));
                Neg.setId_convenio(rs.getInt("id_convenio"));
                Neg.setTipoConv(rs.getString("tipo_conv"));
                Neg.setFecha_liquidacion(rs.getString("fecha_liquidacion"));
                Neg.setAnalista(rs.getString("analista"));
                Neg.setValor_renovacion(rs.getDouble("valor_renovacion"));
                Neg.setAnalista_asignado(rs.getString("analista_asignado"));
                Neg.setTipo_credito(rs.getString("tipo_credito"));
                Neg.setAgencia(rs.getString("agencia"));
                Neg.setPreaprobado_ex_cliente(rs.getString("pre_aprobado_ex_cliente"));
                
                String sql2 = this.obtenerSQL("SQL_TIENE_AJUSTE");
                    ps2=con.prepareStatement(sql2);
                    ps2.setString(1,Neg.getCreation_user());
                    rs2=ps2.executeQuery();
                    if(rs2.next())
                    {   Neg.setAjuste(true);
                }
                    else
                    {   Neg.setAjuste(false);
                    }
                Datos.add(Neg);
                Neg = null;
                     if (rs2  != null){ try{ rs2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                     if (ps2  != null){ try{ ps2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}                  
                    }
                }
            catch (Exception ex)
            {   System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
            }finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (rs2  != null){ try{ rs2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (ps2  != null){ try{ ps2.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO2 " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }

    }

       public boolean PermisosFenalco(String Usuario,String tabla)throws Exception{

           boolean sw=false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "PERMISOS_FENALCO";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, Usuario);
               ps.setString(2,tabla );
            rs = ps.executeQuery();
            if (rs.next()) {
                   sw =rs.getString("dato").equals("S")? true:false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en PermisosFenalco [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }


        return sw;

    }

        public String  PermisosRAD(String Usuario,String tabla)throws Exception{

           String sw="0";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "PERMISOS_FENALCO";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, Usuario);
               ps.setString(2,tabla );
            rs = ps.executeQuery();
            if (rs.next()) {

                   sw =rs.getString("dato");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en PermisosFenalco [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }


        return sw;

    }

       
    public ArrayList<DocumentosNegAceptado> getLiquidacion() {
        return liquidacion;
    }

    public void setLiquidacion(ArrayList<DocumentosNegAceptado> liquidacion) {
        this.liquidacion = liquidacion;
    }

    public void insertNegocioMicrocredito(Negocios neg, String numero_form,  NegocioTrazabilidad negtraza) throws SQLException {
        ResultSet rs = null;
        StringStatement sst = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();

        try {
            batch.add(this.insertNegocio(neg));

            for (int i = 0; i < this.getLiquidacion().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacion().get(i);
                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_MICROCREDITO"), true);

                sst.setString(1, neg.getCod_negocio());
                sst.setString(2, liqui.getItem());
                sst.setString(3, liqui.getFecha());
                sst.setString(4, liqui.getDias());
                sst.setDouble(5, liqui.getSaldo_inicial());
                sst.setDouble(6, liqui.getCapital());
                sst.setDouble(7, liqui.getInteres());
                sst.setDouble(8, liqui.getValor());
                sst.setDouble(9, liqui.getSaldo_final());
                sst.setDouble(10, 0);
                sst.setDouble(11, liqui.getCapacitacion());
                sst.setDouble(12, liqui.getCat());
                sst.setDouble(13, liqui.getSeguro());
                sst.setDouble(14,liqui.getCuota_manejo());
                sst.setDouble(15,liqui.getCapital_Poliza());
                sst.setDouble(16,liqui.getValor_aval());
                sst.setDouble(17,liqui.getCapital_aval());
                sst.setDouble(18,liqui.getInteres_aval());
                sst.setDouble(19,liqui.getAval());

                batch.add(sst.getSql());
                sst = null;
            }
            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_DEL_DOCS_FORM"), true);
            sst.setString(1, numero_form);
            batch.add(sst.getSql());

            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
            batch.add(daotraza.insertNegocioTrazabilidad(negtraza));

            for (int i = 0; i < this.getLiquidacion().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacion().get(i);

                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_FORM"), true);

                sst.setString(1, numero_form);
                sst.setString(2, liqui.getItem());
                sst.setDouble(3, liqui.getValor());
                sst.setString(4, liqui.getFecha());
                sst.setInt(5, 1);
                sst.setString(6, neg.getCreation_user());
                batch.add(sst.getSql());
                sst = null;
            }
            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_UPD_FORM"), true);
            sst.setString(1, neg.getTneg());
            sst.setString(2, neg.getCreation_user());
            sst.setString(3, neg.getCod_negocio());
            sst.setString(4, neg.getEstado());
            sst.setInt(5, neg.getId_convenio());
            sst.setString(6, numero_form);
            String sql2 = "";
            if (neg.getCodigoBanco().equals("0") || neg.getCodigoBanco().equals("") || neg.getCodigoBanco().equals("170")) {
                sql2 = sst.getSql().replaceAll("_BANCO_", "null");
            } else {
                sql2 = sst.getSql().replaceAll("_BANCO_", "'" + neg.getCodigoBanco() + "'");
            }
            batch.add(sql2);

            neg = null;
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE LAS INSERCION DE NEGOCIOS " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (sst != null) {
                try {
                    sst = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        try {
            apdao.ejecutarSQL(batch);
        } catch (SQLException e) {
            throw new SQLException("ERROR insertNegocioMicrocredito (NegociosDAO.java)" + e.getMessage());
        }


    }

    /**
     * Actualiza el negocio cuando ya ha ocurrido una liquidacion de microcredito.
     * @autor Iris Vargas
     * @throws SQLException
     */
     public void updateNegocioMicrocredito(Negocios neg,String numero_form,  NegocioTrazabilidad negtraza) throws SQLException {
        ResultSet rs = null;
        String nombre_sql = "SQL_UPDATE_NEGOCIO_MICROCREDITO";
        StringStatement sst = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();


        try {

            sst = new StringStatement(this.obtenerSQL(nombre_sql), true);
            sst.setString(1, neg.getCod_cli());
            sst.setString(2, Double.toString(neg.getVr_negocio()));
            sst.setString(3, Integer.toString(neg.getNodocs()));
            sst.setDouble(4, neg.getVr_desem());
            sst.setString(5, neg.getCreation_user());
            sst.setString(6, neg.getFpago());
            sst.setString(7, neg.getTneg());
            sst.setString(8, neg.getDist());
            sst.setString(9, neg.getFecha_neg());
            sst.setString(10, Double.toString(neg.getTotpagado()));
            sst.setString(11, neg.getTasa());
            sst.setInt(12, neg.getId_convenio());
            sst.setString(13, neg.getEstado());
            sst.setDouble(14, neg.getPorcentaje_cat());
            sst.setDouble(15, neg.getValor_capacitacion());
            sst.setDouble(16, neg.getValor_central());
            sst.setDouble(17, neg.getValor_seguro());
            sst.setString(18, neg.getCod_cli());
            sst.setString(19, neg.getFecha_liquidacion());
            sst.setString(20, neg.getTipo_cuota());
            sst.setDouble(21, neg.getValor_fianza());
            sst.setDouble(22, neg.getValor_total_poliza());
            sst.setDouble(23, neg.getPorc_fin_aval());
            sst.setDouble(24, neg.getPorc_dto_aval());
            sst.setString(25, neg.getCod_negocio());
            

            String s=sst.getSql();
            batch.add(s);
            if(!negtraza.getActividad().equals("")){
                NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
                batch.add(daotraza.insertNegocioTrazabilidad(negtraza));
                 if(neg.getEstado().equals("V")){
                     batch.add(daotraza.updateActividad(neg.getCod_negocio(),"DEC"));
                }

                if(neg.getActividad().equals("ANA")){
                    batch.add(daotraza.updateActividad(neg.getCod_negocio(),"ANA"));
                }
            }

            sst = new StringStatement(this.obtenerSQL("SQL_DELETE_DOCS"), true);
            sst.setString(1, neg.getCod_negocio());
            batch.add(sst.getSql());
            sst = null;

            for (int i = 0; i < this.getLiquidacion().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacion().get(i);
                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_MICROCREDITO"), true);

                sst.setString(1, neg.getCod_negocio());
                sst.setString(2, liqui.getItem());
                sst.setString(3, liqui.getFecha());
                sst.setString(4, liqui.getDias());
                sst.setDouble(5, liqui.getSaldo_inicial());
                sst.setDouble(6, liqui.getCapital());
                sst.setDouble(7, liqui.getInteres());
                sst.setDouble(8, liqui.getValor());
                sst.setDouble(9, liqui.getSaldo_final());
                sst.setDouble(10, 0);
                sst.setDouble(11, liqui.getCapacitacion());
                sst.setDouble(12, liqui.getCat());
                sst.setDouble(13, liqui.getSeguro());
                sst.setDouble(14,liqui.getCuota_manejo());
                sst.setDouble(15,liqui.getCapital_Poliza());
                sst.setDouble(16,liqui.getValor_aval());
                sst.setDouble(17,liqui.getCapital_aval());
                sst.setDouble(18,liqui.getInteres_aval());
                sst.setDouble(19,liqui.getAval());

                batch.add(sst.getSql());
                sst = null;
            }
            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_ANUL_DOCS_FORM"), true);
            sst.setString(1, numero_form);
            batch.add(sst.getSql());
            int max = this.maxDocForm(Integer.parseInt(numero_form));
            for (int i = 0; i < this.getLiquidacion().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacion().get(i);
                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_FORM"), true);
                sst.setString(1, numero_form);
                sst.setString(2, liqui.getItem());
                sst.setDouble(3, liqui.getValor());
                sst.setString(4, liqui.getFecha());
                sst.setInt(5, (max+1));
                sst.setString(6, neg.getCreation_user());
                batch.add(sst.getSql());
                sst = null;
            }
            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_UPD_FORM"), true);
            sst.setString(1, neg.getTneg());
            sst.setString(2, neg.getCreation_user());
            sst.setString(3, neg.getCod_negocio());
            sst.setString(4, neg.getEstado());
            sst.setInt(5, neg.getId_convenio());
            sst.setString(6, numero_form);
            String sql2 = "";
            if (neg.getCodigoBanco().equals("0") || neg.getCodigoBanco().equals("") || neg.getCodigoBanco().equals("170")) {
                sql2 = sst.getSql().replaceAll("_BANCO_", "null");
            } else {
                sql2 = sst.getSql().replaceAll("_BANCO_", "'" + neg.getCodigoBanco() + "'");
            }
            batch.add(sql2);
            neg = null;
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE NEGOCIO " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (sst != null) {
                try {
                    sst = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        try {

            apdao.ejecutarSQL(batch);
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE ACTUALIZACION DE NEGOCIOS" + e.getMessage());
        }
    }

    /**
     * Datos del cliente del negocio
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public Cliente datosCliente(String cod_neg) throws Exception{
        Cliente cliente = new Cliente();
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "DATA_CLIENTE";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            if(rs.next()){
                cliente.setNomcli(rs.getString("nombre"));
                cliente.setCodcli(rs.getString("identificacion"));
                cliente.setDireccion(rs.getString("direccion"));
                cliente.setTelefono(rs.getString("telefono"));
                cliente.setBarrio(rs.getString("barrio"));
                cliente.setCiudad(rs.getString("nomciu"));
                cliente.setCelular(rs.getString("celular"));
                cliente.setTipo(rs.getString("tipo_id"));

              
                        
                
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en cabeceraTabla en GestionCarteraDAO.java: "+e.toString());
                }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
            }
        return cliente;
    }

    
    public String ingresarCXCMicrocreditoPost(BeanGeneral bg, double valor_factura, String documento) throws SQLException {
        String query = "SQL_INSERTAR_CXC_MC_POST";
        StringStatement st = null;
        String sql = "";
        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo documento
            st.setString(2,documento); //document type para generar el numero de factura
            st.setString(3, bg.getValor_02()); //nit
            st.setString(4, bg.getValor_02()); //nit para obtener el codcli
            st.setString(5, bg.getValor_18()); //concepto
            st.setString(6, bg.getValor_12()); //Fecha factura
            st.setString(7, bg.getValor_04()); //fecha_vencimiento
            st.setString(8, bg.getValor_01()); //descripcion document_type
            st.setString(9, valor_factura + ""); //valor_factura
            st.setInt(10, 1); //valor_tasa
            st.setString(11, "PES"); //moneda
            st.setInt(12, 1); //cantidad_items
            st.setString(13, "CREDITO"); //forma_pago
            st.setString(14, "OP"); //agencia_facturacion
            st.setString(15, "BQ"); //agencia_cobro
            //st.setString(16, "ADMIN"); //creation_user
            st.setString(16, bg.getValor_19());
            st.setString(17, valor_factura + ""); //valor_facturame
            st.setString(18, valor_factura + ""); //valor_saldo
            st.setString(19, valor_factura + ""); //valor_saldome
            st.setString(20, bg.getValor_13()); //negasoc
            st.setString(21, "COL"); //base
            st.setString(22, bg.getValor_07()); //num_doc_fen
            st.setString(23, ""); //tipo_ref1
            st.setString(24, ""); //ref1
            st.setString(25, bg.getValor_11()); //cmc
            st.setString(26, bg.getValor_09()); //dstrct
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXCMicrocreditoPost[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXCMicrocreditoPost(BeanGeneral bg, double valor_factura, String documento, String item) throws SQLException {
        String query = "SQL_INSERTAR_DET_CXC_MC_POST";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo_documento
            st.setString(2, documento); //document type para generar el numero de factura
            st.setString(3, item); //item
            st.setString(4, bg.getValor_02()); //nit
            st.setString(5, bg.getValor_18()); //concepto
            st.setString(6, bg.getValor_03()); //descripcion
            st.setInt(7, 1); //cantidad
            st.setString(8, valor_factura + ""); //valor_unitario
            st.setString(9, valor_factura + ""); //valor_item
            st.setInt(10, 1); //valor_tasa
            st.setString(11, "PES"); //moneda
            st.setString(12, "ADMIN"); //creation_user
            st.setString(13, valor_factura + ""); //valor_unitariome
            st.setString(14, valor_factura + ""); //valor_itemme
            st.setString(15,  bg.getValor_10()); //numero_remesa numero de factura capital
            st.setString(16, "COL"); //base
            st.setString(17, "RD-" + bg.getValor_02()); //auxiliar
            st.setString(18, bg.getValor_08()); //Cuenta (codigo_cuenta_contable)
            st.setString(19, bg.getValor_09()); //Distrito
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXCCausacionIntereses[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    /**
     * M�todo que carga los datos necesarios para la creacion de la cxc de intereses microcredito
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public BeanGeneral loadDatosCxcInteres(ResultSet rs) throws Exception {
        BeanGeneral bean = new BeanGeneral();
        try {
            bean.setValor_01(rs.getString("prefijo_cxc_interes"));
            bean.setValor_02(rs.getString("cod_cli"));
            bean.setValor_03(rs.getString("nomb_cuenta"));
            bean.setValor_04(rs.getString("fecha"));
            bean.setValor_05(rs.getString("interes"));
            bean.setValor_06(rs.getString("interes_causado"));
            bean.setValor_07(rs.getString("item"));
            bean.setValor_08(rs.getString("cuenta_interes"));
            bean.setValor_09(rs.getString("dist"));
            bean.setValor_10(rs.getString("documento"));
            bean.setValor_11(rs.getString("cmc"));
            bean.setValor_12(rs.getString("fecha_doc"));
            bean.setValor_13(rs.getString("cod_neg"));
            bean.setValor_14(rs.getString("fecha_ant"));
            bean.setValor_15(rs.getString("saldo_inicial"));
            bean.setValor_16(rs.getString("tasa"));
            bean.setValor_17(rs.getString("fch_interes_causado"));
            bean.setValor_18(rs.getString("concepto"));

        } catch (Exception e) {
            throw new Exception("loadDatosCxcInteres" + e.getMessage());
        }
        return bean;
    }

    public BeanGeneral loadDatosCuotaManejo(ResultSet rs) throws Exception {
        BeanGeneral bean = new BeanGeneral();
        try {
            bean.setValor_01(rs.getString("prefijo_cxc_cuota_manejo"));
            bean.setValor_02(rs.getString("cod_cli"));
            bean.setValor_03(rs.getString("nomb_cuenta"));
            bean.setValor_04(rs.getString("fecha"));
            bean.setValor_05(rs.getString("cuota_manejo"));
            bean.setValor_06(rs.getString("cuota_manejo_causada"));
            bean.setValor_07(rs.getString("item"));
            bean.setValor_08(rs.getString("cuenta_cuota_manejo"));
            bean.setValor_09(rs.getString("dist"));
            bean.setValor_10(rs.getString("documento"));
            bean.setValor_11(rs.getString("cmc"));
            bean.setValor_12(rs.getString("fecha_doc"));
            bean.setValor_13(rs.getString("cod_neg"));
            bean.setValor_14(rs.getString("fecha_ant"));
            bean.setValor_15(rs.getString("saldo_inicial"));
            bean.setValor_16(rs.getString("tasa"));
            bean.setValor_17(rs.getString("fch_cuota_manejo_causada"));
            bean.setValor_18(rs.getString("concepto"));

        } catch (Exception e) {
            throw new Exception("loadDatosCuotaManejo" + e.getMessage());
        }
        return bean;
    }

    /**
     * M�todo que trae los negocios pendientes  para la creacion de la cxc de intereses microcredito por fecha de vencimiento
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosNegInteresMC() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_NEG_MC_CAUSAR_INTERES";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                rs = st.executeQuery();

                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean = this.loadDatosCxcInteres(rs);
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en datosNegInteresMC..." + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    /**
     * Metodo que trae los negocios pendientes  para la creacion de la cxc de intereses microcredito por fecha de vencimiento
     * @param ciclo
     * @param periodo
     * @return
     * @throws java.lang.Exception
     * @autor.......egonzalez
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosNegInteresMIMC(String ciclo, String periodo) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_NEG_MC_CAUSAR_INTERES_CICLO";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                st.setString(1, ciclo);
                st.setString(2, periodo);

                rs = st.executeQuery();

                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean = this.loadDatosCxcInteres(rs);
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en datosNegInteresMC..." + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    /**
     * M�todo que trae los negocios pendientes  para la creacion de la cxc de intereses microcredito de la primera cuota en fin de mes
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosNegInteresMCFMesPC() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_NEG_MC_CAUSAR_INTERES_FMES_PC";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                rs = st.executeQuery();
                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean = this.loadDatosCxcInteres(rs);
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en datosNegInteresMCFMesPC(..." + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    /**
     * M�todo que trae los negocios pendientes  para la creacion de la cxc de intereses microcredito en fin de mes
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosNegInteresMCFMes() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_NEG_MC_CAUSAR_INTERES_FMES";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                rs = st.executeQuery();
                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean = this.loadDatosCxcInteres(rs);
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en datosNegInteresMC..." + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    public String ingresarCXCMicrocredito(BeanGeneral bg, String documentType,int iterador ,int numCuota, ArrayList<DocumentosNegAceptado> liq) throws SQLException {
        String query = "SQL_INSERTAR_CXC";
        StringStatement st = null;
        String sql = "";
        try {
            double central = Double.parseDouble(bg.getValor_03());
            double valor_factura = this.getLiquidacion().get(iterador - 1).getCapital() + 
                                   this.getLiquidacion().get(iterador - 1).getCapacitacion() + 
                                   this.getLiquidacion().get(iterador - 1).getSeguro() +
                                   this.getLiquidacion().get(iterador-1).getInteres()+
                                   this.getLiquidacion().get(iterador-1).getCat()+
                                   this.getLiquidacion().get(iterador-1).getCuota_manejo()+central+
                                   this.getLiquidacion().get(iterador - 1).getCapital_Poliza()+
                                   this.getLiquidacion().get(iterador - 1).getCapital_aval()+
                                   this.getLiquidacion().get(iterador - 1).getInteres_aval()+
                                   this.getLiquidacion().get(iterador - 1).getAval();

            
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo documento
            st.setString(2, documentType); //document type para generar el numero de factura
            st.setString(3, String.valueOf(numCuota)); //numero de cuota para generar el numero de factura
            st.setString(4, bg.getValor_01()); //nit
            st.setString(5, bg.getValor_01()); //nit para obtener el codcli
            st.setString(6, bg.getValor_05()); //concepto
            st.setString(7, bg.getValor_12()); //Fecha factura
            st.setString(8, liq.get(iterador - 1).getFecha()); //fecha_vencimiento
            st.setString(9, documentType); //descripcion(se guarda el document_type)
            st.setString(10, valor_factura + ""); //valor_factura
            st.setInt(11, 1); //valor_tasa
            st.setString(12, "PES"); //moneda
            st.setInt(13, 1); //cantidad_items
            st.setString(14, "CREDITO"); //forma_pago
            st.setString(15, "OP"); //agencia_facturacion
            st.setString(16, "BQ"); //agencia_cobro
            st.setString(17, bg.getValor_04()); //creation_user
            st.setString(18, valor_factura + ""); //valor_facturame
            st.setString(19, valor_factura + ""); //valor_saldo
            st.setString(20, valor_factura + ""); //valor_saldome
            st.setString(21, bg.getValor_06()); //negasoc
            st.setString(22, bg.getValor_07()); //base
            st.setString(23, liq.get(iterador - 1).getItem()); //num_doc_fen
            st.setString(24, ""); //tipo_ref1
            st.setString(25, ""); //ref1
            st.setString(26, bg.getValor_22()); //cmc
            st.setString(27, bg.getValor_18()); //dstrct
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXCMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXCMicrocredito(BeanGeneral bg, String documentType, int numCuota, int item, String descripcion) throws SQLException {
        String query = "SQL_INSERTAR_DETALLE_CXC_MC";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo_documento
            st.setString(2, documentType); //document type para generar el numero de factura
            st.setString(3, String.valueOf(numCuota)); //numero de cuota para generar el numero de factura
            st.setString(4, item + ""); //item
            st.setString(5, bg.getValor_01()); //nit
            st.setString(6, bg.getValor_05()); //concepto
            st.setString(7, descripcion); //descripcion
            st.setInt(8, 1); //cantidad
            st.setString(9, bg.getValor_03()); //valor_unitario
            st.setString(10, bg.getValor_03()); //valor_item
            st.setInt(11, 1); //valor_tasa
            st.setString(12, "PES"); //moneda
            st.setString(13, bg.getValor_04()); //creation_user
            st.setString(14, bg.getValor_03()); //valor_unitariome
            st.setString(15, bg.getValor_03()); //valor_itemme
            st.setString(16, bg.getValor_06()); //numero_remesa (se envia el negocio)
            st.setString(17, bg.getValor_07()); //base
            st.setString(18, bg.getValor_08()); //auxiliar
            st.setString(19, bg.getValor_21()); //Cuenta (codigo_cuenta_contable)
            st.setString(20, bg.getValor_18()); //Distrito
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXCMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarCXPMicrocredito(BeanGeneral bg, String documento, Convenio convenio) throws SQLException {
        String query = "SQL_INSERTAR_CXP";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            
            double vlr_estudio = valorCentral("VLRCENTRAL", "MICRO");
            double vlr_fianza = 0;
            double porc_fianza = 0;
            if (aplicarDeduccionFianza(bg.getValor_06().trim())) {
//                String nit_proveedor_fianza = this.obtenerProveedorFianza(bg.getValor_17(), Integer.parseInt(bg.getValor_02()));
                vlr_fianza = obtenerValorFianza(bg.getValor_17(), Double.parseDouble(bg.getValor_29()), Integer.parseInt(bg.getValor_02()), bg.getValor_30(),Integer.parseInt(bg.getValor_34()),Integer.parseInt(bg.getValor_35()),bg.getValor_33());
                porc_fianza = obtenerPorcFianza(bg.getValor_17(), bg.getValor_33(), Integer.parseInt(bg.getValor_02()),Integer.parseInt(bg.getValor_34()),Integer.parseInt(bg.getValor_35()));

                vlr_fianza = Math.round(vlr_fianza * (porc_fianza / 100));
                if (vlr_fianza == 0) {
                    throw new SQLException("ERROR en ingresarCXPMicrocredito[NegociosDAO] el valor de la fianza es 0");
                }
            }
            double vd = Double.parseDouble(bg.getValor_29()) - vlr_estudio; //- vlr_fianza; se comenta ya que se capitalizo la fianza


            st.setString(1, bg.getValor_01()); //proveedor
            st.setString(2, "FAP");//tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, bg.getValor_01()); //descripcion (Se envia el nit del afiliado para colocarlo en la descripcion)
            st.setString(5, "OP"); //agencia
            st.setString(6, bg.getValor_01()); //banco (Se envia el nit del afiliado)
            st.setString(7, bg.getValor_01()); //sucursal (Se envia el nit del afiliado)
            st.setDouble(8, vd);//vlr_neto
            st.setDouble(9, vd);//vlr_saldo
            st.setDouble(10, vd);//vlr_neto_me
            st.setDouble(11, vd);//vlr_saldo_me
            st.setInt(12, 1);//tasa
            st.setString(13, bg.getValor_04());//usuario creacion
            st.setString(14, bg.getValor_07());//base
            st.setString(15, "PES");//moneda_banco
            st.setString(16, "NEG");//clase_documento_rel
            st.setString(17, "PES");//moneda
            st.setString(18, "NEG");//tipo_documento_rel
            st.setString(19, bg.getValor_06());//documento_relacionado
            st.setString(20, bg.getValor_23()); //handle_code
            st.setString(21, bg.getValor_18());//Distrito
            st.setString(22, this.aprobadorCxPNeg());//aprobador
            st.setString(23, bg.getValor_04());//usuario aprobacion
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXPMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXPMicrocredito(BeanGeneral bg, String documento, Convenio convenio) throws SQLException {
        String query = "INSERTAR_DETALLE_CXP";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            double vlr_fianza=0;
            if(aplicarDeduccionFianza(bg.getValor_06())){
//               String nit_proveedor_fianza=this.obtenerProveedorFianza(bg.getValor_17(),Integer.parseInt(bg.getValor_02())); 
               vlr_fianza = obtenerValorFianza(bg.getValor_17(),Double.parseDouble(bg.getValor_29()),Integer.parseInt(bg.getValor_02()),bg.getValor_30(),Integer.parseInt(bg.getValor_34()),Integer.parseInt(bg.getValor_35()),bg.getValor_33());
               double porc_fianza =obtenerPorcFianza(bg.getValor_17(),bg.getValor_33(), Integer.parseInt(bg.getValor_02()),Integer.parseInt(bg.getValor_34()),Integer.parseInt(bg.getValor_35()));

               vlr_fianza=vlr_fianza*porc_fianza/100;
               if (vlr_fianza == 0) {
                    throw new SQLException("ERROR en ingresarCXPMicrocredito[NegociosDAO] el valor de la fianza es 0");
               }
               
            }
            double vd = Double.parseDouble(bg.getValor_29());// - vlr_fianza; se comenta ya que se capitalizo la fianza
            st.setString(1, bg.getValor_01()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, "1"); //item
            st.setString(5, "DESEMBOLSO " + bg.getValor_01()); //descripcion
            st.setDouble(6, vd);//vlr
            st.setDouble(7, vd);//vlr_me
            st.setString(8, bg.getValor_24());//codigo_cuenta
            st.setString(9, bg.getValor_06());//planilla
            st.setString(10, bg.getValor_04());//creation_user
            st.setString(11, bg.getValor_07());//base
            st.setString(12, "AR-" + bg.getValor_01());//AUXILIAR
            st.setString(13, bg.getValor_18());//Distrito
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXPMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXPMicrocreditoItem2(BeanGeneral bg, String documento, Convenio convenio) throws SQLException {
        String query = "INSERTAR_DETALLE_CXP";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            double vd = valorCentral("VLRCENTRAL", "MICRO") * (-1);
            st.setString(1, bg.getValor_01()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, "2"); //item
            st.setString(5, "DESCUENTO ESTUDIO DE CREDITO " + bg.getValor_01()); //descripcion
            st.setDouble(6, vd);//vlr
            st.setDouble(7, vd);//vlr_me
            st.setString(8, convenio.getCuenta_central());//codigo_cuenta
            st.setString(9, "");//planilla
            st.setString(10, bg.getValor_04());//creation_user
            st.setString(11, bg.getValor_07());//base
            st.setString(12, "AR-" + bg.getValor_01());//AUXILIAR
            st.setString(13, bg.getValor_18());//Distrito
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXPMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    
    public String ingresarDetalleCXPMicrocreditoItem3(BeanGeneral bg, String documento, double vlr_fianza, String nitEmpresaFianza) throws SQLException {
        String query = "INSERTAR_DETALLE_CXP";
        StringStatement st = null;
        String sql = "";
        try {
            
            st = new StringStatement(this.obtenerSQL(query), true);
            double porc_fianza = obtenerPorcFianza(bg.getValor_17(), bg.getValor_33(), Integer.parseInt(bg.getValor_02()),Integer.parseInt(bg.getValor_34()),Integer.parseInt(bg.getValor_35()));
            vlr_fianza = Math.round(vlr_fianza * (porc_fianza / 100))*-1;
            if (vlr_fianza == 0) {
                throw new SQLException("ERROR en ingresarCXPMicrocredito[NegociosDAO] el valor de la fianza es 0");
            }

            st.setString(1, bg.getValor_01()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, "3"); //item
            st.setString(5, "DESCUENTO AVAL NO FINANCIADO " + bg.getValor_01()); //descripcion
            st.setDouble(6, vlr_fianza);//vlr
            st.setDouble(7, vlr_fianza);//vlr_me
            st.setString(8, bg.getValor_24());//codigo_cuenta
            st.setString(9, "");//planilla
            st.setString(10, bg.getValor_04());//creation_user
            st.setString(11, bg.getValor_07());//base
            st.setString(12, "AR-" + bg.getValor_01());//AUXILIAR
            st.setString(13, bg.getValor_18());//Distrito

            sql = st.getSql();
          
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXPMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    
        public String ingresarDetalleCXPMicrocreditoItem4(BeanGeneral bg, String documento, double vlr_fianza, String nitEmpresaFianza) throws SQLException {
        String query = "INSERTAR_DETALLE_CXP";
        StringStatement st = null;
        String sql = "";
        try {
            
            st = new StringStatement(this.obtenerSQL(query), true);
            double porc_fianza = obtenerPorcFianza(bg.getValor_17(),bg.getValor_33(), Integer.parseInt(bg.getValor_02()), Integer.parseInt(bg.getValor_34()), Integer.parseInt(bg.getValor_35()));
            vlr_fianza = Math.round(vlr_fianza * (porc_fianza / 100));
            if (vlr_fianza == 0) {
                throw new SQLException("ERROR en ingresarCXPMicrocredito[NegociosDAO] el valor de la fianza es 0");
            }

            st.setString(1, bg.getValor_01()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, "4"); //item
            st.setString(5, "DESCUENTO AVAL NO FINANCIADO " + bg.getValor_01()); //descripcion
            st.setDouble(6, vlr_fianza);//vlr
            st.setDouble(7, vlr_fianza);//vlr_me
            st.setString(8, bg.getValor_24());//codigo_cuenta
            st.setString(9, "");//planilla
            st.setString(10, bg.getValor_04());//creation_user
            st.setString(11, bg.getValor_07());//base
            st.setString(12, "AR-" + bg.getValor_01());//AUXILIAR
            st.setString(13, bg.getValor_18());//Distrito

            sql = st.getSql();
          
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXPMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String updateInteresCausado(double valor, String fecha, String negocio, String item) throws SQLException {
        String query = "SQL_UPDATE_INTERES_CAUSADO";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setDouble(1, valor);
            st.setString(2, fecha);
            st.setString(3, negocio);
            st.setString(4, item);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en updateInteresCausado[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String updateDocumentoCat( String documento, String negocio, String item) throws SQLException {
        String query = "SQL_UPDATE_DOCUMENTO_CAT";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, documento);
            st.setString(2, negocio);
            st.setString(3, item);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en updateInteresCausado[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ultimoDiaMes(String fecha) throws Exception {
        String query = "SQL_ULTIMO_DIA_MES";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String dia = "0";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, fecha);

            rs = ps.executeQuery();
            if (rs.next()) {
                dia = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en ultimoDiaMes [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return dia;
    }

    /**
     * M�todo que trae los datos necesarios para la creacion de la cxc de comision cat
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosCxcCat() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_DATOS_CXC_CAT";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                rs = st.executeQuery();
                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean= this.loadDatosCxcCat(rs);
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en datosCxcCat(..." + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    /**
     * M�todo que trae los datos necesarios para la creacion de la cxc de comision cat
     * @param ciclo
     * @param periodo
     * @return
     * @throws java.lang.Exception
     * @autor.......egonzalez
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosCxcCat(String ciclo, String periodo) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_DATOS_CXC_CAT_MICRO";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                st.setString(1,ciclo);
                st.setString(2,periodo);
                rs = st.executeQuery();
                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean= this.loadDatosCxcCat(rs);
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en datosCxcCat(..." + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    /**
     * DEVUELVE  TIPO DE PROCESO DE UN NEGOCIO
     * @param cod_neg Codigo del negocio
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public String getTipoProceso(String cod_neg) throws Exception{
        String tipoProceso="";
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "TIPO_PROCESO";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_neg);
            rs = st.executeQuery();
            if(rs.next()){
                tipoProceso=rs.getString("tipo_proceso");
            }
                }
        catch (Exception e) {
            throw new Exception("Error en cabeceraTabla en GestionCarteraDAO.java: "+e.toString());
            }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
                }
        return tipoProceso;
    }

    
    /**
     * Trae el numero de negocio de un cliente el dia de hoy
     * @autor Iris Vargas
     * @throws SQLException
     */
    public int NumNegocioClienteHoy(String cliente) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int num = 0;
        String query = "NUM_NEG_CLIENTE_HOY";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, cliente);
                rs = st.executeQuery();
                if (rs.next()) {
                    num= rs.getInt("num");
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR NumNegocioClienteHoy " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return num;
    }

    
    public String ingresarDetalleCXPAval(BeanGeneral bg, String documento, Convenio convenio) throws SQLException {
        String query = "INSERTAR_DETALLE_CXP";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            double vd = Double.valueOf(bg.getValor_29()).doubleValue();
            st.setString(1, bg.getValor_01()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, "1"); //item
            st.setString(5, "DESEMBOLSO " + bg.getValor_01()); //descripcion
            st.setDouble(6, vd);//vlr
            st.setDouble(7, vd);//vlr_me
            st.setString(8, bg.getValor_24());//codigo_cuenta
            st.setString(9, bg.getValor_06());//planilla
            st.setString(10, bg.getValor_04());//creation_user
            st.setString(11, bg.getValor_07());//base
            st.setString(12, "AR-" + bg.getValor_01());//AUXILIAR
            st.setString(13, bg.getValor_18());//Distrito
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXP[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }


    /*
     * Guardar Negocio
     * Jpinedo
     */
    public String GuardarNegocio(Negocios neg, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws SQLException {
        ResultSet rs = null;
        String nombre_sql = "";
        StringStatement sst = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();
        String sql = "";

        /* ----------------------------------------------------------------------------------*/
        try {

            if ((neg.getTneg()).equals("02")) {
                nombre_sql = "SQL_INSERTAR_NEGOCIOS";
            } else {
                nombre_sql = "SQL_INSERTAR_NEGOCIOS2";
            }
            sst = new StringStatement(this.obtenerSQL(nombre_sql), true);

            sst.setString(1, neg.getCod_cli());
            sst.setString(2, Double.toString(neg.getVr_negocio()));
            sst.setString(3, Integer.toString(neg.getNodocs()));
            sst.setString(4, Double.toString(neg.getVr_desem()));
            sst.setString(5, Double.toString(neg.getVr_aval()));
            sst.setString(6, Double.toString(neg.getVr_cust()));
            sst.setString(7, neg.getMod_aval());
            sst.setString(8, neg.getMod_cust());
            sst.setString(9, Double.toString(neg.getPor_rem()));
            sst.setString(10, neg.getMod_rem());
            sst.setString(11, neg.getCreation_user());
            sst.setString(12, neg.getFpago());
            sst.setString(13, neg.getTneg());
            sst.setString(14, neg.getCod_tabla());
            sst.setString(15, neg.getEsta());
            sst.setString(16, neg.getFecha_neg());
            sst.setString(17, neg.getNitp());
            sst.setString(18, Double.toString(neg.getTotpagado()));
            if ((neg.getTneg()).equals("02")) {
                sst.setString(19, neg.getNitp());
                sst.setString(20, neg.getNitp());
                sst.setString(21, neg.getCod_negocio());
                sst.setString(22, neg.getTasa());
                sst.setString(23, neg.getvalor_remesa());
                sst.setDouble(24, neg.getVr_aval());
                sst.setInt(25, neg.getId_convenio());
            } else {
                sst.setString(19, neg.getCod_negocio());
                sst.setString(20, neg.getvalor_aval());
                sst.setString(21, neg.getvalor_remesa());
                sst.setString(22, neg.getTasa());
                sst.setString(23, neg.getCnd_aval());
                sst.setInt(24, neg.getId_convenio());
                sst.setString(25, neg.getCodigoBanco());
            }
            sst.setString(26, neg.getId_sector());
            sst.setString(27, neg.getId_subsector());
            sst.setString(28, neg.getEstado());
            sst.setString(29, neg.getTipoProceso());
            sst.setString(30, negtraza.getActividad());
            sst.setString(31, neg.getNegocio_rel());
            sst.setString(32, String.valueOf(neg.isFinanciaAval()));


            if (neg.getId_remesa() == 0) {
                sql = sst.getSql().replace("IDREME", "null");
            } else {
                sql = sst.getSql().replace("IDREME", "" + neg.getId_remesa());
            }
            for (int i = 0; i < this.getLiquidacion().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacion().get(i);
                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOC_ACEPTADOS"), true);
                sst.setString(1, neg.getCod_negocio());
                sst.setString(2, liqui.getItem());
                sst.setString(3, liqui.getFecha());
                sst.setString(4, liqui.getDias());
                sst.setDouble(5, liqui.getSaldo_inicial());
                sst.setDouble(6, liqui.getCapital());
                sst.setDouble(7, liqui.getInteres());
                sst.setDouble(8, liqui.getValor());
                sst.setDouble(9, liqui.getSaldo_final());
                sst.setDouble(10, liqui.getSeguro());
                sst.setDouble(11, liqui.getCustodia());
                sst.setDouble(12, liqui.getRemesa());
                sst.setDouble(13, liqui.getCuota_manejo());

                // batch.add(sst.getSql());
                sql = sql + sst.getSql();
                sst = null;
            }
            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_DEL_DOCS_FORM"), true);
            sst.setString(1, numero_form);
            // batch.add(sst.getSql());
            sql = sql + sst.getSql();

            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
            negtraza.setCodNeg(neg.getCod_negocio());
            sql = sql + daotraza.insertNegocioTrazabilidad(negtraza);
            for (int i = 0; i < this.getLiquidacion().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacion().get(i);
                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_FORM"), true);
                sst.setString(1, numero_form);
                sst.setString(2, liqui.getItem());
		if(!neg.isFinanciaAval() && Integer.parseInt(liqui.getItem())==1 )
                {
                    liqui.setValor(liqui.getValor()+Double.parseDouble(neg.getvalor_aval()));
                }
                sst.setDouble(3, liqui.getValor());
                sst.setString(4, liqui.getFecha());
                sst.setInt(5, 1);
                sst.setString(6, neg.getCreation_user());
                // batch.add(sst.getSql());
                sql = sql + sst.getSql();
                sst = null;
            }
            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_UPD_FORM"), true);
            sst.setString(1, neg.getTneg());
            sst.setString(2, neg.getCreation_user());
            sst.setString(3, neg.getCod_negocio());
            sst.setString(4, neg.getEstado());
            sst.setInt(5, neg.getId_convenio());
            sst.setString(6, numero_form);
            String sql2 = "";
            if (neg.getCodigoBanco().equals("0") || neg.getCodigoBanco().equals("") || neg.getCodigoBanco().equals("170")) {
                sql2 = sst.getSql().replaceAll("_BANCO_", "null");
            } else {
                sql2 = sst.getSql().replaceAll("_BANCO_", "'" + neg.getCodigoBanco() + "'");
            }
            //batch.add(sql2);
            sql = sql + sql2;
            Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.getDatabaseName());
            Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(neg.getDist(), convenio.getImpuesto(), neg.getFecha_neg());
            double porcImpuesto = impuesto.getPorcentaje1() / 100;
            for (int i = 0; i < convenio.getConvenioComision().size(); i++) {

                double vlrComision = neg.getTotpagado() * (convenio.getConvenioComision().get(i).getPorcentaje_comision() / 100);
                double impComision = vlrComision * porcImpuesto;
                sst = null;
                sst = new StringStatement(this.obtenerSQL("SQL_INSERTAR_COMISION"), true);
                sst.setString(1, convenio.getId_convenio());
                sst.setString(2, convenio.getConvenioComision().get(i).getId_comision());
                sst.setString(3, neg.getCod_negocio());
                sst.setDouble(4, convenio.getConvenioComision().get(i).getPorcentaje_comision());
                sst.setDouble(5, vlrComision + impComision);
                sst.setString(6, neg.getCreation_user());
                // batch.add(sst.getSql());
                sql = sql + sst.getSql();
            }
            neg = null;
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE LAS INSERCION DE NEGOCIOS " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (sst != null) {
                try {
                    sst = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        //-----------------------------------------------------------------------
        // batch.add(sql);
       /* try {
         apdao.ejecutarSQL(batch);
         } catch (SQLException e) {
         throw new SQLException("ERROR DURANTE INSERCION de negocio" + e.getMessage());
         }*/


        return sql;


    }



    /*
     * Guardar Negocio
     * Jpinedo
     */
    public String GuardarNegocioAval(Negocios neg, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws SQLException {
        ResultSet rs = null;
        String nombre_sql = "";
        StringStatement sst = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();
        String sql = "";

        /* ----------------------------------------------------------------------------------*/
        try {

            if ((neg.getTneg()).equals("02")) {
                nombre_sql = "SQL_INSERTAR_NEGOCIOS";
            } else {
                nombre_sql = "SQL_INSERTAR_NEGOCIOS2";
            }
            sst = new StringStatement(this.obtenerSQL(nombre_sql), true);

            sst.setString(1, neg.getCod_cli());
            sst.setString(2, Double.toString(neg.getVr_negocio()));
            sst.setString(3, Integer.toString(neg.getNodocs()));
            sst.setString(4, Double.toString(neg.getVr_desem()));
            sst.setString(5, Double.toString(neg.getVr_aval()));
            sst.setString(6, Double.toString(neg.getVr_cust()));
            sst.setString(7, neg.getMod_aval());
            sst.setString(8, neg.getMod_cust());
            sst.setString(9, Double.toString(neg.getPor_rem()));
            sst.setString(10, neg.getMod_rem());
            sst.setString(11, neg.getCreation_user());
            sst.setString(12, neg.getFpago());
            sst.setString(13, neg.getTneg());
            sst.setString(14, neg.getCod_tabla());
            sst.setString(15, neg.getEsta());
            sst.setString(16, neg.getFecha_neg());
            sst.setString(17, neg.getNit_tercero());// sst.setString(17, neg.getNitp());
            sst.setString(18, Double.toString(neg.getTotpagado()));
            if ((neg.getTneg()).equals("02")) {
                sst.setString(19, neg.getNitp());
                sst.setString(20, neg.getNitp());
                sst.setString(21, neg.getCod_negocio());
                sst.setString(22, neg.getTasa());
                sst.setString(23, neg.getvalor_remesa());
                sst.setDouble(24, neg.getVr_aval());
                sst.setInt(25, neg.getId_convenio());
            } else {
                sst.setString(19, neg.getCod_negocio());
                sst.setString(20, neg.getvalor_aval());
                sst.setString(21, neg.getvalor_remesa());
                sst.setString(22, neg.getTasa());
                sst.setString(23, neg.getCnd_aval());
                sst.setInt(24, neg.getId_convenio());
                sst.setString(25, neg.getCodigoBanco());
            }

            sst.setString(26, neg.getId_sector()!=null?neg.getId_sector():neg.getSector());
            // sst.setString(26, neg.getSector());
            sst.setString(27, neg.getId_subsector()!=null?neg.getId_subsector():neg.getSubsector());
            //sst.setString(27, neg.getSubsector());
            sst.setString(28, neg.getEstado());
            sst.setString(29, neg.getTipoProceso());
            sst.setString(30, negtraza.getActividad());
            sst.setString(31, neg.getNegocio_rel());
            sst.setString(32, String.valueOf(neg.isFinanciaAval()));

            if (neg.getId_remesa() == 0) {
                sql = sst.getSql().replace("IDREME", "null");
            } else {
                sql = sst.getSql().replace("IDREME", "" + neg.getId_remesa());
            }

            /* NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO();
             negtraza.setCodNeg(neg.getCod_negocio());
             negtraza.setNumeroSolicitud(numero_form);

             sql = sql + daotraza.insertNegocioTrazabilidad(negtraza);*/
            for (int i = 0; i < this.getLiquidacionAval().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacionAval().get(i);
                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOC_ACEPTADOS"), true);
                sst.setString(1, neg.getCod_negocio());
                sst.setString(2, liqui.getItem());
                sst.setString(3, liqui.getFecha());
                sst.setString(4, liqui.getDias());
                sst.setDouble(5, liqui.getSaldo_inicial());
                sst.setDouble(6, liqui.getCapital());
                sst.setDouble(7, liqui.getInteres());
                sst.setDouble(8, liqui.getValor());
                sst.setDouble(9, liqui.getSaldo_final());
                sst.setDouble(10, liqui.getSeguro());
                sst.setDouble(11, liqui.getCustodia());
                sst.setDouble(12, liqui.getRemesa());
                sst.setDouble(13, liqui.getCuota_manejo());
                sql = sql + sst.getSql();
                sst = null;
            }



            Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.getDatabaseName());
            Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(neg.getDist(), convenio.getImpuesto(), neg.getFecha_neg());
            double porcImpuesto = impuesto.getPorcentaje1() / 100;
            for (int i = 0; i < convenio.getConvenioComision().size(); i++) {

                double vlrComision = neg.getTotpagado() * (convenio.getConvenioComision().get(i).getPorcentaje_comision() / 100);
                double impComision = vlrComision * porcImpuesto;
                sst = null;
                sst = new StringStatement(this.obtenerSQL("SQL_INSERTAR_COMISION"), true);
                sst.setString(1, convenio.getId_convenio());
                sst.setString(2, convenio.getConvenioComision().get(i).getId_comision());
                sst.setString(3, neg.getCod_negocio());
                sst.setDouble(4, convenio.getConvenioComision().get(i).getPorcentaje_comision());
                sst.setDouble(5, vlrComision + impComision);
                sst.setString(6, neg.getCreation_user());
                sql = sql + sst.getSql();
            }
            neg = null;
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE LAS INSERCION DE NEGOCIOS " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (sst != null) {
                try {
                    sst = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return sql;


    }




    public Negocios getNegocio() {
        return negocio;
    }

    public void setNegocio(Negocios negocio) {
        this.negocio = negocio;
    }

    public Negocios getNegocioAval() {
        return negocioAval;
    }

    public void setNegocioAval(Negocios negocioAval) {
        this.negocioAval = negocioAval;
    }




    public String InsertaDocumentosForms(Negocios neg,String numero_form) throws Exception
    {
        String sql="";
        StringStatement sst = null;
        sst = new StringStatement(this.obtenerSQL("SQL_DEL_DOCS_FORM"), true);
        sst.setString(1, numero_form);
        sql = sql + sst.getSql();
        for (int i = 0; i < this.getLiquidacionAval().size(); i++) {
            DocumentosNegAceptado liqui = new DocumentosNegAceptado();
            liqui = this.getLiquidacionAval().get(i);
            sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_FORM"), true);
            sst.setString(1, numero_form);
            sst.setString(2, liqui.getItem());
            sst.setDouble(3, liqui.getValor());
            sst.setString(4, liqui.getFecha());
            sst.setInt(5, 1);
            sst.setString(6, neg.getCreation_user());
            sql = sql + sst.getSql();
            sst = null;
        }
        return sql;
    }

    public String ingresarCXCNegocio(BeanGeneral bg, String documentType,int iterador, int numCuota, ArrayList<DocumentosNegAceptado> liq, boolean intermediario_aval) throws SQLException {
        String query = "SQL_INSERTAR_CXC";
        StringStatement st = null;
        String sql = "";
        try {
            double valor_factura;
                valor_factura = Math.round(this.getLiquidacion().get(iterador - 1).getCapital() + this.getLiquidacion().get(iterador - 1).getCustodia() + 
                                           this.getLiquidacion().get(iterador - 1).getSeguro()+this.getLiquidacion().get(iterador - 1).getRemesa()+
                                           this.getLiquidacion().get(iterador - 1).getCuota_manejo()+this.getLiquidacion().get(iterador - 1).getAval());
           if(!intermediario_aval)
           {
               valor_factura=Math.round(valor_factura+this.getLiquidacion().get(iterador - 1).getInteres());
            }

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo documento
            st.setString(2, documentType); //document type para generar el numero de factura
            st.setString(3, String.valueOf(numCuota)); //numero de cuota para generar el numero de factura
            st.setString(4, bg.getValor_01()); //nit
            st.setString(5, bg.getValor_01()); //nit para obtener el codcli
            st.setString(6, bg.getValor_05()); //concepto
            st.setString(7, bg.getValor_12()); //Fecha factura
            st.setString(8, liq.get(iterador - 1).getFecha()); //fecha_vencimiento
            st.setString(9, documentType); //descripcion(se guarda el document_type)
            st.setString(10, valor_factura + ""); //valor_factura
            st.setInt(11, 1); //valor_tasa
            st.setString(12, "PES"); //moneda
            st.setInt(13, 1); //cantidad_items
            st.setString(14, "CREDITO"); //forma_pago
            st.setString(15, "OP"); //agencia_facturacion
            st.setString(16, "BQ"); //agencia_cobro
            st.setString(17, bg.getValor_04()); //creation_user
            st.setString(18, valor_factura + ""); //valor_facturame
            st.setString(19, valor_factura + ""); //valor_saldo
            st.setString(20, valor_factura + ""); //valor_saldome
            st.setString(21, bg.getValor_06()); //negasoc
            st.setString(22, bg.getValor_07()); //base
            st.setString(23, liq.get(iterador - 1).getItem()); //num_doc
            st.setString(24, ""); //tipo_ref1
            st.setString(25, ""); //ref1
            st.setString(26, bg.getValor_22()); //cmc
            st.setString(27, bg.getValor_18()); //dstrct
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXCNegocio[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }


    public String ingresarDetalleCXCNegocio(BeanGeneral bg, String documentType, int numCuota, int item, String descripcion) throws SQLException {
        String query = "SQL_INSERTAR_DETALLE_CXC_MC";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo_documento
            st.setString(2, documentType); //document type para generar el numero de factura
            st.setString(3, String.valueOf(numCuota)); //numero de cuota para generar el numero de factura
            st.setString(4, item + ""); //item
            st.setString(5, bg.getValor_01()); //nit
            st.setString(6, bg.getValor_05()); //concepto
            st.setString(7, descripcion); //descripcion
            st.setInt(8, 1); //cantidad
            st.setString(9, bg.getValor_03()); //valor_unitario
            st.setString(10, bg.getValor_03()); //valor_item
            st.setInt(11, 1); //valor_tasa
            st.setString(12, "PES"); //moneda
            st.setString(13, bg.getValor_04()); //creation_user
            st.setString(14, bg.getValor_03()); //valor_unitariome
            st.setString(15, bg.getValor_03()); //valor_itemme
            st.setString(16, bg.getValor_06()); //numero_remesa (se envia el negocio)
            st.setString(17, bg.getValor_07()); //base
            st.setString(18, bg.getValor_08()); //auxiliar
            st.setString(19, bg.getValor_21()); //Cuenta (codigo_cuenta_contable)
            st.setString(20, bg.getValor_18()); //Distrito
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXCMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }


    public String ingresarCXPAval(BeanGeneral bg, String documento, Convenio convenio) throws SQLException {
        String query = "SQL_INSERTAR_CXP_AVAL";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            double vd = Double.valueOf(bg.getValor_29()).doubleValue();
            st.setString(1, bg.getValor_01()); //proveedor
            st.setString(2, "FAP");//tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, bg.getValor_01()); //descripcion (Se envia el nit del afiliado para colocarlo en la descripcion)
            st.setString(5, "OP"); //agencia
            st.setString(6, bg.getValor_01()); //banco (Se envia el nit del afiliado)
            st.setString(7, bg.getValor_01()); //sucursal (Se envia el nit del afiliado)
            st.setDouble(8, vd);//vlr_neto
            st.setDouble(9, vd);//vlr_saldo
            st.setDouble(10, vd);//vlr_neto_me
            st.setDouble(11, vd);//vlr_saldo_me
            st.setInt(12, 1);//tasa
            st.setString(13, bg.getValor_04());//usuario creacion
            st.setString(14, bg.getValor_07());//base
            st.setString(15, "PES");//moneda_banco
            st.setString(16, bg.getValor_30());//Fecha de vencimiento.
            st.setString(17, "NEG");//clase_documento_rel
            st.setString(18, "PES");//moneda
            st.setString(19, "NEG");//tipo_documento_rel
            st.setString(20, bg.getValor_06());//documento_relacionado
            st.setString(21, bg.getValor_23()); //handle_code
            st.setString(22, bg.getValor_18());//Distrito
            st.setString(23, this.aprobadorCxPNeg());//aprobador
            st.setString(24, bg.getValor_04());//usuario aprobacion
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXPMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }





    public String ingresarCXCAvalNegocio(BeanGeneral bg, String documentType, int numCuota, ArrayList<DocumentosNegAceptado> liq) throws SQLException {
        String query = "SQL_INSERTAR_CXC";
        StringStatement st = null;
        String sql = "";
        try {



            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo documento
            st.setString(2, documentType); //document type para generar el numero de factura
            st.setString(3, String.valueOf(numCuota)); //numero de cuota para generar el numero de factura
            st.setString(4, bg.getValor_01()); //nit
            st.setString(5, bg.getValor_01()); //nit para obtener el codcli
            st.setString(6, bg.getValor_05()); //concepto
            st.setString(7, bg.getValor_12()); //Fecha factura
            st.setString(8, liq.get(0).getFecha()); //fecha_vencimiento
            st.setString(9, "CXC AVAL"); //descripcion(se guarda el document_type)
            st.setString(10, bg.getValor_03() + ""); //valor_factura
            st.setInt(11, 1); //valor_tasa
            st.setString(12, "PES"); //moneda
            st.setInt(13, 1); //cantidad_items
            st.setString(14, "CREDITO"); //forma_pago
            st.setString(15, "OP"); //agencia_facturacion
            st.setString(16, "BQ"); //agencia_cobro
            st.setString(17, bg.getValor_04()); //creation_user
            st.setString(18,  bg.getValor_03() + ""); //valor_facturame
            st.setString(19, bg.getValor_03() + ""); //valor_saldo
            st.setString(20,  bg.getValor_03()  + ""); //valor_saldome
            st.setString(21, bg.getValor_06()); //negasoc
            st.setString(22, bg.getValor_07()); //base
            st.setString(23, liq.get(0).getItem()); //num_doc
            st.setString(24, ""); //tipo_ref1
            st.setString(25, ""); //ref1
            st.setString(26, bg.getValor_22()); //cmc
            st.setString(27, bg.getValor_18()); //dstrct
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXCNegocio[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public Negocios buscarNegocioAval(String codNegocio) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_NEGOCIO_AVAL";
        Negocios negocio = null;
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codNegocio);
            rs = ps.executeQuery();
            while (rs.next()) {
                negocio = new Negocios();
                negocio.setCod_negocio(rs.getString("cod_neg"));
                negocio.setCod_cli(rs.getString("cod_cli"));
                negocio.setFecha_neg(rs.getString("fecha_negocio"));
                negocio.setCod_tabla(rs.getString("cod_tabla"));
                negocio.setNodocs(rs.getInt("nro_docs"));
                negocio.setVr_desem(Double.parseDouble(rs.getString("vr_desembolso")));
                negocio.setVr_aval(rs.getFloat("vr_aval"));
                negocio.setVr_cust(rs.getFloat("vr_custodia"));
                negocio.setMod_aval(rs.getString("mod_aval"));
                negocio.setMod_cust(rs.getString("mod_custodia"));
                negocio.setPor_rem(rs.getFloat("porc_remesa"));
                negocio.setMod_rem(rs.getString("mod_remesa"));
                negocio.setVr_negocio(Double.valueOf(rs.getString("vr_negocio")).doubleValue());
                negocio.setEsta(rs.getString("esta"));
                negocio.setCod_cod(rs.getString("id_codeudor"));
                negocio.setFpago(rs.getString("fpago"));
                negocio.setTneg(rs.getString("tneg"));
                negocio.setEstado(rs.getString("estado_neg"));
                negocio.setNit_tercero(rs.getString("nit_tercero"));
                negocio.setFechatran(rs.getString("f_desem"));
                negocio.setCmc(rs.getString("cmc"));
                negocio.setConlet(rs.getString("cletras"));
                negocio.setTotpagado(Double.parseDouble(rs.getString("tot_pagado")));
                negocio.setNumaval(rs.getString("num_aval"));
                negocio.setPagare(rs.getString("cpagare"));
                negocio.setCodigoBanco(rs.getString("banco_cheque"));
                negocio.setCuenta_cheque(rs.getString("cuenta_cheque"));
                negocio.setCnd_aval(rs.getString("cnd_aval"));
                negocio.setCreation_user(rs.getString("create_user"));
                negocio.setId_convenio(rs.getInt("id_convenio"));
                negocio.setvalor_aval(rs.getString("valor_aval"));
                negocio.setTasa(rs.getString("tasa"));
                negocio.setNuevaTasa(rs.getString("nueva_tasa"));
                negocio.setActividad(rs.getString("actividad"));
                negocio.setNom_cli(rs.getString("cliente"));
                negocio.setValor_capacitacion(rs.getDouble("valor_capacitacion"));
                negocio.setValor_central(rs.getDouble("valor_central"));
                negocio.setValor_seguro(rs.getDouble("valor_seguro"));
                negocio.setPorcentaje_cat(rs.getDouble("porcentaje_cat"));
                negocio.setFecha_liquidacion(rs.getString("fecha_liquidacion"));
                negocio.setDist(rs.getString("dist"));
                negocio.setSector(rs.getString("cod_sector"));
                negocio.setSubsector(rs.getString("cod_subsector"));
                negocio.setTipo_cuota(rs.getString("tipo_cuota"));
                negocio.setFinanciaAval(rs.getBoolean("financia_aval"));
                negocio.setNegocio_rel(rs.getString("negocio_rel"));
                negocio.setvalor_remesa(rs.getString("valor_remesa"));
                negocio.setNitp(rs.getString("nit_tercero"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarNegocio[NegociosDAO] "+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return negocio;
    }


    public ArrayList<DocumentosNegAceptado> getLiquidacionAval() {
        return liquidacionAval;
    }

    public void setLiquidacionAval(ArrayList<DocumentosNegAceptado> liquidacionAval) {
        this.liquidacionAval = liquidacionAval;
    }



    public boolean isNegocioAval(String neg ) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_VALIDAR_AVAL";
            boolean sw=false;
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, neg);
                rs = ps.executeQuery();
                if (rs.next())
                {
                   sw=true;
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return sw;
    }

    public String getNumeroPagare(String neg ) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_GET_NUMERO_PAGARE";
            String numero_pagare="";
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, neg);
                rs = ps.executeQuery();
                if (rs.next())
                {
                   numero_pagare=rs.getString("num_pagare");
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return numero_pagare;
    }


          public void actualizaNumeroPagare(String neg, String numero_pagare)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR_NUMERO_PAGARE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, numero_pagare);
                st.setString(2, neg);
                st.executeUpdate();
            }
          }
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE actualizaNumeroPagare " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (st  != null){ try{ st.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            }





//-------factura de venta--------------------------------

  public String getNumeroFacVentaAval(String neg ) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_GET_NUMERO_FACTURA_VENTA_AVAL";
            String numero_factura="";
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, neg);
                rs = ps.executeQuery();
                if (rs.next())
                {
                   numero_factura=rs.getString("NUM_FAC_VENTA_AVAL");
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return numero_factura;
    }



          public void actualizaNumeroFacVentaAval(String neg, String numero_factura)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR_NUMERO_FACTURA_VENTA_AVAL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, neg);
                st.setString(2, numero_factura);
                st.executeUpdate();
            }
          }
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE actualizaNumeroFactura " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (st  != null){ try{ st.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            }

          
          
          
          
    /*
     * Actualizar Negocio
     * Jpinedo
     */
    public String ActualizarNegocioRLQ(Negocios neg, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws SQLException {
        ResultSet rs = null;
        String nombre_sql = "";
        StringStatement sst = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();
        String sql = "";
        /* ----------------------------------------------------------------------------------*/
        try {

            nombre_sql = "SQL_UPDATE_NEGOCIOS_RLQ";
            sst = new StringStatement(this.obtenerSQL(nombre_sql), true);
            sst.setString(1, neg.getCod_cli());
            sst.setString(2, Double.toString(neg.getVr_negocio()));
            sst.setString(3, Integer.toString(neg.getNodocs()));
            sst.setString(4, Double.toString(neg.getVr_desem()));
            sst.setString(5, Double.toString(neg.getVr_aval()));
            sst.setString(6, Double.toString(neg.getVr_cust()));
            sst.setString(7, neg.getMod_aval());
            sst.setString(8, neg.getMod_cust());
            sst.setString(9, Double.toString(neg.getPor_rem()));
            sst.setString(10, neg.getMod_rem());
            sst.setString(11, neg.getCreation_user());
            sst.setString(12, neg.getFpago());
            sst.setString(13, neg.getTneg());
            sst.setString(14, neg.getCod_tabla());
            sst.setString(15, neg.getEsta());
            sst.setString(16, neg.getFecha_neg());
            sst.setString(17, neg.getNitp());
            sst.setString(18, Double.toString(neg.getTotpagado()));
            sst.setString(19, neg.getvalor_aval());
            sst.setString(20, neg.getvalor_remesa());
            sst.setString(21, neg.getTasa());
            sst.setInt(22, neg.getId_convenio());
            sst.setString(23, neg.getCodigoBanco());
            sst.setString(24, neg.getId_sector());
            sst.setString(25, neg.getId_subsector());
            sst.setString(26, neg.getEstado());
            sst.setString(27, neg.getNegocio_rel());
            sst.setString(28, String.valueOf(neg.isFinanciaAval()));
            sst.setString(29, neg.getCod_negocio());
            if (neg.getId_remesa() == 0) {
                sql = sst.getSql().replace("IDREME", "null");
            } else {
                sql = sst.getSql().replace("IDREME", "" + neg.getId_remesa());
            }



            if(neg.getEstado().equals("V")){
                NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
                 sql = sql + daotraza.updateActividad(neg.getCod_negocio(),"DEC");
            }

            sst = new StringStatement(this.obtenerSQL("SQL_DELETE_DOCS"), true);
            sst.setString(1, neg.getCod_negocio());
            sql = sql + sst.getSql();
            sst = null;

            for (int i = 0; i < this.getLiquidacion().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacion().get(i);
                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOC_ACEPTADOS"), true);
                sst.setString(1, neg.getCod_negocio());
                sst.setString(2, liqui.getItem());
                sst.setString(3, liqui.getFecha());
                sst.setString(4, liqui.getDias());
                sst.setDouble(5, liqui.getSaldo_inicial());
                sst.setDouble(6, liqui.getCapital());
                sst.setDouble(7, liqui.getInteres());
                sst.setDouble(8, liqui.getValor());
                sst.setDouble(9, liqui.getSaldo_final());
                sst.setDouble(10, liqui.getSeguro());
                sst.setDouble(11, liqui.getCustodia());
                sst.setDouble(12, liqui.getRemesa());
                sst.setDouble(13, liqui.getCuota_manejo());

                // batch.add(sst.getSql());
                sql = sql + sst.getSql();
                sst = null;
            }
            sst = null;




            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_ANUL_DOCS_FORM"), true);
            sst.setString(1,  numero_form);
            sql = sql + sst.getSql();

            int max=this.maxDocForm(Integer.parseInt(numero_form)) ;
            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
            negtraza.setCodNeg(neg.getCod_negocio());
            sql = sql + daotraza.insertNegocioTrazabilidad(negtraza);
            for (int i = 0; i < this.getLiquidacion().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacion().get(i);
                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_FORM"), true);
                sst.setString(1, numero_form);
                sst.setString(2, liqui.getItem());
		if(!neg.isFinanciaAval() && Integer.parseInt(liqui.getItem())==1 )
                {
                    liqui.setValor(liqui.getValor()+Double.parseDouble(neg.getvalor_aval()));
                }
                sst.setDouble(3, liqui.getValor());
                sst.setString(4, liqui.getFecha());
                sst.setInt(5, max+1);
                sst.setString(6, neg.getCreation_user());
                // batch.add(sst.getSql());
                sql = sql + sst.getSql();
                sst = null;
            }

            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_UPD_FORM"), true);
            sst.setString(1, neg.getTneg());
            sst.setString(2, neg.getCreation_user());
            sst.setString(3, neg.getCod_negocio());
            sst.setString(4, neg.getEstado());
            sst.setInt(5, neg.getId_convenio());
            sst.setString(6, numero_form);
            String sql2 = "";
            if (neg.getCodigoBanco().equals("0") || neg.getCodigoBanco().equals("") || neg.getCodigoBanco().equals("170")) {
                sql2 = sst.getSql().replaceAll("_BANCO_", "null");
            } else {
                sql2 = sst.getSql().replaceAll("_BANCO_", "'" + neg.getCodigoBanco() + "'");
            }
            sql = sql + sql2;


            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_ELIMINAR_COMISIONES"), true);
            sst.setString(1,  neg.getCod_negocio());
            sql = sql + sst.getSql();


            Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.getDatabaseName());
            Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(neg.getDist(), convenio.getImpuesto(), neg.getFecha_neg());
            double porcImpuesto = impuesto.getPorcentaje1() / 100;
            for (int i = 0; i < convenio.getConvenioComision().size(); i++) {

                double vlrComision = neg.getTotpagado() * (convenio.getConvenioComision().get(i).getPorcentaje_comision() / 100);
                double impComision = vlrComision * porcImpuesto;
                sst = null;
                sst = new StringStatement(this.obtenerSQL("SQL_INSERTAR_COMISION"), true);
                sst.setString(1, convenio.getId_convenio());
                sst.setString(2, convenio.getConvenioComision().get(i).getId_comision());
                sst.setString(3, neg.getCod_negocio());
                sst.setDouble(4, convenio.getConvenioComision().get(i).getPorcentaje_comision());
                sst.setDouble(5, vlrComision + impComision);
                sst.setString(6, neg.getCreation_user());
                // batch.add(sst.getSql());
                sql = sql + sst.getSql();
            }
            neg = null;
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE LAS INSERCION DE NEGOCIOS " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (sst != null) {
                try {
                    sst = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return sql;

    }

    public String ActualizarNegocioAvalRLQ(Negocios neg, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws SQLException {
        ResultSet rs = null;
        String nombre_sql = "";
        StringStatement sst = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();
        String sql = "";
        /* ----------------------------------------------------------------------------------*/
        try {

            nombre_sql = "SQL_UPDATE_NEGOCIOS_RLQ";
            sst = new StringStatement(this.obtenerSQL(nombre_sql), true);
            sst.setString(1, neg.getCod_cli());
            sst.setString(2, Double.toString(neg.getVr_negocio()));
            sst.setString(3, Integer.toString(neg.getNodocs()));
            sst.setString(4, Double.toString(neg.getVr_desem()));
            sst.setString(5, Double.toString(neg.getVr_aval()));
            sst.setString(6, Double.toString(neg.getVr_cust()));
            sst.setString(7, neg.getMod_aval());
            sst.setString(8, neg.getMod_cust());
            sst.setString(9, Double.toString(neg.getPor_rem()));
            sst.setString(10, neg.getMod_rem());
            sst.setString(11, neg.getCreation_user());
            sst.setString(12, neg.getFpago());
            sst.setString(13, neg.getTneg());
            sst.setString(14, neg.getCod_tabla());
            sst.setString(15, neg.getEsta());
            sst.setString(16, neg.getFecha_neg());
            sst.setString(17, neg.getNit_tercero());
            sst.setString(18, Double.toString(neg.getTotpagado()));
            sst.setString(19, neg.getvalor_aval());
            sst.setString(20, (neg.getvalor_remesa()!= null)?neg.getvalor_remesa():"0");
            sst.setString(21, neg.getTasa());
            sst.setInt(22, neg.getId_convenio());
            sst.setString(23, neg.getCodigoBanco());
            sst.setString(24, (neg.getId_sector()!= null)?neg.getId_sector():neg.getSector());
            sst.setString(25,  (neg.getId_subsector()!= null)?neg.getId_subsector():neg.getSubsector());
            sst.setString(26, neg.getEstado());
            sst.setString(27, neg.getNegocio_rel());
            sst.setString(28, String.valueOf(neg.isFinanciaAval()));
            sst.setString(29, neg.getCod_negocio());
            if (neg.getId_remesa() == 0) {
                sql = sst.getSql().replace("IDREME", "null");
            } else {
                sql = sst.getSql().replace("IDREME", "" + neg.getId_remesa());
            }



            if(neg.getEstado().equals("V")){
                NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
                 sql = sql + daotraza.updateActividad(neg.getCod_negocio(),"DEC");
            }

            sst = new StringStatement(this.obtenerSQL("SQL_DELETE_DOCS"), true);
            sst.setString(1, neg.getCod_negocio());
            sql = sql + sst.getSql();
            sst = null;

            for (int i = 0; i < this.getLiquidacionAval().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacionAval().get(i);
                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOC_ACEPTADOS"), true);
                sst.setString(1, neg.getCod_negocio());
                sst.setString(2, liqui.getItem());
                sst.setString(3, liqui.getFecha());
                sst.setString(4, liqui.getDias());
                sst.setDouble(5, liqui.getSaldo_inicial());
                sst.setDouble(6, liqui.getCapital());
                sst.setDouble(7, liqui.getInteres());
                sst.setDouble(8, liqui.getValor());
                sst.setDouble(9, liqui.getSaldo_final());
                sst.setDouble(10, liqui.getSeguro());
                sst.setDouble(11, liqui.getCustodia());
                sst.setDouble(12, liqui.getRemesa());
                sst.setDouble(13, liqui.getCuota_manejo());

                // batch.add(sst.getSql());
                sql = sql + sst.getSql();
                sst = null;
            }
            sst = null;




            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_ANUL_DOCS_FORM"), true);
            sst.setString(1,  numero_form);
            sql = sql + sst.getSql();

            int max=this.maxDocForm(Integer.parseInt(numero_form)) ;
            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
            negtraza.setCodNeg(neg.getCod_negocio());
            negtraza.setNumeroSolicitud(numero_form);
            sql = sql + daotraza.insertNegocioTrazabilidad(negtraza);
            for (int i = 0; i < this.getLiquidacionAval().size(); i++) {
                DocumentosNegAceptado liqui = new DocumentosNegAceptado();
                liqui = this.getLiquidacionAval().get(i);
                sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_FORM"), true);
                sst.setString(1, numero_form);
                sst.setString(2, liqui.getItem());
                sst.setDouble(3, liqui.getValor());
                sst.setString(4, liqui.getFecha());
                sst.setInt(5, max+1);
                sst.setString(6, neg.getCreation_user());
                // batch.add(sst.getSql());
                sql = sql + sst.getSql();
                sst = null;
            }

            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_UPD_FORM"), true);
            sst.setString(1, neg.getTneg());
            sst.setString(2, neg.getCreation_user());
            sst.setString(3, neg.getCod_negocio());
            sst.setString(4, neg.getEstado());
            sst.setInt(5, neg.getId_convenio());
            sst.setString(6, numero_form);
            String sql2 = "";
            neg.setCodigoBanco("0");
            if (neg.getCodigoBanco().equals("0") || neg.getCodigoBanco().equals("") || neg.getCodigoBanco().equals("170")) {
                sql2 = sst.getSql().replaceAll("_BANCO_", "null");
            } else {
                sql2 = sst.getSql().replaceAll("_BANCO_", "'" + neg.getCodigoBanco() + "'");
            }
            sql = sql + sql2;

            sst = null;
            sst = new StringStatement(this.obtenerSQL("SQL_ELIMINAR_COMISIONES"), true);
            sst.setString(1,  neg.getCod_negocio());
            sql = sql + sst.getSql();


            Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.getDatabaseName());
            Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(neg.getDist(), convenio.getImpuesto(), neg.getFecha_neg());
            double porcImpuesto = impuesto.getPorcentaje1() / 100;
            for (int i = 0; i < convenio.getConvenioComision().size(); i++) {

                double vlrComision = neg.getTotpagado() * (convenio.getConvenioComision().get(i).getPorcentaje_comision() / 100);
                double impComision = vlrComision * porcImpuesto;
                sst = null;
                sst = new StringStatement(this.obtenerSQL("SQL_INSERTAR_COMISION"), true);
                sst.setString(1, convenio.getId_convenio());
                sst.setString(2, convenio.getConvenioComision().get(i).getId_comision());
                sst.setString(3, neg.getCod_negocio());
                sst.setDouble(4, convenio.getConvenioComision().get(i).getPorcentaje_comision());
                sst.setDouble(5, vlrComision + impComision);
                sst.setString(6, neg.getCreation_user());
                // batch.add(sst.getSql());
                sql = sql + sst.getSql();
            }
            neg = null;
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE LAS INSERCION DE NEGOCIOS " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (sst != null) {
                try {
                    sst = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return sql;

    }

    public String insertNegocio(Negocios neg) {

        String sql = "";
        String nombre_sql = "SQL_INSERTAR_NEGOCIO_MICROCREDITO";
        StringStatement sst = null;

        try {
            sst = new StringStatement(this.obtenerSQL(nombre_sql), true);
            sst.setString(1, neg.getCod_cli());
            sst.setString(2, Double.toString(neg.getVr_negocio()));
            sst.setString(3, Integer.toString(neg.getNodocs()));
            sst.setDouble(4, neg.getVr_desem());
            sst.setString(5, neg.getCreation_user());
            sst.setString(6, neg.getFpago());
            sst.setString(7, neg.getTneg());
            sst.setString(8, neg.getDist());
            sst.setString(9, neg.getFecha_neg());
            sst.setString(10, Double.toString(neg.getTotpagado()));
            sst.setString(11, neg.getCod_negocio());
            sst.setString(12, neg.getTasa());
            sst.setInt(13, neg.getId_convenio());
            sst.setString(14, neg.getEstado());
            sst.setDouble(15, neg.getPorcentaje_cat());
            sst.setDouble(16, neg.getValor_capacitacion());
            sst.setDouble(17, neg.getValor_central());
            sst.setDouble(18, neg.getValor_seguro());
            sst.setString(19, neg.getCod_cli());
            sst.setString(20, neg.getFecha_liquidacion());
            sst.setString(21, neg.getTipo_cuota());
            sst.setString(22, neg.getTipoProceso());
            sst.setString(23, Double.toString(neg.getValor_fianza()));
            sst.setString(24, Double.toString(neg.getValor_total_poliza()));
            sst.setDouble(25, neg.getPorc_fin_aval());
            sst.setDouble(26, neg.getPorc_dto_aval());
            sst.setString(27, neg.getPolitica());
            sst.setDouble(28, neg.getValor_renovacion());
            sst.setString(29, Double.toString(neg.getValor_fianza()));
            sql=sst.getSql();

            sst = null;
        } catch (Exception e) {
            System.out.println("Error al generar sql solicitud: " + e.toString());
            e.printStackTrace();
        }
        return sql;
    }

    public String updateFormulario(Negocios neg, String numero_form) {
        String sql = "";
        StringStatement sst2 = null;
        try {
            sst2 = new StringStatement(this.obtenerSQL("SQL_UPD_FORM"), true);
            sst2.setString(1, neg.getTneg());
            sst2.setString(2, neg.getCreation_user());
            sst2.setString(3, neg.getCod_negocio());
            sst2.setString(4, neg.getEstado());
            sst2.setInt(5, neg.getId_convenio());
            sst2.setString(6, numero_form);
            if (neg.getCodigoBanco().equals("0") || neg.getCodigoBanco().equals("") || neg.getCodigoBanco().equals("170")) {
                sql = sst2.getSql().replaceAll("_BANCO_", "null");
            } else {
                sql = sst2.getSql().replaceAll("_BANCO_", "'" + neg.getCodigoBanco() + "'");
            }
        } catch (Exception e) {
            System.out.println("Error al generar sql solicitud: " + e.toString());
            e.printStackTrace();
        }
        return sql;
    }

    /**
     * M�todo que trae las cuotas de un negocio que tiene intereses pendientes por causar
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> interesesPendientes(String negocio) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_NEG_PEND_INTERES";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                st.setString(1, negocio);
                rs = st.executeQuery();

                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean = this.loadDatosCxcInteres(rs);
                    bean.setValor_19(rs.getString("cliente"));
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en interesesPendientes() " + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    /**
     * M�todo que trae las cuotas de un negocio que tiene facturas cat pendientes por causar
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> catPendientes(String negocio) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_NEG_PEND_CAT";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                st.setString(1, negocio);
                rs = st.executeQuery();

                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean = this.loadDatosCxcCat(rs);
                    bean.setValor_14(rs.getString("cliente"));
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en  catPendientes() " + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    /**
     * M�todo que carga los datos necesarios para la creacion de la cxc de cat microcredito
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public BeanGeneral loadDatosCxcCat(ResultSet rs) throws Exception {
        BeanGeneral bean = new BeanGeneral();
        try {
            bean.setValor_01(rs.getString("prefijo_cxc_cat"));
            bean.setValor_02(rs.getString("cod_cli"));
            bean.setValor_03(rs.getString("nomb_cuenta"));
            bean.setValor_04(rs.getString("fecha"));
            bean.setValor_05(rs.getString("cat"));
            bean.setValor_06(rs.getString("impuesto"));
            bean.setValor_07(rs.getString("item"));
            bean.setValor_08(rs.getString("cuenta_cat"));
            bean.setValor_09(rs.getString("dist"));
            bean.setValor_10(rs.getString("documento"));
            bean.setValor_11(rs.getString("cmc"));
            bean.setValor_12(rs.getString("fecha_doc"));
            bean.setValor_13(rs.getString("cod_neg"));
            //bean.setValor_18(rs.getString("concepto"));

        } catch (Exception e) {
            throw new Exception("loadDatosCxcCat" + e.getMessage());
        }
        return bean;
    }

    /**
     * M�todo que trae el query para la anulacion de factura de interes de una cuota o factura
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public String anularFacturaInteres(String factura) throws SQLException {
        String query = "SQL_ANULAR_FACT_INTERES";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, factura);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en anularFacturaInteres[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String NegociosGuardar(Negocios neg, String numero_solicitud, String ciclo, String numero_form, Convenio convenio, NegocioTrazabilidad negtraza) throws SQLException {
        ResultSet rs = null;
        String nombre_sql = "";
        StringStatement sst2 = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();
        String sql = "";
        if(chequesx==null){
            chequesx=new ArrayList();
        }
        //Segunda parte
        if (numero_solicitud != null) {
            try {
                sst2 = new StringStatement(this.obtenerSQL("SQL_ADD_INF_METROTEL"), true);
                sst2.setString(1, numero_solicitud);
                sst2.setString(2, ciclo);
                sst2.setString(3, neg.getCod_negocio());
               sql=sql+sst2.getSql();
            } catch (Exception ex) {
                System.out.println("error por codneg" + ex.toString() + "__" + ex.getMessage());
                ex.printStackTrace();
                throw new SQLException(ex.getMessage());
            } finally {
                if (sst2 != null) {
                    try {
                        sst2 = null;
                    } catch (Exception e) {
                        throw new SQLException("ERROR CERRANDO EL STRINGSTATEMENT " + e.getMessage());
                    }
                }
            }
        }
//Cierre Segunda parte

//Tercera parte
        try {
            if ((neg.getTneg()).equals("02")) {
                nombre_sql = "SQL_INSERTAR_NEGOCIOS";
            } else {
                nombre_sql = "SQL_INSERTAR_NEGOCIOS2";
            }
            sst2 = new StringStatement(this.obtenerSQL(nombre_sql), true);

            sst2.setString(1, neg.getCod_cli());
            sst2.setString(2, Double.toString(neg.getVr_negocio()));
            sst2.setString(3, Integer.toString(neg.getNodocs()));
            sst2.setString(4, Double.toString((numero_solicitud == null) ? neg.getVr_desem() : neg.getVr_negocio()));
            sst2.setString(5, Double.toString(neg.getVr_aval()));
            sst2.setString(6, Double.toString(neg.getVr_cust()));
            sst2.setString(7, neg.getMod_aval());
            sst2.setString(8, neg.getMod_cust());
            sst2.setString(9, Double.toString(neg.getPor_rem()));
            sst2.setString(10, neg.getMod_rem());
            sst2.setString(11, neg.getCreation_user());
            sst2.setString(12, neg.getFpago());
            sst2.setString(13, neg.getTneg());
            sst2.setString(14, neg.getCod_tabla());
            sst2.setString(15, neg.getEsta());
            sst2.setString(16, neg.getFecha_neg());
            sst2.setString(17, neg.getNitp());
            sst2.setString(18, Double.toString(neg.getTotpagado()));
            if ((neg.getTneg()).equals("02")) {
                sst2.setString(19, neg.getNitp());
                sst2.setString(20, neg.getNitp());
                sst2.setString(21, neg.getCod_negocio());
                sst2.setString(22, neg.getvalor_aval());
                sst2.setString(23, neg.getvalor_remesa());
                sst2.setDouble(24, neg.getVr_aval());
                sst2.setInt(25, neg.getId_convenio());
            } else {
                sst2.setString(19, neg.getCod_negocio());
                sst2.setString(20, neg.getvalor_aval());
                sst2.setString(21, neg.getvalor_remesa());
                sst2.setDouble(22, neg.getVr_aval());
                sst2.setString(23, neg.getCnd_aval());
                sst2.setInt(24, neg.getId_convenio());
                sst2.setString(25, neg.getCodigoBanco());
            }
            sst2.setString(26, neg.getId_sector());
            sst2.setString(27, neg.getId_subsector());
            sst2.setString(28, neg.getEstado());
            sst2.setString(29, neg.getTipoProceso());
            sst2.setString(30, negtraza.getActividad());
            sst2.setString(31, neg.getNegocio_rel());
            sst2.setString(32, String.valueOf(neg.isFinanciaAval()));

            if(neg.getId_remesa()==0){
                sql=sql+sst2.getSql().replace("IDREME", "null");
                }else{
                sql=sql+sst2.getSql().replace("IDREME", ""+neg.getId_remesa());
            }


            for (int i = 0; i < chequesx.size(); i++) {
                chequeCartera chequeCarterax = (chequeCartera) chequesx.get(i);
                sst2 = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS"), true);

                sst2.setString(1, neg.getCod_negocio());
                sst2.setString(2, chequeCarterax.getItem());
                sst2.setString(3, chequeCarterax.getFechaCheque());
                sst2.setString(4, chequeCarterax.getDias());
                sst2.setString(5, chequeCarterax.getSaldoInicial());
                sst2.setString(6, chequeCarterax.getValorCapital());
                sst2.setString(7, chequeCarterax.getValorInteres());
                sst2.setString(8, chequeCarterax.getValor());
                sst2.setString(9, chequeCarterax.getSaldoFinal());
                sst2.setString(10, chequeCarterax.getNoAval());
                 sql=sql+sst2.getSql();
                sst2 = null;
            }
            sst2 = null;

            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(this.getDatabaseName());
            negtraza.setCodNeg(neg.getCod_negocio());
               sql=sql+daotraza.insertNegocioTrazabilidad(negtraza);
            Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.getDatabaseName());
            Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(neg.getDist(), convenio.getImpuesto(), neg.getFecha_neg());
            double porcImpuesto = impuesto.getPorcentaje1() / 100;
            for (int i = 0; i < convenio.getConvenioComision().size(); i++) {

                double vlrComision = neg.getTotpagado() * (convenio.getConvenioComision().get(i).getPorcentaje_comision()/100);
                double impComision = vlrComision * porcImpuesto;

                sst2 = null;
                sst2 = new StringStatement(this.obtenerSQL("SQL_INSERTAR_COMISION"), true);
                sst2.setString(1, convenio.getId_convenio());
                sst2.setString(2, convenio.getConvenioComision().get(i).getId_comision());
                sst2.setString(3,neg.getCod_negocio());
                sst2.setDouble(4, convenio.getConvenioComision().get(i).getPorcentaje_comision());
                sst2.setDouble(5, vlrComision+impComision);
                sst2.setString(6,neg.getCreation_user());
               sql=sql+sst2.getSql();
            }
            neg = null;
        } catch (Exception e) {
            throw new SQLException("ERROR DURANTE LAS INSERCION DE NEGOCIOS " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (sst2 != null) {
                try {
                    sst2 = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            //if (con != null){ try{ this.desconectar(con);  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            //rmartinez 2010-05-28
        }

//Cierre Tercera parte

        /*   try {

         // apdao.ejecutarSQL(batch);


         } catch (SQLException e) {
         throw new SQLException("ERROR DURANTE INSERCION DE FACTURAS" + e.getMessage());
         }*/

        return sql;
    }



        public String MarcarNegocio(String negocio ,String negocio_rel,String concepto_rel) throws Exception{
        String cadena = "";
        String query = "MARCAR_NEGOCIO";
        String sql = "";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql, true);

            cadena = st.getSql();

            cadena = cadena.replace("#1", negocio_rel);
            cadena = cadena.replace("#2", concepto_rel);
            cadena = cadena.replace("#3", negocio);
            st = null;
        }
        catch (Exception e) {
            System.out.println("Error al generar sql docs: "+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }


            public String DeleteDocumentosForms(Negocios neg,String numero_form) throws Exception
    {
        String sql="";
        StringStatement sst = null;
        sst = new StringStatement(this.obtenerSQL("SQL_DEL_DOCS_FORM"), true);
        sst.setString(1, numero_form);
        sql = sql + sst.getSql();
        return sql;
    }


        public String InsertaDocumentosFormsReliquidacionConvenio(Negocios neg,String numero_form) throws Exception
    {
        String sql="";
        StringStatement sst = null;
        sst = new StringStatement(this.obtenerSQL("SQL_DEL_DOCS_FORM"), true);
        sst.setString(1, numero_form);
        sql = sql + sst.getSql();
        for (int i = 0; i < chequesx.size(); i++) {
            chequeCartera chequeCarterax = (chequeCartera) chequesx.get(i);
            sst = new StringStatement(this.obtenerSQL("SQL_INSERT_DOCS_FORM"), true);

            sst.setString(1, numero_form);
            sst.setString(2, chequeCarterax.getItem());
            sst.setString(3, chequeCarterax.getValor());
            sst.setString(4, chequeCarterax.getFechaCheque());
            sst.setInt(5, 1);
            sst.setString(6, neg.getCreation_user());
               sql=sql+sst.getSql();
            sst = null;
        }
        return sql;
    }

    public String actualiza_perfeccionamiento(String cod, String estado) throws SQLException, Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("ACTUALIZAR_PERFECCIONAMIENTO"), true);
            st.setString(1, estado);
            st.setString(2, cod);
            sql = st.getSql();

        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE AVALAR NEGOCIO" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE AVALAR NEGOCIO" + e.getMessage());
                }
            }
        }
        return sql;
    }

    public ArrayList<Proveedor> filtroProveedoresTransferencias(String negocio, String banco) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringStatement st = null;
        String query = "FILTRO_TRANSFERENCIA_TRANSFERENCIAS";
       ArrayList<Proveedor> lista=  new ArrayList<Proveedor>();
        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                if ((Integer.parseInt(negocio) == 1) && (banco.toUpperCase().contains("CAJA "))) {
                    filtro = " AND (p.tipo_cuenta = '' OR p.no_cuenta='0001')";
                } else {
                    filtro = "";
                }
                ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro#", filtro));
           ps.setString(1,negocio);
                rs = ps.executeQuery();
                while (rs.next()) {
               Proveedor prov =new Proveedor();             
                    prov.setCedula_cuenta(rs.getString("cedula_cuenta"));
                    prov.setNombre_cuenta(rs.getString("nombre_cuenta"));
                    lista.add(prov);
                    prov = null;//
                }
       }}
        catch (Exception ex)
        {
            System.out.println("errror en negocios dao en obtainDocsAceptados"+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
        return lista;
    }

    public boolean isCxpAfiliado(String neg ,String cxp) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "VALIDAR_CXP_AFILIADO";
            boolean sw=false;
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, neg);
                ps.setString(2, cxp);
                rs = ps.executeQuery();
                if (rs.next())
                {
                   sw=true;
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return sw;
    }

    /**
     * Atualiza la Fecha_Factura_Aval cuando es generada la misma y es priemra vez que se genera.
     * @autor David Paz
     * @throws SQLException
     */
    public void ActualizaFechaFacturaAval(String cod_neg, String fecha_factura_aval) throws SQLException {
        String nombre_sql = "SQL_UPDATE_FECHA_FACTURA_AVAL";
        StringStatement sst2 = null;
        ApplusDAO apdao = new ApplusDAO(this.getDatabaseName());
        Vector batch = new Vector();

        try {
            sst2 = new StringStatement(this.obtenerSQL(nombre_sql), true);
            sst2.setString(1, fecha_factura_aval);
            sst2.setString(2, cod_neg);
            batch.add(sst2.getSql());
            apdao.ejecutarSQL(batch);
        } catch (Exception ex) {
            System.out.println("error al actualizar Fecha_Factura_Aval. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("ERROR DURANTE ACTUALIZACION DE Fecha_Factura_Aval" + ex.getMessage());
        } finally {
            if (sst2 != null) {
                try {
                    sst2 = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL STRINGSTATEMENT " + e.getMessage());
                }
            }
        }
    }

    
    /**
     * Metodo para Ejecutar la funcion de creacion de nuevas facturas.
     * @autor egonzalez
     * @throws SQLException
     */
    public String runEndosoTersero(String factura, String idConvenio) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SP_ENDOSO_TERCERO";
        String resultado="";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, factura);
                ps.setString(2, idConvenio);
                rs = ps.executeQuery();
                while (rs.next()) {
                    resultado=rs.getString("resultado");
                }

            }

            return resultado;
        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :SP_EndosoTercero. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :SP_EndosoTercero." + ex.getMessage());
        } finally {
            if (ps != null) {
                try {
                    ps = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL PreparedStatement " + e.getMessage());
                }
            }
        }

    }

    
       
    /**
     * Metodo para obtener el numero cta.
     * @autor jpacosta
     * @throws SQLException
     */
    public String accountToTxt(String cta) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "get_account_To_Txt";
        String resultado="";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, cta);
                rs = ps.executeQuery();
                while (rs.next()) {
                    resultado = rs.getString("account_number_to_txt");
                }

            }

            return resultado;
        } catch (Exception ex) {
            throw new SQLException("error :" + ex.getMessage());
        } finally {
            if (ps != null) {
                try {
                    ps = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL PreparedStatement " + e.getMessage());
                }
            }
        }

    }

    public double valorCentral(String tbl_gen, String parametro) throws Exception {

        double valor = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VALOR_CENTRAL";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tbl_gen);
            ps.setString(2, parametro);
            rs = ps.executeQuery();
            if (rs.next()) {

                valor = Double.parseDouble(rs.getString("dato"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en valorCentral [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }


        return valor;

    }

    public ArrayList<BeanGeneral> cargarNegocios(String negocio, String banco) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NEGOCIOS_TRANSFERENCIA";
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            if(negocio.equals("7")){
                sql = sql.replace("#negocios#", "n.cod_neg like 'NG%'");
            }else if(negocio.equals("0")){
                sql = sql.replace("#negocios#", "n.cod_neg like 'NG%'");
            }else{
                sql = sql.replace("#negocios#", "n.cod_neg not like 'NG%'");
            }

            if((Integer.parseInt(negocio) == 1) && (banco.toUpperCase().contains("CAJA "))){
                sql = sql.replace("#cond#", " AND (p.tipo_cuenta = '' OR p.no_cuenta='0001')");
            }else{
                sql = sql.replace("#cond#", "");
            }

            if (con != null) {
                ps = con.prepareStatement(sql);//JJCastro fase2
                if(negocio.equals("0")){
                    ps.setString(1, "13");
                    //ps.setString(2, "13");
                }else{
                    ps.setString(1, negocio);
                    //ps.setString(2, negocio);
                }
                System.out.println(ps.toString());
                rs = ps.executeQuery();
                while (rs.next()) {
                    BeanGeneral bg = new BeanGeneral();
                    bg.setValor_01(rs.getString(1));
                    bg.setValor_02(rs.getString(2));
                    bg.setValor_03(rs.getString(3));
                    bg.setValor_04(rs.getString(4));
                    bg.setValor_05(rs.getString(5));
                    bg.setValor_06(rs.getString(6));
                    bg.setValor_07(rs.getString(7));
                    bg.setValor_08(rs.getString(8));
                    bg.setValor_09(rs.getString(9));
                    bg.setValor_10(rs.getString(10));
                    bg.setValor_11(rs.getString(11));
                    bg.setValor_12(rs.getString(12));
                    bg.setValor_13(rs.getString(13));
                    bg.setValor_15(rs.getString(14));
                    bg.setValor_16(rs.getString(15));
                    bg.setValor_17(rs.getString(16));
                    lista.add(bg);

                }

            }

           
        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :cargarNegocios. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :cargarNegocios." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return lista;

    }

    public ArrayList<BeanGeneral> cargarNegociosLibranza(String negocio, String banco) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NEGOCIOS_TRANSFERENCIA_LIBRANZA";
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            sql = sql.replace("#negocios#", "n.cod_neg not like 'NG%'");

            if (con != null) {
                ps = con.prepareStatement(sql);//JJCastro fase2
                ps.setString(1, negocio);
                ps.setString(2, negocio);

                rs = ps.executeQuery();
                while (rs.next()) {
                    BeanGeneral bg = new BeanGeneral();
                    bg.setValor_01(rs.getString(1));
                    bg.setValor_02(rs.getString(2));
                    bg.setValor_03(rs.getString(3));
                    bg.setValor_04(rs.getString(4));
                    bg.setValor_05(rs.getString(5));
                    bg.setValor_06(rs.getString(6));
                    bg.setValor_07(rs.getString(7));
                    bg.setValor_08(rs.getString(8));
                    bg.setValor_09(rs.getString(9));
                    bg.setValor_10(rs.getString(10));
                    bg.setValor_11(rs.getString(11));
                    bg.setValor_12(rs.getString(12));
                    bg.setValor_13(rs.getString(13));
                    bg.setValor_15(rs.getString(14));
                    bg.setValor_22(rs.getString(15));
                    lista.add(bg);
                }
            }
        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :cargarNegocios. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :cargarNegocios." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return lista;
    }

    public ArrayList<BeanGeneral> cargarNegociosCHequeLibranza(String doc_rel) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CXP_LIBRANZA_CHEQUE";
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, doc_rel);
                ps.setString(2, doc_rel);
                rs = ps.executeQuery();
                while (rs.next()) {
                    BeanGeneral bg = new BeanGeneral();
                    bg.setValor_16(rs.getString(1));
                    bg.setValor_17(rs.getString(2));
                    bg.setValor_18(rs.getString(3));
                    bg.setValor_19(rs.getString(4));
                    bg.setValor_20(rs.getString(5));
                    bg.setValor_21(rs.getString(6));
                    bg.setValor_23(rs.getString(7));
                    bg.setValor_24(rs.getString(8));
                    lista.add(bg);
                }
            }
        } catch (Exception ex) {
            System.out.println("error al ejecutar la METODO :cargarNegociosCHequeLibranza. en la carga de la sudgrilla" + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la METODO :METODO. en la carga de la sudgrilla" + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return lista;
    }

     public String perfeccionarNegocioLibranza(String negocio,Usuario usuario) throws SQLException {
         //Connection con = null;
        //PreparedStatement st = null;
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_PERFECCIONAR_NEGOCIOS_LIBRANZA";
        try {
//             con = this.conectarJNDI(query);
//               st = con.prepareStatement(this.obtenerSQL(query));
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, negocio);
            st.setString(2, usuario.getLogin());
            st.setString(3, negocio);
            respuesta = st.getSql();

        } catch (SQLException e) {
            respuesta = "Error";
            throw new SQLException("ERROR DURANTE perfeccionamiento del negocio " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            return respuesta;
        }

    }

     public String marcarCXPGeneradoCheque(String cxp,String negocios,Usuario usuario) throws SQLException {
        Connection con = null;
        // PreparedStatement st = null;
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_MARCAR_CXP_NEGOCIOS_LIBRANZA";
        try {
             //con = this.conectarJNDI(query);
            // st = con.prepareStatement(this.obtenerSQL(query));
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, cxp);
            st.setString(3, negocios);
            respuesta = st.getSql();
            //st.executeUpdate();

        } catch (SQLException e) {
            respuesta = "Error";
            throw new SQLException("ERROR DURANTE perfeccionamiento del negocio " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            return respuesta;
        }

    }

    public String perfeccionarNegocioLI(String negocio, Usuario usuario) throws SQLException {
//        Connection con = null;
//        PreparedStatement st = null;
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_PERFECCIONAR_NEGOCIOS_LI";
        try {
//             con = this.conectarJNDI(query);
//             st = con.prepareStatement(this.obtenerSQL(query));
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, negocio);
            respuesta = st.getSql();

        } catch (SQLException e) {
            respuesta = "Error";
            throw new SQLException("ERROR DURANTE perfeccionamiento del negocio " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            return respuesta;
        }
    }

    public String datosPdf(String cxp, String negocios,String banco_transferencia, Usuario usuario,String conv) {
        String respuesta = "";
        JsonObject objeto = new JsonObject();
        JsonArray arr = (JsonArray) new JsonParser().parse(informacionPdf(cxp, negocios));
        try {
            String ancabezado = "";
            String parrafo = "";
            // JsonArray arr = info.getAsJsonArray("json");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                ancabezado = "SOLICITUD DE EMISION CHEQUE DE GERENCIA.";

                parrafo = "Solicitamos sea emitido cheque de gerencia para ser debitado de la cuenta corriente "
                        + " No.692-605772-98 a nombre de Fintra S.A con Nit 802.022.016-1, mediante la siguiente instrucci�n:";

                respuesta = exportarPdf(usuario, objeto.get("payment_name").getAsString(), objeto.get("documento").getAsString(), objeto.get("proveedor").getAsString(), objeto.get("vlr_transferir").getAsString(), objeto.get("documento_relacionado").getAsString(), ancabezado, parrafo);
                respuesta = desembolsoCheque(objeto.get("documento").getAsString(), banco_transferencia, usuario,conv);
            }

        } catch (Exception e) {
            respuesta = "Error";
            e.printStackTrace();
        }
        return respuesta;
    }

    public String desembolsoCheque(String cheque,String banco_transferencia,Usuario usuario, String conv) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
         String query = "";
        if (conv.equals("Microcredito")){
        query = "SQL_DESEMBOLSO_CHEQUE_MICRO";
        }else{
         query = "SQL_DESEMBOLSO_CHEQUE";
        }
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cheque);
            ps.setString(2, banco_transferencia);
            ps.setString(3, usuario.getLogin());

            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta =  "{\"respuesta\":\""+rs.getString("respuesta")+"\"}";
            }

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "{\"respuesta\":\"ERROR\"}";
        }
        return respuesta;
    }

    public String informacionPdf(String cxp, String negocios) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INFO_CXP_NEGOCIOS_LIBRANZA";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocios);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        }
        return informacion;
    }

    public String exportarPdf(Usuario usuario, String nombre, String documento_rel, String nit, String vlr_transferir,String negocio, String ancabezado, String parrafo) {

        String ruta = "";
        String msg = "OK";
        try {
            ruta = this.directorioArchivo(usuario.getLogin(), "pdf", negocio, documento_rel);

            Font fuente = new Font(Font.HELVETICA, 10, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font fuenteG = new Font(Font.HELVETICA, 10, Font.BOLD, new java.awt.Color(0, 0, 0));
            Font fuenteJ = new Font(Font.HELVETICA, 13, Font.BOLD);
            fuenteJ.setColor(java.awt.Color.WHITE);
            PdfPCell celda = null;
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(ruta));
            documento.open();
            documento.newPage();

            PdfPTable thead = new PdfPTable(1);
            celda = new PdfPCell();
            celda.setBorder(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //row2
            thead = new PdfPTable(1);
            Calendar calendario = Calendar.getInstance();
            String mes = "";
            mes = this.mesToString(calendario.get(Calendar.MONTH) + 1);
            documento.add(new Paragraph(new Phrase("Barranquilla, " + mes + " " + calendario.get(Calendar.DAY_OF_MONTH) + " de " + calendario.get(Calendar.YEAR), fuente)));
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);

            documento.add(thead);
            thead = new PdfPTable(1);
            documento.add(new Paragraph(new Phrase("Se�ores,", fuente)));
            celda.setPhrase(new Paragraph(new Phrase("", fuente)));
            thead.addCell(celda);
            documento.add(new Paragraph(new Phrase("Bancolombia" + ",", fuenteG)));
            celda.setPhrase(new Paragraph(new Phrase("", fuente)));
            documento.add(new Paragraph(new Phrase("Barranquilla" + ",", fuente)));
            celda.setPhrase(new Paragraph(new Phrase("", fuente)));
            thead.addCell(celda);


            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(new Paragraph(new Phrase("Regerencia: " + ancabezado, fuenteG)));
            documento.add(thead);

            thead = new PdfPTable(1);
            documento.add(new Paragraph(new Phrase("Estimado cliente,", fuente)));
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);

                  
            
            thead = new PdfPTable(1);
            documento.add(new Paragraph(new Phrase(parrafo, fuente)));
            thead.addCell(celda);
            documento.add(thead);

            agregarInformacionBasica(documento, nombre, nit, vlr_transferir);
//            String str = "Esperamos su pronta respuesta ya que cumplido el tiempo estipulado ser� renovada automaticamente con la aseguradora"
//                    + " <b>" + "variable" + "</b> , seg�n clausula estipulada en el contrato de prenda.";
//            HTMLWorker htmlWorker = new HTMLWorker(documento);
//            htmlWorker.parse(new StringReader(str));
//
//            celda.setPhrase(new Paragraph(new Phrase("", fuente)));
//            thead.addCell(celda);
//            documento.add(thead);


            thead = new PdfPTable(1);
            documento.add(new Paragraph(new Phrase("Autorizamos al sr. Harold Casadiego Salcedo  con CC 1.129.503.945 de Barranquilla\n" +
                "para reclamar dicho cheque.", fuente)));
            thead.addCell(celda);
            documento.add(thead);

            
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("Atentamente,", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("JOSE LUIS GOMEZ O.", fuenteG)));
            documento.add(new Paragraph(new Phrase("Firma registrada ", fuenteG)));
            documento.close();
        } catch (Exception e) {
            msg = "ERROR";
            e.printStackTrace();
        }
        return msg;
    }

    private void agregarInformacionBasica(Document document, String nombre, String nit,String vlr_transferir) throws Exception {
        DecimalFormat formato = new DecimalFormat("#,###.00");
        Font fuente = new Font(Font.HELVETICA, 10, Font.NORMAL, new java.awt.Color(0, 0, 0));
        Font fuenteG = new Font(Font.HELVETICA, 10, Font.BOLD, new java.awt.Color(0, 0, 0));
        Font fuenteJ = new Font(Font.HELVETICA, 13, Font.BOLD);
        fuenteJ.setColor(java.awt.Color.WHITE);

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(80);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell = new PdfPCell();
//        cell.setPhrase(new Phrase("INFORMACI\u00d3N B\u00c1SICA", fuenteG));
//        cell.setPadding(3);
//        cell.setColspan(10);
//        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("Nombre", fuenteG));
        table.addCell(cell);
        document.add(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("Valor", fuenteG));
        cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase(nombre + " -Nit." + nit, fuente));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("$"+formato.format(Double.parseDouble(vlr_transferir)), fuente));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Total", fuenteG));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(cell);

      

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("$"+formato.format(Double.parseDouble(vlr_transferir)), fuente));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(cell);


        table.setSpacingAfter(10);
        document.add(table);
    }

    private String directorioArchivo(String user, String extension, String negocio, String documento_rel) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rsb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + negocio + "_" + documento_rel + "_" +  fmt.format(6) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    private Document createDoc() {
        Document doc = new Document(PageSize.A4, 25, 25, 35, 30);
        return doc;
    }

    private String mesToString(int mes) {
        String texto = "";
        switch (mes) {
            case 1:
                texto = "Enero";
                break;
            case 2:
                texto = "Febrero";
                break;
            case 3:
                texto = "Marzo";
                break;
            case 4:
                texto = "Abril";
                break;
            case 5:
                texto = "Mayo";
                break;
            case 6:
                texto = "Junio";
                break;
            case 7:
                texto = "Julio";
                break;
            case 8:
                texto = "Agosto";
                break;
            case 9:
                texto = "Septiembre";
                break;
            case 10:
                texto = "Octubre";
                break;
            case 11:
                texto = "Noviembre";
                break;
            case 12:
                texto = "Diciembre";
                break;
            default:
                texto = "Enero";
                break;
        }
        return texto;
    }

    public ArrayList<BeanGeneral> cargarNegociosProveedor(String[] nits, String fechaInicio, String fechaFin, String un_neg, String banco) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NEGOCIOS_TRANSFERENCIA_PROVEEDOR";
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        String ids = "";
        String sql = "";

        for (int i = 0; i < nits.length; i++) {
            if (!ids.equals("")) {
                ids += ",";
            }
            ids += "'" + nits[i] + "'";
        }

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#cond#", " cxp.proveedor in(#nits#) and n.fecha_ap:: date between '" + fechaInicio + "' and '" + fechaFin + "' and ");
            sql = sql.replaceAll("#nits#", ids);

            if ((Integer.parseInt(un_neg) == 1) && (banco.toUpperCase().contains("CAJA "))) {
                sql = sql.replaceAll("#cond2#", " AND (p.tipo_cuenta = '' OR p.no_cuenta='0001')");
            } else {
                sql = sql.replaceAll("#cond2#", "");
            }
            System.out.println(sql);
            if (con != null) {
                ps = con.prepareStatement(sql);
                ps.setString(1,un_neg );
                rs = ps.executeQuery();
                while (rs.next()) {
                    BeanGeneral bg = new BeanGeneral();
                    bg.setValor_01(rs.getString(1));
                    bg.setValor_02(rs.getString(2));
                    bg.setValor_03(rs.getString(3));
                    bg.setValor_04(rs.getString(4));
                    bg.setValor_05(rs.getString(5));
                    bg.setValor_06(rs.getString(6));
                    bg.setValor_07(rs.getString(7));
                    bg.setValor_08(rs.getString(8));
                    bg.setValor_09(rs.getString(9));
                    bg.setValor_10(rs.getString(10));
                    bg.setValor_11(rs.getString(11));
                    bg.setValor_12(rs.getString(12));
                    bg.setValor_13(rs.getString(13));
                    bg.setValor_15(rs.getString(14));
                    lista.add(bg);

                }

            }

        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :cargarNegociosProveedor. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :cargarNegociosProveedor." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return lista;
    }

    public String cargarComision() throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String comision = "";
        String query = "SQL_CARGAR_COMISION";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));

                rs = ps.executeQuery();
                while (rs.next()) {
                   comision += rs.getString(1)+": "+ rs.getString(1)+"; ";
                }
            }

           
        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :cargarComision. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :cargarComision." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return comision;
    }

    public void updateEstadoNegocio(String neg,  String login, String estado, String actividad) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR_ESTADOS_NEGOCIO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, estado);
                st.setString(2, actividad);
                st.setString(3, login);
                st.setString(4, neg);

                st.executeUpdate();
            }
          }
            catch(SQLException e)
            {
            throw new SQLException("ERROR DURANTE updateEstadoNegocio " + e.getMessage() + " " + e.getErrorCode());
             }finally{
            if (st  != null){ try{ st.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            }

    public String obtenerUltimoConceptoTraza(String codneg) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String concepto = "";
        String query = "SQL_OBTENER_ULTIMO_CONCEPTO_TRAZABILIDAD";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, codneg);

                rs = ps.executeQuery();
                while (rs.next()) {
                    concepto = rs.getString(1);
                }
            }

           
        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :obtenerUltimoConceptoTraza. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :obtenerUltimoConceptoTraza." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return concepto;
    }

    public String anularCXPNegocio(String neg, String login) throws SQLException, Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("SQL_ANULAR_CXP_DOC_NEGOCIO"), true);
            st.setString(1, login);
            st.setString(2, login);
            st.setString(3, neg);

            
            sql = st.getSql();

        } catch (SQLException e) {
            throw new SQLException("ERROR AL ANULAR CXP DEL NEGOCIO" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE ANULAR NEGOCIO" + e.getMessage());
                }
            }
        }
        return sql;
    }

    public String obtenerNumCXPNegocio(String neg) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String numero = "";
        String query = "SQL_OBTENER_NUM_CXP_NEGOCIO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, neg);

                rs = ps.executeQuery();
                while (rs.next()) {
                    numero = rs.getString(1);
                }
            }

           
        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :SP_EndosoTercero. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :SP_EndosoTercero." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return numero;
    }

    public String anularCXPItemsDocNegocio(String numCXP) throws Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("SQL_ANULAR_CXP_ITEMS_DOC_NEGOCIO"), true);
            st.setString(1, numCXP);

            sql = st.getSql();

        } catch (SQLException e) {
            throw new SQLException("ERROR AL ANULAR CXP DEL NEGOCIO" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO DE ANULAR NEGOCIO" + e.getMessage());
                }
            }
        }
        return sql;
    }

    public String anularIngresoDifFenalco(String neg) throws SQLException, Exception {
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL("SQL_ANULAR_INGRESO_DIFERIDO_FENALCO"), true);
            st.setString(1, neg);

            sql = st.getSql();

        } catch (SQLException e) {
            throw new SQLException("ERROR AL ANULAR INGRESO DIFERIDO FENALCO" + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO ANULAR INGRESO DIFERIDO" + e.getMessage());
                }
            }
        }
        return sql;
    }

    public String obtenerNegocioAval(String neg) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String numero = "";
        String query = "SQL_OBTENER_NUM_NEGOCIO_AVAL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, neg);

                rs = ps.executeQuery();
                while (rs.next()) {
                    numero = rs.getString(1);
                }
            }

           
        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :obtenerNegocioAval. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :obtenerNegocioAval." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return numero;
    }

    public String obtenerCxPAval(String negAval) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String numero = "";
        String query = "SQL_OBTENER_CXP_AVAL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, negAval);

                rs = ps.executeQuery();
                while (rs.next()) {
                    numero = rs.getString(1);
                }
            }

           
        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :obtenerCxPAval. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :obtenerCxPAval." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return numero;
    }

    public boolean esTransferido(String nit_tercero) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
            String  query = "SQL_VALIDAR_TRANSFERIR_NEGOCIO";
            boolean sw=false;
            try{
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, nit_tercero);

                rs = ps.executeQuery();
                if (rs.next())
                {
                   sw=true;
                }
            }}
            catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return sw;
    }

    public String ingresarCXPNgSeguro(BeanGeneral bg, Convenio convenio, int numCuota, String fechaVen) throws SQLException{
        String query = "SQL_INSERTAR_CXP_SEGUROS";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

                double vs=0;
            int numCuotas = Integer.parseInt(bg.getValor_02());
                if(convenio.isDescuenta_gmf()){
                int max_cuota = Integer.parseInt(convenio.getCuota_gmf()); //Numero max de cuota para descontar gmf

                if(max_cuota >= numCuotas){
                    vs += calcularGMF(bg.getValor_11(),(float) convenio.getPorc_gmf());
                    vs += calcularGMF(bg.getValor_11(),(float) convenio.getPorc_gmf2());
                }
            }
                if(convenio.isDescuenta_aval()){
                vs += Double.valueOf(bg.getValor_15()).doubleValue();
            }
                double vd = Util.redondear(Double.valueOf(bg.getValor_11())/numCuotas,0);
            int res = (int) Math.ceil(Double.valueOf(bg.getValor_11())) % numCuotas;
                if(res != 0){
                    if(numCuota == numCuotas){
                        if((vd * numCuotas) < Double.valueOf(bg.getValor_11())){
                            vd = Double.valueOf(bg.getValor_11()) - (vd * (numCuotas-1));
                        }else{
                            vd = vd - ((vd * numCuotas) - Double.valueOf(bg.getValor_11())) ;
                    }
                }
            }

            st.setString(1, bg.getValor_09()); //proveedor
            st.setString(2, "FAP");//tipo_documento
            st.setString(3, convenio.getPrefijo_cxp()); //documento
            st.setInt(4, numCuota);
            st.setString(5, bg.getValor_09()); //descripcion (Se envia el nit del afiliado para colocarlo en la descripcion)
            st.setString(6, "OP"); //agencia
            st.setString(7, bg.getValor_09()); //banco (Se envia el nit del afiliado)
            st.setString(8, bg.getValor_09()); //sucursal (Se envia el nit del afiliado)
            st.setDouble(9, vd);//vlr_neto
            st.setDouble(10, vd - vs);//vlr_saldo
            st.setDouble(11, vd);//vlr_neto_me
            st.setDouble(12, vd - vs);//vlr_saldo_me
            st.setInt(13, 1);//tasa
            st.setString(14, bg.getValor_04());//usuario creacion
            st.setString(15, bg.getValor_07());//base
            st.setString(16, "PES");//moneda_banco
            st.setString(17, "NEG");//clase_documento_rel
            st.setString(18, "PES");//moneda
            st.setString(19, "NEG");//tipo_documento_rel
            st.setString(20, bg.getValor_06());//documento_relacionado
            st.setString(21, bg.getValor_23()); //handle_code
            st.setString(22, bg.getValor_18());//Distrito
            st.setString(23, this.aprobadorCxPNeg());//aprobador
            st.setString(24, bg.getValor_04());//usuario aprobacion
            st.setString(25, fechaVen);
            sql += st.toString();
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXP[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXPNgSeguro(BeanGeneral bg, Convenio convenio, int numCuota) throws SQLException{
        String query = "INSERTAR_DETALLE_CXP_SEGUROS";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            int numCuotas = Integer.parseInt(bg.getValor_02());
                double vd = Util.redondear(Double.valueOf(bg.getValor_11())/numCuotas,0);
            int res = (int) Math.ceil(Double.valueOf(bg.getValor_11())) % numCuotas;
                if(res != 0){
                    if(numCuota == numCuotas){
                        if((vd * numCuotas) < Double.valueOf(bg.getValor_11())){
                            vd = Double.valueOf(bg.getValor_11()) - (vd * (numCuotas-1));
                        }else{
                            vd = vd - ((vd * numCuotas) - Double.valueOf(bg.getValor_11())) ;
                    }
                }
            }
            st.setString(1, bg.getValor_09()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, convenio.getPrefijo_cxp()); //documento
            st.setInt(4, numCuota);
            st.setString(5, "1"); //item
            st.setString(6, "DESEMBOLSO " + bg.getValor_09()); //descripcion
            st.setDouble(7, vd);//vlr
            st.setDouble(8, vd);//vlr_me
            st.setString(9, bg.getValor_24());//codigo_cuenta
            st.setString(10, bg.getValor_06());//planilla
            st.setString(11, bg.getValor_04());//creation_user
            st.setString(12, bg.getValor_07());//base
            st.setString(13, "AR-" + bg.getValor_09());//AUXILIAR
            st.setString(14, bg.getValor_18());//Distrito
            sql += st.toString();
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXP[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public ArrayList<CmbGeneralScBeans> cargarUnidadesNegocio(String area) throws Exception {
        ArrayList combo = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "CARGAR_UNIDADES_NEGOCIO";
        try {

            con = conectarJNDI(query);
            String sql = this.obtenerSQL(query);

            ps = con.prepareStatement(sql);
            ps.setString(1, area);

            rs = ps.executeQuery();
            combo = new ArrayList();
            while (rs.next()) {
                CmbGeneralScBeans cmb = new CmbGeneralScBeans();
                cmb.setIdCmb(rs.getInt(1));
                cmb.setDescripcionCmb(rs.getString(2));

                combo.add(cmb);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return combo;
    }

    public String ingresarCXCMicrocreditoReestructuracion(BeanGeneral bg, String documentType, int numCuota, ArrayList<DocumentosNegAceptado> liq) throws SQLException {
        String query = "SQL_INSERTAR_CXC";
        StringStatement st = null;
        String sql = "";
        try {
            double central = Double.parseDouble(bg.getValor_03());
            double valor_factura = liq.get(numCuota - 1).getCapital() + liq.get(numCuota - 1).getCapacitacion() + liq.get(numCuota - 1).getSeguro() + central;

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, "FAC"); //tipo documento
            st.setString(2, documentType); //document type para generar el numero de factura
            st.setString(3, String.valueOf(numCuota)); //numero de cuota para generar el numero de factura
            st.setString(4, bg.getValor_01()); //nit
            st.setString(5, bg.getValor_01()); //nit para obtener el codcli
            st.setString(6, bg.getValor_05()); //concepto
            st.setString(7, bg.getValor_12()); //Fecha factura
            st.setString(8, liq.get(numCuota - 1).getFecha()); //fecha_vencimiento
            st.setString(9, documentType); //descripcion(se guarda el document_type)
            st.setString(10, valor_factura + ""); //valor_factura
            st.setInt(11, 1); //valor_tasa
            st.setString(12, "PES"); //moneda
            st.setInt(13, 1); //cantidad_items
            st.setString(14, "CREDITO"); //forma_pago
            st.setString(15, "OP"); //agencia_facturacion
            st.setString(16, "BQ"); //agencia_cobro
            st.setString(17, bg.getValor_04()); //creation_user
            st.setString(18, valor_factura + ""); //valor_facturame
            st.setString(19, valor_factura + ""); //valor_saldo
            st.setString(20, valor_factura + ""); //valor_saldome
            st.setString(21, bg.getValor_06()); //negasoc
            st.setString(22, bg.getValor_07()); //base
            st.setString(23, liq.get(numCuota - 1).getItem()); //num_doc_fen
            st.setString(24, ""); //tipo_ref1
            st.setString(25, ""); //ref1
            st.setString(26, bg.getValor_22()); //cmc
            st.setString(27, bg.getValor_18()); //dstrct
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXCMicrocreditoReestructuracion[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public boolean isCxPAval(String cxp) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_IS_CXP_AVAL";
        boolean sw = false;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, cxp);

                rs = ps.executeQuery();
                if (rs.next()) {
                    sw = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sw;
    }

    public int obtenerCicloFacturacion(String cod_neg) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int num = 0;
        String query = "OBTENER_CICLO_FACTURACION";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, cod_neg);
                rs = st.executeQuery();
                if (rs.next()) {
                    num= rs.getInt("num_ciclo");
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR NumNegocioClienteHoy " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return num;
    }

    public void actualizarCicloAval(String cod_neg, int ciclo) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "ACTUALIZAR_CICLO_NEGOCIO_AVAL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setInt(1, ciclo);
                st.setString(2, cod_neg);

                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR actualizarCicloAval " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    public void actualizarCicloSeguros(String cod_neg, int ciclo) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "ACTUALIZAR_CICLO_NEGOCIOS_SEGURO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setInt(1, ciclo);
                st.setString(2, cod_neg);

                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR actualizarCicloSeguros " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    public void actualizarCicloGps(String cod_neg, int ciclo) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "ACTUALIZAR_CICLO_NEGOCIOS_GPS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setInt(1, ciclo);
                st.setString(2, cod_neg);;

                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR actualizarCicloGps " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    public void actualizarCicloNegocio(String cod_neg, int ciclo) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String query = "ACTUALIZAR_CICLO_NEGOCIOS_PPAL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setInt(1, ciclo);
                st.setString(2, cod_neg);

                st.executeUpdate();

            }
        } catch (SQLException e) {
            throw new SQLException("ERROR actualizarCicloNegocio " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    public boolean permisosGenerarPagare(Negocios neg) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "PERMISO_GENERAR_PAGARE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, neg.getCod_negocio());

                rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getBoolean(1);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR actualizarCicloNegocio " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return false;
    }

    public String estadoReestructuracion(String neg) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "ESTADO_NEGOCIO_REESTRUCTURACION";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, neg);
                st.setString(2, neg);

                rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getString(1);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR actualizarCicloNegocio " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return "";
    }

    public void insert_cxp_banco_transf(String documento, String bank, Double valor_cxp, Usuario usuario) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        String sql = "";
        String query = "";
        if (bank.equals("EFECTY")) {
            query = "SQL_INSERTA_CXP_BANCO_TRANSF_EFECTY";
        } else {
            query = "SQL_INSERTA_CXP_BANCO_TRANSF";
        }
        try {
            con = this.conectarJNDI(query, usuario.getBd());
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, documento);
            ps.setString(2, bank);
            ps.setDouble(3, valor_cxp);
            ps.setString(4, usuario.getLogin());
            ps.executeQuery();
        } catch (Exception e) {
            throw new Exception("Error al insertar cxp banco transferencia... \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        // return sql;
    }

    public String CxCLibranza(String negocios, Usuario usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String respuesta = "";
        String query = "SQL_CXC_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocios);
            ps.setString(2, usuario.getLogin());

            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("respuesta");
            }

        } catch (SQLException e) {
            respuesta = "Error";
            throw new SQLException("ERROR DURANTE perfeccionamiento del negocio " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            return respuesta;
        }

    }

    public double getValor_cxp() {
        return valor_cxp;
    }

    public void setValor_cxp(double valor_cxp) {
        this.valor_cxp = valor_cxp;
    }

    public String esCompraCartera(String negocio, Usuario usuario) throws SQLException {

        /*
         StringStatement st = null;
         String respuesta = "";
         String query = "SQL_PERFECCIONAR_NEGOCIOS_LIBRANZA";
         try {
         //             con = this.conectarJNDI(query);
         //               st = con.prepareStatement(this.obtenerSQL(query));
         query = this.obtenerSQL(query);
         st = new StringStatement(query, true);
         st.setString(1, negocio);
         st.setString(2, usuario.getLogin());
         st.setString(3, negocio);
         respuesta = st.getSql();
             
         } catch (SQLException e) {
         respuesta = "Error";
         throw new SQLException("ERROR DURANTE perfeccionamiento del negocio " + e.getMessage() + " " + e.getErrorCode());
         } finally {
         return respuesta;
         }        
         */
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String Acierta = "";
        String query = "SQL_ES_COMPRACARTERA";
        try {

            con = this.conectarJNDI(query, usuario.getBd());

            if (con != null) {

                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, negocio);

                rs = ps.executeQuery();

                while (rs.next()) {
                    Acierta += rs.getString(1);
                }

            }

           
        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :esCompraCartera. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :esCompraCartera." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return Acierta;
    }

    /**
     * Metodo que trae los negocios pendientes  para la creacion de la cxc de cuota de manejo por fecha de vencimiento
     * @param ciclo
     * @param periodo
     * @return
     * @throws java.lang.Exception
     * @autor.......mcastillo
     * @throws......Exception
     * @version.....1.0.
     **/
    public ArrayList<BeanGeneral> datosNegCuotaManejo(String ciclo, String periodo) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_NEG_CUOTA_MANEJO_CAUSAR_CICLO";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                st.setString(1, ciclo);
                st.setString(2, periodo);

                rs = st.executeQuery();

                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean = this.loadDatosCuotaManejo(rs);
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en datosNegCuotaManejo..." + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    public String updateCuotaManejoCausado(double valor, String fecha, String negocio, String item) throws Exception {
        String query = "SQL_UPDATE_CUOTA_MANEJO";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setDouble(1, valor);
            st.setString(2, fecha);
            st.setString(3, negocio);
            st.setString(4, item);
            sql = st.getSql();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new SQLException("ERROR en updateCuotaMenejoCausado[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public double obtenerValorCuotaManejo(String id_convenio) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double valor = 0;
        String query = "OBTENER_VALOR_CUOTA_MANEJO_CONVENIO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_convenio);
                rs = st.executeQuery();
                if (rs.next()) {
                    valor= rs.getDouble("valor");
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR obtenerValorCuotaManejo " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return valor;
    }

    public ArrayList<BeanGeneral> cuotaManejoPendientes(String negocio) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_NEG_CUOTA_MANEJO_PENDIENTE";
        ArrayList<BeanGeneral> beans = new ArrayList<BeanGeneral>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = this.obtenerSQL(query);
                st = con.prepareStatement(sql);
                st.setString(1, negocio);
                rs = st.executeQuery();

                while (rs.next()) {
                    BeanGeneral bean = new BeanGeneral();
                    bean = this.loadDatosCuotaManejo(rs);
                    bean.setValor_19(rs.getString("cliente"));
                    beans.add(bean);
                }
            }

        } catch (Exception e) {
            System.out.println("error en cuotaManejoPendientes() " + e.toString() + "__" + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return beans;

    }

    public boolean aplicarDeduccionFianza(String cod_neg) throws Exception {

        boolean sw = false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VALIDA_FIANZA_NEGOCIO";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cod_neg);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en aplicarDeduccionFianza [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }


        return sw;

    }

    public double obtenerValorFianza(String id_convenio, double vlr_negocio,  int plazo, String numSolc, int id_producto, int id_cobertura,String nit_empresa) throws Exception {

        double valor = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_VALOR_FIANZA";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numSolc);
            ps.setString(2, id_convenio);
            ps.setInt(3, plazo);
            ps.setDouble(4, vlr_negocio);
            ps.setString(5,nit_empresa);
            ps.setInt(6, id_producto);
            ps.setInt(7, id_cobertura);
           
            rs = ps.executeQuery();
            if (rs.next()) {

                valor = Double.parseDouble(rs.getString("valor_fianza"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en obtenerValorFianza [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }


        return valor;

    }

    public double obtenerValorIVAFianza(String id_convenio, double vlr_negocio,  int plazo) throws Exception {

        double valor = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_VALOR_IVA_FIANZA";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setDouble(1, vlr_negocio);
            ps.setInt(2, plazo);
            ps.setDouble(3, vlr_negocio);
            ps.setString(4, id_convenio);
            ps.setInt(5, plazo);
            rs = ps.executeQuery();
            if (rs.next()) {

                valor = Double.parseDouble(rs.getString("valor_iva"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en obtenerValorIVAFianza [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }


        return valor;

    }

    public String obtenerProveedorFianza(String id_convenio, int plazo) throws Exception {

        String nitProveedor = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GET_INFO_FIANZA";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_convenio);
            ps.setInt(2, plazo);
            rs = ps.executeQuery();
            if (rs.next()) {
                nitProveedor = rs.getString("nit_empresa");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en obtenerProveedorFianza [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return nitProveedor;

    }

    public String ingresarCabeceraCXPMicrocreditoFianza(BeanGeneral bg, String documento, Convenio convenio, String codigo_cuenta) throws SQLException {
        String query = "SQL_INSERTAR_CXP2";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            String nitEmpresaFianza = this.obtenerProveedorFianza(convenio.getId_convenio(),Integer.parseInt(bg.getValor_02()));
            double vd = obtenerValorFianza(convenio.getId_convenio(),Double.parseDouble(bg.getValor_29()),Integer.parseInt(bg.getValor_02()),bg.getValor_30(),Integer.parseInt(bg.getValor_34()),Integer.parseInt(bg.getValor_35()),bg.getValor_33());


            st.setString(1, bg.getValor_01()); //proveedor
            st.setString(2, "FAP");//tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, bg.getValor_01()); //descripcion (Se envia el nit del afiliado para colocarlo en la descripcion)
            st.setString(5, "OP"); //agencia
            st.setString(6, "CAJA FIANZA"); //banco (Se envia el nit del afiliado)
            st.setString(7, "FINTRA"); //sucursal (Se envia el nit del afiliado)
            st.setDouble(8, vd);//vlr_neto
            st.setDouble(9, vd);//vlr_saldo
            st.setDouble(10, vd);//vlr_neto_me
            st.setDouble(11, vd);//vlr_saldo_me
            st.setInt(12, 1);//tasa
            st.setString(13, bg.getValor_04());//usuario creacion
            st.setString(14, bg.getValor_07());//base
            st.setString(15, "PES");//moneda_banco
            st.setString(16, "NEG");//clase_documento_rel
            st.setString(17, "PES");//moneda
            st.setString(18, "NEG");//tipo_documento_rel
            st.setString(19, bg.getValor_06());//documento_relacionado
            st.setString(20, convenio.getHc_cxp_fianza()); //handle_code
            st.setString(21, bg.getValor_18());//Distrito
            st.setString(22, this.aprobadorCxPNeg());//aprobador
            st.setString(23, bg.getValor_04());//usuario aprobacion
            st.setString(24, "NEG");//tipo documento relacionado 1
            st.setString(25, bg.getValor_06());//documento relacionado 1
            st.setString(26, "AGE");//tipo documento relacionado 2
            st.setString(27, convenio.getAgencia());//documento relacionado 2
            st.setString(28, "");//tipo documento relacionado 3
            st.setString(29, "");//documento relacionado 3

            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCabeceraCXPMicrocreditoFianza[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCXPMicrocreditoFianza(BeanGeneral bg, String documento, Convenio convenio, String codigo_cuenta) throws SQLException {
        String query = "INSERTAR_DETALLE_CXP2";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            String nitEmpresaFianza = this.obtenerProveedorFianza(convenio.getId_convenio(),Integer.parseInt(bg.getValor_02()));
            double vd =  obtenerValorFianza(convenio.getId_convenio(),Double.parseDouble(bg.getValor_29()),Integer.parseInt(bg.getValor_02()),bg.getValor_30(),Integer.parseInt(bg.getValor_34()),Integer.parseInt(bg.getValor_35()),bg.getValor_33());
            st.setString(1, bg.getValor_01()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, "1"); //item
            st.setString(5, "DESCUENTO FIANZA " + bg.getValor_01()); //descripcion
            st.setDouble(6, vd);//vlr
            st.setDouble(7, vd);//vlr_me
            st.setString(8, codigo_cuenta);//codigo_cuenta
            st.setString(9, bg.getValor_06());//planilla
            st.setString(10, bg.getValor_04());//creation_user
            st.setString(11, bg.getValor_07());//base
            st.setString(12, "AR-" + bg.getValor_01());//AUXILIAR
            st.setString(13, bg.getValor_18());//Distrito
            st.setString(14, "NEG");//tipo documento relacionado 1
            st.setString(15, bg.getValor_06());//documento relacionado 1
            st.setString(16, "AGE");//tipo documento relacionado 2
            st.setString(17, convenio.getAgencia());//documento relacionado 2
            st.setString(18, "");//tipo documento relacionado 3
            st.setString(19, "");//documento relacionado 3
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXPMicrocreditoFianza[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String insertarTablaControlMicrocreditoFianza(BeanGeneral bg, String documento, String nitEmpresaFianza,String agencia, double valor_cxp, double valor_fianza, double valor_iva) throws SQLException {
        String query = "INSERTAR_HIST_DEDUCCIONES_FIANZA";
        StringStatement st = null;
        String sql = "";
        try {
            double subtotal_fianza = valor_fianza-valor_iva;
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, bg.getValor_18()); //Distrito
            st.setString(2, nitEmpresaFianza); //EMPRESA FIANZA
            st.setString(3, bg.getValor_01()); //PROVEEDOR           
            st.setString(4, documento); //documento relacionado
            st.setString(5, bg.getValor_06()); //Negocio
            st.setString(6, bg.getValor_02()); //No cuotas
            st.setDouble(7, Double.parseDouble(bg.getValor_29()));//vlr negocio
            st.setDouble(8, valor_cxp);//vlr cxp
            st.setDouble(9, subtotal_fianza);//subtotal fianza
            st.setDouble(10, valor_iva);//vlr iva
            st.setDouble(11, valor_fianza);//vlr fianza     
            st.setInt(12, 1);//unidad negocio
            st.setString(13, bg.getValor_17());//convenio         
            st.setString(14, bg.getValor_04());//creation_user
            st.setString(15,agencia);//agencia
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXPMicrocreditoFianza[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public boolean existeValorFianza(String id_convenio, int plazo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SQL_GET_INFO_FIANZA";
        boolean resp = false;

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_convenio);
            ps.setInt(2, plazo);

            rs = ps.executeQuery();
            while (rs.next()) {
                resp = true;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    public String[] buscarCuentasFianza(String codigo, int id_convenio) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "BUSCAR_DATOS_CXP_FIANZA";
        String[] datos = new String[3];
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codigo);
            ps.setInt(2, id_convenio);
            rs = ps.executeQuery();
            if (rs.next()) {
                Array cuentas = rs.getArray(1);
                datos = (String[])cuentas.getArray();                          
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarCuentasFianza[NegociosDAO] "+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return datos;
    }

    public String UpCPFianza(String a) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "UP_SERIES_FIANZA";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2 
                ps.setString(1, a);
                rs = ps.executeQuery();
                if (rs.next()) {
                    ret = rs.getString(1);
                }
            }
      }catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return ret;
    }

    public String obtenerUltimoPagareVigenteCliente(String negocio, String nit_solicitante, String nit_codeudor, String nit_estudiante, String nit_afiliado) throws Exception {

        String num_pagare = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_ULTIMO_PAGARE_VIGENTE";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            ps.setString(2, nit_solicitante);
            ps.setString(3, nit_codeudor);
            ps.setString(4, nit_estudiante);
            ps.setString(5, nit_afiliado);
            rs = ps.executeQuery();
            if (rs.next()) {
                num_pagare = rs.getString("num_pagare");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en obtenerUltimoPagareVigenteCliente [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return num_pagare;

    }

    public void insertarHistoricoPagare(Negocios neg, String nit_solicitante, String nit_codeudor, String nit_estudiante, String pagare, String solicitud, String nit_afiliado, Usuario usuario) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INSERTAR_HISTORICO_PAGARES";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2 
                ps.setInt(1, neg.getId_convenio());
                ps.setString(2, nit_solicitante);
                ps.setString(3, nit_codeudor);
                ps.setString(4, nit_estudiante);
                ps.setString(5, pagare);
                ps.setString(6, neg.getCod_negocio());
                ps.setString(7, neg.getCod_negocio());
                ps.setInt(8, Integer.parseInt(solicitud));
                ps.setString(9, usuario.getLogin());
                ps.setString(10, nit_afiliado);
                ps.executeUpdate();

            }
      }catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
                }

    public String obtenerMailEnvioCorreoAprobCredito(String num_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_CORREO_ENVIO_INFO_SOLICITUD";
        String  correo = "";

        try{
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            while(rs.next()){
                correo = rs.getString("email");
            }

        }catch (Exception e) {         
            System.out.println("Error en getCorreosToSendStandBy: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getCorreosToSendStandBy: " + e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return correo;
        }
    }

    public JsonObject getInfoCuentaEnvioCorreoAprob() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GET_INFO_CORREO_APROBACION_CREDITO";
        JsonObject obj = null;

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("servidor", rs.getString("servidor"));
                fila.addProperty("puerto", rs.getString("puerto"));
                fila.addProperty("usuario", rs.getString("usuario"));
                fila.addProperty("clave", rs.getString("clave"));
                fila.addProperty("bodyMessage", rs.getString("bodyMessage"));
            }
            obj = fila;

        } catch (Exception e) {
            System.out.println("Error en getInfoCuentaEnvioCorreoAprob: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getInfoCuentaEnvioCorreoAprob: " + e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return obj;
        }
    }

    public void insertarNuevoPagare(Negocios neg, String solicitud, Usuario usuario) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INSERTAR_NUEVO_PAGARE_TABLA_CONTROL";
        String ret = "";
        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2 
                ps.setInt(1, Integer.parseInt(solicitud));
                ps.setString(2, neg.getCod_negocio());
                ps.setString(3, neg.getNum_pagare());
                ps.setString(4, usuario.getLogin());
                ps.executeUpdate();
            }
      }catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
                }

    public String obtenerTipoCliente(String identificacion) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "OBTENER_TIPO_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, identificacion);

                rs = ps.executeQuery();
                if (rs.next()) {
                    return rs.getString(1);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR obtenerTipoCliente " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return "";
    }

    public String obtenerUltimaFechaPago(String identificacion) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "OBTENER_INFO_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, identificacion);

                rs = ps.executeQuery();
                if (rs.next()) {
                    return rs.getString("fecha_pago");
                }
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR obtenerUltimaFechaPago " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return (com.tsp.util.Util.AnoActual() + "-" + com.tsp.util.Util.getFechaActual_String(3) + "-" + com.tsp.util.Util.getFechaActual_String(5));
    }

    public ArrayList obtenerValorPimeraCuota(String cod_neg) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_VALOR_PRIMERA_CUOTA";
        ArrayList lista = new ArrayList();
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cod_neg);
            rs = ps.executeQuery();
            while (rs.next()) {
                Negocios neg = new Negocios();
                neg.setPrimera_cuota(rs.getDouble("valor"));
                neg.setPrimera_fecha_Vencimiento(rs.getString("fecha_vencimiento"));

                lista.add(neg);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarDetallesNegocio[NegociosDAO] "+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return lista;
    }

    /**
     * Busca la informacion de un preprobado
     * @param identificacion
     * @return
     */
    public BeanGeneral buscarPreaprobadoForm(String identificacion) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_PREAPROBADO_FORM";
        BeanGeneral bg =new BeanGeneral();
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, identificacion);
            rs = ps.executeQuery();
            if (rs.next()) {
                bg.setValor_01(rs.getString("cedula_deudor"));
                bg.setValor_02(rs.getString("cod_neg"));
                bg.setValor_03(rs.getString("nombre_deudor"));
                bg.setValor_04(rs.getString("vr_negocio"));
                bg.setValor_05(rs.getString("valor_preaprobado"));
                bg.setValor_06(rs.getString("al_dia"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return bg;
    }

    public String crearNcConsumo(BeanGeneral bg, String documento, double valor) throws SQLException {

        String query = "SQL_CREAR_NC_CONSUMO_FINTRA";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
             
         
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, bg.getValor_09());
            st.setString(2, documento);
            st.setString(3, bg.getValor_09());
            st.setString(4, bg.getValor_23());
            st.setDouble(5, valor);//vr calculado
            st.setDouble(6, valor);//vr calculado
            st.setDouble(7, valor);//vr calculado
            st.setDouble(8, valor);//vr calculado
            st.setString(9, bg.getValor_04());//usuario crea
            st.setString(10, documento);//Codigo de la factura
            st.setString(11, bg.getValor_18());//Codigo de la factura
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crearNcConsumo[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

   
    public String crearNcDetalleConsumo(BeanGeneral bg, String documento,double valor) throws SQLException {
        String query = "SQL_CREAR_NC_DETALLE_CONSUMO_FINTRA";
        StringStatement st = null;
        Connection con = null;
        String sql = ""; 
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
                     
      
            st = new StringStatement(this.obtenerSQL(query), true);
                              
            
            st.setString(1, bg.getValor_09()); //PROVEEDOR
            st.setString(2, "NC"); //tipo_documento
            st.setString(3, documento); //documento
            st.setString(4, "001"); //item
            st.setString(5, "PASIVO PUENTE CONSUMO FINTRA " + bg.getValor_09()); //descripcion           
            st.setDouble(6, valor );//vlr
            st.setDouble(7, valor );//vlr_me
            st.setInt(8, 23150102);//codigo_cuenta
            st.setString(9, bg.getValor_04());//creation_user
            st.setString(10, bg.getValor_07());//base
            st.setString(11, bg.getValor_18());//Distrito
            sql += st.toString();
            sql = st.getSql();
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crearNcDetalleConsumo[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        
        return sql;
    }
       
    public String crearIngresoConsumo(BeanGeneral bg, String serialingreso,double valor) throws SQLException {

        String query = "SQL_INSERTAR_INGRESO_CONSUMO";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        try {               
         
            st = new StringStatement(this.obtenerSQL(query), true);

           
            st.setString(1,serialingreso );//NUMERO DOCUMENTO
            st.setString(2, bg.getValor_09());//NIT AFILIADO
            st.setString(3, bg.getValor_09());//NIT AFILIADO
            st.setDouble(4, valor);//vr calculado
            st.setDouble(5, valor);//vr calculad
            st.setString(6, bg.getValor_04());//usuario crea
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crear crearIngresoConsumo[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
        
    }

   
    public String crearIngresoDetalleConsumo(BeanGeneral bg, String serialingreso,double valor,String serialCXC) throws SQLException {
        String query = "SQL_INSERTAR_INGRESO_DETALLE_CONSUMO";
        StringStatement st = null;
        Connection con = null;
        String sql = "";  
        PreparedStatement ps = null;
        ResultSet rs = null;
     
        try {
                 
        
        st = new StringStatement(this.obtenerSQL(query), true);
                                       
            
            
            st.setString(1, serialingreso); //documento
            st.setString(2, bg.getValor_09()); //NIT
            st.setDouble(3, valor);//vlr
            st.setDouble(4, valor);//vlr_me
            st.setString(5, serialCXC);//codigo_cuenta
            st.setString(6, serialCXC);//planilla
            st.setString(7, bg.getValor_04());//creation_user
            st.setDouble(8, valor);//saldofact
            
            sql += st.toString();
            sql = st.getSql();
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crear crearIngresoDetalleConsumo[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    
    public String crearCxcConsumo(BeanGeneral bg, String serialCXC,double valor) throws SQLException {

        String query = "SQL_INSERTAR_CXC_CONSUMO";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {       
         
            st = new StringStatement(this.obtenerSQL(query), true);
           
                st.setString(1, serialCXC);
                st.setString(2, bg.getValor_09());
                st.setString(3, bg.getValor_09());
                st.setDouble(4, valor);
                st.setString(5, bg.getValor_04());
                st.setDouble(6, valor);
                st.setDouble(7, valor);
                st.setDouble(8, valor);
                st.setString(9,bg.getValor_06());
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crear crearCxcConsumo[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    public String crearCxcDetalleConsumo(BeanGeneral bg, String serialCXC,double valor) throws SQLException {

        String query = "SQL_INSERTAR_DET_CXC_CONSUMO";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        double vt=0;
        double vtotal=0;
        try {               
        
         vtotal=valor/1.19;
        
            st = new StringStatement(this.obtenerSQL(query), true);
           
                st.setString(1, serialCXC);
                st.setString(2, bg.getValor_09());                
                st.setString(3, "COMISION CONSUMO FINTRA");
                st.setDouble(4, vtotal);
                st.setDouble(5, vtotal);
                st.setString(6, bg.getValor_04());
                st.setDouble(7, vtotal);
                st.setDouble(8, vtotal);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crear CxcDetalleConsumo[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    
    public String crearCxcDetalleConsumoIVA(BeanGeneral bg, String serialCXC,double valor) throws SQLException {

        String query = "SQL_INSERTAR_DET_CXC_CONSUMO_IVA";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        double vt = 0; 
         double vtotal=0;
        try {              
        
         vt=valor/1.19;
         
         vtotal=valor-vt;
        
            st = new StringStatement(this.obtenerSQL(query), true);
           
                st.setString(1, serialCXC);
                st.setString(2, bg.getValor_09());
                st.setString(3, "IVA TARIFA GENERAL CONSUMO FINTRA");
                st.setDouble(4, vtotal);
                st.setDouble(5, vtotal);
                st.setString(6, bg.getValor_04());
                st.setDouble(7, vtotal);
                st.setDouble(8, vtotal);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crearCxcDetalleConsumoIVA[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    
    public String updateCxpConsumo(BeanGeneral bg, String documento,double valor) throws SQLException {

        String query = "SQL_UPDATE_CXP_CONSUMO";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        double procentaje = 0;
        double vt = 0; 
         double vtotal=0;
        try {               
            st = new StringStatement(this.obtenerSQL(query), true);
                          
                st.setDouble(1, valor);
                st.setDouble(2, valor);
                st.setDouble(3, valor);
                st.setDouble(4, valor);
                st.setString(5, documento);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crear updateCxpConsumo[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    public String updateCxcConsumo(BeanGeneral bg, String serialCXC,double valor) throws SQLException {

        String query = "SQL_UPDATE_CXC_CONSUMO";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null; 
        double vt = 0; 
        try { 
         
            st = new StringStatement(this.obtenerSQL(query), true);
           
                st.setDouble(1, valor);
                st.setDouble(2, valor);
                st.setDouble(3, valor);
                st.setDouble(4, valor);
                st.setString(5, serialCXC);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crear updateCxcConsumo[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    
    public String serialingreso() throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_SERIE_INGRESO";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2 
                rs = ps.executeQuery();
                if (rs.next()) {
                    ret = rs.getString("serie");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return ret;
    }
    
    public String serialCxc() throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_SERIE_CXC";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2 
                rs = ps.executeQuery();
                if (rs.next()) {
                    ret = rs.getString("serie");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return ret;
    }
    
    public String updateserialcxc() throws SQLException {

        String query = "SQL_UPDATE_SERIE_CXC";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crear updateCxcConsumo[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    public String updateserialingreso() throws SQLException {

        String query = "SQL_UPDATE_SERIE_INGRESO";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en crear updateCxcConsumo[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    
    public String deducciones(Convenio convenio) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DEDUCCIONES";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
        if (con != null) {
        ps = con.prepareStatement(this.obtenerSQL(query));
        ps.setString(1, convenio.getId_convenio()); 
        rs = ps.executeQuery();            
        if (rs.next()){                                            
         ret = rs.getString("perc_cobrar");
         
         }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return ret;
    }
    
    public boolean validarcompracartera(String form) throws Exception {

        boolean sw = false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VALIDA_COMPRA_CARTERA";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, form);
            rs = ps.executeQuery();
            if (rs.next()) {
                sw = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en validarcompracartera [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return sw;
    }
    
    public String GenerarDocumentosCompraCartera(BeanGeneral bg, int convenio) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String idconvenio = Integer.toString(convenio);
        String result="";
        String respuesta="";
        String query = "SQL_GENERAR_DOCUMENTOS_COMPRA_CARTERA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, bg.getValor_06());
            ps.setString(2, idconvenio);
            ps.setString(3, bg.getValor_04()); 
            rs = ps.executeQuery();
            if (rs.next()){
                result=rs.getString("RESULTADO");
              
            }
                   
        } 
//            catch (Exception e) {
//            e.printStackTrace();
//            respuesta = "{\"respuesta\":\"" + e.getMessage() + "\"}";  
//        } 
            finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            
           
        }
       
            return result;
        
        
        
        
    }
    
    public String perfeccionarMicrocredito(String negocio,Usuario usuario) throws SQLException {
         //Connection con = null;
        //PreparedStatement st = null;
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_PERFECCIONAR_NEGOCIOS_LIBRANZA";
        try {
//             con = this.conectarJNDI(query);
//               st = con.prepareStatement(this.obtenerSQL(query));
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, negocio);
            st.setString(2, usuario.getLogin());
            st.setString(3, negocio);
            respuesta = st.getSql();

        } catch (SQLException e) {
            respuesta = "Error";
            throw new SQLException("ERROR DURANTE perfeccionamiento del negocio " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            return respuesta;
        }

    }
    
     public String perfeccionarNegocioMicrocredito(String negocio, Usuario usuario) throws SQLException {
//        Connection con = null;
//        PreparedStatement st = null;
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_PERFECCIONAR_NEGOCIOS_LI";
        try {
//             con = this.conectarJNDI(query);
//             st = con.prepareStatement(this.obtenerSQL(query));
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, negocio);
            respuesta = st.getSql();

        } catch (SQLException e) {
            respuesta = "Error";
            throw new SQLException("ERROR DURANTE perfeccionamiento del negocio " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            return respuesta;
        }
    }
     
     public String marcarCXPGeneradoChequeMicro(String cxp,String negocios,Usuario usuario) throws SQLException {
        Connection con = null;
        // PreparedStatement st = null;
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_MARCAR_CXP_NEGOCIOS_LIBRANZA";
        try {
             //con = this.conectarJNDI(query);
            // st = con.prepareStatement(this.obtenerSQL(query));
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, cxp);
            st.setString(3, negocios);
            respuesta = st.getSql();
            //st.executeUpdate();

        } catch (SQLException e) {
            respuesta = "Error";
            throw new SQLException("ERROR DURANTE perfeccionamiento del negocio " + e.getMessage() + " " + e.getErrorCode());
        } finally {
            return respuesta;
        }

    }
     
     public String uptdatefehadesem(String negocio) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_FECHA_TRANSF";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio);
            
            ps.executeUpdate();
            respuesta = "Actualizado";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta =  e.getMessage();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuesta;
        }
    }
     
     public String InsertHistoricoDeduccionesFianza(BeanGeneral bg,Negocios neg, String documento, Convenio convenio) throws SQLException {
        String query = "SQL_INSERTAR_CXP_HISTORICO_DEDUCCIONES";
        StringStatement st = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            double vd = Double.valueOf(bg.getValor_29());
            st.setString(1, bg.getValor_30()); //Periodo Corte
            st.setString(2, bg.getValor_01()); //Empresa Fianza
            st.setString(3, neg.getCod_cli()); //Identificacion cliente
            st.setString(4, documento); //Documento Relacionado      
            st.setString(5, neg.getCod_negocio()); //Codigo Negocio
            st.setInt(6, neg.getNodocs()); //Numero Cuotas
            st.setDouble(7, neg.getVr_negocio()); //Valor Negocio
            st.setDouble(8, neg.getVr_desem());//Valor desembolso            
            st.setDouble(9, vd);//Subtotal Fianza            
            st.setDouble(10, vd);//Valor iva
            st.setDouble(11, vd);//Valor iva
            st.setDouble(12, vd);//Total fianza
            st.setString(13, bg.getValor_30());//Fecha vencimiento 
            st.setString(14, neg.getCod_negocio());//Codigo Negocio            
            st.setInt(15, neg.getId_convenio());//tasa
            st.setString(16, bg.getValor_04());//usuario creacion
            st.setString(17, convenio.getAgencia());//Agencia            
            
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCXPMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
     }
     
     
     public String generar_Nc_Y_Cxps_Polizas(Negocios negocio,Usuario usuario) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String result="";
        String query = "SQL_GENERAR_NC_Y_CXP_POLIZAS";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, negocio.getCod_negocio());
            ps.setString(2, usuario.getLogin()); 
            ps.setString(3, negocio.getCod_cli());
            rs = ps.executeQuery();
            if (rs.next()){
                result=rs.getString("resultado");
              
            }
                   
//        } catch (Exception e) {
//            e.printStackTrace();
//            result=e.toString();
          //  result = "{\"respuesta\":\"" + e.getMessage() + "\"}";  
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            
           
        }
       
            return result;
        
        }

    public double obtenerValorPoliza(int id_convenio, String id_Sucursal, double capital, int nodocs, String fechaItem, String tipo_cuota, String agencia, String fechaBaseRenovacion, String renovacion, String codigo,Usuario usuario) throws Exception {
      Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        double result = 0;
        String query = "SQL_OBTENER_VALOR_POLIZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, id_convenio);
            ps.setString(2, id_Sucursal); 
            ps.setDouble(3, capital);
            ps.setInt(4, nodocs);
            ps.setString(5, fechaItem);
            ps.setString(6, tipo_cuota);
            ps.setString(7, agencia);
            ps.setString(8, fechaBaseRenovacion);
            ps.setString(9, renovacion);
            ps.setString(10, codigo);
            ps.setString(11, usuario.getLogin());
            rs = ps.executeQuery();
            if (rs.next()){
                result=rs.getDouble("resultado");
              
            }
                   
        } catch (Exception e) {
            throw new Exception("Error en obtenerValorPoliza [NegociosDAO] " + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            
           
        }
       
            return result;
    }

    public double total_valor_poliza(String numSolc) throws Exception {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        double result = 0;
        String query = "SQL_OBTENER_VALOR_TOTAL_POLIZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numSolc);
            rs = ps.executeQuery();
            if (rs.next()){
                result=rs.getDouble("total");
              
            }
                   
        } catch (Exception e) {
            throw new Exception("Error en total_valor_poliza [NegociosDAO] " + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            
           
        }
       
            return result;
    }

    public double obtener_valor_poliza_financiada(String numSolc) throws Exception {
      Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        double result = 0;
        String query = "SQL_OBTENER_VALOR_POLIZA_FINANCIADA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numSolc);
            rs = ps.executeQuery();
            if (rs.next()){
                result=rs.getDouble("total");
              
            }
                   
        } catch (Exception e) {
            throw new Exception("Error en total_valor_poliza [NegociosDAO] " + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            
           
        }
       
            return result;
    }

    public String anular_documentos_educativo_renovado(SolicitudAval solicitud, String usuario) {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String result = "";
        String query = "SQL_ANULAR_DOCUMENTOS_EDUCATIVO_RENOVADO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, solicitud.getNumeroSolicitud());
            ps.setString(2, usuario);
            rs = ps.executeQuery();
            if (rs.next()){
                result=rs.getString("resultado");
              
            }
                   
        } catch (Exception e) {
            System.out.println("Error en total_valor_poliza [NegociosDAO] " + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            
           
        }
       
            return result;
    }

    public String ingresarCxpRenovacion(BeanGeneral bg, String num_cxp_renovacion, Convenio convenio, Negocios neg) throws SQLException {
        String query = "SQL_INSERTAR_CXP_RENOVACION";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
              
            st.setString(1, neg.getCod_cli()); //proveedor
            st.setString(2, "FAP");//tipo_documento
            st.setString(3, num_cxp_renovacion); //documento
            st.setString(4,  "CXP RENOVACION CREDITO "+neg.getCod_cli()); //descripcion (Se envia el nit del afiliado para colocarlo en la descripcion)
            st.setString(5, "OP"); //agencia
            st.setString(6, bg.getValor_09()); //banco (Se envia el nit del afiliado)
            st.setString(7, bg.getValor_09()); //sucursal (Se envia el nit del afiliado)
            st.setDouble(8, neg.getValor_renovacion());//vlr_neto
            st.setDouble(9, neg.getValor_renovacion());//vlr_saldo
            st.setDouble(10, neg.getValor_renovacion());//vlr_neto_me
            st.setDouble(11, neg.getValor_renovacion());//vlr_saldo_me
            st.setInt(12, 1);//tasa
            st.setString(13, bg.getValor_04());//usuario creacion
            st.setString(14, bg.getValor_07());//base
            st.setString(15, "PES");//moneda_banco
            st.setString(16, "NEG");//clase_documento_rel
            st.setString(17, "PES");//moneda
            st.setString(18, "NEG");//tipo_documento_rel
            st.setString(19, bg.getValor_06());//documento_relacionado
            st.setString(20, bg.getValor_23()); //handle_code
            st.setString(21, bg.getValor_18());//Distrito
            st.setString(22, this.aprobadorCxPNeg());//aprobador
            st.setString(23, bg.getValor_04());//usuario aprobacion
            sql += st.toString();
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarCxpRenovacion[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String ingresarDetalleCxpRenovacion(BeanGeneral bg, String num_cxp_renovacion, Convenio convenio, Negocios neg) throws SQLException {
       String query = "INSERTAR_DETALLE_CXP_RENOVACION";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            
            st.setString(1, neg.getCod_cli()); //PROVEEDOR
            st.setString(2, "FAP"); //tipo_documento
            st.setString(3, num_cxp_renovacion); //documento
            st.setString(4, "1"); //item
            st.setString(5, "CXP RENOVACION CREDITO "+neg.getCod_cli()); //descripcion
            st.setDouble(6, neg.getValor_renovacion());//vlr
            st.setDouble(7, neg.getValor_renovacion());//vlr_me
            st.setString(8, bg.getValor_24());//codigo_cuenta
            st.setString(9, bg.getValor_06());//planilla
            st.setString(10, bg.getValor_04());//creation_user
            st.setString(11, bg.getValor_07());//base
            st.setString(12, "AR-" + bg.getValor_09());//AUXILIAR
            st.setString(13, bg.getValor_18());//Distrito
            sql += st.toString();
            sql = st.getSql();
        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR en ingresarDetalleCXP[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    
     public ArrayList<BeanGeneral> cargarNegociosArchivoPlano(String negocio, String banco) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_NEGOCIOS_TRANSFERIDOS";
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            if(negocio.equals("7")){
                sql = sql.replace("#negocios#", "n.cod_neg like 'NG%'");
            }else if(negocio.equals("0")){
                sql = sql.replace("#negocios#", "n.cod_neg like 'NG%'");
           }else{
                sql = sql.replace("#negocios#", "n.cod_neg not like 'NG%'");
            }

            if((Integer.parseInt(negocio) == 1) && (banco.toUpperCase().contains("CAJA "))){
                sql = sql.replace("#cond#", " AND (p.tipo_cuenta = '' OR p.no_cuenta='0001')");
            }else{
                sql = sql.replace("#cond#", "");
            }

            if (con != null) {
                ps = con.prepareStatement(sql);//JJCastro fase2
                if(negocio.equals("0")){
                    ps.setString(1, "13");
                    //ps.setString(2, "13");
                }else{
                    ps.setString(1, negocio);
                    //ps.setString(2, negocio);
                }
                System.out.println(ps.toString());
                rs = ps.executeQuery();
                while (rs.next()) {
                    BeanGeneral bg = new BeanGeneral();
                    bg.setValor_01(rs.getString(1));
                    bg.setValor_02(rs.getString(2));
                    bg.setValor_03(rs.getString(3));
                    bg.setValor_04(rs.getString(4));
                    bg.setValor_05(rs.getString(5));
                    bg.setValor_06(rs.getString(6));
                    bg.setValor_07(rs.getString(7));
                    bg.setValor_08(rs.getString(8));
                    bg.setValor_09(rs.getString(9));
                    bg.setValor_10(rs.getString(10));
                    bg.setValor_11(rs.getString(11));
                    bg.setValor_12(rs.getString(12));
                    bg.setValor_13(rs.getString(13));
                    bg.setValor_15(rs.getString(14));
                    bg.setValor_16(rs.getString(15));
                    bg.setValor_17(rs.getString(16));
                    lista.add(bg);

                }

            }

           
        } catch (Exception ex) {
            System.out.println("error al ejecutar la funcion :cargarNegocios. " + ex.toString() + "__" + ex.getMessage());
            throw new SQLException("error al ejecutar la funcion :cargarNegocios." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return lista;

    }

    public void liquidarNegocioMicrocredito(Negocios negocio, SolicitudAval solicitud, Convenio convenio) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "LIQUIDAR_NEGOCIO_MICRO";
        String sql = "";
        String cc="N";
        
        ArrayList<DocumentosNegAceptado> items = new ArrayList();
        DocumentosNegAceptado item = null;
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            if (con != null) {
                
                if (solicitud.isCompraCartera()){
                    cc="S";
                }
                 String fecha = Util.fechaMasDias(negocio.getFecha_liquidacion(), Integer.parseInt(negocio.getFpago()));
                 ps = con.prepareStatement(sql);
                 ps.setDouble(1, negocio.getVr_negocio());
                 ps.setInt(2, negocio.getNodocs());
                 ps.setString(3, fecha);
                 ps.setString(4, convenio.getAgencia());
                 ps.setString(5,cc);
                 ps.setString(6, solicitud.getNumeroSolicitud());
                 ps.setString(7, solicitud.getNit_fondo());
                 ps.setInt(8, solicitud.getProducto_fondo());
                 ps.setInt(9, solicitud.getCobertura_fondo());
                rs = ps.executeQuery();
                while (rs.next()) {
                    item = new DocumentosNegAceptado();
                    item.setItem(rs.getString("item"));
                    item.setFecha(rs.getString("fecha"));
                    item.setDias(rs.getString("dias"));
                    item.setSaldo_inicial(rs.getDouble("saldo_inicial"));
                    item.setValor(rs.getDouble("valor"));
                    item.setCapital(rs.getDouble("capital"));
                    item.setInteres(rs.getDouble("interes"));
                    item.setCat(rs.getDouble("cat"));
                    item.setSeguro(rs.getDouble("seguro"));
                    item.setCuota_manejo(rs.getDouble("cuota_manejo"));
                    item.setSaldo_final(rs.getDouble("saldo_final"));
                    item.setValor_aval(rs.getDouble("valor_aval"));
                    item.setCapital_aval(rs.getDouble("capital_aval"));
                    item.setInteres_aval(rs.getDouble("interes_aval"));
                    item.setAval(rs.getDouble("aval"));
                    items.add(item);
                }
                this.setLiquidacion(items);
            }

        } catch (Exception ex) {
            throw new SQLException("error al ejecutar :liquidarNegocioMicrocredito." + ex.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

    }
    
     public double obtenerPorcFianza(String id_convenio, String nit,  int plazo, int id_producto, int id_cobertura) throws Exception {

        double valor = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_PORC_AVAL_CLIENTE";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_convenio);
            ps.setInt(2, id_producto);
            ps.setInt(3, id_cobertura);
            ps.setString(4, nit);
            ps.setInt(5, plazo);           
            rs = ps.executeQuery();
            if (rs.next()) {

                valor = Double.parseDouble(rs.getString("porcentaje_aval_clie"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en obtenerValorFianza [NegociosDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return valor;

    }

    public String crearNcRetanqueoMicro(BeanGeneral bg, String documento, String valor_retanqueo,String serialNc) throws Exception {
       String query = "SQL_CREAR_NC_RETANQUEO_MICRO";
        StringStatement st = null;
        Connection con = null;
        String sql = "";
        try {
                      
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, bg.getValor_09());
            st.setString(2, serialNc);
            st.setString(3, bg.getValor_09());
            st.setString(4, bg.getValor_23());
            st.setDouble(5, Double.parseDouble(valor_retanqueo));//vr calculado
            st.setDouble(6, Double.parseDouble(valor_retanqueo));//vr calculado
            st.setDouble(7, Double.parseDouble(valor_retanqueo));//vr calculado
            st.setDouble(8, Double.parseDouble(valor_retanqueo));//vr calculado
            st.setString(9, bg.getValor_04());//usuario crea
            st.setString(10, documento);//Codigo de la factura
            st.setString(11, bg.getValor_18());//Codigo de la factura
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("ERROR en crearNcRetanqueoMicro[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }

    public String crearDetalleNcRetanqueoMicro(BeanGeneral bg, String documento, String valor_retanqueo, String serialNc) throws Exception{
        
        String query = "SQL_CREAR_DETALLE_NC_RETANQUEO_MICRO";
        StringStatement st = null;
        Connection con = null;
        String sql = ""; 
        try {
                           
            st = new StringStatement(this.obtenerSQL(query), true);
             
            st.setString(1, bg.getValor_09()); //PROVEEDOR
            st.setString(2, "NC"); //tipo_documento
            st.setString(3, serialNc); //documento
            st.setString(4, "001"); //item
            st.setString(5, "NC RETANQUEO MICRO " + bg.getValor_09()); //descripcion           
            st.setDouble(6, Double.parseDouble(valor_retanqueo) );//vlr
            st.setDouble(7, Double.parseDouble(valor_retanqueo) );//vlr_me
            st.setString(8, bg.getValor_04());//creation_user
            st.setString(9, bg.getValor_07());//base
            st.setString(10, bg.getValor_18());//Distrito
            sql += st.toString();
            sql = st.getSql();
            
        } catch (Exception e) {
            e.printStackTrace();
             throw new Exception("ERROR en crearDetalleNcRetanqueoMicro[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        
        return sql;
       
    }
    
    public String anular_documentos_micro_retanqueo(SolicitudAval solicitud, String usuario) {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String result = "";
        String query = "SQL_REVERSAR_DOCUMENTOS_RETANQUEO_MICRO";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, solicitud.getNumeroSolicitud());
            ps.setString(2, usuario);
            rs = ps.executeQuery();
            if (rs.next()){
                result=rs.getString("resultado");
              
            }
                   
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            
           
        }
       
            return result;
    }

    public String updateCxpMicrocredito(BeanGeneral bg, String documento, String valor_renovacion)throws Exception {
       String query = "SQL_UPDATE_CXP_MICROCREDITO";
        StringStatement st = null;
        Connection con = null;
        String sql = "";       
        try {               
            st = new StringStatement(this.obtenerSQL(query), true);
                          
                st.setDouble(1, Double.parseDouble(valor_renovacion));
                st.setDouble(2, Double.parseDouble(valor_renovacion));
                st.setDouble(3, Double.parseDouble(valor_renovacion));
                st.setDouble(4, Double.parseDouble(valor_renovacion));
                st.setString(5, documento);
            sql = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("ERROR en updateCxpMicrocredito[NegociosDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        }
        return sql;
    }
    
    public String serialNcMicro(String document_type) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SERIAL_NC_MICRO";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, document_type);
                rs = ps.executeQuery();
                if (rs.next()) {
                    ret = rs.getString(1);
                }
            }
      }catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return ret;
    }
    
     /**
     * Busca los detalles de las cuotas de un negocio
     * @param codNegocio codigo del negocio a buscar
     * @param tipo_liquidacion
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList buscarDetallesNegocio(String codNegocio, String tipo_liquidacion) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LIQUIDACION_NEGOCIOS";
        ArrayList lista = new ArrayList();
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codNegocio);
            ps.setString(2, tipo_liquidacion);
            rs = ps.executeQuery();
            while (rs.next()) {
                DocumentosNegAceptado doc = new DocumentosNegAceptado();
                doc.setCod_neg(rs.getString("cod_neg"));
                doc.setItem(rs.getString("item"));
                doc.setFecha(rs.getString("fecha"));
                doc.setDias(rs.getString("dias"));
                doc.setSaldo_inicial(rs.getDouble("saldo_inicial"));
                doc.setCapital(rs.getDouble("capital"));
                doc.setInteres(rs.getDouble("interes"));
                doc.setValor(rs.getDouble("valor"));
                doc.setSaldo_final(rs.getDouble("saldo_final"));
                doc.setNo_aval((rs.getDouble("no_aval")));
                doc.setCapacitacion(rs.getDouble("capacitacion"));
                doc.setSeguro(rs.getDouble("seguro"));
                doc.setCat(rs.getDouble("cat"));
                doc.setRemesa(rs.getDouble("remesa"));
                doc.setCustodia(rs.getDouble("custodia"));
                doc.setCuota_manejo(rs.getDouble("cuota_manejo"));
                doc.setCapital_Poliza(rs.getDouble("capital_poliza"));
                doc.setValor_aval(rs.getDouble("valor_aval"));
                doc.setCapital_aval(rs.getDouble("capital_aval"));
                doc.setInteres_aval(rs.getDouble("interes_aval"));
                doc.setAval(rs.getDouble("aval"));
                lista.add(doc);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarDetallesNegocio[NegociosDAO] "+e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }
        return lista;
    }

    public String serialNegocios(String prefijo)throws Exception {
       Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "GENERAR_COD_NEGOCIO";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2 
                ps.setString(1, prefijo);
                rs = ps.executeQuery();
                if (rs.next()) {
                    ret = rs.getString(1);
                }
            }
      }catch (Exception ex)
            {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
                }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return ret;
    }

    public double obtenerTir(int numcuotas,double tcuota,double valornegocio,String fechaliqui,String fechaPrimeraCuota) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        double result = 0;
        String query = "OBTENER_TIR";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, numcuotas);
            ps.setDouble(2,tcuota);
            ps.setDouble(3,valornegocio);
            ps.setString(4,fechaliqui);
            ps.setString(5,fechaPrimeraCuota);
            rs = ps.executeQuery();
            if (rs.next()){
                result=rs.getDouble("tir");
            }
                   
        } catch (Exception e) {
            throw new Exception("Error en total_valor_poliza [NegociosDAO] " + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            
           
        }
       
            return result;
    }
    
    
    public boolean generarCarteraLibranza(String codNeg, Usuario user) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean result = false;
        String query = "GENERAR_CARTERA_LIBRANZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codNeg);
            ps.setString(2,user.getLogin());
            rs = ps.executeQuery();
            if (rs.next()){
               return true;
            }                   
        } catch (SQLException e) {
            throw new Exception("Error en total_valor_poliza [NegociosDAO] " + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }       
        return result;
    }

}
     
     
