/**
 * Nombre        TblConceptosDAO.java
 * Descripci�n   Opciones de la tabla de tblcon
 * Autor         Mario Fontalvo Solano
 * Fecha         8 de marzo de 2006, 03:03 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.DAOS;


import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.TblConceptos;
import com.tsp.util.Util;

public class TblConceptosDAO extends MainDAO {
    
    
    
    /** Crea una nueva instancia de  TConceptosDAO */
    public TblConceptosDAO() {
        super("ConceptosDAO.xml");
    }
    
    
    /**
     * Metodo que obtiene todos los conceptos no anulados
     * @autor mfontalvo
     * @return Vector, vector de conceptos.
     * @throws Exception.
     */
    
    
    /**
     * Metodo que obtiene todos los conceptos
     * @autor mfontalvo
     * @return Vector, vector de conceptos.
     * @throws Exception.
     */
    public Vector obtenerTodosLosConceptos() throws Exception {
        try{
            return this.obtenerConceptos("");
        }catch (Exception ex){
            throw new Exception( "Error en listado obtenerTodosLosConceptos [TblConceptosDAO] ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo que obtiene todos los conceptos generales
     * @autor mfontalvo
     * @return Vector, vector de conceptos.
     * @throws Exception.
     */    
    public Vector obtenerConceptosGenerales() throws Exception {
        try{
            return this.obtenerConceptos(" WHERE reg_status!='A' and concept_code = concept_class ");
        }catch (Exception ex){
            throw new Exception( "Error en listado obtenerConceptosGenerales [TblConceptosDAO] ...\n" + ex.getMessage() );
        }        
    }
    
    
    
    /**
     * Metodo que obtiene todos los conceptos especificos
     * @autor mfontalvo
     * @param concept_class, clase del concepto
     * @return Vector, vector de conceptos.
     * @throws Exception.
     */    
    public Vector obtenerConceptosEspecificos( String concept_class ) throws Exception {
        try{
            return this.obtenerConceptos(" WHERE reg_status!='A' and concept_class ='"+ concept_class +"' and concept_code != concept_class ");
        }catch (Exception ex){
            throw new Exception( "Error en listado obtenerobtenerConceptosEspecificosodosConceptos [TblConceptosDAO] ...\n" + ex.getMessage() );
        }        
    }    
    
    
    
    /**
     * Metodo que obtiene un concepto especifico
     * @autor mfontalvo
     * @param dstrct, distrito del concepto
     * @param concept_code, codigo del concepto
     * @return TblConceptos, Concepto encontrado
     * @throws Exception.
     */
    public TblConceptos obtenerConceptos(String dstrct, String concept_code) throws Exception {
        try{
            String where = " WHERE dstrct = '"+ dstrct +"' and concept_code = '" + concept_code + "' ";
            Vector vec = this.obtenerConceptos(where);
            if (!vec.isEmpty())
                return (TblConceptos) vec.get(0);
            else
                return null;
        }catch (Exception ex){
            throw new Exception( "Error en busqueda obtenerConceptos [TblConceptosDAO] ...\n" + ex.getMessage() );
        }
    }
    
    /**
     * Metodo que actualiza un concepto a la base de datos
     * @autor hosorio
     * @param concepto, Concepto a actualizar descripcion y  account
     * @throws Exception.
     */     
    public void update2( String desc, String account, String codigo ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR2";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, desc);            
            st.setString(2, account);
            st.setString(3, codigo);
            st.executeUpdate();
            
            }}catch (SQLException ex){
            throw new SQLException( ex.getMessage() );
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Metodo que actualiza un concepto a la base de datos
     * @autor hosorio
     * @param concepto, Concepto a actualizar descripcion y  account
     * @throws Exception.
     */     
    public void anular( String distrito, String codigo ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_ANULAR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, codigo);                        
            st.executeUpdate();
            
            }}catch (SQLException ex){
            throw new SQLException( ex.getMessage() );
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
     /**
      * Determina si un concepto y cual es su estado
      * @autor Ing. Andr�s Maturana D.
      * @param concepto C�digo del concepto
      * @param distrito C�digo del distrito
      * @throws Exception.
      */  
    public String existeConcepto( String distrito, String codigo ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String existe = null;//JJCastro fase2
        String query = "SQL_EXISTE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, codigo);                        
            rs = st.executeQuery();
            
            if( rs.next() ){
                existe =  rs.getString("reg_status");//JJCastro fase2
            }
            
            }}catch (SQLException ex){
            throw new SQLException( ex.getMessage() );
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        return existe;
    }
    
    
    /**
     * Metodo que obtiene un listado de conceptos de la base de datos
     * @autor mfontalvo
     * @param where, restricciones de la consulta
     * @return Vector, vector de Conceptos encontrados
     * @throws Exception.
     */
    private Vector obtenerConceptos( String where ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet         rs = null;
        Vector            listado = new Vector();
        try{
             con = this.conectarJNDI("SQL_LISTADO");//JJCastro fase2
            if (con!=null){
                st = con.prepareStatement(
                this.obtenerSQL("SQL_LISTADO") + " " +
                where + " order by concept_code "
                );
                rs = st.executeQuery();
                while ( rs.next() ) {
                    TblConceptos concepto = new TblConceptos();
                    concepto.setReg_status      ( rs.getString("reg_status"));
                    concepto.setDstrct          ( rs.getString("dstrct"));
                    concepto.setConcept_code    ( rs.getString("concept_code"));
                    concepto.setConcept_desc    ( rs.getString("concept_desc"));
                    concepto.setConcept_class   ( rs.getString("concept_class"));
                    concepto.setClass_desc      ( rs.getString("class_desc"));
                    concepto.setAccount         ( rs.getString("account"));
                    concepto.setVisible         ( rs.getString("visible"));
                    concepto.setModif           ( rs.getString("modif"));
                    concepto.setCodigo_migracion( rs.getString("codigo_migracion"));
                    concepto.setInd_application ( rs.getString("ind_application"));
                    concepto.setInd_signo       ( rs.getInt   ("ind_signo"));
                    concepto.setBase            ( rs.getString("base"));
                    concepto.setTipo_cuenta     ( rs.getString("tipocuenta"));
                    concepto.setAsocia_cheque   ( rs.getString("asocia_cheque"));
                    concepto.setTipo            ( rs.getString("tipo"));
                    concepto.setVlr             ( rs.getDouble("vlr"));
                    concepto.setCurrency        ( rs.getString("currency"));
                    concepto.setDescuento_fijo  ( rs.getString("descuento_fijo"));
                    concepto.setIngreso_costo   ( rs.getString("ingreso_costo"));
                    
                    listado.add(concepto);
                    concepto = null;//Liberar Espacio JJCastro
                }
            }
        }catch (Exception ex){
            throw new Exception( ex.getMessage() );
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listado;
    }
    
    /**
     * Metodo que obtiene todos los conceptos no anulados
     * @autor mfontalvo
     * @return Vector, vector de conceptos.
     * @throws Exception.
     */
    public Vector obtenerConceptos() throws Exception {
        try{
            return this.obtenerConceptos(" WHERE reg_stastus != 'A' ");
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en listado obtenerConceptos [TblConceptosDAO] ...\n" + ex.getMessage() );
        }
    }
    
    /**
     * Metodo que ingresa un concepto a la base de datos
     * @autor mfontalvo
     * @param concepto, Concepto a ingresar
     * @throws Exception.
     */    
    public void insertConcepto( TblConceptos concepto ) throws SQLException {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_INSERTAR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1 , concepto.getDstrct() );
            st.setString(2 , concepto.getConcept_code());
            st.setString(3 , concepto.getConcept_desc());
            st.setString(4 , concepto.getInd_application());
            st.setString(5 , concepto.getAccount());
            st.setString(6 , concepto.getCodigo_migracion());
            st.setString(7 , concepto.getVisible());
            st.setString(8 , concepto.getModif());
            st.setInt   (9 , concepto.getInd_signo());
            st.setString(10, concepto.getConcept_class());
            st.setString(11, Util.getFechaActual_String(6));
            st.setString(12, concepto.getCreation_user());
            st.setString(13, concepto.getBase());
            /*nuevos campos*/
            st.setString(14, concepto.getTipo_cuenta());
            st.setString(15, concepto.getAsocia_cheque());
            st.setString(16, concepto.getTipo());
            st.setDouble(17, concepto.getVlr());
            st.setString(18, concepto.getCurrency());
            st.setString(19, concepto.getDescuento_fijo());
            st.setString(20, concepto.getIngreso_costo());
            st.executeUpdate();
            
            }}catch (SQLException ex){
            ex.printStackTrace();
            throw new SQLException( ex.getMessage() );
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
       }
    }
    
    
    /**
     * Metodo que actualiza un concepto a la base de datos
     * @autor mfontalvo
     * @param concepto, Concepto a actualizar
     * @throws Exception.
     */     
    public void updateConcepto( TblConceptos concepto ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1 , concepto.getConcept_desc());
            st.setString(2 , concepto.getInd_application());
            st.setString(3 , concepto.getAccount());
            st.setString(4 , concepto.getCodigo_migracion());
            st.setString(5 , concepto.getVisible());
            st.setString(6 , concepto.getModif());
            st.setInt   (7 , concepto.getInd_signo());
            st.setString(8 , concepto.getConcept_class());
            st.setString(9 , concepto.getTipo_cuenta() );
            st.setString(10, concepto.getAsocia_cheque() );
            st.setString(11, concepto.getTipo() );
            st.setDouble(12, concepto.getVlr() );
            st.setString(13, concepto.getCurrency() );
            st.setString(14, concepto.getDescuento_fijo());
            st.setString(15, concepto.getIngreso_costo());
            
            st.setString(16, Util.getFechaActual_String(6));
            st.setString(17, concepto.getUser_update());
            st.setString(18, concepto.getDstrct() );
            st.setString(19, concepto.getConcept_code());            
            st.executeUpdate();
            
            }}catch (Exception ex){
            throw new Exception( ex.getMessage() );
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    }
    
    
    
    /**
     * Metodo que modifica el estado de una lista de conceptos de la base de datos
     * @autor mfontalvo
     * @param conceptos, Concepto a actualizar
     * @throws Exception.
     */     
    public void updateEstado( Vector conceptos ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR_ESTADO";//JJCastro fase2
        try{
            
            if (conceptos!=null && !conceptos.isEmpty()){

                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                                Iterator it = conceptos.iterator();
                                while (it.hasNext()){
                                    TblConceptos concepto = (TblConceptos) it.next();
                                    st.clearParameters();
                                    st.setString(1 , concepto.getReg_status());
                                    st.setString(2 , Util.getFechaActual_String(6));
                                    st.setString(3 , concepto.getUser_update());
                                    st.setString(4 , concepto.getDstrct() );
                                    st.setString(5 , concepto.getConcept_code());
                                    st.executeUpdate();
                                }
                            }
            
        }}catch (Exception ex){
            throw new Exception( ex.getMessage() );
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
     /**
     * Metodo que elimina a partir de una lista de conceptos de la base de datos
     * @autor mfontalvo
     * @param conceptos, Concepto a actualizar
     * @throws Exception.
     */ 
    public void deleteConceptos( Vector conceptos ) throws Exception {
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        String query = "SQL_ELIMINAR";//JJCastro fase2
        try{
            
            if (conceptos!=null && !conceptos.isEmpty()){

                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
               Iterator it = conceptos.iterator();
                while (it.hasNext()){
                    TblConceptos concepto = (TblConceptos) it.next();
                    st.clearParameters();
                    st.setString(1 , concepto.getDstrct() );
                    st.setString(2 , concepto.getConcept_code());            
                    st.executeUpdate();                    
                }
            }
            
            }}catch (Exception ex){
            throw new Exception( ex.getMessage() );
        }finally{//JJCastro fase2
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
    } 
    
}
