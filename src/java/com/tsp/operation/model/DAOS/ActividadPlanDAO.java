/***********************************************************************************
 * Nombre clase : ............... ActividadPlanDAO.java                            *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 12 de septiembre de 2005, 12:29 PM              *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

public class ActividadPlanDAO extends MainDAO{ 
    
    private ActividadPlan actplan;
    private Vector VecActPlan;
    private Planilla plan;
    /** Creates a new instance of ActividadPlanDAO */
    public ActividadPlanDAO() {
        super("ActividadPlanDAO.xml");//JJCastro fase2
    }
    
    private static final String buscar = "Select distinct " +
    "                                            p.numpla," +
    "                                            r.numrem,   " +
    "                                            r.cliente as codcliente," +
    "                                            get_nombrecliente(r.cliente) as nom_cliente," +
    "                                            r.tipoviaje  " +
    "                                       FROM Planilla p,                     " +
    "                                            plarem pl,             " +
    "                                            Remesa r                " +
    "                                      WHERE p.numpla = pl.numpla " +
    "                                        and pl.numrem = r.numrem " +
    "                                        and p.stapla = 'A'       " +
    "                                        and p.numpla = ? ";
    
    public static final String SQL_INFO_PLANILLA = "SELECT          " +
                                                    "    fecpla,    " +
                                                    "    numpla,    " +
                                                    "    get_nombreciudad(oripla) as origen,    " +
                                                    "    get_nombreciudad(despla) as destino,    " +
                                                    "    plaveh " +
                                                    "FROM   planilla " +
                                                    "WHERE  stapla = 'A'       " +
                                                    "   and numpla = ?  ";
    
    /**
     * Metodo setActividadPlan, setea el objeto de tipo actividad
     * @param: objeo ActividadPlan
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void  setActividadPlan (ActividadPlan actpla){
        
        this.actplan = actpla;
        
    }
     /**
     * Metodo obtActividadPlan, obtiene  objeto de tipo actividad planill
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public ActividadPlan obtActividadPlan(){
        return actplan;
    }
    /**
     * Metodo obtVecActividadPlan, retorna el vector de activid planilla
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector obtVecActividadPlan (){
        return VecActPlan;
    }
     /**
     * Metodo buscarInfoplanilla, busca la informacion de la planilla
     * @param: numero de la planilla
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarInfoplanilla(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        plan = null;
        String query = "SQL_INFO_PLANILLA";

        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, numpla);
                ////System.out.println(st);
                rs = st.executeQuery();
                //crear objeto 
                if (rs.next()){
                    Planilla pla = new Planilla();
                    pla.setNumpla(rs.getString("numpla"));
                    pla.setFecpla(rs.getDate("fecpla"));
                    pla.setOrinom(rs.getString("origen"));
                    pla.setDespla(rs.getString("destino"));
                    pla.setPlaveh(rs.getString("plaveh"));
                    this.plan = pla; 
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR INFO PLANILLA " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
        
   /**
     * Metodo BuscarCLientexPlanilla, busca los clientes que tiene la planilla
     * @param: numero de la planilla
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean BuscarCLientexPlanilla(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;       
        boolean sw = false;
        VecActPlan = null;
        String query = "SQL_BUSCAR";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, numpla);
                VecActPlan = new Vector();
                ////System.out.println(st);
                rs = st.executeQuery();
                
                while (rs.next()){
                    VecActPlan.add(ActividadPlan.load(rs)); 
                    sw = true;
                }
            }
        }catch(SQLException e){
             throw new SQLException("ERROR AL BUSCAR CLIENTE POR PLANILLA " + e.getMessage()+"" + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Getter for property plan.
     * @return Value of property plan.
     */
    public com.tsp.operation.model.beans.Planilla getPlan() {
        return plan;
    }
    
   
}
