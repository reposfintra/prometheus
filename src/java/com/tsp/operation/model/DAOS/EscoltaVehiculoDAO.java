/*
 *
 EscoltaVehiculo.java
 *
 * Created on 31 de julio de 2005, 09:00 AM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;

public class EscoltaVehiculoDAO extends MainDAO{
    
    private Escolta  e;
    private Vector excluidos = new Vector();
    private Vector datos, vecModificar;
    private Vector escoltaActual = new Vector();
    private Vector reporte;//Tito 13.01.2006
    private Vector placas;
    
    
    private static String SQL_BUSCAR_ESCOLTAS        =
    "SELECT " +
    "     distinct cedu, " +
    "     placa, " +
    "     nomcond, " +
    "     nombre " +
    "from " +
    "     (select " +
    "           cedula as cedu,placa, "+
    "           N.nombre as nomcond, " +
    "           propietario as prop " +
    "      from " +
    "           nit N, placa "+
    "      where " +
    "           conductor=cedula and " +
    "           tipo='ESC' and estado_escolta='L') as consu, " +
    "      nit " +
    "where " +
    "      nit.cedula=prop";
    
    private static final String SQL_ASIGNAR_ESCOLTA  =
    "Insert INTO escolta_vehiculo (numpla,placa,fecasignacion,"+
    "creation_user,base,codorigen,coddestino) VALUES (?,?,?,?,?,?,?);" +
    "UPDATE placa set estado_escolta='O' where placa=?";
    
    private static final String SQL_ESC_ASIGNADO     =
    "select placa from placa where placa=? and tipo='ESC' and estado_escolta='L'";
    
    
    private static final String SQL_ESC_PLANIILLA    =   "select distinct  cedu, placa, nomcond, nombre from (select  cedula as cedu, E.placa, "+
    "N.nombre as nomcond, propietario as prop from nit N, placa P, " +
    "escolta_vehiculo E where conductor=cedula and e.placa=p.placa and tipo='ESC' and E.numpla=?) " +
    "as consu, nit where nit.cedula=prop ";
    
    private static String SQL_ACT_ESC_VEHICULO       =
    "UPDATE escolta_vehiculo set razon_exclusion=?, last_update='now()'," +
    "user_update=?,codorigen=?,coddestino=?, estado='L', fecha_liberacion = 'now()' where placa = ?";
    
    private static String SQL_ACT_ESC_VEHICULO_CARAVANA       =
    "UPDATE escolta_caravana set razon_exclusion=?, last_update='now()'," +
    "user_update=?, estado='L', fecha_liberacion = 'now()' where placa = ?";
    
    private static String SQL_ORIDEST_ESCOLTA       =
    "UPDATE escolta_vehiculo set codorigen=?, coddestino=? where placa = ?";
    
    private static String SQL_ORIDEST_CARAVANA       =
    "UPDATE escolta_caravana set codorigen=?, coddestino=? where placa = ?";
    
    private static String SQL_INFO_ESCOLTA           =
    "select cedula, nombre, nomciu, country_name, telefono, celular, direccion from " +
    "nit N, ciudad C, pais P where N.codciu=C.codciu and N.codpais=P.country_code and cedula=?";
    
    String SQL_BUS_ESCOLTAS_ASIGNADOS =
    /*"SELECT " +
    "      distinct " +
    "      P.placa, " +
    "      N.nombre, " +
    "      case when EC.fecasignacion is null then E.fecasignacion else EC.fecasignacion end," +
    "      C.numcaravana, " +
    "      case when C.numpla is null then E.numpla else C.numpla end " +
    "FROM" +
    "      Nit N,             " +
    "      placa P" +
    "      left join escolta_vehiculo E on (P.placa = E.placa and E.estado='O')  " +
    "      left join escolta_caravana EC on (P.placa=EC.placa and EC.estado='O')" +
    "      right  join caravana C on (C.numcaravana=EC.numcaravana OR C.numpla=E.numpla) " +
    "WHERE" +
    "      C.fecfin='0099-01-01' and " +
    "      C.reg_status<>'A' and ";*/
    "SELECT c1.*, COALESCE(plr.numrem, '') AS numrem " +
    "FROM ( " +
    "       SELECT " +
    "               distinct " +
    "               P.placa, " +
    "               N.nombre, " +
    "               case when EC.fecasignacion is null then E.fecasignacion else EC.fecasignacion end," +
    "               C.numcaravana, " +
    "               case when C.numpla is null then E.numpla else C.numpla end " +
    "       FROM" +
    "               Nit N,             " +
    "               placa P" +
    "               left join escolta_vehiculo E on (P.placa = E.placa and E.estado='O')  " +
    "               left join escolta_caravana EC on (P.placa=EC.placa and EC.estado='O')" +
    "               right  join caravana C on (C.numcaravana=EC.numcaravana OR C.numpla=E.numpla) " +
    "       WHERE" +
    "               C.fecfin='0099-01-01' and " +
    "               C.reg_status<>'A' and ";
    
    private static String SQL_ANULAR_ASIGNACION      =
    "Update caravana set reg_status='A' where numcaravana=0 and numpla=?";
    
    private static String SQL_BUSQ_PLACA             =  "SELECT " +
    "      distinct " +
    "      c.numpla," +
    "      ec.placa as esccarv," +
    "      ev.placa as escveh " +
    "FROM " +
    "    caravana c" +
    "    left outer join escolta_vehiculo ev on (ev.numpla=c.numpla and c.numpla=?)      " +
    "    left join escolta_caravana ec on (ec.numcaravana=c.numcaravana and c.numpla=?)";
    
    private static final String SQL_SEARCH_ESC =
    "SELECT placa FROM escolta_caravana WHERE numcaravana=?";
    
    private static final String SQL_UPDATE_CARAVANA_ESC =
    "UPDATE  caravana set numcaravana=0 "+
    "WHERE numpla=?";
    
    //Tito 13.01.2006
    private static String SQL_REPESCOLTAS =
    "SELECT * " +
    "FROM (( " +
    "   SELECT  pl.numpla, " +
    "           pl.reg_status, " +
    "           fecpla, " +
    "           get_nombreciudad(oripla) AS oripla, " +
    "           get_nombreciudad(despla) AS despla, " +
    "           COALESCE(get_nombrenit(conductor), 'No se encontr�.') AS conductor, " +
    "           COALESCE(get_nombrenit(propietario), 'No se encontr�.') AS propietario," +
    "           escv.placa, " +
    "           COALESCE(get_nombreciudad(coddestino), 'No se encontr�.') as coddestino, " +
    "           fecasignacion " +
    "   FROM planilla pl " +
    "   JOIN escolta_vehiculo escv ON (escv.numpla=pl.numpla) " +
    "   LEFT JOIN placa plc ON (escv.placa=plc.placa) ) " +
    "UNION " +
    "(  SELECT  pl.numpla, " +
    "           pl.reg_status, " +
    "           fecpla, " +
    "           get_nombreciudad(oripla) AS oripla, " +
    "           get_nombreciudad(despla) AS despla, " +
    "           COALESCE(get_nombrenit(conductor), 'No se encontr�.') AS conductor, " +
    "           COALESCE(get_nombrenit(propietario), 'No se encontr�') AS propietario, " +
    "           escc.placa, " +
    "           COALESCE(get_nombreciudad(coddestino), 'No se encontr�') as coddestino, " +
    "           fecasignacion  " +
    "   FROM planilla pl " +
    "   JOIN caravana car ON (car.numpla=pl.numpla)" +
    "   JOIN escolta_caravana escc ON (escc.numcaravana=car.numcaravana) " +
    "   LEFT JOIN placa plc ON (escc.placa=plc.placa) )) AS c1 " +
    "WHERE fecpla BETWEEN ? AND ? AND reg_status<>'A' " +
    "ORDER BY numpla";
    
    private static String SQL_ESCS_X_CARAVANA =
    "SELECT  " +
    "      DISTINCT   " +
    "      cedu,  " +
    "      placa,  " +
    "      nomcond,  " +
    "      nombre," +
    "      codorigen," +
    "      coddestino        " +
    "FROM  " +
    "      (SELECT   " +
    "             N.cedula as cedu,  " +
    "             E.placa, " +
    "             N.nombre as nomcond,  " +
    "             P.propietario as prop," +
    "             E.coddestino," +
    "             E.codorigen " +
    "       FROM  " +
    "             nit N,  " +
    "             placa P,  " +
    "             escolta_caravana E  " +
    "       WHERE  " +
    "             P.conductor=N.cedula and  " +
    "             E.placa=P.placa and  " +
    "             P.tipo='ESC'  and " +
    "             E.numcaravana=? and E.estado!='L') as consu,  " +
    "       Nit  " +
    "WHERE  " +
    "      nit.cedula=prop";
    
    
    
    private static String SQL_OCUPAR_ESCOLTA =
    "UPDATE placa set estado_escolta='O' where placa=?";
    
    private static String SQL_NUMPLA_ESCOLTAS =
    "(SELECT " +
    "       CA.NUMPLA AS ABC FROM CARAVANA CA " +
    "       LEFT JOIN ESCOLTA_CARAVANA EC ON (EC.NUMCARAVANA=CA.NUMCARAVANA) " +
    " WHERE EC.PLACA=? AND EC.ESTADO='O') " +
    "UNION" +
    "(SELECT  " +
    "      EV.NUMPLA AS ABC FROM ESCOLTA_VEHICULO EV " +
    " WHERE EV.PLACA=? AND EV.ESTADO='O')";
    
    
    private static String SQL_BUSCAR_ESCOLTAS_SRCH =  //AMATURANA
    "SELECT " +
    "     distinct cedu, " +
    "     placa, " +
    "     nomcond, " +
    "     nombre " +
    "from " +
    "     (select " +
    "           cedula as cedu,placa, "+
    "           N.nombre as nomcond, " +
    "           propietario as prop " +
    "      from " +
    "           nit N, placa "+
    "      where " +
    "           conductor=cedula and " +
    "           tipo='ESC' and estado_escolta='L') as consu, " +
    "      nit " +
    "where " +
    "       nit.cedula=prop " +
    "       AND UPPER(nomcond) LIKE ?";
    
    
    private static String SQL_DEL_ESCOLTA_CARAVANA =
    "UPDATE placa set estado_escolta='L' where placa=?; " +
    "UPDATE escolta_caravana set estado='L', fecha_liberacion='now()' where placa=? and numcaravana=?";
    
    private static String SQL_DEL_ESCOLTA =
    "UPDATE placa set estado_escolta='L' where placa=?;" +
    "UPDATE escolta_vehiculo  set estado='L', fecha_liberacion='now()' WHERE placa=? and numpla=?;";
    
    private static String SQL_BORRAR_ESCOLTA =
    "UPDATE placa set estado_escolta='L' where placa=?;" +
    "DELETE FROM escolta_vehiculo  WHERE placa=? and numpla=?;";
    
    
    private static String SQL_BORRAR_ESCOLTA_CARAVANA =
    "UPDATE placa set estado_escolta='L' where placa=?;" +
    "DELETE FROM escolta_caravana  WHERE numcaravana=? and placa=?;";
    //"UPDATE escolta_vehiculo set estado='L' where placa=?;" +
    //"UPDATE escolta_caravana set estado='L' where placa=?";
    
    private static String SQL_PLANILLA_ASIGNADA =
    "Select distinct estado from escolta_vehiculo where numpla=? and estado='O'";
    
    private static String SQL_PLACAS_PLANIILLA =
    "SELECT placa from escolta_vehiculo where numpla=?";
    
    public EscoltaVehiculoDAO() {
        super("EscoltaVehiculoDAO.xml");
    }
    
    /**
     * crea una lista de los escoltas disponibles
     * @throws SQLException si existe un error en la conculta.
     * @return void <tt>VistaCaravana</tt>
     */
    
    public void listarEscoltas() throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        datos = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_BUSCAR_ESCOLTAS);
                rs = st.executeQuery();
                ResultSet m = rs;
                while (rs.next()){
                    e = new Escolta();
                    e.setPlaca(rs.getString("placa"));
                    e.setConductor(rs.getString("nomcond"));
                    e.setPropietario(rs.getString("nombre"));
                    e.setCedconductor(rs.getString("cedu"));
                    e.setOrigen("");
                    e.setDestino("");
                    e.setSeleccionado(false);
                    datos.addElement(e);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR DESPACHOS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Crea un objeto <tt>Escolta</tt> de acuerdo a la placa y lo retorna
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>Escolta</tt>
     */
    
    public void getEscoltaXPlanilla(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        escoltaActual = null;
        escoltaActual = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ESC_PLANIILLA);
                st.setString(1, numpla);
                rs = st.executeQuery();
                while (rs.next()){
                    e = new Escolta();
                    e.setPlaca(rs.getString("placa"));
                    e.setConductor(rs.getString("nomcond"));
                    e.setCedconductor(rs.getString("cedu"));
                    e.setPropietario(rs.getString("nombre"));
                    e.setSeleccionado(false);
                    if (!existeVehiculoCaravana(e))
                        escoltaActual.addElement(e);
                }
            }
        }catch(Exception e){
            //throw new SQLException("ERROR AL BUSCAR DESPACHOS" + e.getMessage()+"" + e.getErrorCode());
            e.printStackTrace();
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Cambia el estado de un escolta asignado a un vehiculo a libre
     * @throws SQLException si existe un error en la conculta.
     * @return vacio
     */
    
    public boolean liberarEscolta(String placa,String planilla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        boolean swi = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_DEL_ESCOLTA);
                st.setString(1, placa);
                st.setString(2, placa);
                st.setString(3, planilla);
                swi = st.execute();
            }
            ////System.out.println("SWDEL: "+swi);
            return swi;
        }catch(SQLException e){
            throw new SQLException("ERROR AL LIBERAR ESCOLTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Cambia el estado de un escolta asignado a un vehiculo a libre
     * @throws SQLException si existe un error en la conculta.
     * @return vacio
     */
    
    public boolean eliminarEscolta(String placa,String planilla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        boolean swi = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_BORRAR_ESCOLTA);
                st.setString(1, placa);
                st.setString(2, placa);
                st.setString(3, planilla);
                swi = st.execute();
            }
            ////System.out.println("SWDEL: "+swi);
            return swi;
        }catch(SQLException e){
            throw new SQLException("ERROR AL LIBERAR ESCOLTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Cambia el estado de un escolta asignado a un vehiculo a libre
     * @throws SQLException si existe un error en la conculta.
     * @return vacio
     */
    
    public boolean eliminarEscoltaCaravana(String placa, int numC) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        boolean swi = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_BORRAR_ESCOLTA_CARAVANA);
                st.setString(1, placa);
                st.setInt(2, numC);
                st.setString(3, placa);
                swi = st.execute();
            }
            
            return swi;
        }catch(SQLException e){
            throw new SQLException("ERROR AL LIBERAR ESCOLTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    
    /**
     * Cambia el estado de un escolta asignado a una caravana
     * @throws SQLException si existe un error en la conculta.
     * @return vacio
     */
    
    public boolean liberarEscoltaCaravana(String placa, int numC) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        boolean swi = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_DEL_ESCOLTA_CARAVANA);
                st.setString(1, placa);
                st.setString(2, placa);
                st.setInt(3, numC);
                swi = st.execute();
            }
            ////System.out.println("SWDEL: "+swi);
            return swi;
        }catch(SQLException e){
            throw new SQLException("ERROR AL LIBERAR ESCOLTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public Vector getVectorEscoltas(){
        return datos;
    }
    public void modificarVectorEscoltas(int pos,boolean val){
        e = (Escolta) datos.elementAt(pos);
        e.setSeleccionado(val);
        datos.set(pos, e);
    }
    
    /**
     * Asigna un escolta a un vehiculo
     * @param Objeto EscoltaVehiculo contiene los datos del escolta a asignar a la caravana
     * @throws SQLException si existe un error en la conculta.
     */
    public void insertarEscoltaVehiculo( EscoltaVehiculo e ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PreparedStatement st3 = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                //verificando si ya existe un registo anterior en la caravana con estado L
                st3 = con.prepareStatement("Select numpla from escolta_vehiculo where numpla = ?  and placa = ?");
                st3.setString(1, e.getNumpla());
                st3.setString(2, e.getPlaca());
                ResultSet rs = st3.executeQuery();
                if (rs.next()) {
                    st = con.prepareStatement("Update escolta_vehiculo set estado='O', fecasignacion=?,"+
                    "creation_user=?,base=?,codorigen=?,coddestino=? where numpla=? and placa=?;" +
                    "UPDATE placa set estado_escolta='O' where placa=?");
                    st.setString(1, e.getFecasignacion());
                    st.setString(2, e.getCreation_user());
                    st.setString(3, e.getBase());
                    st.setString(4, e.getOrigen());
                    st.setString(5, e.getDestino());
                    st.setString(6, e.getNumpla());
                    st.setString(7, e.getPlaca());
                    st.setString(8, e.getPlaca());
                    st.execute();
                }else {
                    st = con.prepareStatement(this.SQL_ASIGNAR_ESCOLTA);
                    st.setString(1, e.getNumpla());
                    st.setString(2, e.getPlaca());
                    st.setString(3, e.getFecasignacion());
                    st.setString(4, e.getCreation_user());
                    st.setString(5, e.getBase());
                    st.setString(6, e.getOrigen());
                    st.setString(7, e.getDestino());
                    st.setString(8, e.getPlaca());
                    ////System.out.println("ESC INSERT: "+st);
                    st.execute();
                    
                }
            }
        }catch(SQLException e1){
            throw new SQLException("ERROR AL ASIGNAR ESCOLTAS" + e1.getMessage()+"" + e1.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e1){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e1.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    /**
     * Modifica un escolta asignado a una planilla
     * @param Objeto EscoltaVehiculo contiene los datos del escolta a asignar a la caravana
     * @throws SQLException si existe un error en la conculta.
     */
    public void modificarEscoltaVehiculo( Escolta e ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ACT_ESC_VEHICULO);
                st.setString(1, e.getRazon_exclusion());
                st.setString(2, e.getUser_update());
                st.setString(3, e.getOrigen());
                st.setString(4, e.getDestino());
                st.setString(5, e.getPlaca());
                st.execute();
                
            }
        }catch(SQLException e1){
            throw new SQLException("ERROR AL MODIFICAR ESCOLTAS" + e1.getMessage()+"" + e1.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e1){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e1.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    public Vector getVectorModificarEscolta(){
        return excluidos;
    }
    public void eliminarPosicionVectorModificarEscoltaCaravana(int pos){
        excluidos.remove(pos);
    }
    /**
     * Busca un escolta en el vector y retorna un valor de acuerdo a la busqueda
     * @param es. Objeto <tt>Escolta</tt> que contiene la informacion del vehiculo
     * @return true si existe el objeto o false sino existe
     */
    public boolean existeVehiculoCaravana(Escolta es) {
        for (int i=0; i<excluidos.size(); i++){
            if (es.getPlaca().equals(((Escolta) excluidos.elementAt(i)).getPlaca()))
                return true;
        }
        return false;
    }
    
    public Vector getVectorExcluidos(){
        return excluidos;
    }
    public void agregarVectorExcluidos(Escolta es){
        if (!es.isSeleccionado())
            excluidos.addElement(es);
    }
    public void eliminarPosicionVectorExcluidos(int pos){
        excluidos.remove(pos);
    }
    /**
     * Busca un Escolta en el vector y retorna un valor de acuerdo a la busqueda
     * @param vista. Objeto vista que contiene la informacion del Escolta
     * @return true si existe el objeto o false sino existe
     */
    public boolean existeEscoltaCaravanaActual(Escolta es) {
        for (int i=0; i<escoltaActual.size(); i++){
            if (es.getPlaca().equals(((Escolta) escoltaActual.elementAt(i)).getPlaca()))
                return true;
        }
        return false;
    }
    public Vector getVectorEscoltaCaravanaActual(){
        return escoltaActual;
    }
    public void agregarVectorEscoltaCaravanaActual(Escolta es){
        escoltaActual.addElement(es);
    }
    public void eliminarPosicionVectorEscoltaCaravanaActual(int pos){
        escoltaActual.remove(pos);
    }
    public void setRazonVectorExcluido(int pos, String razon){
        Escolta es = (Escolta) excluidos.elementAt(pos);
        es.setRazon_exclusion(razon);
        eliminarPosicionVectorExcluidos(pos);
        excluidos.add(pos, es);
    }
    public void limpiarVectorEscoltaActual(){
        escoltaActual.removeAllElements();
    }
    public void limpiarVectorExcluidos(){
        excluidos.removeAllElements();
    }
    public void borrarPosicionMayorUnoVectorEscoltaActual(){
        int tam = escoltaActual.size();
        int i = 0;
        while (i<tam){
            if(i<=tam-1)
                escoltaActual.remove(i+1);
            i++;
        }
    }
    public Escolta getInfoEscolta(String ced) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        e = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_INFO_ESCOLTA);
                st.setString(1,ced);
                rs = st.executeQuery();
                ResultSet m = rs;
                while (rs.next()){
                    e = new Escolta();
                    e.setCedconductor(rs.getString("cedula"));
                    e.setConductor(rs.getString("nombre"));
                    e.setCiudad(rs.getString("nomciu"));
                    e.setPlaca(rs.getString("country_name"));
                    e.setPropietario(rs.getString("direccion"));
                    e.setRazon_exclusion(rs.getString("telefono"));
                    e.setUser_update(rs.getString("celular"));
                }
            }
            return e;
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR INFO ESCOLTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public void anularAsignacionEscolta(String numpla) throws SQLException {
        int num = 0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_ANULAR_ASIGNACION);
                st.setString(1, numpla);
                st.execute();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR ASIGNACION" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    
    
    /**
     * Asocia un escolta a una planilla
     * @param EscoltaVehiculo e
     * @throws SQLException si existe un error en la insercci�n.
     */
    public void insertarEscoltaCaravana( EscoltaVehiculo e ) throws SQLException {
        Connection con= null;
        PreparedStatement st  = null;
        PreparedStatement st2 = null;
        PreparedStatement st3 = null;
        PreparedStatement stBatch = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                con.setAutoCommit(false);
                
                stBatch = con.prepareStatement("");
                
                //verificando si ya existe un registo anterior en la caravana con estado L
                st3 = con.prepareStatement("Select numcaravana from escolta_caravana where numcaravana = ?  and placa = ?");
                st3.setInt(1, e.getNumcaravana());
                st3.setString(2, e.getPlaca());
                ResultSet rs = st3.executeQuery();
                if (rs.next()) {
                    st = con.prepareStatement("Update escolta_caravana set estado='O' where numcaravana=? and placa=?");
                    st.setInt(1, e.getNumcaravana());
                    st.setString(2, e.getPlaca());
                }else {
                    st = con.prepareStatement("INSERT INTO escolta_caravana (numcaravana,placa,fecasignacion, "+
                    "creation_user,base,codorigen,coddestino) VALUES (?,?,?,?,?,?,?)");
                    st.setInt(1, e.getNumcaravana());
                    st.setString(2, e.getPlaca());
                    st.setString(3, e.getFecasignacion());
                    st.setString(4, e.getCreation_user());
                    st.setString(5, e.getBase());
                    st.setString(6, e.getOrigen());
                    st.setString(7, e.getDestino());
                }
                stBatch.addBatch(st.toString());
                
                st2 = con.prepareStatement(SQL_OCUPAR_ESCOLTA);
                st2.setString(1,  e.getPlaca());
                stBatch.addBatch(st2.toString());
                
                
                stBatch.executeBatch();
                
                con.commit();
                con.setAutoCommit(true);;
                
            }
        }catch(SQLException e1){
            if (con!=null) con.rollback();
            e1.printStackTrace();
            throw new SQLException("ERROR AL ASIGNAR ESCOLTAS" + e1.getMessage()+"" + e1.getErrorCode());
        }finally{
            if(st != null) {
                st.close();
            }
            if(st2 != null) {
                st2.close();
            }
            if(st3 != null) {
                st3.close();
            }
            if(stBatch != null) {
                stBatch.close();
            }
            
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    public void getEscoltaXNumCaravana(int numCaravana) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        escoltaActual = null;
        escoltaActual = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("select " +
                "      distinct  " +
                "      cedu, " +
                "      placa, " +
                "      nomcond, " +
                "      nombre " +
                "from " +
                "      (select  " +
                "             N.cedula as cedu, " +
                "             E.placa, "+
                "             N.nombre as nomcond, " +
                "             P.propietario as prop " +
                "       from " +
                "            nit N, " +
                "            placa P, " +
                "            escolta_caravana E " +
                "       where " +
                "            conductor=cedula and " +
                "            e.placa=p.placa and " +
                "            tipo='ESC' " +
                "            and E.numcaravana=?" +
                "      ) as consu, " +
                "      nit " +
                "where " +
                "      nit.cedula=prop ");
                st.setInt(1, numCaravana);
                rs = st.executeQuery();
                while (rs.next()){
                    e = new Escolta();
                    e.setPlaca(rs.getString("placa"));
                    e.setConductor(rs.getString("nomcond"));
                    e.setCedconductor(rs.getString("cedu"));
                    e.setPropietario(rs.getString("nombre"));
                    e.setSeleccionado(false);
                    if (!existeVehiculoCaravana(e))
                        escoltaActual.addElement(e);
                }
            }
        } catch(Exception e){
            //throw new SQLException("ERROR AL BUSCAR DESPACHOS" + e.getMessage()+"" + e.getErrorCode());
            e.printStackTrace();
        } finally {
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public int searchEscoltasCaravana( int numCaravana) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        int cont = 0 ;
        placas = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_SEARCH_ESC);
                st.setInt(1,  numCaravana);
                rs = st.executeQuery();
                while (rs.next()){
                    placas.addElement(rs.getString("placa"));
                    cont++;
                }
            }
            return cont;
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR DESPACHOS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    /**
     * Procedimiento que retorna los vehiculos que escoltan una caravana
     * @throws SQLException si existe un error en la conculta.
     */
    
    public void getVectorEscoltasXCaravana(int numCaravana) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        escoltaActual = null;
        escoltaActual = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ESCS_X_CARAVANA);
                st.setInt(1, numCaravana);
                ////System.out.println("ESC_CARAVANA: "+st);
                rs = st.executeQuery();
                while (rs.next()){
                    e = new Escolta();
                    e.setPlaca(rs.getString("placa"));
                    e.setConductor(rs.getString("nomcond"));
                    e.setCedconductor(rs.getString("cedu"));
                    e.setPropietario(rs.getString("nombre"));
                    e.setOrigen(rs.getString("codorigen"));
                    e.setDestino(rs.getString("coddestino"));
                    e.setSeleccionado(false);
                    escoltaActual.addElement(e);
                }
            }
        } catch(Exception e){
            //throw new SQLException("ERROR AL BUSCAR DESPACHOS" + e.getMessage()+"" + e.getErrorCode());
            e.printStackTrace();
        } finally {
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public String getPlacaXPlanilla(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String placa = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_BUSQ_PLACA);
                st.setString(1,numpla);
                st.setString(2,numpla);
                rs = st.executeQuery();
                while (rs.next()){
                    if (rs.getString("esccarv")!=null && !rs.getString("esccarv").equals("")){
                        placa = rs.getString("esccarv");
                    }
                    if (rs.getString("escveh")!=null && !rs.getString("escveh").equals("")){
                        placa = rs.getString("escveh");
                    }
                }
            }
            return placa;
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR INFO ESCOLTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public void actualizarPlanillaAsignada(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_UPDATE_CARAVANA_ESC);
                st.setString(1, numpla);
                st.execute();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR LA PLANILLA EN CARAVANA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Obtiene los escoltas de las planillas con fecpla en un per�odod dado.
     * @autor Ing. Tito Andr�s Maturana
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @throws SQLException
     * @version 1.0
     */
    public void reporteEscoltas(String fechaI, String fechaF) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        this.reporte = new Vector();
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_REPESCOLTAS);
                st.setString(1, fechaI);
                st.setString(2, fechaF);
                
                
                
                rs = st.executeQuery();
                
                while(rs.next()){
                    Hashtable ht = new Hashtable();
                    ht.put("numpla", ((rs.getString("numpla")!=null)? rs.getString("numpla") : "" ));
                    ht.put("fecpla", ((rs.getString("fecpla")!=null)? rs.getString("fecpla") : "" ));
                    ht.put("oripla", ((rs.getString("oripla")!=null)? rs.getString("oripla") : "" ));
                    ht.put("despla", ((rs.getString("despla")!=null)? rs.getString("despla") : "" ));
                    ht.put("placa", ((rs.getString("placa")!=null)? rs.getString("placa") : "" ));
                    ht.put("destino", ((rs.getString("coddestino")!=null)? rs.getString("coddestino") : "" ));
                    ht.put("conductor", ((rs.getString("conductor")!=null)? rs.getString("conductor") : "" ));
                    ht.put("propietario", ((rs.getString("propietario")!=null)? rs.getString("propietario") : "" ));
                    ht.put("asignacion", ((rs.getString("fecasignacion")!=null)? rs.getString("fecasignacion") : "" ));
                    
                    this.reporte.add(ht);
                    
                    //////System.out.println(".............. Se encontro el escolta vehiculo: " + rs.getString("placa"));
                }
                
                //////System.out.println("................. Reporte de Escoltas, filas: " + this.reporte.size());
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN LA CONSULTA DEL REPORTE DE ESCOLTAS " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Getter for property reporte.
     * @return Value of property reporte.
     */
    public java.util.Vector getReporte() {
        return reporte;
    }
    
    /**
     * Setter for property reporte.
     * @param reporte New value of property reporte.
     */
    public void setReporte(java.util.Vector reporte) {
        this.reporte = reporte;
    }
    /**
     * Modifica un escolta asignado a una caravana
     * @param Objeto EscoltaVehiculo contiene los datos del escolta a asignar a la caravana
     * @throws SQLException si existe un error en la conculta.
     */
    public void modificarEscoltaVehiculoCaravana( Escolta e ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ACT_ESC_VEHICULO_CARAVANA);
                st.setString(1, e.getRazon_exclusion());
                st.setString(2, e.getUser_update());
                st.setString(3, e.getPlaca());
                st.execute();
                
            }
        }catch(SQLException e1){
            e1.printStackTrace();
            throw new SQLException("ERROR AL MODIFICAR ESCOLTAS" + e1.getMessage()+"" + e1.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e1){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e1.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    /**
     * Actualiza el origen y el destino de un vehiculo escolta
     * @param Objeto EscoltaVehiculo contiene los datos del escolta a asignar a la caravana
     * @throws SQLException si existe un error en la conculta.
     */
    public void updateOriDestEscoltaCaravana( Escolta e ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ORIDEST_CARAVANA);
                st.setString(1, e.getOrigen());
                st.setString(2, e.getDestino());
                st.setString(3, e.getPlaca());
                //////System.out.println("UPT ORI-DEST: "+st);
                st.execute();
                
            }
        }catch(SQLException e1){
            e1.printStackTrace();
            throw new SQLException("ERROR AL MODIFICAR ESCOLTAS" + e1.getMessage()+"" + e1.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e1){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e1.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    /**
     * Actualiza el origen y el destino de un vehiculo escolta
     * @param Objeto EscoltaVehiculo contiene los datos del escolta a asignar a la caravana
     * @throws SQLException si existe un error en la conculta.
     */
    public void updateOriDestEscolta( Escolta e ) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ORIDEST_ESCOLTA);
                st.setString(1, e.getOrigen());
                st.setString(2, e.getDestino());
                st.setString(3, e.getPlaca());
                ////System.out.println("ORIDEST: "+st);
                st.execute();
                
            }
        }catch(SQLException e1){
            throw new SQLException("ERROR AL MODIFICAR ESCOLTAS" + e1.getMessage()+"" + e1.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e1){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e1.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    /**
     * Cambia el estado de un escolta asignado a una caravana  a libre
     * @throws SQLException si existe un error en la conculta.
     * @return vacio
     */
    
    public Vector getPlanillasEscolta(String placa) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        Vector planillas = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_NUMPLA_ESCOLTAS);
                st.setString(1, placa);
                st.setString(2, placa);
                rs = st.executeQuery();
                while(rs.next()) {
                    planillas.addElement(rs.getString("abc"));
                }
            }
            return planillas;
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL OBTENER NUMPLAS ESCOLTA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Getter for property placas.
     * @return Value of property placas.
     */
    public java.util.Vector getPlacas() {
        return placas;
    }
    
    /************************************************************************
     *                      ANDRES MATURANA DE LA CRUZ                      *
     ************************************************************************/
    /**
     * crea una lista de los escoltas disponibles
     * @param nomcond Nombre del conductor
     * @throws SQLException si existe un error en la conculta.
     * @return void <tt>VistaCaravana</tt>
     */
    
    public void listarEscoltasSearch(String nomcond) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        datos = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_BUSCAR_ESCOLTAS_SRCH);
                st.setString(1, "%" + nomcond.toUpperCase() + "%");
                //////System.out.println("..................... SEARCH ESCOLTA: " + st.toString());
                rs = st.executeQuery();
                ResultSet m = rs;
                while (rs.next()){
                    e = new Escolta();
                    e.setPlaca(rs.getString("placa"));
                    e.setConductor(rs.getString("nomcond"));
                    e.setPropietario(rs.getString("nombre"));
                    e.setCedconductor(rs.getString("cedu"));
                    e.setSeleccionado(false);
                    datos.addElement(e);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR DESPACHOS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public Vector buscarEscoltaAsignado(String placa,String opcion) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector vecEscoltas = new Vector();
        String sql="";
        if (opcion.equals("planilla"))
            sql = SQL_BUS_ESCOLTAS_ASIGNADOS+"C.numpla = ? and cedula=P.conductor ) AS c1 " +
            "LEFT JOIN plarem plr ON ( c1.numpla=plr.numpla ) ";
        else if (opcion.equals("placa"))
            sql = SQL_BUS_ESCOLTAS_ASIGNADOS+"P.PLACA = ? and cedula=P.conductor ) AS c1 " +
            "LEFT JOIN plarem plr ON ( c1.numpla=plr.numpla ) ";
        
        /* AMATURANA
        sql +=
            ") AS c1 LEFT JOIN plarem plr ON ( c1.numpla=plr.numpla ) ";
         
        /*****************************/
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(sql);
                st.setString(1,placa);
                ////System.out.println("ST BUSC:"+st);
                rs = st.executeQuery();
                while (rs.next()){
                    e = new Escolta();
                    e.setPlaca(rs.getString("placa"));
                    e.setConductor(rs.getString("nombre"));
                    e.setPropietario(rs.getString("fecasignacion"));
                    e.setCedconductor(rs.getString("numcaravana"));
                    //e.setRazon_exclusion(rs.getString("numpla"));
                    /*  AMATURANA */
                    e.setNumpla(rs.getString("numpla"));
                    e.setRazon_exclusion(rs.getString("numrem"));
                    /********************************/
                    vecEscoltas.addElement(e);
                }
                
                ////////////////////////////////// AMATURANA 25.08.2006
                
                // Eliminando los posibles repetidos
                
                for( int i=0; i<vecEscoltas.size(); i++){
                    Escolta e0 = (Escolta) vecEscoltas.elementAt(i);
                    
                    for( int j=i+1; j<vecEscoltas.size(); j++){
                        Escolta e1 = (Escolta) vecEscoltas.elementAt(j);
                        
                        if( e0.getPlaca().compareTo(e1.getPlaca())==0 ){
                            vecEscoltas.remove(j);
                            j--;
                        }
                    }
                }
                
                
                ////////////////////////////////////////////////////////
                
            }
            sql = "";
            return vecEscoltas;
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR DESPACHOS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    
    
    public void actualizarEscoltaIngresoTrafico( String planilla, String valor) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update ingreso_trafico set escolta=? where planilla=?");
                st.setString(1, valor);
                st.setString(2, planilla);
                st.execute();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR INGRESO TRAFICO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    public boolean planillaAsignadaVehiculo(String planilla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean esta = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.SQL_PLANILLA_ASIGNADA);
                st.setString(1, planilla);
                rs = st.executeQuery();
                if (rs.next()) {
                    esta = true;
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR INGRESO TRAFICO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return esta;
    }
    /**
     * Retorna un vector de placas de escoltas asignadas a una planilla
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>Vector escoltas</tt>
     */
    
    public Vector getPlacaEscoltaAsignada(String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector placas = new Vector();
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_PLACAS_PLANIILLA);
                st.setString(1, numpla);
                rs = st.executeQuery();
                while (rs.next()){
                    placas.addElement(rs.getString(1));
                }
            }
        }catch(Exception e){
            //throw new SQLException("ERROR AL BUSCAR DESPACHOS" + e.getMessage()+"" + e.getErrorCode());
            e.printStackTrace();
        }finally{
            if(st != null){
                st.close();
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return placas;
    }
    
    
     /**
     * Metodo: listadoEscoltaSinGeneracion, permite buscar los escoltas que no se lengthhan generado facturas
     * @autor : Ing. jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listadoEscoltaSinGeneracion( String proveedor, String distrito ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String numpla;
        escoltaActual = null;
        try{
            st = this.crearPreparedStatement("SQL_LISTADO_ESCOLTA");   
            st.setString(1, proveedor);
            st.setString(2, "");
            rs=st.executeQuery();
            escoltaActual = new Vector();
            while( rs.next() ){
                EscoltaVehiculo e = new EscoltaVehiculo();
                e.setPlaca(         rs.getString("placa")   !=null?rs.getString("placa"):"");
                e.setD_o(           rs.getString("d_o")     !=null?rs.getString("d_o"):"");
                e.setFactura(       rs.getString("factura") !=null?rs.getString("factura"):"");
                e.setDestino(       rs.getString("destino") !=null?rs.getString("destino"):"");
                e.setOrigen(        rs.getString("origen")  !=null?rs.getString("origen"):"");
                e.setNumpla(        rs.getString("planilla")!=null?rs.getString("planilla"):"");
                e.setEscolta(       rs.getString("escolta") !=null?rs.getString("escolta"):"");
                e.setCreation_date( rs.getString("fecha")   !=null?rs.getString("fecha"):"");
                e.setTarifa(        rs.getDouble("tarifa"));
                e.setRegstatus(     rs.getString("estado")  !=null?rs.getString("estado"):"");
                escoltaActual.add(e);
            }
        }catch(SQLException e){
            throw new SQLException("Error en rutina listadoEscoltaSinGeneracion( ).... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_LISTADO_ESCOLTA");
        }
    }
    
     /**
     * Metodo: listadoEscoltaConFactura, permite buscar los escoltas que no se lengthhan generado facturas
     * @autor : Ing. jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listadoEscoltaConFactura( String proveedor, String distrito ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs         = null;
        String numpla;
        escoltaActual = null;
        try{
            st = this.crearPreparedStatement("SQL_LISTADO_ESCOLTA_FACTURA");   
            st.setString(1, proveedor);
            st.setString(2, "");
            rs=st.executeQuery();
            escoltaActual = new  Vector();
            while( rs.next() ){
                EscoltaVehiculo e = new EscoltaVehiculo();
                e.setPlaca(         rs.getString("placa")   !=null?rs.getString("placa"):"");
                e.setD_o(           rs.getString("d_o")     !=null?rs.getString("d_o"):"");
                e.setFactura(       rs.getString("factura") !=null?rs.getString("factura"):"");
                e.setDestino(       rs.getString("destino") !=null?rs.getString("destino"):"");
                e.setOrigen(        rs.getString("origen")  !=null?rs.getString("origen"):"");
                e.setNumpla(        rs.getString("planilla")!=null?rs.getString("planilla"):"");
                e.setEscolta(       rs.getString("escolta") !=null?rs.getString("escolta"):"");
                e.setCreation_date( rs.getString("fecha")   !=null?rs.getString("fecha"):"");
                e.setTarifa(        rs.getDouble("tarifa"));
                e.setRegstatus(     rs.getString("estado")  !=null?rs.getString("estado"):"");
                escoltaActual.add(e);
            }
        }catch(SQLException e){
            throw new SQLException("Error en rutina listadoEscoltaConFactura( ).... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_LISTADO_ESCOLTA_FACTURA");
        }
    }
    
     /**
     * Metodo: updateEscoltaVehiculo, permite actualizar los campos de escoltas vehiculos
     * @autor : Ing. Jose de la rosa
     * @param : objeto escolta vehiculo
     * @version : 1.0
     */
    public String updateEscoltaVehiculo(EscoltaVehiculo es) throws SQLException{
        PreparedStatement st = null;
        String sql = "";
        try{
            st = this.crearPreparedStatement("SQL_UPDATE_ESCOLTA");
            st.setString(1, es.getUsuario_asignacion_factura());
            st.setString(2, es.getD_o());
            st.setString(3, es.getFactura());
            st.setDouble(4, es.getTarifa());
            st.setString(5, es.getRegstatus());
            st.setString(6, es.getNumpla());
            st.setString(7, es.getPlaca());
            sql = st.toString();
        }catch(SQLException e){
            throw new SQLException("ERROR AL ACTUALIZAR LA ASIGNACION DEL ESCOLTA DEL VEHICULO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar("SQL_UPDATE_ESCOLTA");
        }
        return sql;
    }
    
    
     /**
     * Metodo: updateGeneracionEscoltaVehiculo, permite actualizar los campos de escoltas vehiculos
     * @autor : Ing. Jose de la rosa
     * @param : objeto escolta vehiculo
     * @version : 1.0
     */
    public String updateGeneracionEscoltaVehiculo(String usuario, String estado, String numpla, String placa) throws SQLException{
        PreparedStatement st = null;
        String sql = "";
        try{
            st = this.crearPreparedStatement("SQL_UPDATE_GENERACION");
            st.setString(1, usuario);
            st.setString(2, estado);
            st.setString(3, numpla);
            st.setString(4, placa);
            sql = st.toString();
        }catch(SQLException e){
            throw new SQLException("ERROR AL MODIFICAR LA GENERACION DEL ESCOLTA VEHICULO" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar("SQL_UPDATE_GENERACION");
        }
        return sql;
    }    
    
}
