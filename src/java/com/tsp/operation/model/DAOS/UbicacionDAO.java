/*
 * Nombre        UbicacionDAO.java
 * Autor         Ing. Jesus Cuesta
 * Modificado    Ing. Sandra Escalanate   
 * Fecha         02 Febrero 2006
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.*;

public class UbicacionDAO {
    
    Ubicacion ub;
    Vector ubicaciones;
    
    private String U_LISTAR = "SELECT	u.*,                                " +
    "                                   COALESCE ( cia.nombre, 'NO REGISTRA') AS nomcia,                               " +
    "                                   COALESCE ( tu.descripcion, 'NO REGISTRA') AS nomtipo,   " +
    "                               	COALESCE ( cont.nombre, 'NO REGISTRA') AS nomcontacto,   " +
    "                                   p.country_name AS nompais, " +
    "                                   COALESCE ( e.department_name, 'NO REGISTRA') AS nomestado, " +
    "                               	get_nombreciudad ( u.ciudad ) AS nomciudad, " +
    "                               	COALESCE ( get_nombreciudad ( u.torigen ), 'NO REGISTRA' ) AS nomtorigen, " +
    "                               	COALESCE ( get_nombreciudad ( u.tdestino ), 'NO REGISTRA' ) AS nomtdestino " +
    "                           FROM	ubicacion u  " +
    "                                           	LEFT JOIN nit cia ON  ( u.cia = cia.cedula AND cia.tipo_iden = 'NIT' )   " +
    "                               			LEFT JOIN tablagen tu ON  ( u.tipo = tu.table_code AND tu.table_type = 'TUBICACION' )   " +
    "                                               	LEFT JOIN nit cont on ( u.contacto = cont.cedula AND cont.tipo_iden = 'CED')     	  " +
    "                                                   LEFT JOIN pais p ON ( u.pais = p.country_code) " +
    "                                                   LEFT JOIN estado e ON ( u.estado = e.department_code ) " +
    "                           WHERE	u.rec_status != 'A'  "+
    "                           ORDER BY    tu.descripcion," +
    "                                       u.descripcion";


private String U_UPDATE = " UPDATE  ubicacion " +
    "                           SET     descripcion = ?, " +
    "                                   cia = ?, " +
    "                                   tipo = ?, " +
    "                                   contacto = ?, " +
    "                                   torigen = ?," +
    "                                   tdestino = ?, " +
    "                                   relacion = ?," +
    "                                   tiempo = ?," +
    "                                   distancia = ?," +
    "                                   tel1 = ?, " +
    "                                   tel2 = ?, " +
    "                                   tel3 = ?, " +
    "                                   fax = ?," +
    "                                   base = ?, " +
    "                                   last_update = 'now()', " +
    "                                   user_update = ?, " +
    "                                   rec_status='', " +
    "                                   modalidad = ?,  "+
    "                                   estado = ?, "+
    "                                   direccion = ? "+
    "                           WHERE   cod_ubicacion = ? " +
    "                                   AND dstrct = ?";

private String U_BUSCAR = " SELECT	u.*," +
    "                           	COALESCE ( cia.nombre, 'NO REGISTRA') AS nomcia,  " +
    "                           	COALESCE ( tu.descripcion, 'NO REGISTRA') AS nomtipo,  " +
    "                           	COALESCE ( cont.nombre, 'NO REGISTRA') AS nomcontacto,  " +
    "                                   p.country_name AS nompais," +    
    "                                   COALESCE ( e.department_name, 'NO REGISTRA') AS nomestado," +
    "                           	get_nombreciudad ( u.ciudad ) AS nomciudad," +
    "                           	COALESCE ( get_nombreciudad ( u.torigen ), 'NO REGISTRA' ) AS nomtorigen," +
    "                           	COALESCE ( get_nombreciudad ( u.tdestino ), 'NO REGISTRA' ) AS nomtdestino" +
    "                           FROM	ubicacion u " +
    "                                           	LEFT JOIN nit cia ON  ( u.cia = cia.cedula AND cia.tipo_iden = 'NIT' )   " +
    "                               			LEFT JOIN tablagen tu ON  ( u.tipo = tu.table_code AND tu.table_type = 'TUBICACION' )   " +
    "                                               	LEFT JOIN nit cont on ( u.contacto = cont.cedula AND cont.tipo_iden = 'CED')     	  " +
    "                                                   LEFT JOIN pais p ON ( u.pais = p.country_code)" +
    "                                                   LEFT JOIN estado e ON ( u.estado = e.department_code )" +
    "                           WHERE	u.rec_status = ''   " +
    "                               	AND u.cod_ubicacion = ?  " +
    "                                   AND u.dstrct  = ?     	";
    
    private String U_INSERT = "INSERT INTO ubicacion (  cod_ubicacion, descripcion, " +
    "                                                   cia, tipo, contacto, " +
    "                                                   pais, estado, ciudad, " +
    "                                                   direccion, torigen, tdestino, " +
    "                                                   relacion, tiempo, distancia, " +
    "                                                   tel1, tel2, tel3, fax, dstrct, " +
    "                                                   base, rec_status, last_update, " +
    "                                                   user_update, creation_date, " +
    "                                                   creation_user, modalidad)" +
                              "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'', 'now()', ?, 'now()', ?,?)";
    
    private String U_CONSULTAR = "  SELECT  u.*," +
    "                                       COALESCE ( cia.nombre, 'NO REGISTRA') AS nomcia,  " +
    "                                       COALESCE ( tu.descripcion, 'NO REGISTRA') AS nomtipo,  " +
    "                                       COALESCE ( cont.nombre, 'NO REGISTRA') AS nomcontacto,  " +
    "                                       p.country_name AS nompais," +
    "                                       e.department_name AS nomestado," +
    "                                       get_nombreciudad ( u.ciudad ) AS nomciudad," +
    "                                       COALESCE ( get_nombreciudad ( u.torigen ), 'NO REGISTRA' ) AS nomtorigen," +
    "                                       COALESCE ( get_nombreciudad ( u.tdestino ), 'NO REGISTRA' ) AS nomtdestino" +
    "                               FROM    ubicacion u " +
    "                                           JOIN ( SELECT nombre, cedula FROM nit WHERE nombre LIKE ? AND tipo_iden = 'NIT') cia ON  ( u.cia = cia.cedula )  " +
    "                                           JOIN ( SELECT descripcion, table_code, table_type FROM tablagen WHERE descripcion LIKE ?  ) tu ON  ( u.tipo = tu.table_code AND tu.table_type = 'TUBICACION' )  " +
    "                                           JOIN ( SELECT nombre, cedula FROM nit WHERE nombre LIKE ? ) cont ON ( u.contacto = cont.cedula)     	 " +
    "                                           JOIN ( SELECT country_code, country_name FROM pais  WHERE country_name LIKE ? ) p ON ( u.pais = p.country_code)" +
    "                                           JOIN ( SELECT department_code, department_name FROM estado  WHERE department_name LIKE ? ) e ON ( u.estado = e.department_code )" +
    "                                           JOIN ( SELECT nomciu, codciu FROM ciudad  WHERE nomciu LIKE ? ) c ON ( u.ciudad = c.codciu )" +
    "                               WHERE	u.rec_status != 'A'   " +
    "                                           AND u.cod_ubicacion LIKE ? " +
    "                                           AND u.descripcion LIKE ? " +
    "                               ORDER BY    tu.descripcion," +
    "                                           u.descripcion";    
   
    private String U_ANULAR = " UPDATE  ubicacion" +
    "                           SET     rec_status='A', " +
    "                                   last_update = 'now()', " +
    "                                   user_update = ? " +
    "                           WHERE   cod_ubicacion = ? " +
    "                                   AND dstrct=?";
    
    private String U_EXISTE = " SELECT  cod_ubicacion  " +
                              " FROM    ubicacion " +
                              " WHERE   cod_ubicacion = ? " +
                              "         AND dstrct = ? " +
                              "         AND rec_status='' ";
    
    private String U_EXISTEA = "SELECT  cod_ubicacion  " +
    "                           FROM    ubicacion " +
    "                           WHERE   cod_ubicacion = ? " +
    "                                   AND dstrct = ? " +
    "                                   AND rec_status='A' ";
    
    //jdelarosa 19-01-2006
    private String SQL_UBICACION="SELECT u.cod_ubicacion, u.descripcion " +
        "FROM cliente_ubicacion c, ubicacion u , remesa r " +
        "WHERE c.cod_ubicacion = u.cod_ubicacion AND c.cedula=r.cliente AND numrem=? AND c.reg_status!='A' " +
        "ORDER BY cod_ubicacion";
    
    /** Creates a new instance of UbicacionDAO */
    public UbicacionDAO() {
    }
    
     /**
     * Metodo <tt>agregarUbicacion()</tt>, registra una ubicacion en el sistema
     * @autor : Ing. Jesus Cuestas     
     * @version : 1.0
     */
    public void agregarUbicacion()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(U_INSERT);
                st.setString(1, ub.getCod_ubicacion());
                st.setString(2, ub.getDescripcion());
                st.setString(3, ub.getCia());                
                st.setString(4, ub.getTipo());
                st.setString(5, ub.getContacto());
                st.setString(6, ub.getPais());
                st.setString(7, ub.getEstado());
                st.setString(8, ub.getCiudad());
                st.setString(9, ub.getDireccion());
                st.setString(10, ub.getTorigen());
                st.setString(11, ub.getTdestino());
                st.setString(12, ub.getRelacion());
                st.setDouble(13, ub.getTiempo());
                st.setDouble(14, ub.getDistancia());
                st.setString(15, ub.getTel1());
                st.setString(16, ub.getTel2());
                st.setString(17, ub.getTel3());
                st.setString(18, ub.getFax());
                st.setString(19, ub.getDstrct());
                st.setString(20, ub.getBase());
                st.setString(21, ub.getUser_update());
                st.setString(22, ub.getCreation_user());
                st.setString(23, ub.getModalidad());//sescalante 01.02.06
                ////System.out.println("-----"+st);
                st.executeUpdate();                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DE LA UBICACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo <tt>consultarUbicaciones</tt>, busca ubicaciones por datos ingresados
     * @autor : Ing. Jesus Cuestas     
     * @version : 1.0
     */
    public void consultarUbicaciones()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(U_CONSULTAR);
                st.setString(1, ub.getNomcia()+"%");
                st.setString(2, ub.getNomtipo()+"%");
                st.setString(3, ub.getNomcontacto()+"%");                
                st.setString(4, ub.getNompais()+"%");
                st.setString(5, ub.getNomestado()+"%");
                st.setString(6, ub.getNomciudad()+"%");
                st.setString(7, ub.getCod_ubicacion()+"%");
                st.setString(8, ub.getDescripcion()+"%"); 
                
                //////System.out.println("---" + st);
                rs= st.executeQuery();
                ubicaciones = new Vector();
                while(rs.next()){
                    ub = new Ubicacion();
                    ub.setCod_ubicacion(rs.getString("cod_ubicacion"));
                    ub.setDescripcion(rs.getString("descripcion"));                                        
                    ub.setNomcia(rs.getString("nomcia"));
                    ub.setNomtipo(rs.getString("nomtipo"));
                    ub.setNomcontacto(rs.getString("nomcontacto"));
                    ub.setNompais(rs.getString("nompais"));                   
                    ub.setNomestado(rs.getString("nomestado"));                    
                    ub.setNomciudad(rs.getString("nomciudad"));
                    ub.setDireccion(rs.getString("direccion"));                    
                    ub.setTel1(rs.getString("tel1"));
                    ub.setTel2(rs.getString("tel2"));
                    ub.setTel3(rs.getString("tel3"));
                    ub.setFax(rs.getString("fax"));
                    ub.setDstrct(rs.getString("dstrct"));
                    ubicaciones.add(ub);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE LA UBICACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Getter for property ub.
     * @return Value of property ub.
     */
    public com.tsp.operation.model.beans.Ubicacion getUb() {
        return ub;
    }
    
    /**
     * Setter for property ub.
     * @param ub New value of property ub.
     */
    public void setUb(com.tsp.operation.model.beans.Ubicacion ub) {
        this.ub = ub;
    }
    
    /**
     * Getter for property Ubicaciones.
     * @return Value of property Ubicaciones.
     */
    public java.util.Vector getUbicaciones() {
        return ubicaciones;
    }
    
    /**
     * Setter for property Ubicaciones.
     * @param Ubicaciones New value of property Ubicaciones.
     */
    public void setUbicaciones(java.util.Vector ubicaciones) {
        this.ubicaciones = ubicaciones;
    }
    
    /**
     * Metodo <tt>listarUbicaciones</tt>, obtiene todas las ubicaciones
     * @autor : Ing. Jesus Cuestas     
     * @version : 1.0
     */
    public void listarUbicaciones()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(U_LISTAR);                
                rs= st.executeQuery();
                ubicaciones = new Vector();
                while(rs.next()){
                    ub = new Ubicacion();
                    ub.setCod_ubicacion(rs.getString("cod_ubicacion"));
                    ub.setDescripcion(rs.getString("descripcion"));                  
                    ub.setNomcia(rs.getString("nomcia"));
                    ub.setNomtipo(rs.getString("nomtipo"));
                    ub.setNomcontacto(rs.getString("nomcontacto"));                    
                    ub.setNompais(rs.getString("nompais"));                   
                    ub.setNomestado(rs.getString("nomestado"));                    
                    ub.setNomciudad(rs.getString("nomciudad"));
                    ub.setDireccion(rs.getString("direccion"));
                    ub.setTel1(rs.getString("tel1"));
                    ub.setTel2(rs.getString("tel2"));
                    ub.setTel3(rs.getString("tel3"));
                    ub.setFax(rs.getString("fax"));
                    ub.setDstrct(rs.getString("dstrct"));
                    ubicaciones.add(ub);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LA UBICACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo <tt>buscarUbicacion()</tt>, obtiene una ubicacion dado un codigo y distrito
     * @autor : Ing. Jesus Cuestas     
     * @param: codigo de la ubicacion y distrito
     * @version : 1.0
     */
    public void buscarUbicacion(String cod, String dstrct)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(U_BUSCAR);
                st.setString(1,cod);                
                st.setString(2,dstrct);
                rs= st.executeQuery();
                if(rs.next()){
                    ub = new Ubicacion();
                    ub.setCod_ubicacion(rs.getString("cod_ubicacion"));
                    ub.setDescripcion(rs.getString("descripcion"));
                    ub.setCia(rs.getString("cia"));  
                    ub.setNomcia(rs.getString("nomcia"));
                    ub.setTipo(rs.getString("tipo"));
                    ub.setNomtipo(rs.getString("nomtipo"));
                    ub.setContacto(rs.getString("contacto"));
                    ub.setNomcontacto(rs.getString("nomcontacto"));
                    ub.setPais(rs.getString("pais"));
                    ub.setNompais(rs.getString("nompais"));
                    ub.setEstado(rs.getString("estado"));
                    ub.setNomestado(rs.getString("nomestado"));
                    ub.setCiudad(rs.getString("ciudad"));
                    ub.setNomciudad(rs.getString("nomciudad"));
                    ub.setDireccion(rs.getString("direccion"));
                    ub.setTorigen(rs.getString("torigen"));
                    ub.setNomtorigen(rs.getString("nomtorigen"));
                    ub.setTdestino(rs.getString("tdestino"));
                    ub.setNomtdestino(rs.getString("nomtdestino")); 
                    ub.setTiempo(rs.getDouble("tiempo"));
                    ub.setDistancia(rs.getDouble("distancia"));
                    ub.setTel1(rs.getString("tel1"));
                    ub.setTel2(rs.getString("tel2"));
                    ub.setTel3(rs.getString("tel3"));
                    ub.setFax(rs.getString("fax"));
                    ub.setDstrct(rs.getString("dstrct"));
                    ub.setModalidad(rs.getString("modalidad"));
                    ub.setRelacion(rs.getString("relacion"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA UBICACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo <tt>modificarUbicacion</tt>, actualizacion de un registro ubicacion
     * @autor : Ing. Jesus Cuestas          
     * @version : 1.0
     */
    public void modificarUbicacion()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(U_UPDATE);
                st.setString(1, ub.getDescripcion());
                st.setString(2, ub.getCia());                
                st.setString(3, ub.getTipo());
                st.setString(4, ub.getContacto());
                st.setString(5, ub.getTorigen());
                st.setString(6, ub.getTdestino());
                st.setString(7, ub.getRelacion());
                st.setDouble(8, ub.getTiempo());
                st.setDouble(9, ub.getDistancia());
                st.setString(10, ub.getTel1());
                st.setString(11, ub.getTel2());
                st.setString(12, ub.getTel3());
                st.setString(13, ub.getFax());                
                st.setString(14, ub.getBase());
                st.setString(15, ub.getUser_update());
                st.setString(16, ub.getModalidad());//sescalante 02.02.06
                st.setString(17, ub.getEstado());//sescalante 02.03.06
                st.setString(18, ub.getDireccion());//sescalante 02.03.06
                st.setString(19, ub.getCod_ubicacion());
                st.setString(20, ub.getDstrct());
                ////System.out.println("-----"+st);
                st.executeUpdate();        
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACION DE LA UBICACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo <tt>anularUbicacion</tt>, anulacion de un registro ubicacion
     * @autor : Ing. Jesus Cuestas          
     * @version : 1.0
     */
    public void anularUbicacion()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(U_ANULAR);    
                st.setString(1, ub.getUser_update());
                st.setString(2, ub.getCod_ubicacion());                
                st.setString(3, ub.getDstrct());
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LA UBICACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo <tt>existeUbicacion</tt>, verifica la existencia de una ubicacion
     * @autor : Ing. Jesus Cuestas          
     * @param codigo de la ubicacion y distrito
     * @version : 1.0
     */
    public boolean existeUbicacion(String cod, String cia)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;//no existe
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(U_EXISTE);
                st.setString(1,cod);
                st.setString(2,cia);
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;//si existe
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA UBICACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Metodo <tt>existeUbicacionAnulada</tt>, verifica la existencia de una ubicacion anulada
     * @autor : Ing. Jesus Cuestas          
     * @param codigo de la ubicacion y distrito
     * @version : 1.0
     */
    public boolean existeUbicacionAnulada(String cod, String cia)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;//no existe
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(U_EXISTEA);
                st.setString(1,cod);
                st.setString(2,cia);
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;//si existe
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA UBICACION " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
        
     /**
     * Metodo: listarUbicacionesClientes, permite listar las ubicaciones de los clientes
     * @autor : Ing. Jose De La Rosa
     * @param : String remesa
     * @version : 1.0
     */
    public void listarUbicacionesClientes( String remesa )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_UBICACION);
                st.setString(1,remesa);
                rs= st.executeQuery();
                ubicaciones = new Vector();
                while(rs.next()){
                    ub = new Ubicacion();
                    ub.setCod_ubicacion(rs.getString("cod_ubicacion"));
                    ub.setDescripcion(rs.getString("descripcion"));                  
                    ubicaciones.add(ub);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE UBICACIONES POR CLIENTES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
}