/*
 * Esquema_formatoDAO.java
 *
 * Created on 19 de octubre de 2006, 08:33 AM
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  EQUIPO13
 */
public class Esquema_formatoDAO extends MainDAO{
    private Esquema_formato esquema;
    private Vector Vecesquema;
    /** Creates a new instance of Esquema_formatoDAO */
    public Esquema_formatoDAO () {
        super ( "Esquema_formatoDAO.xml" );
    }
    
    /**
     * Getter for property esquema.
     * @return Value of property esquema.
     */
    public Esquema_formato getEsquema () {
        return esquema;
    }
    
    /**
     * Setter for property esquema.
     * @param esquema New value of property esquema.
     */
    public void setEsquema (Esquema_formato esquema) {
        this.esquema = esquema;
    }
    
    /**
     * Getter for property Vecesquema.
     * @return Value of property Vecesquema.
     */
    public Vector getVecesquema () {
        return Vecesquema;
    }
    
    /**
     * Setter for property Vecesquema.
     * @param Vecesquema New value of property Vecesquema.
     */
    public void setVecesquema (Vector Vecesquema) {
        this.Vecesquema = Vecesquema;
    }
    
    
    /**
     * Metodo: insertEsquema_formato, permite ingresar un registro a la tabla esquema_formato.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertEsquema_formato () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_INSERT");
            st.setString (1,esquema.getCodigo_programa ());
            st.setString (2,esquema.getNombre_campo ());
            st.setInt    (3,esquema.getPosicion_inicial ());
            st.setInt    (4,esquema.getPosicion_final ());
            st.setInt    (5,esquema.getOrden ());
            st.setString (6,esquema.getTitulo ());
            st.setString (7,esquema.getDistrito ());
            st.setString (8,esquema.getUsuario ());
            st.setString (9,esquema.getBase ());
            st.setString (10,esquema.getTipo ());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL INGRESAR UN ESQUEMA FORMATO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_INSERT");
        }
    }
    
    /**
     * Metodo: searchEsquema_formato, permite buscar un formato de esquema dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : codigo del programa y distrito
     * @version : 1.0
     */
    public void searchEsquema_formato ( String codigo, String distrito,String nombre )throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        esquema = null;
        try {
            st = crearPreparedStatement ("SQL_SERCH");
            st.setString (1,codigo);
            st.setString (2,distrito);
            st.setString (3,nombre);
            //System.out.println("sql "+st);
            rs= st.executeQuery ();
            if(rs.next ()){
                esquema = new Esquema_formato();
                esquema.setCodigo_programa  ( rs.getString("cod_programa")!=null?rs.getString("cod_programa"):"" );
                esquema.setNombre_campo     ( rs.getString("nom_campo")!=null?rs.getString("nom_campo"):"" );
                esquema.setPosicion_inicial ( rs.getInt("pos_inicial") );
                esquema.setPosicion_final   ( rs.getInt("pos_final") );
                esquema.setOrden            ( rs.getInt("orden") );
                esquema.setTitulo           ( rs.getString("titulo")!=null?rs.getString("titulo"):"" );
                esquema.setNombre_programa  ( rs.getString("nom_programa")!=null?rs.getString("nom_programa"):"" );
                esquema.setNom_campo        ( rs.getString("nombr_campo")!=null?rs.getString("nombr_campo"):"" );
                esquema.setTipo             ( rs.getString("tipo")!=null?rs.getString("tipo"):"" );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UN ESQUEMA FORMATO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH");
        }
    }
    
    /**
     * Metodo: existEsquema_formato, permite buscar un formato de esquema dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : codigo del programa y distrito.
     * @version : 1.0
     */
    public boolean existEsquema_formato ( String codigo, String distrito, String nombre ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_SERCH");
            st.setString (1,codigo);
            st.setString (2,distrito);
            st.setString (3,nombre);
            rs = st.executeQuery ();
            if(rs.next ())
                sw = true;
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR SI EXISTE EL ESQUEMA FORMATO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH");
        }
        return sw;
    }
    
    /**
     * Metodo: listEsquema_formato, permite listar todas los formatos de descuento
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listEsquema_formato (String distrito, String codigo, String nombre, String titulo)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Vecesquema = null;
        try {
            st = crearPreparedStatement ("SQL_LIST");
            st.setString (1,distrito);
            st.setString (2,codigo+"%");
            st.setString (3,nombre+"%");
            st.setString (4,titulo+"%");
            rs= st.executeQuery ();
            Vecesquema = new Vector ();
            while(rs.next ()){
                Vecesquema.add ( esquema.load (rs) );
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL LISTAR LOS ESQUEMA FORMATO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_LIST");
        }
    }
    
    /**
     * Metodo: updateEsquema_formato, permite actualizar un esquema formato dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : 
     * @version : 1.0
     */
    public void updateEsquema_formato () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_UPDATE");
            st.setInt    (1,esquema.getPosicion_inicial ());
            st.setInt    (2,esquema.getPosicion_final ());
            st.setInt    (3,esquema.getOrden ());
            st.setString (4,esquema.getTitulo ());
            st.setString (5,esquema.getUsuario ());
            st.setString (6,esquema.getTipo ());
            st.setString (7,esquema.getDistrito ());
            st.setString (8,esquema.getCodigo_programa ());
            st.setString (9,esquema.getNombre_campo ());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LOS ESQUEMA FORMATO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_UPDATE");
        }
    }
    
    /**
     * Metodo: anularEsquema_formato, permite anular un esquema formato dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : 
     * @version : 1.0
     */
    public void anularEsquema_formato () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_ANULAR");
            st.setString (1,esquema.getUsuario ());
            st.setString (2,esquema.getDistrito ());
            st.setString (3,esquema.getCodigo_programa ());
            st.setString (4,esquema.getNombre_campo ());
            
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ANULAR LOS ESQUEMA FORMATO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_ANULAR");
        }
    }
    
    /**
     * Metodo: rangoEsquema_formato, permite buscar si se puede ingresar los valores iniciales y finales dentro de lo rangos existentes.
     * @autor : Ing. Jose de la rosa
     * @param : codigo del programa, distrito, posicion inicial y posicion final.
     * @version : 1.0
     */
    public boolean rangoEsquema_formato ( String distrito, String codigo, String campo, int ini, int fin ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_RANGO");
            st.setString (1,distrito);
            st.setString (2,codigo);
            st.setString (3,campo);
            st.setInt    (4,ini);
            st.setInt    (5,fin);
            rs = st.executeQuery ();
            if(rs.next ())
                sw = true;
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR SI EXISTE RANGO EL ESQUEMA FORMATO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_RANGO");
        }
        return sw;
    }
    
    /**
     * Metodo: ordenEsquema_formato, permite buscar si se existe el orden en otro registro
     * @autor : Ing. Jose de la rosa
     * @param : codigo del programa, distrito, posicion inicial y posicion final.
     * @version : 1.0
     */
    public boolean ordenEsquema_formato ( String distrito, String codigo, String campo, int orden ) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_ORDEN");
            st.setString (1,distrito);
            st.setString (2,codigo);
            st.setString (3,campo);
            st.setInt    (4,orden);
            rs = st.executeQuery ();
            if(rs.next ())
                sw = true;
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR SI EXISTE ORDEN EL ESQUEMA FORMATO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_ORDEN");
        }
        return sw;
    }
    
}
