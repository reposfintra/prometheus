/************************************************************************
 * Nombre clase: ProductoDAO.java                                       *
 * Descripci�n: Clase que maneja las consultas de los productos.        *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: Created on 17 de octubre de 2005, 10:44 AM                    *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class ProductoDAO {
    private Producto producto;
    private Vector productos;
    private final String SQL_INSERT = "Insert into producto (codigo, descripcion, unidad, rec_status, last_update, user_update, creation_date, creation_user, base, distrito, codcli ) values (?,?,?, '' , 'now()', ?, 'now()', ?,?,?,?)";
    private final String SQL_SERCH = "select * from producto where codigo=? and distrito = ? and rec_status != 'A' AND codcli = ?";
    private final String SQL_LIST = "select * from producto where rec_status != 'A' order by codigo";
    private final String SQL_UPDATE = "update producto set descripcion=?, unidad=?, last_update='now()', user_update=?, rec_status='' where codcli = ? AND codigo = ? AND distrito=?";
    private final String SQL_ANULAR = "update producto set rec_status='A', last_update='now()', user_update = ? where codcli = ? AND codigo = ? AND distrito=?";
    private final String SQL_DETALLE = "Select * from producto where codigo like ? and descripcion like ? and unidad like ? AND codcli LIKE ? AND rec_status != 'A' order by codcli,unidad";
     /*
      *Este query busca una lista de productos disponibles en una ubicacion  dada 
      *en la tabla de inventario_discrepancia. -- kreales
      */
    private final String SQL_PRODUCTOS_UBICACION = "select 	id.cod_producto, " +
    "    	p.descripcion, " +
    "    	cantidad_original::numeric(5,0), " +
    "    	d.nota_debito, " +
    "    	d.nota_credito, " +
    "    	d.numpla, " +
    "           d.num_discre, " +
    "           id.creacion_discrepancia " +
    "     from	inventario_discrepancia id, " +
    "    	producto p, " +
    "    	discrepancia d ," +
    "           discrepancia_producto dp" +
    "     where 	id.codcli = ? " +
    "    	and id.ubicacion = ? " +
    "    	and id.numpla_despacho='' " +
    "    	and p.codigo = id.cod_producto " +
    "    	and d.num_discre=id.num_discre" +
    "           and dp.distrito=id.dstrct" +
    "		and dp.num_discre = id.num_discre" +
    "		and dp.numpla = id.numpla" +
    "		and dp.cod_producto = id.cod_producto" +
    "           and dp.cod_cdiscrepancia not in (select table_code from tablagen where table_type = 'NOINV') ORDER BY p.descripcion";
    
    private final String SQL_TODOSPRODUCTOS_UBICACION =  "select 	id.cod_producto, " +
    "    	p.descripcion, " +
    "    	cantidad_original::numeric(5,0), " +
    "    	d.nota_debito, " +
    "    	d.nota_credito, " +
    "    	d.numpla, " +
    "           d.num_discre, " +
    "           id.creacion_discrepancia " +
    "     from	inventario_discrepancia id, " +
    "    	producto p, " +
    "    	discrepancia d ," +
    "           discrepancia_producto dp" +
    "     where 	id.codcli = ? " +
    "    	and id.ubicacion = ? " +
    "           and (id.numpla_despacho='' or id.numpla_despacho=? )    " +
    "    	and p.codigo = id.cod_producto " +
    "    	and d.num_discre=id.num_discre" +
    "           and dp.distrito=id.dstrct" +
    "		and dp.num_discre = id.num_discre" +
    "		and dp.numpla = id.numpla" +
    "		and dp.cod_producto = id.cod_producto" +
    "           and dp.cod_cdiscrepancia not in (select table_code from tablagen where table_type = 'NOINV')";
    /** Creates a new instance of ProductoDAO */
    public ProductoDAO() {
    }
    public Producto getProducto() {
        return producto;
    }
    
    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    
    public Vector getProductos() {
        return productos;
    }
    
    public void setProductos(Vector productos) {
        this.productos = productos;
    }
    
     /**
     * M�todo que inserta una producto a la BD
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void insertProducto() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_INSERT);
                st.setString(1, producto.getCodigo());
                st.setString(2, producto.getDescripcion());
                st.setString(3, producto.getUnidad());
                st.setString(4, producto.getUsuario_modificacion());
                st.setString(5, producto.getUsuario_creacion());
                st.setString(6, producto.getBase());
                st.setString(7, producto.getDistrito());
                st.setString(8, producto.getCliente ());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DE LOS PRODUCTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public void searchProducto(String cod, String distrito, String cliente)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        producto = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_SERCH);
                st.setString(1,cod);
                st.setString(2,distrito);
                st.setString(3,cliente);
                rs= st.executeQuery();
                while(rs.next()){
                    producto = new Producto();
                    producto.setCodigo(rs.getString("codigo"));
                    producto.setDescripcion(rs.getString("descripcion"));
                    producto.setUnidad(rs.getString("unidad"));
                    producto.setCliente(rs.getString("codcli"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LOS PRODUCTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
     /**
     * M�todo que nos permite saber si el producto existe o no
     * @autor.......Jose de la rosa
     * @param.......Recibe un codigo de producto
     * @throws......SQLException
     * @version.....1.0.
     **/
    public boolean existProducto(String cod, String distrito, String cliente)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_SERCH);
                st.setString(1,cod);
                st.setString(2,distrito);
                st.setString(3,cliente);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DEL PRODUCTO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
     /**
     * M�todo que nos permite obtener un listado de todos los productos existentes en la BD
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void listProducto()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LIST);
                rs= st.executeQuery();
                productos = new Vector();
                while(rs.next()){
                    producto = new Producto();
                    producto.setCodigo(rs.getString("codigo"));
                    producto.setDescripcion(rs.getString("descripcion"));
                    producto.setUnidad(rs.getString("unidad"));
                    producto.setCliente(rs.getString("codcli"));
                    productos.add(producto);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LOS PRODUCTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
     /**
     * M�todo que permite modificar una producto a la BD
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void updateProducto() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_UPDATE);
                st.setString(1, producto.getDescripcion());
                st.setString(2, producto.getUnidad());
                st.setString(3, producto.getUsuario_modificacion());
                st.setString(4, producto.getCliente());
                st.setString(5, producto.getCodigo());
                st.setString(6, producto.getDistrito ());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LOS PRODUCTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
     /**
     * M�todo que permite anular una producto a la BD
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void anularProducto() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_ANULAR);
                st.setString(1, producto.getUsuario_modificacion());
                st.setString(2, producto.getCliente());
                st.setString(3, producto.getCodigo());
                st.setString(4, producto.getDistrito ());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LOS PRODUCTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }

     /**
     * M�todo que nos permite buscar unos productos de acuerdo a unos filtros.
     * @autor.......Jose de la rosa
     * @param.......Recibe un codigo de producto, una descrpcion, un codigo unidad y un codigo cliente
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector searchDetalleProductos(String cod, String desc, String uni, String cli) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
      //  productos=null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                st = con.prepareStatement(SQL_DETALLE);
                st.setString(1, cod+"%");
                st.setString(2, desc+"%");
                st.setString(3, uni+"%");
                st.setString(4, cli+"%");
                rs = st.executeQuery();
                productos = new Vector();
                while(rs.next()){
                    producto = new Producto();
                    producto.setCodigo(rs.getString("codigo"));
                    producto.setDescripcion(rs.getString("descripcion"));
                    producto.setUnidad(rs.getString("unidad"));
                    producto.setCliente(rs.getString("codcli"));
                    productos.add(producto);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PRODUCTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return productos;
    }
    
     /**
     * M�todo que nos permite obtener un listado de todos los productos existentes en la BD
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector listarProductos()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
      //  productos = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_LIST);
                
                rs = st.executeQuery();
                productos = new Vector();
                
                while(rs.next()){
                    producto = new Producto();
                    producto.setCodigo(rs.getString("codigo"));
                    producto.setDescripcion(rs.getString("descripcion"));
                    producto.setUnidad(rs.getString("unidad"));
                    producto.setCliente(rs.getString("codcli"));
                    productos.add(producto);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PRODUCTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return productos;
    }
    /**
     * M�todo que busca una lista de productos dado un
     * codigo de cliente y una ubicacion que no han sido despachados
     * @param.......cliente Recibe un codigo de cliente
     * @param.......ubicacion Recibe un codigo de ubicacion
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void listarProductos(String cliente, String ubicacion )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_PRODUCTOS_UBICACION);
                st.setString(1, cliente);
                st.setString(2, ubicacion);
                ////System.out.println("Query "+st.toString());
                rs = st.executeQuery();
                productos = new Vector();
                
                while(rs.next()){
                    producto = new Producto();
                    producto.setCodigo(rs.getString("cod_producto")+rs.getString("creacion_discrepancia"));
                    producto.setDescripcion(rs.getString("descripcion"));
                    producto.setUnidad(rs.getString("cantidad_original"));
                    producto.setUtilizado(false);
                    producto.setNota_debito(rs.getString("nota_debito"));
                    producto.setDiscrepancia(rs.getString("num_discre"));
                    productos.add(producto);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR LISTAR PRODUCTOS POR CLIENTE Y UBICACION" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
     /**
     * M�todo que busca una lista de productos dado un
     * codigo de cliente y una ubicacion sin importar si estan utilizados
     * @param.......cliente Recibe un codigo de cliente
     * @param.......ubicacion Recibe un codigo de ubicacion
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void listarTodosProductos(String cliente, String ubicacion, String numpla )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_TODOSPRODUCTOS_UBICACION);
                st.setString(1, cliente);
                st.setString(2, ubicacion);
                st.setString(3, numpla);
                ////System.out.println("Query "+st.toString());
                rs = st.executeQuery();
                productos = new Vector();
                
                while(rs.next()){
                    producto = new Producto();
                    producto.setCodigo(rs.getString("cod_producto")+rs.getString("creacion_discrepancia"));
                    producto.setDescripcion(rs.getString("descripcion"));
                    producto.setUnidad(rs.getString("cantidad_original"));
                    producto.setUtilizado(false);
                    producto.setNota_debito(rs.getString("nota_debito"));
                    producto.setDiscrepancia(rs.getString("num_discre"));
                    productos.add(producto);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR LISTAR PRODUCTOS POR CLIENTE Y UBICACION" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     * M�todo que busca una lista de productos dado un
     * codigo de cliente y una ubicacion que no han sido despachados
     * @param.......cliente Recibe un codigo de cliente
     * @param.......ubicacion Recibe un codigo de ubicacion
     * @autor.......David Pi�a
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void listarProductosUbicacion(String cliente, String ubicacion )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_PRODUCTOS_UBICACION);
                st.setString(1, cliente);
                st.setString(2, ubicacion);
                ////System.out.println("Query "+st.toString());
                rs = st.executeQuery();
                productos = new Vector();
                
                while(rs.next()){
                    producto = new Producto();
                    producto.setCodigo(rs.getString("cod_producto"));
                    producto.setDescripcion(rs.getString("descripcion"));
                    producto.setUnidad(rs.getString("cantidad_original"));
                    producto.setUtilizado(false);
                    producto.setNota_debito(rs.getString("nota_debito"));
                    producto.setDiscrepancia(rs.getString("num_discre"));
                    productos.add(producto);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR LISTAR PRODUCTOS POR CLIENTE Y UBICACION" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
}
