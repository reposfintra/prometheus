/***************************************
 * Nombre Clase ............. OpDAO.java
 * Descripci�n  .. . . . . .  Generamos Las OP de OCs.
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  19/10/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.model.DAOS;


import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.TasaService;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;



public class OpDAO extends MainDAO{
    
    private static final String TIPO_DOC       = "010";              //FACTURA
    private static final String PRE_DOC        = "OP";               //prefijo de la OP
    private static final String APROBADOR      = "ADMIN";            //valor definido para OPs automaticas
    private static final String DESC_OP        = "LIQUIDACION OC: "; //prefijo de la descripcion de la OP
    public  static final String COLOMBIA       = "FINV";
    public  static final String RETEFUENTE     = "RFTE";             //tipo de impuesto RETEFUENTE
    public  static final String CODERETEFUENTE = "RT01";
    public  static final String RETEIVA        = "RIVA";             //tipo de impuesto RETEIVA
    public  static final String RETEICA        = "RICA";             //tipo de impuesto RETEICA
    public  static       double TASA           = 0;
    private static       String ELEMENTO       = "9005";             //sufijo de la definicion de la cuenta de la OP
    private boolean      ArmarCodigo           = true;
    public final  String  TIPO_OP             = "0";
    
    
    public final  String  CODIGO_PLANILLA     = "001";
    public final  String  CODIGO_ANTICIPOS    = "003";
    
    
    
    public  final  String  SANCHEZ_POLO        = "890103161";  // "8901031611";
    
    
    
    private final String  TIPO_CREDITO     = "035";
    private final String  TIPO_DEBITO      = "036";
    
    
    
    
    public OpDAO() {
        super("OpDAO.xml");//sescalante
    }
    public OpDAO(String dataBaseName) {
        super("OpDAO.xml", dataBaseName);//sescalante
    }
    
    
    
    
    public String  reset(String val){
        return (val==null)?"":val;
    }
    
    
    
    /**
     * M�todo getValor, vsetea el valor de los campos
     * @autor   fvillacob
     * @param   valor (String)
     * @return  String
     * @throws  Exception
     * @version 1.0.
     **/
    public String getValor(String valor){
        if( valor==null || valor.equals("")  )
            valor = " ";
        return valor;
    }
    
    /*********************************************************
     * metodo buscarOPPlanilla, busca la op de la planilla dada
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param: numer0 de planilla
     * @return: hashtable con datos de la op
     * @throws: en caso de un error de BD
     *********************************************************/
    public Hashtable buscarOPPlanilla( String planilla ) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String         query      = "SQL_GET_OP_PLANILLA";
        Hashtable h                  = null;
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, planilla);
            rs = st.executeQuery();
            
            if( rs.next() ){
                
                h = new Hashtable();
                
                h.put("numpla",         planilla);
                h.put("sta_pla",        reset( rs.getString("sta_pla") ) );
                h.put("sta_fac",        reset( rs.getString("sta_fac") ) );
                h.put("dstrct",         reset( rs.getString("dstrct") ) );
                h.put("proveedor",      reset( rs.getString("nit") ) );
                h.put("nom_proveedor",  reset( rs.getString("nombre") ) );
                h.put("documento",      reset( rs.getString("documento") ) );
                h.put("tipo_documento", reset( rs.getString("tipo_documento") ) );
                h.put("neto",           reset( rs.getString("vlr_neto_me") ) );
                h.put("saldo_local",    reset( rs.getString("vlr_saldo") ) );
                h.put("saldo",          reset( rs.getString("vlr_saldo_me") ) );
                h.put("moneda",         reset( rs.getString("moneda") ) );
                h.put("base",           reset( rs.getString("base") ) );
                h.put("corrida",        reset( rs.getString("corrida") ) );
                h.put("abono",          reset( rs.getString("vlr_total_abonos_me") ) );
                
            }
            
        }}catch(SQLException e){
            throw new SQLException(" ERROR  OpDAO.buscarOPPlanilla "+ e.getMessage());
        }finally {//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
        return h;
    }


/**
 * 
 * @param c
 * @param p
 * @param signo
 * @param tipo_doc
 * @return
 * @throws Exception
 */
    public String crearItemsFacturaCambioPropietarioOP( CXP_Doc c, Proveedor p, int signo, String tipo_doc ) throws Exception{

        StringStatement st    = null;//JJCastro fase2
        String sql   = "";
        String where = "";
 
        try{
            
            String columnas = "";
            
            PGTable t_cxp_doc = PGTable.getInstance( "cxp_items_doc","fin" );
            
            if( t_cxp_doc != null ){
                
                columnas       = t_cxp_doc.getColumns_names();
                
                String doc = (signo == -1)? "documento,'CP_'||documento":"documento,documento";
                
                if( tipo_doc.equals("035") ){ doc = "documento,'NCCP_'||documento"; }
                
                String[] reemp  = {
                    "reg_status,''::text",
                    doc,
                    "tipo_documento,'"+tipo_doc+"'::text",
                    "proveedor,'"+p.getC_nit()+"'::text",
                    "last_update,'0099-01-01'::timestamp",
                    "vlr,vlr*"+signo,
                    "vlr_me,vlr_me*"+signo,
                    "user_update,''::text",
                    "creation_date,now()",
                    "creation_user,'"+c.getCreation_user()+"'::text"};
                      
                    sql += Util.insertFromSelect("cxp_items_doc","cxp_items_doc", columnas, reemp, "",  "fin", "fin" );
                    
                    where += " WHERE dstrct = ? AND proveedor = ? AND documento = ? AND tipo_documento = ?;";
                    
                    sql += where;
                    
                    st = new StringStatement(sql, true);
                    st.setString( 1, c.getDstrct() );
                    st.setString( 2, c.getProveedor() );
                    st.setString( 3, c.getDocumento() );
                    st.setString( 4, c.getTipo_documento() );
                    
                    sql = st.getSql();//JJCastro fase2
            }
            
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en OpDAO.crearItemsFacturaCambioPropietarioOP "+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
               
        return sql;
        
    }


/**
 * 
 * @param c
 * @param p
 * @param signo
 * @param tipo_doc
 * @return
 * @throws Exception
 */
    public String crearImpItemsFacturaCambioPropietarioOP( CXP_Doc c, Proveedor p, int signo, String tipo_doc ) throws Exception{        
        
        
        StringStatement st    = null;        
        String sql   = "";
        String where = "";
        
        try{
            String columnas = "";
            PGTable t_cxp_doc = PGTable.getInstance( "cxp_imp_item","fin" );
            
            if( t_cxp_doc != null ){
                
                columnas       = t_cxp_doc.getColumns_names();
              
                String doc = (signo == -1)? "documento,'CP_'||documento":"documento,documento";
                
                if( tipo_doc.equals("035") ){ doc = "documento,'NCCP_'||documento"; }
                
                String[] reemp  = {
                    "reg_status,''::text",
                    "proveedor,'"+p.getC_nit()+"'::text",
                    doc,
                    "tipo_documento,'"+tipo_doc+"'::text",
                    "vlr_total_impuesto,vlr_total_impuesto*"+signo,
                    "vlr_total_impuesto_me,vlr_total_impuesto_me*"+signo,
                    "last_update,'0099-01-01'",
                    "user_update,''::text",
                    "creation_date,now()",
                    "creation_user,'"+c.getCreation_user()+"'"};
                      
                    sql += Util.insertFromSelect("cxp_imp_item","cxp_imp_item", columnas, reemp, "",  "fin", "fin" );
                    
                    where += " WHERE dstrct = ? AND proveedor = ? AND documento = ? AND tipo_documento = ?;";
                    
                    sql += where;
                    
                    st = new StringStatement(sql, true);
                    
                    st.setString( 1, c.getDstrct() );
                    st.setString( 2, c.getProveedor() );
                    st.setString( 3, c.getDocumento() );
                    st.setString( 4, c.getTipo_documento() );
                    
                    sql = st.getSql();//JJCastro fase2
            }
            
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en OpDAO.crearImpItemsFacturaCambioPropietarioOP "+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
                
        return sql;
        
    }


/**
 * 
 * @param c
 * @return
 * @throws Exception
 */
    public String updateFacturaAbono( CXP_Doc c ) throws Exception{
        StringStatement st         = null;
        ResultSet         rs         = null;
        String         query      = "SQL_UPDATE_SALDO_ABONO_CXP_DOC";
        String          sql          = "";
        
        try{
            
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setDouble( 1, c.getVlr_saldo()*-1 );
            st.setDouble( 2, c.getVlr_saldo_me()*-1 );
            st.setDouble( 3, c.getVlr_saldo() );
            st.setDouble( 4, c.getVlr_saldo_me() );
            st.setString( 5, c.getCreation_user() );
            st.setString( 6, c.getDstrct() );
            st.setString( 7, c.getProveedor() );
            st.setString( 8, c.getTipo_documento() );
            st.setString( 9, c.getDocumento() );
            
            sql = st.getSql();//JJCastro fase2
            
        }catch(SQLException e){
            throw new SQLException(" ERROR  OpDAO.updateFacturaAbono "+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }


/**
 *
 * @param c
 * @param new_proveedor
 * @return
 * @throws Exception
 */
    public String cambiarPropietarioOP( CXP_Doc c, String new_proveedor ) throws Exception{
        Connection con = null;//JJCastro fase2
        Statement st    = null;        
        String sql = "";
        String mens = "";
        
        con = this.conectarBDJNDI("fintra");//JJCastro fase2

        if (con == null)
            throw new SQLException("Sin conexion");
        try{
        ProveedorDAO p = new ProveedorDAO();                
                
        p.obtenerProveedoresPorNit( new_proveedor );
        Proveedor pr = p.getProveedor();
        
        
        if( c.getVlr_saldo_me() == 0 ){
            
            p.obtenerProveedoresPorNit( c.getProveedor() );
            Proveedor pr_orig = p.getProveedor();                        
            
            //Se crea la factura negativa al proveedor inicial
            sql += this.crearFacturaCambioPropietarioOP(c, pr_orig, -1, "010");
            sql += this.crearItemsFacturaCambioPropietarioOP(c, pr_orig, -1, "010");
            sql += this.crearImpItemsFacturaCambioPropietarioOP(c, pr_orig, -1, "010");
            
            mens = "Se cre� la factura negativa CP_"+c.getDocumento()+" al proveedor "+pr_orig.getC_nit();
            
            //Se crea la factura positiva al nuevo proveedor
            sql += this.crearFacturaCambioPropietarioOP(c, pr, 1, "010");
            sql += this.crearItemsFacturaCambioPropietarioOP(c, pr, 1, "010");
            sql += this.crearImpItemsFacturaCambioPropietarioOP(c, pr, 1, "010");
            
            mens += "<br>Se cre� la factura "+c.getDocumento()+" a favor del proveedor "+pr.getC_nit();
            
        }else{
            //Se crea la factura a favor del nuevo proveedor
            sql += this.crearFacturaCambioPropietarioOP(c, pr, 1, "010");
            sql += this.crearItemsFacturaCambioPropietarioOP(c, pr, 1, "010");
            sql += this.crearImpItemsFacturaCambioPropietarioOP(c, pr, 1, "010");
            
            mens = "Se cre� la factura "+c.getDocumento()+" a favor del proveedor "+pr.getC_nit();
                                    
            p.obtenerProveedoresPorNit( c.getProveedor() );
            Proveedor pr_orig = p.getProveedor();
            
            //Se crea la nota credito al propietario anterior
            sql += this.crearFacturaCambioPropietarioOP(c, pr_orig, 1, "035");
            sql += this.crearItemsFacturaCambioPropietarioOP(c, pr_orig, 1, "035");
            sql += this.crearImpItemsFacturaCambioPropietarioOP(c, pr_orig, 1, "035");
                        
            //Se actualiza el saldo de la factura original            
            sql += this.updateFacturaAbono(c);
            mens += "<br>Se cre� la NOTA CREDITO NCCP_"+c.getDocumento()+" al proveedor "+pr_orig.getC_nit();
        }
        
        //System.out.println(sql);        
        st = con.createStatement();
        st.addBatch(sql);
        st.executeBatch();
        con.commit();
        
        
        }catch(Exception e) {            
            e.printStackTrace();
            throw new Exception("Error en OpDAO.cambiarPropietarioOP "+e.getMessage());
        }finally{//JJCastro fase2
          if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return mens;
    }
    
    
    /**
     * M�todo que actualiza saldo de la Factura.
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public String  UpdateCXP_Saldos(OP op, String user)throws Exception{
        StringStatement st         = null;
        String            query          = "SQL_UPDATE_SALDO_CXP_DOC";
        String            sql             = "";
        try {            
            
         // Siempre llegan positivos
            double  saldo       =  op.getVlrNeto();
            double  saldo_me    =  op.getVlr_neto_me();
            double  abono       =  saldo;
            double  abono_me    =  saldo_me;            
            
            if(   op.getTipo_documento().equals( TIPO_DEBITO  )  ){
                  abono    *= -1;
                  abono_me *= -1;
            }
            
            if(   op.getTipo_documento().equals( TIPO_CREDITO  )  ){
                  saldo    *=  -1;
                  saldo_me *=  -1;
            }
            
                st= new StringStatement(this.obtenerSQL(query), true);
                st.setDouble(1,   saldo                         );              
                st.setDouble(2,   saldo_me                      );  
                st.setDouble(3,   abono                         );              
                st.setDouble(4,   abono_me                      );  
                st.setString(5,   user                          );                 
                
                st.setString(6,   op.getDstrct()                ); 
                st.setString(7,   op.getProveedor()             );                
                st.setString(8,   op.getTipo_documento_rel()    );
                st.setString(9,   op.getDocumento_relacionado() );
                
                sql = st.getSql();//JJCastro fase2
            
        } catch(SQLException e){
            throw new SQLException(" DAO: UpdateCXP_Saldos.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    
    
     /**
     * M�todo que busca la cuenta del concepto
     * @autor   fvillacob
     * @param   String dstrct, String nit, String codigoInterno, String oc
     * @throws  Exception
     * @version 1.0.
     **/
    public String getCuentaCMC(String dstrct, String cmcOC, String codigoInterno, String oc)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st                = null;
        ResultSet         rs                = null;
        String            cuenta            = "";
        
       
        String            query_tipoDoc     = "SQL_TIPO_DOC";
        String            query_cta_cmc     = "SQL_CUENTA_CMC";
        String            query_cta_plarem  = "SQL_CUENTA_PLAREM";
        
        
        String            tipo_doc          = "";
        String            cuenta_cmc        = "";
        String            tipo_cta_cmc      = "";
        String            cta_plarem        = "";
        int               swCtaPlarem       = 0;
        
        try{
            
            if( !cmcOC.equals("") ){
                
            
                // TIPO DOCUMENTO CONTABLE
                con = this.conectarJNDI(query_tipoDoc);//JJCastro fase2
                st = con.prepareStatement(this.obtenerSQL(query_tipoDoc));
                st.setString(1, dstrct        );
                st.setString(2, codigoInterno );
                rs=st.executeQuery();
                if(rs.next())
                    tipo_doc = rs.getString("codigo");


                // CUENTA DEL CMC_DOC
                st = con.prepareStatement(this.obtenerSQL(query_cta_cmc));//JJCastro fase2
                st.setString(1, dstrct        );
                st.setString(2, tipo_doc      );
                st.setString(3, cmcOC         );
                rs=st.executeQuery();
                if(rs.next()){
                    cuenta_cmc    = rs.getString("cuenta");
                    tipo_cta_cmc  = rs.getString("tipo_cuenta");
                }


                if( tipo_cta_cmc.equals("C") )
                    cuenta = cuenta_cmc;

                if( tipo_cta_cmc.equals("E") ){
                    swCtaPlarem = 1;
                    st = con.prepareStatement(this.obtenerSQL(query_cta_plarem));//JJCastro fase2
                    st.setString(1, dstrct     );
                    st.setString(2, oc         );
                    rs=st.executeQuery();
                    if(rs.next()){
                        String  cta   =  rs.getString("account_code_c");
                        cuenta        =  cta  + cuenta_cmc;
                    }

                }

            } 
            
        }catch(Exception e){
            throw new Exception( "DAO: getCuentaCMC "+e.getMessage() );
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cuenta;
    }
    
    
    
    
    
    
    /**
     * M�todo que busca la cuenta del concepto
     * @autor   fvillacob
     * @param   String dstrct, String nit, String codigoInterno, String oc
     * @throws  Exception
     * @version 1.0.
     **/
    public String getCuenta(String dstrct, String nit, String codigoInterno, String oc)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st                = null;
        ResultSet         rs                = null;
        String            cuenta            = "";
        
        String            query_cmc         = "SQL_CMC";
        String            query_tipoDoc     = "SQL_TIPO_DOC";
        String            query_cta_cmc     = "SQL_CUENTA_CMC";
        String            query_cta_plarem  = "SQL_CUENTA_PLAREM";
        
        
        String            cmc               = "";
        String            tipo_doc          = "";
        String            cuenta_cmc        = "";
        String            tipo_cta_cmc      = "";
        String            cta_plarem        = "";
        int               swCtaPlarem       = 0;
        
        try{
            
            
            // CMC DEL NIT
            con = this.conectarJNDI(query_cmc);//JJCastro fase2
            st = con.prepareStatement(this.obtenerSQL(query_cmc));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, nit   );
            rs=st.executeQuery();
            if(rs.next())
                cmc = rs.getString("cmc");
            
            
            // TIPO DOCUMENTO CONTABLE
            st = con.prepareStatement(this.obtenerSQL(query_tipoDoc));//JJCastro fase2
            st.setString(1, dstrct        );
            st.setString(2, codigoInterno );
            rs=st.executeQuery();
            if(rs.next())
                tipo_doc = rs.getString("codigo");
            
            
            // CUENTA DEL CMC_DOC
            st = con.prepareStatement(this.obtenerSQL(query_cta_cmc));//JJCastro fase2
            st.setString(1, dstrct        );
            st.setString(2, tipo_doc      );
            st.setString(3, cmc           );
            rs=st.executeQuery();
            if(rs.next()){
                cuenta_cmc    = rs.getString("cuenta");
                tipo_cta_cmc  = rs.getString("tipo_cuenta");
            }
            
            
            if( tipo_cta_cmc.equals("C") )
                cuenta = cuenta_cmc;
            
            if( tipo_cta_cmc.equals("E") ){
                swCtaPlarem = 1;
                st = con.prepareStatement(this.obtenerSQL(query_cta_plarem));//JJCastro fase2
                st.setString(1, dstrct     );
                st.setString(2, oc         );
                rs=st.executeQuery();
                if(rs.next()){
                    String  cta   =  rs.getString("account_code_c");
                    cuenta        =  cta  + cuenta_cmc;
                }
                
            }
            
            
            
        }catch(Exception e){
            throw new Exception( "DAO: getCuenta "+e.getMessage() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cuenta;
    }
    
    
    
    
    /**
     * M�todo existeStd_stdjobsel, verifica si un estandar es de carbon (existencia en la tabla stdjobsel)
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public void deleteDescMmto(String distrito, String oc)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        String            query     = "SQL_DELETE_DESC_MMTO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc       );
            st.setString(2, distrito );
            st.setString(3, oc       );
            st.execute();
            
        }} catch(SQLException e){
            throw new SQLException(" DAO: deleteDescMmto.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    
    
    /**
     * M�todo existeStd_stdjobsel, verifica si un estandar es de carbon (existencia en la tabla stdjobsel)
     * @autor   fvillacob
     * @param   StandarJob (String)
     * @return  boolean
     * @throws  Exception
     * @version 1.0.
     **/
    public boolean existeStd_stdjobsel(String std)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        boolean           estado    = false;
        String            query     = "SQL_BUSCAR_STD_STDJOBSEL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, std);
            rs=st.executeQuery();
            if(rs.next())
                estado = true;
            
        }} catch(SQLException e){
            throw new SQLException(" DAO: existeStd_stdjobsel.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return estado;
    }
    
    
    
    
    /**
     * M�todo deleteDescuentos, elimina movimientos de planilla segun parametros
     * @autor   fvillacob
     * @param   StandarJob, numero de la planilla (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public void deleteDescuentos(String std, String oc)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        String            query     = "SQL_DELETE_DESCUENTOS_MOVPLA_STD";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc);
            st.setString(2, std);
            st.execute();
     
        }} catch(SQLException e){
            throw new SQLException(" DAO: deleteDescuentos.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    
    
    /**
     * M�todo agreagrDescuentos_movpla, busca movimientos de la planilla segun el estadar (carbon)
     * @autor   fvillacob
     * @param   orden de pago (OP)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List agregarDescuentos_movpla(OP op)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        List descuentos = null;
        String hoy = Utility.getHoy("-");
        String query = "SQL_DESCUENTOS_PLANILLA_TBLDES";
        try {
            if (existeStd_stdjobsel(op.getStd())) {
                con = this.conectarJNDI(query);
                if (con != null) {
                    st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                    st.setString(1, op.getStd());
                    rs = st.executeQuery();
                    int cont = 1;
                    while (rs.next()) {
                        if (descuentos == null) {
                            descuentos = new LinkedList();
                        }

                        Movpla mov = new Movpla();
                        mov.setDstrct(rs.getString("dstrct"));
                        mov.setAgency_id(op.getAgencia());
                        mov.setDocument_type("001");
                        mov.setDocument(op.getStd());
                        mov.setItem(Utility.rellenar(String.valueOf(cont), 3));
                        mov.setConcept_code(rs.getString("concept_code"));
                        mov.setPlanilla(op.getOc());
                        mov.setSupplier(op.getPlaca());
                        mov.setDate_doc(hoy);
                        mov.setApplication_ind(rs.getString("ind_application"));
                        mov.setApplicated_ind("N");
                        mov.setInd_vlr(rs.getString("type"));
                        mov.setVlr(rs.getFloat("vlr"));
                        mov.setCurrency(rs.getString("currency"));
                        mov.setProveedor(op.getProveedor());
                        mov.setBranch_code(op.getBanco());
                        mov.setBank_account_no(op.getSucursal());

                        descuentos.add(mov);
                        cont++;
                        mov = null;//Liberar Espacio JJCastro
                    }
                }
            }
        } catch(SQLException e){
            throw new SQLException(" DAO: agregarDescuentos_movpla.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return descuentos;
    }    
    
    /**
     * M�todo loadOP, instancia un objeto OP con resultado de la consulta
     *  Utilizado en Generacion de OP y Liquidacion de OC
     * @autor   fvillacob
     * @param   Resultado de la consulta (ResulSet)
     * @return  OP
     * @throws  Exception
     * @version 1.0.
     **/
    public OP  loadOp(ResultSet  rs)throws Exception{
        OP  op = new OP();
        try{
            
                                 
            op.setDstrct            ( getValor( rs.getString("distrito")          ));
            op.setOc                ( getValor( rs.getString("oc")                ));
            op.setVlrOc             (           rs.getDouble("valorOc")            );
            op.setMonedaOC          ( getValor( rs.getString("monedaOc")          ));
            op.setProveedor         ( getValor( rs.getString("propietario")       ));
            op.setPlaca             ( getValor( rs.getString("placa")             ));
            op.setFechaCumplido     ( getValor( rs.getString("feccum")            ));
            op.setNameProveedor     ( getValor( rs.getString("name")              ));
            op.setId_mims           ( getValor( rs.getString("idMims")            ));
            op.setAgencia           ( getValor( rs.getString("agencia")           ));
            op.setBanco             ( getValor( rs.getString("banco")             ));
            op.setSucursal          ( getValor( rs.getString("sucursal")          ));
            op.setHandle_code       ( getValor( rs.getString("handleCode")        ));
            op.setPlazo             (           rs.getInt   ("plazo")              );
            op.setMoneda            ( getValor( rs.getString("monedaBanco")       ));
            op.setFecha_documento   ( getValor( rs.getString("fechaDocumento")    ));
            op.setFecha_vencimiento( getValor( rs.getString("fechaVencimiento")  ));
            op.setReteFuente        ( getValor( rs.getString("reteFuente")        ));
            op.setReteIca           ( getValor( rs.getString("reteIca")           ));
            op.setReteIva           ( getValor( rs.getString("reteIva")           ));
            op.setRetenedor         ( getValor( rs.getString("agente_retenedor")  ));
            op.setContribuyente     ( getValor( rs.getString("gran_contribuyente")));
            op.setOrigenOC          ( getValor( rs.getString("origenoc")          ));
            op.setRegStatus         ( getValor( rs.getString("reg_status")        ));
            
            
            op.setOT                ( getValor( rs.getString("ot")                ));
            op.setStd               ( "" );
            
            
            op.setVlrUnitarioOC     (           rs.getDouble("unit_cost")          );
            op.setBase              ( getValor( rs.getString("base"))              );
            op.setTipo_documento    ( this.TIPO_DOC                                );
            op.setAgenciaOC         (  reset( rs.getString("agcpla")             ) );
            op.setCmc               (  reset( rs.getString("cmc")                ) );
            
            String vetoNIT          =    getValor( rs.getString("veto") );   // Veto por NIT.
            
            
            String OP               =   buscarNumeroFactura(op.getDstrct(), op.getProveedor(),this.PRE_DOC + op.getOc() );
            String cuentaPLA        =   this.getCuentaCMC(  op.getDstrct(),  op.getCmc() , CODIGO_PLANILLA , op.getOc()  );
            String ageRelacionada   =   this.getAGERelacionada( op.getAgenciaOC() );  // Busca agencia relacionada.
            
            
            
            
         // Si es carb�n, buscamos el banco asignado al nit
            if(  op.getHandle_code().equals("FC")    ){
                 Hashtable bk =  bancoNit( op.getDstrct(),  op.getProveedor() ,  op.getHandle_code()  );
                 if(  bk!=null  ){
                      String  ban  =  (String) bk.get("banco");
                      String  suc  =  (String) bk.get("sucursal");
                      String  mon  =  (String) bk.get("moneda");
                      
                      op.setBanco    ( ban );
                      op.setSucursal ( suc );
                      op.setMoneda   ( mon );           
                 }                 
            }
            
            
            
         // Para Oficina Principal reasignamos a Barranquilla.
            if( ageRelacionada.equals("OP") )
                ageRelacionada = "BQ";
            
            
            op.setDocumento         ( OP                                          );
            op.setDescripcion       ( this.DESC_OP + op.getOc()                   );
            op.setABC               ( this.getABC( ageRelacionada )               );
            op.setCuenta            ( cuentaPLA                                   ); // Cuenta contable Planilla.
            op.setTipocant          ( getValor( rs.getString("tipo_cant"))        ); // sescalante            
            op.setTrailer           ( getValor( rs.getString("trailer")  )        ); // Fernel
            
          // Moneda Local : distrito
            String datosCia         = getDatosCia( op.getDstrct()                 );
            String[] vec            = datosCia.split("-");
            String monedaLocal      = vec[0];
            op.setMonedaLocal       ( monedaLocal );
            
            op.setClase_documento       ( TIPO_OP );            
            op.setTipo_documento_rel    ( "" );
            op.setDocumento_relacionado ( "" );           
            
            
            String msj = "";
            
        //  Validacion CMC  planilla
            if( op.getCmc().equals("") )
                msj += "La planilla no presenta cmc asignado ";
            
        //  Validacion cmc del proveedor
            if( op.getHandle_code().equals("") )
                msj += "El proveedor no presenta hc asignado ";
            
            
        //  Validacion de OT:
            
            if(  vetoNIT.equals("S") )
                 msj += "El propietario "+  op.getProveedor()  +" se encuentra vetado ";
            
            if (  op.getOT().equals("")   )
                 msj += "No presenta remesa valida ";            
            
            
        //  Validamos si tiene discrepancia:  
            if(  getDiscrepancia( op.getOc() ) )
                 msj += "Presenta discrepancia";
            
            
        // Validamos si la placa tiene Veto
             if(  getVetoPlaca(  op.getPlaca()  )  )
                 msj += "La placa " +  op.getPlaca()  +" se encuentra vetada ";
            
                    
            
            op.setComentario( msj );
            
            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return op;
    }
    
    
    
    
    /**
     * M�todo, busca el banco asociado al nit
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public Hashtable  bancoNit(String distrito, String nit, String hc)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_BANCO_NIT";
        Hashtable         info       = null;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
               st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
               st.setString(1, distrito );
               st.setString(2, nit      );
               st.setString(3, hc       );
               rs=st.executeQuery();
               if ( rs.next() ){
                    info = new Hashtable();
                    info.put("banco",     reset(  rs.getString("banco")    ) );
                    info.put("sucursal",  reset(  rs.getString("sucursal") ) );
                    info.put("moneda",    reset(  rs.getString("currency") ) );                   
               }                      
             
            
        }} catch(SQLException e){
            throw new SQLException(" DAO: bancoNit-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return info;
    }
    
    
    
    /**
     * M�todo, busca si la placa tiene veto asignado.
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public boolean  getVetoPlaca(String placa)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_VETO_PLACA";
        boolean           estado     = false;
        try {
            
             if( !placa.equals("") ){
                 con = this.conectarJNDI(query);
                 if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, placa);
                 rs=st.executeQuery();
                 if ( rs.next() )
                      estado = true;
             }
            
        }} catch(SQLException e){
            throw new SQLException(" DAO: getVetoPlaca-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return estado;
    }
    
    
    
    /**
     * M�todo, busca la discrepancia activa de la planilla
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public boolean  getDiscrepancia(String oc)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        String            query      = "SQL_DISCREPANCIA";
        boolean           estado     = false;
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
             st.setString(1, oc);
             rs=st.executeQuery();
             if ( rs.next() )
                  estado = true;
            
        }} catch(SQLException e){
            throw new SQLException(" DAO: getDiscrepancia-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return estado;
    }
    
    
    
    
    
    
    
    
    
    /**
     * M�todo cantidadCumplida, obtiene la cantidad cumplida de una planilla de carga general
     * @autor   fvillacob
     * @param   numero de la planilla (String)
     * @return  double
     * @throws  Exception
     * @version 1.0.
     **/
    public double  cantidadCumplida(String oc) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        double            cantidad  = 0;
        String            query     = "SQL_BUSCAR_CANTIDAD_CUMPLIDA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc);
            rs=st.executeQuery();
            if(rs.next())
                cantidad = rs.getDouble(1);
            
            
        }} catch(SQLException e){
            throw new SQLException(" DAO: cantidadCumplida.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cantidad;
    }
    
    
    
    
    
    // 1. GENERACION DE OP
    
    
    
    /**
     * M�todo getItemOP, busca los movimientos de la planilla, instancia los items con la informacion
     *  resultante y los ingresa en una lista
     * @autor   fvillacob
     * @param   orden de pago (OP)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List getItemOP( OP  op) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String            query     = "SQL_SEARCH_MOV";
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, op.getOc()      );
            rs=st.executeQuery();
            
            while(rs.next()){
                
                OPItems  item = new OPItems();
                
                item.setDstrct       ( this.reset   ( rs.getString("distrito"))       );
                item.setDescripcion  ( this.reset   ( rs.getString("concepto"))       );
                item.setDescconcepto ( this.reset   ( rs.getString("descripcion") )   );
                item.setVlr          (                rs.getDouble("valor")           );
                item.setAsignador    ( this.reset   ( rs.getString("asignador")  )    );
                item.setIndicador    ( this.reset   ( rs.getString("indicador")  )    );
                item.setMoneda       ( this.reset   ( rs.getString("moneda")     )    );
                item.setCodigo_cuenta( this.reset   ( rs.getString("cuenta")     )    );  // PL de Ivan.  get_codigocuenta
                item.setTipo_cuenta  ( this.reset   ( rs.getString("tipo_cuenta"))    );
                item.setAgencia      ( this.reset   ( rs.getString("agencia")    )    );
                
                
                item.setCodigo_abc   ( op.getABC()                                    );  // Codigo ABC               
                item.setCheque       ( this.reset   ( rs.getString("cheque")     )    );  // ivan Gomez
                item.setConcepto     ( this.reset   ( rs.getString("concepto"))       );  // Fernel
            
                String  proveedorMovimiento =  reset( rs.getString("proveedor")  );
                
                
            // Cuenta para los anticipos:
                if( ( item.getDescripcion().equals("01")  || item.getDescripcion().equals("02") || item.getDescripcion().equals("03")   )  &&   ( proveedorMovimiento.equals( SANCHEZ_POLO )  ||   proveedorMovimiento.equals("")  ) ){
                     String cta  = this.getCuentaCMC(  op.getDstrct(),  op.getCmc() , CODIGO_ANTICIPOS , op.getOc()  );
                     item.setCodigo_cuenta( cta );
                }
                 
                
                if(!(item.getDescripcion().equals("01") && item.getCheque().equals("")) ){//Ivan DArio Gomez Vanegas 2006-03-28
                    lista.add(item);
                }
                
                
            }
            
        }}
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException(" DAO: No se pudo buscar Items de OP.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    /**
     * M�todo getValorMoneda, realiza el proceso de convesion de un valor de una moneda a otra
     *  y define el valor de la tasa
     * @autor   fvillacob
     * @param   moneda original del valor (String), valor (double), moneda a convertir (string), fecha de creacion dela tasa (String), distrito de la OP (String)
     * @return  double
     * @throws  Exception
     * @version 1.0.
     **/
    
    public double getValorMoneda(String moneda,  double valor, String monedaToConvertir, String fecha, String dstrct) throws Exception{
        
        try {
            
            this.TASA          = 1;
            
         // Moneda Local
            String datosCia    = getDatosCia( dstrct );
            String[] vec       = datosCia.split("-");
            String monedaLocal = vec[0];
            
            if(fecha==null)
                fecha = Utility.getHoy("-");
            
         // Funcion de diogenes
            TasaService  svc  =  new  TasaService();
            Tasa         obj  = svc.buscarValorTasa(monedaLocal,  moneda , monedaToConvertir , fecha);
            if(obj!=null){
                this.TASA   = obj.getValor_tasa();
                valor       = this.TASA * valor;
            }
            else
                valor = 0;
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudo Convertir la Moneda.-->"+ e.getMessage());
        }
        return valor;
    }
    
    
    
    
    
    
    
    /**
     * M�todo getABC, devuelve el valor ABC para la agencia
     * @autor   fvillacob
     * @param   agencia (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public String getABC(String agencia) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st           = null;
        ResultSet         rs           = null;
        String            abc          = "";         
        String            query1       = "SQL_CODIGO_EQUIVALENCIA";
        String            query2       = "SQL_SEARCH_ABC";
        
        try {
            
            
            if( !agencia.equals("") ){
                
             // Buscamos equivalencia contable de la agencia. tabla: abc.abc_equivalencia
                String equivalencia    =  "";
                con = this.conectarJNDI(query1);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query1));//JJCastro fase2
                st.setString(1, agencia);
                rs=st.executeQuery();
                if(rs.next())
                    equivalencia  = reset( rs.getString(1) );

            // Buscamos el codigo ABC
                if( !equivalencia.equals("") ){            
                    st= con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                    st.setString(1, equivalencia);
                    rs=st.executeQuery();
                    if( rs.next() )
                        abc  = reset( rs.getString(1) ); 

                }
            
            } 
            
            
        }}catch(SQLException e){
            throw new SQLException(" DAO: No se pudo Buscar el ABC.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return abc;
    }
    
    
    
    
     /**
     * M�todo que busca codigo de la agencia relacionada
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public String getAGERelacionada(String agencia) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st           = null;
        ResultSet         rs           = null;
        String            query        = "SQL_AGE_RELACIONADA";
        String            relacionada  = agencia;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, agencia);
            rs=st.executeQuery();
            if(rs.next())
               relacionada = reset(  rs.getString("agasoc") );
            
            }}catch(SQLException e){
            throw new SQLException(" DAO: No se pudo Buscar getAGERelacionada -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return relacionada;
    }
    
    
    
    /**
     * M�todo getDatosCia, obtiene la informacion de una compania
     * @autor   fvillacob
     * @param   codigo de la compania (String)
     * @return  String
     * @throws  Exception
     * @version 1.0.
     **/
    public String getDatosCia(String cia) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            datos     = " - ";
        String            query     = "SQL_SEARCH_CIA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cia);
            rs=st.executeQuery();
            if(rs.next())
                datos  = this.getValor(rs.getString(1)) +"-"+
                         this.getValor(rs.getString(2));
            
            }}
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudo Buscar datos de la Cia.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return datos;
    }
    
    
    
    
    
    
    /**
     * M�todo getAgenciaAsociada, obtiene la informacion de una agencia asociada
     * @autor   fvillacob
     * @param   codigo de la agencia (ciudad) (String)
     * @return  String
     * @throws  Exception
     * @version 1.0.
     **/
    public Hashtable getDatosAgencia(String agencia) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        Hashtable         asoc      = null;
        String            query     = "SQL_SEARCH_AGE_REL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, agencia );
            rs=st.executeQuery();
            if ( rs.next() ){
                asoc =  new  Hashtable();
                asoc.put("pais",      reset( rs.getString("pais")      )  );
                asoc.put("asociada",  reset( rs.getString("asociada")  )  );
                asoc.put("codica",    reset( rs.getString("codica")    )  );
                asoc.put("aplica",    reset( rs.getString("aplica")    )  );
                
            }
            
            }}
        catch(SQLException e){
            throw new SQLException(" DAO:Hashtable  No se pudo Buscar Agencia Relacionada.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return asoc;
    }
    
    
    
    
    
    /**
     * M�todo getAgenciaAsociada, obtiene la informacion de una agencia asociada
     * @autor   fvillacob
     * @param   codigo de la agencia (ciudad) (String)
     * @return  String
     * @throws  Exception
     * @version 1.0.
     **/
    public String getAgenciaAsociada(String agencia) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            asoc      = "";
        String            query     = "SQL_SEARCH_AGE_REL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, agencia);
            rs=st.executeQuery();
            if ( rs.next() )
                asoc  = this.getValor(  rs.getString("pais")     )   +"-"+
                        this.getValor(  rs.getString("asociada") )   +"-"+
                        this.getValor(  rs.getString("codica")   )   +"-"+
                        this.getValor(  rs.getString("aplica")   )   ;
            
            }}
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudo Buscar Agencia Relacionada.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return asoc;
    }
    
    
    
    
    /**
     * M�todo insertOP, registra la cabecera de la factura en cxp_doc
     * @autor   fvillacob
     * @param   orden de pago (OP), usuario (String)
     * @throws  Exception
     * @version 1.0.
     **/
    
    public String insertOP(OP op,  String user) throws Exception{
        StringStatement st        = null;
        String            sql       = "";
        String            query     = "SQL_INSERT_OP";
        try{
            
            //--- CABECERA:
            st= new StringStatement(this.obtenerSQL(query), true );
            st.setString(1,   op.getDstrct()            );
            st.setString(2,   op.getProveedor()         );
            st.setString(3,   op.getTipo_documento()    );
            st.setString(4,   op.getDocumento()         );
            st.setString(5,   op.getDescripcion()       );
            st.setString(6,   op.getAgencia()           );
            st.setString(7,   op.getHandle_code()       );
            st.setString(8,   op.getId_mims()           );
            st.setString(9,   op.getFecha_documento()   );
            st.setString(10,  this.APROBADOR            );
            st.setString(11,  (op.getFecha_vencimiento()==null || op.getFecha_vencimiento().trim().equals("") )?op.getFecha_documento() :op.getFecha_vencimiento()  );
            st.setString(12,  op.getBanco()             );
            st.setString(13,  op.getSucursal()          );
            st.setString(14,  op.getMoneda()            );
            st.setDouble(15,  op.getVlrNeto()           );  // valor Neto
            st.setDouble(16,  op.getVlrNeto()           );  // saldo
            st.setDouble(17,  op.getVlr_neto_me()       );  // valor Neto_me
            st.setDouble(18,  op.getVlr_neto_me()       );  // saldo me
            st.setDouble(19,  op.getTasa()              );
            st.setString(20,  user                      );
            st.setString(21,  op.getBase()              );
            st.setString(22,  this.APROBADOR            );
            st.setString(23,  op.getClase_documento()   );  // Tipo de OP : Fernel
            st.setString(24,  op.getMoneda()            );             
            st.setString(25,  op.getTipo_documento_rel()        );
            st.setString(26,  op.getDocumento_relacionado()     );            
            st.setString(27,  op.getClase_doc_rel()   );           
            
            sql = st.getSql()+";";
            
        } catch(SQLException e){
            throw new SQLException(" DAO:(insertOP) No se pudo insertar la OP.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }
    
    /**
     * M�todo insertImpItem, registra los impuestos aplicados a los items
     * @autor   fvillacob
     * @param   distrito, proveedor, tipo de documento, documento, item, codigo del impuesto (String)
     *          porcentaje del impuesto, valor total del impuesto, valor total del impuesto me (double)
     *          usuario, base (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public String insertImpItem(
                                 String dstrct ,String proveedor,String tipo_documento,String documento,String item ,String cod_impuesto ,
                                 double porcent_impuesto , double vlr_total_impuesto ,double vlr_total_impuesto_me ,String user ,String base
                                ) throws Exception {

        StringStatement st        = null;
        String            sql       = "";
        String            query     = "SQL_INSERT_IMP_ITEM_OP";
        try{
            st= new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,   dstrct                );
            st.setString(2,   proveedor             );
            st.setString(3,   tipo_documento        );
            st.setString(4,   documento             );
            st.setString(5,   Utility.rellenar(item,3) );
            st.setString(6,   cod_impuesto          );
            st.setDouble(7,   porcent_impuesto      );
            st.setDouble(8,   vlr_total_impuesto    );
            st.setDouble(9,   vlr_total_impuesto_me );
            st.setString(10,  user                  );
            st.setString(11,  base                  );
            sql = st.getSql()+";";            
            
        } catch(SQLException e){
            throw new SQLException(" DAO:(insertImpItem) No se pudo insertar Impuesto del Item de la OP.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return sql;
    }
    
    
    
    /**
     * M�todo searchImpByDoc, busca los impuestos aplicados a los items y los registra en la tabla cxp_imp_doc
     * @autor   fvillacob
     * @param   orden de pago (OP), usuario (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public String searchImpByDoc(OP op, String user) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        StringStatement st2       = null;
        ResultSet         rs        = null;
        String            sql       = "";
        String query = "SQL_SEARCH_IMP_ITEM_DOC";//JJCastro fase2
        String query2 = "SQL_INSERT_IMP_DOC";//JJCastro fase2
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2           
            st.setString(1, op.getDstrct()        );
            st.setString(2, op.getProveedor()     );
            st.setString(3, op.getTipo_documento());
            st.setString(4, op.getDocumento()     );
            rs=st.executeQuery();
            String consulta = this.obtenerSQL(query2);//JJCastro fase2
            while(rs.next()){
                st2 = new StringStatement(consulta, true);//JJCastro fase2
                st2.setString(1,  op.getDstrct()            );
                st2.setString(2,  op.getProveedor()         );
                st2.setString(3,  op.getTipo_documento()    );
                st2.setString(4,  op.getDocumento()         );
                st2.setString(5,  rs.getString("codigo")    );
                st2.setDouble(6,  rs.getDouble("porcentaje"));
                st2.setDouble(7,  rs.getDouble("vlr")       );
                st2.setDouble(8,  rs.getDouble("vlr_me")    );
                st2.setString(9,  user);
                st2.setString(10, op.getBase());
                sql += st2.getSql()+";";//JJCastro fase2
                st2  =  null;//Liberar Espacio JJCastro
            }
            
            }}catch(SQLException e){
            throw new SQLException(" DAO: (searchImpByDoc) No se pudo obtener el impuesto-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (st2  != null){ try{ st = null;            } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }
    
    
    
    
     /**
     * M�todo getImpuesto, busca un impuesto
     * @autor   fvillacob
     * @param   codigo del impuesto, tipo de impuesto, fecha, distrito de la OP (String)
     * @return  OPImpuestos
     * @throws  Exception
     * @version 1.0.
     **/
    public OPImpuestos getImpuestoAgencia(String distrito, String codigoIMP, String fechaFinAno, String agencia) throws Exception{
        Connection        con       = null;
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        OPImpuestos       impuesto  = new OPImpuestos();
        String            query     = "SQL_SEARCH_IMPUESTO_AGE";
        
        try{
            
            con = this.conectarJNDI(query);
            if (con != null) {
            agencia         =  (agencia.equals("OP"))?"BQ": agencia;            
            String  AGE     =  ( codigoIMP.equals( CODERETEFUENTE )  )? " like '%' " : " ='"+ agencia + "'";
            String  sql     =  this.obtenerSQL(query).replaceAll("#AGENCIA#", AGE );
            
            st= con.prepareStatement( sql );
            st.setString(1, distrito     );
            st.setString(2, codigoIMP    );
            st.setString(3, fechaFinAno  );
            rs=st.executeQuery();
            
            
            if (rs.next() ){
                
                impuesto.setCodigo      ( reset( rs.getString("codigo")  ));
                impuesto.setAgencia     ( reset( rs.getString("agencia") ));                
                impuesto.setCuenta      ( reset( rs.getString("cuenta")  ));
                impuesto.setTipo        ( reset( rs.getString("tipo")    ));
                impuesto.setPorcentaje  (        rs.getDouble("porc1")    );
                impuesto.setPorcentaje2 (        rs.getDouble("porc2")    );                
                impuesto.setSigno       (        rs.getInt   ("signo")    );                
            }
            
            
        }}catch(SQLException e){
            throw new SQLException(" DAO: No se pudo obtener el impuesto  getImpuestoAgencia-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return impuesto;
    }
    
    
    
    
    
    
    /**
     * M�todo getImpuesto, busca un impuesto
     * @autor   fvillacob
     * @param   codigo del impuesto, tipo de impuesto, fecha, distrito de la OP (String)
     * @return  OPImpuestos
     * @throws  Exception
     * @version 1.0.
     **/
    public OPImpuestos getImpuesto( String codeImpuesto, String tipo, String fecha, String dstrct ) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        OPImpuestos impuesto        = new OPImpuestos();
        try{
            String sql      = this.obtenerSQL("SQL_SEARCH_IMPUESTO").replaceAll("#FECHA#", fecha);
            con  = this.conectarJNDI("SQL_SEARCH_IMPUESTO");//JJCastro fase2
            if(con!=null){
            st= con.prepareStatement( sql );
            st.setString(1, dstrct);
            st.setString(2, codeImpuesto);
            st.setString(3, tipo);
            rs=st.executeQuery();
            if (rs.next() ){
                impuesto.setCodigo      ( rs.getString("codigo")  );
                impuesto.setAgencia     ( rs.getString("agencia") );
                impuesto.setCuenta      ( rs.getString("cuenta")  );
                impuesto.setPorcentaje  ( rs.getDouble("porc1")   );
                impuesto.setPorcentaje2 ( rs.getDouble("porc2")   );
                impuesto.setTipo        ( tipo );
                impuesto.setSigno       ( rs.getInt   ("signo")   );
                
            }
            
        }}catch(SQLException e){
            throw new SQLException(" DAO: No se pudo obtener el impuesto-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return impuesto;
    }
    
    
    
    
    
    
    
    
    
    
    
    /**
     * M�todo buscarCantidadCumplidaCarbon, obtiene la cantidad cumplida de una planilla de carbon
     * @autor   sescalante
     * @param   numero de la planilla (String)
     * @return  double
     * @throws  Exception
     * @version 1.0.
     **/
    public double buscarCantidadCumplidaCarbon(String planilla)throws Exception{
        double cantidad = 0;
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            query     = "SQL_CANTCUMPCARBON";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, planilla);
            rs = st.executeQuery();
            if (rs.next()){
                cantidad = rs.getDouble("cantidad");
            }
        }}catch(SQLException e){
            throw new SQLException(" DAO: No se pudo obtener la cantidad cumplida de la planilla de carbon -->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cantidad;
    }
    
    
    
    /**
     * /**
     * M�todo buscarListaHC,Busca la lista de los codigos HC
     * @autor   fvillacob
     * @param
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List buscarListaHC() throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String query = "SQL_HC";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            while(rs.next()){
                Hashtable fila  = new Hashtable();
                fila.put("codigo",rs.getString("codigo"));
                fila.put("descripcion",rs.getString("descripcion"));
                lista.add(fila);
            }
            
            }}
        catch(SQLException e){
            throw new SQLException(" DAO: Error en la busqueda de las planillas sin proveedor registrado en PROVEEDORES--> "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    /**
     * M�todo buscarNumeroFactura,Busca numero de la factura siguiente si existe
     * @autor   fvillacob
     * @param
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public String buscarNumeroFactura(String Distrito,String Proveedor, String OP) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String             Codigo   = OP;
        String query = "SQL_EXISTE_OP";//JJCastro fase2
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Distrito );
            st.setString(2, Proveedor);
            st.setString(3, OP);
            rs = st.executeQuery();
            
            if(rs.next()){
                
                for(int i=1; i<= 2000;i++){
                    st.clearParameters();
                    st.setString(1, Distrito );
                    st.setString(2, Proveedor);
                    st.setString(3, OP+"_"+i);
                    rs = st.executeQuery();
                    if(!rs.next()){
                        Codigo= OP+"_"+i;
                        break;
                    }
                }
                
            }
            
            }}
        catch(SQLException e){
            throw new SQLException(" DAO: Error en la verificacion de la existencia de la op "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Codigo;
    }
    
    
    
    
    
    /*OP AUTOMATICO TITO 07-10-2006*/
    
    /**
     * M�todo getOPsinProvRegistrado, obtiene las planillas cuyo nitpro no se encuentra registrado en proveedor
     * @autor   fvillacob
     * @param   Fecha inicial, fecha final (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List getOPsinProvRegistrado(String fechaIni, String fechaFin) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = null;
        String query = "SEARCH_OCSINPROV";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            
            if( fechaIni.trim().length()!=0 && fechaFin.trim().length()!=0 ){//AMATURANA 2.10.2006
                st= con.prepareStatement(this.obtenerSQL("SEARCH_OCSINPROV"));
                st.setString(1, fechaIni + " 00:00:00");
                st.setString(2, fechaFin + " 23:59:59");
            } else {
                st= con.prepareStatement(this.obtenerSQL("SEARCH_OCSINPROV_OPAUTO"));
            }
            
            
            rs=st.executeQuery();
            
            lista = new LinkedList();
            while(rs.next()){
                Planilla pla  = new Planilla();
                pla.setNumpla(rs.getString("numpla"));
                pla.setNitpro(rs.getString("nitpro"));
                lista.add(pla);
            }
            
            }}
        catch(SQLException e){
            throw new SQLException(" DAO: Error en la busqueda de las planillas sin proveedor registrado en PROVEEDORES--> "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    
    
    
    
    /**
     * M�todo getOP, busqueda de las planillas cumplidas en el rango de fecha dado
     * @autor   fvillacob
     * @param   Fecha inicial, fecha final (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List getOP(String fechaIni, String fechaFin,String HC, String distrito ) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = null;
        String query = "SQL_SEARCH_OC";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            
            if( fechaIni.trim().length()!=0 && fechaFin.trim().length()!=0 ){//AMATURANA 2.10.2006
                
                st= con.prepareStatement(this.obtenerSQL("SQL_SEARCH_OC"));  //crearPreparedStatement("SQL_SEARCH_OC");
                st.setString(1, fechaIni + " 00:00:00");
                st.setString(2, fechaFin + " 23:59:59");
                st.setString(3, distrito );
                st.setString(4, HC);
                
            } else {                
                String periodo =  "2006-11-01 00:00:00";     // A partir de Noviembre

                st= con.prepareStatement(this.obtenerSQL("SQL_SEARCH_OC_OPAUTO"));
                st.setString(1, distrito );
                st.setString(2, periodo );
            }
          
            
            rs=st.executeQuery();
            while(rs.next()){
                if( lista==null) lista = new LinkedList();
                OP  op  = this.loadOp(rs);
                lista.add(op);
            }
            }}
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudo buscar OP.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    /**
     * M�todo InsertMovimientoMovpla, registra movimientos de la planilla en movpla
     * @autor   fvillacob
     * @param   lista de movimientos (List)
     * @throws  Exception
     * @version 1.0.
     **/
    public void InsertMovimientoMovpla(List lista)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st         = null;
        String            query      = "SQL_INSERT_MOVPLA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for(int i=0;i< lista.size();i++){
                Movpla  mov = (Movpla) lista.get(i);
                try{
                    
                    st.setString(1 , mov.getDstrct()        );
                    st.setString(2 , mov.getAgency_id()     );
                    st.setString(3 , mov.getDocument_type() );
                    st.setString(4 , mov.getDocument()      );
                    st.setString(5 , mov.getItem()          );
                    st.setString(6 , mov.getConcept_code()  );
                    st.setString(7 , mov.getPlanilla()      );
                    st.setString(8 , mov.getSupplier()      );
                    st.setString(9 , mov.getDate_doc()      );
                    st.setString(10, mov.getApplicated_ind());
                    st.setString(11, mov.getApplication_ind());
                    st.setString(12, mov.getInd_vlr()        );
                    st.setDouble(13, mov.getVlr()            );
                    st.setString(14, mov.getCurrency()       );
                    st.setString(15, mov.getProveedor()      );
                    st.setString(16, mov.getBranch_code()    );
                    st.setString(17, mov.getBank_account_no());
                    
                    st.execute();
                    st.clearParameters();
                    
                } catch(SQLException e){
                    updateMovpla( mov );
                }
                
            }
            
        }} catch(SQLException e){
            throw new SQLException(" DAO: InsertMovimientoMovpla.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
     * M�todo, actualiza movimientos de la planilla en movpla
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     **/
    public void updateMovpla(Movpla  mov)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st         = null;
        String            query      = "SQL_UPDATE_MOVPLA";
        try {
            
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, mov.getApplicated_ind()  );
            st.setString(2, mov.getApplication_ind() );
            st.setString(3, mov.getInd_vlr()         );
            st.setDouble (4, mov.getVlr()             );
            st.setString(5, mov.getCurrency()        );
            
            st.setString(6 , mov.getPlanilla()      );
            st.setString(7 , mov.getDocument()      );
            st.setString(8 , mov.getConcept_code()  );
            
            st.execute();
            
        }} catch(SQLException e){
            throw new SQLException(" DAO: updateMovpla-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
  
    /**
     * M�todo getItemOP, busca los movimientos de la planilla, instancia los items con la informacion
     *  resultante y los ingresa en una lista
     * @autor   fvillacob
     * @param   orden de pago (OP)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List getItemOP( OP  op, String  SQL) throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String            query     = SQL;
        try {
            
            String SEPARADOR_PLCTA   = ";";
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, op.getOc()      );
            rs=st.executeQuery();
            
            while(rs.next()){
                
                OPItems  item = new OPItems();
                
                item.setDstrct       ( this.reset   ( rs.getString("distrito"))       );
                item.setDescripcion  ( this.reset   ( rs.getString("concepto"))       );
                item.setDescconcepto ( this.reset   ( rs.getString("descripcion") )   );
                item.setVlr          (                rs.getDouble("valor")           );
                item.setAsignador    ( this.reset   ( rs.getString("asignador")  )    );
                item.setIndicador    ( this.reset   ( rs.getString("indicador")  )    );
                item.setMoneda       ( this.reset   ( rs.getString("moneda")     )    );               
                item.setTipo_cuenta  ( this.reset   ( rs.getString("tipo_cuenta"))    );
                item.setAgencia      ( this.reset   ( rs.getString("agencia")    )    );
                
                
                item.setCodigo_abc   ( op.getABC()                                    );  // Codigo ABC               
                item.setCheque       ( this.reset   ( rs.getString("cheque")     )    );  // ivan Gomez
                item.setConcepto     ( this.reset   ( rs.getString("concepto"))       );  // Fernel
            
                String  proveedorMovimiento =  reset( rs.getString("proveedor")       );
                String  cta                 =  reset( rs.getString("cuenta")          );  // PL get_codigocuenta, realizado por Ivan Gomez.
                String  aux                 =  "";                
                
                
            // Cuenta para los anticipos entregados por TSP:
                if( ( item.getDescripcion().equals("01")  || item.getDescripcion().equals("02") || item.getDescripcion().equals("03")   )  &&   ( proveedorMovimiento.equals( SANCHEZ_POLO )  ||   proveedorMovimiento.equals("")  ) ){
                     cta  = this.getCuentaCMC(  op.getDstrct(),  op.getCmc() , CODIGO_ANTICIPOS , op.getOc()  );                     
                }else{
                     if( !cta.equals("")   ){                          
                          String vecCTA[]  =  cta.split( SEPARADOR_PLCTA );
                          if( vecCTA.length>1  ){
                              cta = reset( vecCTA[0] );    // Cuanta
                              aux = reset( vecCTA[1] );    // Auxiliar                              
                          }
                     }
                }   
                item.setCodigo_cuenta( cta );
                item.setAuxiliar     ( aux );
                    
                
                
                lista.add(item);
                
                item = null;//Liberar Espacio JJCastro
            }
            
        }}
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException(" DAO: No se pudo buscar Items de OP.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }








    /**
     * M�todo insertItem, registra los items de la factura
     * @autor   fvillacob
     * @param   item de la OP (OPItems), usuario (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public String insertItem(  OPItems item,String user) throws Exception {
        StringStatement st        = null;
        String            sql       = "";
        String query = "SQL_INSERT_ITEM_OP";//JJCastro fase2
        try{
            String AUXILIAR_OP      = "";
            
            st= new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,   item.getDstrct()         );
            st.setString(2,   item.getProveedor()      );
            st.setString(3,   item.getTipo_documento() );
            st.setString(4,   item.getDocumento()      );
            st.setString(5,   item.getItem()           );
            st.setString(6,   item.getDescconcepto()   );
            st.setDouble(7,   item.getVlr()            );
            st.setDouble(8,   item.getVlr_me()         );
            st.setString(9,   item.getCodigo_cuenta()  );
            st.setString(10,  item.getCodigo_abc()     );
            st.setString(11,  item.getPlanilla()       );
            st.setString(12,  user                     );
            st.setString(13,  item.getBase()           );
            st.setString(14,  item.getAuxiliar()       ); // Auxiliar
            st.setString(15,  item.getConcepto()       ); // Concepto
                        
            sql = st.getSql()+";";//JJCastro fase2
            
        } catch(SQLException e){
            throw new SQLException(" DAO:(insertItem) No se pudo insertar el Item de la OP.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    








  /**
     * M�todo updatePlanilla, registra el numero de la OP en el campo factura en planilla
     * @autor   fvillacob
     * @param   orden de pago (OP)
     * @throws  Exception
     * @version 1.0.
     **/
    public String updateFactura(OP op, String procedencia) throws Exception {
        StringStatement st            = null;
        String            sql           = "";
        String            queryPlanilla  = "SQL_UPDATE_PLANILLA";        
        String            queryMovpla    = "SQL_UPDATE_MOVPLA_FACTURA";      
        String            queryMovpla2   = "SQL_UPDATE_MOVPLA_FACTURA_ANULADAS";      
        
        try {
            
            
            
            if(  procedencia.equals("OP")  ){
                
              // PLANILLA:
                st= new StringStatement(this.obtenerSQL(queryPlanilla), true);//JJCastro fase2
                st.setString(1, op.getDocumento() );
                st.setString(2, op.getDstrct()    );
                st.setString(3, op.getOc()        );
                sql = st.getSql();//JJCastro fase2
                
                
              // MOVPLA:
                st= new StringStatement(this.obtenerSQL(queryMovpla), true);//JJCastro fase2
                st.setString(1, op.getDocumento() );
                st.setString(2, op.getOc()        );
                st.setString(3, op.getDstrct()    );  
                
            }
            
            
            
            if(  procedencia.equals("ADICION")  ){
                    
                if( op.getEstado().equals("A")   )
                     queryMovpla = queryMovpla2;
                  
              // MOVPLA:
                st= new StringStatement(this.obtenerSQL(queryMovpla), true);//JJCastro fase2
                st.setString(1, op.getDocumento() );
                st.setString(2, op.getOc()        );
                st.setString(3, op.getDstrct()    ); 
                
            }
            
            
            
            sql += st.getSql();//JJCastro fase2
            
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO:updateFactura  No se pudo Actualizar Numero de Factura.-->"+ e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
    
    
    
/**
 * 
 * @param c
 * @param p
 * @return
 * @throws Exception
 */
    public String crearNotaCreditoOP( CXP_Doc c, Proveedor p ) throws Exception{
       
        StringStatement st    = null;
        String sql   = "";
        String where = "";
        String update = "";

        try{
            String columnas = "";
            
            PGTable t_cxp_doc = PGTable.getInstance( "cxp_doc","fin" );

            if( t_cxp_doc != null ){
                
                //Se crea la nota credito a partir del select de la OP
                columnas        = t_cxp_doc.getColumns_names();                
                
                String[] reemp  = {
                    "reg_status,''::text",
                    "tipo_documento,'035'::text",
                    "descripcion,'NOTA CREDITO CAMBIO PROPIETARIO A "+p.getC_nit()+"'",
                    "documento,'NCCP_'||documento",
                    "tipo_documento_rel,'010'::text",
                    "documento_relacionado,documento",
                    "fecha_aprobacion,now()",
                    "vlr_neto,vlr_saldo",
                    "vlr_total_abonos,0",
                    "vlr_neto_me,vlr_saldo_me",
                    "vlr_total_abonos_me,0",
                    "aprobador,'"+c.getCreation_user()+"'::text",
                    "usuario_aprobacion,'"+c.getCreation_user()+"'::text",
                    "usuario_contabilizo,''::text",
                    "fecha_contabilizacion,'0099-01-01'::timestamp",
                    "usuario_anulo,''::text",
                    "fecha_anulacion,'0099-01-01'::timestamp",
                    "fecha_contabilizacion_anulacion,'0099-01-01'::timestamp",
                    "user_update,''::text",
                    "creation_user,'"+c.getCreation_user()+"'::text",
                    "corrida,''::text",
                    "cheque,''::text",
                    "fecha_contabilizacion_ajc,'0099-01-01'::timestamp",
                    "fecha_contabilizacion_ajv,'0099-01-01'::timestamp",
                    "usuario_contabilizo_ajc,''::text",
                    "usuario_contabilizo_ajv,''::text",
                    "transaccion_ajc,0",
                    "transaccion_ajv,0",
                    "clase_documento,'4'::text",
                    "transaccion,0",
                    "fecha_documento,now()::date",
                    "fecha_vencimiento,now()::date + '"+ p.getPlazo() +" days'::interval",
                    "ultima_fecha_pago,'0099-01-01'::date",
                    "transaccion_anulacion,0"};
                                        
                    sql += Util.insertFromSelect("cxp_doc","cxp_doc", columnas, reemp, "",  "fin", "fin" );
                    
                    where += " WHERE dstrct = ? AND proveedor = ? AND documento = ? AND tipo_documento = ?;";

                    update = this.obtenerSQL("SQL_UPDATE_SALDO_ABONO_CXP_DOC");
                    
                    
                    sql += where;
                    sql += update;
                    
                    st = new StringStatement(sql, true);//JJCastro fase2
                    
                    st.setString( 1, c.getDstrct() );
                    st.setString( 2, c.getProveedor() );
                    st.setString( 3, c.getDocumento() );
                    st.setString( 4, c.getTipo_documento() );
                    
                    st.setDouble( 5, c.getVlr_saldo()*-1 );
                    st.setDouble( 6, c.getVlr_saldo_me()*-1 );
                    st.setDouble( 7, c.getVlr_saldo() );
                    st.setDouble( 8, c.getVlr_saldo_me() );
                    st.setString( 9, c.getCreation_user() );
                    st.setString( 10, c.getDstrct() );
                    st.setString( 11, c.getProveedor() );
                    st.setString( 12, c.getTipo_documento() );
                    st.setString( 13, c.getDocumento() );
                    
                    sql = st.getSql();//JJCastro fase2
                    
                    OPItems item = new OPItems();
                    
                    item.setDstrct(         c.getDstrct() );
                    item.setProveedor(      c.getProveedor() );
                    item.setTipo_documento( "035" );
                    item.setDocumento(      "NCCP_" + c.getDocumento() );
                    item.setItem(           "001" );
                    item.setDescconcepto(   "CAMBIO PROPIETARIO" );
                    item.setVlr(            c.getVlr_saldo() );
                    item.setVlr_me(         c.getVlr_saldo_me() );
                    item.setCodigo_cuenta(  "" );
                    item.setCodigo_abc(     "" );
                    item.setPlanilla(       c.getPlanilla() );
                    item.setCreation_user(  c.getCreation_user() );
                    item.setBase(           c.getBase() );
                    item.setAuxiliar(       "" );
                    item.setConcepto(       "" );
                    
                    sql += this.insertItem( item, item.getCreation_user() );
                    
            }

            //System.out.println(sql);
            
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en OpDAO.crearNotaCreditoOP "+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
        
    }


/**
 *
 * @param c
 * @param p
 * @param signo
 * @return
 * @throws Exception
 */
    public String crearFacturaCambioPropietarioOP( CXP_Doc c, Proveedor p, int signo ) throws Exception{

        StringStatement st    = null;
        String sql   = "";
        String where = "";

        try{
            
            String columnas = "";
            
            PGTable t_cxp_doc = PGTable.getInstance( "cxp_doc","fin" );
            
            if( t_cxp_doc != null ){
                
                columnas       = t_cxp_doc.getColumns_names();
                
                String doc = (signo == -1)? "documento,'CP_'||documento::text":"documento,documento";
                
                String[] reemp  = {
                    "reg_status,''::text",
                    "proveedor,'"+p.getC_nit()+"'::text",
                    doc,
                    "fecha_aprobacion,now()",
                    "aprobador,'"+c.getCreation_user()+"'::text",
                    "vlr_neto,vlr_neto*"+signo,
                    "vlr_saldo,vlr_neto*"+signo,
                    "vlr_total_abonos,0",
                    "vlr_neto_me,vlr_neto_me*"+signo,
                    "vlr_saldo_me,vlr_neto_me*"+signo,
                    "vlr_total_abonos_me,0",
                    "handle_code,'"+p.getC_hc()+"'::text",
                    "usuario_aprobacion,'"+c.getCreation_user()+"'::text",
                    "usuario_contabilizo,''::text",
                    "fecha_contabilizacion,'0099-01-01'::timestamp",
                    "usuario_anulo,''::text",
                    "fecha_anulacion,'0099-01-01'::timestamp",
                    "fecha_contabilizacion_anulacion,'0099-01-01'::timestamp",
                    "user_update,''::text",
                    "creation_date,now()",
                    "creation_user,'"+c.getCreation_user()+"'::text",
                    "corrida,''::text",
                    "cheque,''::text",
                    "fecha_contabilizacion_ajc,'0099-01-01'::timestamp",
                    "fecha_contabilizacion_ajv,'0099-01-01'::timestamp",
                    "usuario_contabilizo_ajc,''::text",
                    "usuario_contabilizo_ajv,''::text",
                    "transaccion_ajc,0",
                    "transaccion_ajv,0",                    
                    "transaccion,0",
                    "fecha_documento,now()::date",
                    "fecha_vencimiento,now()::date + '"+ p.getPlazo() +" days'::interval",
                    "ultima_fecha_pago,'0099-01-01'::date",
                    "transaccion_anulacion,0"};
                                        
                    sql += Util.insertFromSelect("cxp_doc","cxp_doc", columnas, reemp, "",  "fin", "fin" );
                    
                    where += " WHERE dstrct = ? AND proveedor = ? AND documento = ? AND tipo_documento = ?;";
                    
                    sql += where;
                    
                    st = new StringStatement(sql, true);//JJCastro fase2
                    
                    st.setString( 1, c.getDstrct() );
                    st.setString( 2, c.getProveedor() );
                    st.setString( 3, c.getDocumento() );
                    st.setString( 4, c.getTipo_documento() );
                    
                    sql = st.getSql();//JJCastro fase2
                    
                    if( signo == 1 ){
                        sql += " UPDATE planilla SET factura = '"+c.getDocumento()+"', nitpro = '"+p.getC_nit()+"' WHERE numpla = '"+c.getPlanilla()+"';"; 
                    }
            }

        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en OpDAO.crearFacturaCambioPropietarioOP "+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
        
    }


/**
 *
 * @param c
 * @param p
 * @param signo
 * @return
 * @throws Exception
 */
    public String crearItemsFacturaCambioPropietarioOP( CXP_Doc c, Proveedor p, int signo ) throws Exception{

        StringStatement st    = null;        
        String sql   = "";
        String where = "";
        try{
            
            String columnas = "";
            PGTable t_cxp_doc = PGTable.getInstance( "cxp_items_doc","fin" );
            
            if( t_cxp_doc != null ){
                
                columnas       = t_cxp_doc.getColumns_names();                
                
                String doc = (signo == -1)? "documento,'CP_'||documento":"documento,documento";
                
                String[] reemp  = {
                    "reg_status,''::text",
                    doc,
                    "proveedor,'"+p.getC_nit()+"'::text",
                    "last_update,'0099-01-01'::timestamp",
                    "vlr,vlr*"+signo,
                    "vlr_me,vlr_me*"+signo,
                    "user_update,''::text",
                    "creation_date,now()",
                    "creation_user,'"+c.getCreation_user()+"'::text"};
                      
                    sql += Util.insertFromSelect("cxp_items_doc","cxp_items_doc", columnas, reemp, "",  "fin", "fin" );
                    
                    where += " WHERE dstrct = ? AND proveedor = ? AND documento = ? AND tipo_documento = ?;";
                    
                    sql += where;
                    
                    st = new StringStatement(sql, true);//JJCastro fase2
                    
                    st.setString( 1, c.getDstrct() );
                    st.setString( 2, c.getProveedor() );
                    st.setString( 3, c.getDocumento() );
                    st.setString( 4, c.getTipo_documento() );
                    
                    sql = st.getSql();//JJCastro fase2
            }
            
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en OpDAO.crearItemsFacturaCambioPropietarioOP "+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }               
        return sql;
        
    }


/**
 *
 * @param c
 * @param p
 * @param signo
 * @return
 * @throws Exception
 */
    public String crearImpItemsFacturaCambioPropietarioOP( CXP_Doc c, Proveedor p, int signo ) throws Exception{
        StringStatement st    = null;
        
        String sql   = "";
        String where = "";
        
        try{
            
            String columnas = "";
            
            PGTable t_cxp_doc = PGTable.getInstance( "cxp_imp_item","fin" );
            
            if( t_cxp_doc != null ){
                
                columnas       = t_cxp_doc.getColumns_names();                
                
                String doc = (signo == -1)? "documento,'CP_'||documento":"documento,documento";
                
                String[] reemp  = {
                    "reg_status,''::text",
                    "proveedor,'"+p.getC_nit()+"'::text",
                    doc,
                    "vlr_total_impuesto,vlr_total_impuesto*"+signo,
                    "vlr_total_impuesto_me,vlr_total_impuesto_me*"+signo,
                    "last_update,'0099-01-01'",
                    "user_update,''::text",
                    "creation_date,now()",
                    "creation_user,'"+c.getCreation_user()+"'"};
                      
                    sql += Util.insertFromSelect("cxp_imp_item","cxp_imp_item", columnas, reemp, "",  "fin", "fin" );
                    
                    where += " WHERE dstrct = ? AND proveedor = ? AND documento = ? AND tipo_documento = ?;";
                    
                    sql += where;
                    
                    st = new StringStatement(sql, true);//JJCastro fase2
                    
                    st.setString( 1, c.getDstrct() );
                    st.setString( 2, c.getProveedor() );
                    st.setString( 3, c.getDocumento() );
                    st.setString( 4, c.getTipo_documento() );
                    
                    sql = st.getSql();//JJCastro fase2
            }
            
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en OpDAO.crearImpItemsFacturaCambioPropietarioOP "+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
        
    }
    //Operez 23-04-2007
/**
 * 
 * @param c
 * @param p
 * @param signo
 * @param tipo_doc
 * @return
 * @throws Exception
 */
    public String crearFacturaCambioPropietarioOP( CXP_Doc c, Proveedor p, int signo, String tipo_doc ) throws Exception{        
        
        StringStatement st    = null;//JJCastro fase2
        
        String sql   = "";
        String where = "";
        
        try{
            
            String columnas = "";
            
            PGTable t_cxp_doc = PGTable.getInstance( "cxp_doc","fin" );
            
            if( t_cxp_doc != null ){
                
                columnas       = t_cxp_doc.getColumns_names();
                
                String doc = (signo == -1)? "documento,'CP_'||documento::text":"documento,documento";
                
                String doc_rel = "";
                String tipo_doc_rel = "";
                String desc = "";
                
                if( tipo_doc.equals("035") ){ 
                    doc = "documento,'NCCP_'||documento";
                    tipo_doc_rel = "tipo_documento_rel,'010'::text";
                    doc_rel = "documento_relacionado,documento";
                    desc = "descripcion,'NOTA CREDITO CAMBIO PROPIETARIO'::text";
                }else{
                    desc = "descripcion,descripcion";
                    tipo_doc_rel = "tipo_documento_rel,tipo_documento_rel";
                    doc_rel = "documento_relacionado,documento_relacionado";
                }
                 
                String[] reemp  = {
                    "reg_status,''::text",
                    "proveedor,'"+p.getC_nit()+"'::text",
                    doc,
                    doc_rel,
                    tipo_doc_rel,
                    desc,
                    "tipo_documento,'"+tipo_doc+"'::text",
                    "fecha_aprobacion,now()",
                    "aprobador,'"+c.getCreation_user()+"'::text",
                    "vlr_neto,vlr_neto*"+signo,
                    "vlr_saldo,vlr_neto*"+signo,
                    "vlr_total_abonos,0",
                    "vlr_neto_me,vlr_neto_me*"+signo,
                    "vlr_saldo_me,vlr_neto_me*"+signo,
                    "vlr_total_abonos_me,0",
                    "handle_code,'"+p.getC_hc()+"'::text",
                    "usuario_aprobacion,'"+c.getCreation_user()+"'::text",
                    "usuario_contabilizo,''::text",
                    "fecha_contabilizacion,'0099-01-01'::timestamp",
                    "usuario_anulo,''::text",
                    "fecha_anulacion,'0099-01-01'::timestamp",
                    "fecha_contabilizacion_anulacion,'0099-01-01'::timestamp",
                    "user_update,''::text",
                    "creation_date,now()",
                    "creation_user,'"+c.getCreation_user()+"'::text",
                    "corrida,''::text",
                    "cheque,''::text",
                    "fecha_contabilizacion_ajc,'0099-01-01'::timestamp",
                    "fecha_contabilizacion_ajv,'0099-01-01'::timestamp",
                    "usuario_contabilizo_ajc,''::text",
                    "usuario_contabilizo_ajv,''::text",
                    "transaccion_ajc,0",
                    "transaccion_ajv,0",                    
                    "transaccion,0",
                    "fecha_documento,now()::date",
                    "fecha_vencimiento,now()::date + '"+ p.getPlazo() +" days'::interval",
                    "ultima_fecha_pago,'0099-01-01'::date",
                    "transaccion_anulacion,0"};
                                        
                    sql += Util.insertFromSelect("cxp_doc","cxp_doc", columnas, reemp, "",  "fin", "fin" );
                    
                    where += " WHERE dstrct = ? AND proveedor = ? AND documento = ? AND tipo_documento = ?;";
                    
                    sql += where;
                                        
                    st = new StringStatement(sql, true);//JJCastro fase2
                    
                    st.setString( 1, c.getDstrct() );
                    st.setString( 2, c.getProveedor() );
                    st.setString( 3, c.getDocumento() );
                    st.setString( 4, c.getTipo_documento() );
                    
                    sql = st.getSql();//JJCastro fase2
                    
                    if( signo == 1 && tipo_doc.equals("010") ){
                        sql += " UPDATE planilla SET factura = '"+c.getDocumento()+"', nitpro = '"+p.getC_nit()+"' WHERE numpla = '"+c.getPlanilla()+"';"; 
                    }
            }
            
        }catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en OpDAO.crearFacturaCambioPropietarioOP "+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        
        
        return sql;
        
    }
    
   

}
