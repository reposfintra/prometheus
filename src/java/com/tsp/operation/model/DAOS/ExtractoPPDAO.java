/***********************************************
 * Nombre       Clase  LiquidarPPDAO.java 
 * Autor        ING JULIO BARROS RUEDA
 * Fecha        25/11/2006
 * Copyright    Transportes Sanchez Polo S.A.
 **********************************************/


package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;


import com.tsp.util.connectionpool.*;

public class ExtractoPPDAO extends MainDAO{
    
    private Planilla pla;
    
       
    public ExtractoPPDAO(String dataBaseName) {
        super("ExtractoPPDAO.xml",dataBaseName);  
    }
    
    /**
     * M�todo buscar ExtractosPP realizados
     * @autor   JULIO BARROS RUEDA
     * @param   parametro de busqueda
     * @param   tipo, tipo de filtro
     * @throws  Exception
     * @version 1.0.
     **/
    public Vector buscarExtractosPP(String propietario, int tipo,String fechai,String fechaf) throws Exception {
        Connection         con    = null;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;             
        Vector          Extractos = new Vector();
        try{            
            String restriccion = (tipo==0?" and reg_status = 'P' ": tipo==1 ? " and reg_status = '' "  : "");
            restriccion += ( !propietario.equals("") ? "and nit = ? " : "");
            String SQL = this.obtenerSQL("SQL_EXTRACTOS").replaceAll("#RESTRICCION#",restriccion);
            con = this.conectarJNDI("SQL_EXTRACTOS");//JJCastro fase2
            st= con.prepareStatement(SQL);
            st.setString(1, fechai + " 00:00:00");
            st.setString(2, fechaf + " 23:59:59");
            if(!propietario.equals(""))
                st.setString(3, propietario);
            rs = st.executeQuery();
            while (rs.next()){
                Extracto extr = new Extracto();
                extr.setReg_status( rs.getString("reg_status") );
                extr.setDstrct( rs.getString("dstrct") );
                extr.setNit( rs.getString("nit") );
                extr.setFecha( rs.getString("fecha") );
                extr.setVlr_pp( Float.parseFloat(rs.getString("vlr_pp")) );
                extr.setVlr_ppa( Float.parseFloat(rs.getString("vlr_ppa")) );
                extr.setBanco( rs.getString("banco") );
                extr.setSucursal( rs.getString("sucursal") );
                extr.setFecha_aprobacion( rs.getString("fecha_aprobacion") );
                extr.setNombre_trans(rs.getString("nombre"));
                extr.setSecuencia(rs.getInt("secuencia"));
                extr.setCreation_date(rs.getString("creation_date"));
                Extractos.add(extr);
                extr = null;//Liberar Espacio JJCastro
            }
        }catch(Exception e){
            throw new SQLException("DAO:  buscarExtractosPP() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Extractos;
    }
    

/**
 * 
 * @param propietario
 * @param fecha
 * @param tipo
 * @return
 * @throws Exception
 */
    public Vector buscarExtractoDetallePP(String propietario,String fecha,int tipo) throws Exception {
        Connection         con    = null;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;  
        Vector    ExtractoDetalle = new Vector();
        try{
            con = this.conectarJNDI("SQL_EXTRACTO_DETALLE_PL");//JJCastro fase2

            if(tipo==1)
                st = con.prepareStatement(this.obtenerSQL("SQL_EXTRACTO_DETALLE_PL"));//JJCastro fase2
            else if(tipo==2)
                st = con.prepareStatement(this.obtenerSQL("SQL_EXTRACTO_DETALLE_PR"));//JJCastro fase2
            else
                st = con.prepareStatement(this.obtenerSQL("SQL_EXTRACTO_DETALLE_PK"));//JJCastro fase2
            st.setString(1, propietario);
            st.setString(2, fecha);
            rs = st.executeQuery();
            while (rs.next()){
                ExtractoDetalle extrd = new ExtractoDetalle();
                extrd.setReg_status     ( rs.getString("reg_status") );
                extrd.setDstrct         ( rs.getString("dstrct") );     
                extrd.setNit            ( rs.getString("nit") );        
                extrd.setFecha          ( rs.getString("fecha") );  
                extrd.setTipo_documento ( rs.getString("tipo_documento") );
                extrd.setDocumento      ( rs.getString("documento") );  
                extrd.setConcepto       ( rs.getString("concepto") );             
                extrd.setDescripcion    ( rs.getString("descripcion") );                       
                extrd.setFactura        ( rs.getString("factura") );      
                extrd.setVlr            ( Float.parseFloat( rs.getString("vlr") ));        
                extrd.setRetefuente     ( Float.parseFloat( rs.getString("retefuente") )); 
                extrd.setReteica        ( Float.parseFloat( rs.getString("reteica") ));
                extrd.setImpuestos      ( Float.parseFloat( rs.getString("impuestos") ));               
                extrd.setVlr_pp_item    ( Float.parseFloat( rs.getString("vlr_pp_item") )); 
                extrd.setVlr_ppa_item   ( Float.parseFloat( rs.getString("vlr_ppa_item") ));         
                extrd.setCreation_user  ( rs.getString("creation_user") );     
                extrd.setCreation_date  ( rs.getString("creation_date") );
                extrd.setUser_update    ( rs.getString("user_update") );
                extrd.setLast_update    ( rs.getString("last_update") );
                extrd.setBase           ( rs.getString("base") );   
                
                if(tipo==1){
                    extrd.setFecdsp         ( rs.getString("fecdsp") );                  
                    extrd.setOripla         ( rs.getString("oripla") );  
                    extrd.setDespla         ( rs.getString("despla") );
                    extrd.setPlaveh         ( rs.getString("placa") );       
                    extrd.setNom            ( rs.getString("nom") );
                    extrd.setNomc           ( rs.getString("nomc") );
                    extrd.setNumrem         ( rs.getString("numrem") );                              
                    extrd.setStapla         ( rs.getString("stapla") );	     
                    extrd.setPesoreal       ( rs.getString("pesoreal") );     
                    extrd.setUnit_vlr       ( rs.getString("unit_vlr") );
                    extrd.setRemision       ( rs.getString("remision"));
                }
                
                extrd.setSecuencia(rs.getInt("secuencia"));
                
                ExtractoDetalle.add(extrd);
                
            }
        }catch(Exception e){
            throw new SQLException("DAO:  buscarExtractoDetallePP(String propietario,String fecha,int tipo) : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ExtractoDetalle;
    }
    

/**
 * 
 * @param secuencia
 * @return
 * @throws Exception
 */
    public Vector buscarExtractoDetallePP(String secuencia) throws Exception {
        Connection         con    = null;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;  
        Vector    Extractos       = new Vector();
        String query = "SQL_EXTRACTO_DETALLE_SEC";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, secuencia);
            rs = st.executeQuery();
            while (rs.next()){
                Extracto extr = new Extracto();
                extr.setReg_status( rs.getString("reg_status") );
                extr.setDstrct( rs.getString("dstrct") );
                extr.setNit( rs.getString("nit") );
                extr.setFecha( rs.getString("fecha") );
                extr.setVlr_pp( Float.parseFloat(rs.getString("vlr_ppa_item")) );
                extr.setBanco( rs.getString("documento") );
                extr.setCreation_date(rs.getString("creation_date"));
                extr.setNombre_trans(rs.getString("nombre"));
                extr.setSecuencia(rs.getInt("secuencia"));
                Extractos.add(extr);
                extr = null;//Liberar Espacio JJCastro
            }
        }}catch(Exception e){
            throw new SQLException("DAO:  buscarExtractoDetallePP(secuencia) : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Extractos;
    }
    
    
    
/**
 * 
 * @param propietario
 * @param fecha
 * @return
 * @throws Exception
 */
    public Extracto buscarExtractoPP(String propietario,String fecha) throws Exception {
        Connection         con    = null;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;  
        Extracto         extr     = new Extracto();
        String query = "SQL_EXTRACTO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fecha);
            st.setString(2, propietario);
            rs = st.executeQuery();
            while (rs.next()){
                extr.setReg_status( rs.getString("reg_status") );
                extr.setDstrct( rs.getString("dstrct") );
                extr.setNit( rs.getString("nit") );
                extr.setFecha( rs.getString("fecha") );
                extr.setVlr_pp( Float.parseFloat(rs.getString("vlr_pp")) );
                extr.setVlr_ppa( Float.parseFloat(rs.getString("vlr_ppa")) );
                extr.setBanco( rs.getString("banco") );
                extr.setSucursal( rs.getString("sucursal") );
                extr.setFecha_aprobacion( rs.getString("fecha_aprobacion") );
                extr.setNombre_trans(rs.getString("nombre"));
                extr.setSecuencia(rs.getInt("secuencia"));
            }
        }}catch(Exception e){
            throw new SQLException("DAO:  buscarExtractoPP() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return extr;
    }



/**
 *
 * @param oc
 * @param fecha
 * @param fecha2
 * @return
 * @throws Exception
 */
    public String buscarExtractoPPOC(String oc,String fecha,String fecha2) throws Exception {
        Connection         con    = null;
        PreparedStatement   st    = null;        
        ResultSet           rs    = null;  
        String             nit    = "";
        String query = "SQL_EXTRACTO_DETALLE_OC";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fecha + " 00:00:00");
            st.setString(2, fecha2+ " 23:59:59");
            st.setString(3, oc);            
            rs = st.executeQuery();
            while (rs.next()){
                nit = rs.getString("nit");
            }
        }}catch(Exception e){
            throw new SQLException("DAO:  buscarExtractoPPOC() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nit;
    }



  /**
   *
   * @return
   * @throws Exception
   */
    public int bucar_Secuencia() throws Exception {
        Connection         con    = null;
        PreparedStatement   st    = null;        
        ResultSet           rs    = null;  
        int                sec    = 0;
        String query = "SQL_BUSCAR_SECUENCIA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()) {
                    sec = rs.getInt("secuencia");
                }
            }
        }catch(Exception e){
            throw new SQLException("DAO:  bucar_Secuencia() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sec;
    }


/**
 * 
 * @param oc
 * @param fecha
 * @param fecha2
 * @return
 * @throws Exception
 */
    public Vector buscarExtractoPPOC_Fechas(String oc,String fecha,String fecha2) throws Exception {
        Connection         con    = null;
        PreparedStatement   st    = null;        
        ResultSet           rs    = null;          
        Vector             vec2    = new Vector();
        String query = "SQL_EXTRACTO_DETALLE_OC_FECHA";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, fecha + " 00:00:00");
                st.setString(2, fecha2 + " 23:59:59");
                st.setString(3, oc);
                rs = st.executeQuery();
                while (rs.next()) {
                    Vector vec = new Vector();
                    vec.add(rs.getString("fecha"));
                    vec.add(rs.getString("vlr_ppa_item"));
                    vec2.add(vec);
                }

            }
        }catch(Exception e){
            throw new SQLException("DAO:  buscarExtractoPPOC_Fecha() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vec2;
    }
    
/**
 *
 * @param propietario
 * @param tipo
 * @param fechas
 * @return
 * @throws Exception
 */
    public Vector buscarExtractosPP_Fechas(String propietario, int tipo,Vector fechas) throws Exception {
        Connection         con    = null;
        PreparedStatement  st     = null;        
        ResultSet          rs     = null;             
        Vector          Extractos = new Vector();
        Vector              Fecha = new Vector();
        String          FechaT    ="";
        try{
            String restriccion ="";
            for(int i=0;i<fechas.size();i++){
                if(i==0){
                    restriccion =" and (";
                }
                if(i==0){
                    restriccion += " fecha = ?";
                }else{
                    restriccion += " or fecha = ?";
                }
                if(i==fechas.size()-1)
                    restriccion +=")";
            }
            String SQL = this.obtenerSQL("SQL_EXTRACTOS_FECHAS").replaceAll("#RESTRICCION#",restriccion);
            con = this.conectarJNDI("SQL_EXTRACTOS_FECHAS");//JJCastro fase2
            st= con.prepareStatement(SQL);
            st.setString(1, propietario);
            int c=2;
            for(int i=0;i<fechas.size();i++){
                Fecha  = (Vector) fechas.get(i);
                FechaT = ""+Fecha.get(0);
                st.setString(c, FechaT);
                c++;
            }
            c=0;
            rs = st.executeQuery();
            while (rs.next()){
                Fecha  = (Vector) fechas.get(c);
                FechaT = ""+Fecha.get(1);
                Extracto extr = new Extracto();
                extr.setReg_status( rs.getString("reg_status") );
                extr.setDstrct( rs.getString("dstrct") );
                extr.setNit( rs.getString("nit") );
                extr.setFecha( rs.getString("fecha") );
                extr.setVlr_pp( Float.parseFloat(rs.getString("vlr_pp")) );
                extr.setVlr_ppa( Float.parseFloat(rs.getString("vlr_ppa")) );
                extr.setBanco( rs.getString("banco") );
                extr.setSucursal( rs.getString("sucursal") );
                extr.setFecha_aprobacion( rs.getString("fecha_aprobacion") );
                extr.setNombre_trans(rs.getString("nombre"));
                extr.setBanco_trans(""+FechaT);
                extr.setSecuencia(rs.getInt("secuencia"));
                extr.setCreation_date(rs.getString("creation_date"));
                Extractos.add(extr);
                c++;
            }
        }catch(Exception e){
            throw new SQLException("DAO:  buscarExtractosPP_Fechas() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return Extractos;
    }
    
    
    
/**
 * 
 * @param fecha
 * @return
 * @throws Exception
 */
    public float retornar_vlr_pp_e(String fecha) throws Exception {
        Connection         con    = null;
        PreparedStatement   st    = null;        
        ResultSet           rs    = null;  
        float              sec    = 0;
        String query = "SQL_RETORNAR_VLR_PP_E";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, fecha);
            rs = st.executeQuery();
            while (rs.next()){
                sec = rs.getFloat("valor");
            }
        }}catch(Exception e){
            throw new SQLException("DAO:  retornar_vlr_pp_e() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sec;
    }
    

/**
 *
 * @param documento
 * @param fecha
 * @return
 * @throws Exception
 */
    public float retornar_vlr_pp_ed(String documento,String fecha) throws Exception {
        Connection         con    = null;
        PreparedStatement   st    = null;        
        ResultSet           rs    = null;  
        float              sec    = 0;
        String query = "SQL_RETORNAR_VLR_PP_ED";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, documento);
            st.setString(2, fecha);
            rs = st.executeQuery();
            while (rs.next()){
                sec = rs.getFloat("valor");
            }
        }}catch(Exception e){
            throw new SQLException("DAO:  retornar_vlr_pp_ed() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sec;
    }
     
    //Julio Barros 21/01/2007
/**
 *
 * @param secuencia
 * @return
 * @throws Exception
 */
    public List buscarBanco_Sucursal(String secuencia) throws Exception {
        Connection         con    = null;
        PreparedStatement   st    = null;        
        ResultSet           rs    = null;          
        List seleccion = new LinkedList();
        String query = "SQL_BUSCAR_BANCO_SUCURSAL";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, secuencia);       
            rs = st.executeQuery();
            while (rs.next()){
                 Hashtable  Rep  =  new  Hashtable();
                   Rep.put("banco",     (  rs.getString("banco")  ) );
                   Rep.put("sucursal",  (  rs.getString("sucursal")  ) );
                seleccion.add( Rep );
               
            }
            
        }}catch(Exception e){
            throw new SQLException("DAO:  buscarBanco_Sucursal() : "+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return seleccion;
    }


 /**
  *
  * @param secuencia
  * @param Banco
  * @param Sucursal
  * @throws SQLException
  */
    public void updateExtracto(String secuencia,String Banco,String Sucursal)throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql ="";
        String query = "SQL_UPDATEEXTRACTO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Banco);
                st.setString(2, Sucursal);
                st.setString(3, secuencia);
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL EXTRACTO " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        //return sql;
    }
    
    
}
