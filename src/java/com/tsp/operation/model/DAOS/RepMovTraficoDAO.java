/***********************************************************************************
 * Nombre ......................RepMovTrafico.java
 * Descripci�n..................Clase DAO para Reportes De Movimiento De Tr�fico
 * Autor........................Armando Oviedo
 * Fecha........................16/09/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;
/**
 *
 * @author Armando Oviedo
 */
public class RepMovTraficoDAO extends MainDAO {
    
    private RepMovTrafico rmt;
    private Connection con;
    private PoolManager pm;
    private DatosPlanillaRMT dp;
    private Vector tu = new Vector();
    private Vector reportes = new Vector();
    private RepMovTrafico ultrmt;
    private RepMovTrafico proxrmt;
    private Vector pc;
    private Vector caravana;
    private Vector clientes;
    private Vector planillas;
    
    //Ivan DArio Gomez
    private String      FECHA_ULTIMO   = "";
    private String      FECHA_ANTERIOR = "";
    private boolean     UnSoloRegistro = false;
    
    //Diogenes Bastidas
    private Vector urbanosUrbanos;
    private Vector urbanosNoAplica;
    private Vector vacios;
    private Vector vecsjsintrafico;

    
    /** Creates a new instance of RepMovTraficoDAO */
    public RepMovTraficoDAO() {
        super("RepMovTraficoDAO.xml");
    }
    
    /**
     * M�todo que retorna un objeto DatosPlanillaRMT
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @return.......objeto DatosPlanillaRMT
     **/
    public DatosPlanillaRMT getDatosPlanillaRMT() {
        return dp;
    }
    
    /**
     * M�todo que setea un Reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @param.......Objeto RepMovTrafico
     * @version.....1.0.
     **/
    public void setRMT(RepMovTrafico rmt){
        this.rmt = rmt;
    }
    /**
     * M�todo que setea el �ltimo reporte de movimiento de tr�fico encontrado
     * @autor.......Armando Oviedo
     * @param.......Objeto RepMovTrafico
     * @version.....1.0.
     **/
    public void setUltRMT(RepMovTrafico ultrmt){
        this.ultrmt = ultrmt;
    }
    
    /**
     * M�todo que retorna el �ltimo reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return.......objeto RepMovTrafico
     **/
    public RepMovTrafico getUltRMT(){
        return this.ultrmt;
    }
    
    /**
     * M�todo que setea el pr�ximo reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @param.......Reporte de movimiento de tr�fico
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void setProxRMT(RepMovTrafico proxrmt){
        this.proxrmt = proxrmt;
    }
    
    /**
     * M�todo que retorna el pr�ximo reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return.......objeto reporte de movimiento de tr�fico
     **/
    public RepMovTrafico getProxRMT(){
        return this.proxrmt;
    }
    
    /**
     * M�todo que setea los datos de una planilla existente
     * @autor.......Armando Oviedo
     * @param.......Objeto DatosPlanillaRMT
     * @version.....1.0.
     **/
    public void setDatosPlanillaRMT(DatosPlanillaRMT dp){
        this.dp = dp;
    }
    
    /**
     * M�todo que retorna un Vector con los tipos de ubicaciones disponibles
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return.......Vector de tipos de ubicaciones: c�digo y descripci�n
     **/
    public Vector getVecTipoUbicaciones(){
        return tu;
    }
    
    /**
     * M�todo que adiciona un registro
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String addRMT() throws SQLException{
        PreparedStatement ps = null;
        String sql ="";
        try{
            ps = crearPreparedStatement("SQL_INGRESAR_RMT");
            ps.setString(1, rmt.getDstrct());
            ps.setString(2, rmt.getNumpla());
            ps.setString(3, rmt.getObservacion());
            ps.setString(4, rmt.getTipo_procedencia());
            ps.setString(5, rmt.getUbicacion_procedencia());
            ps.setString(6,  rmt.getTipo_reporte());
            ps.setString(7, rmt.getCreation_user());
            ps.setString(8, rmt.getBase());
            ps.setString(9, rmt.getFechaReporte());
            ps.setString(10, rmt.getZona());
            ps.setString(11, rmt.getFec_rep_pla());
            ps.setString(12, rmt.getCausa());
            ps.setString(13, rmt.getClasificacion());
            ps.setString(14, rmt.getCreation_date());
            ps.setString(15, rmt.getLast_update());
            sql = ps.toString();
        }catch(SQLException ex){
            throw new SQLException("ERROR AL INGRESAR EL REPORTE DE MOVIMIENTO DE TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_INGRESAR_RMT");
        }
        return sql;
    }
    
    /**
     * M�todo que actualiza un reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String updIfExists() throws SQLException{
        PreparedStatement ps = null;
        String sql ="";
        try{
            ps = crearPreparedStatement("SQL_UPDATE_RMT_ANULADO");
            ps.setString(1, rmt.getDstrct());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, rmt.getTipo_procedencia());
            ps.setString(4, rmt.getUbicacion_procedencia());
            ps.setString(5, rmt.getTipo_reporte());
            ps.setString(6, rmt.getCreation_user());
            ps.setString(7, rmt.getBase());
            ps.setString(8, rmt.getZona());
            ps.setString(9, rmt.getNumpla());
            ps.setString(10,rmt.getFechaReporte());
            sql = ps.toString();
            //System.out.println("SQL_UPDATE_RMT_ANULADO-->"+ps.toString());
            //ps.executeUpdate();
            
            actualizarIngresoTrafico();//se actualizan las tablas de tr�fico e ingreso tr�fico al agregar...
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL INGRESAR EL REPORTE DE MOVIMIENTO DE TRAFICO, IMPOSIBLE ACTUALIZAR. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_RMT_ANULADO");
        }
        return sql;
    }
    
    /**
     * M�todo que retorna true si existe un reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean existeReporte() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        try{
            ps = crearPreparedStatement("SQL_EXISTE_RMT");
            ps.setString(1, rmt.getNumpla());
            ps.setString(2, rmt.getFechaReporte());
            rs = ps.executeQuery();
            while(rs.next()){
                existe = true;
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL COMPROBAR SI EL REGISTRO EXISTE."+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_EXISTE_RMT");
        }
        return existe;
    }
    
    /**
     * M�todo que retorna la duraci�n de un tramo
     * @autor.......Armando Oviedo
     * @param.......el c�digo de origen y el destino
     * @throws......SQLException
     * @version.....1.0.
     * @return.......valor double con la duraci�n del tramo en horas.
     **/
    public double getDuracionTramo(String origen, String destino) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        double duracion = 0;
        try{
            ps = crearPreparedStatement("SQL_DURACION_TRAMO");
            ps.setString(1, origen);
            ps.setString(2, destino);
            
            rs = ps.executeQuery();
            if(rs.next()){
                duracion = rs.getDouble("TIEMPO");
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LA DURACION DEL TRAMO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_DURACION_TRAMO");
        }
        return duracion;
    }
    
    /**
     * M�todo que retorna una cadena con el c�digo del pr�ximo puesto de control
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @return.......C�digo del pr�ximo puesto de control
     **/
    public String getCodNextPC(){
        if(proxrmt!=null)
            return proxrmt.getUbicacion_procedencia();
        else
            return "";
        
    }
    
    /**
     * M�todo que retorna la demora o retraso entre tramos
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @return.......La demora entre los tramos
     **/
    public double getDemora() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        double demora = 0;
        try{
            ps = crearPreparedStatement("SQL_DEMORA_TRAMO");
            ps.setString(1, dp.getNumpla());
            rs = ps.executeQuery();
            if(rs.next()){
                demora = rs.getDouble("DEMORA");
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_DEMORA_TRAMO");
        }
        return demora;
    }
    
    
    
    
    /**
     * M�todo que retorna un boolean si el tipo de despacho es manual
     * @autor.......Armando Oviedo
     * @param.......numero de planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return......boolean esdm
     **/
    public boolean esDespachoManual(String numpla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String valordm = null;
        try{
            
            ps = crearPreparedStatement("SQL_DESPMANUAL");
            ps.setString(1, numpla);
            rs = ps.executeQuery();
            return rs.next();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR LA PLANILLA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_DESPMANUAL");
        }
    }
    /**
     * M�todo que busca una planilla en tr�fico, ingreso tr�fico y en planilla la carga en un objeto DatosPlanillaRMT
     * @autor.......Karen Reales
     * @param.......numero de planilla
     * @throws......SQLException
     * @version.....1.0.
     **/
    public DatosPlanillaRMT obtenerPlanilla(String numpla) throws SQLException{
        PreparedStatement ps = null, st=null;
        ResultSet rs = null,rs2=null;
        DatosPlanillaRMT dp = null;
        Connection con = null;
        try{
            con = this.conectar("SQL_GETDATOSPLANILLA");
            if(con!=null){
                ps = con.prepareStatement(this.obtenerSQL("SQL_GETDATOSPLANILLA"));
                ps.setString(1, numpla);
                rs = ps.executeQuery();
                st = con.prepareStatement(this.obtenerSQL("SQL_CARAVANA"));
                while(rs.next()){
                    dp = new DatosPlanillaRMT();
                    dp.setNumpla(rs.getString("PLANILLA"));
                    dp.setOripla(rs.getString("ORIGEN"));
                    dp.setNomciuori(rs.getString("NOMORIGEN"));
                    dp.setDespla(rs.getString("DESTINO"));
                    dp.setNomciudest(rs.getString("NOMDESTINO"));
                    dp.setPlaveh(rs.getString("PLACA"));
                    dp.setCedcon(rs.getString("CEDCON"));
                    dp.setRutapla(rs.getString("RUTA_PLA"));
                    dp.setNombrecond(rs.getString("NOMCOND"));
                    dp.setCaravana("0");
                    dp.setStdjob(rs.getString("std_job_no"));
                    dp.setFecha_prox_rep(rs.getString("fecha_prox_reporte"));
                    dp.setPto_control_proxreporte(rs.getString("pto_control_proxreporte"));
                    //pto_control_proxreporte
                    if(rs.getString("caravana").equalsIgnoreCase("S")){
                        
                        st.setString(1, numpla);
                        rs2 = st.executeQuery();
                        if(rs2.next()){
                            dp.setCaravana(rs2.getString("NUMCARAVANA"));
                        }
                    }
                }
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LA PLANILLA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GETDATOSPLANILLA");
            
        }
        return dp;
    }
    
    /**
     * M�todo que retorna el nombre de un puesto de control
     * @autor.......Armando Oviedo
     * @param.......c�digo pc
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Nombre del puesto de control
     **/
    public String getNombreCiudad(String codciu) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nombre=null;
        try{
            ps = crearPreparedStatement("SQL_GET_NOMCIUDAD");
            ps.setString(1, codciu);
            rs = ps.executeQuery();
            while(rs.next()){
                nombre = rs.getString(1);
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR EL NOMBRE DE LA CIUDAD, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_NOMCIUDAD");
        }
        return nombre;
    }
    
    /**
     * M�todo que retorna la zona a la que pertenece una ciudad
     * @autor.......Armando Oviedo
     * @param.......C�digo ciudad
     * @throws......SQLException
     * @version.....1.0.
     * @return.......El c�digo de la zona
     **/
    public String getZona(String codciu)throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String zona="";
        try{
            ps = crearPreparedStatement("SQL_GET_CODZONA");
            ps.setString(1, codciu);
            rs = ps.executeQuery();
            while(rs.next()){
                zona = rs.getString(1);
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LA ZONA DE LA CIUDAD, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_CODZONA");
        }
        return zona;
    }
    
    /**
     * M�todo que retorna el nombre de una zona
     * @autor.......Armando Oviedo
     * @param.......C�digo de zona
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Nombre de la zona
     **/
    public String getNombreZona(String codzona)throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nomzona="";
        try{
            ps = crearPreparedStatement("SQL_GET_NOMZONA");
            ps.setString(1, codzona);
            rs = ps.executeQuery();
            while(rs.next()){
                nomzona = rs.getString(1);
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LA ZONA DE LA CIUDAD, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_NOMZONA");
        }
        return nomzona;
    }
    
    /**
     * M�todo que retorna un vector dada la secuencia de la ruta de todos los puestos de control
     * @autor.......Armando Oviedo
     * @param.......Ruta de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Vector de puestos de control: c�digo y descripci�n
     **/
    public Vector getUbicaciones() throws SQLException{
        return pc;
    }
    
    
    /**
     * M�todo que retorna un Vector de tipos de ubicaci�n
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Vector de tipos de ubicaci�n
     **/
    public Vector getTiposubicacion() throws SQLException{
        return tu;
    }
    
    /**
     * M�todo que obtiene un Vector de tipos de ubicaci�n
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarTiposubicacion() throws SQLException{
        tu = new Vector();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            
            ps = crearPreparedStatement("SQL_GET_TIPOS_UBICACION");
            rs = ps.executeQuery();
            while(rs.next()){
                Vector fila = new Vector();
                fila.add(rs.getString(1));
                fila.add(rs.getString(2));
                tu.add(fila);
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LOS TIPOS DE UBICACION, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_TIPOS_UBICACION");
        }
    }
    
    /**
     * M�todo que retorna un Vector de ubicaciones
     * @autor.......Armando Oviedo
     * @param.......c�digo del tipo de ubicaci�n
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Vector de ubicaciones: c�digo, descripci�n
     **/
    public void getOtrasUbicaciones(String tubic) throws SQLException{
        pc = new Vector();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            
            ps = crearPreparedStatement("SQL_GET_UBICACIONES");
            ps.setString(1, tubic);
            rs = ps.executeQuery();
            while(rs.next()){
                Vector fila = new Vector();
                fila.add(rs.getString(1));
                fila.add(rs.getString(2));
                pc.add(fila);
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LAS UBICACIONES, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_UBICACIONES");
        }
    }
    
    /**
     * M�todo que retorna un vector que contiene los reportes de movimiento de tr�fico pertenecientes a la planilla
     * @autor.......Armando Oviedo
     * @param.......N�mero de planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Vector de reportes de la planilla
     **/
    public Vector getReportesPlanilla() throws SQLException{
        return reportes;
    }
    
    public void resetFechas(){
        FECHA_ULTIMO   = "";
        FECHA_ANTERIOR = "";
        UnSoloRegistro = false;
    }
    
    /**
     * M�todo que setea el vector para obtener todos los reportes de una planilla
     * @autor.......Armando Oviedo
     * @param.......N�mero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarReportesPlanilla(String numpla) throws SQLException{
        reportes = new Vector();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            resetFechas();
            ps = crearPreparedStatement("SQL_BUSCAR_PLANILLA");
            ps.setString(1, numpla);
            rs = ps.executeQuery();
            rs.last();
            int NumRegistros = rs.getRow();
            rs.beforeFirst();
            while(rs.next()){
                rmt = new RepMovTrafico();
                rmt = rmt.load(rs);
                rmt.setNombre_procedencia(rs.getString("nomubicacion"));
                rmt.setDesctipo(rs.getString("desctipo"));
                rmt.setDescubicacion(rs.getString("descubicacion"));
                
                if(rs.isLast()){
                    rmt.setIsUltimo(true);
                    this.FECHA_ULTIMO   = rmt.getFechaReporte().replaceAll("-|:| |/","");
                    if(NumRegistros == 1){
                        this.UnSoloRegistro = true;
                    }
                }
                else{
                    this.FECHA_ANTERIOR = rmt.getFechaReporte().replaceAll("-|:| |/","").substring(0,12);
                }
                reportes.add(rmt);
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LOS REPORTES DE LA PLANILLA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_BUSCAR_PLANILLA");
        }
    }
    
    /**
     * M�todo que actualiza un reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @param.......fecha anterior a la actualizaci�n
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void updReport(String olddate, boolean IsUltimo) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        RepMovTrafico tmp = new RepMovTrafico();
        try{
            
            ps = crearPreparedStatement("SQL_GET_RMT");
            ps.setString(1, rmt.getNumpla());
            ps.setString(2, olddate);
            rs = ps.executeQuery();
            while(rs.next()){
                tmp = tmp.load(rs);
            }
            ps = crearPreparedStatement("SQL_UPDATE_RMT");
            ps.setString(1, rmt.getFechaReporte());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, rmt.getTipo_procedencia());
            ps.setString(4, rmt.getUbicacion_procedencia());
            ps.setString(5, rmt.getTipo_reporte());
            ps.setString(6, rmt.getZona());
            ps.setString(7, rmt.getUpdate_user());
            ps.setString(8, rmt.getBase());
            ps.setString(9, rmt.getNumpla());
            ps.setString(10, tmp.getFechaReporte());
            //System.out.println("SQL_UPDATE_RMT-->"+ps.toString());
            ps.executeUpdate();
            if(IsUltimo && !rmt.getTipo_reporte().equals("OBS")){
                updateIngrTrafico(rmt.getTipo_procedencia());
            }
            addLogMovTtrafico(tmp);//Ivan Gomez 2006-04-10
            //            updateIngrTrafico();
            //            boolean existereg = false;
            //            existereg = existeLogMovTrafico(tmp);
            //            if(!existereg){
            //                addLogMovTtrafico(tmp);
            //            }
            //            else{
            //                updLogMobTrafico(tmp);
            //            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL ACTUALIZAR EL REPORTE DE MOVIMIENTO DE TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_RMT");
            desconectar("SQL_UPDATE_RMT");
        }
    }
    
    /**
     * M�todo que retorna si existe o no un registro anulado para log de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @param.......Objeto RepMovTrafico
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean existeLogMovTrafico(RepMovTrafico tmp) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        try{
            ps = crearPreparedStatement("SQL_EXISTE_LOGMOVTRAFICO");
            ps.setString(1, tmp.getNumpla());
            ps.setString(2, tmp.getFechaReporte());
            rs = ps.executeQuery();
            while(rs.next()){
                existe = true;
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL COMPROBAR EXISTENCIA DE REGISTROS EN [EXISTELOGMOVTRAFICO]. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_EXISTE_LOGMOVTRAFICO");
        }
        return existe;
    }
    
    /**
     * M�todo que retorna un objeto DatosPlanillaRMT
     * @autor.......Armando Oviedo
     * @param.......
     * @see.........
     * @throws......SQLException
     * @version.....1.0.
     * @return.......objeto DatosPlanillaRMT
     **/
    public void addLogMovTtrafico(RepMovTrafico tmp) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = crearPreparedStatement("SQL_INGRESAR_LOGMOVTRAFICO");
            ps.setString(1, "");
            ps.setString(2, rmt.getDstrct());
            ps.setString(3, rmt.getNumpla());
            ps.setString(4, rmt.getObservacion());
            ps.setString(5, rmt.getTipo_procedencia());
            ps.setString(6, rmt.getUbicacion_procedencia());
            ps.setString(7, rmt.getTipo_reporte());
            ps.setString(8, rmt.getFechaReporte());
            ps.setString(9,  tmp.getObservacion());
            ps.setString(10,  tmp.getTipo_procedencia());
            ps.setString(11,  tmp.getUbicacion_procedencia());
            ps.setString(12,  tmp.getTipo_reporte());
            ps.setString(13, "now()");
            ps.setString(14,  tmp.getFechaReporte());
            ps.setString(15,  tmp.getUpdate_user());
            ps.setString(16,  "now()");
            ps.setString(17,  tmp.getUpdate_user());
            ps.setString(18,  rmt.getBase());
            ps.setString(19, rmt.getZona());
            ps.setString(20, tmp.getZona());
            ps.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL INGRESAR REGISTROS EN [LOG DE MOVIMIENTO DE TRAFICO]. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_INGRESAR_LOGMOVTRAFICO");
        }
    }
    
    /**
     * M�todo utilizado para actualizar un registro de log de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @param.......Objeto RepMovTrafico
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void updLogMobTrafico(RepMovTrafico tmp) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = crearPreparedStatement("SQL_UPDATE_LOGMOVTRAFICO");
            ps.setString(1, rmt.getDstrct());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, rmt.getTipo_procedencia());
            ps.setString(4, rmt.getUbicacion_procedencia());
            ps.setString(5, rmt.getTipo_reporte());
            ps.setString(6, rmt.getFechaReporte());
            ps.setString(7,  tmp.getObservacion());
            ps.setString(8,  tmp.getTipo_procedencia());
            ps.setString(9,  tmp.getUbicacion_procedencia());
            ps.setString(10,  tmp.getTipo_reporte());
            ps.setString(11, tmp.getFechaReporte());
            ps.setString(12,  tmp.getUpdate_user());
            ps.setString(13,  rmt.getBase());
            ps.setString(14, rmt.getZona());
            ps.setString(15, tmp.getZona());
            ps.setString(16, rmt.getNumpla());
            ps.setString(17, rmt.getFechaReporte());
            ps.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR REGISTROS EN [LOG DE MOVIMIENTO DE TRAFICO]. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_LOGMOVTRAFICO");
        }
    }
    
    /**
     * M�todo que eval�a si actualizar o no la tabla ingreso_tr�fico
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void updateIngrTrafico() throws SQLException{
        try{
            RepMovTrafico tmp = getLastCreatedReport();
            if(tmp.getFechaReporte().equalsIgnoreCase(rmt.getFechaReporte()) && tmp.getNumpla().equalsIgnoreCase(rmt.getNumpla())){
                actualizarIngresoTrafico();
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL ACTUALIZAR EL REPORTE DE MOVIMIENTO DE TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }
    }
    /**
     * M�todo que eval�a si actualizar o no la tabla ingreso_tr�fico
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void updateIngrTrafico(String procedencia) throws SQLException{
        try{
            RepMovTrafico tmp = getLastCreatedReport();
            if(procedencia.equals("PC")){
                actualizarIngresoTrafico2();
            }else{
                actualizarCIU();
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL ACTUALIZAR EL REPORTE DE MOVIMIENTO DE TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }
    }
   
    /**
     * M�todo que elimina un reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void delReport() throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = crearPreparedStatement("SQL_DELETE_RMT");
            ps.setString(1, rmt.getUpdate_user());
            ps.setString(2, rmt.getNumpla());
            ps.setString(3, rmt.getFechaReporte());
            ps.executeUpdate();
            ps = crearPreparedStatement("SQL_DELETE_LRMT");
            ps.setString(1, rmt.getNumpla());
            ps.setString(2, rmt.getFechaReporte());
            ps.executeUpdate();
            delupdIngrTrafico();
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ELIMINAR EL REPORTE DE MOVIMIENTO DE TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_DELETE_RMT");
            desconectar("SQL_DELETE_LRMT");
        }
    }
    
    /**
     * M�todo que setea el �ltimo reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void SearchLastCreatedReport() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_GET_LASTRMT_IT");
            ps.setString(1, dp.getNumpla());
            rs = ps.executeQuery();
            while(rs.next()){
                RepMovTrafico tmp = new RepMovTrafico();
                tmp.setNumpla(dp.getNumpla());
                tmp.setTipo_procedencia("PC");
                tmp.setUbicacion_procedencia(rs.getString(2)!=null?rs.getString(2):"");
                tmp.setFechareporte(rs.getString(3));
                tmp.setCodUbicacion(rs.getString(1)!=null?rs.getString(1):"");
                setUltRMT(tmp);
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL OBTENER EL ULTIMO REPORTE CREADO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_LASTRMT_IT");
        }
    }
    
    /**
     * M�todo que retorna un objeto RepMovTrafico seteado previamente
     * @autor.......Armando Oviedo
     * @version.....1.0.
     * @return.......objeto RepMovTrafico
     **/
    public RepMovTrafico getLastCreatedReport(){
        return this.ultrmt;
    }
    
    /**
     * M�todo que setea el �ltimo reporte de movimiento de tr�fico ingresado no anulado
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void setLastCreatedReport() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        ultrmt = null;
        try{
            ps = crearPreparedStatement("SQL_GET_LASTRMT_REPMOVTRAFICO");
            ps.setString(1, dp.getNumpla());
            ps.setString(2, dp.getNumpla());
            rs = ps.executeQuery();
            while(rs.next()){
                ultrmt = new RepMovTrafico();
                ultrmt = ultrmt.load(rs);
                setUltRMT(ultrmt);
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER EL ULTIMO REPORTE CREADO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_LASTRMT_REPMOVTRAFICO");
        }
    }
    
    /**
     * M�todo que setea un objeto de reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @param.......N�mero de planilla y la fecha del reporte
     * @throws......SQLException
     * @version.....1.0.
     **/
    
    
    /**
     * M�todo que retorna un objeto DatosPlanillaRMT
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @return.......objeto RepMovTrafico
     **/
    public RepMovTrafico getReporteMovTraf(){
        return rmt;
    }
    
    /**
     * M�todo que retorna un nombre de un tipo de ubicaci�n diferente a puesto de control
     * @autor.......Armando Oviedo
     * @param.......C�digo del tipo de ubicaci�n
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Nombre del tipo de ubicaci�n de la tabla tipo_ubicaci�n
     **/
    public String getNombreTipoUbicacion(String cod) throws SQLException{
        String nombre = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_GET_TIPOUBICACIONNAME");
            ps.setString(1, cod);
            rs = ps.executeQuery();
            while(rs.next()){
                nombre = rs.getString(1);
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER EL NOMBRE DEL TIPO DE UBICACION. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_TIPOUBICACIONNAME");
        }
        return nombre;
    }
    
    /**
     * M�todo que retorna el nombre de la ubicaci�n
     * @autor.......Armando Oviedo
     * @param.......c�digo ubicaci�n
     * @throws......SQLException
     * @version.....1.0.
     * @return.......nombre de la ubicaci�n
     **/
    public String getNombreUbicacion(String cod) throws SQLException{
        String nombre = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_GET_NOMBREUBICACION");
            ps.setString(1, cod);
            rs = ps.executeQuery();
            while(rs.next()){
                nombre = rs.getString(1);
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER EL NOMBRE DE LA UBICACION. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_NOMBREUBICACION");
        }
        return nombre;
    }
    
    /**
     * M�todo que retorna si un reporte posee una fecha anterior a la existente en ingreso_tr�fico
     * @autor.......Armando Oviedo
     * @param.......fecha a evaluar
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean esFechaAnteriorAExistente(String fecha) throws SQLException{
        boolean es = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_GET_ESFECHAANTERIOR");
            ps.setString(1, fecha);
            ps.setString(2, dp.getNumpla());
            
            rs = ps.executeQuery();
            while(rs.next()){
                es = true;
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LA ULTIMA FECHA DE REPORTE TRAFICO "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_ESFECHAANTERIOR");
        }
        return es;
    }
    
    /**
     * M�todo que retorna un boolean si la planilla tiene un reporte de entrega
     * @autor.......Jose De La Rosa
     * @param.......N�mero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean planillaConReporteEntrga(String numpla) throws SQLException{
        boolean es = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_ESPLANILLACONREPORTEENTREGA");
            ps.setString(1, numpla);
            rs = ps.executeQuery();
            while(rs.next()){
                es = true;
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER EL TIPO DE REPORTE "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ESPLANILLACONREPORTEENTREGA");
        }
        return es;
    }
    ///
    public Vector getReportesPlanilla(String numpla) throws SQLException{
        Vector reportes = new Vector();
        PreparedStatement st;
        ResultSet rs;
        try{
            st = this.crearPreparedStatement("SQL_REPORTE_PLANILLA");
            st.setString(1, numpla);
            rs = st.executeQuery();
            while(rs.next()){
                reportes.add(rmt.load(rs));
            }
            
            st.close();
            desconectar("SQL_REPORTE_PLANILLA");
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LOS REPORTES DE LA PLANILLA, "+ex.getMessage()+", "+ex.getErrorCode());
        } finally {
            desconectar("SQL_REPORTE_PLANILLA");
        }
        return reportes;
    }
    

    
    /**
     * Getter for property rmt.
     * @return Value of property rmt.
     */
    public com.tsp.operation.model.beans.RepMovTrafico getRmt() {
        return rmt;
    }
    
    /**
     * Setter for property rmt.
     * @param rmt New value of property rmt.
     */
    public void setRmt(com.tsp.operation.model.beans.RepMovTrafico rmt) {
        this.rmt = rmt;
    }
    
    /**
     * M�todo que busca un grupo de planilla que pertenece a una caravana
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarPlanillasCaravana() throws SQLException{
        RepMovTrafico rmt = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            caravana = new Vector();
            ps = crearPreparedStatement("SQL_PLANILLA_CARAVANA");
            ps.setString(1, dp.getCaravana());
            ps.setString(2, dp.getNumpla());
            
            rs = ps.executeQuery();
            if(rs.next()){
                caravana.add(rs.getString("numpla"));
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER EL REPORTE DE MOVIMIENTO DE TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PLANILLA_CARAVANA");
        }
    }
    
    /**
     * Getter for property caravana.
     * @return Value of property caravana.
     */
    public java.util.Vector getCaravana() {
        return caravana;
    }
    
    /**
     * Setter for property caravana.
     * @param caravana New value of property caravana.
     */
    public void setCaravana(java.util.Vector caravana) {
        this.caravana = caravana;
    }
    
    
    /**
     * M�todo que verifica si existe una planilla en ingreso_trafico
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public boolean estaPlanilla(String numpla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        boolean esta =false;
        try{
            ps = crearPreparedStatement("SQL_PLANILLA_INGTRAFICO");
            ps.setString(1, numpla);
            rs = ps.executeQuery();
            if(rs.next()){
                esta = true;
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR VERIFICANDO LA PLANILLA EN ING TRAFICO  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PLANILLA_INGTRAFICO");
        }
        return esta;
    }
    
    //Diogenes 16.12.05
    /**
     * Metodo  sumarDemoraAct, modificar el campo demora, sumando la demora con
     * la demora de la actividad.
     * @param: codigo de la actividad
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void sumarDemoraAct(double demora, String numpla) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        
        
        try{
            st = crearPreparedStatement("SQL_SUMARDEMORA");
            st.setDouble(1, demora);
            st.setString(2, numpla);
            
            st.executeUpdate();
            
        }catch(SQLException e){
            throw new SQLException("ERROR AL Modifcar La demora " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_SUMARDEMORA");
        }
    }
    
    
    
    /**
     * Getter for property clientes.
     * @return Value of property clientes.
     */
    public java.util.Vector getClientes() {
        return clientes;
    }
    
    /**
     * Setter for property clientes.
     * @param clientes New value of property clientes.
     */
    public void setClientes(java.util.Vector clientes) {
        this.clientes = clientes;
    }
    
    /**
     * Busca los clientes destinatarios de una planilla
     * @autor Ing. Karen Reales
     * @param Numero de planilla
     * @throws SQLException
     * @version 1.0.
     **/
    public void buscarClientes(String numpla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        clientes = new Vector();
        
        try{
            ps = crearPreparedStatement("SQL_CLIENTES");
            ps.setString(1, numpla);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Remesa r = new Remesa();
                r.setDestinatario(rs.getString("codigo"));
                r.setNom_destinatario(rs.getString("nombre"));
                r.setCodcli(rs.getString("codcli"));
                r.setNombre_cli(rs.getString("nomcli"));
                r.setCiuDestinatario(rs.getString("codciu"));
                r.setNombre_ciu(rs.getString("nomciu"));
                r.setNumRem(rs.getString("numrem"));
                clientes.add(r);
                
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR BUSCANDO LOS CLIENTES EN TRAFICO "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_CLIENTES");
        }
    }
    
    /**
     * Obtiene las distintas planillas registradas dentro de un per�odo dado
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @return Arreglo de las planillas comprendidas en el per�odo
     * @throws SQLException
     * @version 1.0.
     **/
    public Vector planillasPeriodo(String fechaI, String fechaF) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        Vector lineas = new Vector();
        
        try{
            ps = crearPreparedStatement("SQL_PLANILLAS_PERIODO");
            ps.setString(1, fechaI);
            ps.setString(2, fechaF);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                String planilla = rs.getString("numpla");
                
                lineas.add(planilla);
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LAS DISTINTAS PLANILLAS EN UN PERIODO DADO  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PLANILLAS_PERIODO");
        }
        
        return lineas;
    }
    
    /**
     * Obtiene el tramo de una planilla
     * @autor Ing. Tito Andr�s Maturana
     * @param numpla N�mero de la planilla
     * @return La ruta de la planilla
     * @throws SQLException
     * @version 1.0
     **/
    public String obtenerInformacionTramo(String numpla) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            String tramo = null;
            String sql = obtenerSQL("SQL_TRAMO_PLANILLA");
            con = conectar("SQL_TRAMO_PLANILLA");
            if ( con != null ) {
                PreparedStatement ps = con.prepareStatement(sql);
                ps.setString(1,numpla);
                rs = ps.executeQuery();
                if (rs.next()){
                    tramo = rs.getString(1);
                }
                return tramo;
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL TRAMO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            desconectar("SQL_TRAMO_PLANILLA");
        }
        return null;
    }
    
    /**
     * Establece si existen registros de un puesto de control de la ruta de una planilla
     * @autor Ing. Tito Andr�s Maturana
     * @param numpla N�mero de la planilla
     * @param pc C�digo del puesto de control
     * @return <code>true</code> si existen registros, <code>false</code> si no
     * @throws SQLException
     * @version 1.0
     **/
    public boolean puestoControlRegistrado(String numpla, String pc) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            String tramo = null;
            PreparedStatement ps = crearPreparedStatement("SQL_PC_REGISTRADO");
            ps.setString(1, numpla);
            ps.setString(2, pc);
            
            rs = ps.executeQuery();
            
            if (rs.next()){
                return true;
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA VERIFICACION DEL REGISTRO DEL PUESTO DE CONTROL " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            desconectar("SQL_PC_REGISTRADO");
        }
        
        return false;
    }
    
    /**
     * Obtiene los registros del reporte de moviemiento en trafico de las planillas cuyas fechas estan en un per�odo dado y los almacena en la tabla tem.reporte_generacion
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @throws SQLException
     * @version 1.0.
     **/
    public void iniciarReporteGeneracion(String login, String fechaI, String fechaF) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        
        try{
            ps = this.crearPreparedStatement("SQL_REPORTE_GENERACION");
            ps.setString(1, login);
            ps.setString(2, fechaI);
            ps.setString(3, fechaF);
            
            ps.executeUpdate();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN INICIO DEL REPORTE DE GENERACION "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_REPORTE_GENERACION");
        }
    }
    
    /**
     * Obtiene la ultima fecha del reporte de un puesto de control
     * @autor Ing. Tito Andr�s Maturana
     * @param numpla N�mero de la planilla
     * @param pc C�digo del puesto de control
     * @return La fecha del �ltimo registro del puesto de control
     * @throws SQLException
     * @version 1.0
     **/
    public String ultimoRegistroPCPlanilla(String numpla, String pc) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        String fecha = "";
        
        try{
            ps = crearPreparedStatement("SQL_ULTIMO_REGISTRO");
            ps.setString(1, numpla);
            ps.setString(2, pc);
            
            
            rs = ps.executeQuery();
            
            if(rs.next()){
                fecha = (rs.getString("fechareporte")!=null)? rs.getString("fechareporte") : "";
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DEL ULTIMO REGISTRO DE UNA UBICACION POR PLANILLA "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ULTIMO_REGISTRO");
        }
        
        return fecha;
    }
    
    /**
     * Obtiene la fecha de salida a trafico de una planillla
     * @autor Ing. Tito Andr�s Maturana
     * @param numpla N�mero de la planilla
     * @return La fecha de salida
     * @throws SQLException
     * @version 1.0
     **/
    public String fechaSalida(String numpla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        String fecha = "";
        
        try{
            ps = crearPreparedStatement("SQL_TRAFICO");
            ps.setString(1, numpla);
            
            
            rs = ps.executeQuery();
            
            if(rs.next()){
                fecha = rs.getString("fecha_salida");
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LA FECHA DE SALIDA DE LA PLANILLA "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_TRAFICO");
        }
        
        return fecha;
    }
    
    
    /**
     * Obtiene la zona a la cual pertenece un puesto de control
     * @autor Ing. Tito Andr�s Maturana
     * @param pc C�digo del puesto de control
     * @return El c�digo de la zona del puesto de control
     * @throws SQLException
     * @version 1.0
     **/
    public String zonaPC(String pc) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        String zona = "";
        
        try{
            ps = crearPreparedStatement("SQL_ZONA_PC");
            ps.setString(1, pc);
            
            
            rs = ps.executeQuery();
            
            if(rs.next()){
                zona = (rs.getString("zona")!=null) ? rs.getString("zona") : "" ;
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LA ZONA DEL PUESTO DE CONTROL "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ZONA_PC");
        }
        
        return zona;
    }
    
    
    /**
     * Ingresa un nuevo registro en la tabla tem.reporte_generacion
     * @autor Ing. Tito Andr�s Maturana
     * @param gen Bean de tipo <code>REporteGeneracion</code>
     * @throws SQLException
     * @version 1.0
     **/
    public void ingresarRegistroFaltante(ReporteGeneracion gen, String login) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        
        try{
            ps = crearPreparedStatement("SQL_INSERTAR_FALTANTE");
            ps.setString(1, gen.getPlanilla());
            ps.setString(2, gen.getPc());
            ps.setString(3, gen.getFecha());
            ps.setString(4, gen.getZona());
            ps.setString(5, gen.getLogin());
            ps.setString(6, "NR");
            ps.setString(7, login);
            
            ps.executeUpdate();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA INSERCION DEL REGISTRO EN REPORTE_GENERACION "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_INSERTAR_FALTANTE");
        }
    }
    
    /**
     * Obtiene la duraci�n de un tramo, y se la a�ade a una fecha dada para calcular la fecha de conculsion del recorrido
     * @autor Ing. Tito Andr�s Maturana
     * @param fecha Fecha inicial del recorrido
     * @param ori C�digo del origen
     * @param dest C�digo del destino
     * @return La fecha de conclusi�n del recorrdio del tramo
     * @throws SQLException
     * @version 1.0
     **/
    public String agregarDuracionTramo(String fecha, String ori, String dest)  throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        String nfecha = fecha;
        
        try{
            ps = crearPreparedStatement("SQL_SUMAR_DURACION_TRAMO");
            ps.setString(1, fecha);
            ps.setString(2, ori);
            ps.setString(3, dest);
            
            
            rs = ps.executeQuery();
            
            if(rs.next()){
                nfecha =  (rs.getString("fecha")!=null)? rs.getString("fecha") : fecha;
                
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LA ZONA DEL PUESTO DE CONTROL "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_SUMAR_DURACION_TRAMO");
        }
        
        return nfecha;
    }
    
    /**
     * Obtiene los usuarios en turno a partir de una fecha dada
     * @autor Ing. Tito Andr�s Maturana
     * @param fecha Fecha del turno
     * @return Arreglo con los usuarios en turno en la fech dada
     * @throws SQLException
     * @version 1.0
     **/
    public Vector usuariosTurno(String fecha)  throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        Vector vec = new Vector();
        
        try{
            ps = crearPreparedStatement("SQL_USUARIOS_TURNO");
            ps.setString(1, fecha);
            
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                String usuario = rs.getString("usuario");
                String zonas = rs.getString("zona_trabajo");
                Vector arg = new Vector();
                arg.add(usuario);
                arg.add(zonas);
                vec.add(arg);
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LA ZONA DEL PUESTO DE CONTROL "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_USUARIOS_TURNO");
        }
        
        return vec;
    }
    
    /**
     * Obtiene los registros de la tabla tem.registro_generacion
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @version 1.0.
     **/
    public Vector detalleReporteDeGeneracion() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        Vector lineas = new Vector();
        
        try{
            ps = crearPreparedStatement("SQL_REPORTE_GENERACION_DATA");
            
            
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                ReporteGeneracion rg = new ReporteGeneracion();
                rg.Load(rs);
                
                lineas.add(rg);
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DEL REPORTE DE GENERACION  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_REPORTE_GENERACION_DATA");
        }
        
        return lineas;
    }
    
    /**
     * Obtiene el total de registros, registros no grabados, registros grabadps, y porcentaje de registros grabados por usuario
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @return Los totales por usuario
     * @version 1.0.
     **/
    public Vector totalesReporteDeGeneracion() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        Vector lineas = new Vector();
        
        try{
            ps = crearPreparedStatement("SQL_TTL_REPGENERACION");
            
            
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Hashtable ht = new Hashtable();
                
                ht.put("usuario", rs.getString("usuario"));
                ht.put("total", rs.getString("total"));
                ht.put("R", rs.getString("R"));
                ht.put("NR", rs.getString("NR"));
                ht.put("PR", rs.getString("PR"));
                
                lineas.add(ht);
            }
            
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LOS TOTALES DEL REPORTE DE GENERACION  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_TTL_REPGENERACION");
        }
        return lineas;
    }
    
    /**
     * Obtiene todos los registros de las planilla en un per�odo cuya diferencia entre la fecha
     * del reporte y la fecha del reporte planeado exceda los 30 mins.
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @throws SQLException
     * @return Arreglo con los registros
     * @version 1.0.
     **/
    public Vector reporteCoherencia(String fechaI, String fechaF) throws SQLException{//modificado 24.01.2006
        PreparedStatement ps = null;
        ResultSet rs=null;
        Vector lineas = new Vector();
        
        try{
            ps = crearPreparedStatement("SQL_REPORTE_COHERENCIA");
            ps.setString(1,  fechaI);
            ps.setString(2,  fechaF);
            
            
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Hashtable ht = new Hashtable();
                
                ht.put("usuario", rs.getString("usuario"));
                ht.put("numpla", rs.getString("numpla"));
                ht.put("pc", rs.getString("pc"));
                ht.put("fechareporte", rs.getString("fechareporte"));
                ht.put("fechareporte_planeado", rs.getString("fechareporte_planeado"));
                ht.put("zona", rs.getString("zona"));
                ht.put("descripcion", (rs.getString("descripcion")!=null)? rs.getString("descripcion") : "");
                
                lineas.add(ht);
            }
            
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LOS TOTALES DEL REPORTE DE COHERENCIA  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_REPORTE_COHERENCIA");
        }
        
        return lineas;
    }
    
    /**
     * Actualiza el registro en ingreso trafico y trafico para registrarlo como varado
     * @autor Ing. Karen Reales.
     * @throws SQLException
     * @version 1.0.
     **/
    public void registrarVarado() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_ACTUALIZAR_VARADO");
            ps.setString(1, rmt.getNumpla());
            ps.executeUpdate();
            
        }catch(SQLException ex){
            throw new SQLException("ERROR AL ACTUALIZAR EL REPORTE DE MOVIMIENTO DE TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ACTUALIZAR_VARADO");
        }
    }
    
    /**
     * Actualiza el registro en ingreso trafico y trafico para quitar la marca de varado
     * @autor Ing. Karen Reales.
     * @throws SQLException
     * @version 1.0.
     **/
    public void registrarReinicio() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_ACTUALIZAR_REINICIO");
            ps.setString(1, rmt.getNumpla());
            ps.executeUpdate();
            
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR EL REPORTE DE MOVIMIENTO DE TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ACTUALIZAR_REINICIO");
        }
    }
 
    /**
     * Registra en la tabla de entrega_destinatarios cuando el reporte es de entrega
     * parcial o entrega final
     * @autor Ing. Karen Reales.
     * @throws SQLException
     * @version 1.0.
     **/
    public String registrarDestinatarios(Remesa rem) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql="";
        try{
            ps = crearPreparedStatement("INSERT_DESTINATARIOS");
            ps.setString(1, rmt.getNumpla());
            ps.setString(2, rmt.getFechaReporte());
            ps.setString(3, rem.getCodcli());
            ps.setString(4, rem.getDestinatario());
            ps.setString(5, rem.getNdest());
            ps.setString(6, rem.getDescripcion());
            ps.setString(7, rem.getNumrem());
            ps.setString(8, rem.getDistrito());
            ps.setString(9, rem.getUsuario());
            sql= ps.toString();
            
        }catch(SQLException ex){
            throw new SQLException("ERROR AL ACTUALIZAR EL REPORTE DE MOVIMIENTO DE TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("INSERT_DESTINATARIOS");
        }
        return sql;
    }
    
    /**
     * Getter for property planillas.
     * @return Value of property planillas.
     */
    public java.util.Vector getPlanillas() {
        return planillas;
    }
    
    /**
     * Setter for property planillas.
     * @param planillas New value of property planillas.
     */
    public void setPlanillas(java.util.Vector planillas) {
        this.planillas = planillas;
    }
    public void buscarPlanillas(String numpla, String pco) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        planillas = new Vector();
        
        try{
            ps = crearPreparedStatement("SQL_PLANILLAS");
            ps.setString(1, pco);
            ps.setString(2, numpla);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                planillas.add(rs.getString("planilla")+","+rs.getString("placa"));
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LAS DISTINTAS PLANILLAS EN EL MISMO   "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PLANILLAS");
        }
        
    }
    
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico y tr�fico despu�s de una inserci�n o modificaci�n
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarDatosTrafico(double duracion) throws SQLException{
        PreparedStatement ps = null;
        String sql ="";
        try{
            double demora = getDemora();
            ps = crearPreparedStatement("SQL_UPDATE_ULTREP_INGRE");
            ps.setString(1, rmt.getUbicacion_procedencia());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, getNombreCiudad(rmt.getUbicacion_procedencia()));
            ps.setString(4, rmt.getFechaReporte());
            ps.setString(5, rmt.getFechaReporte());
            ps.setString(6, duracion+" HOUR");
            ps.setString(7, demora+" MINUTE");
            ps.setString(8, rmt.getNumpla());
            sql = ps.toString();
            
            ps = crearPreparedStatement("SQL_UPDATE_ULTREP_TRAFICOS");
            ps.setString(1, rmt.getUbicacion_procedencia());
            ps.setString(2, rmt.getFechaReporte());
            ps.setString(3, rmt.getObservacion());
            ps.setString(4, rmt.getFechaReporte());
            ps.setString(5, duracion+" HOUR");
            ps.setString(6, demora+" MINUTE");
            ps.setString(7, rmt.getNumpla());
            sql = sql+";"+ps.toString();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS TABLAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_ULTREP_INGRE");
            desconectar("SQL_UPDATE_ULTREP_TRAFICOS");
        }
        return sql;
    }
    
    /**
     * M�todo que retorna los datos de la planilla q su remesa es de aduana
     * @autor.......Jose de la rosa
     * @param.......Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Vector de remesas: numero de la remesa, origen de la remesa
     **/
    public Vector planillaRemesasConAduana(String numpla) throws SQLException{
        Vector vec = new Vector();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_REM_PLA_ADUANA");
            ps.setString(1, numpla);
            rs = ps.executeQuery();
            while(rs.next()){
                Remesa r = new Remesa();
                r.setNumRem(rs.getString("numrem"));
                r.setOriRem(rs.getString("orirem"));
                vec.add(r);
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LAS REMESAS DE ADUANA , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_REM_PLA_ADUANA");
        }
        return vec;
    }
    
    /**
     * M�todo que retorna si los el pais de origen de la remesa es diferente al pais origen del PC
     * @autor.......Jose de la rosa
     * @param.......codigo de la ciudad de origen de la remesa, codigo de ciudad del pc
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean remesasInternacionales(String orirem, String ciupc) throws SQLException{
        boolean sw = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_REM_INTERNACIONAL");
            ps.setString(1, orirem);
            ps.setString(2, ciupc);
            rs = ps.executeQuery();
            while(rs.next()){
                sw = rs.getString("case").equals("Y")?true:false;
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LAS REMESAS INTERNACIONALES , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_REM_INTERNACIONAL");
        }
        return sw;
    }
    
    /**
     * M�todo que retorna si la ciudad de PC no es de frontera
     * @autor.......Jose de la rosa
     * @param.......codigo de ciudad del pc
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean pcNoEsFrontera(String ciupc) throws SQLException{
        boolean sw = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_PC_NO_FRONTERA");
            ps.setString(1, ciupc);
            rs = ps.executeQuery();
            while(rs.next()){
                sw = rs.getString("frontera").equals("Y")?false:true;
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LOS PC QUE NO SEAN FRONTERA , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PC_NO_FRONTERA");
        }
        return sw;
    }
    
    /**
     * M�todo que modifica la aduana en 'N' de la remesa
     * @autor.......Jose de la rosa
     * @param.......numero de la remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     **/
    public String actualizacionAduana(String numrem) throws SQLException{
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_ADUANA_UPDATE");
            ps.setString(1, numrem);
            sql = ps.toString();
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL MODIFICAR LA ADUANA DE LAS REMESAS , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ADUANA_UPDATE");
        }
        return sql;
    }
    
    /**
     * M�todo que modifica la aduana en 'N' de la remesa
     * @autor.......Jose de la rosa
     * @param.......numero de la remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     **/
    public String actualizacionProxFecha(String fecha, String numpla, String observacion) throws SQLException{
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_PROX_FECHA_UPDATE");
            ps.setString(1, fecha);
            ps.setString(2, observacion);
            ps.setString(3, numpla);
            ps.setString(4, fecha);
            ps.setString(5, observacion);
            ps.setString(6, numpla);
            sql = ps.toString();
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL MODIFICAR LA ADUANA DE LAS REMESAS , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PROX_FECHA_UPDATE");
        }
        return sql;
    }
    
  
    /**
     * Getter for property FECHA_ULTIMO.
     * @return Value of property FECHA_ULTIMO.
     */
    public java.lang.String getFECHA_ULTIMO() {
        return FECHA_ULTIMO;
    }
    
    /**
     * Setter for property FECHA_ULTIMO.
     * @param FECHA_ULTIMO New value of property FECHA_ULTIMO.
     */
    public void setFECHA_ULTIMO(java.lang.String FECHA_ULTIMO) {
        this.FECHA_ULTIMO = FECHA_ULTIMO;
    }
    
    /**
     * Getter for property FECHA_ANTERIOR.
     * @return Value of property FECHA_ANTERIOR.
     */
    public java.lang.String getFECHA_ANTERIOR() {
        return FECHA_ANTERIOR;
    }
    
    /**
     * Setter for property FECHA_ANTERIOR.
     * @param FECHA_ANTERIOR New value of property FECHA_ANTERIOR.
     */
    public void setFECHA_ANTERIOR(java.lang.String FECHA_ANTERIOR) {
        this.FECHA_ANTERIOR = FECHA_ANTERIOR;
    }
    
    /**
     * Getter for property UnSoloRegistro.
     * @return Value of property UnSoloRegistro.
     */
    public boolean isUnSoloRegistro() {
        return UnSoloRegistro;
    }
    
    /**
     * M�todo que modifica la la observacion de ingreso trafico
     * @autor.......Jose de la rosa
     * @param.......numero de la remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     **/
    public String actualizacionObservacion(String observacion, String numpla) throws SQLException{
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_OBSERVACION_UPDATE");
            ps.setString(1, observacion);
            ps.setString(2, numpla);
            ps.setString(3, observacion);
            ps.setString(4, numpla);
            sql = ps.toString();
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL MODIFICAR LA OBSERVACI�N , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_OBSERVACION_UPDATE");
        }
        return sql;
    }
    
    /**
     * M�todo para buscar el reg_status de la planilla en ingresoTrafico
     * @autor.......Ivan Gomez
     * @param.......numero de la remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     **/
    public String[] BuscarStatus(String numpla) throws SQLException{
        PreparedStatement st = null;
        ResultSet         rs = null;
        String[] Vec         = new String[2];
        try{
            st = crearPreparedStatement("SQL_REG_STATUS");
            st.setString(1, numpla);
            rs = st.executeQuery();
            if (rs.next()){
                Vec[0] = rs.getString(1);
                Vec[1] = rs.getString(2);
            }
            
            
        }catch(SQLException ex){
            throw new SQLException("ERROR AL MODIFICAR LA ADUANA DE LAS REMESAS , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_REG_STATUS");
        }
        return Vec;
    }
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico y tr�fico despu�s de una inserci�n o modificaci�n
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void actualizarCIU() throws SQLException{
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        String sql ="";
        try{
            String nomzona = getNombreZona(rmt.getZona());
            ps = crearPreparedStatement("SQL_UPDATE_INGRESO_TRAFICO_CIU");
            ps.setString(1, ultrmt.getCodUbicacion());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, getNombreCiudad(ultrmt.getCodUbicacion()));
            ps.setString(4, rmt.getFechaReporte());
            ps.setString(5, rmt.getTipo_reporte());
            ps.setString(6, this.getNombreReporte(rmt.getTipo_reporte()));
            ps.setString(7, "");
            ps.setString(8, "");
            ps.setString(9, rmt.getNumpla());
            ps.executeUpdate();
            
            ps2 = crearPreparedStatement("SQL_UPDATE_TRAFICO_CIU");
            ps2.setString(1, rmt.getUbicacion_procedencia());
            ps2.setString(2, rmt.getZona());
            ps2.setString(3, rmt.getFechaReporte());
            ps2.setString(4, rmt.getObservacion());
            ps2.setString(5, rmt.getTipo_reporte());
            ps2.setString(6, "");
            ps2.setString(7, rmt.getNumpla());
            ps2.executeUpdate();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS TABLAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(ps2 != null){
                try{
                    ps2.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_INGRESO_TRAFICO_CIU");
            desconectar("SQL_UPDATE_TRAFICO_CIU");
        }
    }
    /**
     * M�todo que retorna true si existe un una via de una planilla
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean existeVia(String origen, String destino, String secuencia) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        try{
            ps = crearPreparedStatement("SQL_EXISTE_VIA");
            ps.setString(1, origen);
            ps.setString(2, destino);
            ps.setString(3, secuencia);
            rs = ps.executeQuery();
            while(rs.next()){
                existe = true;
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL COMPROBAR SI LA VIA EXISTE."+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_EXISTE_VIA");
        }
        return existe;
    }
    public String cambiarViaPlanilla(boolean sw) throws SQLException{
        PreparedStatement ps = null;
        String sql ="";
        try{
            if(sw)
                ps = crearPreparedStatement("SQL_ACTUALIZAR_VIA_DESPACHO_MANUAL");
            else
                ps = crearPreparedStatement("SQL_ACTUALIZAR_VIA_PLANILLA");
            ps.setString(1, dp.getRutapla());
            ps.setString(2, dp.getNumpla());
            sql = ps.toString();
            
            ps = crearPreparedStatement("SQL_ACTUALIZAR_VIA_INGRESO_TRAFICO");
            ps.setString(1, dp.getRutapla());
            ps.setString(2, dp.getRutapla().substring(0,2) );
            ps.setString(3, getNombreCiudad(dp.getRutapla().substring(0,2) ) );
            ps.setString(4, dp.getRutapla().substring(2,4) );
            ps.setString(5, getNombreCiudad(dp.getRutapla().substring(2,4) ) );
            ps.setString(6, dp.getNumpla());
            ps.setString(7, dp.getRutapla().substring(0,2) );
            ps.setString(8, dp.getRutapla().substring(2,4) );
            ps.setString(9, dp.getNumpla());
            sql = sql+";"+ps.toString();
        }catch(SQLException ex){
            throw new SQLException("ERROR AL ACTUALIZAR LA SECUENCIA DE LA PLANILLA , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ACTUALIZAR_VIA_PLANILLA");
            desconectar("SQL_ACTUALIZAR_VIA_INGRESO_TRAFICO");
            desconectar("SQL_ACTUALIZAR_VIA_DESPACHO_MANUAL");
        }
        return sql;
    }
    
    
    
    
    /**
     * M�todo que retorna si la planilla ya tiene un gegistro en esa hora
     * @autor.......Jose de la rosa
     * @param.......fecha a evaluar
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean existePlanillaConFecha(String numpla, String fecha) throws SQLException{
        boolean es = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_PLANILLA_FECHA");
            ps.setString(1, fecha);
            ps.setString(2, numpla);
            rs = ps.executeQuery();
            return rs.next();
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER LA FECHA DE REPORTE TRAFICO "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PLANILLA_FECHA");
        }
    }
    /**
     * M�todo que modifica la placa, el propietario y el conductor de una planilla
     * @autor.......Jose de la rosa
     * @param.......numero de la remesa
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     **/
    public String actualizacionPlacaConductor(String cedcon, String nomcon, String cedprop, String nomprop, String placa, String planilla) throws SQLException{
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_ACTUALIZAR_PLACA_CONDUCTOR");
            ps.setString(1, cedcon);
            ps.setString(2, nomcon);
            ps.setString(3, cedprop);
            ps.setString(4, nomprop);
            ps.setString(5, placa);
            ps.setString(6, planilla);
            ps.setString(7, cedcon);
            ps.setString(8, cedprop);
            ps.setString(9, placa);
            ps.setString(10, planilla);
            sql = ps.toString();
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL MODIFICAR LA PLACA Y CONDUCTOR , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ACTUALIZAR_PLACA_CONDUCTOR");
        }
        return sql;
    }
    
    
    /**
     * Busca todos los reportes de las planillas que no se reportaron en el PC correspondiente, o siguient en la ruta.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param fecha_ini Fecha inicial del per�odo
     * @param Fecha_fin Fecha final del reporte
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void reportePuestosControl(String fecha_ini, String fecha_fin) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            st = this.crearPreparedStatement("SQL_REPORTE_PCS");
            st.setString(1, fecha_ini + " 00:00:00");
            st.setString(2, fecha_fin + " 00:00:00");
            
            //////System.out.println("........... REPORTE FACTURAS DE CLIENTES \n" + st);
            rs = st.executeQuery();
            
            this.reportes = new Vector();
            while( rs.next() ){
                Hashtable ht = new Hashtable();
                
                ht.put("numpla", rs.getString("numpla"));
                ht.put("oripla", rs.getString("oripla"));
                ht.put("despla", rs.getString("despla"));
                ht.put("cedcond", rs.getString("cedcon"));
                ht.put("nomcond", rs.getString("nomcond"));
                ht.put("pc", rs.getString("uproced"));
                ht.put("login", rs.getString("login"));
                ht.put("plaveh", rs.getString("plaveh"));
                ht.put("causades", rs.getString("causedes"));
                ht.put("treporte", rs.getString("treporte"));
                ht.put("clasif", rs.getString("desclasif"));
                ht.put("fecharep", rs.getString("fechareporte"));
                ht.put("fechapla", rs.getString("fechareporte_planeado"));
                ht.put("zona", rs.getString("codzona"));
                ht.put("cdate", rs.getString("creation_date"));
                ht.put("tproc", rs.getString("tipo_procedencia").equalsIgnoreCase("PC") ? "Puesto de Control" : "Ciudad");
                ht.put("causa", rs.getString("causa"));
                ht.put("observacion", rs.getString("observacion"));
                
                this.reportes.add(ht);
            }
        } catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE reportePuestosControl " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null){ st.close(); }
            this.desconectar("SQL_REPORTE_PCS");
        }
    }
    
    /**
     * Obtiene las distintas planillas en un puesto de control dado
     * @autor Ing. Karen Reales.
     * @throws SQLException
     * @version 1.0.
     **/
    public void buscarPlanillasReinicio(String numpla, String pco) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        planillas = new Vector();
        
        try{
            ps = crearPreparedStatement("SQL_PLANILLAS_REINICIO");
            ps.setString(1, pco);
            ps.setString(2, numpla);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                planillas.add(rs.getString("planilla")+","+rs.getString("placa"));
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LAS DISTINTAS PLANILLAS EN EL MISMO   "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PLANILLAS_REINICIO");
        }
        
    }
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico para marcar los despachos
     *de cadena.
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String marcarCadena(String numpla) throws SQLException{
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        String sql ="";
        try{
            ps = crearPreparedStatement("SQL_UPDATE_CADENA_INGTRAFICO");
            ps.setString(1, numpla);
            sql = ps.toString();
            
            ps2 = crearPreparedStatement("SQL_UPDATE_CADENA_TRAFICO");
            ps2.setString(1, numpla);
            sql = sql +";"+ ps2.toString();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS TABLAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(ps2 != null){
                try{
                    ps2.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_CADENA_INGTRAFICO");
            desconectar("SQL_UPDATE_CADENA_TRAFICO");
        }
        return sql;
    }
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico para desmarcar los despachos
     *de cadena.
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String desmarcarCadena(String numpla) throws SQLException{
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        String sql ="";
        try{
            ps = crearPreparedStatement("SQL_UPDATE_NOCADENA_INGTRAFICO");
            ps.setString(1, numpla);
            sql = ps.toString();
            
            ps2 = crearPreparedStatement("SQL_UPDATE_NOCADENA_TRAFICO");
            ps2.setString(1, numpla);
            sql = sql +";"+ ps2.toString();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS TABLAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(ps2 != null){
                try{
                    ps2.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_NOCADENA_INGTRAFICO");
            desconectar("SQL_UPDATE_NOCADENA_TRAFICO");
        }
        return sql;
    }
    /**
     * M�todo que obtiene TODOS los registros de ingreso_trafico
     * @autor.......Osvaldo P�rez Ferrer
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void obtenerIngresoTrafico() throws SQLException{
        
        PreparedStatement st = null;
        
        try{
            Ingreso_Trafico it ;
            ResultSet rs = null;
            
            st = this.crearPreparedStatement("SQL_OBTENER_ALL_INGRESO_TRAFICO");
            rs = st.executeQuery();
            
            reportes = new Vector();
            
            while( rs.next() ){
                it = new Ingreso_Trafico();
                reportes.add( it.load(rs) );
            }
            
        }catch(Exception ex){
            ////System.out.println("error en obtenerIngresoTrafico: "+ex.getMessage());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar("SQL_OBTENER_ALL_INGRESO_TRAFICO");
        }
        
    }
    
    
    /**
     * M�todo que retorna true si el tipo de documento es una detencion
     * @autor.......Ivan DArio Gomez
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean isDetencion(String tipoReporte) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        try{
            ps = crearPreparedStatement("SQL_IS_DETENCION");
            ps.setString(1, tipoReporte);
            
            rs = ps.executeQuery();
            if(rs.next()){
                existe = true;
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL VERIFICAR SI ES  DETENCION"+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_IS_DETENCION");
        }
        return existe;
    }
    /**
     * M�todo que actualiza las zonas de ingreso tr�fico y tr�fico
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarZonaTrafico() throws SQLException{
        PreparedStatement ps = null;
        String sql ="";
        try{
            String nextpc = getCodNextPC();
            double duracion = getDuracionTramo(ultrmt.getCodUbicacion(),nextpc);
            double demora = getDemora();
            String nomzona = getNombreZona( rmt.getZona() );
            ps = crearPreparedStatement("SQL_UPDATE_ZONA_TR");
            ps.setString(1, rmt.getZona() );
            ps.setString(2, nomzona);
            ps.setString(3, rmt.getNumpla());
            ps.setString(4, rmt.getZona() );
            ps.setString(5, rmt.getNumpla());
            sql = ps.toString();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS ZONAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_ZONA_TR");
        }
        return sql;
    }
    
    
    /**
     * M�todo que setea el �ltimo reporte de movimiento de tr�fico ingresado no anulado
     * @autor.......Ivan Gomez
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void setUltimoPC() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_ULTIMO_PC");
            
            ps.setString(1, dp.getNumpla());
            ps.setString(2, dp.getNumpla());
            
            rs = ps.executeQuery();
            if(rs.next()){
                
                ultrmt.setUbicacion_procedencia(rs.getString("ubicacion_procedencia"));
                ultrmt.setCodUbicacion(ultrmt.getUbicacion_procedencia());
                setUltRMT(ultrmt);
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER EL ULTIMO PC. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ULTIMO_PC");
        }
    }
    /**
     *
     * /**
     *
     *
     *
     *
     * /**
     * eliminarIngresoTrafico, elimina de la tabla ingreso trafico
     * @autor Ing. Diogenes Bastidas
     * @param numero de la planilla
     * @throws SQLException
     * @version 1.0
     **/
    public void eliminarIngresoTrafico(String numpla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        
        try{
            ps = crearPreparedStatement("SQL_DELETE_INGRESO_TRAFICO");
            ps.setString(1, numpla);
            ////System.out.println(ps);
            
            ps.executeUpdate();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA INSERCION DEL REGISTRO EN REPORTE_GENERACION "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_DELETE_INGRESO_TRAFICO");
        }
    }
    /******** RepMovTraficoDAO *********/
    
    /**
     * M�todo que busca y llena un vector de ciudades
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarTodasCiudades() throws SQLException{
        pc = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        pc = null;
        try{
            if(this.getRmt()!=null){
                ps = crearPreparedStatement("SQL_ZONA");
                ps.setString(1,this.getRmt().getZona());
                rs = ps.executeQuery();
                pc = new Vector();
                while(rs.next()){
                    Vector fila = new Vector();
                    fila.add(rs.getString("codciu"));
                    fila.add(rs.getString("nomciu"));
                    pc.add(fila);
                }
                String nextpc = getCodNextPC();
                Vector fila = new Vector();
                fila.add( ultrmt.getCodUbicacion() );
                fila.add( getNombreCiudad( ultrmt.getCodUbicacion() ) );
                pc.add(fila);
                fila = new Vector();
                fila.add( nextpc );
                fila.add( getNombreCiudad(nextpc) );
                pc.add(fila);
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LAS CIUDADES PARA REPORTE, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ZONA");
        }
    }
    /*Jose 16/08/2006*/
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico y tr�fico despu�s de una inserci�n o modificaci�n
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarIngresoTrafico() throws SQLException{
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        String sql ="";
        try{
            String nextpc = getCodNextPC();
            double duracion = getDuracionTramo(ultrmt.getCodUbicacion(),nextpc);
            double demora = getDemora();
            String nomzona = getNombreZona(getZona(nextpc));
            ps = crearPreparedStatement("SQL_UPDATE_INGRESO_TRAFICO");
            ps.setString(1, rmt.getUbicacion_procedencia());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, getZona(nextpc) );
            ps.setString(4, nomzona);
            ps.setString(5, getNombreCiudad(rmt.getUbicacion_procedencia()));
            ps.setString(6, rmt.getFechaReporte());
            ps.setString(7, nextpc);
            ps.setString(8, getNombreCiudad(nextpc));
            ps.setString(9, rmt.getFechaReporte() );
            ps.setString(10, duracion+" HOUR");
            ps.setString(11, demora+" MINUTE");
            ps.setString(12, rmt.getTipo_reporte());
            ps.setString(13, rmt.getNombre_reporte());
            ps.setString(14, "");
            ps.setString(15, "");
            ps.setString(16, rmt.getReg_status());
            ps.setString(17, rmt.getCreation_user());
            ps.setString(18, rmt.getNumpla());
            sql = ps.toString();
            
            ps2 = crearPreparedStatement("SQL_UPDATE_TRAFICO");
            ps2.setString(1, rmt.getUbicacion_procedencia());
            ps2.setString(2, getZona(nextpc) );
            ps2.setString(3, rmt.getFechaReporte());
            ps2.setString(4, nextpc );
            ps2.setString(5, rmt.getObservacion());
            ps2.setString(6, rmt.getFechaReporte());
            ps2.setString(7, duracion+" HOUR");
            ps2.setString(8, demora+" MINUTE");
            ps2.setString(9, rmt.getTipo_reporte());
            ps2.setString(10, "");
            ps2.setString  (11, rmt.getCreation_user());
            ps2.setString(12, rmt.getNumpla());
            sql =sql+";"+ps2.toString();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS TABLAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(ps2 != null){
                try{
                    ps2.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_INGRESO_TRAFICO");
            desconectar("SQL_UPDATE_TRAFICO");
        }
        return sql;
    }
    
    
    public String actualizacionProxFechaRep(String fecha, String numpla, String observacion, String zona, String tiporep) throws SQLException{
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nom_zona = getNombreZona(zona);
        try{
            ps = crearPreparedStatement("SQL_PROX_FECHA_UPDATE_TIPOREP");
            ps.setString(1, fecha);
            ps.setString(2, observacion);
            ps.setString(3, zona );
            ps.setString(4, nom_zona );
            ps.setString(5, tiporep );
            ps.setString(6, tiporep );
            ps.setString(7, numpla);
            ps.setString(8, fecha);
            ps.setString(9, observacion);
            ps.setString(10, zona );
            ps.setString(11, numpla);
            sql = ps.toString();
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL MODIFICAR LA ADUANA DE LAS REMESAS , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PROX_FECHA_UPDATE_TIPOREP");
        }
        return sql;
    }
    public String actualizacionProxFecha(String fecha, String numpla, String observacion, String zona) throws SQLException{
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nom_zona = getNombreZona(zona);
        try{
            ps = crearPreparedStatement("SQL_PROX_FECHA_UPDATE");
            ps.setString(1, fecha);
            ps.setString(2, observacion);
            ps.setString(3, zona );
            ps.setString(4, nom_zona );
            ps.setString(5, numpla);
            ps.setString(6, fecha);
            ps.setString(7, observacion);
            ps.setString(8, zona );
            ps.setString(9, numpla);
            sql = ps.toString();
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL MODIFICAR LA ADUANA DE LAS REMESAS , "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PROX_FECHA_UPDATE");
        }
        return sql;
    }
    /**
     * M�todo que obtiene laa planillas anuladas en MIMS y que se encuentren en ingreso trafico
     * @autor.......Jose De La Rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector planillaAnuladasMimsIngresoTrafico( ) throws SQLException{
        PreparedStatement  ps     = null, st = null;
        ResultSet          rs     = null, rs2= null;
        Vector             vec    = new Vector();
        
        String             query1 = "SQL_PLANILLAS_ANULADAS_MIMS";
        String             query2 = "SQL_PLANILLA_INGTRAFICO";
        
        try{
            
            ps = crearPreparedStatement(query1);
            st = crearPreparedStatement(query2);
            
            rs = ps.executeQuery();
            while(rs.next()){
                st.setString(1, rs.getString(1) );
                rs2 = st.executeQuery();
                if(rs2.next()){
                    vec.add( rs.getString(1) );
                }
                st.clearParameters();
            }
            
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LA PLANILLA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if( ps != null )  ps.close();
            if( st != null )  st.close();
            if( rs != null )  rs.close();
            if( rs2!= null )  rs2.close();
            desconectar(query1);
            desconectar(query2);
        }
        return vec;
    }
    
    
    /**
     * M�todo que setea un objeto de reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @param.......N�mero de planilla y la fecha del reporte
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarReporteMovTraf(String numpla, String fecha) throws SQLException{
        RepMovTrafico rmt = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            
            ps = crearPreparedStatement("SQL_GET_RMT");
            ps.setString(1, numpla);
            ps.setString(2, fecha);
            rs = ps.executeQuery();
            while(rs.next()){
                rmt = new RepMovTrafico();
                rmt = rmt.load(rs);
                
                
                
            }
            
            
            if ( rmt!=null ) {
                //BUSCAMOS UN REPORTE DESPUES DE LA FECHA DEL REPORTE BUSCADO PARA VERIFICAR QUE ESTE SEA EL ULTIMO
                ps = crearPreparedStatement("SQL_REPORTE");
                ps.setString(1, numpla);
                ps.setString(2, fecha);
                rs = ps.executeQuery();rmt.setUltimoRep(true);
                if(rs.next()){
                    rmt.setUltimoRep(false);
                }
            }
            setRMT(rmt);
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER EL REPORTE DE MOVIMIENTO DE TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_RMT");
            desconectar("SQL_REPORTE");
        }
    }
    
    /**
     * M�todo que retorna el nombre de un puesto de control
     * @autor.......Armando Oviedo
     * @param.......c�digo pc
     * @throws......SQLException
     * @version.....1.0.
     * @return.......datos del puesto de control
     **/
    public Vector getDatosCiudad(String codciu) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Vector nombre = null;
        try{
            ps = crearPreparedStatement("SQL_GET_CIUDAD");
            ps.setString(1, codciu);
            rs = ps.executeQuery();
            while(rs.next()){
                nombre = new Vector();
                nombre.add(rs.getString(1)!=null?rs.getString(1):"");
                nombre.add(rs.getString(2)!=null?rs.getString(2):"");
                nombre.add(rs.getString(3)!=null && !rs.getString(3).equals("")?rs.getString(3):"CIU");
                nombre.add( rs.getString( 4 )!=null?rs.getString( 4 ) : "" );//David Pina Lopez
                nombre.add( rs.getString( 5 )!=null?rs.getString( 5 ) : "" );//David Pina Lopez
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR EL NOMBRE DE LA CIUDAD, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_CIUDAD");
        }
        return nombre;
    }
    /**
     * M�todo que actualiza los campos  de ultimo reporte en ingreso trafico y trafico
     * @autor.......Jose de la rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarUltimotPC() throws SQLException{
        PreparedStatement ps = null;
        String sql ="";
        try{
            ps = crearPreparedStatement("SQL_UPDATE_ULT_REP_INGRESO");
            ps.setString(1, rmt.getUbicacion_procedencia());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, getNombreCiudad(rmt.getUbicacion_procedencia()));
            ps.setString(4, rmt.getFechaReporte());
            ps.setString(5, rmt.getTipo_reporte());
            ps.setString(6, rmt.getNombre_reporte());
            ps.setString(7, rmt.getReg_status());
            ps.setString(8, rmt.getCreation_user());
            ps.setString(9, rmt.getNumpla());
            sql = ps.toString();
            
            ps = crearPreparedStatement("SQL_UPDATE_ULT_REP_TRAFICO");
            ps.setString(1, rmt.getUbicacion_procedencia());
            ps.setString(2, rmt.getFechaReporte());
            ps.setString(3, rmt.getObservacion());
            ps.setString(4, rmt.getTipo_reporte());
            ps.setString(5, rmt.getReg_status());
            ps.setString(6, rmt.getCreation_user());
            ps.setString(7, rmt.getNumpla());
            sql = sql+";"+ps.toString();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS TABLAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_ULT_REP_INGRESO");
            desconectar("SQL_UPDATE_ULT_REP_TRAFICO");
        }
        return sql;
    }
    
    
    /**
     * M�todo que obtiene el pr�ximo puesto de control
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void getNextPC() throws SQLException{
        Statement st;
        ResultSet rs;
        String nextpc = "";
        try{
            Vector pc = getUbicaciones();
            boolean sw = false;
            for(int i=0;i<pc.size();i++){
                Vector tmp = (Vector)(pc.get(i));
                String puestocontrol = (String)(tmp.get(0));
                if( puestocontrol.equalsIgnoreCase( ultrmt.getCodUbicacion() ) && (i!=pc.size()-1)){
                    Vector tmp2 = (Vector)(pc.get(i+1));
                    nextpc = (String)(tmp2.get(0));
                    proxrmt = new RepMovTrafico();
                    proxrmt.setNumpla(dp.getNumpla());
                    proxrmt.setUbicacion_procedencia(nextpc);
                    proxrmt.setTipo_procedencia( (String) tmp2.get(3) );
                    setProxRMT(proxrmt);
                }
                else if(puestocontrol.equalsIgnoreCase(ultrmt.getCodUbicacion()) && (i==pc.size()-1)){
                    Vector tmp2 = (Vector)(pc.get(i));
                    nextpc = (String)(tmp2.get(0));
                    proxrmt = new RepMovTrafico();
                    proxrmt.setNumpla(dp.getNumpla());
                    proxrmt.setUbicacion_procedencia(nextpc);
                    proxrmt.setTipo_procedencia( (String) tmp2.get(3) );
                    setProxRMT(proxrmt);
                }
                if(ultrmt.getCodUbicacion().equals("") && i == 0){
                    Vector tmp2 = (Vector)(pc.get(0));
                    nextpc = (String)(tmp2.get(0));
                    proxrmt = new RepMovTrafico();
                    proxrmt.setNumpla(dp.getNumpla());
                    proxrmt.setUbicacion_procedencia(nextpc);
                    proxrmt.setTipo_procedencia( (String) tmp2.get(3) );
                    setProxRMT(proxrmt);
                }
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL BUSCAR EL PROXIMO PUESTO DE CONTROL, "+ex.getMessage());
        }
    }
    
    
    /**
     * M�todo que busca y llena un vector de ciudades
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarCiudades() throws SQLException{
        pc = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        pc = null;
        try{
            if(this.getRmt()!=null){
                ps = crearPreparedStatement("SQL_ZONA");
                ps.setString(1,this.getRmt().getZona());
                rs = ps.executeQuery();
                pc = new Vector();
                int sw=0;
                while(rs.next()){
                    sw=1;
                    Vector fila = new Vector();
                    Vector ciu = getDatosCiudad(rs.getString("codciu"));
                    if( ciu == null ){
                        fila.add(rs.getString("codciu")+" [No existe]");
                        fila.add( "" );
                        fila.add( "" );
                        fila.add( "" );
                        fila.add( "" ); //David Pina Lopez
                        fila.add( "" ); //David Pina Lopez
                    }else{
                        fila.add( rs.getString("codciu"));
                        fila.add( (String) ciu.get(0) );
                        fila.add( (String) ciu.get(1) );
                        fila.add( (String) ciu.get(2) );
                        fila.add( (String) ciu.get(3) ); //David Pina Lopez
                        fila.add( (String) ciu.get(4) ); //David Pina Lopez
                    }
                    pc.add(fila);
                }
                if(sw==0){
                    ps = crearPreparedStatement("SQL_CIUDAD");
                    ps.setString(1,this.getRmt().getUb_ultreporte());
                    rs = ps.executeQuery();
                    while(rs.next()){
                        Vector fila = new Vector();
                        Vector ciu = getDatosCiudad( rs.getString("codciu") );
                        if( ciu == null ){
                            fila.add(rs.getString("codciu")+" [No existe]");
                            fila.add( "" );
                            fila.add( "" );
                            fila.add( "" );
                            fila.add( "" ); //David Pina Lopez
                            fila.add( "" ); //David Pina Lopez
                        }else{
                            fila.add( rs.getString("codciu") );
                            fila.add( (String) ciu.get(0) );
                            fila.add( (String) ciu.get(1) );
                            fila.add( (String) ciu.get(2) );
                            fila.add( (String) ciu.get(3) ); //David Pina Lopez
                            fila.add( (String) ciu.get(4) ); //David Pina Lopez
                        }
                        pc.add(fila);
                    }
                }
                
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LAS CIUDADES PARA REPORTE, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ZONA");
            desconectar("SQL_CIUDAD");
        }
    }
    /**
     * M�todo que actualiza las tablas de ingreso tr�fico y tr�fico despu�s de una inserci�n o modificaci�n
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarTablaIngresoTrafico() throws SQLException{
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        String sql ="";
        try{
            String nextpc = getCodNextPC();
            double demora = getDemora();
            String nomzona = getNombreZona(rmt.getZona()); //David Pina Lopez
            ps = crearPreparedStatement("SQL_UPDATE_INGRESO_TRAFICO");
            ps.setString(1, rmt.getUbicacion_procedencia());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, rmt.getZona() ); //David Pina Lopez
            ps.setString(4, nomzona);
            ps.setString(5, getNombreCiudad(rmt.getUbicacion_procedencia()));
            ps.setString(6, rmt.getFechaReporte());
            ps.setString(7, nextpc);
            ps.setString(8, getNombreCiudad(nextpc));
            ps.setString(9, rmt.getFechaReporte() );
            ps.setString(10, rmt.getTiempo()+" HOUR");
            ps.setString(11, demora+" MINUTE");
            ps.setString(12, rmt.getTipo_reporte());
            ps.setString(13, rmt.getNombre_reporte());
            ps.setString(14, "");
            ps.setString(15, "");
            ps.setString(16, rmt.getReg_status());
            ps.setString(17, rmt.getCreation_user());
            ps.setString(18, rmt.getNumpla());
            sql = ps.toString();
            
            ps2 = crearPreparedStatement("SQL_UPDATE_TRAFICO");
            ps2.setString(1, rmt.getUbicacion_procedencia());
            ps2.setString(2, rmt.getZona() ); //David Pina Lopez
            ps2.setString(3, rmt.getFechaReporte());
            ps2.setString(4, nextpc );
            ps2.setString(5, rmt.getObservacion());
            ps2.setString(6, rmt.getFechaReporte());
            ps2.setString(7, rmt.getTiempo()+" HOUR");
            ps2.setString(8, demora+" MINUTE");
            ps2.setString(9, rmt.getTipo_reporte());
            ps2.setString(10, "");
            ps2.setString  (11, rmt.getCreation_user());
            ps2.setString(12, rmt.getNumpla());
            sql =sql+";"+ps2.toString();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS TABLAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(ps2 != null){
                try{
                    ps2.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_INGRESO_TRAFICO");
            desconectar("SQL_UPDATE_TRAFICO");
        }
        return sql;
    }
    
    /**
     *
     * /**
     * M�todo que actualiza los campos  de ultimo reporte en ingreso trafico y trafico cuando
     * el tipo de reporte es llegada
     * @autor.......David Pina Lopez
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarUltimotPCLlegada() throws SQLException{
        PreparedStatement ps = null;
        String sql ="";
        try{
            ps = crearPreparedStatement("SQL_UPDATE_ULT_REP_INGRESO_LLEGADA");
            ps.setString(1, rmt.getUbicacion_procedencia());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, getNombreCiudad(rmt.getUbicacion_procedencia()));
            ps.setString(4, rmt.getFechaReporte());
            ps.setString(5, rmt.getTipo_reporte());
            ps.setString(6, rmt.getNombre_reporte());
            ps.setString(7, rmt.getReg_status());
            ps.setString(8, rmt.getCreation_user());
            ps.setString(9, rmt.getUbicacion_procedencia());
            ps.setString(10, getNombreCiudad(rmt.getUbicacion_procedencia()));
            ps.setString(11, rmt.getNumpla());
            sql = ps.toString();
            
            ps = crearPreparedStatement("SQL_UPDATE_ULT_REP_TRAFICO_LLEGADA");
            ps.setString(1, rmt.getUbicacion_procedencia());
            ps.setString(2, rmt.getFechaReporte());
            ps.setString(3, rmt.getObservacion());
            ps.setString(4, rmt.getTipo_reporte());
            ps.setString(5, rmt.getReg_status());
            ps.setString(6, rmt.getCreation_user());
            ps.setString(7, rmt.getUbicacion_procedencia());
            ps.setString(8, rmt.getNumpla());
            sql = sql+";"+ps.toString();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS TABLAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_ULT_REP_INGRESO_LLEGADA");
            desconectar("SQL_UPDATE_ULT_REP_TRAFICO_LLEGADA");
        }
        return sql;
    }
    
    /**
     * M�todo que buscar y llena un vector dada la secuencia de la ruta de una planilla
     * @autor.......Armando Oviedo
     * @param.......Ruta de la planilla
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarPuestosControl() throws SQLException{
        pc = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            if(dp!=null && dp.getRutapla()!=null) {
                String origen = dp.getRutapla().substring(0,2);
                String destino = dp.getRutapla().substring(2,4);
                String ruta = dp.getRutapla().substring(4,dp.getRutapla().length());
                String via = "";
                ps = crearPreparedStatement("SQL_GET_VIA");
                ps.setString(1, origen);
                ps.setString(2, destino);
                ps.setString(3, ruta);
                ////System.out.println("SQL -> "+ps);
                rs = ps.executeQuery();
                pc = new Vector();
                while(rs.next()){
                    via = rs.getString("VIA");
                }
                int cont = 0;
                for(int i=0;i<via.length();i++){
                    cont++;
                    if(cont==2){
                        Vector fila = new Vector();
                        //String codciu = via.substring(i-1,i+1);
                        String codciu = via!=null ? via.substring(i-1,i+1) : "";
                        Vector ciu = getDatosCiudad(codciu);
                        if( ciu == null ){
                            fila.add(codciu+" [No existe]");
                            fila.add( "" );
                            fila.add( "" );
                            fila.add( "" );
                            fila.add( "" );//David Pina Lopez
                            fila.add( "" );//David Pina Lopez
                        }else{
                            fila.add( codciu);
                            fila.add( (String) ciu.get(0) );
                            fila.add( (String) ciu.get(1) );
                            fila.add( (String) ciu.get(2) );
                            fila.add( (String) ciu.get(3) );//David Pina Lopez
                            fila.add( (String) ciu.get(4) );//David Pina Lopez
                        }
                        pc.add(fila);
                        cont = 0;
                    }
                }
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LOS PUESTOS DE CONTROL, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_VIA");
        }
    }
    /**
     * M�todo que actualiza los campos  de ultimo reporte en ingreso trafico y trafico
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarUltPC() throws SQLException{
        PreparedStatement ps = null;
        String sql ="";
        try{
            /*String nextpc = getCodNextPC();
            double duracion = getDuracionTramo(ultrmt.getCodUbicacion(),nextpc);
            double demora = getDemora();
            String nomzona = getNombreZona(rmt.getZona());*/
            ps = crearPreparedStatement("SQL_UPDATE_ULTREP_ING");
            ps.setString(1, rmt.getUbicacion_procedencia());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, getNombreCiudad(rmt.getUbicacion_procedencia()));
            ps.setString(4, rmt.getFechaReporte());
            ps.setString(5, rmt.getTipo_reporte());
            ps.setString(6, rmt.getNombre_reporte());
            ps.setString(7, rmt.getReg_status());
            ps.setString(8, rmt.getUbicacion_procedencia() );
            ps.setString(9, getNombreCiudad( rmt.getUbicacion_procedencia() ) );
            ps.setString(10, rmt.getNumpla());
            sql = ps.toString();
            
            ps = crearPreparedStatement("SQL_UPDATE_ULTREP_TRAFICO");
            ps.setString(1, rmt.getUbicacion_procedencia());
            ps.setString(2, rmt.getFechaReporte());
            ps.setString(3, rmt.getObservacion());
            ps.setString(4, rmt.getTipo_reporte());
            ps.setString(5, rmt.getReg_status());
            ps.setString(6, rmt.getNumpla());
            sql = sql+";"+ps.toString();
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS TABLAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_ULTREP_ING");
            desconectar("SQL_UPDATE_ULTREP_TRAFICO");
        }
        return sql;
    }
    
    /*Modificaciond e David*/
    public void BuscarPlanillaDM(String numpla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        dp = null;
        try{
            ps = crearPreparedStatement("SQL_GETDATOSPLANILLADM");
            ps.setString(1, numpla);
            
            rs = ps.executeQuery();
            while(rs.next()){
                dp = new DatosPlanillaRMT();
                dp.setNumpla(rs.getString("PLANILLA"));
                dp.setOripla(rs.getString("ORIGEN"));
                dp.setNomciuori(rs.getString("NOMORIGEN"));
                dp.setDespla(rs.getString("DESTINO"));
                dp.setNomciudest(rs.getString("NOMDESTINO"));
                dp.setPlaveh(rs.getString("PLACA"));
                dp.setCedcon(rs.getString("CEDCON"));
                dp.setNombrecond(rs.getString("NOMCOND"));
                dp.setRutapla(rs.getString("RUTA"));
                dp.setReg_status( rs.getString("reg_status"));
                dp.setFecha_prox_rep(rs.getString("fecha_prox_reporte"));
                dp.setPto_control_ultreporte(rs.getString("pto_control_ultreporte")!=null?rs.getString("pto_control_ultreporte"):rs.getString("ORIGEN"));
                dp.setPto_control_proxreporte(rs.getString("pto_control_proxreporte")!=null?rs.getString("pto_control_proxreporte"):"");
                dp.setNom_ciudad(rs.getString("nom_ciudad")!=null?rs.getString("nom_ciudad"):"");
                dp.setCod_ciudad(rs.getString("cod_ciudad")!=null?rs.getString("cod_ciudad"):"");
                dp.setNom_propietario(rs.getString("nomprop")!=null?rs.getString("nomprop"):"");
                dp.setCed_propietario(rs.getString("cedprop")!=null?rs.getString("cedprop"):"");
                dp.setUser_update( rs.getString("user_update")!=null?rs.getString("user_update"):"");
                
                dp.setZona( rs.getString( "zona" )!=null?rs.getString( "zona" ) : "" ); //David Pina Lopez
                dp.setTipo_reporte( rs.getString("tipo_reporte")!=null?rs.getString("tipo_reporte"):""); //David Pina Lopez
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LA PLANILLA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GETDATOSPLANILLADM");
        }
    }
    
    
    
    
    /* Modificacion Diogenes 07-10-2006*/
    /**
     * M�todo obtenerUrbanoUrbano, que obtiene las planillas cuya zona = 000
     * y esten en campo urbano = 'S'
     * @autor.......Diogenes Bastidas
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector obtenerUrbanoUrbano() throws SQLException{
        
        PreparedStatement st = null;
        
        try{
            Ingreso_Trafico it ;
            ResultSet rs = null;
            
            st = this.crearPreparedStatement("SQL_URBANO_URBANO");
            rs = st.executeQuery();
            
            urbanosUrbanos = new Vector();
            
            while( rs.next() ){
                it = new Ingreso_Trafico();
                Ingreso_Trafico ing_tra = it.load(rs);
                ing_tra.setTipo_eliminacion("URBURB");
                urbanosUrbanos.add( ing_tra );
            }
            
        }catch(Exception ex){
            ////System.out.println("error en obtener Urbano Urbano: "+ex.getMessage());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar("SQL_URBANO_URBANO");
        }
        return  urbanosUrbanos;
    }
    
    /**
     * M�todo obtenerUrbanoNoAplica, que obtiene las planillas cuya zona = 000
     * y esten en campo urbano = 'N'
     * @autor.......Diogenes Bastidas
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector obtenerUrbanoNoAplica() throws SQLException{
        
        PreparedStatement st = null;
        
        try{
            Ingreso_Trafico it ;
            ResultSet rs = null;
            
            st = this.crearPreparedStatement("SQL_URBANO_NO_APLICA");
            rs = st.executeQuery();
            
            urbanosNoAplica = new Vector();
            
            while( rs.next() ){
                it = new Ingreso_Trafico();
                Ingreso_Trafico ing_tra = it.load(rs);
                ing_tra.setTipo_eliminacion("URBNOA");
                urbanosNoAplica.add( ing_tra );
            }
            
        }catch(Exception ex){
            ////System.out.println("error en obtener Urbano No aplica: "+ex.getMessage());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar("SQL_URBANO_NO_APLICA");
        }
        return  urbanosNoAplica;
    }
    
    
    /**
     * M�todo obtenerVacios, que obtiene las planillas que esten vacias
     * @autor.......Diogenes Bastidas
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector obtenerVacios() throws SQLException{
        
        PreparedStatement st = null;
        
        try{
            Ingreso_Trafico it ;
            ResultSet rs = null;
            
            st = this.crearPreparedStatement("SQL_VACIOS");
            rs = st.executeQuery();
            
            vacios = new Vector();
            
            while( rs.next() ){
                it = new Ingreso_Trafico();
                Ingreso_Trafico ing_tra = it.load(rs);
                ing_tra.setTipo_eliminacion("VACIOS");
                vacios.add( ing_tra );
            }
            
        }catch(Exception ex){
            ////System.out.println("error en obtener vacios: "+ex.getMessage());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar("SQL_VACIOS");
        }
        return  vacios;
    }
    
    
    
    
    /**
     * M�todo obtenerSJSinTrafico, que obtiene las planillas cuyas  remesas
     * el  standar esta entablaben como estandar sin trafico
     * y esten en campo urbano = 'S'
     * @autor.......Diogenes Bastidas
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector obtenerSJSinTrafico() throws SQLException{
        
        PreparedStatement st = null;
        String SQL="SQL_SJSINTRAFICO";
        
        try{
            Ingreso_Trafico it ;
            ResultSet rs = null;
            
            st = this.crearPreparedStatement(SQL);
            rs = st.executeQuery();
            
            vecsjsintrafico = new Vector();
            
            while( rs.next() ){
                it = new Ingreso_Trafico();
                Ingreso_Trafico ing_tra = it.load(rs);
                ing_tra.setTipo_eliminacion("SINSJT");
                vecsjsintrafico.add( ing_tra );
            }
            
        }catch(Exception ex){
            ////System.out.println("error en obtener Urbano Urbano: "+ex.getMessage());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar(SQL);
        }
        return  vecsjsintrafico;
    }
    
    /**
     * M�todo que retorna true o false si la planilla ha tenido algun reporte de trafico.
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean tieneReporte(String planilla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        try{
            ps = crearPreparedStatement("SQL_TIENEREP");
            ps.setString(1, planilla);
            rs = ps.executeQuery();
            while(rs.next()){
                existe = true;
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL COMPROBAR SI LA PLANILLA TIENE REPORTES DE TRAFICO."+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_TIENEREP");
        }
        return existe;
    }
    
   
    /**
     * verifica si una planilla tiene Reporte de Entrega
     * @autor Ing. Henry A. Osorio Gonzalez
     * @param planilla
     * @throws SQLException
     * @version 1.0.
     **/
    public boolean tieneReporteEntraga(String planilla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_TIENE_REP_ENTREGA");
            ps.setString(1, planilla);
            rs = ps.executeQuery();
            return rs.next();
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL COMPROBAR SI LA PLANILLA TIENE REPORTES DE TRAFICO."+ex.getMessage()+", "+ex.getErrorCode());
        } finally {
            if(ps != null) {
                ps.close();
            }
            desconectar("SQL_TIENE_REP_ENTREGA");
        }       
    }
    
    /**
     * Obtiene los registros dentro de un per�odo dado, teniendo en cuaneta el campo 'creation_date'
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @throws SQLException
     * @version 1.0.
     **/
    public Vector detalleReporteDeOportunidad(String fechaI, String fechaF) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        Vector lineas = new Vector();
        
        try{
            
            ps = crearPreparedStatement("SQL_REPORTE_OPORTUNIDAD");
            ps.setString(1, fechaI);
            ps.setString(2, fechaF);
            
            
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Vector vec = new Vector();
                
                rmt = new RepMovTrafico();
                rmt = rmt.load(rs);  
                rmt.setFechareporte_planeado(rs.getString("fechareporte_planeado"));
                rmt.setCausa(rs.getString("causa"));
                rmt.setClasificacion(rs.getString("clasificacion"));                
                String nomusuario = rs.getString("nomusuario");                
                String nomusuario_up = rs.getString("user_update_login");
                String nomcliente = rs.getString("nomcliente");
                String cliente = rs.getString("cliente"); 
                
                vec.add(rmt);
                vec.add(nomusuario);
                vec.add(nomcliente);
                vec.add(cliente);
                vec.add(nomusuario_up);
                
                lineas.add(vec);
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DEL REPORTE DE OPORTUNIDAD  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){               
                ps.close();                
            }
            desconectar("SQL_REPORTE_OPORTUNIDAD");
        }        
        return lineas;
    }
    
    /**
     * Obtiene los registros dentro de un per�odo dado, teniendo en cuaneta el campo 'creation_date'
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechaI Fecha inicial del per�odo
     * @param fechaF Fecha final del per�odo
     * @throws SQLException
     * @version 1.0.
     **/
    public Vector detalleReporteDeOportunidadCMSA(String fechaI, String fechaF) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        Vector lineas = new Vector();
        
        try{
            
            ps = crearPreparedStatement("SQL_REPORTE_OPORTUNIDAD_CMSA");
            ps.setString(1, fechaI);
            ps.setString(2, fechaF);
            
            
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Vector vec = new Vector();
                
                rmt = new RepMovTrafico();
                rmt = rmt.load(rs);  
                rmt.setFechareporte_planeado(rs.getString("fechareporte_planeado"));
                rmt.setCausa(rs.getString("causa"));
                rmt.setClasificacion(rs.getString("clasificacion"));                
                String nomusuario = rs.getString("nomusuario");                
                String nomusuario_up = rs.getString("user_update_login");
                String nomcliente = rs.getString("nomcliente");
                String cliente = rs.getString("cliente"); 
                
                vec.add(rmt);
                vec.add(nomusuario);
                vec.add(nomcliente);
                vec.add(cliente);
                vec.add(nomusuario_up);
                
                lineas.add(vec);
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DEL REPORTE DE OPORTUNIDAD  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_REPORTE_OPORTUNIDAD_CMSA");
        }
        
        return lineas;
    }


    
     public String nvl( String value ){
        return value!=null? value : "";
    }
     
     /*MODIFICACIONES IVAN Y MARIO TRAFICO*/
     
     
     /**
     * M�todo que actualiza las tablas de ingreso tr�fico y tr�fico despu�s de una inserci�n o modificaci�n
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public String actualizarIngresoTrafico2() throws SQLException{
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        String sql ="";
        try{
            String nextpc = getCodNextPC();
            double duracion = getDuracionTramo(ultrmt.getCodUbicacion(),nextpc);
            double demora = getDemora();
            String nomzona = getNombreZona(rmt.getZona());
            ps = crearPreparedStatement("SQL_UPDATE_INGRESO_TRAFICO2");
            ps.setString(1, ultrmt.getCodUbicacion());
            ps.setString(2, rmt.getObservacion());
            ps.setString(3, getNombreCiudad(ultrmt.getCodUbicacion()));
            ps.setString(4, rmt.getFechaReporte());
            ps.setString(5, nextpc);
            ps.setString(6, getNombreCiudad(nextpc));
            ps.setString(7, rmt.getFechaReporte() );
            ps.setString(8, duracion+" HOUR");
            ps.setString(9, demora+" MINUTE");
            
            ps.setString(10, rmt.getTipo_reporte());
            ps.setString(11, this.getNombreReporte(rmt.getTipo_reporte()));
            ps.setString(12, "");
            ps.setString(13, "");
            ps.setString(14, rmt.getNumpla());
            ps.executeUpdate();
            
            
            
            ps2 = crearPreparedStatement("SQL_UPDATE_TRAFICO");
            ps2.setString(1, rmt.getUbicacion_procedencia());
            ps2.setString(2, getZona (nextpc));
            ps2.setString(3, rmt.getFechaReporte());
            ps2.setString(4, nextpc);
            ps2.setString(5, rmt.getObservacion());
            ps2.setString(6, rmt.getFechaReporte());
            ps2.setString(7, duracion+" HOUR");
            ps2.setString(8, demora+" MINUTE");
            ps2.setString(9, rmt.getTipo_reporte());
            ps2.setString(10, "");
            ps2.setString(11, rmt.getCreation_user());
            ps2.setString(12, rmt.getNumpla());
            ps2.executeUpdate();
            
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR LAS TABLAS DE INGRESO TRAFICO Y TRAFICO, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(ps2 != null){
                try{
                    ps2.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_INGRESO_TRAFICO2");
            desconectar("SQL_UPDATE_TRAFICO");
        }
        return sql;
    }




    public void BuscarPlanilla(String numpla) throws SQLException{
        PreparedStatement   ps = null, st  = null;
        ResultSet           rs = null, rs2 = null;
        dp                     = null;

        String   query1   = "SQL_GETDATOSPLANILLA";
        String   query2   = "SQL_CARAVANA";
        
        try{
            
            
            ps = crearPreparedStatement(query1);
            st = crearPreparedStatement(query2);
            
            ps.setString(1, numpla);
            rs = ps.executeQuery();
            
            while(rs.next()){
                dp = new DatosPlanillaRMT();
                dp.setNumpla(rs.getString("PLANILLA"));
                dp.setOripla(rs.getString("ORIGEN"));
                dp.setNomciuori(rs.getString("NOMORIGEN"));
                dp.setDespla(rs.getString("DESTINO"));
                dp.setNomciudest(rs.getString("NOMDESTINO"));
                dp.setPlaveh(rs.getString("PLACA"));
                dp.setCedcon(rs.getString("CEDCON"));
                dp.setRutapla(rs.getString("RUTA_PLA"));
                dp.setNombrecond(rs.getString("NOMCOND"));
                dp.setCaravana("0");
                dp.setStdjob(rs.getString("std_job_no"));
                dp.setFecha_prox_rep(rs.getString("fecha_prox_reporte"));
                dp.setPto_control_ultreporte(rs.getString("pto_control_ultreporte")!=null?rs.getString("pto_control_ultreporte"):rs.getString("ORIGEN"));
                dp.setPto_control_proxreporte(rs.getString("pto_control_proxreporte")!=null?rs.getString("pto_control_proxreporte"):"");
                dp.setNom_ciudad(rs.getString("nom_ciudad")!=null?rs.getString("nom_ciudad"):"");
                dp.setCod_ciudad(rs.getString("cod_ciudad")!=null?rs.getString("cod_ciudad"):"");
                dp.setReg_status(rs.getString("reg_status"));
                dp.setNom_propietario(rs.getString("nomprop")!=null?rs.getString("nomprop"):"");
                dp.setCed_propietario(rs.getString("cedprop")!=null?rs.getString("cedprop"):"");
                dp.setUser_update ( rs.getString ("user_update")!=null?rs.getString ("user_update"):"");
                
                dp.setZona ( rs.getString ( "zona" )!=null?rs.getString ( "zona" ) : "" ); //David Pina Lopez
                dp.setTipo_reporte( rs.getString ("tipo_reporte")!=null?rs.getString ("tipo_reporte"):""); //David Pina Lopez
                dp.setEintermedias(rs.getString("eintermedias"));
                dp.setNeintermedias(rs.getString("neintermedias"));
                
                //pto_control_proxreporte
                if(rs.getString("caravana").equalsIgnoreCase("S")){
                    st.setString(1, numpla );
                    rs2 = st.executeQuery();
                    if(rs2.next()){
                        dp.setCaravana(rs2.getString("NUMCARAVANA"));
                    }
                    st.clearParameters();
                }
                
                
            }
            
            
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR LA PLANILLA, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            
            if( ps != null )  ps.close();
            if( st != null )  st.close();
            if( rs != null )  rs.close();
            if( rs2!= null )  rs2.close();
            desconectar(query1);
            desconectar(query2);
            
        }
    }
    

  /**
     * M�todo que actualiza las tablas de tr�fico e ingreso_tr�fico cuando se elimina un registro de reporte de movimiento de tr�fico
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void delupdIngrTrafico() throws SQLException{
        PreparedStatement ps = null;
        try{
            String numpla = dp.getNumpla();
            setLastCreatedReport();
            RepMovTrafico tmp = getLastCreatedReport();
            if(tmp==null){
                ps = crearPreparedStatement("SQL_UPDATE_INGRESOTRAFICO_BL");
                ps.setString(1, numpla);
                ps.executeUpdate();
                ps = crearPreparedStatement("SQL_UPDATE_TRAFICO_BL");
                ps.setString(1, numpla);
                ps.executeUpdate();
            }
            else{
                RepMovTrafico rep = this.getUltimoRepDiferenteDeOBS(rmt.getNumpla());
                //System.out.println("QQQQQQQQQQQQrep.getTipo_reporte()---->" + rep.getTipo_reporte());
                String reg_status =  (isDetencion(rep.getTipo_reporte()))?"D":"";
                String UltimpPC = tmp.getUbicacion_procedencia();
                setUltimoPC();
                tmp = getLastCreatedReport();
                BuscarPuestosControl();
                getNextPC();
                String nextpc = getProxRMT ().getUbicacion_procedencia();



                double duracion = getDuracionTramo(tmp.getUbicacion_procedencia(),nextpc);
                tmp.setZona(getZona(nextpc));



                ps = crearPreparedStatement("SQL_UPDATE_INGRESO_TRAFICO_TRAF");
                ps.setString(1, UltimpPC);
                ps.setString(2, tmp.getObservacion());
                ps.setString(3, tmp.getZona());
                ps.setString(4, getNombreZona(tmp.getZona()));
                ps.setString(5, getNombreCiudad(UltimpPC));
                ps.setString(6, tmp.getFechaReporte());
                ps.setString(7, nextpc);
                ps.setString(8, getNombreCiudad(nextpc));
                ps.setString(9, tmp.getFechaReporte());
                ps.setString(10, duracion+" HOUR");
                ps.setString(11, "0 MINUTE");
                ps.setString(12, reg_status);
                ps.setString(13, rep.getTipo_reporte());
                ps.setString(14, this.getNombreReporte(rep.getTipo_reporte()));
                ps.setString(15, tmp.getNumpla());
                ps.executeUpdate();
                ps = crearPreparedStatement("SQL_UPDATE_TRAFICO_TRAF");
                ps.setString(1, UltimpPC);
                ps.setString(2, tmp.getZona());
                ps.setString(3, tmp.getFechaReporte());
                ps.setString(4, nextpc);
                ps.setString(5, tmp.getObservacion());
                ps.setString(6, tmp.getFechaReporte());
                ps.setString(7, duracion+" HOUR");
                ps.setString(8, "0 MINUTE");
                ps.setString(9, reg_status);
                ps.setString(10, rep.getTipo_reporte());
                ps.setString(11, tmp.getNumpla());
                ps.executeUpdate();
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR CAMPOS VACIOS EN INGRESO_TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_INGRESOTRAFICO_BL");
            desconectar("SQL_UPDATE_TRAFICO_BL");
            desconectar("SQL_UPDATE_INGRESO_TRAFICO_TRAF");
            desconectar("SQL_UPDATE_TRAFICO_TRAF");
        }
    }
    
       /**
     * M�todo que retorna true o false si la placa tiene entrega.
     * @autor.......Karen Reales
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean tieneEntrega(String placa) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = true;
        try{
            ps = crearPreparedStatement("SQL_TIENEENTREGA");
            ps.setString(1, placa);
            rs = ps.executeQuery();
            if(rs.next()){
                existe = false;
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL COMPROBAR SI LA PLACA TIENE ENTREGA DE TRAFICO."+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_TIENEENTREGA");
        }
        return existe;
    }
    



    /**
     * Obtiene los registros de los vehiculos varados y demorados del archivo de reporte
     * movimiento tr�fico
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws <code>SQLException</code>
     * @version 1.0
     */
    public void reporteVehiculosDemoradosPlaca() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        this.reportes = new Vector();
        
        try{
            
            
            String sql = this.obtenerSQL("SQL_REPVARADOS_D");
            con = conectar("SQL_REPVARADOS_D");
            ps = con.prepareStatement(sql);
            
            org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
            logger.info("REPORTE VEHVARADOS: " + ps);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Hashtable ht = new Hashtable();
                
                ht.put("agencia", rs.getString("agencia"));
                ht.put("agenciad", rs.getString("agenciad"));
                ht.put("plaveh", rs.getString("plaveh"));
                ht.put("cliente", rs.getString("cliente"));
                ht.put("nomcliente", rs.getString("nomcliente"));
                ht.put("origen", rs.getString("origen"));
                ht.put("destino", rs.getString("destino"));
                ht.put("numpla", rs.getString("numpla"));
                ht.put("tipo_procedencia", rs.getString("tipo_procedencia"));
                ht.put("ubicacion", rs.getString("ubicacion"));
                ht.put("tipo_reporte", rs.getString("tipo_reporte"));
                ht.put("fechareporte", rs.getString("fechareporte"));
                ht.put("zona", (rs.getString("zona")!=null)? rs.getString("zona") : "");
                ht.put("tiempo", rs.getString("tiempo"));
                ht.put("observacion", rs.getString("observacion"));
                
                this.reportes.add(ht);
            }
            
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LOS TOTALES DEL REPORTE DE VEHICULOS DEMORADOS Y/O VARADOS  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_REPVARADOS_D");
        }
    }    
    
    
     /**
     * M�todo que actualiza en la tabla tr�fico 
     * @autor.......Ivan DArio Gomez
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void updateTrafico(RepMovTrafico rmt) throws SQLException{
        PreparedStatement ps = null;
        try{
            
                ps = crearPreparedStatement("SQL_UPDATE_TRAFICO_ENTREGA");
                ps.setString(1, rmt.getUbicacion_procedencia() );
                ps.setString(2, rmt.getObservacion());
                ps.setString(3, rmt.getCreation_user());
                ps.setString(4, rmt.getTipo_reporte());
                ps.setString(5, rmt.getNumpla());
                ps.executeUpdate();
               
                ps.executeUpdate();
           
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL ACTUALIZAR  EN SQL_UPDATE_TRAFICO_ENTREGA. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_UPDATE_TRAFICO_ENTREGA");
            
        }
    }



 /**
     * M�todo que setea un objeto de reporte de movimiento de tr�fico con el ultimo encontrado
     * @autor.......Karen Reales
     * @param.......N�mero de planilla
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarReporteMovTraf(String numpla) throws SQLException{
        RepMovTrafico rmt = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = crearPreparedStatement("SQL_GET_ING_TRAF");
            ps.setString(1, numpla);
            
            rs = ps.executeQuery();
            if(rs.next()){
                rmt = new RepMovTrafico();
                rmt.setNumpla(rs.getString("planilla"));
                rmt.setZona(rs.getString("zona"));
                rmt.setUb_ultreporte(rs.getString("pto_control_ultreporte"));
                rmt.setFec_ultreporte(rs.getString("fecha_ult_reporte"));
                rmt.setFecha_prox_reporte(rs.getString("fecha_prox_reporte"));
                rmt.setUbicacion_procedencia( rs.getString("pto_control_proxreporte") ); //David Pina Lopez
                rmt.setDestino(rs.getString("destino"));//Ivan Dario gomez
                this.setRmt(rmt);
            }
            else{
                this.setRmt(rmt);
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER EL REPORTE DE MOVIMIENTO DE TRAFICO. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_GET_ING_TRAF");
        }
    }
    
    /**
     * Obtiene el reporte de salida de una planilla.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws <code>SQLException</code>
     * @version 1.0
     */
    public String reporteSalida(String numpla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        
        String reporte = "";
        
        try{
            
            
            ps = this.crearPreparedStatement("SQL_PLANVJE_REPSALIDA");
            ps.setString(1, numpla);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                reporte = rs.getString("fechareporte");
            }
            
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DEL REPORTE DE SALIDA  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PLANVJE_REPSALIDA");
        }
        
        return reporte;
    }
    
    /**
     * Obtiene el reportes posteriores al reporte de salida de una planilla.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws <code>SQLException</code>
     * @version 1.0
     */
    public int reportesPosteriores(String numpla, String fecharep) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        
        int reportes = 0;
        
        try{
            
            
            ps = this.crearPreparedStatement("SQL_PLANVJE_POS_REPSALIDA");
            ps.setString(1, numpla);
            ps.setString(2, fecharep);
            
            rs = ps.executeQuery();
                        
            while(rs.next()){                
                reportes++;
            }
            
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DEL REPORTES POSTERIORES "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_PLANVJE_POS_REPSALIDA");
        }
        
        return reportes;
    }
    
    
    /**********************************************************************
     *Metodo para obtener el reporte de vahiculos varados del cliente dado
     *            en el periodo dado
     *@param: String cliente cliente al cual se le van a consultar los varados
     *@param: String inicio fecha inicial
     *@param: String fin fecha final
     *@param: String dstrct distrito
     *@author: Ing. Osvaldo P�rez Ferrer
     *@return: Vector de Hashtable con el reporte
     *@throws: Exception en caso de un error
     ************************************************************************/
    public Vector reporteVaradosCliente( String cliente, String inicio, String fin, String dstrct) throws Exception{
        
        Vector varados = new Vector();
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            try{
                
                st = this.crearPreparedStatement("SQL_REPORTE_VARADOS_X_CLIENTE");
                                
                st.setString( 1, inicio );
                st.setString( 2, fin );
                st.setString( 3, dstrct );
                st.setString( 4, cliente );
                
                ////System.out.println( st.toString() );
                
                rs = st.executeQuery();
                
            }catch (Exception e){
                e.printStackTrace();
            }finally{
                this.desconectar("SQL_REPORTE_VARADOS_X_CLIENTE");
            }
            
            if( rs!=null ){
                
                while(rs.next()){
                    Hashtable ht = new Hashtable();
                    
                    ht.put("cliente", nvl( rs.getString("cliente") ));
                    ht.put("ot", nvl( rs.getString("ots") ));
                    ht.put("desc_ot", nvl( rs.getString("descripcion") ));
                    ht.put("ruta_ot", nvl( rs.getString("ruta_ot") ));
                    ht.put("ag_desp", nvl( rs.getString("ag_desp") ));
                    ht.put("oc", nvl( rs.getString("oc") ));
                    ht.put("ruta_oc", nvl( rs.getString("ruta_oc") ));
                    ht.put("placa", nvl( rs.getString("placa") ));
                    ht.put("fecha_reporte", nvl( rs.getString("fecha_reporte") ));
                    ht.put("hora_reporte", nvl( rs.getString("hora_reporte") ));
                    ht.put("puesto_control", nvl( rs.getString("puesto_control") ));
                    
                    ht.put("observacion", nvl( rs.getString("observacion") ));                    
                    ht.put("fecha_despacho", nvl( rs.getString("fecha_despacho") ));
                    ht.put("hora_despacho", nvl( rs.getString("hora_despacho") ));
                    
                    ht.put("fecha_entrega", nvl( rs.getString("fecha_entrega") ));
                    ht.put("hora_entrega", nvl( rs.getString("hora_entrega") ));
                    Vector r = this.reinicioVarado( nvl(rs.getString("oc") ), nvl( rs.getString("fechareporte")) );
                    
                    if( r!=null & r.size()==4 ){
                        ht.put( "sig_fec", r.get(0) );
                        ht.put( "sig_hor", r.get(1) );
                        ht.put( "sig_pto", r.get(2) );
                        ht.put( "tiempo", r.get(3) );
                    }else{
                        ht.put( "sig_fec", "" );
                        ht.put( "sig_hor", "" );
                        ht.put( "sig_pto", "" );
                        ht.put( "tiempo", "" );
                    }
                    
                    varados.add( ht );
                }
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
        }finally{
            if( rs!=null ) rs.close();
            if( st!=null ) st.close();
        }
        
        return varados;
    }
    
    
    /**********************************************************************
     *Metodo para obtener datos de reinicio despues del varado dado
     *@param: String numpla planilla del varado
     *@param: String fecha fecha del varado     
     *@return: Vector con datos del varado
     *@author: Ing. Osvaldo P�rez Ferrer
     *@throws: Exception en caso de un error
     ************************************************************************/
    public Vector reinicioVarado( String numpla, String fecha ) throws Exception{
        
        Vector v = new Vector();
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            try{
                
                st = this.crearPreparedStatement("SQL_REINICIO_VARADO");
                
                st.setString( 1, fecha );
                st.setString( 2, numpla );
                st.setString( 3, fecha );
                               
                rs = st.executeQuery();
               
            }catch (Exception e){
                e.printStackTrace();
            }finally{
                this.desconectar("SQL_REINICIO_VARADO");
            }
            
            if( rs!=null && rs.next() ){
                                
                v.add( 0, nvl( rs.getString("fecha_reporte") ) );
                v.add( 1, nvl( rs.getString("hora_reporte") ) );
                v.add( 2, nvl( rs.getString("puesto_control") ) );
                v.add( 3, nvl( rs.getString("tiempo") ) );

            }
            
        }catch (Exception ex){
            ex.printStackTrace();
        }finally{
            if( rs!=null ) rs.close();
            if( st!=null ) st.close();
        }
        
        return v;
    }  
    
    /**
     * M�todo que retorna el nombre DEL TPO DE REPORTE
     * @autor.......iVAN gOMEZ
     * @param.......String tipo_reporte
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Nombre del reporte
     **/
    public String getNombreReporte(String tipo_reporte)throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nombre="";
        try{
            ps = crearPreparedStatement("SQL_NOMBRE_REPORTE");
            ps.setString(1, tipo_reporte);
            rs = ps.executeQuery();
            while(rs.next()){
                nombre = rs.getString(1);
            }
        }
        catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR SQL_NOMBRE_REPORTE, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_NOMBRE_REPORTE");
        }
        return nombre;
    }
    
    
     /**
     * M�todo que busca el ultimo reporte de trafico diferente a una observacion
     * @autor.......Ivan Gomez
     * @throws......SQLException
     * @version.....1.0.
     **/
    public RepMovTrafico getUltimoRepDiferenteDeOBS(String planilla) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        RepMovTrafico rep = null;
        try{
            ps = crearPreparedStatement("SQL_ULT_REP_DIF_DE_OBS");
            ps.setString(1, planilla);
            //System.out.println("SQLLLLLLLLLLL------->"+ ps.toString());
            rs = ps.executeQuery();
            if(rs.next()){
                rep = new RepMovTrafico();
                rep = rep.load(rs);
               
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL OBTENER SQL_ULT_REP_DIF_DE_OBS. "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_ULT_REP_DIF_DE_OBS");
        }
        return rep;
    }    

    public void reporteVehiculosDemoradosPlacaMod(String plaveh, String cliente, String ori, String dest, String agencia, String fechai, String fechaf, String tiprep, String zona) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs=null;
        PreparedStatement st2 = null;
        ResultSet rs2 = null;
        this.reportes = new Vector();
        
        try{
            
            String sql = this.obtenerSQL("SQL_REPVARADOS_D");
            st2 = crearPreparedStatement("SQL_CLIENTE_REMESAS");
            int pos = 1;
            
            if ( plaveh.length()!=0 ) {
                if ( pos>=1 ) sql += " AND ";
                sql += " UPPER( pl.plaveh ) = UPPER( ? ) ";
                pos++;
            }
            
            if ( cliente.length()!=0 ) {
                if ( pos>=1 ) sql += " AND ";
                sql +=  " codcli = ? ";
                pos++;
            }
            
            if ( ori.length()!=0 && dest.length()!=0 ) {
                if ( pos>=1 ) sql += " AND ";
                sql +=  " oripla = ? AND despla = ? ";
                pos++;
            }
            
            if ( agencia.length()!=0 ) {
                if ( pos>=1 ) sql += " AND ";
                sql +=  " agcpla = ? ";
                pos++;
            }
            
            if ( fechai.length()!=0 && fechaf.length()!=0) {
                if( pos>=1 ) sql += "AND";
                sql += " rmt.fechareporte BETWEEN ? AND ? ";
                pos++;
            }
            
            /* zona */
            
            if ( zona.length()!=0 ){
                if( pos>=1 ) sql += "AND";
                sql += " codzona = ? ";
                pos++;
            }
            
            if ( tiprep.length()!=0 ) {
                if( pos>=1 ) sql += "AND";
                if( tiprep.equals("VAR") ) {
                    sql += " rmt.tipo_reporte IN ('02')  ";
                } else {
                    sql += " rmt.tipo_reporte NOT IN ('02') ";
                }
                pos++;
            } else {
                sql += "";
            }
            
            sql += " AND pl.reg_status <> 'A' AND rmt.reg_status <> 'A' " +
            "GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13 ORDER BY numpla";
            con = conectar("SQL_REPVARADOS_D");
         
            ps = con.prepareStatement(sql);
            
            org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
            
            pos = 1;
            
            if ( plaveh.length()!=0 ) {
                ps.setString(pos, plaveh);
                pos++;
            }
            
            if ( cliente.length()!=0 ) {
                ps.setString(pos, cliente);
                pos++;
            }
            
            if ( ori.length()!=0 && dest.length()!=0 ) {
                ps.setString(pos, ori);
                pos++;
                ps.setString(pos, dest);
                pos++;
            }
            
            if ( agencia.length()!=0 ) {
                ps.setString(pos, agencia);
                pos++;
            }
            
            if ( fechai.length()!=0 && fechaf.length()!=0) {
                ps.setString(pos, fechai + " 00:00:00");
                pos++;
                ps.setString(pos, fechaf + " 23:59:59"); 
                pos++;
            }
            
            if ( zona.length()!=0 ){
                ps.setString(pos, zona);
                pos++;
            }
            
            logger.info("REPORTE VEHVARADOS: " + ps);
            //System.out.println(ps);
            rs = ps.executeQuery();
            
            while(rs.next()){
                Hashtable ht = new Hashtable();
                
                ht.put("agencia", rs.getString("agencia"));
                ht.put("plaveh", rs.getString("plaveh"));
                ht.put("origen", rs.getString("origen"));
                ht.put("destino", rs.getString("destino"));
                ht.put("numpla", rs.getString("numpla"));
                ht.put("tipo_procedencia", rs.getString("tipo_procedencia"));
                ht.put("ubicacion", rs.getString("ubicacion"));
                ht.put("tipo_reporte", rs.getString("tipo_reporte"));
                ht.put("fechareporte", rs.getString("fechareporte"));
                ht.put("zona", (rs.getString("zona")!=null)? rs.getString("zona") : "");
                ht.put("tiempo", rs.getString("tiempo"));
                ht.put("observacion", rs.getString("observacion"));
                ht.put("OTS", rs.getString("ots").replaceAll("-",","));
                
                st2.clearParameters();
                st2.setString(1,rs.getString("numpla"));
                ////System.out.println("SQL FLETE " + st2.toString()); 
                rs2 = st2.executeQuery();
                if(rs2.next()){
                    
                    ht.put("agenciad", rs2.getString("agduenia"));
                    ht.put("nomcliente", rs2.getString("cliente")); 
                }
                
                this.reportes.add(ht);
            }
            
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LOS TOTALES DEL REPORTE DE VEHICULOS DEMORADOS Y/O VARADOS  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_REPVARADOS_D");
            desconectar("SQL_CLIENTE_REMESAS");
        }
    }




public void reporteVehiculosDemoradosPlaca(String plaveh, String cliente, String ori, String dest, String agencia, String fechai, String fechaf, String tiprep, String zona) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        PreparedStatement st2 = null;
        ResultSet rs2 = null;
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        
        this.reportes = new Vector();
        
        try{
            
            st2 = crearPreparedStatement("SQL_CLIENTE_REMESAS");
            String sql = this.obtenerSQL("SQL_REPVARADOS");
            
            int pos = 1;
            
            if ( plaveh.length()!=0 ) {
                if ( pos>1 ) sql += " AND ";
                sql += " UPPER( pl.plaveh ) = UPPER( ? ) ";
                pos++;
            }
            
            if ( cliente.length()!=0 ) {
                if ( pos>1 ) sql += " AND ";
                sql +=  " cliente=? ";
                pos++;
            }
            
            if ( ori.length()!=0 && dest.length()!=0 ) {
                if ( pos>1 ) sql += " AND ";
                sql +=  " oripla=? AND despla=? ";
                pos++;
            }
            
            if ( agencia.length()!=0 ) {
                if ( pos>1 ) sql += " AND ";
                sql +=  " agcpla=? ";
                pos++;
            }
            
            if ( fechai.length()!=0 && fechaf.length()!=0) {
                if( pos>1 ) sql += "AND";
                sql += " rmt.fechareporte BETWEEN ? AND ? ";
                pos++;
            }
            
            /* zona */
            
            if ( zona.length()!=0 ){
                if( pos>1 ) sql += "AND";
                sql += " rmt.zona = ? ";
                pos++;
            }
            
            if ( tiprep.length()!=0 ) {
                if( pos>1 ) sql += "AND";
                if( tiprep.equals("VAR") ) {
                    sql += " tipo_reporte IN ('02')  ";
                } else {
                    sql += " tipo_reporte NOT IN ('02') ";
                }
                pos++;
            } else {
                sql += "";
            }
            
            sql += " AND pl.reg_status <> 'A' AND rmt.reg_status <> 'A' " +
            "GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12 ORDER BY numpla";
            con = conectar("SQL_REPVARADOS");
            ps = con.prepareStatement(sql);
            
            org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
            
            pos = 1;
            
            if ( plaveh.length()!=0 ) {
                ps.setString(pos, plaveh);
                pos++;
            }
            
            if ( cliente.length()!=0 ) {
                ps.setString(pos, cliente);
                pos++;
            }
            
            if ( ori.length()!=0 && dest.length()!=0 ) {
                ps.setString(pos, ori);
                pos++;
                ps.setString(pos, dest);
                pos++;
            }
            
            if ( agencia.length()!=0 ) {
                ps.setString(pos, agencia);
                pos++;
            }
            
            if ( fechai.length()!=0 && fechaf.length()!=0) {
                ps.setString(pos, fechai + " 00:00:00");
                pos++;
                ps.setString(pos, fechaf + " 23:59:59"); 
                pos++;
            }
            
            if ( zona.length()!=0 ){
                ps.setString(pos, zona);
                pos++;
            }
            
            
            logger.info("REPORTE VEHVARADOS: " + ps);
            rs = ps.executeQuery();
            
            while(rs.next()){
                Hashtable ht = new Hashtable();

                ht.put("agencia", rs.getString("agencia"));
                ht.put("plaveh", rs.getString("plaveh"));
                ht.put("origen", rs.getString("origen"));
                ht.put("destino", rs.getString("destino"));
                ht.put("numpla", rs.getString("numpla"));
                ht.put("tipo_procedencia", rs.getString("tipo_procedencia"));
                ht.put("ubicacion", rs.getString("ubicacion"));
                ht.put("tipo_reporte", rs.getString("tipo_reporte"));
                ht.put("fechareporte", rs.getString("fechareporte"));
                ht.put("observacion", rs.getString("observacion"));
                ht.put("zona", (rs.getString("zona")!=null)? rs.getString("zona") : "");               
                ht.put("OTS", rs.getString("ots").replaceAll("-",","));
                
                ps1 = con.prepareStatement(this.obtenerSQL("SQL_REPVARADOS_TIEMPO"));
                ps1.setString(1, rs.getString("fechareporte"));
                ps1.setString(2, rs.getString("numpla"));
                ps1.setString(3, rs.getString("fechareporte"));
                
                ht.put("tiempo", "&nbsp;");
                logger.info("REPORTE VEHVARADOS. Tiempo de la detenci�n: " + ps1);
                rs1 = ps1.executeQuery();
                
                if( rs1.next() ){
                    ht.put("tiempo", rs1.getString("tiempo"));
                }
                
                st2.clearParameters();
                st2.setString(1,rs.getString("numpla"));
                ////System.out.println("SQL FLETE " + st2.toString()); 
                rs2 = st2.executeQuery();
                if(rs2.next()){
                    
                    ht.put("agenciad", rs2.getString("agduenia"));
                    ht.put("nomcliente", rs2.getString("cliente")); 
                }
                
                this.reportes.add(ht);
            }
            
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new SQLException("ERROR EN LA OBTENCION DE LOS TOTALES DEL REPORTE DE VEHICULOS DEMORADOS Y/O VARADOS  "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar("SQL_REPVARADOS");
            desconectar("SQL_CLIENTE_REMESAS");
        }
    }
}

