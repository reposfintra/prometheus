/****************************************************************************
 * Nombre clase: Ingreso_especialDAO.java                                   *
 * Descripci�n: Clase que maneja las consultas de los ingresos especiales.  *
 * Autor: Ing. Jose de la rosa                                              *
 * Fecha: 6 de diciembre de 2005, 10:03 AM                                  *
 * Versi�n: Java 1.0                                                        *
 * Copyright: Fintravalores S.A. S.A.                                  *
 ***************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class Ingreso_especialDAO extends MainDAO{
    private Ingreso_especial ingreso_especial;
    private Vector vecingreso_especial;
    
    /** Creates a new instance of Ingreso_especial */
    public Ingreso_especialDAO () {
        super ( "Ingreso_especialDAO.xml" );
    }
    
    /**
     * Metodo: getIngreso_especial, permite retornar un objeto de registros de Ingreso especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Ingreso_especial getIngreso_especial () {
        return ingreso_especial;
    }
    
    /**
     * Metodo: setIngreso_especial, permite obtener un objeto de registros de Ingreso especial.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setIngreso_especial (Ingreso_especial ingreso_especial) {
        this.ingreso_especial = ingreso_especial;
    }
    
    /**
     * Metodo: getIngreso_especiales, permite retornar un vector de registros de Ingreso especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getIngreso_especiales () {
        return vecingreso_especial;
    }
    
    /**
     * Metodo: setIngreso_especiales, permite obtener un vector de registros de Ingreso especial.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setIngreso_especiales (Vector vecingreso_especial) {
        this.vecingreso_especial = vecingreso_especial;
    }
    
    /**
     * Metodo: insertIngreso_especial, permite ingresar un registro a la tabla Ingreso_especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertIngreso_especial () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_INSERT");
            st.setString (1,ingreso_especial.getTipo_acuerdo ());
            st.setString (2,ingreso_especial.getCodigo_concepto ());
            st.setString (3,ingreso_especial.getClase_equipo ());
            st.setFloat (4,ingreso_especial.getPorcentaje_ingreso ());
            st.setString (5,ingreso_especial.getUsuario_creacion ());
            st.setString (6,ingreso_especial.getUsuario_modificacion ());
            st.setString (7,ingreso_especial.getBase ());
            st.setString (8,ingreso_especial.getDistrito ());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL INGRESAR UN INGRESO ESPECIAL, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_INSERT");
        }
    }
    
    /**
     * Metodo: searchIngreso_especial, permite buscar un Ingreso especial dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : tipo acuerdo, codigo concepto y clase equipo.
     * @version : 1.0
     */
    public void searchIngreso_especial (String tipo, String concepto, String clase, String distrito)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_SERCH");
            st.setString (1,tipo);
            st.setString (2,concepto);
            st.setString (3,clase);
            st.setString (4,distrito);
            rs= st.executeQuery ();
            while(rs.next ()){
                ingreso_especial.setTipo_acuerdo ((rs.getString ("tipo_acuerdo")!=null)?rs.getString ("tipo_acuerdo"):"");
                ingreso_especial.setCodigo_concepto ((rs.getString ("codigo_concepto")!=null)?rs.getString ("codigo_concepto"):"");
                ingreso_especial.setClase_equipo ((rs.getString ("clase_equipo")!=null)?rs.getString ("clase_equipo"):"");
                ingreso_especial.setPorcentaje_ingreso (rs.getFloat ("porcentaje_ingreso"));
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UN INGRESO ESPECIAL, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH");
        }
    }
    
    /**
     * Metodo: existIngreso_especial, permite buscar un Ingreso especial dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : tipo acuerdo, codigo concepto y clase equipo.
     * @version : 1.0
     */
    public boolean existIngreso_especial (String tipo, String concepto, String clase, String distrito) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        try {
            st = crearPreparedStatement ("SQL_SERCH");
            st.setString (1,tipo);
            st.setString (2,concepto);
            st.setString (3,clase);
            st.setString (4,distrito);
            rs = st.executeQuery ();
            if (rs.next ()){
                sw = true;
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR SI EXISTEN LOS INGRESOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SERCH");
        }
        return sw;
    }
    
    /**
     * Metodo: listIngreso_especial, permite listar todas los Ingreso especiales
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listIngreso_especial (String distrito)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_LIST");
            st.setString (1,distrito);
            rs= st.executeQuery ();
            vecingreso_especial = new Vector ();
            while(rs.next ()){
                ingreso_especial = new Ingreso_especial ();
                ingreso_especial.setTipo_acuerdo (rs.getString ("std_job_no"));
                ingreso_especial.setCodigo_concepto (rs.getString ("codigo_concepto"));
                ingreso_especial.setClase_equipo (rs.getString ("clase_equipo"));
                ingreso_especial.setPorcentaje_ingreso (rs.getFloat ("porcentaje_ingreso"));
                ingreso_especial.setNumero (rs.getInt ("num_ingreso"));
                vecingreso_especial.add (ingreso_especial);
            }
            
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL LISTAR LOS INGRESOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_LIST");
        }
    }
    
    /**
     * Metodo: updateIngreso_especial, permite actualizar un Ingreso especial dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario, la placa y usuario.
     * @version : 1.0
     */
    public void updateIngreso_especial () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_UPDATE");
            st.setFloat (1,ingreso_especial.getPorcentaje_ingreso ());
            st.setString (2,ingreso_especial.getUsuario_modificacion ());
            st.setString (3,ingreso_especial.getTipo_acuerdo ());
            st.setString (4,ingreso_especial.getCodigo_concepto ());
            st.setString (5, ingreso_especial.getClase_equipo ());
            st.setString (6, ingreso_especial.getDistrito ());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LOS INGRESOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_UPDATE");
        }
    }
    
    /**
     * Metodo: anularIngreso_especial, permite anular una Ingreso especial.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void anularIngreso_especial () throws SQLException{
        PreparedStatement st = null;
        try{
            st = crearPreparedStatement ("SQL_ANULAR");
            st.setString (1,ingreso_especial.getUsuario_modificacion ());
            st.setString (2,ingreso_especial.getTipo_acuerdo ());
            st.setString (3,ingreso_especial.getCodigo_concepto ());
            st.setString (4,ingreso_especial.getClase_equipo ());
            st.setString (5, ingreso_especial.getDistrito ());
            st.executeUpdate ();
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL ANULAR LOS INGRESOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_ANULAR");
        }
    }
    
    /**
     * Metodo: searchDetalleIngreso_especial, permite listar Ingreso especiales por detalles dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */
    public void searchDetalleIngreso_especial (String tipo, String codigo, String clase, String distrito) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = crearPreparedStatement ("SQL_SEARCH_DETALLES");
            st.setString (1, tipo+"%");
            st.setString (2, codigo+"%");
            st.setString (3, clase+"%");
            st.setString (4, distrito);
            rs = st.executeQuery ();
            vecingreso_especial = new Vector ();
            while(rs.next ()){
                ingreso_especial = new Ingreso_especial ();
                ingreso_especial.setTipo_acuerdo (rs.getString ("tipo_acuerdo"));
                ingreso_especial.setCodigo_concepto (rs.getString ("codigo_concepto"));
                ingreso_especial.setClase_equipo (rs.getString ("clase_equipo"));
                ingreso_especial.setPorcentaje_ingreso (rs.getFloat ("porcentaje_ingreso"));
                vecingreso_especial.add (ingreso_especial);
            }
        }catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR LOS INGRESOS ESPECIALES, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            desconectar ("SQL_SEARCH_DETALLES");
        }
    }
    
}
