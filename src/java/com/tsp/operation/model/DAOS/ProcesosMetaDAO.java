/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.ProcesoMeta;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

/**
 *
 * @author desarrollo
 */
public interface ProcesosMetaDAO {

    public ArrayList<ProcesoMeta> cargarProcesosMeta(String empresa, String status);
    
    public ArrayList<CmbGeneralScBeans> cargarComboEmpresa();

    public boolean existeMetaProceso(String empresa, String descripcion);

    public String guardarMetaProceso(String empresa, String nombre, String descripcion, String usuario);

    public String actualizarMetaProceso(String empresa, String nombre, String descripcion, String idProceso, String usuario);
    
    public String cargarComboProcesoMeta(String empresa);

    public ArrayList<ProcesoMeta> cargarProcesoInterno();

    public boolean existeProcesoInterno(String procesoMeta, String descripcion);

    public String guardarProcesoInterno(String procesoMeta, String nombre, String descripcion, String usuario, String empresa);

    public int obtenerIdProcesoInterno(String nomProceso, String idProMeta);

    public String insertarRelUnidadProInterno(int idProInterno, String string, String empresa);

    public ArrayList<UnidadesNegocio> cargarUndNegocioProinterno(String idProinterno);

    public String actualizarProcesoInterno(int idProinterno, String nombre, String descripcion, int idProMeta, String usuario, String empresa);

    public String eliminarUndProinterno(String idProinterno, String idUnidad);

    public ArrayList<Usuario> listarUsuario();

    public ArrayList<ProcesoMeta> listarProinternoUsuario(int parseInt);

    public ArrayList<ProcesoMeta> listarProinterno();

    public String insertarRelProInternoUser(String idProInt, int codUsuario, String login, String empresa);
    
    public boolean existeMetaProceso(String empresa, String descripcion, int idProceso);
    
    public boolean existeProcesoInterno(String procesoMeta, String descripcion, int idProceso);
    
    public ArrayList<ProcesoMeta> cargarProcesoInterno(int idMetaProceso, String status);
    
    public ArrayList<UnidadesNegocio> cargarUnidadesNegocio(int parseInt);
       
    public String anularMetaProceso(int idProceso);
     
    public String anularProcesoInterno(int idProinterno);
    
    public ArrayList<Usuario> listarUsuariosProinterno(int parseInt);
    
    public ArrayList<Usuario> listarUsuariosRelProInterno(int idProceso);
    
    public String eliminarUsuarioProinterno(int idProinterno, String idUsuario);
    
    public boolean existenUsuariosRelProceso(int idProceso, String tipo);
    
    public String activarMetaProceso(int idProceso);
    
    public String activarProcesoInterno(int idProceso);
    
    public String actualizarModerador(int idProceso, int idusuario, String moderador);
    
    public ArrayList<Usuario> listarUsuariosRelProInterno();
    
    public ArrayList<ProcesoMeta> cargarProcesosRelUsuario(String usuario);
    
    public ArrayList<ProcesoMeta> cargarTipoRequisicionRelUsuario(String usuario);
    
    public String eliminarRelProcesoReqUser(int idUsuario);
    
    public String insertarRelProcesoReqUser(String idProceso, int codUsuario, String login, Usuario usuario);
    
    public String eliminarRelTipoReqUser(int idUsuario);
    
    public String insertarRelTipoReqUser(String idTipoReq, int codUsuario, String login, Usuario usuario);
    
}
