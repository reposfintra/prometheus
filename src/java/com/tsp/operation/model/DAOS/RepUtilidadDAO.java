/*******************************************************************************
 *      Nombre Clase.................   RepUtilidadDAO.java
 *      Descripci�n..................   DAO del reporte de utilidad
 *      Autor........................   Ing. Tito Andr�s Maturana De La Cruz
 *      Fecha........................   22 de febrero de 2006, 08:29 AM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************************/

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.UtilFinanzas;

public class RepUtilidadDAO extends MainDAO {
    
    private ResultSet rs;
    private Vector vector;
    private RepUtilidad repUtil;
    private String nombretbl;
    
    /** Crea una nueva instancia de  RepUtilidadDAO */
    public RepUtilidadDAO() {
        super("RepUtilidadDAO.xml");
    }
    
    /**
     * Obtiene informacion espec�fica de las planillas despachadas en un per�odo determinado y de las remesas correspondientes
     * @autor Ing. Tito Andr�s Maturana D.
     * @param ano A�o del per�odo
     * @param mes Mes del per�odo
     * @param cia C�digo del distrito
     * @tipo 1- Carga General, 2 - Carb�n
     * @throws SQLException
     * @version 1.0.
     **/
    public void planillasPeriodo(String ano, String mes, String cia, String tipo) throws SQLException{
        PreparedStatement st = null;
        rs = null;
        
        try{
            
            String fechai = ano + "-" + mes + "-01 00:00:00";
            String fechaf = ano + "-" + mes + "-" + UtilFinanzas.diaFinal(ano, mes) + " 23:59:59";
            
            if( tipo.compareTo("1")==0 ){
                st = this.crearPreparedStatement("SQL_PLANILLAS_GRAL");                
            } else {
                st = this.crearPreparedStatement("SQL_PLANILLAS_CARBON");
            }
            
            st.setString(1, cia);
            st.setString(2, fechai);
            st.setString(3, fechaf);    
            
            //////System.out.println(".................. planillas: " + st);
            
            rs = st.executeQuery();
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DE LAS PLANILLAS DEL PERIODO " + e.getMessage() + " " + e.getErrorCode());
        } catch(Exception e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DE LAS PLANILLAS DEL PERIODO " + e.getMessage());
        } finally{
            //if (st != null) st.close();
            this.desconectar("SQL_PLANILLAS_GRAL");
            this.desconectar("SQL_PLANILLAS_CARBON");
        }
        
    }
    
    /**
     * Getter for property rs.
     * @return Value of property rs.
     */
    public java.sql.ResultSet getRs() {
        return rs;
    }    
    
    /**
     * Setter for property rs.
     * @param rs New value of property rs.
     */
    public void setRs(java.sql.ResultSet rs) {
        this.rs = rs;
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
    
    /**
     * Obtiene informacion espec�fica de las planillas perteneciente a una remesa
     * @autor Ing. Tito Andr�s Maturana D.
     * @param numrem N�mero de la remesa
     * @throws SQLException
     * @version 1.0.
     **/
    public void getPlanillas(String numrem) throws SQLException{
        PreparedStatement st = null;
        ResultSet rset = null;
        this.vector = new Vector();
        
        try {
            
            st = this.crearPreparedStatement("SQL_PLANILLAS_REMESA");
            
            st.setString(1, numrem);
            
            rset = st.executeQuery();
            
            while(rset.next()){
                RepUtilidad rep = new RepUtilidad();
                rep.Load(rset);
                this.vector.add(rep);
            }
            
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL REPORTE DE UTILIDAD ( REMESAS DE PLANILLA ) " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null) st.close();
            this.desconectar("SQL_PLANILLAS_REMESA");
        }
    }
    
    /**
     * Obtiene informacion espec�fica de las remesas perteneciente a una planilla
     * @autor Ing. Tito Andr�s Maturana D.
     * @param numpla N�mero de la planilla
     * @throws SQLException
     * @version 1.0.
     **/
    public void getRemesas(String numpla) throws SQLException{
        PreparedStatement st = null;
        ResultSet rset = null;
        this.vector = new Vector();
        
        try {
            
            st = this.crearPreparedStatement("SQL_REMESAS_PLANILLA");
            
            st.setString(1, numpla);
            
            //////System.out.println("..... consulta reporte utilidad planillas de la remesa : " + st.toString() );
            
            rset = st.executeQuery();
            
            while(rset.next()){
                RepUtilidad rep = new RepUtilidad();
                rep.Load(rset);
                this.vector.add(rep);
            }
            //////System.out.println("...................... PLANILLA: " + numpla + " - REMESAS ENCONTRADAS: " + vector.size());
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL REPORTE DE UTILIDAD ( PLANILLAS DE REMESA ) " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null) st.close();
            this.desconectar("SQL_REMESAS_PLANILLA");
        }
    }
    
    /**
     * Crea una tabla temporal para la generacion del reporte
     * @autor Ing. Tito Andr�s Maturana D.
     * @param tblname Terminacion del nombre de la tabla _AAAA_MM_DD
     * @cia C�digo del Distrito
     * @throws SQLException
     * @version 1.0.
     **/
    public void crearTabla(String tblname, String cia) throws SQLException{
        PreparedStatement st = null;
        ResultSet rset = null;
        
        this.nombretbl = tblname;
        Connection con = null;
        
        try{
            
            st = this.crearPreparedStatement("SQL_EXISTE_TABLA");
            st.setString(1, "reporte_utilidad" + tblname);            
            
            rset = st.executeQuery();
            
            if( !rset.next() ){
                con = this.conectar("SQL_CREAR_TABLA"); 
                st = con.prepareStatement(this.obtenerSQL("SQL_CREAR_TABLA").replaceAll("#SUFIJO#", this.nombretbl));
                //st = this.crearPreparedStatement("SQL_CREAR_TABLA");
                st.executeUpdate();
            }
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DE LA TABLA " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null) st.close();
            this.desconectar("SQL_EXISTE_TABLA");
            //this.desconectar("SQL_DELETE");
            this.desconectar("SQL_CREAR_TABLA");
        }
        
    }
    
    /**
     * Getter for property repUtil.
     * @return Value of property repUtil.
     */
    public com.tsp.operation.model.beans.RepUtilidad getRepUtil() {
        return repUtil;
    }
    
    /**
     * Setter for property repUtil.
     * @param repUtil New value of property repUtil.
     */
    public void setRepUtil(com.tsp.operation.model.beans.RepUtilidad repUtil) {
        this.repUtil = repUtil;
    }
    
    /**
     * Inserta un registro en la tabla temporal.
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @version 1.0.
     **/
    public void insertar() throws SQLException{
        PreparedStatement st = null;
        ResultSet rset = null;
        Connection con = null;
        
        try{
            con = this.conectar("SQL_INSERT"); 
            st = con.prepareStatement(this.obtenerSQL("SQL_INSERT").replaceAll("#SUFIJO#", this.nombretbl));
            
            st.setString(1, this.repUtil.getDstrct());                
            st.setString(2, this.repUtil.getPlanilla());                  
            st.setString(3, this.repUtil.getRemesa());                  
            st.setString(4, this.repUtil.getPeriodo()); 
            st.setString(5, this.repUtil.getPlaca());                    
            st.setString(6, this.repUtil.getAgencia());                  
            st.setString(7, this.repUtil.getOripla());                  
            st.setString(8, this.repUtil.getDespla());                  
            st.setDouble(9, this.repUtil.getVlrpla());                  
            st.setString(10, this.repUtil.getFecdsp());                  
            st.setDouble(11, this.repUtil.getUnidadesdsp());                  
            st.setString(12, this.repUtil.getVacio());                  
            st.setString(13, this.repUtil.getFecrem());                  
            st.setString(14, this.repUtil.getOrirem());                  
            st.setString(15, this.repUtil.getDesrem());                  
            st.setString(16, this.repUtil.getCliente());
            st.setDouble(17, this.repUtil.getVlrrem());
            st.setString(18, this.repUtil.getEstandar());
            st.setString(19, this.repUtil.getDescripcion());
            st.setString(20, this.repUtil.getPorcentaje());
            st.setString(21, this.repUtil.getUsuario()); 
            
            //////System.out.println("................. INSERT: " + st);
            st.executeUpdate();
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL REPORTE DE UTILIDAD " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null) st.close();
            this.desconectar("SQL_INSERT");
        }
        
    }
    
    
    /**
     * Verifica la existencia de un registro en la tabla temporal.
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @returns True si existe, false si no
     * @version 1.0.
     **/
    public boolean existe() throws SQLException{
        PreparedStatement st = null;
        ResultSet rset = null;
        Connection con = null;
        boolean sw = false;
        
        try{
            con = this.conectar("SQL_EXISTE"); 
            st = con.prepareStatement(this.obtenerSQL("SQL_EXISTE").replaceAll("#SUFIJO#", this.nombretbl));
            
            st.setString(1, this.repUtil.getDstrct());                
            st.setString(2, this.repUtil.getPlanilla());                  
            st.setString(3, this.repUtil.getRemesa());
            st.setString(4, this.repUtil.getPeriodo());
            //////System.out.println("...................... EXISTE: " + st);
            rset = st.executeQuery();
            
            if( rset.next() ){
                sw = true;
            }
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA EN EL REPORTE DE UTILIDAD " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null) st.close();
            this.desconectar("SQL_EXISTE");
        }
        
        return sw;
        
    }
    
    /**
     * Elimina la tabla temporal
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @version 1.0.
     **/
    public void dropTable() throws SQLException{
        PreparedStatement st = null;
        Connection con = null;
        
        try{
            con = this.conectar("SQL_DROP_TABLE"); 
            st = con.prepareStatement(this.obtenerSQL("SQL_DROP_TABLE").replaceAll("#SUFIJO#", this.nombretbl));
            
            st.executeUpdate();
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ELIMINICACION DE LA TABLA TEMPORAL DEL REPORTE DE UTILIDAD " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            if (st != null) st.close();
            this.desconectar("SQL_DROP_TABLE");
        }
        
    }
    
    /**
     * Lee la informacion del reporte de la tabla temporal
     * @autor Ing. Tito Andr�s Maturana D.
     * @throws SQLException
     * @version 1.0.
     **/
    public void leer(String cia, String periodo) throws SQLException{
        PreparedStatement st = null;
        rs = null;
        Connection con = null;
        
        try{
            con = this.conectar("SQL_REPORTE"); 
            st = con.prepareStatement(this.obtenerSQL("SQL_REPORTE").replaceAll("#SUFIJO#", this.nombretbl));
            
            st.setString(1, cia);
            st.setString(2, periodo);
            
            rs = st.executeQuery();
            
        } catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DEL REPORTE DE UTILIDAD " + e.getMessage() + " " + e.getErrorCode());
        } finally{
            //if (st != null) st.close();
            this.desconectar("SQL_REPORTE");
        }
        
    }
    
}
