/***********************************************************************************
 * Nombre clase : ............... CaravanaDAO.java                                   *
 * Descripcion :................. Clase que maneja los DAO ( Data Access Object )  *
 *                                los cuales contienen los metodos que interactuan *
 *                                con la BD.                                       *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 8 de Julio de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;

public class CaravanaDAO {
    
    private VistaCaravana  vc;
    private int numcaravana = 0;
    private Vector datos; //vecModificar;
    private Vector excluidos = new Vector ();
    private Vector caravanaActual = new Vector ();
    
    private Vector mostrar = new Vector ();
    private Caravana caravana;
    //MODIFICACION NUMESCOLTAS<>'0'
    
    private static final String SQL_LISTAR_DATOS     =
    "SELECT * FROM " +
    "(SELECT" +
    " distinct P.fecpla,                 " +
    "     P.plaveh,                      " +
    "     CA.numpla,                     " +
    "     CA.origen,                     " +
    "     CA.destino,                    " +
    "     CA.fecinicio,                   " +
    "     P.ruta_pla,                    " +
    "     PR.numrem,                     " +
    "     C.nomcli,                      " +
    "     C.codcli,                      " +
    "     P.oripla,                      " +
    "     N.nombre as nombre,            " +
    "     N.cedula,                      " +
    "     P.despla,                      " +
    "     CO.nomciu as orinom,           " +
    "     CD.nomciu as desnom,           " +
    "     S.std_job_desc,                " +
    "     V.via,                          " +
    "     R.std_job_no, " +
    "     CA.creation_date " +
    "FROM  " +
    "     caravana CA" +
    "     JOIN Planilla P on(P.numpla=CA.numpla)     " +
    "     LEFT JOIN VIA    V   on(substr(P.ruta_pla,5,  length( P.ruta_pla ) - 4 ) =  V.secuencia and substr(P.ruta_pla,3,2) = V.destino and substr(P.ruta_pla,1,2) = V.origen ) "+
    "     LEFT JOIN plarem PR  on(PR.numpla     =  P.numpla) " +
    "     LEFT JOIN remesa R   on(PR.numrem     =  R.numrem) " +
    "     LEFT JOIN cliente C  on(R.cliente     =  C.codcli) " +
    "     LEFT JOIN ciudad CO  on(CO.codciu     =  P.oripla) " +
    "     LEFT JOIN ciudad CD  on(CD.codciu     =  P.despla) " +
    "     LEFT JOIN Stdjob S   on(R.std_job_no  =  S.std_job_no) " +
    "     LEFT JOIN Nit    N   on(P.cedcon      =  N.cedula)    " +
    "WHERE     " +
    "     CA.numcaravana=? ) AS c1 UNION (" +
    "SELECT " +
    "       fecha_despacho AS fecpla, " +
    "       placa AS plaveh, " +
    "       CA.numpla, " +
    "       CA.origen, " +
    "       CA.destino, " +
    "       CA.fecinicio, " +
    "       ruta AS ruta_pla, " +
    "       ''," +
    "       get_nombrecliente(cliente) AS nomcli, " +
    "       cliente AS codcli, " +
    " 	    SUBSTRING(ruta FROM 1 FOR 2) AS oripla, " +
    "       COALESCE(get_nombrenit(conductor), 'NO REGISTRA') AS nombre, " +//AMATURANA 24.08.2006
    "       conductor AS cedula, " +
    "       SUBSTRING(ruta FROM 3 FOR 2) AS despla, " +
    "       get_nombreciudad(SUBSTRING(ruta FROM 1 FOR 2)) AS orinom, " +
    "       get_nombreciudad(SUBSTRING(ruta FROM 3 FOR 2)) AS desnom, " +
    "       'Est�ndar no definido por despacho manual', " +
    "       V.via, " +
    "       ''," +
    "       CA.creation_date " +
    "FROM   caravana CA " +
    "JOIN despacho_manual ON ( despacho_manual.numpla=CA.numpla ) " +
    "LEFT JOIN VIA V ON ( " +
    "       SUBSTRING(ruta FROM 5 FOR 4) =  V.secuencia " +
    "       AND SUBSTRING(ruta FROM 3 FOR 2) = V.destino " +
    "       AND SUBSTRING(ruta FROM 1 FOR 2) = V.origen ) " +
    "WHERE CA.numcaravana=? )" +
    "ORDER BY creation_date ";


private static final String SQL_VEHICULO_PLAMANUAL = 
            "SELECT " +
            "       fecha_despacho AS fecpla, " +
            "       placa AS plaveh, " +
            "       numpla, " +
            "       ruta AS ruta_pla, " +
            "       cliente AS codcli, " +
	    "       get_nombrecliente(cliente) AS nomcli, " +
	    " 	    SUBSTRING(ruta FROM 1 FOR 2) AS oripla, " +
            "       SUBSTRING(ruta FROM 3 FOR 2) AS despla, " +
            "       get_nombreciudad(SUBSTRING(ruta FROM 1 FOR 2)) AS orinom, " +
            "       get_nombreciudad(SUBSTRING(ruta FROM 3 FOR 2)) AS desnom, " +
            "       conductor AS cedula, " +
            "       COALESCE(get_nombrenit(conductor), 'NO REGISTRA') AS nombre, " +//AMATURANA 24.08.2006
            "       V.via " +
            "FROM despacho_manual " +
	    "LEFT JOIN VIA V ON (" +
            "       SUBSTRING(ruta FROM 5 FOR 4) =  V.secuencia " +
            "       AND SUBSTRING(ruta FROM 3 FOR 2) = V.destino " +
            "       AND SUBSTRING(ruta FROM 1 FOR 2) = V.origen ) " +
            "WHERE numpla = ?";
    
    private static final String SQL_LISTAR_DATOS_PMANUAL   =  
            "SELECT " +
            "       fecha_despacho AS fecpla, " +
            "       placa AS plaveh, " +
            "       despacho_manual.numpla, " +
            "       ruta AS ruta_pla, " +
            "       cliente AS codcli, " +
	    "       get_nombrecliente(cliente) AS nomcli, " +
	    " 	    SUBSTRING(ruta FROM 1 FOR 2) AS oripla, " +
            "       SUBSTRING(ruta FROM 3 FOR 2) AS despla, " +
            "       get_nombreciudad(SUBSTRING(ruta FROM 1 FOR 2)) AS orinom, " +
            "       get_nombreciudad(SUBSTRING(ruta FROM 3 FOR 2)) AS desnom, " +
            "       conductor AS cedula, " +
            "       COALESCE(get_nombrenit(conductor), 'NO REGISTRA') AS nombre, " +//AMATURANA 24.08.2006
            "       V.via, " +
            "       CA.origen, " +
            "       CA.destino, " +
            "       CA.fecinicio " +
            "FROM   caravana CA " +
            "JOIN despacho_manual ON ( despacho_manual.numpla=CA.numpla ) " +
	    "LEFT JOIN VIA V ON ( " +
            "       SUBSTRING(ruta FROM 5 FOR 4) =  V.secuencia " +
            "       AND SUBSTRING(ruta FROM 3 FOR 2) = V.destino " +
            "       AND SUBSTRING(ruta FROM 1 FOR 2) = V.origen ) " +
            "WHERE CA.numcaravana=?";	
    private static final String SQL_BUSCAR_PLANILLAS =  "SELECT  " +
    "     distinct P.fecpla,  " +
    "     P.plaveh, " +
    "     P.numpla,  " +
    "     V.via,    "+
    "     P.ruta_pla, " +
    "     PR.numrem, " +
    "     C.nomcli, " +
    "     C.codcli, " +
    "     P.oripla, " +
    "     N.nombre as nombre,  " +
    "     N.cedula,  " +
    "     P.despla, " +
    "     CO.nomciu as orinom, " +
    "     CD.nomciu as desnom, " +
    "     S.std_job_desc,R.std_job_no  " +
    "FROM  " +
    "     Planilla P                  " +
    "     LEFT JOIN plarem PR  on(PR.numpla     =  P.numpla)" +
    "     LEFT JOIN remesa R   on(R.numrem      =  PR.numrem )" +
    "     LEFT JOIN cliente C  on(C.codcli      =  R.cliente)" +
    "     LEFT JOIN ciudad CO  on(CO.codciu     =  P.oripla)" +
    "     LEFT JOIN ciudad CD  on(CD.codciu     =  P.despla)" +
    "     LEFT JOIN Stdjob S   on(S.std_job_no  =  R.std_job_no AND S.numescoltas<>0)                                                                         " +
    "     LEFT JOIN VIA    V   on(substr(P.ruta_pla,5,  length( P.ruta_pla ) - 4 ) =  V.secuencia and substr(P.ruta_pla,3,2) = V.destino and substr(P.ruta_pla,1,2) = V.origen )  "+
    "     LEFT JOIN Nit    N   on(N.cedula      =  P.cedcon)  " +
    "WHERE" +
    "     P.agcpla=? and  " +
    "     P.fecdsp between ? and ? and " +
    "     P.numpla not in   " +
    "     (SELECT  " +
    "            numpla  " +
    "      FROM  " +
    "            caravana  " +
    "      WHERE  " +
    "            razon_exclusion='' and  " +
    "            reg_status='')   ";
    
    private static String SQL_VEHIC_X_PLANILLA       =  " SELECT   " +
    "       distinct P.fecpla,              " +
    "       P.plaveh,                       " +
    "       P.numpla,                       " +
    "       P.ruta_pla,                     " +
    "       V.via,                          "+
    "       PR.numrem,                      " +
    "       C.nomcli,                       " +
    "       C.codcli,                       " +
    "       P.oripla,                       " +
    "       P.despla,                       " +
    "       CO.nomciu as orinom,            " +
    "       CD.nomciu as desnom,            " +
    "       S.std_job_desc,                 " +
    "       R.std_job_no,                   " +
    "       N.nombre as nombre,             " +
    "       N.cedula                        " +
    " FROM                                  " +
    "   Planilla P     " +
    "   LEFT JOIN plarem PR  on(PR.numpla     =  P.numpla)" +
    "   LEFT JOIN remesa R   on(PR.numrem     =  R.numrem)" +
    "   LEFT JOIN cliente C  on(R.cliente     =  C.codcli)" +
    "   LEFT JOIN ciudad CO  on(CO.codciu     =  P.oripla)" +
    "   LEFT JOIN ciudad CD  on(CD.codciu     =  P.despla)" +
    "   LEFT JOIN Stdjob S   on(R.std_job_no  =  S.std_job_no)" +
    "   LEFT JOIN Nit    N   on(P.cedcon      =  N.cedula)  " +
    "   LEFT JOIN VIA    V   on(substr(P.ruta_pla,5,  length( P.ruta_pla ) - 4 ) =  V.secuencia and substr(P.ruta_pla,3,2) = V.destino and substr(P.ruta_pla,1,2) = V.origen )  "+
    " WHERE " +
    "   P.numpla = ?  ";
    
    private static final String SQL_GET_NO_ESCOLTAS  = "Select numescolta from stdjob where escolta='SI' and std_job_no=?";
    
    private static final String SQL_NEXT_NO_CARAVANA = "Select max(numcaravana) as num from caravana";
    
    private static final String SQL_INSERT_CARAVANA  = "insert into caravana (dstrct, agencia, numcaravana, "+
    "numpla,numrem,fecinicio,creation_user,base, origen, destino) VALUES (?,?,?,?,?,?,?,?,?,?)";
    
    private static final String SQL_EXISTE_PLANILLA  = "select numpla from planilla where numpla=?";
    
    private static final String SQL_PLANI_ASIGNADA   = 
    "select numpla from caravana where numpla=? and reg_status='' and razon_exclusion='' and numcaravana!=0";
    
    private static final String SQL_FIN_CARAVANA     = "update caravana set fecfin='now()',reg_status='F' where numcaravana=? and razon_exclusion=''";
    
    /*private static final String SQL_LISTAR_DATOS     =  "SELECT" +
    " distinct P.fecpla,                 " +
    "     P.plaveh,                      " +
    "     CA.numpla,                     " +
    "     CA.origen,                     " +
    "     CA.destino,                    " +
    "     CA.fecinicio,                   " +
    "     P.ruta_pla,                    " +
    "     PR.numrem,                     " +
    "     C.nomcli,                      " +
    "     C.codcli,                      " +
    "     P.oripla,                      " +
    "     N.nombre as nombre,            " +
    "     N.cedula,                      " +
    "     P.despla,                      " +
    "     CO.nomciu as orinom,           " +
    "     CD.nomciu as desnom,           " +
    "     S.std_job_desc,                " +
    "     V.via,                          " +
    "     R.std_job_no " +
    "FROM  " +
    "     caravana CA" +
    "     LEFT JOIN Planilla P on(P.numpla=CA.numpla)     " +
    "     LEFT JOIN VIA    V   on(substr(P.ruta_pla,5,  length( P.ruta_pla ) - 4 ) =  V.secuencia and substr(P.ruta_pla,3,2) = V.destino and substr(P.ruta_pla,1,2) = V.origen ) "+
    "     LEFT JOIN plarem PR  on(PR.numpla     =  P.numpla) " +
    "     LEFT JOIN remesa R   on(PR.numrem     =  R.numrem) " +
    "     LEFT JOIN cliente C  on(R.cliente     =  C.codcli) " +
    "     LEFT JOIN ciudad CO  on(CO.codciu     =  P.oripla) " +
    "     LEFT JOIN ciudad CD  on(CD.codciu     =  P.despla) " +
    "     LEFT JOIN Stdjob S   on(R.std_job_no  =  S.std_job_no) " +
    "     LEFT JOIN Nit    N   on(P.cedcon      =  N.cedula)    " +
    "WHERE     " +
    "     CA.numcaravana=?";
    //"     CA.reg_status ='' ";*/
    
    
    private static final String SQL_NUMERO_CARAVANA  =  "select numcaravana from caravana where numpla=? and reg_status=''";
    
    private static final String SQL_ACT_PLANILLA     =  "update caravana set numcaravana=0, razon_exclusion=?, user_update=?, last_update='now()', " +
    "caravanaorig=? where numcaravana = ? and numpla = ?";
    
    private static final String SQL_BUS_FEC_CARAVANA =  "select distinct fecinicio,fecfin from caravana where numcaravana=?";
    
    private static final String SQL_INFO_CLIENTE     =  "SELECT                                                                       " +
    "    c.codcli,                                                                " +
    "    c.nomcli,                                                                " +
    "    a.nombre,                                                                " +
    "    s.numescoltas                                                            " +
    "FROM                                                                         " +
    "    Cliente c,                                                               " +
    "    agencia a,                                                               " +
    "    stdjob s                                                                 " +
    "WHERE                                                                        " +
    "   substring(c.codcli from 4 for 6) = substring (s.std_job_no from 1 for 3)  " +
    "AND c.agduenia   = a.id_agencia                                              " +
    "AND c.codcli     = ?                                                         " +
    "AND s.std_job_no = ?                                                         ";
    
    private static final String SQL_INFO_PLACA       =  "SELECT                          " +
    "    p.estado,                   " +
    "    p.placa,                    " +
    "    p.condicion,                " +
    "    p.tarjetaoper,              " +
    "    p.venctarjetaoper,          " +
    "    p.marca,                    " +
    "    p.clase,                    " +
    "    p.capacidad,                " +
    "    p.carroceria,               " +
    "    p.modelo,                   " +
    "    p.color,                    " +
    "    p.nomotor,                  " +
    "    p.nochasis,                 " +
    "    p.noejes,                   " +
    "    p.agencia,                  " +
    "    p.dimcarroceria,            " +
    "    p.venseguroobliga,          " +
    "    p.homologado,               " +
    "    p.atitulo,                  " +
    "    p.empresaafil,              " +
    "    p.propietario,              " +
    "    p.conductor,                " +
    "    p.tenedor,                  " +
    "    p.tipo,                     " +
    "    p.tara,                     " +
    "    p.largo,                    " +
    "    p.alto,                     " +
    "    p.llantas,                  " +
    "    p.enganche,                 " +
    "    p.ancho,                    " +
    "    p.piso,                     " +
    "    p.cargue,                   " +
    "    p.volumen,                  " +
    "    p.fechaultact,              " +
    "    p.usuario,                  " +
    "    p.recurso,                  " +
    "    p.grupoid,                  " +
    "    nit.nombre,                 " +
    "    p.grupo,                    " +
    "    p.estadoequipo,             " +
    "    p.localizacion              " +
    "FROM                            " +
    "    placa p,                    " +
    "    nit                         " +
    "WHERE placa = ?                 " +
    "AND p.propietario = nit.cedula  " ;
    
    private static final String SQL_LISTAR_CARAVANA  =  "Select numcaravana,fecinicio,fecfin,caravanaorig from caravana where numcaravana like ?  and numcaravana<>0";
    private static final String SQL_LIS_CAR_GENERADA =  "Select * from caravana where caravanaorigen<>0 and reg_status=''";
    private static final String SQL_LISTA_PLANILLAS  =  "Select numpla from caravana where numcaravana=? and numcaravana!=0";
    private static final String SQL_EXCLUIR_PLANILLA =  "Update caravana set numcaravana=0, caravanaorig=? where numcaravana=? and numpla=?";
    private static final String SQL_ANULAR_CARAVANA  =  "Update caravana set reg_status='A' where numcaravana=?";
    //JOSE 2006-05-08
    private static final String SQL_ORIGEN_DESTINO_CARAVANA = "UPDATE caravana SET origen = ?, destino = ? WHERE numpla = ?";
    //consultas para ingreso trafico
    
    
    /* ANDRES MATURANA DE LA CRUZ */
     private static final String SQL_PLANILLA_MANUAL = 
            "SELECT numpla " +
            "FROM despacho_manual " +
            "WHERE numpla = ? ";
    
   
    
    public CaravanaDAO () {
    }
    
    /**
     * Obtiene el numero de escoltas que permite un cliente para sus vehiculos en carretera
     * @param std_job_no el numero del stdjob de un cliente
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>0</tt> si no permite escoltas este cliente � un valor diferente de <tt>0</tt> si lo permite
     */
    
    public int getNumeroEscoltas (String std_job_no) throws SQLException {
        int num = 0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vc = new VistaCaravana ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (this.SQL_GET_NO_ESCOLTAS);
                st.setString (1, std_job_no);
                rs = st.executeQuery ();
                if (rs.next ()){
                    num  = rs.getInt (1);
                }
            }
            return num;
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR NUMERO DE ESCOLTAS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    
    /**
     * Busca una planilla dada en los registros almacenados para determinar si existe o no
     * @param numpla. El numero de la planilla
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>true</tt> si existe la planilla  � <tt>false</tt> si no existe
     */
    
    public boolean existePlanilla (String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_EXISTE_PLANILLA);
                st.setString (1, numpla);
                rs = st.executeQuery ();
                return rs.next ();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR NUMERO DE ESCOLTAS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    /**
     * Busca una si una planilla esta asignada a una caravana
     * @param numpla. El numero de la planilla
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>true</tt> si existe la planilla ya fue asignada a una caravana � <tt>false</tt> si no fue asignada
     */
    
    public boolean existePlanillaCaravana (String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_PLANI_ASIGNADA);
                st.setString (1, numpla);
                rs = st.executeQuery ();
                return rs.next ();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR NUMERO DE ESCOLTAS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    /**
     * Agrega una planilla y asigna escoltas a una caravana
     * @param Objeto VistaCaravana contiene los datos de la planilla a ser agregada a la caravana
     * @param Objeto Escolta contiene los datos del escolta a asignar a la caravana
     * @throws SQLException si existe un error en la conculta.
     */
    
    public void insertarCaravana ( Caravana c) throws SQLException {
        Connection con= null;
        PreparedStatement st  = null;
        PreparedStatement st2 = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                if (this.existePlanillaEnCaravana(c.getNumpla())){
                    st2 = con.prepareStatement ("UPDATE caravana set fecinicio=?,creation_user=?,origen=?,destino=? where numcaravana=? and numpla=? and numrem=?");
                    st2.setString (1, c.getFecinicio ());
                    st2.setString (2, c.getCreation_user ());
                    st2.setString (3, c.getOrigen ());
                    st2.setString (4, c.getDestino ());
                    st2.setInt    (5, c.getNumcaravana());
                    st2.setString (6, c.getNumpla ());
                    st2.setString (7, c.getNumrem ());                    
                    st2.execute ();
                } else {
                    st = con.prepareStatement (SQL_INSERT_CARAVANA);
                    st.setString (1, c.getDstrct ());
                    st.setString (2, c.getAgencia ());
                    st.setInt (3, c.getNumcaravana ());
                    st.setString (4, c.getNumpla ());
                    st.setString (5, c.getNumrem ());
                    st.setString (6, c.getFecinicio ());
                    st.setString (7, c.getCreation_user ());
                    st.setString (8, c.getBase ());
                    st.setString (9, c.getOrigen ());
                    st.setString (10, c.getDestino ());
                    st.execute ();
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL INSERTAR  CARAVANA" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    /**
     * Actualiza el campo caravana a S de la tabla ingreso trafico
     * @param String planilla
     * @throws SQLException si existe un error en la conculta.
     */
    
    public void actualizarCaravanaIngresoTrafico ( String planilla, String valor) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement ("update ingreso_trafico set caravana=? where planilla=?");
                st.setString (1, valor);
                st.setString (2, planilla);
                st.execute ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL ACTUALIZAR INGRESO TRAFICO" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    
    
    /**
     * Modifica una planilla asignada a una caravana
     * @param Objeto VistaCaravana contiene los datos de la planilla a ser modificada a la caravana
     * @throws SQLException si existe un error en la conculta.
     */
    
    public void modificarCaravana ( Caravana c) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_ACT_PLANILLA);
                st.setString (1, c.getRazon_exclusion ());
                st.setString (2, c.getUser_update ());
                st.setInt (3, c.getNumcaravana ());
                st.setInt (4, c.getNumcaravana ());
                st.setString (5, c.getNumpla ());
                st.execute ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL INSERTAR  CARAVANA" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    /**
     * Obtiene el numero siguiente para una nueva caravana
     * @param Ninguno
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>num</tt> donde este sera el nuevo numero de la caravana a crear
     */
    
    public int getNextNumeroCaravana () throws SQLException {
        int num = 0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_NEXT_NO_CARAVANA);
                rs = st.executeQuery ();
                if (rs.next ()){
                    num  = rs.getInt (1);
                }
                num = num + 1;
            }
            return num;
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR NUMERO DE ESCOLTAS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    /**
     * Selecciona los vehiculos despachados entre dos fechas y los almacena en un <tt>vector</tt>
     * @param fecini Fecha inicial de despachos
     * @param fecfin Fecha final de despachos
     * @throws SQLException si existe un error en la conculta.
     * @return un vector de objetos <tt>VistaCaravana</tt>
     */
    public void getVehiculosDespachados (String fecini, String fecfin, String ag) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        datos = new Vector ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_BUSCAR_PLANILLAS);
                st.setString (1, ag);
                st.setString (2, fecini);
                st.setString (3, fecfin);
                rs = st.executeQuery ();
                ResultSet m = rs;
                while (rs.next ()){
                    VistaCaravana v = new VistaCaravana ();
                    v.setPlaveh (rs.getString ("plaveh"));
                    v.setNumpla (rs.getString ("numpla"));
                    v.setNumrem (rs.getString ("numrem"));
                    v.setNomcli (rs.getString ("nomcli"));
                    v.setCodcli (rs.getString ("codcli"));
                    v.setOripla (rs.getString ("oripla"));
                    v.setDespla (rs.getString ("despla"));
                    v.setOrinom (rs.getString ("orinom"));
                    v.setDesnom (rs.getString ("desnom"));
                    v.setNomConductor (rs.getString ("nombre"));
                    v.setCedConductor (rs.getString ("cedula"));
                    v.setStd_job_desc (rs.getString ("std_job_desc"));
                    v.setStd_job_no (rs.getString ("std_job_no"));
                    v.setFecpla (rs.getString ("fecpla"));
                    v.setRuta_pla (rs.getString ("ruta_pla"));
                    v.setVia (rs.getString ("via"));
                    v.setSeleccionado (false);
                    datos.addElement (v);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR DESPACHOS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    /**
     * Selecciona los vehiculos despachados entre dos fechas y los almacena en un <tt>vector</tt>
     * @param fecini Fecha inicial de despachos
     * @param fecfin Fecha final de despachos
     * @throws SQLException si existe un error en la conculta.
     * @return un vector de objetos <tt>VistaCaravana</tt>
     */
    public void getVehiculosCaravana (String numcaravana) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        caravanaActual = new Vector ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_LISTAR_DATOS);
                st.setString (1, numcaravana);
                st.setString (2, numcaravana);//AMATURANA
                ////System.out.println(".......... VEHIVULOS CARAVANA: " + st.toString());
                rs = st.executeQuery ();
                // ResultSet m = rs;
                while (rs.next ()){
                    VistaCaravana v = new VistaCaravana ();
                    v.setPlaveh (rs.getString ("plaveh"));
                    v.setNumpla (rs.getString ("numpla"));
                    v.setNumrem (rs.getString ("numrem"));
                    v.setNomcli (rs.getString ("nomcli"));
                    v.setCodcli (rs.getString ("codcli"));
                    v.setOripla (rs.getString ("oripla"));
                    v.setDespla (rs.getString ("despla"));
                    v.setOrinom (rs.getString ("orinom"));
                    v.setDesnom (rs.getString ("desnom"));
                    v.setNomConductor (rs.getString ("nombre"));
                    v.setCedConductor (rs.getString ("cedula"));
                    v.setStd_job_desc (rs.getString ("std_job_desc"));
                    v.setStd_job_no (rs.getString ("std_job_no"));
                    v.setFecpla (rs.getString ("fecpla"));
                    v.setRuta_pla (rs.getString ("ruta_pla"));
                    v.setOrigen (rs.getString ("origen"));
                    v.setDestino (rs.getString ("destino"));
                    v.setVia (rs.getString ("via"));
                    v.setFecinicio(rs.getString ("fecinicio"));
                    v.setSeleccionado (false);
                    caravanaActual.addElement (v);
                }

		st.close();
                rs.close();
                
                /* ANDRES MATURANA - DATOS PLANILLAS MANUALES */
                /*st = con.prepareStatement (SQL_LISTAR_DATOS_PMANUAL);
                st.setString (1, numcaravana);
                rs = st.executeQuery ();
                // ResultSet m = rs;
                while (rs.next ()){
                    VistaCaravana v = new VistaCaravana ();
                    v.setPlaveh (rs.getString ("plaveh"));
                    v.setNumpla (rs.getString ("numpla"));
                    v.setNumrem ("");
                    v.setNomcli (rs.getString ("nomcli"));
                    v.setCodcli (rs.getString ("codcli"));
                    v.setOripla (rs.getString ("oripla"));
                    v.setDespla (rs.getString ("despla"));
                    v.setOrinom (rs.getString ("orinom"));
                    v.setDesnom (rs.getString ("desnom"));
                    v.setNomConductor (rs.getString ("nombre"));
                    v.setCedConductor (rs.getString ("cedula"));
                    v.setStd_job_desc ("Est�ndar no definido por despacho manual.");
                    v.setStd_job_no ("");
                    v.setFecpla (rs.getString ("fecpla").substring(0, 10));
                    v.setRuta_pla (rs.getString ("ruta_pla"));
                    v.setOrigen (rs.getString ("origen"));
                    v.setDestino (rs.getString ("destino"));
                    v.setVia (rs.getString ("via"));
                    v.setFecinicio(rs.getString ("fecinicio"));
                    v.setSeleccionado (false);
                    caravanaActual.addElement (v);
                }*/
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR DESPACHOS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    /**
     * Busca los datos de un vehiculo filtrados por el numero de la planilla
     * @param numpla. Numero de la planilla
     * @param fecfin Fecha final de despachos
     * @throws SQLException si existe un error en la conculta.
     * @return un vector de objetos <tt>VistaCaravana</tt>
     */
    public void getVehiculosXPlanilla (String numPla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vc = new VistaCaravana ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_VEHIC_X_PLANILLA);
                st.setString (1, numPla);
                rs = st.executeQuery ();
                if (rs.next ()){
                    vc.setPlaveh (rs.getString ("plaveh"));
                    vc.setNumpla (rs.getString ("numpla"));
                    vc.setNumrem (rs.getString ("numrem"));
                    vc.setNomcli (rs.getString ("nomcli"));
                    vc.setCodcli (rs.getString ("codcli"));
                    vc.setOripla (rs.getString ("oripla"));
                    vc.setDespla (rs.getString ("despla"));
                    vc.setOrinom (rs.getString ("orinom"));
                    vc.setDesnom (rs.getString ("desnom"));
                    vc.setNomConductor (rs.getString ("nombre"));
                    vc.setCedConductor (rs.getString ("cedula"));
                    vc.setStd_job_desc (rs.getString ("std_job_desc"));
                    vc.setStd_job_no (rs.getString ("std_job_no"));
                    vc.setFecpla (rs.getString ("fecpla"));
                    vc.setRuta_pla (rs.getString ("ruta_pla"));
                    vc.setVia (rs.getString ("via"));
                    vc.setSeleccionado (true);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR DESPACHOS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    public VistaCaravana getVehiculoPlanilla (){
        return vc;
    }
    public Vector getVectorVehiculos (){
        return datos;
    }
  /*  public void eliminarPosicionVectorModificarCaravana(int pos){
        vecModificar.remove(pos);
    }*/
    /**
     * Modifica una posici�n especifica del vector de vehiculos que se genero en la busqueda,
     * @param pos. Posici�n a modificar
     * @param val Valor a guardar en la posici�n especificada
     */
    public void modificarVectorVehiculos (int pos,boolean val){
        vc = (VistaCaravana) datos.elementAt (pos);
        vc.setSeleccionado (val);
        datos.set (pos, vc);
    }
    /*public void modificarVectorModifica(int pos,boolean val){
        vc = (VistaCaravana) vecModificar.elementAt(pos);
        vc.setSeleccionado(val);
        vecModificar.set(pos, vc);
    }*/
    public Vector getVectorCaravanaActual (){
        return caravanaActual;
    }
    public void agregarVectorCaravanaActual (VistaCaravana vc){
        caravanaActual.addElement (vc);
    }
    public void eliminarPosicionVectorCaravanaActual (int pos){
        caravanaActual.remove (pos);
    }
    /**
     * Busca un vehiculo en el vector y retorna un valor de acuerdo a la busqueda
     * @param vista. Objeto vista que contiene la informacion del vehiculo
     * @return true si existe el objeto o false sino existe
     */
    public boolean existeVehiculoCaravana (VistaCaravana vista) {
        for (int i=0; i<caravanaActual.size (); i++){
            if (vista.getNumpla ().equals (((VistaCaravana) caravanaActual.elementAt (i)).getNumpla ()))
                return true;
        }
        return false;
    }
    /**
     * Actualiza las fechas de finlaizacion de la caravana
     * @param numcaravana. Numero de la caravana a finalizar
     * @throws SQLException si existe un error en la conculta.
     */
    public void finalizarCaravana (int numcaravana) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        datos = new Vector ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_FIN_CARAVANA);
                st.setInt (1, numcaravana);
                st.execute ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL FINALIZAR CARAVANA" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    public String getNumeroCaravanaXPlanilla (String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_NUMERO_CARAVANA);
                st.setString (1, numpla);
                rs = st.executeQuery ();
                if (rs.next ())
                    return rs.getString ("numcaravana");
            }
            return "0";
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR NUMERO DE ESCOLTAS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    public Vector getVectorExcluidos (){
        return excluidos;
    }
    //modificado 31-08-2002
    public void agregarVectorExcluidos (VistaCaravana vc){
        if (!vc.isSeleccionado ())
            excluidos.addElement (vc);
    }
    public void eliminarPosicionVectorExcluidos (int pos){
        excluidos.remove (pos);
    }
    public void setNumCaravana (int num){
        numcaravana = num;
    }
    public int getNumCaravana (){
        return numcaravana;
    }
    public void setRazonVectorExcluido (int pos, String razon){
        VistaCaravana vi = (VistaCaravana) excluidos.elementAt (pos);
        vi.setRazon_exclusion (razon);
        eliminarPosicionVectorExcluidos (pos);
        excluidos.add (pos, vi);
    }
    public void limpiarVectorActualPlanillas (){
        caravanaActual.removeAllElements();
    }
    public void limpiarVectorPlanillasExcluidas (){
        excluidos.removeAllElements ();
    }
    public void borrarPosicionMayorUnoVectorActualPlanillas (){
        VistaCaravana vca = (VistaCaravana)caravanaActual.elementAt (0);
        caravanaActual.removeAllElements ();
        caravanaActual.addElement (vca);
    }
    public Vector getInfoClienteCaravana (String codcli, String std_job_no) throws SQLException {
        int num = 0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector cliente = new Vector ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_INFO_CLIENTE);
                st.setString (1, codcli);
                st.setString (2, std_job_no);
                rs = st.executeQuery ();
                if (rs.next ()){
                    cliente.addElement (rs.getString (1));
                    cliente.addElement (rs.getString (2));
                    cliente.addElement (rs.getString (3));
                    cliente.addElement (rs.getString (4));
                }
            }
            return cliente;
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR INFO LCIENTE" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    //HOYYYYYYYYYy
    public void getVectorCaravanas (String car, boolean fin ) throws SQLException {
        int num = 0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        mostrar = null;
        mostrar = new Vector ();
        caravana = null;
        String resto = " and reg_status!='F' and fecfin='0099-01-01'  group by 1,2,3,4";
        if (fin==true){
            resto = " and  reg_status='F' and fecfin<>'0099-01-01'  group by 1,2,3,4";
        }
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_LISTAR_CARAVANA+resto);
                st.setString (1, car+"%");
                rs = st.executeQuery ();
                while (rs.next ()){
                    caravana = new Caravana ();
                    caravana.setNumcaravana (rs.getInt ("numcaravana"));
                    caravana.setFecinicio (rs.getString ("fecinicio"));
                    caravana.setFecfin (rs.getString ("caravanaorig"));
                    caravana.setFecfin (rs.getString ("fecfin"));
                    mostrar.addElement (caravana);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR INFO LCIENTE" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    public void getVectorCaravanasGeneradas () throws SQLException {
        int num = 0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        mostrar = null;
        mostrar = new Vector ();
        caravana = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_LIS_CAR_GENERADA);
                rs = st.executeQuery ();
                while (rs.next ()){
                    caravana = new Caravana ();
                    caravana.setNumcaravana (rs.getInt ("numcaravana"));
                    caravana.setFecinicio (rs.getString ("fecinicio"));
                    caravana.setFecfin (rs.getString ("caravanaorig"));
                    caravana.setFecfin (rs.getString ("fecfin"));
                    mostrar.addElement (caravana);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR INFO LCIENTE" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    public Vector getVectorPlanillasCaravana (int numCar) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector plani = new Vector ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_LISTA_PLANILLAS);
                st.setInt (1, numCar);
                rs = st.executeQuery ();
                while (rs.next ()){
                    plani.addElement (rs.getString ("numpla"));
                }
            }
            return plani;
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR PLANILLAS DE UNA CARAVANA" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
    public Vector getVectorListarCaravana (){
        return mostrar;
    }
    public boolean validarOrigenDestino (String ori, String des){
        for (int i=0; i<caravanaActual.size (); i++){
            VistaCaravana vist = (VistaCaravana) caravanaActual.elementAt (i);
            if (!vist.getOripla ().equals (ori) || !vist.getDespla ().equals (des))
                return false;
        }
        return true;
    }
    public String getFechaInicioCaravana (int car) throws SQLException {
        int num = 0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String fecha = "";
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_BUS_FEC_CARAVANA);
                st.setInt (1,car);
                rs = st.executeQuery ();
                if (rs.next ()){
                    return rs.getString ("fecinicio");
                }
            }
            return fecha;
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR FECHA CARVANA" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    public String getFechaFinCaravana (int car) throws SQLException {
        int num = 0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String fecha = "";
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_BUS_FEC_CARAVANA);
                st.setInt (1,car);
                rs = st.executeQuery ();
                if (rs.next ()){
                    return rs.getString ("fecfin");
                }
            }
            return fecha;
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR FECHA CARVANA" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    /**
     * Obtiene el numero de escoltas que permite un cliente para sus vehiculos en carretera
     * @param std_job_no el numero del stdjob de un cliente
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>0</tt> si no permite escoltas este cliente � un valor diferente de <tt>0</tt> si lo permite
     */
    
    public void anularCaravana (int numcaravana) throws SQLException {
        int num = 0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vc = new VistaCaravana ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (this.SQL_ANULAR_CARAVANA);
                st.setInt (1, numcaravana);
                st.execute ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL ANULAR CARAVANA" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    
    /**
     * Metodo: modificarDestinoOrigenCaravana, permite modificar el origen y el destino de una caravana con el numero de la planilla
     * @autor : Ing. Jose de la rosa
     * @param : numero de planillla, el codigo de origen y el codigo de destino de la caravana
     * @version : 1.0
     */    
    public void modificarDestinoOrigenCaravana (String numpla, String origen, String destino) throws SQLException {
        int num = 0;
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector cliente = new Vector ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_ORIGEN_DESTINO_CARAVANA);
                st.setString (1, origen);
                st.setString (2, destino);
                st.setString (3, numpla);
                st.execute ();
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL MODIFICAR EL ORIGEN Y EL DESTINO DE LA CARAVANA " + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }    
    
    
    /***********************************************
     * METODOS POR ANDRES MATURANA DE LA CRUZ
     ***********************************************/
    /**
     * Busca una planilla manual dada en los registros almacenados para determinar si existe o no
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla. El numero de la planilla
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>true</tt> si existe la planilla  � <tt>false</tt> si no existe
     */
    
    public boolean existePlanillaManual (String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_PLANILLA_MANUAL);
                st.setString (1, numpla);
                rs = st.executeQuery ();
                return rs.next ();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR LA PLANILLA MANUAL" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    
    /**
     * Busca los datos de un vehiculo filtrados por el numero de la planilla manual
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param numpla. Numero de la planilla
     * @param fecfin Fecha final de despachos
     * @throws SQLException si existe un error en la conculta.
     * @return un vector de objetos <tt>VistaCaravana</tt>
     */
    public void getVehiculoXPlanillaManual (String numPla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        vc = new VistaCaravana ();
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement (SQL_VEHICULO_PLAMANUAL);
                st.setString (1, numPla);
                rs = st.executeQuery ();
                if (rs.next ()){
                    vc.setPlaveh (rs.getString ("plaveh"));
                    vc.setNumpla (rs.getString ("numpla"));
                    vc.setNumrem ("");
                    vc.setNomcli (rs.getString ("nomcli"));
                    vc.setCodcli (rs.getString ("codcli"));
                    vc.setOripla (rs.getString ("oripla"));
                    vc.setDespla (rs.getString ("despla"));
                    vc.setOrinom (rs.getString ("orinom"));
                    vc.setDesnom (rs.getString ("desnom"));
                    vc.setNomConductor (rs.getString ("nombre"));
                    vc.setCedConductor (rs.getString ("cedula"));
                    vc.setStd_job_desc ("Est�ndar no definido por despacho manual.");
                    vc.setStd_job_no ("");
                    vc.setFecpla (rs.getString ("fecpla"));
                    vc.setFecpla(vc.getFecpla().length()>=10 ? vc.getFecpla().substring(0, 10) : vc.getFecpla());
                    vc.setRuta_pla (rs.getString ("ruta_pla"));
                    vc.setVia (rs.getString ("via"));
                    vc.setSeleccionado (true);
                }
            }
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR DATOS VEHICULO PLANILLA MANUAL " + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
    }
     /**
     * Busca una si una planilla esta asignada a una caravana
     * @param numpla. El numero de la planilla
     * @throws SQLException si existe un error en la conculta.
     * @return <tt>true</tt> si existe la planilla ya fue asignada a una caravana � <tt>false</tt> si no fue asignada
     */
    
    public boolean existePlanillaEnCaravana (String numpla) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ("fintra");
            if (con != null){
                st = con.prepareStatement ("select numpla from caravana where numpla=? and reg_status='' and razon_exclusion=''");
                st.setString (1, numpla);
                rs = st.executeQuery ();
                return rs.next ();
            }
            return false;
        }catch(SQLException e){
            throw new SQLException ("ERROR AL BUSCAR NUMERO DE ESCOLTAS" + e.getMessage ()+"" + e.getErrorCode ());
        }finally{
            if(st != null){
                try{
                    st.close ();
                }
                catch(SQLException e){
                    throw new SQLException ("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null){
                poolManager.freeConnection ("fintra", con );
            }
        }
        
    }
    
}
