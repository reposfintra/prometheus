/*
* Nombre        GrupoEquipoDAO.java
* Descripci�n   Clase para el acceso a los datos de grupo_equipo
* Autor         Alejandro Payares
* Fecha         24 de noviembre de 2005, 11:14 AM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.DAOS;

import java.util.LinkedList;
import com.tsp.operation.model.beans.GrupoEquipo;

import java.sql.*;

/**
 * Clase para el acceso a los datos de grupo_equipo
 * @author Alejandro Payares
 */
public class GrupoEquipoDAO extends MainDAO {
    
    /** Creates a new instance of GrupoEquipoDAO */
    public GrupoEquipoDAO() {
        super("GrupoEquipoDAO.xml");
    }
    
    /**
     * Carga los grupos de equipos en memoria.
     * @return Lista con los grupos de equipos.
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     * @author Alejandro Payares
     */
    public LinkedList getGruposEquipos() throws SQLException {
        Connection con = null;
        PreparedStatement stmt  = null;
        ResultSet rs    = null;
        LinkedList gruposEquiposList = new LinkedList();
        String query = "SQL_GRUPOS_EQUIPOS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            stmt = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs   = stmt.executeQuery();
            while( rs.next() ){
                gruposEquiposList.add( GrupoEquipo.load(rs) );
            }
        }}
        finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (stmt  != null){ try{ stmt.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return gruposEquiposList;
    }
}
