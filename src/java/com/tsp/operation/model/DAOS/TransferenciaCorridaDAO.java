/***************************************
 * Nombre Clase ............. TransferenciaCorridaDAO.java
 * Descripci�n  .. . . . . .  Genera transferencia de corridas para bancos 
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  02/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/




package com.tsp.operation.model.DAOS;




import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.TasaService;
import com.tsp.util.*;



public class TransferenciaCorridaDAO extends MainDAO{
    
    
    
    
    public  String OFICINA_PPAL          = "OP";     
    public  String TIPO_FACTURA          = "F";    
    public  String TIPO_CHEQUE           = "C";    
    private String  TABLA_CODIGO_BANCOS  = "BANCOLOMBI";  
    private String  CONCEPTO             = "TR";
    private String  TIPO_DOC             = "004";
     
    
    
    public TransferenciaCorridaDAO() {
        super("TransferenciaCorridaDAO.xml");
    }
    public TransferenciaCorridaDAO(String dataBaseName) {
        super("TransferenciaCorridaDAO.xml", dataBaseName);
    }
    
    
    
    
    
    
     /**
     * M�todos que setea valores nulos
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
       private String reset(String val){
            if(val==null)
               val = "";
            return val;
       }
      
    
      
      /**
       * M�todo que devuelve la agencia real para la consulta
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      private String getAgencia(String agencia){
           String newAgencia  =   ( agencia.equals( this.OFICINA_PPAL ) )?" like '%' " : " ='" + agencia + "'";
           return newAgencia;
      }
      
      
      
      
      
      
       /**
       * M�todo que verifica la existencia del cheque
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  boolean  existeCheque(String distrito, String banco, String sucursal, String cheque) throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          String query = "SQL_EXISTE_CHEQUE";
          boolean estado = false;
          try {
              con = this.conectarJNDI(query);
              if (con != null) {
                  st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                  st.setString(1, distrito);
                  st.setString(2, banco);
                  st.setString(3, sucursal);
                  st.setString(4, cheque);
                  rs = st.executeQuery();
                  if (rs.next()) {
                      estado = true;
                  }

              }
          } catch (Exception e){
                  throw new Exception( "existeCheque " + e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
            return estado; 
      }
      
      
      
      
      
    
      /**
       * M�todo que busca las distintas corridas que tengan facturas aprobadas para pago por transferencia, 
         teniendo en cuenta distrito y agencia del usuario.
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getCorridasConFacturasPago(String distrito, String agencia) throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          List lista = new LinkedList();
          String query = "SQL_CORRIDAS_CON_FACTURAS_APROBADAS_TRANSFERENCIA";
          try {

              con = this.conectarJNDI(query);//JJCastro fase2
              if (con != null) {
                  String newAgencia = getAgencia(agencia);
                  String sql = this.obtenerSQL(query).replaceAll("#AGENCIA#", newAgencia);
                  st = con.prepareStatement(sql);
                  st.setString(1, distrito);

                  rs = st.executeQuery();
                  while (rs.next()) {
                      String corrida = rs.getString("corrida");
                      lista.add(corrida);
                  }

              }
          }catch(Exception e){
                  throw new Exception( "getCorridasConFacturasPago " + e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
            return lista; 
      }
      
      
      
      
      
       /**
       * M�todo que busca los bancos de la  corridas que tengan facturas aprobadas para pago  por transferencia, 
         teniendo en cuenta distrito y agencia del usuario.
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getBancosConFacturasPago(String distrito, String corrida, String agencia) throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          List lista = new LinkedList();
          String query = "SQL_BANCOS_CON_FACTURAS_APROBADAS_TRANSFERENCIA";
          try {

              con = this.conectarJNDI(query);//JJCastro fase2
              if (con != null) {
                  String newAgencia = getAgencia(agencia);
                  String sql = this.obtenerSQL(query).replaceAll("#AGENCIA#", newAgencia);
                  st = con.prepareStatement(sql);
                  st.setString(1, distrito);
                  st.setString(2, corrida);

                  rs = st.executeQuery();
                  while (rs.next()) {
                      String banco = rs.getString("banco");
                      lista.add(banco);
                  }

              }
          }catch(Exception e){
                  throw new Exception( "getBancosConFacturasPago " + e.getMessage());
          }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
         }
            return lista; 
      }
      
      
      
      
      
       /**
       * M�todo que busca las sucursales de los bancos de la  corridas que tengan facturas aprobadas para pago  por transferencia, 
         teniendo en cuenta distrito y agencia del usuario.
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getSucursalesBancosConFacturasPago(String distrito, String corrida, String banco, String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_SUCURSALES_BANCOS_CON_FACTURAS_APROBADAS_TRANSFERENCIA";
            try{
                
                con = this.conectarJNDI(query);//JJCastro fase2
                if(con!=null){
                String newAgencia  =   getAgencia(agencia);
                String sql         =   this.obtenerSQL( query ).replaceAll("#AGENCIA#", newAgencia );
                st                 =   con.prepareStatement( sql );
                st.setString(1, distrito   );
                st.setString(2, corrida    );
                st.setString(3, banco      );
                 
                rs=st.executeQuery();                
                while(rs.next()){
                    String sucursal =  rs.getString("sucursal");
                    lista.add( sucursal );
                }
                
                }}catch(Exception e){
                  throw new Exception( " getSucursalesBancosConFacturasPago " + e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           }
            return lista; 
      }
      
      
      
      
      
      
      
      
      /**
       *  M�todo que busca los distintos beneficiarios a los cuales se le han aprobado para pago  por transferencia
       *  las facturas dentro de una corrida. Tener en cuenta distrito y agencia del usuario.
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getBeneficiariosFacturasCorrida(String distrito, String corrida, String banco, String sucursal, String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_BENEFICIARIOS_FACTURA_CORRIDA_TRANSFERENCIA";
            ChequeXFacturaDAO  dao     = new ChequeXFacturaDAO();
            try{
                
                con = this.conectarJNDI(query);//JJCastro fase2
              if (con != null) {
                  String newAgencia = getAgencia(agencia);
                  String sql = this.obtenerSQL(query).replaceAll("#AGENCIA#", newAgencia);
                  st = con.prepareStatement(sql);
                  st.setString(1, distrito);
                  st.setString(2, corrida);
                  st.setString(3, banco);
                  st.setString(4, sucursal);

                  rs = st.executeQuery();
                  while (rs.next()) {
                      Hashtable datoProveedor = new Hashtable();
                      datoProveedor.put("nit", reset(rs.getString("nit")));
                      datoProveedor.put("nombre", reset(rs.getString("nombre")));
                      lista.add(datoProveedor);
                      datoProveedor = null;//Liberar Espacio JJCastro
                  }

              }
          }catch(Exception e){
                  throw new Exception( " getBeneficiariosFacturasCorrida " +e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           }
            return lista; 
      }
      
      
      
      
      /**
       *  M�todo que busca las facturas aprobadas para el beneficiario dentro de la corrida
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getFacturasAprobadasBeneficiarios(String distrito, String corrida, String banco, String sucursal, String proveedor, String agencia) throws Exception{
            Connection         con     = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String query = "SQL_FACTURAS_APROBADAS_PROVEEDOR_CORRIDA_TRANSFERENCIA";
          try {

              //ArchivoMovimientoDAO dao = new ArchivoMovimientoDAO();

              con = this.conectarJNDI(query);//JJCastro fase2
              if (con != null) {
                  String newAgencia = getAgencia(agencia);
                  String sql = this.obtenerSQL(query).replaceAll("#AGENCIA#", newAgencia);
                  st = con.prepareStatement(sql);
                  st.setString(1, distrito);
                  st.setString(2, corrida);
                  st.setString(3, banco);
                  st.setString(4, sucursal);
                  st.setString(5, proveedor);
                  rs = st.executeQuery();
                  while (rs.next()) {
                      FacturasCheques factura = this.load(rs);

                      if (this.isValida(factura)) {
                          factura.setCodeBancoTransf(this.searchCodeBanco(factura.getBanco_transfer(), factura.getBanco()));//Mod TMOLINA 28-08-2008
                          lista.add(factura);
                      }

                  }

              }
          }catch(Exception e){
                  throw new Exception( " getFacturasAprobadasBeneficiarios " + e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           }
            return lista; 
      }
      
    
    
      
      
      
      
      
      /**
       *  M�todo que busca detalle de la transferencia
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getDetalleTransferencia(String distrito, String banco, String sucursal, String transferencia) throws Exception{
          Connection con = null;
          PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_DETALLE_TRANSFERENCIA";
            try{
                
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito         );
                st.setString(2, banco            );
                st.setString(3, sucursal         );
                st.setString(4, transferencia    );
                rs = st.executeQuery(); 
                while(rs.next()){
                      Hashtable  info =  new Hashtable(); 
                            info.put("dstrct",          reset(  rs.getString("dstrct"))              );
                            info.put("banco_transfer",  reset(  rs.getString("banco_transfer"))      );
                            info.put("suc_transfer",    reset(  rs.getString("suc_transfer"))        );
                            info.put("transferencia",   reset(  rs.getString("transferencia"))       );
                            info.put("cheque",          reset(  rs.getString("cheque"))              );
                            info.put("banco_cuenta",    reset(  rs.getString("banco_cuenta"))        );
                            info.put("tipo_cuenta",     reset(  rs.getString("tipo_cuenta"))         );
                            info.put("no_cuenta",       reset(  rs.getString("no_cuenta"))           );
                            info.put("cedula_cuenta",   reset(  rs.getString("cedula_cuenta"))       );
                            info.put("nombre_cuenta",   reset(  rs.getString("nombre_cuenta"))       );
                            info.put("valor",           reset(  rs.getString("valor"))               );
                    
                     lista.add( info );
                }
                
                }}catch(Exception e){
                  throw new Exception( " getDetalleTransferencia " + e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
           }
            return lista; 
      }
      
      
      
      /**
       *  M�todo que busca facturas
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      
      
      
      public  List getFacturas(
                                 String dstrct,       String corrida , String transferencia   , String beneficiario, String banco        , String sucursal,     String banco_transfer,
                                 String suc_transfer, String tipo_cuenta, String no_cuenta   , String cedula_cuenta, String nombre_cuenta
                              ) throws Exception{
                                  
          Connection con = null;
          PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_FACTURAS_TRANSFERENCIA";
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,   dstrct           );
                st.setString(2,   corrida          );
                st.setString(3,   transferencia    );
                st.setString(4,   beneficiario     );
                st.setString(5,   banco            );
                st.setString(6,   sucursal         );
                st.setString(7,   banco_transfer   );
                st.setString(8,   suc_transfer     );
                st.setString(9,   tipo_cuenta      );                
                st.setString(10,  no_cuenta        );
                st.setString(11,  cedula_cuenta    );
                st.setString(12,  nombre_cuenta    );
                rs = st.executeQuery(); 
                while(rs.next()){
                    FacturasCheques  factura  = new FacturasCheques();
                    
                     factura.setDstrct            (  reset( rs.getString("dstrct")          ) );
                     factura.setCorrida           (  reset( rs.getString("corrida")         ) );
                     factura.setTipo_documento    (  reset( rs.getString("tipo_documento")  ) );                     
                     factura.setDocumento         (  reset( rs.getString("documento")       ) );
                     factura.setDescripcion       (  reset( rs.getString("descripcion")     ) );                
                     factura.setProveedor         (  reset( rs.getString("beneficiario")    ) );
                     factura.setVlr_saldo         (         rs.getDouble("valor")             );
                     factura.setVlrFactura        (         rs.getDouble("vlrBruto")          );
                     factura.setVlrRetencion      (         rs.getDouble("vlrImpuestos")      );
                     
                    
                lista.add(factura);
                factura = null;//Liberar Espacio JJCastro
                }
                
                
                }}catch(Exception e){
                  throw new Exception( " getFacturas " + e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
            return lista; 
      }
      
      
      
      
      
       /**
       * M�todo que busca la moneda del banco
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  String getMonedaBanco(String distrito, String banco, String sucursal) throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          String query = "MONEDA_BANCO";
          String moneda = "PES";
          try {
              con = this.conectarJNDI(query);
              if (con != null) {
                  st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                  st.setString(1, distrito);
                  st.setString(2, banco);
                  st.setString(3, sucursal);
                  rs = st.executeQuery();
                  if (rs.next()) {
                      moneda = reset(rs.getString("moneda"));
                  }


              }
          } catch (Exception e){
                  throw new Exception( "getMonedaBanco " + e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
            return moneda; 
      }
      
      
      
    
      
      
      /**
       * M�todo que busca los cheques de la corrida establecida en mims: 281
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      private  String searchCodeBanco(String banco, String bancoTR) throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          String code = "";
          String query = "SQL_CODIGO_BANCO";
          String codigoBancos = "";   //Mod Tmolina 2008-08-26
          try {

              banco = banco.toUpperCase();
              bancoTR = bancoTR.toUpperCase();

              if (bancoTR.equals("BANCOLOMBIA")) {    //Mod Tmolina 2008-08-26
                  codigoBancos = this.TABLA_CODIGO_BANCOS;
              } else {
                  codigoBancos = "BOCCIDENTE";       //Codigos de banco en Banco Occidente y BBVA
              }

              con = this.conectarJNDI(query);
              if (con != null) {
                  st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                  st.setString(1, codigoBancos);  //Mod Tmolina 2008-08-26
                  st.setString(2, banco);
                  rs = st.executeQuery();
                  if (rs.next()) {
                      code = this.reset(rs.getString("descripcion"));
                  }
              }
          }catch(Exception e){
                  throw new Exception( "searchCodeBanco " + e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
            return code; 
      }
      
      
       
      
      /**
       * M�todo que carga datos del sql para la factura
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      private FacturasCheques load(ResultSet   rs )throws Exception{
          FacturasCheques    factura = new FacturasCheques();
          try{
              
                factura.setDstrct            (  reset( rs.getString("dstrct")          ) );
                factura.setCorrida           (  reset( rs.getString("corrida")         ) );
                factura.setTipo_documento    (  reset( rs.getString("tipo_documento")  ) );
                factura.setDocumento         (  reset( rs.getString("documento")       ) );
                factura.setDescripcion       (  reset( rs.getString("descripcion")     ) );                
                factura.setProveedor         (  reset( rs.getString("beneficiario")    ) );
                factura.setNomProveedor      (  reset( rs.getString("nombre")          ) );
                factura.setVlr_saldo         (         rs.getDouble("valor")             );                
                factura.setVlr_saldo_me      (         rs.getDouble("valor_me")          );                
                factura.setBanco             (  reset( rs.getString("banco")           ) );
                factura.setSucursal          (  reset( rs.getString("sucursal")        ) );                    
                factura.setAgencia           (  reset( rs.getString("agencia_banco")   ) );
                factura.setMoneda            (  reset( rs.getString("moneda")          ) );
                factura.setFecha_aprobacion  (  reset( rs.getString("pago")            ) );
                factura.setUsuario_aprobacion(  reset( rs.getString("usuario_pago")    ) );
                factura.setBase              (  reset( rs.getString("base")            ) );
                
                
                factura.setTipo_pago         ( reset( rs.getString("tipo_pago")        ) );
                factura.setBanco_transfer    ( reset( rs.getString("banco_transfer")   ) );
                factura.setSuc_transfer      ( reset( rs.getString("suc_transfer")     ) );
                factura.setNo_cuenta         ( reset( rs.getString("no_cuenta")        ) );
                factura.setTipo_cuenta       ( reset( rs.getString("tipo_cuenta")      ) );
                factura.setNombre_cuenta     ( reset( rs.getString("nombre_cuenta")    ) );
                factura.setCedula_cuenta     ( reset( rs.getString("cedula_cuenta")    ) );
                
                                                         
          }catch(Exception e){
              throw new Exception(e.getMessage());
          }
          return factura;
      }
      
    
      
      
      
      
      
      
       /**
     * M�todo que determina si la factura es valida , aqui validamos campos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    private boolean  isValida(FacturasCheques   factura)throws Exception{
        boolean estado = false;
        try{
                  
               if (    !factura.getBanco_transfer().trim().equals("")   
                    && !factura.getNo_cuenta().trim().equals("")        &&  !factura.getTipo_cuenta().trim().equals("")
                    && !factura.getNombre_cuenta().trim().equals("")    &&  !factura.getCedula_cuenta().trim().equals("")
                   )
                      estado = true;
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }        
        return estado;
    }
      
    
    
    
    //*********************************************************************************
    
    
    
    /**
       * M�todo que actualiza la serie
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public String  updateSerie( String distrito, String banco, String sucursal, int cantidad, String user ) throws Exception{
            StringStatement  st      = null;
            String             query   = "SQL_UPDATE_SERIE";
            String             sql     = "";
            try{
                
                  st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                  st.setInt   (1,   cantidad         );
                  st.setString(2,   user             );
                  st.setString(3,   distrito         );  
                  st.setString(4,   banco            );  
                  st.setString(5,   sucursal         ); 
                
                  sql = st.getSql();
                
            }catch(Exception e){
                  throw new Exception( "updateSerie " + e.getMessage());
            }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             }
            return sql;
      }
      
      
    
    
     /**
       * M�todo que inserta el egreso
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public String  insertEgreso( Hashtable   cheque , String user) throws Exception{
            StringStatement  st      = null;
            String             query   = "SQL_INSERT_CHEQUE";
            String             sql     = "";
            try{
                
                st=  new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                st.setString(1,    (String)cheque.get("distrito")       );
                st.setString(2,    (String)cheque.get("banco")          );
                st.setString(3,    (String)cheque.get("sucursal")       );
                st.setString(4,    (String)cheque.get("cheque")         );
                st.setString(5,    (String)cheque.get("proveedor")      );
                st.setString(6,    (String)cheque.get("nombre")         );
                st.setString(7,    (String)cheque.get("agencia")        );
                st.setString(8,    (String)cheque.get("fechaCheque")    );
                st.setString(9,    (String)cheque.get("fechaCheque")    );
                st.setString(10,   this.CONCEPTO                        );
                st.setString(11,   (String)cheque.get("vlrLocal")       ); // vlr
                st.setString(12,   (String)cheque.get("valor")          ); // vlr for
                st.setString(13,   (String)cheque.get("moneda")         ); // moneda
                st.setString(14,   this.TIPO_DOC                        );
                st.setString(15,   (String)cheque.get("tasa")           ); // tasa
                st.setString(16,   (String)cheque.get("fechaCheque")    );
                st.setString(17,    user                                ); // usuario_impresion
                st.setString(18,   (String)cheque.get("cedula_cuenta")  ); // Beneficiario
                st.setString(19,   (String)cheque.get("proveedor")      ); // Proveedor
                st.setString(20,    user                                ); // creation_user
                st.setString(21,   (String)cheque.get("base")           );  
                
                sql = st.getSql();//JJCastro fase2
                
            }catch(Exception e){
                  throw new Exception( "updateCorrida " + e.getMessage());
            }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             }
            return sql;
      }
    
    
    
      
        /**
       * M�todo que actualiza la tabla corrida los datos de transferencia
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public String  insertEgresoDet( String cheque, FacturasCheques  factura , String user ) throws Exception{
            StringStatement  st      = null;
            String             query   = "SQL_INSERT_DETALLECHEQUE";
            String             sql     = "";
            try{
                
                 st=  new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
              
                  st.setString(1,   factura.getDstrct()         );
                  st.setString(2,   factura.getBancoTR()        );                  
                  st.setString(3,   factura.getSucursalTR()     );
                  st.setString(4,   cheque                      );                  
                  st.setString(5,   factura.getItem()           ); 
                  
                  st.setString(6,   this.CONCEPTO               );
                  st.setDouble(7,   factura.getValorTRLocal()   );
                  st.setDouble(8,   factura.getValorTR()        );
                  st.setString(9,   factura.getMonedaTR()       );
                  st.setString(10,  user                        );
                  
                  st.setString(11,  factura.getDescripcion()    );
                  st.setString(12,  factura.getBase()           );
                  st.setDouble(13,  factura.getTasa()           );                  
                  st.setString(14,  factura.getTipo_documento() );                  
                  st.setString(15,  factura.getDocumento()     );
                  st.setString(16,  "C"  );
                
                sql = st.getSql();//JJCastro fase2
                
            }catch(Exception e){
                  throw new Exception( "insertEgresoDet " + e.getMessage());
            }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             }
            return sql;
      }
      
      
      
    
    
    
    
    
    /**
       * M�todo que actualiza la factura, campos abono, saldo, y/o fecha y usuario de cancelacion
       * @autor.......fvillacob
       * @parameter   FacturasCheques    factura, double abono
       * @throws......Exception
       * @version.....1.0.
       **/
      public String updateCXP_DOC  (String cheque,  FacturasCheques  factura, String user )throws Exception{
       
          StringStatement  st       = null;
          String             sql      = "";
          String             query    = "SQL_UPDATE_CXP_DOC";
          try{
            
              TasaService  svc   =  new  TasaService();
              String  hoy        =  Util.getFechaActual_String(4);              
              
              String monedaLocal =  factura.getMonedaLocal();
              String monedaME    =  factura.getMoneda();
              double vlrME       =  factura.getVlr_saldo_me();              
              double vlrLocal    =  vlrME;
              
          // Convertimos Moneda:              
              if( !monedaME.equals( monedaLocal )  ){
                  Tasa  obj  =  svc.buscarValorTasa(monedaLocal,  monedaME , monedaLocal , hoy);
                  if(obj!=null)
                        vlrLocal *=  obj.getValor_tasa();
              } 
              vlrME     =  ( monedaME.   equals("DOL")   )? Util.roundByDecimal(vlrME,    2) : (int)Math.round(vlrME);
              vlrLocal  =  ( monedaLocal.equals("DOL")   )? Util.roundByDecimal(vlrLocal, 2) : (int)Math.round(vlrLocal);                        
                          
              
              
               st=  new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
           // set:               
                st.setString(1,    factura.getBancoTR()      );
                st.setString(2,    factura.getSucursalTR()   );
                st.setString(3,    factura.getMonedaTR()     );
                st.setString(4,    cheque                    );                
                st.setDouble(5,    vlrLocal  );
                st.setDouble(6,    vlrLocal  );                
                st.setDouble(7,    vlrME     );
                st.setDouble(8,    vlrME     );
                st.setString(9,    user      );
            // where:    
                st.setString(10,   factura.getDstrct()         );
                st.setString(11,   factura.getProveedor()      );
                st.setString(12,   factura.getTipo_documento() );
                st.setString(13,   factura.getDocumento()      );
              
              sql = st.getSql();
              
              
          }catch(Exception e){
              throw new Exception( " updateCXP_DOC: " + e.getMessage());
          }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             }
          return sql;
      }
      
      
      
      
    
    
    
    
    
    /**
       * M�todo que actualiza la tabla corrida los datos de transferencia
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public String  updateCorrida( String cheque, FacturasCheques    factura, String user) throws Exception{
            Connection         con     = null;
            StringStatement  st      = null;
            String             query   = "SQL_UPDATE_TRANSFERENCIA_CORRIDA";
            String             sql     = "";
            try{
               st=  new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
              
              //set
                st.setString(1,   factura.getBancoTR()        );
                st.setString(2,   factura.getSucursalTR()     );
                st.setString(3,   factura.getTransferencia()  );
                st.setString(4,   cheque         );
                st.setString(5,   user           );
              //where
                st.setString(6,   factura.getDstrct()         );
                st.setString(7,   factura.getCorrida()        );
                st.setString(8,   factura.getProveedor()      );
                st.setString(9,   factura.getTipo_documento() );
                st.setString(10,  factura.getDocumento()      );
                
                sql = st.getSql();
                
            }catch(Exception e){
                  throw new Exception( "updateCorrida " + e.getMessage());
            }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             }
            return sql;
      }
    
    
    
    
    
    
    
    
     
      /**
       * M�todo que inserta datos de la transferencia
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  String  insertTransferencia(      
                                            String distrito        ,
                                            String banco           ,
                                            String sucursal        ,
                                            String transferencia   ,
                                            String cheque          ,
                                            
                                            String banco_cta       ,
                                            String tipo_cuenta     ,
                                            String numero_cuenta   ,
                                            String nit_cuenta      ,
                                            String nombre_cuenta   ,
                                            
                                            double valor           ,
                                            String user
      ) throws Exception{
            StringStatement  st      = null;
            ResultSet          rs      = null;
            String             query   = "SQL_INSERT_PAGO_TRANSFERENCIA";
            String             sql     = "";            
            try{
                
                   st=  new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                   st.setString(1,   distrito      );
                   st.setString(2,   banco         );
                   st.setString(3,   sucursal      );
                   st.setString(4,   transferencia );
                   st.setString(5,   cheque        );                   
                   st.setString(6,   banco_cta     );
                   st.setString(7,   tipo_cuenta   );
                   st.setString(8,   numero_cuenta );
                   st.setString(9,   nit_cuenta    );
                   st.setString(10,  nombre_cuenta );
                   st.setDouble(11,  valor         );
                   st.setString(12,  user          );
                
                    sql = st.getSql();
               
                
            }catch(Exception e){
                  throw new Exception( "insertTransferencia " + e.getMessage());
            }finally{//JJCastro fase2
                    if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
             }
            return sql; 
      }
    
      
    /**
       * M�todo que verifica si existe un formato para un determinado banco
       * @autor.......Tmolina
       * @throws......Exception
       * @version.....1.0.
       **/
      public  boolean  existeFormatoBanco(String banco) throws Exception{//2008-08-28
          Connection con = null;
          PreparedStatement  st      = null;
            ResultSet          rs      = null;
            String             query   = "SQL_EXISTE_FORMATO_BANCO";
            boolean            estado  = false;
            try{
                con = this.conectarJNDI(query);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, banco);
                rs = st.executeQuery(); 
                if(rs.next()){
                     estado = true; 
                }  
                }}catch(Exception e){
                  throw new Exception( "existeFormatoBanco " + e.getMessage());
            }finally{//JJCastro fase2
                    if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                    if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
                }
            return estado; 
      }
      
      

    public BeanGeneral getDataTransferencia(String tipo_cuenta) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_INFO_TRANSFERENCIA";
      
        BeanGeneral bg=null;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, tipo_cuenta);
                rs = st.executeQuery();
                if (rs.next()) {
                    bg=new BeanGeneral();
                    bg.setValor_01(rs.getString("nit"));
                    bg.setValor_02(rs.getString("numero_cuenta"));
                    bg.setValor_03(rs.getString("tipo_cuenta"));
                    bg.setValor_04(rs.getString("tipo_transaccion_proveedor"));
                    bg.setValor_05(rs.getString("nombre"));                   
                
                }
            }
        } catch (Exception e) {
            throw new Exception("existeFormatoBanco " + e.getMessage());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return bg;
    }
    
    
      
      
      
    
    
}
