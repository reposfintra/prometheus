/*
 *  Nombre clase    :  Caja_lecDAO.java
 *  Descripcion     :
 *  Autor           : Ing. Juan Manuel Escand�n P�rez
 *  Fecha           : 8 de agosto de 2006, 08:57 AM
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;

public class Caja_lecDAO extends MainDAO{
    
    /** Crea una nueva instancia de  Caja_lecDAO */
    public Caja_lecDAO() {
        super( "Caja_lecDAO.xml" );
    }
    
    
    /*INSERT*/
/**
 * 
 * @param num_caja
 * @param cliente
 * @param descripcion
 * @param dato
 * @param creation_user
 * @param user_update
 * @throws SQLException
 */
    public void INSERT(String num_caja, String cliente, String descripcion, String dato, String creation_user, String user_update) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_INSERT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, "caja_lec");
            st.setString(2, num_caja);
            st.setString(3, cliente);
            st.setString(4, descripcion);
            st.setString(5, dato);
            st.setString(6, creation_user);
            st.setString(7, user_update);
            st.executeUpdate();
            }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT [Caja_lecDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /* ANULAR / ACTIVAR */
/**
 * 
 * @param reg_status
 * @param num_caja
 * @param usuario
 * @throws SQLException
 */
    public void CESTADO( String reg_status, String num_caja, String usuario ) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_UPDATEEST";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, reg_status);
            st.setString(2, usuario);
            st.setString(3, "caja_lec");
            st.setString(4, num_caja);
            st.executeUpdate();
            }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina CESTADO [Caja_lecDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /*DELETE*/
/**
 *
 * @param num_caja
 * @throws SQLException
 */
    public void DELETE( String num_caja ) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_DELETE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, "caja_lec");
            st.setString(2, num_caja);
            st.executeUpdate();
            }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina DELETE[Caja_lecDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
    /*LISTADO*/
 /**
  * 
  * @return
  * @throws Exception
  */
    public List LIST() throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_LIST";
        List lista           = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, "caja_lec");
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(Caja_lec.load(rs));
                }
            }
            }}catch(Exception e){
            throw new SQLException("Error en rutina LIST [Caja_lecDAO].... \n"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    
    /*BUSCAR NUMCAJA*/
/**
 *
 * @param num_caja
 * @return
 * @throws SQLException
 */
    public boolean BUSCARCAJA( String num_caja ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag         = false;
        String query = "SQL_SEARCHCAJA";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, "caja_lec");
            st.setString(2, num_caja);
            ////System.out.println("BUSCAR " + st.toString());
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
            }}
        catch(Exception e) {
            throw new SQLException("Error en rutina BUSCARCAJA[Caja_lecDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return flag;
    }
    
    /*CAJA LEC*/
/**
 * 
 * @param num_caja
 * @return
 * @throws SQLException
 */
    public Caja_lec SEARCHCAJA( String num_caja ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        Caja_lec datos = null;
        String query = "SQL_SEARCHCAJA";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, "caja_lec");
            st.setString(2, num_caja);
            rs = st.executeQuery();
            while(rs.next()){
                datos = Caja_lec.load(rs);
                break;
            }
            }}
        catch(Exception e) {
            throw new SQLException("Error en rutina SEARCHCAJA[Caja_lecDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return datos;
    }
    
    /* MODIFICAR */
/**
 * 
 * @param num_caja
 * @param nnum_caja
 * @param ncliente
 * @param ndescripcion
 * @param ndato
 * @param nuser_update
 * @throws SQLException
 */
    public void UPDATE( String num_caja, String nnum_caja, String ncliente, String ndescripcion, String ndato, String nuser_update ) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query                  = "SQL_UPDATE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, nnum_caja);
            st.setString(2, ncliente);
            st.setString(3, ndescripcion);
            st.setString(4, ndato);
            st.setString(5, nuser_update);
            st.setString(6, "caja_lec");
            st.setString(7, num_caja);
            st.executeUpdate();
            }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATE[Caja_lecDAO]... \n"+e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }

/**
 *
 * @param num_caja
 * @param cliente
 * @param descripcion
 * @param dato
 * @return
 * @throws Exception
 */
    public List LISTBUSQUEDA( String num_caja, String cliente, String descripcion , String dato ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_LIST2";
        List lista           = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, "caja_lec");
            st.setString(2, num_caja);
            st.setString(3, cliente);
            st.setString(4, descripcion);
            st.setString(5, dato);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(Caja_lec.load(rs));
                }
            }
            }}catch(Exception e){
            throw new SQLException("Error en rutina LISTBUSQUEDA [Caja_lecDAO].... \n"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }


/**
 * 
 * @param num_caja
 * @return
 * @throws Exception
 */
    public String secuencia( String num_caja ) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String query         = "SQL_SECUENCIA";
        String sec           = "";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, num_caja+"-%");
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    sec = rs.getString("secuencia");
                }
            }
            }}catch(Exception e){
            throw new SQLException("Error en rutina secuencia [Caja_lecDAO].... \n"+ e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sec;
    }
    
    
}
