/***************************************
* Nombre Clase ............. CuotasDAO.java
* Descripci�n  .. . . . . .  Ofrece los sql para las cuotas
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  16/02/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/




package com.tsp.operation.model.DAOS;



import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import com.tsp.operation.model.DAOS.MainDAO;




public class CuotasDAO extends MainDAO{
    
    
    
    /* _______________________________________________________________________________________
    *                                 ATRIBUTOS
    *________________________________________________________________________________________ */
   
    
    
    
    // MIGRACION GENERAL
    
    private static String SQL_DISTINCT_PRESTAMOS =    

                                " SELECT                                                  "+              
                                "         DISTINCT(a.PRESTAMO)                            "+
                                " FROM    fin.amortizaciones   a,                         "+
                                "         fin.prestamo         b                          "+                                
                                " WHERE                                                   "+
                                "         a.dstrct               = ?                      "+
                                "    AND  a.tercero              = ?                      "+
                                "    AND  a.fecha_pago           BETWEEN  ? AND ?         "+
                                "    AND  a.fecha_transferencia  = '0099-01-01 00:00:00'  "+
                                "    AND  a.usuario_tranferencia = ''                     "+
                                "    AND  b.dstrct               = a.dstrct               "+
                                "    AND  b.id                   = a.prestamo             "+
                                "    AND  b.clasificacion        = '01'                   "+// Clasificacion proveedor tsp
                                "    AND  b.aprobado             = 'S'                    ";
    
    

    private static String SQL_BUSCAR_CUOTAS = 
    
                               " SELECT                                                     "+
                               "       dstrct                  ,                            "+
                               "       prestamo                ,                            "+
                               "       item                    ,                            "+
                               "       beneficiario            ,                            "+                               
                               "       get_NombreNit(beneficiario) as nameBeneficiario ,    "+
                               "       tercero                 ,                            "+
                               "       substr(fecha_pago,1,10)       as fecha_pago,         "+
                               "       valor_a_pagar           ,                            "+
                               "       valor_capital           ,                            "+
                               "       valor_interes           ,                            "+
                               "       case when ( substr(fecha_pago_ter,1,10)='0099-01-01' ) then '' else  substr(fecha_pago_ter,1,10)   end   as fecha_real_pago,    "+   
                               "       fecha_transferencia     ,                            "+
                               "       usuario_tranferencia                                 "+
                               "     FROM                                                   "+
                               "             fin.amortizaciones                             "+
                               "     WHERE                                                  "+
                               "              dstrct       = ?                              "+
                               "        AND   prestamo     = ?                              "+ 
                               "        AND   fecha_pago   between  ? and  ?                "+
                               "        AND   fecha_transferencia  = '0099-01-01 00:00:00'  "+
                               "        AND   usuario_tranferencia = ''                     "+ 
                               "        AND   estado_pago_ter      <>'50'                   "+
                               "     ORDER  BY  2,3                                         ";
                           //    "     LIMIT   ?                                              ";
                               
    
    
    
    
    
    
    
  // MIGRACION PROVEEDOR
    
   private static String SQL_DISTINCT_PRESTAMOS_PROVEEDOR =
                             " SELECT                                                  "+
                             "                 a.dstrct                 as dstrct,     "+
                             "                 a.beneficiario           as nit,        "+
                             "                 a.id                     as id,         "+
                             "                 b.payment_name           as nombre,     "+
                             "                 sum( a.monto         )   as capital,    "+
                             "                 sum( a.interes       )   as intereses,  "+
                             "                 sum( a.vlracu_capdes )   as capdesc,    "+
                             "                 sum( a.vlracu_intdes )   as intdesc,    "+
                             "                 sum( a.vlracu_migmims)   as migmims,    "+
                             "                 sum( a.vlracu_regmims)   as regmims,    "+
                             "                 sum( a.vlracu_desprop)   as descprop,   "+
                             "                 sum( a.vlracu_pagfintra) as pagfintra   "+
                             " FROM   fin.prestamo  a,                                 "+
                             "        proveedor     b                                  "+ 
                             " WHERE                                                   "+
                             "           a.dstrct        = ?                           "+
                             "      AND  a.tercero       = ?                           "+  
                             "      AND  a.beneficiario  IN ( #CODE# )                 "+
                             "      AND  a.aprobado      = 'S'                         "+
                             "      AND  a.clasificacion = '01'                        "+
                             "      AND  b.nit           = a.beneficiario              "+
                             " GROUP BY 1,2,3,4                                        "+
                             " ORDER  BY  beneficiario, id                             ";



   
    
     private static String SQL_BUSCAR_CUOTAS_PRESTAMO = 
    
                               " SELECT                                                     "+
                               "       dstrct                  ,                            "+
                               "       prestamo                ,                            "+
                               "       item                    ,                            "+
                               "       beneficiario            ,                            "+                               
                               "       get_NombreNit(beneficiario) as nameBeneficiario ,    "+
                               "       tercero                 ,                            "+
                               "       substr(fecha_pago,1,10)       as fecha_pago,         "+
                               "       valor_a_pagar           ,                            "+
                               "       valor_capital           ,                            "+
                               "       valor_interes           ,                            "+
                               "       case when ( substr(fecha_pago_ter,1,10)='0099-01-01' ) then '' else  substr(fecha_pago_ter,1,10)   end   as fecha_real_pago,    "+   
                               "       fecha_transferencia     ,                            "+
                               "       usuario_tranferencia                                 "+
                               "     FROM                                                   "+
                               "             fin.amortizaciones                             "+
                               "     WHERE                                                  "+
                               "              dstrct       = ?                              "+
                               "        AND   prestamo     = ?                              "+ 
                               "        AND   fecha_transferencia  = '0099-01-01 00:00:00'  "+
                               "        AND   usuario_tranferencia = ''                     "+
                               "        AND   estado_pago_ter      <>'50'                   "+
                               "     ORDER  BY  prestamo, item,  substr(fecha_pago,1,10)    ";
                               
    
    
    
    
     
     
     
    
    
    
    private static String SQL_UPDATE_FECHA_PAGO_CUOTA = 
    
                            " UPDATE   fin.amortizaciones    "+
                            " SET      fecha_pago  = ?,      "+
                            "          user_update = ?       "+
                            " WHERE                          "+
                            "          dstrct      = ?       "+
                            "    AND   prestamo    = ?       "+
                            "    AND   item        = ?       ";

   
    
    
    
    
     private static String SQL_UPDATE_DATOS_MIGRACION = 
     
                            " UPDATE   fin.amortizaciones              "+
                            " SET      fecha_transferencia  = now(),   "+
                            "          usuario_tranferencia = ?        "+
                            " WHERE                                    "+
                            "          dstrct      = ?                 "+
                            "    AND   prestamo    = ?                 "+
                            "    AND   item        = ?                ;";
    
    
    
     
     
     
     private static String SQL_DATOS_PROVEEDOR_MIGRACION = 

                            "  SELECT                    "+
                            "        id_mims         ,   "+
                            "        payment_name    ,   "+
                            "        branch_code     ,   "+
                            "        bank_account_no     "+
                            " FROM   proveedor           "+
                            " WHERE  nit= ?              ";

   
  /* _______________________________________________________________________________________
    *                                 METODOS
    *________________________________________________________________________________________ */
   
    
    
    
    public CuotasDAO() {
    super("CuotasDAO.xml");//JJCastro fase2
    }
    
    
    
    
    /**
     * Obtiene los prestamos comprendido entre una fecha para un tercero, y por cada prestamo saca los n primeros
     * @autor fvillacob
     * @param  String distrito, String tercero, String fechaIni, String fechaFin, int limit
     * @throws Exception.
     */
     public List getCuotas(String distrito, String tercero, String fecha, int limit)throws Exception{
        Connection        con         = null;//JJCastro fase2
        PreparedStatement st            = null; 
        ResultSet         rs            = null;
        PreparedStatement st2           = null; 
        ResultSet         rs2           = null;
        List              listaPrestamo = new LinkedList(); 
        List              listaCuotas   = new LinkedList(); 
         String query = "SQL_DISTINCT_PRESTAMOS";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            String ano =  Util.getFechaActual_String(1);
            String fechaIni = ano      + "-01-01";
            String fechaFin = fecha;
            // Obtenemos la Lista de Prestamos:
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito );            
            st.setString(2, tercero  );            
            st.setString(3, fechaIni );
            st.setString(4, fechaFin );
            rs = st.executeQuery();
            while(rs.next()){
                String prestamoId = rs.getString(1);
                listaPrestamo.add( prestamoId );                
            }
            
            
            // Por cada prestamo, obtenemos  las n cuotas
            st2 = con.prepareStatement(this.obtenerSQL("SQL_BUSCAR_CUOTAS"));//JJCastro fase2
            int contCuotas = 1;
            for(int i=0;i<listaPrestamo.size();i++){
                String idPrestamo = (String) listaPrestamo.get(i);                
                st2.setString(1, distrito   );            
                st2.setString(2, idPrestamo ); 
                st2.setString(3, fechaIni   );
                st2.setString(4, fechaFin   );
                rs2 = st2.executeQuery();
                while(rs2.next()){                     
                    Amortizacion  amort = this.loadAmortizacion(rs2);  
                    amort.setIdObjeto( contCuotas );
                    listaCuotas.add( amort );
                    contCuotas++;
                amort = null;//Liberar Espacio JJCastro
                }
                st2.clearParameters();
             }
            
        }}catch(Exception e) {
             throw new SQLException(" DAO: getCuotas()  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (rs2  != null){ try{ rs2.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st2  != null){ try{ st2.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listaCuotas;
    }
     
     
     
     
     
     
     
    /**
     * Obtiene los prestamos comprendido entre una fecha para un tercero, y por cada prestamo saca los n primeros
     * @autor fvillacob
     * @param  String distrito, String tercero, String fechaIni, String fechaFin, int limit
     * @throws Exception.
     */
     public List getPrestamosProveedores(String distrito, String tercero, String proveedores)throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st            = null;
        ResultSet         rs            = null;
        List              lista         = new LinkedList(); 
        try{
            
            // Obtenemos la Lista de Prestamos:
            st = con.prepareStatement(this.obtenerSQL("SQL_DISTINCT_PRESTAMOS_PROVEEDOR").replaceAll("#CODE#", proveedores ) );
            st.setString(1, distrito    );            
            st.setString(2, tercero     );   
            rs = st.executeQuery();
            while(rs.next()){
                ResumenPrestamo pr = this.loadResumenPrestamo(rs);
                   pr.setAmortizaciones( this.getCuotasPrestamo( pr.getDistrito(), pr.getId()  )   );
                lista.add( pr );                
            }
            
        }catch(Exception e) {
             throw new SQLException(" DAO: getPrestamosProveedores()  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
     
     
     
     
     
     /**
     * Obtiene las cuotas para para  un determinado prestamo
     * @autor fvillacob
     * @param  Prestamo pr
     * @throws Exception.
     */
     public List getCuotasPrestamo(String distrito, int id)throws Exception{
        Connection con = null;//JJCastro fase2
         PreparedStatement st = null;
         ResultSet rs = null;
         List lista = new LinkedList();
         String query = "SQL_BUSCAR_CUOTAS_PRESTAMO";//JJCastro fase2
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, distrito);
                 st.setString(2, String.valueOf(id));
                 rs = st.executeQuery();
                 int contCuotas = 1;
                 while (rs.next()) {
                     Amortizacion amort = this.loadAmortizacion(rs);
                     amort.setIdObjeto(contCuotas);
                     lista.add(amort);
                     contCuotas++;
                 }
             }
         }catch(Exception e) {
             throw new SQLException(" DAO: getCuotasPrestamo()  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
     
     
     
     
     
     
     /**
     * Carga el objeto Amortizacion
     * @autor fvillacob
     * @param  ResultSet  rs
     * @throws Exception.
     */
     public ResumenPrestamo loadResumenPrestamo(ResultSet  rs) throws Exception{
         ResumenPrestamo  rp = new  ResumenPrestamo();
         try{
             
             
               rp.setDistrito          (   rs.getString("dstrct")    );
               rp.setId                (   rs.getInt   ("id")        );
               rp.setNit               (   rs.getString("nit")       );
               rp.setNombre            (   rs.getString("nombre")    );
               rp.setValor             (   rs.getDouble("capital")   );
               rp.setIntereses         (   rs.getDouble("intereses") ); 
               rp.setValorDesc         (   rs.getDouble("capdesc")   );
               rp.setInteresesDesc     (   rs.getDouble("intdesc")   );
               rp.setVlrMigradoMims    (   rs.getDouble("migmims")   );
               rp.setVlrRegistradoMims (   rs.getDouble("regmims")   );
               rp.setVlrDescontadoProp (   rs.getDouble("descprop")  );
               rp.setVlrPagadoFintra   (   rs.getDouble("pagfintra") );
                   
                                
         }catch(Exception e){
             throw new Exception(e.getMessage());
         }
         return rp;
     }
     
     
     
     
     
     
     
     
     
     /**
     * Carga el objeto Amortizacion
     * @autor fvillacob
     * @param  ResultSet  rs
     * @throws Exception.
     */
     public Amortizacion loadAmortizacion(ResultSet  rs) throws Exception{
         Amortizacion  amort = new  Amortizacion();
         try{
            
                           
               amort.setDstrct          (  rs.getString("dstrct")               );
               amort.setPrestamo        (  rs.getString("prestamo")             );
               amort.setItem            (  rs.getString("item")                 );
               amort.setBeneficiario    (  rs.getString("beneficiario")         );
               amort.setNameBeneficiario(  rs.getString("nameBeneficiario")     );
               amort.setTercero         (  rs.getString("tercero")              );
               amort.setFechaPago       (  rs.getString("fecha_pago")           );
               amort.setTotalAPagar     (  rs.getDouble("valor_a_pagar")        );
               amort.setCapital         (  rs.getDouble("valor_capital")        );
               amort.setInteres         (  rs.getDouble("valor_interes")        );
               amort.setFechaMigracion  (  rs.getString("fecha_transferencia")  );
               amort.setUserMigracion   (  rs.getString("usuario_tranferencia") );
             
         }catch(Exception e){
             throw new Exception(e.getMessage());
         }
         return amort;
     }
     
    
     
     
     
     
     
     /**
     * Actualiza la fecha de pago de una cuota de un prestamo
     * @autor  fvillacob
     * @param  Amortizacion amort, String user
     * @throws Exception.
     */
     public void updateFechaPago( Amortizacion amort, String user)throws Exception{
        Connection con = null;//JJCastro fase2
         PreparedStatement st = null;
         String query = "SQL_UPDATE_FECHA_PAGO_CUOTA";//JJCastro fase2
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, amort.getFechaPago());
                 st.setString(2, user);
                 st.setString(3, amort.getDstrct());
                 st.setString(4, amort.getPrestamo());
                 st.setString(5, amort.getItem());
                 st.execute();

             }
         }catch(Exception e) {
             throw new SQLException(" DAO: updateFechaPago()  -> " +e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
     
     
     
     
     
     /**
     * Actualiza la fecha y usurio de migracion de una cuota de un prestamo
     * @autor  fvillacob
     * @param  Amortizacion amort, String user
     * @throws Exception.
     */
     public String updateMigracion( Amortizacion amort, String user)throws Exception{

         StringStatement st            = null;
         String query = "SQL_UPDATE_DATOS_MIGRACION";//JJCastro fase2
         String sql = "";//JJCastro fase2
         try{
              st = new StringStatement(this.obtenerSQL(query));//JJCastro fase2
              st.setString(1, user                 );              
              st.setString(2, amort.getDstrct()    );
              st.setString(3, amort.getPrestamo()  );
              st.setString(4, amort.getItem()      );
              sql = st.getSql();//JJCastro fase2
            
        }catch(Exception e) {
             throw new SQLException(" DAO: updateMigracion()  -> " +e.getMessage());
        } 
        finally {
          if (st  != null){ try{ st=null;            } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return sql;
    }
     
     
     
     
     
     
     
     
     
     /**
     * Actualiza la fecha y usurio de migracion de una cuota de un prestamo
     * @autor  fvillacob
     * @param  Amortizacion amort, String user
     * @throws Exception.
     */
     public Hashtable getDatosProveedor( String nit)throws Exception{
        Connection con = null;//JJCastro fase2
         PreparedStatement st = null;
         ResultSet rs = null;
         Hashtable ht = new Hashtable();
         String query = "SQL_DATOS_PROVEEDOR_MIGRACION";//JJCastro fase2
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
                 st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1, nit);
                 rs = st.executeQuery();
                 while (rs.next()) {
                     ht.put("idmims", rs.getString("id_mims"));
                     ht.put("nombre", rs.getString("payment_name"));
                     ht.put("banco", rs.getString("branch_code"));
                     ht.put("sucursal", rs.getString("bank_account_no"));
                     ht.put("nit", nit);
                     break;
                 }

             }
         }catch(Exception e) {
             throw new SQLException(" DAO: getDatosProveedor()  -> " +e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ht;
    }
     
    
}
