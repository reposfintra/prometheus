/******************************************************************
* Nombre ......................ClasificacionPlacaDAO.java
* Descripción..................Clase DAO para clasificacion de placas
* Autor........................Armando Oviedo
* Fecha........................17/01/2006
* Versión......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Armando Oviedo
 */
public class ClasificacionPlacaDAO {    

    private PoolManager pm;
    boolean escribe;
    
    /**************SQL Queries*********************/
    
    private String SQL_BUSCARPLACA      = "SELECT DISTINCT(plaveh) FROM est_carga_placa WHERE reg_status <> 'A'";    
    private String SQL_ACTUALIZARPLACA  = "UPDATE placa SET clasificacion = ? WHERE placa = ?";
    private String SQL_FILTROPLACA      = "SELECT SUM(num_viaje) FROM est_carga_placa WHERE plaveh = ? AND periodo = ? AND carga = ? OR carga = ?";
    private String SQL_FILTROPLACA2     = "SELECT SUM(num_viaje) FROM est_carga_placa WHERE plaveh = ? AND periodo = ? AND carga >= ? AND carga <= ?";
    private String SQL_FILTROPLACA3     = "SELECT SUM(num_viaje) FROM est_carga_placa WHERE plaveh = ? AND periodo = ?";
    
    /**********************************************/
    
    /** Creates a new instance of ClasificacionPlacaDAO */
    public ClasificacionPlacaDAO() {
    }                  
    
    /**
     * Método que actualiza y coloca una clasificacion a la tabla placa
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void actualizarPlaca() throws SQLException{
        PreparedStatement ps = null;    
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        Connection con = null;                        
        try{                                                                                      
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");                                                            
            ps = con.prepareStatement( SQL_BUSCARPLACA );
            rs = ps.executeQuery();
            while(rs.next()){
                this.escribe = false;
                String placa = rs.getString(1);
                evaluarClasificacion1(placa);
                if(!this.escribe){
                    evaluarClasificacion2(placa);
                }
                if(!this.escribe){
                    evaluarClasificacion3(placa);
                }
                if(!this.escribe){
                    evaluarClasificacion4(placa);
                }
                if(!this.escribe){
                    ps2 = con.prepareStatement(SQL_ACTUALIZARPLACA);
                    ps2.setString(1, "5");
                    ps2.setString(2, placa);
                    ps2.executeUpdate();
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
               try{
                    ps.close();
               }
               catch(SQLException e){
                   throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
               }
            }  
            if(ps2 != null){
                try{
                    ps.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }        
    }
    
    /**
     * Método que evalua el tipo de clasificación 1 y actualiza la tabla placa
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/
    private void evaluarClasificacion1(String placa) throws SQLException{        
        PreparedStatement ps = null;  
        PreparedStatement ps2 = null;  
        ResultSet rs = null;
        Connection con = null;
        try{
            int viajes = 0;
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");                                                            
            ps = con.prepareStatement( SQL_FILTROPLACA );
            ps.setString(1, placa);
            ps.setInt(2, 6);
            ps.setInt(3, 1);
            ps.setInt(4, 2);                    
            rs = ps.executeQuery();
            while(rs.next()){                        
                viajes = rs.getInt(1);
            }        
            if(viajes>=11){
                //Clasificación 1
                ps2 = con.prepareStatement(SQL_ACTUALIZARPLACA);
                ps2.setString(1, "1");
                ps2.setString(2, placa);
                ps2.executeUpdate();
                this.escribe = true;
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
               try{
                    ps.close();
               }
               catch(SQLException e){
                   throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
               }
            }  
            if(ps2 != null){
                try{
                    ps.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }        
    }
    
    /**
     * Método que evalua el tipo de clasificación 2 y actualiza la tabla placa
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/
    private void evaluarClasificacion2(String placa) throws SQLException{        
        PreparedStatement ps = null;  
        PreparedStatement ps2 = null;  
        ResultSet rs = null;
        Connection con = null;
        try{
            int viajes = 0;
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");                                                            
            ps = con.prepareStatement( SQL_FILTROPLACA );
            ps.setString(1, placa);
            ps.setInt(2, 6);
            ps.setInt(3, 3);
            ps.setInt(4, 4);                    
            rs = ps.executeQuery();
            while(rs.next()){                        
                viajes = rs.getInt(1);
            }        
            if(viajes>=11){
                //Clasificación 2
                ps2 = con.prepareStatement(SQL_ACTUALIZARPLACA);
                ps2.setString(1, "2");
                ps2.setString(2, placa);
                ps2.executeUpdate();
                this.escribe = true;
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
               try{
                    ps.close();
               }
               catch(SQLException e){
                   throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
               }
            }  
            if(ps2 != null){
                try{
                    ps.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }        
    }
    
    /**
     * Método que evalua el tipo de clasificación 3 y actualiza la tabla placa
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/
    private void evaluarClasificacion3(String placa) throws SQLException{        
        PreparedStatement ps = null;  
        PreparedStatement ps2 = null;  
        ResultSet rs = null;
        Connection con = null;
        try{
            int viajes = 0;
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");                                                            
            ps = con.prepareStatement( SQL_FILTROPLACA2 );
            ps.setString(1, placa);
            ps.setInt(2, 6);
            ps.setInt(3, 1);
            ps.setInt(4, 4);                    
            rs = ps.executeQuery();
            while(rs.next()){                        
                viajes = rs.getInt(1);
            }        
            if(viajes<11){
                //Clasificación 3
                ps2 = con.prepareStatement(SQL_ACTUALIZARPLACA);
                ps2.setString(1, "3");
                ps2.setString(2, placa);
                ps2.executeUpdate();
                this.escribe = true;
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
               try{
                    ps.close();
               }
               catch(SQLException e){
                   throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
               }
            }  
            if(ps2 != null){
                try{
                    ps.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }        
    }
    
    /**
     * Método que evalua el tipo de clasificación 4 y actualiza la tabla placa
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/
    private void evaluarClasificacion4(String placa) throws SQLException{        
        PreparedStatement ps = null;  
        PreparedStatement ps2 = null;  
        ResultSet rs = null;
        Connection con = null;
        try{
            int viajes = 0;
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");                                                            
            ps = con.prepareStatement( SQL_FILTROPLACA3 );
            ps.setString(1, placa);
            ps.setInt(2, 12);            
            rs = ps.executeQuery();
            while(rs.next()){                        
                viajes = rs.getInt(1);
            }        
            if(viajes<11){
                //Clasificación 4
                ps2 = con.prepareStatement(SQL_ACTUALIZARPLACA);
                ps2.setString(1, "4");
                ps2.setString(2, placa);
                ps2.executeUpdate();
                this.escribe = true;
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            if(rs != null){
                try{
                    rs.close();
                   }
                   catch(SQLException e){
                       throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                   }
            }
            if(ps != null){
               try{
                    ps.close();
               }
               catch(SQLException e){
                   throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
               }
            }  
            if(ps2 != null){
                try{
                    ps.close();
                }catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }        
    }            
}
