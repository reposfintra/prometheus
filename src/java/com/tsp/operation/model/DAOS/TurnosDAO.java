/******************************************************************************************
 *      Nombre Clase.................   TurnosDAO.java                                    *
 *      Descripci�n..................   lase que maneja los DAO ( Data Access Object )    *
 *                                      los cuales contienen los metodos que interactuan  *
 *                                      con la BD.                                        *
 *      Autor........................   David Lamadrid                                    *
 *      Fecha........................   28.12.2005                                        *
 *      Versi�n......................   1.0                                               *
 *      Copyright....................   Transportes Sanchez Polo S.A.                     *
 ******************************************************************* **********************/

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  dlamadrid
 */
public class TurnosDAO {
    
    /** Creates a new instance of TurnosDAO */
    public TurnosDAO() {
    }
    
    private Turnos turno;
    private Vector vTurnos;
    
    //Declaracion de Consultas
    private final static String INSERTAR            ="INSERT INTO turnos (reg_status,dstrct,usuario_turno,fecha_turno,h_entrada,h_salida,zona,last_update,creation_date,creation_user,base) values ('',?,upper(?),?,?,?,?,'0099-01-01 00:00:00',now(),?,?)";
    private final static String UPDATE              ="INSERT INTO turnos (reg_status, dstrct, usuario_turno, fecha_turno, h_entrada, h_salida, zona, last_update, user_update, creation_date, creation_user, base) VALUES (?, ?, ?, ?, ?, ?, ?, #now#, ?, ?, ?, ?)";
    private final static String TURNOS_POR_USUARIO  ="select usuario_turno,fecha_turno,h_entrada,h_salida,zona,nombre from turnos,usuarios where upper(usuario_turno)=upper(?) and upper(usuarios.idusuario)=turnos.usuario_turno and fecha_turno between ? and ?  order by fecha_turno,h_entrada";
    private final static String TURNOS_POR_ID       ="select usuario_turno,fecha_turno,h_entrada,h_salida,zona,nombre from turnos,usuarios where usuario_turno=upper(?) and upper(usuarios.idusuario)=turnos.usuario_turno and fecha_turno=? and h_entrada=? and h_salida=?";
    private final static String ELIMINAR            ="delete from turnos where usuario_turno=upper(?) and fecha_turno=? and h_entrada=? and h_salida=?";
    private final static String ZONAS               ="select desczona from zona where codzona=?";
    private final static String EXISTE_TURNO        ="select * from turnos where usuario_turno=upper(?) and fecha_turno=? and h_entrada=? and h_salida=?";
    private final static String TURNO_VALIDO        ="select usuario_turno from turnos where usuario_turno=upper(?) and fecha_turno=? and ((h_entrada <= ? and h_salida >= ?) or (h_entrada >= ? and h_salida <= ?) or (h_entrada <= ? and h_salida >= ?) or (h_entrada <= ? and h_salida >=?))";
    private final static String ACTUALIZAR_TURNO    ="update turnos set usuario_turno=?,fecha_turno=?,h_entrada=?,h_salida=?,zona=?,user_update=?, last_update=now() where usuario_turno=upper(?) and fecha_turno=? and h_entrada=? and h_salida=?";
    
    /**
     * Metodo que inserta un registro en la tabla turnos
     * @autor.......David Lamadrid
     * @param.......
     * @see.........
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     */
    public void insertar() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(INSERTAR);
                st.setString(1,turno.getDstrct());
                st.setString(2,turno.getUsuario_turno());
                st.setString(3,turno.getFecha_turno());
                st.setString(4,turno.getH_entrada());
                st.setString(5,turno.getH_salida());
                st.setString(6,turno.getZona());
                st.setString(7,turno.getCreation_user());
                st.setString(8,turno.getBase());
                st.executeUpdate();
            }
            ////System.out.println("consulta"+st);
        }catch(SQLException e) {
            throw new SQLException("ERROR DURANTE LA INSERCION DE TURNOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null) {
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo turnosPorCodigo , Metodo que llena un Vector con resitros de turnos pertenecientes a un usuario
     * @autor : Ing. David Lamadrid
     * @param : String usuario
     * @version : 1.0
     */
    public void turnosPorCodigo(String usuario, String Fecini, String Fecfin) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PreparedStatement st1 = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        PoolManager poolManager = null;
        boolean sw = false;
        this.vTurnos=new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.TURNOS_POR_USUARIO);
                
                st.setString(1, usuario);
                st.setString(2, Fecini);
                st.setString(3, Fecfin);
                ////System.out.println("CONSULTA TURNOS "+st);
                rs = st.executeQuery();
                ////System.out.println(st);
                this.turno=null;
                while (rs.next()) {
                    // ////System.out.println("entro en next");
                    String zonas = rs.getString("zona");
                    String[] vzonas=zonas.split(",");
                    Vector vZonas=new Vector();
                    for(int i=0;i<vzonas.length;i++){
                        st1=con.prepareStatement(ZONAS);
                        st1.setString(1, ""+vzonas[i]);
                        //////System.out.println("-----" + st1);
                        rs1=st1.executeQuery();
                        while(rs1.next()){
                            vZonas.add(rs1.getString("desczona"));
                        }
                    }
                    
                    this.vTurnos.add(this.turno.load(rs,vZonas));
                }
            }
        }
        catch(SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage()+"" + e.getErrorCode());
        }
        finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(st1 != null) {
                try {
                    st1.close();
                }
                catch(SQLException ex) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + ex.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        
    }
    
    /**
     * Metodo que retorna un registro de turnos dadp la llave Primaria
     * @autor : Ing. David Lamadrid
     * @param : String usuario
     * @version : 1.0
     */
    public void turnosPorId(String usuario,String fecha,String h_entrada,String h_salida) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        PreparedStatement st1 = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        PoolManager poolManager = null;
        boolean sw = false;
        this.vTurnos=new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.TURNOS_POR_ID);
                st.setString(1, usuario);
                st.setString(2, fecha);
                st.setString(3, h_entrada);
                st.setString(4, h_salida);
                rs = st.executeQuery();
                ////System.out.println(st);
                this.turno=null;
                while (rs.next()) {
                    ////System.out.println("entro en next");
                    String zonas = rs.getString("zona");
                    String[] vzonas=zonas.split(",");
                    Vector vZonas=new Vector();
                    for(int i=0;i<vzonas.length;i++){
                        st1=con.prepareStatement(ZONAS);
                        st1.setString(1, ""+vzonas[i]);
                        rs1=st1.executeQuery();
                        while(rs1.next()){
                            vZonas.add(rs1.getString("desczona"));
                        }
                    }
                    
                    this.turno=this.turno.load(rs,vZonas);
                }
            }
        }
        catch(SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage()+"" + e.getErrorCode());
        }
        finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Metodo que elimina un registro en la tabla turnos
     * @autor.......David Lamadrid
     * @param String usuario,String fecha,String h_entrada,String h_salida
     * @see
     * @throws SQLException
     * @version 1.0.
     * @return
     */
    public void eliminar(String usuario,String fecha,String h_entrada,String h_salida) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.ELIMINAR);
                st.setString(1,usuario);
                st.setString(2,fecha);
                st.setString(3,h_entrada);
                st.setString(4,h_salida);
                st.executeUpdate();
            }
            ////System.out.println("consulta"+st);
        }catch(SQLException e) {
            throw new SQLException("ERROR DURANTE LA INSERCION DE TURNOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null) {
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo existe_turno metodo que retorna un true si existe un turno y false si no
     * @autor : Ing. David Lamadrid
     * @param : String usuario,String fecha,String h_entrada,String h_salida
     * @version : 1.0
     */
    public boolean existe_turno(String usuario,String fecha,String h_entrada,String h_salida) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.EXISTE_TURNO);
                st.setString(1, usuario);
                st.setString(2, fecha);
                st.setString(3, h_entrada);
                st.setString(4, h_salida);
                rs = st.executeQuery();
                ////System.out.println("existe_turno: "+st);
                while (rs.next()) {
                    sw= true;
                }
            }
        }
        catch(SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage()+"" + e.getErrorCode());
        }
        finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    
    /**
     * Metodo que verifica si un turno al ser insertado es valido en secuencia
     * @autor : Ing. David Lamadrid
     * @param : String usuario,String fecha,String h_entrada,String h_salida
     * @version : 1.0
     */
    public boolean turnoValido(String usuario,String fecha,String h_entrada,String h_salida) throws SQLException {
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.TURNO_VALIDO);
                st.setString(1, usuario);
                st.setString(2, fecha);
                st.setString(3, h_entrada);
                st.setString(4, h_salida);
                st.setString(5, h_entrada);
                st.setString(6, h_salida);
                st.setString(7, h_entrada);
                st.setString(8, h_entrada);
                st.setString(9, h_salida);
                st.setString(10, h_salida);
                ////System.out.println("turno valido --> "+st.toString());
                rs = st.executeQuery();
                ////System.out.println(st);
                
                while (rs.next()) {
                    sw= true;
                }
            }
        }
        catch(SQLException e) {
            throw new SQLException("ERROR AL ENCONTRAR DOCUMENTO " + e.getMessage()+"" + e.getErrorCode());
        }
        finally {
            if(st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null) {
                poolManager.freeConnection("fintra", con );
            }
        }
        return sw;
    }
    
    /**
     * Metodo que actualiza un registro en la tabla de turnos
     * @autor.......David Lamadrid
     * @param.......
     * @see.........
     * @throws......SQLException
     * @version.....1.0.
     * @return.......
     */
    public void actualizarTurno(String fecha,String h_entrada,String h_salida) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                st = con.prepareStatement(this.ACTUALIZAR_TURNO);
                st.setString(1,turno.getUsuario_turno());
                st.setString(2,turno.getFecha_turno());
                st.setString(3,turno.getH_entrada());
                st.setString(4,turno.getH_salida());
                st.setString(5,turno.getZona());
                st.setString(6,turno.getUser_update());
                st.setString(7,turno.getUsuario_turno());
                st.setString(8,fecha);
                st.setString(9,h_entrada);
                st.setString(10,h_salida);
                ////System.out.println("CONSULTA MODIFICAR "+st);
                st.executeUpdate();
            }
            ////System.out.println("consulta"+st);
        }catch(SQLException e) {
            throw new SQLException("ERROR DURANTE LA INSERCION DE TURNOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null) {
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Getter for property turno.
     * @return Value of property turno.
     */
    public com.tsp.operation.model.beans.Turnos getTurno() {
        return turno;
    }
    
    /**
     * Setter for property turno.
     * @param turno New value of property turno.
     */
    public void setTurno(com.tsp.operation.model.beans.Turnos turno) {
        this.turno = turno;
    }
    
    /**
     * Getter for property vTurnos.
     * @return Value of property vTurnos.
     */
    public java.util.Vector getVTurnos() {
        return vTurnos;
    }
    
    /**
     * Setter for property vTurnos.
     * @param vTurnos New value of property vTurnos.
     */
    public void setVTurnos(java.util.Vector vTurnos) {
        this.vTurnos = vTurnos;
    }
    
    
    /**
     * Metodo load metodo que retorna un objeto Turnos con los atributos
     *        de la consulta
     * @autor : Ing. Osvaldo P�rez Ferrer
     * @param : ResultSet rs resultSet con el resultado de la consulta
     * @version : 1.0
     */
    public Turnos load( ResultSet rs ){
        
        Turnos t = null;
        
        try{
            
            t = new Turnos();
            
            t.setReg_status( rs.getString( "reg_status" ) );
            t.setDstrct( rs.getString( "dstrct" ) );
            t.setUsuario_turno( rs.getString( "usuario_turno" ) );
            t.setFecha_turno( rs.getString( "fecha_turno" ) );
            t.setH_entrada( rs.getString( "h_entrada" ) );
            t.setH_salida( rs.getString( "h_salida" ) );
            t.setZona( rs.getString( "zona" ) );
            t.setLast_update( rs.getString( "last_update" ) );
            t.setUser_update( rs.getString( "user_update" ) );
            t.setCreation_date( rs.getString( "creation_date" ) );
            t.setCreation_user( rs.getString( "creation_user" ) );
            t.setBase( rs.getString( "base" ) );
            
        }catch (Exception ex){
            ////System.out.println("error en TurnosDAO.load "+ex.getMessage() );
        }
        return t;
    }
    
    
    /**
     * Metodo obtener_turno metodo que retorna el turno con el usuario, fecha,
     *        hora_entrada y hora_salida dada
     * @autor : Ing. Osvaldo P�rez Ferrer
     * @param : String usuario,String fecha,String h_entrada,String h_salida
     * @version : 1.0
     */
    public Turnos obtener_turno(String usuario,String fecha,String h_entrada,String h_salida) throws SQLException{
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        Turnos trn = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(this.EXISTE_TURNO);
                st.setString(1, usuario);
                st.setString(2, fecha);
                st.setString(3, h_entrada);
                st.setString(4, h_salida);
                rs = st.executeQuery();
                ////System.out.println("existe_turno: "+st);
                if(rs.next()){
                    trn = this.load( rs );
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR en TurnosDAO.obtener_turno " + e.getMessage()+"" + e.getErrorCode());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                poolManager.freeConnection("fintra", con );
            }
        }
        return trn;
    }
    
    /**
     * Metodo que inserta un registro con todos los campos en la tabla turnos
     * @autor.......Ing. Osvaldo P�rez Ferrer
     * @throws......SQLException
     * @version.....1.0.
     */
    public void update( String usuario ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = UPDATE;
        
        if( usuario.length() == 0 ){
            sql = sql.replaceAll("#now#",  "'"+turno.getLast_update()+"'" );
        }
        else{
            sql = sql.replaceAll("#now#","now()");
        }
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null) {
                
                st = con.prepareStatement(sql);
                
                st.setString( 1, turno.getReg_status() );
                st.setString( 2, turno.getDstrct() );
                st.setString( 3, turno.getUsuario_turno() );
                st.setString( 4, turno.getFecha_turno() );
                st.setString( 5, turno.getH_entrada() );
                st.setString( 6, turno.getH_salida() );
                st.setString( 7, turno.getZona() );
                st.setString( 8, (usuario.length() > 0)? usuario : turno.getUser_update() );
                st.setString( 9, turno.getCreation_date() );
                st.setString( 10, turno.getCreation_user() );
                st.setString( 11, turno.getBase() );                              
                
                st.executeUpdate();
            }
            
        }catch(SQLException e) {
            throw new SQLException("ERROR en turnosDAO.update " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (st != null) {
                try {
                    st.close();
                }
                catch(SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
        }
        if (con != null) {
            poolManager.freeConnection("fintra", con);
        }
    }
    
    
}
