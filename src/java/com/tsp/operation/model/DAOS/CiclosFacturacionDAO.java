/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Ing. Rhonalf Martinez (rhonalf) <rhonaldomaster@gmail.com>
 * @version 0.1
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CiclosFacturacionDAO extends MainDAO {
    public CiclosFacturacionDAO(){
        super("CiclosFacturacionDAO.xml");
    }

    /**
     * Busca los ciclos de facturaci&oacute;n para un a&ntilde;o
     * @param anio A&ntilde;o a buscar
     * @return Listado con los datos
     * @throws Exception Cuando hay error
     */
    public ArrayList<BeanGeneral> buscarCiclosAnio(String anio) throws Exception{
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        BeanGeneral bean = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_SRC";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, anio);
            rs = st.executeQuery();
            while(rs.next()){
                bean = new BeanGeneral();
                bean.setValor_01(rs.getString("anio"));
                bean.setValor_02(rs.getString("ciclo"));
                bean.setValor_03(this.nombreMes(rs.getInt("mes"))); 
                bean.setValor_04(rs.getString("lectura"));
                bean.setValor_05(rs.getString("liquidacion"));
                bean.setValor_06(rs.getString("facturacion"));
                bean.setValor_07(rs.getString("impresion"));
                bean.setValor_08(rs.getString("distribucion"));
                bean.setValor_09(rs.getString("primer_vencimiento"));
                bean.setValor_10(rs.getString("suspension"));
                bean.setValor_11(rs.getString("segundo_vencimiento"));
                bean.setValor_12(rs.getString("periodo"));
                bean.setValor_13(rs.getString("dias"));
                lista.add(bean);
            }
        }
        catch (Exception e) {
            System.out.println("Error en buscarCiclosAnio en CiclosFacturacionDAO.java: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscarCiclosAnio en CiclosFacturacionDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return lista;
    }

    /**
     * Dado un numero de mes, devuelve su nombre
     * @param mes Numero del mes
     * @return Nombre del mes
     */
    private String nombreMes(int mes){
        String nombre = "";
        switch(mes){
            case 1:
                nombre = "Enero";
                break;
            case 2:
                nombre = "Febrero";
                break;
            case 3:
                nombre = "Marzo";
                break;
            case 4:
                nombre = "Abril";
                break;
            case 5:
                nombre = "Mayo";
                break;
            case 6:
                nombre = "Junio";
                break;
            case 7:
                nombre = "Julio";
                break;
            case 8:
                nombre = "Agosto";
                break;
            case 9:
                nombre = "Septiembre";
                break;
            case 10:
                nombre = "Octubre";
                break;
            case 11:
                nombre = "Noviembre";
                break;
            case 12:
                nombre = "Diciembre";
                break;
        }
        return nombre;
    }

    /**
     * Busca si existen datos de ciclos para el a&ntilde;o
     * @param anio A&ntilde;o a buscar
     * @return Si existen o no datos
     * @throws Exception Cuando hay error
     */
    public boolean existeDatosAnio(String anio) throws Exception{
        boolean existe = false;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_SRC";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, anio);
            rs = st.executeQuery();
            if(rs.next()) existe = true;
        }
        catch (Exception e) {
            throw new Exception("Error en existeDatosAnio en CiclosFacturacionDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return existe;
    }

    /**
     * Genera sql para insertar datos en la tabla
     * @param anio A&ntilde;o de los ciclos
     * @param bean Objeto con los datos
     * @return Cadena con el codigo generado
     * @throws Exception Cuando hay error
     */
    public String insertar(String anio, BeanGeneral bean) throws Exception{
        String sql = "";
        String query = "SQL_INS";
        StringStatement st = null;
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql,true);
            st.setString(1, anio);
            st.setString(2, bean.getValor_01());
            st.setString(3, bean.getValor_02());
            st.setString(4, bean.getValor_03());
            st.setString(5, bean.getValor_04());
            st.setString(6, bean.getValor_05());
            st.setString(7, bean.getValor_06());
            st.setString(8, bean.getValor_07());
            st.setString(9, bean.getValor_08());
            st.setString(10, bean.getValor_09());
            st.setString(11, bean.getValor_10());
            st.setString(12, bean.getValor_11());
            st.setString(13, bean.getValor_12());
            sql = st.getSql();
        }
        catch (Exception e) {
            throw new Exception("Error en insertar en CiclosFacturacionDAO.java: "+e.toString());
        }
        return sql;
    }

    /**
     * Genera el codigo para eliminar los datos de ciclos de un a&ntilde;o
     * @param anio A&ntilde;o a eliminar
     * @return Cadena con el codigo generado
     * @throws Exception Cuando hay error
     */
    public String eliminar(String anio) throws Exception{
        StringStatement st = null;
        String query = "SQL_DEL";
        String sql="";
        try {
            sql = this.obtenerSQL(query);
            st = new StringStatement(sql,true);
            st.setString(1, anio);
            sql = st.getSql();
        }
        catch (Exception e) {
            throw new Exception("Error en eliminar en CiclosFacturacionDAO.java: "+e.toString());
        }
        return sql;
    }

    /**
     * Ejecuta el codigo que se le indique
     * @param sql Codigo a ejecutar
     * @return Numero de filas afectadas
     * @throws Exception Cuando hay error
     */
    public int ejecutarSQL(String sql) throws Exception{
        int rows = 0;
        PreparedStatement st = null;
        Connection con = null;
        try {
            con = this.conectarBDJNDI("fintra");
            boolean autocommit = con.getAutoCommit();
            con.setAutoCommit(false);
            st = con.prepareStatement(sql);
            rows = st.executeUpdate();
            con.commit();
            con.setAutoCommit(autocommit);
        }
        catch (Exception e) {
            con.rollback();
            throw new Exception("Error en ejecutarSQL en CiclosFacturacionDAO.java: "+e.toString());
        }
        finally {
            if(st!=null){ try{st.close();} catch(Exception e){ throw new Exception("Error cerrando el statement: "+e.toString()); } }
            if(!con.isClosed()){ try{con.close();} catch(Exception e){ throw new Exception("Error cerrando la conexion: "+e.toString()); } }
        }
        return rows;
    }
}