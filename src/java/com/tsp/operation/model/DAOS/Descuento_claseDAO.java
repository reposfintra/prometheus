/************************************************************************************
 * Nombre clase: Descuento_claseDAO.java                                            *
 * Descripci�n: Clase que maneja las consultas de los descuentos por clase equipo.  *
 * Autor: Ing. Jose de la rosa                                                      *
 * Fecha: 7 de diciembre de 2005, 11:07 AM                                          *
 * Versi�n: Java 1.0                                                                *
 * Copyright: Fintravalores S.A. S.A.                                          *
 ***********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class Descuento_claseDAO extends MainDAO{
    private Descuento_clase descuento_clase;
    private Vector vecdescuento_clase;
    
    private static final String SQL_INSERT = "";
    
    private static final String SQL_SERCH = "";
    
    private static final String SQL_LIST = "";
    
    private static final String SQL_UPDATE = "";
    
    private static final String SQL_ANULAR = "";
    
    private static final String SQL_SEARCH_DETALLES = "";
    
    private static final String SQL_EXISTE_CLASE = "";
    
    /** Creates a new instance of descuento_claseDAO */
    public Descuento_claseDAO () {
        super ( "Descuento_claseDAO.xml" );
    }
    /**
     * Metodo: getDescuento_clase, permite retornar un objeto de registros de descuento clase.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Descuento_clase getDescuento_clase () {
        return descuento_clase;
    }
    
    /**
     * Metodo: setDescuento_clase, permite obtener un objeto de registros de descuento clase.
     * @autor : Ing. Jose de la rosa
     * @param : objeto
     * @version : 1.0
     */
    public void setDescuento_clase (Descuento_clase descuento_clase) {
        this.descuento_clase = descuento_clase;
    }
    
    /**
     * Metodo: getDescuento_clases, permite retornar un vector de registros de descuento clase.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getDescuento_clases () {
        return vecdescuento_clase;
    }
    
    /**
     * Metodo: setDescuento_clases, permite obtener un vector de registros de descuento clase.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setDescuento_clases (Vector vecdescuento_clase) {
        this.vecdescuento_clase = vecdescuento_clase;
    }
    
    /**
     * Metodo: insertDescuento_clase, permite ingresar un registro a la tabla Descuento_clase.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void insertDescuento_clase () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_INSERT";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1,descuento_clase.getClase_equipo ());
            st.setString (2,descuento_clase.getCodigo_concepto ());
            st.setFloat (3,descuento_clase.getPorcentaje_descuento ());
            st.setString (4,descuento_clase.getFrecuencia ());
            st.setDouble (5,descuento_clase.getValor_mes ());
            st.setString (6,descuento_clase.getMes_proceso ());
            st.setString (7,descuento_clase.getUsuario_creacion ());
            st.setString (8,descuento_clase.getUsuario_modificacion ());
            st.setString (9,descuento_clase.getBase ());
            st.setString (10,descuento_clase.getDistrito ());
            st.executeUpdate ();
        }}catch(SQLException ex){
            throw new SQLException ("ERROR AL INGRESAR UN DESCUENTO POR CLASE DE EQUIPO, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo: searchDescuento_clase, permite buscar un descuento clase dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */
    public void searchDescuento_clase (String clase, String distrito, String concepto )throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SERCH";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1,clase);
            st.setString (2,distrito);
            st.setString (3,concepto);
            rs= st.executeQuery ();
            while(rs.next ()){
                descuento_clase = new Descuento_clase ();
                descuento_clase.setClase_equipo (rs.getString ("clase_equipo"));
                descuento_clase.setCodigo_concepto (rs.getString ("codigo_concepto"));
                descuento_clase.setMes_proceso (rs.getString ("mes_proceso"));
                descuento_clase.setFrecuencia (rs.getString ("frecuencia"));
                descuento_clase.setValor_mes (rs.getDouble ("valor_mes"));
                descuento_clase.setPorcentaje_descuento (rs.getFloat ("porcentaje_descuento"));
            }
        }}catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR UN DESCUENTO POR CLASE DE EQUIPOv, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo: existDescuento_clase, permite buscar un descuento clase dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */
    public boolean existDescuento_clase (String clase, String distrito, String concepto) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_SERCH";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1,clase);
            st.setString (2,distrito);
            st.setString (3,concepto);
            rs = st.executeQuery ();
            if (rs.next ()){
                sw = true;
            }
        }}catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR SI EXISTEN LOS DESCUENTOS POR CLASE DE EQUIPOS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }
    
    /**
     * Metodo: existClase, permite buscar un descuento clase dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */
    public void existeClase (String clase, String frecuencia, String distrito, String placa, String cliente) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        vecdescuento_clase = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE_CLASE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1,cliente);
            st.setString (2,distrito);
            st.setString (3,clase);
            st.setString (4,placa);
            st.setString (5,frecuencia);
            rs = st.executeQuery ();
            vecdescuento_clase = new Vector ();
            while(rs.next ()){
                descuento_clase = new Descuento_clase ();
                descuento_clase.setClase_equipo (rs.getString ("clase_equipo"));
                descuento_clase.setCodigo_concepto (rs.getString ("codigo_concepto"));
                descuento_clase.setMes_proceso (rs.getString ("mes_proceso"));
                descuento_clase.setFrecuencia (rs.getString ("frecuencia"));
                descuento_clase.setValor_mes (rs.getDouble ("valor_mes"));
                descuento_clase.setPorcentaje_descuento (rs.getFloat ("porcentaje_descuento"));
                vecdescuento_clase.add (descuento_clase);
            }
        }}catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR SI EXISTEN LOS DESCUENTOS POR CLASE DE EQUIPOS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo: listDescuento_clase, permite listar todas los descuento clasees
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void listDescuento_clase (String distrito)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LIST";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1,distrito);
            rs= st.executeQuery ();
            vecdescuento_clase = new Vector ();
            while(rs.next ()){
                descuento_clase = new Descuento_clase ();
                descuento_clase.setClase_equipo (rs.getString ("clase_equipo"));
                descuento_clase.setCodigo_concepto (rs.getString ("codigo_concepto"));
                descuento_clase.setMes_proceso (rs.getString ("mes_proceso"));
                descuento_clase.setFrecuencia (rs.getString ("frecuencia"));
                descuento_clase.setValor_mes (rs.getDouble ("valor_mes"));
                descuento_clase.setPorcentaje_descuento (rs.getFloat ("porcentaje_descuento"));
                vecdescuento_clase.add (descuento_clase);
            }
        }}catch(SQLException ex){
            throw new SQLException ("ERROR AL LISTAR LOS DESCUENTOS POR CLASE DE EQUIPOS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo: updateDescuento_clase, permite actualizar un descuento clase dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario, la placa y usuario.
     * @version : 1.0
     */
    public void updateDescuento_clase () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_UPDATE";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setFloat (1,descuento_clase.getPorcentaje_descuento ());
            st.setString (2,descuento_clase.getFrecuencia ());
            st.setDouble (3,descuento_clase.getValor_mes ());
            st.setString (4,descuento_clase.getMes_proceso ());
            st.setString (5,descuento_clase.getUsuario_modificacion ());
            st.setString (6,descuento_clase.getClase_equipo ());
            st.setString (7,descuento_clase.getDistrito ());
            st.setString (8,descuento_clase.getCodigo_concepto ());
            st.executeUpdate ();
        }}catch(SQLException ex){
            throw new SQLException ("ERROR AL ACTUALIZAR LOS DESCUENTOS POR CLASE DE EQUIPOS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo: anularDescuento_clase, permite anular una descuento clase.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public void anularDescuento_clase () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ANULAR";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1,descuento_clase.getUsuario_modificacion ());
            st.setString (2,descuento_clase.getClase_equipo ());
            st.setString (3,descuento_clase.getDistrito ());
            st.setString (4,descuento_clase.getCodigo_concepto ());
            st.executeUpdate ();
            }}catch(SQLException ex){
            throw new SQLException ("ERROR AL ANULAR LOS DESCUENTOS POR CLASE DE EQUIPOS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo: searchDetalleDescuento_clase, permite listar descuento clasees por detalles dados unos parametros.
     * @autor : Ing. Jose de la rosa
     * @param : numero de nit, nombre del propietario y la placa.
     * @version : 1.0
     */
    public void searchDetalleDescuento_clase (String clase, String codigo, String frecuencia, String mes, String distrito) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SEARCH_DETALLES";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString (1, clase+"%");
            st.setString (2, codigo+"%");
            st.setString (3, frecuencia+"%");
            st.setString (4, mes+"%");
            st.setString (5, distrito);
            rs = st.executeQuery ();
            vecdescuento_clase = new Vector ();
            while(rs.next ()){
                descuento_clase = new Descuento_clase ();
                descuento_clase.setClase_equipo (rs.getString ("clase_equipo"));
                descuento_clase.setCodigo_concepto (rs.getString ("codigo_concepto"));
                descuento_clase.setMes_proceso (rs.getString ("mes_proceso"));
                descuento_clase.setFrecuencia (rs.getString ("frecuencia"));
                descuento_clase.setValor_mes (rs.getDouble ("valor_mes"));
                descuento_clase.setPorcentaje_descuento (rs.getFloat ("porcentaje_descuento"));
                vecdescuento_clase.add (descuento_clase);
            }
        }}catch(SQLException ex){
            throw new SQLException ("ERROR AL BUSCAR LOS DESCUENTOS POR CLASE DE EQUIPOS, "+ex.getMessage ()+", "+ex.getErrorCode ());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
}
