/*
 * Nombre        UbicacionVehicularPropietarioDAO.java
 * Descripci�n   Clase que maneja todas las consultas para el
 *               reporte de ubicacion vehicular Propietario
 * Autor         Ivan Dario Gomez Vanegas
 * Fecha         31 de enero de 2006, 9:25
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import java.util.LinkedList;
import java.util.Vector;
import java.util.Iterator;
import java.util.Hashtable;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Ivan Dario Gomez Vanegas
 */
public class UbicacionVehicularPropietarioDAO extends MainDAO {
    
    /** Creates a new instance of UbicacionVehicularPropietarioService */
    public UbicacionVehicularPropietarioDAO() {
        super("UbicacionVehicularPropietarioDAO.xml");
    }
    
    private String []  camposReporte;
    private LinkedList datosReporte;
    private Vector infoFinanzas;
    private Hashtable infoPlanilla;
    
    /**
     * Metodo   : BuscarPlacas, retorna una lista de placas del propirtario.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : los nit del propietario
     * @version : 1.0
     */
    public List BuscarPlacas(String nitPro) throws SQLException{
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        List         listado    = null;
        Connection       con    = null;
        try {
            String sql = this.obtenerSQL("SQL_PLACAS_PROPIETARIO");
            sql = sql.replaceAll("#nitpro#",nitPro);
            con = this.conectarJNDI("SQL_PLACAS_PROPIETARIO");//JJCastro fase2
            if(con!=null){
            st = con.prepareStatement(sql);
            
            rs= st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    Placa  placa  = new Placa();
                    placa.setPlaca(rs.getString(1));
                    listado.add(placa);
                }
            }
        }}catch(SQLException e) {
            throw new SQLException("Error en rutina BuscarPlaca  [UbicacionVehicularPropietarioDAO]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listado;
    }
    
    
    /**
     * Metodo   : BuscarPlacasADMIN, retorna una lista de placas Para un Administrador.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   :
     * @version : 1.0
     */
    public List BuscarPlacasADMIN() throws SQLException{
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        List         listado    = null;
        String query = "SQL_PLACAS_ADMIN";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs= st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    Placa  placa  = new Placa();
                    placa.setPlaca(rs.getString(1));
                    listado.add(placa);
                }
            }
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina BuscarPlacasADMIN  [UbicacionVehicularPropietarioDAO]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listado;
    }
    
    
    /**
     * Metodo   : buscarDatosReporte, Metodo que busca los datos del reporte.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : fecha inicial, fecha final, placa, tipo de viaje, estado del viaje y el usuario
     * @version : 1.0
     */
    public void buscarDatosReporte(String Fecini, String Fecfin, String Placas,String listaTipoViaje, String estadoViajes, String User )throws SQLException {
        Connection con = null;
        Statement ps = null;
        ResultSet rs = null;
        
        try {
            String sql = this.obtenerSQL("SQL_PRINCIPAL");
            sql = sql.replaceAll("#LISTAPLACAS#", Placas);
            sql = sql.replaceAll("#FECINI#",Fecini );
            sql = sql.replaceAll("#FECFIN#",Fecfin );
            if (!estadoViajes.equals("TODOS")) {
                
                if (estadoViajes.equals("RUTA")) {
                    sql = sql.replaceAll("#ESTADO#"," get_ultimoreportetrafico(pl.numpla) <> '' AND " +
                    " get_ultimoreportetrafico(pl.numpla) <> '-' AND " +
                    " SUBSTR(get_ultimoreportetrafico(pl.numpla)," +
                    " LENGTH(get_ultimoreportetrafico(pl.numpla)) - 1, 2) <> '-E' AND "
                    );
                    
                }else if (estadoViajes.equals("CONENTREGA")) {
                    sql = sql.replaceAll("#ESTADO#"," SUBSTR(get_ultimoreportetrafico(pl.numpla)," +
                    " LENGTH(get_ultimoreportetrafico(pl.numpla)) - 1, 2) = '-E' AND "
                    );
                }else if (estadoViajes.equals("PORCONF")) {
                    sql = sql.replaceAll("#ESTADO#"," (get_ultimoreportetrafico(pl.numpla) = '' OR" +
                    " get_ultimoreportetrafico(pl.numpla) = '-') AND "
                    );
                }
            }else{
                sql = sql.replaceAll("#ESTADO#", "");
            }
            
            int tipo = Integer.parseInt(listaTipoViaje);
            switch (tipo){
                case 1: sql = sql.replaceAll("#CONDICION#"," pl.despla != '' ORDER BY pl.plaveh,  pl.fecdsp desc"); break;
                case 2: sql = sql.replaceAll("#CONDICION#"," rm.tipoviaje = 'NA' ORDER BY pl.plaveh,  pl.fecdsp desc"); break;
                case 3: sql = sql.replaceAll("#CONDICION#"," rm.tipoviaje = 'RM' ORDER BY pl.plaveh,  pl.fecdsp desc"); break;
                case 4: sql = sql.replaceAll("#CONDICION#"," rm.tipoviaje = 'DM' ORDER BY pl.plaveh,  pl.fecdsp desc"); break;
                case 5: sql = sql.replaceAll("#CONDICION#"," rm.tipoviaje = 'RC' ORDER BY pl.plaveh,  pl.fecdsp desc"); break;
                case 6: sql = sql.replaceAll("#CONDICION#"," rm.tipoviaje = 'DC' ORDER BY pl.plaveh,  pl.fecdsp desc"); break;
                case 7: sql = sql.replaceAll("#CONDICION#"," rm.tipoviaje = 'RE' ORDER BY pl.plaveh,  pl.fecdsp desc"); break;
                
            }
            
             con = this.conectarJNDI("SQL_PRINCIPAL");//JJCastro fase2
            if (con != null) {
                ps = con.createStatement();
                rs = ps.executeQuery(sql);

                buscarCamposDeReporte("(default)", User);
                String camposReporte[] = obtenerCamposDeReporte();
                datosReporte = new LinkedList();
                while (rs.next()) {
                    UbicacionVehicularPropietario uvp = UbicacionVehicularPropietario.load(rs, camposReporte);

                    //apayares 2006-03-30
                    uvp.establecerValor("base", rs.getString("base"));
                    uvp.establecerValor("estado", "A".equals(rs.getString("reg_status")) ? "Anulada" : uvp.obtenerValor("estado"));
                    datosReporte.add(uvp);
                }

            }
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    
    /**
     * Metodo   : BuscarPropietarios, retorna una lista de propirtarios.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   :
     * @version : 1.0
     */
    public List BuscarPropietarios() throws SQLException{
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        List         listado    = null;
        String query = "SQL_PROPIETARIOS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs= st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                while(rs.next()){
                    Usuario  usuario  = new Usuario();
                    usuario.setNombre(rs.getString(1));
                    usuario.setLogin(rs.getString(2));
                    listado.add(usuario);
                }
            }
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina BuscarPropietarios  [UbicacionVehicularPropietarioDAO]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listado;
    }
    
    
    /**
     * Metodo   : BuscarNits, retorna una lista con los nit del propietario.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : el id del usuario
     * @version : 1.0
     */
    public List BuscarNits(String idUsuario) throws SQLException{
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        List         listado    = null;
        String query = "SQL_NITS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, idUsuario);
            rs= st.executeQuery();
            if(rs!=null){
                listado = new LinkedList();
                if(rs.next()){
                    Usuario  usuario  = new Usuario();
                    usuario.setNombre(rs.getString(1));
                    usuario.setLogin(rs.getString(2));
                    usuario.setNitPropietario(rs.getString(3));
                    listado.add(usuario);
                }
            }
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina BuscarNits  [UbicacionVehicularPropietarioDAO]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listado;
    }
    
    /**
     * Metodo   : BuscarNits, retorna una lista con los nit del propietario.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : el id del usuario
     * @version : 1.0
     */
    public String BuscarNitsPropietario(String idUsuario) throws SQLException{
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        String            NitPropietario = "";
        String query = "SQL_NIT_PROPIETARIO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, idUsuario);
            rs= st.executeQuery();
            if(rs!=null){
                if(rs.next()){
                    NitPropietario = rs.getString(1);
                }
            }
        }}catch(SQLException e) {
            throw new SQLException("Error en rutina BuscarNitsPropietario  [UbicacionVehicularPropietarioDAO]... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return NitPropietario;
    }
    
    
    /**
     * Metodo   : UpdateNit, actualiza el campo datos de la tabla perfilusuarios.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : Los nit del usuario y el id del usuario
     * @version : 1.0
     */
    public void UpdateNit(String Nits,String idUsuario) throws SQLException{
        Connection con = null;
        PreparedStatement st    = null;
        String query = "SQL_UPDATE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Nits);
            st.setString(2, idUsuario);
            ////System.out.println(st);
            st.executeUpdate();
            
        }} catch(SQLException e) {
            throw new SQLException("Error en rutina UpdateNit  [UbicacionVehicularPropietarioDAO]... \n"+e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     * Este m�todo permite obtener los titulos del encabezado del reporte de
     * informaci�n al cliente. El Hashtable que es devuelto contiene los titulos de todos
     * los campos del reporte y la clave de cada titulo es su respectivo nombre
     * de campo en la Base de datos, para obtener los nombre de los campos del
     * reporte utilize el metodo <CODE>String [] obtenerCamposDeReporte()</CODE>.
     * @throws SQLException Si algun problema ocurre en el acceso a la Base de Datos.
     * @return Una tabla con los titulos de los campos del reporte de informaci�n al cliente.
     */
    public Hashtable obtenerTitulosDeReporte() throws SQLException{
        ConsultasGeneralesDeReportesDAO cgrd = new ConsultasGeneralesDeReportesDAO();
        return cgrd.obtenerTitulosDeReporte("ubicacionVehProp");
    }
    
    /**
     * Este m�todo solo puede ser accesado dentro del DAO, y permite buscar los
     * nombres de los campos del reporte ubicacionVehicular en la base de datos. Es
     * invocado por el m�todo <code>void buscarDatosDeReporte(String [])</code> y el resultado
     * de la busqueda el devuelto por el m�todo
     * <CODE>String [] obtenerCamposReporte()</CODE>.
     * @param codigoCliente El codigo del cliente que ver� el reporte, este codigo es obtenido del usuario
     * que est� actualmente activo en la sesi�n.
     * @param codigoReporte El codigo del reporte de devoluciones.
     * @throws SQLException Si algun problema ocurre con el acceso a la Base de datos.
     */
    private void buscarCamposDeReporte(String codigoCliente, String loggedUser) throws SQLException{
        ConsultasGeneralesDeReportesDAO cgr = new ConsultasGeneralesDeReportesDAO();
        camposReporte = cgr.buscarCamposDeReporte(codigoCliente, "ubicacionVehProp", "C", loggedUser );
        
    }
    
    /**
     * Devuelve un array de tipo String [] donde se encuentran almacenados los nombres
     * de los campos del reporte de informaci�n al cliente.
     * @return El array con los nombres de los campos del reporte de informaci�n al cliente.
     */
    public String [] obtenerCamposDeReporte(){
        return camposReporte;
    }
    /**
     * Devuelve una lista con los datos del reporte
     * @return El array con los nombres de los campos del reporte de informaci�n al cliente.
     */
    public LinkedList obtenerDatosReporte(){
        return datosReporte;
    }
    
    /**
     * Busca la informaci�n financiera del viaje.
     * @param numpla La planilla a la cual se le buscar� la informaci�n financiera.
     * @throws SQLException Si algun error ocurre en la base de datos
     */
    public void buscarInformacionFinanciera(String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        String query = "SQL_INFO_FINANZAS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, numpla);
            rs= st.executeQuery();
            infoFinanzas = new Vector();
            while(rs.next()){
                Hashtable fila = new Hashtable();
                fila.put("documento",rs.getString("documento"));
                fila.put("item",rs.getString("item"));
                fila.put("descripcion",rs.getString("descripcion"));
                fila.put("valor",rs.getString("vlr"));
                fila.put("valor_moneda",rs.getString("vlr_me"));
                fila.put("impuesto",rs.getString("impuesto"));
                fila.put("retencion",rs.getString("retencion"));
                fila.put("moneda",rs.getString("moneda"));
                infoFinanzas.addElement(fila);
                fila = null;//Liberar Espacio JJCastro
            }
            this.buscarInfoPlanilla(numpla);
        }}catch(SQLException e) {
            throw new SQLException("Error en buscarInformacionFinanciera ... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Devuelve la informaci�n financiera buscada por el m�todo buscarInformacionFinanaciera
     * @return EL vector que contiene las filas con la informaci�n de cada factura o pagos realizados
     * para ese viaje.
     * @autor Alejandro Payares
     */
    public Vector obtenerInformacionFinanciera(){
        return infoFinanzas;
    }
    
    /**
     * Busca la la fecha y el nombre del conductor de la planilla dada
     * @param numpla el numero de la planilla
     * @throws SQLException Si algun error ocurre en la base de datos
     * @autor Alejandro Payares
     */    
    public void buscarInfoPlanilla(String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        String query = "SQL_OBTENER_INFO_PLANILLA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, numpla);
            rs= st.executeQuery();
            infoPlanilla = new Hashtable();
            if(rs.next()){
                infoPlanilla.put("fecpla",rs.getString("fecpla"));
                infoPlanilla.put("fecdsp",rs.getString("fecdsp"));
                infoPlanilla.put("conductor",rs.getString("conductor"));
            }
        }}catch(SQLException e) {
            throw new SQLException("Error en buscarInfoPlanilla ... \n"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Devuelve la informaci�n buscada por el m�todo buscarInfoPlanilla
     * @return Un Hashtable que contiene la fecha, fecha de despacho y nombre del conductor de la planilla
     * @autor Alejandro Payares
     */    
    public Hashtable getInfoPlanilla(){
        return infoPlanilla;
    }



  /*Belleza8
    public String separarPorMiles(double valor){
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        try {
            st = crearPreparedStatement("SQL_FORMAT_NUMBER");
            st.setDouble(1, valor);
            rs= st.executeQuery();
            if(rs.next()){
                return rs.getString("valor");
            }
            return ""+valor;
        }
        catch( Exception e ){
            return ""+valor;
        }
        finally {
            try {
                if(st!=null)  st.close();
                desconectar("SQL_FORMAT_NUMBER");
            }
            catch( Exception ex ){}

        }
    }

   /**
     * Separa por miles un numero tipo double
     * @param valor El valor a formatear
     * @return El valor formateado.
     * @Modificado Ing. Jose Castro
   */
    public String separarPorMiles(double valor) throws Exception{
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        String query = "SQL_FORMAT_NUMBER";
        String value =  ""+valor;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setDouble(1, valor);
            rs= st.executeQuery();
            if(rs.next()){
                value = rs.getString("valor");
            }
        }
        }catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE SQL_GET_CLENTE \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return value;
    }




}
