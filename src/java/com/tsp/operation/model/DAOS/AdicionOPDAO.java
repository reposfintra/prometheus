/***************************************
 * Nombre Clase ............. AdicionOPDAO.java
 * Descripci�n  .. . . . . .  Generamos facturas para los movimeintos de la planilla con OP que no esten incluidos en la OP y no contabilizados
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  12/11/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/




package com.tsp.operation.model.DAOS;




import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.TasaService;
import com.tsp.util.*;




public class AdicionOPDAO extends MainDAO{
    
    
    
    private final  String  CODIGO_ANTICIPOS    = "003";
    private final  String  CLASE_DOCUMENTO     = "4";
    
    private  OpDAO   OPDao  =  new OpDAO();

    
    
    
    
    
    
    
    
    public AdicionOPDAO() {
        super("AdicionOPDAO.xml");
    }
    
    
    
    public String reset(String val){
        return (val==null)?"":val;
    }
    
    
    
    
    
    /**
     * M�todo busca las planillas con OP y que tenga con movimiento pendientes en movpla
     * @autor   fvillacob
     * @throws  Exception
     **/
    public List getOCMovPendientes(String distrito) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String            query     = "SQL_BUSCAR_PLANILLAS";
        try {
            
            OpDAO          OPDAO  = new  OpDAO();
            
            ChequeXFacturaDAO dao = new ChequeXFacturaDAO();
            String monedaLocal    = dao.getMonedaLocal(distrito);
            
            st= crearPreparedStatement(query);
            st.setString(1, distrito );
            rs=st.executeQuery();
            while(rs.next()){
                OP  op  =  loadOp(rs);
                op.setMonedaLocal( monedaLocal );
                
             
                String  msj = "";
                
               // Validamos si la planilla tiene Discrepancia   
                if( OPDAO.getDiscrepancia( op.getOc() ) )
                     msj = " Presenta discrepancia ";
                
               // Validamos si la placa esta vetada  
                if(  OPDAO.getVetoPlaca(  op.getPlaca()  )  )
                     msj += "La placa " +  op.getPlaca()  +" se encuentra vetada ";
            
                
                op.setComentario( msj );                
                
                lista.add(op);
            }
            
            System.gc();
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getOCMovPendientes -->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return lista;
    }
    
    
    /**
     * M�todo busca las planillas con OP y que tenga con movimiento pendientes en movpla
     * @autor   fvillacob
     * @throws  Exception
     **/
    public List getMovPendientesOC(String distrito, String oc, String proveedor) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String            query     = "SQL_BUSCAR_MOV_PLANILLAS";
        try {
            
            OpDAO  dao = new OpDAO();
            
            st= crearPreparedStatement(query);
            st.setString(1, oc);
            rs=st.executeQuery();
            int  noItem = 1;
            while(rs.next()){
               
                OPItems  item = new OPItems();
                    item.setItem         ( Utility.rellenar( String.valueOf(noItem) , 3 ) );
                    item.setDstrct       ( this.reset   ( rs.getString("distrito"))       );
                    item.setConcepto     ( this.reset   ( rs.getString("concepto"))       );
                    item.setDescripcion  ( this.reset   ( rs.getString("descripcion"))    );
                    item.setVlr          (                rs.getDouble("valor")           );
                    item.setMoneda       ( this.reset   ( rs.getString("moneda")     )    );
                    item.setAsignador    ( this.reset   ( rs.getString("asignador")  )    );
                    item.setIndicador    ( this.reset   ( rs.getString("indicador")  )    );                    
                    item.setAgencia      ( this.reset   ( rs.getString("agencia")    )    );
                    item.setCodigo_cuenta( this.reset   ( rs.getString("cuenta")     )    );
                    item.setPlanilla     ( oc                                             );
                    item.setDescconcepto ( item.getDescripcion()                          );
                    
                    
               // Codigo ABC:    
                    String  ageRelacionada = dao.getAGERelacionada( item.getAgencia() );                
                    item.setCodigo_abc   ( dao.getABC  ( ageRelacionada               )    );                 
                    
                // Cuenta para los anticipos:
                    if(  item.getConcepto().equals("01") ){
                         String cta  =  dao.getCuenta( distrito,  proveedor ,  CODIGO_ANTICIPOS , oc  );
                         item.setCodigo_cuenta( cta );
                    }
                    
               lista.add( item );
               noItem ++;
               
            }
            
            System.gc();
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getMovPendientesOC -->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return lista;
    }
    
   
    
     /**
     * M�todo loadOP, carga datos obtneidos
     * @autor   fvillacob
     * @throws  Exception
     * @version 1.0.
     ********************************************************************************************************************
     * @ Este m�todo esta clonado en LiquidarOCDAO.java si lo modifican por favor avisar al programador de Fintra       * 
     * o en su defecto modificarlo tambi�n. La modificaci�n de este m�todo afecta de importante manera la forma         *
     * como se cargan las planillas para los pronto pago ya que utiliza m�todos afines                                  *
     ********************************************************************************************************************/
    
    public OP  loadOp(ResultSet  rs)throws Exception{
        OP  op = new OP();
        try{
               String ANULADO = "RA";   // Recuperaci�n de Anticipo
               String ADICION = "AOP";  // Adicion a la OP
            
            
               op.setOc                   (  reset(  rs.getString("oc")          )  );  
               op.setDstrct               (  reset(  rs.getString("distrito")    )  );  
               op.setProveedor            (  reset(  rs.getString("propietario") )  );
               op.setPlaca                (  reset(  rs.getString("placa")       )  );
               op.setFechaCumplido        (  reset(  rs.getString("feccum")      )  );
               op.setOrigenOC             (  reset(  rs.getString("origenoc")    )  );
               op.setBase                 (  reset(  rs.getString("base")        )  );
               op.setId_mims              (  reset(  rs.getString("idmims")      )  );
               op.setAgencia              (  reset(  rs.getString("agencia")     )  );
               op.setBanco                (  reset(  rs.getString("banco")       )  );
               op.setSucursal             (  reset(  rs.getString("sucursal")    )  );
               op.setMoneda               (  reset(  rs.getString("monedabanco") )  );
               op.setHandle_code          (  reset(  rs.getString("handlecode")  )  );
               op.setReteFuente           (  reset(  rs.getString("retefuente")  )  );
               op.setReteIca              (  reset(  rs.getString("reteica")     )  );
               op.setReteIva              (  reset(  rs.getString("reteiva")     )  );
               op.setRetenedor            (  reset(  rs.getString("agente_retenedor")    ) );
               op.setPlazo                (          rs.getInt   ("plazo")                 );
               op.setMoneda               (  reset(  rs.getString("monedabanco")         ) );               
               op.setFecha_documento      (  reset(  rs.getString("fechaDocumento")      ) );
               op.setFecha_vencimiento    (  reset(  rs.getString("fechaVencimiento")    ) ); 
               
               op.setDescripcion          ( "RECUPERACION ANT " + op.getOc()               );
               op.setDocumento            ( ANULADO +  op.getOc()                          );
               
               op.setDocumento_relacionado( reset(  rs.getString("factura")              ) );
               op.setSaldo                (         rs.getDouble("saldo")                  ); // Saldo de la factura
               op.setTipo_documento_rel   ( "010"                                          );
               op.setTipo_documento       ( ""                                             );
               op.setClase_documento      ( CLASE_DOCUMENTO                                );  // Tipo 4.
               
               //Jescandon 05/03/07
               op.setEstado              ( reset(   rs.getString("estado")                 ));
               
               op.setVlrOc               (  rs.getDouble("valorOC")                         );
               
               if( op.getEstado().equals("C")  ){
                     op.setDocumento    ( ADICION +  op.getOc()  );
                     op.setDescripcion  ( "ADICION DE OP " + reset(  rs.getString("factura") ) );                   
               }
                   
                              
               String  numdoc  =         OPDao.buscarNumeroFactura( op.getDstrct(), op.getProveedor(), op.getDocumento() );
               op.setDocumento( numdoc   );
               
               
               
             // Si es carb�n, buscamos el banco asignado al nit
                if(  op.getHandle_code().equals("FC")    ){
                     Hashtable bk =  this.OPDao.bancoNit( op.getDstrct(),  op.getProveedor() ,  op.getHandle_code()  );
                     if(  bk!=null  ){
                          String  ban  =  (String) bk.get("banco");
                          String  suc  =  (String) bk.get("sucursal");
                          String  mon  =  (String) bk.get("moneda");

                          op.setBanco    ( ban );
                          op.setSucursal ( suc );
                          op.setMoneda   ( mon );           
                     }                 
                }
               
               
               
               
               
               
            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return op;
    }

    /**
     * M�todo busca las planillas con OP y que tenga con movimiento pendientes en movpla
     * @autor   fvillacob
     * @throws  Exception
     **/
    public List getMovPendientesOC(String distrito, String oc, String proveedor, String cmc) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = new LinkedList();
        String            query     = "SQL_BUSCAR_MOV_PLANILLAS";
        try {
            
            String SEPARADOR_PLCTA   = ";";
            OpDAO  dao = new OpDAO();
            
            st= crearPreparedStatement(query);
            st.setString(1, oc       );
            st.setString(2, distrito );
            rs=st.executeQuery();
            int  noItem = 1;
            while(rs.next()){
               
                OPItems  item = new OPItems();
                    item.setItem         ( Utility.rellenar( String.valueOf(noItem) , 3 ) );
                    item.setDstrct       ( this.reset   ( rs.getString("distrito"))       );
                    item.setConcepto     ( this.reset   ( rs.getString("concepto"))       );
                    item.setDescripcion  ( this.reset   ( rs.getString("descripcion"))    );
                    item.setVlr          (                rs.getDouble("valor")           );
                    item.setMoneda       ( this.reset   ( rs.getString("moneda")     )    );
                    item.setAsignador    ( this.reset   ( rs.getString("asignador")  )    );
                    item.setIndicador    ( this.reset   ( rs.getString("indicador")  )    );                    
                    item.setAgencia      ( this.reset   ( rs.getString("agencia")    )    );                   
                    item.setPlanilla     ( oc                                             );
                    item.setDescconcepto ( item.getDescripcion()                          );
                    
                    String  proveedorMov =  reset( rs.getString("proveedor_anticipo")     ); 
                    String  cta          =  reset( rs.getString("cuenta")                 );  // PL get_codigocuenta, realizado por Ivan Gomez.
                    String  aux          =  "";                
               
                    
               // Codigo ABC:    
                    String  ageRelacionada = dao.getAGERelacionada( item.getAgencia() );                
                    item.setCodigo_abc   ( dao.getABC  ( ageRelacionada          )    );   
                    
                    
                // Cuenta para los anticipos entregados por TSP:
                   if( ( item.getConcepto().equals("01")  || item.getConcepto().equals("02") || item.getConcepto().equals("03")   )  &&   
                       ( proveedorMov.equals( dao.SANCHEZ_POLO )  ||   proveedorMov.equals("")  ) 
                       ){
                         cta  = dao.getCuentaCMC( distrito, cmc , dao.CODIGO_ANTICIPOS , oc  );                     
                   }else{
                         if( !cta.equals("")   ){
                              String vecCTA[]  =  cta.split( SEPARADOR_PLCTA );
                              if( vecCTA.length>1  ){
                                  cta = reset( vecCTA[0] );    // Cuanta
                                  aux = reset( vecCTA[1] );    // Auxiliar
                              }
                         }
                   } 
                    
                   item.setCodigo_cuenta( cta );
                   item.setAuxiliar     ( aux );
                    
                    
               lista.add( item );
               noItem ++;
               
            }
            
            System.gc();
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getMovPendientesOC -->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            desconectar(query);
        }
        return lista;
    }
    
    
}