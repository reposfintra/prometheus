/*
 * Nombre        Tipo_ubicacionDAO.java
 * Autor         Ing. Jose de la Rosa
 * Fecha         17 Junio 2005
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;


public class Tipo_ubicacionDAO extends MainDAO{
    private Tipo_ubicacion tipo_ubicacion;
    private Vector tipo_ubicaciones;
        
    private String TU_BUSCAR = "SELECT  table_code as cod_tubicacion, " +
    "                                   descripcion as desc_tubicacion " +
    "                           FROM    tablagen " +
    "                           WHERE   table_type = 'TUBICACION' " +
    "                                   table_code = ? " +
    "                                   AND reg_status != 'A'";
    
    private String TU_LISTAR = "SELECT  table_code as cod_tubicacion, " +
    "                                   descripcion as desc_tubicacion " +
    "                           FROM    tablagen " +
    "                           WHERE   table_type = 'TUBICACION' " +
    "                                   AND reg_status != 'A' " +
    "                           ORDER BY table_code";
    
    
    /** Creates a new instance of Tipo_ubicacionDAO */
    public Tipo_ubicacionDAO() {
        super("Tipo_ubicacionDAO.xml");//JJCastro fase2
    }
     
    /**
     * Metodo <tt>getTipo_ubicaciones</tt>, obtiene el vector de ubicaciones
     * @autor : Ing. Jose de la Rosa
     * @return Vector     
     * @version : 1.0
     */
    public Vector getTipo_ubicaciones() {
        return tipo_ubicaciones;
    }
    
    /**
     * Metodo <tt>setTipo_ubicaciones</tt>, instancia el vector de ubicaciones
     * @autor : Ing. Jose de la Rosa
     * @param Vector     
     * @version : 1.0
     */
    public void setTipo_ubicaciones(Vector tipo_ubicaciones) {
        this.tipo_ubicaciones = tipo_ubicaciones;
    }
    
    /**
     * Metodo <tt>searchTipo_ubicacion</tt>, obtiene un tipo de ubicacion dado el codigo
     * @autor : Ing. Jose de la Rosa
     * @return Vector     
     * @version : 1.0
     */
    public void searchTipo_ubicacion(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "TU_BUSCAR";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,cod);
                rs= st.executeQuery();
                tipo_ubicaciones = new Vector();
                while(rs.next()){
                    tipo_ubicacion = new Tipo_ubicacion();
                    tipo_ubicacion.setCodigo(rs.getString("cod_tubicacion"));
                    tipo_ubicacion.setDescripcion(rs.getString("desc_tubicacion"));
                    tipo_ubicaciones.add(tipo_ubicacion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL TIPO DE UBICACI�N " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
          }
        
    }
    
    /**
     * Metodo <tt>listTipo_ubicacion</tt>, obtiene los tipo de ubicaciones
     *  registradas en el sistema
     * @autor : Ing. Jose de la Rosa
     * @return Vector     
     * @version : 1.0
     */
    public void listTipo_ubicacion()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "TU_LISTAR";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs= st.executeQuery();
                tipo_ubicaciones = new Vector();
                while(rs.next()){
                    tipo_ubicacion = new Tipo_ubicacion();
                    tipo_ubicacion.setCodigo(rs.getString("cod_tubicacion"));
                    tipo_ubicacion.setDescripcion(rs.getString("desc_tubicacion"));
                    tipo_ubicaciones.add(tipo_ubicacion);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TIPOS DE UBICACION (TODOS) " + e.getMessage() + " " + e.getErrorCode());
        }finally{//JJCastro fase2
                if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
                if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
          }
    }
}
