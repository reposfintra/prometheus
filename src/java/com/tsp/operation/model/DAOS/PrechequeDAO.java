/*
 * PrechequeDAO.java
 *
 * Created on 9 de febrero de 2007, 04:23 PM
 */
package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.Vector;
import com.tsp.operation.model.beans.Precheque;
import com.tsp.operation.model.beans.Egreso;
import com.tsp.operation.model.beans.StringStatement;

/**
 *
 * @author  Osvaldo P�rez Ferrer
 */
public class PrechequeDAO extends MainDAO{
    
    /** Creates a new instance of PrechequeDAO */
    private Egreso egreso;
    private Vector egresos;   
    Precheque precheque = new Precheque();
    
    public PrechequeDAO(){
        super("PrechequeDAO.xml");
    }
    public PrechequeDAO(String dataBaseName){
        super("PrechequeDAO.xml", dataBaseName);
    }
    
    /***********************************************************
     * Metodo insertPrecheque, inserta un registro en fin.precheque
     * @author : Ing. Osvaldo P�rez Ferrer
     * @param Precheque, objeto con los datos a ingresar
     * @throws en caso de un error de BD
     ***********************************************************/
    public String insertPrecheque( Precheque pre ) throws SQLException{
        
        String sql = "";
        StringStatement st = null;
        String query = "SQL_INSERT_PRECHEQUE";//JJCastro fase2
        
        try{
            st = new StringStatement(this.obtenerSQL(query), true );//JJCastro fase2
            
            st.setString( 1,  pre.getId() );
            st.setString( 2,  pre.getDstrct() );
            st.setString( 3,  pre.getBanco() );
            st.setString( 4,  pre.getSucursal() );
            st.setString( 5,  pre.getCheque() );
            st.setString( 6,  pre.getBeneficiario() );
            st.setString( 7,  pre.getProveedor() );
            st.setString( 8,  pre.getAgencia() );
            st.setDouble( 9,  pre.getValor() );
            st.setString( 10, pre.getMoneda() );
            st.setString( 11, pre.getCreation_user() );
            st.setString( 12, pre.getBase() );
            
            sql = st.getSql();//JJCastro fase2
            
        }catch (Exception ex){
            throw new SQLException( "ERROR  PrechequeDAO.insertPrecheque " + ex.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st= null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
          }
        return sql;
    }
    
    
    /***********************************************************
     * Metodo insertPrechequeDetalle, inserta un registro en fin.precheque_detalle
     * @author : Ing. Osvaldo P�rez Ferrer
     * @param Precheque, objeto con los datos a ingresar
     * @throws en caso de un error de BD
     ***********************************************************/
    public String insertPrechequeDetalle( Precheque pre ) throws SQLException{
        
        StringStatement st = null;
        String query = "SQL_INSERT_PRECHEQUE_DETALLE";
        String sql = "";
        
        try{
            st = new StringStatement(this.obtenerSQL(query), true );//JJCastro fase2
            
            st.setString( 1, pre.getId() );
            st.setString( 2, pre.getDstrct() );                        
            st.setString( 3, pre.getItem() );            
            st.setString( 4, pre.getProveedor() );
            st.setString( 5, pre.getTipo_documento() );
            st.setString( 6, pre.getDocumento() );
            st.setDouble( 7, pre.getValor() );
            st.setString( 8, pre.getTipo_pago() );
            st.setString( 9, pre.getCreation_user() );            
            st.setString(10, pre.getBase() );            
            
            sql = st.getSql();//JJCastro fase2
            
        }catch (Exception ex){
            throw new SQLException( "ERROR  PrechequeDAO.insertPrechequeDetalle " + ex.getMessage() );
        }finally{//JJCastro fase2
            if (st  != null){ try{ st= null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
          }
        return sql;
    }
    
/**
 *
 * @param agencia
 * @param dstrct
 * @return
 * @throws SQLException
 */
    public Vector getPrecheques( String agencia, String dstrct ) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;       
        ResultSet rs = null;
        String query = "SQL_GET_PRECHEQUES";//JJCastro fase2
        Vector v = new Vector();
        String sql = "";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {//JJCastro fase2
            sql = this.obtenerSQL(query);//JJCastro fase2
            sql = agencia.equals("OP")? sql.replaceAll( "#COMMENT#", "") : sql.replaceAll( "#COMMENT#", "agencia = ? AND");

            st = con.prepareStatement(sql);//JJCastro fase2
            if(agencia.equals("OP")) {
            st.setString( 1, dstrct ); 
            } else {
            st.setString( 1, agencia );            
            st.setString( 2, dstrct );
            }
            rs = st.executeQuery();
            
            while( rs.next() ){
                Precheque p = new Precheque();
                
                p.setId(                reset( rs.getString("id") ) );
                p.setDstrct(            reset( rs.getString("dstrct") ) );
                p.setBanco(             reset( rs.getString("banco") ) );
                p.setSucursal(          reset( rs.getString("sucursal") ) );
                p.setBeneficiario(      reset( rs.getString("beneficiario") ) );
                p.setNom_beneficiario(  reset( rs.getString("nom_beneficiario") ) );
                p.setProveedor(         reset( rs.getString("proveedor") ) );
                p.setNom_proveedor(     reset( rs.getString("nom_proveedor") ) );                
                p.setValor(             rs.getDouble("valor") );
                p.setMoneda(            reset( rs.getString("moneda") ) );
                p.setNom_agencia(       reset( rs.getString("nom_agencia") ) );
                p.setCreation_date(     reset( rs.getString("fecha") ) );
                p.setCreation_user(     reset( rs.getString("creation_user") ) );
                
                p.setItems( this.getPrechequesDetalle( p ) );
                p.setFacturas( this.precheque.getFacturas() );
                
                v.add( p );
                p = null;//Liberar Espacio JJCastro
            }
            
        }}catch (SQLException ex){
            throw new SQLException( "ERROR  PrechequeDAO.getPrecheques " + ex.getMessage() + " " + ex.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return v;
        
    }    
    
    
    /***********************************************************
     * Metodo updatePrecheque, actualiza precheque luego de haber impreso el cheque
     * @author : Ing. Osvaldo P�rez Ferrer
     * @param Precheque, objeto con los datos a ingresar
     * @throws en caso de un error de BD
     ***********************************************************/
    public String updatePrecheque( Precheque pre ) throws SQLException{
        
        String sql = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_PRECHEQUE_IMPRESION";
        
        try{
            st = new StringStatement( this.obtenerSQL(query), true );//JJCastro fase2
            
            st.setString( 1,  pre.getCheque() );
            st.setString( 2,  pre.getUsuario_impresion() );
            st.setString( 3,  pre.getUsuario_impresion() );
            st.setString( 4,  pre.getId() );
            sql = st.getSql();//JJCastro fase2
            
        }catch (Exception ex){
            throw new SQLException( "ERROR  PrechequeDAO.updatePrecheque " + ex.getMessage() );
        }finally{//JJCastro fase2
              if (st  != null){ try{ st=null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
          }
        return sql;
    }
  
    /***********************************************************
     * Metodo anularPrecheque, anula cabecera y detalle de precheque
     * @author : Ing. Osvaldo P�rez Ferrer
     * @param Precheque, objeto con los datos a ingresar
     * @throws en caso de un error de BD
     ***********************************************************/
    public void anularPrecheque( String id, String usuario ) throws SQLException{
        Connection con = null;//JJCastro fase2
        String sql = "";
        PreparedStatement st = null;       
        String query = "SQL_ANULAR_PRECHEQUE";//JJCastro fase2
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll( "#ID#", id ));//JJCastro fase2
            st.setString( 1, usuario );                        
            st.setString( 2, usuario );                       
            st.executeUpdate();
            
        }}catch (SQLException ex){
            throw new SQLException( "ERROR  PrechequeDAO.anularPrecheque " + ex.getMessage() + " " + ex.getErrorCode() );
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    public String reset(String value){
        return value!=null? value : "";
    }
    
    /**
     * Getter for property precheque.
     * @return Value of property precheque.
     */
    public com.tsp.operation.model.beans.Precheque getPrecheque() {
        return precheque;
    }
    
    /**
     * Setter for property precheque.
     * @param precheque New value of property precheque.
     */
    public void setPrecheque(com.tsp.operation.model.beans.Precheque precheque) {
        this.precheque = precheque;
    }

      /*
       * M�todo que obtiene la cabecera de un precheque
       * @autor Andr�s Maturana De La Cruz
       * @version 1.0
       */
    public void obtenerCabeceraPrecheque() throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CONSULTAR_CABECERA_EGRESO";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, egreso.getDocument_no() );
            
            rs = st.executeQuery();
            egreso = new Egreso();
            if( rs.next() ){
                egreso.setEstado( rs.getString("estado") );
                egreso.setDstrct         ( rs.getString("dstrct") );
                egreso.setBranch_code    ( rs.getString("banco") );
                egreso.setBank_account_no( rs.getString("sucursal") );
                egreso.setDocument_no    ( rs.getString("id") );
                egreso.setNit            ( rs.getString("beneficiario") );
                egreso.setPayment_name   ( rs.getString("nom_beneficiario") );
                egreso.setAgency_id  ( rs.getString("nomagc") );
                egreso.setVlr            ( rs.getFloat("valor") );
                egreso.setCurrency       ( rs.getString("moneda") );
                egreso.setFechaEntrega( rs.getString("fecha_cheque") );//fecha cheque
            }
            
        }}catch( Exception e ){
            throw new Exception( "Error en la rutina obtenerCabeceraPrecheque [PrechequeDAO]...\n" + e.getMessage() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Getter for property egreso.
     * @return Value of property egreso.
     */
    public Egreso getEgreso() {
        return egreso;
    }
    
    /**
     * Setter for property egreso.
     * @param egreso New value of property egreso.
     */
    public void setEgreso(Egreso egreso) {
        this.egreso = egreso;
    }
    
    /**
     * Getter for property egresos.
     * @return Value of property egresos.
     */
    public java.util.Vector getEgresos() {
        return egresos;
    }
    
    /**
     * Setter for property egresos.
     * @param egresos New value of property egresos.
     */
    public void setEgresos(java.util.Vector egresos) {
        this.egresos = egresos;
    }
    
    /*     
     * M�todo que obtiene el detalle de un egreso
     * @autor Ing. Andr�s Maturana De La Cruz
     * @version 1.0
     */
    public void obtenerDetallePrecheque() throws Exception{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;
        ResultSet rs = null;
        egresos = new Vector();
        String query = "SQL_CONSULTAR_DET_EGRESO";//JJCastro fase2
         try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, egreso.getDocument_no() );
            rs = st.executeQuery();

            while ( rs.next() ){
                Egreso dt = new Egreso();
                dt.setItem_no( rs.getString("item") );
                dt.setVlr( rs.getFloat("valor") );
                dt.setDocument_no( rs.getString("id") );
                dt.setNit(rs.getString("proveedor"));
                dt.setPayment_name(rs.getString("nom_nit"));
                dt.setTipo_documento(rs.getString("tipo_documento"));
                dt.setDocumento(rs.getString("documento"));
                dt.setTipo_pago(rs.getString("tipo_pago"));
                egresos.add( dt );
                dt = null;//Liberar Espacio JJCastro
            }
        }}catch( Exception e ){
            throw new Exception( "Error en la rutina obtenerDetallePrecheque [PrechequeDAO]...\n" + e.getMessage() );        
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


/**
 *
 * @param serial
 * @param factura
 * @param nit
 * @param nitbe
 * @param agc
 * @param banco
 * @param sucursal
 * @param usercre
 * @param fechai
 * @param fechaf
 * @param dstrct
 * @param estado
 * @throws Exception
 */
    public void consultaPrecheque(
            String serial,
            String factura,
            String nit,
            String nitbe,
            String agc,
            String banco,
            String sucursal,
            String usercre,
            String fechai,
            String fechaf,
            String dstrct,
            String estado) throws Exception{
                
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());

        logger.info("SERIAL: " + serial);
        logger.info("FACTURA: " + factura);
        logger.info("PROVEEDOR: " + nit);
        logger.info("BENEFICIARIO: " + nitbe);
        logger.info("AGENCIA: " + agc);
        logger.info("BANCO: " + banco);
        logger.info("SUCURSAL: " + sucursal);
        logger.info("FECHAI: " + fechai);
        logger.info("FECHAF: " + fechaf);
        logger.info("USUARIO CREO: " + usercre);

        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        this.egresos = null;
        this.egresos = new Vector();
        System.gc();
        try{
            
            con = this.conectarJNDI("SQL_CONSULTAR");//JJCastro fase2
            String sql = this.obtenerSQL("SQL_CONSULTAR");
            
            int c = 0; 
            
            if( serial.length()!=0 ){
                sql = sql.replaceAll("#1", "egre.id = '" + serial + "'");
                c++;
            } else {
                sql = sql.replaceAll("#1","");
            }
            
            if( factura.length()!=0 || nit.length()!=0 ){
                sql =  sql.replaceAll("#DSTRCT", (c == 0 ? "" : "AND ") + "det.dstrct = '" + dstrct + "'");
                if( nit.length()!=0 ){
                    sql = sql.replaceAll("#2", "AND det.proveedor = '" + nit + "'");
                } else {
                    sql = sql.replaceAll("#2", "");
                }
                sql = sql.replaceAll("#TDOC", "AND det.tipo_documento = '010'");
                if( factura.length()!=0 ){
                    logger.info(".. filtro fra.");
                    sql = sql.replaceAll("#3", " AND det.documento = '" + factura + "'");
                } else {
                    sql = sql.replaceAll("#3", "");
                }
                c++;    
            } else {
                sql = sql.replaceAll("#DSTRCT", "");
                sql = sql.replaceAll("#TDOC", "");
                sql = sql.replaceAll("#2", "");
                sql = sql.replaceAll("#3", "");
            }
            
            if( nitbe.length()!=0 ){
                sql =  sql.replaceAll("#4", (c == 0 ? "" : "AND ") + "egre.beneficiario = '" + nitbe + "'");
                c++;
            } else {
                sql = sql.replaceAll("#4", "");
            }
            
            if( agc.length()!=0 ){
                sql =  sql.replaceAll("#5", (c == 0 ? "" : "AND ") + "egre.agencia = '" + agc + "'");
                c++;
            } else {
                sql = sql.replaceAll("#5", "");
            }
            
            if( banco.length()!=0 ){
                sql =  sql.replaceAll("#6", (c == 0 ? "" : "AND ") + "egre.banco = '" + banco + "'");
                c++;
            } else {
                sql = sql.replaceAll("#6", "");
            }
            
            if( sucursal.length()!=0 ){
                sql =  sql.replaceAll("#7", (c == 0 ? "" : "AND ") + "egre.sucursal = '" + sucursal + "'");
                c++;
            } else {
                sql = sql.replaceAll("#7", "");
            }
            
            if( usercre.length()!=0 ){
                sql =  sql.replaceAll("#8", (c == 0 ? "" : "AND ") + "egre.creation_user = '" + usercre + "'");
                c++;
            } else {
                sql = sql.replaceAll("#8", "");
            }
            
            if( fechai.length()!=0 && fechaf.length()!=0){
                sql =  sql.replaceAll("#9", (c == 0 ? "" : "AND ") + "egre.creation_date BETWEEN '" + fechai + "' AND '" + fechaf + "'");
                c++;
            } else {
                sql = sql.replaceAll("#9", "");
            }
            
            if( estado.length()!=0 ){
                estado = estado.equals("E")?"":estado;
                sql =  sql.replaceAll("#A1", (c == 0 ? "" : "AND ") + "egre.reg_status = '" + estado + "'");
                c++;
            } else {
                sql = sql.replaceAll("#A1", "");
            }
            
            st = con.prepareStatement(sql);            
            //System.out.println("sql consulta precheques: " + st);
            rs = st.executeQuery();

            while ( rs.next() ){
                Precheque p = new Precheque();
                
                p.setId(                reset( rs.getString("id") ) );
                p.setBanco(             reset( rs.getString("banco") ) );
                p.setSucursal(          reset( rs.getString("sucursal") ) );
                p.setBeneficiario(      reset( rs.getString("beneficiario") ) );
                p.setNom_beneficiario(  reset( rs.getString("nom_beneficiario") ) );               
                p.setValor(             rs.getDouble("valor") );
                p.setMoneda(            reset( rs.getString("moneda") ) );
                p.setNom_agencia(       reset( rs.getString("nomagc") ) );
                p.setCreation_date(     reset( rs.getString("fecha_creacion") ) );
                p.setCreation_user(     reset( rs.getString("creation_user") ) );
                p.setFecha_impresion(   reset(rs.getString("impreso")));
                p.setUsuario_impresion( reset(rs.getString("usuario_impresion")));
                p.setReg_status (       reset(rs.getString("reg_status")));//jose de la rosa
                this.egresos.add(p);
                p = null;//Liberar Espacio JJCastro
            }        
        }catch( Exception e ){
            e.printStackTrace();
            throw new Exception( "Error en la rutina obtenerDetallePrecheque [PrechequeDAO]...\n" + e.getMessage() );        
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


 /**
  *
  * @param cab
  * @return
  * @throws SQLException
  */
      public Vector getPrechequesDetalle( Precheque cab ) throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement st = null;       
        ResultSet rs = null;
        String query = "SQL_GET_PRECHEQUES_DETALLE";
        Vector v = new Vector();
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, cab.getId() );
            st.setString( 2, cab.getDstrct() );
            
            rs = st.executeQuery();
            
            String facturas = "";
            this.precheque = new Precheque();
            
            while( rs.next() ){
                
                Precheque p = new Precheque();
                
                p.setDstrct(    reset( rs.getString("dstrct") ) );
                p.setItem(      reset( rs.getString("item") ) );
                p.setValor(     rs.getDouble("valor") );
                p.setDocumento( reset( rs.getString("documento") ) );
                p.setTipo_pago( reset( rs.getString("tipo_pago" ) ) );
                v.add( p );
                //String de facturas que tiene el precheque, separadas por ','
                precheque.setFacturas(  p.getDocumento() + "," +precheque.getFacturas() ); 
            p = null;//Liberar Espacio JJCastro
            }
            
        }}catch (SQLException ex){
            throw new SQLException( "ERROR  PrechequeDAO.getPrechequesDetalle " + ex.getMessage() + " " + ex.getErrorCode() );
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return v;
        
    }
       /***********************************************************
     * Metodo updatePrecheque, actualiza precheque luego de haber impreso el cheque
     * @author : Ing. Osvaldo P�rez Ferrer
     * @param Precheque, objeto con los datos a ingresar
     * @throws en caso de un error de BD
     ***********************************************************/
    public String updatePrechequeDetalle( Precheque pre ) throws SQLException{
        Connection con = null;//JJCastro fase2
        String sql = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_PRECHEQUE_DETALLE_IMPRESION";
        
        try{
            st = new StringStatement( this.obtenerSQL(query), true );//JJCastro fase2
                        
            st.setString( 1, pre.getUsuario_impresion() );            
            st.setString( 2, pre.getId().trim() );            
            
            sql = st.getSql();//JJCastro fase2
            
        }catch (Exception ex){
            throw new SQLException( "ERROR  PrechequeDAO.updatePrechequeDetalle " + ex.getMessage() );
        }finally{//JJCastro fase2
            if( st != null ){try{st= null;} catch( Exception e ){throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
          }
          }
        }
        return sql;
    }
        
}
