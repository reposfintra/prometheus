 /**************************************************************************
 * Nombre clase: ReporteDrummondDAO.java                                           *
 * Descripci�n: Clase que maneja las consultas de los reportes del cliente *
 *  DRUNMONND                                                              *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 1 de octubre de 2005, 08:20 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  igomez
 */
public class ReporteDrummondDAO {
    private final String SQL_CLIENTES ="SELECT  codcli, nomcli 	FROM cliente ";
    private final String SQL_REPORTE ="select                                          " +
                                     "      a.numrem,                                  " +
                                     "      a.numpla,                                  " +
                                     "      a.fecrem,                                  " +
                                     "      a.remitente,                               " +
                                     "      a.destinatario,                            " +
                                     "      a.placa,                                   " +
                                     "      a.nombre,                                  " +
                                     "      a.celular,                                 " +
                                     "      a.cedcon,                                  " +
                                     "      te.descripcion       as tipovehiculo,      " +
                                     "      colplaca.descripcion as colorPlaca,        " +
                                     "      b.ref_code           as despacho,          " +
                                     "      c.ref_code           as du,                " +
                                     "      d.ref_code           as po                 " +
                                     " from                                            " +
                                     "     (                                           " +
                                     "      select                                     " +
                                     "          a.numrem,                              " +
                                     "          c.numpla,                              " +
                                     "          a.fecrem,                              " +
                                     "          a.remitente,                           " +
                                     "          a.destinatario,                        " +
                                     "          d.recurso,                             " +
                                     "          d.color,                               " +
                                     "          d.placa,                               " +
                                     "          e.nombre,                              " +
                                     "          e.celular,                             " +
                                     "          c.cedcon                               " +
                                     "      from                                       " +
                                     "         remesa            a,                    " +
                                     "         plarem            b,                    " +
                                     "         planilla          c,                    " +
                                     "         placa             d,                    " +
                                     "         nit               e                     " +
                                     "      where                                      " +
                                     "           a.fecrem between ? and ?              " +
                                     "      and  a.cliente = ?                         " +
                                     "      and  b.numrem  = a.numrem                  " +
                                     "      and  c.numpla  = b.numpla                  " +
                                     "      and  d.placa   = c.plaveh                  " +
                                     "      and  e.cedula  = c.cedcon                  " +
                                     "      order by a.numrem,c.numpla                 " +
                                     "     ) a                                         " +
                                     " LEFT JOIN                                       " +
                                     "  tipo_equipo_placa  as  te                      " +
                                     " ON                                              " +
                                     "  te.codigo = a.recurso                          " +
                                     " LEFT JOIN                                       " +
                                     "  color_placa as  colplaca                       " +
                                     " ON                                              " +
                                     "  colplaca.codigo = a.color                      " +
                                     " LEFT JOIN                                       " +
                                     "  refcode  as  b                                 " +
                                     " ON                                              " +
                                     " (    b.entity_type  = 'WKO'                     " +
                                     "  and b.entity_value = '1TSP ' || a.numrem       " +
                                     "  and b.ref_no       = '001'                     " +
                                     "  and b.seq_num      = '001'                     " +
                                     " )                                               " +
                                     " LEFT JOIN                                       " +
                                     "  refcode  as  c                                 " +
                                     " ON                                              " +
                                     "  (    c.entity_type  = 'WKO'                    " +
                                     "   and c.entity_value = '1TSP ' || a.numrem      " +
                                     "   and c.ref_no       = '001'                    " +
                                     "   and c.seq_num      = '002'                    " +
                                     "  )                                              " +
                                     " LEFT JOIN                                       " +
                                     "  refcode  as d                                  " +
                                     " ON                                              " +
                                     "  (    d.entity_type  = 'WKO'                    " +
                                     "   and d.entity_value = '1TSP ' || a.numrem      " +
                                     "   and d.ref_no       = '002'                    " +
                                     "  )                                              "; 
                                     
    
    
    
    /** Creates a new instance of ReporteDrummmondDAO */
    public ReporteDrummondDAO() {
    }
    
    
     /************************************************************************
     * Metodo   : searchCliente, retorna una lista de clientes.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   :
     * @version : 1.0
     *************************************************************************/
    public List searchCliente() throws SQLException{
        PreparedStatement st          = null;      
        ResultSet         rs    = null;
        PoolManager       poolManager = PoolManager.getInstance();              
        Connection        Conexion    = poolManager.getConnection("fintra");
        List         listado = null;
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
           st = Conexion.prepareStatement(SQL_CLIENTES);
           rs= st.executeQuery();        
           if(rs!=null){
              listado = new LinkedList();
              while(rs.next()){
                 Cliente  cliente  = new Cliente();
                   cliente.setCodcli(rs.getString(1));
                   cliente.setNomcli(rs.getString(2));
                   
                listado.add(cliente); 
              }
           }
        } catch(SQLException e) {
            throw new SQLException("Error en rutina searchCliente  [ReporteDrummondDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();  
            if(rs!=null)  rs.close();  
            poolManager.freeConnection("fintra",Conexion);
        } 
        return listado;
     }
    
    
      /*************************************************************************************
     * Metodo   : searchReporte, retorna una lista Con los datos necesarios para el cliente.
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : Fecha inicial, fecha Final y el codigo del cliente.
     * @version : 1.0
     **************************************************************************************/
     public List searchReporte(String FecIni, String FecFin, String CodCliente) throws SQLException{
        PreparedStatement st          = null;      
        ResultSet         rs    = null;
        PoolManager       poolManager = PoolManager.getInstance();              
        Connection        Conexion    = poolManager.getConnection("fintra");
        List         listado = null;
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try {
           st = Conexion.prepareStatement(SQL_REPORTE);
           st.setString(1, FecIni);
           st.setString(2, FecFin);
           st.setString(3, CodCliente);
           rs= st.executeQuery();        
           if(rs!=null){
              listado = new LinkedList();
              while(rs.next()){
                 ReporteDrummond  reporDrum  = new ReporteDrummond();
                   reporDrum.setRemision    (rs.getString(1));
                   reporDrum.setOC          (rs.getString(2));
                   reporDrum.setFecDespacho (rs.getString(3));
                   reporDrum.setOrigen      (rs.getString(4));
                   reporDrum.setDestino     (rs.getString(5));
                   reporDrum.setPlaca       (rs.getString(6));
                   reporDrum.setConductor   (rs.getString(7));
                   reporDrum.setCelular     (rs.getString(8));
                   reporDrum.setCedula      (rs.getString(9));
                   reporDrum.setTipoVehiculo(rs.getString(10));
                   reporDrum.setColor       (rs.getString(11));
                   reporDrum.setDespacho    (rs.getString(12));
                   reporDrum.setDO          (rs.getString(13));
                   reporDrum.setPO          (rs.getString(14));
                   
                   
                listado.add(reporDrum);  
              }
           }
        } catch(SQLException e) {
            throw new SQLException("Error en rutina searchCliente  [ReporteDrummondDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();  
            if(rs!=null)  rs.close();  
            poolManager.freeConnection("fintra",Conexion);
        } 
        return listado;
     }
    
    
}
