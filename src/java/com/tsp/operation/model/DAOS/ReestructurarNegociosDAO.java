/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.ConfiguracionDescuentosObligaciones;
import com.tsp.operation.model.beans.ConfiguracionTablaInicial;
import com.tsp.operation.model.beans.DocumentosNegAceptado;
import com.tsp.operation.model.beans.LiquidacionFenalcoBeans;
import com.tsp.operation.model.beans.Negocios;
import com.tsp.operation.model.beans.SaldosReestructuracionBeans;
import com.tsp.operation.model.beans.SolicitudNegocio;
import com.tsp.operation.model.beans.SolicitudPersona;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.resumenSaldosReestructuracion;
import com.tsp.operation.model.beans.tablaPagoInicial;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author egonzalez
 */
public interface ReestructurarNegociosDAO {

    /**
     *
     * @param cedula
     * @param codigoNegocio
     * @return
     * @throws SQLException
     */
    public ArrayList<BeanGeneral> getNegocios(String cedula, String codigoNegocio) throws SQLException;

    /**
     *
     * @param codNegocio
     * @return
     * @throws SQLException
     */
    public ArrayList<SaldosReestructuracionBeans> getFacturasNegocios(String codNegocio) throws SQLException;

  
    /**
     *
     * @param tipo_cuota
     * @param cuota
     * @param valor_negocio
     * @param primeracuota
     * @param titulo_valor
     * @return
     * @throws SQLException
     */
    public ArrayList<DocumentosNegAceptado> simuladorCreditoMicro(String tipo_cuota, int cuota, double valor_negocio, String primeracuota, String titulo_valor) throws SQLException;

    /**
     *
     * @param negocio_padre
     * @param usuario
     * @param idconvenio
     * @param valor_negocio
     * @param numcuota
     * @param fecha_pr_cuota
     * @param tipo_cuotas
     * @param saldo_capital
     * @param saldo_interes
     * @param saldo_cat
     * @param saldo_seguro
     * @param intxmora
     * @param gac
     * @return
     */
    public String crearNegoMicroNuevo(String negocio_padre, String usuario, int idconvenio, double valor_negocio, 
                                        int numcuota, String fecha_pr_cuota, String tipo_cuotas,
                                        double saldo_capital, double saldo_interes, double saldo_cat,
                                        double saldo_seguro, double intxmora, double gac);

    /**
     *
     * @return
     */
    public String cargarDepartamento();

    /**
     *
     * @param coddpt
     * @return
     */
    public String cargarCiudad(String coddpt);

    /**
     *
     * @param numero_solicitud
     * @return
     */
    public String buscarFormulario(String numero_solicitud);

    /**
     *
     * @param persona
     * @param negocio
     * @param usuario
     * @param observacion
     * @return
     */
    public String actualizarFormulario(SolicitudPersona persona, SolicitudNegocio negocio, String usuario, String observacion);

    /**
     *
     * @param actividad
     * @param estado
     * @return @throws SQLException
     */
    public ArrayList<Negocios> getListarNegociosRees(String actividad, String estado) throws SQLException;

    /**
     *
     * @param negocio
     * @return
     * @throws SQLException
     */
    public ArrayList<DocumentosNegAceptado> buscarLiquidacionNegocio(String negocio) throws SQLException;

    /**
     *
     * @param negocio
     * @param usuario
     * @return
     */
    public String rechazarNegocio(String negocio, String usuario);

    /**
     *
     * @param negocio
     * @return
     */
    public String buscarTraza(String negocio);

    /**
     *
     * @param negocio
     * @param formulario
     * @param usuario
     * @param observacion
     * @return
     */
    public String aprobarNegocio(String negocio, String formulario, String usuario, String observacion);

    /**
     *
     * @param negocio
     * @param usuario
     * @param formulario
     * @param observacion
     * @return
     */
    public String actualizarNegocio(String negocio, String usuario, String formulario, String observacion);
 
    /**
     *
     * @param cedula
     * @param user
     * @return
     * @throws SQLException
     */
    public ArrayList<BeanGeneral> buscarNegocioFenalco(String cedula,Usuario user) throws SQLException;

    /**
     *
     * @return @throws SQLException
     */
    public String buscarUnidadNegocio() throws SQLException;

    /**
     *
     * @param vectorNegocio
     * @param user
     * @return
     * @throws SQLException
     */
    public ArrayList<SaldosReestructuracionBeans> saldosNegocioSegFenalco(ArrayList vectorNegocio,Usuario user) throws SQLException;

    /**
     *
     * @param cuota
     * @param valor_negocio
     * @param primeracuota
     * @param titulo_valor
     * @param idconvenio
     * @param afiliado
     * @return
     * @throws SQLException
     */
    public ArrayList<DocumentosNegAceptado> liquidadorCreditoFenalco(int cuota, double valor_negocio, String primeracuota, String titulo_valor, String idconvenio, String afiliado) throws SQLException;

    /**
     *
     * @param listaInicial
     * @param usuario
     * @return
     * @throws SQLException
     */
    public String guardarExtracto(ArrayList<tablaPagoInicial> listaInicial, String usuario) throws SQLException;

    /**
     *
     * @param sr
     * @param user
     * @return
     * @throws SQLException
     */
    public String guardarSaldosResumen(resumenSaldosReestructuracion sr, Usuario user) throws SQLException;

    /**
     *
     * @param liquidacionBeans
     * @return
     * @throws SQLException
     */
    public String guardarLiquidacion(LiquidacionFenalcoBeans liquidacionBeans) throws SQLException;

    public void borrarExtracto(int idRop);

    /**
     *
     * @param userlogin
     * @param id_rop
     */
    public void generarPdf(String userlogin, int id_rop);

    /**
     *
     * @param user
     * @return
     * @throws SQLException
     */
    public ArrayList<ConfiguracionDescuentosObligaciones> cargarDescuentos(Usuario user) throws SQLException;

    /**
     *
     * @param user
     * @return
     * @throws SQLException
     */
    public ArrayList<ConfiguracionTablaInicial> cargarConfInicial(Usuario user) throws SQLException;
    
    /**
     *
     * @param id_rop
     * @param usuario
     * @return
     * @throws SQLException
     */
    public String crearNotaAjusteFenalco(int id_rop,Usuario usuario)throws SQLException;
    
    /**
     *
     * @param pagoInicial
     * @param idRop
     * @param user
     * @return
     * @throws SQLException
     */
    public String guardarTablaPagoInicial(tablaPagoInicial pagoInicial,int idRop, Usuario user) throws SQLException;
    
    public ArrayList<BeanGeneral> buscarNegociosPorAprobar(String aprobar)throws SQLException;
    
    /**
     *
     * @param idRop
     * @return
     * @throws SQLException
     */
    public ArrayList<resumenSaldosReestructuracion> buscarResumenSaldos(int idRop)throws SQLException;

    /**
     *
     * @param idRop
     * @return
     * @throws SQLException
     */
    public ArrayList<tablaPagoInicial> buscarTablaPagoInicial(int idRop)throws SQLException;
    
    /**
     *
     * @param usuario
     * @return
     * @throws SQLException
     */
    public String validarPermisos(String usuario)throws SQLException;
    
    /**
     *
     * @param idRop
     * @param usuario
     * @return
     * @throws SQLException
     */
    public String aprobarReestructuracion(int idRop,String usuario)throws SQLException;
    
    /**
     *
     * @param idRop
     * @param usuario
     * @return
     * @throws SQLException
     */
    public String actualizarImpresion(int idRop,String usuario)throws SQLException;
    
    /**
     *
     * @param idRop
     * @return
     * @throws SQLException
     */
    public String buscarCabeceralIquidacion(int idRop)throws SQLException;
    
    /**
     *
     * @param idRop
     * @return
     * @throws SQLException
     */
    public ArrayList<DocumentosNegAceptado> buscarliquidacionFenalco(int idRop) throws SQLException;
    
    public String rechazarReestructuracionFenalco(int id_rop)throws SQLException;
    
      /**
     * 
     * @return
     * @throws SQLException
     */
    public ArrayList<ConfiguracionDescuentosObligaciones> buscarConfigActual() throws SQLException;
    
     /**
     *
     * @param id
     * @param tiponeg
     * @param descuento
     * @param porc_cta_ini
     * @param tipo
     * @param usuario
     * @return
     * @throws SQLException
     */
    public String actualizarConfigActual(int id,String tiponeg,int descuento, int porc_cta_ini,String tipo, String usuario) throws SQLException;
    
     /**
     *  
     * @param usuario   
     * @return
     * @throws SQLException
     */
    public int insertarConfigActual(String usuario) throws SQLException;
   
      /**
     *  
     * @param usuario   
     * @return
     * @throws SQLException
     */
    public int insertarConfigPagoIni(String usuario) throws SQLException;
    
     /**
     * 
     * @return
     * @throws SQLException
     */
    public ArrayList<ConfiguracionTablaInicial> buscarConfigPagoIni() throws SQLException;
    
    /**
     *
     * @param negocio
     * @param usuario
     * @return
     * @throws SQLException
     */
    public String cambiarCuentasOrden(String negocio,Usuario usuario) throws SQLException;
    
    /**
     *
     * @param userlogin
     * @param id_rop
     */
    public void generarPdfDuplicado(String userlogin, int id_rop);
    
     /**
     *
     * @return
     */
    public String cargarConveniosMicro();
    
       /**
     *    
     * @param valor_negocio
     * @param num_cuotas
     * @param fecha_item
     * @param tipo_cuota
     * @param id_convenio
     * @param fecha_liquidacion
     * @param usuario
     * @return
     * @throws SQLException
     */
    public ArrayList<DocumentosNegAceptado> buscarLiquidacionMicro(Double valor_negocio, int num_cuotas, String fecha_item,String tipo_cuota,String id_convenio, String fecha_liquidacion) throws SQLException;

    public String getInfoNegociosLiquidarMicro(String codigoNegocio, Usuario usuario) throws SQLException;
    
    public String actualizarLiquidacionMicro(String codigoNegocio, String tipo_cuota, int num_cuotas, Double valor_negocio, String fecha_liquidacion, String fecha_item,String id_convenio);
}
