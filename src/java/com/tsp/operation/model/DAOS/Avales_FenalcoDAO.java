/**
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : DAO que maneja los avales de Fenalco
**/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.lang.*;


public class Avales_FenalcoDAO extends MainDAO  {
        
        private Aval_Fenalco Aval;
        private Vector Avales;
        private String err=null;
        
    public Avales_FenalcoDAO() {
            super("Avales_FenalcoDAO.xml");
            //System.out.println("Entra aqui");
        }
    public Avales_FenalcoDAO(String dataBaseName) {
            super("Avales_FenalcoDAO.xml", dataBaseName);
            //System.out.println("Entra aqui");
        }
        
    public void ListadoAvales(String nid,String us ) throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  query = "SQL_AVALES";
       
        Avales = new Vector();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString( 1, us );
            ps.setString( 2, us );
            rs = ps.executeQuery();
            //System.out.println("Q- "+ps.toString());
            while (rs.next())
            {
                Aval = new Aval_Fenalco();
                Aval.setNit   ( rs.getString(1));
                Aval.setNodoc ( rs.getInt(2));
                Aval.setTc30  ( rs.getFloat(3));
                Aval.setTc45  ( rs.getFloat(4));
                Aval.setTl30  ( rs.getFloat(5));
                Aval.setTl45  ( rs.getFloat(6));
                Avales.add(Aval);
                //System.out.println("No");
                Aval = null;
            }
        }}
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       
       
 }
    
    public java.util.Vector getAvales() {
          return Avales;
      }  
      
    public String verificar(){
          return err;
      }
      
      
}