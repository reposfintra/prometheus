/********************************************************************
 *      Nombre Clase.................   DocumentoActividadDAO.java    
 *      Descripci�n..................   DAO de la tabla tblact_doc    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   19.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.DAOS;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;

public class DocumentoActividadDAO {
        private DocumentoActividad act_doc;
        private Vector act_docs;
        
        private static final String SQL_LISTAR_DOC = "select * from tblact_doc where dstrct=? and documento=? and reg_status<>'A'";
        
        private static final String SQL_INSERT = "insert into tblact_doc(reg_status, dstrct, documento," +
                " actividad, last_update, user_update, creation_date, creation_user, base) " +
                "values('',?,?,?,'now()',?,'now()',?,?)";
        
        private static final String SQL_UPDATE = "update tblact_doc set reg_status=?, last_update='now()', user_update=?" +
                " where dstrct=? and documento=? and actividad=?";
        
        private static final String SQL_OBTENER = "select * from tblact_doc where dstrct=? and documento=? and actividad=?";
        
        /** Creates a new instance of DocumentoActividadDAO */
        public DocumentoActividadDAO() {
        }
        
        /**
         * Inserta un nuevo registro.
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public void ingresarActividadeDocumento() throws SQLException {
                
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_INSERT);
                                st.setString(1, act_doc.getDistrito());
                                st.setString(2, act_doc.getDocumento() );
                                st.setString(3, act_doc.getActividad());
                                st.setString(4, act_doc.getUsuario_creacion());
                                st.setString(5, act_doc.getUsuario_creacion());
                                st.setString(6, act_doc.getBase());
                                                               
                                st.executeUpdate();                                
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL INGRESAR LA RELACION ACTIVIDAD-DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }                  
                
        }
        
        /**
         * Actualiza un registro.
         * @autor Tito Andr�s Maturana
         * @throws SQLException
         * @version 1.0
         */
        public void updateActividadeDocumento() throws SQLException {
                
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_UPDATE);
                                st.setString(1, act_doc.getEstado());
                                st.setString(2, act_doc.getUsuario_modificacion());
                                st.setString(3, act_doc.getDistrito());
                                st.setString(4, act_doc.getDocumento());
                                st.setString(5, act_doc.getActividad());
                                                               
                                st.executeUpdate();
                                
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL ACTUALIZAR LA RELACION ACTIVIDAD-DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }                  
                
        }
        
        /**
         * Obtiene una lista registros por documento
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito
         * @param documento documento C�digo del documento
         * @throws SQLException
         * @version 1.0
         */
        public void obtenerActividadesDocumento(String distrito, String documento) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
                this.act_doc = null;
                this.act_docs = new Vector();
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_LISTAR_DOC);
                                st.setString(1, distrito);
                                st.setString(2, documento);
                                                               
                                rs = st.executeQuery();
                                
                                while( rs.next() ){
                                        this.act_doc = new DocumentoActividad();
                                        act_doc.Load(rs);
                                        act_docs.add(act_doc);
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL LISTAR LAS ACTIVIDADES POR DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }                  
                
        }
        
        /**
         * Estblece si un registro a existe
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito
         * @param documento documento C�digo del documento
         * @param documento actividad C�digo de la actividad
         * @throws SQLException
         * @version 1.0
         */
        public boolean existeActividadDocumento(String distrito, String documento, String actividad) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
                boolean existe = false;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_OBTENER);
                                st.setString(1, distrito);
                                st.setString(2, documento);
                                st.setString(3, actividad);
                                                               
                                rs = st.executeQuery();
                                
                                while( rs.next() ){
                                        existe = true;
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL OBTENER LA ACTIVIDAD-DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }                  
                
                return existe;
        }
        
        /**
         * Obtiene un registro
         * @autor Tito Andr�s Maturana
         * @param distrito Distrito
         * @param documento documento C�digo del documento
         * @param documento actividad C�digo de la actividad
         * @throws SQLException
         * @version 1.0
         */
        public void obtenerActividadDocumento(String distrito, String documento, String actividad) throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;       
                PoolManager poolManager = null;
                this.act_doc = null;
        
                try{
                        poolManager = PoolManager.getInstance();
                        con = poolManager.getConnection("fintra");
                        if (con != null){
                                st = con.prepareStatement(this.SQL_OBTENER);
                                st.setString(1, distrito);
                                st.setString(2, documento);
                                st.setString(3, actividad);
                                                               
                                rs = st.executeQuery();
                                
                                while( rs.next() ){
                                        this.act_doc = new DocumentoActividad();
                                        this.act_doc.Load(rs);
                                }
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR AL OBTENER LA ACTIVIDAD-DOCUMENTO: " + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() ); 
                                }
                        }
                        if ( con != null){
                                poolManager.freeConnection("fintra", con );
                        }
                }
        }
        
        /**
         * Getter for property act_doc.
         * @return Value of property act_doc.
         */
        public com.tsp.operation.model.beans.DocumentoActividad getAct_doc() {
                return act_doc;
        }
        
        /**
         * Setter for property act_doc.
         * @param act_doc New value of property act_doc.
         */
        public void setAct_doc(com.tsp.operation.model.beans.DocumentoActividad act_doc) {
                this.act_doc = act_doc;
        }
        
        /**
         * Getter for property act_docs.
         * @return Value of property act_docs.
         */
        public java.util.Vector getAct_docs() {
                return act_docs;
        }
        
        /**
         * Setter for property act_docs.
         * @param act_docs New value of property act_docs.
         */
        public void setAct_docs(java.util.Vector act_docs) {
                this.act_docs = act_docs;
        }
        
}
