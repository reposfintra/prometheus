/*********************************************************************************
 * Nombre clase :      ImprimirOrdenDeCargaDAO.java                              *
 * Descripcion :       DAO del ImprimirOrdenDeCargaDAO.java                      *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  *
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             LREALES                                                   *
 * Fecha :             12 de mayo de 2006, 11:02 AM                              *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.model.DAOS;

import java.util.*;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;

public class ImprimirOrdenDeCargaDAO extends MainDAO {
    
    private Vector vec_lista;
    private Vector vec_buscar;
    private HojaOrdenDeCarga hoc = null;
    
    /** Creates a new instance of ImprimirOrdenDeCargaDAO */
    public ImprimirOrdenDeCargaDAO() {
        
        super( "ImprimirOrdenDeCargaDAO.xml" );
        
    }
    
    /**
     * Getter for property vec_lista.
     * @return Value of property vec_lista.
     */
    public java.util.Vector getVec_lista() {
        
        return vec_lista;
        
    }
    
    /**
     * Setter for property vec_lista.
     * @param vec_lista New value of property vec_lista.
     */
    public void setVec_lista(java.util.Vector vec_lista) {
        
        this.vec_lista = vec_lista;
        
    }
    
    /**
     * Getter for property vec_buscar.
     * @return Value of property vec_buscar.
     */
    public java.util.Vector getVec_buscar() {
        
        return vec_buscar;
        
    }
    
    /**
     * Setter for property vec_buscar.
     * @param vec_buscar New value of property vec_buscar.
     */
    public void setVec_buscar(java.util.Vector vec_buscar) {
        
        this.vec_buscar = vec_buscar;
        
    }
    
    /**
     * Getter for property hoc.
     * @return Value of property hoc.
     */
    public HojaOrdenDeCarga getHojaOrden() {
        
        return this.hoc;
        
    }
    
    /** Funcion publica que verifica si existe la orden_de_carga. */
    public boolean existeOrdenDeCarga( String distrito, String orden ) throws SQLException {
        
        boolean boo = false;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_ORDEN_DE_CARGA" );
            
            st.setString( 1, distrito );
            st.setString( 2, orden );
            
            rs = st.executeQuery();
            
            hoc = new HojaOrdenDeCarga();
            
            while( rs.next() ){
                
                boo = true;
                hoc.setOrden( (rs.getString("orden") != null)?rs.getString("orden"):"" );
                
            }
            
            
        } catch( SQLException e ){
            
            throw new SQLException( "ERROR DURANTE LA BUSQUEDA DE LAS PLANILLAS POR NUMERO DE PLANILLA" + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_ORDEN_DE_CARGA" );
            
        }
        
        return boo;
        
    }
    
    /** Funcion publica que obtiene una lista con los datos de una orden_de_carga. */
    public void listaOrdenDeCarga( String distrito, String orden ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_LISTA_ORDEN_DE_CARGA" );
            
            st.setString( 1, distrito );
            st.setString( 2, distrito );
            st.setString( 3, orden );
            
            rs = st.executeQuery();
            
            this.vec_lista = new Vector();
            
            while( rs.next() ){
                
                hoc = new HojaOrdenDeCarga();
                
                hoc.setCreation_date( (rs.getString("fecha") != null)?rs.getString("fecha"):"0099-01-01 00:00:00" );
                hoc.setDia( (rs.getString("dia") != null)?rs.getString("dia"):"" );
                hoc.setMes( (rs.getString("mes") != null)?rs.getString("mes"):"" );
                hoc.setAno( (rs.getString("ano") != null)?rs.getString("ano"):"" );
                hoc.setOrden( (rs.getString("orden") != null)?rs.getString("orden"):"" );
                hoc.setConductor( (rs.getString("cedula") != null)?rs.getString("cedula"):"" );
                hoc.setPlaca( (rs.getString("placa") != null)?rs.getString("placa"):"" );
                hoc.setTrailer( (rs.getString("trailer") != null)?rs.getString("trailer"):"" );
                hoc.setContenido( (rs.getString("contenido") != null)?rs.getString("contenido"):"" );
                hoc.setContenedores( (rs.getString("contenedores") != null)?rs.getString("contenedores"):"" );
                hoc.setEntregar( (rs.getString("entregar") != null)?rs.getString("entregar"):"" );
                hoc.setObservacion( (rs.getString("observacion") != null)?rs.getString("observacion"):"" );
                hoc.setCreation_user( (rs.getString("creation_user") != null)?rs.getString("creation_user"):"" );
                hoc.setPrecinto1( (rs.getString("precinto1") != null)?rs.getString("precinto1"):"" );
                hoc.setPrecinto2( (rs.getString("precinto2") != null)?rs.getString("precinto2"):"" );
                hoc.setPrecinto3( (rs.getString("precinto3") != null)?rs.getString("precinto3"):"" );
                hoc.setPrecinto4( (rs.getString("precinto4") != null)?rs.getString("precinto4"):"" );
                hoc.setPrecinto5( (rs.getString("precinto5") != null)?rs.getString("precinto5"):"" );
                hoc.setPrecintoc1( (rs.getString("precintoc1") != null)?rs.getString("precintoc1"):"" );
                hoc.setPrecintoc2( (rs.getString("precintoc2") != null)?rs.getString("precintoc2"):"" );
                hoc.setEmpresa( (rs.getString("empresa") != null)?rs.getString("empresa"):"" );
                hoc.setDireccion( (rs.getString("direccion") != null)?rs.getString("direccion"):"" );
                hoc.setNomconductor( (rs.getString("nomconductor") != null)?rs.getString("nomconductor"):"" );
                hoc.setExpced( (rs.getString("expced") != null)?rs.getString("expced"):"" );
                hoc.setPase( (rs.getString("pase") != null)?rs.getString("pase"):"" );
                hoc.setMarca( (rs.getString("marca") != null)?rs.getString("marca"):"" );
                hoc.setModelo( (rs.getString("modelo") != null)?rs.getString("modelo"):"" );
                hoc.setMotor( (rs.getString("motor") != null)?rs.getString("motor"):"" );
                hoc.setChasis( (rs.getString("chasis") != null)?rs.getString("chasis"):"" );
                hoc.setEmpresaafil( (rs.getString("empresaafil") != null)?rs.getString("empresaafil"):"" );
                hoc.setTarjetaoper( (rs.getString("tarjetaoper") != null)?rs.getString("tarjetaoper"):"" );
                hoc.setColor( (rs.getString("color") != null)?rs.getString("color"):"" );
                hoc.setCiudad( (rs.getString("ciudad") != null)?rs.getString("ciudad"):"" );
                hoc.setDestino( (rs.getString("destino") != null)?rs.getString("destino"):"" );
                
                vec_lista.add( hoc );
                
            }
            
        } catch( SQLException e ){
            
            throw new SQLException( "Error en listaOrdenDeCarga [ImprimirOrdenDeCargaDAO]......." + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_LISTA_ORDEN_DE_CARGA" );
            
        }
        
    }
      /** Funcion publica que busca la orden de carga ingresada,
     *  y verifica si existe, verifica su estado y verifica su fecha de impresion.
     */
    public void buscarOrdenDeCarga( String distrito, String orden ) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_ORDEN_DE_CARGA" );
            
            st.setString( 1, distrito );
            st.setString( 2, orden );
            
            rs = st.executeQuery();
            
            this.vec_buscar = new Vector();
            hoc=null;
            while( rs.next() ){
                
                hoc= hoc.load(rs);
                
                hoc.setOrden( (rs.getString("orden") != null)?rs.getString("orden"):"" );
                hoc.setReg_status( (rs.getString("reg_status") != null)?rs.getString("reg_status"):"" );
                hoc.setFecha_impresion( (rs.getString("fecha_impresion") != null)?rs.getString("fecha_impresion"):"0099-01-01 00:00:00" );
                hoc.setFecha_cargue(rs.getString("Fecha_cargue"));
                hoc.setDestinatarios(rs.getString("destinatarios"));
                hoc.setNumpla(rs.getString("numpla"));
                hoc.setNombre(rs.getString("nombre_contacto"));
                hoc.setTelefono(rs.getString("tele_contacto"));
                vec_buscar.add( hoc );
                
            }
            
        } catch( SQLException e ){
            
            throw new SQLException( "Error en buscarOrdenDeCarga [ImprimirOrdenDeCargaDAO]......." + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_ORDEN_DE_CARGA" );
            
        }
        
    }
    
    
    /** Funcion publica que actualiza algunos datos de la orden_de_carga */
    public void Update_Fecha_De_Impresion( String user_update, String distrito, String orden ) throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_UPDATE_FECHA_DE_IMPRESION" );
            
            st.setString( 1, user_update );
            st.setString( 2, distrito );
            st.setString( 3, orden );
            
            st.executeUpdate();
            
        } catch( SQLException e ){
            
            throw new SQLException( "Error en Update_Fecha_De_Impresion [ImprimirOrdenDeCargaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_UPDATE_FECHA_DE_IMPRESION" );
            
        }
    }
    
       /**
     *Metodo que inserta orden de carga
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void insert() throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_INSERT" );
            st.setString(1,hoc.getDstrct());
            st.setString(2,hoc.getOrden());
            st.setString(3,hoc.getCodciu());
            st.setString(4,hoc.getRemitente());
            st.setString(5,hoc.getConductor());
            st.setString(6,hoc.getPlaca());
            st.setString(7,hoc.getTrailer());
            st.setString(8,hoc.getContenido());
            st.setString(9,hoc.getStd_job_no());
            st.setString(10,hoc.getEntregar());
            st.setString(11,hoc.getObservacion());
            st.setString(12,hoc.getPrecinto1());
            st.setString(13,hoc.getPrecinto2());
            st.setString(14,hoc.getPrecinto3());
            st.setString(15,hoc.getPrecinto4());
            st.setString(16,hoc.getPrecinto5());
            st.setString(17,hoc.getCreation_user());
            st.setString(18,hoc.getBase());
            st.setString(19,hoc.getPrecintoc1());
            st.setString(20,hoc.getPrecintoc2());
            st.setString(21,hoc.getContenedores());
            st.setString(22,hoc.getTipocont());
            st.setString(23,hoc.getTipotrailer());
            st.setString(24,hoc.getFecha_cargue());
            st.setString(25,hoc.getDestinatarios());
            st.setString(26,hoc.getNombre());
            st.setString(27,hoc.getTelefono());
            st.executeUpdate();
            
        } catch( SQLException e ){
            
            throw new SQLException( "Error en Update_Fecha_De_Impresion [ImprimirOrdenDeCargaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_INSERT" );
            
        }
        
    }
        
     /**
     *Metodo que modifica una orden de carga
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void update() throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_UPDATE" );
            st.setString(1,hoc.getCodciu());
            st.setString(2,hoc.getRemitente());
            st.setString(3,hoc.getConductor());
            st.setString(4,hoc.getPlaca());
            st.setString(5,hoc.getTrailer());
            st.setString(6,hoc.getContenido());
            st.setString(7,hoc.getStd_job_no());
            st.setString(8,hoc.getEntregar());
            st.setString(9,hoc.getObservacion());
            st.setString(10,hoc.getPrecinto1());
            st.setString(11,hoc.getPrecinto2());
            st.setString(12,hoc.getPrecinto3());
            st.setString(13,hoc.getPrecinto4());
            st.setString(14,hoc.getPrecinto5());
            st.setString(15,hoc.getCreation_user());
            st.setString(16,hoc.getBase());
            st.setString(17,hoc.getPrecintoc1());
            st.setString(18,hoc.getPrecintoc2());
            st.setString(19,hoc.getContenedores());
            st.setString(20,hoc.getTipocont());
            st.setString(21,hoc.getTipotrailer());
            st.setString(22,hoc.getFecha_cargue());
            st.setString(23,hoc.getDestinatarios());
            st.setString(24,hoc.getNombre());
            st.setString(25,hoc.getTelefono());
            st.setString(26,hoc.getDstrct());
            st.setString(27,hoc.getOrden());
            
            
            st.executeUpdate();
            
        } catch( SQLException e ){
            
            throw new SQLException( "Error en Update_Fecha_De_Impresion [ImprimirOrdenDeCargaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_UPDATE" );
            
        }
        
    }
    
    /**
     *Metodo que anula una orden de carga
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void delete() throws SQLException {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_DELETE" );
            st.setString(1,hoc.getDstrct());
            st.setString(2,hoc.getOrden());
            
            st.executeUpdate();
            
        } catch( SQLException e ){
            
            throw new SQLException( "Error en delete [ImprimirOrdenDeCargaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_DELETE" );
            
        }
        
    }
    
    /**
     * Setter for property hoc.
     * @param hoc New value of property hoc.
     */
    public void setHoc(com.tsp.operation.model.beans.HojaOrdenDeCarga hoc) {
        this.hoc = hoc;
    }
    /**
     *Metodo que retorna un String con comandos sql para actualizar
     *una orden de cargue con el numero de la planilla que la utilizo
     *@return: String con comandos sql
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcar() throws SQLException {
        
        PreparedStatement st = null;
        String sql="";
        try {
            
            st = this.crearPreparedStatement( "SQL_MARCAR" );
            st.setString(1,hoc.getNumpla());
            st.setString(2,hoc.getDstrct());
            st.setString(3,hoc.getOrden());
            sql = st.toString();
            //st.executeUpdate();
            
        } catch( SQLException e ){
            
            throw new SQLException( "Error en Marcar Orden de Carga [ImprimirOrdenDeCargaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_MARCAR" );
            
        }
        return sql;
    }
   /**
     *Metodo llena un vector de ordenes de cargue realizadas por un usuario
     *@autor: Karen Reales
     *@param: Usuario usuario
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listar(Usuario usuario) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            
            st = this.crearPreparedStatement( "SQL_LISTA_USUARIO" );
            st.setString(1, usuario.getDstrct());
            st.setString(2, usuario.getLogin());
            rs = st.executeQuery();
            this.vec_lista = new Vector();
            
            while( rs.next() ){
                
                hoc = new HojaOrdenDeCarga();
                hoc.setOrden( (rs.getString("orden") != null)?rs.getString("orden"):"" );
                hoc.setStd_job_no( (rs.getString("std_job_desc") != null)?rs.getString("std_job_desc"):"" );
                hoc.setPlaca( (rs.getString("placa") != null)?rs.getString("placa"):"" );
                hoc.setCreation_user(rs.getString("Creation_user"));
                vec_lista.add( hoc );
                
            }
            
        } catch( SQLException e ){
            
            throw new SQLException( "Error en Listar [ImprimirOrdenDeCargaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_LISTA_USUARIO" );
            
        }
        
    }
     /**
     *Metodo que retorna un Vector con el reporte de Cumplimiento Colocacion     
     *@param:String Inicio fecha inicial
     *@param: String fin fecha final
     *@return: Vector con HojaOrdenDeCarga
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public Vector obtenerReporteCumplimiento(String inicio, String fin) throws SQLException{ 
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            
            st = this.crearPreparedStatement( "SQL_REPORTE_CUMPLIMIENTO" );
            st.setString(1, inicio);
            st.setString(2, fin);
                        
            rs = st.executeQuery();
            
            String dif="";
            this.vec_lista = new Vector();
            
            while( rs.next() ){
                
                hoc = new HojaOrdenDeCarga();
                hoc.setOrden( (rs.getString("orden") != null)?rs.getString("orden"):"" );
                hoc.setNumpla( (rs.getString("numpla") != null)? rs.getString("numpla"):"");
                hoc.setFecha_cargue( (rs.getString("fecha_cargue") != null)? rs.getString("fecha_cargue"):"");
                //date_time_traffic(fecha real)
                hoc.setFecha_impresion( (rs.getString("date_time_traffic") != null)? rs.getString("date_time_traffic"):"");
                hoc.setPlaca( (rs.getString("placa") != null)? rs.getString("placa"):"");
                hoc.setCiudad( (rs.getString("ciudad") != null)? rs.getString("ciudad"):"");
                //origen
                hoc.setCodciu( (rs.getString("origen") != null)? rs.getString("origen"):"");
                hoc.setDestino( (rs.getString("destino") != null)? rs.getString("destino"):"");
                hoc.setStd_job_no( (rs.getString("std_job_no") != null)?rs.getString("std_job_no"):"" );
                //standard job descripcion
                hoc.setDstrct( (rs.getString("std_job_desc") != null)?rs.getString("std_job_desc"):"" );
                //cliente
                hoc.setEmpresa( (rs.getString("cliente") != null)?rs.getString("cliente"):"" );
                //diferencia
                dif = (rs.getString("diferencia") != null)?rs.getString("diferencia"):"" ;
                if(dif.length()>0){
                    dif = dif.substring(0,dif.indexOf(':'));
                    dif = dif.replaceAll(" days | day ", ":");
                }
                hoc.setDireccion(dif);
                
                vec_lista.add( hoc );
                
            }
            
        } catch( SQLException e ){
            
            throw new SQLException( "Error en Listar [ImprimirOrdenDeCargaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar( "SQL_REPORTE_CUMPLIMIENTO" );
        }
        return vec_lista;
    }
    /**
     *Metodo llena un vector de ordenes de cargue filtradas por el usuario
     *@autor: Karen Reales
     *@param: String usuario, String agencia, String cliente
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void filtrar(String usuario, String agencia, String cliente) throws SQLException {
        
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            
            st = this.crearPreparedStatement( "SQL_FILTRO" );
            st.setString(1,cliente+"%");
            st.setString(2,agencia+"%");
            st.setString(3,usuario+"%");
            rs = st.executeQuery();
            ////System.out.println("SQL "+st.toString());
            this.vec_lista = new Vector();
            
            while( rs.next() ){
                
                hoc = new HojaOrdenDeCarga();
                hoc.setOrden( (rs.getString("orden") != null)?rs.getString("orden"):"" );
                hoc.setStd_job_no( (rs.getString("std_job_desc") != null)?rs.getString("std_job_desc"):"" );
                hoc.setPlaca( (rs.getString("placa") != null)?rs.getString("placa"):"" );
                hoc.setCreation_user(rs.getString("Creation_user"));
                vec_lista.add( hoc );
                
            }
            
        } catch( SQLException e ){
            
            throw new SQLException( "Error en Listar [ImprimirOrdenDeCargaDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                
                try{
                    
                    st.close();
                    
                } catch( SQLException e ){
                    
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_FILTRO" );
            
        }
        
    }
    
}
