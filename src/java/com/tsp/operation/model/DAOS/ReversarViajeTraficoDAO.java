/**
 * Nombre        ReversarViajeTraficoDAO.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         24 de noviembre de 2006, 04:36 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.DAOS;

import java.sql.*;
import java.util.*;

public class ReversarViajeTraficoDAO extends MainDAO{
    
    
    private Hashtable ht;
    
    
    
    
    /** Crea una nueva instancia de  ReversarViajeTraficoDAO */
    public ReversarViajeTraficoDAO() {
        super("ReversarViajeTraficoDAO.xml");
    }
    
    
    /**
     * M�todo que retorna true si existe la planilla en ingreso trafico
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean existePlanillaIngresoTrafico( String numpla ) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        try{
            ps = crearPreparedStatement( "SQL_EXISTE_PLANILLA" );
            ps.setString( 1, numpla );            
            rs = ps.executeQuery();
            if(rs.next()){
                existe = true;
            }
        }catch( SQLException ex ){
            throw new SQLException( "ERROR existePlanillaIngresoTrafico( String numpla ) "+ex.getMessage()+", "+ex.getErrorCode() );
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar( "SQL_EXISTE_PLANILLA" );
        }
        return existe;
    }
    
    
    
    /**
     * M�todo que retorna true si existe la planilla en trafico
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean existePlanillaTrafico( String numpla ) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        try{
            ps = crearPreparedStatement( "SQL_EXISTE_TRAFICO" );
            ps.setString( 1, numpla );            
            rs = ps.executeQuery();
            if(rs.next()){
                existe = true;
            }
        }catch( SQLException ex ){
            throw new SQLException( "ERROR existePlanillaTrafico( String numpla ) "+ex.getMessage()+", "+ex.getErrorCode() );
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar( "SQL_EXISTE_TRAFICO" );
        }
        return existe;
    }    
    
    
    
    
    /**
     * M�todo que retorna true si la ultima planilla en rep_mov_trafico
     * es un tipo de reporte finalizado
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public boolean esPlanillaFinalizada( String numpla ) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        try{
            ps = crearPreparedStatement( "SQL_GET_ULTIMO_REP_MOV_TRAFICO" );
            ps.setString( 1, numpla );            
            rs = ps.executeQuery();
            if( rs.next() ){
                String tiporeporte = rs.getString( "tipo_reporte" );
                tiporeporte = (tiporeporte != null)? tiporeporte : "";
                if( tiporeporte.equalsIgnoreCase( "ECL" ) || tiporeporte.equalsIgnoreCase( "EFR" ) || tiporeporte.equalsIgnoreCase( "EIN" )){
                    existe = true;
                }
            }
        }catch( SQLException ex ){
            throw new SQLException( "ERROR esPlanillaFinalizada( String numpla ) "+ex.getMessage()+", "+ex.getErrorCode() );
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar( "SQL_GET_ULTIMO_REP_MOV_TRAFICO" );
        }
        return existe;
    }

    

    
    /**
     * M�todo que obtiene el reporte a copiar en ingreso_trafico
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.     
     **/
    public void obtenerReporteTrafico( String numpla ) throws SQLException{      
        
        PreparedStatement st = null;
        ResultSet rs = null;
        try{
            st = this.crearPreparedStatement( "SQL_GET_COPIA_TRAFICO" );
            st.setString( 1, numpla );            
            rs = st.executeQuery();
            
            ht = new Hashtable();
            if( rs.next() ){
                ht.put( "reg_status", (rs.getString( "reg_status" )!=null)? rs.getString( "reg_status" ) : "" );
                ht.put( "dstrct", (rs.getString( "dstrct" )!=null)? rs.getString( "dstrct" ) : "" );
                ht.put( "planilla", (rs.getString( "planilla" )!=null)? rs.getString( "planilla" ) : "" );
                ht.put( "placa", (rs.getString( "placa" )!=null)? rs.getString( "placa" ) : "" );
                ht.put( "cedcon", (rs.getString( "cedcon" )!=null)? rs.getString( "cedcon" ) : "" );
                ht.put( "nomcond", (rs.getString( "nomcond" )!=null)? rs.getString( "nomcond" ) : "" );
                ht.put( "cedprop", (rs.getString( "cedprop" )!=null)? rs.getString( "cedprop" ) : "" );
                ht.put( "nomprop", (rs.getString( "nomprop" )!=null)? rs.getString( "nomprop" ) : "" );
                ht.put( "origen", (rs.getString( "origen" )!=null)? rs.getString( "origen" ) : "" );
                ht.put( "nomorigen", (rs.getString( "nomorigen" )!=null)? rs.getString( "nomorigen" ) : "" );
                ht.put( "destino", (rs.getString( "destino" )!=null)? rs.getString( "destino" ) : "" );
                ht.put( "nomdestino", (rs.getString( "nomdestino" )!=null)? rs.getString( "nomdestino" ) : "" );
                ht.put( "zona", (rs.getString( "zona" )!=null)? rs.getString( "zona" ) : "" );
                ht.put( "desczona", (rs.getString( "desczona" )!=null)? rs.getString( "desczona" ) : "" );
                ht.put( "escolta", (rs.getString( "escolta" )!=null)? rs.getString( "escolta" ) : "" );
                ht.put( "caravana", (rs.getString( "caravana" )!=null)? rs.getString( "caravana" ) : "" );
                ht.put( "fecha_despacho", (rs.getString( "fecha_despacho" )!=null)? rs.getString( "fecha_despacho" ) : "" );
                ht.put( "pto_control_ultreporte", (rs.getString( "pto_control_ultreporte" )!=null)? rs.getString( "pto_control_ultreporte" ) : "" );
                ht.put( "nompto_control_ultreporte", (rs.getString( "nompto_control_ultreporte" )!=null)? rs.getString( "nompto_control_ultreporte" ) : "" );
                ht.put( "fecha_ult_reporte", (rs.getString( "fecha_ult_reporte" )!=null)? rs.getString( "fecha_ult_reporte" ) : "" );
                ht.put( "pto_control_proxreporte", (rs.getString( "pto_control_proxreporte" )!=null)? rs.getString( "pto_control_proxreporte" ) : "" );
                ht.put( "nompto_control_proxreporte", (rs.getString( "nompto_control_proxreporte" )!=null)? rs.getString( "nompto_control_proxreporte" ) : "" );
                ht.put( "fecha_prox_reporte", (rs.getString( "fecha_prox_reporte" )!=null)? rs.getString( "fecha_prox_reporte" ) : "" );
                ht.put( "ult_observacion", (rs.getString( "ult_observacion" )!=null)? rs.getString( "ult_observacion" ) : "" );
                ht.put( "last_update", (rs.getString( "last_update" )!=null)? rs.getString( "last_update" ) : "" );
                ht.put( "user_update", (rs.getString( "user_update" )!=null)? rs.getString( "user_update" ) : "" );
                ht.put( "creation_date", (rs.getString( "creation_date" )!=null)? rs.getString( "creation_date" ) : "" );
                ht.put( "creation_user", (rs.getString( "creation_user" )!=null)? rs.getString( "creation_user" ) : "" );
                ht.put( "base", (rs.getString( "base" )!=null)? rs.getString( "base" ) : "" );
                ht.put( "via", (rs.getString( "ruta_pla" )!=null)? rs.getString( "ruta_pla" ) : ""  );
                ht.put( "fecha_salida", (rs.getString( "fecha_salida" )!=null)? rs.getString( "fecha_salida" ) : "" );
                ht.put( "cel_cond", (rs.getString( "cel_cond" )!=null)? rs.getString( "cel_cond" ) : "" );
                ht.put( "cliente", (rs.getString( "cliente" )!=null)? rs.getString( "cliente" ) : "" );
                ht.put( "tipo_despacho", (rs.getString( "tipo_despacho" )!=null)? rs.getString( "tipo_despacho" ) : "" );
                ht.put( "tipo_reporte", (rs.getString( "tipo_reporte" )!=null)? rs.getString( "tipo_reporte" ) : "" );
                ht.put( "nom_reporte", (rs.getString( "nom_reporte" )!=null)? rs.getString( "nom_reporte" ) : "" );
                ht.put( "cod_ciudad", (rs.getString( "cod_ciudad" )!=null)? rs.getString( "cod_ciudad" ) : "" );
                ht.put( "nom_ciudad", (rs.getString( "nom_ciudad" )!=null)? rs.getString( "nom_ciudad" ) : "" );
                ht.put( "cedcon_anterior", (rs.getString( "cedcon_anterior" )!=null)? rs.getString( "cedcon_anterior" ) : "" );
                ht.put( "nomcond_anterior", (rs.getString( "nomcond_anterior" )!=null)? rs.getString( "nomcond_anterior" ) : "" );
                ht.put( "cedprop_anterior", (rs.getString( "cedprop_anterior" )!=null)? rs.getString( "cedprop_anterior" ) : "" );
                ht.put( "nomprop_anterior", (rs.getString( "nomprop_anterior" )!=null)? rs.getString( "nomprop_anterior" ) : "" );
                ht.put( "placa_anterior", (rs.getString( "placa_anterior" )!=null)? rs.getString( "placa_anterior" ) : "" );
                ht.put( "cadena", (rs.getString( "cadena" )!=null)? rs.getString( "cadena" ) : "" );                
            }            
        }
        catch(SQLException ex){
            throw new SQLException("ERROR obtenerReporteTrafico( String numpla ), "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            rs.close();
            st.close();            
            desconectar("SQL_GET_COPIA_TRAFICO");
        }        
    }
    
    /**
     * Getter for property ht.
     * @return Value of property ht.
     */
    public Hashtable getHt() {
        return ht;
    }    
    
    /**
     * Setter for property ht.
     * @param ht New value of property ht.
     */
    public void setHt(Hashtable ht) {
        this.ht = ht;
    }    
    
    /**
     * M�todo que Obtiene el String de via
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     * @return.......boolean
     **/
    public String obtenerVia( String ruta ) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        String via = "";
        try{
            String origen = "";
            String destino = "";
            String secuencia = "";
            if( ruta != null && ruta.length() > 4 ){
                origen = ruta.substring( 0, 2 );
                destino = ruta.substring( 2, 4 );
                secuencia = ruta.substring( 4, ruta.length() );
            }
            ps = crearPreparedStatement( "SQL_GET_VIA" );
            ps.setString( 1, origen );
            ps.setString( 2, destino );
            ps.setString( 3, secuencia );
            rs = ps.executeQuery();            
            if( rs.next() ){
                via = rs.getString( "via" );
            }            
        }catch( SQLException ex ){
            throw new SQLException( "ERROR obtenerVia( String ruta ) "+ex.getMessage()+", "+ex.getErrorCode() );
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar( "SQL_GET_VIA" );
        }
        return via;
    } 
    
    
    
    /**
     * M�todo que insertar en ingreso_trafico
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.     
     **/
    public void insertarCopiaIngresoTrafico() throws SQLException{        
        PreparedStatement st = null;        
        try{
            st = this.crearPreparedStatement( "SQL_INSERT_COPIA_INGRESO_TRAFICO" );
                        
            st.setString( 1, (String)ht.get( "reg_status" ) );
            st.setString( 2, (String)ht.get( "dstrct" ) );
            st.setString( 3, (String)ht.get( "planilla" ) );
            st.setString( 4, (String)ht.get( "placa" ) );
            st.setString( 5, (String)ht.get( "cedcon" ) );
            st.setString( 6, (String)ht.get( "nomcond" ) );
            st.setString( 7, (String)ht.get( "cedprop" ) );
            st.setString( 8, (String)ht.get( "nomprop" ) );
            st.setString( 9, (String)ht.get( "origen" ) );
            st.setString( 10, (String)ht.get( "nomorigen" ) );
            st.setString( 11, (String)ht.get( "destino" ) );
            st.setString( 12, (String)ht.get( "nomdestino" ) );
            st.setString( 13, (String)ht.get( "zona" ) );
            st.setString( 14, (String)ht.get( "desczona" ) );
            st.setString( 15, (String)ht.get( "escolta" ) );
            st.setString( 16, (String)ht.get( "caravana" ) );
            st.setString( 17, (String)ht.get( "fecha_despacho" ) );
            st.setString( 18, (String)ht.get( "pto_control_ultreporte" ) );
            st.setString( 19, (String)ht.get( "nompto_control_ultreporte" ) );
            st.setString( 20, (String)ht.get( "fecha_ult_reporte" ) );
            st.setString( 21, (String)ht.get( "pto_control_proxreporte" ) );
            st.setString( 22, (String)ht.get( "nompto_control_proxreporte" ) );
            st.setString( 23, (String)ht.get( "fecha_prox_reporte" ) );
            st.setString( 24, (String)ht.get( "ult_observacion" ) );
            st.setString( 25, (String)ht.get( "last_update" ) );
            st.setString( 26, (String)ht.get( "user_update" ) );
            st.setString( 27, (String)ht.get( "creation_date" ) );
            st.setString( 28, (String)ht.get( "creation_user" ) );
            st.setString( 29, (String)ht.get( "base" ) );
            st.setString( 30, (String)ht.get( "via" ) );
            st.setString( 31, (String)ht.get( "fecha_salida" ) );
            st.setString( 32, (String)ht.get( "cel_cond" ) );
            st.setString( 33, (String)ht.get( "cliente" ) );
            st.setString( 34, (String)ht.get( "tipo_despacho" ) );
            st.setString( 35, (String)ht.get( "tipo_reporte" ) );
            st.setString( 36, (String)ht.get( "nom_reporte" ) );
            st.setString( 37, (String)ht.get( "cod_ciudad" ) );
            st.setString( 38, (String)ht.get( "nom_ciudad" ) );
            st.setString( 39, (String)ht.get( "cedcon_anterior" ) );
            st.setString( 40, (String)ht.get( "nomcond_anterior" ) );
            st.setString( 41, (String)ht.get( "cedprop_anterior" ) );
            st.setString( 42, (String)ht.get( "nomprop_anterior" ) );
            st.setString( 43, (String)ht.get( "placa_anterior" ) );
            st.setString( 44, (String)ht.get( "cadena" ) );
            
            st.executeUpdate();
            this.AnularUltimoReporte((String)ht.get( "planilla" ));
        }
        catch(SQLException ex){
            throw new SQLException("ERROR obtenerReporteTrafico( String numpla ), "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{            
            st.close();             
            desconectar("SQL_INSERT_COPIA_INGRESO_TRAFICO");
        }        
    }
    
    
    
    /**
     * M�todo para anular el ultimo reporte de trafico
     * @autor.......Ing. Ivan Gomez
     * @param numpla Numero de la planilla
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void AnularUltimoReporte( String numpla ) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = crearPreparedStatement( "SQL_ANULAR_ULT_REPORTE" );
            ps.setString( 1, numpla );            
            ps.executeUpdate();
           
        }catch( SQLException ex ){
            throw new SQLException( "ERROR SQL_ANULAR_ULT_REPORTE "+ex.getMessage()+", "+ex.getErrorCode() );
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            desconectar( "SQL_ANULAR_ULT_REPORTE" );
        }
        
    }
    
}

