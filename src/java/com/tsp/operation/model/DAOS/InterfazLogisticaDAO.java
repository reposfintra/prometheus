/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.DAOS;

import com.google.gson.JsonArray;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.BeansAgencia;
import com.tsp.operation.model.beans.BeansAnticipo;
import com.tsp.operation.model.beans.BeansConductor;
import com.tsp.operation.model.beans.BeansPropietario;
import com.tsp.operation.model.beans.EDSPropietarioBeans;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

/**
 *
 * @author egonzalez
 */
public interface InterfazLogisticaDAO {
    
    /**
     *
     * @param user
     * @return
     */
    public String getTransportadorasLista(Usuario user);
    
    public String getTitulosGrid();
    
    /**
     *
     * @param transportadora
     * @param fecha_inicio
     * @param fecha_fin
     * @return
     */
    public String getReportProduction(String transportadora, String fecha_inicio, String fecha_fin, String c_conductor, String c_propietario, String planilla, String placa, String factura);
    
    /**
     *
     * @param user
     * @param fecha_inicio
     * @param fecha_fin
     * @param nombre
     * @param nit
     * @return
     */
    public String getReportExtractEds(Usuario user,String fecha_inicio, String fecha_fin, String nombre , String nit);
    
    /**
     *
     * @param user
     * @return
     */
    public String loadEds(Usuario user);
    
    /**
     *
     * @param user
     * @param propietario
     * @param placa
     * @param planilla
     * @param fecha_inicio
     * @param fecha_fin
     * @return
     */
    public String getReportExtactOwner(Usuario user, String propietario,
            String placa, String planilla, String fecha_inicio, String fecha_fin);
    
    /**
     *
     * @param user
     * @return
     */
    public String loadOwner(Usuario user);
    
    public String loadPlaca(int id_propietario);
    
    /**
     *
     * @param id_transportadora
     * @return
     */
    public String aprobateAavances(int id_transportadora);
    
    /**
     *
     * @param array
     * @param u
     * @return
     */
    public String aprobarTransferencias(JsonArray array,Usuario u);
    
    /**
     *
     * @param id_transportadora
     * @param banco
     * @param cod_banco
     * @param cuenta
     * @param tipo_cuenta
     * @param u
     * @return
     */
    public String buscarAnticiporTransferencia(int id_transportadora, String banco,String cod_banco, String cuenta, String tipo_cuenta,Usuario u); 
    
    /**
     *
     * @param array
     * @param u
     * @return
     */
    public String desAprobarAnticipos(JsonArray array,Usuario u);
    
    /**
     *
     * @param array
     * @param u
     * @return
     */
    public String anularAnticipos(JsonArray array,Usuario u);
    
    /**
     *
     * @param array
     * @param u
     * @param banco
     * @return
     */
    public String transferir(JsonArray array,Usuario u, String [] banco);
    
    /**
     *
     * @param transportadora
     * @param fecha_inicio
     * @param fecha_fin
     * @return
     */
    public String reporteProduccionTrans(String transportadora, String fecha_inicio, String fecha_fin);
    
    /**
     *
     * @param transportadora
     * @param fecha_corrida
     * @param u
     * @return
     */
    public String cuentaCobroTransportadora(int transportadora,String fecha_corrida, Usuario u);
    
    /**
     *
     * @param transportadora
     * @param fecha_corrida
     * @param u
     * @return
     */
    public String generarCuentaCobroTransportadora(int transportadora,String fecha_corrida, Usuario u);
    
    /**
     *
     * @param transportadora
     * @param fecha_corrida
     * @param u
     * @return
     */
    public String detalleCorridaCXCTransportadora(int transportadora,String fecha_corrida, Usuario u);
    
    /**
     *
     * @param id_eds
     * @param fecha
     * @param u
     * @return
     */
    public String facturasPendientesEDS(int id_eds,String fecha,Usuario u);
    
    /**
     *
     * @param id_eds
     * @param fecha
     * @param u
     * @return
     */
    public String facturarEds(int id_eds,String fecha,Usuario u);
    
    /**
     *
     * @param id_eds
     * @param u
     * @return
     */
    public EDSPropietarioBeans buscarPropietario(int id_eds,Usuario u);
    
    public ArrayList<MenuOpcionesModulos> cargarMenuTransportadora(String usuario, String distrito);
    
    public boolean copiarArchivoEnCarpetaDestino(String usuario, String rutaOrigen, String rutaDestino, String filename);
    
    public String  cargarAnticposTransportadora(String id_transportadora, String fechaini, String fechafin, String planilla, String placa, String producto);
    
    public String generarJsonListarReanticipo(String id_transportadora, String planilla);
    
    public String cargarProductosTransportadora(Usuario u);
    
    public void setearBeansPropietario(BeansPropietario prop);

    public void setearBeansConductor(BeansConductor cond);

    public void setearBeansAnticipo(BeansAnticipo veh);
    
    public BeansAgencia getInfoAgenciaUsuario(String usuario);   
    
    
    /**
     *
     * @param array
     * @param u
     * @return
     */
    public String reversarLote(JsonArray array,Usuario u);
    
    
    /**
     *
     * @param id_transportadora
     * @return
     */
    public String buscarLote(String id_transportadora);
    
    /**
     *
     * @param user
     * @return
     */
    public String getTransportadorasListaXusuario(Usuario user);
    
}
