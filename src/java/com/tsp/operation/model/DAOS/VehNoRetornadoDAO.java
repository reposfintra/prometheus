/******************************************************************
* Nombre ......................VehNoRetornadoDAO.java
* Descripci�n..................Clase DAO para Veh�culos No Retornados
* Autor........................Armando Oviedo
* Fecha........................18/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import java.sql.*;
import java.util.*;

public class VehNoRetornadoDAO  extends MainDAO {
    
    private VehNoRetornado vnr;
    private Connection con;
    private Vector vecvnr;
    private Vector causas;
    
    /****  SQL QUERIES *****/
    
    private static String SQL_INSERT = "INSERT INTO veh_no_retornado VALUES(?,?,?,?,?,?,?,?,?,?)";
    
    private static String SQL_UPDATE = "UPDATE " +
                                       "    veh_no_retornado " +
                                       "SET " +
                                       "    last_update='now()', user_update=?, causa=? " +                                       
                                       "WHERE " +
                                       "    placa=? AND fecha=?";
    
    private static String SQL_BUSCAR = "SELECT * FROM veh_no_retornado WHERE placa=? AND fecha=?";
    
    private static String SQL_UPDEXIST = "UPDATE " +
                                         "  veh_no_retornado " +
                                         "SET " +
                                         "  REG_STATUS='', causa=?," +
                                         "  last_update='now()', user_update=?, base=?, dstrct=? " +
                                         "WHERE " +
                                         "  placa=? AND fecha=?";
    
    private static String SQL_EXISTE = "SELECT causa FROM veh_no_retornado WHERE placa=? AND fecha=? AND reg_status!='A'";
    
    private static String SQL_EXISTEANULADO = "SELECT placa FROM veh_no_retornado WHERE placa=? AND fecha=? AND reg_status='A'";
    
    private static String SQL_DELETE = "UPDATE " +
                                         "  veh_no_retornado SET reg_status = 'A', last_update='now()', user_update=?  " +
                                         "WHERE " +
                                         "  placa=? AND fecha=?" ;
    
    private static String SQL_BUSCARTODOSVNR = "SELECT " +
                                               "     TO_CHAR(fecha, 'YYYY-MM-DD'), placa, causa " +
                                               "FROM " +
                                               "     veh_no_retornado " +
                                               "WHERE " +
                                               "     reg_status<>'A'";        
    
    private static String SQL_BUSCAR_CAUSAS = "SELECT " +
                                              "     table_code, descripcion " +
                                              "FROM " +
                                              "     tablagen " +
                                              "WHERE " + 
                                              "     table_type = 'VNR' " +
                                              "AND reg_status!='A'";
    
    private static String SQL_BUSCAR_VEHICULOS_NO_RETORNADOS = "SELECT " +
                                                               "    TO_CHAR(fecha, 'YYYY-MM-DD'), placa, causa " +
                                                               "FROM " +
                                                               "    veh_no_retornado " +
                                                               "WHERE " +
                                                               "    (fecha LIKE ? AND placa LIKE ?) AND reg_status<>'A'";
    
    private static String SQL_BUSCAR_NOMBRECAUSA = "SELECT " +
                                                   "    descripcion " +
                                                   "FROM " +
                                                   "    tablagen " +
                                                   "WHERE " +
                                                   "    table_code=? " +
                                                   "AND " +
                                                   "    table_type = 'VNR' " +
                                                   "AND " +
                                                   "    reg_status!='A'";
    
    /** Creates a new instance of VehNoRetornadoDAO */
    public VehNoRetornadoDAO() {
      super("VehNoRetornadoDAO.xml");//JJCastro fase2
    }
    
    /**
     * M�todo que retorna un vector con las causas de VNR
     * @autor.......Armando Oviedo               
     * @version.....1.0.     
     **/
    public Vector getCausas(){
        return this.causas;
    }
    
    /**
     * M�todo que busca causas de VNR definidas en tablagen
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......VehNoRetornado de
     **/
    public void buscarCausas() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        causas = new Vector();
        String query = "SQL_BUSCAR_CAUSAS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = ps.executeQuery();
                while (rs.next()) {
                    Vector fila = new Vector();
                    fila.add(rs.getString(1));
                    fila.add(rs.getString(2));
                    causas.add(fila);
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que setea un objeto VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......VehNoRetornado de
     **/     
    public void setVehiculoNoRetornado(VehNoRetornado vnr){
        this.vnr = vnr;
    }
    
    /**
     * M�todo que retorna un objeto VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......VehNoRetornado de
     **/     
    public VehNoRetornado getVehiculoNoRetornado(){
        return this.vnr;
    }
    
    /**
     * M�todo que retorna el nombre de la causa de no retorno
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......nombre causa
     **/         
    public String getNombreCausa(String codcausa) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;  
        String nombre = "";
        String query = "SQL_BUSCAR_NOMBRECAUSA";//JJCastro fase2
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, codcausa);
                rs = ps.executeQuery();
                if (rs.next()) {
                    nombre = rs.getString(1);
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nombre;
    }
    
    /**
     * M�todo que busca y setea un objeto VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......VehNoRetornado de cargado con el set
     **/
    public void buscarVehiculosNoRetornados() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;  
        vecvnr = new Vector();
        String query = "SQL_BUSCAR_VEHICULOS_NO_RETORNADOS";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, vnr.getFecha().concat("%"));
                ps.setString(2, vnr.getPlaca().concat("%"));
                rs = ps.executeQuery();
                while (rs.next()) {
                    VehNoRetornado tmp = new VehNoRetornado();
                    tmp.setFecha(rs.getString(1));
                    tmp.setPlaca(rs.getString(2));
                    tmp.setCausa(getNombreCausa(rs.getString(3)));
                    vecvnr.add(tmp);
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que busca y setea un objeto VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......VehNoRetornado de cargado con el set
     **/ 
    public void buscarVehiculoNoRetornado() throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        VehNoRetornado tmp = null;
        String query = "SQL_BUSCAR";//JJCastro fase2
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, vnr.getPlaca());
            ps.setString(2, vnr.getFecha());
            rs = ps.executeQuery();
            if(rs.next()){
                tmp = new VehNoRetornado();                
                tmp = tmp.loadResultSet(rs);                                                            
            }                        
            setVehiculoNoRetornado(tmp);            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que retorna un boolean si existe el VehNoRetornado o no
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo del descuento cargado con el set
     **/     
    public boolean existeVehiculoNoRetornado() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, vnr.getPlaca());
                ps.setString(2, vnr.getFecha());
                rs = ps.executeQuery();
                return rs.next();
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return false; 
    }
    
    /**
     * M�todo que retorna un boolean si existe el VehNoRetornado o no, anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......c�digo del descuento cargado con el set
     **/   
    public boolean existeVehiculoNoRetornadoAnulado() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTEANULADO";//JJCastro fase2
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                ps.setString(1, vnr.getPlaca());
                ps.setString(2, vnr.getFecha());
                rs = ps.executeQuery();
                if (rs.next()) {
                    existe = true;
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return existe;        
    }
    
    /**
     * M�todo que actualiza el reg_status de un VehNoRetornado anulado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void updateVehiculoNoRetornadoAnulado() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_UPDEXIST";//JJCastro fase2
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1,vnr.getCausa());                        
            ps.setString(2,vnr.getUserUpdate());
            ps.setString(3,vnr.getBase());
            ps.setString(4,vnr.getDstrct());
            ps.setString(5,vnr.getPlaca());    
            ps.setString(6,vnr.getFecha());            
            ps.executeUpdate();
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que agrega un VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void addVehiculoNoRetornado() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_INSERT";//JJCastro fase2
        try{            
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1,vnr.getRegStatus());
            ps.setString(2,vnr.getDstrct());
            ps.setString(3,vnr.getPlaca());
            ps.setString(4,vnr.getCausa());
            ps.setString(5,vnr.getFecha());
            ps.setString(6,vnr.getLastUpdate());
            ps.setString(7,vnr.getUserUpdate());
            ps.setString(8,vnr.getCreationDate());
            ps.setString(9,vnr.getCreationUser());
            ps.setString(10,vnr.getBase());            
            ps.executeUpdate();
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que modifica un VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void updateVehiculoNoRetornado() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_UPDATE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, vnr.getUserUpdate());
            ps.setString(2, vnr.getCausa());
            ps.setString(3, vnr.getPlaca());
            ps.setString(4, vnr.getFecha());                        
            ps.executeUpdate();           
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que elimina un VehNoRetornado
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void deleteVehiculoNoRetornado() throws SQLException{
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        String query = "SQL_DELETE";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, vnr.getUserUpdate());
            ps.setString(2, vnr.getPlaca());
            ps.setString(3, vnr.getFecha());
            ps.executeUpdate();            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que setea un vector que contiene todos los objetos de la tabla
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     **/ 
    public void buscarTodosVehiculosNoRetornados() throws SQLException{        
        Connection con = null;//JJCastro fase2
        PreparedStatement ps = null;
        ResultSet rs = null;
        vecvnr = new Vector();
        String query = "SQL_BUSCARTODOSVNR";//JJCastro fase2
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();            
            while(rs.next()){
                VehNoRetornado tmp = new VehNoRetornado();
                tmp.setFecha(rs.getString(1));
                tmp.setPlaca(rs.getString(2));
                tmp.setCausa(getNombreCausa(rs.getString(3)));                
                vecvnr.add(tmp);
            }
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * M�todo que guetea todos los descuentosequipos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @return......Vector de objetos VehNoRetornado
     **/     
    public Vector getTodosVehiculosNoRetornados(){
        return this.vecvnr;
    }          
}
