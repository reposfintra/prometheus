package com.tsp.operation.model.DAOS;

/**
 *
 * @author  Luigi
 */
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
//import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Luigi
 */
public class VetoDAO extends MainDAO {
 
    private Veto veto;
    
    public VetoDAO() {
        super("VetoDao.xml");
    }
    public VetoDAO(String dataBaseName) {
        super("VetoDao.xml", dataBaseName);
    }
    
    public String BuscarVeto(String documento) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_VETO";
        String llave = "";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString ( 1, documento );
            rs = ps.executeQuery();
            if(rs.next ()){
                llave = rs.getString("serie");
            }
            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return llave;
    }


 /**
  * 
  * @param dstrct
  * @param usuario
  * @param documento
  * @param base
  * @throws SQLException
  */
    public void insertarVeto (String dstrct, String usuario, String documento, String base) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_INSERTAR_VETO_NIT_CREACION";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString ( 1, documento );
            ps.setString ( 2, usuario );
            ps.setString ( 3, usuario );
            ps.setString ( 4, dstrct );
            ps.setString ( 5, base );
       
            ps.executeUpdate();
            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo buscarCausaVeto, busca la causa del veto
     * @param: cedula
     * @autor : Ing. Luis Eduardo Frieri
     * @version : 1.0
     */
    public void buscarCausaVeto(String ced, String clase) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query;
        if(clase.equals("N")){
            query = "SQL_BUSCAR_VETO_NIT";
        }else{
            query = "SQL_BUSCAR_VETO_PLACA";
        }
        
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString ( 1, ced );
            ps.setString ( 2, ced );
            rs = ps.executeQuery();
            veto = null;
            
            if (rs.next()){ 
               veto = new Veto();
               veto.setCausa(rs.getString(1));
               veto.setObservacion(rs.getString(2));
               veto.setReferencia(rs.getString(3));
               veto.setDescripcion(rs.getString(4));
               veto.setFuente(rs.getString(5));
            }
            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
             if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /**
     * Metodo obtVeto, retorna el objeto de Veto
     * @autor : Ing. Luis Eduardo Frieri
     * @version : 1.0
     */
    public Veto obtVeto(){
        return veto;
    }
    
    /**
     * Metodo ModificarVeto, Modifica el ultimo registro de un documento en la
     * tabla veto_historia
     * @autor : Ing. Luis Eduardo Frieri
     * @version : 1.0
     */
    
    public void ModificarVeto(String doc, String usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR_VETO";
        String serie = this.BuscarVeto(doc);
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString ( 1, usuario );
            ps.setString ( 2, serie );
            ps.executeUpdate();
            
            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
             if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


 /**
  * 
  * @param dstrct
  * @param usuario
  * @param documento
  * @param base
  * @param causa
  * @param obs
  * @param evento
  * @param fuente
  * @param tipo
  * @throws SQLException
  */
    public void insertarVetoNuevo (String dstrct, String usuario, String documento, String base, String causa, String obs, String evento, String fuente, String tipo) throws SQLException{
        Connection con = null;
        PreparedStatement ps = null;
        String query = "SQL_INSERTAR_VETO";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString ( 1, documento );
            ps.setString ( 2, usuario );
            ps.setString ( 3, usuario );
            ps.setString ( 4, dstrct );
            ps.setString ( 5, base );
            ps.setString ( 6, causa );
            ps.setString ( 7, obs );
            ps.setString ( 8, evento );
            ps.setString ( 9, fuente );
            ps.setString ( 10, tipo );
       
            ps.executeUpdate();
            
        }}catch(SQLException ex){
            ex.printStackTrace();
        }finally{//JJCastro fase2
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
}
