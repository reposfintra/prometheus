/********************************************************************
* Nombre ......................DescuentoEquipoDAO.java              *
* Descripci�n..................Clase DAO para descuento de equipos  *
* Autor........................Armando Oviedo                       *
* Fecha Creaci�n...............06/12/2005                           *
* Modificado por...............LREALES                              *
* Fecha Modificaci�n...........22/05/2006                           *
* Versi�n......................1.0                                  *
* Coyright.....................Transportes Sanchez Polo S.A.        *
********************************************************************/

package com.tsp.operation.model.DAOS;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;

public class DescuentoEquipoDAO extends MainDAO {
    
    private DescuentoEquipo de;
    private Vector desc; 
    
    /** Creates a new instance of DescuentoEquipoDAO */
    public DescuentoEquipoDAO() {
        
        super ( "DescuentoEquipoDAO.xml" );
        
    }
    
    /**
     * M�todo que setea un objeto DescuentoEquipo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......DescuentoEquipo de
     **/     
    public void setDE( DescuentoEquipo de ){
        
        this.de = de;
        
    }
    
    /**
     * M�todo que retorna un objeto DescuentoEquipo
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @return.......DescuentoEquipo de
     **/     
    public DescuentoEquipo getDescuentoEquipo(){
        
        return de;
        
    }
    
    /**
     * Metodo:          BuscarDescuentoEquipo
     * Descripcion :    M�todo que busca y setea un objeto DescuentoEquipo
     * @autor :         Armando Oviedo
     * @modificado por: LREALES
     * @param:          objeto DescuentoEquipo cargado con el set
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void BuscarDescuentoEquipo() throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        DescuentoEquipo tmp = null;
        
        try{      
            
            st = crearPreparedStatement( "SQL_BuscarDescuentoEquipo" );
            
            st.setString( 1, de.getCodigo() );
            
            rs = st.executeQuery();
            ////System.out.println("***** SQL_BuscarDescuentoEquipo: "+st);
            while ( rs.next() ){
                
                tmp = new DescuentoEquipo();                
                tmp = tmp.loadResultSet( rs ); 
                
            }       
            
            setDE( tmp );   
            
        } catch( SQLException e ){ 
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'BuscarDescuentoEquipo' - [DescuentoEquipoDAO].. " + e.getMessage()+ " " + e.getErrorCode() );
        }
        
        finally{
            
            if( rs != null ){
                try{
                    rs.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() ); 
                }
            }
            
            if( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() ); 
                }
            }         
            
            this.desconectar( "SQL_BuscarDescuentoEquipo" );
            
        }
        
    }
    
    /**
     * Metodo:          existeDE
     * Descripcion :    M�todo que retorna un boolean si existe el DescuentoEquipo o no
     * @autor :         Armando Oviedo
     * @modificado por: LREALES
     * @param:          c�digo del descuento cargado con el set
     * @return:         boolean
     * @throws:         SQLException
     * @version :       1.0
     */    
    public boolean existeDE() throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{     
            
            st = crearPreparedStatement( "SQL_existeDE" );
            
            st.setString( 1, de.getCodigo() );
            
            rs = st.executeQuery();             
            ////System.out.println("***** SQL_existeDE: "+st);
            if( rs.next() ){ 
                return true;
            }
            
        } catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existeDE' - [DescuentoEquipoDAO].. " + e.getMessage()+ " " + e.getErrorCode() );
        }
        
        finally{
            
            if( rs != null ){
                try{
                    rs.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() ); 
                }
            }
            
            if( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() ); 
                }
            }
            
            this.desconectar( "SQL_existeDE" );
            
        }
        
        return false;
        
    }
      
     /**
     * Metodo:          existeDEAnulado
     * Descripcion :    M�todo que retorna un boolean si existe el DescuentoEquipo o no, anulado
     * @autor :         Armando Oviedo
     * @modificado por: LREALES
     * @param:          c�digo del descuento cargado con el set
     * @return:         boolean
     * @throws:         SQLException
     * @version :       1.0
     */ 
    public boolean existeDEAnulado() throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{   
            
            st = crearPreparedStatement( "SQL_existeDEAnulado" );
            
            st.setString( 1, de.getCodigo() );
            
            rs = st.executeQuery();             
            ////System.out.println("***** SQL_existeDEAnulado: "+st);
            if( rs.next() ){ 
                return true;
            }
            
        } catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existeDEAnulado' - [DescuentoEquipoDAO].. " + e.getMessage()+ " " + e.getErrorCode() );
        }
        
        finally{
            
            if( rs != null ){
                try{
                    rs.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
                }
            }
            
            if( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() ); 
                }
            }
            
            this.desconectar( "SQL_existeDEAnulado" );
            
        }
        
        return false; 
        
    }
     
    /**
     * Metodo:          addDEUPD
     * Descripcion :    M�todo que actualiza el reg_status de un DescuentoEquipo anulado
     * @autor :         Armando Oviedo
     * @modificado por: LREALES
     * @param:          descripcion, concepto contable, concepto especial, usuario que creo,
     *                  usuario que actualizo, fecha modificacion, base, distrito, y codigo 
     *                  del descuento de equipo, cargado con el set.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */ 
    public void addDEUPD() throws SQLException{
        
        PreparedStatement st = null;
        
        try{            
            
            st = this.crearPreparedStatement( "SQL_addDEUPD" );
            
            st.setString( 1, de.getDescripcion() );
            st.setString( 2, de.getConcContable() );
            st.setString( 3, de.getConcEspecial() );
            st.setString( 4, de.getCreationUser() );
            st.setString( 5, de.getLastUpdate() );
            st.setString( 6, de.getUserUpdate() );
            st.setString( 7, de.getBase() );
            st.setString( 8, de.getDstrct() );
            st.setString( 9, de.getCodigo() );                                                                        
            
            st.executeUpdate();
            ////System.out.println("***** SQL_addDEUPD: "+st);
        } catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'addDEUPD' - [DescuentoEquipoDAO].. " + e.getMessage()+ " " + e.getErrorCode() );
        }
        
        finally{
            
            if( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
                }
            }
            
            this.desconectar( "SQL_addDEUPD" );
            
        }
        
    }
    
    /**
     * Metodo:          addDE
     * Descripcion :    M�todo que agrega un DescuentoEquipo
     * @autor :         Armando Oviedo
     * @modificado por: LREALES
     * @param:          descripcion, concepto contable, concepto especial, usuario que creo,
     *                  usuario que actualizo, fecha creacion, fecha modificacion, base, distrito,
     *                  estado del registro y codigo del descuento de equipo, cargado con el set.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */ 
    public void addDE() throws SQLException{
        
        PreparedStatement st = null;
        
        try{            
            
            st = this.crearPreparedStatement( "SQL_addDE" );
            
            st.setString( 1, de.getRegStatus() );
            st.setString( 2, de.getCodigo() );
            st.setString( 3, de.getConcContable() );
            st.setString( 4, de.getConcEspecial() );
            st.setString( 5, de.getDescripcion() );
            st.setString( 6, de.getDstrct() );
            st.setString( 7, de.getCreationUser() );
            st.setString( 8, de.getCreationDate() );
            st.setString( 9, de.getUserUpdate() );
            st.setString( 10, de.getLastUpdate() );
            st.setString( 11, de.getBase() );  
            
            st.executeUpdate();
            ////System.out.println("***** SQL_addDE: "+st);
        } catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'addDE' - [DescuentoEquipoDAO].. " + e.getMessage()+ " " + e.getErrorCode() );
        }
        
        finally{
            
            if( st != null ){                
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
                }
            }
            
            this.desconectar( "SQL_addDE" );
            
        } 
        
    }
    
    /**
     * Metodo:          updateDE
     * Descripcion :    M�todo que modifica un DescuentoEquipo
     * @autor :         Armando Oviedo
     * @modificado por: LREALES
     * @param:          descripcion, concepto contable, concepto especial, usuario que modifico,
     *                  y codigo del descuento de equipo, cargado con el set.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */ 
    public void updateDE() throws SQLException{
        
        PreparedStatement st = null;
        
        try{
            
            st = this.crearPreparedStatement( "SQL_updateDE" );
            
            st.setString( 1, de.getUserUpdate() );
            st.setString( 2, de.getCodigo() );
            st.setString( 3, de.getDescripcion() );
            st.setString( 4, de.getConcContable() );
            st.setString( 5, de.getConcEspecial() );
            st.setString( 6, de.getCodigo() );
            
            st.executeUpdate();         
            ////System.out.println("***** SQL_updateDE: "+st);
        } catch( SQLException e ){       
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'updateDE' - [DescuentoEquipoDAO].. " + e.getMessage()+ " " + e.getErrorCode() );
        }
        
        finally{
            
            if( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
                }
            }
            
            this.desconectar( "SQL_updateDE" );
            
        }    
        
    }
    
    /**
     * Metodo:          deleteDE
     * Descripcion :    M�todo que anula un DescuentoEquipo
     * @autor :         Armando Oviedo
     * @modificado por: LREALES
     * @param:          codigo del descuento de equipo, cargado con el set.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */ 
    public void deleteDE() throws SQLException{
        
        PreparedStatement st = null;
        
        try{
            
            st = this.crearPreparedStatement( "SQL_deleteDE" );
            
            st.setString( 1, de.getCodigo() );
            
            st.executeUpdate();   
            ////System.out.println("***** SQL_deleteDE: "+st);
        } catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'deleteDE' - [DescuentoEquipoDAO].. " + e.getMessage()+ " " + e.getErrorCode() );
        }
        
        finally{
            
            if( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
                }
            }
            
            this.desconectar( "SQL_deleteDE" );
            
        }
        
    }
    
    /**
     * Metodo:          BuscarTodosDescuentos
     * Descripcion :    M�todo que setea un vector que contiene todos los objetos de la tabla
     * @autor :         Armando Oviedo
     * @modificado por: LREALES
     * @param:          codigo del descuento de equipo, cargado con el set.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void BuscarTodosDescuentos() throws SQLException{  
        
        PreparedStatement st = null;
        ResultSet rs = null;
        desc = new Vector();
        
        try{
            
            st = this.crearPreparedStatement( "SQL_BuscarTodosDescuentos" );
            
            rs = st.executeQuery(); 
            ////System.out.println("***** SQL_BuscarTodosDescuentos: "+st);
            while( rs.next() ){
                
                DescuentoEquipo tmp = new DescuentoEquipo();
                
                tmp.setCodigo( rs.getString(1) );
                tmp.setDescripcion( rs.getString(2) );
                tmp.setConcContable( rs.getString(3) );
                tmp.setConcEspecial( rs.getString(4) );
                tmp.setLastUpdate( rs.getString(5) );
                
                desc.add( tmp );
                
            }
            
        } catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'BuscarTodosDescuentos' - [DescuentoEquipoDAO].. " + e.getMessage()+ " " + e.getErrorCode() );
        }
        
        finally{
            
            if( rs != null ){
                try{
                    rs.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
                }
            }
            
            if( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO " + e.getMessage() );
                }
            }
            
            this.desconectar( "SQL_BuscarTodosDescuentos" );
            
        }
        
    }
    
    /**
     * M�todo que guetea todos los descuentosequipos
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.     
     * @return......Vector de objetos DescuentoEquipo
     **/     
    public Vector getTodosDescuentos(){
        
        return this.desc;
        
    }        
    
}