/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.DAOS;

import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;



/**
 *
 * @author Alvaro
 */
public class ProntoPagoDAO extends MainDAO {


    /** Creates a new instance of ProntoPagoDAO */
    public ProntoPagoDAO() {
        super("ProntoPagoDAO.xml");
    }
    public ProntoPagoDAO(String dataBaseName) {
        super("ProntoPagoDAO.xml", dataBaseName);
    }



    private List listaProntoPago;
    private List listaProntoPagoDetalle;




    /**
     * Crea una lista de ordenes de servicio para una secuencia de anticipos pagos terceros
     */


    public void buscaProntoPago()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_ANTICIPO";
        listaProntoPago = null;
        try {
            con   = this.conectarJNDI( query );
        if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();
            listaProntoPago =  new LinkedList();

            while (rs.next()){
                listaProntoPago.add(ProntoPago.load(rs));
            }

        }}catch(Exception e){
            System.out.println("buscaProntoPago"+e.getMessage());
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS ANTICIPOS PAGOS TERCEROS. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }




    public List getProntoPago(){

        return listaProntoPago;
    }





    /**
     * Crea una lista de ordenes de servicio para una secuencia de anticipos pagos terceros
     */


    public void buscaProntoPagoPrestamo()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_ANTICIPO_PRESTAMO";
        listaProntoPago = null;
        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();
            listaProntoPago =  new LinkedList();
            while (rs.next()){
                listaProntoPago.add(ProntoPago.load(rs));
            }
        }catch(Exception e){
            System.out.println("buscaProntoPagoPrestamo"+e.getMessage());
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS ANTICIPOS PRESTAMOS. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public List getProntoPagoPrestamo(){

        return listaProntoPago;
    }


/**
 *
 * @throws SQLException
 */
    public void buscaProntoPagoEfectivo()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_GET_ANTICIPO_EFECTIVO";
        listaProntoPago = null;
        try {
           con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();
            listaProntoPago =  new LinkedList();

            while (rs.next()){
                listaProntoPago.add(ProntoPago.load(rs));
            }

        }catch(Exception e){
            System.out.println("buscaProntoPagoEfectivo"+e.getMessage());
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS ANTICIPOS PAGOS TERCEROS. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public List getProntoPagoEfectivo(){

        return listaProntoPago;
    }





/**
 *
 * @param secuencia
 * @throws SQLException
 */
    public void buscaProntoPagoDetalle(String secuencia)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_GET_ANTICIPO_DETALLE";
        listaProntoPagoDetalle     = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, secuencia );
            rs = st.executeQuery();

            listaProntoPagoDetalle =  new LinkedList();

            while (rs.next()){
                listaProntoPagoDetalle.add(ProntoPagoDetalle.load(rs));
            }

        }catch(Exception e){
            System.out.println("buscaProntoPagoDetalle"+e.getMessage());
            throw new SQLException("ERROR DURANTE LA SELECCION DEL DETALLE DE ANTICIPOS PAGOS TERCEROS. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    public List getProntoPagoDetalle(){

        return listaProntoPagoDetalle;
    }










   public TotalExtracto get_TotalExtracto(String secuencia)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String            query    = "SQL_GET_TOTAL_EXTRACTO";
        TotalExtracto totalExtracto = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, secuencia );
            rs = st.executeQuery();

            if(rs.next()){
                totalExtracto = new TotalExtracto();
                totalExtracto = TotalExtracto.load(rs);
            }
        }catch(Exception e){
            System.out.println(e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TOTALES DEL EXTRACTO. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return totalExtracto;
    }


/**
 *
 * @param cuenta_transferencia
 * @param banco_transferencia
 * @param tcta_transferencia
 * @param cuentaCorriente
 * @param tipoCuenta
 * @param cuentaCorriente_tipoCuenta
 * @return
 * @throws SQLException
 */
   public Vector getBanco(String cuenta_transferencia, String banco_transferencia, String tcta_transferencia,
                          String cuentaCorriente, String tipoCuenta, String cuentaCorriente_tipoCuenta)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_BANCO";
        Vector            banco    = null;

        try {
            banco = new Vector();
            con   = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, banco_transferencia );
            st.setString( 2, cuentaCorriente );
            st.setString( 3, tipoCuenta );
            st.setString( 4, cuentaCorriente_tipoCuenta );
            st.setString( 5, cuentaCorriente_tipoCuenta );
            st.setString( 6, cuentaCorriente_tipoCuenta );
            st.setString( 7, cuenta_transferencia );

            System.out.println("Query de banco : " + st.toString() ) ;

            rs = st.executeQuery();
            if(rs.next()){
                banco.add(rs.getString("branch_code"));
                banco.add(rs.getString("bank_account_no"));
                banco.add(rs.getString("codigo_cuenta"));
                banco.add(rs.getString("currency"));
            }
        }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DE CUENTA DEL BANCO POR DONDE SE TRANSFIERE. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return banco;
    }




   public Vector getBancoAnticipo(String agencia)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_BANCO_ANTICIPO";
        Vector            banco    = null;

        try {
            System.out.println("agencia"+agencia);
            banco = new Vector();
            con   = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, agencia );
            System.out.println("agencia"+agencia);

            rs = st.executeQuery();
            if(rs.next()){
                String descripcion_tablagen  = rs.getString("descripcion");

                String elementos[]= null;
                elementos = descripcion_tablagen.split(",");

                banco.add(elementos[0]);
                banco.add(elementos[1]);
                banco.add(elementos[2]);
                banco.add(elementos[3]);
            }
        }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DE CUENTA DEL BANCO POR DONDE SE TRANSFIEREN LOS EFECTIVOS. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return banco;
    }



/**
 *
 * @param dstrct
 * @param tipodoc
 * @param codigoCmc
 * @return
 * @throws SQLException
 */
   public Cmc getCmc(String dstrct, String tipodoc, String codigoCmc)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String            query    = "SQL_GET_CMC";
        Cmc               cmc      = null;

        try {
            con   = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, dstrct );
            st.setString( 2, tipodoc );
            st.setString( 3, codigoCmc );
            rs = st.executeQuery();

            if(rs.next()){
                cmc = new Cmc();
                cmc = Cmc.load(rs);
            }
        }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO CMC. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return cmc;
    }


/**
 *
 * @param prontoPago
 * @param dstrct
 * @param user
 * @param creation_date
 * @return
 * @throws SQLException
 */
    public String setOrdenServicio(ProntoPago prontoPago,String dstrct, String user,String creation_date)throws SQLException{

        StringStatement st       = null;
        String comando_sql  = "";
        String            query    = "SQL_INSERT_OS";

        try {
             st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,dstrct);
            st.setString(2,prontoPago.getTipo_operacion());
            st.setString(3,prontoPago.getNumero_operacion());
            st.setString(4,prontoPago.getNit());
            st.setString(5,prontoPago.getCmc());
            st.setString(6,prontoPago.getNumero_transferencia() );
            st.setString(7,prontoPago.getNombre_propietario() );
            st.setString(8,prontoPago.getCuenta_transferencia() );
            st.setString(9,prontoPago.getBanco() );  // banco
            st.setString(10,prontoPago.getSucursal() );
            st.setDouble(11,prontoPago.getVlr_extracto() );
            st.setDouble(12,prontoPago.getPorcentaje() );
            st.setDouble(13,prontoPago.getVlr_descuento() );
            st.setDouble(14,prontoPago.getVlr_neto() );
            st.setDouble(15,prontoPago.getVlr_combancaria() );
            st.setDouble(16,prontoPago.getVlr_consignacion() );
            st.setDouble(17,prontoPago.getVlr_consignacion_calculada() );
            st.setDouble(18,prontoPago.getVlr_ajuste() );
            st.setDouble(19,prontoPago.getVlr_ext_detalle_calculado() );
            st.setDouble(20,prontoPago.getVlr_ext_detalle_registrado() );
            st.setDouble(21,prontoPago.getVlr_ext_detalle_diferencia() );
            st.setDouble(22,prontoPago.getVlr_diferencia_ext_ant() );
            st.setString(23,user );
            st.setString(24,user );
            st.setInt(25,prontoPago.getGrupo_transaccion() ); // grupo transaccion os
            st.setString(26,prontoPago.getPeriodo_os());
            st.setString(27,creation_date );   // fecha_contabilizacion_os
            st.setString(28,user );
            st.setString(29,prontoPago.getCuenta_contable_banco());
            st.setString(30,prontoPago.getCuenta_os());
            st.setString(31,prontoPago.getDbcr_os());
            st.setString(32,prontoPago.getSigla_comprobante_os());
            st.setString(33,creation_date ); // last_update
            st.setString(34,creation_date ); // creation_date


            String fechaTransferencia = prontoPago.getFecha_transferencia();
            if(fechaTransferencia.equalsIgnoreCase("0099-01-01 00:00:00")){
                st.setString(35,prontoPago.getCreation_date() );
            }
            else{
                st.setString(35,prontoPago.getFecha_transferencia() );
            }


            st.setString(35,prontoPago.getFecha_transferencia() );
            st.setString(36,prontoPago.getFactura_cxp() );
           // st.setString(37,prontoPago.getCreation_date() ); // creation_date
            comando_sql = st.getSql();//JJCastro fase2

            }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE EL REGISTRO DE UNA ORDEN DE SERVICIO. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st =null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
           }
        return comando_sql;
    }


/**
 *
 * @return
 * @throws SQLException
 */
       public int getGrupoTransaccion()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String            query    = "SQL_GRUPO_TRANSACCION";

        int      grupo_transaccion = 0;
        try {
            con   = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            if(rs.next()){
                grupo_transaccion = rs.getInt("grupo_transaccion");
            }
        }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SECUENCIA DEL GRUPO_TRANSACCION. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return grupo_transaccion;
    }



/**
 *
 * @return
 * @throws SQLException
 */
       public int getTransaccion()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_TRANSACCION";

        int      transaccion = 0;
        try {
            con   = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            if(rs.next()){
                transaccion = rs.getInt("transaccion");
            }
        }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SECUENCIA DE TANSACCION. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return transaccion;
    }




/**
 *
 * @param prontoPago
 * @param dstrct
 * @param creation_date
 * @return
 * @throws SQLException
 */
    public String setAnticipo(ProntoPago prontoPago,String dstrct, String creation_date)throws SQLException{

        StringStatement st = null;
        String comando_sql = "";
        String query = "SQL_UPDATE_ANTICIPOS";
        String query1 = "SQL_UPDATE_ANTICIPOS_EFECTIVO";

        try {
            if (prontoPago.getTipo_operacion().equalsIgnoreCase("EXT")) {
                st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
                st.setString(1, prontoPago.getTipo_operacion());
                st.setString(2, prontoPago.getNumero_operacion());
                st.setString(3, creation_date);
                st.setString(4, dstrct);
                st.setString(5, prontoPago.getNumero_operacion());
            } else {
                st = new StringStatement(this.obtenerSQL(query1), true);//JJCastro fase2
                st.setString(1, prontoPago.getTipo_operacion());
                st.setString(2, prontoPago.getNumero_operacion());
                st.setString(3, creation_date);
                st.setInt(4, prontoPago.getId());
            }

            comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE ANTICIPO PAGOS TERCEROS CON EL NUMERO DE OPERACION. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return comando_sql;
    }

/**
 *
 * @param prontoPago
 * @param prontoPagoDetalle
 * @param dstrct
 * @param user
 * @param creation_date
 * @return
 * @throws SQLException
 */
    public String setOrdenServicioDetalle(ProntoPago prontoPago,ProntoPagoDetalle prontoPagoDetalle,
                                          String dstrct, String user,String creation_date)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql  = "";
        String            query    = "SQL_INSERT_OS_DETALLE";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,dstrct);
            st.setString(2,prontoPago.getTipo_operacion());
            st.setString(3,prontoPago.getNumero_operacion());
            st.setString(4,prontoPagoDetalle.getTipo_documento());
            st.setString(5,prontoPagoDetalle.getDocumento());
            st.setString(6,prontoPago.getNit());
            st.setString(7,prontoPagoDetalle.getClase());
            st.setDouble(8,prontoPagoDetalle.getValor_calculado());
            st.setString(9,prontoPagoDetalle.getPlaca());
            st.setString(10,creation_date);
            st.setString(11,user);
            st.setString(12,creation_date);
            st.setString(13,user);
           // st.setString(14,prontoPago.getCreation_date());

            comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE EL REGISTRO DE UNA ORDEN DE SERVICIO DETALLADA. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return comando_sql;
    }

/**
 *
 * @param comprobante
 * @return
 * @throws SQLException
 */
    public String setComprobante(Comprobante comprobante)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql  = "";
        String            query    = "SQL_INSERT_COMPROBANTE";

        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2;
            st.setString(1,comprobante.getReg_status());
            st.setString(2,comprobante.getDstrct());
            st.setString(3,comprobante.getTipodoc());
            st.setString(4,comprobante.getNumdoc());
            st.setInt(5,comprobante.getGrupo_transaccion());
            st.setString(6,comprobante.getSucursal());
            st.setString(7,comprobante.getPeriodo());
            st.setString(8,comprobante.getFechadoc());
            st.setString(9,comprobante.getDetalle());
            st.setString(10,comprobante.getTercero());
            st.setDouble(11,comprobante.getTotal_debito());
            st.setDouble(12,comprobante.getTotal_credito());
            st.setInt(13,comprobante.getTotal_items());
            st.setString(14,comprobante.getMoneda());
            st.setString(15,comprobante.getFecha_aplicacion());
            st.setString(16,comprobante.getAprobador());
            st.setString(17,comprobante.getLast_update());
            st.setString(18,comprobante.getUser_update());
            st.setString(19,comprobante.getCreation_date());
            st.setString(20,comprobante.getCreation_user());
            st.setString(21,comprobante.getBase());
            st.setString(22,comprobante.getUsuario_aplicacion());
            st.setString(23,comprobante.getTipo_operacion());
            st.setString(24,comprobante.getMoneda_foranea());
            st.setDouble(25,comprobante.getValor_for());
            st.setString(26,comprobante.getRef_1());
            st.setString(27,comprobante.getRef_2());

            comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE EL REGISTRO DE LA CABECERA DEL COMPROBANTE. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return comando_sql;
    }


/**
 *
 * @param comprobanteDetalle
 * @return
 * @throws SQLException
 */
    public String setComprobanteDetalle(ComprobanteDetalle comprobanteDetalle)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql  = "";
        String            query    = "SQL_INSERT_COMPROBANTE_DETALLE";

        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2;

            st.setString(1,comprobanteDetalle.getReg_status());
            st.setString(2,comprobanteDetalle.getDstrct());
            st.setString(3,comprobanteDetalle.getTipodoc());
            st.setString(4,comprobanteDetalle.getNumdoc());
            st.setInt(5,comprobanteDetalle.getGrupo_transaccion());
            st.setInt(6,comprobanteDetalle.getTransaccion());
            st.setString(7,comprobanteDetalle.getPeriodo());
            st.setString(8,comprobanteDetalle.getCuenta());
            st.setString(9,comprobanteDetalle.getAuxiliar());
            st.setString(10,comprobanteDetalle.getDetalle());
            st.setDouble(11,comprobanteDetalle.getValor_debito());
            st.setDouble(12,comprobanteDetalle.getValor_credito());
            st.setString(13,comprobanteDetalle.getTercero());
            st.setString(14,comprobanteDetalle.getDocumento_interno());
            st.setString(15,comprobanteDetalle.getLast_update());
            st.setString(16,comprobanteDetalle.getUser_update());
            st.setString(17,comprobanteDetalle.getCreation_date());
            st.setString(18,comprobanteDetalle.getCreation_user());
            st.setString(19,comprobanteDetalle.getBase());
            st.setString(20,comprobanteDetalle.getTipodoc_rel());
            st.setString(21,comprobanteDetalle.getDocumento_rel());
            st.setString(22,comprobanteDetalle.getAbc());
            st.setDouble(23,comprobanteDetalle.getVlr_for());

            comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE EL REGISTRO DEL DETALLE DEL COMPROBANTE. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return comando_sql;
    }




/**
 *
 * @param egreso
 * @return
 * @throws SQLException
 */
    public String setEgreso(EgresoCabecera egreso)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql  = "";
        String            query    = "SQL_INSERT_EGRESO";

        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2;
            st.setString(1,egreso.getReg_status() ) ;         // reg_status
            st.setString(2,egreso.getDstrct() ) ;             // dstrct
            st.setString(3,egreso.getBranch_code() ) ;        // branch_code
            st.setString(4,egreso.getBank_account_no() ) ;    // bank_account_no
            st.setString(5,egreso.getDocument_no() ) ;        // document_no
            st.setString(6,egreso.getNit() ) ;                // nit
            st.setString(7,egreso.getPayment_name() ) ;       // payment_name
            st.setString(8,egreso.getAgency_id() ) ;          // agency_id
            st.setString(9,egreso.getPmt_date() ) ;           // pmt_date date
            st.setString(10,egreso.getPrinter_date() ) ;      // printer_date date
            st.setString(11,egreso.getConcept_code() ) ;      // concept_code
            st.setDouble(12,egreso.getVlr() ) ;               // vlr
            st.setDouble(13,egreso.getVlr_for() ) ;           // vlr_for
            st.setString(14,egreso.getCurrency() ) ;          // currency
            st.setString(15,egreso.getLast_update() ) ;       // last_update
            st.setString(16,egreso.getUser_update() ) ;       // user_update
            st.setString(17,egreso.getCreation_date() ) ;     // creation_date
            st.setString(18,egreso.getCreation_user() ) ;     // creation_user
            st.setString(19,egreso.getBase() ) ;              // base
            st.setString(20,egreso.getTipo_documento() ) ;    // tipo_documento
            st.setDouble(21,egreso.getTasa() ) ;              // tasa
            st.setString(22,egreso.getFecha_cheque() ) ;      // fecha_cheque
            st.setString(23,egreso.getUsuario_impresion() ) ;          // usuario_impresion
            st.setString(24,egreso.getUsuario_contabilizacion() ) ;    // usuario_contabilizacion
            st.setString(25,egreso.getFecha_contabilizacion() ) ;      // fecha_contabilizacion
            st.setString(26,egreso.getFecha_entrega() ) ;              // fecha_entrega
            st.setString(27,egreso.getUsuario_envio() ) ;              // usuario_envio
            st.setString(28,egreso.getFecha_envio() ) ;                // fecha_envio
            st.setString(29,egreso.getUsuario_recibido() ) ;           // usuario_recibido
            st.setString(30,egreso.getFecha_recibido() ) ;             // fecha_recibido
            st.setString(31,egreso.getUsuario_entrega() ) ;            // usuario_entrega
            st.setString(32,egreso.getFecha_registro_entrega() ) ;     // fecha_registro_entrega
            st.setString(33,egreso.getFecha_registro_envio() ) ;       // fecha_registro_envio
            st.setString(34,egreso.getFecha_registro_recibido() ) ;    // fecha_registro_recibido
            st.setInt   (35,egreso.getTransaccion() ) ;                // transaccion
            st.setString(36,egreso.getNit_beneficiario() ) ;           // nit_beneficiario
            st.setString(37,egreso.getFecha_reporte() ) ;              // fecha_reporte
            st.setString(38,egreso.getNit_proveedor() ) ;              // nit_proveedor
            st.setString(39,egreso.getUsuario_generacion() ) ;         // usuario_generacion
            st.setString(40,egreso.getPeriodo() ) ;                    // periodo
            st.setString(41,egreso.getReimpresion() ) ;                // reimpresion
            st.setString(42,egreso.getContabilizable() ) ;             // contabilizable

            comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA INSERCION DE UN REGISTRO EN EGRESO. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return comando_sql;
    }




/**
 *
 * @param egresoDetalle
 * @return
 * @throws SQLException
 */

    public String setEgresoDetalle(EgresoDetalle egresoDetalle)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql  = "";
        String            query    = "SQL_INSERT_EGRESO_DETALLE";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2;;
            st.setString(1,egresoDetalle.getReg_status() ) ;
            st.setString(2,egresoDetalle.getDstrct() );
            st.setString(3,egresoDetalle.getBranch_code() );
            st.setString(4,egresoDetalle.getBank_account_no() );
            st.setString(5,egresoDetalle.getDocument_no() );
            st.setString(6,egresoDetalle.getItem_no() );
            st.setString(7,egresoDetalle.getConcept_code() );
            st.setDouble(8,egresoDetalle.getVlr() );
            st.setDouble(9,egresoDetalle.getVlr_for() );
            st.setString(10,egresoDetalle.getCurrency() );
            st.setString(11,egresoDetalle.getOc() );
            st.setString(12,egresoDetalle.getLast_update() );
            st.setString(13,egresoDetalle.getUser_update() );
            st.setString(14,egresoDetalle.getCreation_date() );
            st.setString(15,egresoDetalle.getCreation_user() );
            st.setString(16,egresoDetalle.getDescription() );
            st.setString(17,egresoDetalle.getBase() );
            st.setDouble(18,egresoDetalle.getTasa() );
            st.setInt   (19,egresoDetalle.getTransaccion() );
            st.setString(20,egresoDetalle.getTipo_documento() );
            st.setString(21,egresoDetalle.getDocumento() );
            st.setString(22,egresoDetalle.getTipo_pago() );
            st.setString(23,egresoDetalle.getCuenta() );
            st.setString(24,egresoDetalle.getAuxiliar() );

            comando_sql = st.getSql();//JJCastro fase2

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA INSERCION DE UN REGISTRO EN EGRESO DETALLE. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return comando_sql;
    }





/**
 *
 * @return
 * @throws SQLException
 */
    public List getOrdenServicio( )throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_OS";
        List listaOS               = null;

        try {
            con          = this.conectarJNDI( query );
           if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaOS      =  new LinkedList();

            while (rs.next()){
                listaOS.add(OrdenServicio.load(rs));
            }

           }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OS PARA ELABORAR EGRESOS. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return listaOS;
    }



    public List getOrdenServicioDetalle(String distrito, String tipo_operacion, String numero_operacion )throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_OS_DETALLE";
        List     listaOSDetalle    = null;

        try {
            con          = this.conectarJNDI( query );
           if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, distrito) ;          //   reg_status
            st.setString(2, tipo_operacion ) ;   //   dstrct
            st.setString(3, numero_operacion) ;  //   branch_code

            rs = st.executeQuery();

            listaOSDetalle      =  new LinkedList();

            while (rs.next()){
                listaOSDetalle.add(OrdenServicioDetalle.load(rs));
            }

        }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE DETALLE DE OS PARA ELABORAR EGRESOS. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return listaOSDetalle;
    }





    public String setFechaEgreso(String distrito, String tipo_operacion, String numero_operacion,
                                 String egreso, String fecha_egreso)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql  = "";
        String            query    = "SQL_SET_FECHA_EGRESO";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,egreso ) ;
            st.setString(2,fecha_egreso ) ;
            st.setString(3,distrito ) ;
            st.setString(4,tipo_operacion ) ;
            st.setString(5,numero_operacion ) ;
            comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE FECHA EGRESO EN ORDEN DE SERVICIO. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return comando_sql;
    }




    public String setSaldos(double vlr_cheque, String cheque, String distrito, String proveedor,
                            String tipo_documento, String documento) throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql  = "";
        String            query    = "SQL_SET_SALDOS";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setDouble(1,vlr_cheque ) ;
            st.setDouble(2,vlr_cheque ) ;
            st.setDouble(3,vlr_cheque ) ;
            st.setDouble(4,vlr_cheque ) ;
            st.setString(5,cheque ) ;
            st.setString(6,distrito ) ;
            st.setString(7,proveedor) ;
            st.setString(8,tipo_documento ) ;
            st.setString(9,documento ) ;

            comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE SALDOS EN LA FACTURA. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return comando_sql;
    }


/**
 *
 * @param distrito
 * @param proveedor
 * @param tipo_documento
 * @param secuencia
 * @return
 * @throws SQLException
 */
    public String buscarDocumento(String  distrito,String  proveedor,String  tipo_documento, String  secuencia) throws SQLException {

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String      query     = "SQL_SECUENCIA_FACTURA";

        String      documento = "";
        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            st.setString(1,distrito ) ;
            st.setString(2,proveedor ) ;
            st.setString(3,tipo_documento ) ;
            st.setString(4,secuencia + "_1" ) ;  // 2010-06-30
            st.setString(5,secuencia + "_99" );  // 2010-06-30
            rs = st.executeQuery();

            if(rs.next()){
                documento = rs.getString("documento");
            }
        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SECUENCIA DEL DOCUMENTO DE FACTURA AL PROVEEDOR. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return documento;
    }




/**
 *
 * @param distrito
 * @param proveedor
 * @param tipo_documento
 * @param secuencia
 * @return
 * @throws SQLException
 */
    public String buscarDocumentoUnico(String  distrito,String  proveedor,String  tipo_documento, String  secuencia) throws SQLException {

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String      query     = "SQL_SECUENCIA_FACTURA_UNICA";

        String      documento = "";
        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            st.setString(1,distrito ) ;
            st.setString(2,proveedor ) ;
            st.setString(3,tipo_documento ) ;
            st.setString(4,secuencia ) ;  // 2010-06-30

            rs = st.executeQuery();

            if(rs.next()){
                documento = rs.getString("documento");
            }
        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SECUENCIA DEL DOCUMENTO DE FACTURA AL PROVEEDOR. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return documento;
    }







/**
 *
 * @param distrito
 * @param tipo_operacion
 * @param numero_operacion
 * @return
 * @throws SQLException
 */
    public String buscarDocumentoOS(String  distrito, String  tipo_operacion, String  numero_operacion) throws SQLException {

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String      query     = "SQL_SECUENCIA_OS";
        String      documento = "";
        System.out.println("distrito= "+distrito);
        System.out.println("tipo_operacion= "+tipo_operacion);
        System.out.println("numero_operacion= "+numero_operacion);
        try {
            con   = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,distrito ) ;
            st.setString(2,tipo_operacion ) ;
            st.setString(3,numero_operacion + "%" ) ;
            System.out.println("sql= "+sql);
            rs = st.executeQuery();

            if(rs.next()){
                documento = rs.getString("numero_operacion");
            }
            }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SECUENCIA DEL DOCUMENTO DE ORDEN DE SERVICIO. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return documento;
    }

/**
 *
 * @param distrito
 * @param banco
 * @param sucursal
 * @param secuencia
 * @return
 * @throws SQLException
 */

 public String buscarDocumentoEgreso(String  distrito,String  banco,String  sucursal, String  secuencia) throws SQLException {

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;
        String      query     = "SQL_SECUENCIA_EGRESO";
        String      documento = "";
         System.out.println("____");
         System.out.println("distrito= "+distrito);
         System.out.println("banco= "+banco);
         System.out.println("sucursal= "+sucursal);
         System.out.println("secuencia= "+secuencia);
         System.out.println("____");
        try {
            con   = this.conectarJNDI( query );
            if(con!=null){
                        String  sql  = obtenerSQL( query );
                        st           = con.prepareStatement( sql );

                        st.setString(1,secuencia);
                        st.setString(2,secuencia);
                        st.setString(3,secuencia);
                        st.setString(4,secuencia);
                        st.setString(5,distrito ) ;
                        st.setString(6,banco ) ;
                        st.setString(7,sucursal ) ;
                        st.setString(8,secuencia) ;
                        st.setString(9,secuencia) ;
                        rs = st.executeQuery();

                        if(rs.next()){
                            documento = rs.getString("document_no");
                        }
            }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA SECUENCIA DEL DOCUMENTO DEL EGRESO. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return documento;
    }








/**
 *
 * @param factura_cxc
 * @param distrito
 * @param tipo_operacion
 * @param numero_operacion
 * @param tipo_documento
 * @param documento
 * @return
 * @throws SQLException
 */
    public String  setFacturaCxC(String factura_cxc, String distrito, String tipo_operacion, String numero_operacion, String tipo_documento, String documento )throws SQLException{

        StringStatement st       = null;
        String comando_sql  = "";
        String            query    = "SET_FACTURA_CXC";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, factura_cxc) ;       //
            st.setString(2, distrito) ;          //
            st.setString(3, tipo_operacion ) ;   //
            st.setString(4, numero_operacion) ;  //
            st.setString(5, tipo_documento) ;  //
            st.setString(6, documento) ;  //
            comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL NUMERO DE FACTURA A LAS ORDENES DE SERVICIO DETALLADAS A TSP. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return comando_sql;
    }

/**
 *
 * @param dstrct
 * @param proveedor
 * @param tipo_documento
 * @param documento
 * @param descripcion
 * @param agencia
 * @param handle_code
 * @param aprobador
 * @param usuario_aprobacion
 * @param banco
 * @param sucursal
 * @param moneda
 * @param vlr_neto
 * @param vlr_total_abonos
 * @param vlr_saldo
 * @param vlr_neto_me
 * @param vlr_total_abonos_me
 * @param vlr_saldo_me
 * @param tasa
 * @param observacion
 * @param user_update
 * @param creation_user
 * @param base
 * @param clase_documento
 * @param moneda_banco
 * @param fecha_documento
 * @param fecha_vencimiento
 * @param clase_documento_rel
 * @param last_update
 * @param creation_date
 * @param fecha_aprobacion
 * @return
 * @throws SQLException
 */
    public String setCxp_doc(String dstrct,String proveedor, String tipo_documento,String documento,
                             String descripcion,String agencia,
                             String handle_code,String aprobador,String usuario_aprobacion,
                             String banco,String sucursal,String moneda, double vlr_neto,
                             double vlr_total_abonos,double vlr_saldo,double vlr_neto_me,double vlr_total_abonos_me,
                             double vlr_saldo_me, double tasa,String observacion,String user_update,
                             String creation_user,String base,String clase_documento,String moneda_banco,
                             String fecha_documento,String fecha_vencimiento,String clase_documento_rel,
                             String last_update, String creation_date, String fecha_aprobacion)throws SQLException{

        StringStatement st       = null;
        String comando_sql  = "";
        String            query    = "SQL_INSERTAR_CXP_DOC";

        try {
           st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,tipo_documento);
            st.setString(4,documento);
            st.setString(5,descripcion);
            st.setString(6,agencia);
            st.setString(7,handle_code);
            st.setString(8,aprobador);
            st.setString(9,usuario_aprobacion);
            st.setString(10,banco);
            st.setString(11,sucursal);
            st.setString(12,moneda);
            st.setDouble(13,vlr_neto);
            st.setDouble(14,vlr_total_abonos);
            st.setDouble(15,vlr_saldo);
            st.setDouble(16,vlr_neto_me);
            st.setDouble(17,vlr_total_abonos_me);
            st.setDouble(18,vlr_saldo_me);
            st.setDouble(19,tasa);
            st.setString(20,observacion);
            st.setString(21,user_update);
            st.setString(22,creation_user);
            st.setString(23,base);
            st.setString(24,clase_documento);
            st.setString(25,moneda_banco);
            st.setString(26,fecha_documento);
            st.setString(27,fecha_vencimiento);
            st.setString(28,clase_documento_rel);
            st.setString(29,last_update);
            st.setString(30,creation_date);
            st.setString(31,fecha_aprobacion);
            comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA INSERTAR EN CXP_DOC. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return comando_sql;
    }

     /**
     * Crea una lista de egresos de TSP
     */

/**
 *
 * @param distrito
 * @return
 * @throws SQLException
 */
    public List getEgresoTsp(String distrito)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs               = null;
        String            query    = "SQL_GET_EGRESO_TSP";
        List listaEgresoTsp        = null;

        try {
            con   = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();
            listaEgresoTsp =  new LinkedList();

            while (rs.next()){
                listaEgresoTsp.add(EgresoCabeceraTsp.load(rs));
            }

        }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS EGRESOS TSP. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listaEgresoTsp;
    }




/**
 *
 * @param dstrct
 * @param branch_code
 * @param bank_account_no
 * @param document_no
 * @return
 * @throws SQLException
 */
    public List getEgresoDetalleTsp(String dstrct, String branch_code,  String bank_account_no,  String document_no )throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs               = null;
        String            query    = "SQL_GET_EGRESO_DETALLE_TSP";
        List listaEgresoDetalleTsp        = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,dstrct);
            st.setString(2,branch_code);
            st.setString(3,bank_account_no);
            st.setString(4,document_no);
            rs = st.executeQuery();
            listaEgresoDetalleTsp =  new LinkedList();
             while (rs.next()){
                listaEgresoDetalleTsp.add(EgresoDetalleTsp.load(rs));
            }

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS EGRESOS DETALLADOS TSP. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listaEgresoDetalleTsp;
    }



/**
 *
 * @param dstrct
 * @param branch_code
 * @param bank_account_no
 * @param document_no
 * @return
 * @throws SQLException
 */
    public List getEgresoItem(String dstrct, String branch_code,String bank_account_no,String document_no )throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs               = null;

        String            query    = "SQL_GET_EGRESO_ITEM_TSP";
        List listaEgresoItem             = null;

        try {

            con   = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,dstrct);
            st.setString(2,dstrct);
            st.setString(3,branch_code);
            st.setString(4,bank_account_no);
            st.setString(5,document_no);

            rs = st.executeQuery();

            listaEgresoItem =  new LinkedList();

            while (rs.next()){
                listaEgresoItem.add(EgresoItem.load(rs));
            }

        }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS ITEMS DE FACTURAS DE CXP PAGADAS EN UN EGRESO. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listaEgresoItem;
    }

/**
 *
 * @param dstrct
 * @param proveedor
 * @param tipo_documento
 * @param documento
 * @return
 * @throws SQLException
 */
    public List getItem(String dstrct, String proveedor,String tipo_documento,String documento )throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs               = null;

        String            query    = "SQL_GET_CXP_ITEM_TSP";
        List listaItem        = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,tipo_documento);
            st.setString(4,documento);
            rs = st.executeQuery();

            listaItem =  new LinkedList();

            while (rs.next()){
                listaItem.add(CXPItem.load(rs));
            }

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS ITEMS DE FACTURAS DE CXP PAGADAS EN UN EGRESO. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listaItem;
    }

/**
 *
 * @throws SQLException
 */
    public void creaFacturaFintra()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        String            query    = "SQL_CREA_FACTURA_FINTRA";
        try {
            con          = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.execute();
            }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DE LAS FACTURAS PENDIENTES POR CANCELAR. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



/**
 *
 * @param tipo_factura
 * @param factura
 * @param tipo_operacion
 * @param numero_operacion
 * @param valor_item
 * @param numero_query
 * @return
 * @throws SQLException
 */
    public FacturaFintra getFacturaFintra(String tipo_factura, String factura,
                                          String tipo_operacion,
                                          String numero_operacion,
                                          double valor_item,
                                          String numero_query)throws SQLException{

        PreparedStatement st         = null;
        Connection        con        = null;
        ResultSet rs                 = null;

        String            query      = "SQL_GET_FACTURA_FINTRA_"+numero_query;
        FacturaFintra facturaFintra  = null;

        try {

            con = this.conectarJNDI(query);
            if (con != null) {
                String sql = obtenerSQL(query);
                st = con.prepareStatement(sql);

                if (numero_query.equals("1") || numero_query.equals("2")) {
                    st.setString(1, tipo_factura);
                    st.setString(2, factura);
                    st.setString(3, tipo_operacion);
                    st.setDouble(4, valor_item);
                } else if (numero_query.equals("3")) {
                    st.setString(1, tipo_factura);
                    st.setString(2, factura);
                    st.setString(3, tipo_operacion);
                    st.setString(4, numero_operacion);
                    st.setDouble(5, valor_item);
                } else {
                    st.setString(1, tipo_factura);
                    st.setString(2, factura);
                    st.setString(3, tipo_operacion);
                    st.setDouble(4, valor_item);
                }

                rs = st.executeQuery();

                if (rs.next()) {
                    facturaFintra = new FacturaFintra();
                    facturaFintra = FacturaFintra.load(rs);
                }

            }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA FACTURA A CANCELAR POR UN EGRESO DE TSP. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return facturaFintra;
    }




/**
 *
 * @return
 * @throws SQLException
 */
    public List getListaFacturaFintra()throws SQLException{

        PreparedStatement st         = null;
        Connection        con        = null;
        ResultSet rs                 = null;
        List listaFacturaFintra      = null;
        String            query      = "SQL_GET_FACTURA_FINTRA";
        try {
            con   = this.conectarJNDI( query );
            if(con!=null){
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();
            listaFacturaFintra =  new LinkedList();
             while (rs.next()){
                listaFacturaFintra.add(FacturaFintra.load(rs));
            }

            }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA FACTURA PENDIENTES POR CANCELAR. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listaFacturaFintra;
    }


/**
 *
 * @param dstrct_egreso
 * @param branch_code_egreso
 * @param bank_account_no_egreso
 * @param document_no_egreso
 * @param item_no_egreso
 * @param dstrct
 * @param tipo_documento
 * @param documento
 * @param item
 * @throws SQLException
 */
    public void actualizaFacturaFintra(String dstrct_egreso, String branch_code_egreso, String bank_account_no_egreso,
                                       String document_no_egreso, String item_no_egreso,
                                       String dstrct, String tipo_documento, String documento,
                                       int item)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String            query    = "SQL_SET_FACTURA_FINTRA";

        try {
            con          = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1,dstrct_egreso);
            st.setString(2,branch_code_egreso);
            st.setString(3,bank_account_no_egreso);
            st.setString(4,document_no_egreso);
            st.setString(5,item_no_egreso);
            st.setString(6,dstrct);
            st.setString(7,tipo_documento);
            st.setString(8,documento);
            st.setInt(9,item);

            st.execute();
        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE UNA FACTURA POR UN EGRESO ITEM. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }



/**
 *
 * @param dstrct_factura
 * @param tipo_documento_factura
 * @param documento_factura
 * @param item_factura
 * @param dstrct_ingreso
 * @param tipo_documento_ingreso
 * @param num_ingreso
 * @param item_ingreso
 * @param dstrct
 * @param branch_code
 * @param bank_account_no
 * @param document_no
 * @param item_no
 * @return
 * @throws SQLException
 */
    public String setEgresodetTsp ( String dstrct_factura, String tipo_documento_factura,
                                    String documento_factura, int item_factura,

                                    String dstrct_ingreso,
                                    String tipo_documento_ingreso,
                                    String num_ingreso,
                                    int item_ingreso,

                                    String dstrct , String branch_code,
                                    String bank_account_no, String document_no, String item_no)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql  = "";
        String            query    = "SQL_SET_EGRESODET_TSP";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,dstrct_factura);
            st.setString(2,tipo_documento_factura);
            st.setString(3,documento_factura);
            st.setInt(4,item_factura);

            st.setString(5,dstrct_ingreso);
            st.setString(6,tipo_documento_ingreso);
            st.setString(7,num_ingreso);
            st.setInt(8,item_ingreso);

            st.setString(9,dstrct);
            st.setString(10,branch_code);
            st.setString(11,bank_account_no);
            st.setString(12,document_no);
            st.setString(13,item_no);
           // st.execute();

           comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL EGRESODET TSP CON UNA FACTURA E INGRESO QUE CRUZO. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }


/**
 *
 * @param tipo_documento_ingreso
 * @param num_ingreso
 * @param fecha_creacion_ingreso
 * @param dstrct
 * @param branch_code
 * @param bank_account_no
 * @param document_no
 * @return
 * @throws SQLException
 */
    public String setEgresoTsp ( String tipo_documento_ingreso, String num_ingreso,
                                    String fecha_creacion_ingreso,
                                    String dstrct , String branch_code,
                                    String bank_account_no, String document_no)throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql  = "";
        String            query    = "SQL_SET_INGRESO_TSP";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,tipo_documento_ingreso);
            st.setString(2,num_ingreso);
            st.setString(3,fecha_creacion_ingreso);
            st.setString(4,dstrct);
            st.setString(5,branch_code);
            st.setString(6,bank_account_no);
            st.setString(7,document_no);
           // st.execute();
           comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL EGRESO TSP CON EL NUMERO DE INGRESO DE FINTRA. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;
    }



/**
 *
 * @param reg_status
 * @param dstrct
 * @param tipo_documento
 * @param num_ingreso
 * @param codcli
 * @param nitcli
 * @param concepto
 * @param tipo_ingreso
 * @param fecha_consignacion
 * @param fecha_ingreso
 * @param branch_code
 * @param bank_account_no
 * @param codmoneda
 * @param agencia_ingreso
 * @param descripcion_ingreso
 * @param periodo
 * @param vlr_ingreso
 * @param vlr_ingreso_me
 * @param vlr_tasa
 * @param fecha_tasa
 * @param cant_item
 * @param transaccion
 * @param transaccion_anulacion
 * @param fecha_impresion
 * @param fecha_contabilizacion
 * @param fecha_anulacion_contabilizacion
 * @param fecha_anulacion
 * @param creation_user
 * @param creation_date
 * @param user_update
 * @param last_update
 * @param base
 * @param nro_consignacion
 * @param periodo_anulacion
 * @param cuenta
 * @param auxiliar
 * @param abc
 * @param tasa_dol_bol
 * @param saldo_ingreso
 * @param cmc
 * @param corficolombiana
 * @return
 * @throws SQLException
 */
    public String setIngreso (
            String reg_status, String dstrct, String tipo_documento, String num_ingreso, String codcli, String nitcli, String
            concepto, String tipo_ingreso, String fecha_consignacion, String fecha_ingreso, String branch_code, String
            bank_account_no, String codmoneda, String agencia_ingreso, String descripcion_ingreso, String
            periodo, double vlr_ingreso, double vlr_ingreso_me, double vlr_tasa, String fecha_tasa, int cant_item, int
            transaccion, int transaccion_anulacion, String fecha_impresion, String fecha_contabilizacion, String
            fecha_anulacion_contabilizacion, String fecha_anulacion, String creation_user, String
            creation_date, String user_update, String last_update, String base, String nro_consignacion, String
            periodo_anulacion, String cuenta, String auxiliar, String abc, double tasa_dol_bol, double saldo_ingreso, String
            cmc, String corficolombiana) throws SQLException{


        StringStatement st       = null;
        String comando_sql  = "";
        String            query    = "SQL_SET_INGRESO";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2;
            st.setString(1,reg_status);
            st.setString(2,dstrct); // character varying(15) NOT NULL DEFAULT ''::character varying,
            st.setString(3,tipo_documento); //  character varying(5) NOT NULL DEFAULT ''::character varying, -- Tipo de documento del Ingreso
            st.setString(4,num_ingreso); //  character varying(11) NOT NULL DEFAULT ''::character varying, -- numero de ingreso
            st.setString(5,codcli); //  character varying(10) NOT NULL DEFAULT ''::character varying, -- codigo del cliente
            st.setString(6,nitcli); //  character varying(15) DEFAULT ''::character varying, -- nit del cliente
            st.setString(7,concepto); //  character varying(30) NOT NULL DEFAULT ''::character varying, -- concepto de ingreso
            st.setString(8,tipo_ingreso); //  character varying(2) NOT NULL DEFAULT ''::character varying, -- tipo de ingreso 'C' cliente, 'M' miselaneo
            st.setString(9,fecha_consignacion); //  date NOT NULL DEFAULT '0099-01-01'::date, -- fecha en que se realizo la consignacion
            st.setString(10,fecha_ingreso); //  date NOT NULL DEFAULT '0099-01-01'::date, -- fecha en que se realiza el ingreso
            st.setString(11,branch_code); //  character varying(15) NOT NULL DEFAULT ''::character varying, -- banco de consignacion
            st.setString(12,bank_account_no); //  character varying(30) NOT NULL DEFAULT ''::character varying, -- sucursal del banco de consignacion
            st.setString(13,codmoneda); //  character varying(3) NOT NULL DEFAULT ''::character varying, -- moneda de la consignacion
            st.setString(14,agencia_ingreso); //  character varying(6) NOT NULL DEFAULT ''::character varying, -- agencia realiza el ingreso
            st.setString(15,descripcion_ingreso); //  text NOT NULL DEFAULT ''::character varying, -- descripcion de ingreso
            st.setString(16,periodo); //  character varying(6) NOT NULL DEFAULT ''::character varying,
            st.setDouble(17,vlr_ingreso); //  moneda, -- valor de ingreso
            st.setDouble(18,vlr_ingreso_me); //  moneda, -- valor de imgreso moneda extranjera
            st.setDouble(19,vlr_tasa); //  numeric(15,6) NOT NULL DEFAULT 0.0, -- valor de conversion
            st.setString(20,fecha_tasa); //  date NOT NULL DEFAULT '0099-01-01'::date, -- fecha de la tasa
            st.setInt(21,cant_item); //  numeric(4) DEFAULT 0, -- numero de items que existen en la tabla ingreso detalle
            st.setInt(22,transaccion); //  integer NOT NULL DEFAULT 0, -- Identificacion unica del comprobante en contabilidad
            st.setInt(23,transaccion_anulacion); //  integer NOT NULL DEFAULT 0,
            st.setString(24,fecha_impresion); //  timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone, -- fecha de impresion
            st.setString(25,fecha_contabilizacion); //  timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone, -- fecha de contabilizacion
            st.setString(26,fecha_anulacion_contabilizacion); //  timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone, -- fecha anulacion contabilizacion
            st.setString(27,fecha_anulacion); //  timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone, -- fecha anulacion
            st.setString(28,creation_user); //  character varying(15) NOT NULL DEFAULT ''::character varying,
            st.setString(29,creation_date); //  timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
            st.setString(30,user_update); //  character varying(15) NOT NULL DEFAULT ''::character varying,
            st.setString(31,last_update); //  timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
            st.setString(32,base); //  character varying(3) NOT NULL DEFAULT ''::character varying,
            st.setString(33,nro_consignacion); //  character varying(25) NOT NULL DEFAULT ''::character varying,
            st.setString(34,periodo_anulacion); //  character varying(6) NOT NULL DEFAULT ''::character varying,
            st.setString(35,cuenta); //  character varying(25) NOT NULL DEFAULT ''::character varying, -- Cuenta contable para las Nota Creditos
            st.setString(36,auxiliar); //  character varying(25) NOT NULL DEFAULT ''::character varying, -- Codigo tipo de subledger
            st.setString(37,abc); //  character varying(10) NOT NULL DEFAULT ''::character varying, -- Codigo ABC de las notas creditos del ingreso
            st.setDouble(38,tasa_dol_bol); //  numeric(15,6) NOT NULL DEFAULT 0, -- La tasa de conversi?�n de Dolares a Bolivares
            st.setDouble(39,saldo_ingreso); //  moneda NOT NULL DEFAULT 0, -- El valor del saldo del ingreso
            st.setString(40,cmc); //  character varying(5) DEFAULT '00'::character varying,
            st.setString(41,corficolombiana); //  character varying(1) DEFAULT ''::character varying,
            // st.execute();
           comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA INSERCION DE UN REGISTRO EN INGRESO. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return comando_sql;
    }


/**
 *
 * @param reg_status
 * @param dstrct
 * @param tipo_documento
 * @param num_ingreso
 * @param item
 * @param nitcli
 * @param valor_ingreso
 * @param valor_ingreso_me
 * @param factura
 * @param fecha_factura
 * @param codigo_retefuente
 * @param valor_retefuente
 * @param valor_retefuente_me
 * @param tipo_doc
 * @param documento
 * @param codigo_reteica
 * @param valor_reteica
 * @param valor_reteica_me
 * @param valor_diferencia_tasa
 * @param creation_user
 * @param creation_date
 * @param user_update
 * @param last_update
 * @param base
 * @param cuenta
 * @param auxiliar
 * @param fecha_contabilizacion
 * @param fecha_anulacion_contabilizacion
 * @param periodo
 * @param fecha_anulacion
 * @param periodo_anulacion
 * @param transaccion
 * @param transaccion_anulacion
 * @param descripcion
 * @param valor_tasa
 * @param saldo_factura
 * @return
 * @throws SQLException
 */
    public String setIngresoDetalle (String reg_status, String dstrct, String tipo_documento, String num_ingreso, int item, String nitcli, double
                        valor_ingreso, double valor_ingreso_me, String factura, String fecha_factura, String codigo_retefuente, double
                        valor_retefuente, double valor_retefuente_me, String tipo_doc, String documento, String codigo_reteica, double
                        valor_reteica, double valor_reteica_me, double valor_diferencia_tasa, String creation_user, String
                        creation_date, String user_update, String last_update, String base, String cuenta, String auxiliar, String
                        fecha_contabilizacion, String fecha_anulacion_contabilizacion, String periodo, String
                        fecha_anulacion, String periodo_anulacion, int transaccion, int transaccion_anulacion,
                        String descripcion, double valor_tasa, double saldo_factura) throws SQLException{


        StringStatement st       = null;
        String comando_sql  = "";
        String            query    = "SQL_SET_INGRESO_DETALLE";

        try {
          st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
          st.setString(1,reg_status) ;    //   character varying(1) NOT NULL DEFAULT ''::character varying,
          st.setString(2,dstrct) ;    //   character varying(4) NOT NULL DEFAULT ''::character varying,
          st.setString(3,tipo_documento) ;    //  _documento character varying(5) NOT NULL DEFAULT ''::character varying, -- Tipo de documento del ingreso
          st.setString(4,num_ingreso) ;    //   character varying(11) NOT NULL DEFAULT ''::character varying, -- Numero del ingreso del cliente
          st.setInt(5,item) ;    //   bigint NOT NULL DEFAULT 0, -- Consecutivo del ingreso del cliente
          st.setString(6,nitcli) ;    //   character varying(15) DEFAULT ''::character varying, -- NIT del cliente
          st.setDouble(7,valor_ingreso) ;    //   moneda NOT NULL DEFAULT 0, -- Valor del ingreso del cliente en moneda local
          st.setDouble(8,valor_ingreso_me) ;    //   moneda NOT NULL DEFAULT 0, -- Valor del ingreso del cliente en moneda extranjera
          st.setString(9,factura) ;    //   character varying(10) NOT NULL DEFAULT ''::character varying, -- Numero de la factura del cliente
          st.setString(10,fecha_factura) ;    //   date NOT NULL DEFAULT '0099-01-01'::date,
          st.setString(11,codigo_retefuente) ;    //   character varying(6) NOT NULL DEFAULT ''::character varying, -- Codigo del porcentaje de la retefuente
          st.setDouble(12,valor_retefuente) ;    //   moneda NOT NULL DEFAULT 0, -- Valor de la retefuente del cliente en moneda local
          st.setDouble(13,valor_retefuente_me) ;    //   moneda NOT NULL DEFAULT 0, -- Valor de la retefuente del cliente en moneda extranjera
          st.setString(14,tipo_doc) ;    //   character varying(5) NOT NULL DEFAULT ''::character varying, -- Tipo de documento del detalle del ingreso
          st.setString(15,documento) ;    //   text NOT NULL DEFAULT ''::character varying, -- Documento del detalle de ingreso
          st.setString(16,codigo_reteica) ;    //   character varying(6) NOT NULL DEFAULT ''::character varying, -- Codigo del porcentaje de la reteica
          st.setDouble(17,valor_reteica) ;    //   moneda NOT NULL DEFAULT 0, -- Valor de la reteica del cliente en moneda local
          st.setDouble(18,valor_reteica_me) ;    //   moneda NOT NULL DEFAULT 0, -- Valor de la reteica del cliente en moneda extranjera
          st.setDouble(19,valor_diferencia_tasa) ;    //   moneda NOT NULL DEFAULT 0, -- Valor de la diferencia de la tasa
          st.setString(20,creation_user) ;    //   character varying(15) NOT NULL DEFAULT ''::character varying,
          st.setString(21,creation_date) ;    //   timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
          st.setString(22,user_update) ;    //   character varying(15) NOT NULL DEFAULT ''::character varying,
          st.setString(23,last_update) ;    //   timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone,
          st.setString(24,base) ;    //   character varying(3) NOT NULL DEFAULT ''::character varying,
          st.setString(25,cuenta) ;    //   character varying(25) NOT NULL DEFAULT ''::character varying, -- Cuenta contable
          st.setString(26,auxiliar) ;    //   character varying(25) NOT NULL DEFAULT ''::character varying, -- Codigo contable auxiliar o Sublegers
          st.setString(27,fecha_contabilizacion) ;    //   timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone, -- fecha de contabilizacion
          st.setString(28,fecha_anulacion_contabilizacion) ;    //   timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone, -- fecha anulacion contabilizacion
          st.setString(29,periodo) ;    //   character varying(6) NOT NULL DEFAULT ''::character varying, -- periodo de contabilizacion del ingreso
          st.setString(30,fecha_anulacion) ;    //   timestamp without time zone NOT NULL DEFAULT '0099-01-01 00:00:00'::timestamp without time zone, -- fecha anulacion
          st.setString(31,periodo_anulacion) ;    //   character varying(6) NOT NULL DEFAULT ''::character varying, -- periodo de anulacion de contabilizacion del ingreso
          st.setInt(32,transaccion) ;    //   integer NOT NULL DEFAULT 0, -- Anulacion de la identificacion unica del comprobante en contabilidad
          st.setInt(33,transaccion_anulacion) ;    //   integer NOT NULL DEFAULT 0,
          st.setString(34,descripcion) ;    //   text NOT NULL DEFAULT ''::text,
          st.setDouble(35,valor_tasa) ;    //   numeric(15,10) NOT NULL DEFAULT 0, -- Valor de la tasa del ingreso
          st.setDouble(36,saldo_factura) ;    //   moneda NOT NULL DEFAULT 0, -- Valor del saldo de la factura al momneto del ingreso
            // st.execute();

           comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA INSERCION DE UN REGISTRO EN INGRESO DETALLE. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return comando_sql;
    }


/**
 *
 * @param tipo_operacion
 * @return
 * @throws SQLException
 */
   public String getCuentaTipoOperacion(String tipo_operacion)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet         rs       = null;
        String            cuenta   = "";
        String            query    = "SQL_GET_CUENTA_TIPO_OPERACION";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, tipo_operacion );
            rs = st.executeQuery();
            if(rs.next()){
                cuenta = rs.getString("cuenta");
            }
            }}catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA CUENTA CONTABLE PARA UN TIPO DE OPERACION . \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return cuenta;
    }




    public String setFactura ( double valor_ingreso, String dstrct, String tipo_documento, String documento,
                               String dstrct_ultimo_ingreso, String tipo_documento_ultimo_ingreso,
                               String num_ingreso_ultimo_ingreso, int item_ultimo_ingreso) throws SQLException{

        StringStatement st       = null;
        String comando_sql  = "";
        String            query    = "SQL_SET_FACTURA";
        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setDouble(1,valor_ingreso);
            st.setDouble(2,valor_ingreso);
            st.setDouble(3,valor_ingreso);
            st.setDouble(4,valor_ingreso);
            st.setString(5,dstrct_ultimo_ingreso);
            st.setString(6,tipo_documento_ultimo_ingreso);
            st.setString(7,num_ingreso_ultimo_ingreso);
            st.setInt(8,item_ultimo_ingreso);

            st.setString(9,dstrct);
            st.setString(10,tipo_documento);
            st.setString(11,documento);
            // st.execute();
           comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA CON LOS VALORES ABONADOS. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return comando_sql;
    }



/**
 *
 * @param valor_ingreso
 * @param dstrct
 * @param tipo_documento
 * @param documento
 * @param item
 * @param dstrct_ultimo_ingreso
 * @param tipo_documento_ultimo_ingreso
 * @param num_ingreso_ultimo_ingreso
 * @param item_ultimo_ingreso
 * @return
 * @throws SQLException
 */
    public String setFacturaDetalle ( double valor_ingreso, String dstrct, String tipo_documento, String documento, int item,
                                      String dstrct_ultimo_ingreso, String tipo_documento_ultimo_ingreso,
                                      String num_ingreso_ultimo_ingreso, int item_ultimo_ingreso) throws SQLException{

        StringStatement st       = null;
        Connection        con      = null;
        String comando_sql  = "";
        String            query    = "SQL_SET_FACTURA_DETALLE";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setDouble(1,valor_ingreso);
            st.setString(2,dstrct);
            st.setString(3,tipo_documento);
            st.setString(4,documento);
            st.setInt(5,item);

            // st.execute();

           comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println(e.getMessage());System.out.println("error:"+e.getMessage());e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA DETALLE DE FINTRA CON LOS VALORES ABONADOS. \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return comando_sql;
    }


}


