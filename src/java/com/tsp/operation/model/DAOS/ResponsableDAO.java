/*
 * ResponsableDAO.java
 *
 * Created on 28 de octubre de 2005, 04:11 PM
 */

package com.tsp.operation.model.DAOS;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Jose
 */
public class ResponsableDAO {
    private Responsable responsable;
    private Vector responsables; 
    
    private static final String SQL_INSERT = "insert into responsable (codigo, descripcion, rec_status, last_update, user_update, creation_date, creation_user, base) values "+
        "(?,?,'','now()',?, 'now()',?,?)";
    
    private static final String SQL_SERCH = "select * from responsable where codigo=? and rec_status != 'A'";
    
    private static final String SQL_LIST = "select * from responsable where rec_status != 'A' order by codigo";
    
    /** Creates a new instance of ResponsableDAO */
    public ResponsableDAO() {
    }
    public Responsable getResponsable() {
        return responsable;
    }
    
    public void setResponsable(Responsable responsable) {
        this.responsable = responsable;
    }
    
    public Vector getResponsables() {
        return responsables;
    }
    
    public void setResponsables(Vector codigo_discrepancias) {
        this.responsables = responsables;
    }
    
    public void insertResponsable() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_INSERT);
                st.setString(1,responsable.getCodigo());
                st.setString(2,responsable.getDescripcion());
                st.setString(3,responsable.getUsuario_creacion());
                st.setString(4,responsable.getUsuario_modificacion());
                st.setString(5,responsable.getBase());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL RESPONSABLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void searchResponsable(String cod)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_SERCH);
                st.setString(1,cod);
                rs= st.executeQuery();
                responsables = new Vector();
                while(rs.next()){
                    responsable = new Responsable();
                    responsable.setCodigo(rs.getString("codigo"));
                    responsable.setDescripcion(rs.getString("descripcion"));
                    responsables.add(responsable);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL RESPONSABLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }        
    }
    
    public boolean existResponsable(String cod) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement(SQL_SERCH);
                st.setString(1,cod);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL RESPONSABLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    public void listResponsable()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LIST);
                rs= st.executeQuery();
                responsables = new Vector();
                while(rs.next()){
                    responsable = new Responsable();
                    responsable.setCodigo(rs.getString("codigo"));
                    responsable.setDescripcion(rs.getString("descripcion"));
                    responsable.setBase(rs.getString("base"));
                    responsables.add(responsable);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL RESPONSABLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void updateResponsable() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update responsable set descripcion=?, last_update='now()', user_update=?, rec_status='' where codigo = ?");
                st.setString(1,responsable.getDescripcion());
                st.setString(2,responsable.getUsuario_modificacion());
                st.setString(3,responsable.getCodigo());                
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACI�N DEL RESPONSABLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void anularResponsable() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if (con != null){
                st = con.prepareStatement("update responsable set rec_status='A',last_update='now()', user_update=? where codigo = ?");
                st.setString(1,responsable.getUsuario_modificacion());
                st.setString(2,responsable.getCodigo());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL RESPONSABLE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void searchDetalleResponsables(String cod, String desc) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        responsables=null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            boolean sw = false;
            if(con!=null){
                st = con.prepareStatement("Select * from responsable where codigo like ? and descripcion like ? and rec_status != 'A'");
                st.setString(1, cod+"%");
                st.setString(2, desc+"%");
                rs = st.executeQuery();
                responsables = new Vector();
                while(rs.next()){
                    responsable = new Responsable();
                    responsable.setCodigo(rs.getString("codigo"));
                    responsable.setDescripcion(rs.getString("descripcion"));
                    responsable.setRec_status(rs.getString("rec_status"));
                    responsable.setUsuario_modificacion(rs.getString("user_update"));
                    responsable.setUsuario_creacion(rs.getString("creation_user"));
                    responsable.setBase(rs.getString("base"));
                    responsables.add(responsable);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS RESPONSABLES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
}
